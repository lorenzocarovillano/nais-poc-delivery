package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.PoliDao;
import it.accenture.jnais.commons.data.to.IPoli;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbspol0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.PoliIdbspol0;
import it.accenture.jnais.ws.redefines.PolAaDiffProrDflt;
import it.accenture.jnais.ws.redefines.PolDir1oVers;
import it.accenture.jnais.ws.redefines.PolDirEmis;
import it.accenture.jnais.ws.redefines.PolDirQuiet;
import it.accenture.jnais.ws.redefines.PolDirVersAgg;
import it.accenture.jnais.ws.redefines.PolDtApplzConv;
import it.accenture.jnais.ws.redefines.PolDtIniVldtConv;
import it.accenture.jnais.ws.redefines.PolDtPresc;
import it.accenture.jnais.ws.redefines.PolDtProp;
import it.accenture.jnais.ws.redefines.PolDtScad;
import it.accenture.jnais.ws.redefines.PolDurAa;
import it.accenture.jnais.ws.redefines.PolDurGg;
import it.accenture.jnais.ws.redefines.PolDurMm;
import it.accenture.jnais.ws.redefines.PolIdAccComm;
import it.accenture.jnais.ws.redefines.PolIdMoviChiu;
import it.accenture.jnais.ws.redefines.PolSpeMed;

/**Original name: IDBSPOL0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  07 DIC 2017.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbspol0 extends Program implements IPoli {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private PoliDao poliDao = new PoliDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbspol0Data ws = new Idbspol0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: POLI
    private PoliIdbspol0 poli;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSPOL0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, PoliIdbspol0 poli) {
        this.idsv0003 = idsv0003;
        this.poli = poli;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbspol0 getInstance() {
        return ((Idbspol0)Programs.getInstance(Idbspol0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSPOL0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSPOL0");
        // COB_CODE: MOVE 'POLI' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("POLI");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,DT_PROP
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_EMIS
        //                ,TP_POLI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DT_SCAD
        //                ,COD_PROD
        //                ,DT_INI_VLDT_PROD
        //                ,COD_CONV
        //                ,COD_RAMO
        //                ,DT_INI_VLDT_CONV
        //                ,DT_APPLZ_CONV
        //                ,TP_FRM_ASSVA
        //                ,TP_RGM_FISC
        //                ,FL_ESTAS
        //                ,FL_RSH_COMUN
        //                ,FL_RSH_COMUN_COND
        //                ,TP_LIV_GENZ_TIT
        //                ,FL_COP_FINANZ
        //                ,TP_APPLZ_DIR
        //                ,SPE_MED
        //                ,DIR_EMIS
        //                ,DIR_1O_VERS
        //                ,DIR_VERS_AGG
        //                ,COD_DVS
        //                ,FL_FNT_AZ
        //                ,FL_FNT_ADER
        //                ,FL_FNT_TFR
        //                ,FL_FNT_VOLO
        //                ,TP_OPZ_A_SCAD
        //                ,AA_DIFF_PROR_DFLT
        //                ,FL_VER_PROD
        //                ,DUR_GG
        //                ,DIR_QUIET
        //                ,TP_PTF_ESTNO
        //                ,FL_CUM_PRE_CNTR
        //                ,FL_AMMB_MOVI
        //                ,CONV_GECO
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_SCUDO_FISC
        //                ,FL_TRASFE
        //                ,FL_TFR_STRC
        //                ,DT_PRESC
        //                ,COD_CONV_AGG
        //                ,SUBCAT_PROD
        //                ,FL_QUEST_ADEGZ_ASS
        //                ,COD_TPA
        //                ,ID_ACC_COMM
        //                ,FL_POLI_CPI_PR
        //                ,FL_POLI_BUNDLING
        //                ,IND_POLI_PRIN_COLL
        //                ,FL_VND_BUNDLE
        //                ,IB_BS
        //                ,FL_POLI_IFP
        //             INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //             FROM POLI
        //             WHERE     DS_RIGA = :POL-DS-RIGA
        //           END-EXEC.
        poliDao.selectByPolDsRiga(poli.getPolDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO POLI
            //                  (
            //                     ID_POLI
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,IB_OGG
            //                    ,IB_PROP
            //                    ,DT_PROP
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,DT_DECOR
            //                    ,DT_EMIS
            //                    ,TP_POLI
            //                    ,DUR_AA
            //                    ,DUR_MM
            //                    ,DT_SCAD
            //                    ,COD_PROD
            //                    ,DT_INI_VLDT_PROD
            //                    ,COD_CONV
            //                    ,COD_RAMO
            //                    ,DT_INI_VLDT_CONV
            //                    ,DT_APPLZ_CONV
            //                    ,TP_FRM_ASSVA
            //                    ,TP_RGM_FISC
            //                    ,FL_ESTAS
            //                    ,FL_RSH_COMUN
            //                    ,FL_RSH_COMUN_COND
            //                    ,TP_LIV_GENZ_TIT
            //                    ,FL_COP_FINANZ
            //                    ,TP_APPLZ_DIR
            //                    ,SPE_MED
            //                    ,DIR_EMIS
            //                    ,DIR_1O_VERS
            //                    ,DIR_VERS_AGG
            //                    ,COD_DVS
            //                    ,FL_FNT_AZ
            //                    ,FL_FNT_ADER
            //                    ,FL_FNT_TFR
            //                    ,FL_FNT_VOLO
            //                    ,TP_OPZ_A_SCAD
            //                    ,AA_DIFF_PROR_DFLT
            //                    ,FL_VER_PROD
            //                    ,DUR_GG
            //                    ,DIR_QUIET
            //                    ,TP_PTF_ESTNO
            //                    ,FL_CUM_PRE_CNTR
            //                    ,FL_AMMB_MOVI
            //                    ,CONV_GECO
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,FL_SCUDO_FISC
            //                    ,FL_TRASFE
            //                    ,FL_TFR_STRC
            //                    ,DT_PRESC
            //                    ,COD_CONV_AGG
            //                    ,SUBCAT_PROD
            //                    ,FL_QUEST_ADEGZ_ASS
            //                    ,COD_TPA
            //                    ,ID_ACC_COMM
            //                    ,FL_POLI_CPI_PR
            //                    ,FL_POLI_BUNDLING
            //                    ,IND_POLI_PRIN_COLL
            //                    ,FL_VND_BUNDLE
            //                    ,IB_BS
            //                    ,FL_POLI_IFP
            //                  )
            //              VALUES
            //                  (
            //                    :POL-ID-POLI
            //                    ,:POL-ID-MOVI-CRZ
            //                    ,:POL-ID-MOVI-CHIU
            //                     :IND-POL-ID-MOVI-CHIU
            //                    ,:POL-IB-OGG
            //                     :IND-POL-IB-OGG
            //                    ,:POL-IB-PROP
            //                    ,:POL-DT-PROP-DB
            //                     :IND-POL-DT-PROP
            //                    ,:POL-DT-INI-EFF-DB
            //                    ,:POL-DT-END-EFF-DB
            //                    ,:POL-COD-COMP-ANIA
            //                    ,:POL-DT-DECOR-DB
            //                    ,:POL-DT-EMIS-DB
            //                    ,:POL-TP-POLI
            //                    ,:POL-DUR-AA
            //                     :IND-POL-DUR-AA
            //                    ,:POL-DUR-MM
            //                     :IND-POL-DUR-MM
            //                    ,:POL-DT-SCAD-DB
            //                     :IND-POL-DT-SCAD
            //                    ,:POL-COD-PROD
            //                    ,:POL-DT-INI-VLDT-PROD-DB
            //                    ,:POL-COD-CONV
            //                     :IND-POL-COD-CONV
            //                    ,:POL-COD-RAMO
            //                     :IND-POL-COD-RAMO
            //                    ,:POL-DT-INI-VLDT-CONV-DB
            //                     :IND-POL-DT-INI-VLDT-CONV
            //                    ,:POL-DT-APPLZ-CONV-DB
            //                     :IND-POL-DT-APPLZ-CONV
            //                    ,:POL-TP-FRM-ASSVA
            //                    ,:POL-TP-RGM-FISC
            //                     :IND-POL-TP-RGM-FISC
            //                    ,:POL-FL-ESTAS
            //                     :IND-POL-FL-ESTAS
            //                    ,:POL-FL-RSH-COMUN
            //                     :IND-POL-FL-RSH-COMUN
            //                    ,:POL-FL-RSH-COMUN-COND
            //                     :IND-POL-FL-RSH-COMUN-COND
            //                    ,:POL-TP-LIV-GENZ-TIT
            //                    ,:POL-FL-COP-FINANZ
            //                     :IND-POL-FL-COP-FINANZ
            //                    ,:POL-TP-APPLZ-DIR
            //                     :IND-POL-TP-APPLZ-DIR
            //                    ,:POL-SPE-MED
            //                     :IND-POL-SPE-MED
            //                    ,:POL-DIR-EMIS
            //                     :IND-POL-DIR-EMIS
            //                    ,:POL-DIR-1O-VERS
            //                     :IND-POL-DIR-1O-VERS
            //                    ,:POL-DIR-VERS-AGG
            //                     :IND-POL-DIR-VERS-AGG
            //                    ,:POL-COD-DVS
            //                     :IND-POL-COD-DVS
            //                    ,:POL-FL-FNT-AZ
            //                     :IND-POL-FL-FNT-AZ
            //                    ,:POL-FL-FNT-ADER
            //                     :IND-POL-FL-FNT-ADER
            //                    ,:POL-FL-FNT-TFR
            //                     :IND-POL-FL-FNT-TFR
            //                    ,:POL-FL-FNT-VOLO
            //                     :IND-POL-FL-FNT-VOLO
            //                    ,:POL-TP-OPZ-A-SCAD
            //                     :IND-POL-TP-OPZ-A-SCAD
            //                    ,:POL-AA-DIFF-PROR-DFLT
            //                     :IND-POL-AA-DIFF-PROR-DFLT
            //                    ,:POL-FL-VER-PROD
            //                     :IND-POL-FL-VER-PROD
            //                    ,:POL-DUR-GG
            //                     :IND-POL-DUR-GG
            //                    ,:POL-DIR-QUIET
            //                     :IND-POL-DIR-QUIET
            //                    ,:POL-TP-PTF-ESTNO
            //                     :IND-POL-TP-PTF-ESTNO
            //                    ,:POL-FL-CUM-PRE-CNTR
            //                     :IND-POL-FL-CUM-PRE-CNTR
            //                    ,:POL-FL-AMMB-MOVI
            //                     :IND-POL-FL-AMMB-MOVI
            //                    ,:POL-CONV-GECO
            //                     :IND-POL-CONV-GECO
            //                    ,:POL-DS-RIGA
            //                    ,:POL-DS-OPER-SQL
            //                    ,:POL-DS-VER
            //                    ,:POL-DS-TS-INI-CPTZ
            //                    ,:POL-DS-TS-END-CPTZ
            //                    ,:POL-DS-UTENTE
            //                    ,:POL-DS-STATO-ELAB
            //                    ,:POL-FL-SCUDO-FISC
            //                     :IND-POL-FL-SCUDO-FISC
            //                    ,:POL-FL-TRASFE
            //                     :IND-POL-FL-TRASFE
            //                    ,:POL-FL-TFR-STRC
            //                     :IND-POL-FL-TFR-STRC
            //                    ,:POL-DT-PRESC-DB
            //                     :IND-POL-DT-PRESC
            //                    ,:POL-COD-CONV-AGG
            //                     :IND-POL-COD-CONV-AGG
            //                    ,:POL-SUBCAT-PROD
            //                     :IND-POL-SUBCAT-PROD
            //                    ,:POL-FL-QUEST-ADEGZ-ASS
            //                     :IND-POL-FL-QUEST-ADEGZ-ASS
            //                    ,:POL-COD-TPA
            //                     :IND-POL-COD-TPA
            //                    ,:POL-ID-ACC-COMM
            //                     :IND-POL-ID-ACC-COMM
            //                    ,:POL-FL-POLI-CPI-PR
            //                     :IND-POL-FL-POLI-CPI-PR
            //                    ,:POL-FL-POLI-BUNDLING
            //                     :IND-POL-FL-POLI-BUNDLING
            //                    ,:POL-IND-POLI-PRIN-COLL
            //                     :IND-POL-IND-POLI-PRIN-COLL
            //                    ,:POL-FL-VND-BUNDLE
            //                     :IND-POL-FL-VND-BUNDLE
            //                    ,:POL-IB-BS
            //                     :IND-POL-IB-BS
            //                    ,:POL-FL-POLI-IFP
            //                     :IND-POL-FL-POLI-IFP
            //                  )
            //           END-EXEC
            poliDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE POLI SET
        //                   ID_POLI                =
        //                :POL-ID-POLI
        //                  ,ID_MOVI_CRZ            =
        //                :POL-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :POL-ID-MOVI-CHIU
        //                                       :IND-POL-ID-MOVI-CHIU
        //                  ,IB_OGG                 =
        //                :POL-IB-OGG
        //                                       :IND-POL-IB-OGG
        //                  ,IB_PROP                =
        //                :POL-IB-PROP
        //                  ,DT_PROP                =
        //           :POL-DT-PROP-DB
        //                                       :IND-POL-DT-PROP
        //                  ,DT_INI_EFF             =
        //           :POL-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :POL-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :POL-COD-COMP-ANIA
        //                  ,DT_DECOR               =
        //           :POL-DT-DECOR-DB
        //                  ,DT_EMIS                =
        //           :POL-DT-EMIS-DB
        //                  ,TP_POLI                =
        //                :POL-TP-POLI
        //                  ,DUR_AA                 =
        //                :POL-DUR-AA
        //                                       :IND-POL-DUR-AA
        //                  ,DUR_MM                 =
        //                :POL-DUR-MM
        //                                       :IND-POL-DUR-MM
        //                  ,DT_SCAD                =
        //           :POL-DT-SCAD-DB
        //                                       :IND-POL-DT-SCAD
        //                  ,COD_PROD               =
        //                :POL-COD-PROD
        //                  ,DT_INI_VLDT_PROD       =
        //           :POL-DT-INI-VLDT-PROD-DB
        //                  ,COD_CONV               =
        //                :POL-COD-CONV
        //                                       :IND-POL-COD-CONV
        //                  ,COD_RAMO               =
        //                :POL-COD-RAMO
        //                                       :IND-POL-COD-RAMO
        //                  ,DT_INI_VLDT_CONV       =
        //           :POL-DT-INI-VLDT-CONV-DB
        //                                       :IND-POL-DT-INI-VLDT-CONV
        //                  ,DT_APPLZ_CONV          =
        //           :POL-DT-APPLZ-CONV-DB
        //                                       :IND-POL-DT-APPLZ-CONV
        //                  ,TP_FRM_ASSVA           =
        //                :POL-TP-FRM-ASSVA
        //                  ,TP_RGM_FISC            =
        //                :POL-TP-RGM-FISC
        //                                       :IND-POL-TP-RGM-FISC
        //                  ,FL_ESTAS               =
        //                :POL-FL-ESTAS
        //                                       :IND-POL-FL-ESTAS
        //                  ,FL_RSH_COMUN           =
        //                :POL-FL-RSH-COMUN
        //                                       :IND-POL-FL-RSH-COMUN
        //                  ,FL_RSH_COMUN_COND      =
        //                :POL-FL-RSH-COMUN-COND
        //                                       :IND-POL-FL-RSH-COMUN-COND
        //                  ,TP_LIV_GENZ_TIT        =
        //                :POL-TP-LIV-GENZ-TIT
        //                  ,FL_COP_FINANZ          =
        //                :POL-FL-COP-FINANZ
        //                                       :IND-POL-FL-COP-FINANZ
        //                  ,TP_APPLZ_DIR           =
        //                :POL-TP-APPLZ-DIR
        //                                       :IND-POL-TP-APPLZ-DIR
        //                  ,SPE_MED                =
        //                :POL-SPE-MED
        //                                       :IND-POL-SPE-MED
        //                  ,DIR_EMIS               =
        //                :POL-DIR-EMIS
        //                                       :IND-POL-DIR-EMIS
        //                  ,DIR_1O_VERS            =
        //                :POL-DIR-1O-VERS
        //                                       :IND-POL-DIR-1O-VERS
        //                  ,DIR_VERS_AGG           =
        //                :POL-DIR-VERS-AGG
        //                                       :IND-POL-DIR-VERS-AGG
        //                  ,COD_DVS                =
        //                :POL-COD-DVS
        //                                       :IND-POL-COD-DVS
        //                  ,FL_FNT_AZ              =
        //                :POL-FL-FNT-AZ
        //                                       :IND-POL-FL-FNT-AZ
        //                  ,FL_FNT_ADER            =
        //                :POL-FL-FNT-ADER
        //                                       :IND-POL-FL-FNT-ADER
        //                  ,FL_FNT_TFR             =
        //                :POL-FL-FNT-TFR
        //                                       :IND-POL-FL-FNT-TFR
        //                  ,FL_FNT_VOLO            =
        //                :POL-FL-FNT-VOLO
        //                                       :IND-POL-FL-FNT-VOLO
        //                  ,TP_OPZ_A_SCAD          =
        //                :POL-TP-OPZ-A-SCAD
        //                                       :IND-POL-TP-OPZ-A-SCAD
        //                  ,AA_DIFF_PROR_DFLT      =
        //                :POL-AA-DIFF-PROR-DFLT
        //                                       :IND-POL-AA-DIFF-PROR-DFLT
        //                  ,FL_VER_PROD            =
        //                :POL-FL-VER-PROD
        //                                       :IND-POL-FL-VER-PROD
        //                  ,DUR_GG                 =
        //                :POL-DUR-GG
        //                                       :IND-POL-DUR-GG
        //                  ,DIR_QUIET              =
        //                :POL-DIR-QUIET
        //                                       :IND-POL-DIR-QUIET
        //                  ,TP_PTF_ESTNO           =
        //                :POL-TP-PTF-ESTNO
        //                                       :IND-POL-TP-PTF-ESTNO
        //                  ,FL_CUM_PRE_CNTR        =
        //                :POL-FL-CUM-PRE-CNTR
        //                                       :IND-POL-FL-CUM-PRE-CNTR
        //                  ,FL_AMMB_MOVI           =
        //                :POL-FL-AMMB-MOVI
        //                                       :IND-POL-FL-AMMB-MOVI
        //                  ,CONV_GECO              =
        //                :POL-CONV-GECO
        //                                       :IND-POL-CONV-GECO
        //                  ,DS_RIGA                =
        //                :POL-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :POL-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :POL-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :POL-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :POL-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :POL-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :POL-DS-STATO-ELAB
        //                  ,FL_SCUDO_FISC          =
        //                :POL-FL-SCUDO-FISC
        //                                       :IND-POL-FL-SCUDO-FISC
        //                  ,FL_TRASFE              =
        //                :POL-FL-TRASFE
        //                                       :IND-POL-FL-TRASFE
        //                  ,FL_TFR_STRC            =
        //                :POL-FL-TFR-STRC
        //                                       :IND-POL-FL-TFR-STRC
        //                  ,DT_PRESC               =
        //           :POL-DT-PRESC-DB
        //                                       :IND-POL-DT-PRESC
        //                  ,COD_CONV_AGG           =
        //                :POL-COD-CONV-AGG
        //                                       :IND-POL-COD-CONV-AGG
        //                  ,SUBCAT_PROD            =
        //                :POL-SUBCAT-PROD
        //                                       :IND-POL-SUBCAT-PROD
        //                  ,FL_QUEST_ADEGZ_ASS     =
        //                :POL-FL-QUEST-ADEGZ-ASS
        //                                       :IND-POL-FL-QUEST-ADEGZ-ASS
        //                  ,COD_TPA                =
        //                :POL-COD-TPA
        //                                       :IND-POL-COD-TPA
        //                  ,ID_ACC_COMM            =
        //                :POL-ID-ACC-COMM
        //                                       :IND-POL-ID-ACC-COMM
        //                  ,FL_POLI_CPI_PR         =
        //                :POL-FL-POLI-CPI-PR
        //                                       :IND-POL-FL-POLI-CPI-PR
        //                  ,FL_POLI_BUNDLING       =
        //                :POL-FL-POLI-BUNDLING
        //                                       :IND-POL-FL-POLI-BUNDLING
        //                  ,IND_POLI_PRIN_COLL     =
        //                :POL-IND-POLI-PRIN-COLL
        //                                       :IND-POL-IND-POLI-PRIN-COLL
        //                  ,FL_VND_BUNDLE          =
        //                :POL-FL-VND-BUNDLE
        //                                       :IND-POL-FL-VND-BUNDLE
        //                  ,IB_BS                  =
        //                :POL-IB-BS
        //                                       :IND-POL-IB-BS
        //                  ,FL_POLI_IFP            =
        //                :POL-FL-POLI-IFP
        //                                       :IND-POL-FL-POLI-IFP
        //                WHERE     DS_RIGA = :POL-DS-RIGA
        //           END-EXEC.
        poliDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM POLI
        //                WHERE     DS_RIGA = :POL-DS-RIGA
        //           END-EXEC.
        poliDao.deleteByPolDsRiga(poli.getPolDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-POL CURSOR FOR
        //              SELECT
        //                     ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,IB_OGG
        //                    ,IB_PROP
        //                    ,DT_PROP
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_EMIS
        //                    ,TP_POLI
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DT_SCAD
        //                    ,COD_PROD
        //                    ,DT_INI_VLDT_PROD
        //                    ,COD_CONV
        //                    ,COD_RAMO
        //                    ,DT_INI_VLDT_CONV
        //                    ,DT_APPLZ_CONV
        //                    ,TP_FRM_ASSVA
        //                    ,TP_RGM_FISC
        //                    ,FL_ESTAS
        //                    ,FL_RSH_COMUN
        //                    ,FL_RSH_COMUN_COND
        //                    ,TP_LIV_GENZ_TIT
        //                    ,FL_COP_FINANZ
        //                    ,TP_APPLZ_DIR
        //                    ,SPE_MED
        //                    ,DIR_EMIS
        //                    ,DIR_1O_VERS
        //                    ,DIR_VERS_AGG
        //                    ,COD_DVS
        //                    ,FL_FNT_AZ
        //                    ,FL_FNT_ADER
        //                    ,FL_FNT_TFR
        //                    ,FL_FNT_VOLO
        //                    ,TP_OPZ_A_SCAD
        //                    ,AA_DIFF_PROR_DFLT
        //                    ,FL_VER_PROD
        //                    ,DUR_GG
        //                    ,DIR_QUIET
        //                    ,TP_PTF_ESTNO
        //                    ,FL_CUM_PRE_CNTR
        //                    ,FL_AMMB_MOVI
        //                    ,CONV_GECO
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_SCUDO_FISC
        //                    ,FL_TRASFE
        //                    ,FL_TFR_STRC
        //                    ,DT_PRESC
        //                    ,COD_CONV_AGG
        //                    ,SUBCAT_PROD
        //                    ,FL_QUEST_ADEGZ_ASS
        //                    ,COD_TPA
        //                    ,ID_ACC_COMM
        //                    ,FL_POLI_CPI_PR
        //                    ,FL_POLI_BUNDLING
        //                    ,IND_POLI_PRIN_COLL
        //                    ,FL_VND_BUNDLE
        //                    ,IB_BS
        //                    ,FL_POLI_IFP
        //              FROM POLI
        //              WHERE     ID_POLI = :POL-ID-POLI
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,DT_PROP
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_EMIS
        //                ,TP_POLI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DT_SCAD
        //                ,COD_PROD
        //                ,DT_INI_VLDT_PROD
        //                ,COD_CONV
        //                ,COD_RAMO
        //                ,DT_INI_VLDT_CONV
        //                ,DT_APPLZ_CONV
        //                ,TP_FRM_ASSVA
        //                ,TP_RGM_FISC
        //                ,FL_ESTAS
        //                ,FL_RSH_COMUN
        //                ,FL_RSH_COMUN_COND
        //                ,TP_LIV_GENZ_TIT
        //                ,FL_COP_FINANZ
        //                ,TP_APPLZ_DIR
        //                ,SPE_MED
        //                ,DIR_EMIS
        //                ,DIR_1O_VERS
        //                ,DIR_VERS_AGG
        //                ,COD_DVS
        //                ,FL_FNT_AZ
        //                ,FL_FNT_ADER
        //                ,FL_FNT_TFR
        //                ,FL_FNT_VOLO
        //                ,TP_OPZ_A_SCAD
        //                ,AA_DIFF_PROR_DFLT
        //                ,FL_VER_PROD
        //                ,DUR_GG
        //                ,DIR_QUIET
        //                ,TP_PTF_ESTNO
        //                ,FL_CUM_PRE_CNTR
        //                ,FL_AMMB_MOVI
        //                ,CONV_GECO
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_SCUDO_FISC
        //                ,FL_TRASFE
        //                ,FL_TFR_STRC
        //                ,DT_PRESC
        //                ,COD_CONV_AGG
        //                ,SUBCAT_PROD
        //                ,FL_QUEST_ADEGZ_ASS
        //                ,COD_TPA
        //                ,ID_ACC_COMM
        //                ,FL_POLI_CPI_PR
        //                ,FL_POLI_BUNDLING
        //                ,IND_POLI_PRIN_COLL
        //                ,FL_VND_BUNDLE
        //                ,IB_BS
        //                ,FL_POLI_IFP
        //             INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //             FROM POLI
        //             WHERE     ID_POLI = :POL-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        poliDao.selectRec(poli.getPolIdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE POLI SET
        //                   ID_POLI                =
        //                :POL-ID-POLI
        //                  ,ID_MOVI_CRZ            =
        //                :POL-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :POL-ID-MOVI-CHIU
        //                                       :IND-POL-ID-MOVI-CHIU
        //                  ,IB_OGG                 =
        //                :POL-IB-OGG
        //                                       :IND-POL-IB-OGG
        //                  ,IB_PROP                =
        //                :POL-IB-PROP
        //                  ,DT_PROP                =
        //           :POL-DT-PROP-DB
        //                                       :IND-POL-DT-PROP
        //                  ,DT_INI_EFF             =
        //           :POL-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :POL-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :POL-COD-COMP-ANIA
        //                  ,DT_DECOR               =
        //           :POL-DT-DECOR-DB
        //                  ,DT_EMIS                =
        //           :POL-DT-EMIS-DB
        //                  ,TP_POLI                =
        //                :POL-TP-POLI
        //                  ,DUR_AA                 =
        //                :POL-DUR-AA
        //                                       :IND-POL-DUR-AA
        //                  ,DUR_MM                 =
        //                :POL-DUR-MM
        //                                       :IND-POL-DUR-MM
        //                  ,DT_SCAD                =
        //           :POL-DT-SCAD-DB
        //                                       :IND-POL-DT-SCAD
        //                  ,COD_PROD               =
        //                :POL-COD-PROD
        //                  ,DT_INI_VLDT_PROD       =
        //           :POL-DT-INI-VLDT-PROD-DB
        //                  ,COD_CONV               =
        //                :POL-COD-CONV
        //                                       :IND-POL-COD-CONV
        //                  ,COD_RAMO               =
        //                :POL-COD-RAMO
        //                                       :IND-POL-COD-RAMO
        //                  ,DT_INI_VLDT_CONV       =
        //           :POL-DT-INI-VLDT-CONV-DB
        //                                       :IND-POL-DT-INI-VLDT-CONV
        //                  ,DT_APPLZ_CONV          =
        //           :POL-DT-APPLZ-CONV-DB
        //                                       :IND-POL-DT-APPLZ-CONV
        //                  ,TP_FRM_ASSVA           =
        //                :POL-TP-FRM-ASSVA
        //                  ,TP_RGM_FISC            =
        //                :POL-TP-RGM-FISC
        //                                       :IND-POL-TP-RGM-FISC
        //                  ,FL_ESTAS               =
        //                :POL-FL-ESTAS
        //                                       :IND-POL-FL-ESTAS
        //                  ,FL_RSH_COMUN           =
        //                :POL-FL-RSH-COMUN
        //                                       :IND-POL-FL-RSH-COMUN
        //                  ,FL_RSH_COMUN_COND      =
        //                :POL-FL-RSH-COMUN-COND
        //                                       :IND-POL-FL-RSH-COMUN-COND
        //                  ,TP_LIV_GENZ_TIT        =
        //                :POL-TP-LIV-GENZ-TIT
        //                  ,FL_COP_FINANZ          =
        //                :POL-FL-COP-FINANZ
        //                                       :IND-POL-FL-COP-FINANZ
        //                  ,TP_APPLZ_DIR           =
        //                :POL-TP-APPLZ-DIR
        //                                       :IND-POL-TP-APPLZ-DIR
        //                  ,SPE_MED                =
        //                :POL-SPE-MED
        //                                       :IND-POL-SPE-MED
        //                  ,DIR_EMIS               =
        //                :POL-DIR-EMIS
        //                                       :IND-POL-DIR-EMIS
        //                  ,DIR_1O_VERS            =
        //                :POL-DIR-1O-VERS
        //                                       :IND-POL-DIR-1O-VERS
        //                  ,DIR_VERS_AGG           =
        //                :POL-DIR-VERS-AGG
        //                                       :IND-POL-DIR-VERS-AGG
        //                  ,COD_DVS                =
        //                :POL-COD-DVS
        //                                       :IND-POL-COD-DVS
        //                  ,FL_FNT_AZ              =
        //                :POL-FL-FNT-AZ
        //                                       :IND-POL-FL-FNT-AZ
        //                  ,FL_FNT_ADER            =
        //                :POL-FL-FNT-ADER
        //                                       :IND-POL-FL-FNT-ADER
        //                  ,FL_FNT_TFR             =
        //                :POL-FL-FNT-TFR
        //                                       :IND-POL-FL-FNT-TFR
        //                  ,FL_FNT_VOLO            =
        //                :POL-FL-FNT-VOLO
        //                                       :IND-POL-FL-FNT-VOLO
        //                  ,TP_OPZ_A_SCAD          =
        //                :POL-TP-OPZ-A-SCAD
        //                                       :IND-POL-TP-OPZ-A-SCAD
        //                  ,AA_DIFF_PROR_DFLT      =
        //                :POL-AA-DIFF-PROR-DFLT
        //                                       :IND-POL-AA-DIFF-PROR-DFLT
        //                  ,FL_VER_PROD            =
        //                :POL-FL-VER-PROD
        //                                       :IND-POL-FL-VER-PROD
        //                  ,DUR_GG                 =
        //                :POL-DUR-GG
        //                                       :IND-POL-DUR-GG
        //                  ,DIR_QUIET              =
        //                :POL-DIR-QUIET
        //                                       :IND-POL-DIR-QUIET
        //                  ,TP_PTF_ESTNO           =
        //                :POL-TP-PTF-ESTNO
        //                                       :IND-POL-TP-PTF-ESTNO
        //                  ,FL_CUM_PRE_CNTR        =
        //                :POL-FL-CUM-PRE-CNTR
        //                                       :IND-POL-FL-CUM-PRE-CNTR
        //                  ,FL_AMMB_MOVI           =
        //                :POL-FL-AMMB-MOVI
        //                                       :IND-POL-FL-AMMB-MOVI
        //                  ,CONV_GECO              =
        //                :POL-CONV-GECO
        //                                       :IND-POL-CONV-GECO
        //                  ,DS_RIGA                =
        //                :POL-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :POL-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :POL-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :POL-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :POL-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :POL-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :POL-DS-STATO-ELAB
        //                  ,FL_SCUDO_FISC          =
        //                :POL-FL-SCUDO-FISC
        //                                       :IND-POL-FL-SCUDO-FISC
        //                  ,FL_TRASFE              =
        //                :POL-FL-TRASFE
        //                                       :IND-POL-FL-TRASFE
        //                  ,FL_TFR_STRC            =
        //                :POL-FL-TFR-STRC
        //                                       :IND-POL-FL-TFR-STRC
        //                  ,DT_PRESC               =
        //           :POL-DT-PRESC-DB
        //                                       :IND-POL-DT-PRESC
        //                  ,COD_CONV_AGG           =
        //                :POL-COD-CONV-AGG
        //                                       :IND-POL-COD-CONV-AGG
        //                  ,SUBCAT_PROD            =
        //                :POL-SUBCAT-PROD
        //                                       :IND-POL-SUBCAT-PROD
        //                  ,FL_QUEST_ADEGZ_ASS     =
        //                :POL-FL-QUEST-ADEGZ-ASS
        //                                       :IND-POL-FL-QUEST-ADEGZ-ASS
        //                  ,COD_TPA                =
        //                :POL-COD-TPA
        //                                       :IND-POL-COD-TPA
        //                  ,ID_ACC_COMM            =
        //                :POL-ID-ACC-COMM
        //                                       :IND-POL-ID-ACC-COMM
        //                  ,FL_POLI_CPI_PR         =
        //                :POL-FL-POLI-CPI-PR
        //                                       :IND-POL-FL-POLI-CPI-PR
        //                  ,FL_POLI_BUNDLING       =
        //                :POL-FL-POLI-BUNDLING
        //                                       :IND-POL-FL-POLI-BUNDLING
        //                  ,IND_POLI_PRIN_COLL     =
        //                :POL-IND-POLI-PRIN-COLL
        //                                       :IND-POL-IND-POLI-PRIN-COLL
        //                  ,FL_VND_BUNDLE          =
        //                :POL-FL-VND-BUNDLE
        //                                       :IND-POL-FL-VND-BUNDLE
        //                  ,IB_BS                  =
        //                :POL-IB-BS
        //                                       :IND-POL-IB-BS
        //                  ,FL_POLI_IFP            =
        //                :POL-FL-POLI-IFP
        //                                       :IND-POL-FL-POLI-IFP
        //                WHERE     DS_RIGA = :POL-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        poliDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-POL
        //           END-EXEC.
        poliDao.openCIdUpdEffPol(poli.getPolIdPoli(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-POL
        //           END-EXEC.
        poliDao.closeCIdUpdEffPol();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-POL
        //           INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //           END-EXEC.
        poliDao.fetchCIdUpdEffPol(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-EFF-POL CURSOR FOR
        //              SELECT
        //                     ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,IB_OGG
        //                    ,IB_PROP
        //                    ,DT_PROP
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_EMIS
        //                    ,TP_POLI
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DT_SCAD
        //                    ,COD_PROD
        //                    ,DT_INI_VLDT_PROD
        //                    ,COD_CONV
        //                    ,COD_RAMO
        //                    ,DT_INI_VLDT_CONV
        //                    ,DT_APPLZ_CONV
        //                    ,TP_FRM_ASSVA
        //                    ,TP_RGM_FISC
        //                    ,FL_ESTAS
        //                    ,FL_RSH_COMUN
        //                    ,FL_RSH_COMUN_COND
        //                    ,TP_LIV_GENZ_TIT
        //                    ,FL_COP_FINANZ
        //                    ,TP_APPLZ_DIR
        //                    ,SPE_MED
        //                    ,DIR_EMIS
        //                    ,DIR_1O_VERS
        //                    ,DIR_VERS_AGG
        //                    ,COD_DVS
        //                    ,FL_FNT_AZ
        //                    ,FL_FNT_ADER
        //                    ,FL_FNT_TFR
        //                    ,FL_FNT_VOLO
        //                    ,TP_OPZ_A_SCAD
        //                    ,AA_DIFF_PROR_DFLT
        //                    ,FL_VER_PROD
        //                    ,DUR_GG
        //                    ,DIR_QUIET
        //                    ,TP_PTF_ESTNO
        //                    ,FL_CUM_PRE_CNTR
        //                    ,FL_AMMB_MOVI
        //                    ,CONV_GECO
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_SCUDO_FISC
        //                    ,FL_TRASFE
        //                    ,FL_TFR_STRC
        //                    ,DT_PRESC
        //                    ,COD_CONV_AGG
        //                    ,SUBCAT_PROD
        //                    ,FL_QUEST_ADEGZ_ASS
        //                    ,COD_TPA
        //                    ,ID_ACC_COMM
        //                    ,FL_POLI_CPI_PR
        //                    ,FL_POLI_BUNDLING
        //                    ,IND_POLI_PRIN_COLL
        //                    ,FL_VND_BUNDLE
        //                    ,IB_BS
        //                    ,FL_POLI_IFP
        //              FROM POLI
        //              WHERE     IB_OGG = :POL-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_POLI ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,DT_PROP
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_EMIS
        //                ,TP_POLI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DT_SCAD
        //                ,COD_PROD
        //                ,DT_INI_VLDT_PROD
        //                ,COD_CONV
        //                ,COD_RAMO
        //                ,DT_INI_VLDT_CONV
        //                ,DT_APPLZ_CONV
        //                ,TP_FRM_ASSVA
        //                ,TP_RGM_FISC
        //                ,FL_ESTAS
        //                ,FL_RSH_COMUN
        //                ,FL_RSH_COMUN_COND
        //                ,TP_LIV_GENZ_TIT
        //                ,FL_COP_FINANZ
        //                ,TP_APPLZ_DIR
        //                ,SPE_MED
        //                ,DIR_EMIS
        //                ,DIR_1O_VERS
        //                ,DIR_VERS_AGG
        //                ,COD_DVS
        //                ,FL_FNT_AZ
        //                ,FL_FNT_ADER
        //                ,FL_FNT_TFR
        //                ,FL_FNT_VOLO
        //                ,TP_OPZ_A_SCAD
        //                ,AA_DIFF_PROR_DFLT
        //                ,FL_VER_PROD
        //                ,DUR_GG
        //                ,DIR_QUIET
        //                ,TP_PTF_ESTNO
        //                ,FL_CUM_PRE_CNTR
        //                ,FL_AMMB_MOVI
        //                ,CONV_GECO
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_SCUDO_FISC
        //                ,FL_TRASFE
        //                ,FL_TFR_STRC
        //                ,DT_PRESC
        //                ,COD_CONV_AGG
        //                ,SUBCAT_PROD
        //                ,FL_QUEST_ADEGZ_ASS
        //                ,COD_TPA
        //                ,ID_ACC_COMM
        //                ,FL_POLI_CPI_PR
        //                ,FL_POLI_BUNDLING
        //                ,IND_POLI_PRIN_COLL
        //                ,FL_VND_BUNDLE
        //                ,IB_BS
        //                ,FL_POLI_IFP
        //             INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //             FROM POLI
        //             WHERE     IB_OGG = :POL-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        poliDao.selectRec1(poli.getPolIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-EFF-POL
        //           END-EXEC.
        poliDao.openCIboEffPol(poli.getPolIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-EFF-POL
        //           END-EXEC.
        poliDao.closeCIboEffPol();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-EFF-POL
        //           INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //           END-EXEC.
        poliDao.fetchCIboEffPol(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX
            a570CloseCursorIbo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A605-DCL-CUR-IBS-PROP<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DclCurIbsProp() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-EFF-POL-0 CURSOR FOR
    //              SELECT
    //                     ID_POLI
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,IB_OGG
    //                    ,IB_PROP
    //                    ,DT_PROP
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,DT_DECOR
    //                    ,DT_EMIS
    //                    ,TP_POLI
    //                    ,DUR_AA
    //                    ,DUR_MM
    //                    ,DT_SCAD
    //                    ,COD_PROD
    //                    ,DT_INI_VLDT_PROD
    //                    ,COD_CONV
    //                    ,COD_RAMO
    //                    ,DT_INI_VLDT_CONV
    //                    ,DT_APPLZ_CONV
    //                    ,TP_FRM_ASSVA
    //                    ,TP_RGM_FISC
    //                    ,FL_ESTAS
    //                    ,FL_RSH_COMUN
    //                    ,FL_RSH_COMUN_COND
    //                    ,TP_LIV_GENZ_TIT
    //                    ,FL_COP_FINANZ
    //                    ,TP_APPLZ_DIR
    //                    ,SPE_MED
    //                    ,DIR_EMIS
    //                    ,DIR_1O_VERS
    //                    ,DIR_VERS_AGG
    //                    ,COD_DVS
    //                    ,FL_FNT_AZ
    //                    ,FL_FNT_ADER
    //                    ,FL_FNT_TFR
    //                    ,FL_FNT_VOLO
    //                    ,TP_OPZ_A_SCAD
    //                    ,AA_DIFF_PROR_DFLT
    //                    ,FL_VER_PROD
    //                    ,DUR_GG
    //                    ,DIR_QUIET
    //                    ,TP_PTF_ESTNO
    //                    ,FL_CUM_PRE_CNTR
    //                    ,FL_AMMB_MOVI
    //                    ,CONV_GECO
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,FL_SCUDO_FISC
    //                    ,FL_TRASFE
    //                    ,FL_TFR_STRC
    //                    ,DT_PRESC
    //                    ,COD_CONV_AGG
    //                    ,SUBCAT_PROD
    //                    ,FL_QUEST_ADEGZ_ASS
    //                    ,COD_TPA
    //                    ,ID_ACC_COMM
    //                    ,FL_POLI_CPI_PR
    //                    ,FL_POLI_BUNDLING
    //                    ,IND_POLI_PRIN_COLL
    //                    ,FL_VND_BUNDLE
    //                    ,IB_BS
    //                    ,FL_POLI_IFP
    //              FROM POLI
    //              WHERE     IB_PROP = :POL-IB-PROP
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND ID_MOVI_CHIU IS NULL
    //              ORDER BY ID_POLI ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DCL-CUR-IBS-BS<br>*/
    private void a605DclCurIbsBs() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-EFF-POL-1 CURSOR FOR
    //              SELECT
    //                     ID_POLI
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,IB_OGG
    //                    ,IB_PROP
    //                    ,DT_PROP
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,DT_DECOR
    //                    ,DT_EMIS
    //                    ,TP_POLI
    //                    ,DUR_AA
    //                    ,DUR_MM
    //                    ,DT_SCAD
    //                    ,COD_PROD
    //                    ,DT_INI_VLDT_PROD
    //                    ,COD_CONV
    //                    ,COD_RAMO
    //                    ,DT_INI_VLDT_CONV
    //                    ,DT_APPLZ_CONV
    //                    ,TP_FRM_ASSVA
    //                    ,TP_RGM_FISC
    //                    ,FL_ESTAS
    //                    ,FL_RSH_COMUN
    //                    ,FL_RSH_COMUN_COND
    //                    ,TP_LIV_GENZ_TIT
    //                    ,FL_COP_FINANZ
    //                    ,TP_APPLZ_DIR
    //                    ,SPE_MED
    //                    ,DIR_EMIS
    //                    ,DIR_1O_VERS
    //                    ,DIR_VERS_AGG
    //                    ,COD_DVS
    //                    ,FL_FNT_AZ
    //                    ,FL_FNT_ADER
    //                    ,FL_FNT_TFR
    //                    ,FL_FNT_VOLO
    //                    ,TP_OPZ_A_SCAD
    //                    ,AA_DIFF_PROR_DFLT
    //                    ,FL_VER_PROD
    //                    ,DUR_GG
    //                    ,DIR_QUIET
    //                    ,TP_PTF_ESTNO
    //                    ,FL_CUM_PRE_CNTR
    //                    ,FL_AMMB_MOVI
    //                    ,CONV_GECO
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,FL_SCUDO_FISC
    //                    ,FL_TRASFE
    //                    ,FL_TFR_STRC
    //                    ,DT_PRESC
    //                    ,COD_CONV_AGG
    //                    ,SUBCAT_PROD
    //                    ,FL_QUEST_ADEGZ_ASS
    //                    ,COD_TPA
    //                    ,ID_ACC_COMM
    //                    ,FL_POLI_CPI_PR
    //                    ,FL_POLI_BUNDLING
    //                    ,IND_POLI_PRIN_COLL
    //                    ,FL_VND_BUNDLE
    //                    ,IB_BS
    //                    ,FL_POLI_IFP
    //              FROM POLI
    //              WHERE     IB_BS = :POL-IB-BS
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND ID_MOVI_CHIU IS NULL
    //              ORDER BY ID_POLI ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF POL-IB-PROP NOT = HIGH-VALUES
        //                  THRU A605-PROP-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(poli.getPolIbProp(), PoliIdbspol0.Len.POL_IB_PROP)) {
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-PROP
            //              THRU A605-PROP-EX
            a605DclCurIbsProp();
        }
        else if (!Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: IF POL-IB-BS NOT = HIGH-VALUES
            //                  THRU A605-BS-EX
            //           END-IF
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-BS
            //              THRU A605-BS-EX
            a605DclCurIbsBs();
        }
    }

    /**Original name: A610-SELECT-IBS-PROP<br>*/
    private void a610SelectIbsProp() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,DT_PROP
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_EMIS
        //                ,TP_POLI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DT_SCAD
        //                ,COD_PROD
        //                ,DT_INI_VLDT_PROD
        //                ,COD_CONV
        //                ,COD_RAMO
        //                ,DT_INI_VLDT_CONV
        //                ,DT_APPLZ_CONV
        //                ,TP_FRM_ASSVA
        //                ,TP_RGM_FISC
        //                ,FL_ESTAS
        //                ,FL_RSH_COMUN
        //                ,FL_RSH_COMUN_COND
        //                ,TP_LIV_GENZ_TIT
        //                ,FL_COP_FINANZ
        //                ,TP_APPLZ_DIR
        //                ,SPE_MED
        //                ,DIR_EMIS
        //                ,DIR_1O_VERS
        //                ,DIR_VERS_AGG
        //                ,COD_DVS
        //                ,FL_FNT_AZ
        //                ,FL_FNT_ADER
        //                ,FL_FNT_TFR
        //                ,FL_FNT_VOLO
        //                ,TP_OPZ_A_SCAD
        //                ,AA_DIFF_PROR_DFLT
        //                ,FL_VER_PROD
        //                ,DUR_GG
        //                ,DIR_QUIET
        //                ,TP_PTF_ESTNO
        //                ,FL_CUM_PRE_CNTR
        //                ,FL_AMMB_MOVI
        //                ,CONV_GECO
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_SCUDO_FISC
        //                ,FL_TRASFE
        //                ,FL_TFR_STRC
        //                ,DT_PRESC
        //                ,COD_CONV_AGG
        //                ,SUBCAT_PROD
        //                ,FL_QUEST_ADEGZ_ASS
        //                ,COD_TPA
        //                ,ID_ACC_COMM
        //                ,FL_POLI_CPI_PR
        //                ,FL_POLI_BUNDLING
        //                ,IND_POLI_PRIN_COLL
        //                ,FL_VND_BUNDLE
        //                ,IB_BS
        //                ,FL_POLI_IFP
        //             INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //             FROM POLI
        //             WHERE     IB_PROP = :POL-IB-PROP
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        poliDao.selectRec2(poli.getPolIbProp(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
    }

    /**Original name: A610-SELECT-IBS-BS<br>*/
    private void a610SelectIbsBs() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,DT_PROP
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_EMIS
        //                ,TP_POLI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DT_SCAD
        //                ,COD_PROD
        //                ,DT_INI_VLDT_PROD
        //                ,COD_CONV
        //                ,COD_RAMO
        //                ,DT_INI_VLDT_CONV
        //                ,DT_APPLZ_CONV
        //                ,TP_FRM_ASSVA
        //                ,TP_RGM_FISC
        //                ,FL_ESTAS
        //                ,FL_RSH_COMUN
        //                ,FL_RSH_COMUN_COND
        //                ,TP_LIV_GENZ_TIT
        //                ,FL_COP_FINANZ
        //                ,TP_APPLZ_DIR
        //                ,SPE_MED
        //                ,DIR_EMIS
        //                ,DIR_1O_VERS
        //                ,DIR_VERS_AGG
        //                ,COD_DVS
        //                ,FL_FNT_AZ
        //                ,FL_FNT_ADER
        //                ,FL_FNT_TFR
        //                ,FL_FNT_VOLO
        //                ,TP_OPZ_A_SCAD
        //                ,AA_DIFF_PROR_DFLT
        //                ,FL_VER_PROD
        //                ,DUR_GG
        //                ,DIR_QUIET
        //                ,TP_PTF_ESTNO
        //                ,FL_CUM_PRE_CNTR
        //                ,FL_AMMB_MOVI
        //                ,CONV_GECO
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_SCUDO_FISC
        //                ,FL_TRASFE
        //                ,FL_TFR_STRC
        //                ,DT_PRESC
        //                ,COD_CONV_AGG
        //                ,SUBCAT_PROD
        //                ,FL_QUEST_ADEGZ_ASS
        //                ,COD_TPA
        //                ,ID_ACC_COMM
        //                ,FL_POLI_CPI_PR
        //                ,FL_POLI_BUNDLING
        //                ,IND_POLI_PRIN_COLL
        //                ,FL_VND_BUNDLE
        //                ,IB_BS
        //                ,FL_POLI_IFP
        //             INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //             FROM POLI
        //             WHERE     IB_BS = :POL-IB-BS
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        poliDao.selectRec3(poli.getPolIbBs(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF POL-IB-PROP NOT = HIGH-VALUES
        //                  THRU A610-PROP-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(poli.getPolIbProp(), PoliIdbspol0.Len.POL_IB_PROP)) {
            // COB_CODE: PERFORM A610-SELECT-IBS-PROP
            //              THRU A610-PROP-EX
            a610SelectIbsProp();
        }
        else if (!Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: IF POL-IB-BS NOT = HIGH-VALUES
            //                  THRU A610-BS-EX
            //           END-IF
            // COB_CODE: PERFORM A610-SELECT-IBS-BS
            //              THRU A610-BS-EX
            a610SelectIbsBs();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: IF POL-IB-PROP NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(poli.getPolIbProp(), PoliIdbspol0.Len.POL_IB_PROP)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-EFF-POL-0
            //           END-EXEC
            poliDao.openCIbsEffPol0(poli.getPolIbProp(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        }
        else if (!Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: IF POL-IB-BS NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-EFF-POL-1
            //           END-EXEC
            poliDao.openCIbsEffPol1(poli.getPolIbBs(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: IF POL-IB-PROP NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(poli.getPolIbProp(), PoliIdbspol0.Len.POL_IB_PROP)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-EFF-POL-0
            //           END-EXEC
            poliDao.closeCIbsEffPol0();
        }
        else if (!Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: IF POL-IB-BS NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-EFF-POL-1
            //           END-EXEC
            poliDao.closeCIbsEffPol1();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FN-IBS-PROP<br>*/
    private void a690FnIbsProp() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-EFF-POL-0
        //           INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //           END-EXEC.
        poliDao.fetchCIbsEffPol0(this);
    }

    /**Original name: A690-FN-IBS-BS<br>*/
    private void a690FnIbsBs() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-EFF-POL-1
        //           INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //           END-EXEC.
        poliDao.fetchCIbsEffPol1(this);
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: IF POL-IB-PROP NOT = HIGH-VALUES
        //                  THRU A690-PROP-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(poli.getPolIbProp(), PoliIdbspol0.Len.POL_IB_PROP)) {
            // COB_CODE: PERFORM A690-FN-IBS-PROP
            //              THRU A690-PROP-EX
            a690FnIbsProp();
        }
        else if (!Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: IF POL-IB-BS NOT = HIGH-VALUES
            //                  THRU A690-BS-EX
            //           END-IF
            // COB_CODE: PERFORM A690-FN-IBS-BS
            //              THRU A690-BS-EX
            a690FnIbsBs();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX
            a670CloseCursorIbs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,DT_PROP
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_EMIS
        //                ,TP_POLI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DT_SCAD
        //                ,COD_PROD
        //                ,DT_INI_VLDT_PROD
        //                ,COD_CONV
        //                ,COD_RAMO
        //                ,DT_INI_VLDT_CONV
        //                ,DT_APPLZ_CONV
        //                ,TP_FRM_ASSVA
        //                ,TP_RGM_FISC
        //                ,FL_ESTAS
        //                ,FL_RSH_COMUN
        //                ,FL_RSH_COMUN_COND
        //                ,TP_LIV_GENZ_TIT
        //                ,FL_COP_FINANZ
        //                ,TP_APPLZ_DIR
        //                ,SPE_MED
        //                ,DIR_EMIS
        //                ,DIR_1O_VERS
        //                ,DIR_VERS_AGG
        //                ,COD_DVS
        //                ,FL_FNT_AZ
        //                ,FL_FNT_ADER
        //                ,FL_FNT_TFR
        //                ,FL_FNT_VOLO
        //                ,TP_OPZ_A_SCAD
        //                ,AA_DIFF_PROR_DFLT
        //                ,FL_VER_PROD
        //                ,DUR_GG
        //                ,DIR_QUIET
        //                ,TP_PTF_ESTNO
        //                ,FL_CUM_PRE_CNTR
        //                ,FL_AMMB_MOVI
        //                ,CONV_GECO
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_SCUDO_FISC
        //                ,FL_TRASFE
        //                ,FL_TFR_STRC
        //                ,DT_PRESC
        //                ,COD_CONV_AGG
        //                ,SUBCAT_PROD
        //                ,FL_QUEST_ADEGZ_ASS
        //                ,COD_TPA
        //                ,ID_ACC_COMM
        //                ,FL_POLI_CPI_PR
        //                ,FL_POLI_BUNDLING
        //                ,IND_POLI_PRIN_COLL
        //                ,FL_VND_BUNDLE
        //                ,IB_BS
        //                ,FL_POLI_IFP
        //             INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //             FROM POLI
        //             WHERE     ID_POLI = :POL-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        poliDao.selectRec4(poli.getPolIdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-CPZ-POL CURSOR FOR
        //              SELECT
        //                     ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,IB_OGG
        //                    ,IB_PROP
        //                    ,DT_PROP
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_EMIS
        //                    ,TP_POLI
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DT_SCAD
        //                    ,COD_PROD
        //                    ,DT_INI_VLDT_PROD
        //                    ,COD_CONV
        //                    ,COD_RAMO
        //                    ,DT_INI_VLDT_CONV
        //                    ,DT_APPLZ_CONV
        //                    ,TP_FRM_ASSVA
        //                    ,TP_RGM_FISC
        //                    ,FL_ESTAS
        //                    ,FL_RSH_COMUN
        //                    ,FL_RSH_COMUN_COND
        //                    ,TP_LIV_GENZ_TIT
        //                    ,FL_COP_FINANZ
        //                    ,TP_APPLZ_DIR
        //                    ,SPE_MED
        //                    ,DIR_EMIS
        //                    ,DIR_1O_VERS
        //                    ,DIR_VERS_AGG
        //                    ,COD_DVS
        //                    ,FL_FNT_AZ
        //                    ,FL_FNT_ADER
        //                    ,FL_FNT_TFR
        //                    ,FL_FNT_VOLO
        //                    ,TP_OPZ_A_SCAD
        //                    ,AA_DIFF_PROR_DFLT
        //                    ,FL_VER_PROD
        //                    ,DUR_GG
        //                    ,DIR_QUIET
        //                    ,TP_PTF_ESTNO
        //                    ,FL_CUM_PRE_CNTR
        //                    ,FL_AMMB_MOVI
        //                    ,CONV_GECO
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_SCUDO_FISC
        //                    ,FL_TRASFE
        //                    ,FL_TFR_STRC
        //                    ,DT_PRESC
        //                    ,COD_CONV_AGG
        //                    ,SUBCAT_PROD
        //                    ,FL_QUEST_ADEGZ_ASS
        //                    ,COD_TPA
        //                    ,ID_ACC_COMM
        //                    ,FL_POLI_CPI_PR
        //                    ,FL_POLI_BUNDLING
        //                    ,IND_POLI_PRIN_COLL
        //                    ,FL_VND_BUNDLE
        //                    ,IB_BS
        //                    ,FL_POLI_IFP
        //              FROM POLI
        //              WHERE     IB_OGG = :POL-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_POLI ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,DT_PROP
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_EMIS
        //                ,TP_POLI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DT_SCAD
        //                ,COD_PROD
        //                ,DT_INI_VLDT_PROD
        //                ,COD_CONV
        //                ,COD_RAMO
        //                ,DT_INI_VLDT_CONV
        //                ,DT_APPLZ_CONV
        //                ,TP_FRM_ASSVA
        //                ,TP_RGM_FISC
        //                ,FL_ESTAS
        //                ,FL_RSH_COMUN
        //                ,FL_RSH_COMUN_COND
        //                ,TP_LIV_GENZ_TIT
        //                ,FL_COP_FINANZ
        //                ,TP_APPLZ_DIR
        //                ,SPE_MED
        //                ,DIR_EMIS
        //                ,DIR_1O_VERS
        //                ,DIR_VERS_AGG
        //                ,COD_DVS
        //                ,FL_FNT_AZ
        //                ,FL_FNT_ADER
        //                ,FL_FNT_TFR
        //                ,FL_FNT_VOLO
        //                ,TP_OPZ_A_SCAD
        //                ,AA_DIFF_PROR_DFLT
        //                ,FL_VER_PROD
        //                ,DUR_GG
        //                ,DIR_QUIET
        //                ,TP_PTF_ESTNO
        //                ,FL_CUM_PRE_CNTR
        //                ,FL_AMMB_MOVI
        //                ,CONV_GECO
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_SCUDO_FISC
        //                ,FL_TRASFE
        //                ,FL_TFR_STRC
        //                ,DT_PRESC
        //                ,COD_CONV_AGG
        //                ,SUBCAT_PROD
        //                ,FL_QUEST_ADEGZ_ASS
        //                ,COD_TPA
        //                ,ID_ACC_COMM
        //                ,FL_POLI_CPI_PR
        //                ,FL_POLI_BUNDLING
        //                ,IND_POLI_PRIN_COLL
        //                ,FL_VND_BUNDLE
        //                ,IB_BS
        //                ,FL_POLI_IFP
        //             INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //             FROM POLI
        //             WHERE     IB_OGG = :POL-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        poliDao.selectRec5(poli.getPolIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-CPZ-POL
        //           END-EXEC.
        poliDao.openCIboCpzPol(poli.getPolIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-CPZ-POL
        //           END-EXEC.
        poliDao.closeCIboCpzPol();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-CPZ-POL
        //           INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //           END-EXEC.
        poliDao.fetchCIboCpzPol(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX
            b570CloseCursorIboCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B605-DCL-CUR-IBS-PROP<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DclCurIbsProp() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-CPZ-POL-0 CURSOR FOR
    //              SELECT
    //                     ID_POLI
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,IB_OGG
    //                    ,IB_PROP
    //                    ,DT_PROP
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,DT_DECOR
    //                    ,DT_EMIS
    //                    ,TP_POLI
    //                    ,DUR_AA
    //                    ,DUR_MM
    //                    ,DT_SCAD
    //                    ,COD_PROD
    //                    ,DT_INI_VLDT_PROD
    //                    ,COD_CONV
    //                    ,COD_RAMO
    //                    ,DT_INI_VLDT_CONV
    //                    ,DT_APPLZ_CONV
    //                    ,TP_FRM_ASSVA
    //                    ,TP_RGM_FISC
    //                    ,FL_ESTAS
    //                    ,FL_RSH_COMUN
    //                    ,FL_RSH_COMUN_COND
    //                    ,TP_LIV_GENZ_TIT
    //                    ,FL_COP_FINANZ
    //                    ,TP_APPLZ_DIR
    //                    ,SPE_MED
    //                    ,DIR_EMIS
    //                    ,DIR_1O_VERS
    //                    ,DIR_VERS_AGG
    //                    ,COD_DVS
    //                    ,FL_FNT_AZ
    //                    ,FL_FNT_ADER
    //                    ,FL_FNT_TFR
    //                    ,FL_FNT_VOLO
    //                    ,TP_OPZ_A_SCAD
    //                    ,AA_DIFF_PROR_DFLT
    //                    ,FL_VER_PROD
    //                    ,DUR_GG
    //                    ,DIR_QUIET
    //                    ,TP_PTF_ESTNO
    //                    ,FL_CUM_PRE_CNTR
    //                    ,FL_AMMB_MOVI
    //                    ,CONV_GECO
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,FL_SCUDO_FISC
    //                    ,FL_TRASFE
    //                    ,FL_TFR_STRC
    //                    ,DT_PRESC
    //                    ,COD_CONV_AGG
    //                    ,SUBCAT_PROD
    //                    ,FL_QUEST_ADEGZ_ASS
    //                    ,COD_TPA
    //                    ,ID_ACC_COMM
    //                    ,FL_POLI_CPI_PR
    //                    ,FL_POLI_BUNDLING
    //                    ,IND_POLI_PRIN_COLL
    //                    ,FL_VND_BUNDLE
    //                    ,IB_BS
    //                    ,FL_POLI_IFP
    //              FROM POLI
    //              WHERE     IB_PROP = :POL-IB-PROP
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DS_TS_INI_CPTZ <=
    //                         :WS-TS-COMPETENZA
    //                    AND DS_TS_END_CPTZ >
    //                         :WS-TS-COMPETENZA
    //              ORDER BY ID_POLI ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B605-DCL-CUR-IBS-BS<br>*/
    private void b605DclCurIbsBs() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-CPZ-POL-1 CURSOR FOR
    //              SELECT
    //                     ID_POLI
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,IB_OGG
    //                    ,IB_PROP
    //                    ,DT_PROP
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,DT_DECOR
    //                    ,DT_EMIS
    //                    ,TP_POLI
    //                    ,DUR_AA
    //                    ,DUR_MM
    //                    ,DT_SCAD
    //                    ,COD_PROD
    //                    ,DT_INI_VLDT_PROD
    //                    ,COD_CONV
    //                    ,COD_RAMO
    //                    ,DT_INI_VLDT_CONV
    //                    ,DT_APPLZ_CONV
    //                    ,TP_FRM_ASSVA
    //                    ,TP_RGM_FISC
    //                    ,FL_ESTAS
    //                    ,FL_RSH_COMUN
    //                    ,FL_RSH_COMUN_COND
    //                    ,TP_LIV_GENZ_TIT
    //                    ,FL_COP_FINANZ
    //                    ,TP_APPLZ_DIR
    //                    ,SPE_MED
    //                    ,DIR_EMIS
    //                    ,DIR_1O_VERS
    //                    ,DIR_VERS_AGG
    //                    ,COD_DVS
    //                    ,FL_FNT_AZ
    //                    ,FL_FNT_ADER
    //                    ,FL_FNT_TFR
    //                    ,FL_FNT_VOLO
    //                    ,TP_OPZ_A_SCAD
    //                    ,AA_DIFF_PROR_DFLT
    //                    ,FL_VER_PROD
    //                    ,DUR_GG
    //                    ,DIR_QUIET
    //                    ,TP_PTF_ESTNO
    //                    ,FL_CUM_PRE_CNTR
    //                    ,FL_AMMB_MOVI
    //                    ,CONV_GECO
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,FL_SCUDO_FISC
    //                    ,FL_TRASFE
    //                    ,FL_TFR_STRC
    //                    ,DT_PRESC
    //                    ,COD_CONV_AGG
    //                    ,SUBCAT_PROD
    //                    ,FL_QUEST_ADEGZ_ASS
    //                    ,COD_TPA
    //                    ,ID_ACC_COMM
    //                    ,FL_POLI_CPI_PR
    //                    ,FL_POLI_BUNDLING
    //                    ,IND_POLI_PRIN_COLL
    //                    ,FL_VND_BUNDLE
    //                    ,IB_BS
    //                    ,FL_POLI_IFP
    //              FROM POLI
    //              WHERE     IB_BS = :POL-IB-BS
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DS_TS_INI_CPTZ <=
    //                         :WS-TS-COMPETENZA
    //                    AND DS_TS_END_CPTZ >
    //                         :WS-TS-COMPETENZA
    //              ORDER BY ID_POLI ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF POL-IB-PROP NOT = HIGH-VALUES
        //                  THRU B605-PROP-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(poli.getPolIbProp(), PoliIdbspol0.Len.POL_IB_PROP)) {
            // COB_CODE: PERFORM B605-DCL-CUR-IBS-PROP
            //              THRU B605-PROP-EX
            b605DclCurIbsProp();
        }
        else if (!Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: IF POL-IB-BS NOT = HIGH-VALUES
            //                  THRU B605-BS-EX
            //           END-IF
            // COB_CODE: PERFORM B605-DCL-CUR-IBS-BS
            //              THRU B605-BS-EX
            b605DclCurIbsBs();
        }
    }

    /**Original name: B610-SELECT-IBS-PROP<br>*/
    private void b610SelectIbsProp() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,DT_PROP
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_EMIS
        //                ,TP_POLI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DT_SCAD
        //                ,COD_PROD
        //                ,DT_INI_VLDT_PROD
        //                ,COD_CONV
        //                ,COD_RAMO
        //                ,DT_INI_VLDT_CONV
        //                ,DT_APPLZ_CONV
        //                ,TP_FRM_ASSVA
        //                ,TP_RGM_FISC
        //                ,FL_ESTAS
        //                ,FL_RSH_COMUN
        //                ,FL_RSH_COMUN_COND
        //                ,TP_LIV_GENZ_TIT
        //                ,FL_COP_FINANZ
        //                ,TP_APPLZ_DIR
        //                ,SPE_MED
        //                ,DIR_EMIS
        //                ,DIR_1O_VERS
        //                ,DIR_VERS_AGG
        //                ,COD_DVS
        //                ,FL_FNT_AZ
        //                ,FL_FNT_ADER
        //                ,FL_FNT_TFR
        //                ,FL_FNT_VOLO
        //                ,TP_OPZ_A_SCAD
        //                ,AA_DIFF_PROR_DFLT
        //                ,FL_VER_PROD
        //                ,DUR_GG
        //                ,DIR_QUIET
        //                ,TP_PTF_ESTNO
        //                ,FL_CUM_PRE_CNTR
        //                ,FL_AMMB_MOVI
        //                ,CONV_GECO
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_SCUDO_FISC
        //                ,FL_TRASFE
        //                ,FL_TFR_STRC
        //                ,DT_PRESC
        //                ,COD_CONV_AGG
        //                ,SUBCAT_PROD
        //                ,FL_QUEST_ADEGZ_ASS
        //                ,COD_TPA
        //                ,ID_ACC_COMM
        //                ,FL_POLI_CPI_PR
        //                ,FL_POLI_BUNDLING
        //                ,IND_POLI_PRIN_COLL
        //                ,FL_VND_BUNDLE
        //                ,IB_BS
        //                ,FL_POLI_IFP
        //             INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //             FROM POLI
        //             WHERE     IB_PROP = :POL-IB-PROP
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        poliDao.selectRec6(poli.getPolIbProp(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
    }

    /**Original name: B610-SELECT-IBS-BS<br>*/
    private void b610SelectIbsBs() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,DT_PROP
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_EMIS
        //                ,TP_POLI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DT_SCAD
        //                ,COD_PROD
        //                ,DT_INI_VLDT_PROD
        //                ,COD_CONV
        //                ,COD_RAMO
        //                ,DT_INI_VLDT_CONV
        //                ,DT_APPLZ_CONV
        //                ,TP_FRM_ASSVA
        //                ,TP_RGM_FISC
        //                ,FL_ESTAS
        //                ,FL_RSH_COMUN
        //                ,FL_RSH_COMUN_COND
        //                ,TP_LIV_GENZ_TIT
        //                ,FL_COP_FINANZ
        //                ,TP_APPLZ_DIR
        //                ,SPE_MED
        //                ,DIR_EMIS
        //                ,DIR_1O_VERS
        //                ,DIR_VERS_AGG
        //                ,COD_DVS
        //                ,FL_FNT_AZ
        //                ,FL_FNT_ADER
        //                ,FL_FNT_TFR
        //                ,FL_FNT_VOLO
        //                ,TP_OPZ_A_SCAD
        //                ,AA_DIFF_PROR_DFLT
        //                ,FL_VER_PROD
        //                ,DUR_GG
        //                ,DIR_QUIET
        //                ,TP_PTF_ESTNO
        //                ,FL_CUM_PRE_CNTR
        //                ,FL_AMMB_MOVI
        //                ,CONV_GECO
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_SCUDO_FISC
        //                ,FL_TRASFE
        //                ,FL_TFR_STRC
        //                ,DT_PRESC
        //                ,COD_CONV_AGG
        //                ,SUBCAT_PROD
        //                ,FL_QUEST_ADEGZ_ASS
        //                ,COD_TPA
        //                ,ID_ACC_COMM
        //                ,FL_POLI_CPI_PR
        //                ,FL_POLI_BUNDLING
        //                ,IND_POLI_PRIN_COLL
        //                ,FL_VND_BUNDLE
        //                ,IB_BS
        //                ,FL_POLI_IFP
        //             INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //             FROM POLI
        //             WHERE     IB_BS = :POL-IB-BS
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        poliDao.selectRec7(poli.getPolIbBs(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF POL-IB-PROP NOT = HIGH-VALUES
        //                  THRU B610-PROP-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(poli.getPolIbProp(), PoliIdbspol0.Len.POL_IB_PROP)) {
            // COB_CODE: PERFORM B610-SELECT-IBS-PROP
            //              THRU B610-PROP-EX
            b610SelectIbsProp();
        }
        else if (!Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: IF POL-IB-BS NOT = HIGH-VALUES
            //                  THRU B610-BS-EX
            //           END-IF
            // COB_CODE: PERFORM B610-SELECT-IBS-BS
            //              THRU B610-BS-EX
            b610SelectIbsBs();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: IF POL-IB-PROP NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(poli.getPolIbProp(), PoliIdbspol0.Len.POL_IB_PROP)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-CPZ-POL-0
            //           END-EXEC
            poliDao.openCIbsCpzPol0(poli.getPolIbProp(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        }
        else if (!Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: IF POL-IB-BS NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-CPZ-POL-1
            //           END-EXEC
            poliDao.openCIbsCpzPol1(poli.getPolIbBs(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: IF POL-IB-PROP NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(poli.getPolIbProp(), PoliIdbspol0.Len.POL_IB_PROP)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-CPZ-POL-0
            //           END-EXEC
            poliDao.closeCIbsCpzPol0();
        }
        else if (!Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: IF POL-IB-BS NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-CPZ-POL-1
            //           END-EXEC
            poliDao.closeCIbsCpzPol1();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FN-IBS-PROP<br>*/
    private void b690FnIbsProp() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-CPZ-POL-0
        //           INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //           END-EXEC.
        poliDao.fetchCIbsCpzPol0(this);
    }

    /**Original name: B690-FN-IBS-BS<br>*/
    private void b690FnIbsBs() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-CPZ-POL-1
        //           INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //           END-EXEC.
        poliDao.fetchCIbsCpzPol1(this);
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: IF POL-IB-PROP NOT = HIGH-VALUES
        //                  THRU B690-PROP-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(poli.getPolIbProp(), PoliIdbspol0.Len.POL_IB_PROP)) {
            // COB_CODE: PERFORM B690-FN-IBS-PROP
            //              THRU B690-PROP-EX
            b690FnIbsProp();
        }
        else if (!Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: IF POL-IB-BS NOT = HIGH-VALUES
            //                  THRU B690-BS-EX
            //           END-IF
            // COB_CODE: PERFORM B690-FN-IBS-BS
            //              THRU B690-BS-EX
            b690FnIbsBs();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX
            b670CloseCursorIbsCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-POL-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndPoli().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
            poli.getPolIdMoviChiu().setPolIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdMoviChiu.Len.POL_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-POL-IB-OGG = -1
        //              MOVE HIGH-VALUES TO POL-IB-OGG-NULL
        //           END-IF
        if (ws.getIndPoli().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IB-OGG-NULL
            poli.setPolIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_IB_OGG));
        }
        // COB_CODE: IF IND-POL-DT-PROP = -1
        //              MOVE HIGH-VALUES TO POL-DT-PROP-NULL
        //           END-IF
        if (ws.getIndPoli().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-PROP-NULL
            poli.getPolDtProp().setPolDtPropNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtProp.Len.POL_DT_PROP_NULL));
        }
        // COB_CODE: IF IND-POL-DUR-AA = -1
        //              MOVE HIGH-VALUES TO POL-DUR-AA-NULL
        //           END-IF
        if (ws.getIndPoli().getPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-AA-NULL
            poli.getPolDurAa().setPolDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurAa.Len.POL_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-POL-DUR-MM = -1
        //              MOVE HIGH-VALUES TO POL-DUR-MM-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-MM-NULL
            poli.getPolDurMm().setPolDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurMm.Len.POL_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-POL-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
            poli.getPolDtScad().setPolDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtScad.Len.POL_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-POL-COD-CONV = -1
        //              MOVE HIGH-VALUES TO POL-COD-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-CONV-NULL
            poli.setPolCodConv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_CONV));
        }
        // COB_CODE: IF IND-POL-COD-RAMO = -1
        //              MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
            poli.setPolCodRamo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_RAMO));
        }
        // COB_CODE: IF IND-POL-DT-INI-VLDT-CONV = -1
        //              MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
            poli.getPolDtIniVldtConv().setPolDtIniVldtConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtIniVldtConv.Len.POL_DT_INI_VLDT_CONV_NULL));
        }
        // COB_CODE: IF IND-POL-DT-APPLZ-CONV = -1
        //              MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
            poli.getPolDtApplzConv().setPolDtApplzConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtApplzConv.Len.POL_DT_APPLZ_CONV_NULL));
        }
        // COB_CODE: IF IND-POL-TP-RGM-FISC = -1
        //              MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
        //           END-IF
        if (ws.getIndPoli().getTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
            poli.setPolTpRgmFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_RGM_FISC));
        }
        // COB_CODE: IF IND-POL-FL-ESTAS = -1
        //              MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
            poli.setPolFlEstas(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-RSH-COMUN = -1
        //              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
            poli.setPolFlRshComun(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-RSH-COMUN-COND = -1
        //              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
            poli.setPolFlRshComunCond(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-COP-FINANZ = -1
        //              MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
            poli.setPolFlCopFinanz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-TP-APPLZ-DIR = -1
        //              MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
            poli.setPolTpApplzDir(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_APPLZ_DIR));
        }
        // COB_CODE: IF IND-POL-SPE-MED = -1
        //              MOVE HIGH-VALUES TO POL-SPE-MED-NULL
        //           END-IF
        if (ws.getIndPoli().getPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-SPE-MED-NULL
            poli.getPolSpeMed().setPolSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolSpeMed.Len.POL_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-EMIS = -1
        //              MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
        //           END-IF
        if (ws.getIndPoli().getPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
            poli.getPolDirEmis().setPolDirEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirEmis.Len.POL_DIR_EMIS_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-1O-VERS = -1
        //              MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
        //           END-IF
        if (ws.getIndPoli().getPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
            poli.getPolDir1oVers().setPolDir1oVersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDir1oVers.Len.POL_DIR1O_VERS_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-VERS-AGG = -1
        //              MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
        //           END-IF
        if (ws.getIndPoli().getCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
            poli.getPolDirVersAgg().setPolDirVersAggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirVersAgg.Len.POL_DIR_VERS_AGG_NULL));
        }
        // COB_CODE: IF IND-POL-COD-DVS = -1
        //              MOVE HIGH-VALUES TO POL-COD-DVS-NULL
        //           END-IF
        if (ws.getIndPoli().getCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-DVS-NULL
            poli.setPolCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_DVS));
        }
        // COB_CODE: IF IND-POL-FL-FNT-AZ = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
        //           END-IF
        if (ws.getIndPoli().getCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
            poli.setPolFlFntAz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-ADER = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
        //           END-IF
        if (ws.getIndPoli().getProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
            poli.setPolFlFntAder(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-TFR = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
        //           END-IF
        if (ws.getIndPoli().getProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
            poli.setPolFlFntTfr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-VOLO = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
        //           END-IF
        if (ws.getIndPoli().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
            poli.setPolFlFntVolo(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-TP-OPZ-A-SCAD = -1
        //              MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
        //           END-IF
        if (ws.getIndPoli().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
            poli.setPolTpOpzAScad(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_OPZ_A_SCAD));
        }
        // COB_CODE: IF IND-POL-AA-DIFF-PROR-DFLT = -1
        //              MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
        //           END-IF
        if (ws.getIndPoli().getProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
            poli.getPolAaDiffProrDflt().setPolAaDiffProrDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolAaDiffProrDflt.Len.POL_AA_DIFF_PROR_DFLT_NULL));
        }
        // COB_CODE: IF IND-POL-FL-VER-PROD = -1
        //              MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
        //           END-IF
        if (ws.getIndPoli().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
            poli.setPolFlVerProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_FL_VER_PROD));
        }
        // COB_CODE: IF IND-POL-DUR-GG = -1
        //              MOVE HIGH-VALUES TO POL-DUR-GG-NULL
        //           END-IF
        if (ws.getIndPoli().getFrqMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-GG-NULL
            poli.getPolDurGg().setPolDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurGg.Len.POL_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-QUIET = -1
        //              MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
        //           END-IF
        if (ws.getIndPoli().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
            poli.getPolDirQuiet().setPolDirQuietNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirQuiet.Len.POL_DIR_QUIET_NULL));
        }
        // COB_CODE: IF IND-POL-TP-PTF-ESTNO = -1
        //              MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
        //           END-IF
        if (ws.getIndPoli().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
            poli.setPolTpPtfEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_PTF_ESTNO));
        }
        // COB_CODE: IF IND-POL-FL-CUM-PRE-CNTR = -1
        //              MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
        //           END-IF
        if (ws.getIndPoli().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
            poli.setPolFlCumPreCntr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-AMMB-MOVI = -1
        //              MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
            poli.setPolFlAmmbMovi(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-CONV-GECO = -1
        //              MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
        //           END-IF
        if (ws.getIndPoli().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
            poli.setPolConvGeco(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_CONV_GECO));
        }
        // COB_CODE: IF IND-POL-FL-SCUDO-FISC = -1
        //              MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
            poli.setPolFlScudoFisc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-TRASFE = -1
        //              MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
            poli.setPolFlTrasfe(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
            poli.setPolFlTfrStrc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-DT-PRESC = -1
        //              MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
        //           END-IF
        if (ws.getIndPoli().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
            poli.getPolDtPresc().setPolDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtPresc.Len.POL_DT_PRESC_NULL));
        }
        // COB_CODE: IF IND-POL-COD-CONV-AGG = -1
        //              MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
        //           END-IF
        if (ws.getIndPoli().getSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
            poli.setPolCodConvAgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_CONV_AGG));
        }
        // COB_CODE: IF IND-POL-SUBCAT-PROD = -1
        //              MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
        //           END-IF
        if (ws.getIndPoli().getCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
            poli.setPolSubcatProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_SUBCAT_PROD));
        }
        // COB_CODE: IF IND-POL-FL-QUEST-ADEGZ-ASS = -1
        //              MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
        //           END-IF
        if (ws.getIndPoli().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
            poli.setPolFlQuestAdegzAss(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-COD-TPA = -1
        //              MOVE HIGH-VALUES TO POL-COD-TPA-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-TPA-NULL
            poli.setPolCodTpa(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_TPA));
        }
        // COB_CODE: IF IND-POL-ID-ACC-COMM = -1
        //              MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
            poli.getPolIdAccComm().setPolIdAccCommNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdAccComm.Len.POL_ID_ACC_COMM_NULL));
        }
        // COB_CODE: IF IND-POL-FL-POLI-CPI-PR = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
        //           END-IF
        if (ws.getIndPoli().getNumGgRitardoPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
            poli.setPolFlPoliCpiPr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-POLI-BUNDLING = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
        //           END-IF
        if (ws.getIndPoli().getNumGgRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
            poli.setPolFlPoliBundling(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-IND-POLI-PRIN-COLL = -1
        //              MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
        //           END-IF
        if (ws.getIndPoli().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
            poli.setPolIndPoliPrinColl(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-VND-BUNDLE = -1
        //              MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
        //           END-IF
        if (ws.getIndPoli().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
            poli.setPolFlVndBundle(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-IB-BS = -1
        //              MOVE HIGH-VALUES TO POL-IB-BS-NULL
        //           END-IF
        if (ws.getIndPoli().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IB-BS-NULL
            poli.setPolIbBs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_IB_BS));
        }
        // COB_CODE: IF IND-POL-FL-POLI-IFP = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
        //           END-IF.
        if (ws.getIndPoli().getCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
            poli.setPolFlPoliIfp(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO POL-DS-OPER-SQL
        poli.setPolDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO POL-DS-VER
        poli.setPolDsVer(0);
        // COB_CODE: INITIALIZE WK-ID-OGG
        ws.setWkIdOggFormatted("000000000");
        // COB_CODE: MOVE POL-ID-POLI
        //             TO WK-ID-OGG
        ws.setWkIdOgg(TruncAbs.toInt(poli.getPolIdPoli(), 9));
        // COB_CODE: MOVE WK-ID-OGG(8:2)
        //             TO POL-DS-VER
        poli.setPolDsVerFormatted(ws.getWkIdOggFormatted().substring((8) - 1, 9));
        // COB_CODE: IF POL-DS-VER = 00
        //                TO POL-DS-VER
        //           END-IF
        if (poli.getPolDsVer() == 0) {
            // COB_CODE: MOVE 100
            //             TO POL-DS-VER
            poli.setPolDsVer(100);
        }
        // COB_CODE: MOVE IDSV0003-USER-NAME TO POL-DS-UTENTE
        poli.setPolDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO POL-DS-STATO-ELAB.
        poli.setPolDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO POL-DS-OPER-SQL
        poli.setPolDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO POL-DS-UTENTE.
        poli.setPolDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF POL-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-POL-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolIdMoviChiu().getPolIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-ID-MOVI-CHIU
            ws.getIndPoli().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-ID-MOVI-CHIU
            ws.getIndPoli().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF POL-IB-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-IB-OGG
        //           ELSE
        //              MOVE 0 TO IND-POL-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolIbOgg(), PoliIdbspol0.Len.POL_IB_OGG)) {
            // COB_CODE: MOVE -1 TO IND-POL-IB-OGG
            ws.getIndPoli().setDtIniCop(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-IB-OGG
            ws.getIndPoli().setDtIniCop(((short)0));
        }
        // COB_CODE: IF POL-DT-PROP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DT-PROP
        //           ELSE
        //              MOVE 0 TO IND-POL-DT-PROP
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDtProp().getPolDtPropNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DT-PROP
            ws.getIndPoli().setDtEndCop(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DT-PROP
            ws.getIndPoli().setDtEndCop(((short)0));
        }
        // COB_CODE: IF POL-DUR-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DUR-AA
        //           ELSE
        //              MOVE 0 TO IND-POL-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDurAa().getPolDurAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DUR-AA
            ws.getIndPoli().setPreNet(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DUR-AA
            ws.getIndPoli().setPreNet(((short)0));
        }
        // COB_CODE: IF POL-DUR-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DUR-MM
        //           ELSE
        //              MOVE 0 TO IND-POL-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDurMm().getPolDurMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DUR-MM
            ws.getIndPoli().setIntrFraz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DUR-MM
            ws.getIndPoli().setIntrFraz(((short)0));
        }
        // COB_CODE: IF POL-DT-SCAD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DT-SCAD
        //           ELSE
        //              MOVE 0 TO IND-POL-DT-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDtScad().getPolDtScadNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DT-SCAD
            ws.getIndPoli().setIntrMora(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DT-SCAD
            ws.getIndPoli().setIntrMora(((short)0));
        }
        // COB_CODE: IF POL-COD-CONV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-COD-CONV
        //           ELSE
        //              MOVE 0 TO IND-POL-COD-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolCodConvFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-COD-CONV
            ws.getIndPoli().setIntrRetdt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-COD-CONV
            ws.getIndPoli().setIntrRetdt(((short)0));
        }
        // COB_CODE: IF POL-COD-RAMO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-COD-RAMO
        //           ELSE
        //              MOVE 0 TO IND-POL-COD-RAMO
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolCodRamoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-COD-RAMO
            ws.getIndPoli().setIntrRiat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-COD-RAMO
            ws.getIndPoli().setIntrRiat(((short)0));
        }
        // COB_CODE: IF POL-DT-INI-VLDT-CONV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DT-INI-VLDT-CONV
        //           ELSE
        //              MOVE 0 TO IND-POL-DT-INI-VLDT-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDtIniVldtConv().getPolDtIniVldtConvNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DT-INI-VLDT-CONV
            ws.getIndPoli().setDir(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DT-INI-VLDT-CONV
            ws.getIndPoli().setDir(((short)0));
        }
        // COB_CODE: IF POL-DT-APPLZ-CONV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DT-APPLZ-CONV
        //           ELSE
        //              MOVE 0 TO IND-POL-DT-APPLZ-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDtApplzConv().getPolDtApplzConvNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DT-APPLZ-CONV
            ws.getIndPoli().setSpeMed(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DT-APPLZ-CONV
            ws.getIndPoli().setSpeMed(((short)0));
        }
        // COB_CODE: IF POL-TP-RGM-FISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-TP-RGM-FISC
        //           ELSE
        //              MOVE 0 TO IND-POL-TP-RGM-FISC
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolTpRgmFiscFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-TP-RGM-FISC
            ws.getIndPoli().setTax(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-TP-RGM-FISC
            ws.getIndPoli().setTax(((short)0));
        }
        // COB_CODE: IF POL-FL-ESTAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-ESTAS
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-ESTAS
        //           END-IF
        if (Conditions.eq(poli.getPolFlEstas(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-ESTAS
            ws.getIndPoli().setSoprSan(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-ESTAS
            ws.getIndPoli().setSoprSan(((short)0));
        }
        // COB_CODE: IF POL-FL-RSH-COMUN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-RSH-COMUN
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-RSH-COMUN
        //           END-IF
        if (Conditions.eq(poli.getPolFlRshComun(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-RSH-COMUN
            ws.getIndPoli().setSoprSpo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-RSH-COMUN
            ws.getIndPoli().setSoprSpo(((short)0));
        }
        // COB_CODE: IF POL-FL-RSH-COMUN-COND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-RSH-COMUN-COND
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-RSH-COMUN-COND
        //           END-IF
        if (Conditions.eq(poli.getPolFlRshComunCond(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-RSH-COMUN-COND
            ws.getIndPoli().setSoprTec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-RSH-COMUN-COND
            ws.getIndPoli().setSoprTec(((short)0));
        }
        // COB_CODE: IF POL-FL-COP-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-COP-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-COP-FINANZ
        //           END-IF
        if (Conditions.eq(poli.getPolFlCopFinanz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-COP-FINANZ
            ws.getIndPoli().setSoprProf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-COP-FINANZ
            ws.getIndPoli().setSoprProf(((short)0));
        }
        // COB_CODE: IF POL-TP-APPLZ-DIR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-TP-APPLZ-DIR
        //           ELSE
        //              MOVE 0 TO IND-POL-TP-APPLZ-DIR
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolTpApplzDirFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-TP-APPLZ-DIR
            ws.getIndPoli().setSoprAlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-TP-APPLZ-DIR
            ws.getIndPoli().setSoprAlt(((short)0));
        }
        // COB_CODE: IF POL-SPE-MED-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-SPE-MED
        //           ELSE
        //              MOVE 0 TO IND-POL-SPE-MED
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolSpeMed().getPolSpeMedNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-SPE-MED
            ws.getIndPoli().setPreTot(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-SPE-MED
            ws.getIndPoli().setPreTot(((short)0));
        }
        // COB_CODE: IF POL-DIR-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DIR-EMIS
        //           ELSE
        //              MOVE 0 TO IND-POL-DIR-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDirEmis().getPolDirEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DIR-EMIS
            ws.getIndPoli().setPrePpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DIR-EMIS
            ws.getIndPoli().setPrePpIas(((short)0));
        }
        // COB_CODE: IF POL-DIR-1O-VERS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DIR-1O-VERS
        //           ELSE
        //              MOVE 0 TO IND-POL-DIR-1O-VERS
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDir1oVers().getPolDir1oVersNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DIR-1O-VERS
            ws.getIndPoli().setPreSoloRsh(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DIR-1O-VERS
            ws.getIndPoli().setPreSoloRsh(((short)0));
        }
        // COB_CODE: IF POL-DIR-VERS-AGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DIR-VERS-AGG
        //           ELSE
        //              MOVE 0 TO IND-POL-DIR-VERS-AGG
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDirVersAgg().getPolDirVersAggNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DIR-VERS-AGG
            ws.getIndPoli().setCarAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DIR-VERS-AGG
            ws.getIndPoli().setCarAcq(((short)0));
        }
        // COB_CODE: IF POL-COD-DVS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-COD-DVS
        //           ELSE
        //              MOVE 0 TO IND-POL-COD-DVS
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolCodDvsFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-COD-DVS
            ws.getIndPoli().setCarGest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-COD-DVS
            ws.getIndPoli().setCarGest(((short)0));
        }
        // COB_CODE: IF POL-FL-FNT-AZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-FNT-AZ
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-FNT-AZ
        //           END-IF
        if (Conditions.eq(poli.getPolFlFntAz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-FNT-AZ
            ws.getIndPoli().setCarInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-FNT-AZ
            ws.getIndPoli().setCarInc(((short)0));
        }
        // COB_CODE: IF POL-FL-FNT-ADER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-FNT-ADER
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-FNT-ADER
        //           END-IF
        if (Conditions.eq(poli.getPolFlFntAder(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-FNT-ADER
            ws.getIndPoli().setProvAcq1aa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-FNT-ADER
            ws.getIndPoli().setProvAcq1aa(((short)0));
        }
        // COB_CODE: IF POL-FL-FNT-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-FNT-TFR
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-FNT-TFR
        //           END-IF
        if (Conditions.eq(poli.getPolFlFntTfr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-FNT-TFR
            ws.getIndPoli().setProvAcq2aa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-FNT-TFR
            ws.getIndPoli().setProvAcq2aa(((short)0));
        }
        // COB_CODE: IF POL-FL-FNT-VOLO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-FNT-VOLO
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-FNT-VOLO
        //           END-IF
        if (Conditions.eq(poli.getPolFlFntVolo(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-FNT-VOLO
            ws.getIndPoli().setProvRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-FNT-VOLO
            ws.getIndPoli().setProvRicor(((short)0));
        }
        // COB_CODE: IF POL-TP-OPZ-A-SCAD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-TP-OPZ-A-SCAD
        //           ELSE
        //              MOVE 0 TO IND-POL-TP-OPZ-A-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolTpOpzAScadFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-TP-OPZ-A-SCAD
            ws.getIndPoli().setProvInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-TP-OPZ-A-SCAD
            ws.getIndPoli().setProvInc(((short)0));
        }
        // COB_CODE: IF POL-AA-DIFF-PROR-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-AA-DIFF-PROR-DFLT
        //           ELSE
        //              MOVE 0 TO IND-POL-AA-DIFF-PROR-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolAaDiffProrDflt().getPolAaDiffProrDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-AA-DIFF-PROR-DFLT
            ws.getIndPoli().setProvDaRec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-AA-DIFF-PROR-DFLT
            ws.getIndPoli().setProvDaRec(((short)0));
        }
        // COB_CODE: IF POL-FL-VER-PROD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-VER-PROD
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-VER-PROD
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolFlVerProdFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-VER-PROD
            ws.getIndPoli().setCodDvs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-VER-PROD
            ws.getIndPoli().setCodDvs(((short)0));
        }
        // COB_CODE: IF POL-DUR-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DUR-GG
        //           ELSE
        //              MOVE 0 TO IND-POL-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDurGg().getPolDurGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DUR-GG
            ws.getIndPoli().setFrqMovi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DUR-GG
            ws.getIndPoli().setFrqMovi(((short)0));
        }
        // COB_CODE: IF POL-DIR-QUIET-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DIR-QUIET
        //           ELSE
        //              MOVE 0 TO IND-POL-DIR-QUIET
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDirQuiet().getPolDirQuietNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DIR-QUIET
            ws.getIndPoli().setCodTari(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DIR-QUIET
            ws.getIndPoli().setCodTari(((short)0));
        }
        // COB_CODE: IF POL-TP-PTF-ESTNO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-TP-PTF-ESTNO
        //           ELSE
        //              MOVE 0 TO IND-POL-TP-PTF-ESTNO
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolTpPtfEstnoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-TP-PTF-ESTNO
            ws.getIndPoli().setImpAz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-TP-PTF-ESTNO
            ws.getIndPoli().setImpAz(((short)0));
        }
        // COB_CODE: IF POL-FL-CUM-PRE-CNTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-CUM-PRE-CNTR
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-CUM-PRE-CNTR
        //           END-IF
        if (Conditions.eq(poli.getPolFlCumPreCntr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-CUM-PRE-CNTR
            ws.getIndPoli().setImpAder(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-CUM-PRE-CNTR
            ws.getIndPoli().setImpAder(((short)0));
        }
        // COB_CODE: IF POL-FL-AMMB-MOVI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-AMMB-MOVI
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-AMMB-MOVI
        //           END-IF
        if (Conditions.eq(poli.getPolFlAmmbMovi(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-AMMB-MOVI
            ws.getIndPoli().setImpTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-AMMB-MOVI
            ws.getIndPoli().setImpTfr(((short)0));
        }
        // COB_CODE: IF POL-CONV-GECO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-CONV-GECO
        //           ELSE
        //              MOVE 0 TO IND-POL-CONV-GECO
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolConvGecoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-CONV-GECO
            ws.getIndPoli().setImpVolo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-CONV-GECO
            ws.getIndPoli().setImpVolo(((short)0));
        }
        // COB_CODE: IF POL-FL-SCUDO-FISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-SCUDO-FISC
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-SCUDO-FISC
        //           END-IF
        if (Conditions.eq(poli.getPolFlScudoFisc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-SCUDO-FISC
            ws.getIndPoli().setManfeeAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-SCUDO-FISC
            ws.getIndPoli().setManfeeAntic(((short)0));
        }
        // COB_CODE: IF POL-FL-TRASFE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-TRASFE
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-TRASFE
        //           END-IF
        if (Conditions.eq(poli.getPolFlTrasfe(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-TRASFE
            ws.getIndPoli().setManfeeRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-TRASFE
            ws.getIndPoli().setManfeeRicor(((short)0));
        }
        // COB_CODE: IF POL-FL-TFR-STRC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-TFR-STRC
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-TFR-STRC
        //           END-IF
        if (Conditions.eq(poli.getPolFlTfrStrc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-TFR-STRC
            ws.getIndPoli().setManfeeRec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-TFR-STRC
            ws.getIndPoli().setManfeeRec(((short)0));
        }
        // COB_CODE: IF POL-DT-PRESC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DT-PRESC
        //           ELSE
        //              MOVE 0 TO IND-POL-DT-PRESC
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDtPresc().getPolDtPrescNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DT-PRESC
            ws.getIndPoli().setDtEsiTit(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DT-PRESC
            ws.getIndPoli().setDtEsiTit(((short)0));
        }
        // COB_CODE: IF POL-COD-CONV-AGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-COD-CONV-AGG
        //           ELSE
        //              MOVE 0 TO IND-POL-COD-CONV-AGG
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolCodConvAggFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-COD-CONV-AGG
            ws.getIndPoli().setSpeAge(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-COD-CONV-AGG
            ws.getIndPoli().setSpeAge(((short)0));
        }
        // COB_CODE: IF POL-SUBCAT-PROD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-SUBCAT-PROD
        //           ELSE
        //              MOVE 0 TO IND-POL-SUBCAT-PROD
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolSubcatProdFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-SUBCAT-PROD
            ws.getIndPoli().setCarIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-SUBCAT-PROD
            ws.getIndPoli().setCarIas(((short)0));
        }
        // COB_CODE: IF POL-FL-QUEST-ADEGZ-ASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-QUEST-ADEGZ-ASS
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-QUEST-ADEGZ-ASS
        //           END-IF
        if (Conditions.eq(poli.getPolFlQuestAdegzAss(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-QUEST-ADEGZ-ASS
            ws.getIndPoli().setTotIntrPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-QUEST-ADEGZ-ASS
            ws.getIndPoli().setTotIntrPrest(((short)0));
        }
        // COB_CODE: IF POL-COD-TPA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-COD-TPA
        //           ELSE
        //              MOVE 0 TO IND-POL-COD-TPA
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolCodTpaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-COD-TPA
            ws.getIndPoli().setImpTrasfe(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-COD-TPA
            ws.getIndPoli().setImpTrasfe(((short)0));
        }
        // COB_CODE: IF POL-ID-ACC-COMM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-ID-ACC-COMM
        //           ELSE
        //              MOVE 0 TO IND-POL-ID-ACC-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolIdAccComm().getPolIdAccCommNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-ID-ACC-COMM
            ws.getIndPoli().setImpTfrStrc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-ID-ACC-COMM
            ws.getIndPoli().setImpTfrStrc(((short)0));
        }
        // COB_CODE: IF POL-FL-POLI-CPI-PR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-POLI-CPI-PR
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-POLI-CPI-PR
        //           END-IF
        if (Conditions.eq(poli.getPolFlPoliCpiPr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-POLI-CPI-PR
            ws.getIndPoli().setNumGgRitardoPag(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-POLI-CPI-PR
            ws.getIndPoli().setNumGgRitardoPag(((short)0));
        }
        // COB_CODE: IF POL-FL-POLI-BUNDLING-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-POLI-BUNDLING
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-POLI-BUNDLING
        //           END-IF
        if (Conditions.eq(poli.getPolFlPoliBundling(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-POLI-BUNDLING
            ws.getIndPoli().setNumGgRival(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-POLI-BUNDLING
            ws.getIndPoli().setNumGgRival(((short)0));
        }
        // COB_CODE: IF POL-IND-POLI-PRIN-COLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-IND-POLI-PRIN-COLL
        //           ELSE
        //              MOVE 0 TO IND-POL-IND-POLI-PRIN-COLL
        //           END-IF
        if (Conditions.eq(poli.getPolIndPoliPrinColl(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-IND-POLI-PRIN-COLL
            ws.getIndPoli().setAcqExp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-IND-POLI-PRIN-COLL
            ws.getIndPoli().setAcqExp(((short)0));
        }
        // COB_CODE: IF POL-FL-VND-BUNDLE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-VND-BUNDLE
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-VND-BUNDLE
        //           END-IF
        if (Conditions.eq(poli.getPolFlVndBundle(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-VND-BUNDLE
            ws.getIndPoli().setRemunAss(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-VND-BUNDLE
            ws.getIndPoli().setRemunAss(((short)0));
        }
        // COB_CODE: IF POL-IB-BS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-IB-BS
        //           ELSE
        //              MOVE 0 TO IND-POL-IB-BS
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: MOVE -1 TO IND-POL-IB-BS
            ws.getIndPoli().setCommisInter(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-IB-BS
            ws.getIndPoli().setCommisInter(((short)0));
        }
        // COB_CODE: IF POL-FL-POLI-IFP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-POLI-IFP
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-POLI-IFP
        //           END-IF.
        if (Conditions.eq(poli.getPolFlPoliIfp(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-POLI-IFP
            ws.getIndPoli().setCnbtAntirac(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-POLI-IFP
            ws.getIndPoli().setCnbtAntirac(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : POL-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE POLI TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(poli.getPoliFormatted());
        // COB_CODE: MOVE POL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(poli.getPolIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO POL-ID-MOVI-CHIU
                poli.getPolIdMoviChiu().setPolIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO POL-DS-TS-END-CPTZ
                poli.setPolDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO POL-ID-MOVI-CRZ
                    poli.setPolIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO POL-ID-MOVI-CHIU-NULL
                    poli.getPolIdMoviChiu().setPolIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdMoviChiu.Len.POL_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO POL-DT-END-EFF
                    poli.setPolDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO POL-DS-TS-INI-CPTZ
                    poli.setPolDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO POL-DS-TS-END-CPTZ
                    poli.setPolDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE POLI TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(poli.getPoliFormatted());
        // COB_CODE: MOVE POL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(poli.getPolIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO POLI.
        poli.setPoliFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO POL-ID-MOVI-CRZ.
        poli.setPolIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO POL-ID-MOVI-CHIU-NULL.
        poli.getPolIdMoviChiu().setPolIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdMoviChiu.Len.POL_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO POL-DT-INI-EFF.
        poli.setPolDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO POL-DT-END-EFF.
        poli.setPolDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO POL-DS-TS-INI-CPTZ.
        poli.setPolDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO POL-DS-TS-END-CPTZ.
        poli.setPolDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO POL-COD-COMP-ANIA.
        poli.setPolCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: IF IND-POL-DT-PROP = 0
        //               MOVE WS-DATE-X      TO POL-DT-PROP-DB
        //           END-IF
        if (ws.getIndPoli().getDtEndCop() == 0) {
            // COB_CODE: MOVE POL-DT-PROP TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(poli.getPolDtProp().getPolDtProp(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO POL-DT-PROP-DB
            ws.getPoliDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: MOVE POL-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(poli.getPolDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO POL-DT-INI-EFF-DB
        ws.getPoliDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE POL-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(poli.getPolDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO POL-DT-END-EFF-DB
        ws.getPoliDb().setDecorDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE POL-DT-DECOR TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(poli.getPolDtDecor(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO POL-DT-DECOR-DB
        ws.getPoliDb().setScadDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE POL-DT-EMIS TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(poli.getPolDtEmis(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO POL-DT-EMIS-DB
        ws.getPoliDb().setVarzTpIasDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-POL-DT-SCAD = 0
        //               MOVE WS-DATE-X      TO POL-DT-SCAD-DB
        //           END-IF
        if (ws.getIndPoli().getIntrMora() == 0) {
            // COB_CODE: MOVE POL-DT-SCAD TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(poli.getPolDtScad().getPolDtScad(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO POL-DT-SCAD-DB
            ws.getPoliDb().setNovaRgmFiscDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: MOVE POL-DT-INI-VLDT-PROD TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(poli.getPolDtIniVldtProd(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO POL-DT-INI-VLDT-PROD-DB
        ws.getPoliDb().setDecorPrestBanDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-POL-DT-INI-VLDT-CONV = 0
        //               MOVE WS-DATE-X      TO POL-DT-INI-VLDT-CONV-DB
        //           END-IF
        if (ws.getIndPoli().getDir() == 0) {
            // COB_CODE: MOVE POL-DT-INI-VLDT-CONV TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(poli.getPolDtIniVldtConv().getPolDtIniVldtConv(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO POL-DT-INI-VLDT-CONV-DB
            ws.getPoliDb().setEffVarzStatTDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-POL-DT-APPLZ-CONV = 0
        //               MOVE WS-DATE-X      TO POL-DT-APPLZ-CONV-DB
        //           END-IF
        if (ws.getIndPoli().getSpeMed() == 0) {
            // COB_CODE: MOVE POL-DT-APPLZ-CONV TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(poli.getPolDtApplzConv().getPolDtApplzConv(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO POL-DT-APPLZ-CONV-DB
            ws.getPoliDb().setUltConsCnbtDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-POL-DT-PRESC = 0
        //               MOVE WS-DATE-X      TO POL-DT-PRESC-DB
        //           END-IF.
        if (ws.getIndPoli().getDtEsiTit() == 0) {
            // COB_CODE: MOVE POL-DT-PRESC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(poli.getPolDtPresc().getPolDtPresc(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO POL-DT-PRESC-DB
            ws.getPoliDb().setPrescDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: IF IND-POL-DT-PROP = 0
        //               MOVE WS-DATE-N      TO POL-DT-PROP
        //           END-IF
        if (ws.getIndPoli().getDtEndCop() == 0) {
            // COB_CODE: MOVE POL-DT-PROP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getIniEffDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-PROP
            poli.getPolDtProp().setPolDtProp(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE POL-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-EFF
        poli.setPolDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getDecorDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-END-EFF
        poli.setPolDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-DECOR-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getScadDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-DECOR
        poli.setPolDtDecor(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-EMIS-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getVarzTpIasDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-EMIS
        poli.setPolDtEmis(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-POL-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO POL-DT-SCAD
        //           END-IF
        if (ws.getIndPoli().getIntrMora() == 0) {
            // COB_CODE: MOVE POL-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getNovaRgmFiscDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-SCAD
            poli.getPolDtScad().setPolDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE POL-DT-INI-VLDT-PROD-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getDecorPrestBanDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-VLDT-PROD
        poli.setPolDtIniVldtProd(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-POL-DT-INI-VLDT-CONV = 0
        //               MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
        //           END-IF
        if (ws.getIndPoli().getDir() == 0) {
            // COB_CODE: MOVE POL-DT-INI-VLDT-CONV-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getEffVarzStatTDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
            poli.getPolDtIniVldtConv().setPolDtIniVldtConv(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-POL-DT-APPLZ-CONV = 0
        //               MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
        //           END-IF
        if (ws.getIndPoli().getSpeMed() == 0) {
            // COB_CODE: MOVE POL-DT-APPLZ-CONV-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getUltConsCnbtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
            poli.getPolDtApplzConv().setPolDtApplzConv(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-POL-DT-PRESC = 0
        //               MOVE WS-DATE-N      TO POL-DT-PRESC
        //           END-IF.
        if (ws.getIndPoli().getDtEsiTit() == 0) {
            // COB_CODE: MOVE POL-DT-PRESC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getPrescDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-PRESC
            poli.getPolDtPresc().setPolDtPresc(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getAaDiffProrDflt() {
        return poli.getPolAaDiffProrDflt().getPolAaDiffProrDflt();
    }

    @Override
    public void setAaDiffProrDflt(int aaDiffProrDflt) {
        this.poli.getPolAaDiffProrDflt().setPolAaDiffProrDflt(aaDiffProrDflt);
    }

    @Override
    public Integer getAaDiffProrDfltObj() {
        if (ws.getIndPoli().getProvDaRec() >= 0) {
            return ((Integer)getAaDiffProrDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaDiffProrDfltObj(Integer aaDiffProrDfltObj) {
        if (aaDiffProrDfltObj != null) {
            setAaDiffProrDflt(((int)aaDiffProrDfltObj));
            ws.getIndPoli().setProvDaRec(((short)0));
        }
        else {
            ws.getIndPoli().setProvDaRec(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return poli.getPolCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.poli.setPolCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodConv() {
        return poli.getPolCodConv();
    }

    @Override
    public String getCodConvAgg() {
        return poli.getPolCodConvAgg();
    }

    @Override
    public void setCodConvAgg(String codConvAgg) {
        this.poli.setPolCodConvAgg(codConvAgg);
    }

    @Override
    public String getCodConvAggObj() {
        if (ws.getIndPoli().getSpeAge() >= 0) {
            return getCodConvAgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodConvAggObj(String codConvAggObj) {
        if (codConvAggObj != null) {
            setCodConvAgg(codConvAggObj);
            ws.getIndPoli().setSpeAge(((short)0));
        }
        else {
            ws.getIndPoli().setSpeAge(((short)-1));
        }
    }

    @Override
    public void setCodConv(String codConv) {
        this.poli.setPolCodConv(codConv);
    }

    @Override
    public String getCodConvObj() {
        if (ws.getIndPoli().getIntrRetdt() >= 0) {
            return getCodConv();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodConvObj(String codConvObj) {
        if (codConvObj != null) {
            setCodConv(codConvObj);
            ws.getIndPoli().setIntrRetdt(((short)0));
        }
        else {
            ws.getIndPoli().setIntrRetdt(((short)-1));
        }
    }

    @Override
    public String getCodDvs() {
        return poli.getPolCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.poli.setPolCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndPoli().getCarGest() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndPoli().setCarGest(((short)0));
        }
        else {
            ws.getIndPoli().setCarGest(((short)-1));
        }
    }

    @Override
    public String getCodProd() {
        return poli.getPolCodProd();
    }

    @Override
    public void setCodProd(String codProd) {
        this.poli.setPolCodProd(codProd);
    }

    @Override
    public String getCodRamo() {
        return poli.getPolCodRamo();
    }

    @Override
    public void setCodRamo(String codRamo) {
        this.poli.setPolCodRamo(codRamo);
    }

    @Override
    public String getCodRamoObj() {
        if (ws.getIndPoli().getIntrRiat() >= 0) {
            return getCodRamo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodRamoObj(String codRamoObj) {
        if (codRamoObj != null) {
            setCodRamo(codRamoObj);
            ws.getIndPoli().setIntrRiat(((short)0));
        }
        else {
            ws.getIndPoli().setIntrRiat(((short)-1));
        }
    }

    @Override
    public String getCodTpa() {
        return poli.getPolCodTpa();
    }

    @Override
    public void setCodTpa(String codTpa) {
        this.poli.setPolCodTpa(codTpa);
    }

    @Override
    public String getCodTpaObj() {
        if (ws.getIndPoli().getImpTrasfe() >= 0) {
            return getCodTpa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTpaObj(String codTpaObj) {
        if (codTpaObj != null) {
            setCodTpa(codTpaObj);
            ws.getIndPoli().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndPoli().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public String getConvGeco() {
        return poli.getPolConvGeco();
    }

    @Override
    public void setConvGeco(String convGeco) {
        this.poli.setPolConvGeco(convGeco);
    }

    @Override
    public String getConvGecoObj() {
        if (ws.getIndPoli().getImpVolo() >= 0) {
            return getConvGeco();
        }
        else {
            return null;
        }
    }

    @Override
    public void setConvGecoObj(String convGecoObj) {
        if (convGecoObj != null) {
            setConvGeco(convGecoObj);
            ws.getIndPoli().setImpVolo(((short)0));
        }
        else {
            ws.getIndPoli().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getDir1oVers() {
        return poli.getPolDir1oVers().getPolDir1oVers();
    }

    @Override
    public void setDir1oVers(AfDecimal dir1oVers) {
        this.poli.getPolDir1oVers().setPolDir1oVers(dir1oVers.copy());
    }

    @Override
    public AfDecimal getDir1oVersObj() {
        if (ws.getIndPoli().getPreSoloRsh() >= 0) {
            return getDir1oVers();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDir1oVersObj(AfDecimal dir1oVersObj) {
        if (dir1oVersObj != null) {
            setDir1oVers(new AfDecimal(dir1oVersObj, 15, 3));
            ws.getIndPoli().setPreSoloRsh(((short)0));
        }
        else {
            ws.getIndPoli().setPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getDirEmis() {
        return poli.getPolDirEmis().getPolDirEmis();
    }

    @Override
    public void setDirEmis(AfDecimal dirEmis) {
        this.poli.getPolDirEmis().setPolDirEmis(dirEmis.copy());
    }

    @Override
    public AfDecimal getDirEmisObj() {
        if (ws.getIndPoli().getPrePpIas() >= 0) {
            return getDirEmis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDirEmisObj(AfDecimal dirEmisObj) {
        if (dirEmisObj != null) {
            setDirEmis(new AfDecimal(dirEmisObj, 15, 3));
            ws.getIndPoli().setPrePpIas(((short)0));
        }
        else {
            ws.getIndPoli().setPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getDirQuiet() {
        return poli.getPolDirQuiet().getPolDirQuiet();
    }

    @Override
    public void setDirQuiet(AfDecimal dirQuiet) {
        this.poli.getPolDirQuiet().setPolDirQuiet(dirQuiet.copy());
    }

    @Override
    public AfDecimal getDirQuietObj() {
        if (ws.getIndPoli().getCodTari() >= 0) {
            return getDirQuiet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDirQuietObj(AfDecimal dirQuietObj) {
        if (dirQuietObj != null) {
            setDirQuiet(new AfDecimal(dirQuietObj, 15, 3));
            ws.getIndPoli().setCodTari(((short)0));
        }
        else {
            ws.getIndPoli().setCodTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getDirVersAgg() {
        return poli.getPolDirVersAgg().getPolDirVersAgg();
    }

    @Override
    public void setDirVersAgg(AfDecimal dirVersAgg) {
        this.poli.getPolDirVersAgg().setPolDirVersAgg(dirVersAgg.copy());
    }

    @Override
    public AfDecimal getDirVersAggObj() {
        if (ws.getIndPoli().getCarAcq() >= 0) {
            return getDirVersAgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDirVersAggObj(AfDecimal dirVersAggObj) {
        if (dirVersAggObj != null) {
            setDirVersAgg(new AfDecimal(dirVersAggObj, 15, 3));
            ws.getIndPoli().setCarAcq(((short)0));
        }
        else {
            ws.getIndPoli().setCarAcq(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return poli.getPolDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.poli.setPolDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return poli.getPolDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.poli.setPolDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return poli.getPolDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.poli.setPolDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return poli.getPolDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.poli.setPolDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return poli.getPolDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.poli.setPolDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return poli.getPolDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.poli.setPolDsVer(dsVer);
    }

    @Override
    public String getDtApplzConvDb() {
        return ws.getPoliDb().getUltConsCnbtDb();
    }

    @Override
    public void setDtApplzConvDb(String dtApplzConvDb) {
        this.ws.getPoliDb().setUltConsCnbtDb(dtApplzConvDb);
    }

    @Override
    public String getDtApplzConvDbObj() {
        if (ws.getIndPoli().getSpeMed() >= 0) {
            return getDtApplzConvDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtApplzConvDbObj(String dtApplzConvDbObj) {
        if (dtApplzConvDbObj != null) {
            setDtApplzConvDb(dtApplzConvDbObj);
            ws.getIndPoli().setSpeMed(((short)0));
        }
        else {
            ws.getIndPoli().setSpeMed(((short)-1));
        }
    }

    @Override
    public String getDtDecorDb() {
        return ws.getPoliDb().getScadDb();
    }

    @Override
    public void setDtDecorDb(String dtDecorDb) {
        this.ws.getPoliDb().setScadDb(dtDecorDb);
    }

    @Override
    public String getDtEmisDb() {
        return ws.getPoliDb().getVarzTpIasDb();
    }

    @Override
    public void setDtEmisDb(String dtEmisDb) {
        this.ws.getPoliDb().setVarzTpIasDb(dtEmisDb);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getPoliDb().getDecorDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getPoliDb().setDecorDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getPoliDb().getEndEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getPoliDb().setEndEffDb(dtIniEffDb);
    }

    @Override
    public String getDtIniVldtConvDb() {
        return ws.getPoliDb().getEffVarzStatTDb();
    }

    @Override
    public void setDtIniVldtConvDb(String dtIniVldtConvDb) {
        this.ws.getPoliDb().setEffVarzStatTDb(dtIniVldtConvDb);
    }

    @Override
    public String getDtIniVldtConvDbObj() {
        if (ws.getIndPoli().getDir() >= 0) {
            return getDtIniVldtConvDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniVldtConvDbObj(String dtIniVldtConvDbObj) {
        if (dtIniVldtConvDbObj != null) {
            setDtIniVldtConvDb(dtIniVldtConvDbObj);
            ws.getIndPoli().setDir(((short)0));
        }
        else {
            ws.getIndPoli().setDir(((short)-1));
        }
    }

    @Override
    public String getDtIniVldtProdDb() {
        return ws.getPoliDb().getDecorPrestBanDb();
    }

    @Override
    public void setDtIniVldtProdDb(String dtIniVldtProdDb) {
        this.ws.getPoliDb().setDecorPrestBanDb(dtIniVldtProdDb);
    }

    @Override
    public String getDtPrescDb() {
        return ws.getPoliDb().getPrescDb();
    }

    @Override
    public void setDtPrescDb(String dtPrescDb) {
        this.ws.getPoliDb().setPrescDb(dtPrescDb);
    }

    @Override
    public String getDtPrescDbObj() {
        if (ws.getIndPoli().getDtEsiTit() >= 0) {
            return getDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtPrescDbObj(String dtPrescDbObj) {
        if (dtPrescDbObj != null) {
            setDtPrescDb(dtPrescDbObj);
            ws.getIndPoli().setDtEsiTit(((short)0));
        }
        else {
            ws.getIndPoli().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getDtPropDb() {
        return ws.getPoliDb().getIniEffDb();
    }

    @Override
    public void setDtPropDb(String dtPropDb) {
        this.ws.getPoliDb().setIniEffDb(dtPropDb);
    }

    @Override
    public String getDtPropDbObj() {
        if (ws.getIndPoli().getDtEndCop() >= 0) {
            return getDtPropDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtPropDbObj(String dtPropDbObj) {
        if (dtPropDbObj != null) {
            setDtPropDb(dtPropDbObj);
            ws.getIndPoli().setDtEndCop(((short)0));
        }
        else {
            ws.getIndPoli().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getDtScadDb() {
        return ws.getPoliDb().getNovaRgmFiscDb();
    }

    @Override
    public void setDtScadDb(String dtScadDb) {
        this.ws.getPoliDb().setNovaRgmFiscDb(dtScadDb);
    }

    @Override
    public String getDtScadDbObj() {
        if (ws.getIndPoli().getIntrMora() >= 0) {
            return getDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadDbObj(String dtScadDbObj) {
        if (dtScadDbObj != null) {
            setDtScadDb(dtScadDbObj);
            ws.getIndPoli().setIntrMora(((short)0));
        }
        else {
            ws.getIndPoli().setIntrMora(((short)-1));
        }
    }

    @Override
    public int getDurAa() {
        return poli.getPolDurAa().getPolDurAa();
    }

    @Override
    public void setDurAa(int durAa) {
        this.poli.getPolDurAa().setPolDurAa(durAa);
    }

    @Override
    public Integer getDurAaObj() {
        if (ws.getIndPoli().getPreNet() >= 0) {
            return ((Integer)getDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        if (durAaObj != null) {
            setDurAa(((int)durAaObj));
            ws.getIndPoli().setPreNet(((short)0));
        }
        else {
            ws.getIndPoli().setPreNet(((short)-1));
        }
    }

    @Override
    public int getDurGg() {
        return poli.getPolDurGg().getPolDurGg();
    }

    @Override
    public void setDurGg(int durGg) {
        this.poli.getPolDurGg().setPolDurGg(durGg);
    }

    @Override
    public Integer getDurGgObj() {
        if (ws.getIndPoli().getFrqMovi() >= 0) {
            return ((Integer)getDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        if (durGgObj != null) {
            setDurGg(((int)durGgObj));
            ws.getIndPoli().setFrqMovi(((short)0));
        }
        else {
            ws.getIndPoli().setFrqMovi(((short)-1));
        }
    }

    @Override
    public int getDurMm() {
        return poli.getPolDurMm().getPolDurMm();
    }

    @Override
    public void setDurMm(int durMm) {
        this.poli.getPolDurMm().setPolDurMm(durMm);
    }

    @Override
    public Integer getDurMmObj() {
        if (ws.getIndPoli().getIntrFraz() >= 0) {
            return ((Integer)getDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        if (durMmObj != null) {
            setDurMm(((int)durMmObj));
            ws.getIndPoli().setIntrFraz(((short)0));
        }
        else {
            ws.getIndPoli().setIntrFraz(((short)-1));
        }
    }

    @Override
    public char getFlAmmbMovi() {
        return poli.getPolFlAmmbMovi();
    }

    @Override
    public void setFlAmmbMovi(char flAmmbMovi) {
        this.poli.setPolFlAmmbMovi(flAmmbMovi);
    }

    @Override
    public Character getFlAmmbMoviObj() {
        if (ws.getIndPoli().getImpTfr() >= 0) {
            return ((Character)getFlAmmbMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlAmmbMoviObj(Character flAmmbMoviObj) {
        if (flAmmbMoviObj != null) {
            setFlAmmbMovi(((char)flAmmbMoviObj));
            ws.getIndPoli().setImpTfr(((short)0));
        }
        else {
            ws.getIndPoli().setImpTfr(((short)-1));
        }
    }

    @Override
    public char getFlCopFinanz() {
        return poli.getPolFlCopFinanz();
    }

    @Override
    public void setFlCopFinanz(char flCopFinanz) {
        this.poli.setPolFlCopFinanz(flCopFinanz);
    }

    @Override
    public Character getFlCopFinanzObj() {
        if (ws.getIndPoli().getSoprProf() >= 0) {
            return ((Character)getFlCopFinanz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCopFinanzObj(Character flCopFinanzObj) {
        if (flCopFinanzObj != null) {
            setFlCopFinanz(((char)flCopFinanzObj));
            ws.getIndPoli().setSoprProf(((short)0));
        }
        else {
            ws.getIndPoli().setSoprProf(((short)-1));
        }
    }

    @Override
    public char getFlCumPreCntr() {
        return poli.getPolFlCumPreCntr();
    }

    @Override
    public void setFlCumPreCntr(char flCumPreCntr) {
        this.poli.setPolFlCumPreCntr(flCumPreCntr);
    }

    @Override
    public Character getFlCumPreCntrObj() {
        if (ws.getIndPoli().getImpAder() >= 0) {
            return ((Character)getFlCumPreCntr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCumPreCntrObj(Character flCumPreCntrObj) {
        if (flCumPreCntrObj != null) {
            setFlCumPreCntr(((char)flCumPreCntrObj));
            ws.getIndPoli().setImpAder(((short)0));
        }
        else {
            ws.getIndPoli().setImpAder(((short)-1));
        }
    }

    @Override
    public char getFlEstas() {
        return poli.getPolFlEstas();
    }

    @Override
    public void setFlEstas(char flEstas) {
        this.poli.setPolFlEstas(flEstas);
    }

    @Override
    public Character getFlEstasObj() {
        if (ws.getIndPoli().getSoprSan() >= 0) {
            return ((Character)getFlEstas());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlEstasObj(Character flEstasObj) {
        if (flEstasObj != null) {
            setFlEstas(((char)flEstasObj));
            ws.getIndPoli().setSoprSan(((short)0));
        }
        else {
            ws.getIndPoli().setSoprSan(((short)-1));
        }
    }

    @Override
    public char getFlFntAder() {
        return poli.getPolFlFntAder();
    }

    @Override
    public void setFlFntAder(char flFntAder) {
        this.poli.setPolFlFntAder(flFntAder);
    }

    @Override
    public Character getFlFntAderObj() {
        if (ws.getIndPoli().getProvAcq1aa() >= 0) {
            return ((Character)getFlFntAder());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlFntAderObj(Character flFntAderObj) {
        if (flFntAderObj != null) {
            setFlFntAder(((char)flFntAderObj));
            ws.getIndPoli().setProvAcq1aa(((short)0));
        }
        else {
            ws.getIndPoli().setProvAcq1aa(((short)-1));
        }
    }

    @Override
    public char getFlFntAz() {
        return poli.getPolFlFntAz();
    }

    @Override
    public void setFlFntAz(char flFntAz) {
        this.poli.setPolFlFntAz(flFntAz);
    }

    @Override
    public Character getFlFntAzObj() {
        if (ws.getIndPoli().getCarInc() >= 0) {
            return ((Character)getFlFntAz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlFntAzObj(Character flFntAzObj) {
        if (flFntAzObj != null) {
            setFlFntAz(((char)flFntAzObj));
            ws.getIndPoli().setCarInc(((short)0));
        }
        else {
            ws.getIndPoli().setCarInc(((short)-1));
        }
    }

    @Override
    public char getFlFntTfr() {
        return poli.getPolFlFntTfr();
    }

    @Override
    public void setFlFntTfr(char flFntTfr) {
        this.poli.setPolFlFntTfr(flFntTfr);
    }

    @Override
    public Character getFlFntTfrObj() {
        if (ws.getIndPoli().getProvAcq2aa() >= 0) {
            return ((Character)getFlFntTfr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlFntTfrObj(Character flFntTfrObj) {
        if (flFntTfrObj != null) {
            setFlFntTfr(((char)flFntTfrObj));
            ws.getIndPoli().setProvAcq2aa(((short)0));
        }
        else {
            ws.getIndPoli().setProvAcq2aa(((short)-1));
        }
    }

    @Override
    public char getFlFntVolo() {
        return poli.getPolFlFntVolo();
    }

    @Override
    public void setFlFntVolo(char flFntVolo) {
        this.poli.setPolFlFntVolo(flFntVolo);
    }

    @Override
    public Character getFlFntVoloObj() {
        if (ws.getIndPoli().getProvRicor() >= 0) {
            return ((Character)getFlFntVolo());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlFntVoloObj(Character flFntVoloObj) {
        if (flFntVoloObj != null) {
            setFlFntVolo(((char)flFntVoloObj));
            ws.getIndPoli().setProvRicor(((short)0));
        }
        else {
            ws.getIndPoli().setProvRicor(((short)-1));
        }
    }

    @Override
    public char getFlPoliBundling() {
        return poli.getPolFlPoliBundling();
    }

    @Override
    public void setFlPoliBundling(char flPoliBundling) {
        this.poli.setPolFlPoliBundling(flPoliBundling);
    }

    @Override
    public Character getFlPoliBundlingObj() {
        if (ws.getIndPoli().getNumGgRival() >= 0) {
            return ((Character)getFlPoliBundling());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPoliBundlingObj(Character flPoliBundlingObj) {
        if (flPoliBundlingObj != null) {
            setFlPoliBundling(((char)flPoliBundlingObj));
            ws.getIndPoli().setNumGgRival(((short)0));
        }
        else {
            ws.getIndPoli().setNumGgRival(((short)-1));
        }
    }

    @Override
    public char getFlPoliCpiPr() {
        return poli.getPolFlPoliCpiPr();
    }

    @Override
    public void setFlPoliCpiPr(char flPoliCpiPr) {
        this.poli.setPolFlPoliCpiPr(flPoliCpiPr);
    }

    @Override
    public Character getFlPoliCpiPrObj() {
        if (ws.getIndPoli().getNumGgRitardoPag() >= 0) {
            return ((Character)getFlPoliCpiPr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPoliCpiPrObj(Character flPoliCpiPrObj) {
        if (flPoliCpiPrObj != null) {
            setFlPoliCpiPr(((char)flPoliCpiPrObj));
            ws.getIndPoli().setNumGgRitardoPag(((short)0));
        }
        else {
            ws.getIndPoli().setNumGgRitardoPag(((short)-1));
        }
    }

    @Override
    public char getFlPoliIfp() {
        return poli.getPolFlPoliIfp();
    }

    @Override
    public void setFlPoliIfp(char flPoliIfp) {
        this.poli.setPolFlPoliIfp(flPoliIfp);
    }

    @Override
    public Character getFlPoliIfpObj() {
        if (ws.getIndPoli().getCnbtAntirac() >= 0) {
            return ((Character)getFlPoliIfp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPoliIfpObj(Character flPoliIfpObj) {
        if (flPoliIfpObj != null) {
            setFlPoliIfp(((char)flPoliIfpObj));
            ws.getIndPoli().setCnbtAntirac(((short)0));
        }
        else {
            ws.getIndPoli().setCnbtAntirac(((short)-1));
        }
    }

    @Override
    public char getFlQuestAdegzAss() {
        return poli.getPolFlQuestAdegzAss();
    }

    @Override
    public void setFlQuestAdegzAss(char flQuestAdegzAss) {
        this.poli.setPolFlQuestAdegzAss(flQuestAdegzAss);
    }

    @Override
    public Character getFlQuestAdegzAssObj() {
        if (ws.getIndPoli().getTotIntrPrest() >= 0) {
            return ((Character)getFlQuestAdegzAss());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlQuestAdegzAssObj(Character flQuestAdegzAssObj) {
        if (flQuestAdegzAssObj != null) {
            setFlQuestAdegzAss(((char)flQuestAdegzAssObj));
            ws.getIndPoli().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndPoli().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public char getFlRshComun() {
        return poli.getPolFlRshComun();
    }

    @Override
    public void setFlRshComun(char flRshComun) {
        this.poli.setPolFlRshComun(flRshComun);
    }

    @Override
    public char getFlRshComunCond() {
        return poli.getPolFlRshComunCond();
    }

    @Override
    public void setFlRshComunCond(char flRshComunCond) {
        this.poli.setPolFlRshComunCond(flRshComunCond);
    }

    @Override
    public Character getFlRshComunCondObj() {
        if (ws.getIndPoli().getSoprTec() >= 0) {
            return ((Character)getFlRshComunCond());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlRshComunCondObj(Character flRshComunCondObj) {
        if (flRshComunCondObj != null) {
            setFlRshComunCond(((char)flRshComunCondObj));
            ws.getIndPoli().setSoprTec(((short)0));
        }
        else {
            ws.getIndPoli().setSoprTec(((short)-1));
        }
    }

    @Override
    public Character getFlRshComunObj() {
        if (ws.getIndPoli().getSoprSpo() >= 0) {
            return ((Character)getFlRshComun());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlRshComunObj(Character flRshComunObj) {
        if (flRshComunObj != null) {
            setFlRshComun(((char)flRshComunObj));
            ws.getIndPoli().setSoprSpo(((short)0));
        }
        else {
            ws.getIndPoli().setSoprSpo(((short)-1));
        }
    }

    @Override
    public char getFlScudoFisc() {
        return poli.getPolFlScudoFisc();
    }

    @Override
    public void setFlScudoFisc(char flScudoFisc) {
        this.poli.setPolFlScudoFisc(flScudoFisc);
    }

    @Override
    public Character getFlScudoFiscObj() {
        if (ws.getIndPoli().getManfeeAntic() >= 0) {
            return ((Character)getFlScudoFisc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlScudoFiscObj(Character flScudoFiscObj) {
        if (flScudoFiscObj != null) {
            setFlScudoFisc(((char)flScudoFiscObj));
            ws.getIndPoli().setManfeeAntic(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public char getFlTfrStrc() {
        return poli.getPolFlTfrStrc();
    }

    @Override
    public void setFlTfrStrc(char flTfrStrc) {
        this.poli.setPolFlTfrStrc(flTfrStrc);
    }

    @Override
    public Character getFlTfrStrcObj() {
        if (ws.getIndPoli().getManfeeRec() >= 0) {
            return ((Character)getFlTfrStrc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlTfrStrcObj(Character flTfrStrcObj) {
        if (flTfrStrcObj != null) {
            setFlTfrStrc(((char)flTfrStrcObj));
            ws.getIndPoli().setManfeeRec(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeRec(((short)-1));
        }
    }

    @Override
    public char getFlTrasfe() {
        return poli.getPolFlTrasfe();
    }

    @Override
    public void setFlTrasfe(char flTrasfe) {
        this.poli.setPolFlTrasfe(flTrasfe);
    }

    @Override
    public Character getFlTrasfeObj() {
        if (ws.getIndPoli().getManfeeRicor() >= 0) {
            return ((Character)getFlTrasfe());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlTrasfeObj(Character flTrasfeObj) {
        if (flTrasfeObj != null) {
            setFlTrasfe(((char)flTrasfeObj));
            ws.getIndPoli().setManfeeRicor(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public String getFlVerProd() {
        return poli.getPolFlVerProd();
    }

    @Override
    public void setFlVerProd(String flVerProd) {
        this.poli.setPolFlVerProd(flVerProd);
    }

    @Override
    public String getFlVerProdObj() {
        if (ws.getIndPoli().getCodDvs() >= 0) {
            return getFlVerProd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlVerProdObj(String flVerProdObj) {
        if (flVerProdObj != null) {
            setFlVerProd(flVerProdObj);
            ws.getIndPoli().setCodDvs(((short)0));
        }
        else {
            ws.getIndPoli().setCodDvs(((short)-1));
        }
    }

    @Override
    public char getFlVndBundle() {
        return poli.getPolFlVndBundle();
    }

    @Override
    public void setFlVndBundle(char flVndBundle) {
        this.poli.setPolFlVndBundle(flVndBundle);
    }

    @Override
    public Character getFlVndBundleObj() {
        if (ws.getIndPoli().getRemunAss() >= 0) {
            return ((Character)getFlVndBundle());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlVndBundleObj(Character flVndBundleObj) {
        if (flVndBundleObj != null) {
            setFlVndBundle(((char)flVndBundleObj));
            ws.getIndPoli().setRemunAss(((short)0));
        }
        else {
            ws.getIndPoli().setRemunAss(((short)-1));
        }
    }

    @Override
    public String getIbBs() {
        return poli.getPolIbBs();
    }

    @Override
    public void setIbBs(String ibBs) {
        this.poli.setPolIbBs(ibBs);
    }

    @Override
    public String getIbBsObj() {
        if (ws.getIndPoli().getCommisInter() >= 0) {
            return getIbBs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbBsObj(String ibBsObj) {
        if (ibBsObj != null) {
            setIbBs(ibBsObj);
            ws.getIndPoli().setCommisInter(((short)0));
        }
        else {
            ws.getIndPoli().setCommisInter(((short)-1));
        }
    }

    @Override
    public String getIbOgg() {
        return poli.getPolIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.poli.setPolIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndPoli().getDtIniCop() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndPoli().setDtIniCop(((short)0));
        }
        else {
            ws.getIndPoli().setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getIbProp() {
        return poli.getPolIbProp();
    }

    @Override
    public void setIbProp(String ibProp) {
        this.poli.setPolIbProp(ibProp);
    }

    @Override
    public int getIdAccComm() {
        return poli.getPolIdAccComm().getPolIdAccComm();
    }

    @Override
    public void setIdAccComm(int idAccComm) {
        this.poli.getPolIdAccComm().setPolIdAccComm(idAccComm);
    }

    @Override
    public Integer getIdAccCommObj() {
        if (ws.getIndPoli().getImpTfrStrc() >= 0) {
            return ((Integer)getIdAccComm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdAccCommObj(Integer idAccCommObj) {
        if (idAccCommObj != null) {
            setIdAccComm(((int)idAccCommObj));
            ws.getIndPoli().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndPoli().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return poli.getPolIdMoviChiu().getPolIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.poli.getPolIdMoviChiu().setPolIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndPoli().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndPoli().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndPoli().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return poli.getPolIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.poli.setPolIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPoli() {
        return poli.getPolIdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.poli.setPolIdPoli(idPoli);
    }

    @Override
    public char getIndPoliPrinColl() {
        return poli.getPolIndPoliPrinColl();
    }

    @Override
    public void setIndPoliPrinColl(char indPoliPrinColl) {
        this.poli.setPolIndPoliPrinColl(indPoliPrinColl);
    }

    @Override
    public Character getIndPoliPrinCollObj() {
        if (ws.getIndPoli().getAcqExp() >= 0) {
            return ((Character)getIndPoliPrinColl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPoliPrinCollObj(Character indPoliPrinCollObj) {
        if (indPoliPrinCollObj != null) {
            setIndPoliPrinColl(((char)indPoliPrinCollObj));
            ws.getIndPoli().setAcqExp(((short)0));
        }
        else {
            ws.getIndPoli().setAcqExp(((short)-1));
        }
    }

    @Override
    public long getPolDsRiga() {
        return poli.getPolDsRiga();
    }

    @Override
    public void setPolDsRiga(long polDsRiga) {
        this.poli.setPolDsRiga(polDsRiga);
    }

    @Override
    public AfDecimal getSpeMed() {
        return poli.getPolSpeMed().getPolSpeMed();
    }

    @Override
    public void setSpeMed(AfDecimal speMed) {
        this.poli.getPolSpeMed().setPolSpeMed(speMed.copy());
    }

    @Override
    public AfDecimal getSpeMedObj() {
        if (ws.getIndPoli().getPreTot() >= 0) {
            return getSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpeMedObj(AfDecimal speMedObj) {
        if (speMedObj != null) {
            setSpeMed(new AfDecimal(speMedObj, 15, 3));
            ws.getIndPoli().setPreTot(((short)0));
        }
        else {
            ws.getIndPoli().setPreTot(((short)-1));
        }
    }

    @Override
    public String getSubcatProd() {
        return poli.getPolSubcatProd();
    }

    @Override
    public void setSubcatProd(String subcatProd) {
        this.poli.setPolSubcatProd(subcatProd);
    }

    @Override
    public String getSubcatProdObj() {
        if (ws.getIndPoli().getCarIas() >= 0) {
            return getSubcatProd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSubcatProdObj(String subcatProdObj) {
        if (subcatProdObj != null) {
            setSubcatProd(subcatProdObj);
            ws.getIndPoli().setCarIas(((short)0));
        }
        else {
            ws.getIndPoli().setCarIas(((short)-1));
        }
    }

    @Override
    public String getTpApplzDir() {
        return poli.getPolTpApplzDir();
    }

    @Override
    public void setTpApplzDir(String tpApplzDir) {
        this.poli.setPolTpApplzDir(tpApplzDir);
    }

    @Override
    public String getTpApplzDirObj() {
        if (ws.getIndPoli().getSoprAlt() >= 0) {
            return getTpApplzDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpApplzDirObj(String tpApplzDirObj) {
        if (tpApplzDirObj != null) {
            setTpApplzDir(tpApplzDirObj);
            ws.getIndPoli().setSoprAlt(((short)0));
        }
        else {
            ws.getIndPoli().setSoprAlt(((short)-1));
        }
    }

    @Override
    public String getTpFrmAssva() {
        return poli.getPolTpFrmAssva();
    }

    @Override
    public void setTpFrmAssva(String tpFrmAssva) {
        this.poli.setPolTpFrmAssva(tpFrmAssva);
    }

    @Override
    public String getTpLivGenzTit() {
        return poli.getPolTpLivGenzTit();
    }

    @Override
    public void setTpLivGenzTit(String tpLivGenzTit) {
        this.poli.setPolTpLivGenzTit(tpLivGenzTit);
    }

    @Override
    public String getTpOpzAScad() {
        return poli.getPolTpOpzAScad();
    }

    @Override
    public void setTpOpzAScad(String tpOpzAScad) {
        this.poli.setPolTpOpzAScad(tpOpzAScad);
    }

    @Override
    public String getTpOpzAScadObj() {
        if (ws.getIndPoli().getProvInc() >= 0) {
            return getTpOpzAScad();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpOpzAScadObj(String tpOpzAScadObj) {
        if (tpOpzAScadObj != null) {
            setTpOpzAScad(tpOpzAScadObj);
            ws.getIndPoli().setProvInc(((short)0));
        }
        else {
            ws.getIndPoli().setProvInc(((short)-1));
        }
    }

    @Override
    public String getTpPoli() {
        return poli.getPolTpPoli();
    }

    @Override
    public void setTpPoli(String tpPoli) {
        this.poli.setPolTpPoli(tpPoli);
    }

    @Override
    public String getTpPtfEstno() {
        return poli.getPolTpPtfEstno();
    }

    @Override
    public void setTpPtfEstno(String tpPtfEstno) {
        this.poli.setPolTpPtfEstno(tpPtfEstno);
    }

    @Override
    public String getTpPtfEstnoObj() {
        if (ws.getIndPoli().getImpAz() >= 0) {
            return getTpPtfEstno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPtfEstnoObj(String tpPtfEstnoObj) {
        if (tpPtfEstnoObj != null) {
            setTpPtfEstno(tpPtfEstnoObj);
            ws.getIndPoli().setImpAz(((short)0));
        }
        else {
            ws.getIndPoli().setImpAz(((short)-1));
        }
    }

    @Override
    public String getTpRgmFisc() {
        return poli.getPolTpRgmFisc();
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        this.poli.setPolTpRgmFisc(tpRgmFisc);
    }

    @Override
    public String getTpRgmFiscObj() {
        if (ws.getIndPoli().getTax() >= 0) {
            return getTpRgmFisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRgmFiscObj(String tpRgmFiscObj) {
        if (tpRgmFiscObj != null) {
            setTpRgmFisc(tpRgmFiscObj);
            ws.getIndPoli().setTax(((short)0));
        }
        else {
            ws.getIndPoli().setTax(((short)-1));
        }
    }
}
