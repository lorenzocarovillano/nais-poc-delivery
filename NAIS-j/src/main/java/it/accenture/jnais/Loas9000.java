package it.accenture.jnais;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Lccc00011;
import it.accenture.jnais.copy.WcomDatiDeroghe;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Iabv0002;
import it.accenture.jnais.ws.Iabv0003;
import it.accenture.jnais.ws.Iabv0006;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Loas9000Data;
import it.accenture.jnais.ws.MoviBatchSospLoas9000;
import it.accenture.jnais.ws.OggBloccoIdbsl110;
import it.accenture.jnais.ws.ParamMoviLdbs1470;
import it.accenture.jnais.ws.WcomIntervalloElab;

/**Original name: LOAS9000<br>
 * <pre>*****************************************************************
 * *                                                        ********
 * *                                                        ********
 * *                                                        ********
 * *                                                        ********
 * *****************************************************************
 * AUTHOR.   ATS NAPOLI.
 * DATE-WRITTEN.  AGOSTO 2008.
 * DATE-COMPILED.
 * REMARKS.
 * °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
 *     PROGRAAMMA .... LOAS9000
 *     FUNZIONE ......
 * °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°</pre>*/
public class Loas9000 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Loas9000Data ws = new Loas9000Data();
    //Original name: IDSV0001
    private AreaIdsv0001 idsv0001;
    //Original name: IABV0006
    private Iabv0006 iabv0006;
    //Original name: IABV0002
    private Iabv0002 iabv0002;
    //Original name: IABV0003
    private Iabv0003 iabv0003;
    //Original name: OGG-BLOCCO
    private OggBloccoIdbsl110 oggBlocco;
    //Original name: MOVI-BATCH-SOSP
    private MoviBatchSospLoas9000 moviBatchSosp;

    //==== METHODS ====
    /**Original name: PROGRAM_LOAS9000_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 idsv0001, Iabv0006 iabv0006, Iabv0002 iabv0002, Iabv0003 iabv0003, OggBloccoIdbsl110 oggBlocco, MoviBatchSospLoas9000 moviBatchSosp) {
        this.idsv0001 = idsv0001;
        this.iabv0006 = iabv0006;
        this.iabv0002 = iabv0002;
        this.iabv0003 = iabv0003;
        this.oggBlocco = oggBlocco;
        this.moviBatchSosp = moviBatchSosp;
        // COB_CODE: MOVE 'PROCEDURE DIVISION'       TO WK-LABEL.
        ws.setWkLabel("PROCEDURE DIVISION");
        // COB_CODE: PERFORM A000-OPERAZ-INIZ        THRU A000-EX.
        a000OperazIniz();
        // COB_CODE: PERFORM A020-VALORIZZA-IDSV0003 THRU A020-EX.
        a020ValorizzaIdsv0003();
        // COB_CODE: PERFORM B000-ELABORA            THRU B000-EX.
        b000Elabora();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Loas9000 getInstance() {
        return ((Loas9000)Programs.getInstance(Loas9000.class));
    }

    /**Original name: A000-OPERAZ-INIZ<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPERAZIONI INIZIALI                                            *
	 * ----------------------------------------------------------------*</pre>*/
    private void a000OperazIniz() {
        // COB_CODE: MOVE 'A000-OPERAZ-INIZ'           TO WK-LABEL.
        ws.setWkLabel("A000-OPERAZ-INIZ");
        //
        // COB_CODE: PERFORM A050-INITIALIZE           THRU A050-EX.
        a050Initialize();
        // COB_CODE: PERFORM A030-IDENTIFICA-TIPOLOGIE THRU A030-EX.
        a030IdentificaTipologie();
    }

    /**Original name: A020-VALORIZZA-IDSV0003<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZA AREA IDSV0003
	 * ----------------------------------------------------------------*</pre>*/
    private void a020ValorizzaIdsv0003() {
        // COB_CODE: MOVE 'A020-VALORIZZA-IDSV0003'        TO WK-LABEL.
        ws.setWkLabel("A020-VALORIZZA-IDSV0003");
        //
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                                     TO IDSV0003-MODALITA-ESECUTIVA
        ws.getIdsv0003().getModalitaEsecutiva().setModalitaEsecutiva(idsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                     TO IDSV0003-CODICE-COMPAGNIA-ANIA
        ws.getIdsv0003().setCodiceCompagniaAnia(idsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                                     TO IDSV0003-COD-MAIN-BATCH
        ws.getIdsv0003().setCodMainBatch(idsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                                     TO IDSV0003-TIPO-MOVIMENTO
        ws.getIdsv0003().setTipoMovimentoFormatted(idsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-USER-NAME   TO IDSV0003-USER-NAME
        ws.getIdsv0003().setUserName(idsv0001.getAreaComune().getUserName());
        // COB_CODE: MOVE IDSV0001-SESSIONE    TO IDSV0003-SESSIONE
        ws.getIdsv0003().setSessione(idsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
        //                                     TO IDSV0003-TRATTAMENTO-STORICITA
        ws.getIdsv0003().getTrattamentoStoricita().setTrattamentoStoricita(idsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                                     TO IDSV0003-FORMATO-DATA-DB
        ws.getIdsv0003().getFormatoDataDb().setFormatoDataDb(idsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
        //                                     TO IDSV0003-DATA-INIZIO-EFFETTO
        ws.getIdsv0003().setDataInizioEffetto(idsv0001.getAreaComune().getIdsv0001DataEffetto());
        // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
        //                                     TO IDSV0003-DATA-COMPETENZA
        ws.getIdsv0003().setDataCompetenza(idsv0001.getAreaComune().getIdsv0001DataCompetenza());
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                                      TO IDSV0003-DATA-COMP-AGG-STOR.
        ws.getIdsv0003().setDataCompAggStor(idsv0001.getAreaComune().getIdsv0001DataCompAggStor());
    }

    /**Original name: A030-IDENTIFICA-TIPOLOGIE<br>
	 * <pre>----------------------------------------------------------------*
	 *  IDENTIFICA TIPOLOGIE DI OBL ED MBS
	 * ----------------------------------------------------------------*</pre>*/
    private void a030IdentificaTipologie() {
        // COB_CODE: MOVE 'A030-IDENTIFICA-TIPOLOGIE' TO WK-LABEL.
        ws.setWkLabel("A030-IDENTIFICA-TIPOLOGIE");
        // COB_CODE: MOVE L11-TP-OGG                  TO FLAG-TP-OBL.
        ws.getFlagTpObl().setFlagTpObl(oggBlocco.getL11TpOgg());
        // COB_CODE: MOVE MBS-TP-OGG                  TO FLAG-TP-OGG-MBS.
        ws.getFlagTpOggMbs().setFlagTpOggMbs(moviBatchSosp.getMbsTpOgg());
        // COB_CODE: MOVE MBS-TP-FRM-ASSVA            TO FLAG-TP-FRM-ASSVA.
        ws.getFlagTpFrmAssva().setFlagTpFrmAssva(moviBatchSosp.getMbsTpFrmAssva());
    }

    /**Original name: A050-INITIALIZE<br>
	 * <pre>----------------------------------------------------------------*
	 *  INIZIALIZZA AREE DI WORKING                                    *
	 * ----------------------------------------------------------------*</pre>*/
    private void a050Initialize() {
        // COB_CODE: MOVE ZEROES                    TO IX-TAB-PMO.
        ws.setIxTabPmo(((short)0));
        // COB_CODE: SET IDSV0001-ESITO-OK          TO TRUE.
        idsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: SET WK-FINE-GUIDE-NO           TO TRUE.
        ws.getFlagFineGuide().setNo();
        // COB_CODE: INITIALIZE                     IDSV0901
        //                                          WCOM-INTERVALLO-ELAB
        //                                          WCOM-IO-STATI
        //                                          WK-LABEL.
        initIdsv0901();
        initWcomIntervalloElab();
        initWcomIoStati();
        ws.setWkLabel("");
        // COB_CODE: SET ALPO-CALL                  TO TRUE.
        ws.getWcomIoStati().getLccc0261().getBsCallType().setAlpoCall();
        // COB_CODE: MOVE MBS-ID-MOVI               TO WCOM-ID-MOVI-CREAZ.
        ws.getWcomIoStati().getLccc0261().setIdMoviCreaz(moviBatchSosp.getMbsIdMovi());
        // COB_CODE: MOVE MBS-ID-OGG-BLOCCO         TO WCOM-ID-BLOCCO-CRZ.
        ws.getWcomIoStati().getLccc0261().getDatiBlocco().setIdBloccoCrz(moviBatchSosp.getMbsIdOggBlocco());
        // CONTIENE UNA SEQUENZA DI ISTRUZIONI PER AZZERAR 'AREA DEGLI
        //ERRORI DELL'AREA CONTESTO.
        // COB_CODE: MOVE 'OK'   TO IDSV0001-ESITO
        idsv0001.getEsito().setEsito("OK");
        // COB_CODE: MOVE SPACES TO IDSV0001-LOG-ERRORE
        idsv0001.getLogErrore().initLogErroreSpaces();
        // COB_CODE: SET IDSV0001-FORZ-RC-04-NO        TO TRUE
        idsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        // COB_CODE: PERFORM VARYING IDSV0001-MAX-ELE-ERRORI FROM 1 BY 1
        //                     UNTIL IDSV0001-MAX-ELE-ERRORI > 10
        //               TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
        //           END-PERFORM
        idsv0001.setMaxEleErrori(((short)1));
        while (!(idsv0001.getMaxEleErrori() > 10)) {
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setDescErrore("");
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setIdsv0001CodErrore(0);
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)0));
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
            idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setTipoTrattFe(Types.SPACE_CHAR);
            idsv0001.setMaxEleErrori(Trunc.toShort(idsv0001.getMaxEleErrori() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES TO IDSV0001-MAX-ELE-ERRORI.
        idsv0001.setMaxEleErrori(((short)0));
    }

    /**Original name: B000-ELABORA<br>
	 * <pre>----------------------------------------------------------------*
	 *  ELABORAZIONE                                                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void b000Elabora() {
        // COB_CODE: IF IABV0006-GUIDE-AD-HOC
        //              PERFORM E000-ESTRAI-DATI          THRU E000-EX
        //           END-IF.
        if (iabv0006.getFlagProcessType().isIabv0006GuideAdHoc()) {
            // COB_CODE: PERFORM E000-ESTRAI-DATI          THRU E000-EX
            e000EstraiDati();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              PERFORM E500-ELABORA-BUSINESS     THRU E500-EX
        //           END-IF.
        if (idsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM E500-ELABORA-BUSINESS     THRU E500-EX
            e500ElaboraBusiness();
        }
    }

    /**Original name: E000-ESTRAI-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *  ELABORAZIONE                                                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void e000EstraiDati() {
        // COB_CODE: MOVE 'E000-ESTRAI-DATI'        TO WK-LABEL.
        ws.setWkLabel("E000-ESTRAI-DATI");
        // COB_CODE: PERFORM E100-PREPARA-GUIDE     THRU E100-EX
        e100PreparaGuide();
        // COB_CODE: PERFORM E200-ACCEDI-GUIDE      THRU E200-EX.
        e200AccediGuide();
    }

    /**Original name: E100-PREPARA-GUIDE<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA WHERE X GUIDE SERVICE                                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void e100PreparaGuide() {
        // COB_CODE: MOVE 'E100-PREPARA-GUIDE' TO WK-LABEL.
        ws.setWkLabel("E100-PREPARA-GUIDE");
        // COB_CODE: SET IDSV0003-WHERE-CONDITION-01 TO TRUE
        ws.getIdsv0003().getTipologiaOperazione().setIdsv0003WhereCondition01();
        // COB_CODE: MOVE HIGH-VALUES          TO PARAM-MOVI
        ws.getParamMovi().initParamMoviHighValues();
        // COB_CODE: MOVE MBS-COD-COMP-ANIA    TO PMO-COD-COMP-ANIA
        ws.getParamMovi().setPmoCodCompAnia(moviBatchSosp.getMbsCodCompAnia());
        // COB_CODE: MOVE MBS-TP-MOVI          TO PMO-TP-MOVI
        ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(moviBatchSosp.getMbsTpMovi());
        // COB_CODE: MOVE GARANZIA             TO PMO-TP-OGG
        ws.getParamMovi().setPmoTpOgg(ws.getGaranzia());
        // COB_CODE: MOVE MBS-TP-FRM-ASSVA     TO PMO-TP-FRM-ASSVA
        ws.getParamMovi().setPmoTpFrmAssva(moviBatchSosp.getMbsTpFrmAssva());
        // COB_CODE: MOVE L11-ID-OGG-1RIO      TO PMO-ID-POLI
        ws.getParamMovi().setPmoIdPoli(oggBlocco.getL11IdOgg1rio().getL11IdOgg1rio());
        // COB_CODE: MOVE MBS-ID-OGG           TO PMO-ID-ADES
        ws.getParamMovi().getPmoIdAdes().setPmoIdAdes(moviBatchSosp.getMbsIdOgg());
        // COB_CODE: MOVE MBS-DT-EFF           TO PMO-DT-RICOR-SUCC
        ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSucc(moviBatchSosp.getMbsDtEff());
        // COB_CODE: MOVE MBS-DT-EFF           TO PMO-DT-INI-EFF
        //                                        PMO-DT-END-EFF
        ws.getParamMovi().setPmoDtIniEff(moviBatchSosp.getMbsDtEff());
        ws.getParamMovi().setPmoDtEndEff(moviBatchSosp.getMbsDtEff());
        // COB_CODE: MOVE MBS-DS-TS-CPTZ       TO PMO-DS-TS-INI-CPTZ
        //                                        PMO-DS-TS-END-CPTZ.
        ws.getParamMovi().setPmoDsTsIniCptz(moviBatchSosp.getMbsDsTsCptz());
        ws.getParamMovi().setPmoDsTsEndCptz(moviBatchSosp.getMbsDsTsCptz());
    }

    /**Original name: E200-ACCEDI-GUIDE<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAZIONE DATI DA ELABORARE
	 * ----------------------------------------------------------------*</pre>*/
    private void e200AccediGuide() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'E200-ACCEDI-GUIDE'          TO WK-LABEL.
        ws.setWkLabel("E200-ACCEDI-GUIDE");
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE
        ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE
        ws.getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: SET WK-FINE-GUIDE-NO              TO TRUE
        ws.getFlagFineGuide().setNo();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION-01   TO TRUE
        ws.getIdsv0003().getTipologiaOperazione().setIdsv0003WhereCondition01();
        // COB_CODE: SET IDSV0003-FETCH-FIRST          TO TRUE
        ws.getIdsv0003().getOperazione().setFetchFirst();
        // COB_CODE: PERFORM UNTIL WK-FINE-GUIDE-YES         OR
        //                        NOT IDSV0003-SUCCESSFUL-RC OR
        //                        NOT IDSV0003-SUCCESSFUL-SQL
        //                 END-IF
        //           END-PERFORM.
        while (!(ws.getFlagFineGuide().isYes() || !ws.getIdsv0003().getReturnCode().isSuccessfulRc() || !ws.getIdsv0003().getSqlcode().isSuccessfulSql())) {
            // COB_CODE: PERFORM CALL-PGM-GUIDA         THRU CALL-PGM-GUIDA-EX
            callPgmGuida();
            //
            // COB_CODE:            IF IDSV0003-SUCCESSFUL-RC
            //                         END-EVALUATE
            //                      ELSE
            //           *-->          GESTIRE ERRORE
            //                                                       THRU EX-S0300
            //                      END-IF
            if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:               EVALUATE TRUE
                //                             WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->              OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                SET IDSV0003-FETCH-NEXT        TO TRUE
                //                             WHEN IDSV0003-NOT-FOUND
                //           *---   >          CAMPO $ NON TROVATO
                //                                END-IF
                //                             WHEN OTHER
                //                                                                THRU EX-S0300
                //                         END-EVALUATE
                switch (ws.getIdsv0003().getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->              OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: PERFORM E300-CARICA-DATI-ESTRATTI
                        //                                          THRU E300-EX
                        e300CaricaDatiEstratti();
                        // COB_CODE: SET IDSV0003-FETCH-NEXT        TO TRUE
                        ws.getIdsv0003().getOperazione().setFetchNext();
                        break;

                    case Idsv0003Sqlcode.NOT_FOUND://---   >          CAMPO $ NON TROVATO
                        // COB_CODE: IF IDSV0003-FETCH-FIRST
                        //              SET WK-FINE-GUIDE-YES        TO TRUE
                        //           END-IF
                        if (ws.getIdsv0003().getOperazione().isFetchFirst()) {
                            // COB_CODE: MOVE WK-PGM   TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                            // COB_CODE: MOVE WK-LABEL TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                            // COB_CODE: MOVE '5166'   TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("5166");
                            // COB_CODE: STRING 'DATI NON TROVATI SU '
                            //                  WK-PGM-GUIDA
                            //                  DELIMITED BY SIZE
                            //                  INTO IEAI9901-PARAMETRI-ERR
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "DATI NON TROVATI SU ", ws.getWkPgmGuidaFormatted());
                            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //                                        THRU EX-S0300
                            s0300RicercaGravitaErrore();
                            // COB_CODE: SET WK-FINE-GUIDE-YES        TO TRUE
                            ws.getFlagFineGuide().setYes();
                        }
                        break;

                    default:// COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                        // COB_CODE: MOVE '5166'     TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("5166");
                        // COB_CODE: STRING 'ERRORE MODULO '
                        //                   WK-PGM-GUIDA
                        //                   ' - '
                        //                   'SQLCODE '
                        //                   IDSV0003-SQLCODE
                        //                   DELIMITED BY SIZE
                        //                   INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE MODULO ", ws.getWkPgmGuidaFormatted(), " - ", "SQLCODE ", ws.getIdsv0003().getSqlcode().getSqlcodeAsString());
                        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //                                         THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //-->          GESTIRE ERRORE
                // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                // COB_CODE: MOVE '5166'     TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("5166");
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   WK-PGM-GUIDA
                //                   ' - '
                //                   'RC '
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE
                //                   INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE MODULO ", ws.getWkPgmGuidaFormatted(), " - ", "RC ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //                                         THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: E300-CARICA-DATI-ESTRATTI<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA DATI ESTRATTI
	 * ----------------------------------------------------------------*</pre>*/
    private void e300CaricaDatiEstratti() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'E300-CARICA-DATI-ESTRATTI' TO WK-LABEL.
        ws.setWkLabel("E300-CARICA-DATI-ESTRATTI");
        // COB_CODE: IF IX-TAB-PMO < WK-LIMITE-MAX-PMO
        //                      THRU VALORIZZA-OUTPUT-PMO-EX
        //           ELSE
        //                                            THRU EX-S0300
        //           END-IF.
        if (ws.getIxTabPmo() < ws.getWkLimiteMaxPmo()) {
            // COB_CODE: ADD 1                            TO IX-TAB-PMO
            ws.setIxTabPmo(Trunc.toShort(1 + ws.getIxTabPmo(), 4));
            // COB_CODE: MOVE IX-TAB-PMO                  TO WPMO-ELE-PMO-MAX
            ws.getAreaMain().setWpmoElePmoMax(ws.getIxTabPmo());
            // COB_CODE: SET WPMO-ST-INV(IX-TAB-PMO)      TO TRUE
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getStatus().setInv();
            // COB_CODE: PERFORM VALORIZZA-OUTPUT-PMO
            //                   THRU VALORIZZA-OUTPUT-PMO-EX
            valorizzaOutputPmo();
        }
        else {
            // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
            // COB_CODE: MOVE '5166'     TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("5166");
            // COB_CODE: STRING 'OVERFLOW DATI GUIDA : '
            //                   WK-PGM-GUIDA
            //                   DELIMITED BY SIZE
            //                   INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "OVERFLOW DATI GUIDA : ", ws.getWkPgmGuidaFormatted());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //                                         THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: E500-ELABORA-BUSINESS<br>
	 * <pre>----------------------------------------------------------------*
	 *  ELABORAZIONE BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void e500ElaboraBusiness() {
        // COB_CODE: MOVE 'E500-ELABORA-BUSINESS'   TO WK-LABEL.
        ws.setWkLabel("E500-ELABORA-BUSINESS");
        // COB_CODE: PERFORM E600-PRE-BUSINESS      THRU E600-EX.
        e600PreBusiness();
        // COB_CODE: PERFORM CALL-PGM-BUSINESS      THRU CALL-PGM-BUSINESS-EX.
        callPgmBusiness();
        // COB_CODE: PERFORM E700-POST-BUSINESS     THRU E700-EX.
        e700PostBusiness();
    }

    /**Original name: E600-PRE-BUSINESS<br>
	 * <pre>----------------------------------------------------------------*
	 *  prepara lancio del BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void e600PreBusiness() {
        // COB_CODE: MOVE 'E600-PRE-BUSINESS'         TO WK-LABEL.
        ws.setWkLabel("E600-PRE-BUSINESS");
        // COB_CODE: MOVE MBS-D-INPUT-MOVI-SOSP       TO IDSV0901.
        ws.getIdsv0901().setIdsv0901Formatted(moviBatchSosp.getMbsDInputMoviSospFormatted());
        // COB_CODE: MOVE MBS-DT-EFF                  TO WCOM-PERIODO-ELAB-DA
        //                                               WCOM-PERIODO-ELAB-A
        ws.getWcomIntervalloElab().setDa(TruncAbs.toInt(moviBatchSosp.getMbsDtEff(), 8));
        ws.getWcomIntervalloElab().setA(TruncAbs.toInt(moviBatchSosp.getMbsDtEff(), 8));
        // COB_CODE: MOVE IABV0006-COD-GRU-AUT-PROFIL TO WCOM-COD-GRU-AUT-PROFIL
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setCodGruAutProfil(iabv0006.getCodGruAutProfil());
        // COB_CODE: MOVE IABV0006-COD-LIV-AUT-PROFIL TO WCOM-COD-LIV-AUT-PROFIL
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setCodLivAutProfil(iabv0006.getCodLivAutProfil());
        // COB_CODE: MOVE IABV0006-CANALE-VENDITA     TO WCOM-CANALE-VENDITA
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setCanaleVendita(iabv0006.getCanaleVendita());
        // COB_CODE: MOVE IABV0006-PUNTO-VENDITA      TO WCOM-PUNTO-VENDITA
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setPuntoVendita(iabv0006.getPuntoVendita());
        // COB_CODE: IF MBS-ADESIONE
        //              MOVE MBS-ID-OGG               TO WCOM-ID-ADES
        //           END-IF
        if (ws.getFlagTpOggMbs().isAdesione()) {
            // COB_CODE: MOVE MBS-ID-OGG               TO WCOM-ID-ADES
            ws.getWcomIoStati().getLccc0261().setIdAdes(moviBatchSosp.getMbsIdOgg());
        }
        // COB_CODE: MOVE L11-ID-OGG-1RIO             TO WCOM-ID-POLIZZA.
        ws.getWcomIoStati().getLccc0261().getDatiPolizza().setIdPolizza(oggBlocco.getL11IdOgg1rio().getL11IdOgg1rio());
    }

    /**Original name: E700-POST-BUSINESS<br>
	 * <pre>----------------------------------------------------------------*
	 *  POST lancio del BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void e700PostBusiness() {
        // COB_CODE: MOVE 'E700-POST-BUSINESS'         TO WK-LABEL.
        ws.setWkLabel("E700-POST-BUSINESS");
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              PERFORM E800-AGGIORNA-JOB      THRU E800-EX
        //           END-IF.
        if (idsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM E800-AGGIORNA-JOB      THRU E800-EX
            e800AggiornaJob();
        }
    }

    /**Original name: E800-AGGIORNA-JOB<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAZIONE DATI DA ELABORARE
	 * ----------------------------------------------------------------*</pre>*/
    private void e800AggiornaJob() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'E800-AGGIORNA-JOB'          TO WK-LABEL.
        ws.setWkLabel("E800-AGGIORNA-JOB");
        // COB_CODE: SET IDSV0003-WHERE-CONDITION-01   TO TRUE
        ws.getIdsv0003().getTipologiaOperazione().setIdsv0003WhereCondition01();
        // COB_CODE: SET IDSV0003-UPDATE               TO TRUE
        ws.getIdsv0003().getOperazione().setIdsi0011Update();
        // COB_CODE: MOVE JOB-ESEGUITO-OK              TO IABV0002-STATE-CURRENT
        iabv0002.setIabv0002StateCurrent(ws.getIabv0004().getJobEseguitoOk());
        // COB_CODE: PERFORM CALL-PGM-GUIDA         THRU CALL-PGM-GUIDA-EX
        callPgmGuida();
        //
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
        //                      END-EVALUATE
        //                ELSE
        //           *-->          GESTIRE ERRORE
        //                                                 THRU EX-S0300
        //                END-IF.
        if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                      WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
            //                           CONTINUE
            //                      WHEN OTHER
            //                                                         THRU EX-S0300
            //                      END-EVALUATE
            switch (ws.getIdsv0003().getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE: CONTINUE
                //continue
                    break;

                default:// COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                    // COB_CODE: MOVE '5166'     TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("5166");
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                   WK-PGM-GUIDA
                    //                   ' - '
                    //                   'SQLCODE '
                    //                   IDSV0003-SQLCODE
                    //                   DELIMITED BY SIZE
                    //                   INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE MODULO ", ws.getWkPgmGuidaFormatted(), " - ", "SQLCODE ", ws.getIdsv0003().getSqlcode().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //                                         THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->          GESTIRE ERRORE
            // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
            // COB_CODE: MOVE '5166'     TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("5166");
            // COB_CODE: STRING 'ERRORE MODULO '
            //                   WK-PGM-GUIDA
            //                   ' - '
            //                   'RC '
            //                   IDSV0003-RETURN-CODE
            //                   DELIMITED BY SIZE
            //                   INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE MODULO ", ws.getWkPgmGuidaFormatted(), " - ", "RC ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //                                         THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: CALL-PGM-GUIDA<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL AL MODULO GUIDA
	 * ----------------------------------------------------------------*</pre>*/
    private void callPgmGuida() {
        ConcatUtil concatUtil = null;
        Ldbs3540 ldbs3540 = null;
        // COB_CODE: MOVE 'CALL-PGM-GUIDA'        TO WK-LABEL.
        ws.setWkLabel("CALL-PGM-GUIDA");
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC   TO TRUE.
        ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL  TO TRUE.
        ws.getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: CALL WK-PGM-GUIDA      USING IDSV0003
        //                                        IABV0002
        //                                        PARAM-MOVI
        //           ON EXCEPTION
        //                                            THRU EX-S0300
        //           END-CALL.
        try {
            ldbs3540 = Ldbs3540.getInstance();
            ldbs3540.run(ws.getIdsv0003(), iabv0002, ws.getParamMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
            // COB_CODE: MOVE '5166'     TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("5166");
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //                   DELIMITED BY SIZE
            //                   WK-PGM-GUIDA
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                    DELIMITED BY SIZE INTO
            //                   IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE CHIAMATA MODULO : ", ws.getWkPgmGuidaFormatted(), " - ");
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //                                         THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: CALL-PGM-BUSINESS<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL AL MODULO BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void callPgmBusiness() {
        ConcatUtil concatUtil = null;
        Loas0310 loas0310 = null;
        // COB_CODE: MOVE 'CALL-PGM-BUSINESS'        TO WK-LABEL.
        ws.setWkLabel("CALL-PGM-BUSINESS");
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0001-ESITO-OK           TO TRUE.
        idsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: CALL WK-PGM-BUSINESS      USING IDSV0001
        //                                           IABV0006
        //                                           AREA-MAIN
        //                                           WCOM-INTERVALLO-ELAB
        //                                           WCOM-IO-STATI
        //           ON EXCEPTION
        //                                            THRU EX-S0300
        //           END-CALL.
        try {
            loas0310 = Loas0310.getInstance();
            loas0310.run(idsv0001, iabv0006, ws.getAreaMain(), ws.getWcomIntervalloElab(), ws.getWcomIoStati());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //                   DELIMITED BY SIZE
            //                   WK-PGM-BUSINESS
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                    DELIMITED BY SIZE INTO
            //                   IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE CHIAMATA MODULO : ", ws.getWkPgmBusinessFormatted(), " - ");
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //                                         THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: VALORIZZA-OUTPUT-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *  COPY VALORIZZA OUTPUT TABELLE
	 * ----------------------------------------------------------------*
	 *  --> PARAMETRO MOVIMENTO
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVPMO3
	 *    ULTIMO AGG. 17 FEB 2015
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputPmo() {
        // COB_CODE: MOVE PMO-ID-PARAM-MOVI
        //             TO (SF)-ID-PTF(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().setIdPtf(ws.getParamMovi().getPmoIdParamMovi());
        // COB_CODE: MOVE PMO-ID-PARAM-MOVI
        //             TO (SF)-ID-PARAM-MOVI(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoIdParamMovi(ws.getParamMovi().getPmoIdParamMovi());
        // COB_CODE: MOVE PMO-ID-OGG
        //             TO (SF)-ID-OGG(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoIdOgg(ws.getParamMovi().getPmoIdOgg());
        // COB_CODE: MOVE PMO-TP-OGG
        //             TO (SF)-TP-OGG(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpOgg(ws.getParamMovi().getPmoTpOgg());
        // COB_CODE: MOVE PMO-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoIdMoviCrz(ws.getParamMovi().getPmoIdMoviCrz());
        // COB_CODE: IF PMO-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoIdMoviChiu().getPmoIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE PMO-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdMoviChiu().setWpmoIdMoviChiuNull(ws.getParamMovi().getPmoIdMoviChiu().getPmoIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE PMO-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdMoviChiu().setWpmoIdMoviChiu(ws.getParamMovi().getPmoIdMoviChiu().getPmoIdMoviChiu());
        }
        // COB_CODE: MOVE PMO-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoDtIniEff(ws.getParamMovi().getPmoDtIniEff());
        // COB_CODE: MOVE PMO-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoDtEndEff(ws.getParamMovi().getPmoDtEndEff());
        // COB_CODE: MOVE PMO-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoCodCompAnia(ws.getParamMovi().getPmoCodCompAnia());
        // COB_CODE: IF PMO-TP-MOVI-NULL = HIGH-VALUES
        //                TO (SF)-TP-MOVI-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-MOVI(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpMovi().getPmoTpMoviNullFormatted())) {
            // COB_CODE: MOVE PMO-TP-MOVI-NULL
            //             TO (SF)-TP-MOVI-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMoviNull(ws.getParamMovi().getPmoTpMovi().getPmoTpMoviNull());
        }
        else {
            // COB_CODE: MOVE PMO-TP-MOVI
            //             TO (SF)-TP-MOVI(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(ws.getParamMovi().getPmoTpMovi().getPmoTpMovi());
        }
        // COB_CODE: IF PMO-FRQ-MOVI-NULL = HIGH-VALUES
        //                TO (SF)-FRQ-MOVI-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-FRQ-MOVI(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoFrqMovi().getPmoFrqMoviNullFormatted())) {
            // COB_CODE: MOVE PMO-FRQ-MOVI-NULL
            //             TO (SF)-FRQ-MOVI-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().setWpmoFrqMoviNull(ws.getParamMovi().getPmoFrqMovi().getPmoFrqMoviNull());
        }
        else {
            // COB_CODE: MOVE PMO-FRQ-MOVI
            //             TO (SF)-FRQ-MOVI(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().setWpmoFrqMovi(ws.getParamMovi().getPmoFrqMovi().getPmoFrqMovi());
        }
        // COB_CODE: IF PMO-DUR-AA-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DUR-AA(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDurAa().getPmoDurAaNullFormatted())) {
            // COB_CODE: MOVE PMO-DUR-AA-NULL
            //             TO (SF)-DUR-AA-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurAa().setWpmoDurAaNull(ws.getParamMovi().getPmoDurAa().getPmoDurAaNull());
        }
        else {
            // COB_CODE: MOVE PMO-DUR-AA
            //             TO (SF)-DUR-AA(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurAa().setWpmoDurAa(ws.getParamMovi().getPmoDurAa().getPmoDurAa());
        }
        // COB_CODE: IF PMO-DUR-MM-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DUR-MM(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDurMm().getPmoDurMmNullFormatted())) {
            // COB_CODE: MOVE PMO-DUR-MM-NULL
            //             TO (SF)-DUR-MM-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurMm().setWpmoDurMmNull(ws.getParamMovi().getPmoDurMm().getPmoDurMmNull());
        }
        else {
            // COB_CODE: MOVE PMO-DUR-MM
            //             TO (SF)-DUR-MM(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurMm().setWpmoDurMm(ws.getParamMovi().getPmoDurMm().getPmoDurMm());
        }
        // COB_CODE: IF PMO-DUR-GG-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DUR-GG(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDurGg().getPmoDurGgNullFormatted())) {
            // COB_CODE: MOVE PMO-DUR-GG-NULL
            //             TO (SF)-DUR-GG-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurGg().setWpmoDurGgNull(ws.getParamMovi().getPmoDurGg().getPmoDurGgNull());
        }
        else {
            // COB_CODE: MOVE PMO-DUR-GG
            //             TO (SF)-DUR-GG(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurGg().setWpmoDurGg(ws.getParamMovi().getPmoDurGg().getPmoDurGg());
        }
        // COB_CODE: IF PMO-DT-RICOR-PREC-NULL = HIGH-VALUES
        //                TO (SF)-DT-RICOR-PREC-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DT-RICOR-PREC(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrecNullFormatted())) {
            // COB_CODE: MOVE PMO-DT-RICOR-PREC-NULL
            //             TO (SF)-DT-RICOR-PREC-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrecNull(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrecNull());
        }
        else {
            // COB_CODE: MOVE PMO-DT-RICOR-PREC
            //             TO (SF)-DT-RICOR-PREC(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrec());
        }
        // COB_CODE: IF PMO-DT-RICOR-SUCC-NULL = HIGH-VALUES
        //                TO (SF)-DT-RICOR-SUCC-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DT-RICOR-SUCC(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSuccNullFormatted())) {
            // COB_CODE: MOVE PMO-DT-RICOR-SUCC-NULL
            //             TO (SF)-DT-RICOR-SUCC-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSuccNull(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSuccNull());
        }
        else {
            // COB_CODE: MOVE PMO-DT-RICOR-SUCC
            //             TO (SF)-DT-RICOR-SUCC(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSucc());
        }
        // COB_CODE: IF PMO-PC-INTR-FRAZ-NULL = HIGH-VALUES
        //                TO (SF)-PC-INTR-FRAZ-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-PC-INTR-FRAZ(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoPcIntrFraz().getPmoPcIntrFrazNullFormatted())) {
            // COB_CODE: MOVE PMO-PC-INTR-FRAZ-NULL
            //             TO (SF)-PC-INTR-FRAZ-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcIntrFraz().setWpmoPcIntrFrazNull(ws.getParamMovi().getPmoPcIntrFraz().getPmoPcIntrFrazNull());
        }
        else {
            // COB_CODE: MOVE PMO-PC-INTR-FRAZ
            //             TO (SF)-PC-INTR-FRAZ(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcIntrFraz().setWpmoPcIntrFraz(Trunc.toDecimal(ws.getParamMovi().getPmoPcIntrFraz().getPmoPcIntrFraz(), 6, 3));
        }
        // COB_CODE: IF PMO-IMP-BNS-DA-SCO-TOT-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-DA-SCO-TOT-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IMP-BNS-DA-SCO-TOT(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoImpBnsDaScoTot().getPmoImpBnsDaScoTotNullFormatted())) {
            // COB_CODE: MOVE PMO-IMP-BNS-DA-SCO-TOT-NULL
            //             TO (SF)-IMP-BNS-DA-SCO-TOT-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().setWpmoImpBnsDaScoTotNull(ws.getParamMovi().getPmoImpBnsDaScoTot().getPmoImpBnsDaScoTotNull());
        }
        else {
            // COB_CODE: MOVE PMO-IMP-BNS-DA-SCO-TOT
            //             TO (SF)-IMP-BNS-DA-SCO-TOT(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().setWpmoImpBnsDaScoTot(Trunc.toDecimal(ws.getParamMovi().getPmoImpBnsDaScoTot().getPmoImpBnsDaScoTot(), 15, 3));
        }
        // COB_CODE: IF PMO-IMP-BNS-DA-SCO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-DA-SCO-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IMP-BNS-DA-SCO(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoImpBnsDaSco().getPmoImpBnsDaScoNullFormatted())) {
            // COB_CODE: MOVE PMO-IMP-BNS-DA-SCO-NULL
            //             TO (SF)-IMP-BNS-DA-SCO-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaSco().setWpmoImpBnsDaScoNull(ws.getParamMovi().getPmoImpBnsDaSco().getPmoImpBnsDaScoNull());
        }
        else {
            // COB_CODE: MOVE PMO-IMP-BNS-DA-SCO
            //             TO (SF)-IMP-BNS-DA-SCO(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaSco().setWpmoImpBnsDaSco(Trunc.toDecimal(ws.getParamMovi().getPmoImpBnsDaSco().getPmoImpBnsDaSco(), 15, 3));
        }
        // COB_CODE: IF PMO-PC-ANTIC-BNS-NULL = HIGH-VALUES
        //                TO (SF)-PC-ANTIC-BNS-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-PC-ANTIC-BNS(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoPcAnticBns().getPmoPcAnticBnsNullFormatted())) {
            // COB_CODE: MOVE PMO-PC-ANTIC-BNS-NULL
            //             TO (SF)-PC-ANTIC-BNS-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcAnticBns().setWpmoPcAnticBnsNull(ws.getParamMovi().getPmoPcAnticBns().getPmoPcAnticBnsNull());
        }
        else {
            // COB_CODE: MOVE PMO-PC-ANTIC-BNS
            //             TO (SF)-PC-ANTIC-BNS(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcAnticBns().setWpmoPcAnticBns(Trunc.toDecimal(ws.getParamMovi().getPmoPcAnticBns().getPmoPcAnticBns(), 6, 3));
        }
        // COB_CODE: IF PMO-TP-RINN-COLL-NULL = HIGH-VALUES
        //                TO (SF)-TP-RINN-COLL-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-RINN-COLL(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpRinnCollFormatted())) {
            // COB_CODE: MOVE PMO-TP-RINN-COLL-NULL
            //             TO (SF)-TP-RINN-COLL-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRinnColl(ws.getParamMovi().getPmoTpRinnColl());
        }
        else {
            // COB_CODE: MOVE PMO-TP-RINN-COLL
            //             TO (SF)-TP-RINN-COLL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRinnColl(ws.getParamMovi().getPmoTpRinnColl());
        }
        // COB_CODE: IF PMO-TP-RIVAL-PRE-NULL = HIGH-VALUES
        //                TO (SF)-TP-RIVAL-PRE-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-RIVAL-PRE(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpRivalPreFormatted())) {
            // COB_CODE: MOVE PMO-TP-RIVAL-PRE-NULL
            //             TO (SF)-TP-RIVAL-PRE-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRivalPre(ws.getParamMovi().getPmoTpRivalPre());
        }
        else {
            // COB_CODE: MOVE PMO-TP-RIVAL-PRE
            //             TO (SF)-TP-RIVAL-PRE(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRivalPre(ws.getParamMovi().getPmoTpRivalPre());
        }
        // COB_CODE: IF PMO-TP-RIVAL-PRSTZ-NULL = HIGH-VALUES
        //                TO (SF)-TP-RIVAL-PRSTZ-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-RIVAL-PRSTZ(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpRivalPrstzFormatted())) {
            // COB_CODE: MOVE PMO-TP-RIVAL-PRSTZ-NULL
            //             TO (SF)-TP-RIVAL-PRSTZ-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRivalPrstz(ws.getParamMovi().getPmoTpRivalPrstz());
        }
        else {
            // COB_CODE: MOVE PMO-TP-RIVAL-PRSTZ
            //             TO (SF)-TP-RIVAL-PRSTZ(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRivalPrstz(ws.getParamMovi().getPmoTpRivalPrstz());
        }
        // COB_CODE: IF PMO-FL-EVID-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-FL-EVID-RIVAL-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-FL-EVID-RIVAL(IX-TAB-PMO)
        //           END-IF
        if (Conditions.eq(ws.getParamMovi().getPmoFlEvidRival(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PMO-FL-EVID-RIVAL-NULL
            //             TO (SF)-FL-EVID-RIVAL-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoFlEvidRival(ws.getParamMovi().getPmoFlEvidRival());
        }
        else {
            // COB_CODE: MOVE PMO-FL-EVID-RIVAL
            //             TO (SF)-FL-EVID-RIVAL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoFlEvidRival(ws.getParamMovi().getPmoFlEvidRival());
        }
        // COB_CODE: IF PMO-ULT-PC-PERD-NULL = HIGH-VALUES
        //                TO (SF)-ULT-PC-PERD-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-ULT-PC-PERD(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoUltPcPerd().getPmoUltPcPerdNullFormatted())) {
            // COB_CODE: MOVE PMO-ULT-PC-PERD-NULL
            //             TO (SF)-ULT-PC-PERD-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoUltPcPerd().setWpmoUltPcPerdNull(ws.getParamMovi().getPmoUltPcPerd().getPmoUltPcPerdNull());
        }
        else {
            // COB_CODE: MOVE PMO-ULT-PC-PERD
            //             TO (SF)-ULT-PC-PERD(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoUltPcPerd().setWpmoUltPcPerd(Trunc.toDecimal(ws.getParamMovi().getPmoUltPcPerd().getPmoUltPcPerd(), 6, 3));
        }
        // COB_CODE: IF PMO-TOT-AA-GIA-PROR-NULL = HIGH-VALUES
        //                TO (SF)-TOT-AA-GIA-PROR-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TOT-AA-GIA-PROR(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTotAaGiaPror().getPmoTotAaGiaProrNullFormatted())) {
            // COB_CODE: MOVE PMO-TOT-AA-GIA-PROR-NULL
            //             TO (SF)-TOT-AA-GIA-PROR-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaProrNull(ws.getParamMovi().getPmoTotAaGiaPror().getPmoTotAaGiaProrNull());
        }
        else {
            // COB_CODE: MOVE PMO-TOT-AA-GIA-PROR
            //             TO (SF)-TOT-AA-GIA-PROR(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaPror(ws.getParamMovi().getPmoTotAaGiaPror().getPmoTotAaGiaPror());
        }
        // COB_CODE: IF PMO-TP-OPZ-NULL = HIGH-VALUES
        //                TO (SF)-TP-OPZ-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-OPZ(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpOpzFormatted())) {
            // COB_CODE: MOVE PMO-TP-OPZ-NULL
            //             TO (SF)-TP-OPZ-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpOpz(ws.getParamMovi().getPmoTpOpz());
        }
        else {
            // COB_CODE: MOVE PMO-TP-OPZ
            //             TO (SF)-TP-OPZ(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpOpz(ws.getParamMovi().getPmoTpOpz());
        }
        // COB_CODE: IF PMO-AA-REN-CER-NULL = HIGH-VALUES
        //                TO (SF)-AA-REN-CER-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-AA-REN-CER(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoAaRenCer().getPmoAaRenCerNullFormatted())) {
            // COB_CODE: MOVE PMO-AA-REN-CER-NULL
            //             TO (SF)-AA-REN-CER-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoAaRenCer().setWpmoAaRenCerNull(ws.getParamMovi().getPmoAaRenCer().getPmoAaRenCerNull());
        }
        else {
            // COB_CODE: MOVE PMO-AA-REN-CER
            //             TO (SF)-AA-REN-CER(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoAaRenCer().setWpmoAaRenCer(ws.getParamMovi().getPmoAaRenCer().getPmoAaRenCer());
        }
        // COB_CODE: IF PMO-PC-REVRSB-NULL = HIGH-VALUES
        //                TO (SF)-PC-REVRSB-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-PC-REVRSB(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoPcRevrsb().getPmoPcRevrsbNullFormatted())) {
            // COB_CODE: MOVE PMO-PC-REVRSB-NULL
            //             TO (SF)-PC-REVRSB-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcRevrsb().setWpmoPcRevrsbNull(ws.getParamMovi().getPmoPcRevrsb().getPmoPcRevrsbNull());
        }
        else {
            // COB_CODE: MOVE PMO-PC-REVRSB
            //             TO (SF)-PC-REVRSB(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcRevrsb().setWpmoPcRevrsb(Trunc.toDecimal(ws.getParamMovi().getPmoPcRevrsb().getPmoPcRevrsb(), 6, 3));
        }
        // COB_CODE: IF PMO-IMP-RISC-PARZ-PRGT-NULL = HIGH-VALUES
        //                TO (SF)-IMP-RISC-PARZ-PRGT-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IMP-RISC-PARZ-PRGT(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoImpRiscParzPrgt().getPmoImpRiscParzPrgtNullFormatted())) {
            // COB_CODE: MOVE PMO-IMP-RISC-PARZ-PRGT-NULL
            //             TO (SF)-IMP-RISC-PARZ-PRGT-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().setWpmoImpRiscParzPrgtNull(ws.getParamMovi().getPmoImpRiscParzPrgt().getPmoImpRiscParzPrgtNull());
        }
        else {
            // COB_CODE: MOVE PMO-IMP-RISC-PARZ-PRGT
            //             TO (SF)-IMP-RISC-PARZ-PRGT(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().setWpmoImpRiscParzPrgt(Trunc.toDecimal(ws.getParamMovi().getPmoImpRiscParzPrgt().getPmoImpRiscParzPrgt(), 15, 3));
        }
        // COB_CODE: IF PMO-IMP-LRD-DI-RAT-NULL = HIGH-VALUES
        //                TO (SF)-IMP-LRD-DI-RAT-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IMP-LRD-DI-RAT(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoImpLrdDiRat().getPmoImpLrdDiRatNullFormatted())) {
            // COB_CODE: MOVE PMO-IMP-LRD-DI-RAT-NULL
            //             TO (SF)-IMP-LRD-DI-RAT-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpLrdDiRat().setWpmoImpLrdDiRatNull(ws.getParamMovi().getPmoImpLrdDiRat().getPmoImpLrdDiRatNull());
        }
        else {
            // COB_CODE: MOVE PMO-IMP-LRD-DI-RAT
            //             TO (SF)-IMP-LRD-DI-RAT(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpLrdDiRat().setWpmoImpLrdDiRat(Trunc.toDecimal(ws.getParamMovi().getPmoImpLrdDiRat().getPmoImpLrdDiRat(), 15, 3));
        }
        // COB_CODE: IF PMO-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IB-OGG(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoIbOgg(), ParamMoviLdbs1470.Len.PMO_IB_OGG)) {
            // COB_CODE: MOVE PMO-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoIbOgg(ws.getParamMovi().getPmoIbOgg());
        }
        else {
            // COB_CODE: MOVE PMO-IB-OGG
            //             TO (SF)-IB-OGG(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoIbOgg(ws.getParamMovi().getPmoIbOgg());
        }
        // COB_CODE: IF PMO-COS-ONER-NULL = HIGH-VALUES
        //                TO (SF)-COS-ONER-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-COS-ONER(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoCosOner().getPmoCosOnerNullFormatted())) {
            // COB_CODE: MOVE PMO-COS-ONER-NULL
            //             TO (SF)-COS-ONER-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCosOner().setWpmoCosOnerNull(ws.getParamMovi().getPmoCosOner().getPmoCosOnerNull());
        }
        else {
            // COB_CODE: MOVE PMO-COS-ONER
            //             TO (SF)-COS-ONER(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCosOner().setWpmoCosOner(Trunc.toDecimal(ws.getParamMovi().getPmoCosOner().getPmoCosOner(), 15, 3));
        }
        // COB_CODE: IF PMO-SPE-PC-NULL = HIGH-VALUES
        //                TO (SF)-SPE-PC-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-SPE-PC(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoSpePc().getPmoSpePcNullFormatted())) {
            // COB_CODE: MOVE PMO-SPE-PC-NULL
            //             TO (SF)-SPE-PC-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoSpePc().setWpmoSpePcNull(ws.getParamMovi().getPmoSpePc().getPmoSpePcNull());
        }
        else {
            // COB_CODE: MOVE PMO-SPE-PC
            //             TO (SF)-SPE-PC(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoSpePc().setWpmoSpePc(Trunc.toDecimal(ws.getParamMovi().getPmoSpePc().getPmoSpePc(), 6, 3));
        }
        // COB_CODE: IF PMO-FL-ATTIV-GAR-NULL = HIGH-VALUES
        //                TO (SF)-FL-ATTIV-GAR-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-FL-ATTIV-GAR(IX-TAB-PMO)
        //           END-IF
        if (Conditions.eq(ws.getParamMovi().getPmoFlAttivGar(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PMO-FL-ATTIV-GAR-NULL
            //             TO (SF)-FL-ATTIV-GAR-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoFlAttivGar(ws.getParamMovi().getPmoFlAttivGar());
        }
        else {
            // COB_CODE: MOVE PMO-FL-ATTIV-GAR
            //             TO (SF)-FL-ATTIV-GAR(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoFlAttivGar(ws.getParamMovi().getPmoFlAttivGar());
        }
        // COB_CODE: IF PMO-CAMBIO-VER-PROD-NULL = HIGH-VALUES
        //                TO (SF)-CAMBIO-VER-PROD-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-CAMBIO-VER-PROD(IX-TAB-PMO)
        //           END-IF
        if (Conditions.eq(ws.getParamMovi().getPmoCambioVerProd(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PMO-CAMBIO-VER-PROD-NULL
            //             TO (SF)-CAMBIO-VER-PROD-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoCambioVerProd(ws.getParamMovi().getPmoCambioVerProd());
        }
        else {
            // COB_CODE: MOVE PMO-CAMBIO-VER-PROD
            //             TO (SF)-CAMBIO-VER-PROD(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoCambioVerProd(ws.getParamMovi().getPmoCambioVerProd());
        }
        // COB_CODE: IF PMO-MM-DIFF-NULL = HIGH-VALUES
        //                TO (SF)-MM-DIFF-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-MM-DIFF(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoMmDiff().getPmoMmDiffNullFormatted())) {
            // COB_CODE: MOVE PMO-MM-DIFF-NULL
            //             TO (SF)-MM-DIFF-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoMmDiff().setWpmoMmDiffNull(ws.getParamMovi().getPmoMmDiff().getPmoMmDiffNull());
        }
        else {
            // COB_CODE: MOVE PMO-MM-DIFF
            //             TO (SF)-MM-DIFF(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoMmDiff().setWpmoMmDiff(ws.getParamMovi().getPmoMmDiff().getPmoMmDiff());
        }
        // COB_CODE: IF PMO-IMP-RAT-MANFEE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-RAT-MANFEE-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IMP-RAT-MANFEE(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoImpRatManfee().getPmoImpRatManfeeNullFormatted())) {
            // COB_CODE: MOVE PMO-IMP-RAT-MANFEE-NULL
            //             TO (SF)-IMP-RAT-MANFEE-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfeeNull(ws.getParamMovi().getPmoImpRatManfee().getPmoImpRatManfeeNull());
        }
        else {
            // COB_CODE: MOVE PMO-IMP-RAT-MANFEE
            //             TO (SF)-IMP-RAT-MANFEE(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfee(Trunc.toDecimal(ws.getParamMovi().getPmoImpRatManfee().getPmoImpRatManfee(), 15, 3));
        }
        // COB_CODE: IF PMO-DT-ULT-EROG-MANFEE-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EROG-MANFEE-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DT-ULT-EROG-MANFEE(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDtUltErogManfee().getPmoDtUltErogManfeeNullFormatted())) {
            // COB_CODE: MOVE PMO-DT-ULT-EROG-MANFEE-NULL
            //             TO (SF)-DT-ULT-EROG-MANFEE-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtUltErogManfee().setWpmoDtUltErogManfeeNull(ws.getParamMovi().getPmoDtUltErogManfee().getPmoDtUltErogManfeeNull());
        }
        else {
            // COB_CODE: MOVE PMO-DT-ULT-EROG-MANFEE
            //             TO (SF)-DT-ULT-EROG-MANFEE(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtUltErogManfee().setWpmoDtUltErogManfee(ws.getParamMovi().getPmoDtUltErogManfee().getPmoDtUltErogManfee());
        }
        // COB_CODE: IF PMO-TP-OGG-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-TP-OGG-RIVAL-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-OGG-RIVAL(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpOggRivalFormatted())) {
            // COB_CODE: MOVE PMO-TP-OGG-RIVAL-NULL
            //             TO (SF)-TP-OGG-RIVAL-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpOggRival(ws.getParamMovi().getPmoTpOggRival());
        }
        else {
            // COB_CODE: MOVE PMO-TP-OGG-RIVAL
            //             TO (SF)-TP-OGG-RIVAL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpOggRival(ws.getParamMovi().getPmoTpOggRival());
        }
        // COB_CODE: IF PMO-SOM-ASSTA-GARAC-NULL = HIGH-VALUES
        //                TO (SF)-SOM-ASSTA-GARAC-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-SOM-ASSTA-GARAC(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoSomAsstaGarac().getPmoSomAsstaGaracNullFormatted())) {
            // COB_CODE: MOVE PMO-SOM-ASSTA-GARAC-NULL
            //             TO (SF)-SOM-ASSTA-GARAC-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoSomAsstaGarac().setWpmoSomAsstaGaracNull(ws.getParamMovi().getPmoSomAsstaGarac().getPmoSomAsstaGaracNull());
        }
        else {
            // COB_CODE: MOVE PMO-SOM-ASSTA-GARAC
            //             TO (SF)-SOM-ASSTA-GARAC(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoSomAsstaGarac().setWpmoSomAsstaGarac(Trunc.toDecimal(ws.getParamMovi().getPmoSomAsstaGarac().getPmoSomAsstaGarac(), 15, 3));
        }
        // COB_CODE: IF PMO-PC-APPLZ-OPZ-NULL = HIGH-VALUES
        //                TO (SF)-PC-APPLZ-OPZ-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-PC-APPLZ-OPZ(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoPcApplzOpz().getPmoPcApplzOpzNullFormatted())) {
            // COB_CODE: MOVE PMO-PC-APPLZ-OPZ-NULL
            //             TO (SF)-PC-APPLZ-OPZ-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcApplzOpz().setWpmoPcApplzOpzNull(ws.getParamMovi().getPmoPcApplzOpz().getPmoPcApplzOpzNull());
        }
        else {
            // COB_CODE: MOVE PMO-PC-APPLZ-OPZ
            //             TO (SF)-PC-APPLZ-OPZ(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcApplzOpz().setWpmoPcApplzOpz(Trunc.toDecimal(ws.getParamMovi().getPmoPcApplzOpz().getPmoPcApplzOpz(), 6, 3));
        }
        // COB_CODE: IF PMO-ID-ADES-NULL = HIGH-VALUES
        //                TO (SF)-ID-ADES-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-ID-ADES(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoIdAdes().getPmoIdAdesNullFormatted())) {
            // COB_CODE: MOVE PMO-ID-ADES-NULL
            //             TO (SF)-ID-ADES-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdAdes().setWpmoIdAdesNull(ws.getParamMovi().getPmoIdAdes().getPmoIdAdesNull());
        }
        else {
            // COB_CODE: MOVE PMO-ID-ADES
            //             TO (SF)-ID-ADES(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdAdes().setWpmoIdAdes(ws.getParamMovi().getPmoIdAdes().getPmoIdAdes());
        }
        // COB_CODE: MOVE PMO-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoIdPoli(ws.getParamMovi().getPmoIdPoli());
        // COB_CODE: MOVE PMO-TP-FRM-ASSVA
        //             TO (SF)-TP-FRM-ASSVA(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpFrmAssva(ws.getParamMovi().getPmoTpFrmAssva());
        // COB_CODE: MOVE PMO-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsRiga(ws.getParamMovi().getPmoDsRiga());
        // COB_CODE: MOVE PMO-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsOperSql(ws.getParamMovi().getPmoDsOperSql());
        // COB_CODE: MOVE PMO-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsVer(ws.getParamMovi().getPmoDsVer());
        // COB_CODE: MOVE PMO-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsTsIniCptz(ws.getParamMovi().getPmoDsTsIniCptz());
        // COB_CODE: MOVE PMO-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsTsEndCptz(ws.getParamMovi().getPmoDsTsEndCptz());
        // COB_CODE: MOVE PMO-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsUtente(ws.getParamMovi().getPmoDsUtente());
        // COB_CODE: MOVE PMO-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-PMO)
        ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsStatoElab(ws.getParamMovi().getPmoDsStatoElab());
        // COB_CODE: IF PMO-TP-ESTR-CNT-NULL = HIGH-VALUES
        //                TO (SF)-TP-ESTR-CNT-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-ESTR-CNT(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpEstrCntFormatted())) {
            // COB_CODE: MOVE PMO-TP-ESTR-CNT-NULL
            //             TO (SF)-TP-ESTR-CNT-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpEstrCnt(ws.getParamMovi().getPmoTpEstrCnt());
        }
        else {
            // COB_CODE: MOVE PMO-TP-ESTR-CNT
            //             TO (SF)-TP-ESTR-CNT(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpEstrCnt(ws.getParamMovi().getPmoTpEstrCnt());
        }
        // COB_CODE: IF PMO-COD-RAMO-NULL = HIGH-VALUES
        //                TO (SF)-COD-RAMO-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-COD-RAMO(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoCodRamoFormatted())) {
            // COB_CODE: MOVE PMO-COD-RAMO-NULL
            //             TO (SF)-COD-RAMO-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoCodRamo(ws.getParamMovi().getPmoCodRamo());
        }
        else {
            // COB_CODE: MOVE PMO-COD-RAMO
            //             TO (SF)-COD-RAMO(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoCodRamo(ws.getParamMovi().getPmoCodRamo());
        }
        // COB_CODE: IF PMO-GEN-DA-SIN-NULL = HIGH-VALUES
        //                TO (SF)-GEN-DA-SIN-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-GEN-DA-SIN(IX-TAB-PMO)
        //           END-IF
        if (Conditions.eq(ws.getParamMovi().getPmoGenDaSin(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PMO-GEN-DA-SIN-NULL
            //             TO (SF)-GEN-DA-SIN-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoGenDaSin(ws.getParamMovi().getPmoGenDaSin());
        }
        else {
            // COB_CODE: MOVE PMO-GEN-DA-SIN
            //             TO (SF)-GEN-DA-SIN(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoGenDaSin(ws.getParamMovi().getPmoGenDaSin());
        }
        // COB_CODE: IF PMO-COD-TARI-NULL = HIGH-VALUES
        //                TO (SF)-COD-TARI-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-COD-TARI(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoCodTariFormatted())) {
            // COB_CODE: MOVE PMO-COD-TARI-NULL
            //             TO (SF)-COD-TARI-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoCodTari(ws.getParamMovi().getPmoCodTari());
        }
        else {
            // COB_CODE: MOVE PMO-COD-TARI
            //             TO (SF)-COD-TARI(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoCodTari(ws.getParamMovi().getPmoCodTari());
        }
        // COB_CODE: IF PMO-NUM-RAT-PAG-PRE-NULL = HIGH-VALUES
        //                TO (SF)-NUM-RAT-PAG-PRE-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-NUM-RAT-PAG-PRE(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoNumRatPagPre().getPmoNumRatPagPreNullFormatted())) {
            // COB_CODE: MOVE PMO-NUM-RAT-PAG-PRE-NULL
            //             TO (SF)-NUM-RAT-PAG-PRE-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoNumRatPagPre().setWpmoNumRatPagPreNull(ws.getParamMovi().getPmoNumRatPagPre().getPmoNumRatPagPreNull());
        }
        else {
            // COB_CODE: MOVE PMO-NUM-RAT-PAG-PRE
            //             TO (SF)-NUM-RAT-PAG-PRE(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoNumRatPagPre().setWpmoNumRatPagPre(ws.getParamMovi().getPmoNumRatPagPre().getPmoNumRatPagPre());
        }
        // COB_CODE: IF PMO-PC-SERV-VAL-NULL = HIGH-VALUES
        //                TO (SF)-PC-SERV-VAL-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-PC-SERV-VAL(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoPcServVal().getPmoPcServValNullFormatted())) {
            // COB_CODE: MOVE PMO-PC-SERV-VAL-NULL
            //             TO (SF)-PC-SERV-VAL-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcServVal().setWpmoPcServValNull(ws.getParamMovi().getPmoPcServVal().getPmoPcServValNull());
        }
        else {
            // COB_CODE: MOVE PMO-PC-SERV-VAL
            //             TO (SF)-PC-SERV-VAL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcServVal().setWpmoPcServVal(Trunc.toDecimal(ws.getParamMovi().getPmoPcServVal().getPmoPcServVal(), 6, 3));
        }
        // COB_CODE: IF PMO-ETA-AA-SOGL-BNFICR-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-SOGL-BNFICR-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-ETA-AA-SOGL-BNFICR(IX-TAB-PMO)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoEtaAaSoglBnficr().getPmoEtaAaSoglBnficrNullFormatted())) {
            // COB_CODE: MOVE PMO-ETA-AA-SOGL-BNFICR-NULL
            //             TO (SF)-ETA-AA-SOGL-BNFICR-NULL(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().setWpmoEtaAaSoglBnficrNull(ws.getParamMovi().getPmoEtaAaSoglBnficr().getPmoEtaAaSoglBnficrNull());
        }
        else {
            // COB_CODE: MOVE PMO-ETA-AA-SOGL-BNFICR
            //             TO (SF)-ETA-AA-SOGL-BNFICR(IX-TAB-PMO)
            ws.getAreaMain().getWpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().setWpmoEtaAaSoglBnficr(ws.getParamMovi().getPmoEtaAaSoglBnficr().getPmoEtaAaSoglBnficr());
        }
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        idsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        idsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(idsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            idsv0001.setMaxEleErrori(Trunc.toShort(1 + idsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        idsv0001.setMaxEleErrori(Trunc.toShort(1 + idsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (idsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                idsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                idsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                idsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                idsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            idsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            idsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            idsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            idsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            idsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            idsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            idsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            idsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            idsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        idsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        idsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        idsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        idsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        idsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        idsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        idsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        idsv0001.getEleErrori(idsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    public void initIdsv0901() {
        ws.getIdsv0901().setGenerici("");
        ws.getIdsv0901().setFunz("");
    }

    public void initWcomIntervalloElab() {
        ws.getWcomIntervalloElab().setDaFormatted("00000000");
        ws.getWcomIntervalloElab().setAFormatted("00000000");
    }

    public void initWcomIoStati() {
        ws.getWcomIoStati().getLccc0001().getTipoOperazione().setTipoOperazione("");
        ws.getWcomIoStati().getLccc0001().getNavigabilita().setNavigabilita(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getTastiDaAbilitare().setAnnulla(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getTastiDaAbilitare().setDaAutorizzare(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getTastiDaAbilitare().setAutorizza(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getTastiDaAbilitare().setAggiornaPtf(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getTastiDaAbilitare().setRespingi(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getTastiDaAbilitare().setElimina(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getTastiDaAbilitare().setAvanti(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getTastiDaAbilitare().setIndietro(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getTastiDaAbilitare().setNote(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().setWcomDtUltVersProdFormatted("00000000");
        ws.getWcomIoStati().getLccc0001().getStati().setStatoMovimento("");
        ws.getWcomIoStati().getLccc0001().getStati().getMovFlagStFinale().setMovFlagStFinale(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getStati().setStatoOggDeroga("");
        ws.getWcomIoStati().getLccc0001().getStati().getDerFlagStFinale().setDerFlagStFinale(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getStati().setStatoRichiestaEst("");
        ws.getWcomIoStati().getLccc0001().getStati().getRicFlagStFinale().setRicFlagStFinale(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setKeyAutOper1("");
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setKeyAutOper2("");
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setKeyAutOper3("");
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setKeyAutOper4("");
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setKeyAutOper5("");
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setCodProcessoWf("");
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setCodAttivitaWf("");
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setWcomNumEleVariabiliCondFormatted("000");
        for (int idx0 = 1; idx0 <= WcomDatiDeroghe.VARIABILI_COND_WF_MAXOCCURS; idx0++) {
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getVariabiliCondWf(idx0).setCodOperandoWf("");
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getVariabiliCondWf(idx0).setValOperandoWf("");
        }
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setCodLivAutProfil(0);
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setCodGruAutProfil(0);
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setCanaleVendita(0);
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setPuntoVendita("");
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getOggettoDeroga().setIdOggDeroga(0);
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getOggettoDeroga().setCodLivAutAppartza(0);
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getOggettoDeroga().setCodGruAutAppartza(0);
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getOggettoDeroga().setCodLivAutSuper(0);
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getOggettoDeroga().setCodGruAutSuper(0);
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getOggettoDeroga().setTipoDeroga("");
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getOggettoDeroga().setIbOgg("");
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getOggettoDeroga().setIdOgg(0);
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getOggettoDeroga().setTpOgg("");
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getOggettoDeroga().setDsVer(0);
        ws.getWcomIoStati().getLccc0001().getDatiDeroghe().setNumEleMotDeroga(((short)0));
        for (int idx0 = 1; idx0 <= WcomDatiDeroghe.TAB_MOT_DEROGA_MAXOCCURS; idx0++) {
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).getStatus().setStatus(Types.SPACE_CHAR);
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).setIdMotDeroga(0);
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).setCodErr("");
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).setTpErr("");
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).setDescrizioneErr("");
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).setTpMotDeroga("");
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).setCodLivAutorizzativo(0);
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).setIdcForzabilita(Types.SPACE_CHAR);
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).setMotDerDtEffetto(0);
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).setMotDerDtCompetenza(0);
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).setMotDerDsVer(0);
            ws.getWcomIoStati().getLccc0001().getDatiDeroghe().getTabMotDeroga(idx0).setMotDerStepElab(Types.SPACE_CHAR);
        }
        ws.getWcomIoStati().getLccc0001().setEleMovAnnullMax(((short)0));
        for (int idx0 = 1; idx0 <= Lccc00011.TAB_MOV_ANNULL_MAXOCCURS; idx0++) {
            ws.getWcomIoStati().getLccc0001().getTabMovAnnull(idx0).setWcomIdMoviAnn(0);
        }
        ws.getWcomIoStati().getLccc0001().setIdMoviCrz(0);
        ws.getWcomIoStati().getLccc0001().setFlagTariffaRischio(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().setEleMaxPlatfond(((short)0));
        for (int idx0 = 1; idx0 <= Lccc00011.TAB_PLATFOND_MAXOCCURS; idx0++) {
            ws.getWcomIoStati().getLccc0001().getTabPlatfond(idx0).setImpPlatfond(new AfDecimal(0, 18, 3));
            ws.getWcomIoStati().getLccc0001().getTabPlatfond(idx0).setCodFondo("");
            ws.getWcomIoStati().getLccc0001().getTabPlatfond(idx0).setPercFondOld(new AfDecimal(0, 6, 3));
            ws.getWcomIoStati().getLccc0001().getTabPlatfond(idx0).setImportoOld(new AfDecimal(0, 15, 3));
        }
        ws.getWcomIoStati().getLccc0001().getModificaDtdecor().setModificaDtdecor(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().getStatusDer().setStatus(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0001().setIdRichEst(0);
        ws.getWcomIoStati().getLccc0001().setCodiceIniziativa("");
        ws.getWcomIoStati().getLccc0001().getTpVisualizPag().setTpVisualizPag(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0261().getBsCallType().setBsCallType(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0261().getDatiPolizza().setIdPolizza(0);
        ws.getWcomIoStati().getLccc0261().getDatiPolizza().setPolIbOgg("");
        ws.getWcomIoStati().getLccc0261().getDatiPolizza().getLivelloGenTit().setLivelloGenTit("");
        ws.getWcomIoStati().getLccc0261().setIdAdes(0);
        ws.getWcomIoStati().getLccc0261().setAdeIbOgg("");
        ws.getWcomIoStati().getLccc0261().setIdMoviCreaz(0);
        ws.getWcomIoStati().getLccc0261().getPrimaVolta().setPrimaVolta(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0261().getStepSucc().setStepSucc(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0261().getWrite().setWrite("");
        ws.getWcomIoStati().getLccc0261().getDatiMbs().getWcomTpOggMbs().setWcomTpOggMbs("");
        ws.getWcomIoStati().getLccc0261().getDatiMbs().getWcomIdBatch().setWcomIdBatch(0);
        ws.getWcomIoStati().getLccc0261().getDatiMbs().getWcomIdJob().setWcomIdJob(0);
        ws.getWcomIoStati().getLccc0261().getDatiMbs().getWcomTpFrmAssva().setWcomTpFrmAssva("");
        ws.getWcomIoStati().getLccc0261().getDatiMbs().setWcomStepElab(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0261().getDatiMbs().setWcomDInputMoviSosp("");
        ws.getWcomIoStati().getLccc0261().getDatiBlocco().getVerificaBlocco().setVerificaBlocco(Types.SPACE_CHAR);
        ws.getWcomIoStati().getLccc0261().getDatiBlocco().setIdBloccoCrz(0);
        ws.getWcomIoStati().getLccc0261().getDatiBlocco().getTpOggBlocco().setTpOggBlocco("");
    }
}
