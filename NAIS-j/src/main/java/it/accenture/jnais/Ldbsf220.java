package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DettTitContDao;
import it.accenture.jnais.commons.data.to.IDettTitCont;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DettTitContIdbsdtc0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbsf220Data;
import it.accenture.jnais.ws.redefines.DtcAcqExp;
import it.accenture.jnais.ws.redefines.DtcCarAcq;
import it.accenture.jnais.ws.redefines.DtcCarGest;
import it.accenture.jnais.ws.redefines.DtcCarIas;
import it.accenture.jnais.ws.redefines.DtcCarInc;
import it.accenture.jnais.ws.redefines.DtcCnbtAntirac;
import it.accenture.jnais.ws.redefines.DtcCommisInter;
import it.accenture.jnais.ws.redefines.DtcDir;
import it.accenture.jnais.ws.redefines.DtcDtEndCop;
import it.accenture.jnais.ws.redefines.DtcDtEsiTit;
import it.accenture.jnais.ws.redefines.DtcDtIniCop;
import it.accenture.jnais.ws.redefines.DtcFrqMovi;
import it.accenture.jnais.ws.redefines.DtcIdMoviChiu;
import it.accenture.jnais.ws.redefines.DtcImpAder;
import it.accenture.jnais.ws.redefines.DtcImpAz;
import it.accenture.jnais.ws.redefines.DtcImpTfr;
import it.accenture.jnais.ws.redefines.DtcImpTfrStrc;
import it.accenture.jnais.ws.redefines.DtcImpTrasfe;
import it.accenture.jnais.ws.redefines.DtcImpVolo;
import it.accenture.jnais.ws.redefines.DtcIntrFraz;
import it.accenture.jnais.ws.redefines.DtcIntrMora;
import it.accenture.jnais.ws.redefines.DtcIntrRetdt;
import it.accenture.jnais.ws.redefines.DtcIntrRiat;
import it.accenture.jnais.ws.redefines.DtcManfeeAntic;
import it.accenture.jnais.ws.redefines.DtcManfeeRec;
import it.accenture.jnais.ws.redefines.DtcManfeeRicor;
import it.accenture.jnais.ws.redefines.DtcNumGgRitardoPag;
import it.accenture.jnais.ws.redefines.DtcNumGgRival;
import it.accenture.jnais.ws.redefines.DtcPreNet;
import it.accenture.jnais.ws.redefines.DtcPrePpIas;
import it.accenture.jnais.ws.redefines.DtcPreSoloRsh;
import it.accenture.jnais.ws.redefines.DtcPreTot;
import it.accenture.jnais.ws.redefines.DtcProvAcq1aa;
import it.accenture.jnais.ws.redefines.DtcProvAcq2aa;
import it.accenture.jnais.ws.redefines.DtcProvDaRec;
import it.accenture.jnais.ws.redefines.DtcProvInc;
import it.accenture.jnais.ws.redefines.DtcProvRicor;
import it.accenture.jnais.ws.redefines.DtcRemunAss;
import it.accenture.jnais.ws.redefines.DtcSoprAlt;
import it.accenture.jnais.ws.redefines.DtcSoprProf;
import it.accenture.jnais.ws.redefines.DtcSoprSan;
import it.accenture.jnais.ws.redefines.DtcSoprSpo;
import it.accenture.jnais.ws.redefines.DtcSoprTec;
import it.accenture.jnais.ws.redefines.DtcSpeAge;
import it.accenture.jnais.ws.redefines.DtcSpeMed;
import it.accenture.jnais.ws.redefines.DtcTax;
import it.accenture.jnais.ws.redefines.DtcTotIntrPrest;

/**Original name: LDBSF220<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  25 GIU 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbsf220 extends Program implements IDettTitCont {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DettTitContDao dettTitContDao = new DettTitContDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbsf220Data ws = new Ldbsf220Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: DETT-TIT-CONT
    private DettTitContIdbsdtc0 dettTitCont;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBSF220_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, DettTitContIdbsdtc0 dettTitCont) {
        this.idsv0003 = idsv0003;
        this.dettTitCont = dettTitCont;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbsf220 getInstance() {
        return ((Ldbsf220)Programs.getInstance(Ldbsf220.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBSF220'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBSF220");
        // COB_CODE: MOVE 'DETT-TIT-CONT' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("DETT-TIT-CONT");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_DETT_TIT_CONT
        //                    ,ID_TIT_CONT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,DT_ESI_TIT
        //                    ,SPE_AGE
        //                    ,CAR_IAS
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,NUM_GG_RITARDO_PAG
        //                    ,NUM_GG_RIVAL
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //              FROM DETT_TIT_CONT
        //              WHERE  TP_OGG = :DTC-TP-OGG
        //                    AND ID_OGG =  :DTC-ID-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DS_TS_INI_CPTZ
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_DETT_TIT_CONT
        //                    ,ID_TIT_CONT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,DT_ESI_TIT
        //                    ,SPE_AGE
        //                    ,CAR_IAS
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,NUM_GG_RITARDO_PAG
        //                    ,NUM_GG_RIVAL
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //             INTO
        //                :DTC-ID-DETT-TIT-CONT
        //               ,:DTC-ID-TIT-CONT
        //               ,:DTC-ID-OGG
        //               ,:DTC-TP-OGG
        //               ,:DTC-ID-MOVI-CRZ
        //               ,:DTC-ID-MOVI-CHIU
        //                :IND-DTC-ID-MOVI-CHIU
        //               ,:DTC-DT-INI-EFF-DB
        //               ,:DTC-DT-END-EFF-DB
        //               ,:DTC-COD-COMP-ANIA
        //               ,:DTC-DT-INI-COP-DB
        //                :IND-DTC-DT-INI-COP
        //               ,:DTC-DT-END-COP-DB
        //                :IND-DTC-DT-END-COP
        //               ,:DTC-PRE-NET
        //                :IND-DTC-PRE-NET
        //               ,:DTC-INTR-FRAZ
        //                :IND-DTC-INTR-FRAZ
        //               ,:DTC-INTR-MORA
        //                :IND-DTC-INTR-MORA
        //               ,:DTC-INTR-RETDT
        //                :IND-DTC-INTR-RETDT
        //               ,:DTC-INTR-RIAT
        //                :IND-DTC-INTR-RIAT
        //               ,:DTC-DIR
        //                :IND-DTC-DIR
        //               ,:DTC-SPE-MED
        //                :IND-DTC-SPE-MED
        //               ,:DTC-TAX
        //                :IND-DTC-TAX
        //               ,:DTC-SOPR-SAN
        //                :IND-DTC-SOPR-SAN
        //               ,:DTC-SOPR-SPO
        //                :IND-DTC-SOPR-SPO
        //               ,:DTC-SOPR-TEC
        //                :IND-DTC-SOPR-TEC
        //               ,:DTC-SOPR-PROF
        //                :IND-DTC-SOPR-PROF
        //               ,:DTC-SOPR-ALT
        //                :IND-DTC-SOPR-ALT
        //               ,:DTC-PRE-TOT
        //                :IND-DTC-PRE-TOT
        //               ,:DTC-PRE-PP-IAS
        //                :IND-DTC-PRE-PP-IAS
        //               ,:DTC-PRE-SOLO-RSH
        //                :IND-DTC-PRE-SOLO-RSH
        //               ,:DTC-CAR-ACQ
        //                :IND-DTC-CAR-ACQ
        //               ,:DTC-CAR-GEST
        //                :IND-DTC-CAR-GEST
        //               ,:DTC-CAR-INC
        //                :IND-DTC-CAR-INC
        //               ,:DTC-PROV-ACQ-1AA
        //                :IND-DTC-PROV-ACQ-1AA
        //               ,:DTC-PROV-ACQ-2AA
        //                :IND-DTC-PROV-ACQ-2AA
        //               ,:DTC-PROV-RICOR
        //                :IND-DTC-PROV-RICOR
        //               ,:DTC-PROV-INC
        //                :IND-DTC-PROV-INC
        //               ,:DTC-PROV-DA-REC
        //                :IND-DTC-PROV-DA-REC
        //               ,:DTC-COD-DVS
        //                :IND-DTC-COD-DVS
        //               ,:DTC-FRQ-MOVI
        //                :IND-DTC-FRQ-MOVI
        //               ,:DTC-TP-RGM-FISC
        //               ,:DTC-COD-TARI
        //                :IND-DTC-COD-TARI
        //               ,:DTC-TP-STAT-TIT
        //               ,:DTC-IMP-AZ
        //                :IND-DTC-IMP-AZ
        //               ,:DTC-IMP-ADER
        //                :IND-DTC-IMP-ADER
        //               ,:DTC-IMP-TFR
        //                :IND-DTC-IMP-TFR
        //               ,:DTC-IMP-VOLO
        //                :IND-DTC-IMP-VOLO
        //               ,:DTC-MANFEE-ANTIC
        //                :IND-DTC-MANFEE-ANTIC
        //               ,:DTC-MANFEE-RICOR
        //                :IND-DTC-MANFEE-RICOR
        //               ,:DTC-MANFEE-REC
        //                :IND-DTC-MANFEE-REC
        //               ,:DTC-DT-ESI-TIT-DB
        //                :IND-DTC-DT-ESI-TIT
        //               ,:DTC-SPE-AGE
        //                :IND-DTC-SPE-AGE
        //               ,:DTC-CAR-IAS
        //                :IND-DTC-CAR-IAS
        //               ,:DTC-TOT-INTR-PREST
        //                :IND-DTC-TOT-INTR-PREST
        //               ,:DTC-DS-RIGA
        //               ,:DTC-DS-OPER-SQL
        //               ,:DTC-DS-VER
        //               ,:DTC-DS-TS-INI-CPTZ
        //               ,:DTC-DS-TS-END-CPTZ
        //               ,:DTC-DS-UTENTE
        //               ,:DTC-DS-STATO-ELAB
        //               ,:DTC-IMP-TRASFE
        //                :IND-DTC-IMP-TRASFE
        //               ,:DTC-IMP-TFR-STRC
        //                :IND-DTC-IMP-TFR-STRC
        //               ,:DTC-NUM-GG-RITARDO-PAG
        //                :IND-DTC-NUM-GG-RITARDO-PAG
        //               ,:DTC-NUM-GG-RIVAL
        //                :IND-DTC-NUM-GG-RIVAL
        //               ,:DTC-ACQ-EXP
        //                :IND-DTC-ACQ-EXP
        //               ,:DTC-REMUN-ASS
        //                :IND-DTC-REMUN-ASS
        //               ,:DTC-COMMIS-INTER
        //                :IND-DTC-COMMIS-INTER
        //               ,:DTC-CNBT-ANTIRAC
        //                :IND-DTC-CNBT-ANTIRAC
        //             FROM DETT_TIT_CONT
        //             WHERE  TP_OGG = :DTC-TP-OGG
        //                    AND ID_OGG =  :DTC-ID-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DS_TS_INI_CPTZ
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        dettTitContDao.selectRec16(dettTitCont.getDtcTpOgg(), dettTitCont.getDtcIdOgg(), idsv0003.getCodiceCompagniaAnia(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        dettTitContDao.openCEff56(dettTitCont.getDtcTpOgg(), dettTitCont.getDtcIdOgg(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        dettTitContDao.closeCEff56();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :DTC-ID-DETT-TIT-CONT
        //               ,:DTC-ID-TIT-CONT
        //               ,:DTC-ID-OGG
        //               ,:DTC-TP-OGG
        //               ,:DTC-ID-MOVI-CRZ
        //               ,:DTC-ID-MOVI-CHIU
        //                :IND-DTC-ID-MOVI-CHIU
        //               ,:DTC-DT-INI-EFF-DB
        //               ,:DTC-DT-END-EFF-DB
        //               ,:DTC-COD-COMP-ANIA
        //               ,:DTC-DT-INI-COP-DB
        //                :IND-DTC-DT-INI-COP
        //               ,:DTC-DT-END-COP-DB
        //                :IND-DTC-DT-END-COP
        //               ,:DTC-PRE-NET
        //                :IND-DTC-PRE-NET
        //               ,:DTC-INTR-FRAZ
        //                :IND-DTC-INTR-FRAZ
        //               ,:DTC-INTR-MORA
        //                :IND-DTC-INTR-MORA
        //               ,:DTC-INTR-RETDT
        //                :IND-DTC-INTR-RETDT
        //               ,:DTC-INTR-RIAT
        //                :IND-DTC-INTR-RIAT
        //               ,:DTC-DIR
        //                :IND-DTC-DIR
        //               ,:DTC-SPE-MED
        //                :IND-DTC-SPE-MED
        //               ,:DTC-TAX
        //                :IND-DTC-TAX
        //               ,:DTC-SOPR-SAN
        //                :IND-DTC-SOPR-SAN
        //               ,:DTC-SOPR-SPO
        //                :IND-DTC-SOPR-SPO
        //               ,:DTC-SOPR-TEC
        //                :IND-DTC-SOPR-TEC
        //               ,:DTC-SOPR-PROF
        //                :IND-DTC-SOPR-PROF
        //               ,:DTC-SOPR-ALT
        //                :IND-DTC-SOPR-ALT
        //               ,:DTC-PRE-TOT
        //                :IND-DTC-PRE-TOT
        //               ,:DTC-PRE-PP-IAS
        //                :IND-DTC-PRE-PP-IAS
        //               ,:DTC-PRE-SOLO-RSH
        //                :IND-DTC-PRE-SOLO-RSH
        //               ,:DTC-CAR-ACQ
        //                :IND-DTC-CAR-ACQ
        //               ,:DTC-CAR-GEST
        //                :IND-DTC-CAR-GEST
        //               ,:DTC-CAR-INC
        //                :IND-DTC-CAR-INC
        //               ,:DTC-PROV-ACQ-1AA
        //                :IND-DTC-PROV-ACQ-1AA
        //               ,:DTC-PROV-ACQ-2AA
        //                :IND-DTC-PROV-ACQ-2AA
        //               ,:DTC-PROV-RICOR
        //                :IND-DTC-PROV-RICOR
        //               ,:DTC-PROV-INC
        //                :IND-DTC-PROV-INC
        //               ,:DTC-PROV-DA-REC
        //                :IND-DTC-PROV-DA-REC
        //               ,:DTC-COD-DVS
        //                :IND-DTC-COD-DVS
        //               ,:DTC-FRQ-MOVI
        //                :IND-DTC-FRQ-MOVI
        //               ,:DTC-TP-RGM-FISC
        //               ,:DTC-COD-TARI
        //                :IND-DTC-COD-TARI
        //               ,:DTC-TP-STAT-TIT
        //               ,:DTC-IMP-AZ
        //                :IND-DTC-IMP-AZ
        //               ,:DTC-IMP-ADER
        //                :IND-DTC-IMP-ADER
        //               ,:DTC-IMP-TFR
        //                :IND-DTC-IMP-TFR
        //               ,:DTC-IMP-VOLO
        //                :IND-DTC-IMP-VOLO
        //               ,:DTC-MANFEE-ANTIC
        //                :IND-DTC-MANFEE-ANTIC
        //               ,:DTC-MANFEE-RICOR
        //                :IND-DTC-MANFEE-RICOR
        //               ,:DTC-MANFEE-REC
        //                :IND-DTC-MANFEE-REC
        //               ,:DTC-DT-ESI-TIT-DB
        //                :IND-DTC-DT-ESI-TIT
        //               ,:DTC-SPE-AGE
        //                :IND-DTC-SPE-AGE
        //               ,:DTC-CAR-IAS
        //                :IND-DTC-CAR-IAS
        //               ,:DTC-TOT-INTR-PREST
        //                :IND-DTC-TOT-INTR-PREST
        //               ,:DTC-DS-RIGA
        //               ,:DTC-DS-OPER-SQL
        //               ,:DTC-DS-VER
        //               ,:DTC-DS-TS-INI-CPTZ
        //               ,:DTC-DS-TS-END-CPTZ
        //               ,:DTC-DS-UTENTE
        //               ,:DTC-DS-STATO-ELAB
        //               ,:DTC-IMP-TRASFE
        //                :IND-DTC-IMP-TRASFE
        //               ,:DTC-IMP-TFR-STRC
        //                :IND-DTC-IMP-TFR-STRC
        //               ,:DTC-NUM-GG-RITARDO-PAG
        //                :IND-DTC-NUM-GG-RITARDO-PAG
        //               ,:DTC-NUM-GG-RIVAL
        //                :IND-DTC-NUM-GG-RIVAL
        //               ,:DTC-ACQ-EXP
        //                :IND-DTC-ACQ-EXP
        //               ,:DTC-REMUN-ASS
        //                :IND-DTC-REMUN-ASS
        //               ,:DTC-COMMIS-INTER
        //                :IND-DTC-COMMIS-INTER
        //               ,:DTC-CNBT-ANTIRAC
        //                :IND-DTC-CNBT-ANTIRAC
        //           END-EXEC.
        dettTitContDao.fetchCEff56(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_DETT_TIT_CONT
        //                    ,ID_TIT_CONT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,DT_ESI_TIT
        //                    ,SPE_AGE
        //                    ,CAR_IAS
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,NUM_GG_RITARDO_PAG
        //                    ,NUM_GG_RIVAL
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //              FROM DETT_TIT_CONT
        //              WHERE  TP_OGG = :DTC-TP-OGG
        //                        AND ID_OGG =  :DTC-ID-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DS_TS_INI_CPTZ
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_DETT_TIT_CONT
        //                    ,ID_TIT_CONT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,DT_ESI_TIT
        //                    ,SPE_AGE
        //                    ,CAR_IAS
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,NUM_GG_RITARDO_PAG
        //                    ,NUM_GG_RIVAL
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //             INTO
        //                :DTC-ID-DETT-TIT-CONT
        //               ,:DTC-ID-TIT-CONT
        //               ,:DTC-ID-OGG
        //               ,:DTC-TP-OGG
        //               ,:DTC-ID-MOVI-CRZ
        //               ,:DTC-ID-MOVI-CHIU
        //                :IND-DTC-ID-MOVI-CHIU
        //               ,:DTC-DT-INI-EFF-DB
        //               ,:DTC-DT-END-EFF-DB
        //               ,:DTC-COD-COMP-ANIA
        //               ,:DTC-DT-INI-COP-DB
        //                :IND-DTC-DT-INI-COP
        //               ,:DTC-DT-END-COP-DB
        //                :IND-DTC-DT-END-COP
        //               ,:DTC-PRE-NET
        //                :IND-DTC-PRE-NET
        //               ,:DTC-INTR-FRAZ
        //                :IND-DTC-INTR-FRAZ
        //               ,:DTC-INTR-MORA
        //                :IND-DTC-INTR-MORA
        //               ,:DTC-INTR-RETDT
        //                :IND-DTC-INTR-RETDT
        //               ,:DTC-INTR-RIAT
        //                :IND-DTC-INTR-RIAT
        //               ,:DTC-DIR
        //                :IND-DTC-DIR
        //               ,:DTC-SPE-MED
        //                :IND-DTC-SPE-MED
        //               ,:DTC-TAX
        //                :IND-DTC-TAX
        //               ,:DTC-SOPR-SAN
        //                :IND-DTC-SOPR-SAN
        //               ,:DTC-SOPR-SPO
        //                :IND-DTC-SOPR-SPO
        //               ,:DTC-SOPR-TEC
        //                :IND-DTC-SOPR-TEC
        //               ,:DTC-SOPR-PROF
        //                :IND-DTC-SOPR-PROF
        //               ,:DTC-SOPR-ALT
        //                :IND-DTC-SOPR-ALT
        //               ,:DTC-PRE-TOT
        //                :IND-DTC-PRE-TOT
        //               ,:DTC-PRE-PP-IAS
        //                :IND-DTC-PRE-PP-IAS
        //               ,:DTC-PRE-SOLO-RSH
        //                :IND-DTC-PRE-SOLO-RSH
        //               ,:DTC-CAR-ACQ
        //                :IND-DTC-CAR-ACQ
        //               ,:DTC-CAR-GEST
        //                :IND-DTC-CAR-GEST
        //               ,:DTC-CAR-INC
        //                :IND-DTC-CAR-INC
        //               ,:DTC-PROV-ACQ-1AA
        //                :IND-DTC-PROV-ACQ-1AA
        //               ,:DTC-PROV-ACQ-2AA
        //                :IND-DTC-PROV-ACQ-2AA
        //               ,:DTC-PROV-RICOR
        //                :IND-DTC-PROV-RICOR
        //               ,:DTC-PROV-INC
        //                :IND-DTC-PROV-INC
        //               ,:DTC-PROV-DA-REC
        //                :IND-DTC-PROV-DA-REC
        //               ,:DTC-COD-DVS
        //                :IND-DTC-COD-DVS
        //               ,:DTC-FRQ-MOVI
        //                :IND-DTC-FRQ-MOVI
        //               ,:DTC-TP-RGM-FISC
        //               ,:DTC-COD-TARI
        //                :IND-DTC-COD-TARI
        //               ,:DTC-TP-STAT-TIT
        //               ,:DTC-IMP-AZ
        //                :IND-DTC-IMP-AZ
        //               ,:DTC-IMP-ADER
        //                :IND-DTC-IMP-ADER
        //               ,:DTC-IMP-TFR
        //                :IND-DTC-IMP-TFR
        //               ,:DTC-IMP-VOLO
        //                :IND-DTC-IMP-VOLO
        //               ,:DTC-MANFEE-ANTIC
        //                :IND-DTC-MANFEE-ANTIC
        //               ,:DTC-MANFEE-RICOR
        //                :IND-DTC-MANFEE-RICOR
        //               ,:DTC-MANFEE-REC
        //                :IND-DTC-MANFEE-REC
        //               ,:DTC-DT-ESI-TIT-DB
        //                :IND-DTC-DT-ESI-TIT
        //               ,:DTC-SPE-AGE
        //                :IND-DTC-SPE-AGE
        //               ,:DTC-CAR-IAS
        //                :IND-DTC-CAR-IAS
        //               ,:DTC-TOT-INTR-PREST
        //                :IND-DTC-TOT-INTR-PREST
        //               ,:DTC-DS-RIGA
        //               ,:DTC-DS-OPER-SQL
        //               ,:DTC-DS-VER
        //               ,:DTC-DS-TS-INI-CPTZ
        //               ,:DTC-DS-TS-END-CPTZ
        //               ,:DTC-DS-UTENTE
        //               ,:DTC-DS-STATO-ELAB
        //               ,:DTC-IMP-TRASFE
        //                :IND-DTC-IMP-TRASFE
        //               ,:DTC-IMP-TFR-STRC
        //                :IND-DTC-IMP-TFR-STRC
        //               ,:DTC-NUM-GG-RITARDO-PAG
        //                :IND-DTC-NUM-GG-RITARDO-PAG
        //               ,:DTC-NUM-GG-RIVAL
        //                :IND-DTC-NUM-GG-RIVAL
        //               ,:DTC-ACQ-EXP
        //                :IND-DTC-ACQ-EXP
        //               ,:DTC-REMUN-ASS
        //                :IND-DTC-REMUN-ASS
        //               ,:DTC-COMMIS-INTER
        //                :IND-DTC-COMMIS-INTER
        //               ,:DTC-CNBT-ANTIRAC
        //                :IND-DTC-CNBT-ANTIRAC
        //             FROM DETT_TIT_CONT
        //             WHERE  TP_OGG = :DTC-TP-OGG
        //                    AND ID_OGG =  :DTC-ID-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DS_TS_INI_CPTZ
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        dettTitContDao.selectRec16(dettTitCont.getDtcTpOgg(), dettTitCont.getDtcIdOgg(), idsv0003.getCodiceCompagniaAnia(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        dettTitContDao.openCEff56(dettTitCont.getDtcTpOgg(), dettTitCont.getDtcIdOgg(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        dettTitContDao.closeCEff56();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :DTC-ID-DETT-TIT-CONT
        //               ,:DTC-ID-TIT-CONT
        //               ,:DTC-ID-OGG
        //               ,:DTC-TP-OGG
        //               ,:DTC-ID-MOVI-CRZ
        //               ,:DTC-ID-MOVI-CHIU
        //                :IND-DTC-ID-MOVI-CHIU
        //               ,:DTC-DT-INI-EFF-DB
        //               ,:DTC-DT-END-EFF-DB
        //               ,:DTC-COD-COMP-ANIA
        //               ,:DTC-DT-INI-COP-DB
        //                :IND-DTC-DT-INI-COP
        //               ,:DTC-DT-END-COP-DB
        //                :IND-DTC-DT-END-COP
        //               ,:DTC-PRE-NET
        //                :IND-DTC-PRE-NET
        //               ,:DTC-INTR-FRAZ
        //                :IND-DTC-INTR-FRAZ
        //               ,:DTC-INTR-MORA
        //                :IND-DTC-INTR-MORA
        //               ,:DTC-INTR-RETDT
        //                :IND-DTC-INTR-RETDT
        //               ,:DTC-INTR-RIAT
        //                :IND-DTC-INTR-RIAT
        //               ,:DTC-DIR
        //                :IND-DTC-DIR
        //               ,:DTC-SPE-MED
        //                :IND-DTC-SPE-MED
        //               ,:DTC-TAX
        //                :IND-DTC-TAX
        //               ,:DTC-SOPR-SAN
        //                :IND-DTC-SOPR-SAN
        //               ,:DTC-SOPR-SPO
        //                :IND-DTC-SOPR-SPO
        //               ,:DTC-SOPR-TEC
        //                :IND-DTC-SOPR-TEC
        //               ,:DTC-SOPR-PROF
        //                :IND-DTC-SOPR-PROF
        //               ,:DTC-SOPR-ALT
        //                :IND-DTC-SOPR-ALT
        //               ,:DTC-PRE-TOT
        //                :IND-DTC-PRE-TOT
        //               ,:DTC-PRE-PP-IAS
        //                :IND-DTC-PRE-PP-IAS
        //               ,:DTC-PRE-SOLO-RSH
        //                :IND-DTC-PRE-SOLO-RSH
        //               ,:DTC-CAR-ACQ
        //                :IND-DTC-CAR-ACQ
        //               ,:DTC-CAR-GEST
        //                :IND-DTC-CAR-GEST
        //               ,:DTC-CAR-INC
        //                :IND-DTC-CAR-INC
        //               ,:DTC-PROV-ACQ-1AA
        //                :IND-DTC-PROV-ACQ-1AA
        //               ,:DTC-PROV-ACQ-2AA
        //                :IND-DTC-PROV-ACQ-2AA
        //               ,:DTC-PROV-RICOR
        //                :IND-DTC-PROV-RICOR
        //               ,:DTC-PROV-INC
        //                :IND-DTC-PROV-INC
        //               ,:DTC-PROV-DA-REC
        //                :IND-DTC-PROV-DA-REC
        //               ,:DTC-COD-DVS
        //                :IND-DTC-COD-DVS
        //               ,:DTC-FRQ-MOVI
        //                :IND-DTC-FRQ-MOVI
        //               ,:DTC-TP-RGM-FISC
        //               ,:DTC-COD-TARI
        //                :IND-DTC-COD-TARI
        //               ,:DTC-TP-STAT-TIT
        //               ,:DTC-IMP-AZ
        //                :IND-DTC-IMP-AZ
        //               ,:DTC-IMP-ADER
        //                :IND-DTC-IMP-ADER
        //               ,:DTC-IMP-TFR
        //                :IND-DTC-IMP-TFR
        //               ,:DTC-IMP-VOLO
        //                :IND-DTC-IMP-VOLO
        //               ,:DTC-MANFEE-ANTIC
        //                :IND-DTC-MANFEE-ANTIC
        //               ,:DTC-MANFEE-RICOR
        //                :IND-DTC-MANFEE-RICOR
        //               ,:DTC-MANFEE-REC
        //                :IND-DTC-MANFEE-REC
        //               ,:DTC-DT-ESI-TIT-DB
        //                :IND-DTC-DT-ESI-TIT
        //               ,:DTC-SPE-AGE
        //                :IND-DTC-SPE-AGE
        //               ,:DTC-CAR-IAS
        //                :IND-DTC-CAR-IAS
        //               ,:DTC-TOT-INTR-PREST
        //                :IND-DTC-TOT-INTR-PREST
        //               ,:DTC-DS-RIGA
        //               ,:DTC-DS-OPER-SQL
        //               ,:DTC-DS-VER
        //               ,:DTC-DS-TS-INI-CPTZ
        //               ,:DTC-DS-TS-END-CPTZ
        //               ,:DTC-DS-UTENTE
        //               ,:DTC-DS-STATO-ELAB
        //               ,:DTC-IMP-TRASFE
        //                :IND-DTC-IMP-TRASFE
        //               ,:DTC-IMP-TFR-STRC
        //                :IND-DTC-IMP-TFR-STRC
        //               ,:DTC-NUM-GG-RITARDO-PAG
        //                :IND-DTC-NUM-GG-RITARDO-PAG
        //               ,:DTC-NUM-GG-RIVAL
        //                :IND-DTC-NUM-GG-RIVAL
        //               ,:DTC-ACQ-EXP
        //                :IND-DTC-ACQ-EXP
        //               ,:DTC-REMUN-ASS
        //                :IND-DTC-REMUN-ASS
        //               ,:DTC-COMMIS-INTER
        //                :IND-DTC-COMMIS-INTER
        //               ,:DTC-CNBT-ANTIRAC
        //                :IND-DTC-CNBT-ANTIRAC
        //           END-EXEC.
        dettTitContDao.fetchCEff56(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-DTC-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO DTC-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-ID-MOVI-CHIU-NULL
            dettTitCont.getDtcIdMoviChiu().setDtcIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIdMoviChiu.Len.DTC_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-DTC-DT-INI-COP = -1
        //              MOVE HIGH-VALUES TO DTC-DT-INI-COP-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DT-INI-COP-NULL
            dettTitCont.getDtcDtIniCop().setDtcDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDtIniCop.Len.DTC_DT_INI_COP_NULL));
        }
        // COB_CODE: IF IND-DTC-DT-END-COP = -1
        //              MOVE HIGH-VALUES TO DTC-DT-END-COP-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DT-END-COP-NULL
            dettTitCont.getDtcDtEndCop().setDtcDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDtEndCop.Len.DTC_DT_END_COP_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-NET = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-NET-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-NET-NULL
            dettTitCont.getDtcPreNet().setDtcPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPreNet.Len.DTC_PRE_NET_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-FRAZ = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-FRAZ-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-FRAZ-NULL
            dettTitCont.getDtcIntrFraz().setDtcIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrFraz.Len.DTC_INTR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-MORA-NULL
            dettTitCont.getDtcIntrMora().setDtcIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrMora.Len.DTC_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-RETDT = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-RETDT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-RETDT-NULL
            dettTitCont.getDtcIntrRetdt().setDtcIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrRetdt.Len.DTC_INTR_RETDT_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-RIAT-NULL
            dettTitCont.getDtcIntrRiat().setDtcIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrRiat.Len.DTC_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-DTC-DIR = -1
        //              MOVE HIGH-VALUES TO DTC-DIR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DIR-NULL
            dettTitCont.getDtcDir().setDtcDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDir.Len.DTC_DIR_NULL));
        }
        // COB_CODE: IF IND-DTC-SPE-MED = -1
        //              MOVE HIGH-VALUES TO DTC-SPE-MED-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SPE-MED-NULL
            dettTitCont.getDtcSpeMed().setDtcSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSpeMed.Len.DTC_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-DTC-TAX = -1
        //              MOVE HIGH-VALUES TO DTC-TAX-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-TAX-NULL
            dettTitCont.getDtcTax().setDtcTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcTax.Len.DTC_TAX_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-SAN-NULL
            dettTitCont.getDtcSoprSan().setDtcSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprSan.Len.DTC_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-SPO-NULL
            dettTitCont.getDtcSoprSpo().setDtcSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprSpo.Len.DTC_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-TEC-NULL
            dettTitCont.getDtcSoprTec().setDtcSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprTec.Len.DTC_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-PROF-NULL
            dettTitCont.getDtcSoprProf().setDtcSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprProf.Len.DTC_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-ALT = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-ALT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-ALT-NULL
            dettTitCont.getDtcSoprAlt().setDtcSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprAlt.Len.DTC_SOPR_ALT_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-TOT = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-TOT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-TOT-NULL
            dettTitCont.getDtcPreTot().setDtcPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPreTot.Len.DTC_PRE_TOT_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-PP-IAS = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-PP-IAS-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-PP-IAS-NULL
            dettTitCont.getDtcPrePpIas().setDtcPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPrePpIas.Len.DTC_PRE_PP_IAS_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-SOLO-RSH = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-SOLO-RSH-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-SOLO-RSH-NULL
            dettTitCont.getDtcPreSoloRsh().setDtcPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPreSoloRsh.Len.DTC_PRE_SOLO_RSH_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-ACQ-NULL
            dettTitCont.getDtcCarAcq().setDtcCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarAcq.Len.DTC_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-GEST-NULL
            dettTitCont.getDtcCarGest().setDtcCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarGest.Len.DTC_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-INC = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-INC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-INC-NULL
            dettTitCont.getDtcCarInc().setDtcCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarInc.Len.DTC_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-ACQ-1AA = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-ACQ-1AA-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-ACQ-1AA-NULL
            dettTitCont.getDtcProvAcq1aa().setDtcProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvAcq1aa.Len.DTC_PROV_ACQ1AA_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-ACQ-2AA = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-ACQ-2AA-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-ACQ-2AA-NULL
            dettTitCont.getDtcProvAcq2aa().setDtcProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvAcq2aa.Len.DTC_PROV_ACQ2AA_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-RICOR-NULL
            dettTitCont.getDtcProvRicor().setDtcProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvRicor.Len.DTC_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-INC = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-INC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-INC-NULL
            dettTitCont.getDtcProvInc().setDtcProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvInc.Len.DTC_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-DA-REC = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-DA-REC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-DA-REC-NULL
            dettTitCont.getDtcProvDaRec().setDtcProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvDaRec.Len.DTC_PROV_DA_REC_NULL));
        }
        // COB_CODE: IF IND-DTC-COD-DVS = -1
        //              MOVE HIGH-VALUES TO DTC-COD-DVS-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-COD-DVS-NULL
            dettTitCont.setDtcCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitContIdbsdtc0.Len.DTC_COD_DVS));
        }
        // COB_CODE: IF IND-DTC-FRQ-MOVI = -1
        //              MOVE HIGH-VALUES TO DTC-FRQ-MOVI-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getFrqMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-FRQ-MOVI-NULL
            dettTitCont.getDtcFrqMovi().setDtcFrqMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcFrqMovi.Len.DTC_FRQ_MOVI_NULL));
        }
        // COB_CODE: IF IND-DTC-COD-TARI = -1
        //              MOVE HIGH-VALUES TO DTC-COD-TARI-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-COD-TARI-NULL
            dettTitCont.setDtcCodTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitContIdbsdtc0.Len.DTC_COD_TARI));
        }
        // COB_CODE: IF IND-DTC-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-AZ-NULL
            dettTitCont.getDtcImpAz().setDtcImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpAz.Len.DTC_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-ADER-NULL
            dettTitCont.getDtcImpAder().setDtcImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpAder.Len.DTC_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-TFR-NULL
            dettTitCont.getDtcImpTfr().setDtcImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpTfr.Len.DTC_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-VOLO-NULL
            dettTitCont.getDtcImpVolo().setDtcImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpVolo.Len.DTC_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-DTC-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO DTC-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-MANFEE-ANTIC-NULL
            dettTitCont.getDtcManfeeAntic().setDtcManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcManfeeAntic.Len.DTC_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DTC-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO DTC-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-MANFEE-RICOR-NULL
            dettTitCont.getDtcManfeeRicor().setDtcManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcManfeeRicor.Len.DTC_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-DTC-MANFEE-REC = -1
        //              MOVE HIGH-VALUES TO DTC-MANFEE-REC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-MANFEE-REC-NULL
            dettTitCont.getDtcManfeeRec().setDtcManfeeRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcManfeeRec.Len.DTC_MANFEE_REC_NULL));
        }
        // COB_CODE: IF IND-DTC-DT-ESI-TIT = -1
        //              MOVE HIGH-VALUES TO DTC-DT-ESI-TIT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DT-ESI-TIT-NULL
            dettTitCont.getDtcDtEsiTit().setDtcDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDtEsiTit.Len.DTC_DT_ESI_TIT_NULL));
        }
        // COB_CODE: IF IND-DTC-SPE-AGE = -1
        //              MOVE HIGH-VALUES TO DTC-SPE-AGE-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SPE-AGE-NULL
            dettTitCont.getDtcSpeAge().setDtcSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSpeAge.Len.DTC_SPE_AGE_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-IAS = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-IAS-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-IAS-NULL
            dettTitCont.getDtcCarIas().setDtcCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarIas.Len.DTC_CAR_IAS_NULL));
        }
        // COB_CODE: IF IND-DTC-TOT-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO DTC-TOT-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-TOT-INTR-PREST-NULL
            dettTitCont.getDtcTotIntrPrest().setDtcTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcTotIntrPrest.Len.DTC_TOT_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-TRASFE-NULL
            dettTitCont.getDtcImpTrasfe().setDtcImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpTrasfe.Len.DTC_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-TFR-STRC-NULL
            dettTitCont.getDtcImpTfrStrc().setDtcImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpTfrStrc.Len.DTC_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-DTC-NUM-GG-RITARDO-PAG = -1
        //              MOVE HIGH-VALUES TO DTC-NUM-GG-RITARDO-PAG-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getNumGgRitardoPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-NUM-GG-RITARDO-PAG-NULL
            dettTitCont.getDtcNumGgRitardoPag().setDtcNumGgRitardoPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcNumGgRitardoPag.Len.DTC_NUM_GG_RITARDO_PAG_NULL));
        }
        // COB_CODE: IF IND-DTC-NUM-GG-RIVAL = -1
        //              MOVE HIGH-VALUES TO DTC-NUM-GG-RIVAL-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getNumGgRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-NUM-GG-RIVAL-NULL
            dettTitCont.getDtcNumGgRival().setDtcNumGgRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcNumGgRival.Len.DTC_NUM_GG_RIVAL_NULL));
        }
        // COB_CODE: IF IND-DTC-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO DTC-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-ACQ-EXP-NULL
            dettTitCont.getDtcAcqExp().setDtcAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcAcqExp.Len.DTC_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-DTC-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO DTC-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-REMUN-ASS-NULL
            dettTitCont.getDtcRemunAss().setDtcRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcRemunAss.Len.DTC_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-DTC-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO DTC-COMMIS-INTER-NULL
        //           END-IF.
        if (ws.getIndDettTitCont().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-COMMIS-INTER-NULL
            dettTitCont.getDtcCommisInter().setDtcCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCommisInter.Len.DTC_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-DTC-CNBT-ANTIRAC = -1
        //              MOVE HIGH-VALUES TO DTC-CNBT-ANTIRAC-NULL
        //           END-IF.
        if (ws.getIndDettTitCont().getCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CNBT-ANTIRAC-NULL
            dettTitCont.getDtcCnbtAntirac().setDtcCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCnbtAntirac.Len.DTC_CNBT_ANTIRAC_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE DTC-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettTitContDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DTC-DT-INI-EFF
        dettTitCont.setDtcDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE DTC-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettTitContDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DTC-DT-END-EFF
        dettTitCont.setDtcDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-DTC-DT-INI-COP = 0
        //               MOVE WS-DATE-N      TO DTC-DT-INI-COP
        //           END-IF
        if (ws.getIndDettTitCont().getDtIniCop() == 0) {
            // COB_CODE: MOVE DTC-DT-INI-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettTitContDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DTC-DT-INI-COP
            dettTitCont.getDtcDtIniCop().setDtcDtIniCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-DTC-DT-END-COP = 0
        //               MOVE WS-DATE-N      TO DTC-DT-END-COP
        //           END-IF
        if (ws.getIndDettTitCont().getDtEndCop() == 0) {
            // COB_CODE: MOVE DTC-DT-END-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettTitContDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DTC-DT-END-COP
            dettTitCont.getDtcDtEndCop().setDtcDtEndCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-DTC-DT-ESI-TIT = 0
        //               MOVE WS-DATE-N      TO DTC-DT-ESI-TIT
        //           END-IF.
        if (ws.getIndDettTitCont().getDtEsiTit() == 0) {
            // COB_CODE: MOVE DTC-DT-ESI-TIT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettTitContDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DTC-DT-ESI-TIT
            dettTitCont.getDtcDtEsiTit().setDtcDtEsiTit(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getAcqExp() {
        return dettTitCont.getDtcAcqExp().getDtcAcqExp();
    }

    @Override
    public void setAcqExp(AfDecimal acqExp) {
        this.dettTitCont.getDtcAcqExp().setDtcAcqExp(acqExp.copy());
    }

    @Override
    public AfDecimal getAcqExpObj() {
        if (ws.getIndDettTitCont().getAcqExp() >= 0) {
            return getAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAcqExpObj(AfDecimal acqExpObj) {
        if (acqExpObj != null) {
            setAcqExp(new AfDecimal(acqExpObj, 15, 3));
            ws.getIndDettTitCont().setAcqExp(((short)0));
        }
        else {
            ws.getIndDettTitCont().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarAcq() {
        return dettTitCont.getDtcCarAcq().getDtcCarAcq();
    }

    @Override
    public void setCarAcq(AfDecimal carAcq) {
        this.dettTitCont.getDtcCarAcq().setDtcCarAcq(carAcq.copy());
    }

    @Override
    public AfDecimal getCarAcqObj() {
        if (ws.getIndDettTitCont().getCarAcq() >= 0) {
            return getCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarAcqObj(AfDecimal carAcqObj) {
        if (carAcqObj != null) {
            setCarAcq(new AfDecimal(carAcqObj, 15, 3));
            ws.getIndDettTitCont().setCarAcq(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarGest() {
        return dettTitCont.getDtcCarGest().getDtcCarGest();
    }

    @Override
    public void setCarGest(AfDecimal carGest) {
        this.dettTitCont.getDtcCarGest().setDtcCarGest(carGest.copy());
    }

    @Override
    public AfDecimal getCarGestObj() {
        if (ws.getIndDettTitCont().getCarGest() >= 0) {
            return getCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarGestObj(AfDecimal carGestObj) {
        if (carGestObj != null) {
            setCarGest(new AfDecimal(carGestObj, 15, 3));
            ws.getIndDettTitCont().setCarGest(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarIas() {
        return dettTitCont.getDtcCarIas().getDtcCarIas();
    }

    @Override
    public void setCarIas(AfDecimal carIas) {
        this.dettTitCont.getDtcCarIas().setDtcCarIas(carIas.copy());
    }

    @Override
    public AfDecimal getCarIasObj() {
        if (ws.getIndDettTitCont().getCarIas() >= 0) {
            return getCarIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarIasObj(AfDecimal carIasObj) {
        if (carIasObj != null) {
            setCarIas(new AfDecimal(carIasObj, 15, 3));
            ws.getIndDettTitCont().setCarIas(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCarIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarInc() {
        return dettTitCont.getDtcCarInc().getDtcCarInc();
    }

    @Override
    public void setCarInc(AfDecimal carInc) {
        this.dettTitCont.getDtcCarInc().setDtcCarInc(carInc.copy());
    }

    @Override
    public AfDecimal getCarIncObj() {
        if (ws.getIndDettTitCont().getCarInc() >= 0) {
            return getCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarIncObj(AfDecimal carIncObj) {
        if (carIncObj != null) {
            setCarInc(new AfDecimal(carIncObj, 15, 3));
            ws.getIndDettTitCont().setCarInc(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getCnbtAntirac() {
        return dettTitCont.getDtcCnbtAntirac().getDtcCnbtAntirac();
    }

    @Override
    public void setCnbtAntirac(AfDecimal cnbtAntirac) {
        this.dettTitCont.getDtcCnbtAntirac().setDtcCnbtAntirac(cnbtAntirac.copy());
    }

    @Override
    public AfDecimal getCnbtAntiracObj() {
        if (ws.getIndDettTitCont().getCnbtAntirac() >= 0) {
            return getCnbtAntirac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCnbtAntiracObj(AfDecimal cnbtAntiracObj) {
        if (cnbtAntiracObj != null) {
            setCnbtAntirac(new AfDecimal(cnbtAntiracObj, 15, 3));
            ws.getIndDettTitCont().setCnbtAntirac(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCnbtAntirac(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return dettTitCont.getDtcCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.dettTitCont.setDtcCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return dettTitCont.getDtcCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.dettTitCont.setDtcCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndDettTitCont().getCodDvs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndDettTitCont().setCodDvs(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCodDvs(((short)-1));
        }
    }

    @Override
    public String getCodTari() {
        return dettTitCont.getDtcCodTari();
    }

    @Override
    public void setCodTari(String codTari) {
        this.dettTitCont.setDtcCodTari(codTari);
    }

    @Override
    public String getCodTariObj() {
        if (ws.getIndDettTitCont().getCodTari() >= 0) {
            return getCodTari();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTariObj(String codTariObj) {
        if (codTariObj != null) {
            setCodTari(codTariObj);
            ws.getIndDettTitCont().setCodTari(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCodTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getCommisInter() {
        return dettTitCont.getDtcCommisInter().getDtcCommisInter();
    }

    @Override
    public void setCommisInter(AfDecimal commisInter) {
        this.dettTitCont.getDtcCommisInter().setDtcCommisInter(commisInter.copy());
    }

    @Override
    public AfDecimal getCommisInterObj() {
        if (ws.getIndDettTitCont().getCommisInter() >= 0) {
            return getCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCommisInterObj(AfDecimal commisInterObj) {
        if (commisInterObj != null) {
            setCommisInter(new AfDecimal(commisInterObj, 15, 3));
            ws.getIndDettTitCont().setCommisInter(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getDir() {
        return dettTitCont.getDtcDir().getDtcDir();
    }

    @Override
    public void setDir(AfDecimal dir) {
        this.dettTitCont.getDtcDir().setDtcDir(dir.copy());
    }

    @Override
    public AfDecimal getDirObj() {
        if (ws.getIndDettTitCont().getDir() >= 0) {
            return getDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDirObj(AfDecimal dirObj) {
        if (dirObj != null) {
            setDir(new AfDecimal(dirObj, 15, 3));
            ws.getIndDettTitCont().setDir(((short)0));
        }
        else {
            ws.getIndDettTitCont().setDir(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return dettTitCont.getDtcDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.dettTitCont.setDtcDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return dettTitCont.getDtcDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.dettTitCont.setDtcDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return dettTitCont.getDtcDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dettTitCont.setDtcDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return dettTitCont.getDtcDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dettTitCont.setDtcDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return dettTitCont.getDtcDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.dettTitCont.setDtcDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return dettTitCont.getDtcDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.dettTitCont.setDtcDsVer(dsVer);
    }

    @Override
    public String getDtEndCopDb() {
        return ws.getDettTitContDb().getEndCopDb();
    }

    @Override
    public void setDtEndCopDb(String dtEndCopDb) {
        this.ws.getDettTitContDb().setEndCopDb(dtEndCopDb);
    }

    @Override
    public String getDtEndCopDbObj() {
        if (ws.getIndDettTitCont().getDtEndCop() >= 0) {
            return getDtEndCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndCopDbObj(String dtEndCopDbObj) {
        if (dtEndCopDbObj != null) {
            setDtEndCopDb(dtEndCopDbObj);
            ws.getIndDettTitCont().setDtEndCop(((short)0));
        }
        else {
            ws.getIndDettTitCont().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getDettTitContDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getDettTitContDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtEsiTitDb() {
        return ws.getDettTitContDb().getEsiTitDb();
    }

    @Override
    public void setDtEsiTitDb(String dtEsiTitDb) {
        this.ws.getDettTitContDb().setEsiTitDb(dtEsiTitDb);
    }

    @Override
    public String getDtEsiTitDbObj() {
        if (ws.getIndDettTitCont().getDtEsiTit() >= 0) {
            return getDtEsiTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEsiTitDbObj(String dtEsiTitDbObj) {
        if (dtEsiTitDbObj != null) {
            setDtEsiTitDb(dtEsiTitDbObj);
            ws.getIndDettTitCont().setDtEsiTit(((short)0));
        }
        else {
            ws.getIndDettTitCont().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getDettTitContDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getDettTitContDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public long getDtcDsRiga() {
        return dettTitCont.getDtcDsRiga();
    }

    @Override
    public void setDtcDsRiga(long dtcDsRiga) {
        this.dettTitCont.setDtcDsRiga(dtcDsRiga);
    }

    @Override
    public String getDtcDtIniCopDb() {
        return ws.getDettTitContDb().getIniCopDb();
    }

    @Override
    public void setDtcDtIniCopDb(String dtcDtIniCopDb) {
        this.ws.getDettTitContDb().setIniCopDb(dtcDtIniCopDb);
    }

    @Override
    public String getDtcDtIniCopDbObj() {
        if (ws.getIndDettTitCont().getDtIniCop() >= 0) {
            return getDtcDtIniCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcDtIniCopDbObj(String dtcDtIniCopDbObj) {
        if (dtcDtIniCopDbObj != null) {
            setDtcDtIniCopDb(dtcDtIniCopDbObj);
            ws.getIndDettTitCont().setDtIniCop(((short)0));
        }
        else {
            ws.getIndDettTitCont().setDtIniCop(((short)-1));
        }
    }

    @Override
    public int getDtcIdOgg() {
        return dettTitCont.getDtcIdOgg();
    }

    @Override
    public void setDtcIdOgg(int dtcIdOgg) {
        this.dettTitCont.setDtcIdOgg(dtcIdOgg);
    }

    @Override
    public String getDtcTpOgg() {
        return dettTitCont.getDtcTpOgg();
    }

    @Override
    public void setDtcTpOgg(String dtcTpOgg) {
        this.dettTitCont.setDtcTpOgg(dtcTpOgg);
    }

    @Override
    public String getDtcTpStatTit() {
        return dettTitCont.getDtcTpStatTit();
    }

    @Override
    public void setDtcTpStatTit(String dtcTpStatTit) {
        this.dettTitCont.setDtcTpStatTit(dtcTpStatTit);
    }

    @Override
    public int getFrqMovi() {
        return dettTitCont.getDtcFrqMovi().getDtcFrqMovi();
    }

    @Override
    public void setFrqMovi(int frqMovi) {
        this.dettTitCont.getDtcFrqMovi().setDtcFrqMovi(frqMovi);
    }

    @Override
    public Integer getFrqMoviObj() {
        if (ws.getIndDettTitCont().getFrqMovi() >= 0) {
            return ((Integer)getFrqMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrqMoviObj(Integer frqMoviObj) {
        if (frqMoviObj != null) {
            setFrqMovi(((int)frqMoviObj));
            ws.getIndDettTitCont().setFrqMovi(((short)0));
        }
        else {
            ws.getIndDettTitCont().setFrqMovi(((short)-1));
        }
    }

    @Override
    public int getIdDettTitCont() {
        return dettTitCont.getDtcIdDettTitCont();
    }

    @Override
    public void setIdDettTitCont(int idDettTitCont) {
        this.dettTitCont.setDtcIdDettTitCont(idDettTitCont);
    }

    @Override
    public int getIdMoviChiu() {
        return dettTitCont.getDtcIdMoviChiu().getDtcIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.dettTitCont.getDtcIdMoviChiu().setDtcIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndDettTitCont().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndDettTitCont().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndDettTitCont().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return dettTitCont.getDtcIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.dettTitCont.setDtcIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdTitCont() {
        return dettTitCont.getDtcIdTitCont();
    }

    @Override
    public void setIdTitCont(int idTitCont) {
        this.dettTitCont.setDtcIdTitCont(idTitCont);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public AfDecimal getImpAder() {
        return dettTitCont.getDtcImpAder().getDtcImpAder();
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        this.dettTitCont.getDtcImpAder().setDtcImpAder(impAder.copy());
    }

    @Override
    public AfDecimal getImpAderObj() {
        if (ws.getIndDettTitCont().getImpAder() >= 0) {
            return getImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        if (impAderObj != null) {
            setImpAder(new AfDecimal(impAderObj, 15, 3));
            ws.getIndDettTitCont().setImpAder(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAz() {
        return dettTitCont.getDtcImpAz().getDtcImpAz();
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        this.dettTitCont.getDtcImpAz().setDtcImpAz(impAz.copy());
    }

    @Override
    public AfDecimal getImpAzObj() {
        if (ws.getIndDettTitCont().getImpAz() >= 0) {
            return getImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        if (impAzObj != null) {
            setImpAz(new AfDecimal(impAzObj, 15, 3));
            ws.getIndDettTitCont().setImpAz(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfr() {
        return dettTitCont.getDtcImpTfr().getDtcImpTfr();
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        this.dettTitCont.getDtcImpTfr().setDtcImpTfr(impTfr.copy());
    }

    @Override
    public AfDecimal getImpTfrObj() {
        if (ws.getIndDettTitCont().getImpTfr() >= 0) {
            return getImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        if (impTfrObj != null) {
            setImpTfr(new AfDecimal(impTfrObj, 15, 3));
            ws.getIndDettTitCont().setImpTfr(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfrStrc() {
        return dettTitCont.getDtcImpTfrStrc().getDtcImpTfrStrc();
    }

    @Override
    public void setImpTfrStrc(AfDecimal impTfrStrc) {
        this.dettTitCont.getDtcImpTfrStrc().setDtcImpTfrStrc(impTfrStrc.copy());
    }

    @Override
    public AfDecimal getImpTfrStrcObj() {
        if (ws.getIndDettTitCont().getImpTfrStrc() >= 0) {
            return getImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrStrcObj(AfDecimal impTfrStrcObj) {
        if (impTfrStrcObj != null) {
            setImpTfrStrc(new AfDecimal(impTfrStrcObj, 15, 3));
            ws.getIndDettTitCont().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTrasfe() {
        return dettTitCont.getDtcImpTrasfe().getDtcImpTrasfe();
    }

    @Override
    public void setImpTrasfe(AfDecimal impTrasfe) {
        this.dettTitCont.getDtcImpTrasfe().setDtcImpTrasfe(impTrasfe.copy());
    }

    @Override
    public AfDecimal getImpTrasfeObj() {
        if (ws.getIndDettTitCont().getImpTrasfe() >= 0) {
            return getImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTrasfeObj(AfDecimal impTrasfeObj) {
        if (impTrasfeObj != null) {
            setImpTrasfe(new AfDecimal(impTrasfeObj, 15, 3));
            ws.getIndDettTitCont().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpVolo() {
        return dettTitCont.getDtcImpVolo().getDtcImpVolo();
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        this.dettTitCont.getDtcImpVolo().setDtcImpVolo(impVolo.copy());
    }

    @Override
    public AfDecimal getImpVoloObj() {
        if (ws.getIndDettTitCont().getImpVolo() >= 0) {
            return getImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        if (impVoloObj != null) {
            setImpVolo(new AfDecimal(impVoloObj, 15, 3));
            ws.getIndDettTitCont().setImpVolo(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrFraz() {
        return dettTitCont.getDtcIntrFraz().getDtcIntrFraz();
    }

    @Override
    public void setIntrFraz(AfDecimal intrFraz) {
        this.dettTitCont.getDtcIntrFraz().setDtcIntrFraz(intrFraz.copy());
    }

    @Override
    public AfDecimal getIntrFrazObj() {
        if (ws.getIndDettTitCont().getIntrFraz() >= 0) {
            return getIntrFraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrFrazObj(AfDecimal intrFrazObj) {
        if (intrFrazObj != null) {
            setIntrFraz(new AfDecimal(intrFrazObj, 15, 3));
            ws.getIndDettTitCont().setIntrFraz(((short)0));
        }
        else {
            ws.getIndDettTitCont().setIntrFraz(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrMora() {
        return dettTitCont.getDtcIntrMora().getDtcIntrMora();
    }

    @Override
    public void setIntrMora(AfDecimal intrMora) {
        this.dettTitCont.getDtcIntrMora().setDtcIntrMora(intrMora.copy());
    }

    @Override
    public AfDecimal getIntrMoraObj() {
        if (ws.getIndDettTitCont().getIntrMora() >= 0) {
            return getIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrMoraObj(AfDecimal intrMoraObj) {
        if (intrMoraObj != null) {
            setIntrMora(new AfDecimal(intrMoraObj, 15, 3));
            ws.getIndDettTitCont().setIntrMora(((short)0));
        }
        else {
            ws.getIndDettTitCont().setIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrRetdt() {
        return dettTitCont.getDtcIntrRetdt().getDtcIntrRetdt();
    }

    @Override
    public void setIntrRetdt(AfDecimal intrRetdt) {
        this.dettTitCont.getDtcIntrRetdt().setDtcIntrRetdt(intrRetdt.copy());
    }

    @Override
    public AfDecimal getIntrRetdtObj() {
        if (ws.getIndDettTitCont().getIntrRetdt() >= 0) {
            return getIntrRetdt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrRetdtObj(AfDecimal intrRetdtObj) {
        if (intrRetdtObj != null) {
            setIntrRetdt(new AfDecimal(intrRetdtObj, 15, 3));
            ws.getIndDettTitCont().setIntrRetdt(((short)0));
        }
        else {
            ws.getIndDettTitCont().setIntrRetdt(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrRiat() {
        return dettTitCont.getDtcIntrRiat().getDtcIntrRiat();
    }

    @Override
    public void setIntrRiat(AfDecimal intrRiat) {
        this.dettTitCont.getDtcIntrRiat().setDtcIntrRiat(intrRiat.copy());
    }

    @Override
    public AfDecimal getIntrRiatObj() {
        if (ws.getIndDettTitCont().getIntrRiat() >= 0) {
            return getIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrRiatObj(AfDecimal intrRiatObj) {
        if (intrRiatObj != null) {
            setIntrRiat(new AfDecimal(intrRiatObj, 15, 3));
            ws.getIndDettTitCont().setIntrRiat(((short)0));
        }
        else {
            ws.getIndDettTitCont().setIntrRiat(((short)-1));
        }
    }

    @Override
    public int getLdbv2131IdOgg() {
        throw new FieldNotMappedException("ldbv2131IdOgg");
    }

    @Override
    public void setLdbv2131IdOgg(int ldbv2131IdOgg) {
        throw new FieldNotMappedException("ldbv2131IdOgg");
    }

    @Override
    public AfDecimal getLdbv2131TotPremi() {
        throw new FieldNotMappedException("ldbv2131TotPremi");
    }

    @Override
    public void setLdbv2131TotPremi(AfDecimal ldbv2131TotPremi) {
        throw new FieldNotMappedException("ldbv2131TotPremi");
    }

    @Override
    public AfDecimal getLdbv2131TotPremiObj() {
        return getLdbv2131TotPremi();
    }

    @Override
    public void setLdbv2131TotPremiObj(AfDecimal ldbv2131TotPremiObj) {
        setLdbv2131TotPremi(new AfDecimal(ldbv2131TotPremiObj, 18, 3));
    }

    @Override
    public String getLdbv2131TpOgg() {
        throw new FieldNotMappedException("ldbv2131TpOgg");
    }

    @Override
    public void setLdbv2131TpOgg(String ldbv2131TpOgg) {
        throw new FieldNotMappedException("ldbv2131TpOgg");
    }

    @Override
    public String getLdbv2131TpStatTit01() {
        throw new FieldNotMappedException("ldbv2131TpStatTit01");
    }

    @Override
    public void setLdbv2131TpStatTit01(String ldbv2131TpStatTit01) {
        throw new FieldNotMappedException("ldbv2131TpStatTit01");
    }

    @Override
    public String getLdbv2131TpStatTit02() {
        throw new FieldNotMappedException("ldbv2131TpStatTit02");
    }

    @Override
    public void setLdbv2131TpStatTit02(String ldbv2131TpStatTit02) {
        throw new FieldNotMappedException("ldbv2131TpStatTit02");
    }

    @Override
    public String getLdbv2131TpStatTit03() {
        throw new FieldNotMappedException("ldbv2131TpStatTit03");
    }

    @Override
    public void setLdbv2131TpStatTit03(String ldbv2131TpStatTit03) {
        throw new FieldNotMappedException("ldbv2131TpStatTit03");
    }

    @Override
    public String getLdbv2131TpStatTit04() {
        throw new FieldNotMappedException("ldbv2131TpStatTit04");
    }

    @Override
    public void setLdbv2131TpStatTit04(String ldbv2131TpStatTit04) {
        throw new FieldNotMappedException("ldbv2131TpStatTit04");
    }

    @Override
    public String getLdbv2131TpStatTit05() {
        throw new FieldNotMappedException("ldbv2131TpStatTit05");
    }

    @Override
    public void setLdbv2131TpStatTit05(String ldbv2131TpStatTit05) {
        throw new FieldNotMappedException("ldbv2131TpStatTit05");
    }

    @Override
    public String getLdbv4151DataInizPeriodoDb() {
        throw new FieldNotMappedException("ldbv4151DataInizPeriodoDb");
    }

    @Override
    public void setLdbv4151DataInizPeriodoDb(String ldbv4151DataInizPeriodoDb) {
        throw new FieldNotMappedException("ldbv4151DataInizPeriodoDb");
    }

    @Override
    public int getLdbv4151IdOgg() {
        throw new FieldNotMappedException("ldbv4151IdOgg");
    }

    @Override
    public void setLdbv4151IdOgg(int ldbv4151IdOgg) {
        throw new FieldNotMappedException("ldbv4151IdOgg");
    }

    @Override
    public AfDecimal getLdbv4151PreTot() {
        throw new FieldNotMappedException("ldbv4151PreTot");
    }

    @Override
    public void setLdbv4151PreTot(AfDecimal ldbv4151PreTot) {
        throw new FieldNotMappedException("ldbv4151PreTot");
    }

    @Override
    public String getLdbv4151TpOgg() {
        throw new FieldNotMappedException("ldbv4151TpOgg");
    }

    @Override
    public void setLdbv4151TpOgg(String ldbv4151TpOgg) {
        throw new FieldNotMappedException("ldbv4151TpOgg");
    }

    @Override
    public String getLdbv4151TpStatTit() {
        throw new FieldNotMappedException("ldbv4151TpStatTit");
    }

    @Override
    public void setLdbv4151TpStatTit(String ldbv4151TpStatTit) {
        throw new FieldNotMappedException("ldbv4151TpStatTit");
    }

    @Override
    public AfDecimal getManfeeAntic() {
        return dettTitCont.getDtcManfeeAntic().getDtcManfeeAntic();
    }

    @Override
    public void setManfeeAntic(AfDecimal manfeeAntic) {
        this.dettTitCont.getDtcManfeeAntic().setDtcManfeeAntic(manfeeAntic.copy());
    }

    @Override
    public AfDecimal getManfeeAnticObj() {
        if (ws.getIndDettTitCont().getManfeeAntic() >= 0) {
            return getManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setManfeeAnticObj(AfDecimal manfeeAnticObj) {
        if (manfeeAnticObj != null) {
            setManfeeAntic(new AfDecimal(manfeeAnticObj, 15, 3));
            ws.getIndDettTitCont().setManfeeAntic(((short)0));
        }
        else {
            ws.getIndDettTitCont().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getManfeeRec() {
        return dettTitCont.getDtcManfeeRec().getDtcManfeeRec();
    }

    @Override
    public void setManfeeRec(AfDecimal manfeeRec) {
        this.dettTitCont.getDtcManfeeRec().setDtcManfeeRec(manfeeRec.copy());
    }

    @Override
    public AfDecimal getManfeeRecObj() {
        if (ws.getIndDettTitCont().getManfeeRec() >= 0) {
            return getManfeeRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setManfeeRecObj(AfDecimal manfeeRecObj) {
        if (manfeeRecObj != null) {
            setManfeeRec(new AfDecimal(manfeeRecObj, 15, 3));
            ws.getIndDettTitCont().setManfeeRec(((short)0));
        }
        else {
            ws.getIndDettTitCont().setManfeeRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getManfeeRicor() {
        return dettTitCont.getDtcManfeeRicor().getDtcManfeeRicor();
    }

    @Override
    public void setManfeeRicor(AfDecimal manfeeRicor) {
        this.dettTitCont.getDtcManfeeRicor().setDtcManfeeRicor(manfeeRicor.copy());
    }

    @Override
    public AfDecimal getManfeeRicorObj() {
        if (ws.getIndDettTitCont().getManfeeRicor() >= 0) {
            return getManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setManfeeRicorObj(AfDecimal manfeeRicorObj) {
        if (manfeeRicorObj != null) {
            setManfeeRicor(new AfDecimal(manfeeRicorObj, 15, 3));
            ws.getIndDettTitCont().setManfeeRicor(((short)0));
        }
        else {
            ws.getIndDettTitCont().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public int getNumGgRitardoPag() {
        return dettTitCont.getDtcNumGgRitardoPag().getDtcNumGgRitardoPag();
    }

    @Override
    public void setNumGgRitardoPag(int numGgRitardoPag) {
        this.dettTitCont.getDtcNumGgRitardoPag().setDtcNumGgRitardoPag(numGgRitardoPag);
    }

    @Override
    public Integer getNumGgRitardoPagObj() {
        if (ws.getIndDettTitCont().getNumGgRitardoPag() >= 0) {
            return ((Integer)getNumGgRitardoPag());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumGgRitardoPagObj(Integer numGgRitardoPagObj) {
        if (numGgRitardoPagObj != null) {
            setNumGgRitardoPag(((int)numGgRitardoPagObj));
            ws.getIndDettTitCont().setNumGgRitardoPag(((short)0));
        }
        else {
            ws.getIndDettTitCont().setNumGgRitardoPag(((short)-1));
        }
    }

    @Override
    public int getNumGgRival() {
        return dettTitCont.getDtcNumGgRival().getDtcNumGgRival();
    }

    @Override
    public void setNumGgRival(int numGgRival) {
        this.dettTitCont.getDtcNumGgRival().setDtcNumGgRival(numGgRival);
    }

    @Override
    public Integer getNumGgRivalObj() {
        if (ws.getIndDettTitCont().getNumGgRival() >= 0) {
            return ((Integer)getNumGgRival());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumGgRivalObj(Integer numGgRivalObj) {
        if (numGgRivalObj != null) {
            setNumGgRival(((int)numGgRivalObj));
            ws.getIndDettTitCont().setNumGgRival(((short)0));
        }
        else {
            ws.getIndDettTitCont().setNumGgRival(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreNet() {
        return dettTitCont.getDtcPreNet().getDtcPreNet();
    }

    @Override
    public void setPreNet(AfDecimal preNet) {
        this.dettTitCont.getDtcPreNet().setDtcPreNet(preNet.copy());
    }

    @Override
    public AfDecimal getPreNetObj() {
        if (ws.getIndDettTitCont().getPreNet() >= 0) {
            return getPreNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreNetObj(AfDecimal preNetObj) {
        if (preNetObj != null) {
            setPreNet(new AfDecimal(preNetObj, 15, 3));
            ws.getIndDettTitCont().setPreNet(((short)0));
        }
        else {
            ws.getIndDettTitCont().setPreNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrePpIas() {
        return dettTitCont.getDtcPrePpIas().getDtcPrePpIas();
    }

    @Override
    public void setPrePpIas(AfDecimal prePpIas) {
        this.dettTitCont.getDtcPrePpIas().setDtcPrePpIas(prePpIas.copy());
    }

    @Override
    public AfDecimal getPrePpIasObj() {
        if (ws.getIndDettTitCont().getPrePpIas() >= 0) {
            return getPrePpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrePpIasObj(AfDecimal prePpIasObj) {
        if (prePpIasObj != null) {
            setPrePpIas(new AfDecimal(prePpIasObj, 15, 3));
            ws.getIndDettTitCont().setPrePpIas(((short)0));
        }
        else {
            ws.getIndDettTitCont().setPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreSoloRsh() {
        return dettTitCont.getDtcPreSoloRsh().getDtcPreSoloRsh();
    }

    @Override
    public void setPreSoloRsh(AfDecimal preSoloRsh) {
        this.dettTitCont.getDtcPreSoloRsh().setDtcPreSoloRsh(preSoloRsh.copy());
    }

    @Override
    public AfDecimal getPreSoloRshObj() {
        if (ws.getIndDettTitCont().getPreSoloRsh() >= 0) {
            return getPreSoloRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreSoloRshObj(AfDecimal preSoloRshObj) {
        if (preSoloRshObj != null) {
            setPreSoloRsh(new AfDecimal(preSoloRshObj, 15, 3));
            ws.getIndDettTitCont().setPreSoloRsh(((short)0));
        }
        else {
            ws.getIndDettTitCont().setPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreTot() {
        return dettTitCont.getDtcPreTot().getDtcPreTot();
    }

    @Override
    public void setPreTot(AfDecimal preTot) {
        this.dettTitCont.getDtcPreTot().setDtcPreTot(preTot.copy());
    }

    @Override
    public AfDecimal getPreTotObj() {
        if (ws.getIndDettTitCont().getPreTot() >= 0) {
            return getPreTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreTotObj(AfDecimal preTotObj) {
        if (preTotObj != null) {
            setPreTot(new AfDecimal(preTotObj, 15, 3));
            ws.getIndDettTitCont().setPreTot(((short)0));
        }
        else {
            ws.getIndDettTitCont().setPreTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvAcq1aa() {
        return dettTitCont.getDtcProvAcq1aa().getDtcProvAcq1aa();
    }

    @Override
    public void setProvAcq1aa(AfDecimal provAcq1aa) {
        this.dettTitCont.getDtcProvAcq1aa().setDtcProvAcq1aa(provAcq1aa.copy());
    }

    @Override
    public AfDecimal getProvAcq1aaObj() {
        if (ws.getIndDettTitCont().getProvAcq1aa() >= 0) {
            return getProvAcq1aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvAcq1aaObj(AfDecimal provAcq1aaObj) {
        if (provAcq1aaObj != null) {
            setProvAcq1aa(new AfDecimal(provAcq1aaObj, 15, 3));
            ws.getIndDettTitCont().setProvAcq1aa(((short)0));
        }
        else {
            ws.getIndDettTitCont().setProvAcq1aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvAcq2aa() {
        return dettTitCont.getDtcProvAcq2aa().getDtcProvAcq2aa();
    }

    @Override
    public void setProvAcq2aa(AfDecimal provAcq2aa) {
        this.dettTitCont.getDtcProvAcq2aa().setDtcProvAcq2aa(provAcq2aa.copy());
    }

    @Override
    public AfDecimal getProvAcq2aaObj() {
        if (ws.getIndDettTitCont().getProvAcq2aa() >= 0) {
            return getProvAcq2aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvAcq2aaObj(AfDecimal provAcq2aaObj) {
        if (provAcq2aaObj != null) {
            setProvAcq2aa(new AfDecimal(provAcq2aaObj, 15, 3));
            ws.getIndDettTitCont().setProvAcq2aa(((short)0));
        }
        else {
            ws.getIndDettTitCont().setProvAcq2aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvDaRec() {
        return dettTitCont.getDtcProvDaRec().getDtcProvDaRec();
    }

    @Override
    public void setProvDaRec(AfDecimal provDaRec) {
        this.dettTitCont.getDtcProvDaRec().setDtcProvDaRec(provDaRec.copy());
    }

    @Override
    public AfDecimal getProvDaRecObj() {
        if (ws.getIndDettTitCont().getProvDaRec() >= 0) {
            return getProvDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvDaRecObj(AfDecimal provDaRecObj) {
        if (provDaRecObj != null) {
            setProvDaRec(new AfDecimal(provDaRecObj, 15, 3));
            ws.getIndDettTitCont().setProvDaRec(((short)0));
        }
        else {
            ws.getIndDettTitCont().setProvDaRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvInc() {
        return dettTitCont.getDtcProvInc().getDtcProvInc();
    }

    @Override
    public void setProvInc(AfDecimal provInc) {
        this.dettTitCont.getDtcProvInc().setDtcProvInc(provInc.copy());
    }

    @Override
    public AfDecimal getProvIncObj() {
        if (ws.getIndDettTitCont().getProvInc() >= 0) {
            return getProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvIncObj(AfDecimal provIncObj) {
        if (provIncObj != null) {
            setProvInc(new AfDecimal(provIncObj, 15, 3));
            ws.getIndDettTitCont().setProvInc(((short)0));
        }
        else {
            ws.getIndDettTitCont().setProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvRicor() {
        return dettTitCont.getDtcProvRicor().getDtcProvRicor();
    }

    @Override
    public void setProvRicor(AfDecimal provRicor) {
        this.dettTitCont.getDtcProvRicor().setDtcProvRicor(provRicor.copy());
    }

    @Override
    public AfDecimal getProvRicorObj() {
        if (ws.getIndDettTitCont().getProvRicor() >= 0) {
            return getProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvRicorObj(AfDecimal provRicorObj) {
        if (provRicorObj != null) {
            setProvRicor(new AfDecimal(provRicorObj, 15, 3));
            ws.getIndDettTitCont().setProvRicor(((short)0));
        }
        else {
            ws.getIndDettTitCont().setProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getRemunAss() {
        return dettTitCont.getDtcRemunAss().getDtcRemunAss();
    }

    @Override
    public void setRemunAss(AfDecimal remunAss) {
        this.dettTitCont.getDtcRemunAss().setDtcRemunAss(remunAss.copy());
    }

    @Override
    public AfDecimal getRemunAssObj() {
        if (ws.getIndDettTitCont().getRemunAss() >= 0) {
            return getRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRemunAssObj(AfDecimal remunAssObj) {
        if (remunAssObj != null) {
            setRemunAss(new AfDecimal(remunAssObj, 15, 3));
            ws.getIndDettTitCont().setRemunAss(((short)0));
        }
        else {
            ws.getIndDettTitCont().setRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprAlt() {
        return dettTitCont.getDtcSoprAlt().getDtcSoprAlt();
    }

    @Override
    public void setSoprAlt(AfDecimal soprAlt) {
        this.dettTitCont.getDtcSoprAlt().setDtcSoprAlt(soprAlt.copy());
    }

    @Override
    public AfDecimal getSoprAltObj() {
        if (ws.getIndDettTitCont().getSoprAlt() >= 0) {
            return getSoprAlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprAltObj(AfDecimal soprAltObj) {
        if (soprAltObj != null) {
            setSoprAlt(new AfDecimal(soprAltObj, 15, 3));
            ws.getIndDettTitCont().setSoprAlt(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSoprAlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprProf() {
        return dettTitCont.getDtcSoprProf().getDtcSoprProf();
    }

    @Override
    public void setSoprProf(AfDecimal soprProf) {
        this.dettTitCont.getDtcSoprProf().setDtcSoprProf(soprProf.copy());
    }

    @Override
    public AfDecimal getSoprProfObj() {
        if (ws.getIndDettTitCont().getSoprProf() >= 0) {
            return getSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprProfObj(AfDecimal soprProfObj) {
        if (soprProfObj != null) {
            setSoprProf(new AfDecimal(soprProfObj, 15, 3));
            ws.getIndDettTitCont().setSoprProf(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprSan() {
        return dettTitCont.getDtcSoprSan().getDtcSoprSan();
    }

    @Override
    public void setSoprSan(AfDecimal soprSan) {
        this.dettTitCont.getDtcSoprSan().setDtcSoprSan(soprSan.copy());
    }

    @Override
    public AfDecimal getSoprSanObj() {
        if (ws.getIndDettTitCont().getSoprSan() >= 0) {
            return getSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprSanObj(AfDecimal soprSanObj) {
        if (soprSanObj != null) {
            setSoprSan(new AfDecimal(soprSanObj, 15, 3));
            ws.getIndDettTitCont().setSoprSan(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprSpo() {
        return dettTitCont.getDtcSoprSpo().getDtcSoprSpo();
    }

    @Override
    public void setSoprSpo(AfDecimal soprSpo) {
        this.dettTitCont.getDtcSoprSpo().setDtcSoprSpo(soprSpo.copy());
    }

    @Override
    public AfDecimal getSoprSpoObj() {
        if (ws.getIndDettTitCont().getSoprSpo() >= 0) {
            return getSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprSpoObj(AfDecimal soprSpoObj) {
        if (soprSpoObj != null) {
            setSoprSpo(new AfDecimal(soprSpoObj, 15, 3));
            ws.getIndDettTitCont().setSoprSpo(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprTec() {
        return dettTitCont.getDtcSoprTec().getDtcSoprTec();
    }

    @Override
    public void setSoprTec(AfDecimal soprTec) {
        this.dettTitCont.getDtcSoprTec().setDtcSoprTec(soprTec.copy());
    }

    @Override
    public AfDecimal getSoprTecObj() {
        if (ws.getIndDettTitCont().getSoprTec() >= 0) {
            return getSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprTecObj(AfDecimal soprTecObj) {
        if (soprTecObj != null) {
            setSoprTec(new AfDecimal(soprTecObj, 15, 3));
            ws.getIndDettTitCont().setSoprTec(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpeAge() {
        return dettTitCont.getDtcSpeAge().getDtcSpeAge();
    }

    @Override
    public void setSpeAge(AfDecimal speAge) {
        this.dettTitCont.getDtcSpeAge().setDtcSpeAge(speAge.copy());
    }

    @Override
    public AfDecimal getSpeAgeObj() {
        if (ws.getIndDettTitCont().getSpeAge() >= 0) {
            return getSpeAge();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpeAgeObj(AfDecimal speAgeObj) {
        if (speAgeObj != null) {
            setSpeAge(new AfDecimal(speAgeObj, 15, 3));
            ws.getIndDettTitCont().setSpeAge(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSpeAge(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpeMed() {
        return dettTitCont.getDtcSpeMed().getDtcSpeMed();
    }

    @Override
    public void setSpeMed(AfDecimal speMed) {
        this.dettTitCont.getDtcSpeMed().setDtcSpeMed(speMed.copy());
    }

    @Override
    public AfDecimal getSpeMedObj() {
        if (ws.getIndDettTitCont().getSpeMed() >= 0) {
            return getSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpeMedObj(AfDecimal speMedObj) {
        if (speMedObj != null) {
            setSpeMed(new AfDecimal(speMedObj, 15, 3));
            ws.getIndDettTitCont().setSpeMed(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSpeMed(((short)-1));
        }
    }

    @Override
    public AfDecimal getTax() {
        return dettTitCont.getDtcTax().getDtcTax();
    }

    @Override
    public void setTax(AfDecimal tax) {
        this.dettTitCont.getDtcTax().setDtcTax(tax.copy());
    }

    @Override
    public AfDecimal getTaxObj() {
        if (ws.getIndDettTitCont().getTax() >= 0) {
            return getTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTaxObj(AfDecimal taxObj) {
        if (taxObj != null) {
            setTax(new AfDecimal(taxObj, 15, 3));
            ws.getIndDettTitCont().setTax(((short)0));
        }
        else {
            ws.getIndDettTitCont().setTax(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrPrest() {
        return dettTitCont.getDtcTotIntrPrest().getDtcTotIntrPrest();
    }

    @Override
    public void setTotIntrPrest(AfDecimal totIntrPrest) {
        this.dettTitCont.getDtcTotIntrPrest().setDtcTotIntrPrest(totIntrPrest.copy());
    }

    @Override
    public AfDecimal getTotIntrPrestObj() {
        if (ws.getIndDettTitCont().getTotIntrPrest() >= 0) {
            return getTotIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrPrestObj(AfDecimal totIntrPrestObj) {
        if (totIntrPrestObj != null) {
            setTotIntrPrest(new AfDecimal(totIntrPrestObj, 15, 3));
            ws.getIndDettTitCont().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndDettTitCont().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public String getTpRgmFisc() {
        return dettTitCont.getDtcTpRgmFisc();
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        this.dettTitCont.setDtcTpRgmFisc(tpRgmFisc);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }
}
