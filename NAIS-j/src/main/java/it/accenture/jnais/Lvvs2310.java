package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs2310Data;

/**Original name: LVVS2310<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2012.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS2310
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... MODULO CALCOLO VARIABILE ISVISMEDICA1
 * **------------------------------------------------------------***</pre>*/
public class Lvvs2310 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs2310Data ws = new Lvvs2310Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0001
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS2310_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU EX-S1000
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc() && this.idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs2310 getInstance() {
        return ((Lvvs2310)Programs.getInstance(Lvvs2310.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                           IX-INDICI.
        initIxIndici();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET WCOM-NO-TROVATO               TO TRUE.
        ws.getWcomFlagTrovato().setNoTrovato();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: PERFORM S0005-CTRL-DATI-INPUT
        //                THRU EX-S0005.
        s0005CtrlDatiInput();
    }

    /**Original name: S0005-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI INPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void s0005CtrlDatiInput() {
        // COB_CODE:      IF IVVC0213-COD-PARAMETRO = SPACES OR LOW-VALUE
        //           *---    COD-PARAMETRO NON VALORIZZATO
        //                     TO IDSV0003-DESCRIZ-ERR-DB2
        //                END-IF.
        if (Characters.EQ_SPACE.test(ivvc0213.getCodParametro()) || Characters.EQ_LOW.test(ivvc0213.getCodParametroFormatted())) {
            //---    COD-PARAMETRO NON VALORIZZATO
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'COD-PARAMETRO NON VALORIZZATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("COD-PARAMETRO NON VALORIZZATO");
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE:         IF IVVC0213-ID-ADESIONE > ZEROES AND
            //                      IVVC0213-ID-POLIZZA  = ZEROES
            //           *---       ID-POLIZZA NON VALORIZZATO
            //                        TO IDSV0003-DESCRIZ-ERR-DB2
            //                   END-IF
            if (Characters.GT_ZERO.test(ivvc0213.getIdAdesioneFormatted()) && Characters.EQ_ZERO.test(ivvc0213.getIdPolizzaFormatted())) {
                //---       ID-POLIZZA NON VALORIZZATO
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
            }
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.getIxIndici().setIxDclgen(((short)1));
        while (!(ws.getIxIndici().getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.getIxIndici().setIxDclgen(Trunc.toShort(ws.getIxIndici().getIxDclgen() + 1, 4));
        }
        //--> ROUTINE PER CERCARE IL CODICE PARAMETRO SULLA DCLGEN
        //--> PARAMETRO OGGETTO PRESENTE A CONTESTO
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                 THRU S1200-CONTROLLO-DATI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-PARAM-OGG
        //                TO DPOG-AREA-PARAM-OGG
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasParamOgg())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOG-AREA-PARAM-OGG
            ws.getDispatcherVariables().setDpogAreaParamOggFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF IVVC0213-COD-PARAMETRO = DPOG-COD-PARAM(IVVC0213-IX-TABB)
        //              END-IF
        //           END-IF.
        if (Conditions.eq(ivvc0213.getCodParametro(), ws.getDispatcherVariables().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogCodParam())) {
            // COB_CODE: IF  DPOG-VAL-NUM(IVVC0213-IX-TABB) IS NUMERIC
            //               END-EVALUATE
            //           ELSE
            //               SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
            //           END-IF
            if (Functions.isNumber(ws.getDispatcherVariables().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValNum().getWpogValNum())) {
                // COB_CODE: EVALUATE DPOG-VAL-NUM(IVVC0213-IX-TABB)
                //              WHEN 1
                //              WHEN 2
                //                   TO IVVC0213-VAL-IMP-O
                //              WHEN 0
                //                   TO IVVC0213-VAL-IMP-O
                //              WHEN OTHER
                //                 SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
                //           END-EVALUATE
                if ((ws.getDispatcherVariables().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValNum().getWpogValNum() == 1) || (ws.getDispatcherVariables().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValNum().getWpogValNum() == 2)) {
                    // COB_CODE: MOVE 1
                    //             TO IVVC0213-VAL-IMP-O
                    ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(1, 18, 7));
                }
                else if (ws.getDispatcherVariables().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValNum().getWpogValNum() == 0) {
                    // COB_CODE: MOVE 0
                    //             TO IVVC0213-VAL-IMP-O
                    ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
                }
                else {
                    // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.getIxIndici().setIxTabParamOgg(((short)0));
        ws.getIxIndici().setIxTabDco(((short)0));
        ws.getIxIndici().setIxTabAde(((short)0));
        ws.getIxIndici().setIxTabGrz(((short)0));
        ws.getIxIndici().setIxTabTga(((short)0));
        ws.getIxIndici().setIxTabPog(((short)0));
        ws.getIxIndici().setIxTabPmo(((short)0));
        ws.getIxIndici().setContFetch(((short)0));
        ws.getIxIndici().setIxDclgen(((short)0));
    }
}
