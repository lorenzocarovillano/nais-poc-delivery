package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.lang.types.RoundingMode;
import com.bphx.ctu.af.lang.util.MathUtil;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.A2kInput;
import it.accenture.jnais.copy.A2kOugbmba;
import it.accenture.jnais.copy.A2kOurid;
import it.accenture.jnais.copy.A2kOusta;
import it.accenture.jnais.copy.A2kOutput;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.copy.WtgaDati;
import it.accenture.jnais.ws.A2kOuamgX;
import it.accenture.jnais.ws.A2kOugl07X;
import it.accenture.jnais.ws.A2kOugmaX;
import it.accenture.jnais.ws.A2kOujulX;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WsTpTrch;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0450;
import it.accenture.jnais.ws.Lccs0450Data;
import it.accenture.jnais.ws.redefines.Lccc0450TabMatrice;
import it.accenture.jnais.ws.redefines.Wl19DtRilevazioneNav;
import it.accenture.jnais.ws.redefines.Wl19ValQuo;
import it.accenture.jnais.ws.redefines.Wl19ValQuoAcq;
import it.accenture.jnais.ws.redefines.Wl19ValQuoManfee;
import it.accenture.jnais.ws.redefines.WtgaAbbAnnuUlt;
import it.accenture.jnais.ws.redefines.WtgaAbbTotIni;
import it.accenture.jnais.ws.redefines.WtgaAbbTotUlt;
import it.accenture.jnais.ws.redefines.WtgaAcqExp;
import it.accenture.jnais.ws.redefines.WtgaAlqCommisInter;
import it.accenture.jnais.ws.redefines.WtgaAlqProvAcq;
import it.accenture.jnais.ws.redefines.WtgaAlqProvInc;
import it.accenture.jnais.ws.redefines.WtgaAlqProvRicor;
import it.accenture.jnais.ws.redefines.WtgaAlqRemunAss;
import it.accenture.jnais.ws.redefines.WtgaAlqScon;
import it.accenture.jnais.ws.redefines.WtgaBnsGiaLiqto;
import it.accenture.jnais.ws.redefines.WtgaCommisGest;
import it.accenture.jnais.ws.redefines.WtgaCommisInter;
import it.accenture.jnais.ws.redefines.WtgaCosRunAssva;
import it.accenture.jnais.ws.redefines.WtgaCosRunAssvaIdc;
import it.accenture.jnais.ws.redefines.WtgaCptInOpzRivto;
import it.accenture.jnais.ws.redefines.WtgaCptMinScad;
import it.accenture.jnais.ws.redefines.WtgaCptRshMor;
import it.accenture.jnais.ws.redefines.WtgaDtEffStab;
import it.accenture.jnais.ws.redefines.WtgaDtEmis;
import it.accenture.jnais.ws.redefines.WtgaDtIniValTar;
import it.accenture.jnais.ws.redefines.WtgaDtScad;
import it.accenture.jnais.ws.redefines.WtgaDtUltAdegPrePr;
import it.accenture.jnais.ws.redefines.WtgaDtVldtProd;
import it.accenture.jnais.ws.redefines.WtgaDurAa;
import it.accenture.jnais.ws.redefines.WtgaDurAbb;
import it.accenture.jnais.ws.redefines.WtgaDurGg;
import it.accenture.jnais.ws.redefines.WtgaDurMm;
import it.accenture.jnais.ws.redefines.WtgaEtaAa1oAssto;
import it.accenture.jnais.ws.redefines.WtgaEtaAa2oAssto;
import it.accenture.jnais.ws.redefines.WtgaEtaAa3oAssto;
import it.accenture.jnais.ws.redefines.WtgaEtaMm1oAssto;
import it.accenture.jnais.ws.redefines.WtgaEtaMm2oAssto;
import it.accenture.jnais.ws.redefines.WtgaEtaMm3oAssto;
import it.accenture.jnais.ws.redefines.WtgaIdMoviChiu;
import it.accenture.jnais.ws.redefines.WtgaImpAder;
import it.accenture.jnais.ws.redefines.WtgaImpAltSopr;
import it.accenture.jnais.ws.redefines.WtgaImpAz;
import it.accenture.jnais.ws.redefines.WtgaImpbCommisInter;
import it.accenture.jnais.ws.redefines.WtgaImpBns;
import it.accenture.jnais.ws.redefines.WtgaImpBnsAntic;
import it.accenture.jnais.ws.redefines.WtgaImpbProvAcq;
import it.accenture.jnais.ws.redefines.WtgaImpbProvInc;
import it.accenture.jnais.ws.redefines.WtgaImpbProvRicor;
import it.accenture.jnais.ws.redefines.WtgaImpbRemunAss;
import it.accenture.jnais.ws.redefines.WtgaImpbVisEnd2000;
import it.accenture.jnais.ws.redefines.WtgaImpCarAcq;
import it.accenture.jnais.ws.redefines.WtgaImpCarGest;
import it.accenture.jnais.ws.redefines.WtgaImpCarInc;
import it.accenture.jnais.ws.redefines.WtgaImpScon;
import it.accenture.jnais.ws.redefines.WtgaImpSoprProf;
import it.accenture.jnais.ws.redefines.WtgaImpSoprSan;
import it.accenture.jnais.ws.redefines.WtgaImpSoprSpo;
import it.accenture.jnais.ws.redefines.WtgaImpSoprTec;
import it.accenture.jnais.ws.redefines.WtgaImpTfr;
import it.accenture.jnais.ws.redefines.WtgaImpTfrStrc;
import it.accenture.jnais.ws.redefines.WtgaImpTrasfe;
import it.accenture.jnais.ws.redefines.WtgaImpVolo;
import it.accenture.jnais.ws.redefines.WtgaIncrPre;
import it.accenture.jnais.ws.redefines.WtgaIncrPrstz;
import it.accenture.jnais.ws.redefines.WtgaIntrMora;
import it.accenture.jnais.ws.redefines.WtgaManfeeAntic;
import it.accenture.jnais.ws.redefines.WtgaManfeeRicor;
import it.accenture.jnais.ws.redefines.WtgaMatuEnd2000;
import it.accenture.jnais.ws.redefines.WtgaMinGarto;
import it.accenture.jnais.ws.redefines.WtgaMinTrnut;
import it.accenture.jnais.ws.redefines.WtgaNumGgRival;
import it.accenture.jnais.ws.redefines.WtgaOldTsTec;
import it.accenture.jnais.ws.redefines.WtgaPcCommisGest;
import it.accenture.jnais.ws.redefines.WtgaPcIntrRiat;
import it.accenture.jnais.ws.redefines.WtgaPcRetr;
import it.accenture.jnais.ws.redefines.WtgaPcRipPre;
import it.accenture.jnais.ws.redefines.WtgaPreAttDiTrch;
import it.accenture.jnais.ws.redefines.WtgaPreCasoMor;
import it.accenture.jnais.ws.redefines.WtgaPreIniNet;
import it.accenture.jnais.ws.redefines.WtgaPreInvrioIni;
import it.accenture.jnais.ws.redefines.WtgaPreInvrioUlt;
import it.accenture.jnais.ws.redefines.WtgaPreLrd;
import it.accenture.jnais.ws.redefines.WtgaPrePattuito;
import it.accenture.jnais.ws.redefines.WtgaPrePpIni;
import it.accenture.jnais.ws.redefines.WtgaPrePpUlt;
import it.accenture.jnais.ws.redefines.WtgaPreRivto;
import it.accenture.jnais.ws.redefines.WtgaPreStab;
import it.accenture.jnais.ws.redefines.WtgaPreTariIni;
import it.accenture.jnais.ws.redefines.WtgaPreTariUlt;
import it.accenture.jnais.ws.redefines.WtgaPreUniRivto;
import it.accenture.jnais.ws.redefines.WtgaProv1aaAcq;
import it.accenture.jnais.ws.redefines.WtgaProv2aaAcq;
import it.accenture.jnais.ws.redefines.WtgaProvInc;
import it.accenture.jnais.ws.redefines.WtgaProvRicor;
import it.accenture.jnais.ws.redefines.WtgaPrstzAggIni;
import it.accenture.jnais.ws.redefines.WtgaPrstzAggUlt;
import it.accenture.jnais.ws.redefines.WtgaPrstzIni;
import it.accenture.jnais.ws.redefines.WtgaPrstzIniNewfis;
import it.accenture.jnais.ws.redefines.WtgaPrstzIniNforz;
import it.accenture.jnais.ws.redefines.WtgaPrstzIniStab;
import it.accenture.jnais.ws.redefines.WtgaPrstzRidIni;
import it.accenture.jnais.ws.redefines.WtgaPrstzUlt;
import it.accenture.jnais.ws.redefines.WtgaRatLrd;
import it.accenture.jnais.ws.redefines.WtgaRemunAss;
import it.accenture.jnais.ws.redefines.WtgaRendtoLrd;
import it.accenture.jnais.ws.redefines.WtgaRendtoRetr;
import it.accenture.jnais.ws.redefines.WtgaRenIniTsTec0;
import it.accenture.jnais.ws.redefines.WtgaRisMat;
import it.accenture.jnais.ws.redefines.WtgaTsRivalFis;
import it.accenture.jnais.ws.redefines.WtgaTsRivalIndiciz;
import it.accenture.jnais.ws.redefines.WtgaTsRivalNet;
import it.accenture.jnais.ws.redefines.WtgaVisEnd2000;
import it.accenture.jnais.ws.redefines.WtgaVisEnd2000Nforz;
import it.accenture.jnais.ws.Variabili;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.WkDtRicorTranche;
import it.accenture.jnais.ws.Wl19AreaFondi;
import it.accenture.jnais.ws.WtgaAreaTrancheLoas0800;
import static java.lang.Math.abs;

/**Original name: LCCS0450<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                      *
 *  F A S E         : SERVIZIO PER POPOLAMENTO MATRICE             *
 *                    RIEPILOGATIVA DELLA SITUAZIONE DEI FONDI     *
 *                    PER LA RISERVA MATEMATICA                    *
 * ----------------------------------------------------------------*</pre>*/
public class Lccs0450 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0450Data ws = new Lccs0450Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WTGA-AREA-TRCH-DI-GAR
    private WtgaAreaTrancheLoas0800 wtgaAreaTrchDiGar;
    //Original name: WL19-AREA-FONDI
    private Wl19AreaFondi wl19AreaFondi;
    //Original name: LCCC0450
    private Lccc0450 lccc0450;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0450_FIRST_SENTENCES<br>
	 * <pre>--> COPY PER L'INIZIALIZZAZIONE AREA ERRORI
	 * --> DELL'AREA CONTESTO.
	 *  CONTIENE UNA SEQUENZA DI ISTRUZIONI PER AZZERAR 'AREA DEGLI
	 * ERRORI DELL'AREA CONTESTO.</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WtgaAreaTrancheLoas0800 wtgaAreaTrchDiGar, Wl19AreaFondi wl19AreaFondi, Lccc0450 lccc0450) {
        this.areaIdsv0001 = areaIdsv0001;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaTrchDiGar = wtgaAreaTrchDiGar;
        this.wl19AreaFondi = wl19AreaFondi;
        this.lccc0450 = lccc0450;
        // COB_CODE: MOVE 'OK'   TO IDSV0001-ESITO
        this.areaIdsv0001.getEsito().setEsito("OK");
        // COB_CODE: MOVE SPACES TO IDSV0001-LOG-ERRORE
        this.areaIdsv0001.getLogErrore().initLogErroreSpaces();
        // COB_CODE: SET IDSV0001-FORZ-RC-04-NO        TO TRUE
        this.areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        // COB_CODE: PERFORM VARYING IDSV0001-MAX-ELE-ERRORI FROM 1 BY 1
        //                     UNTIL IDSV0001-MAX-ELE-ERRORI > 10
        //               TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
        //           END-PERFORM
        this.areaIdsv0001.setMaxEleErrori(((short)1));
        while (!(this.areaIdsv0001.getMaxEleErrori() > 10)) {
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            this.areaIdsv0001.getEleErrori(this.areaIdsv0001.getMaxEleErrori()).setDescErrore("");
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            this.areaIdsv0001.getEleErrori(this.areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(0);
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            this.areaIdsv0001.getEleErrori(this.areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)0));
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
            this.areaIdsv0001.getEleErrori(this.areaIdsv0001.getMaxEleErrori()).setTipoTrattFe(Types.SPACE_CHAR);
            this.areaIdsv0001.setMaxEleErrori(Trunc.toShort(this.areaIdsv0001.getMaxEleErrori() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES TO IDSV0001-MAX-ELE-ERRORI.
        this.areaIdsv0001.setMaxEleErrori(((short)0));
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              PERFORM A100-ELABORA          THRU A100-EX
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM A100-ELABORA          THRU A100-EX
            a100Elabora();
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Lccs0450 getInstance() {
        return ((Lccs0450)Programs.getInstance(Lccs0450.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************
	 *                 WK-MAX-ELE-RAMO</pre>*/
    private void a000Inizio() {
        ConcatUtil concatUtil = null;
        // COB_CODE:      INITIALIZE  VAL-AST
        //           *                WK-MAX-ELE-RAMO
        //                            WK-NUMERO-QUOTE
        //                            WK-DT-RICOR-TRANCHE-NUM
        //                            WK-ANNO
        //                            VARIABILI
        //                            INDICI
        //                            WCOM-TABELLE
        initValAst();
        ws.getVariabili().setWkNumeroQuote(new AfDecimal(0, 12, 5));
        ws.getVariabili().getWkDtRicorTranche().setWkDtRicorTrancheNumFormatted("00000000");
        ws.getVariabili().setWkAnnoFormatted("0000");
        initVariabili();
        initIndici();
        initWcomTabelle();
        //                WK-RAMO-GAR.
        // COB_CODE: MOVE 'A000-INIZIO'               TO WK-LABEL.
        ws.getVariabili().setWkLabel("A000-INIZIO");
        //    inizializzazione tabella ramo
        //    INITIALIZE WK-TAB-RAMO-GAR(1).
        //    MOVE WK-TAB-RAMO1     TO WK-RESTO-TAB-RAMO1.
        //    INITIALIZE LCCC0450-AREA-OUTPUT.
        // COB_CODE: INITIALIZE LCCC0450-ELE-TAB-MAX
        //                      LCCC0450-MATRICE(1).
        lccc0450.setLccc0450EleTabMax(((short)0));
        initMatrice();
        // COB_CODE: MOVE LCCC0450-TAB-MATRICE TO LCCC0450-RESTO-TAB-MATRICE.
        lccc0450.getLccc0450TabMatrice().setRestoTabMatrice(lccc0450.getLccc0450TabMatrice().getLccc0450TabMatriceFormatted());
        // COB_CODE: MOVE ZEROES
        //             TO WL19-ELE-FND-MAX
        wl19AreaFondi.setEleFndMax(((short)0));
        // COB_CODE: IF LCCC0450-FLAG-OPER  = 'TG' OR 'GA'
        //              CONTINUE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (Conditions.eq(lccc0450.getLccc0450FlagOper(), "TG") || Conditions.eq(lccc0450.getLccc0450FlagOper(), "GA")) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
            // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: STRING 'FLAG-OPERAZIONE = '
            //                  LCCC0450-FLAG-OPER
            //                  ' NON CONSENTITO'
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "FLAG-OPERAZIONE = ", lccc0450.getLccc0450FlagOperFormatted(), " NON CONSENTITO");
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        // COB_CODE: IF LCCC0450-FLAG-OPER = 'TG'
        //              END-IF
        //           END-IF.
        if (Conditions.eq(lccc0450.getLccc0450FlagOper(), "TG")) {
            // COB_CODE: IF WTGA-ELE-TGA-MAX > ZEROES
            //              CONTINUE
            //           ELSE
            //                 THRU EX-S0300
            //           END-IF
            if (wtgaAreaTrchDiGar.getEleTranMax() > 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
                // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005166");
                // COB_CODE: STRING 'OPERAZIONE NON CONSENTITA - '
                //                  'TRANCHE NON ESISTENTI'
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "OPERAZIONE NON CONSENTITA - ", "TRANCHE NON ESISTENTI");
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        // COB_CODE: IF LCCC0450-FLAG-OPER = 'GA'
        //              END-IF
        //           END-IF.
        if (Conditions.eq(lccc0450.getLccc0450FlagOper(), "GA")) {
            // COB_CODE: IF WGRZ-ELE-GAR-MAX > ZEROES
            //              CONTINUE
            //           ELSE
            //                 THRU EX-S0300
            //           END-IF
            if (wgrzAreaGaranzia.getEleGarMax() > 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
                // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005166");
                // COB_CODE: STRING 'OPERAZIONE NON CONSENTITA - '
                //                  'GARANZIE NON ESISTENTI'
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "OPERAZIONE NON CONSENTITA - ", "GARANZIE NON ESISTENTI");
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        //--> CONTROLLO SULLA DATA RISERVA
        // COB_CODE: IF LCCC0450-DATA-RISERVA IS NOT NUMERIC
        //           OR LCCC0450-DATA-RISERVA = ZEROES
        //                 THRU EX-S0300
        //           END-IF.
        if (!Functions.isNumber(lccc0450.getLccc0450DataRiservaFormatted()) || Characters.EQ_ZERO.test(lccc0450.getLccc0450DataRiservaFormatted())) {
            // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
            // COB_CODE: MOVE '005007'   TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005007");
            // COB_CODE: STRING 'DATA RISERVA'
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr("DATA RISERVA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: A100-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100Elabora() {
        // COB_CODE: MOVE 'A100-ELABORA'            TO WK-LABEL.
        ws.getVariabili().setWkLabel("A100-ELABORA");
        //--> SE FLAG DI MODALITA = 'GA' RECUPERA SOLO LE GARANZIA DI
        //--> RAMO III E LE RELATIVE TRANCHE
        // COB_CODE: IF LCCC0450-FLAG-OPER = 'GA'
        //                   OR IDSV0001-ESITO-KO
        //           END-IF.
        if (Conditions.eq(lccc0450.getLccc0450FlagOper(), "GA")) {
            // COB_CODE: PERFORM A120-FILTRA-GAR-RAMO-III
            //              THRU A120-EX
            a120FiltraGarRamoIii();
            // COB_CODE: PERFORM INIZIA-TOT-TGA
            //              THRU INIZIA-TOT-TGA-EX
            //           VARYING IX-TAB-TGA FROM 1 BY 1
            //             UNTIL IX-TAB-TGA > 100
            ws.getIndici().setTabTga(1);
            while (!(ws.getIndici().getTabTga() > 100)) {
                iniziaTotTga();
                ws.getIndici().setTabTga(Trunc.toInt(ws.getIndici().getTabTga() + 1, 4));
            }
            // COB_CODE: MOVE ZEROES                 TO IX-TAB-TGA
            ws.getIndici().setTabTga(0);
            // COB_CODE: PERFORM A130-RECUP-TRCH-DI-GAR
            //              THRU A130-EX
            //           VARYING IX-TAB-GAR FROM 1 BY 1
            //             UNTIL IX-TAB-GAR > WGAR-ELE-GAR-MAX
            //                OR IDSV0001-ESITO-KO
            ws.getIndici().setTabGar(1);
            while (!(ws.getIndici().getTabGar() > ws.getWgarEleGarMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                a130RecupTrchDiGar();
                ws.getIndici().setTabGar(Trunc.toInt(ws.getIndici().getTabGar() + 1, 4));
            }
        }
        //--> SE FLAG DI MODALITA = 'TG' ACCEDE ALLA VALOR ASSET
        // COB_CODE: PERFORM A140-RECUP-VALORE-ASSET
        //              THRU A140-EX
        //           VARYING IX-TAB-TGA FROM 1 BY 1
        //             UNTIL IX-TAB-TGA > WTGA-ELE-TGA-MAX
        //                OR IDSV0001-ESITO-KO.
        ws.getIndici().setTabTga(1);
        while (!(ws.getIndici().getTabTga() > wtgaAreaTrchDiGar.getEleTranMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            a140RecupValoreAsset();
            ws.getIndici().setTabTga(Trunc.toInt(ws.getIndici().getTabTga() + 1, 4));
        }
        //--> CALCOLO IL TOTALE DEL CONTRATTO
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU A180-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM A180-CALC-TOTALE
            //              THRU A180-EX
            a180CalcTotale();
        }
    }

    /**Original name: A120-FILTRA-GAR-RAMO-III<br>
	 * <pre>----------------------------------------------------------------*
	 *     FILTRA LE GARANZIA DI RAMO III
	 * ----------------------------------------------------------------*</pre>*/
    private void a120FiltraGarRamoIii() {
        // COB_CODE: MOVE 'A120-FILTRA-GAR-RAMO-III'     TO WK-LABEL.
        ws.getVariabili().setWkLabel("A120-FILTRA-GAR-RAMO-III");
        // COB_CODE: PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
        //             UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
        //             END-IF
        //           END-PERFORM.
        ws.getIndici().setTabGrz(1);
        while (!(ws.getIndici().getTabGrz() > wgrzAreaGaranzia.getEleGarMax())) {
            // COB_CODE: IF WGRZ-RAMO-BILA(IX-TAB-GRZ) = '3'
            //                TO WGAR-TAB-GAR(IX-TAB-GAR)
            //           END-IF
            if (Conditions.eq(wgrzAreaGaranzia.getTabGar(ws.getIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzRamoBila(), "3")) {
                // COB_CODE: code not available
                ws.setIntRegister1(((short)1));
                ws.setWgarEleGarMax(Trunc.toShort(ws.getIntRegister1() + ws.getWgarEleGarMax(), 4));
                ws.getIndici().setTabGar(Trunc.toInt(abs(ws.getIntRegister1() + ws.getIndici().getTabGar()), 4));
                // COB_CODE: MOVE WGRZ-TAB-GAR(IX-TAB-GRZ)
                //             TO WGAR-TAB-GAR(IX-TAB-GAR)
                ws.getWgarTabGar(ws.getIndici().getTabGar()).setWgarTabGarBytes(wgrzAreaGaranzia.getTabGar(ws.getIndici().getTabGrz()).getTabGarBytes());
            }
            ws.getIndici().setTabGrz(Trunc.toInt(ws.getIndici().getTabGrz() + 1, 4));
        }
    }

    /**Original name: A130-RECUP-TRCH-DI-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA DATI TRANCHE DI GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void a130RecupTrchDiGar() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A130-RECUP-TRCH-DI-GAR'  TO WK-LABEL.
        ws.getVariabili().setWkLabel("A130-RECUP-TRCH-DI-GAR");
        // COB_CODE: MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
        ws.getIdsv8888().setAreaDisplay(ws.getVariabili().getWkLabel());
        //    PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX.
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: SET INIT-CUR-TGA               TO TRUE.
        ws.getFlagCurTga().setInitCurTga();
        // COB_CODE: SET IDSI0011-FETCH-FIRST       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC     TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR IDSV0001-ESITO-KO
        //                      OR FINE-CUR-TGA
        //             END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || areaIdsv0001.getEsito().isIdsv0001EsitoKo() || ws.getFlagCurTga().isFineCurTga())) {
            // COB_CODE: INITIALIZE LDBV0011
            initLdbv0011();
            // COB_CODE: SET IDSI0011-WHERE-CONDITION TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
            // COB_CODE: SET IDSI0011-TRATT-DEFAULT   TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
            // COB_CODE: MOVE LCCC0450-DATA-EFFETTO  TO IDSI0011-DATA-INIZIO-EFFETTO
            //                                          IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(lccc0450.getLccc0450DataEffetto());
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(lccc0450.getLccc0450DataEffetto());
            // COB_CODE: MOVE LCCC0450-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(lccc0450.getLccc0450DataCompetenza());
            // COB_CODE: MOVE WGRZ-ID-GAR(IX-TAB-GRZ)  TO LDBV0011-ID-GAR
            ws.getLdbv0011().setGar(wgrzAreaGaranzia.getTabGar(ws.getIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar());
            // COB_CODE: MOVE LDBV0011                 TO IDSI0011-BUFFER-WHERE-COND
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv0011().getLdbv0011Formatted());
            // COB_CODE: MOVE 'LDBS0130'               TO IDSI0011-CODICE-STR-DATO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS0130");
            // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
            // COB_CODE: PERFORM CALL-DISPATCHER      THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:        IF IDSO0011-SUCCESSFUL-RC
            //                     END-EVALUATE
            //                  ELSE
            //           *-->   GESTIONE ERRORE
            //                        THRU EX-S0300
            //                  END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:           EVALUATE TRUE
                //                         WHEN IDSO0011-NOT-FOUND
                //           *-->          NESSUN DATO IN TABELLA
                //                            END-IF
                //                         WHEN IDSO0011-SUCCESSFUL-SQL
                //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                //                            SET IDSI0011-FETCH-NEXT     TO TRUE
                //                         WHEN OTHER
                //           *--->         ERRORE DI ACCESSO AL DB
                //                               THRU EX-S0300
                //                     END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.NOT_FOUND://-->          NESSUN DATO IN TABELLA
                        // COB_CODE: SET FINE-CUR-TGA TO TRUE
                        ws.getFlagCurTga().setFineCurTga();
                        // COB_CODE: IF IDSI0011-FETCH-FIRST
                        //                 THRU EX-S0300
                        //           END-IF
                        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                            // COB_CODE: MOVE WK-PGM   TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                            // COB_CODE: MOVE WK-LABEL TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
                            // COB_CODE: MOVE '005016' TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("005016");
                            // COB_CODE: STRING PGM-LDBS0130         ';'
                            //                  IDSO0011-RETURN-CODE ';'
                            //                  IDSO0011-SQLCODE
                            //                  DELIMITED BY SIZE
                            //             INTO IEAI9901-PARAMETRI-ERR
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmLdbs0130Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //              THRU EX-S0300
                            s0300RicercaGravitaErrore();
                        }
                        break;

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: MOVE IDSO0011-BUFFER-DATI   TO TRCH-DI-GAR
                        ws.getTrchDiGar().setTrchDiGarFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        // COB_CODE: ADD 1                       TO IX-TAB-TGA
                        ws.getIndici().setTabTga(Trunc.toInt(1 + ws.getIndici().getTabTga(), 4));
                        // COB_CODE: SET WTGA-ST-INV(IX-TAB-TGA) TO TRUE
                        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getStatus().setInv();
                        // COB_CODE: MOVE IX-TAB-TGA             TO WTGA-ELE-TGA-MAX
                        wtgaAreaTrchDiGar.setEleTranMax(((short)(ws.getIndici().getTabTga())));
                        // COB_CODE: PERFORM VALORIZZA-OUTPUT-TGA
                        //              THRU VALORIZZA-OUTPUT-TGA-EX
                        valorizzaOutputTga();
                        // COB_CODE: SET IDSI0011-FETCH-NEXT     TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                        break;

                    default://--->         ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
                        // COB_CODE: MOVE '005016'   TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005016");
                        // COB_CODE: STRING PGM-LDBS0130         ';'
                        //                  IDSO0011-RETURN-CODE ';'
                        //                  IDSO0011-SQLCODE
                        //                  DELIMITED BY SIZE
                        //                  INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmLdbs0130Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //-->   GESTIONE ERRORE
                // COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL         TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
                // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING PGM-LDBS0130         ';'
                //                  IDSO0011-RETURN-CODE ';'
                //                  IDSO0011-SQLCODE
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmLdbs0130Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: A140-RECUP-VALORE-ASSET<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA DATI TABELLA VALORE ASSET
	 * ----------------------------------------------------------------*</pre>*/
    private void a140RecupValoreAsset() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A140-RECUP-VALORE-ASSET' TO WK-LABEL.
        ws.getVariabili().setWkLabel("A140-RECUP-VALORE-ASSET");
        // COB_CODE: MOVE ZEROES                    TO IX-TAB-GRZ
        ws.getIndici().setTabGrz(0);
        // COB_CODE: MOVE WTGA-TP-TRCH(IX-TAB-TGA)  TO WS-TP-TRCH
        ws.getWsTpTrch().setWsTpTrch(wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaTpTrch());
        // COB_CODE: EVALUATE TRUE
        //             WHEN TP-TRCH-NEG-PRCOS
        //             WHEN TP-TRCH-NEG-RIS-PAR
        //             WHEN TP-TRCH-NEG-RIS-PRO
        //             WHEN TP-TRCH-NEG-IMPST-SOST
        //             WHEN TP-TRCH-NEG-DA-DIS
        //             WHEN TP-TRCH-NEG-INV-DA-SWIT
        //                  SET TRANCHE-NEGATIVA    TO TRUE
        //             WHEN OTHER
        //                  SET TRANCHE-POSITIVA    TO TRUE
        //           END-EVALUATE.
        switch (ws.getWsTpTrch().getWsTpTrch()) {

            case WsTpTrch.NEG_PRCOS:
            case WsTpTrch.NEG_RIS_PAR:
            case WsTpTrch.NEG_RIS_PRO:
            case WsTpTrch.NEG_IMPST_SOST:
            case WsTpTrch.NEG_DA_DIS:
            case WsTpTrch.NEG_INV_DA_SWIT:// COB_CODE: SET TRANCHE-NEGATIVA    TO TRUE
                ws.getFlagTpTrch().setNegativa();
                break;

            default:// COB_CODE: SET TRANCHE-POSITIVA    TO TRUE
                ws.getFlagTpTrch().setPositiva();
                break;
        }
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: SET  INIT-CUR-VAS              TO TRUE.
        ws.getFlagCurVas().setInitCurVas();
        // COB_CODE: SET  IDSI0011-FETCH-FIRST      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR IDSV0001-ESITO-KO
        //                      OR FINE-CUR-VAS
        //              END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || areaIdsv0001.getEsito().isIdsv0001EsitoKo() || ws.getFlagCurVas().isFineCurVas())) {
            // COB_CODE: INITIALIZE VAL-AST
            //                      LDBV4911
            initValAst();
            initLdbv4911();
            // COB_CODE: SET IDSI0011-WHERE-CONDITION TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
            // COB_CODE: SET IDSI0011-TRATT-DEFAULT   TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
            // COB_CODE: MOVE LCCC0450-DATA-EFFETTO TO IDSI0011-DATA-INIZIO-EFFETTO
            //                                         IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(lccc0450.getLccc0450DataEffetto());
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(lccc0450.getLccc0450DataEffetto());
            // COB_CODE: MOVE LCCC0450-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(lccc0450.getLccc0450DataCompetenza());
            // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
            //                                        TO LDBV4911-ID-TRCH-DI-GAR
            ws.getLdbv4911().setIdTrchDiGar(wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
            // COB_CODE: MOVE SPACES                  TO LDBV4911-TP-VAL-AST-1
            //                                           LDBV4911-TP-VAL-AST-2
            //                                           LDBV4911-TP-VAL-AST-3
            //                                           LDBV4911-TP-VAL-AST-4
            //                                           LDBV4911-TP-VAL-AST-5
            //                                           LDBV4911-TP-VAL-AST-6
            //                                           LDBV4911-TP-VAL-AST-7
            //                                           LDBV4911-TP-VAL-AST-8
            //                                           LDBV4911-TP-VAL-AST-9
            //                                           LDBV4911-TP-VAL-AST-10
            ws.getLdbv4911().setTpValAst1("");
            ws.getLdbv4911().setTpValAst2("");
            ws.getLdbv4911().setTpValAst3("");
            ws.getLdbv4911().setTpValAst4("");
            ws.getLdbv4911().setTpValAst5("");
            ws.getLdbv4911().setTpValAst6("");
            ws.getLdbv4911().setTpValAst7("");
            ws.getLdbv4911().setTpValAst8("");
            ws.getLdbv4911().setTpValAst9("");
            ws.getLdbv4911().setTpValAst10("");
            // COB_CODE: MOVE LDBV4911                TO IDSI0011-BUFFER-WHERE-COND
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv4911().getLdbv4911Formatted());
            // COB_CODE: MOVE 'LDBS4910'              TO IDSI0011-CODICE-STR-DATO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS4910");
            // COB_CODE: MOVE VAL-AST                 TO IDSI0011-BUFFER-DATI
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getValAst().getValAstFormatted());
            // COB_CODE: PERFORM CALL-DISPATCHER      THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:         IF IDSO0011-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *-->    GESTIONE ERRORE
            //                         THRU EX-S0300
            //                   END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                          WHEN IDSO0011-NOT-FOUND
                //           *-->           NESSUN DATO IN TABELLA
                //                             SET FINE-CUR-VAS          TO TRUE
                //                          WHEN IDSO0011-SUCCESSFUL-SQL
                //           *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                //                             SET IDSI0011-FETCH-NEXT   TO TRUE
                //                          WHEN OTHER
                //           *--->          ERRORE DI ACCESSO AL DB
                //                                THRU EX-S0300
                //                      END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.NOT_FOUND://-->           NESSUN DATO IN TABELLA
                        // COB_CODE: SET FINE-CUR-VAS          TO TRUE
                        ws.getFlagCurVas().setFineCurVas();
                        break;

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO VAL-AST
                        ws.getValAst().setValAstFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        // COB_CODE: PERFORM A150-CARICA-COD-FONDO
                        //              THRU A150-EX
                        a150CaricaCodFondo();
                        // COB_CODE: SET IDSI0011-FETCH-NEXT   TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                        break;

                    default://--->          ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
                        // COB_CODE: MOVE '005016'   TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005016");
                        // COB_CODE: STRING PGM-LDBS4910         ';'
                        //                  IDSO0011-RETURN-CODE ';'
                        //                  IDSO0011-SQLCODE
                        //                  DELIMITED BY SIZE
                        //                  INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmLdbs4910Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //-->    GESTIONE ERRORE
                // COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL         TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
                // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING PGM-LDBS4910         ';'
                //                  IDSO0011-RETURN-CODE ';'
                //                  IDSO0011-SQLCODE
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmLdbs4910Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: A150-CARICA-COD-FONDO<br>
	 * <pre>----------------------------------------------------------------*
	 *   CARICO CODICE FONDO, NUMERO QUOTE , VALORE QUOTA
	 * ----------------------------------------------------------------*</pre>*/
    private void a150CaricaCodFondo() {
        // COB_CODE: MOVE 'A150-CARICA-COD-FONDO'   TO WK-LABEL.
        ws.getVariabili().setWkLabel("A150-CARICA-COD-FONDO");
        // COB_CODE: PERFORM VARYING IX-TAB-MAT FROM 1 BY 1
        //             UNTIL IX-TAB-MAT > LCCC0450-ELE-TAB-MAX
        //                OR (VAS-COD-FND        = LCCC0450-COD-FONDO(IX-TAB-MAT)
        //                AND VAS-ID-TRCH-DI-GAR = LCCC0450-ID-TRCH-DI-GAR
        //                                                           (IX-TAB-MAT))
        //           END-PERFORM.
        ws.getIndici().setTabMat(1);
        while (!(ws.getIndici().getTabMat() > lccc0450.getLccc0450EleTabMax() || Conditions.eq(ws.getValAst().getVasCodFnd(), lccc0450.getLccc0450TabMatrice().getCodFondo(ws.getIndici().getTabMat())) && ws.getValAst().getVasIdTrchDiGar().getVasIdTrchDiGar() == lccc0450.getLccc0450TabMatrice().getIdTrchDiGar(ws.getIndici().getTabMat()))) {
            ws.getIndici().setTabMat(Trunc.toInt(ws.getIndici().getTabMat() + 1, 4));
        }
        // COB_CODE: IF IX-TAB-MAT > LCCC0450-ELE-TAB-MAX
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIndici().getTabMat() > lccc0450.getLccc0450EleTabMax()) {
            // COB_CODE: MOVE LCCC0450-ELE-TAB-MAX   TO IX-TAB-TOT
            ws.getIndici().setTabTot(TruncAbs.toInt(lccc0450.getLccc0450EleTabMax(), 4));
            // COB_CODE: ADD 1                       TO IX-TAB-TOT
            ws.getIndici().setTabTot(Trunc.toInt(1 + ws.getIndici().getTabTot(), 4));
            //-->    VALORIZZO IL CAMPO CODICE FONDO PER LA VARIABILE LCODFONDO
            // COB_CODE: MOVE VAS-COD-FND
            //             TO LCCC0450-COD-FONDO(IX-TAB-TOT)
            lccc0450.getLccc0450TabMatrice().setCodFondo(ws.getIndici().getTabTot(), ws.getValAst().getVasCodFnd());
            //-->    CALCOLO PER LA VARIABILE LVALQUOTE
            // COB_CODE: PERFORM A250-RECUP-QTZ-AGG
            //              THRU A250-EX
            a250RecupQtzAgg();
            // COB_CODE: IF IDSV0001-ESITO-OK
            //               END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: MOVE L41-VAL-QUO
                //             TO LCCC0450-VAL-QUOTA(IX-TAB-TOT)
                lccc0450.getLccc0450TabMatrice().setValQuota(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getQuotzAggFnd().getValQuo(), 15, 3));
                //-->      SALVO I DATI DELLA VALORE ASSET ORIGINARIA DELLA TRANCHE
                // COB_CODE: IF  TRANCHE-POSITIVA
                //           AND VAS-TP-VAL-AST = VALORE-POSITIVO
                //              END-IF
                //           END-IF
                if (ws.getFlagTpTrch().isPositiva() && Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValorePositivo())) {
                    // COB_CODE: IF VAS-NUM-QUO-NULL NOT = HIGH-VALUES
                    //                TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted())) {
                        // COB_CODE: MOVE VAS-NUM-QUO
                        //             TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                        lccc0450.getLccc0450TabMatrice().setNumQuoteIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getValAst().getVasNumQuo().getVasNumQuo(), 12, 5));
                    }
                    // COB_CODE: IF VAS-VAL-QUO-NULL NOT = HIGH-VALUES
                    //                TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                    //           ELSE
                    //                TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getValAst().getVasValQuo().getVasValQuoNullFormatted())) {
                        // COB_CODE: MOVE VAS-VAL-QUO
                        //             TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                        lccc0450.getLccc0450TabMatrice().setValQuotaIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getValAst().getVasValQuo().getVasValQuo(), 15, 3));
                    }
                    else {
                        // COB_CODE: MOVE L41-VAL-QUO
                        //             TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                        lccc0450.getLccc0450TabMatrice().setValQuotaIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getQuotzAggFnd().getValQuo(), 15, 3));
                    }
                    // COB_CODE: IF VAS-DT-VALZZ-NULL NOT = HIGH-VALUES
                    //                TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getValAst().getVasDtValzz().getVasDtValzzNullFormatted())) {
                        // COB_CODE: MOVE VAS-DT-VALZZ
                        //             TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                        lccc0450.getLccc0450TabMatrice().setDtValQuote(ws.getIndici().getTabTot(), TruncAbs.toInt(ws.getValAst().getVasDtValzz().getVasDtValzz(), 8));
                    }
                }
                //-->      SALVO I DATI DELLA VALORE ASSET ORIGINARIA DELLA TRANCHE
                // COB_CODE: IF  TRANCHE-NEGATIVA
                //           AND VAS-TP-VAL-AST = VALORE-NEGATIVO
                //              END-IF
                //           END-IF
                if (ws.getFlagTpTrch().isNegativa() && Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValoreNegativo())) {
                    // COB_CODE: IF VAS-NUM-QUO-NULL NOT = HIGH-VALUES
                    //                TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted())) {
                        // COB_CODE: MOVE VAS-NUM-QUO
                        //             TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                        lccc0450.getLccc0450TabMatrice().setNumQuoteIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getValAst().getVasNumQuo().getVasNumQuo(), 12, 5));
                    }
                    // COB_CODE: IF VAS-VAL-QUO-NULL NOT = HIGH-VALUES
                    //                TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                    //           ELSE
                    //                TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getValAst().getVasValQuo().getVasValQuoNullFormatted())) {
                        // COB_CODE: MOVE VAS-VAL-QUO
                        //             TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                        lccc0450.getLccc0450TabMatrice().setValQuotaIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getValAst().getVasValQuo().getVasValQuo(), 15, 3));
                    }
                    else {
                        // COB_CODE: MOVE L41-VAL-QUO
                        //             TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                        lccc0450.getLccc0450TabMatrice().setValQuotaIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getQuotzAggFnd().getValQuo(), 15, 3));
                    }
                    // COB_CODE: IF VAS-DT-VALZZ-NULL NOT = HIGH-VALUES
                    //                TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getValAst().getVasDtValzz().getVasDtValzzNullFormatted())) {
                        // COB_CODE: MOVE VAS-DT-VALZZ
                        //             TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                        lccc0450.getLccc0450TabMatrice().setDtValQuote(ws.getIndici().getTabTot(), TruncAbs.toInt(ws.getValAst().getVasDtValzz().getVasDtValzz(), 8));
                    }
                }
                //-->        VALORIZZO IL CAMPO TRANCHE_DI GARANZIA
                // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                //             TO LCCC0450-ID-TRCH-DI-GAR(IX-TAB-TOT)
                lccc0450.getLccc0450TabMatrice().setIdTrchDiGar(ws.getIndici().getTabTot(), wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
                // COB_CODE:             IF VAS-ID-RICH-INVST-FND-NULL NOT = HIGH-VALUES
                //           *-->           RECUPERO LA RICHIESTA DI INVESTIMENTO
                //                        END-IF
                //                       END-IF
                if (!Characters.EQ_HIGH.test(ws.getValAst().getVasIdRichInvstFnd().getVasIdRichInvstFndNullFormatted())) {
                    //-->           RECUPERO LA RICHIESTA DI INVESTIMENTO
                    // COB_CODE: IF VAS-ID-RICH-INVST-FND > 0
                    //             END-IF
                    //           END-IF
                    if (ws.getValAst().getVasIdRichInvstFnd().getVasIdRichInvstFnd() > 0) {
                        // COB_CODE: PERFORM A220-RECUP-IMP-INVES
                        //              THRU A220-EX
                        a220RecupImpInves();
                        // COB_CODE:                IF IDSV0001-ESITO-OK
                        //           *-->              SALVO I DATI DELLA RICHIESTA DI INVESTIMENTO
                        //           *-->              ORIGINARIA DELLA TRANCHE
                        //                                THRU CALCOLA-NUM-QUOTE-EX
                        //                          END-IF
                        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                            //-->              SALVO I DATI DELLA RICHIESTA DI INVESTIMENTO
                            //-->              ORIGINARIA DELLA TRANCHE
                            // COB_CODE:                   IF  TRANCHE-POSITIVA
                            //                             AND VAS-TP-VAL-AST = VALORE-POSITIVO
                            //           *-->                  VALORIZZAZIONE PERCENTUALE DI INVESTIMENTO
                            //                                 END-IF
                            //                             END-IF
                            if (ws.getFlagTpTrch().isPositiva() && Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValorePositivo())) {
                                //-->                  VALORIZZAZIONE PERCENTUALE DI INVESTIMENTO
                                // COB_CODE: IF RIF-PC-NULL NOT = HIGH-VALUES
                                //                TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                                //           END-IF
                                if (!Characters.EQ_HIGH.test(ws.getRichInvstFnd().getRifPc().getRifPcNullFormatted())) {
                                    // COB_CODE: MOVE RIF-PC
                                    //             TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                                    lccc0450.getLccc0450TabMatrice().setPercentInv(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getRichInvstFnd().getRifPc().getRifPc(), 6, 3));
                                }
                                // COB_CODE: IF RIF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                                //                TO LCCC0450-IMP-INVES(IX-TAB-TOT)
                                //           END-IF
                                if (!Characters.EQ_HIGH.test(ws.getRichInvstFnd().getRifImpMovto().getRifImpMovtoNullFormatted())) {
                                    // COB_CODE: MOVE RIF-IMP-MOVTO
                                    //             TO LCCC0450-IMP-INVES(IX-TAB-TOT)
                                    lccc0450.getLccc0450TabMatrice().setImpInves(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getRichInvstFnd().getRifImpMovto().getRifImpMovto(), 15, 3));
                                }
                            }
                            // COB_CODE: IF  (VAS-NUM-QUO-NULL = HIGH-VALUES OR ZERO)
                            //           AND L41-VAL-QUO > 0
                            //              END-IF
                            //           ELSE
                            //              END-IF
                            //           END-IF
                            if ((Characters.EQ_HIGH.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted()) || Characters.EQ_ZERO.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted())) && ws.getQuotzAggFnd().getValQuo().compareTo(0) > 0) {
                                // COB_CODE: IF RIF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                                //                  RIF-IMP-MOVTO / L41-VAL-QUO
                                //           ELSE
                                //                TO WK-NUMERO-QUOTE
                                //           END-IF
                                if (!Characters.EQ_HIGH.test(ws.getRichInvstFnd().getRifImpMovto().getRifImpMovtoNullFormatted())) {
                                    // COB_CODE: COMPUTE WK-NUMERO-QUOTE ROUNDED =
                                    //               RIF-IMP-MOVTO / L41-VAL-QUO
                                    ws.getVariabili().setWkNumeroQuote(Trunc.toDecimal(MathUtil.convertRoundDecimal((new AfDecimal((ws.getRichInvstFnd().getRifImpMovto().getRifImpMovto().divide(ws.getQuotzAggFnd().getValQuo())), 24, 6)), 5, RoundingMode.ROUND_UP, 31, 5), 12, 5));
                                }
                                else {
                                    // COB_CODE: MOVE ZEROES
                                    //             TO WK-NUMERO-QUOTE
                                    ws.getVariabili().setWkNumeroQuote(new AfDecimal(0, 12, 5));
                                }
                            }
                            else if (Functions.isNumber(ws.getValAst().getVasNumQuo().getVasNumQuo())) {
                                // COB_CODE: IF VAS-NUM-QUO IS NUMERIC
                                //                TO WK-NUMERO-QUOTE
                                //           ELSE
                                //                TO WK-NUMERO-QUOTE
                                //           END-IF
                                // COB_CODE: MOVE VAS-NUM-QUO
                                //             TO WK-NUMERO-QUOTE
                                ws.getVariabili().setWkNumeroQuote(Trunc.toDecimal(ws.getValAst().getVasNumQuo().getVasNumQuo(), 12, 5));
                            }
                            else {
                                // COB_CODE: MOVE ZEROES
                                //             TO WK-NUMERO-QUOTE
                                ws.getVariabili().setWkNumeroQuote(new AfDecimal(0, 12, 5));
                            }
                            // COB_CODE: PERFORM CALCOLA-NUM-QUOTE
                            //              THRU CALCOLA-NUM-QUOTE-EX
                            calcolaNumQuote();
                        }
                    }
                }
                // COB_CODE:             IF VAS-ID-RICH-DIS-FND-NULL NOT = HIGH-VALUES
                //           *-->           RECUPERO LA RICHIESTA DI DISINVESTIMENTO
                //                          END-IF
                //                       END-IF
                if (!Characters.EQ_HIGH.test(ws.getValAst().getVasIdRichDisFnd().getVasIdRichDisFndNullFormatted())) {
                    //-->           RECUPERO LA RICHIESTA DI DISINVESTIMENTO
                    // COB_CODE: PERFORM A230-RECUP-IMP-DISIN
                    //              THRU A230-EX
                    a230RecupImpDisin();
                    // COB_CODE:                IF IDSV0001-ESITO-OK
                    //           *-->              SALVO I DATI DELLA RICHIESTA DI DISINVESTIMENTO
                    //           *-->              ORIGINARIA DELLA TRANCHE
                    //                                THRU CALCOLA-NUM-QUOTE-EX
                    //                          END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        //-->              SALVO I DATI DELLA RICHIESTA DI DISINVESTIMENTO
                        //-->              ORIGINARIA DELLA TRANCHE
                        // COB_CODE:                   IF  TRANCHE-NEGATIVA
                        //                             AND VAS-TP-VAL-AST = VALORE-NEGATIVO
                        //           *-->                 VALORIZZAZIONE PERCENTUALE DI DISINVESTIMENTO
                        //                                 END-IF
                        //                             END-IF
                        if (ws.getFlagTpTrch().isNegativa() && Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValoreNegativo())) {
                            //-->                 VALORIZZAZIONE PERCENTUALE DI DISINVESTIMENTO
                            // COB_CODE: IF RDF-PC-NULL NOT = HIGH-VALUES
                            //                TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                            //           END-IF
                            if (!Characters.EQ_HIGH.test(ws.getRichDisFnd().getRdfPc().getRdfPcNullFormatted())) {
                                // COB_CODE: MOVE RDF-PC
                                //             TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                                lccc0450.getLccc0450TabMatrice().setPercentInv(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getRichDisFnd().getRdfPc().getRdfPc(), 6, 3));
                            }
                            // COB_CODE: IF RDF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                            //                TO LCCC0450-IMP-DISINV(IX-TAB-TOT)
                            //           END-IF
                            if (!Characters.EQ_HIGH.test(ws.getRichDisFnd().getRdfImpMovto().getRdfImpMovtoNullFormatted())) {
                                // COB_CODE: MOVE RDF-IMP-MOVTO
                                //             TO LCCC0450-IMP-DISINV(IX-TAB-TOT)
                                lccc0450.getLccc0450TabMatrice().setImpDisinv(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getRichDisFnd().getRdfImpMovto().getRdfImpMovto(), 15, 3));
                            }
                        }
                        //-->              EFFETTUO UNA MODIFICA AL NUMERO QUOTE SOLO SE
                        //-->              IL TIPO VALORE ASSET E' VALORE NEGATIVO
                        //-->              OPPURE SE E' DI LIQUIDAZIONE DOBBIAMO
                        //-->              CONSIDERARLA SOLO SE LO STATO DELLA RICHIESTA
                        //-->              DI DISINVESTIMENTO ASSOCIATA E' CONCLUSO
                        // COB_CODE:                   IF (VAS-NUM-QUO-NULL = HIGH-VALUES OR ZERO)
                        //                             AND (L41-VAL-QUO > 0)
                        //           *-->              EFFETTUO UNA MODIFICA AL NUMERO QUOTE SOLO SE
                        //           *-->              IL TIPO VALORE ASSET E' VALORE NEGATIVO
                        //           *-->              OPPURE SE E' DI LIQUIDAZIONE DOBBIAMO
                        //           *-->              CONSIDERARLA SOLO SE LO STATO DELLA RICHIESTA
                        //           *-->              DI DISINVESTIMENTO ASSOCIATA E' CONCLUSO
                        //                             AND ((VAS-TP-VAL-AST = VALORE-NEGATIVO
                        //                              OR   VAS-TP-VAL-AST = ANNULLO-POSITIVO
                        //                              OR   VAS-TP-VAL-AST = ANNULLO-LIQUIDAZIONE)
                        //                              OR  (VAS-TP-VAL-AST = VAS-LIQUIDAZIONE
                        //                             AND   RDF-TP-STAT = 'CL'))
                        //                                 END-IF
                        //                             ELSE
                        //                                END-IF
                        //                             END-IF
                        if ((Characters.EQ_HIGH.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted()) || Characters.EQ_ZERO.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted())) && ws.getQuotzAggFnd().getValQuo().compareTo(0) > 0 && (Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValoreNegativo()) || Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getAnnulloPositivo()) || Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getAnnulloLiquidazione()) || Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getVasLiquidazione()) && Conditions.eq(ws.getRichDisFnd().getRdfTpStat(), "CL"))) {
                            // COB_CODE: IF RDF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                            //                  RDF-IMP-MOVTO / L41-VAL-QUO
                            //           ELSE
                            //                TO WK-NUMERO-QUOTE
                            //           END-IF
                            if (!Characters.EQ_HIGH.test(ws.getRichDisFnd().getRdfImpMovto().getRdfImpMovtoNullFormatted())) {
                                // COB_CODE: COMPUTE WK-NUMERO-QUOTE ROUNDED =
                                //               RDF-IMP-MOVTO / L41-VAL-QUO
                                ws.getVariabili().setWkNumeroQuote(Trunc.toDecimal(MathUtil.convertRoundDecimal((new AfDecimal((ws.getRichDisFnd().getRdfImpMovto().getRdfImpMovto().divide(ws.getQuotzAggFnd().getValQuo())), 24, 6)), 5, RoundingMode.ROUND_UP, 31, 5), 12, 5));
                            }
                            else {
                                // COB_CODE: MOVE ZEROES
                                //             TO WK-NUMERO-QUOTE
                                ws.getVariabili().setWkNumeroQuote(new AfDecimal(0, 12, 5));
                            }
                        }
                        else if (Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getVasLiquidazione()) && !Conditions.eq(ws.getRichDisFnd().getRdfTpStat(), "CL")) {
                            // COB_CODE: IF  (VAS-TP-VAL-AST = VAS-LIQUIDAZIONE)
                            //           AND (RDF-TP-STAT NOT = 'CL')
                            //                 TO WK-NUMERO-QUOTE
                            //           ELSE
                            //               END-IF
                            //           END-IF
                            // COB_CODE: MOVE ZEROES
                            //             TO WK-NUMERO-QUOTE
                            ws.getVariabili().setWkNumeroQuote(new AfDecimal(0, 12, 5));
                        }
                        else if (Functions.isNumber(ws.getValAst().getVasNumQuo().getVasNumQuo())) {
                            // COB_CODE: IF VAS-NUM-QUO IS NUMERIC
                            //                TO WK-NUMERO-QUOTE
                            //           ELSE
                            //                TO WK-NUMERO-QUOTE
                            //           END-IF
                            // COB_CODE: MOVE VAS-NUM-QUO
                            //             TO WK-NUMERO-QUOTE
                            ws.getVariabili().setWkNumeroQuote(Trunc.toDecimal(ws.getValAst().getVasNumQuo().getVasNumQuo(), 12, 5));
                        }
                        else {
                            // COB_CODE: MOVE ZEROES
                            //             TO WK-NUMERO-QUOTE
                            ws.getVariabili().setWkNumeroQuote(new AfDecimal(0, 12, 5));
                        }
                        // COB_CODE: PERFORM CALCOLA-NUM-QUOTE
                        //              THRU CALCOLA-NUM-QUOTE-EX
                        calcolaNumQuote();
                    }
                }
                // COB_CODE:             IF IDSV0001-ESITO-OK
                //           *-->           VALORIZZO IL CAMPO CONTROVALORE
                //                          MOVE IX-TAB-TOT      TO LCCC0450-ELE-TAB-MAX
                //                       END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    //-->           VALORIZZO IL CAMPO CONTROVALORE
                    // COB_CODE: MOVE ZERO             TO WK-CONTROVALORE
                    ws.getVariabili().setWkControvalore(new AfDecimal(0, 15, 3));
                    // COB_CODE: COMPUTE WK-CONTROVALORE                    =
                    //                  (LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT) *
                    //                   LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)  )
                    ws.getVariabili().setWkControvalore(Trunc.toDecimal(lccc0450.getLccc0450TabMatrice().getNumQuoteIni(ws.getIndici().getTabTot()).multiply(lccc0450.getLccc0450TabMatrice().getValQuotaIni(ws.getIndici().getTabTot())), 15, 3));
                    // COB_CODE: COMPUTE LCCC0450-CONTROVALORE (IX-TAB-TOT) =
                    //                   LCCC0450-CONTROVALORE (IX-TAB-TOT) +
                    //                   WK-CONTROVALORE
                    lccc0450.getLccc0450TabMatrice().setControvalore(ws.getIndici().getTabTot(), Trunc.toDecimal(lccc0450.getLccc0450TabMatrice().getControvalore(ws.getIndici().getTabTot()).add(ws.getVariabili().getWkControvalore()), 15, 3));
                    // COB_CODE: MOVE IX-TAB-TOT      TO LCCC0450-ELE-TAB-MAX
                    lccc0450.setLccc0450EleTabMax(((short)(ws.getIndici().getTabTot())));
                }
            }
        }
        else {
            // COB_CODE: MOVE IX-TAB-MAT             TO IX-TAB-TOT
            ws.getIndici().setTabTot(ws.getIndici().getTabMat());
            //-->    SALVO I DATI DELLA VALORE ASSET ORIGINARIA DELLA TRANCHE
            // COB_CODE: IF  TRANCHE-POSITIVA
            //           AND VAS-TP-VAL-AST = VALORE-POSITIVO
            //              END-IF
            //           END-IF
            if (ws.getFlagTpTrch().isPositiva() && Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValorePositivo())) {
                // COB_CODE: IF VAS-NUM-QUO-NULL NOT = HIGH-VALUES
                //                TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted())) {
                    // COB_CODE: MOVE VAS-NUM-QUO
                    //             TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                    lccc0450.getLccc0450TabMatrice().setNumQuoteIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getValAst().getVasNumQuo().getVasNumQuo(), 12, 5));
                }
                // COB_CODE: IF VAS-VAL-QUO-NULL NOT = HIGH-VALUES
                //                TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                //           ELSE
                //                TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getValAst().getVasValQuo().getVasValQuoNullFormatted())) {
                    // COB_CODE: MOVE VAS-VAL-QUO
                    //             TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                    lccc0450.getLccc0450TabMatrice().setValQuotaIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getValAst().getVasValQuo().getVasValQuo(), 15, 3));
                }
                else {
                    // COB_CODE: MOVE L41-VAL-QUO
                    //             TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                    lccc0450.getLccc0450TabMatrice().setValQuotaIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getQuotzAggFnd().getValQuo(), 15, 3));
                }
                // COB_CODE: IF VAS-DT-VALZZ-NULL NOT = HIGH-VALUES
                //                TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getValAst().getVasDtValzz().getVasDtValzzNullFormatted())) {
                    // COB_CODE: MOVE VAS-DT-VALZZ
                    //             TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                    lccc0450.getLccc0450TabMatrice().setDtValQuote(ws.getIndici().getTabTot(), TruncAbs.toInt(ws.getValAst().getVasDtValzz().getVasDtValzz(), 8));
                }
            }
            //-->    SALVO I DATI DELLA VALORE ASSET ORIGINARIA DELLA TRANCHE
            // COB_CODE: IF  TRANCHE-NEGATIVA
            //           AND VAS-TP-VAL-AST = VALORE-NEGATIVO
            //              END-IF
            //           END-IF
            if (ws.getFlagTpTrch().isNegativa() && Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValoreNegativo())) {
                // COB_CODE: IF VAS-NUM-QUO-NULL NOT = HIGH-VALUES
                //                TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted())) {
                    // COB_CODE: MOVE VAS-NUM-QUO
                    //             TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                    lccc0450.getLccc0450TabMatrice().setNumQuoteIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getValAst().getVasNumQuo().getVasNumQuo(), 12, 5));
                }
                // COB_CODE: IF VAS-VAL-QUO-NULL NOT = HIGH-VALUES
                //                TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                //           ELSE
                //                TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getValAst().getVasValQuo().getVasValQuoNullFormatted())) {
                    // COB_CODE: MOVE VAS-VAL-QUO
                    //             TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                    lccc0450.getLccc0450TabMatrice().setValQuotaIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getValAst().getVasValQuo().getVasValQuo(), 15, 3));
                }
                else {
                    // COB_CODE: MOVE L41-VAL-QUO
                    //             TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                    lccc0450.getLccc0450TabMatrice().setValQuotaIni(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getQuotzAggFnd().getValQuo(), 15, 3));
                }
                // COB_CODE: IF VAS-DT-VALZZ-NULL NOT = HIGH-VALUES
                //                TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getValAst().getVasDtValzz().getVasDtValzzNullFormatted())) {
                    // COB_CODE: MOVE VAS-DT-VALZZ
                    //             TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                    lccc0450.getLccc0450TabMatrice().setDtValQuote(ws.getIndici().getTabTot(), TruncAbs.toInt(ws.getValAst().getVasDtValzz().getVasDtValzz(), 8));
                }
            }
            // COB_CODE: IF VAS-ID-RICH-INVST-FND-NULL NOT = HIGH-VALUES
            //            END-IF
            //           END-IF
            if (!Characters.EQ_HIGH.test(ws.getValAst().getVasIdRichInvstFnd().getVasIdRichInvstFndNullFormatted())) {
                // COB_CODE: IF VAS-ID-RICH-INVST-FND > 0
                //             END-IF
                //           END-IF
                if (ws.getValAst().getVasIdRichInvstFnd().getVasIdRichInvstFnd() > 0) {
                    // COB_CODE:            IF VAS-NUM-QUO-NULL = HIGH-VALUES OR ZERO
                    //           *-->          RECUPERO LA RICHIESTA DI INVESTIMENTO
                    //                         END-IF
                    //                      ELSE
                    //                           TO WK-NUMERO-QUOTE
                    //                      END-IF
                    if (Characters.EQ_HIGH.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted()) || Characters.EQ_ZERO.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted())) {
                        //-->          RECUPERO LA RICHIESTA DI INVESTIMENTO
                        // COB_CODE: PERFORM A220-RECUP-IMP-INVES
                        //              THRU A220-EX
                        a220RecupImpInves();
                        // COB_CODE: MOVE ZEROES
                        //             TO WK-NUMERO-QUOTE
                        ws.getVariabili().setWkNumeroQuote(new AfDecimal(0, 12, 5));
                        // COB_CODE:               IF IDSV0001-ESITO-OK
                        //           *-->             SALVO I DATI DELLA RICHIESTA DI INVESTIMENTO
                        //           *-->             ORIGINARIA DELLA TRANCHE
                        //                            END-IF
                        //                         END-IF
                        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                            //-->             SALVO I DATI DELLA RICHIESTA DI INVESTIMENTO
                            //-->             ORIGINARIA DELLA TRANCHE
                            // COB_CODE:                  IF  TRANCHE-POSITIVA
                            //                            AND VAS-TP-VAL-AST = VALORE-POSITIVO
                            //           *-->                 VALORIZZAZIONE PERCENTUALE DI INVESTIMENTO
                            //                                END-IF
                            //                            END-IF
                            if (ws.getFlagTpTrch().isPositiva() && Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValorePositivo())) {
                                //-->                 VALORIZZAZIONE PERCENTUALE DI INVESTIMENTO
                                // COB_CODE: IF RIF-PC-NULL NOT = HIGH-VALUES
                                //                TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                                //           END-IF
                                if (!Characters.EQ_HIGH.test(ws.getRichInvstFnd().getRifPc().getRifPcNullFormatted())) {
                                    // COB_CODE: MOVE RIF-PC
                                    //             TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                                    lccc0450.getLccc0450TabMatrice().setPercentInv(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getRichInvstFnd().getRifPc().getRifPc(), 6, 3));
                                }
                                // COB_CODE: IF RIF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                                //                TO LCCC0450-IMP-INVES(IX-TAB-TOT)
                                //           END-IF
                                if (!Characters.EQ_HIGH.test(ws.getRichInvstFnd().getRifImpMovto().getRifImpMovtoNullFormatted())) {
                                    // COB_CODE: MOVE RIF-IMP-MOVTO
                                    //             TO LCCC0450-IMP-INVES(IX-TAB-TOT)
                                    lccc0450.getLccc0450TabMatrice().setImpInves(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getRichInvstFnd().getRifImpMovto().getRifImpMovto(), 15, 3));
                                }
                            }
                            // COB_CODE: PERFORM A250-RECUP-QTZ-AGG
                            //              THRU A250-EX
                            a250RecupQtzAgg();
                            // COB_CODE: IF IDSV0001-ESITO-OK
                            //           AND QTZ-AGG-OK
                            //           AND L41-VAL-QUO > 0
                            //               END-IF
                            //           END-IF
                            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getFlagQtzAgg().isOk() && ws.getQuotzAggFnd().getValQuo().compareTo(0) > 0) {
                                // COB_CODE: IF RIF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                                //                  RIF-IMP-MOVTO / L41-VAL-QUO
                                //           END-IF
                                if (!Characters.EQ_HIGH.test(ws.getRichInvstFnd().getRifImpMovto().getRifImpMovtoNullFormatted())) {
                                    // COB_CODE: COMPUTE WK-NUMERO-QUOTE ROUNDED =
                                    //               RIF-IMP-MOVTO / L41-VAL-QUO
                                    ws.getVariabili().setWkNumeroQuote(Trunc.toDecimal(MathUtil.convertRoundDecimal((new AfDecimal((ws.getRichInvstFnd().getRifImpMovto().getRifImpMovto().divide(ws.getQuotzAggFnd().getValQuo())), 24, 6)), 5, RoundingMode.ROUND_UP, 31, 5), 12, 5));
                                }
                            }
                        }
                    }
                    else {
                        // COB_CODE: MOVE VAS-NUM-QUO
                        //             TO WK-NUMERO-QUOTE
                        ws.getVariabili().setWkNumeroQuote(Trunc.toDecimal(ws.getValAst().getVasNumQuo().getVasNumQuo(), 12, 5));
                    }
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //                 THRU CALCOLA-NUM-QUOTE-EX
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: PERFORM CALCOLA-NUM-QUOTE
                        //              THRU CALCOLA-NUM-QUOTE-EX
                        calcolaNumQuote();
                    }
                }
            }
            // COB_CODE: IF VAS-ID-RICH-DIS-FND-NULL NOT = HIGH-VALUES
            //              END-IF
            //           END-IF
            if (!Characters.EQ_HIGH.test(ws.getValAst().getVasIdRichDisFnd().getVasIdRichDisFndNullFormatted())) {
                // COB_CODE:            IF VAS-NUM-QUO-NULL = HIGH-VALUES OR ZERO
                //           *-->          RECUPERO LA RICHIESTA DI DISINVESTIMENTO
                //                         END-IF
                //                      ELSE
                //                           TO WK-NUMERO-QUOTE
                //                      END-IF
                if (Characters.EQ_HIGH.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted()) || Characters.EQ_ZERO.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted())) {
                    //-->          RECUPERO LA RICHIESTA DI DISINVESTIMENTO
                    // COB_CODE: PERFORM A230-RECUP-IMP-DISIN
                    //              THRU A230-EX
                    a230RecupImpDisin();
                    // COB_CODE: MOVE ZEROES
                    //             TO WK-NUMERO-QUOTE
                    ws.getVariabili().setWkNumeroQuote(new AfDecimal(0, 12, 5));
                    // COB_CODE:               IF IDSV0001-ESITO-OK
                    //           *-->             SALVO I DATI DELLA RICHIESTA DI DISINVESTIMENTO
                    //           *-->             ORIGINARIA DELLA TRANCHE
                    //                            END-IF
                    //                         END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        //-->             SALVO I DATI DELLA RICHIESTA DI DISINVESTIMENTO
                        //-->             ORIGINARIA DELLA TRANCHE
                        // COB_CODE:                  IF  TRANCHE-NEGATIVA
                        //                            AND VAS-TP-VAL-AST = VALORE-NEGATIVO
                        //           *-->                 VALORIZZAZIONE PERCENTUALE DI DISINVESTIMENTO
                        //                                END-IF
                        //                            END-IF
                        if (ws.getFlagTpTrch().isNegativa() && Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValoreNegativo())) {
                            //-->                 VALORIZZAZIONE PERCENTUALE DI DISINVESTIMENTO
                            // COB_CODE: IF RDF-PC-NULL NOT = HIGH-VALUES
                            //                TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                            //           END-IF
                            if (!Characters.EQ_HIGH.test(ws.getRichDisFnd().getRdfPc().getRdfPcNullFormatted())) {
                                // COB_CODE: MOVE RDF-PC
                                //             TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                                lccc0450.getLccc0450TabMatrice().setPercentInv(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getRichDisFnd().getRdfPc().getRdfPc(), 6, 3));
                            }
                            // COB_CODE: IF RDF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                            //                TO LCCC0450-IMP-DISINV(IX-TAB-TOT)
                            //           END-IF
                            if (!Characters.EQ_HIGH.test(ws.getRichDisFnd().getRdfImpMovto().getRdfImpMovtoNullFormatted())) {
                                // COB_CODE: MOVE RDF-IMP-MOVTO
                                //             TO LCCC0450-IMP-DISINV(IX-TAB-TOT)
                                lccc0450.getLccc0450TabMatrice().setImpDisinv(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getRichDisFnd().getRdfImpMovto().getRdfImpMovto(), 15, 3));
                            }
                        }
                        // COB_CODE: PERFORM A250-RECUP-QTZ-AGG
                        //              THRU A250-EX
                        a250RecupQtzAgg();
                        //-->             EFFETTUO UNA MODIFICA AL NUMERO QUOTE SOLO SE
                        //-->             IL TIPO VALORE ASSET E' VALORE NEGATIVO
                        //-->             OPPURE SE E' DI LIQUIDAZIONE DOBBIAMO
                        //-->             CONSIDERARLA SOLO SE LO STATO DELLA RICHIESTA
                        //-->             DI DISINVESTIMENTO ASSOCIATA E' CONCLUSO
                        // COB_CODE:                  IF IDSV0001-ESITO-OK
                        //                            AND QTZ-AGG-OK
                        //                            AND L41-VAL-QUO > 0
                        //           *-->             EFFETTUO UNA MODIFICA AL NUMERO QUOTE SOLO SE
                        //           *-->             IL TIPO VALORE ASSET E' VALORE NEGATIVO
                        //           *-->             OPPURE SE E' DI LIQUIDAZIONE DOBBIAMO
                        //           *-->             CONSIDERARLA SOLO SE LO STATO DELLA RICHIESTA
                        //           *-->             DI DISINVESTIMENTO ASSOCIATA E' CONCLUSO
                        //                            AND ((VAS-TP-VAL-AST = VALORE-NEGATIVO
                        //                             OR   VAS-TP-VAL-AST = ANNULLO-POSITIVO
                        //                             OR   VAS-TP-VAL-AST = ANNULLO-LIQUIDAZIONE)
                        //                             OR  (VAS-TP-VAL-AST = VAS-LIQUIDAZIONE
                        //                            AND   RDF-TP-STAT = 'CL'))
                        //                                END-IF
                        //                            END-IF
                        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getFlagQtzAgg().isOk() && ws.getQuotzAggFnd().getValQuo().compareTo(0) > 0 && (Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValoreNegativo()) || Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getAnnulloPositivo()) || Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getAnnulloLiquidazione()) || Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getVasLiquidazione()) && Conditions.eq(ws.getRichDisFnd().getRdfTpStat(), "CL"))) {
                            // COB_CODE: IF RDF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                            //                   RDF-IMP-MOVTO / L41-VAL-QUO
                            //           END-IF
                            if (!Characters.EQ_HIGH.test(ws.getRichDisFnd().getRdfImpMovto().getRdfImpMovtoNullFormatted())) {
                                // COB_CODE: COMPUTE WK-NUMERO-QUOTE ROUNDED =
                                //               RDF-IMP-MOVTO / L41-VAL-QUO
                                ws.getVariabili().setWkNumeroQuote(Trunc.toDecimal(MathUtil.convertRoundDecimal((new AfDecimal((ws.getRichDisFnd().getRdfImpMovto().getRdfImpMovto().divide(ws.getQuotzAggFnd().getValQuo())), 24, 6)), 5, RoundingMode.ROUND_UP, 31, 5), 12, 5));
                            }
                        }
                    }
                }
                else {
                    // COB_CODE: MOVE VAS-NUM-QUO
                    //             TO WK-NUMERO-QUOTE
                    ws.getVariabili().setWkNumeroQuote(Trunc.toDecimal(ws.getValAst().getVasNumQuo().getVasNumQuo(), 12, 5));
                }
                // COB_CODE: IF IDSV0001-ESITO-OK
                //                 THRU CALCOLA-NUM-QUOTE-EX
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM CALCOLA-NUM-QUOTE
                    //              THRU CALCOLA-NUM-QUOTE-EX
                    calcolaNumQuote();
                }
            }
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       VALORIZZO IL CAMPO CONTROVALORE
            //                              WK-CONTROVALORE
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->       VALORIZZO IL CAMPO CONTROVALORE
                // COB_CODE: MOVE ZERO             TO WK-CONTROVALORE
                ws.getVariabili().setWkControvalore(new AfDecimal(0, 15, 3));
                // COB_CODE: COMPUTE WK-CONTROVALORE                    =
                //                  (LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT) *
                //                   LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)  )
                ws.getVariabili().setWkControvalore(Trunc.toDecimal(lccc0450.getLccc0450TabMatrice().getNumQuoteIni(ws.getIndici().getTabTot()).multiply(lccc0450.getLccc0450TabMatrice().getValQuotaIni(ws.getIndici().getTabTot())), 15, 3));
                // COB_CODE: COMPUTE LCCC0450-CONTROVALORE (IX-TAB-TOT) =
                //                   LCCC0450-CONTROVALORE (IX-TAB-TOT) +
                //                   WK-CONTROVALORE
                lccc0450.getLccc0450TabMatrice().setControvalore(ws.getIndici().getTabTot(), Trunc.toDecimal(lccc0450.getLccc0450TabMatrice().getControvalore(ws.getIndici().getTabTot()).add(ws.getVariabili().getWkControvalore()), 15, 3));
            }
            //-->    VALORIZZA LA VARIABILE VALQUOTET
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU A190-EX
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM A190-VALORIZZA-VALQUOTET
                //              THRU A190-EX
                a190ValorizzaValquotet();
            }
        }
    }

    /**Original name: CALCOLA-NUM-QUOTE<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    private void calcolaNumQuote() {
        // COB_CODE: IF VAS-TP-VAL-AST = VALORE-POSITIVO
        //           OR VAS-TP-VAL-AST = ANNULLO-NEGATIVO
        //           OR VAS-TP-VAL-AST = ANNULLO-LIQUIDAZIONE
        //               TO LCCC0450-NUM-QUOTE(IX-TAB-TOT)
        //           ELSE
        //              END-IF
        //           END-IF.
        if (Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValorePositivo()) || Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getAnnulloNegativo()) || Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getAnnulloLiquidazione())) {
            // COB_CODE: ADD WK-NUMERO-QUOTE
            //            TO LCCC0450-NUM-QUOTE(IX-TAB-TOT)
            lccc0450.getLccc0450TabMatrice().setNumQuote(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getVariabili().getWkNumeroQuote().add(lccc0450.getLccc0450TabMatrice().getNumQuote(ws.getIndici().getTabTot())), 12, 5));
        }
        else if (Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getValoreNegativo()) || Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getAnnulloPositivo()) || Conditions.eq(ws.getValAst().getVasTpValAst(), ws.getVasLiquidazione())) {
            // COB_CODE: IF VAS-TP-VAL-AST = VALORE-NEGATIVO
            //           OR VAS-TP-VAL-AST = ANNULLO-POSITIVO
            //           OR VAS-TP-VAL-AST = VAS-LIQUIDAZIONE
            //                  FROM LCCC0450-NUM-QUOTE(IX-TAB-TOT)
            //           END-IF
            // COB_CODE: SUBTRACT WK-NUMERO-QUOTE
            //               FROM LCCC0450-NUM-QUOTE(IX-TAB-TOT)
            lccc0450.getLccc0450TabMatrice().setNumQuote(ws.getIndici().getTabTot(), Trunc.toDecimal(lccc0450.getLccc0450TabMatrice().getNumQuote(ws.getIndici().getTabTot()).subtract(ws.getVariabili().getWkNumeroQuote()), 12, 5));
        }
    }

    /**Original name: A190-VALORIZZA-VALQUOTET<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA LA VARIABILE VALQUOTET
	 * ----------------------------------------------------------------*
	 * --> Calcolo della variabile VALQUOTET:
	 * --> Calcolare per ogni fondo legato alla Tranche e
	 * --> presente sulla Valore Asset (fondi con numero quote > 0)
	 *     IF LCCC0450-NUM-QUOTE(IX-TAB-TOT) > 0</pre>*/
    private void a190ValorizzaValquotet() {
        // COB_CODE:      IF LCCC0450-NUM-QUOTE(IX-TAB-TOT) NOT = 0
        //           *-->    Calcolo prima la Data Ricorrenza Anniversaria di Tranche
        //           *-->    (utilizzata per reperire valore quote al tempo T)
        //                   END-IF
        //                END-IF.
        if (lccc0450.getLccc0450TabMatrice().getNumQuote(ws.getIndici().getTabTot()).compareTo(0) != 0) {
            //-->    Calcolo prima la Data Ricorrenza Anniversaria di Tranche
            //-->    (utilizzata per reperire valore quote al tempo T)
            // COB_CODE: PERFORM A191-CALCOLA-DT-RICOR-TRANCHE
            //              THRU A191-EX
            a191CalcolaDtRicorTranche();
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       Valore delle quote dalla tabella QUOTAZIONE_FONDI_UNIT
            //           *-->       in T, ossia alla data di ricorrenza anniversaria
            //           *-->       (ricorrenza calcolata rispetto alla data
            //           *-->       calcolo Riserva).
            //                      END-IF
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->       Valore delle quote dalla tabella QUOTAZIONE_FONDI_UNIT
                //-->       in T, ossia alla data di ricorrenza anniversaria
                //-->       (ricorrenza calcolata rispetto alla data
                //-->       calcolo Riserva).
                // COB_CODE: PERFORM A160-RECUP-QUOTAZ-FONDO
                //              THRU A160-EX
                a160RecupQuotazFondo();
                // COB_CODE:            IF IDSV0001-ESITO-OK
                //           *-->          VALORIZZO IL CAMPO VALORE QUOTA T
                //                         END-IF
                //                      END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    //-->          VALORIZZO IL CAMPO VALORE QUOTA T
                    // COB_CODE: IF L19-VAL-QUO-NULL = HIGH-VALUES
                    //                TO LCCC0450-VAL-QUOTA-T(IX-TAB-TOT)
                    //           ELSE
                    //                TO LCCC0450-VAL-QUOTA-T(IX-TAB-TOT)
                    //           END-IF
                    if (Characters.EQ_HIGH.test(ws.getQuotzFndUnit().getL19ValQuo().getL19ValQuoNullFormatted())) {
                        // COB_CODE: MOVE ZEROES
                        //             TO LCCC0450-VAL-QUOTA-T(IX-TAB-TOT)
                        lccc0450.getLccc0450TabMatrice().setValQuotaT(ws.getIndici().getTabTot(), new AfDecimal(0, 15, 3));
                    }
                    else {
                        // COB_CODE: MOVE L19-VAL-QUO
                        //             TO LCCC0450-VAL-QUOTA-T(IX-TAB-TOT)
                        lccc0450.getLccc0450TabMatrice().setValQuotaT(ws.getIndici().getTabTot(), Trunc.toDecimal(ws.getQuotzFndUnit().getL19ValQuo().getL19ValQuo(), 15, 3));
                    }
                }
            }
        }
    }

    /**Original name: A191-CALCOLA-DT-RICOR-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONVERTI VALORE DA STRINGA IN NUMERICO
	 * ----------------------------------------------------------------*</pre>*/
    private void a191CalcolaDtRicorTranche() {
        // COB_CODE: INITIALIZE           IO-A2K-LCCC0003
        //                                IN-RCODE
        initIoA2kLccc0003();
        ws.setInRcodeFormatted("00");
        //--> somma DELTA e conversione data
        // COB_CODE: MOVE '02'                        TO A2K-FUNZ
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        //--> FORMATO AAAAMMGG
        // COB_CODE: MOVE '03'                        TO A2K-INFO
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        //--> 1 ANNO
        // COB_CODE: MOVE 1                           TO A2K-DELTA
        ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)1));
        //--> ANNI
        // COB_CODE: MOVE 'A'                         TO A2K-TDELTA
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("A");
        //--> GIORNI FISSI
        // COB_CODE: MOVE '0'                         TO A2K-FISLAV
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        //--> INIZIO CONTEGGIO DA STESSO GIORNO
        // COB_CODE: MOVE 0                           TO A2K-INICON
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        //--> DATA INPUT
        //--> Data decorrenza di tranche
        // COB_CODE: MOVE WTGA-DT-DECOR(IX-TAB-TGA)
        //             TO A2K-INDATA
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata(wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtDecorFormatted());
        // COB_CODE: PERFORM CALL-ROUTINE-DATE
        //              THRU CALL-ROUTINE-DATE-EX
        callRoutineDate();
        //--> Se Data decorrenza di tranche + 1 anno e' > della Data
        //--> calcolo Riserva allora la data di ricorrenza
        //--> e' la Data decorrenza di tranche
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *-->    DATA OUTPUT (CALCOLATA)
        //                   END-IF
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //-->    DATA OUTPUT (CALCOLATA)
            // COB_CODE:         IF A2K-OUAMG > LCCC0450-DATA-RISERVA
            //                        TO WK-DT-RICOR-TRANCHE-NUM
            //                   ELSE
            //           *-->       Altrimenti calcolare data ricorrenza con gg e mm
            //           *-->       di decorrenza tranche e anno = anno di calcolo
            //           *-->       riserva
            //                      END-IF
            //                   END-IF
            if (ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamg() > lccc0450.getLccc0450DataRiserva()) {
                // COB_CODE: MOVE WTGA-DT-DECOR(IX-TAB-TGA)
                //             TO WK-DT-RICOR-TRANCHE-NUM
                ws.getVariabili().getWkDtRicorTranche().setWkDtRicorTrancheNum(TruncAbs.toInt(wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtDecor(), 8));
            }
            else {
                //-->       Altrimenti calcolare data ricorrenza con gg e mm
                //-->       di decorrenza tranche e anno = anno di calcolo
                //-->       riserva
                // COB_CODE: MOVE ZEROES
                //             TO WK-DT-RICOR-TRANCHE-NUM
                ws.getVariabili().getWkDtRicorTranche().setWkDtRicorTrancheNum(0);
                // COB_CODE: MOVE WTGA-DT-DECOR(IX-TAB-TGA)
                //             TO WK-DT-RICOR-TRANCHE-NUM
                ws.getVariabili().getWkDtRicorTranche().setWkDtRicorTrancheNum(TruncAbs.toInt(wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtDecor(), 8));
                //-->       Se la data cosl calcolata e' < = alla data di calcolo
                //-->       riserva allora impostare la data ricorrenza
                //-->       con tale data
                // COB_CODE:            IF WK-DT-RICOR-TRANCHE-NUM <= LCCC0450-DATA-RISERVA
                //                         CONTINUE
                //                      ELSE
                //           *-->          Altrimenti (se > di data calcolo riserva)
                //           *-->          sottrarre 1 anno e valorizzare la data
                //           *-->          ricorrenza con tale data
                //                           TO WK-DT-RICOR-TRANCHE-AA
                //                      END-IF
                if (ws.getVariabili().getWkDtRicorTranche().getWkDtRicorTrancheNum() <= lccc0450.getLccc0450DataRiserva()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    //-->          Altrimenti (se > di data calcolo riserva)
                    //-->          sottrarre 1 anno e valorizzare la data
                    //-->          ricorrenza con tale data
                    // COB_CODE: MOVE WK-DT-RICOR-TRANCHE-AA
                    //             TO WK-ANNO
                    ws.getVariabili().setWkAnnoFormatted(ws.getVariabili().getWkDtRicorTranche().getAaFormatted());
                    // COB_CODE: SUBTRACT 1
                    //               FROM WK-ANNO
                    ws.getVariabili().setWkAnno(Trunc.toShort(abs(ws.getVariabili().getWkAnno() - 1), 4));
                    // COB_CODE: MOVE WK-ANNO
                    //             TO WK-DT-RICOR-TRANCHE-AA
                    ws.getVariabili().getWkDtRicorTranche().setAaFormatted(ws.getVariabili().getWkAnnoFormatted());
                }
            }
        }
    }

    /**Original name: CALL-ROUTINE-DATE<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIAMATA AL SERVIZIO CALCOLA DATA
	 * ----------------------------------------------------------------*</pre>*/
    private void callRoutineDate() {
        Lccs0003 lccs0003 = null;
        GenericParam inRcode = null;
        // COB_CODE: CALL LCCS0003      USING IO-A2K-LCCC0003
        //                                    IN-RCODE
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            inRcode = new GenericParam(MarshalByteExt.strToBuffer(ws.getInRcodeFormatted(), Lccs0450Data.Len.IN_RCODE));
            lccs0003.run(ws.getIoA2kLccc0003(), inRcode);
            ws.setInRcodeFromBuffer(inRcode.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ROUTINE CALCOLO DATA (LCCS0003)'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ROUTINE CALCOLO DATA (LCCS0003)");
            // COB_CODE: MOVE 'CALL-ROUTINE-DATE'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-ROUTINE-DATE");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IN-RCODE  = '00'
        //              CONTINUE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getInRcodeFormatted().equals("00")) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CALL-ROUTINE-DATE'    TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-ROUTINE-DATE");
            // COB_CODE: MOVE '005016'               TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: A160-RECUP-QUOTAZ-FONDO<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONVERTI VALORE DA STRINGA IN NUMERICO
	 * ----------------------------------------------------------------*</pre>*/
    private void a160RecupQuotazFondo() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A160-RECUP-QUOTAZ-FONDO' TO WK-LABEL.
        ws.getVariabili().setWkLabel("A160-RECUP-QUOTAZ-FONDO");
        // COB_CODE: MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
        ws.getIdsv8888().setAreaDisplay(ws.getVariabili().getWkLabel());
        // COB_CODE: INITIALIZE QUOTZ-FND-UNIT
        //                      IDSI0011-BUFFER-DATI.
        initQuotzFndUnit();
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET  IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET  IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET  IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                          TO L19-COD-COMP-ANIA.
        ws.getQuotzFndUnit().setL19CodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE LCCC0450-COD-FONDO(IX-TAB-TOT)
        //                                          TO L19-COD-FND.
        ws.getQuotzFndUnit().setL19CodFnd(lccc0450.getLccc0450TabMatrice().getCodFondo(ws.getIndici().getTabTot()));
        // COB_CODE: MOVE WK-DT-RICOR-TRANCHE-NUM   TO L19-DT-QTZ.
        ws.getQuotzFndUnit().setL19DtQtz(ws.getVariabili().getWkDtRicorTranche().getWkDtRicorTrancheNum());
        // COB_CODE: MOVE SPACES                    TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: MOVE 'LDBS2080'                TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS2080");
        // COB_CODE: MOVE QUOTZ-FND-UNIT            TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getQuotzFndUnit().getQuotzFndUnitFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER        THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--> GESTIONE ERRORE
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                          MOVE IDSO0011-BUFFER-DATI TO QUOTZ-FND-UNIT
            //                       WHEN IDSO0011-NOT-FOUND
            //           *--->       QUOTAZIONE NON TROVATA PER IL FONDO $ ALLA DATA $
            //                          CONTINUE
            //                       WHEN OTHER
            //           *--->       ERRORE DI ACCESSO AL DB
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO QUOTZ-FND-UNIT
                    ws.getQuotzFndUnit().setQuotzFndUnitFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->       QUOTAZIONE NON TROVATA PER IL FONDO $ ALLA DATA $
                // COB_CODE: CONTINUE
                //continue
                    break;

                default://--->       ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL      TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
                    // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING PGM-LDBS2080         ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmLdbs2080Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //--> GESTIONE ERRORE
            // COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
            // COB_CODE: MOVE '005016'           TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING PGM-LDBS2080         ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmLdbs2080Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: A180-CALC-TOTALE<br>
	 * <pre>----------------------------------------------------------------*
	 *     CALCOLO IL TOTALE DEL CONTRATTO
	 * ----------------------------------------------------------------*</pre>*/
    private void a180CalcTotale() {
        // COB_CODE: MOVE 'A180-CALC-TOTALE'        TO WK-LABEL.
        ws.getVariabili().setWkLabel("A180-CALC-TOTALE");
        // COB_CODE: MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
        ws.getIdsv8888().setAreaDisplay(ws.getVariabili().getWkLabel());
        //    PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX
        // COB_CODE: MOVE ZERO                      TO LCCC0450-TOT-CONTRATTO.
        lccc0450.setLccc0450TotContratto(new AfDecimal(0, 15, 3));
        // COB_CODE: MOVE ZERO                      TO LCCC0450-TOT-IMP-INVES.
        lccc0450.setLccc0450TotImpInves(new AfDecimal(0, 15, 3));
        // COB_CODE: MOVE ZERO                      TO LCCC0450-TOT-IMP-DISINV.
        lccc0450.setLccc0450TotImpDisinv(new AfDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-TAB-TOT FROM 1 BY 1
        //             UNTIL IX-TAB-TOT > LCCC0450-ELE-TAB-MAX
        //                       LCCC0450-IMP-DISINV(IX-TAB-TOT)
        //           END-PERFORM.
        ws.getIndici().setTabTot(1);
        while (!(ws.getIndici().getTabTot() > lccc0450.getLccc0450EleTabMax())) {
            // COB_CODE: COMPUTE LCCC0450-TOT-CONTRATTO =
            //                   LCCC0450-TOT-CONTRATTO +
            //                   LCCC0450-CONTROVALORE(IX-TAB-TOT)
            lccc0450.setLccc0450TotContratto(Trunc.toDecimal(lccc0450.getLccc0450TotContratto().add(lccc0450.getLccc0450TabMatrice().getControvalore(ws.getIndici().getTabTot())), 15, 3));
            // COB_CODE: COMPUTE LCCC0450-TOT-IMP-INVES =
            //                   LCCC0450-TOT-IMP-INVES +
            //                   LCCC0450-IMP-INVES(IX-TAB-TOT)
            lccc0450.setLccc0450TotImpInves(Trunc.toDecimal(lccc0450.getLccc0450TotImpInves().add(lccc0450.getLccc0450TabMatrice().getImpInves(ws.getIndici().getTabTot())), 15, 3));
            // COB_CODE: COMPUTE LCCC0450-TOT-IMP-DISINV =
            //                   LCCC0450-TOT-IMP-DISINV +
            //                   LCCC0450-IMP-DISINV(IX-TAB-TOT)
            lccc0450.setLccc0450TotImpDisinv(Trunc.toDecimal(lccc0450.getLccc0450TotImpDisinv().add(lccc0450.getLccc0450TabMatrice().getImpDisinv(ws.getIndici().getTabTot())), 15, 3));
            ws.getIndici().setTabTot(Trunc.toInt(ws.getIndici().getTabTot() + 1, 4));
        }
    }

    /**Original name: A220-RECUP-IMP-INVES<br>
	 * <pre>----------------------------------------------------------------*
	 *     RECUPERA RICHIESTA DI INVESTIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void a220RecupImpInves() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A220-RECUP-IMP-INVES'    TO WK-LABEL.
        ws.getVariabili().setWkLabel("A220-RECUP-IMP-INVES");
        // COB_CODE: MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
        ws.getIdsv8888().setAreaDisplay(ws.getVariabili().getWkLabel());
        //    PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX
        // COB_CODE: INITIALIZE RICH-INVST-FND
        //                      IDSI0011-BUFFER-DATI.
        initRichInvstFnd();
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: SET  IDSI0011-ID              TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        // COB_CODE: SET  IDSI0011-SELECT          TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET  IDSI0011-TRATT-DEFAULT   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: MOVE LCCC0450-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(lccc0450.getLccc0450DataEffetto());
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(lccc0450.getLccc0450DataEffetto());
        // COB_CODE: MOVE LCCC0450-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(lccc0450.getLccc0450DataCompetenza());
        // COB_CODE: MOVE VAS-ID-RICH-INVST-FND    TO RIF-ID-RICH-INVST-FND.
        ws.getRichInvstFnd().setRifIdRichInvstFnd(ws.getValAst().getVasIdRichInvstFnd().getVasIdRichInvstFnd());
        // COB_CODE: MOVE 'RICH-INVST-FND'         TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("RICH-INVST-FND");
        // COB_CODE: MOVE RICH-INVST-FND           TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getRichInvstFnd().getRichInvstFndFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER       THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--> GESTIRE ERRORE
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                         MOVE IDSO0011-BUFFER-DATI TO RICH-INVST-FND
            //                       WHEN OTHER
            //           *--->       ERRORE DI ACCESSO AL DB
            //                            THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO RICH-INVST-FND
                    ws.getRichInvstFnd().setRichInvstFndFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default://--->       ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL      TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
                    // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING PGM-IDBSRIF0         ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmIdbsrif0Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //--> GESTIRE ERRORE
            // COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
            // COB_CODE: MOVE '005016'           TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING PGM-IDBSRIF0         ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmIdbsrif0Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: A230-RECUP-IMP-DISIN<br>
	 * <pre>*---------------------------------------------------------------*
	 *     RECUPERA RICHIESTA DI DISINVESTIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void a230RecupImpDisin() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A230-RECUP-IMP-DISIN'    TO WK-LABEL.
        ws.getVariabili().setWkLabel("A230-RECUP-IMP-DISIN");
        // COB_CODE: MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
        ws.getIdsv8888().setAreaDisplay(ws.getVariabili().getWkLabel());
        //    PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX
        // COB_CODE: INITIALIZE RICH-DIS-FND
        //                      IDSI0011-BUFFER-DATI.
        initRichDisFnd();
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: SET  IDSI0011-ID              TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        // COB_CODE: SET  IDSI0011-SELECT          TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET  IDSI0011-TRATT-DEFAULT   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: MOVE LCCC0450-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(lccc0450.getLccc0450DataEffetto());
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(lccc0450.getLccc0450DataEffetto());
        // COB_CODE: MOVE LCCC0450-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(lccc0450.getLccc0450DataCompetenza());
        // COB_CODE: MOVE VAS-ID-RICH-DIS-FND      TO RDF-ID-RICH-DIS-FND.
        ws.getRichDisFnd().setRdfIdRichDisFnd(ws.getValAst().getVasIdRichDisFnd().getVasIdRichDisFnd());
        // COB_CODE: MOVE 'RICH-DIS-FND'           TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("RICH-DIS-FND");
        // COB_CODE: MOVE RICH-DIS-FND             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getRichDisFnd().getRichDisFndFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER       THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--> GESTIONE ERRORE
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                         MOVE IDSO0011-BUFFER-DATI TO RICH-DIS-FND
            //                       WHEN OTHER
            //           *--->       ERRORE DI ACCESSO AL DB
            //                            THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO RICH-DIS-FND
                    ws.getRichDisFnd().setRichDisFndFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default://--->       ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL      TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
                    // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING PGM-IDBSRDF0         ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmIdbsrdf0Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //--> GESTIONE ERRORE
            // COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
            // COB_CODE: MOVE '005016'           TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING PGM-IDBSRDF0         ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmIdbsrdf0Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: A250-RECUP-QTZ-AGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     RECUPERA QUOTAZIONE AGGIUNTIVA FONDO
	 * ----------------------------------------------------------------*</pre>*/
    private void a250RecupQtzAgg() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A250-RECUP-QTZ-AGG'           TO WK-LABEL.
        ws.getVariabili().setWkLabel("A250-RECUP-QTZ-AGG");
        // COB_CODE: INITIALIZE QUOTZ-AGG-FND
        //                      IDSI0011-BUFFER-DATI.
        initQuotzAggFnd();
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: SET  IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET  IDSI0011-PRIMARY-KEY      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET  IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET  QTZ-AGG-OK                TO TRUE.
        ws.getFlagQtzAgg().setOk();
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA    TO L41-COD-COMP-ANIA
        ws.getQuotzAggFnd().setCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE 'CR'                           TO L41-TP-QTZ
        ws.getQuotzAggFnd().setTpQtz("CR");
        // COB_CODE: MOVE LCCC0450-COD-FONDO(IX-TAB-TOT) TO L41-COD-FND
        ws.getQuotzAggFnd().setCodFnd(lccc0450.getLccc0450TabMatrice().getCodFondo(ws.getIndici().getTabTot()));
        // COB_CODE: MOVE LCCC0450-DATA-EFFETTO          TO L41-DT-QTZ
        ws.getQuotzAggFnd().setDtQtz(lccc0450.getLccc0450DataEffetto());
        // COB_CODE: MOVE LCCC0450-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(lccc0450.getLccc0450DataEffetto());
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(lccc0450.getLccc0450DataEffetto());
        // COB_CODE: MOVE LCCC0450-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(lccc0450.getLccc0450DataCompetenza());
        // COB_CODE: MOVE 'QUOTZ-AGG-FND'          TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("QUOTZ-AGG-FND");
        // COB_CODE: MOVE QUOTZ-AGG-FND            TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getQuotzAggFnd().getQuotzAggFndFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER       THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--> GESTIONE ERRORE
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                             THRU A260-EX
            //                       WHEN IDSO0011-NOT-FOUND
            //           *--->       QUOTAZIONE NON TROVATA PER IL FONDO $ ALLA DATA $
            //                          CONTINUE
            //                       WHEN OTHER
            //           *--->       ERRORE DI ACCESSO AL DB
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO QUOTZ-AGG-FND
                    ws.getQuotzAggFnd().setQuotzAggFndFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //-->           SALVA I DATI LETTI DALLA TABELLA QUOTZ-AGG-FND
                    //-->           IN WORKCOMMAREA (WL19)
                    // COB_CODE: PERFORM A260-SALVA-QUOTZ-AGG-WK
                    //              THRU A260-EX
                    a260SalvaQuotzAggWk();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->       QUOTAZIONE NON TROVATA PER IL FONDO $ ALLA DATA $
                // COB_CODE: CONTINUE
                //continue
                    break;

                default://--->       ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM         TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL       TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
                    // COB_CODE: MOVE '005016'       TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING PGM-IDBSL410         ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmIdbsl410Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //--> GESTIONE ERRORE
            // COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
            // COB_CODE: MOVE '005016'           TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING PGM-IDBSL410         ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmIdbsl410Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: A260-SALVA-QUOTZ-AGG-WK<br>
	 * <pre>----------------------------------------------------------------*
	 *     SALVA I DATI LETTI DALLA TABELLA QUOTZ-AGG-FND
	 *     IN WORKCOMMAREA (WL19)
	 * ----------------------------------------------------------------*</pre>*/
    private void a260SalvaQuotzAggWk() {
        // COB_CODE: MOVE WL19-ELE-FND-MAX
        //             TO IX-TAB-L19.
        ws.getIndici().setTabL19(TruncAbs.toInt(wl19AreaFondi.getEleFndMax(), 4));
        // COB_CODE: SET FONDO-NON-TROVATO  TO TRUE
        ws.getFlagFondo().setNonTrovato();
        // COB_CODE: PERFORM VARYING IX-TAB-WL19 FROM 1 BY 1
        //                   UNTIL IX-TAB-WL19 > MAX-VAL-L19
        //                   OR WL19-COD-FND(IX-TAB-WL19) NOT > SPACES
        //                   OR FONDO-TROVATO
        //                   END-IF
        //           END-PERFORM
        ws.getIndici().setTabWl19(1);
        while (!(ws.getIndici().getTabWl19() > ws.getMaxValL19() || !Conditions.gt(wl19AreaFondi.getTabFnd(ws.getIndici().getTabWl19()).getLccvl191().getDati().getWl19CodFnd(), "") || ws.getFlagFondo().isTrovato())) {
            // COB_CODE: IF WL19-COD-FND(IX-TAB-WL19) = L41-COD-FND
            //              SET FONDO-TROVATO  TO TRUE
            //           END-IF
            if (Conditions.eq(wl19AreaFondi.getTabFnd(ws.getIndici().getTabWl19()).getLccvl191().getDati().getWl19CodFnd(), ws.getQuotzAggFnd().getCodFnd())) {
                // COB_CODE: SET FONDO-TROVATO  TO TRUE
                ws.getFlagFondo().setTrovato();
            }
            ws.getIndici().setTabWl19(Trunc.toInt(ws.getIndici().getTabWl19() + 1, 4));
        }
        // COB_CODE: IF WL19-ELE-FND-MAX < MAX-VAL-L19
        //              END-IF
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (wl19AreaFondi.getEleFndMax() < ws.getMaxValL19()) {
            // COB_CODE: IF FONDO-NON-TROVATO
            //                 THRU VALORIZZA-OUTPUT-L41-EX
            //           END-IF
            if (ws.getFlagFondo().isNonTrovato()) {
                // COB_CODE: ADD 1
                //            TO IX-TAB-L19
                ws.getIndici().setTabL19(Trunc.toInt(1 + ws.getIndici().getTabL19(), 4));
                // COB_CODE: PERFORM INIZIA-TOT-L19
                //              THRU INIZIA-TOT-L19-EX
                iniziaTotL19();
                // COB_CODE: PERFORM VALORIZZA-OUTPUT-L41
                //              THRU VALORIZZA-OUTPUT-L41-EX
                valorizzaOutputL41();
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getVariabili().getWkLabel());
            // COB_CODE: MOVE '005059'           TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005059");
            // COB_CODE: MOVE 'WL19-AREA-FONDI'  TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("WL19-AREA-FONDI");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: VALORIZZA-OUTPUT-L41<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA OUTPUT QUOTZ-AGG-FND
	 * ----------------------------------------------------------------*</pre>*/
    private void valorizzaOutputL41() {
        // COB_CODE: SET WL19-ST-INV(IX-TAB-L19)
        //            TO TRUE
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getStatus().setInv();
        // COB_CODE: MOVE L41-COD-COMP-ANIA
        //             TO WL19-COD-COMP-ANIA(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19CodCompAnia(ws.getQuotzAggFnd().getCodCompAnia());
        // COB_CODE: MOVE L41-COD-FND
        //             TO WL19-COD-FND(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19CodFnd(ws.getQuotzAggFnd().getCodFnd());
        // COB_CODE: MOVE L41-DT-QTZ
        //             TO WL19-DT-QTZ(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DtQtz(ws.getQuotzAggFnd().getDtQtz());
        // COB_CODE: MOVE L41-VAL-QUO
        //             TO WL19-VAL-QUO(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuo().setWl19ValQuo(Trunc.toDecimal(ws.getQuotzAggFnd().getValQuo(), 12, 5));
        // COB_CODE: MOVE L41-DS-OPER-SQL
        //             TO WL19-DS-OPER-SQL(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DsOperSql(ws.getQuotzAggFnd().getDsOperSql());
        // COB_CODE: MOVE L41-DS-VER
        //             TO WL19-DS-VER(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DsVer(ws.getQuotzAggFnd().getDsVer());
        // COB_CODE: MOVE L41-DS-TS-CPTZ
        //             TO WL19-DS-TS-CPTZ(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DsTsCptz(ws.getQuotzAggFnd().getDsTsCptz());
        // COB_CODE: MOVE L41-DS-UTENTE
        //             TO WL19-DS-UTENTE(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DsUtente(ws.getQuotzAggFnd().getDsUtente());
        // COB_CODE: MOVE L41-DS-STATO-ELAB
        //             TO WL19-DS-STATO-ELAB(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DsStatoElab(ws.getQuotzAggFnd().getDsStatoElab());
        // COB_CODE: MOVE HIGH-VALUES
        //             TO WL19-VAL-QUO-MANFEE-NULL(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuoManfee().setWl19ValQuoManfeeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wl19ValQuoManfee.Len.WL19_VAL_QUO_MANFEE_NULL));
        // COB_CODE: MOVE HIGH-VALUES
        //             TO WL19-TP-FND(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19TpFnd(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES
        //             TO WL19-DT-RILEVAZIONE-NAV-NULL(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().getWl19DtRilevazioneNav().setWl19DtRilevazioneNavNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wl19DtRilevazioneNav.Len.WL19_DT_RILEVAZIONE_NAV_NULL));
        // COB_CODE: MOVE IX-TAB-L19
        //             TO WL19-ELE-FND-MAX.
        wl19AreaFondi.setEleFndMax(((short)(ws.getIndici().getTabL19())));
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES GESTIONE ERRORE
	 * ----------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: VALORIZZA-OUTPUT-TGA<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVTGA3
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputTga() {
        // COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
        //             TO (SF)-ID-PTF(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().setIdPtf(ws.getTrchDiGar().getTgaIdTrchDiGar());
        // COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
        //             TO (SF)-ID-TRCH-DI-GAR(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdTrchDiGar(ws.getTrchDiGar().getTgaIdTrchDiGar());
        // COB_CODE: MOVE TGA-ID-GAR
        //             TO (SF)-ID-GAR(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdGar(ws.getTrchDiGar().getTgaIdGar());
        // COB_CODE: MOVE TGA-ID-ADES
        //             TO (SF)-ID-ADES(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdAdes(ws.getTrchDiGar().getTgaIdAdes());
        // COB_CODE: MOVE TGA-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdPoli(ws.getTrchDiGar().getTgaIdPoli());
        // COB_CODE: MOVE TGA-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdMoviCrz(ws.getTrchDiGar().getTgaIdMoviCrz());
        // COB_CODE: IF TGA-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE TGA-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiuNull(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE TGA-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiu());
        }
        // COB_CODE: MOVE TGA-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDtIniEff(ws.getTrchDiGar().getTgaDtIniEff());
        // COB_CODE: MOVE TGA-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDtEndEff(ws.getTrchDiGar().getTgaDtEndEff());
        // COB_CODE: MOVE TGA-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaCodCompAnia(ws.getTrchDiGar().getTgaCodCompAnia());
        // COB_CODE: MOVE TGA-DT-DECOR
        //             TO (SF)-DT-DECOR(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDtDecor(ws.getTrchDiGar().getTgaDtDecor());
        // COB_CODE: IF TGA-DT-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-SCAD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtScad().getTgaDtScadNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-SCAD-NULL
            //             TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScadNull(ws.getTrchDiGar().getTgaDtScad().getTgaDtScadNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-SCAD
            //             TO (SF)-DT-SCAD(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(ws.getTrchDiGar().getTgaDtScad().getTgaDtScad());
        }
        // COB_CODE: IF TGA-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IB-OGG(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIbOgg(), TrchDiGarIvvs0216.Len.TGA_IB_OGG)) {
            // COB_CODE: MOVE TGA-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIbOgg(ws.getTrchDiGar().getTgaIbOgg());
        }
        else {
            // COB_CODE: MOVE TGA-IB-OGG
            //             TO (SF)-IB-OGG(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIbOgg(ws.getTrchDiGar().getTgaIbOgg());
        }
        // COB_CODE: MOVE TGA-TP-RGM-FISC
        //             TO (SF)-TP-RGM-FISC(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpRgmFisc(ws.getTrchDiGar().getTgaTpRgmFisc());
        // COB_CODE: IF TGA-DT-EMIS-NULL = HIGH-VALUES
        //                TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-EMIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmisNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-EMIS-NULL
            //             TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmisNull(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmisNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-EMIS
            //             TO (SF)-DT-EMIS(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmis());
        }
        // COB_CODE: MOVE TGA-TP-TRCH
        //             TO (SF)-TP-TRCH(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpTrch(ws.getTrchDiGar().getTgaTpTrch());
        // COB_CODE: IF TGA-DUR-AA-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-AA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurAa().getTgaDurAaNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-AA-NULL
            //             TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAaNull(ws.getTrchDiGar().getTgaDurAa().getTgaDurAaNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-AA
            //             TO (SF)-DUR-AA(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(ws.getTrchDiGar().getTgaDurAa().getTgaDurAa());
        }
        // COB_CODE: IF TGA-DUR-MM-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-MM(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurMm().getTgaDurMmNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-MM-NULL
            //             TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMmNull(ws.getTrchDiGar().getTgaDurMm().getTgaDurMmNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-MM
            //             TO (SF)-DUR-MM(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(ws.getTrchDiGar().getTgaDurMm().getTgaDurMm());
        }
        // COB_CODE: IF TGA-DUR-GG-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-GG(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurGg().getTgaDurGgNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-GG-NULL
            //             TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGgNull(ws.getTrchDiGar().getTgaDurGg().getTgaDurGgNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-GG
            //             TO (SF)-DUR-GG(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(ws.getTrchDiGar().getTgaDurGg().getTgaDurGg());
        }
        // COB_CODE: IF TGA-PRE-CASO-MOR-NULL = HIGH-VALUES
        //                TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-CASO-MOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMorNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-CASO-MOR-NULL
            //             TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMorNull(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMorNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-CASO-MOR
            //             TO (SF)-PRE-CASO-MOR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMor(), 15, 3));
        }
        // COB_CODE: IF TGA-PC-INTR-RIAT-NULL = HIGH-VALUES
        //                TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-INTR-RIAT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiatNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-INTR-RIAT-NULL
            //             TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiatNull(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-INTR-RIAT
            //             TO (SF)-PC-INTR-RIAT(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiat(), 6, 3));
        }
        // COB_CODE: IF TGA-IMP-BNS-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAnticNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-BNS-ANTIC-NULL
            //             TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAnticNull(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAnticNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-BNS-ANTIC
            //             TO (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAntic(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INI-NET-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INI-NET(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNetNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INI-NET-NULL
            //             TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNetNull(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNetNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INI-NET
            //             TO (SF)-PRE-INI-NET(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNet(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-PP-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PP-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PP-INI-NULL
            //             TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIniNull(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PP-INI
            //             TO (SF)-PRE-PP-INI(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-PP-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PP-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PP-ULT-NULL
            //             TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUltNull(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PP-ULT
            //             TO (SF)-PRE-PP-ULT(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-TARI-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-TARI-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-TARI-INI-NULL
            //             TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIniNull(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-TARI-INI
            //             TO (SF)-PRE-TARI-INI(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-TARI-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-TARI-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-TARI-ULT-NULL
            //             TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUltNull(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-TARI-ULT
            //             TO (SF)-PRE-TARI-ULT(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INVRIO-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INVRIO-INI-NULL
            //             TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIniNull(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INVRIO-INI
            //             TO (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INVRIO-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INVRIO-ULT-NULL
            //             TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUltNull(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INVRIO-ULT
            //             TO (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-RIVTO-NULL
            //             TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivtoNull(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-RIVTO
            //             TO (SF)-PRE-RIVTO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-PROF-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProfNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-PROF-NULL
            //             TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProfNull(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProfNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-PROF
            //             TO (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProf(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-SAN-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSanNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-SAN-NULL
            //             TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSanNull(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSanNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-SAN
            //             TO (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSan(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-SPO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpoNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-SPO-NULL
            //             TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpoNull(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-SPO
            //             TO (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpo(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-TEC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTecNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-TEC-NULL
            //             TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTecNull(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTecNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-TEC
            //             TO (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTec(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-ALT-SOPR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSoprNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-ALT-SOPR-NULL
            //             TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSoprNull(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSoprNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-ALT-SOPR
            //             TO (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSopr(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-STAB-NULL = HIGH-VALUES
        //                TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreStab().getTgaPreStabNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-STAB-NULL
            //             TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStabNull(ws.getTrchDiGar().getTgaPreStab().getTgaPreStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-STAB
            //             TO (SF)-PRE-STAB(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreStab().getTgaPreStab(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-EFF-STAB-NULL = HIGH-VALUES
        //                TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-EFF-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStabNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-EFF-STAB-NULL
            //             TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStabNull(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-EFF-STAB
            //             TO (SF)-DT-EFF-STAB(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStab());
        }
        // COB_CODE: IF TGA-TS-RIVAL-FIS-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFisNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-FIS-NULL
            //             TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFisNull(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFisNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-FIS
            //             TO (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFis(), 14, 9));
        }
        // COB_CODE: IF TGA-TS-RIVAL-INDICIZ-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndicizNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-INDICIZ-NULL
            //             TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndicizNull(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndicizNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-INDICIZ
            //             TO (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndiciz(), 14, 9));
        }
        // COB_CODE: IF TGA-OLD-TS-TEC-NULL = HIGH-VALUES
        //                TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-OLD-TS-TEC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTecNullFormatted())) {
            // COB_CODE: MOVE TGA-OLD-TS-TEC-NULL
            //             TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTecNull(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTecNull());
        }
        else {
            // COB_CODE: MOVE TGA-OLD-TS-TEC
            //             TO (SF)-OLD-TS-TEC(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(Trunc.toDecimal(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTec(), 14, 9));
        }
        // COB_CODE: IF TGA-RAT-LRD-NULL = HIGH-VALUES
        //                TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RAT-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-RAT-LRD-NULL
            //             TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrdNull(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-RAT-LRD
            //             TO (SF)-RAT-LRD(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrd(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-LRD-NULL = HIGH-VALUES
        //                TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-LRD-NULL
            //             TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrdNull(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-LRD
            //             TO (SF)-PRE-LRD(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrd(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NULL
            //             TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIniNull(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI
            //             TO (SF)-PRSTZ-INI(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-ULT-NULL
            //             TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUltNull(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-ULT
            //             TO (SF)-PRSTZ-ULT(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-IN-OPZ-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-IN-OPZ-RIVTO-NULL
            //             TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivtoNull(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-IN-OPZ-RIVTO
            //             TO (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-INI-STAB-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStabNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-STAB-NULL
            //             TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStabNull(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-STAB
            //             TO (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStab(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-RSH-MOR-NULL = HIGH-VALUES
        //                TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-RSH-MOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMorNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-RSH-MOR-NULL
            //             TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMorNull(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMorNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-RSH-MOR
            //             TO (SF)-CPT-RSH-MOR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMor(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-RID-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-RID-INI-NULL
            //             TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIniNull(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-RID-INI
            //             TO (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIni(), 15, 3));
        }
        // COB_CODE: IF TGA-FL-CAR-CONT-NULL = HIGH-VALUES
        //                TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-CAR-CONT(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlCarCont(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-CAR-CONT-NULL
            //             TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlCarCont(ws.getTrchDiGar().getTgaFlCarCont());
        }
        else {
            // COB_CODE: MOVE TGA-FL-CAR-CONT
            //             TO (SF)-FL-CAR-CONT(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlCarCont(ws.getTrchDiGar().getTgaFlCarCont());
        }
        // COB_CODE: IF TGA-BNS-GIA-LIQTO-NULL = HIGH-VALUES
        //                TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqtoNullFormatted())) {
            // COB_CODE: MOVE TGA-BNS-GIA-LIQTO-NULL
            //             TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqtoNull(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-BNS-GIA-LIQTO
            //             TO (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(Trunc.toDecimal(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqto(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-BNS-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-BNS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpBns().getTgaImpBnsNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-BNS-NULL
            //             TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBnsNull(ws.getTrchDiGar().getTgaImpBns().getTgaImpBnsNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-BNS
            //             TO (SF)-IMP-BNS(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpBns().getTgaImpBns(), 15, 3));
        }
        // COB_CODE: MOVE TGA-COD-DVS
        //             TO (SF)-COD-DVS(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaCodDvs(ws.getTrchDiGar().getTgaCodDvs());
        // COB_CODE: IF TGA-PRSTZ-INI-NEWFIS-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfisNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NEWFIS-NULL
            //             TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfisNull(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfisNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NEWFIS
            //             TO (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfis(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SCON-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SCON(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpScon().getTgaImpSconNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SCON-NULL
            //             TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpSconNull(ws.getTrchDiGar().getTgaImpScon().getTgaImpSconNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SCON
            //             TO (SF)-IMP-SCON(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpScon().getTgaImpScon(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-SCON-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-SCON(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqSconNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-SCON-NULL
            //             TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqSconNull(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqSconNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-SCON
            //             TO (SF)-ALQ-SCON(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqScon(), 6, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-ACQ-NULL
            //             TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcqNull(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-ACQ
            //             TO (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-INC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarIncNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-INC-NULL
            //             TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarIncNull(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-INC
            //             TO (SF)-IMP-CAR-INC(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarInc(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-GEST-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGestNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-GEST-NULL
            //             TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGestNull(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-GEST
            //             TO (SF)-IMP-CAR-GEST(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGest(), 15, 3));
        }
        // COB_CODE: IF TGA-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-1O-ASSTO-NULL
            //             TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAsstoNull(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-1O-ASSTO
            //             TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-1O-ASSTO-NULL
            //             TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAsstoNull(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-1O-ASSTO
            //             TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAssto());
        }
        // COB_CODE: IF TGA-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-2O-ASSTO-NULL
            //             TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAsstoNull(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-2O-ASSTO
            //             TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-2O-ASSTO-NULL
            //             TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAsstoNull(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-2O-ASSTO
            //             TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAssto());
        }
        // COB_CODE: IF TGA-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-3O-ASSTO-NULL
            //             TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAsstoNull(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-3O-ASSTO
            //             TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-3O-ASSTO-NULL
            //             TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAsstoNull(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-3O-ASSTO
            //             TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAssto());
        }
        // COB_CODE: IF TGA-RENDTO-LRD-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RENDTO-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-RENDTO-LRD-NULL
            //             TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrdNull(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-RENDTO-LRD
            //             TO (SF)-RENDTO-LRD(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrd(), 14, 9));
        }
        // COB_CODE: IF TGA-PC-RETR-NULL = HIGH-VALUES
        //                TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-RETR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetrNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-RETR-NULL
            //             TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetrNull(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetrNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-RETR
            //             TO (SF)-PC-RETR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetr(), 6, 3));
        }
        // COB_CODE: IF TGA-RENDTO-RETR-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RENDTO-RETR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetrNullFormatted())) {
            // COB_CODE: MOVE TGA-RENDTO-RETR-NULL
            //             TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetrNull(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetrNull());
        }
        else {
            // COB_CODE: MOVE TGA-RENDTO-RETR
            //             TO (SF)-RENDTO-RETR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(Trunc.toDecimal(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetr(), 14, 9));
        }
        // COB_CODE: IF TGA-MIN-GARTO-NULL = HIGH-VALUES
        //                TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MIN-GARTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGartoNullFormatted())) {
            // COB_CODE: MOVE TGA-MIN-GARTO-NULL
            //             TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGartoNull(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGartoNull());
        }
        else {
            // COB_CODE: MOVE TGA-MIN-GARTO
            //             TO (SF)-MIN-GARTO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(Trunc.toDecimal(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGarto(), 14, 9));
        }
        // COB_CODE: IF TGA-MIN-TRNUT-NULL = HIGH-VALUES
        //                TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MIN-TRNUT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnutNullFormatted())) {
            // COB_CODE: MOVE TGA-MIN-TRNUT-NULL
            //             TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnutNull(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnutNull());
        }
        else {
            // COB_CODE: MOVE TGA-MIN-TRNUT
            //             TO (SF)-MIN-TRNUT(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(Trunc.toDecimal(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnut(), 14, 9));
        }
        // COB_CODE: IF TGA-PRE-ATT-DI-TRCH-NULL = HIGH-VALUES
        //                TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrchNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-ATT-DI-TRCH-NULL
            //             TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrchNull(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrchNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-ATT-DI-TRCH
            //             TO (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrch(), 15, 3));
        }
        // COB_CODE: IF TGA-MATU-END2000-NULL = HIGH-VALUES
        //                TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MATU-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-MATU-END2000-NULL
            //             TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000Null(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-MATU-END2000
            //             TO (SF)-MATU-END2000(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-TOT-INI-NULL = HIGH-VALUES
        //                TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-TOT-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIniNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-TOT-INI-NULL
            //             TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIniNull(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-TOT-INI
            //             TO (SF)-ABB-TOT-INI(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIni(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-TOT-ULT-NULL = HIGH-VALUES
        //                TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-TOT-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUltNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-TOT-ULT-NULL
            //             TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUltNull(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-TOT-ULT
            //             TO (SF)-ABB-TOT-ULT(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-ANNU-ULT-NULL = HIGH-VALUES
        //                TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUltNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-ANNU-ULT-NULL
            //             TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUltNull(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-ANNU-ULT
            //             TO (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-DUR-ABB-NULL = HIGH-VALUES
        //                TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-ABB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbbNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-ABB-NULL
            //             TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbbNull(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbbNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-ABB
            //             TO (SF)-DUR-ABB(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbb());
        }
        // COB_CODE: IF TGA-TP-ADEG-ABB-NULL = HIGH-VALUES
        //                TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-ADEG-ABB(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaTpAdegAbb(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-TP-ADEG-ABB-NULL
            //             TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpAdegAbb(ws.getTrchDiGar().getTgaTpAdegAbb());
        }
        else {
            // COB_CODE: MOVE TGA-TP-ADEG-ABB
            //             TO (SF)-TP-ADEG-ABB(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpAdegAbb(ws.getTrchDiGar().getTgaTpAdegAbb());
        }
        // COB_CODE: IF TGA-MOD-CALC-NULL = HIGH-VALUES
        //                TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MOD-CALC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaModCalcFormatted())) {
            // COB_CODE: MOVE TGA-MOD-CALC-NULL
            //             TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaModCalc(ws.getTrchDiGar().getTgaModCalc());
        }
        else {
            // COB_CODE: MOVE TGA-MOD-CALC
            //             TO (SF)-MOD-CALC(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaModCalc(ws.getTrchDiGar().getTgaModCalc());
        }
        // COB_CODE: IF TGA-IMP-AZ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-AZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAz().getTgaImpAzNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-AZ-NULL
            //             TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAzNull(ws.getTrchDiGar().getTgaImpAz().getTgaImpAzNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-AZ
            //             TO (SF)-IMP-AZ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAz().getTgaImpAz(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-ADER-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-ADER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAder().getTgaImpAderNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-ADER-NULL
            //             TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAderNull(ws.getTrchDiGar().getTgaImpAder().getTgaImpAderNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-ADER
            //             TO (SF)-IMP-ADER(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAder().getTgaImpAder(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-TFR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TFR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfrNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TFR-NULL
            //             TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfrNull(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfrNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TFR
            //             TO (SF)-IMP-TFR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfr(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-VOLO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVoloNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-VOLO-NULL
            //             TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVoloNull(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVoloNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-VOLO
            //             TO (SF)-IMP-VOLO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVolo(), 15, 3));
        }
        // COB_CODE: IF TGA-VIS-END2000-NULL = HIGH-VALUES
        //                TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-VIS-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-VIS-END2000-NULL
            //             TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000Null(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-VIS-END2000
            //             TO (SF)-VIS-END2000(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-VLDT-PROD-NULL = HIGH-VALUES
        //                TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-VLDT-PROD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProdNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD-NULL
            //             TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProdNull(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProdNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD
            //             TO (SF)-DT-VLDT-PROD(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProd());
        }
        // COB_CODE: IF TGA-DT-INI-VAL-TAR-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTarNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR-NULL
            //             TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTarNull(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTarNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR
            //             TO (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTar());
        }
        // COB_CODE: IF TGA-IMPB-VIS-END2000-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-VIS-END2000-NULL
            //             TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000Null(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-VIS-END2000
            //             TO (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-REN-INI-TS-TEC-0-NULL = HIGH-VALUES
        //                TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0NullFormatted())) {
            // COB_CODE: MOVE TGA-REN-INI-TS-TEC-0-NULL
            //             TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0Null(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0Null());
        }
        else {
            // COB_CODE: MOVE TGA-REN-INI-TS-TEC-0
            //             TO (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(Trunc.toDecimal(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0(), 15, 3));
        }
        // COB_CODE: IF TGA-PC-RIP-PRE-NULL = HIGH-VALUES
        //                TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-RIP-PRE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPreNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-RIP-PRE-NULL
            //             TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPreNull(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPreNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-RIP-PRE
            //             TO (SF)-PC-RIP-PRE(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPre(), 6, 3));
        }
        // COB_CODE: IF TGA-FL-IMPORTI-FORZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlImportiForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-IMPORTI-FORZ-NULL
            //             TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlImportiForz(ws.getTrchDiGar().getTgaFlImportiForz());
        }
        else {
            // COB_CODE: MOVE TGA-FL-IMPORTI-FORZ
            //             TO (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlImportiForz(ws.getTrchDiGar().getTgaFlImportiForz());
        }
        // COB_CODE: IF TGA-PRSTZ-INI-NFORZ-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforzNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NFORZ-NULL
            //             TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforzNull(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforzNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NFORZ
            //             TO (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforz(), 15, 3));
        }
        // COB_CODE: IF TGA-VIS-END2000-NFORZ-NULL = HIGH-VALUES
        //                TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000NforzNullFormatted())) {
            // COB_CODE: MOVE TGA-VIS-END2000-NFORZ-NULL
            //             TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000NforzNull(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000NforzNull());
        }
        else {
            // COB_CODE: MOVE TGA-VIS-END2000-NFORZ
            //             TO (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(Trunc.toDecimal(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000Nforz(), 15, 3));
        }
        // COB_CODE: IF TGA-INTR-MORA-NULL = HIGH-VALUES
        //                TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INTR-MORA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMoraNullFormatted())) {
            // COB_CODE: MOVE TGA-INTR-MORA-NULL
            //             TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMoraNull(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE TGA-INTR-MORA
            //             TO (SF)-INTR-MORA(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(Trunc.toDecimal(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMora(), 15, 3));
        }
        // COB_CODE: IF TGA-MANFEE-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MANFEE-ANTIC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE TGA-MANFEE-ANTIC-NULL
            //             TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAnticNull(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE TGA-MANFEE-ANTIC
            //             TO (SF)-MANFEE-ANTIC(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(Trunc.toDecimal(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF TGA-MANFEE-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MANFEE-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-MANFEE-RICOR-NULL
            //             TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicorNull(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-MANFEE-RICOR
            //             TO (SF)-MANFEE-RICOR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-UNI-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-UNI-RIVTO-NULL
            //             TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivtoNull(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-UNI-RIVTO
            //             TO (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-1AA-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-1AA-ACQ-NULL
            //             TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcqNull(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-1AA-ACQ
            //             TO (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-2AA-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-2AA-ACQ-NULL
            //             TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcqNull(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-2AA-ACQ
            //             TO (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-RICOR-NULL
            //             TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicorNull(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-RICOR
            //             TO (SF)-PROV-RICOR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProvInc().getTgaProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-INC-NULL
            //             TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvIncNull(ws.getTrchDiGar().getTgaProvInc().getTgaProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-INC
            //             TO (SF)-PROV-INC(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaProvInc().getTgaProvInc(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-ACQ-NULL
            //             TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcqNull(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-ACQ
            //             TO (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcq(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-INC-NULL
            //             TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvIncNull(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-INC
            //             TO (SF)-ALQ-PROV-INC(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvInc(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-RICOR-NULL
            //             TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicorNull(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-RICOR
            //             TO (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicor(), 6, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-ACQ-NULL
            //             TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcqNull(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-ACQ
            //             TO (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-INC-NULL
            //             TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvIncNull(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-INC
            //             TO (SF)-IMPB-PROV-INC(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvInc(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-RICOR-NULL
            //             TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicorNull(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-RICOR
            //             TO (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-FL-PROV-FORZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-PROV-FORZ(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlProvForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-PROV-FORZ-NULL
            //             TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlProvForz(ws.getTrchDiGar().getTgaFlProvForz());
        }
        else {
            // COB_CODE: MOVE TGA-FL-PROV-FORZ
            //             TO (SF)-FL-PROV-FORZ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlProvForz(ws.getTrchDiGar().getTgaFlProvForz());
        }
        // COB_CODE: IF TGA-PRSTZ-AGG-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-INI-NULL
            //             TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIniNull(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-INI
            //             TO (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIni(), 15, 3));
        }
        // COB_CODE: IF TGA-INCR-PRE-NULL = HIGH-VALUES
        //                TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INCR-PRE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPreNullFormatted())) {
            // COB_CODE: MOVE TGA-INCR-PRE-NULL
            //             TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPreNull(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPreNull());
        }
        else {
            // COB_CODE: MOVE TGA-INCR-PRE
            //             TO (SF)-INCR-PRE(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(Trunc.toDecimal(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPre(), 15, 3));
        }
        // COB_CODE: IF TGA-INCR-PRSTZ-NULL = HIGH-VALUES
        //                TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INCR-PRSTZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstzNullFormatted())) {
            // COB_CODE: MOVE TGA-INCR-PRSTZ-NULL
            //             TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstzNull(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstzNull());
        }
        else {
            // COB_CODE: MOVE TGA-INCR-PRSTZ
            //             TO (SF)-INCR-PRSTZ(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(Trunc.toDecimal(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstz(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-ULT-ADEG-PRE-PR-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePrNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR-NULL
            //             TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePrNull(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePrNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR
            //             TO (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePr());
        }
        // COB_CODE: IF TGA-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-ULT-NULL
            //             TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUltNull(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-ULT
            //             TO (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-TS-RIVAL-NET-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-NET(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNetNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-NET-NULL
            //             TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNetNull(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNetNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-NET
            //             TO (SF)-TS-RIVAL-NET(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNet(), 14, 9));
        }
        // COB_CODE: IF TGA-PRE-PATTUITO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PATTUITO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuitoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PATTUITO-NULL
            //             TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuitoNull(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuitoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PATTUITO
            //             TO (SF)-PRE-PATTUITO(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuito(), 15, 3));
        }
        // COB_CODE: IF TGA-TP-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-RIVAL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTpRivalFormatted())) {
            // COB_CODE: MOVE TGA-TP-RIVAL-NULL
            //             TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpRival(ws.getTrchDiGar().getTgaTpRival());
        }
        else {
            // COB_CODE: MOVE TGA-TP-RIVAL
            //             TO (SF)-TP-RIVAL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpRival(ws.getTrchDiGar().getTgaTpRival());
        }
        // COB_CODE: IF TGA-RIS-MAT-NULL = HIGH-VALUES
        //                TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RIS-MAT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRisMat().getTgaRisMatNullFormatted())) {
            // COB_CODE: MOVE TGA-RIS-MAT-NULL
            //             TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMatNull(ws.getTrchDiGar().getTgaRisMat().getTgaRisMatNull());
        }
        else {
            // COB_CODE: MOVE TGA-RIS-MAT
            //             TO (SF)-RIS-MAT(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(Trunc.toDecimal(ws.getTrchDiGar().getTgaRisMat().getTgaRisMat(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-MIN-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScadNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-MIN-SCAD-NULL
            //             TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScadNull(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScadNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-MIN-SCAD
            //             TO (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScad(), 15, 3));
        }
        // COB_CODE: IF TGA-COMMIS-GEST-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COMMIS-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGestNullFormatted())) {
            // COB_CODE: MOVE TGA-COMMIS-GEST-NULL
            //             TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGestNull(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-COMMIS-GEST
            //             TO (SF)-COMMIS-GEST(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGest(), 15, 3));
        }
        // COB_CODE: IF TGA-TP-MANFEE-APPL-NULL = HIGH-VALUES
        //                TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTpManfeeApplFormatted())) {
            // COB_CODE: MOVE TGA-TP-MANFEE-APPL-NULL
            //             TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl(ws.getTrchDiGar().getTgaTpManfeeAppl());
        }
        else {
            // COB_CODE: MOVE TGA-TP-MANFEE-APPL
            //             TO (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl(ws.getTrchDiGar().getTgaTpManfeeAppl());
        }
        // COB_CODE: MOVE TGA-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsRiga(ws.getTrchDiGar().getTgaDsRiga());
        // COB_CODE: MOVE TGA-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsOperSql(ws.getTrchDiGar().getTgaDsOperSql());
        // COB_CODE: MOVE TGA-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsVer(ws.getTrchDiGar().getTgaDsVer());
        // COB_CODE: MOVE TGA-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsTsIniCptz(ws.getTrchDiGar().getTgaDsTsIniCptz());
        // COB_CODE: MOVE TGA-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsTsEndCptz(ws.getTrchDiGar().getTgaDsTsEndCptz());
        // COB_CODE: MOVE TGA-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsUtente(ws.getTrchDiGar().getTgaDsUtente());
        // COB_CODE: MOVE TGA-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsStatoElab(ws.getTrchDiGar().getTgaDsStatoElab());
        // COB_CODE: IF TGA-PC-COMMIS-GEST-NULL = HIGH-VALUES
        //                TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGestNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-COMMIS-GEST-NULL
            //             TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGestNull(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-COMMIS-GEST
            //             TO (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGest(), 6, 3));
        }
        // COB_CODE: IF TGA-NUM-GG-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRivalNullFormatted())) {
            // COB_CODE: MOVE TGA-NUM-GG-RIVAL-NULL
            //             TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRivalNull(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRivalNull());
        }
        else {
            // COB_CODE: MOVE TGA-NUM-GG-RIVAL
            //             TO (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRival());
        }
        // COB_CODE: IF TGA-IMP-TRASFE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TRASFE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TRASFE-NULL
            //             TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfeNull(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TRASFE
            //             TO (SF)-IMP-TRASFE(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-TFR-STRC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TFR-STRC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TFR-STRC-NULL
            //             TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrcNull(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TFR-STRC
            //             TO (SF)-IMP-TFR-STRC(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF TGA-ACQ-EXP-NULL = HIGH-VALUES
        //                TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ACQ-EXP(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExpNullFormatted())) {
            // COB_CODE: MOVE TGA-ACQ-EXP-NULL
            //             TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExpNull(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExpNull());
        }
        else {
            // COB_CODE: MOVE TGA-ACQ-EXP
            //             TO (SF)-ACQ-EXP(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(Trunc.toDecimal(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExp(), 15, 3));
        }
        // COB_CODE: IF TGA-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-REMUN-ASS-NULL
            //             TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAssNull(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-REMUN-ASS
            //             TO (SF)-REMUN-ASS(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAss(), 15, 3));
        }
        // COB_CODE: IF TGA-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-COMMIS-INTER-NULL
            //             TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInterNull(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-COMMIS-INTER
            //             TO (SF)-COMMIS-INTER(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInter(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-REMUN-ASS-NULL
            //             TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAssNull(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-REMUN-ASS
            //             TO (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAss(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-COMMIS-INTER-NULL
            //             TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInterNull(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-COMMIS-INTER
            //             TO (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInter(), 6, 3));
        }
        // COB_CODE: IF TGA-IMPB-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-REMUN-ASS-NULL
            //             TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAssNull(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-REMUN-ASS
            //             TO (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAss(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-COMMIS-INTER-NULL
            //             TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInterNull(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-COMMIS-INTER
            //             TO (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInter(), 15, 3));
        }
        // COB_CODE: IF TGA-COS-RUN-ASSVA-NULL = HIGH-VALUES
        //                TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssvaNullFormatted())) {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-NULL
            //             TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssvaNull(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssvaNull());
        }
        else {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA
            //             TO (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(Trunc.toDecimal(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssva(), 15, 3));
        }
        // COB_CODE: IF TGA-COS-RUN-ASSVA-IDC-NULL = HIGH-VALUES
        //                TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdcNullFormatted())) {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-IDC-NULL
            //             TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdcNull(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdcNull());
        }
        else {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-IDC
            //             TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
            wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(Trunc.toDecimal(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdc(), 15, 3));
        }
    }

    /**Original name: INIZIA-TOT-TGA<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVTGA4
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotTga() {
        // COB_CODE: PERFORM INIZIA-ZEROES-TGA THRU INIZIA-ZEROES-TGA-EX
        iniziaZeroesTga();
        // COB_CODE: PERFORM INIZIA-SPACES-TGA THRU INIZIA-SPACES-TGA-EX
        iniziaSpacesTga();
        // COB_CODE: PERFORM INIZIA-NULL-TGA THRU INIZIA-NULL-TGA-EX.
        iniziaNullTga();
    }

    /**Original name: INIZIA-NULL-TGA<br>*/
    private void iniziaNullTga() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaIdMoviChiu.Len.WTGA_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDtScad.Len.WTGA_DT_SCAD_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDati.Len.WTGA_IB_OGG));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDtEmis.Len.WTGA_DT_EMIS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDurAa.Len.WTGA_DUR_AA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDurMm.Len.WTGA_DUR_MM_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDurGg.Len.WTGA_DUR_GG_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreCasoMor.Len.WTGA_PRE_CASO_MOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPcIntrRiat.Len.WTGA_PC_INTR_RIAT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpBnsAntic.Len.WTGA_IMP_BNS_ANTIC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreIniNet.Len.WTGA_PRE_INI_NET_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrePpIni.Len.WTGA_PRE_PP_INI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrePpUlt.Len.WTGA_PRE_PP_ULT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreTariIni.Len.WTGA_PRE_TARI_INI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreTariUlt.Len.WTGA_PRE_TARI_ULT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreInvrioIni.Len.WTGA_PRE_INVRIO_INI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreInvrioUlt.Len.WTGA_PRE_INVRIO_ULT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreRivto.Len.WTGA_PRE_RIVTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpSoprProf.Len.WTGA_IMP_SOPR_PROF_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpSoprSan.Len.WTGA_IMP_SOPR_SAN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpSoprSpo.Len.WTGA_IMP_SOPR_SPO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpSoprTec.Len.WTGA_IMP_SOPR_TEC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSoprNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpAltSopr.Len.WTGA_IMP_ALT_SOPR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreStab.Len.WTGA_PRE_STAB_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDtEffStab.Len.WTGA_DT_EFF_STAB_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaTsRivalFis.Len.WTGA_TS_RIVAL_FIS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndicizNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaTsRivalIndiciz.Len.WTGA_TS_RIVAL_INDICIZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaOldTsTec.Len.WTGA_OLD_TS_TEC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaRatLrd.Len.WTGA_RAT_LRD_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreLrd.Len.WTGA_PRE_LRD_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrstzIni.Len.WTGA_PRSTZ_INI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrstzUlt.Len.WTGA_PRSTZ_ULT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaCptInOpzRivto.Len.WTGA_CPT_IN_OPZ_RIVTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrstzIniStab.Len.WTGA_PRSTZ_INI_STAB_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaCptRshMor.Len.WTGA_CPT_RSH_MOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrstzRidIni.Len.WTGA_PRSTZ_RID_INI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlCarCont(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaBnsGiaLiqto.Len.WTGA_BNS_GIA_LIQTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBnsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpBns.Len.WTGA_IMP_BNS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrstzIniNewfis.Len.WTGA_PRSTZ_INI_NEWFIS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpScon.Len.WTGA_IMP_SCON_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaAlqScon.Len.WTGA_ALQ_SCON_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpCarAcq.Len.WTGA_IMP_CAR_ACQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpCarInc.Len.WTGA_IMP_CAR_INC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpCarGest.Len.WTGA_IMP_CAR_GEST_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaEtaAa1oAssto.Len.WTGA_ETA_AA1O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaEtaMm1oAssto.Len.WTGA_ETA_MM1O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaEtaAa2oAssto.Len.WTGA_ETA_AA2O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaEtaMm2oAssto.Len.WTGA_ETA_MM2O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaEtaAa3oAssto.Len.WTGA_ETA_AA3O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaEtaMm3oAssto.Len.WTGA_ETA_MM3O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaRendtoLrd.Len.WTGA_RENDTO_LRD_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPcRetr.Len.WTGA_PC_RETR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaRendtoRetr.Len.WTGA_RENDTO_RETR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGartoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaMinGarto.Len.WTGA_MIN_GARTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnutNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaMinTrnut.Len.WTGA_MIN_TRNUT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreAttDiTrch.Len.WTGA_PRE_ATT_DI_TRCH_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaMatuEnd2000.Len.WTGA_MATU_END2000_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaAbbTotIni.Len.WTGA_ABB_TOT_INI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaAbbTotUlt.Len.WTGA_ABB_TOT_ULT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaAbbAnnuUlt.Len.WTGA_ABB_ANNU_ULT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDurAbb.Len.WTGA_DUR_ABB_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpAdegAbb(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaModCalc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDati.Len.WTGA_MOD_CALC));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpAz.Len.WTGA_IMP_AZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpAder.Len.WTGA_IMP_ADER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpTfr.Len.WTGA_IMP_TFR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpVolo.Len.WTGA_IMP_VOLO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaVisEnd2000.Len.WTGA_VIS_END2000_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDtVldtProd.Len.WTGA_DT_VLDT_PROD_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDtIniValTar.Len.WTGA_DT_INI_VAL_TAR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpbVisEnd2000.Len.WTGA_IMPB_VIS_END2000_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaRenIniTsTec0.Len.WTGA_REN_INI_TS_TEC0_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPcRipPre.Len.WTGA_PC_RIP_PRE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlImportiForz(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrstzIniNforz.Len.WTGA_PRSTZ_INI_NFORZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000NforzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaVisEnd2000Nforz.Len.WTGA_VIS_END2000_NFORZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaIntrMora.Len.WTGA_INTR_MORA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaManfeeAntic.Len.WTGA_MANFEE_ANTIC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaManfeeRicor.Len.WTGA_MANFEE_RICOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreUniRivto.Len.WTGA_PRE_UNI_RIVTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaProv1aaAcq.Len.WTGA_PROV1AA_ACQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaProv2aaAcq.Len.WTGA_PROV2AA_ACQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaProvRicor.Len.WTGA_PROV_RICOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaProvInc.Len.WTGA_PROV_INC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaAlqProvAcq.Len.WTGA_ALQ_PROV_ACQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaAlqProvInc.Len.WTGA_ALQ_PROV_INC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaAlqProvRicor.Len.WTGA_ALQ_PROV_RICOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpbProvAcq.Len.WTGA_IMPB_PROV_ACQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpbProvInc.Len.WTGA_IMPB_PROV_INC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpbProvRicor.Len.WTGA_IMPB_PROV_RICOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlProvForz(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrstzAggIni.Len.WTGA_PRSTZ_AGG_INI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaIncrPre.Len.WTGA_INCR_PRE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaIncrPrstz.Len.WTGA_INCR_PRSTZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDtUltAdegPrePr.Len.WTGA_DT_ULT_ADEG_PRE_PR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrstzAggUlt.Len.WTGA_PRSTZ_AGG_ULT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaTsRivalNet.Len.WTGA_TS_RIVAL_NET_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuitoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrePattuito.Len.WTGA_PRE_PATTUITO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpRival(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDati.Len.WTGA_TP_RIVAL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaRisMat.Len.WTGA_RIS_MAT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaCptMinScad.Len.WTGA_CPT_MIN_SCAD_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaCommisGest.Len.WTGA_COMMIS_GEST_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaDati.Len.WTGA_TP_MANFEE_APPL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPcCommisGest.Len.WTGA_PC_COMMIS_GEST_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaNumGgRival.Len.WTGA_NUM_GG_RIVAL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpTrasfe.Len.WTGA_IMP_TRASFE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpTfrStrc.Len.WTGA_IMP_TFR_STRC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaAcqExp.Len.WTGA_ACQ_EXP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaRemunAss.Len.WTGA_REMUN_ASS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaCommisInter.Len.WTGA_COMMIS_INTER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaAlqRemunAss.Len.WTGA_ALQ_REMUN_ASS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaAlqCommisInter.Len.WTGA_ALQ_COMMIS_INTER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpbRemunAss.Len.WTGA_IMPB_REMUN_ASS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaImpbCommisInter.Len.WTGA_IMPB_COMMIS_INTER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssvaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaCosRunAssva.Len.WTGA_COS_RUN_ASSVA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA).
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaCosRunAssvaIdc.Len.WTGA_COS_RUN_ASSVA_IDC_NULL));
    }

    /**Original name: INIZIA-ZEROES-TGA<br>*/
    private void iniziaZeroesTga() {
        // COB_CODE: MOVE 0 TO (SF)-ID-TRCH-DI-GAR(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdTrchDiGar(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-GAR(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdGar(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-ADES(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdAdes(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-POLI(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdPoli(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdMoviCrz(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDtIniEff(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDtEndEff(0);
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaCodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-DECOR(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDtDecor(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-RIGA(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsRiga(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsTsIniCptz(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-TGA).
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsTsEndCptz(0);
    }

    /**Original name: INIZIA-SPACES-TGA<br>*/
    private void iniziaSpacesTga() {
        // COB_CODE: MOVE SPACES TO (SF)-TP-RGM-FISC(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpRgmFisc("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-TRCH(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpTrch("");
        // COB_CODE: MOVE SPACES TO (SF)-COD-DVS(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaCodDvs("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-TGA)
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsUtente("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-TGA).
        wtgaAreaTrchDiGar.getTabTran(ws.getIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsStatoElab(Types.SPACE_CHAR);
    }

    /**Original name: INIZIA-TOT-L19<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVL194
	 *    ULTIMO AGG. 24 APR 2015
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotL19() {
        // COB_CODE: PERFORM INIZIA-ZEROES-L19 THRU INIZIA-ZEROES-L19-EX
        iniziaZeroesL19();
        // COB_CODE: PERFORM INIZIA-SPACES-L19 THRU INIZIA-SPACES-L19-EX
        iniziaSpacesL19();
        // COB_CODE: PERFORM INIZIA-NULL-L19 THRU INIZIA-NULL-L19-EX.
        iniziaNullL19();
    }

    /**Original name: INIZIA-NULL-L19<br>*/
    private void iniziaNullL19() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VAL-QUO-NULL(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuo().setWl19ValQuoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wl19ValQuo.Len.WL19_VAL_QUO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VAL-QUO-MANFEE-NULL(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuoManfee().setWl19ValQuoManfeeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wl19ValQuoManfee.Len.WL19_VAL_QUO_MANFEE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-RILEVAZIONE-NAV-NULL(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().getWl19DtRilevazioneNav().setWl19DtRilevazioneNavNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wl19DtRilevazioneNav.Len.WL19_DT_RILEVAZIONE_NAV_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VAL-QUO-ACQ-NULL(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuoAcq().setWl19ValQuoAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wl19ValQuoAcq.Len.WL19_VAL_QUO_ACQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-NO-NAV-NULL(IX-TAB-L19).
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19FlNoNav(Types.HIGH_CHAR_VAL);
    }

    /**Original name: INIZIA-ZEROES-L19<br>*/
    private void iniziaZeroesL19() {
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19CodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-QTZ(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DtQtz(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-CPTZ(IX-TAB-L19).
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DsTsCptz(0);
    }

    /**Original name: INIZIA-SPACES-L19<br>*/
    private void iniziaSpacesL19() {
        // COB_CODE: MOVE SPACES TO (SF)-COD-FND(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19CodFnd("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DsUtente("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-L19)
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19DsStatoElab(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-TP-FND(IX-TAB-L19).
        wl19AreaFondi.getTabFnd(ws.getIndici().getTabL19()).getLccvl191().getDati().setWl19TpFnd(Types.SPACE_CHAR);
    }

    public void initValAst() {
        ws.getValAst().setVasIdValAst(0);
        ws.getValAst().getVasIdTrchDiGar().setVasIdTrchDiGar(0);
        ws.getValAst().setVasIdMoviFinrio(0);
        ws.getValAst().setVasIdMoviCrz(0);
        ws.getValAst().getVasIdMoviChiu().setVasIdMoviChiu(0);
        ws.getValAst().setVasDtIniEff(0);
        ws.getValAst().setVasDtEndEff(0);
        ws.getValAst().setVasCodCompAnia(0);
        ws.getValAst().setVasCodFnd("");
        ws.getValAst().getVasNumQuo().setVasNumQuo(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasValQuo().setVasValQuo(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasDtValzz().setVasDtValzz(0);
        ws.getValAst().setVasTpValAst("");
        ws.getValAst().getVasIdRichInvstFnd().setVasIdRichInvstFnd(0);
        ws.getValAst().getVasIdRichDisFnd().setVasIdRichDisFnd(0);
        ws.getValAst().setVasDsRiga(0);
        ws.getValAst().setVasDsOperSql(Types.SPACE_CHAR);
        ws.getValAst().setVasDsVer(0);
        ws.getValAst().setVasDsTsIniCptz(0);
        ws.getValAst().setVasDsTsEndCptz(0);
        ws.getValAst().setVasDsUtente("");
        ws.getValAst().setVasDsStatoElab(Types.SPACE_CHAR);
        ws.getValAst().getVasPreMovto().setVasPreMovto(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasImpMovto().setVasImpMovto(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasPcInvDis().setVasPcInvDis(new AfDecimal(0, 14, 9));
        ws.getValAst().getVasNumQuoLorde().setVasNumQuoLorde(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasDtValzzCalc().setVasDtValzzCalc(0);
        ws.getValAst().getVasMinusValenza().setVasMinusValenza(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasPlusValenza().setVasPlusValenza(new AfDecimal(0, 15, 3));
    }

    public void initVariabili() {
        ws.getVariabili().setWkNumeroQuote(new AfDecimal(0, 12, 5));
        ws.getVariabili().setWkControvalore(new AfDecimal(0, 15, 3));
        ws.getVariabili().getWkDtRicorTranche().setWkDtRicorTrancheNumFormatted("00000000");
        ws.getVariabili().setWkAnnoFormatted("0000");
        ws.getVariabili().setWkLabel("");
    }

    public void initIndici() {
        ws.getIndici().setTabGrz(0);
        ws.getIndici().setTabTga(0);
        ws.getIndici().setTabMat(0);
        ws.getIndici().setTabTot(0);
        ws.getIndici().setIndTar(0);
        ws.getIndici().setTabDtc(0);
        ws.getIndici().setTabAde(0);
        ws.getIndici().setTabGar(0);
        ws.getIndici().setTabL19(0);
        ws.getIndici().setTabWl19(0);
    }

    public void initWcomTabelle() {
        ws.setWgarEleGarMax(((short)0));
        for (int idx0 = 1; idx0 <= Lccs0450Data.WGAR_TAB_GAR_MAXOCCURS; idx0++) {
            ws.getWgarTabGar(idx0).getLccvgrz1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getWgarTabGar(idx0).getLccvgrz1().setIdPtf(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzIdGar(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzIdPoli(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzIdMoviCrz(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzDtIniEff(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzDtEndEff(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzCodCompAnia(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzIbOgg("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzCodSez("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTari("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzRamoBila("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(((short)0));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRsh("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(((short)0));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzModPagGarcol("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPerPre("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(((short)0));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(((short)0));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(((short)0));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(((short)0));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(((short)0));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(((short)0));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpEmisPur(Types.SPACE_CHAR);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPre(Types.SPACE_CHAR);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDur("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(((short)0));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(new AfDecimal(0, 6, 3));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPrstzAssta("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(new AfDecimal(0, 6, 3));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzCodFnd("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzAaRenCer("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPcRip("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(new AfDecimal(0, 6, 3));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpIas("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpStab("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpAdegPre(Types.SPACE_CHAR);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTratRiass("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDtEmisRiass("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCessRiass("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzDsRiga(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzDsOperSql(Types.SPACE_CHAR);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzDsVer(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsIniCptz(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsEndCptz(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzDsUtente("");
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzDsStatoElab(Types.SPACE_CHAR);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(new AfDecimal(0, 14, 9));
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(0);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzRshInvst(Types.SPACE_CHAR);
            ws.getWgarTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRamoBila("");
        }
    }

    public void initMatrice() {
        lccc0450.getLccc0450TabMatrice().setIdTrchDiGar(1, 0);
        lccc0450.getLccc0450TabMatrice().setCodFondo(1, "");
        lccc0450.getLccc0450TabMatrice().setNumQuote(1, new AfDecimal(0, 12, 5));
        lccc0450.getLccc0450TabMatrice().setValQuota(1, new AfDecimal(0, 15, 3));
        lccc0450.getLccc0450TabMatrice().setNumQuoteIni(1, new AfDecimal(0, 12, 5));
        lccc0450.getLccc0450TabMatrice().setValQuotaIni(1, new AfDecimal(0, 15, 3));
        lccc0450.getLccc0450TabMatrice().setValQuotaT(1, new AfDecimal(0, 15, 3));
        lccc0450.getLccc0450TabMatrice().setPercentInv(1, new AfDecimal(0, 6, 3));
        lccc0450.getLccc0450TabMatrice().setControvalore(1, new AfDecimal(0, 15, 3));
        lccc0450.getLccc0450TabMatrice().setDtValQuoteFormatted(1, "00000000");
        lccc0450.getLccc0450TabMatrice().setImpInves(1, new AfDecimal(0, 15, 3));
        lccc0450.getLccc0450TabMatrice().setImpDisinv(1, new AfDecimal(0, 15, 3));
    }

    public void initLdbv0011() {
        ws.getLdbv0011().setTitCont(0);
        ws.getLdbv0011().setDettTitCont(0);
        ws.getLdbv0011().setPoli(0);
        ws.getLdbv0011().setAdes(0);
        ws.getLdbv0011().setTrchDiGar(0);
        ws.getLdbv0011().setGar(0);
    }

    public void initLdbv4911() {
        ws.getLdbv4911().setTpValAst1("");
        ws.getLdbv4911().setTpValAst2("");
        ws.getLdbv4911().setTpValAst3("");
        ws.getLdbv4911().setTpValAst4("");
        ws.getLdbv4911().setTpValAst5("");
        ws.getLdbv4911().setTpValAst6("");
        ws.getLdbv4911().setTpValAst7("");
        ws.getLdbv4911().setTpValAst8("");
        ws.getLdbv4911().setTpValAst9("");
        ws.getLdbv4911().setTpValAst10("");
        ws.getLdbv4911().setIdTrchDiGar(0);
    }

    public void initIoA2kLccc0003() {
        ws.getIoA2kLccc0003().getInput().setA2kFunz("");
        ws.getIoA2kLccc0003().getInput().setA2kInfo("");
        ws.getIoA2kLccc0003().getInput().setA2kDeltaFormatted("000");
        ws.getIoA2kLccc0003().getInput().setA2kTdelta(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kFislav(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kInicon(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata("");
        ws.getIoA2kLccc0003().getOutput().getA2kOugmaX().setA2kOugmaFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOugg02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus102(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOumm02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus202(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuss02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuaa02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().setA2kOuamgFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().setA2kOuamgp(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuamg0p(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog9(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprogc(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuproco(0);
        ws.getIoA2kLccc0003().getOutput().getA2kOujulX().setA2kOujulFormatted("0000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOugg04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOumm04("");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuss04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuaa04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOugg05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOumm05("");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOuaa05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg06("");
        ws.getIoA2kLccc0003().getOutput().setA2kOung06Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOutg06(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg07Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugl07X().setA2kOufa07Formatted("000");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg08Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg09(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg10(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().setRcode("");
    }

    public void initQuotzFndUnit() {
        ws.getQuotzFndUnit().setL19CodCompAnia(0);
        ws.getQuotzFndUnit().setL19CodFnd("");
        ws.getQuotzFndUnit().setL19DtQtz(0);
        ws.getQuotzFndUnit().getL19ValQuo().setL19ValQuo(new AfDecimal(0, 12, 5));
        ws.getQuotzFndUnit().getL19ValQuoManfee().setL19ValQuoManfee(new AfDecimal(0, 12, 5));
        ws.getQuotzFndUnit().setL19DsOperSql(Types.SPACE_CHAR);
        ws.getQuotzFndUnit().setL19DsVer(0);
        ws.getQuotzFndUnit().setL19DsTsCptz(0);
        ws.getQuotzFndUnit().setL19DsUtente("");
        ws.getQuotzFndUnit().setL19DsStatoElab(Types.SPACE_CHAR);
        ws.getQuotzFndUnit().setL19TpFnd(Types.SPACE_CHAR);
        ws.getQuotzFndUnit().getL19DtRilevazioneNav().setL19DtRilevazioneNav(0);
        ws.getQuotzFndUnit().getL19ValQuoAcq().setL19ValQuoAcq(new AfDecimal(0, 12, 5));
        ws.getQuotzFndUnit().setL19FlNoNav(Types.SPACE_CHAR);
    }

    public void initRichInvstFnd() {
        ws.getRichInvstFnd().setRifIdRichInvstFnd(0);
        ws.getRichInvstFnd().setRifIdMoviFinrio(0);
        ws.getRichInvstFnd().setRifIdMoviCrz(0);
        ws.getRichInvstFnd().getRifIdMoviChiu().setRifIdMoviChiu(0);
        ws.getRichInvstFnd().setRifDtIniEff(0);
        ws.getRichInvstFnd().setRifDtEndEff(0);
        ws.getRichInvstFnd().setRifCodCompAnia(0);
        ws.getRichInvstFnd().setRifCodFnd("");
        ws.getRichInvstFnd().getRifNumQuo().setRifNumQuo(new AfDecimal(0, 12, 5));
        ws.getRichInvstFnd().getRifPc().setRifPc(new AfDecimal(0, 6, 3));
        ws.getRichInvstFnd().getRifImpMovto().setRifImpMovto(new AfDecimal(0, 15, 3));
        ws.getRichInvstFnd().getRifDtInvst().setRifDtInvst(0);
        ws.getRichInvstFnd().setRifCodTari("");
        ws.getRichInvstFnd().setRifTpStat("");
        ws.getRichInvstFnd().setRifTpModInvst("");
        ws.getRichInvstFnd().setRifCodDiv("");
        ws.getRichInvstFnd().getRifDtCambioVlt().setRifDtCambioVlt(0);
        ws.getRichInvstFnd().setRifTpFnd(Types.SPACE_CHAR);
        ws.getRichInvstFnd().setRifDsRiga(0);
        ws.getRichInvstFnd().setRifDsOperSql(Types.SPACE_CHAR);
        ws.getRichInvstFnd().setRifDsVer(0);
        ws.getRichInvstFnd().setRifDsTsIniCptz(0);
        ws.getRichInvstFnd().setRifDsTsEndCptz(0);
        ws.getRichInvstFnd().setRifDsUtente("");
        ws.getRichInvstFnd().setRifDsStatoElab(Types.SPACE_CHAR);
        ws.getRichInvstFnd().getRifDtInvstCalc().setRifDtInvstCalc(0);
        ws.getRichInvstFnd().setRifFlCalcInvto(Types.SPACE_CHAR);
        ws.getRichInvstFnd().getRifImpGapEvent().setRifImpGapEvent(new AfDecimal(0, 15, 3));
        ws.getRichInvstFnd().setRifFlSwmBp2s(Types.SPACE_CHAR);
    }

    public void initRichDisFnd() {
        ws.getRichDisFnd().setRdfIdRichDisFnd(0);
        ws.getRichDisFnd().setRdfIdMoviFinrio(0);
        ws.getRichDisFnd().setRdfIdMoviCrz(0);
        ws.getRichDisFnd().getRdfIdMoviChiu().setRdfIdMoviChiu(0);
        ws.getRichDisFnd().setRdfDtIniEff(0);
        ws.getRichDisFnd().setRdfDtEndEff(0);
        ws.getRichDisFnd().setRdfCodCompAnia(0);
        ws.getRichDisFnd().setRdfCodFnd("");
        ws.getRichDisFnd().getRdfNumQuo().setRdfNumQuo(new AfDecimal(0, 12, 5));
        ws.getRichDisFnd().getRdfPc().setRdfPc(new AfDecimal(0, 6, 3));
        ws.getRichDisFnd().getRdfImpMovto().setRdfImpMovto(new AfDecimal(0, 15, 3));
        ws.getRichDisFnd().getRdfDtDis().setRdfDtDis(0);
        ws.getRichDisFnd().setRdfCodTari("");
        ws.getRichDisFnd().setRdfTpStat("");
        ws.getRichDisFnd().setRdfTpModDis("");
        ws.getRichDisFnd().setRdfCodDiv("");
        ws.getRichDisFnd().getRdfDtCambioVlt().setRdfDtCambioVlt(0);
        ws.getRichDisFnd().setRdfTpFnd(Types.SPACE_CHAR);
        ws.getRichDisFnd().setRdfDsRiga(0);
        ws.getRichDisFnd().setRdfDsOperSql(Types.SPACE_CHAR);
        ws.getRichDisFnd().setRdfDsVer(0);
        ws.getRichDisFnd().setRdfDsTsIniCptz(0);
        ws.getRichDisFnd().setRdfDsTsEndCptz(0);
        ws.getRichDisFnd().setRdfDsUtente("");
        ws.getRichDisFnd().setRdfDsStatoElab(Types.SPACE_CHAR);
        ws.getRichDisFnd().getRdfDtDisCalc().setRdfDtDisCalc(0);
        ws.getRichDisFnd().setRdfFlCalcDis(Types.SPACE_CHAR);
        ws.getRichDisFnd().getRdfCommisGest().setRdfCommisGest(new AfDecimal(0, 18, 7));
        ws.getRichDisFnd().getRdfNumQuoCdgFnz().setRdfNumQuoCdgFnz(new AfDecimal(0, 12, 5));
        ws.getRichDisFnd().getRdfNumQuoCdgtotFnz().setRdfNumQuoCdgtotFnz(new AfDecimal(0, 12, 5));
        ws.getRichDisFnd().getRdfCosRunAssvaIdc().setRdfCosRunAssvaIdc(new AfDecimal(0, 15, 3));
        ws.getRichDisFnd().setRdfFlSwmBp2s(Types.SPACE_CHAR);
    }

    public void initQuotzAggFnd() {
        ws.getQuotzAggFnd().setCodCompAnia(0);
        ws.getQuotzAggFnd().setTpQtz("");
        ws.getQuotzAggFnd().setCodFnd("");
        ws.getQuotzAggFnd().setDtQtz(0);
        ws.getQuotzAggFnd().setTpFnd(Types.SPACE_CHAR);
        ws.getQuotzAggFnd().setValQuo(new AfDecimal(0, 12, 5));
        ws.getQuotzAggFnd().setDescAggLen(((short)0));
        ws.getQuotzAggFnd().setDescAgg("");
        ws.getQuotzAggFnd().setDsOperSql(Types.SPACE_CHAR);
        ws.getQuotzAggFnd().setDsVer(0);
        ws.getQuotzAggFnd().setDsTsCptz(0);
        ws.getQuotzAggFnd().setDsUtente("");
        ws.getQuotzAggFnd().setDsStatoElab(Types.SPACE_CHAR);
    }
}
