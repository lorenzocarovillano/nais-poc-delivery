package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BtcParallelismDao;
import it.accenture.jnais.commons.data.to.IBtcParallelism;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BtcParallelism;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsbpa0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.BpaDtEnd;
import it.accenture.jnais.ws.redefines.BpaDtStart;
import it.accenture.jnais.ws.redefines.BpaIdOggA;
import it.accenture.jnais.ws.redefines.BpaIdOggDa;
import it.accenture.jnais.ws.redefines.BpaNumRowSchedule;

/**Original name: IDBSBPA0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  07 MAR 2008.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsbpa0 extends Program implements IBtcParallelism {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BtcParallelismDao btcParallelismDao = new BtcParallelismDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsbpa0Data ws = new Idbsbpa0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: BTC-PARALLELISM
    private BtcParallelism btcParallelism;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSBPA0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, BtcParallelism btcParallelism) {
        this.idsv0003 = idsv0003;
        this.btcParallelism = btcParallelism;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //             END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //             END-IF
                //           END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsbpa0 getInstance() {
        return ((Idbsbpa0)Programs.getInstance(Idbsbpa0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSBPA0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSBPA0");
        // COB_CODE: MOVE 'BTC_PARALLELISM' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BTC_PARALLELISM");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                COD_COMP_ANIA
        //                ,PROTOCOL
        //                ,PROG_PROTOCOL
        //                ,COD_BATCH_STATE
        //                ,DT_INS
        //                ,USER_INS
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,DESC_PARALLELISM
        //                ,ID_OGG_DA
        //                ,ID_OGG_A
        //                ,TP_OGG
        //                ,NUM_ROW_SCHEDULE
        //             INTO
        //                :BPA-COD-COMP-ANIA
        //               ,:BPA-PROTOCOL
        //               ,:BPA-PROG-PROTOCOL
        //               ,:BPA-COD-BATCH-STATE
        //               ,:BPA-DT-INS-DB
        //               ,:BPA-USER-INS
        //               ,:BPA-DT-START-DB
        //                :IND-BPA-DT-START
        //               ,:BPA-DT-END-DB
        //                :IND-BPA-DT-END
        //               ,:BPA-USER-START
        //                :IND-BPA-USER-START
        //               ,:BPA-DESC-PARALLELISM-VCHAR
        //                :IND-BPA-DESC-PARALLELISM
        //               ,:BPA-ID-OGG-DA
        //                :IND-BPA-ID-OGG-DA
        //               ,:BPA-ID-OGG-A
        //                :IND-BPA-ID-OGG-A
        //               ,:BPA-TP-OGG
        //                :IND-BPA-TP-OGG
        //               ,:BPA-NUM-ROW-SCHEDULE
        //                :IND-BPA-NUM-ROW-SCHEDULE
        //             FROM BTC_PARALLELISM
        //             WHERE     COD_COMP_ANIA = :BPA-COD-COMP-ANIA
        //                   AND PROTOCOL = :BPA-PROTOCOL
        //                   AND PROG_PROTOCOL = :BPA-PROG-PROTOCOL
        //           END-EXEC.
        btcParallelismDao.selectRec1(btcParallelism.getBpaCodCompAnia(), btcParallelism.getBpaProtocol(), btcParallelism.getBpaProgProtocol(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX
            z150ValorizzaDataServices();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO BTC_PARALLELISM
            //                  (
            //                     COD_COMP_ANIA
            //                    ,PROTOCOL
            //                    ,PROG_PROTOCOL
            //                    ,COD_BATCH_STATE
            //                    ,DT_INS
            //                    ,USER_INS
            //                    ,DT_START
            //                    ,DT_END
            //                    ,USER_START
            //                    ,DESC_PARALLELISM
            //                    ,ID_OGG_DA
            //                    ,ID_OGG_A
            //                    ,TP_OGG
            //                    ,NUM_ROW_SCHEDULE
            //                  )
            //              VALUES
            //                  (
            //                    :BPA-COD-COMP-ANIA
            //                    ,:BPA-PROTOCOL
            //                    ,:BPA-PROG-PROTOCOL
            //                    ,:BPA-COD-BATCH-STATE
            //                    ,:BPA-DT-INS-DB
            //                    ,:BPA-USER-INS
            //                    ,:BPA-DT-START-DB
            //                     :IND-BPA-DT-START
            //                    ,:BPA-DT-END-DB
            //                     :IND-BPA-DT-END
            //                    ,:BPA-USER-START
            //                     :IND-BPA-USER-START
            //                    ,:BPA-DESC-PARALLELISM-VCHAR
            //                     :IND-BPA-DESC-PARALLELISM
            //                    ,:BPA-ID-OGG-DA
            //                     :IND-BPA-ID-OGG-DA
            //                    ,:BPA-ID-OGG-A
            //                     :IND-BPA-ID-OGG-A
            //                    ,:BPA-TP-OGG
            //                     :IND-BPA-TP-OGG
            //                    ,:BPA-NUM-ROW-SCHEDULE
            //                     :IND-BPA-NUM-ROW-SCHEDULE
            //                  )
            //           END-EXEC
            btcParallelismDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_PARALLELISM SET
        //                   COD_COMP_ANIA          =
        //                :BPA-COD-COMP-ANIA
        //                  ,PROTOCOL               =
        //                :BPA-PROTOCOL
        //                  ,PROG_PROTOCOL          =
        //                :BPA-PROG-PROTOCOL
        //                  ,COD_BATCH_STATE        =
        //                :BPA-COD-BATCH-STATE
        //                  ,DT_INS                 =
        //           :BPA-DT-INS-DB
        //                  ,USER_INS               =
        //                :BPA-USER-INS
        //                  ,DT_START               =
        //           :BPA-DT-START-DB
        //                                       :IND-BPA-DT-START
        //                  ,DT_END                 =
        //           :BPA-DT-END-DB
        //                                       :IND-BPA-DT-END
        //                  ,USER_START             =
        //                :BPA-USER-START
        //                                       :IND-BPA-USER-START
        //                  ,DESC_PARALLELISM       =
        //                :BPA-DESC-PARALLELISM-VCHAR
        //                                       :IND-BPA-DESC-PARALLELISM
        //                  ,ID_OGG_DA              =
        //                :BPA-ID-OGG-DA
        //                                       :IND-BPA-ID-OGG-DA
        //                  ,ID_OGG_A               =
        //                :BPA-ID-OGG-A
        //                                       :IND-BPA-ID-OGG-A
        //                  ,TP_OGG                 =
        //                :BPA-TP-OGG
        //                                       :IND-BPA-TP-OGG
        //                  ,NUM_ROW_SCHEDULE       =
        //                :BPA-NUM-ROW-SCHEDULE
        //                                       :IND-BPA-NUM-ROW-SCHEDULE
        //                WHERE     COD_COMP_ANIA = :BPA-COD-COMP-ANIA
        //                   AND PROTOCOL = :BPA-PROTOCOL
        //                   AND PROG_PROTOCOL = :BPA-PROG-PROTOCOL
        //           END-EXEC.
        btcParallelismDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM BTC_PARALLELISM
        //                WHERE     COD_COMP_ANIA = :BPA-COD-COMP-ANIA
        //                   AND PROTOCOL = :BPA-PROTOCOL
        //                   AND PROG_PROTOCOL = :BPA-PROG-PROTOCOL
        //           END-EXEC.
        btcParallelismDao.deleteRec(btcParallelism.getBpaCodCompAnia(), btcParallelism.getBpaProtocol(), btcParallelism.getBpaProgProtocol());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-BPA-DT-START = -1
        //              MOVE HIGH-VALUES TO BPA-DT-START-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getDtStart() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-DT-START-NULL
            btcParallelism.getBpaDtStart().setBpaDtStartNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BpaDtStart.Len.BPA_DT_START_NULL));
        }
        // COB_CODE: IF IND-BPA-DT-END = -1
        //              MOVE HIGH-VALUES TO BPA-DT-END-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getDtEnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-DT-END-NULL
            btcParallelism.getBpaDtEnd().setBpaDtEndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BpaDtEnd.Len.BPA_DT_END_NULL));
        }
        // COB_CODE: IF IND-BPA-USER-START = -1
        //              MOVE HIGH-VALUES TO BPA-USER-START-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getUserStart() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-USER-START-NULL
            btcParallelism.setBpaUserStart(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcParallelism.Len.BPA_USER_START));
        }
        // COB_CODE: IF IND-BPA-DESC-PARALLELISM = -1
        //              MOVE HIGH-VALUES TO BPA-DESC-PARALLELISM
        //           END-IF
        if (ws.getIndBtcParallelism().getDescParallelism() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-DESC-PARALLELISM
            btcParallelism.setBpaDescParallelism(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcParallelism.Len.BPA_DESC_PARALLELISM));
        }
        // COB_CODE: IF IND-BPA-ID-OGG-DA = -1
        //              MOVE HIGH-VALUES TO BPA-ID-OGG-DA-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getIdOggDa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-ID-OGG-DA-NULL
            btcParallelism.getBpaIdOggDa().setBpaIdOggDaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BpaIdOggDa.Len.BPA_ID_OGG_DA_NULL));
        }
        // COB_CODE: IF IND-BPA-ID-OGG-A = -1
        //              MOVE HIGH-VALUES TO BPA-ID-OGG-A-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getIdOggA() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-ID-OGG-A-NULL
            btcParallelism.getBpaIdOggA().setBpaIdOggANull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BpaIdOggA.Len.BPA_ID_OGG_A_NULL));
        }
        // COB_CODE: IF IND-BPA-TP-OGG = -1
        //              MOVE HIGH-VALUES TO BPA-TP-OGG-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getTpOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-TP-OGG-NULL
            btcParallelism.setBpaTpOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcParallelism.Len.BPA_TP_OGG));
        }
        // COB_CODE: IF IND-BPA-NUM-ROW-SCHEDULE = -1
        //              MOVE HIGH-VALUES TO BPA-NUM-ROW-SCHEDULE-NULL
        //           END-IF.
        if (ws.getIndBtcParallelism().getNumRowSchedule() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-NUM-ROW-SCHEDULE-NULL
            btcParallelism.getBpaNumRowSchedule().setBpaNumRowScheduleNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BpaNumRowSchedule.Len.BPA_NUM_ROW_SCHEDULE_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES<br>*/
    private void z150ValorizzaDataServices() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF BPA-DT-START-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-DT-START
        //           ELSE
        //              MOVE 0 TO IND-BPA-DT-START
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaDtStart().getBpaDtStartNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-DT-START
            ws.getIndBtcParallelism().setDtStart(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-DT-START
            ws.getIndBtcParallelism().setDtStart(((short)0));
        }
        // COB_CODE: IF BPA-DT-END-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-DT-END
        //           ELSE
        //              MOVE 0 TO IND-BPA-DT-END
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaDtEnd().getBpaDtEndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-DT-END
            ws.getIndBtcParallelism().setDtEnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-DT-END
            ws.getIndBtcParallelism().setDtEnd(((short)0));
        }
        // COB_CODE: IF BPA-USER-START-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-USER-START
        //           ELSE
        //              MOVE 0 TO IND-BPA-USER-START
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaUserStart(), BtcParallelism.Len.BPA_USER_START)) {
            // COB_CODE: MOVE -1 TO IND-BPA-USER-START
            ws.getIndBtcParallelism().setUserStart(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-USER-START
            ws.getIndBtcParallelism().setUserStart(((short)0));
        }
        // COB_CODE: IF BPA-DESC-PARALLELISM = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-DESC-PARALLELISM
        //           ELSE
        //              MOVE 0 TO IND-BPA-DESC-PARALLELISM
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaDescParallelism(), BtcParallelism.Len.BPA_DESC_PARALLELISM)) {
            // COB_CODE: MOVE -1 TO IND-BPA-DESC-PARALLELISM
            ws.getIndBtcParallelism().setDescParallelism(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-DESC-PARALLELISM
            ws.getIndBtcParallelism().setDescParallelism(((short)0));
        }
        // COB_CODE: IF BPA-ID-OGG-DA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-ID-OGG-DA
        //           ELSE
        //              MOVE 0 TO IND-BPA-ID-OGG-DA
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaIdOggDa().getBpaIdOggDaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-ID-OGG-DA
            ws.getIndBtcParallelism().setIdOggDa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-ID-OGG-DA
            ws.getIndBtcParallelism().setIdOggDa(((short)0));
        }
        // COB_CODE: IF BPA-ID-OGG-A-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-ID-OGG-A
        //           ELSE
        //              MOVE 0 TO IND-BPA-ID-OGG-A
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaIdOggA().getBpaIdOggANullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-ID-OGG-A
            ws.getIndBtcParallelism().setIdOggA(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-ID-OGG-A
            ws.getIndBtcParallelism().setIdOggA(((short)0));
        }
        // COB_CODE: IF BPA-TP-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-TP-OGG
        //           ELSE
        //              MOVE 0 TO IND-BPA-TP-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaTpOggFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-TP-OGG
            ws.getIndBtcParallelism().setTpOgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-TP-OGG
            ws.getIndBtcParallelism().setTpOgg(((short)0));
        }
        // COB_CODE: IF BPA-NUM-ROW-SCHEDULE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-NUM-ROW-SCHEDULE
        //           ELSE
        //              MOVE 0 TO IND-BPA-NUM-ROW-SCHEDULE
        //           END-IF.
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaNumRowSchedule().getBpaNumRowScheduleNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-NUM-ROW-SCHEDULE
            ws.getIndBtcParallelism().setNumRowSchedule(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-NUM-ROW-SCHEDULE
            ws.getIndBtcParallelism().setNumRowSchedule(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE BPA-DT-INS TO WS-TIMESTAMP-N
        ws.getIdsv0010().setWsTimestampN(TruncAbs.toLong(btcParallelism.getBpaDtIns(), 18));
        // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
        z701TsNToX();
        // COB_CODE: MOVE WS-TIMESTAMP-X   TO BPA-DT-INS-DB
        ws.getBtcParallelismDb().setInsDb(ws.getIdsv0010().getWsTimestampX());
        // COB_CODE: IF IND-BPA-DT-START = 0
        //               MOVE WS-TIMESTAMP-X      TO BPA-DT-START-DB
        //           END-IF
        if (ws.getIndBtcParallelism().getDtStart() == 0) {
            // COB_CODE: MOVE BPA-DT-START TO WS-TIMESTAMP-N
            ws.getIdsv0010().setWsTimestampN(TruncAbs.toLong(btcParallelism.getBpaDtStart().getBpaDtStart(), 18));
            // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
            z701TsNToX();
            // COB_CODE: MOVE WS-TIMESTAMP-X      TO BPA-DT-START-DB
            ws.getBtcParallelismDb().setStartDb(ws.getIdsv0010().getWsTimestampX());
        }
        // COB_CODE: IF IND-BPA-DT-END = 0
        //               MOVE WS-TIMESTAMP-X      TO BPA-DT-END-DB
        //           END-IF.
        if (ws.getIndBtcParallelism().getDtEnd() == 0) {
            // COB_CODE: MOVE BPA-DT-END TO WS-TIMESTAMP-N
            ws.getIdsv0010().setWsTimestampN(TruncAbs.toLong(btcParallelism.getBpaDtEnd().getBpaDtEnd(), 18));
            // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
            z701TsNToX();
            // COB_CODE: MOVE WS-TIMESTAMP-X      TO BPA-DT-END-DB
            ws.getBtcParallelismDb().setEndDb(ws.getIdsv0010().getWsTimestampX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE BPA-DT-INS-DB TO WS-TIMESTAMP-X
        ws.getIdsv0010().setWsTimestampX(ws.getBtcParallelismDb().getInsDb());
        // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
        z801TsXToN();
        // COB_CODE: MOVE WS-TIMESTAMP-N      TO BPA-DT-INS
        btcParallelism.setBpaDtIns(ws.getIdsv0010().getWsTimestampN());
        // COB_CODE: IF IND-BPA-DT-START = 0
        //               MOVE WS-TIMESTAMP-N      TO BPA-DT-START
        //           END-IF
        if (ws.getIndBtcParallelism().getDtStart() == 0) {
            // COB_CODE: MOVE BPA-DT-START-DB TO WS-TIMESTAMP-X
            ws.getIdsv0010().setWsTimestampX(ws.getBtcParallelismDb().getStartDb());
            // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
            z801TsXToN();
            // COB_CODE: MOVE WS-TIMESTAMP-N      TO BPA-DT-START
            btcParallelism.getBpaDtStart().setBpaDtStart(ws.getIdsv0010().getWsTimestampN());
        }
        // COB_CODE: IF IND-BPA-DT-END = 0
        //               MOVE WS-TIMESTAMP-N      TO BPA-DT-END
        //           END-IF.
        if (ws.getIndBtcParallelism().getDtEnd() == 0) {
            // COB_CODE: MOVE BPA-DT-END-DB TO WS-TIMESTAMP-X
            ws.getIdsv0010().setWsTimestampX(ws.getBtcParallelismDb().getEndDb());
            // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
            z801TsXToN();
            // COB_CODE: MOVE WS-TIMESTAMP-N      TO BPA-DT-END
            btcParallelism.getBpaDtEnd().setBpaDtEnd(ws.getIdsv0010().getWsTimestampN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF BPA-DESC-PARALLELISM
        //                       TO BPA-DESC-PARALLELISM-LEN.
        btcParallelism.setBpaDescParallelismLen(((short)BtcParallelism.Len.BPA_DESC_PARALLELISM));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z701-TS-N-TO-X<br>*/
    private void z701TsNToX() {
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(1:4)
        //                TO WS-TIMESTAMP-X(1:4)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(5:2)
        //                TO WS-TIMESTAMP-X(6:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(7:2)
        //                TO WS-TIMESTAMP-X(9:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(9:2)
        //                TO WS-TIMESTAMP-X(12:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((9) - 1, 10), 12, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(11:2)
        //                TO WS-TIMESTAMP-X(15:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((11) - 1, 12), 15, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(13:2)
        //                TO WS-TIMESTAMP-X(18:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((13) - 1, 14), 18, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(15:4)
        //                TO WS-TIMESTAMP-X(21:4)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((15) - 1, 18), 21, 4));
        // COB_CODE: MOVE '00'
        //                TO WS-TIMESTAMP-X(25:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "00", 25, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-TIMESTAMP-X(5:1)
        //                   WS-TIMESTAMP-X(8:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "-", 5, 1));
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "-", 8, 1));
        // COB_CODE: MOVE SPACE
        //                TO WS-TIMESTAMP-X(11:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), LiteralGenerator.create(Types.SPACE_CHAR, 1), 11, 1));
        // COB_CODE: MOVE ':'
        //                TO WS-TIMESTAMP-X(14:1)
        //                   WS-TIMESTAMP-X(17:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ":", 14, 1));
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ":", 17, 1));
        // COB_CODE: MOVE '.'
        //                TO WS-TIMESTAMP-X(20:1).
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ".", 20, 1));
    }

    /**Original name: Z801-TS-X-TO-N<br>*/
    private void z801TsXToN() {
        // COB_CODE: MOVE WS-TIMESTAMP-X(1:4)
        //                TO WS-STR-TIMESTAMP-N(1:4)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-TIMESTAMP-X(6:2)
        //                TO WS-STR-TIMESTAMP-N(5:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(9:2)
        //                TO WS-STR-TIMESTAMP-N(7:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((9) - 1, 10), 7, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(12:2)
        //                TO WS-STR-TIMESTAMP-N(9:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((12) - 1, 13), 9, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(15:2)
        //                TO WS-STR-TIMESTAMP-N(11:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((15) - 1, 16), 11, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(18:2)
        //                TO WS-STR-TIMESTAMP-N(13:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((18) - 1, 19), 13, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(21:4)
        //                TO WS-STR-TIMESTAMP-N(15:4).
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((21) - 1, 24), 15, 4));
    }

    @Override
    public int getBpaCodCompAnia() {
        return btcParallelism.getBpaCodCompAnia();
    }

    @Override
    public void setBpaCodCompAnia(int bpaCodCompAnia) {
        this.btcParallelism.setBpaCodCompAnia(bpaCodCompAnia);
    }

    @Override
    public int getBpaProgProtocol() {
        return btcParallelism.getBpaProgProtocol();
    }

    @Override
    public void setBpaProgProtocol(int bpaProgProtocol) {
        this.btcParallelism.setBpaProgProtocol(bpaProgProtocol);
    }

    @Override
    public String getBpaProtocol() {
        return btcParallelism.getBpaProtocol();
    }

    @Override
    public void setBpaProtocol(String bpaProtocol) {
        this.btcParallelism.setBpaProtocol(bpaProtocol);
    }

    @Override
    public char getCodBatchState() {
        return btcParallelism.getBpaCodBatchState();
    }

    @Override
    public void setCodBatchState(char codBatchState) {
        this.btcParallelism.setBpaCodBatchState(codBatchState);
    }

    @Override
    public String getDescParallelismVchar() {
        return btcParallelism.getBpaDescParallelismVcharFormatted();
    }

    @Override
    public void setDescParallelismVchar(String descParallelismVchar) {
        this.btcParallelism.setBpaDescParallelismVcharFormatted(descParallelismVchar);
    }

    @Override
    public String getDescParallelismVcharObj() {
        if (ws.getIndBtcParallelism().getDescParallelism() >= 0) {
            return getDescParallelismVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescParallelismVcharObj(String descParallelismVcharObj) {
        if (descParallelismVcharObj != null) {
            setDescParallelismVchar(descParallelismVcharObj);
            ws.getIndBtcParallelism().setDescParallelism(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setDescParallelism(((short)-1));
        }
    }

    @Override
    public String getDtEndDb() {
        return ws.getBtcParallelismDb().getEndDb();
    }

    @Override
    public void setDtEndDb(String dtEndDb) {
        this.ws.getBtcParallelismDb().setEndDb(dtEndDb);
    }

    @Override
    public String getDtEndDbObj() {
        if (ws.getIndBtcParallelism().getDtEnd() >= 0) {
            return getDtEndDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndDbObj(String dtEndDbObj) {
        if (dtEndDbObj != null) {
            setDtEndDb(dtEndDbObj);
            ws.getIndBtcParallelism().setDtEnd(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setDtEnd(((short)-1));
        }
    }

    @Override
    public String getDtInsDb() {
        return ws.getBtcParallelismDb().getInsDb();
    }

    @Override
    public void setDtInsDb(String dtInsDb) {
        this.ws.getBtcParallelismDb().setInsDb(dtInsDb);
    }

    @Override
    public String getDtStartDb() {
        return ws.getBtcParallelismDb().getStartDb();
    }

    @Override
    public void setDtStartDb(String dtStartDb) {
        this.ws.getBtcParallelismDb().setStartDb(dtStartDb);
    }

    @Override
    public String getDtStartDbObj() {
        if (ws.getIndBtcParallelism().getDtStart() >= 0) {
            return getDtStartDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtStartDbObj(String dtStartDbObj) {
        if (dtStartDbObj != null) {
            setDtStartDb(dtStartDbObj);
            ws.getIndBtcParallelism().setDtStart(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setDtStart(((short)-1));
        }
    }

    @Override
    public char getIabv0002State01() {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public char getIabv0002State02() {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public char getIabv0002State03() {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public char getIabv0002State04() {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public char getIabv0002State05() {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public char getIabv0002State06() {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public char getIabv0002State07() {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public char getIabv0002State08() {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public char getIabv0002State09() {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public char getIabv0002State10() {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public int getIdOggA() {
        return btcParallelism.getBpaIdOggA().getBpaIdOggA();
    }

    @Override
    public void setIdOggA(int idOggA) {
        this.btcParallelism.getBpaIdOggA().setBpaIdOggA(idOggA);
    }

    @Override
    public Integer getIdOggAObj() {
        if (ws.getIndBtcParallelism().getIdOggA() >= 0) {
            return ((Integer)getIdOggA());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdOggAObj(Integer idOggAObj) {
        if (idOggAObj != null) {
            setIdOggA(((int)idOggAObj));
            ws.getIndBtcParallelism().setIdOggA(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setIdOggA(((short)-1));
        }
    }

    @Override
    public int getIdOggDa() {
        return btcParallelism.getBpaIdOggDa().getBpaIdOggDa();
    }

    @Override
    public void setIdOggDa(int idOggDa) {
        this.btcParallelism.getBpaIdOggDa().setBpaIdOggDa(idOggDa);
    }

    @Override
    public Integer getIdOggDaObj() {
        if (ws.getIndBtcParallelism().getIdOggDa() >= 0) {
            return ((Integer)getIdOggDa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdOggDaObj(Integer idOggDaObj) {
        if (idOggDaObj != null) {
            setIdOggDa(((int)idOggDaObj));
            ws.getIndBtcParallelism().setIdOggDa(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setIdOggDa(((short)-1));
        }
    }

    @Override
    public int getNumRowSchedule() {
        return btcParallelism.getBpaNumRowSchedule().getBpaNumRowSchedule();
    }

    @Override
    public void setNumRowSchedule(int numRowSchedule) {
        this.btcParallelism.getBpaNumRowSchedule().setBpaNumRowSchedule(numRowSchedule);
    }

    @Override
    public Integer getNumRowScheduleObj() {
        if (ws.getIndBtcParallelism().getNumRowSchedule() >= 0) {
            return ((Integer)getNumRowSchedule());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumRowScheduleObj(Integer numRowScheduleObj) {
        if (numRowScheduleObj != null) {
            setNumRowSchedule(((int)numRowScheduleObj));
            ws.getIndBtcParallelism().setNumRowSchedule(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setNumRowSchedule(((short)-1));
        }
    }

    @Override
    public String getTpOgg() {
        return btcParallelism.getBpaTpOgg();
    }

    @Override
    public void setTpOgg(String tpOgg) {
        this.btcParallelism.setBpaTpOgg(tpOgg);
    }

    @Override
    public String getTpOggObj() {
        if (ws.getIndBtcParallelism().getTpOgg() >= 0) {
            return getTpOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpOggObj(String tpOggObj) {
        if (tpOggObj != null) {
            setTpOgg(tpOggObj);
            ws.getIndBtcParallelism().setTpOgg(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setTpOgg(((short)-1));
        }
    }

    @Override
    public String getUserIns() {
        return btcParallelism.getBpaUserIns();
    }

    @Override
    public void setUserIns(String userIns) {
        this.btcParallelism.setBpaUserIns(userIns);
    }

    @Override
    public String getUserStart() {
        return btcParallelism.getBpaUserStart();
    }

    @Override
    public void setUserStart(String userStart) {
        this.btcParallelism.setBpaUserStart(userStart);
    }

    @Override
    public String getUserStartObj() {
        if (ws.getIndBtcParallelism().getUserStart() >= 0) {
            return getUserStart();
        }
        else {
            return null;
        }
    }

    @Override
    public void setUserStartObj(String userStartObj) {
        if (userStartObj != null) {
            setUserStart(userStartObj);
            ws.getIndBtcParallelism().setUserStart(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setUserStart(((short)-1));
        }
    }
}
