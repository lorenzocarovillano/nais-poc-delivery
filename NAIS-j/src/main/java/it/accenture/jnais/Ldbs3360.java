package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.OggDerogaStatWfDao;
import it.accenture.jnais.commons.data.to.IOggDerogaStatWf;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs3360Data;
import it.accenture.jnais.ws.OggDerogaIdbsode0;
import it.accenture.jnais.ws.redefines.OdeCodLivAutApprt;
import it.accenture.jnais.ws.redefines.OdeIdMoviChiu;

/**Original name: LDBS3360<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs3360 extends Program implements IOggDerogaStatWf {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private OggDerogaStatWfDao oggDerogaStatWfDao = new OggDerogaStatWfDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs3360Data ws = new Ldbs3360Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: OGG-DEROGA
    private OggDerogaIdbsode0 oggDeroga;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS3360_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, OggDerogaIdbsode0 oggDeroga) {
        this.idsv0003 = idsv0003;
        this.oggDeroga = oggDeroga;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV3361.
        ws.getLdbv3361().setLdbv3361Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: MOVE LDBV3361 TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getLdbv3361().getLdbv3361Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs3360 getInstance() {
        return ((Ldbs3360)Programs.getInstance(Ldbs3360.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS3360'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS3360");
        // COB_CODE: MOVE 'OGG-DEROGA' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("OGG-DEROGA");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     B.ID_OGG_DEROGA
        //                    ,B.ID_MOVI_CRZ
        //                    ,B.ID_MOVI_CHIU
        //                    ,B.DT_INI_EFF
        //                    ,B.DT_END_EFF
        //                    ,B.COD_COMP_ANIA
        //                    ,B.ID_OGG
        //                    ,B.TP_OGG
        //                    ,B.IB_OGG
        //                    ,B.TP_DEROGA
        //                    ,B.COD_GR_AUT_APPRT
        //                    ,B.COD_LIV_AUT_APPRT
        //                    ,B.COD_GR_AUT_SUP
        //                    ,B.COD_LIV_AUT_SUP
        //                    ,B.DS_RIGA
        //                    ,B.DS_OPER_SQL
        //                    ,B.DS_VER
        //                    ,B.DS_TS_INI_CPTZ
        //                    ,B.DS_TS_END_CPTZ
        //                    ,B.DS_UTENTE
        //                    ,B.DS_STATO_ELAB
        //              FROM OGG_DEROGA B,
        //                   STAT_OGG_WF A
        //              WHERE  B.ID_OGG = :LDBV3361-ID-OGG
        //                    AND B.TP_OGG = :LDBV3361-TP-OGG
        //                    AND A.ID_OGG = B.ID_OGG_DEROGA
        //                    AND A.TP_OGG = 'DE'
        //                    AND A.FL_STAT_END = '3'
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //              ORDER BY B.DS_TS_INI_CPTZ
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     B.ID_OGG_DEROGA
        //                    ,B.ID_MOVI_CRZ
        //                    ,B.ID_MOVI_CHIU
        //                    ,B.DT_INI_EFF
        //                    ,B.DT_END_EFF
        //                    ,B.COD_COMP_ANIA
        //                    ,B.ID_OGG
        //                    ,B.TP_OGG
        //                    ,B.IB_OGG
        //                    ,B.TP_DEROGA
        //                    ,B.COD_GR_AUT_APPRT
        //                    ,B.COD_LIV_AUT_APPRT
        //                    ,B.COD_GR_AUT_SUP
        //                    ,B.COD_LIV_AUT_SUP
        //                    ,B.DS_RIGA
        //                    ,B.DS_OPER_SQL
        //                    ,B.DS_VER
        //                    ,B.DS_TS_INI_CPTZ
        //                    ,B.DS_TS_END_CPTZ
        //                    ,B.DS_UTENTE
        //                    ,B.DS_STATO_ELAB
        //             INTO
        //                :ODE-ID-OGG-DEROGA
        //               ,:ODE-ID-MOVI-CRZ
        //               ,:ODE-ID-MOVI-CHIU
        //                :IND-ODE-ID-MOVI-CHIU
        //               ,:ODE-DT-INI-EFF-DB
        //               ,:ODE-DT-END-EFF-DB
        //               ,:ODE-COD-COMP-ANIA
        //               ,:ODE-ID-OGG
        //               ,:ODE-TP-OGG
        //               ,:ODE-IB-OGG
        //                :IND-ODE-IB-OGG
        //               ,:ODE-TP-DEROGA
        //               ,:ODE-COD-GR-AUT-APPRT
        //               ,:ODE-COD-LIV-AUT-APPRT
        //                :IND-ODE-COD-LIV-AUT-APPRT
        //               ,:ODE-COD-GR-AUT-SUP
        //               ,:ODE-COD-LIV-AUT-SUP
        //               ,:ODE-DS-RIGA
        //               ,:ODE-DS-OPER-SQL
        //               ,:ODE-DS-VER
        //               ,:ODE-DS-TS-INI-CPTZ
        //               ,:ODE-DS-TS-END-CPTZ
        //               ,:ODE-DS-UTENTE
        //               ,:ODE-DS-STATO-ELAB
        //             FROM OGG_DEROGA B,
        //                  STAT_OGG_WF A
        //             WHERE  B.ID_OGG = :LDBV3361-ID-OGG
        //                    AND B.TP_OGG = :LDBV3361-TP-OGG
        //                    AND A.ID_OGG = B.ID_OGG_DEROGA
        //                    AND A.TP_OGG = 'DE'
        //                    AND A.FL_STAT_END = '3'
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //              ORDER BY B.DS_TS_INI_CPTZ
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        oggDerogaStatWfDao.selectRec(ws.getLdbv3361().getLdbv3361IdOgg(), ws.getLdbv3361().getLdbv3361TpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        oggDerogaStatWfDao.openCEff17(ws.getLdbv3361().getLdbv3361IdOgg(), ws.getLdbv3361().getLdbv3361TpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        oggDerogaStatWfDao.closeCEff17();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :ODE-ID-OGG-DEROGA
        //               ,:ODE-ID-MOVI-CRZ
        //               ,:ODE-ID-MOVI-CHIU
        //                :IND-ODE-ID-MOVI-CHIU
        //               ,:ODE-DT-INI-EFF-DB
        //               ,:ODE-DT-END-EFF-DB
        //               ,:ODE-COD-COMP-ANIA
        //               ,:ODE-ID-OGG
        //               ,:ODE-TP-OGG
        //               ,:ODE-IB-OGG
        //                :IND-ODE-IB-OGG
        //               ,:ODE-TP-DEROGA
        //               ,:ODE-COD-GR-AUT-APPRT
        //               ,:ODE-COD-LIV-AUT-APPRT
        //                :IND-ODE-COD-LIV-AUT-APPRT
        //               ,:ODE-COD-GR-AUT-SUP
        //               ,:ODE-COD-LIV-AUT-SUP
        //               ,:ODE-DS-RIGA
        //               ,:ODE-DS-OPER-SQL
        //               ,:ODE-DS-VER
        //               ,:ODE-DS-TS-INI-CPTZ
        //               ,:ODE-DS-TS-END-CPTZ
        //               ,:ODE-DS-UTENTE
        //               ,:ODE-DS-STATO-ELAB
        //           END-EXEC.
        oggDerogaStatWfDao.fetchCEff17(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     B.ID_OGG_DEROGA
        //                    ,B.ID_MOVI_CRZ
        //                    ,B.ID_MOVI_CHIU
        //                    ,B.DT_INI_EFF
        //                    ,B.DT_END_EFF
        //                    ,B.COD_COMP_ANIA
        //                    ,B.ID_OGG
        //                    ,B.TP_OGG
        //                    ,B.IB_OGG
        //                    ,B.TP_DEROGA
        //                    ,B.COD_GR_AUT_APPRT
        //                    ,B.COD_LIV_AUT_APPRT
        //                    ,B.COD_GR_AUT_SUP
        //                    ,B.COD_LIV_AUT_SUP
        //                    ,B.DS_RIGA
        //                    ,B.DS_OPER_SQL
        //                    ,B.DS_VER
        //                    ,B.DS_TS_INI_CPTZ
        //                    ,B.DS_TS_END_CPTZ
        //                    ,B.DS_UTENTE
        //                    ,B.DS_STATO_ELAB
        //              FROM OGG_DEROGA B,
        //                   STAT_OGG_WF A
        //              WHERE  B.ID_OGG = :LDBV3361-ID-OGG
        //                        AND B.TP_OGG = :LDBV3361-TP-OGG
        //                        AND A.ID_OGG = B.ID_OGG_DEROGA
        //                        AND A.TP_OGG = 'DE'
        //                        AND A.FL_STAT_END = '3'
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY B.DS_TS_INI_CPTZ
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     B.ID_OGG_DEROGA
        //                    ,B.ID_MOVI_CRZ
        //                    ,B.ID_MOVI_CHIU
        //                    ,B.DT_INI_EFF
        //                    ,B.DT_END_EFF
        //                    ,B.COD_COMP_ANIA
        //                    ,B.ID_OGG
        //                    ,B.TP_OGG
        //                    ,B.IB_OGG
        //                    ,B.TP_DEROGA
        //                    ,B.COD_GR_AUT_APPRT
        //                    ,B.COD_LIV_AUT_APPRT
        //                    ,B.COD_GR_AUT_SUP
        //                    ,B.COD_LIV_AUT_SUP
        //                    ,B.DS_RIGA
        //                    ,B.DS_OPER_SQL
        //                    ,B.DS_VER
        //                    ,B.DS_TS_INI_CPTZ
        //                    ,B.DS_TS_END_CPTZ
        //                    ,B.DS_UTENTE
        //                    ,B.DS_STATO_ELAB
        //             INTO
        //                :ODE-ID-OGG-DEROGA
        //               ,:ODE-ID-MOVI-CRZ
        //               ,:ODE-ID-MOVI-CHIU
        //                :IND-ODE-ID-MOVI-CHIU
        //               ,:ODE-DT-INI-EFF-DB
        //               ,:ODE-DT-END-EFF-DB
        //               ,:ODE-COD-COMP-ANIA
        //               ,:ODE-ID-OGG
        //               ,:ODE-TP-OGG
        //               ,:ODE-IB-OGG
        //                :IND-ODE-IB-OGG
        //               ,:ODE-TP-DEROGA
        //               ,:ODE-COD-GR-AUT-APPRT
        //               ,:ODE-COD-LIV-AUT-APPRT
        //                :IND-ODE-COD-LIV-AUT-APPRT
        //               ,:ODE-COD-GR-AUT-SUP
        //               ,:ODE-COD-LIV-AUT-SUP
        //               ,:ODE-DS-RIGA
        //               ,:ODE-DS-OPER-SQL
        //               ,:ODE-DS-VER
        //               ,:ODE-DS-TS-INI-CPTZ
        //               ,:ODE-DS-TS-END-CPTZ
        //               ,:ODE-DS-UTENTE
        //               ,:ODE-DS-STATO-ELAB
        //             FROM OGG_DEROGA B,
        //                  STAT_OGG_WF A
        //             WHERE  B.ID_OGG = :LDBV3361-ID-OGG
        //                    AND B.TP_OGG = :LDBV3361-TP-OGG
        //                    AND A.ID_OGG = B.ID_OGG_DEROGA
        //                    AND A.TP_OGG = 'DE'
        //                    AND A.FL_STAT_END = '3'
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY B.DS_TS_INI_CPTZ
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        oggDerogaStatWfDao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        oggDerogaStatWfDao.openCCpz17(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        oggDerogaStatWfDao.closeCCpz17();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :ODE-ID-OGG-DEROGA
        //               ,:ODE-ID-MOVI-CRZ
        //               ,:ODE-ID-MOVI-CHIU
        //                :IND-ODE-ID-MOVI-CHIU
        //               ,:ODE-DT-INI-EFF-DB
        //               ,:ODE-DT-END-EFF-DB
        //               ,:ODE-COD-COMP-ANIA
        //               ,:ODE-ID-OGG
        //               ,:ODE-TP-OGG
        //               ,:ODE-IB-OGG
        //                :IND-ODE-IB-OGG
        //               ,:ODE-TP-DEROGA
        //               ,:ODE-COD-GR-AUT-APPRT
        //               ,:ODE-COD-LIV-AUT-APPRT
        //                :IND-ODE-COD-LIV-AUT-APPRT
        //               ,:ODE-COD-GR-AUT-SUP
        //               ,:ODE-COD-LIV-AUT-SUP
        //               ,:ODE-DS-RIGA
        //               ,:ODE-DS-OPER-SQL
        //               ,:ODE-DS-VER
        //               ,:ODE-DS-TS-INI-CPTZ
        //               ,:ODE-DS-TS-END-CPTZ
        //               ,:ODE-DS-UTENTE
        //               ,:ODE-DS-STATO-ELAB
        //           END-EXEC.
        oggDerogaStatWfDao.fetchCCpz17(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-ODE-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO ODE-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndOggDeroga().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ODE-ID-MOVI-CHIU-NULL
            oggDeroga.getOdeIdMoviChiu().setOdeIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OdeIdMoviChiu.Len.ODE_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-ODE-IB-OGG = -1
        //              MOVE HIGH-VALUES TO ODE-IB-OGG-NULL
        //           END-IF
        if (ws.getIndOggDeroga().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ODE-IB-OGG-NULL
            oggDeroga.setOdeIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OggDerogaIdbsode0.Len.ODE_IB_OGG));
        }
        // COB_CODE: IF IND-ODE-COD-LIV-AUT-APPRT = -1
        //              MOVE HIGH-VALUES TO ODE-COD-LIV-AUT-APPRT-NULL
        //           END-IF.
        if (ws.getIndOggDeroga().getCodLivAutApprt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ODE-COD-LIV-AUT-APPRT-NULL
            oggDeroga.getOdeCodLivAutApprt().setOdeCodLivAutApprtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OdeCodLivAutApprt.Len.ODE_COD_LIV_AUT_APPRT_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE ODE-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvode3().getOdeDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO ODE-DT-INI-EFF
        oggDeroga.setOdeDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE ODE-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvode3().getOdeDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO ODE-DT-END-EFF.
        oggDeroga.setOdeDtEndEff(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return oggDeroga.getOdeCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.oggDeroga.setOdeCodCompAnia(codCompAnia);
    }

    @Override
    public long getCodGrAutApprt() {
        return oggDeroga.getOdeCodGrAutApprt();
    }

    @Override
    public void setCodGrAutApprt(long codGrAutApprt) {
        this.oggDeroga.setOdeCodGrAutApprt(codGrAutApprt);
    }

    @Override
    public long getCodGrAutSup() {
        return oggDeroga.getOdeCodGrAutSup();
    }

    @Override
    public void setCodGrAutSup(long codGrAutSup) {
        this.oggDeroga.setOdeCodGrAutSup(codGrAutSup);
    }

    @Override
    public int getCodLivAutApprt() {
        return oggDeroga.getOdeCodLivAutApprt().getOdeCodLivAutApprt();
    }

    @Override
    public void setCodLivAutApprt(int codLivAutApprt) {
        this.oggDeroga.getOdeCodLivAutApprt().setOdeCodLivAutApprt(codLivAutApprt);
    }

    @Override
    public Integer getCodLivAutApprtObj() {
        if (ws.getIndOggDeroga().getCodLivAutApprt() >= 0) {
            return ((Integer)getCodLivAutApprt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodLivAutApprtObj(Integer codLivAutApprtObj) {
        if (codLivAutApprtObj != null) {
            setCodLivAutApprt(((int)codLivAutApprtObj));
            ws.getIndOggDeroga().setCodLivAutApprt(((short)0));
        }
        else {
            ws.getIndOggDeroga().setCodLivAutApprt(((short)-1));
        }
    }

    @Override
    public int getCodLivAutSup() {
        return oggDeroga.getOdeCodLivAutSup();
    }

    @Override
    public void setCodLivAutSup(int codLivAutSup) {
        this.oggDeroga.setOdeCodLivAutSup(codLivAutSup);
    }

    @Override
    public char getDsOperSql() {
        return oggDeroga.getOdeDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.oggDeroga.setOdeDsOperSql(dsOperSql);
    }

    @Override
    public long getDsRiga() {
        return oggDeroga.getOdeDsRiga();
    }

    @Override
    public void setDsRiga(long dsRiga) {
        this.oggDeroga.setOdeDsRiga(dsRiga);
    }

    @Override
    public char getDsStatoElab() {
        return oggDeroga.getOdeDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.oggDeroga.setOdeDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return oggDeroga.getOdeDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.oggDeroga.setOdeDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return oggDeroga.getOdeDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.oggDeroga.setOdeDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return oggDeroga.getOdeDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.oggDeroga.setOdeDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return oggDeroga.getOdeDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.oggDeroga.setOdeDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getIdbvode3().getOdeDtEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getIdbvode3().setOdeDtEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getIdbvode3().getOdeDtIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getIdbvode3().setOdeDtIniEffDb(dtIniEffDb);
    }

    @Override
    public String getIbOgg() {
        return oggDeroga.getOdeIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.oggDeroga.setOdeIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndOggDeroga().getIbOgg() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndOggDeroga().setIbOgg(((short)0));
        }
        else {
            ws.getIndOggDeroga().setIbOgg(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return oggDeroga.getOdeIdMoviChiu().getOdeIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.oggDeroga.getOdeIdMoviChiu().setOdeIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndOggDeroga().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndOggDeroga().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndOggDeroga().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return oggDeroga.getOdeIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.oggDeroga.setOdeIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdOgg() {
        return oggDeroga.getOdeIdOgg();
    }

    @Override
    public void setIdOgg(int idOgg) {
        this.oggDeroga.setOdeIdOgg(idOgg);
    }

    @Override
    public int getIdOggDeroga() {
        return oggDeroga.getOdeIdOggDeroga();
    }

    @Override
    public void setIdOggDeroga(int idOggDeroga) {
        this.oggDeroga.setOdeIdOggDeroga(idOggDeroga);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public int getLdbv3361IdOgg() {
        return ws.getLdbv3361().getLdbv3361IdOgg();
    }

    @Override
    public void setLdbv3361IdOgg(int ldbv3361IdOgg) {
        this.ws.getLdbv3361().setLdbv3361IdOgg(ldbv3361IdOgg);
    }

    @Override
    public String getLdbv3361TpOgg() {
        return ws.getLdbv3361().getLdbv3361TpOgg();
    }

    @Override
    public void setLdbv3361TpOgg(String ldbv3361TpOgg) {
        this.ws.getLdbv3361().setLdbv3361TpOgg(ldbv3361TpOgg);
    }

    @Override
    public String getTpDeroga() {
        return oggDeroga.getOdeTpDeroga();
    }

    @Override
    public void setTpDeroga(String tpDeroga) {
        this.oggDeroga.setOdeTpDeroga(tpDeroga);
    }

    @Override
    public String getTpOgg() {
        return oggDeroga.getOdeTpOgg();
    }

    @Override
    public void setTpOgg(String tpOgg) {
        this.oggDeroga.setOdeTpOgg(tpOgg);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
