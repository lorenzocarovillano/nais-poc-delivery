package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0501OutputLvvs3480;
import it.accenture.jnais.copy.Idsv0502;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Ivvc0501Output;
import it.accenture.jnais.ws.Lvvs3480Data;
import static java.lang.Math.abs;

/**Original name: LVVS3480<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2018.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 * *************************************************************</pre>*/
public class Lvvs3480 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs3480Data ws = new Lvvs3480Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS3480
    private Ivvc0213 ivvc0213;
    //Original name: IVVC0501-OUTPUT
    private Ivvc0501Output ivvc0501Output;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS3480_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213, Ivvc0501Output ivvc0501Output) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        this.ivvc0501Output = ivvc0501Output;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000.
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs3480 getInstance() {
        return ((Lvvs3480)Programs.getInstance(Lvvs3480.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IVVC0501-OUTPUT
        initIvvc0501Output();
        // COB_CODE: INITIALIZE                        IDSV0501-OUTPUT
        initIdsv0501Output();
        // COB_CODE: INITIALIZE                        IDSV0501-INPUT
        initIdsv0501Input();
        // COB_CODE: MOVE ZEROES                       TO IX-TAB-0501
        ws.getIxIndici().setTab0501(((short)0));
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        //    MOVE IVVC0213-TIPO-MOVI-ORIG
        //      TO WS-MOVIMENTO.
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        //--> VALORIZZA LA VARIABILE A LISTA
        // COB_CODE: PERFORM VARYING IX-TAB-CDG FROM 1 BY 1
        //                     UNTIL IX-TAB-CDG > WCDG-ELE-MAX-CDG-VV
        //               END-IF
        //           END-PERFORM
        ws.getIxIndici().setTabCdg(((short)1));
        while (!(ws.getIxIndici().getTabCdg() > ws.getIvvc0224().getEleMaxCdgVv())) {
            // COB_CODE: IF IX-TAB-0501 < LIMITE-ARRAY-INPUT
            //                TO IDSV0501-VAL-STR(IX-TAB-0501)
            //           ELSE
            //               SET IDSV0003-INVALID-OPER       TO TRUE
            //           END-IF
            if (ws.getIxIndici().getTab0501() < ws.getIdsv0502().getLimiteArrayInput()) {
                // COB_CODE: ADD 1               TO IX-TAB-0501
                ws.getIxIndici().setTab0501(Trunc.toShort(1 + ws.getIxIndici().getTab0501(), 4));
                // COB_CODE: MOVE 'L'            TO IDSV0501-TP-DATO(IX-TAB-0501)
                ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501TpDatoFormatted("L");
                // COB_CODE: MOVE WCDG-COD-FND-VV(IX-TAB-CDG)
                //             TO IDSV0501-VAL-STR(IX-TAB-0501)
                ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501ValStr(ws.getIvvc0224().getFndXCdgVv(ws.getIxIndici().getTabCdg()).getCodFndVv());
            }
            else {
                // COB_CODE: MOVE 'VARLIST'
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe("VARLIST");
                // COB_CODE: MOVE 'LVVS3480 - LIMITE-ARRAY-INPUT SUPERATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("LVVS3480 - LIMITE-ARRAY-INPUT SUPERATO");
                // COB_CODE: SET IDSV0003-INVALID-OPER       TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IX-TAB-0501 < LIMITE-ARRAY-INPUT
            //                TO IDSV0501-VAL-PERC(IX-TAB-0501)
            //           ELSE
            //               SET IDSV0003-INVALID-OPER       TO TRUE
            //           END-IF
            if (ws.getIxIndici().getTab0501() < ws.getIdsv0502().getLimiteArrayInput()) {
                // COB_CODE: ADD 1               TO IX-TAB-0501
                ws.getIxIndici().setTab0501(Trunc.toShort(1 + ws.getIxIndici().getTab0501(), 4));
                //           MOVE 'R'            TO IDSV0501-TP-DATO(IX-TAB-0501)
                // COB_CODE: MOVE 'T'            TO IDSV0501-TP-DATO(IX-TAB-0501)
                ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501TpDatoFormatted("T");
                //             TO IDSV0501-VAL-IMP(IX-TAB-0501)
                // COB_CODE:             MOVE WCDG-CDG-REST-VV(IX-TAB-CDG)
                //           *             TO IDSV0501-VAL-IMP(IX-TAB-0501)
                //                         TO IDSV0501-VAL-PERC(IX-TAB-0501)
                ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501ValPerc(TruncAbs.toDecimal(ws.getIvvc0224().getFndXCdgVv(ws.getIxIndici().getTabCdg()).getCdgRestVv(), 18, 9));
            }
            else {
                // COB_CODE: MOVE 'VARLIST'
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe("VARLIST");
                // COB_CODE: MOVE 'LVVS3480 - LIMITE-ARRAY-INPUT SUPERATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("LVVS3480 - LIMITE-ARRAY-INPUT SUPERATO");
                // COB_CODE: SET IDSV0003-INVALID-OPER       TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            ws.getIxIndici().setTabCdg(Trunc.toShort(ws.getIxIndici().getTabCdg() + 1, 4));
        }
        //    SE NON HO NESSUN ELEMENTO, INIZIALIZZO LA VARIABILE
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              TO IVVC0213-COD-VARIABILE-O.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         IF IX-TAB-0501 = ZEROES
            //                         THRU S1360-EX
            //           *       END-IF
            //                END-IF
            if (ws.getIxIndici().getTab0501() == 0) {
                // COB_CODE: PERFORM S1360-INIT-VARIABILE
                //              THRU S1360-EX
                s1360InitVariabile();
                //       END-IF
            }
            //--> VALORIZZA LA VARIABILE A LISTA
            // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
            //           *       IF FL-LIQUI-SI
            //                         THRU S1400-EX
            //           *       END-IF
            //                END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                //       IF FL-LIQUI-SI
                // COB_CODE: PERFORM S1400-VALORIZZA-VARIABILE
                //              THRU S1400-EX
                s1400ValorizzaVariabile();
                //       END-IF
            }
            // COB_CODE: MOVE IVVC0213-COD-VARIABILE
            //              TO IVVC0213-COD-VARIABILE-O.
            ivvc0213.getTabOutput().setCodVariabileO(ivvc0213.getDatiLivello().getCodVariabile());
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-AREA-FND-X-CDG
        //                TO WCDG-AREA-COMMIS-GEST-VV
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasAreaFndXCdg())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO WCDG-AREA-COMMIS-GEST-VV
            ws.getIvvc0224().setAreaCommisGestVvFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1360-INIT-VARIABILE<br>
	 * <pre>----------------------------------------------------------------*
	 *    INIZIALIZZO LA VARIABILE SE NON VALORIZZATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1360InitVariabile() {
        // COB_CODE: MOVE 1                      TO IX-TAB-0501.
        ws.getIxIndici().setTab0501(((short)1));
        // COB_CODE: MOVE 'L'                    TO IDSV0501-TP-DATO(IX-TAB-0501).
        ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501TpDatoFormatted("L");
        // COB_CODE: MOVE SPACES                 TO IDSV0501-VAL-STR(IX-TAB-0501).
        ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501ValStr("");
        // COB_CODE: MOVE 2                      TO IX-TAB-0501.
        ws.getIxIndici().setTab0501(((short)2));
        //    MOVE 'R'                    TO IDSV0501-TP-DATO(IX-TAB-0501).
        // COB_CODE: MOVE 'T'                    TO IDSV0501-TP-DATO(IX-TAB-0501).
        ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501TpDatoFormatted("T");
        //    MOVE ZEROES                 TO IDSV0501-VAL-IMP(IX-TAB-0501).
        // COB_CODE: MOVE ZEROES                TO IDSV0501-VAL-PERC(IX-TAB-0501).
        ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501ValPerc(new AfDecimal(0, 18, 9));
    }

    /**Original name: S1400-VALORIZZA-VARIABILE<br>
	 * <pre>----------------------------------------------------------------*
	 *    VALORIZZO LA VARIABILE A LISTA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1400ValorizzaVariabile() {
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC  TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM STRINGA-X-VALORIZZATORE
            //              THRU STRINGA-X-VALORIZZATORE-EX
            stringaXValorizzatore();
            // COB_CODE: IF IDSV0501-SUCCESSFUL-RC
            //                TO IVVC0501-OUTPUT
            //           ELSE
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           END-IF
            if (ws.getIdsv0502().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc()) {
                // COB_CODE: MOVE IDSV0501-OUTPUT
                //             TO IVVC0501-OUTPUT
                ivvc0501Output.setIvvc0501OutputBytes(ws.getIdsv0502().getIdsv0501Output().getIdsv0501OutputBytes());
            }
            else {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: STRINGA-X-VALORIZZATORE<br>
	 * <pre>----------------------------------------------------------------*
	 *    CODICE PER STRINGATURA VARIABILI
	 * ----------------------------------------------------------------*
	 * *************************************************************</pre>*/
    private void stringaXValorizzatore() {
        // COB_CODE: SET IDSV0501-SUCCESSFUL-RC TO TRUE
        ws.getIdsv0502().getIdsv0501ReturnCode().setIdsv0501SuccessfulRc();
        // COB_CODE: MOVE SPACES                TO IDSV0501-DESCRIZ-ERR
        ws.getIdsv0502().setIdsv0501DescrizErr("");
        // COB_CODE: SET IDSV0501-SEGNO-POSTIV-NO TO TRUE
        ws.getIdsv0502().getIdsv0501SegnoPostiv().setIdsv0501SegnoPostivNo();
        // COB_CODE: SET IDSV0501-SEGNO-ANTERIORE TO TRUE
        ws.getIdsv0502().getIdsv0501PosSegno().setIdsv0501SegnoAnteriore();
        // COB_CODE: MOVE 3               TO IDSV0501-DECIMALI-ESPOSTI-IMP
        ws.getIdsv0502().setIdsv0501DecimaliEspostiImp(((short)3));
        // COB_CODE: MOVE 3               TO IDSV0501-DECIMALI-ESPOSTI-PERC
        ws.getIdsv0502().setIdsv0501DecimaliEspostiPerc(((short)3));
        // COB_CODE: MOVE 9               TO IDSV0501-DECIMALI-ESPOSTI-TASS
        ws.getIdsv0502().setIdsv0501DecimaliEspostiTass(((short)9));
        // COB_CODE: MOVE ';'             TO IDSV0501-SEPARATORE
        ws.getIdsv0502().setIdsv0501SeparatoreFormatted(";");
        // COB_CODE: MOVE '.'             TO IDSV0501-SIMBOLO-DECIMALE
        ws.getIdsv0502().setIdsv0501SimboloDecimaleFormatted(".");
        // COB_CODE: PERFORM CONVERTI-FORMATI THRU CONVERTI-FORMATI-EX
        convertiFormati();
        // COB_CODE: IF IDSV0501-SUCCESSFUL-RC
        //              PERFORM CALL-STRINGATURA THRU CALL-STRINGATURA-EX
        //           END-IF.
        if (ws.getIdsv0502().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc()) {
            // COB_CODE: PERFORM CALL-STRINGATURA THRU CALL-STRINGATURA-EX
            callStringatura();
        }
    }

    /**Original name: CONVERTI-FORMATI<br>
	 * <pre>*************************************************************</pre>*/
    private void convertiFormati() {
        // COB_CODE: PERFORM VARYING IND-VAR FROM 1 BY 1
        //                     UNTIL IND-VAR > LIMITE-ARRAY-INPUT OR
        //                           IDSV0501-TP-DATO(IND-VAR) =
        //                           SPACES OR LOW-VALUES OR HIGH-VALUES
        //                    END-EVALUATE
        //           END-PERFORM.
        ConcatUtil concatUtil = null;
        ws.getIdsv0502().setIndVar(((short)1));
        while (!(ws.getIdsv0502().getIndVar() > ws.getIdsv0502().getLimiteArrayInput() || Conditions.eq(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501TpDato(), Types.SPACE_CHAR) || Conditions.eq(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501TpDato(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501TpDato(), Types.HIGH_CHAR_VAL))) {
            // COB_CODE: EVALUATE IDSV0501-TP-DATO(IND-VAR)
            //              WHEN 'R'
            //                    MOVE IMPORTO  TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'Z'
            //                   MOVE MILLESIMI TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'O'
            //                                  TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'T'
            //                   MOVE TASSO     TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'L'
            //                   MOVE STRINGA   TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'C'
            //                   MOVE DT        TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'H'
            //                   MOVE NUMERICO  TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN OTHER
            //                   END-STRING
            //           END-EVALUATE
            switch (ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501TpDato()) {

                case 'R':// COB_CODE: MOVE IMPORTO  TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).setTpDatoParallelo(ws.getIdsv0502().getTipoFormato().getImporto());
                    break;

                case 'Z':// COB_CODE: MOVE MILLESIMI TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).setTpDatoParallelo(ws.getIdsv0502().getTipoFormato().getMillesimi());
                    break;

                case 'O':// COB_CODE: MOVE PERCENTUALE
                    //                          TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).setTpDatoParallelo(ws.getIdsv0502().getTipoFormato().getPercentuale());
                    break;

                case 'T':// COB_CODE: MOVE TASSO     TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).setTpDatoParallelo(ws.getIdsv0502().getTipoFormato().getTasso());
                    break;

                case 'L':// COB_CODE: MOVE STRINGA   TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).setTpDatoParallelo(ws.getIdsv0502().getTipoFormato().getStringa());
                    break;

                case 'C':// COB_CODE: MOVE DT        TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).setTpDatoParallelo(ws.getIdsv0502().getTipoFormato().getDt());
                    break;

                case 'H':// COB_CODE: MOVE NUMERICO  TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).setTpDatoParallelo(ws.getIdsv0502().getTipoFormato().getNumerico());
                    break;

                default:// COB_CODE: SET IDSV0501-GENERIC-ERROR  TO TRUE
                    ws.getIdsv0502().getIdsv0501ReturnCode().setIdsv0501GenericError();
                    // COB_CODE: STRING 'TIPO DATO LISTA NON VALIDO  : '''
                    //                  IDSV0501-TP-DATO(IND-VAR)
                    //                  ''' SU OCCORRENZA : '
                    //                  IND-VAR
                    //                  DELIMITED BY SIZE
                    //                  INTO IDSV0501-DESCRIZ-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0502.Len.IDSV0501_DESCRIZ_ERR, "TIPO DATO LISTA NON VALIDO  : '", String.valueOf(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501TpDato()), "' SU OCCORRENZA : ", ws.getIdsv0502().getIndVarAsString());
                    ws.getIdsv0502().setIdsv0501DescrizErr(concatUtil.replaceInString(ws.getIdsv0502().getIdsv0501DescrizErrFormatted()));
                    break;
            }
            ws.getIdsv0502().setIndVar(Trunc.toShort(ws.getIdsv0502().getIndVar() + 1, 2));
        }
    }

    /**Original name: CALL-STRINGATURA<br>
	 * <pre>*************************************************************</pre>*/
    private void callStringatura() {
        // COB_CODE: PERFORM INITIALIZE-CAMPI THRU INITIALIZE-CAMPI-EX
        initializeCampi();
        // COB_CODE: PERFORM ELABORA-DATI     THRU ELABORA-DATI-EX.
        elaboraDati();
    }

    /**Original name: INITIALIZE-CAMPI<br>
	 * <pre>*************************************************************</pre>*/
    private void initializeCampi() {
        // COB_CODE: SET SENZA-SEGNO                 TO TRUE.
        ws.getIdsv0502().getTipoSegno().setSenzaSegno();
        // COB_CODE: SET DATO-INPUT-TROVATO-NO       TO TRUE.
        ws.getIdsv0502().getDatoInputTrovato().setDatoInputTrovatoNo();
        // COB_CODE: SET PRIMA-VOLTA-SI              TO TRUE.
        ws.getIdsv0502().getPrimaVolta().setPrimaVoltaSi();
        // COB_CODE: INITIALIZE                         IDSV0501-OUTPUT
        //                                              CAMPO-ALFA
        //                                              CAMPO-INTERI
        //                                              CAMPO-DECIMALI.
        initIdsv0501Output();
        ws.getIdsv0502().getCampoAlfa().setCampoAlfa("");
        ws.getIdsv0502().getCampoInteri().setCampoInteri("");
        ws.getIdsv0502().getCampoDecimali().setCampoDecimali("");
        // COB_CODE: MOVE 1                          TO IND-OUT
        //                                              POSIZIONE
        //                                              IDSV0501-MAX-TAB-STR.
        ws.getIdsv0502().setIndOut(((short)1));
        ws.getIdsv0502().setPosizione(((short)1));
        ws.getIdsv0502().getIdsv0501Output().setMaxTabStr(((short)1));
        // COB_CODE: MOVE LIMITE-STRINGA             TO SPAZIO-RESTANTE
        ws.getIdsv0502().setSpazioRestanteFormatted(ws.getIdsv0502().getLimiteStringaFormatted());
        // COB_CODE: MOVE 0                          TO IND-VAR
        //                                              IND-STRINGA
        //                                              IND-INT
        //                                              IND-DEC.
        ws.getIdsv0502().setIndVar(((short)0));
        ws.getIdsv0502().setIndStringa(((short)0));
        ws.getIdsv0502().setIndInt(((short)0));
        ws.getIdsv0502().setIndDec(((short)0));
    }

    /**Original name: ELABORA-DATI<br>
	 * <pre>*************************************************************</pre>*/
    private void elaboraDati() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM VARYING IND-VAR FROM 1 BY 1
        //                     UNTIL IND-VAR > LIMITE-ARRAY-INPUT        OR
        //                           IDSV0501-TP-DATO(IND-VAR) =
        //                           SPACES OR LOW-VALUES OR HIGH-VALUES OR
        //                           NOT IDSV0501-SUCCESSFUL-RC
        //              END-IF
        //           END-PERFORM.
        ws.getIdsv0502().setIndVar(((short)1));
        while (!(ws.getIdsv0502().getIndVar() > ws.getIdsv0502().getLimiteArrayInput() || Conditions.eq(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501TpDato(), Types.SPACE_CHAR) || Conditions.eq(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501TpDato(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501TpDato(), Types.HIGH_CHAR_VAL) || !ws.getIdsv0502().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc())) {
            // COB_CODE: SET DATO-INPUT-TROVATO-SI      TO TRUE
            ws.getIdsv0502().getDatoInputTrovato().setDatoInputTrovatoSi();
            // COB_CODE: SET SENZA-SEGNO                TO TRUE
            ws.getIdsv0502().getTipoSegno().setSenzaSegno();
            // COB_CODE: IF PRIMA-VOLTA-SI
            //                                          TO WK-TP-DATO-PARALLELO
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIdsv0502().getPrimaVolta().isPrimaVoltaSi()) {
                // COB_CODE: SET PRIMA-VOLTA-NO          TO TRUE
                ws.getIdsv0502().getPrimaVolta().setPrimaVoltaNo();
                // COB_CODE: MOVE TP-DATO-PARALLELO(IND-VAR)
                //                                       TO IDSV0501-TP-STRINGA
                ws.getIdsv0502().getIdsv0501Output().setTpStringa(ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo());
                // COB_CODE: MOVE TP-DATO-PARALLELO(IND-VAR)
                //                                       TO WK-TP-DATO-PARALLELO
                ws.getIdsv0502().setWkTpDatoParallelo(ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo());
            }
            else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() != ws.getIdsv0502().getWkTpDatoParallelo()) {
                // COB_CODE:            IF TP-DATO-PARALLELO(IND-VAR) NOT =
                //                         WK-TP-DATO-PARALLELO
                //                         MOVE MISTA               TO IDSV0501-TP-STRINGA
                //                      ELSE
                //           * SIR 20310 GESTIONE LISTE OMOGENEE
                //                         PERFORM LISTA-OMOGENEA   THRU LISTA-OMOGENEA-EX
                //                      END-IF
                // COB_CODE: MOVE MISTA               TO IDSV0501-TP-STRINGA
                ws.getIdsv0502().getIdsv0501Output().setTpStringa(ws.getIdsv0502().getMista());
            }
            else {
                // SIR 20310 GESTIONE LISTE OMOGENEE
                // COB_CODE: PERFORM LISTA-OMOGENEA   THRU LISTA-OMOGENEA-EX
                listaOmogenea();
            }
            // COB_CODE: EVALUATE TP-DATO-PARALLELO(IND-VAR)
            //              WHEN IMPORTO
            //                   PERFORM TRATTA-IMPORTO THRU TRATTA-IMPORTO-EX
            //              WHEN NUMERICO
            //                                          THRU TRATTA-NUMERICO-EX
            //              WHEN TASSO
            //              WHEN PERCENTUALE
            //              WHEN MILLESIMI
            //                                          THRU TRATTA-PERCENTUALE-EX
            //              WHEN DT
            //              WHEN STRINGA
            //                                          THRU TRATTA-STRINGA-EX
            //              WHEN OTHER
            //                   END-STRING
            //           END-EVALUATE
            if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getImporto()) {
                // COB_CODE: PERFORM TRATTA-IMPORTO THRU TRATTA-IMPORTO-EX
                trattaImporto();
            }
            else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getNumerico()) {
                // COB_CODE: PERFORM TRATTA-NUMERICO
                //                                  THRU TRATTA-NUMERICO-EX
                trattaNumerico();
            }
            else if ((ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getTasso()) || (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getPercentuale()) || (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getMillesimi())) {
                // COB_CODE: PERFORM TRATTA-PERCENTUALE
                //                                  THRU TRATTA-PERCENTUALE-EX
                trattaPercentuale();
            }
            else if ((ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getDt()) || (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getStringa())) {
                // COB_CODE: PERFORM TRATTA-STRINGA
                //                                  THRU TRATTA-STRINGA-EX
                trattaStringa();
            }
            else {
                // COB_CODE: SET IDSV0501-GENERIC-ERROR  TO TRUE
                ws.getIdsv0502().getIdsv0501ReturnCode().setIdsv0501GenericError();
                // COB_CODE: STRING 'TIPO DATO LISTA NON VALIDO  : '''
                //                  IDSV0501-TP-DATO(IND-VAR)
                //                  ''' SU OCCORRENZA : '
                //                  IND-VAR
                //                  DELIMITED BY SIZE
                //                  INTO IDSV0501-DESCRIZ-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0502.Len.IDSV0501_DESCRIZ_ERR, "TIPO DATO LISTA NON VALIDO  : '", String.valueOf(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501TpDato()), "' SU OCCORRENZA : ", ws.getIdsv0502().getIndVarAsString());
                ws.getIdsv0502().setIdsv0501DescrizErr(concatUtil.replaceInString(ws.getIdsv0502().getIdsv0501DescrizErrFormatted()));
            }
            // COB_CODE: IF IDSV0501-SUCCESSFUL-RC
            //              ADD 1                       TO POSIZIONE
            //           END-IF
            if (ws.getIdsv0502().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc()) {
                // COB_CODE: MOVE IDSV0501-SEPARATORE    TO IDSV0501-STRINGA-TOT
                //                                         (IND-OUT)(POSIZIONE:1)
                ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring(String.valueOf(ws.getIdsv0502().getIdsv0501Separatore()), ws.getIdsv0502().getPosizione(), 1);
                // COB_CODE: ADD 1                       TO POSIZIONE
                ws.getIdsv0502().setPosizione(Trunc.toShort(1 + ws.getIdsv0502().getPosizione(), 2));
            }
            ws.getIdsv0502().setIndVar(Trunc.toShort(ws.getIdsv0502().getIndVar() + 1, 2));
        }
        // COB_CODE: IF IDSV0501-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (ws.getIdsv0502().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc()) {
            // COB_CODE: IF DATO-INPUT-TROVATO-NO
            //              END-STRING
            //           END-IF
            if (ws.getIdsv0502().getDatoInputTrovato().isDatoInputTrovatoNo()) {
                // COB_CODE: SET IDSV0501-GENERIC-ERROR  TO TRUE
                ws.getIdsv0502().getIdsv0501ReturnCode().setIdsv0501GenericError();
                // COB_CODE: STRING 'NESSUN DATO DI INPUT TROVATO'
                //                  DELIMITED BY SIZE
                //                  INTO IDSV0501-DESCRIZ-ERR
                //           END-STRING
                ws.getIdsv0502().setIdsv0501DescrizErr("NESSUN DATO DI INPUT TROVATO");
            }
        }
    }

    /**Original name: TRATTA-IMPORTO<br>
	 * <pre>*************************************************************</pre>*/
    private void trattaImporto() {
        // COB_CODE: MOVE LIM-INT-IMP                 TO LIMITE-INTERI.
        ws.getIdsv0502().setLimiteInteriFormatted(ws.getIdsv0502().getLimIntImpFormatted());
        // COB_CODE: MOVE LIM-DEC-IMP                 TO LIMITE-DECIMALI.
        ws.getIdsv0502().setLimiteDecimaliFormatted(ws.getIdsv0502().getLimDecImpFormatted());
        // COB_CODE: IF IDSV0501-VAL-IMP (IND-VAR) >= 0
        //              SET SEGNO-POSITIVO            TO TRUE
        //           END-IF.
        if (ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501ValImp().compareTo(0) >= 0) {
            // COB_CODE: SET SEGNO-POSITIVO            TO TRUE
            ws.getIdsv0502().getTipoSegno().setSegnoPositivo();
        }
        // COB_CODE: IF IDSV0501-VAL-IMP (IND-VAR) <  0
        //              SET SEGNO-NEGATIVO            TO TRUE
        //           END-IF.
        if (ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501ValImp().compareTo(0) < 0) {
            // COB_CODE: SET SEGNO-NEGATIVO            TO TRUE
            ws.getIdsv0502().getTipoSegno().setSegnoNegativo();
        }
        // COB_CODE: INITIALIZE                       STRUCT-IMP.
        ws.getIdsv0502().getStructNum().setStructImp(new AfDecimal(0, 18, 7));
        // COB_CODE: MOVE IDSV0501-VAL-IMP (IND-VAR)  TO STRUCT-IMP.
        ws.getIdsv0502().getStructNum().setStructImp(TruncAbs.toDecimal(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501ValImp(), 18, 7));
        // COB_CODE: PERFORM ESTRAI-INTERO            THRU ESTRAI-INTERO-EX.
        estraiIntero();
        // COB_CODE: PERFORM ESTRAI-DECIMALI          THRU ESTRAI-DECIMALI-EX.
        estraiDecimali();
        // COB_CODE: PERFORM STRINGA-CAMPO            THRU STRINGA-CAMPO-EX.
        stringaCampo();
        // COB_CODE: IF IDSV0501-SEGNO-POSTERIORE
        //              PERFORM TRATTA-SEGNO          THRU TRATTA-SEGNO-EX
        //           END-IF.
        if (ws.getIdsv0502().getIdsv0501PosSegno().isIdsv0501SegnoPosteriore()) {
            // COB_CODE: PERFORM TRATTA-SEGNO          THRU TRATTA-SEGNO-EX
            trattaSegno();
        }
    }

    /**Original name: TRATTA-NUMERICO<br>
	 * <pre>*************************************************************</pre>*/
    private void trattaNumerico() {
        // COB_CODE: MOVE LIMITE-INT-DEC              TO LIMITE-INTERI.
        ws.getIdsv0502().setLimiteInteriFormatted(ws.getIdsv0502().getLimiteIntDecFormatted());
        // COB_CODE: IF IDSV0501-VAL-NUM (IND-VAR) >= ZEROES
        //              SET SEGNO-POSITIVO            TO TRUE
        //           END-IF.
        if (ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501ValNum() >= 0) {
            // COB_CODE: SET SEGNO-POSITIVO            TO TRUE
            ws.getIdsv0502().getTipoSegno().setSegnoPositivo();
        }
        // COB_CODE: IF IDSV0501-VAL-NUM (IND-VAR) < ZEROES
        //              SET SEGNO-NEGATIVO            TO TRUE
        //           END-IF.
        if (ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501ValNum() < 0) {
            // COB_CODE: SET SEGNO-NEGATIVO            TO TRUE
            ws.getIdsv0502().getTipoSegno().setSegnoNegativo();
        }
        // COB_CODE: INITIALIZE                          STRUCT-NUM.
        ws.getIdsv0502().getStructNum().setStructNum(0);
        // COB_CODE: MOVE IDSV0501-VAL-NUM (IND-VAR)  TO STRUCT-NUM.
        ws.getIdsv0502().getStructNum().setStructNum(TruncAbs.toLong(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501ValNum(), 18));
        // COB_CODE: PERFORM ESTRAI-INTERO            THRU ESTRAI-INTERO-EX.
        estraiIntero();
        // COB_CODE: PERFORM STRINGA-CAMPO            THRU STRINGA-CAMPO-EX.
        stringaCampo();
        // COB_CODE: IF IDSV0501-SEGNO-POSTERIORE
        //              PERFORM TRATTA-SEGNO          THRU TRATTA-SEGNO-EX
        //           END-IF.
        if (ws.getIdsv0502().getIdsv0501PosSegno().isIdsv0501SegnoPosteriore()) {
            // COB_CODE: PERFORM TRATTA-SEGNO          THRU TRATTA-SEGNO-EX
            trattaSegno();
        }
    }

    /**Original name: TRATTA-PERCENTUALE<br>
	 * <pre>*************************************************************</pre>*/
    private void trattaPercentuale() {
        // COB_CODE: MOVE LIM-INT-PERC                TO LIMITE-INTERI.
        ws.getIdsv0502().setLimiteInteriFormatted(ws.getIdsv0502().getLimIntPercFormatted());
        // COB_CODE: MOVE LIM-DEC-PERC                TO LIMITE-DECIMALI.
        ws.getIdsv0502().setLimiteDecimaliFormatted(ws.getIdsv0502().getLimDecPercFormatted());
        // COB_CODE: INITIALIZE                          STRUCT-IMP.
        ws.getIdsv0502().getStructNum().setStructImp(new AfDecimal(0, 18, 7));
        // COB_CODE: MOVE IDSV0501-VAL-PERC (IND-VAR) TO STRUCT-PERC.
        ws.getIdsv0502().getStructNum().setStructPerc(Trunc.toDecimal(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501ValPerc(), 18, 9));
        // COB_CODE: PERFORM ESTRAI-INTERO            THRU ESTRAI-INTERO-EX.
        estraiIntero();
        // COB_CODE: PERFORM ESTRAI-DECIMALI          THRU ESTRAI-DECIMALI-EX.
        estraiDecimali();
        // COB_CODE: PERFORM STRINGA-CAMPO            THRU STRINGA-CAMPO-EX.
        stringaCampo();
    }

    /**Original name: TRATTA-STRINGA<br>
	 * <pre>*************************************************************</pre>*/
    private void trattaStringa() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE                          STRUCT-ALFA.
        ws.getIdsv0502().getStructAlfa().setStructAlfa("");
        // COB_CODE: MOVE IDSV0501-VAL-STR (IND-VAR)  TO STRUCT-ALFA.
        ws.getIdsv0502().getStructAlfa().setStructAlfa(ws.getIdsv0502().getIdsv0501TabVariabili(ws.getIdsv0502().getIndVar()).getIdsv0501ValStr());
        // COB_CODE: PERFORM ESTRAI-ALFANUMERICO      THRU ESTRAI-ALFANUMERICO-EX.
        estraiAlfanumerico();
        // COB_CODE: IF IND-ALFA >= LIMITE-STRINGA
        //              END-STRING
        //           ELSE
        //              PERFORM STRINGA-CAMPO         THRU STRINGA-CAMPO-EX
        //           END-IF.
        if (ws.getIdsv0502().getIndAlfa() >= ws.getIdsv0502().getLimiteStringa()) {
            // COB_CODE: SET IDSV0501-GENERIC-ERROR    TO TRUE
            ws.getIdsv0502().getIdsv0501ReturnCode().setIdsv0501GenericError();
            // COB_CODE: STRING 'SUPERATO IL LIMITE DI VALORE PER STRINGA'
            //                  ' SU OCCORRENZA : '
            //                  IND-VAR
            //                  DELIMITED BY SIZE
            //                  INTO IDSV0501-DESCRIZ-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0502.Len.IDSV0501_DESCRIZ_ERR, "SUPERATO IL LIMITE DI VALORE PER STRINGA", " SU OCCORRENZA : ", ws.getIdsv0502().getIndVarAsString());
            ws.getIdsv0502().setIdsv0501DescrizErr(concatUtil.replaceInString(ws.getIdsv0502().getIdsv0501DescrizErrFormatted()));
        }
        else {
            // COB_CODE: PERFORM STRINGA-CAMPO         THRU STRINGA-CAMPO-EX
            stringaCampo();
        }
    }

    /**Original name: ESTRAI-ALFANUMERICO<br>
	 * <pre>*************************************************************</pre>*/
    private void estraiAlfanumerico() {
        // COB_CODE: SET ALFANUMERICO-TROVATO-NO TO TRUE.
        ws.getIdsv0502().getAlfanumericoTrovato().setAlfanumericoTrovatoNo();
        // COB_CODE: SET FINE-STRINGA-NO         TO TRUE
        ws.getIdsv0502().getFineStringa().setFineStringaNo();
        // COB_CODE: INITIALIZE                  CAMPO-ALFA
        //                                       IND-ALFA.
        ws.getIdsv0502().getCampoAlfa().setCampoAlfa("");
        ws.getIdsv0502().setIndAlfa(((short)0));
        // COB_CODE: PERFORM VARYING IND-STRINGA FROM 1 BY 1
        //                   UNTIL   IND-STRINGA > LIMITE-STRINGA OR
        //                           FINE-STRINGA-SI
        //                      END-IF
        //            END-PERFORM.
        ws.getIdsv0502().setIndStringa(((short)1));
        while (!(ws.getIdsv0502().getIndStringa() > ws.getIdsv0502().getLimiteStringa() || ws.getIdsv0502().getFineStringa().isFineStringaSi())) {
            // COB_CODE: IF ELE-ALFA(IND-STRINGA) =
            //              SPACES OR LOW-VALUE OR HIGH-VALUE
            //              SET FINE-STRINGA-SI         TO TRUE
            //           ELSE
            //                TO ELE-STRINGA-ALFA(IND-ALFA)
            //           END-IF
            if (Conditions.eq(ws.getIdsv0502().getStructAlfa().getEleAlfa(ws.getIdsv0502().getIndStringa()), Types.SPACE_CHAR) || Conditions.eq(ws.getIdsv0502().getStructAlfa().getEleAlfa(ws.getIdsv0502().getIndStringa()), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIdsv0502().getStructAlfa().getEleAlfa(ws.getIdsv0502().getIndStringa()), Types.HIGH_CHAR_VAL)) {
                // COB_CODE: SET FINE-STRINGA-SI         TO TRUE
                ws.getIdsv0502().getFineStringa().setFineStringaSi();
            }
            else {
                // COB_CODE: SET ALFANUMERICO-TROVATO-SI TO TRUE
                ws.getIdsv0502().getAlfanumericoTrovato().setAlfanumericoTrovatoSi();
                // COB_CODE: ADD 1                       TO IND-ALFA
                ws.getIdsv0502().setIndAlfa(Trunc.toShort(1 + ws.getIdsv0502().getIndAlfa(), 2));
                // COB_CODE: MOVE ELE-ALFA(IND-STRINGA)
                //             TO ELE-STRINGA-ALFA(IND-ALFA)
                ws.getIdsv0502().getCampoAlfa().setEleStringaAlfa(ws.getIdsv0502().getIndAlfa(), ws.getIdsv0502().getStructAlfa().getEleAlfa(ws.getIdsv0502().getIndStringa()));
            }
            ws.getIdsv0502().setIndStringa(Trunc.toShort(ws.getIdsv0502().getIndStringa() + 1, 2));
        }
    }

    /**Original name: ESTRAI-INTERO<br>
	 * <pre>*************************************************************</pre>*/
    private void estraiIntero() {
        // COB_CODE: SET INT-TROVATO-NO          TO TRUE.
        ws.getIdsv0502().getIntTrovato().setIntTrovatoNo();
        // COB_CODE: INITIALIZE                      CAMPO-INTERI
        //                                           IND-INT.
        ws.getIdsv0502().getCampoInteri().setCampoInteri("");
        ws.getIdsv0502().setIndInt(((short)0));
        // COB_CODE: PERFORM VARYING IND-STRINGA FROM 1 BY 1
        //                   UNTIL   IND-STRINGA > LIMITE-INT-DEC    OR
        //                           IND-STRINGA > LIMITE-INTERI
        //                      END-IF
        //            END-PERFORM.
        ws.getIdsv0502().setIndStringa(((short)1));
        while (!(ws.getIdsv0502().getIndStringa() > ws.getIdsv0502().getLimiteIntDec() || ws.getIdsv0502().getIndStringa() > ws.getIdsv0502().getLimiteInteri())) {
            // COB_CODE: IF ELE-NUM-IMP(IND-STRINGA)
            //                                     NOT = ZEROES
            //              SET INT-TROVATO-SI     TO TRUE
            //           END-IF
            if (!Conditions.eq(ws.getIdsv0502().getStructNum().getEleNumImp(ws.getIdsv0502().getIndStringa()), '0')) {
                // COB_CODE: SET INT-TROVATO-SI     TO TRUE
                ws.getIdsv0502().getIntTrovato().setIntTrovatoSi();
            }
            // COB_CODE: IF INT-TROVATO-SI
            //                TO ELE-STRINGA-INTERI(IND-INT)
            //           END-IF
            if (ws.getIdsv0502().getIntTrovato().isIntTrovatoSi()) {
                // COB_CODE: ADD 1                  TO IND-INT
                ws.getIdsv0502().setIndInt(Trunc.toShort(1 + ws.getIdsv0502().getIndInt(), 2));
                // COB_CODE: MOVE ELE-NUM-IMP(IND-STRINGA)
                //             TO ELE-STRINGA-INTERI(IND-INT)
                ws.getIdsv0502().getCampoInteri().setEleStringaInteri(ws.getIdsv0502().getIndInt(), ws.getIdsv0502().getStructNum().getEleNumImp(ws.getIdsv0502().getIndStringa()));
            }
            ws.getIdsv0502().setIndStringa(Trunc.toShort(ws.getIdsv0502().getIndStringa() + 1, 2));
        }
        // COB_CODE: IF INT-TROVATO-NO
        //              MOVE 1                 TO  IND-INT
        //           END-IF.
        if (ws.getIdsv0502().getIntTrovato().isIntTrovatoNo()) {
            // COB_CODE: MOVE 1                 TO  IND-INT
            ws.getIdsv0502().setIndInt(((short)1));
        }
    }

    /**Original name: ESTRAI-DECIMALI<br>
	 * <pre>*************************************************************</pre>*/
    private void estraiDecimali() {
        // COB_CODE: INITIALIZE                      CAMPO-DECIMALI
        //                                           IND-DEC.
        ws.getIdsv0502().getCampoDecimali().setCampoDecimali("");
        ws.getIdsv0502().setIndDec(((short)0));
        // COB_CODE: PERFORM VARYING IND-STRINGA FROM LIMITE-DECIMALI BY 1
        //                   UNTIL   IND-STRINGA > LIMITE-INT-DEC
        //                           TO ELE-STRINGA-DECIMALI(IND-DEC)
        //           END-PERFORM.
        ws.getIdsv0502().setIndStringa(ws.getIdsv0502().getLimiteDecimali());
        while (!(ws.getIdsv0502().getIndStringa() > ws.getIdsv0502().getLimiteIntDec())) {
            // COB_CODE: ADD 1                  TO IND-DEC
            ws.getIdsv0502().setIndDec(Trunc.toShort(1 + ws.getIdsv0502().getIndDec(), 2));
            // COB_CODE: MOVE ELE-NUM-IMP(IND-STRINGA)
            //             TO ELE-STRINGA-DECIMALI(IND-DEC)
            ws.getIdsv0502().getCampoDecimali().setEleStringaDecimali(ws.getIdsv0502().getIndDec(), ws.getIdsv0502().getStructNum().getEleNumImp(ws.getIdsv0502().getIndStringa()));
            ws.getIdsv0502().setIndStringa(Trunc.toShort(ws.getIdsv0502().getIndStringa() + 1, 2));
        }
    }

    /**Original name: TRATTA-SEGNO<br>
	 * <pre>*************************************************************</pre>*/
    private void trattaSegno() {
        // COB_CODE: IF SEGNO-NEGATIVO
        //              ADD 1               TO POSIZIONE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIdsv0502().getTipoSegno().isSegnoNegativo()) {
            // COB_CODE: MOVE '-'            TO IDSV0501-STRINGA-TOT
            //                                 (IND-OUT)(POSIZIONE:1)
            ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring("-", ws.getIdsv0502().getPosizione(), 1);
            // COB_CODE: ADD 1               TO POSIZIONE
            ws.getIdsv0502().setPosizione(Trunc.toShort(1 + ws.getIdsv0502().getPosizione(), 2));
        }
        else if (ws.getIdsv0502().getIdsv0501SegnoPostiv().isIdsv0501SegnoPostivSi()) {
            // COB_CODE: IF IDSV0501-SEGNO-POSTIV-SI
            //              ADD 1            TO POSIZIONE
            //           END-IF
            // COB_CODE: MOVE '+'         TO IDSV0501-STRINGA-TOT
            //                              (IND-OUT)(POSIZIONE:1)
            ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring("+", ws.getIdsv0502().getPosizione(), 1);
            // COB_CODE: ADD 1            TO POSIZIONE
            ws.getIdsv0502().setPosizione(Trunc.toShort(1 + ws.getIdsv0502().getPosizione(), 2));
        }
    }

    /**Original name: STRINGA-CAMPO<br>
	 * <pre>*************************************************************</pre>*/
    private void stringaCampo() {
        // COB_CODE: PERFORM CALCOLA-SPAZIO       THRU CALCOLA-SPAZIO-EX.
        calcolaSpazio();
        // COB_CODE: IF IDSV0501-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (ws.getIdsv0502().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc()) {
            // COB_CODE: EVALUATE TP-DATO-PARALLELO(IND-VAR)
            //              WHEN STRINGA
            //              WHEN DT
            //                  END-IF
            //              WHEN IMPORTO
            //              WHEN PERCENTUALE
            //              WHEN TASSO
            //                  END-IF
            //              WHEN NUMERICO
            //                  END-IF
            //           END-EVALUATE
            if ((ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getStringa()) || (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getDt())) {
                // COB_CODE: IF ALFANUMERICO-TROVATO-SI
                //              ADD IND-ALFA    TO POSIZIONE
                //           ELSE
                //              ADD 1           TO POSIZIONE
                //           END-IF
                if (ws.getIdsv0502().getAlfanumericoTrovato().isAlfanumericoTrovatoSi()) {
                    // COB_CODE: MOVE CAMPO-ALFA(1:IND-ALFA)
                    //                           TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:IND-ALFA)
                    ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring(ws.getIdsv0502().getCampoAlfa().getCampoAlfaFormatted().substring((1) - 1, ws.getIdsv0502().getIndAlfa()), ws.getIdsv0502().getPosizione(), ws.getIdsv0502().getIndAlfa());
                    // COB_CODE: ADD IND-ALFA    TO POSIZIONE
                    ws.getIdsv0502().setPosizione(Trunc.toShort(ws.getIdsv0502().getIndAlfa() + ws.getIdsv0502().getPosizione(), 2));
                }
                else {
                    // COB_CODE: MOVE SPACES     TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:1)
                    ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring(LiteralGenerator.create(Types.SPACE_CHAR, 1), ws.getIdsv0502().getPosizione(), 1);
                    // COB_CODE: ADD 1           TO POSIZIONE
                    ws.getIdsv0502().setPosizione(Trunc.toShort(1 + ws.getIdsv0502().getPosizione(), 2));
                }
            }
            else if ((ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getImporto()) || (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getPercentuale()) || (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getTasso())) {
                // COB_CODE: IF IDSV0501-SEGNO-ANTERIORE
                //              PERFORM TRATTA-SEGNO  THRU TRATTA-SEGNO-EX
                //           END-IF
                if (ws.getIdsv0502().getIdsv0501PosSegno().isIdsv0501SegnoAnteriore()) {
                    // COB_CODE: PERFORM TRATTA-SEGNO  THRU TRATTA-SEGNO-EX
                    trattaSegno();
                }
                // COB_CODE: IF INT-TROVATO-SI
                //              ADD IND-INT     TO POSIZIONE
                //           ELSE
                //              ADD 1           TO POSIZIONE
                //           END-IF
                if (ws.getIdsv0502().getIntTrovato().isIntTrovatoSi()) {
                    // COB_CODE: MOVE CAMPO-INTERI(1:IND-INT)
                    //                           TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:IND-INT)
                    ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring(ws.getIdsv0502().getCampoInteri().getCampoInteriFormatted().substring((1) - 1, ws.getIdsv0502().getIndInt()), ws.getIdsv0502().getPosizione(), ws.getIdsv0502().getIndInt());
                    // COB_CODE: ADD IND-INT     TO POSIZIONE
                    ws.getIdsv0502().setPosizione(Trunc.toShort(ws.getIdsv0502().getIndInt() + ws.getIdsv0502().getPosizione(), 2));
                }
                else {
                    // COB_CODE: MOVE 0          TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:1)
                    ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring("0", ws.getIdsv0502().getPosizione(), 1);
                    // COB_CODE: ADD 1           TO POSIZIONE
                    ws.getIdsv0502().setPosizione(Trunc.toShort(1 + ws.getIdsv0502().getPosizione(), 2));
                }
                // COB_CODE: MOVE IDSV0501-SIMBOLO-DECIMALE
                //                              TO IDSV0501-STRINGA-TOT
                //                                (IND-OUT)(POSIZIONE:1)
                ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring(String.valueOf(ws.getIdsv0502().getIdsv0501SimboloDecimale()), ws.getIdsv0502().getPosizione(), 1);
                // COB_CODE: ADD 1              TO POSIZIONE
                ws.getIdsv0502().setPosizione(Trunc.toShort(1 + ws.getIdsv0502().getPosizione(), 2));
                // COB_CODE: IF DEC-TROVATO-SI
                //              ADD IND-DEC     TO POSIZIONE
                //           ELSE
                //              ADD 1           TO POSIZIONE
                //           END-IF
                if (ws.getIdsv0502().getDecTrovato().isDecTrovatoSi()) {
                    // COB_CODE: MOVE CAMPO-DECIMALI(1:IND-DEC)
                    //                           TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:IND-DEC)
                    ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring(ws.getIdsv0502().getCampoDecimali().getCampoDecimaliFormatted().substring((1) - 1, ws.getIdsv0502().getIndDec()), ws.getIdsv0502().getPosizione(), ws.getIdsv0502().getIndDec());
                    // COB_CODE: ADD IND-DEC     TO POSIZIONE
                    ws.getIdsv0502().setPosizione(Trunc.toShort(ws.getIdsv0502().getIndDec() + ws.getIdsv0502().getPosizione(), 2));
                }
                else {
                    // COB_CODE: MOVE 0          TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:1)
                    ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring("0", ws.getIdsv0502().getPosizione(), 1);
                    // COB_CODE: ADD 1           TO POSIZIONE
                    ws.getIdsv0502().setPosizione(Trunc.toShort(1 + ws.getIdsv0502().getPosizione(), 2));
                }
            }
            else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getNumerico()) {
                // COB_CODE: IF IDSV0501-SEGNO-ANTERIORE
                //              PERFORM TRATTA-SEGNO THRU TRATTA-SEGNO-EX
                //           END-IF
                if (ws.getIdsv0502().getIdsv0501PosSegno().isIdsv0501SegnoAnteriore()) {
                    // COB_CODE: PERFORM TRATTA-SEGNO THRU TRATTA-SEGNO-EX
                    trattaSegno();
                }
                // COB_CODE: IF INT-TROVATO-SI
                //              ADD IND-INT     TO POSIZIONE
                //           ELSE
                //              ADD 1           TO POSIZIONE
                //           END-IF
                if (ws.getIdsv0502().getIntTrovato().isIntTrovatoSi()) {
                    // COB_CODE: MOVE CAMPO-INTERI(1:IND-INT)
                    //                           TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:IND-INT)
                    ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring(ws.getIdsv0502().getCampoInteri().getCampoInteriFormatted().substring((1) - 1, ws.getIdsv0502().getIndInt()), ws.getIdsv0502().getPosizione(), ws.getIdsv0502().getIndInt());
                    // COB_CODE: ADD IND-INT     TO POSIZIONE
                    ws.getIdsv0502().setPosizione(Trunc.toShort(ws.getIdsv0502().getIndInt() + ws.getIdsv0502().getPosizione(), 2));
                }
                else {
                    // COB_CODE: MOVE 0          TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:1)
                    ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(ws.getIdsv0502().getIndOut()).setIdsv0501StringaTotSubstring("0", ws.getIdsv0502().getPosizione(), 1);
                    // COB_CODE: ADD 1           TO POSIZIONE
                    ws.getIdsv0502().setPosizione(Trunc.toShort(1 + ws.getIdsv0502().getPosizione(), 2));
                }
            }
        }
    }

    /**Original name: CALCOLA-SPAZIO<br>
	 * <pre>*************************************************************</pre>*/
    private void calcolaSpazio() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF TP-DATO-PARALLELO(IND-VAR) NOT = STRINGA
        //              PERFORM INDIVIDUA-IND-DEC    THRU INDIVIDUA-IND-DEC-EX
        //           END-IF.
        if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() != ws.getIdsv0502().getTipoFormato().getStringa()) {
            // COB_CODE: PERFORM INDIVIDUA-IND-DEC    THRU INDIVIDUA-IND-DEC-EX
            individuaIndDec();
        }
        // COB_CODE: MOVE ZEROES                     TO SPAZIO-RICHIESTO.
        ws.getIdsv0502().setSpazioRichiesto(((short)0));
        // COB_CODE: EVALUATE TP-DATO-PARALLELO(IND-VAR)
        //              WHEN STRINGA
        //              WHEN DT
        //                   COMPUTE SPAZIO-RICHIESTO = IND-ALFA + 1
        //              WHEN NUMERICO
        //                   COMPUTE SPAZIO-RICHIESTO = IND-INT + 1
        //              WHEN OTHER
        //                   COMPUTE SPAZIO-RICHIESTO = IND-INT + IND-DEC + 1 + 1
        //           END-EVALUATE.
        if ((ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getStringa()) || (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getDt())) {
            // COB_CODE: COMPUTE SPAZIO-RICHIESTO = IND-ALFA + 1
            ws.getIdsv0502().setSpazioRichiesto(Trunc.toShort(ws.getIdsv0502().getIndAlfa() + 1, 2));
        }
        else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getNumerico()) {
            // COB_CODE: COMPUTE SPAZIO-RICHIESTO = IND-INT + 1
            ws.getIdsv0502().setSpazioRichiesto(Trunc.toShort(ws.getIdsv0502().getIndInt() + 1, 2));
        }
        else {
            // COB_CODE: COMPUTE SPAZIO-RICHIESTO = IND-INT + IND-DEC + 1 + 1
            ws.getIdsv0502().setSpazioRichiesto(Trunc.toShort(ws.getIdsv0502().getIndInt() + ws.getIdsv0502().getIndDec() + 1 + 1, 2));
        }
        // COB_CODE: IF (IDSV0501-SEGNO-ANTERIORE OR
        //               IDSV0501-SEGNO-POSTERIORE)
        //               END-IF
        //           END-IF.
        if (ws.getIdsv0502().getIdsv0501PosSegno().isIdsv0501SegnoAnteriore() || ws.getIdsv0502().getIdsv0501PosSegno().isIdsv0501SegnoPosteriore()) {
            // COB_CODE: IF SEGNO-NEGATIVO
            //              SUBTRACT  1    FROM  SPAZIO-RESTANTE
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIdsv0502().getTipoSegno().isSegnoNegativo()) {
                // COB_CODE: SUBTRACT  1    FROM  SPAZIO-RESTANTE
                ws.getIdsv0502().setSpazioRestante(Trunc.toShort(abs(ws.getIdsv0502().getSpazioRestante() - 1), 2));
            }
            else if (ws.getIdsv0502().getIdsv0501SegnoPostiv().isIdsv0501SegnoPostivSi()) {
                // COB_CODE: IF IDSV0501-SEGNO-POSTIV-SI
                //              SUBTRACT  1    FROM  SPAZIO-RESTANTE
                //           END-IF
                // COB_CODE: SUBTRACT  1    FROM  SPAZIO-RESTANTE
                ws.getIdsv0502().setSpazioRestante(Trunc.toShort(abs(ws.getIdsv0502().getSpazioRestante() - 1), 2));
            }
        }
        // COB_CODE: IF SPAZIO-RICHIESTO >= SPAZIO-RESTANTE
        //              END-IF
        //           ELSE
        //                                        SPAZIO-RICHIESTO
        //           END-IF.
        if (ws.getIdsv0502().getSpazioRichiesto() >= ws.getIdsv0502().getSpazioRestante()) {
            // COB_CODE: code not available
            ws.setIntRegister1(((short)1));
            ws.getIdsv0502().setIndOut(Trunc.toShort(abs(ws.getIntRegister1() + ws.getIdsv0502().getIndOut()), 2));
            ws.getIdsv0502().getIdsv0501Output().setMaxTabStr(Trunc.toShort(ws.getIntRegister1() + ws.getIdsv0502().getIdsv0501Output().getMaxTabStr(), 2));
            // COB_CODE: IF IND-OUT > LIMITE-ARRAY-OUTPUT
            //              END-STRING
            //           ELSE
            //                                        SPAZIO-RICHIESTO
            //           END-IF
            if (ws.getIdsv0502().getIndOut() > ws.getIdsv0502().getLimiteArrayOutput()) {
                // COB_CODE: SET IDSV0501-GENERIC-ERROR  TO TRUE
                ws.getIdsv0502().getIdsv0501ReturnCode().setIdsv0501GenericError();
                // COB_CODE: STRING 'SUPERATO LIMITE STRUTTURE OUTPUT'
                //                  DELIMITED BY SIZE
                //                  INTO IDSV0501-DESCRIZ-ERR
                //           END-STRING
                ws.getIdsv0502().setIdsv0501DescrizErr("SUPERATO LIMITE STRUTTURE OUTPUT");
            }
            else {
                // COB_CODE: MOVE LIMITE-STRINGA  TO SPAZIO-RESTANTE
                ws.getIdsv0502().setSpazioRestanteFormatted(ws.getIdsv0502().getLimiteStringaFormatted());
                // COB_CODE: MOVE 1               TO POSIZIONE
                ws.getIdsv0502().setPosizione(((short)1));
                // COB_CODE: COMPUTE SPAZIO-RESTANTE = SPAZIO-RESTANTE -
                //                                     SPAZIO-RICHIESTO
                ws.getIdsv0502().setSpazioRestante(Trunc.toShort(abs(ws.getIdsv0502().getSpazioRestante() - ws.getIdsv0502().getSpazioRichiesto()), 2));
            }
        }
        else {
            // COB_CODE: COMPUTE SPAZIO-RESTANTE = SPAZIO-RESTANTE -
            //                                     SPAZIO-RICHIESTO
            ws.getIdsv0502().setSpazioRestante(Trunc.toShort(abs(ws.getIdsv0502().getSpazioRestante() - ws.getIdsv0502().getSpazioRichiesto()), 2));
        }
    }

    /**Original name: INDIVIDUA-IND-DEC<br>
	 * <pre>*************************************************************</pre>*/
    private void individuaIndDec() {
        // COB_CODE: SET DEC-TROVATO-NO                    TO TRUE.
        ws.getIdsv0502().getDecTrovato().setDecTrovatoNo();
        // COB_CODE: MOVE 1                                TO IND-DEC.
        ws.getIdsv0502().setIndDec(((short)1));
        // COB_CODE: EVALUATE TP-DATO-PARALLELO(IND-VAR)
        //               WHEN IMPORTO
        //                    END-PERFORM
        //               WHEN PERCENTUALE
        //                    END-PERFORM
        //               WHEN TASSO
        //                    END-PERFORM
        //           END-EVALUATE.
        if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getImporto()) {
            // COB_CODE: PERFORM VARYING IND-RICERCA
            //               FROM IDSV0501-DECIMALI-ESPOSTI-IMP
            //               BY -1
            //               UNTIL IND-RICERCA <= ZEROES OR
            //                     DEC-TROVATO-SI
            //               END-IF
            //           END-PERFORM
            ws.getIdsv0502().setIndRicerca(ws.getIdsv0502().getIdsv0501DecimaliEspostiImp());
            while (!(ws.getIdsv0502().getIndRicerca() <= 0 || ws.getIdsv0502().getDecTrovato().isDecTrovatoSi())) {
                // COB_CODE: IF  ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) IS NUMERIC
                //           AND ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) NOT = ZEROES
                //               MOVE IND-RICERCA   TO IND-DEC
                //           END-IF
                if (Functions.isNumber(ws.getIdsv0502().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0502().getIndRicerca())) && !Conditions.eq(ws.getIdsv0502().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0502().getIndRicerca()), '0')) {
                    // COB_CODE: SET DEC-TROVATO-SI TO TRUE
                    ws.getIdsv0502().getDecTrovato().setDecTrovatoSi();
                    // COB_CODE: MOVE IND-RICERCA   TO IND-DEC
                    ws.getIdsv0502().setIndDec(ws.getIdsv0502().getIndRicerca());
                }
                ws.getIdsv0502().setIndRicerca(Trunc.toShort(ws.getIdsv0502().getIndRicerca() + (-1), 2));
            }
        }
        else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getPercentuale()) {
            // COB_CODE: PERFORM VARYING IND-RICERCA
            //               FROM IDSV0501-DECIMALI-ESPOSTI-PERC
            //               BY -1
            //               UNTIL IND-RICERCA <= ZEROES OR
            //                     DEC-TROVATO-SI
            //               END-IF
            //           END-PERFORM
            ws.getIdsv0502().setIndRicerca(ws.getIdsv0502().getIdsv0501DecimaliEspostiPerc());
            while (!(ws.getIdsv0502().getIndRicerca() <= 0 || ws.getIdsv0502().getDecTrovato().isDecTrovatoSi())) {
                // COB_CODE: IF  ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) IS NUMERIC
                //           AND ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) NOT = ZEROES
                //               MOVE IND-RICERCA   TO IND-DEC
                //           END-IF
                if (Functions.isNumber(ws.getIdsv0502().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0502().getIndRicerca())) && !Conditions.eq(ws.getIdsv0502().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0502().getIndRicerca()), '0')) {
                    // COB_CODE: SET DEC-TROVATO-SI TO TRUE
                    ws.getIdsv0502().getDecTrovato().setDecTrovatoSi();
                    // COB_CODE: MOVE IND-RICERCA   TO IND-DEC
                    ws.getIdsv0502().setIndDec(ws.getIdsv0502().getIndRicerca());
                }
                ws.getIdsv0502().setIndRicerca(Trunc.toShort(ws.getIdsv0502().getIndRicerca() + (-1), 2));
            }
        }
        else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getTasso()) {
            // COB_CODE: PERFORM VARYING IND-RICERCA
            //               FROM IDSV0501-DECIMALI-ESPOSTI-TASS
            //               BY -1
            //               UNTIL IND-RICERCA <= ZEROES OR
            //                     DEC-TROVATO-SI
            //               END-IF
            //           END-PERFORM
            ws.getIdsv0502().setIndRicerca(ws.getIdsv0502().getIdsv0501DecimaliEspostiTass());
            while (!(ws.getIdsv0502().getIndRicerca() <= 0 || ws.getIdsv0502().getDecTrovato().isDecTrovatoSi())) {
                // COB_CODE: IF  ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) IS NUMERIC
                //           AND ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) NOT = ZEROES
                //               MOVE IND-RICERCA   TO IND-DEC
                //           END-IF
                if (Functions.isNumber(ws.getIdsv0502().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0502().getIndRicerca())) && !Conditions.eq(ws.getIdsv0502().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0502().getIndRicerca()), '0')) {
                    // COB_CODE: SET DEC-TROVATO-SI TO TRUE
                    ws.getIdsv0502().getDecTrovato().setDecTrovatoSi();
                    // COB_CODE: MOVE IND-RICERCA   TO IND-DEC
                    ws.getIdsv0502().setIndDec(ws.getIdsv0502().getIndRicerca());
                }
                ws.getIdsv0502().setIndRicerca(Trunc.toShort(ws.getIdsv0502().getIndRicerca() + (-1), 2));
            }
        }
    }

    /**Original name: LISTA-OMOGENEA<br>
	 * <pre>*************************************************************
	 *  SIR 20310 GESTIONE LISTE OMOGENEE
	 * *************************************************************</pre>*/
    private void listaOmogenea() {
        // COB_CODE: EVALUATE TP-DATO-PARALLELO(IND-VAR)
        //               WHEN STRINGA
        //                    MOVE LISTA-STRINGA        TO IDSV0501-TP-STRINGA
        //               WHEN DT
        //                    MOVE LISTA-DT             TO IDSV0501-TP-STRINGA
        //               WHEN IMPORTO
        //                    MOVE LISTA-IMPORTO        TO IDSV0501-TP-STRINGA
        //               WHEN PERCENTUALE
        //                    MOVE LISTA-PERCENTUALE    TO IDSV0501-TP-STRINGA
        //               WHEN TASSO
        //                    MOVE LISTA-TASSO          TO IDSV0501-TP-STRINGA
        //               WHEN NUMERICO
        //                    MOVE LISTA-NUMERICO       TO IDSV0501-TP-STRINGA
        //               WHEN MILLESIMI
        //                    MOVE LISTA-MILLESIMI      TO IDSV0501-TP-STRINGA
        //           END-EVALUATE.
        if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getStringa()) {
            // COB_CODE: MOVE LISTA-STRINGA        TO IDSV0501-TP-STRINGA
            ws.getIdsv0502().getIdsv0501Output().setTpStringa(ws.getIdsv0502().getTipoFormato().getListaStringa());
        }
        else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getDt()) {
            // COB_CODE: MOVE LISTA-DT             TO IDSV0501-TP-STRINGA
            ws.getIdsv0502().getIdsv0501Output().setTpStringa(ws.getIdsv0502().getTipoFormato().getListaDt());
        }
        else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getImporto()) {
            // COB_CODE: MOVE LISTA-IMPORTO        TO IDSV0501-TP-STRINGA
            ws.getIdsv0502().getIdsv0501Output().setTpStringa(ws.getIdsv0502().getTipoFormato().getListaImporto());
        }
        else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getPercentuale()) {
            // COB_CODE: MOVE LISTA-PERCENTUALE    TO IDSV0501-TP-STRINGA
            ws.getIdsv0502().getIdsv0501Output().setTpStringa(ws.getIdsv0502().getTipoFormato().getListaPercentuale());
        }
        else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getTasso()) {
            // COB_CODE: MOVE LISTA-TASSO          TO IDSV0501-TP-STRINGA
            ws.getIdsv0502().getIdsv0501Output().setTpStringa(ws.getIdsv0502().getTipoFormato().getListaTasso());
        }
        else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getNumerico()) {
            // COB_CODE: MOVE LISTA-NUMERICO       TO IDSV0501-TP-STRINGA
            ws.getIdsv0502().getIdsv0501Output().setTpStringa(ws.getIdsv0502().getTipoFormato().getListaNumerico());
        }
        else if (ws.getIdsv0502().getTabellaTpDato(ws.getIdsv0502().getIndVar()).getTpDatoParallelo() == ws.getIdsv0502().getTipoFormato().getMillesimi()) {
            // COB_CODE: MOVE LISTA-MILLESIMI      TO IDSV0501-TP-STRINGA
            ws.getIdsv0502().getIdsv0501Output().setTpStringa(ws.getIdsv0502().getTipoFormato().getListaMillesimi());
        }
    }

    public void initIvvc0501Output() {
        ivvc0501Output.setTpStringa(Types.SPACE_CHAR);
        ivvc0501Output.setMaxTabStr(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0501Output.TAB_STRINGHE_TOT_MAXOCCURS; idx0++) {
            ivvc0501Output.getTabStringheTot(idx0).setIvvc0501StringaTot("");
        }
    }

    public void initIdsv0501Output() {
        ws.getIdsv0502().getIdsv0501Output().setTpStringa(Types.SPACE_CHAR);
        ws.getIdsv0502().getIdsv0501Output().setMaxTabStr(((short)0));
        for (int idx0 = 1; idx0 <= Idsv0501OutputLvvs3480.TAB_STRINGHE_TOT_MAXOCCURS; idx0++) {
            ws.getIdsv0502().getIdsv0501Output().getTabStringheTot(idx0).setIdsv0501StringaTot("");
        }
    }

    public void initIdsv0501Input() {
        for (int idx0 = 1; idx0 <= Idsv0502.IDSV0501_TAB_VARIABILI_MAXOCCURS; idx0++) {
            ws.getIdsv0502().getIdsv0501TabVariabili(idx0).setIdsv0501TpDato(Types.SPACE_CHAR);
            ws.getIdsv0502().getIdsv0501TabVariabili(idx0).setIdsv0501ValNum(0);
            ws.getIdsv0502().getIdsv0501TabVariabili(idx0).setIdsv0501ValImp(new AfDecimal(0, 18, 7));
            ws.getIdsv0502().getIdsv0501TabVariabili(idx0).setIdsv0501ValPerc(new AfDecimal(0, 18, 9));
            ws.getIdsv0502().getIdsv0501TabVariabili(idx0).setIdsv0501ValStr("");
        }
    }
}
