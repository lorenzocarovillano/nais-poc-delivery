package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DCristDao;
import it.accenture.jnais.commons.data.to.IDCrist;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DCristIdbsp610;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsp610Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.P61CptIni30062014;
import it.accenture.jnais.ws.redefines.P61CptRivto31122011;
import it.accenture.jnais.ws.redefines.P61IdAdes;
import it.accenture.jnais.ws.redefines.P61IdMoviChiu;
import it.accenture.jnais.ws.redefines.P61IdTrchDiGar;
import it.accenture.jnais.ws.redefines.P61ImpbIs30062014;
import it.accenture.jnais.ws.redefines.P61ImpbIs31122011;
import it.accenture.jnais.ws.redefines.P61ImpbIsRpP2011;
import it.accenture.jnais.ws.redefines.P61ImpbIsRpP62014;
import it.accenture.jnais.ws.redefines.P61ImpbVis30062014;
import it.accenture.jnais.ws.redefines.P61ImpbVis31122011;
import it.accenture.jnais.ws.redefines.P61ImpbVisRpP2011;
import it.accenture.jnais.ws.redefines.P61ImpbVisRpP62014;
import it.accenture.jnais.ws.redefines.P61MontLrdDal2007;
import it.accenture.jnais.ws.redefines.P61MontLrdEnd2000;
import it.accenture.jnais.ws.redefines.P61MontLrdEnd2006;
import it.accenture.jnais.ws.redefines.P61PreLrdDal2007;
import it.accenture.jnais.ws.redefines.P61PreLrdEnd2000;
import it.accenture.jnais.ws.redefines.P61PreLrdEnd2006;
import it.accenture.jnais.ws.redefines.P61PreRshV30062014;
import it.accenture.jnais.ws.redefines.P61PreRshV31122011;
import it.accenture.jnais.ws.redefines.P61PreV30062014;
import it.accenture.jnais.ws.redefines.P61PreV31122011;
import it.accenture.jnais.ws.redefines.P61RendtoLrdDal2007;
import it.accenture.jnais.ws.redefines.P61RendtoLrdEnd2000;
import it.accenture.jnais.ws.redefines.P61RendtoLrdEnd2006;
import it.accenture.jnais.ws.redefines.P61RisMat30062014;
import it.accenture.jnais.ws.redefines.P61RisMat31122011;

/**Original name: IDBSP610<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  24 OTT 2019.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsp610 extends Program implements IDCrist {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DCristDao dCristDao = new DCristDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsp610Data ws = new Idbsp610Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: D-CRIST
    private DCristIdbsp610 dCrist;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSP610_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, DCristIdbsp610 dCrist) {
        this.idsv0003 = idsv0003;
        this.dCrist = dCrist;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsp610 getInstance() {
        return ((Idbsp610)Programs.getInstance(Idbsp610.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSP610'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSP610");
        // COB_CODE: MOVE 'D_CRIST' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("D_CRIST");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_CRIST
        //                ,ID_POLI
        //                ,COD_COMP_ANIA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_PROD
        //                ,DT_DECOR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RIS_MAT_31122011
        //                ,PRE_V_31122011
        //                ,PRE_RSH_V_31122011
        //                ,CPT_RIVTO_31122011
        //                ,IMPB_VIS_31122011
        //                ,IMPB_IS_31122011
        //                ,IMPB_VIS_RP_P2011
        //                ,IMPB_IS_RP_P2011
        //                ,PRE_V_30062014
        //                ,PRE_RSH_V_30062014
        //                ,CPT_INI_30062014
        //                ,IMPB_VIS_30062014
        //                ,IMPB_IS_30062014
        //                ,IMPB_VIS_RP_P62014
        //                ,IMPB_IS_RP_P62014
        //                ,RIS_MAT_30062014
        //                ,ID_ADES
        //                ,MONT_LRD_END2000
        //                ,PRE_LRD_END2000
        //                ,RENDTO_LRD_END2000
        //                ,MONT_LRD_END2006
        //                ,PRE_LRD_END2006
        //                ,RENDTO_LRD_END2006
        //                ,MONT_LRD_DAL2007
        //                ,PRE_LRD_DAL2007
        //                ,RENDTO_LRD_DAL2007
        //                ,ID_TRCH_DI_GAR
        //             INTO
        //                :P61-ID-D-CRIST
        //               ,:P61-ID-POLI
        //               ,:P61-COD-COMP-ANIA
        //               ,:P61-ID-MOVI-CRZ
        //               ,:P61-ID-MOVI-CHIU
        //                :IND-P61-ID-MOVI-CHIU
        //               ,:P61-DT-INI-EFF-DB
        //               ,:P61-DT-END-EFF-DB
        //               ,:P61-COD-PROD
        //               ,:P61-DT-DECOR-DB
        //               ,:P61-DS-RIGA
        //               ,:P61-DS-OPER-SQL
        //               ,:P61-DS-VER
        //               ,:P61-DS-TS-INI-CPTZ
        //               ,:P61-DS-TS-END-CPTZ
        //               ,:P61-DS-UTENTE
        //               ,:P61-DS-STATO-ELAB
        //               ,:P61-RIS-MAT-31122011
        //                :IND-P61-RIS-MAT-31122011
        //               ,:P61-PRE-V-31122011
        //                :IND-P61-PRE-V-31122011
        //               ,:P61-PRE-RSH-V-31122011
        //                :IND-P61-PRE-RSH-V-31122011
        //               ,:P61-CPT-RIVTO-31122011
        //                :IND-P61-CPT-RIVTO-31122011
        //               ,:P61-IMPB-VIS-31122011
        //                :IND-P61-IMPB-VIS-31122011
        //               ,:P61-IMPB-IS-31122011
        //                :IND-P61-IMPB-IS-31122011
        //               ,:P61-IMPB-VIS-RP-P2011
        //                :IND-P61-IMPB-VIS-RP-P2011
        //               ,:P61-IMPB-IS-RP-P2011
        //                :IND-P61-IMPB-IS-RP-P2011
        //               ,:P61-PRE-V-30062014
        //                :IND-P61-PRE-V-30062014
        //               ,:P61-PRE-RSH-V-30062014
        //                :IND-P61-PRE-RSH-V-30062014
        //               ,:P61-CPT-INI-30062014
        //                :IND-P61-CPT-INI-30062014
        //               ,:P61-IMPB-VIS-30062014
        //                :IND-P61-IMPB-VIS-30062014
        //               ,:P61-IMPB-IS-30062014
        //                :IND-P61-IMPB-IS-30062014
        //               ,:P61-IMPB-VIS-RP-P62014
        //                :IND-P61-IMPB-VIS-RP-P62014
        //               ,:P61-IMPB-IS-RP-P62014
        //                :IND-P61-IMPB-IS-RP-P62014
        //               ,:P61-RIS-MAT-30062014
        //                :IND-P61-RIS-MAT-30062014
        //               ,:P61-ID-ADES
        //                :IND-P61-ID-ADES
        //               ,:P61-MONT-LRD-END2000
        //                :IND-P61-MONT-LRD-END2000
        //               ,:P61-PRE-LRD-END2000
        //                :IND-P61-PRE-LRD-END2000
        //               ,:P61-RENDTO-LRD-END2000
        //                :IND-P61-RENDTO-LRD-END2000
        //               ,:P61-MONT-LRD-END2006
        //                :IND-P61-MONT-LRD-END2006
        //               ,:P61-PRE-LRD-END2006
        //                :IND-P61-PRE-LRD-END2006
        //               ,:P61-RENDTO-LRD-END2006
        //                :IND-P61-RENDTO-LRD-END2006
        //               ,:P61-MONT-LRD-DAL2007
        //                :IND-P61-MONT-LRD-DAL2007
        //               ,:P61-PRE-LRD-DAL2007
        //                :IND-P61-PRE-LRD-DAL2007
        //               ,:P61-RENDTO-LRD-DAL2007
        //                :IND-P61-RENDTO-LRD-DAL2007
        //               ,:P61-ID-TRCH-DI-GAR
        //                :IND-P61-ID-TRCH-DI-GAR
        //             FROM D_CRIST
        //             WHERE     DS_RIGA = :P61-DS-RIGA
        //           END-EXEC.
        dCristDao.selectByP61DsRiga(dCrist.getP61DsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO D_CRIST
            //                  (
            //                     ID_D_CRIST
            //                    ,ID_POLI
            //                    ,COD_COMP_ANIA
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_PROD
            //                    ,DT_DECOR
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,RIS_MAT_31122011
            //                    ,PRE_V_31122011
            //                    ,PRE_RSH_V_31122011
            //                    ,CPT_RIVTO_31122011
            //                    ,IMPB_VIS_31122011
            //                    ,IMPB_IS_31122011
            //                    ,IMPB_VIS_RP_P2011
            //                    ,IMPB_IS_RP_P2011
            //                    ,PRE_V_30062014
            //                    ,PRE_RSH_V_30062014
            //                    ,CPT_INI_30062014
            //                    ,IMPB_VIS_30062014
            //                    ,IMPB_IS_30062014
            //                    ,IMPB_VIS_RP_P62014
            //                    ,IMPB_IS_RP_P62014
            //                    ,RIS_MAT_30062014
            //                    ,ID_ADES
            //                    ,MONT_LRD_END2000
            //                    ,PRE_LRD_END2000
            //                    ,RENDTO_LRD_END2000
            //                    ,MONT_LRD_END2006
            //                    ,PRE_LRD_END2006
            //                    ,RENDTO_LRD_END2006
            //                    ,MONT_LRD_DAL2007
            //                    ,PRE_LRD_DAL2007
            //                    ,RENDTO_LRD_DAL2007
            //                    ,ID_TRCH_DI_GAR
            //                  )
            //              VALUES
            //                  (
            //                    :P61-ID-D-CRIST
            //                    ,:P61-ID-POLI
            //                    ,:P61-COD-COMP-ANIA
            //                    ,:P61-ID-MOVI-CRZ
            //                    ,:P61-ID-MOVI-CHIU
            //                     :IND-P61-ID-MOVI-CHIU
            //                    ,:P61-DT-INI-EFF-DB
            //                    ,:P61-DT-END-EFF-DB
            //                    ,:P61-COD-PROD
            //                    ,:P61-DT-DECOR-DB
            //                    ,:P61-DS-RIGA
            //                    ,:P61-DS-OPER-SQL
            //                    ,:P61-DS-VER
            //                    ,:P61-DS-TS-INI-CPTZ
            //                    ,:P61-DS-TS-END-CPTZ
            //                    ,:P61-DS-UTENTE
            //                    ,:P61-DS-STATO-ELAB
            //                    ,:P61-RIS-MAT-31122011
            //                     :IND-P61-RIS-MAT-31122011
            //                    ,:P61-PRE-V-31122011
            //                     :IND-P61-PRE-V-31122011
            //                    ,:P61-PRE-RSH-V-31122011
            //                     :IND-P61-PRE-RSH-V-31122011
            //                    ,:P61-CPT-RIVTO-31122011
            //                     :IND-P61-CPT-RIVTO-31122011
            //                    ,:P61-IMPB-VIS-31122011
            //                     :IND-P61-IMPB-VIS-31122011
            //                    ,:P61-IMPB-IS-31122011
            //                     :IND-P61-IMPB-IS-31122011
            //                    ,:P61-IMPB-VIS-RP-P2011
            //                     :IND-P61-IMPB-VIS-RP-P2011
            //                    ,:P61-IMPB-IS-RP-P2011
            //                     :IND-P61-IMPB-IS-RP-P2011
            //                    ,:P61-PRE-V-30062014
            //                     :IND-P61-PRE-V-30062014
            //                    ,:P61-PRE-RSH-V-30062014
            //                     :IND-P61-PRE-RSH-V-30062014
            //                    ,:P61-CPT-INI-30062014
            //                     :IND-P61-CPT-INI-30062014
            //                    ,:P61-IMPB-VIS-30062014
            //                     :IND-P61-IMPB-VIS-30062014
            //                    ,:P61-IMPB-IS-30062014
            //                     :IND-P61-IMPB-IS-30062014
            //                    ,:P61-IMPB-VIS-RP-P62014
            //                     :IND-P61-IMPB-VIS-RP-P62014
            //                    ,:P61-IMPB-IS-RP-P62014
            //                     :IND-P61-IMPB-IS-RP-P62014
            //                    ,:P61-RIS-MAT-30062014
            //                     :IND-P61-RIS-MAT-30062014
            //                    ,:P61-ID-ADES
            //                     :IND-P61-ID-ADES
            //                    ,:P61-MONT-LRD-END2000
            //                     :IND-P61-MONT-LRD-END2000
            //                    ,:P61-PRE-LRD-END2000
            //                     :IND-P61-PRE-LRD-END2000
            //                    ,:P61-RENDTO-LRD-END2000
            //                     :IND-P61-RENDTO-LRD-END2000
            //                    ,:P61-MONT-LRD-END2006
            //                     :IND-P61-MONT-LRD-END2006
            //                    ,:P61-PRE-LRD-END2006
            //                     :IND-P61-PRE-LRD-END2006
            //                    ,:P61-RENDTO-LRD-END2006
            //                     :IND-P61-RENDTO-LRD-END2006
            //                    ,:P61-MONT-LRD-DAL2007
            //                     :IND-P61-MONT-LRD-DAL2007
            //                    ,:P61-PRE-LRD-DAL2007
            //                     :IND-P61-PRE-LRD-DAL2007
            //                    ,:P61-RENDTO-LRD-DAL2007
            //                     :IND-P61-RENDTO-LRD-DAL2007
            //                    ,:P61-ID-TRCH-DI-GAR
            //                     :IND-P61-ID-TRCH-DI-GAR
            //                  )
            //           END-EXEC
            dCristDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE D_CRIST SET
        //                   ID_D_CRIST             =
        //                :P61-ID-D-CRIST
        //                  ,ID_POLI                =
        //                :P61-ID-POLI
        //                  ,COD_COMP_ANIA          =
        //                :P61-COD-COMP-ANIA
        //                  ,ID_MOVI_CRZ            =
        //                :P61-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :P61-ID-MOVI-CHIU
        //                                       :IND-P61-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :P61-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :P61-DT-END-EFF-DB
        //                  ,COD_PROD               =
        //                :P61-COD-PROD
        //                  ,DT_DECOR               =
        //           :P61-DT-DECOR-DB
        //                  ,DS_RIGA                =
        //                :P61-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :P61-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :P61-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :P61-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :P61-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :P61-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :P61-DS-STATO-ELAB
        //                  ,RIS_MAT_31122011       =
        //                :P61-RIS-MAT-31122011
        //                                       :IND-P61-RIS-MAT-31122011
        //                  ,PRE_V_31122011         =
        //                :P61-PRE-V-31122011
        //                                       :IND-P61-PRE-V-31122011
        //                  ,PRE_RSH_V_31122011     =
        //                :P61-PRE-RSH-V-31122011
        //                                       :IND-P61-PRE-RSH-V-31122011
        //                  ,CPT_RIVTO_31122011     =
        //                :P61-CPT-RIVTO-31122011
        //                                       :IND-P61-CPT-RIVTO-31122011
        //                  ,IMPB_VIS_31122011      =
        //                :P61-IMPB-VIS-31122011
        //                                       :IND-P61-IMPB-VIS-31122011
        //                  ,IMPB_IS_31122011       =
        //                :P61-IMPB-IS-31122011
        //                                       :IND-P61-IMPB-IS-31122011
        //                  ,IMPB_VIS_RP_P2011      =
        //                :P61-IMPB-VIS-RP-P2011
        //                                       :IND-P61-IMPB-VIS-RP-P2011
        //                  ,IMPB_IS_RP_P2011       =
        //                :P61-IMPB-IS-RP-P2011
        //                                       :IND-P61-IMPB-IS-RP-P2011
        //                  ,PRE_V_30062014         =
        //                :P61-PRE-V-30062014
        //                                       :IND-P61-PRE-V-30062014
        //                  ,PRE_RSH_V_30062014     =
        //                :P61-PRE-RSH-V-30062014
        //                                       :IND-P61-PRE-RSH-V-30062014
        //                  ,CPT_INI_30062014       =
        //                :P61-CPT-INI-30062014
        //                                       :IND-P61-CPT-INI-30062014
        //                  ,IMPB_VIS_30062014      =
        //                :P61-IMPB-VIS-30062014
        //                                       :IND-P61-IMPB-VIS-30062014
        //                  ,IMPB_IS_30062014       =
        //                :P61-IMPB-IS-30062014
        //                                       :IND-P61-IMPB-IS-30062014
        //                  ,IMPB_VIS_RP_P62014     =
        //                :P61-IMPB-VIS-RP-P62014
        //                                       :IND-P61-IMPB-VIS-RP-P62014
        //                  ,IMPB_IS_RP_P62014      =
        //                :P61-IMPB-IS-RP-P62014
        //                                       :IND-P61-IMPB-IS-RP-P62014
        //                  ,RIS_MAT_30062014       =
        //                :P61-RIS-MAT-30062014
        //                                       :IND-P61-RIS-MAT-30062014
        //                  ,ID_ADES                =
        //                :P61-ID-ADES
        //                                       :IND-P61-ID-ADES
        //                  ,MONT_LRD_END2000       =
        //                :P61-MONT-LRD-END2000
        //                                       :IND-P61-MONT-LRD-END2000
        //                  ,PRE_LRD_END2000        =
        //                :P61-PRE-LRD-END2000
        //                                       :IND-P61-PRE-LRD-END2000
        //                  ,RENDTO_LRD_END2000     =
        //                :P61-RENDTO-LRD-END2000
        //                                       :IND-P61-RENDTO-LRD-END2000
        //                  ,MONT_LRD_END2006       =
        //                :P61-MONT-LRD-END2006
        //                                       :IND-P61-MONT-LRD-END2006
        //                  ,PRE_LRD_END2006        =
        //                :P61-PRE-LRD-END2006
        //                                       :IND-P61-PRE-LRD-END2006
        //                  ,RENDTO_LRD_END2006     =
        //                :P61-RENDTO-LRD-END2006
        //                                       :IND-P61-RENDTO-LRD-END2006
        //                  ,MONT_LRD_DAL2007       =
        //                :P61-MONT-LRD-DAL2007
        //                                       :IND-P61-MONT-LRD-DAL2007
        //                  ,PRE_LRD_DAL2007        =
        //                :P61-PRE-LRD-DAL2007
        //                                       :IND-P61-PRE-LRD-DAL2007
        //                  ,RENDTO_LRD_DAL2007     =
        //                :P61-RENDTO-LRD-DAL2007
        //                                       :IND-P61-RENDTO-LRD-DAL2007
        //                  ,ID_TRCH_DI_GAR         =
        //                :P61-ID-TRCH-DI-GAR
        //                                       :IND-P61-ID-TRCH-DI-GAR
        //                WHERE     DS_RIGA = :P61-DS-RIGA
        //           END-EXEC.
        dCristDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM D_CRIST
        //                WHERE     DS_RIGA = :P61-DS-RIGA
        //           END-EXEC.
        dCristDao.deleteByP61DsRiga(dCrist.getP61DsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        //       ORDER BY DT_INI_EFF ASC
        // COB_CODE:      EXEC SQL
        //                     DECLARE C-ID-UPD-EFF-P61 CURSOR FOR
        //                   SELECT
        //                          ID_D_CRIST
        //                         ,ID_POLI
        //                         ,COD_COMP_ANIA
        //                         ,ID_MOVI_CRZ
        //                         ,ID_MOVI_CHIU
        //                         ,DT_INI_EFF
        //                         ,DT_END_EFF
        //                         ,COD_PROD
        //                         ,DT_DECOR
        //                         ,DS_RIGA
        //                         ,DS_OPER_SQL
        //                         ,DS_VER
        //                         ,DS_TS_INI_CPTZ
        //                         ,DS_TS_END_CPTZ
        //                         ,DS_UTENTE
        //                         ,DS_STATO_ELAB
        //                         ,RIS_MAT_31122011
        //                         ,PRE_V_31122011
        //                         ,PRE_RSH_V_31122011
        //                         ,CPT_RIVTO_31122011
        //                         ,IMPB_VIS_31122011
        //                         ,IMPB_IS_31122011
        //                         ,IMPB_VIS_RP_P2011
        //                         ,IMPB_IS_RP_P2011
        //                         ,PRE_V_30062014
        //                         ,PRE_RSH_V_30062014
        //                         ,CPT_INI_30062014
        //                         ,IMPB_VIS_30062014
        //                         ,IMPB_IS_30062014
        //                         ,IMPB_VIS_RP_P62014
        //                         ,IMPB_IS_RP_P62014
        //                         ,RIS_MAT_30062014
        //                         ,ID_ADES
        //                         ,MONT_LRD_END2000
        //                         ,PRE_LRD_END2000
        //                         ,RENDTO_LRD_END2000
        //                         ,MONT_LRD_END2006
        //                         ,PRE_LRD_END2006
        //                         ,RENDTO_LRD_END2006
        //                         ,MONT_LRD_DAL2007
        //                         ,PRE_LRD_DAL2007
        //                         ,RENDTO_LRD_DAL2007
        //                         ,ID_TRCH_DI_GAR
        //                   FROM D_CRIST
        //                   WHERE     ID_D_CRIST = :P61-ID-D-CRIST
        //                         AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                         AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                         AND COD_COMP_ANIA =
        //                             :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                   ORDER BY  ID_D_CRIST,
        //                             DS_TS_END_CPTZ, DT_INI_EFF
        //           *       ORDER BY DT_INI_EFF ASC
        //                END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_CRIST
        //                ,ID_POLI
        //                ,COD_COMP_ANIA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_PROD
        //                ,DT_DECOR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RIS_MAT_31122011
        //                ,PRE_V_31122011
        //                ,PRE_RSH_V_31122011
        //                ,CPT_RIVTO_31122011
        //                ,IMPB_VIS_31122011
        //                ,IMPB_IS_31122011
        //                ,IMPB_VIS_RP_P2011
        //                ,IMPB_IS_RP_P2011
        //                ,PRE_V_30062014
        //                ,PRE_RSH_V_30062014
        //                ,CPT_INI_30062014
        //                ,IMPB_VIS_30062014
        //                ,IMPB_IS_30062014
        //                ,IMPB_VIS_RP_P62014
        //                ,IMPB_IS_RP_P62014
        //                ,RIS_MAT_30062014
        //                ,ID_ADES
        //                ,MONT_LRD_END2000
        //                ,PRE_LRD_END2000
        //                ,RENDTO_LRD_END2000
        //                ,MONT_LRD_END2006
        //                ,PRE_LRD_END2006
        //                ,RENDTO_LRD_END2006
        //                ,MONT_LRD_DAL2007
        //                ,PRE_LRD_DAL2007
        //                ,RENDTO_LRD_DAL2007
        //                ,ID_TRCH_DI_GAR
        //             INTO
        //                :P61-ID-D-CRIST
        //               ,:P61-ID-POLI
        //               ,:P61-COD-COMP-ANIA
        //               ,:P61-ID-MOVI-CRZ
        //               ,:P61-ID-MOVI-CHIU
        //                :IND-P61-ID-MOVI-CHIU
        //               ,:P61-DT-INI-EFF-DB
        //               ,:P61-DT-END-EFF-DB
        //               ,:P61-COD-PROD
        //               ,:P61-DT-DECOR-DB
        //               ,:P61-DS-RIGA
        //               ,:P61-DS-OPER-SQL
        //               ,:P61-DS-VER
        //               ,:P61-DS-TS-INI-CPTZ
        //               ,:P61-DS-TS-END-CPTZ
        //               ,:P61-DS-UTENTE
        //               ,:P61-DS-STATO-ELAB
        //               ,:P61-RIS-MAT-31122011
        //                :IND-P61-RIS-MAT-31122011
        //               ,:P61-PRE-V-31122011
        //                :IND-P61-PRE-V-31122011
        //               ,:P61-PRE-RSH-V-31122011
        //                :IND-P61-PRE-RSH-V-31122011
        //               ,:P61-CPT-RIVTO-31122011
        //                :IND-P61-CPT-RIVTO-31122011
        //               ,:P61-IMPB-VIS-31122011
        //                :IND-P61-IMPB-VIS-31122011
        //               ,:P61-IMPB-IS-31122011
        //                :IND-P61-IMPB-IS-31122011
        //               ,:P61-IMPB-VIS-RP-P2011
        //                :IND-P61-IMPB-VIS-RP-P2011
        //               ,:P61-IMPB-IS-RP-P2011
        //                :IND-P61-IMPB-IS-RP-P2011
        //               ,:P61-PRE-V-30062014
        //                :IND-P61-PRE-V-30062014
        //               ,:P61-PRE-RSH-V-30062014
        //                :IND-P61-PRE-RSH-V-30062014
        //               ,:P61-CPT-INI-30062014
        //                :IND-P61-CPT-INI-30062014
        //               ,:P61-IMPB-VIS-30062014
        //                :IND-P61-IMPB-VIS-30062014
        //               ,:P61-IMPB-IS-30062014
        //                :IND-P61-IMPB-IS-30062014
        //               ,:P61-IMPB-VIS-RP-P62014
        //                :IND-P61-IMPB-VIS-RP-P62014
        //               ,:P61-IMPB-IS-RP-P62014
        //                :IND-P61-IMPB-IS-RP-P62014
        //               ,:P61-RIS-MAT-30062014
        //                :IND-P61-RIS-MAT-30062014
        //               ,:P61-ID-ADES
        //                :IND-P61-ID-ADES
        //               ,:P61-MONT-LRD-END2000
        //                :IND-P61-MONT-LRD-END2000
        //               ,:P61-PRE-LRD-END2000
        //                :IND-P61-PRE-LRD-END2000
        //               ,:P61-RENDTO-LRD-END2000
        //                :IND-P61-RENDTO-LRD-END2000
        //               ,:P61-MONT-LRD-END2006
        //                :IND-P61-MONT-LRD-END2006
        //               ,:P61-PRE-LRD-END2006
        //                :IND-P61-PRE-LRD-END2006
        //               ,:P61-RENDTO-LRD-END2006
        //                :IND-P61-RENDTO-LRD-END2006
        //               ,:P61-MONT-LRD-DAL2007
        //                :IND-P61-MONT-LRD-DAL2007
        //               ,:P61-PRE-LRD-DAL2007
        //                :IND-P61-PRE-LRD-DAL2007
        //               ,:P61-RENDTO-LRD-DAL2007
        //                :IND-P61-RENDTO-LRD-DAL2007
        //               ,:P61-ID-TRCH-DI-GAR
        //                :IND-P61-ID-TRCH-DI-GAR
        //             FROM D_CRIST
        //             WHERE     ID_D_CRIST = :P61-ID-D-CRIST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dCristDao.selectRec(dCrist.getP61IdDCrist(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE D_CRIST SET
        //                   ID_D_CRIST             =
        //                :P61-ID-D-CRIST
        //                  ,ID_POLI                =
        //                :P61-ID-POLI
        //                  ,COD_COMP_ANIA          =
        //                :P61-COD-COMP-ANIA
        //                  ,ID_MOVI_CRZ            =
        //                :P61-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :P61-ID-MOVI-CHIU
        //                                       :IND-P61-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :P61-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :P61-DT-END-EFF-DB
        //                  ,COD_PROD               =
        //                :P61-COD-PROD
        //                  ,DT_DECOR               =
        //           :P61-DT-DECOR-DB
        //                  ,DS_RIGA                =
        //                :P61-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :P61-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :P61-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :P61-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :P61-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :P61-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :P61-DS-STATO-ELAB
        //                  ,RIS_MAT_31122011       =
        //                :P61-RIS-MAT-31122011
        //                                       :IND-P61-RIS-MAT-31122011
        //                  ,PRE_V_31122011         =
        //                :P61-PRE-V-31122011
        //                                       :IND-P61-PRE-V-31122011
        //                  ,PRE_RSH_V_31122011     =
        //                :P61-PRE-RSH-V-31122011
        //                                       :IND-P61-PRE-RSH-V-31122011
        //                  ,CPT_RIVTO_31122011     =
        //                :P61-CPT-RIVTO-31122011
        //                                       :IND-P61-CPT-RIVTO-31122011
        //                  ,IMPB_VIS_31122011      =
        //                :P61-IMPB-VIS-31122011
        //                                       :IND-P61-IMPB-VIS-31122011
        //                  ,IMPB_IS_31122011       =
        //                :P61-IMPB-IS-31122011
        //                                       :IND-P61-IMPB-IS-31122011
        //                  ,IMPB_VIS_RP_P2011      =
        //                :P61-IMPB-VIS-RP-P2011
        //                                       :IND-P61-IMPB-VIS-RP-P2011
        //                  ,IMPB_IS_RP_P2011       =
        //                :P61-IMPB-IS-RP-P2011
        //                                       :IND-P61-IMPB-IS-RP-P2011
        //                  ,PRE_V_30062014         =
        //                :P61-PRE-V-30062014
        //                                       :IND-P61-PRE-V-30062014
        //                  ,PRE_RSH_V_30062014     =
        //                :P61-PRE-RSH-V-30062014
        //                                       :IND-P61-PRE-RSH-V-30062014
        //                  ,CPT_INI_30062014       =
        //                :P61-CPT-INI-30062014
        //                                       :IND-P61-CPT-INI-30062014
        //                  ,IMPB_VIS_30062014      =
        //                :P61-IMPB-VIS-30062014
        //                                       :IND-P61-IMPB-VIS-30062014
        //                  ,IMPB_IS_30062014       =
        //                :P61-IMPB-IS-30062014
        //                                       :IND-P61-IMPB-IS-30062014
        //                  ,IMPB_VIS_RP_P62014     =
        //                :P61-IMPB-VIS-RP-P62014
        //                                       :IND-P61-IMPB-VIS-RP-P62014
        //                  ,IMPB_IS_RP_P62014      =
        //                :P61-IMPB-IS-RP-P62014
        //                                       :IND-P61-IMPB-IS-RP-P62014
        //                  ,RIS_MAT_30062014       =
        //                :P61-RIS-MAT-30062014
        //                                       :IND-P61-RIS-MAT-30062014
        //                  ,ID_ADES                =
        //                :P61-ID-ADES
        //                                       :IND-P61-ID-ADES
        //                  ,MONT_LRD_END2000       =
        //                :P61-MONT-LRD-END2000
        //                                       :IND-P61-MONT-LRD-END2000
        //                  ,PRE_LRD_END2000        =
        //                :P61-PRE-LRD-END2000
        //                                       :IND-P61-PRE-LRD-END2000
        //                  ,RENDTO_LRD_END2000     =
        //                :P61-RENDTO-LRD-END2000
        //                                       :IND-P61-RENDTO-LRD-END2000
        //                  ,MONT_LRD_END2006       =
        //                :P61-MONT-LRD-END2006
        //                                       :IND-P61-MONT-LRD-END2006
        //                  ,PRE_LRD_END2006        =
        //                :P61-PRE-LRD-END2006
        //                                       :IND-P61-PRE-LRD-END2006
        //                  ,RENDTO_LRD_END2006     =
        //                :P61-RENDTO-LRD-END2006
        //                                       :IND-P61-RENDTO-LRD-END2006
        //                  ,MONT_LRD_DAL2007       =
        //                :P61-MONT-LRD-DAL2007
        //                                       :IND-P61-MONT-LRD-DAL2007
        //                  ,PRE_LRD_DAL2007        =
        //                :P61-PRE-LRD-DAL2007
        //                                       :IND-P61-PRE-LRD-DAL2007
        //                  ,RENDTO_LRD_DAL2007     =
        //                :P61-RENDTO-LRD-DAL2007
        //                                       :IND-P61-RENDTO-LRD-DAL2007
        //                  ,ID_TRCH_DI_GAR         =
        //                :P61-ID-TRCH-DI-GAR
        //                                       :IND-P61-ID-TRCH-DI-GAR
        //                WHERE     DS_RIGA = :P61-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dCristDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-P61
        //           END-EXEC.
        dCristDao.openCIdUpdEffP61(dCrist.getP61IdDCrist(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-P61
        //           END-EXEC.
        dCristDao.closeCIdUpdEffP61();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-P61
        //           INTO
        //                :P61-ID-D-CRIST
        //               ,:P61-ID-POLI
        //               ,:P61-COD-COMP-ANIA
        //               ,:P61-ID-MOVI-CRZ
        //               ,:P61-ID-MOVI-CHIU
        //                :IND-P61-ID-MOVI-CHIU
        //               ,:P61-DT-INI-EFF-DB
        //               ,:P61-DT-END-EFF-DB
        //               ,:P61-COD-PROD
        //               ,:P61-DT-DECOR-DB
        //               ,:P61-DS-RIGA
        //               ,:P61-DS-OPER-SQL
        //               ,:P61-DS-VER
        //               ,:P61-DS-TS-INI-CPTZ
        //               ,:P61-DS-TS-END-CPTZ
        //               ,:P61-DS-UTENTE
        //               ,:P61-DS-STATO-ELAB
        //               ,:P61-RIS-MAT-31122011
        //                :IND-P61-RIS-MAT-31122011
        //               ,:P61-PRE-V-31122011
        //                :IND-P61-PRE-V-31122011
        //               ,:P61-PRE-RSH-V-31122011
        //                :IND-P61-PRE-RSH-V-31122011
        //               ,:P61-CPT-RIVTO-31122011
        //                :IND-P61-CPT-RIVTO-31122011
        //               ,:P61-IMPB-VIS-31122011
        //                :IND-P61-IMPB-VIS-31122011
        //               ,:P61-IMPB-IS-31122011
        //                :IND-P61-IMPB-IS-31122011
        //               ,:P61-IMPB-VIS-RP-P2011
        //                :IND-P61-IMPB-VIS-RP-P2011
        //               ,:P61-IMPB-IS-RP-P2011
        //                :IND-P61-IMPB-IS-RP-P2011
        //               ,:P61-PRE-V-30062014
        //                :IND-P61-PRE-V-30062014
        //               ,:P61-PRE-RSH-V-30062014
        //                :IND-P61-PRE-RSH-V-30062014
        //               ,:P61-CPT-INI-30062014
        //                :IND-P61-CPT-INI-30062014
        //               ,:P61-IMPB-VIS-30062014
        //                :IND-P61-IMPB-VIS-30062014
        //               ,:P61-IMPB-IS-30062014
        //                :IND-P61-IMPB-IS-30062014
        //               ,:P61-IMPB-VIS-RP-P62014
        //                :IND-P61-IMPB-VIS-RP-P62014
        //               ,:P61-IMPB-IS-RP-P62014
        //                :IND-P61-IMPB-IS-RP-P62014
        //               ,:P61-RIS-MAT-30062014
        //                :IND-P61-RIS-MAT-30062014
        //               ,:P61-ID-ADES
        //                :IND-P61-ID-ADES
        //               ,:P61-MONT-LRD-END2000
        //                :IND-P61-MONT-LRD-END2000
        //               ,:P61-PRE-LRD-END2000
        //                :IND-P61-PRE-LRD-END2000
        //               ,:P61-RENDTO-LRD-END2000
        //                :IND-P61-RENDTO-LRD-END2000
        //               ,:P61-MONT-LRD-END2006
        //                :IND-P61-MONT-LRD-END2006
        //               ,:P61-PRE-LRD-END2006
        //                :IND-P61-PRE-LRD-END2006
        //               ,:P61-RENDTO-LRD-END2006
        //                :IND-P61-RENDTO-LRD-END2006
        //               ,:P61-MONT-LRD-DAL2007
        //                :IND-P61-MONT-LRD-DAL2007
        //               ,:P61-PRE-LRD-DAL2007
        //                :IND-P61-PRE-LRD-DAL2007
        //               ,:P61-RENDTO-LRD-DAL2007
        //                :IND-P61-RENDTO-LRD-DAL2007
        //               ,:P61-ID-TRCH-DI-GAR
        //                :IND-P61-ID-TRCH-DI-GAR
        //           END-EXEC.
        dCristDao.fetchCIdUpdEffP61(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-P61 CURSOR FOR
        //              SELECT
        //                     ID_D_CRIST
        //                    ,ID_POLI
        //                    ,COD_COMP_ANIA
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_PROD
        //                    ,DT_DECOR
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,RIS_MAT_31122011
        //                    ,PRE_V_31122011
        //                    ,PRE_RSH_V_31122011
        //                    ,CPT_RIVTO_31122011
        //                    ,IMPB_VIS_31122011
        //                    ,IMPB_IS_31122011
        //                    ,IMPB_VIS_RP_P2011
        //                    ,IMPB_IS_RP_P2011
        //                    ,PRE_V_30062014
        //                    ,PRE_RSH_V_30062014
        //                    ,CPT_INI_30062014
        //                    ,IMPB_VIS_30062014
        //                    ,IMPB_IS_30062014
        //                    ,IMPB_VIS_RP_P62014
        //                    ,IMPB_IS_RP_P62014
        //                    ,RIS_MAT_30062014
        //                ,ID_ADES
        //                ,MONT_LRD_END2000
        //                ,PRE_LRD_END2000
        //                ,RENDTO_LRD_END2000
        //                ,MONT_LRD_END2006
        //                ,PRE_LRD_END2006
        //                ,RENDTO_LRD_END2006
        //                ,MONT_LRD_DAL2007
        //                ,PRE_LRD_DAL2007
        //                ,RENDTO_LRD_DAL2007
        //                ,ID_TRCH_DI_GAR
        //              FROM D_CRIST
        //              WHERE     ID_POLI = :P61-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_D_CRIST ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_CRIST
        //                ,ID_POLI
        //                ,COD_COMP_ANIA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_PROD
        //                ,DT_DECOR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RIS_MAT_31122011
        //                ,PRE_V_31122011
        //                ,PRE_RSH_V_31122011
        //                ,CPT_RIVTO_31122011
        //                ,IMPB_VIS_31122011
        //                ,IMPB_IS_31122011
        //                ,IMPB_VIS_RP_P2011
        //                ,IMPB_IS_RP_P2011
        //                ,PRE_V_30062014
        //                ,PRE_RSH_V_30062014
        //                ,CPT_INI_30062014
        //                ,IMPB_VIS_30062014
        //                ,IMPB_IS_30062014
        //                ,IMPB_VIS_RP_P62014
        //                ,IMPB_IS_RP_P62014
        //                ,RIS_MAT_30062014
        //                ,ID_ADES
        //                ,MONT_LRD_END2000
        //                ,PRE_LRD_END2000
        //                ,RENDTO_LRD_END2000
        //                ,MONT_LRD_END2006
        //                ,PRE_LRD_END2006
        //                ,RENDTO_LRD_END2006
        //                ,MONT_LRD_DAL2007
        //                ,PRE_LRD_DAL2007
        //                ,RENDTO_LRD_DAL2007
        //                ,ID_TRCH_DI_GAR
        //             INTO
        //                :P61-ID-D-CRIST
        //               ,:P61-ID-POLI
        //               ,:P61-COD-COMP-ANIA
        //               ,:P61-ID-MOVI-CRZ
        //               ,:P61-ID-MOVI-CHIU
        //                :IND-P61-ID-MOVI-CHIU
        //               ,:P61-DT-INI-EFF-DB
        //               ,:P61-DT-END-EFF-DB
        //               ,:P61-COD-PROD
        //               ,:P61-DT-DECOR-DB
        //               ,:P61-DS-RIGA
        //               ,:P61-DS-OPER-SQL
        //               ,:P61-DS-VER
        //               ,:P61-DS-TS-INI-CPTZ
        //               ,:P61-DS-TS-END-CPTZ
        //               ,:P61-DS-UTENTE
        //               ,:P61-DS-STATO-ELAB
        //               ,:P61-RIS-MAT-31122011
        //                :IND-P61-RIS-MAT-31122011
        //               ,:P61-PRE-V-31122011
        //                :IND-P61-PRE-V-31122011
        //               ,:P61-PRE-RSH-V-31122011
        //                :IND-P61-PRE-RSH-V-31122011
        //               ,:P61-CPT-RIVTO-31122011
        //                :IND-P61-CPT-RIVTO-31122011
        //               ,:P61-IMPB-VIS-31122011
        //                :IND-P61-IMPB-VIS-31122011
        //               ,:P61-IMPB-IS-31122011
        //                :IND-P61-IMPB-IS-31122011
        //               ,:P61-IMPB-VIS-RP-P2011
        //                :IND-P61-IMPB-VIS-RP-P2011
        //               ,:P61-IMPB-IS-RP-P2011
        //                :IND-P61-IMPB-IS-RP-P2011
        //               ,:P61-PRE-V-30062014
        //                :IND-P61-PRE-V-30062014
        //               ,:P61-PRE-RSH-V-30062014
        //                :IND-P61-PRE-RSH-V-30062014
        //               ,:P61-CPT-INI-30062014
        //                :IND-P61-CPT-INI-30062014
        //               ,:P61-IMPB-VIS-30062014
        //                :IND-P61-IMPB-VIS-30062014
        //               ,:P61-IMPB-IS-30062014
        //                :IND-P61-IMPB-IS-30062014
        //               ,:P61-IMPB-VIS-RP-P62014
        //                :IND-P61-IMPB-VIS-RP-P62014
        //               ,:P61-IMPB-IS-RP-P62014
        //                :IND-P61-IMPB-IS-RP-P62014
        //               ,:P61-RIS-MAT-30062014
        //                :IND-P61-RIS-MAT-30062014
        //               ,:P61-ID-ADES
        //                :IND-P61-ID-ADES
        //               ,:P61-MONT-LRD-END2000
        //                :IND-P61-MONT-LRD-END2000
        //               ,:P61-PRE-LRD-END2000
        //                :IND-P61-PRE-LRD-END2000
        //               ,:P61-RENDTO-LRD-END2000
        //                :IND-P61-RENDTO-LRD-END2000
        //               ,:P61-MONT-LRD-END2006
        //                :IND-P61-MONT-LRD-END2006
        //               ,:P61-PRE-LRD-END2006
        //                :IND-P61-PRE-LRD-END2006
        //               ,:P61-RENDTO-LRD-END2006
        //                :IND-P61-RENDTO-LRD-END2006
        //               ,:P61-MONT-LRD-DAL2007
        //                :IND-P61-MONT-LRD-DAL2007
        //               ,:P61-PRE-LRD-DAL2007
        //                :IND-P61-PRE-LRD-DAL2007
        //               ,:P61-RENDTO-LRD-DAL2007
        //                :IND-P61-RENDTO-LRD-DAL2007
        //               ,:P61-ID-TRCH-DI-GAR
        //                :IND-P61-ID-TRCH-DI-GAR
        //             FROM D_CRIST
        //             WHERE     ID_POLI = :P61-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dCristDao.selectRec1(dCrist.getP61IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-P61
        //           END-EXEC.
        dCristDao.openCIdpEffP61(dCrist.getP61IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-P61
        //           END-EXEC.
        dCristDao.closeCIdpEffP61();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-P61
        //           INTO
        //                :P61-ID-D-CRIST
        //               ,:P61-ID-POLI
        //               ,:P61-COD-COMP-ANIA
        //               ,:P61-ID-MOVI-CRZ
        //               ,:P61-ID-MOVI-CHIU
        //                :IND-P61-ID-MOVI-CHIU
        //               ,:P61-DT-INI-EFF-DB
        //               ,:P61-DT-END-EFF-DB
        //               ,:P61-COD-PROD
        //               ,:P61-DT-DECOR-DB
        //               ,:P61-DS-RIGA
        //               ,:P61-DS-OPER-SQL
        //               ,:P61-DS-VER
        //               ,:P61-DS-TS-INI-CPTZ
        //               ,:P61-DS-TS-END-CPTZ
        //               ,:P61-DS-UTENTE
        //               ,:P61-DS-STATO-ELAB
        //               ,:P61-RIS-MAT-31122011
        //                :IND-P61-RIS-MAT-31122011
        //               ,:P61-PRE-V-31122011
        //                :IND-P61-PRE-V-31122011
        //               ,:P61-PRE-RSH-V-31122011
        //                :IND-P61-PRE-RSH-V-31122011
        //               ,:P61-CPT-RIVTO-31122011
        //                :IND-P61-CPT-RIVTO-31122011
        //               ,:P61-IMPB-VIS-31122011
        //                :IND-P61-IMPB-VIS-31122011
        //               ,:P61-IMPB-IS-31122011
        //                :IND-P61-IMPB-IS-31122011
        //               ,:P61-IMPB-VIS-RP-P2011
        //                :IND-P61-IMPB-VIS-RP-P2011
        //               ,:P61-IMPB-IS-RP-P2011
        //                :IND-P61-IMPB-IS-RP-P2011
        //               ,:P61-PRE-V-30062014
        //                :IND-P61-PRE-V-30062014
        //               ,:P61-PRE-RSH-V-30062014
        //                :IND-P61-PRE-RSH-V-30062014
        //               ,:P61-CPT-INI-30062014
        //                :IND-P61-CPT-INI-30062014
        //               ,:P61-IMPB-VIS-30062014
        //                :IND-P61-IMPB-VIS-30062014
        //               ,:P61-IMPB-IS-30062014
        //                :IND-P61-IMPB-IS-30062014
        //               ,:P61-IMPB-VIS-RP-P62014
        //                :IND-P61-IMPB-VIS-RP-P62014
        //               ,:P61-IMPB-IS-RP-P62014
        //                :IND-P61-IMPB-IS-RP-P62014
        //               ,:P61-RIS-MAT-30062014
        //                :IND-P61-RIS-MAT-30062014
        //               ,:P61-ID-ADES
        //                :IND-P61-ID-ADES
        //               ,:P61-MONT-LRD-END2000
        //                :IND-P61-MONT-LRD-END2000
        //               ,:P61-PRE-LRD-END2000
        //                :IND-P61-PRE-LRD-END2000
        //               ,:P61-RENDTO-LRD-END2000
        //                :IND-P61-RENDTO-LRD-END2000
        //               ,:P61-MONT-LRD-END2006
        //                :IND-P61-MONT-LRD-END2006
        //               ,:P61-PRE-LRD-END2006
        //                :IND-P61-PRE-LRD-END2006
        //               ,:P61-RENDTO-LRD-END2006
        //                :IND-P61-RENDTO-LRD-END2006
        //               ,:P61-MONT-LRD-DAL2007
        //                :IND-P61-MONT-LRD-DAL2007
        //               ,:P61-PRE-LRD-DAL2007
        //                :IND-P61-PRE-LRD-DAL2007
        //               ,:P61-RENDTO-LRD-DAL2007
        //                :IND-P61-RENDTO-LRD-DAL2007
        //               ,:P61-ID-TRCH-DI-GAR
        //                :IND-P61-ID-TRCH-DI-GAR
        //           END-EXEC.
        dCristDao.fetchCIdpEffP61(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_CRIST
        //                ,ID_POLI
        //                ,COD_COMP_ANIA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_PROD
        //                ,DT_DECOR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RIS_MAT_31122011
        //                ,PRE_V_31122011
        //                ,PRE_RSH_V_31122011
        //                ,CPT_RIVTO_31122011
        //                ,IMPB_VIS_31122011
        //                ,IMPB_IS_31122011
        //                ,IMPB_VIS_RP_P2011
        //                ,IMPB_IS_RP_P2011
        //                ,PRE_V_30062014
        //                ,PRE_RSH_V_30062014
        //                ,CPT_INI_30062014
        //                ,IMPB_VIS_30062014
        //                ,IMPB_IS_30062014
        //                ,IMPB_VIS_RP_P62014
        //                ,IMPB_IS_RP_P62014
        //                ,RIS_MAT_30062014
        //                ,ID_ADES
        //                ,MONT_LRD_END2000
        //                ,PRE_LRD_END2000
        //                ,RENDTO_LRD_END2000
        //                ,MONT_LRD_END2006
        //                ,PRE_LRD_END2006
        //                ,RENDTO_LRD_END2006
        //                ,MONT_LRD_DAL2007
        //                ,PRE_LRD_DAL2007
        //                ,RENDTO_LRD_DAL2007
        //                ,ID_TRCH_DI_GAR
        //             INTO
        //                :P61-ID-D-CRIST
        //               ,:P61-ID-POLI
        //               ,:P61-COD-COMP-ANIA
        //               ,:P61-ID-MOVI-CRZ
        //               ,:P61-ID-MOVI-CHIU
        //                :IND-P61-ID-MOVI-CHIU
        //               ,:P61-DT-INI-EFF-DB
        //               ,:P61-DT-END-EFF-DB
        //               ,:P61-COD-PROD
        //               ,:P61-DT-DECOR-DB
        //               ,:P61-DS-RIGA
        //               ,:P61-DS-OPER-SQL
        //               ,:P61-DS-VER
        //               ,:P61-DS-TS-INI-CPTZ
        //               ,:P61-DS-TS-END-CPTZ
        //               ,:P61-DS-UTENTE
        //               ,:P61-DS-STATO-ELAB
        //               ,:P61-RIS-MAT-31122011
        //                :IND-P61-RIS-MAT-31122011
        //               ,:P61-PRE-V-31122011
        //                :IND-P61-PRE-V-31122011
        //               ,:P61-PRE-RSH-V-31122011
        //                :IND-P61-PRE-RSH-V-31122011
        //               ,:P61-CPT-RIVTO-31122011
        //                :IND-P61-CPT-RIVTO-31122011
        //               ,:P61-IMPB-VIS-31122011
        //                :IND-P61-IMPB-VIS-31122011
        //               ,:P61-IMPB-IS-31122011
        //                :IND-P61-IMPB-IS-31122011
        //               ,:P61-IMPB-VIS-RP-P2011
        //                :IND-P61-IMPB-VIS-RP-P2011
        //               ,:P61-IMPB-IS-RP-P2011
        //                :IND-P61-IMPB-IS-RP-P2011
        //               ,:P61-PRE-V-30062014
        //                :IND-P61-PRE-V-30062014
        //               ,:P61-PRE-RSH-V-30062014
        //                :IND-P61-PRE-RSH-V-30062014
        //               ,:P61-CPT-INI-30062014
        //                :IND-P61-CPT-INI-30062014
        //               ,:P61-IMPB-VIS-30062014
        //                :IND-P61-IMPB-VIS-30062014
        //               ,:P61-IMPB-IS-30062014
        //                :IND-P61-IMPB-IS-30062014
        //               ,:P61-IMPB-VIS-RP-P62014
        //                :IND-P61-IMPB-VIS-RP-P62014
        //               ,:P61-IMPB-IS-RP-P62014
        //                :IND-P61-IMPB-IS-RP-P62014
        //               ,:P61-RIS-MAT-30062014
        //                :IND-P61-RIS-MAT-30062014
        //               ,:P61-ID-ADES
        //                :IND-P61-ID-ADES
        //               ,:P61-MONT-LRD-END2000
        //                :IND-P61-MONT-LRD-END2000
        //               ,:P61-PRE-LRD-END2000
        //                :IND-P61-PRE-LRD-END2000
        //               ,:P61-RENDTO-LRD-END2000
        //                :IND-P61-RENDTO-LRD-END2000
        //               ,:P61-MONT-LRD-END2006
        //                :IND-P61-MONT-LRD-END2006
        //               ,:P61-PRE-LRD-END2006
        //                :IND-P61-PRE-LRD-END2006
        //               ,:P61-RENDTO-LRD-END2006
        //                :IND-P61-RENDTO-LRD-END2006
        //               ,:P61-MONT-LRD-DAL2007
        //                :IND-P61-MONT-LRD-DAL2007
        //               ,:P61-PRE-LRD-DAL2007
        //                :IND-P61-PRE-LRD-DAL2007
        //               ,:P61-RENDTO-LRD-DAL2007
        //                :IND-P61-RENDTO-LRD-DAL2007
        //               ,:P61-ID-TRCH-DI-GAR
        //                :IND-P61-ID-TRCH-DI-GAR
        //             FROM D_CRIST
        //             WHERE     ID_D_CRIST = :P61-ID-D-CRIST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dCristDao.selectRec2(dCrist.getP61IdDCrist(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-P61 CURSOR FOR
        //              SELECT
        //                     ID_D_CRIST
        //                    ,ID_POLI
        //                    ,COD_COMP_ANIA
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_PROD
        //                    ,DT_DECOR
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,RIS_MAT_31122011
        //                    ,PRE_V_31122011
        //                    ,PRE_RSH_V_31122011
        //                    ,CPT_RIVTO_31122011
        //                    ,IMPB_VIS_31122011
        //                    ,IMPB_IS_31122011
        //                    ,IMPB_VIS_RP_P2011
        //                    ,IMPB_IS_RP_P2011
        //                    ,PRE_V_30062014
        //                    ,PRE_RSH_V_30062014
        //                    ,CPT_INI_30062014
        //                    ,IMPB_VIS_30062014
        //                    ,IMPB_IS_30062014
        //                    ,IMPB_VIS_RP_P62014
        //                    ,IMPB_IS_RP_P62014
        //                    ,RIS_MAT_30062014
        //                ,ID_ADES
        //                ,MONT_LRD_END2000
        //                ,PRE_LRD_END2000
        //                ,RENDTO_LRD_END2000
        //                ,MONT_LRD_END2006
        //                ,PRE_LRD_END2006
        //                ,RENDTO_LRD_END2006
        //                ,MONT_LRD_DAL2007
        //                ,PRE_LRD_DAL2007
        //                ,RENDTO_LRD_DAL2007
        //                ,ID_TRCH_DI_GAR
        //              FROM D_CRIST
        //              WHERE     ID_POLI = :P61-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_D_CRIST ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_CRIST
        //                ,ID_POLI
        //                ,COD_COMP_ANIA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_PROD
        //                ,DT_DECOR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RIS_MAT_31122011
        //                ,PRE_V_31122011
        //                ,PRE_RSH_V_31122011
        //                ,CPT_RIVTO_31122011
        //                ,IMPB_VIS_31122011
        //                ,IMPB_IS_31122011
        //                ,IMPB_VIS_RP_P2011
        //                ,IMPB_IS_RP_P2011
        //                ,PRE_V_30062014
        //                ,PRE_RSH_V_30062014
        //                ,CPT_INI_30062014
        //                ,IMPB_VIS_30062014
        //                ,IMPB_IS_30062014
        //                ,IMPB_VIS_RP_P62014
        //                ,IMPB_IS_RP_P62014
        //                ,RIS_MAT_30062014
        //                ,ID_ADES
        //                ,MONT_LRD_END2000
        //                ,PRE_LRD_END2000
        //                ,RENDTO_LRD_END2000
        //                ,MONT_LRD_END2006
        //                ,PRE_LRD_END2006
        //                ,RENDTO_LRD_END2006
        //                ,MONT_LRD_DAL2007
        //                ,PRE_LRD_DAL2007
        //                ,RENDTO_LRD_DAL2007
        //                ,ID_TRCH_DI_GAR
        //             INTO
        //                :P61-ID-D-CRIST
        //               ,:P61-ID-POLI
        //               ,:P61-COD-COMP-ANIA
        //               ,:P61-ID-MOVI-CRZ
        //               ,:P61-ID-MOVI-CHIU
        //                :IND-P61-ID-MOVI-CHIU
        //               ,:P61-DT-INI-EFF-DB
        //               ,:P61-DT-END-EFF-DB
        //               ,:P61-COD-PROD
        //               ,:P61-DT-DECOR-DB
        //               ,:P61-DS-RIGA
        //               ,:P61-DS-OPER-SQL
        //               ,:P61-DS-VER
        //               ,:P61-DS-TS-INI-CPTZ
        //               ,:P61-DS-TS-END-CPTZ
        //               ,:P61-DS-UTENTE
        //               ,:P61-DS-STATO-ELAB
        //               ,:P61-RIS-MAT-31122011
        //                :IND-P61-RIS-MAT-31122011
        //               ,:P61-PRE-V-31122011
        //                :IND-P61-PRE-V-31122011
        //               ,:P61-PRE-RSH-V-31122011
        //                :IND-P61-PRE-RSH-V-31122011
        //               ,:P61-CPT-RIVTO-31122011
        //                :IND-P61-CPT-RIVTO-31122011
        //               ,:P61-IMPB-VIS-31122011
        //                :IND-P61-IMPB-VIS-31122011
        //               ,:P61-IMPB-IS-31122011
        //                :IND-P61-IMPB-IS-31122011
        //               ,:P61-IMPB-VIS-RP-P2011
        //                :IND-P61-IMPB-VIS-RP-P2011
        //               ,:P61-IMPB-IS-RP-P2011
        //                :IND-P61-IMPB-IS-RP-P2011
        //               ,:P61-PRE-V-30062014
        //                :IND-P61-PRE-V-30062014
        //               ,:P61-PRE-RSH-V-30062014
        //                :IND-P61-PRE-RSH-V-30062014
        //               ,:P61-CPT-INI-30062014
        //                :IND-P61-CPT-INI-30062014
        //               ,:P61-IMPB-VIS-30062014
        //                :IND-P61-IMPB-VIS-30062014
        //               ,:P61-IMPB-IS-30062014
        //                :IND-P61-IMPB-IS-30062014
        //               ,:P61-IMPB-VIS-RP-P62014
        //                :IND-P61-IMPB-VIS-RP-P62014
        //               ,:P61-IMPB-IS-RP-P62014
        //                :IND-P61-IMPB-IS-RP-P62014
        //               ,:P61-RIS-MAT-30062014
        //                :IND-P61-RIS-MAT-30062014
        //               ,:P61-ID-ADES
        //                :IND-P61-ID-ADES
        //               ,:P61-MONT-LRD-END2000
        //                :IND-P61-MONT-LRD-END2000
        //               ,:P61-PRE-LRD-END2000
        //                :IND-P61-PRE-LRD-END2000
        //               ,:P61-RENDTO-LRD-END2000
        //                :IND-P61-RENDTO-LRD-END2000
        //               ,:P61-MONT-LRD-END2006
        //                :IND-P61-MONT-LRD-END2006
        //               ,:P61-PRE-LRD-END2006
        //                :IND-P61-PRE-LRD-END2006
        //               ,:P61-RENDTO-LRD-END2006
        //                :IND-P61-RENDTO-LRD-END2006
        //               ,:P61-MONT-LRD-DAL2007
        //                :IND-P61-MONT-LRD-DAL2007
        //               ,:P61-PRE-LRD-DAL2007
        //                :IND-P61-PRE-LRD-DAL2007
        //               ,:P61-RENDTO-LRD-DAL2007
        //                :IND-P61-RENDTO-LRD-DAL2007
        //               ,:P61-ID-TRCH-DI-GAR
        //                :IND-P61-ID-TRCH-DI-GAR
        //             FROM D_CRIST
        //             WHERE     ID_POLI = :P61-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dCristDao.selectRec3(dCrist.getP61IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-P61
        //           END-EXEC.
        dCristDao.openCIdpCpzP61(dCrist.getP61IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-P61
        //           END-EXEC.
        dCristDao.closeCIdpCpzP61();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-P61
        //           INTO
        //                :P61-ID-D-CRIST
        //               ,:P61-ID-POLI
        //               ,:P61-COD-COMP-ANIA
        //               ,:P61-ID-MOVI-CRZ
        //               ,:P61-ID-MOVI-CHIU
        //                :IND-P61-ID-MOVI-CHIU
        //               ,:P61-DT-INI-EFF-DB
        //               ,:P61-DT-END-EFF-DB
        //               ,:P61-COD-PROD
        //               ,:P61-DT-DECOR-DB
        //               ,:P61-DS-RIGA
        //               ,:P61-DS-OPER-SQL
        //               ,:P61-DS-VER
        //               ,:P61-DS-TS-INI-CPTZ
        //               ,:P61-DS-TS-END-CPTZ
        //               ,:P61-DS-UTENTE
        //               ,:P61-DS-STATO-ELAB
        //               ,:P61-RIS-MAT-31122011
        //                :IND-P61-RIS-MAT-31122011
        //               ,:P61-PRE-V-31122011
        //                :IND-P61-PRE-V-31122011
        //               ,:P61-PRE-RSH-V-31122011
        //                :IND-P61-PRE-RSH-V-31122011
        //               ,:P61-CPT-RIVTO-31122011
        //                :IND-P61-CPT-RIVTO-31122011
        //               ,:P61-IMPB-VIS-31122011
        //                :IND-P61-IMPB-VIS-31122011
        //               ,:P61-IMPB-IS-31122011
        //                :IND-P61-IMPB-IS-31122011
        //               ,:P61-IMPB-VIS-RP-P2011
        //                :IND-P61-IMPB-VIS-RP-P2011
        //               ,:P61-IMPB-IS-RP-P2011
        //                :IND-P61-IMPB-IS-RP-P2011
        //               ,:P61-PRE-V-30062014
        //                :IND-P61-PRE-V-30062014
        //               ,:P61-PRE-RSH-V-30062014
        //                :IND-P61-PRE-RSH-V-30062014
        //               ,:P61-CPT-INI-30062014
        //                :IND-P61-CPT-INI-30062014
        //               ,:P61-IMPB-VIS-30062014
        //                :IND-P61-IMPB-VIS-30062014
        //               ,:P61-IMPB-IS-30062014
        //                :IND-P61-IMPB-IS-30062014
        //               ,:P61-IMPB-VIS-RP-P62014
        //                :IND-P61-IMPB-VIS-RP-P62014
        //               ,:P61-IMPB-IS-RP-P62014
        //                :IND-P61-IMPB-IS-RP-P62014
        //               ,:P61-RIS-MAT-30062014
        //                :IND-P61-RIS-MAT-30062014
        //               ,:P61-ID-ADES
        //                :IND-P61-ID-ADES
        //               ,:P61-MONT-LRD-END2000
        //                :IND-P61-MONT-LRD-END2000
        //               ,:P61-PRE-LRD-END2000
        //                :IND-P61-PRE-LRD-END2000
        //               ,:P61-RENDTO-LRD-END2000
        //                :IND-P61-RENDTO-LRD-END2000
        //               ,:P61-MONT-LRD-END2006
        //                :IND-P61-MONT-LRD-END2006
        //               ,:P61-PRE-LRD-END2006
        //                :IND-P61-PRE-LRD-END2006
        //               ,:P61-RENDTO-LRD-END2006
        //                :IND-P61-RENDTO-LRD-END2006
        //               ,:P61-MONT-LRD-DAL2007
        //                :IND-P61-MONT-LRD-DAL2007
        //               ,:P61-PRE-LRD-DAL2007
        //                :IND-P61-PRE-LRD-DAL2007
        //               ,:P61-RENDTO-LRD-DAL2007
        //                :IND-P61-RENDTO-LRD-DAL2007
        //               ,:P61-ID-TRCH-DI-GAR
        //                :IND-P61-ID-TRCH-DI-GAR
        //           END-EXEC.
        dCristDao.fetchCIdpCpzP61(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-P61-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO P61-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndDCrist().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-ID-MOVI-CHIU-NULL
            dCrist.getP61IdMoviChiu().setP61IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61IdMoviChiu.Len.P61_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-P61-RIS-MAT-31122011 = -1
        //              MOVE HIGH-VALUES TO P61-RIS-MAT-31122011-NULL
        //           END-IF
        if (ws.getIndDCrist().getRisMat31122011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-RIS-MAT-31122011-NULL
            dCrist.getP61RisMat31122011().setP61RisMat31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61RisMat31122011.Len.P61_RIS_MAT31122011_NULL));
        }
        // COB_CODE: IF IND-P61-PRE-V-31122011 = -1
        //              MOVE HIGH-VALUES TO P61-PRE-V-31122011-NULL
        //           END-IF
        if (ws.getIndDCrist().getPreV31122011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-PRE-V-31122011-NULL
            dCrist.getP61PreV31122011().setP61PreV31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61PreV31122011.Len.P61_PRE_V31122011_NULL));
        }
        // COB_CODE: IF IND-P61-PRE-RSH-V-31122011 = -1
        //              MOVE HIGH-VALUES TO P61-PRE-RSH-V-31122011-NULL
        //           END-IF
        if (ws.getIndDCrist().getPreRshV31122011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-PRE-RSH-V-31122011-NULL
            dCrist.getP61PreRshV31122011().setP61PreRshV31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61PreRshV31122011.Len.P61_PRE_RSH_V31122011_NULL));
        }
        // COB_CODE: IF IND-P61-CPT-RIVTO-31122011 = -1
        //              MOVE HIGH-VALUES TO P61-CPT-RIVTO-31122011-NULL
        //           END-IF
        if (ws.getIndDCrist().getCptRivto31122011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-CPT-RIVTO-31122011-NULL
            dCrist.getP61CptRivto31122011().setP61CptRivto31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61CptRivto31122011.Len.P61_CPT_RIVTO31122011_NULL));
        }
        // COB_CODE: IF IND-P61-IMPB-VIS-31122011 = -1
        //              MOVE HIGH-VALUES TO P61-IMPB-VIS-31122011-NULL
        //           END-IF
        if (ws.getIndDCrist().getImpbVis31122011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-IMPB-VIS-31122011-NULL
            dCrist.getP61ImpbVis31122011().setP61ImpbVis31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61ImpbVis31122011.Len.P61_IMPB_VIS31122011_NULL));
        }
        // COB_CODE: IF IND-P61-IMPB-IS-31122011 = -1
        //              MOVE HIGH-VALUES TO P61-IMPB-IS-31122011-NULL
        //           END-IF
        if (ws.getIndDCrist().getImpbIs31122011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-IMPB-IS-31122011-NULL
            dCrist.getP61ImpbIs31122011().setP61ImpbIs31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61ImpbIs31122011.Len.P61_IMPB_IS31122011_NULL));
        }
        // COB_CODE: IF IND-P61-IMPB-VIS-RP-P2011 = -1
        //              MOVE HIGH-VALUES TO P61-IMPB-VIS-RP-P2011-NULL
        //           END-IF
        if (ws.getIndDCrist().getImpbVisRpP2011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-IMPB-VIS-RP-P2011-NULL
            dCrist.getP61ImpbVisRpP2011().setP61ImpbVisRpP2011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61ImpbVisRpP2011.Len.P61_IMPB_VIS_RP_P2011_NULL));
        }
        // COB_CODE: IF IND-P61-IMPB-IS-RP-P2011 = -1
        //              MOVE HIGH-VALUES TO P61-IMPB-IS-RP-P2011-NULL
        //           END-IF
        if (ws.getIndDCrist().getImpbIsRpP2011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-IMPB-IS-RP-P2011-NULL
            dCrist.getP61ImpbIsRpP2011().setP61ImpbIsRpP2011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61ImpbIsRpP2011.Len.P61_IMPB_IS_RP_P2011_NULL));
        }
        // COB_CODE: IF IND-P61-PRE-V-30062014 = -1
        //              MOVE HIGH-VALUES TO P61-PRE-V-30062014-NULL
        //           END-IF
        if (ws.getIndDCrist().getPreV30062014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-PRE-V-30062014-NULL
            dCrist.getP61PreV30062014().setP61PreV30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61PreV30062014.Len.P61_PRE_V30062014_NULL));
        }
        // COB_CODE: IF IND-P61-PRE-RSH-V-30062014 = -1
        //              MOVE HIGH-VALUES TO P61-PRE-RSH-V-30062014-NULL
        //           END-IF
        if (ws.getIndDCrist().getPreRshV30062014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-PRE-RSH-V-30062014-NULL
            dCrist.getP61PreRshV30062014().setP61PreRshV30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61PreRshV30062014.Len.P61_PRE_RSH_V30062014_NULL));
        }
        // COB_CODE: IF IND-P61-CPT-INI-30062014 = -1
        //              MOVE HIGH-VALUES TO P61-CPT-INI-30062014-NULL
        //           END-IF
        if (ws.getIndDCrist().getCptIni30062014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-CPT-INI-30062014-NULL
            dCrist.getP61CptIni30062014().setP61CptIni30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61CptIni30062014.Len.P61_CPT_INI30062014_NULL));
        }
        // COB_CODE: IF IND-P61-IMPB-VIS-30062014 = -1
        //              MOVE HIGH-VALUES TO P61-IMPB-VIS-30062014-NULL
        //           END-IF
        if (ws.getIndDCrist().getImpbVis30062014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-IMPB-VIS-30062014-NULL
            dCrist.getP61ImpbVis30062014().setP61ImpbVis30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61ImpbVis30062014.Len.P61_IMPB_VIS30062014_NULL));
        }
        // COB_CODE: IF IND-P61-IMPB-IS-30062014 = -1
        //              MOVE HIGH-VALUES TO P61-IMPB-IS-30062014-NULL
        //           END-IF
        if (ws.getIndDCrist().getImpbIs30062014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-IMPB-IS-30062014-NULL
            dCrist.getP61ImpbIs30062014().setP61ImpbIs30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61ImpbIs30062014.Len.P61_IMPB_IS30062014_NULL));
        }
        // COB_CODE: IF IND-P61-IMPB-VIS-RP-P62014 = -1
        //              MOVE HIGH-VALUES TO P61-IMPB-VIS-RP-P62014-NULL
        //           END-IF
        if (ws.getIndDCrist().getImpbVisRpP62014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-IMPB-VIS-RP-P62014-NULL
            dCrist.getP61ImpbVisRpP62014().setP61ImpbVisRpP62014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61ImpbVisRpP62014.Len.P61_IMPB_VIS_RP_P62014_NULL));
        }
        // COB_CODE: IF IND-P61-IMPB-IS-RP-P62014 = -1
        //              MOVE HIGH-VALUES TO P61-IMPB-IS-RP-P62014-NULL
        //           END-IF
        if (ws.getIndDCrist().getImpbIsRpP62014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-IMPB-IS-RP-P62014-NULL
            dCrist.getP61ImpbIsRpP62014().setP61ImpbIsRpP62014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61ImpbIsRpP62014.Len.P61_IMPB_IS_RP_P62014_NULL));
        }
        // COB_CODE: IF IND-P61-RIS-MAT-30062014 = -1
        //              MOVE HIGH-VALUES TO P61-RIS-MAT-30062014-NULL
        //           END-IF
        if (ws.getIndDCrist().getRisMat30062014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-RIS-MAT-30062014-NULL
            dCrist.getP61RisMat30062014().setP61RisMat30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61RisMat30062014.Len.P61_RIS_MAT30062014_NULL));
        }
        // COB_CODE: IF IND-P61-ID-ADES = -1
        //              MOVE HIGH-VALUES TO P61-ID-ADES-NULL
        //           END-IF
        if (ws.getIndDCrist().getIdAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-ID-ADES-NULL
            dCrist.getP61IdAdes().setP61IdAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61IdAdes.Len.P61_ID_ADES_NULL));
        }
        // COB_CODE: IF IND-P61-MONT-LRD-END2000 = -1
        //              MOVE HIGH-VALUES TO P61-MONT-LRD-END2000-NULL
        //           END-IF
        if (ws.getIndDCrist().getMontLrdEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-MONT-LRD-END2000-NULL
            dCrist.getP61MontLrdEnd2000().setP61MontLrdEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61MontLrdEnd2000.Len.P61_MONT_LRD_END2000_NULL));
        }
        // COB_CODE: IF IND-P61-PRE-LRD-END2000 = -1
        //              MOVE HIGH-VALUES TO P61-PRE-LRD-END2000-NULL
        //           END-IF
        if (ws.getIndDCrist().getPreLrdEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-PRE-LRD-END2000-NULL
            dCrist.getP61PreLrdEnd2000().setP61PreLrdEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61PreLrdEnd2000.Len.P61_PRE_LRD_END2000_NULL));
        }
        // COB_CODE: IF IND-P61-RENDTO-LRD-END2000 = -1
        //              MOVE HIGH-VALUES TO P61-RENDTO-LRD-END2000-NULL
        //           END-IF
        if (ws.getIndDCrist().getRendtoLrdEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-RENDTO-LRD-END2000-NULL
            dCrist.getP61RendtoLrdEnd2000().setP61RendtoLrdEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61RendtoLrdEnd2000.Len.P61_RENDTO_LRD_END2000_NULL));
        }
        // COB_CODE: IF IND-P61-MONT-LRD-END2006 = -1
        //              MOVE HIGH-VALUES TO P61-MONT-LRD-END2006-NULL
        //           END-IF
        if (ws.getIndDCrist().getMontLrdEnd2006() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-MONT-LRD-END2006-NULL
            dCrist.getP61MontLrdEnd2006().setP61MontLrdEnd2006Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61MontLrdEnd2006.Len.P61_MONT_LRD_END2006_NULL));
        }
        // COB_CODE: IF IND-P61-PRE-LRD-END2006 = -1
        //              MOVE HIGH-VALUES TO P61-PRE-LRD-END2006-NULL
        //           END-IF
        if (ws.getIndDCrist().getPreLrdEnd2006() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-PRE-LRD-END2006-NULL
            dCrist.getP61PreLrdEnd2006().setP61PreLrdEnd2006Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61PreLrdEnd2006.Len.P61_PRE_LRD_END2006_NULL));
        }
        // COB_CODE: IF IND-P61-RENDTO-LRD-END2006 = -1
        //              MOVE HIGH-VALUES TO P61-RENDTO-LRD-END2006-NULL
        //           END-IF
        if (ws.getIndDCrist().getRendtoLrdEnd2006() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-RENDTO-LRD-END2006-NULL
            dCrist.getP61RendtoLrdEnd2006().setP61RendtoLrdEnd2006Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61RendtoLrdEnd2006.Len.P61_RENDTO_LRD_END2006_NULL));
        }
        // COB_CODE: IF IND-P61-MONT-LRD-DAL2007 = -1
        //              MOVE HIGH-VALUES TO P61-MONT-LRD-DAL2007-NULL
        //           END-IF
        if (ws.getIndDCrist().getMontLrdDal2007() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-MONT-LRD-DAL2007-NULL
            dCrist.getP61MontLrdDal2007().setP61MontLrdDal2007Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61MontLrdDal2007.Len.P61_MONT_LRD_DAL2007_NULL));
        }
        // COB_CODE: IF IND-P61-PRE-LRD-DAL2007 = -1
        //              MOVE HIGH-VALUES TO P61-PRE-LRD-DAL2007-NULL
        //           END-IF
        if (ws.getIndDCrist().getPreLrdDal2007() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-PRE-LRD-DAL2007-NULL
            dCrist.getP61PreLrdDal2007().setP61PreLrdDal2007Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61PreLrdDal2007.Len.P61_PRE_LRD_DAL2007_NULL));
        }
        // COB_CODE: IF IND-P61-RENDTO-LRD-DAL2007 = -1
        //              MOVE HIGH-VALUES TO P61-RENDTO-LRD-DAL2007-NULL
        //           END-IF
        if (ws.getIndDCrist().getRendtoLrdDal2007() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-RENDTO-LRD-DAL2007-NULL
            dCrist.getP61RendtoLrdDal2007().setP61RendtoLrdDal2007Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61RendtoLrdDal2007.Len.P61_RENDTO_LRD_DAL2007_NULL));
        }
        // COB_CODE: IF IND-P61-ID-TRCH-DI-GAR = -1
        //              MOVE HIGH-VALUES TO P61-ID-TRCH-DI-GAR-NULL
        //           END-IF.
        if (ws.getIndDCrist().getIdTrchDiGar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P61-ID-TRCH-DI-GAR-NULL
            dCrist.getP61IdTrchDiGar().setP61IdTrchDiGarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61IdTrchDiGar.Len.P61_ID_TRCH_DI_GAR_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO P61-DS-OPER-SQL
        dCrist.setP61DsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO P61-DS-VER
        dCrist.setP61DsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P61-DS-UTENTE
        dCrist.setP61DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO P61-DS-STATO-ELAB.
        dCrist.setP61DsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO P61-DS-OPER-SQL
        dCrist.setP61DsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P61-DS-UTENTE.
        dCrist.setP61DsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF P61-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-P61-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61IdMoviChiu().getP61IdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-ID-MOVI-CHIU
            ws.getIndDCrist().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-ID-MOVI-CHIU
            ws.getIndDCrist().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF P61-RIS-MAT-31122011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-RIS-MAT-31122011
        //           ELSE
        //              MOVE 0 TO IND-P61-RIS-MAT-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61RisMat31122011().getP61RisMat31122011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-RIS-MAT-31122011
            ws.getIndDCrist().setRisMat31122011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-RIS-MAT-31122011
            ws.getIndDCrist().setRisMat31122011(((short)0));
        }
        // COB_CODE: IF P61-PRE-V-31122011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-PRE-V-31122011
        //           ELSE
        //              MOVE 0 TO IND-P61-PRE-V-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61PreV31122011().getP61PreV31122011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-PRE-V-31122011
            ws.getIndDCrist().setPreV31122011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-PRE-V-31122011
            ws.getIndDCrist().setPreV31122011(((short)0));
        }
        // COB_CODE: IF P61-PRE-RSH-V-31122011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-PRE-RSH-V-31122011
        //           ELSE
        //              MOVE 0 TO IND-P61-PRE-RSH-V-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61PreRshV31122011().getP61PreRshV31122011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-PRE-RSH-V-31122011
            ws.getIndDCrist().setPreRshV31122011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-PRE-RSH-V-31122011
            ws.getIndDCrist().setPreRshV31122011(((short)0));
        }
        // COB_CODE: IF P61-CPT-RIVTO-31122011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-CPT-RIVTO-31122011
        //           ELSE
        //              MOVE 0 TO IND-P61-CPT-RIVTO-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61CptRivto31122011().getP61CptRivto31122011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-CPT-RIVTO-31122011
            ws.getIndDCrist().setCptRivto31122011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-CPT-RIVTO-31122011
            ws.getIndDCrist().setCptRivto31122011(((short)0));
        }
        // COB_CODE: IF P61-IMPB-VIS-31122011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-IMPB-VIS-31122011
        //           ELSE
        //              MOVE 0 TO IND-P61-IMPB-VIS-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61ImpbVis31122011().getP61ImpbVis31122011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-IMPB-VIS-31122011
            ws.getIndDCrist().setImpbVis31122011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-IMPB-VIS-31122011
            ws.getIndDCrist().setImpbVis31122011(((short)0));
        }
        // COB_CODE: IF P61-IMPB-IS-31122011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-IMPB-IS-31122011
        //           ELSE
        //              MOVE 0 TO IND-P61-IMPB-IS-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61ImpbIs31122011().getP61ImpbIs31122011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-IMPB-IS-31122011
            ws.getIndDCrist().setImpbIs31122011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-IMPB-IS-31122011
            ws.getIndDCrist().setImpbIs31122011(((short)0));
        }
        // COB_CODE: IF P61-IMPB-VIS-RP-P2011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-IMPB-VIS-RP-P2011
        //           ELSE
        //              MOVE 0 TO IND-P61-IMPB-VIS-RP-P2011
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61ImpbVisRpP2011().getP61ImpbVisRpP2011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-IMPB-VIS-RP-P2011
            ws.getIndDCrist().setImpbVisRpP2011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-IMPB-VIS-RP-P2011
            ws.getIndDCrist().setImpbVisRpP2011(((short)0));
        }
        // COB_CODE: IF P61-IMPB-IS-RP-P2011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-IMPB-IS-RP-P2011
        //           ELSE
        //              MOVE 0 TO IND-P61-IMPB-IS-RP-P2011
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61ImpbIsRpP2011().getP61ImpbIsRpP2011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-IMPB-IS-RP-P2011
            ws.getIndDCrist().setImpbIsRpP2011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-IMPB-IS-RP-P2011
            ws.getIndDCrist().setImpbIsRpP2011(((short)0));
        }
        // COB_CODE: IF P61-PRE-V-30062014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-PRE-V-30062014
        //           ELSE
        //              MOVE 0 TO IND-P61-PRE-V-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61PreV30062014().getP61PreV30062014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-PRE-V-30062014
            ws.getIndDCrist().setPreV30062014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-PRE-V-30062014
            ws.getIndDCrist().setPreV30062014(((short)0));
        }
        // COB_CODE: IF P61-PRE-RSH-V-30062014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-PRE-RSH-V-30062014
        //           ELSE
        //              MOVE 0 TO IND-P61-PRE-RSH-V-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61PreRshV30062014().getP61PreRshV30062014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-PRE-RSH-V-30062014
            ws.getIndDCrist().setPreRshV30062014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-PRE-RSH-V-30062014
            ws.getIndDCrist().setPreRshV30062014(((short)0));
        }
        // COB_CODE: IF P61-CPT-INI-30062014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-CPT-INI-30062014
        //           ELSE
        //              MOVE 0 TO IND-P61-CPT-INI-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61CptIni30062014().getP61CptIni30062014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-CPT-INI-30062014
            ws.getIndDCrist().setCptIni30062014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-CPT-INI-30062014
            ws.getIndDCrist().setCptIni30062014(((short)0));
        }
        // COB_CODE: IF P61-IMPB-VIS-30062014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-IMPB-VIS-30062014
        //           ELSE
        //              MOVE 0 TO IND-P61-IMPB-VIS-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61ImpbVis30062014().getP61ImpbVis30062014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-IMPB-VIS-30062014
            ws.getIndDCrist().setImpbVis30062014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-IMPB-VIS-30062014
            ws.getIndDCrist().setImpbVis30062014(((short)0));
        }
        // COB_CODE: IF P61-IMPB-IS-30062014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-IMPB-IS-30062014
        //           ELSE
        //              MOVE 0 TO IND-P61-IMPB-IS-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61ImpbIs30062014().getP61ImpbIs30062014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-IMPB-IS-30062014
            ws.getIndDCrist().setImpbIs30062014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-IMPB-IS-30062014
            ws.getIndDCrist().setImpbIs30062014(((short)0));
        }
        // COB_CODE: IF P61-IMPB-VIS-RP-P62014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-IMPB-VIS-RP-P62014
        //           ELSE
        //              MOVE 0 TO IND-P61-IMPB-VIS-RP-P62014
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61ImpbVisRpP62014().getP61ImpbVisRpP62014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-IMPB-VIS-RP-P62014
            ws.getIndDCrist().setImpbVisRpP62014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-IMPB-VIS-RP-P62014
            ws.getIndDCrist().setImpbVisRpP62014(((short)0));
        }
        // COB_CODE: IF P61-IMPB-IS-RP-P62014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-IMPB-IS-RP-P62014
        //           ELSE
        //              MOVE 0 TO IND-P61-IMPB-IS-RP-P62014
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61ImpbIsRpP62014().getP61ImpbIsRpP62014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-IMPB-IS-RP-P62014
            ws.getIndDCrist().setImpbIsRpP62014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-IMPB-IS-RP-P62014
            ws.getIndDCrist().setImpbIsRpP62014(((short)0));
        }
        // COB_CODE: IF P61-RIS-MAT-30062014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-RIS-MAT-30062014
        //           ELSE
        //              MOVE 0 TO IND-P61-RIS-MAT-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61RisMat30062014().getP61RisMat30062014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-RIS-MAT-30062014
            ws.getIndDCrist().setRisMat30062014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-RIS-MAT-30062014
            ws.getIndDCrist().setRisMat30062014(((short)0));
        }
        // COB_CODE: IF P61-ID-ADES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-ID-ADES
        //           ELSE
        //              MOVE 0 TO IND-P61-ID-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61IdAdes().getP61IdAdesNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-ID-ADES
            ws.getIndDCrist().setIdAdes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-ID-ADES
            ws.getIndDCrist().setIdAdes(((short)0));
        }
        // COB_CODE: IF P61-MONT-LRD-END2000-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-MONT-LRD-END2000
        //           ELSE
        //              MOVE 0 TO IND-P61-MONT-LRD-END2000
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61MontLrdEnd2000().getP61MontLrdEnd2000NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-MONT-LRD-END2000
            ws.getIndDCrist().setMontLrdEnd2000(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-MONT-LRD-END2000
            ws.getIndDCrist().setMontLrdEnd2000(((short)0));
        }
        // COB_CODE: IF P61-PRE-LRD-END2000-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-PRE-LRD-END2000
        //           ELSE
        //              MOVE 0 TO IND-P61-PRE-LRD-END2000
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61PreLrdEnd2000().getP61PreLrdEnd2000NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-PRE-LRD-END2000
            ws.getIndDCrist().setPreLrdEnd2000(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-PRE-LRD-END2000
            ws.getIndDCrist().setPreLrdEnd2000(((short)0));
        }
        // COB_CODE: IF P61-RENDTO-LRD-END2000-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-RENDTO-LRD-END2000
        //           ELSE
        //              MOVE 0 TO IND-P61-RENDTO-LRD-END2000
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61RendtoLrdEnd2000().getP61RendtoLrdEnd2000NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-RENDTO-LRD-END2000
            ws.getIndDCrist().setRendtoLrdEnd2000(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-RENDTO-LRD-END2000
            ws.getIndDCrist().setRendtoLrdEnd2000(((short)0));
        }
        // COB_CODE: IF P61-MONT-LRD-END2006-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-MONT-LRD-END2006
        //           ELSE
        //              MOVE 0 TO IND-P61-MONT-LRD-END2006
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61MontLrdEnd2006().getP61MontLrdEnd2006NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-MONT-LRD-END2006
            ws.getIndDCrist().setMontLrdEnd2006(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-MONT-LRD-END2006
            ws.getIndDCrist().setMontLrdEnd2006(((short)0));
        }
        // COB_CODE: IF P61-PRE-LRD-END2006-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-PRE-LRD-END2006
        //           ELSE
        //              MOVE 0 TO IND-P61-PRE-LRD-END2006
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61PreLrdEnd2006().getP61PreLrdEnd2006NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-PRE-LRD-END2006
            ws.getIndDCrist().setPreLrdEnd2006(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-PRE-LRD-END2006
            ws.getIndDCrist().setPreLrdEnd2006(((short)0));
        }
        // COB_CODE: IF P61-RENDTO-LRD-END2006-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-RENDTO-LRD-END2006
        //           ELSE
        //              MOVE 0 TO IND-P61-RENDTO-LRD-END2006
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61RendtoLrdEnd2006().getP61RendtoLrdEnd2006NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-RENDTO-LRD-END2006
            ws.getIndDCrist().setRendtoLrdEnd2006(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-RENDTO-LRD-END2006
            ws.getIndDCrist().setRendtoLrdEnd2006(((short)0));
        }
        // COB_CODE: IF P61-MONT-LRD-DAL2007-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-MONT-LRD-DAL2007
        //           ELSE
        //              MOVE 0 TO IND-P61-MONT-LRD-DAL2007
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61MontLrdDal2007().getP61MontLrdDal2007NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-MONT-LRD-DAL2007
            ws.getIndDCrist().setMontLrdDal2007(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-MONT-LRD-DAL2007
            ws.getIndDCrist().setMontLrdDal2007(((short)0));
        }
        // COB_CODE: IF P61-PRE-LRD-DAL2007-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-PRE-LRD-DAL2007
        //           ELSE
        //              MOVE 0 TO IND-P61-PRE-LRD-DAL2007
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61PreLrdDal2007().getP61PreLrdDal2007NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-PRE-LRD-DAL2007
            ws.getIndDCrist().setPreLrdDal2007(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-PRE-LRD-DAL2007
            ws.getIndDCrist().setPreLrdDal2007(((short)0));
        }
        // COB_CODE: IF P61-RENDTO-LRD-DAL2007-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-RENDTO-LRD-DAL2007
        //           ELSE
        //              MOVE 0 TO IND-P61-RENDTO-LRD-DAL2007
        //           END-IF
        if (Characters.EQ_HIGH.test(dCrist.getP61RendtoLrdDal2007().getP61RendtoLrdDal2007NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-RENDTO-LRD-DAL2007
            ws.getIndDCrist().setRendtoLrdDal2007(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-RENDTO-LRD-DAL2007
            ws.getIndDCrist().setRendtoLrdDal2007(((short)0));
        }
        // COB_CODE: IF P61-ID-TRCH-DI-GAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P61-ID-TRCH-DI-GAR
        //           ELSE
        //              MOVE 0 TO IND-P61-ID-TRCH-DI-GAR
        //           END-IF.
        if (Characters.EQ_HIGH.test(dCrist.getP61IdTrchDiGar().getP61IdTrchDiGarNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P61-ID-TRCH-DI-GAR
            ws.getIndDCrist().setIdTrchDiGar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P61-ID-TRCH-DI-GAR
            ws.getIndDCrist().setIdTrchDiGar(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : P61-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE D-CRIST TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(dCrist.getdCristFormatted());
        // COB_CODE: MOVE P61-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(dCrist.getP61IdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO P61-ID-MOVI-CHIU
                dCrist.getP61IdMoviChiu().setP61IdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO P61-DS-TS-END-CPTZ
                dCrist.setP61DsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO P61-ID-MOVI-CRZ
                    dCrist.setP61IdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO P61-ID-MOVI-CHIU-NULL
                    dCrist.getP61IdMoviChiu().setP61IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61IdMoviChiu.Len.P61_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO P61-DT-END-EFF
                    dCrist.setP61DtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO P61-DS-TS-INI-CPTZ
                    dCrist.setP61DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO P61-DS-TS-END-CPTZ
                    dCrist.setP61DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE D-CRIST TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(dCrist.getdCristFormatted());
        // COB_CODE: MOVE P61-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(dCrist.getP61IdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO D-CRIST.
        dCrist.setdCristFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO P61-ID-MOVI-CRZ.
        dCrist.setP61IdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO P61-ID-MOVI-CHIU-NULL.
        dCrist.getP61IdMoviChiu().setP61IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P61IdMoviChiu.Len.P61_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO P61-DT-INI-EFF.
        dCrist.setP61DtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO P61-DT-END-EFF.
        dCrist.setP61DtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO P61-DS-TS-INI-CPTZ.
        dCrist.setP61DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO P61-DS-TS-END-CPTZ.
        dCrist.setP61DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO P61-COD-COMP-ANIA.
        dCrist.setP61CodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE P61-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dCrist.getP61DtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P61-DT-INI-EFF-DB
        ws.getdCristDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE P61-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dCrist.getP61DtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P61-DT-END-EFF-DB
        ws.getdCristDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE P61-DT-DECOR TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dCrist.getP61DtDecor(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P61-DT-DECOR-DB.
        ws.getdCristDb().setDecorDb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE P61-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getdCristDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P61-DT-INI-EFF
        dCrist.setP61DtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P61-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getdCristDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P61-DT-END-EFF
        dCrist.setP61DtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P61-DT-DECOR-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getdCristDb().getDecorDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P61-DT-DECOR.
        dCrist.setP61DtDecor(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return dCrist.getP61CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.dCrist.setP61CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodProd() {
        return dCrist.getP61CodProd();
    }

    @Override
    public void setCodProd(String codProd) {
        this.dCrist.setP61CodProd(codProd);
    }

    @Override
    public AfDecimal getCptIni30062014() {
        return dCrist.getP61CptIni30062014().getP61CptIni30062014();
    }

    @Override
    public void setCptIni30062014(AfDecimal cptIni30062014) {
        this.dCrist.getP61CptIni30062014().setP61CptIni30062014(cptIni30062014.copy());
    }

    @Override
    public AfDecimal getCptIni30062014Obj() {
        if (ws.getIndDCrist().getCptIni30062014() >= 0) {
            return getCptIni30062014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptIni30062014Obj(AfDecimal cptIni30062014Obj) {
        if (cptIni30062014Obj != null) {
            setCptIni30062014(new AfDecimal(cptIni30062014Obj, 15, 3));
            ws.getIndDCrist().setCptIni30062014(((short)0));
        }
        else {
            ws.getIndDCrist().setCptIni30062014(((short)-1));
        }
    }

    @Override
    public AfDecimal getCptRivto31122011() {
        return dCrist.getP61CptRivto31122011().getP61CptRivto31122011();
    }

    @Override
    public void setCptRivto31122011(AfDecimal cptRivto31122011) {
        this.dCrist.getP61CptRivto31122011().setP61CptRivto31122011(cptRivto31122011.copy());
    }

    @Override
    public AfDecimal getCptRivto31122011Obj() {
        if (ws.getIndDCrist().getCptRivto31122011() >= 0) {
            return getCptRivto31122011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptRivto31122011Obj(AfDecimal cptRivto31122011Obj) {
        if (cptRivto31122011Obj != null) {
            setCptRivto31122011(new AfDecimal(cptRivto31122011Obj, 15, 3));
            ws.getIndDCrist().setCptRivto31122011(((short)0));
        }
        else {
            ws.getIndDCrist().setCptRivto31122011(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return dCrist.getP61DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.dCrist.setP61DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return dCrist.getP61DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.dCrist.setP61DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return dCrist.getP61DsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dCrist.setP61DsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return dCrist.getP61DsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dCrist.setP61DsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return dCrist.getP61DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.dCrist.setP61DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return dCrist.getP61DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.dCrist.setP61DsVer(dsVer);
    }

    @Override
    public String getDtDecorDb() {
        return ws.getdCristDb().getDecorDb();
    }

    @Override
    public void setDtDecorDb(String dtDecorDb) {
        this.ws.getdCristDb().setDecorDb(dtDecorDb);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getdCristDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getdCristDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getdCristDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getdCristDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public int getIdAdes() {
        return dCrist.getP61IdAdes().getP61IdAdes();
    }

    @Override
    public void setIdAdes(int idAdes) {
        this.dCrist.getP61IdAdes().setP61IdAdes(idAdes);
    }

    @Override
    public Integer getIdAdesObj() {
        if (ws.getIndDCrist().getIdAdes() >= 0) {
            return ((Integer)getIdAdes());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdAdesObj(Integer idAdesObj) {
        if (idAdesObj != null) {
            setIdAdes(((int)idAdesObj));
            ws.getIndDCrist().setIdAdes(((short)0));
        }
        else {
            ws.getIndDCrist().setIdAdes(((short)-1));
        }
    }

    @Override
    public int getIdDCrist() {
        return dCrist.getP61IdDCrist();
    }

    @Override
    public void setIdDCrist(int idDCrist) {
        this.dCrist.setP61IdDCrist(idDCrist);
    }

    @Override
    public int getIdMoviChiu() {
        return dCrist.getP61IdMoviChiu().getP61IdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.dCrist.getP61IdMoviChiu().setP61IdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndDCrist().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndDCrist().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndDCrist().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return dCrist.getP61IdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.dCrist.setP61IdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPoli() {
        return dCrist.getP61IdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.dCrist.setP61IdPoli(idPoli);
    }

    @Override
    public int getIdTrchDiGar() {
        return dCrist.getP61IdTrchDiGar().getP61IdTrchDiGar();
    }

    @Override
    public void setIdTrchDiGar(int idTrchDiGar) {
        this.dCrist.getP61IdTrchDiGar().setP61IdTrchDiGar(idTrchDiGar);
    }

    @Override
    public Integer getIdTrchDiGarObj() {
        if (ws.getIndDCrist().getIdTrchDiGar() >= 0) {
            return ((Integer)getIdTrchDiGar());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdTrchDiGarObj(Integer idTrchDiGarObj) {
        if (idTrchDiGarObj != null) {
            setIdTrchDiGar(((int)idTrchDiGarObj));
            ws.getIndDCrist().setIdTrchDiGar(((short)0));
        }
        else {
            ws.getIndDCrist().setIdTrchDiGar(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs30062014() {
        return dCrist.getP61ImpbIs30062014().getP61ImpbIs30062014();
    }

    @Override
    public void setImpbIs30062014(AfDecimal impbIs30062014) {
        this.dCrist.getP61ImpbIs30062014().setP61ImpbIs30062014(impbIs30062014.copy());
    }

    @Override
    public AfDecimal getImpbIs30062014Obj() {
        if (ws.getIndDCrist().getImpbIs30062014() >= 0) {
            return getImpbIs30062014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs30062014Obj(AfDecimal impbIs30062014Obj) {
        if (impbIs30062014Obj != null) {
            setImpbIs30062014(new AfDecimal(impbIs30062014Obj, 15, 3));
            ws.getIndDCrist().setImpbIs30062014(((short)0));
        }
        else {
            ws.getIndDCrist().setImpbIs30062014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs31122011() {
        return dCrist.getP61ImpbIs31122011().getP61ImpbIs31122011();
    }

    @Override
    public void setImpbIs31122011(AfDecimal impbIs31122011) {
        this.dCrist.getP61ImpbIs31122011().setP61ImpbIs31122011(impbIs31122011.copy());
    }

    @Override
    public AfDecimal getImpbIs31122011Obj() {
        if (ws.getIndDCrist().getImpbIs31122011() >= 0) {
            return getImpbIs31122011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs31122011Obj(AfDecimal impbIs31122011Obj) {
        if (impbIs31122011Obj != null) {
            setImpbIs31122011(new AfDecimal(impbIs31122011Obj, 15, 3));
            ws.getIndDCrist().setImpbIs31122011(((short)0));
        }
        else {
            ws.getIndDCrist().setImpbIs31122011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIsRpP2011() {
        return dCrist.getP61ImpbIsRpP2011().getP61ImpbIsRpP2011();
    }

    @Override
    public void setImpbIsRpP2011(AfDecimal impbIsRpP2011) {
        this.dCrist.getP61ImpbIsRpP2011().setP61ImpbIsRpP2011(impbIsRpP2011.copy());
    }

    @Override
    public AfDecimal getImpbIsRpP2011Obj() {
        if (ws.getIndDCrist().getImpbIsRpP2011() >= 0) {
            return getImpbIsRpP2011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsRpP2011Obj(AfDecimal impbIsRpP2011Obj) {
        if (impbIsRpP2011Obj != null) {
            setImpbIsRpP2011(new AfDecimal(impbIsRpP2011Obj, 15, 3));
            ws.getIndDCrist().setImpbIsRpP2011(((short)0));
        }
        else {
            ws.getIndDCrist().setImpbIsRpP2011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIsRpP62014() {
        return dCrist.getP61ImpbIsRpP62014().getP61ImpbIsRpP62014();
    }

    @Override
    public void setImpbIsRpP62014(AfDecimal impbIsRpP62014) {
        this.dCrist.getP61ImpbIsRpP62014().setP61ImpbIsRpP62014(impbIsRpP62014.copy());
    }

    @Override
    public AfDecimal getImpbIsRpP62014Obj() {
        if (ws.getIndDCrist().getImpbIsRpP62014() >= 0) {
            return getImpbIsRpP62014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsRpP62014Obj(AfDecimal impbIsRpP62014Obj) {
        if (impbIsRpP62014Obj != null) {
            setImpbIsRpP62014(new AfDecimal(impbIsRpP62014Obj, 15, 3));
            ws.getIndDCrist().setImpbIsRpP62014(((short)0));
        }
        else {
            ws.getIndDCrist().setImpbIsRpP62014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis30062014() {
        return dCrist.getP61ImpbVis30062014().getP61ImpbVis30062014();
    }

    @Override
    public void setImpbVis30062014(AfDecimal impbVis30062014) {
        this.dCrist.getP61ImpbVis30062014().setP61ImpbVis30062014(impbVis30062014.copy());
    }

    @Override
    public AfDecimal getImpbVis30062014Obj() {
        if (ws.getIndDCrist().getImpbVis30062014() >= 0) {
            return getImpbVis30062014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis30062014Obj(AfDecimal impbVis30062014Obj) {
        if (impbVis30062014Obj != null) {
            setImpbVis30062014(new AfDecimal(impbVis30062014Obj, 15, 3));
            ws.getIndDCrist().setImpbVis30062014(((short)0));
        }
        else {
            ws.getIndDCrist().setImpbVis30062014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis31122011() {
        return dCrist.getP61ImpbVis31122011().getP61ImpbVis31122011();
    }

    @Override
    public void setImpbVis31122011(AfDecimal impbVis31122011) {
        this.dCrist.getP61ImpbVis31122011().setP61ImpbVis31122011(impbVis31122011.copy());
    }

    @Override
    public AfDecimal getImpbVis31122011Obj() {
        if (ws.getIndDCrist().getImpbVis31122011() >= 0) {
            return getImpbVis31122011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis31122011Obj(AfDecimal impbVis31122011Obj) {
        if (impbVis31122011Obj != null) {
            setImpbVis31122011(new AfDecimal(impbVis31122011Obj, 15, 3));
            ws.getIndDCrist().setImpbVis31122011(((short)0));
        }
        else {
            ws.getIndDCrist().setImpbVis31122011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVisRpP2011() {
        return dCrist.getP61ImpbVisRpP2011().getP61ImpbVisRpP2011();
    }

    @Override
    public void setImpbVisRpP2011(AfDecimal impbVisRpP2011) {
        this.dCrist.getP61ImpbVisRpP2011().setP61ImpbVisRpP2011(impbVisRpP2011.copy());
    }

    @Override
    public AfDecimal getImpbVisRpP2011Obj() {
        if (ws.getIndDCrist().getImpbVisRpP2011() >= 0) {
            return getImpbVisRpP2011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVisRpP2011Obj(AfDecimal impbVisRpP2011Obj) {
        if (impbVisRpP2011Obj != null) {
            setImpbVisRpP2011(new AfDecimal(impbVisRpP2011Obj, 15, 3));
            ws.getIndDCrist().setImpbVisRpP2011(((short)0));
        }
        else {
            ws.getIndDCrist().setImpbVisRpP2011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVisRpP62014() {
        return dCrist.getP61ImpbVisRpP62014().getP61ImpbVisRpP62014();
    }

    @Override
    public void setImpbVisRpP62014(AfDecimal impbVisRpP62014) {
        this.dCrist.getP61ImpbVisRpP62014().setP61ImpbVisRpP62014(impbVisRpP62014.copy());
    }

    @Override
    public AfDecimal getImpbVisRpP62014Obj() {
        if (ws.getIndDCrist().getImpbVisRpP62014() >= 0) {
            return getImpbVisRpP62014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVisRpP62014Obj(AfDecimal impbVisRpP62014Obj) {
        if (impbVisRpP62014Obj != null) {
            setImpbVisRpP62014(new AfDecimal(impbVisRpP62014Obj, 15, 3));
            ws.getIndDCrist().setImpbVisRpP62014(((short)0));
        }
        else {
            ws.getIndDCrist().setImpbVisRpP62014(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontLrdDal2007() {
        return dCrist.getP61MontLrdDal2007().getP61MontLrdDal2007();
    }

    @Override
    public void setMontLrdDal2007(AfDecimal montLrdDal2007) {
        this.dCrist.getP61MontLrdDal2007().setP61MontLrdDal2007(montLrdDal2007.copy());
    }

    @Override
    public AfDecimal getMontLrdDal2007Obj() {
        if (ws.getIndDCrist().getMontLrdDal2007() >= 0) {
            return getMontLrdDal2007();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontLrdDal2007Obj(AfDecimal montLrdDal2007Obj) {
        if (montLrdDal2007Obj != null) {
            setMontLrdDal2007(new AfDecimal(montLrdDal2007Obj, 15, 3));
            ws.getIndDCrist().setMontLrdDal2007(((short)0));
        }
        else {
            ws.getIndDCrist().setMontLrdDal2007(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontLrdEnd2000() {
        return dCrist.getP61MontLrdEnd2000().getP61MontLrdEnd2000();
    }

    @Override
    public void setMontLrdEnd2000(AfDecimal montLrdEnd2000) {
        this.dCrist.getP61MontLrdEnd2000().setP61MontLrdEnd2000(montLrdEnd2000.copy());
    }

    @Override
    public AfDecimal getMontLrdEnd2000Obj() {
        if (ws.getIndDCrist().getMontLrdEnd2000() >= 0) {
            return getMontLrdEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontLrdEnd2000Obj(AfDecimal montLrdEnd2000Obj) {
        if (montLrdEnd2000Obj != null) {
            setMontLrdEnd2000(new AfDecimal(montLrdEnd2000Obj, 15, 3));
            ws.getIndDCrist().setMontLrdEnd2000(((short)0));
        }
        else {
            ws.getIndDCrist().setMontLrdEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontLrdEnd2006() {
        return dCrist.getP61MontLrdEnd2006().getP61MontLrdEnd2006();
    }

    @Override
    public void setMontLrdEnd2006(AfDecimal montLrdEnd2006) {
        this.dCrist.getP61MontLrdEnd2006().setP61MontLrdEnd2006(montLrdEnd2006.copy());
    }

    @Override
    public AfDecimal getMontLrdEnd2006Obj() {
        if (ws.getIndDCrist().getMontLrdEnd2006() >= 0) {
            return getMontLrdEnd2006();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontLrdEnd2006Obj(AfDecimal montLrdEnd2006Obj) {
        if (montLrdEnd2006Obj != null) {
            setMontLrdEnd2006(new AfDecimal(montLrdEnd2006Obj, 15, 3));
            ws.getIndDCrist().setMontLrdEnd2006(((short)0));
        }
        else {
            ws.getIndDCrist().setMontLrdEnd2006(((short)-1));
        }
    }

    @Override
    public long getP61DsRiga() {
        return dCrist.getP61DsRiga();
    }

    @Override
    public void setP61DsRiga(long p61DsRiga) {
        this.dCrist.setP61DsRiga(p61DsRiga);
    }

    @Override
    public AfDecimal getPreLrdDal2007() {
        return dCrist.getP61PreLrdDal2007().getP61PreLrdDal2007();
    }

    @Override
    public void setPreLrdDal2007(AfDecimal preLrdDal2007) {
        this.dCrist.getP61PreLrdDal2007().setP61PreLrdDal2007(preLrdDal2007.copy());
    }

    @Override
    public AfDecimal getPreLrdDal2007Obj() {
        if (ws.getIndDCrist().getPreLrdDal2007() >= 0) {
            return getPreLrdDal2007();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreLrdDal2007Obj(AfDecimal preLrdDal2007Obj) {
        if (preLrdDal2007Obj != null) {
            setPreLrdDal2007(new AfDecimal(preLrdDal2007Obj, 15, 3));
            ws.getIndDCrist().setPreLrdDal2007(((short)0));
        }
        else {
            ws.getIndDCrist().setPreLrdDal2007(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreLrdEnd2000() {
        return dCrist.getP61PreLrdEnd2000().getP61PreLrdEnd2000();
    }

    @Override
    public void setPreLrdEnd2000(AfDecimal preLrdEnd2000) {
        this.dCrist.getP61PreLrdEnd2000().setP61PreLrdEnd2000(preLrdEnd2000.copy());
    }

    @Override
    public AfDecimal getPreLrdEnd2000Obj() {
        if (ws.getIndDCrist().getPreLrdEnd2000() >= 0) {
            return getPreLrdEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreLrdEnd2000Obj(AfDecimal preLrdEnd2000Obj) {
        if (preLrdEnd2000Obj != null) {
            setPreLrdEnd2000(new AfDecimal(preLrdEnd2000Obj, 15, 3));
            ws.getIndDCrist().setPreLrdEnd2000(((short)0));
        }
        else {
            ws.getIndDCrist().setPreLrdEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreLrdEnd2006() {
        return dCrist.getP61PreLrdEnd2006().getP61PreLrdEnd2006();
    }

    @Override
    public void setPreLrdEnd2006(AfDecimal preLrdEnd2006) {
        this.dCrist.getP61PreLrdEnd2006().setP61PreLrdEnd2006(preLrdEnd2006.copy());
    }

    @Override
    public AfDecimal getPreLrdEnd2006Obj() {
        if (ws.getIndDCrist().getPreLrdEnd2006() >= 0) {
            return getPreLrdEnd2006();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreLrdEnd2006Obj(AfDecimal preLrdEnd2006Obj) {
        if (preLrdEnd2006Obj != null) {
            setPreLrdEnd2006(new AfDecimal(preLrdEnd2006Obj, 15, 3));
            ws.getIndDCrist().setPreLrdEnd2006(((short)0));
        }
        else {
            ws.getIndDCrist().setPreLrdEnd2006(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreRshV30062014() {
        return dCrist.getP61PreRshV30062014().getP61PreRshV30062014();
    }

    @Override
    public void setPreRshV30062014(AfDecimal preRshV30062014) {
        this.dCrist.getP61PreRshV30062014().setP61PreRshV30062014(preRshV30062014.copy());
    }

    @Override
    public AfDecimal getPreRshV30062014Obj() {
        if (ws.getIndDCrist().getPreRshV30062014() >= 0) {
            return getPreRshV30062014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreRshV30062014Obj(AfDecimal preRshV30062014Obj) {
        if (preRshV30062014Obj != null) {
            setPreRshV30062014(new AfDecimal(preRshV30062014Obj, 15, 3));
            ws.getIndDCrist().setPreRshV30062014(((short)0));
        }
        else {
            ws.getIndDCrist().setPreRshV30062014(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreRshV31122011() {
        return dCrist.getP61PreRshV31122011().getP61PreRshV31122011();
    }

    @Override
    public void setPreRshV31122011(AfDecimal preRshV31122011) {
        this.dCrist.getP61PreRshV31122011().setP61PreRshV31122011(preRshV31122011.copy());
    }

    @Override
    public AfDecimal getPreRshV31122011Obj() {
        if (ws.getIndDCrist().getPreRshV31122011() >= 0) {
            return getPreRshV31122011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreRshV31122011Obj(AfDecimal preRshV31122011Obj) {
        if (preRshV31122011Obj != null) {
            setPreRshV31122011(new AfDecimal(preRshV31122011Obj, 15, 3));
            ws.getIndDCrist().setPreRshV31122011(((short)0));
        }
        else {
            ws.getIndDCrist().setPreRshV31122011(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreV30062014() {
        return dCrist.getP61PreV30062014().getP61PreV30062014();
    }

    @Override
    public void setPreV30062014(AfDecimal preV30062014) {
        this.dCrist.getP61PreV30062014().setP61PreV30062014(preV30062014.copy());
    }

    @Override
    public AfDecimal getPreV30062014Obj() {
        if (ws.getIndDCrist().getPreV30062014() >= 0) {
            return getPreV30062014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreV30062014Obj(AfDecimal preV30062014Obj) {
        if (preV30062014Obj != null) {
            setPreV30062014(new AfDecimal(preV30062014Obj, 15, 3));
            ws.getIndDCrist().setPreV30062014(((short)0));
        }
        else {
            ws.getIndDCrist().setPreV30062014(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreV31122011() {
        return dCrist.getP61PreV31122011().getP61PreV31122011();
    }

    @Override
    public void setPreV31122011(AfDecimal preV31122011) {
        this.dCrist.getP61PreV31122011().setP61PreV31122011(preV31122011.copy());
    }

    @Override
    public AfDecimal getPreV31122011Obj() {
        if (ws.getIndDCrist().getPreV31122011() >= 0) {
            return getPreV31122011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreV31122011Obj(AfDecimal preV31122011Obj) {
        if (preV31122011Obj != null) {
            setPreV31122011(new AfDecimal(preV31122011Obj, 15, 3));
            ws.getIndDCrist().setPreV31122011(((short)0));
        }
        else {
            ws.getIndDCrist().setPreV31122011(((short)-1));
        }
    }

    @Override
    public AfDecimal getRendtoLrdDal2007() {
        return dCrist.getP61RendtoLrdDal2007().getP61RendtoLrdDal2007();
    }

    @Override
    public void setRendtoLrdDal2007(AfDecimal rendtoLrdDal2007) {
        this.dCrist.getP61RendtoLrdDal2007().setP61RendtoLrdDal2007(rendtoLrdDal2007.copy());
    }

    @Override
    public AfDecimal getRendtoLrdDal2007Obj() {
        if (ws.getIndDCrist().getRendtoLrdDal2007() >= 0) {
            return getRendtoLrdDal2007();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRendtoLrdDal2007Obj(AfDecimal rendtoLrdDal2007Obj) {
        if (rendtoLrdDal2007Obj != null) {
            setRendtoLrdDal2007(new AfDecimal(rendtoLrdDal2007Obj, 15, 3));
            ws.getIndDCrist().setRendtoLrdDal2007(((short)0));
        }
        else {
            ws.getIndDCrist().setRendtoLrdDal2007(((short)-1));
        }
    }

    @Override
    public AfDecimal getRendtoLrdEnd2000() {
        return dCrist.getP61RendtoLrdEnd2000().getP61RendtoLrdEnd2000();
    }

    @Override
    public void setRendtoLrdEnd2000(AfDecimal rendtoLrdEnd2000) {
        this.dCrist.getP61RendtoLrdEnd2000().setP61RendtoLrdEnd2000(rendtoLrdEnd2000.copy());
    }

    @Override
    public AfDecimal getRendtoLrdEnd2000Obj() {
        if (ws.getIndDCrist().getRendtoLrdEnd2000() >= 0) {
            return getRendtoLrdEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRendtoLrdEnd2000Obj(AfDecimal rendtoLrdEnd2000Obj) {
        if (rendtoLrdEnd2000Obj != null) {
            setRendtoLrdEnd2000(new AfDecimal(rendtoLrdEnd2000Obj, 15, 3));
            ws.getIndDCrist().setRendtoLrdEnd2000(((short)0));
        }
        else {
            ws.getIndDCrist().setRendtoLrdEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getRendtoLrdEnd2006() {
        return dCrist.getP61RendtoLrdEnd2006().getP61RendtoLrdEnd2006();
    }

    @Override
    public void setRendtoLrdEnd2006(AfDecimal rendtoLrdEnd2006) {
        this.dCrist.getP61RendtoLrdEnd2006().setP61RendtoLrdEnd2006(rendtoLrdEnd2006.copy());
    }

    @Override
    public AfDecimal getRendtoLrdEnd2006Obj() {
        if (ws.getIndDCrist().getRendtoLrdEnd2006() >= 0) {
            return getRendtoLrdEnd2006();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRendtoLrdEnd2006Obj(AfDecimal rendtoLrdEnd2006Obj) {
        if (rendtoLrdEnd2006Obj != null) {
            setRendtoLrdEnd2006(new AfDecimal(rendtoLrdEnd2006Obj, 15, 3));
            ws.getIndDCrist().setRendtoLrdEnd2006(((short)0));
        }
        else {
            ws.getIndDCrist().setRendtoLrdEnd2006(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMat30062014() {
        return dCrist.getP61RisMat30062014().getP61RisMat30062014();
    }

    @Override
    public void setRisMat30062014(AfDecimal risMat30062014) {
        this.dCrist.getP61RisMat30062014().setP61RisMat30062014(risMat30062014.copy());
    }

    @Override
    public AfDecimal getRisMat30062014Obj() {
        if (ws.getIndDCrist().getRisMat30062014() >= 0) {
            return getRisMat30062014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMat30062014Obj(AfDecimal risMat30062014Obj) {
        if (risMat30062014Obj != null) {
            setRisMat30062014(new AfDecimal(risMat30062014Obj, 15, 3));
            ws.getIndDCrist().setRisMat30062014(((short)0));
        }
        else {
            ws.getIndDCrist().setRisMat30062014(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMat31122011() {
        return dCrist.getP61RisMat31122011().getP61RisMat31122011();
    }

    @Override
    public void setRisMat31122011(AfDecimal risMat31122011) {
        this.dCrist.getP61RisMat31122011().setP61RisMat31122011(risMat31122011.copy());
    }

    @Override
    public AfDecimal getRisMat31122011Obj() {
        if (ws.getIndDCrist().getRisMat31122011() >= 0) {
            return getRisMat31122011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMat31122011Obj(AfDecimal risMat31122011Obj) {
        if (risMat31122011Obj != null) {
            setRisMat31122011(new AfDecimal(risMat31122011Obj, 15, 3));
            ws.getIndDCrist().setRisMat31122011(((short)0));
        }
        else {
            ws.getIndDCrist().setRisMat31122011(((short)-1));
        }
    }
}
