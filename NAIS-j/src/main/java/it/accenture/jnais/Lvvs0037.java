package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0037;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0037Data;

/**Original name: LVVS0037<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0037 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0037Data ws = new Lvvs0037Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0037_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0037 getInstance() {
        return ((Lvvs0037)Programs.getInstance(Lvvs0037.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE LDBV1701
        //                      AREA-IO-POL
        //                      AREA-IO-ADE
        //                      AREA-IO-GAR
        //                      AREA-IO-TGA.
        initLdbv1701();
        initAreaIoPol();
        initAreaIoAde();
        initAreaIoGar();
        initAreaIoTga();
        // COB_CODE: SET COMUN-TROV-NO              TO TRUE.
        ws.getFlagComunTrov().setNo();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-CONTROLLO-DATI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF DPOL-ELE-POL-MAX GREATER ZERO
            //              MOVE 'PO'                    TO LDBV1701-TP-OGG
            //           END-IF
            if (ws.getDpolElePolMax() > 0) {
                // COB_CODE: MOVE IVVC0213-ID-POLIZZA     TO LDBV1701-ID-OGG
                ws.getLdbv1701().setIdOgg(ivvc0213.getIdPolizza());
                // COB_CODE: MOVE 'PO'                    TO LDBV1701-TP-OGG
                ws.getLdbv1701().setTpOgg("PO");
            }
            // COB_CODE: IF DADE-ELE-ADES-MAX GREATER ZERO
            //              MOVE 'AD'                    TO LDBV1701-TP-OGG
            //           END-IF
            if (ws.getDadeEleAdesMax() > 0) {
                // COB_CODE: MOVE IVVC0213-ID-ADESIONE    TO LDBV1701-ID-OGG
                ws.getLdbv1701().setIdOgg(ivvc0213.getIdAdesione());
                // COB_CODE: MOVE 'AD'                    TO LDBV1701-TP-OGG
                ws.getLdbv1701().setTpOgg("AD");
            }
            // COB_CODE: IF DGRZ-ELE-GAR-MAX GREATER ZERO
            //              MOVE 'GA'                    TO LDBV1701-TP-OGG
            //           END-IF
            if (ws.getDgrzEleGarMax() > 0) {
                // COB_CODE: MOVE IVVC0213-ID-GARANZIA    TO LDBV1701-ID-OGG
                ws.getLdbv1701().setIdOgg(ivvc0213.getIdGaranzia());
                // COB_CODE: MOVE 'GA'                    TO LDBV1701-TP-OGG
                ws.getLdbv1701().setTpOgg("GA");
            }
            // COB_CODE: IF DTGA-ELE-TGA-MAX GREATER ZERO
            //              MOVE 'TG'                    TO LDBV1701-TP-OGG
            //           END-IF
            if (ws.getDtgaEleTgaMax() > 0) {
                // COB_CODE: MOVE IVVC0213-ID-TRANCHE     TO LDBV1701-ID-OGG
                ws.getLdbv1701().setIdOgg(ivvc0213.getIdTranche());
                // COB_CODE: MOVE 'TG'                    TO LDBV1701-TP-OGG
                ws.getLdbv1701().setTpOgg("TG");
            }
            // COB_CODE: IF (LDBV1701-TP-OGG = 'PO' AND
            //                                 IVVC0213-ID-POL-AUTENTICO) OR
            //              (LDBV1701-TP-OGG = 'AD' AND
            //                                 IVVC0213-ID-ADE-AUTENTICO) OR
            //              (LDBV1701-TP-OGG = 'GA' AND
            //                                 IVVC0213-ID-GRZ-AUTENTICO) OR
            //              (LDBV1701-TP-OGG = 'TG' AND
            //                                 IVVC0213-ID-TGA-AUTENTICO)
            //              END-IF
            //           ELSE
            //              SET IVVC0213-SALTA-SI          TO TRUE
            //           END-IF
            if (Conditions.eq(ws.getLdbv1701().getTpOgg(), "PO") && ivvc0213.getModalitaIdPol().isIvvc0213IdPolAutentico() || Conditions.eq(ws.getLdbv1701().getTpOgg(), "AD") && ivvc0213.getModalitaIdAde().isIvvc0213IdAdeAutentico() || Conditions.eq(ws.getLdbv1701().getTpOgg(), "GA") && ivvc0213.getModalitaIdGrz().isIvvc0213IdGrzAutentico() || Conditions.eq(ws.getLdbv1701().getTpOgg(), "TG") && ivvc0213.getModalitaIdTga().isIvvc0213IdTgaAutentico()) {
                // COB_CODE: PERFORM VERIFICA-MOVIMENTO
                //              THRU VERIFICA-MOVIMENTO-EX
                verificaMovimento();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
                //              PERFORM S1250-CALCOLA-DATA1    THRU S1250-EX
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: PERFORM S1250-CALCOLA-DATA1    THRU S1250-EX
                    s1250CalcolaData1();
                }
            }
            else {
                // COB_CODE: SET IVVC0213-SALTA-SI          TO TRUE
                ivvc0213.getSaltaValorizzazione().setIvvc0213SaltaSi();
            }
        }
    }

    /**Original name: VERIFICA-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA MOVIMENTO
	 * ----------------------------------------------------------------*
	 * --> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
	 * --> IMMAGINI IN FASE DI COMUNICAZIONE</pre>*/
    private void verificaMovimento() {
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG
        //             TO WS-MOVIMENTO
        //                WS-MOVIMENTO-ORIG
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
        ws.setWsMovimentoOrigFormatted(ivvc0213.getTipoMoviOrigFormatted());
        // COB_CODE:      IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
        //                   LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
        //                   LIQUI-RPP-TAKE-PROFIT
        //                OR LIQUI-RISTOT-INCAPIENZA
        //                OR LIQUI-RPP-REDDITO-PROGR
        //                OR LIQUI-RPP-BENEFICIO-CONTR
        //                OR LIQUI-RISTOT-INCAP
        //           *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
        //                   END-IF
        //                END-IF.
        if (ws.getWsMovimento().isLiquiRistotInd() || ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiScapol() || ws.getWsMovimento().isLiquiSinind() || ws.getWsMovimento().isLiquiRecind() || ws.getWsMovimento().isLiquiRppTakeProfit() || ws.getWsMovimento().isLiquiRistotIncapienza() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRppBeneficioContr() || ws.getWsMovimento().isLiquiRistotIncap()) {
            //--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
            // COB_CODE: PERFORM RECUP-MOVI-COMUN
            //              THRU RECUP-MOVI-COMUN-EX
            recupMoviComun();
            // COB_CODE: IF COMUN-TROV-SI
            //                   IDSV0003-DATA-FINE-EFFETTO
            //           END-IF
            if (ws.getFlagComunTrov().isSi()) {
                // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
                //             TO WK-DATA-CPTZ-RIP
                ws.getWkVarMoviComun().setDataCptzRip(idsv0003.getDataCompetenza());
                // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                //             TO WK-DATA-EFF-RIP
                ws.getWkVarMoviComun().setDataEffRip(idsv0003.getDataInizioEffetto());
                // COB_CODE: MOVE WK-DATA-CPTZ-PREC
                //             TO IDSV0003-DATA-COMPETENZA
                idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getDataCptzPrec());
                // COB_CODE: MOVE WK-DATA-EFF-PREC
                //             TO IDSV0003-DATA-INIZIO-EFFETTO
                //                IDSV0003-DATA-FINE-EFFETTO
                idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getDataEffPrec());
                idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getDataEffPrec());
            }
        }
    }

    /**Original name: RECUP-MOVI-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Recupero il movimento di comunicazione
	 * ----------------------------------------------------------------*</pre>*/
    private void recupMoviComun() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET INIT-CUR-MOV               TO TRUE
        ws.getFlagCurMov().setInitCurMov();
        //    SET COMUN-TROV-NO              TO TRUE
        // COB_CODE: INITIALIZE MOVI
        initMovi();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET  NO-ULTIMA-LETTURA         TO TRUE
        ws.getFlagUltimaLettura().setNoUltimaLettura();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-CUR-MOV
        //                      OR COMUN-TROV-SI
        //                 END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagCurMov().isFineCurMov() || ws.getFlagComunTrov().isSi())) {
            // COB_CODE: INITIALIZE MOVI
            initMovi();
            //
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                   TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR   TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
            //-->    LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //-->    INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL  LDBS6040   USING IDSV0003 MOVI
            //             ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                ldbs6040 = Ldbs6040.getInstance();
                ldbs6040.run(idsv0003, ws.getMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:           IF IDSV0003-SUCCESSFUL-RC
            //                         END-EVALUATE
            //                      ELSE
            //           *--> GESTIRE ERRORE
            //                         SET IDSV0003-INVALID-OPER   TO TRUE
            //                      END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:               EVALUATE TRUE
                //                             WHEN IDSV0003-NOT-FOUND
                //           *-->                NESSUN DATO IN TABELLA
                //           *-->                LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                //           *-->                BOLLO IN INPUT
                //                               SET FINE-CUR-MOV TO TRUE
                //                             WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->                OPERAZIONE ESEGUITA CORRETTAMENTE
                //           *                   MOVE IDSV0003-BUFFER-DATI TO MOVI
                //           *                   TROVO IL MOVIMENTO DI
                //           *                   COMUNICAZIONE RISCATTO PARZIALE
                //                               END-EVALUATE
                //                             WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                                  SET IDSV0003-INVALID-OPER   TO TRUE
                //                         END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->                NESSUN DATO IN TABELLA
                        //-->                LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                        //-->                BOLLO IN INPUT
                        // COB_CODE: SET FINE-CUR-MOV TO TRUE
                        ws.getFlagCurMov().setFineCurMov();
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->                OPERAZIONE ESEGUITA CORRETTAMENTE
                        //                   MOVE IDSV0003-BUFFER-DATI TO MOVI
                        //                   TROVO IL MOVIMENTO DI
                        //                   COMUNICAZIONE RISCATTO PARZIALE
                        // COB_CODE: MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                        ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                        // COB_CODE: EVALUATE TRUE
                        //              WHEN COMUN-RISPAR-IND
                        //              WHEN RISTO-INDIVI
                        //              WHEN VARIA-OPZION
                        //              WHEN SINIS-INDIVI
                        //              WHEN RECES-INDIVI
                        //              WHEN RPP-TAKE-PROFIT
                        //              WHEN COMUN-RISTOT-INCAPIENZA
                        //              WHEN RPP-REDDITO-PROGRAMMATO
                        //              WHEN RPP-BENEFICIO-CONTR
                        //              WHEN COMUN-RISTOT-INCAP
                        //                  SET FINE-CUR-MOV   TO TRUE
                        //              WHEN OTHER
                        //                  SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-EVALUATE
                        switch (ws.getWsMovimento().getWsMovimentoFormatted()) {

                            case WsMovimentoLvvs0037.COMUN_RISPAR_IND:
                            case WsMovimentoLvvs0037.RISTO_INDIVI:
                            case WsMovimentoLvvs0037.VARIA_OPZION:
                            case WsMovimentoLvvs0037.SINIS_INDIVI:
                            case WsMovimentoLvvs0037.RECES_INDIVI:
                            case WsMovimentoLvvs0037.RPP_TAKE_PROFIT:
                            case WsMovimentoLvvs0037.COMUN_RISTOT_INCAPIENZA:
                            case WsMovimentoLvvs0037.RPP_REDDITO_PROGRAMMATO:
                            case WsMovimentoLvvs0037.RPP_BENEFICIO_CONTR:
                            case WsMovimentoLvvs0037.COMUN_RISTOT_INCAP:// COB_CODE: MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                                ws.getWkVarMoviComun().setIdMoviComun(ws.getMovi().getMovIdMovi());
                                // COB_CODE: SET SI-ULTIMA-LETTURA  TO TRUE
                                ws.getFlagUltimaLettura().setSiUltimaLettura();
                                // COB_CODE: MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                                ws.getWkVarMoviComun().setDataEffPrec(ws.getMovi().getMovDtEff());
                                // COB_CODE: COMPUTE WK-DATA-CPTZ-PREC =
                                //                   MOV-DS-TS-CPTZ - 1
                                ws.getWkVarMoviComun().setDataCptzPrec(Trunc.toLong(ws.getMovi().getMovDsTsCptz() - 1, 18));
                                // COB_CODE: SET COMUN-TROV-SI   TO TRUE
                                ws.getFlagComunTrov().setSi();
                                // COB_CODE: PERFORM CLOSE-MOVI
                                //               THRU CLOSE-MOVI-EX
                                closeMovi();
                                // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                                ws.getFlagCurMov().setFineCurMov();
                                break;

                            default:// COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                                idsv0003.getOperazione().setFetchNext();
                                break;
                        }
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE LDBS6040
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                        // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                        // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE LDBS6040
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
        // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO         TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
    }

    /**Original name: CLOSE-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
	 * ----------------------------------------------------------------*</pre>*/
    private void closeMovi() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR            TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: CALL LDBS6040 USING IDSV0003 MOVI
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            ldbs6040 = Ldbs6040.getInstance();
            ldbs6040.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS6040
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
            // COB_CODE: MOVE 'ERRORE CALL LDBS6040 CLOSE'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS6040 CLOSE");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                TO TRUE
        //           END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CLOSE CURSORE LDBS6040'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CLOSE CURSORE LDBS6040");
            // COB_CODE: SET IDSV0003-INVALID-OPER
            //            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POL
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POL
            ws.setDpolAreaPolFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
        //
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-ADES
        //                TO DADE-AREA-ADES
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasAdes())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DADE-AREA-ADES
            ws.setDadeAreaAdesFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
        //
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-GARANZIA
        //                TO DGRZ-AREA-GAR
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasGaranzia())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DGRZ-AREA-GAR
            ws.setDgrzAreaGarFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
        //
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-TRCH-GAR
        //                TO DTGA-AREA-TGA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTrchGar())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DTGA-AREA-TGA
            ws.setDtgaAreaTgaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1250-CALCOLA-DATA1<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaData1() {
        Ldbs1700 ldbs1700 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-WHERE-CONDITION        TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-SELECT                 TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL         TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC          TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        //
        // COB_CODE: MOVE 'LDBS1700'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS1700");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV1701
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs1700 = Ldbs1700.getInstance();
            ldbs1700.run(idsv0003, ws.getLdbv1701());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS1700 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS1700 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE LDBV1701-TP-CAUS        TO IVVC0213-VAL-STR-O
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LDBV1701-TP-CAUS        TO IVVC0213-VAL-STR-O
            ivvc0213.getTabOutput().setValStrO(ws.getLdbv1701().getTpCaus());
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS1700 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS1700 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*
	 *     MOVE DPOL-TP-FRM-ASSVA            TO WS-TP-FRM-ASSVA</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF DTGA-ELE-TGA-MAX GREATER ZERO
        //              END-IF
        //           END-IF.
        if (ws.getDtgaEleTgaMax() > 0) {
            // COB_CODE: IF IVVC0213-ID-TRANCHE IS NUMERIC
            //              END-IF
            //           ELSE
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (Functions.isNumber(ivvc0213.getIdTrancheFormatted())) {
                // COB_CODE: IF IVVC0213-ID-TRANCHE = 0
                //               TO IDSV0003-DESCRIZ-ERR-DB2
                //           END-IF
                if (ivvc0213.getIdTranche() == 0) {
                    // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                    //             TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
                }
            }
            else {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-TRCH NON VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-TRCH NON VALORIZZATO");
            }
        }
        // COB_CODE: IF DPOL-ELE-POL-MAX GREATER ZEROES
        //              END-IF
        //           END-IF.
        if (ws.getDpolElePolMax() > 0) {
            // COB_CODE: IF IVVC0213-ID-POLIZZA IS NUMERIC
            //              END-IF
            //           ELSE
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (Functions.isNumber(ivvc0213.getIdPolizzaFormatted())) {
                // COB_CODE: IF IVVC0213-ID-POLIZZA = 0
                //               TO IDSV0003-DESCRIZ-ERR-DB2
                //           END-IF
                if (ivvc0213.getIdPolizza() == 0) {
                    // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                    //             TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
                }
            }
            else {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
            }
        }
        // COB_CODE: IF DADE-ELE-ADES-MAX GREATER ZEROES
        //              END-IF
        //           END-IF.
        if (ws.getDadeEleAdesMax() > 0) {
            // COB_CODE: IF IVVC0213-ID-ADESIONE IS NUMERIC
            //              END-IF
            //           ELSE
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (Functions.isNumber(ivvc0213.getIdAdesioneFormatted())) {
                // COB_CODE: IF IVVC0213-ID-ADESIONE = 0
                //               TO IDSV0003-DESCRIZ-ERR-DB2
                //           END-IF
                if (ivvc0213.getIdAdesione() == 0) {
                    // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                    //             TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
                }
            }
            else {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
            }
        }
        // COB_CODE: IF DGRZ-ELE-GAR-MAX GREATER ZEROES
        //              END-IF
        //           END-IF.
        if (ws.getDgrzEleGarMax() > 0) {
            // COB_CODE: IF IVVC0213-ID-GARANZIA IS NUMERIC
            //              END-IF
            //           ELSE
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (Functions.isNumber(ivvc0213.getIdGaranziaFormatted())) {
                // COB_CODE: IF IVVC0213-ID-GARANZIA = 0
                //               TO IDSV0003-DESCRIZ-ERR-DB2
                //           END-IF
                if (ivvc0213.getIdGaranzia() == 0) {
                    // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                    //             TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
                }
            }
            else {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: IF COMUN-TROV-SI
        //                   IDSV0003-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getFlagComunTrov().isSi()) {
            // COB_CODE: MOVE WS-MOVIMENTO-ORIG
            //             TO IDSV0003-TIPO-MOVIMENTO
            idsv0003.setTipoMovimentoFormatted(ws.getWsMovimentoOrigFormatted());
            // COB_CODE: MOVE WK-DATA-CPTZ-RIP
            //             TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getDataCptzRip());
            // COB_CODE: MOVE WK-DATA-EFF-RIP
            //             TO IDSV0003-DATA-INIZIO-EFFETTO
            //                IDSV0003-DATA-FINE-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getDataEffRip());
            idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getDataEffRip());
        }
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbv1701() {
        ws.getLdbv1701().setIdOgg(0);
        ws.getLdbv1701().setTpOgg("");
        ws.getLdbv1701().setTpCaus("");
        ws.getLdbv1701().setTpStat("");
    }

    public void initAreaIoPol() {
        ws.setDpolElePolMax(((short)0));
        ws.getLccvpol1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvpol1().setIdPtf(0);
        ws.getLccvpol1().getDati().setWpolIdPoli(0);
        ws.getLccvpol1().getDati().setWpolIdMoviCrz(0);
        ws.getLccvpol1().getDati().getWpolIdMoviChiu().setWpolIdMoviChiu(0);
        ws.getLccvpol1().getDati().setWpolIbOgg("");
        ws.getLccvpol1().getDati().setWpolIbProp("");
        ws.getLccvpol1().getDati().getWpolDtProp().setWpolDtProp(0);
        ws.getLccvpol1().getDati().setWpolDtIniEff(0);
        ws.getLccvpol1().getDati().setWpolDtEndEff(0);
        ws.getLccvpol1().getDati().setWpolCodCompAnia(0);
        ws.getLccvpol1().getDati().setWpolDtDecor(0);
        ws.getLccvpol1().getDati().setWpolDtEmis(0);
        ws.getLccvpol1().getDati().setWpolTpPoli("");
        ws.getLccvpol1().getDati().getWpolDurAa().setWpolDurAa(0);
        ws.getLccvpol1().getDati().getWpolDurMm().setWpolDurMm(0);
        ws.getLccvpol1().getDati().getWpolDtScad().setWpolDtScad(0);
        ws.getLccvpol1().getDati().setWpolCodProd("");
        ws.getLccvpol1().getDati().setWpolDtIniVldtProd(0);
        ws.getLccvpol1().getDati().setWpolCodConv("");
        ws.getLccvpol1().getDati().setWpolCodRamo("");
        ws.getLccvpol1().getDati().getWpolDtIniVldtConv().setWpolDtIniVldtConv(0);
        ws.getLccvpol1().getDati().getWpolDtApplzConv().setWpolDtApplzConv(0);
        ws.getLccvpol1().getDati().setWpolTpFrmAssva("");
        ws.getLccvpol1().getDati().setWpolTpRgmFisc("");
        ws.getLccvpol1().getDati().setWpolFlEstas(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComun(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComunCond(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpLivGenzTit("");
        ws.getLccvpol1().getDati().setWpolFlCopFinanz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpApplzDir("");
        ws.getLccvpol1().getDati().getWpolSpeMed().setWpolSpeMed(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirEmis().setWpolDirEmis(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDir1oVers().setWpolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirVersAgg().setWpolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolCodDvs("");
        ws.getLccvpol1().getDati().setWpolFlFntAz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntAder(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntTfr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntVolo(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpOpzAScad("");
        ws.getLccvpol1().getDati().getWpolAaDiffProrDflt().setWpolAaDiffProrDflt(0);
        ws.getLccvpol1().getDati().setWpolFlVerProd("");
        ws.getLccvpol1().getDati().getWpolDurGg().setWpolDurGg(0);
        ws.getLccvpol1().getDati().getWpolDirQuiet().setWpolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolTpPtfEstno("");
        ws.getLccvpol1().getDati().setWpolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolConvGeco("");
        ws.getLccvpol1().getDati().setWpolDsRiga(0);
        ws.getLccvpol1().getDati().setWpolDsOperSql(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolDsVer(0);
        ws.getLccvpol1().getDati().setWpolDsTsIniCptz(0);
        ws.getLccvpol1().getDati().setWpolDsTsEndCptz(0);
        ws.getLccvpol1().getDati().setWpolDsUtente("");
        ws.getLccvpol1().getDati().setWpolDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlScudoFisc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTrasfe(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTfrStrc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().getWpolDtPresc().setWpolDtPresc(0);
        ws.getLccvpol1().getDati().setWpolCodConvAgg("");
        ws.getLccvpol1().getDati().setWpolSubcatProd("");
        ws.getLccvpol1().getDati().setWpolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolCodTpa("");
        ws.getLccvpol1().getDati().getWpolIdAccComm().setWpolIdAccComm(0);
        ws.getLccvpol1().getDati().setWpolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlPoliBundling(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlVndBundle(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIbBs("");
        ws.getLccvpol1().getDati().setWpolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initAreaIoAde() {
        ws.setDadeEleAdesMax(((short)0));
        ws.getLccvade1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvade1().setIdPtf(0);
        ws.getLccvade1().getDati().setWadeIdAdes(0);
        ws.getLccvade1().getDati().setWadeIdPoli(0);
        ws.getLccvade1().getDati().setWadeIdMoviCrz(0);
        ws.getLccvade1().getDati().getWadeIdMoviChiu().setWadeIdMoviChiu(0);
        ws.getLccvade1().getDati().setWadeDtIniEff(0);
        ws.getLccvade1().getDati().setWadeDtEndEff(0);
        ws.getLccvade1().getDati().setWadeIbPrev("");
        ws.getLccvade1().getDati().setWadeIbOgg("");
        ws.getLccvade1().getDati().setWadeCodCompAnia(0);
        ws.getLccvade1().getDati().getWadeDtDecor().setWadeDtDecor(0);
        ws.getLccvade1().getDati().getWadeDtScad().setWadeDtScad(0);
        ws.getLccvade1().getDati().getWadeEtaAScad().setWadeEtaAScad(0);
        ws.getLccvade1().getDati().getWadeDurAa().setWadeDurAa(0);
        ws.getLccvade1().getDati().getWadeDurMm().setWadeDurMm(0);
        ws.getLccvade1().getDati().getWadeDurGg().setWadeDurGg(0);
        ws.getLccvade1().getDati().setWadeTpRgmFisc("");
        ws.getLccvade1().getDati().setWadeTpRiat("");
        ws.getLccvade1().getDati().setWadeTpModPagTit("");
        ws.getLccvade1().getDati().setWadeTpIas("");
        ws.getLccvade1().getDati().getWadeDtVarzTpIas().setWadeDtVarzTpIas(0);
        ws.getLccvade1().getDati().getWadePreNetInd().setWadePreNetInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePreLrdInd().setWadePreLrdInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeRatLrdInd().setWadeRatLrdInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePrstzIniInd().setWadePrstzIniInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().setWadeFlCoincAssto(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeIbDflt("");
        ws.getLccvade1().getDati().setWadeModCalc("");
        ws.getLccvade1().getDati().setWadeTpFntCnbtva("");
        ws.getLccvade1().getDati().getWadeImpAz().setWadeImpAz(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpAder().setWadeImpAder(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpTfr().setWadeImpTfr(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpVolo().setWadeImpVolo(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePcAz().setWadePcAz(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcAder().setWadePcAder(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcTfr().setWadePcTfr(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcVolo().setWadePcVolo(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadeDtNovaRgmFisc().setWadeDtNovaRgmFisc(0);
        ws.getLccvade1().getDati().setWadeFlAttiv(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeImpRecRitVis().setWadeImpRecRitVis(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpRecRitAcc().setWadeImpRecRitAcc(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().setWadeFlVarzStatTbgc(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeFlProvzaMigraz(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeImpbVisDaRec().setWadeImpbVisDaRec(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeDtDecorPrestBan().setWadeDtDecorPrestBan(0);
        ws.getLccvade1().getDati().getWadeDtEffVarzStatT().setWadeDtEffVarzStatT(0);
        ws.getLccvade1().getDati().setWadeDsRiga(0);
        ws.getLccvade1().getDati().setWadeDsOperSql(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeDsVer(0);
        ws.getLccvade1().getDati().setWadeDsTsIniCptz(0);
        ws.getLccvade1().getDati().setWadeDsTsEndCptz(0);
        ws.getLccvade1().getDati().setWadeDsUtente("");
        ws.getLccvade1().getDati().setWadeDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeCumCnbtCap().setWadeCumCnbtCap(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpGarCnbt().setWadeImpGarCnbt(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeDtUltConsCnbt().setWadeDtUltConsCnbt(0);
        ws.getLccvade1().getDati().setWadeIdenIscFnd("");
        ws.getLccvade1().getDati().getWadeNumRatPian().setWadeNumRatPian(new AfDecimal(0, 12, 5));
        ws.getLccvade1().getDati().getWadeDtPresc().setWadeDtPresc(0);
        ws.getLccvade1().getDati().setWadeConcsPrest(Types.SPACE_CHAR);
    }

    public void initAreaIoGar() {
        ws.setDgrzEleGarMax(((short)0));
        ws.getLccvgrz1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvgrz1().setIdPtf(0);
        ws.getLccvgrz1().getDati().setWgrzIdGar(0);
        ws.getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(0);
        ws.getLccvgrz1().getDati().setWgrzIdPoli(0);
        ws.getLccvgrz1().getDati().setWgrzIdMoviCrz(0);
        ws.getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(0);
        ws.getLccvgrz1().getDati().setWgrzDtIniEff(0);
        ws.getLccvgrz1().getDati().setWgrzDtEndEff(0);
        ws.getLccvgrz1().getDati().setWgrzCodCompAnia(0);
        ws.getLccvgrz1().getDati().setWgrzIbOgg("");
        ws.getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(0);
        ws.getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(0);
        ws.getLccvgrz1().getDati().setWgrzCodSez("");
        ws.getLccvgrz1().getDati().setWgrzCodTari("");
        ws.getLccvgrz1().getDati().setWgrzRamoBila("");
        ws.getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(0);
        ws.getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(0);
        ws.getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(0);
        ws.getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(0);
        ws.getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(((short)0));
        ws.getLccvgrz1().getDati().setWgrzTpRsh("");
        ws.getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(((short)0));
        ws.getLccvgrz1().getDati().setWgrzModPagGarcol("");
        ws.getLccvgrz1().getDati().setWgrzTpPerPre("");
        ws.getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(((short)0));
        ws.getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(((short)0));
        ws.getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(((short)0));
        ws.getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(((short)0));
        ws.getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(((short)0));
        ws.getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(((short)0));
        ws.getLccvgrz1().getDati().setWgrzTpEmisPur(Types.SPACE_CHAR);
        ws.getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(0);
        ws.getLccvgrz1().getDati().setWgrzTpCalcPrePrstz("");
        ws.getLccvgrz1().getDati().setWgrzTpPre(Types.SPACE_CHAR);
        ws.getLccvgrz1().getDati().setWgrzTpDur("");
        ws.getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(0);
        ws.getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(0);
        ws.getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(0);
        ws.getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(0);
        ws.getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(0);
        ws.getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(0);
        ws.getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(0);
        ws.getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(((short)0));
        ws.getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(new AfDecimal(0, 6, 3));
        ws.getLccvgrz1().getDati().setWgrzTpPrstzAssta("");
        ws.getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(0);
        ws.getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(new AfDecimal(0, 6, 3));
        ws.getLccvgrz1().getDati().setWgrzCodFnd("");
        ws.getLccvgrz1().getDati().setWgrzAaRenCer("");
        ws.getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getLccvgrz1().getDati().setWgrzTpPcRip("");
        ws.getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(new AfDecimal(0, 6, 3));
        ws.getLccvgrz1().getDati().setWgrzTpIas("");
        ws.getLccvgrz1().getDati().setWgrzTpStab("");
        ws.getLccvgrz1().getDati().setWgrzTpAdegPre(Types.SPACE_CHAR);
        ws.getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(0);
        ws.getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(0);
        ws.getLccvgrz1().getDati().setWgrzCodTratRiass("");
        ws.getLccvgrz1().getDati().setWgrzTpDtEmisRiass("");
        ws.getLccvgrz1().getDati().setWgrzTpCessRiass("");
        ws.getLccvgrz1().getDati().setWgrzDsRiga(0);
        ws.getLccvgrz1().getDati().setWgrzDsOperSql(Types.SPACE_CHAR);
        ws.getLccvgrz1().getDati().setWgrzDsVer(0);
        ws.getLccvgrz1().getDati().setWgrzDsTsIniCptz(0);
        ws.getLccvgrz1().getDati().setWgrzDsTsEndCptz(0);
        ws.getLccvgrz1().getDati().setWgrzDsUtente("");
        ws.getLccvgrz1().getDati().setWgrzDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(0);
        ws.getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(new AfDecimal(0, 14, 9));
        ws.getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(0);
        ws.getLccvgrz1().getDati().setWgrzRshInvst(Types.SPACE_CHAR);
        ws.getLccvgrz1().getDati().setWgrzTpRamoBila("");
    }

    public void initAreaIoTga() {
        ws.setDtgaEleTgaMax(((short)0));
        ws.getLccvtga1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvtga1().setIdPtf(0);
        ws.getLccvtga1().getDati().setWtgaIdTrchDiGar(0);
        ws.getLccvtga1().getDati().setWtgaIdGar(0);
        ws.getLccvtga1().getDati().setWtgaIdAdes(0);
        ws.getLccvtga1().getDati().setWtgaIdPoli(0);
        ws.getLccvtga1().getDati().setWtgaIdMoviCrz(0);
        ws.getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(0);
        ws.getLccvtga1().getDati().setWtgaDtIniEff(0);
        ws.getLccvtga1().getDati().setWtgaDtEndEff(0);
        ws.getLccvtga1().getDati().setWtgaCodCompAnia(0);
        ws.getLccvtga1().getDati().setWtgaDtDecor(0);
        ws.getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(0);
        ws.getLccvtga1().getDati().setWtgaIbOgg("");
        ws.getLccvtga1().getDati().setWtgaTpRgmFisc("");
        ws.getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(0);
        ws.getLccvtga1().getDati().setWtgaTpTrch("");
        ws.getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(0);
        ws.getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(0);
        ws.getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(0);
        ws.getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(new AfDecimal(0, 6, 3));
        ws.getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(0);
        ws.getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(new AfDecimal(0, 14, 9));
        ws.getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
        ws.getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(new AfDecimal(0, 14, 9));
        ws.getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().setWtgaFlCarCont(Types.SPACE_CHAR);
        ws.getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().setWtgaCodDvs("");
        ws.getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(new AfDecimal(0, 6, 3));
        ws.getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(((short)0));
        ws.getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(((short)0));
        ws.getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(((short)0));
        ws.getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(((short)0));
        ws.getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(((short)0));
        ws.getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(((short)0));
        ws.getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(new AfDecimal(0, 14, 9));
        ws.getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(new AfDecimal(0, 6, 3));
        ws.getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(new AfDecimal(0, 14, 9));
        ws.getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(new AfDecimal(0, 14, 9));
        ws.getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(new AfDecimal(0, 14, 9));
        ws.getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(0);
        ws.getLccvtga1().getDati().setWtgaTpAdegAbb(Types.SPACE_CHAR);
        ws.getLccvtga1().getDati().setWtgaModCalc("");
        ws.getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(0);
        ws.getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(0);
        ws.getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(new AfDecimal(0, 6, 3));
        ws.getLccvtga1().getDati().setWtgaFlImportiForz(Types.SPACE_CHAR);
        ws.getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(new AfDecimal(0, 6, 3));
        ws.getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(new AfDecimal(0, 6, 3));
        ws.getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(new AfDecimal(0, 6, 3));
        ws.getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().setWtgaFlProvForz(Types.SPACE_CHAR);
        ws.getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(0);
        ws.getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(new AfDecimal(0, 14, 9));
        ws.getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().setWtgaTpRival("");
        ws.getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().setWtgaTpManfeeAppl("");
        ws.getLccvtga1().getDati().setWtgaDsRiga(0);
        ws.getLccvtga1().getDati().setWtgaDsOperSql(Types.SPACE_CHAR);
        ws.getLccvtga1().getDati().setWtgaDsVer(0);
        ws.getLccvtga1().getDati().setWtgaDsTsIniCptz(0);
        ws.getLccvtga1().getDati().setWtgaDsTsEndCptz(0);
        ws.getLccvtga1().getDati().setWtgaDsUtente("");
        ws.getLccvtga1().getDati().setWtgaDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(new AfDecimal(0, 6, 3));
        ws.getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(0);
        ws.getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(new AfDecimal(0, 6, 3));
        ws.getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(new AfDecimal(0, 6, 3));
        ws.getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(new AfDecimal(0, 15, 3));
        ws.getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }
}
