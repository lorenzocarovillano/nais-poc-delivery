package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BtcBatchDao;
import it.accenture.jnais.commons.data.to.IBtcBatch;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BtcBatch;
import it.accenture.jnais.ws.enums.FlagOperazione;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.enums.Idsv0003TipologiaOperazione;
import it.accenture.jnais.ws.Iabs0040Data;
import it.accenture.jnais.ws.Iabv0002;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.BtcDtEff;
import it.accenture.jnais.ws.redefines.BtcDtEnd;
import it.accenture.jnais.ws.redefines.BtcDtStart;
import it.accenture.jnais.ws.redefines.BtcIdRich;

/**Original name: IABS0040<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE
 *  F A S E         : GESTIONE TABELLA BTC_BATCH
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0040 extends Program implements IBtcBatch {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BtcBatchDao btcBatchDao = new BtcBatchDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Iabs0040Data ws = new Iabs0040Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: IABV0002
    private Iabv0002 iabv0002;
    //Original name: BTC-BATCH
    private BtcBatch btcBatch;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0040_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Iabv0002 iabv0002, BtcBatch btcBatch) {
        this.idsv0003 = idsv0003;
        this.iabv0002 = iabv0002;
        this.btcBatch = btcBatch;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: PERFORM A300-ELABORA             THRU A300-EX
        a300Elabora();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0040 getInstance() {
        return ((Iabs0040)Programs.getInstance(Iabs0040.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0040'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0040");
        // COB_CODE: MOVE 'BTC_BATCH'             TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BTC_BATCH");
        // COB_CODE: MOVE '00'                    TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                  TO   IDSV0003-SQLCODE
        //                                             IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                             IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
    private void a300Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                 PERFORM A301-ELABORA-PK             THRU A301-EX
        //              WHEN IDSV0003-WHERE-CONDITION
        //                 PERFORM A302-ELABORA-WC             THRU A302-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-LEVEL-OPER     TO TRUE
        //           END-EVALUATE.
        switch (idsv0003.getLivelloOperazione().getLivelloOperazione()) {

            case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A301-ELABORA-PK             THRU A301-EX
                a301ElaboraPk();
                break;

            case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A302-ELABORA-WC             THRU A302-EX
                a302ElaboraWc();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidLevelOper();
                break;
        }
    }

    /**Original name: A301-ELABORA-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a301ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A306-SELECT                 THRU A306-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST            THRU A380-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT             THRU A390-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A320-INSERT                 THRU A320-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A330-UPDATE                 THRU A330-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A306-SELECT                 THRU A306-EX
            a306Select();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST            THRU A380-EX
            a380FetchFirst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT             THRU A390-EX
            a390FetchNext();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A320-INSERT                 THRU A320-EX
            a320Insert();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A330-UPDATE                 THRU A330-EX
            a330Update();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A302-ELABORA-WC<br>
	 * <pre>*****************************************************************</pre>*/
    private void a302ElaboraWc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-WHERE-CONDITION-01
        //                 PERFORM W001-WHERE-CONDITION-01     THRU W001-01-EX
        //              WHEN IDSV0003-WHERE-CONDITION-02
        //                 PERFORM W002-WHERE-CONDITION-02     THRU W002-02-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER           TO TRUE
        //           END-EVALUATE.
        switch (idsv0003.getTipologiaOperazione().getTipologiaOperazione()) {

            case Idsv0003TipologiaOperazione.CONDITION01:// COB_CODE: PERFORM W001-WHERE-CONDITION-01     THRU W001-01-EX
                w001WhereCondition01();
                break;

            case Idsv0003TipologiaOperazione.CONDITION02:// COB_CODE: PERFORM W002-WHERE-CONDITION-02     THRU W002-02-EX
                w002WhereCondition02();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-OPER           TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                break;
        }
    }

    /**Original name: A305-DECLARE-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursor() {
    // COB_CODE: EXEC SQL
    //                DECLARE CUR-BTC CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_BATCH
    //                ,PROTOCOL
    //                ,DT_INS
    //                ,USER_INS
    //                ,COD_BATCH_TYPE
    //                ,COD_COMP_ANIA
    //                ,COD_BATCH_STATE
    //                ,DT_START
    //                ,DT_END
    //                ,USER_START
    //                ,ID_RICH
    //                ,DT_EFF
    //                ,DATA_BATCH
    //              FROM BTC_BATCH
    //              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
    //                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
    //                AND
    //                    (
    //                    COD_BATCH_STATE IN (
    //                                     :IABV0002-STATE-01,
    //                                     :IABV0002-STATE-02,
    //                                     :IABV0002-STATE-03,
    //                                     :IABV0002-STATE-04,
    //                                     :IABV0002-STATE-05,
    //                                     :IABV0002-STATE-06,
    //                                     :IABV0002-STATE-07,
    //                                     :IABV0002-STATE-08,
    //                                     :IABV0002-STATE-09,
    //                                     :IABV0002-STATE-10
    //                                        )
    //                     )
    //             ORDER BY ID_BATCH
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A306-SELECT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a306Select() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        //       WITH UR
        // COB_CODE:      EXEC SQL
        //                   SELECT
        //                     ID_BATCH
        //                     ,PROTOCOL
        //                     ,DT_INS
        //                     ,USER_INS
        //                     ,COD_BATCH_TYPE
        //                     ,COD_COMP_ANIA
        //                     ,COD_BATCH_STATE
        //                     ,DT_START
        //                     ,DT_END
        //                     ,USER_START
        //                     ,ID_RICH
        //                     ,DT_EFF
        //                     ,DATA_BATCH
        //                  INTO
        //                     :BTC-ID-BATCH
        //                    ,:BTC-PROTOCOL
        //                    ,:BTC-DT-INS-DB
        //                    ,:BTC-USER-INS
        //                    ,:BTC-COD-BATCH-TYPE
        //                    ,:BTC-COD-COMP-ANIA
        //                    ,:BTC-COD-BATCH-STATE
        //                    ,:BTC-DT-START-DB
        //                     :IND-BTC-DT-START
        //                    ,:BTC-DT-END-DB
        //                     :IND-BTC-DT-END
        //                    ,:BTC-USER-START
        //                     :IND-BTC-USER-START
        //                    ,:BTC-ID-RICH
        //                     :IND-BTC-ID-RICH
        //                    ,:BTC-DT-EFF-DB
        //                     :IND-BTC-DT-EFF
        //                    ,:BTC-DATA-BATCH-VCHAR
        //                     :IND-BTC-DATA-BATCH
        //                   FROM BTC_BATCH
        //                   WHERE PROTOCOL       = :BTC-PROTOCOL
        //                     AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
        //                     AND
        //                         (
        //                         COD_BATCH_STATE IN (
        //                                          :IABV0002-STATE-01,
        //                                          :IABV0002-STATE-02,
        //                                          :IABV0002-STATE-03,
        //                                          :IABV0002-STATE-04,
        //                                          :IABV0002-STATE-05,
        //                                          :IABV0002-STATE-06,
        //                                          :IABV0002-STATE-07,
        //                                          :IABV0002-STATE-08,
        //                                          :IABV0002-STATE-09,
        //                                          :IABV0002-STATE-10
        //                                             )
        //                          )
        //           *       WITH UR
        //                END-EXEC.
        btcBatchDao.selectRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A320-INSERT<br>*/
    private void a320Insert() {
        // COB_CODE: PERFORM Z400-SEQ                        THRU Z400-EX.
        z400Seq();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX
            z150ValorizzaDataServices();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO BTC_BATCH
            //                  (
            //                    ID_BATCH
            //                   ,PROTOCOL
            //                   ,DT_INS
            //                   ,USER_INS
            //                   ,COD_BATCH_TYPE
            //                   ,COD_COMP_ANIA
            //                   ,COD_BATCH_STATE
            //                   ,DT_START
            //                   ,DT_END
            //                   ,USER_START
            //                   ,ID_RICH
            //                   ,DT_EFF
            //                   ,DATA_BATCH
            //                  )
            //              VALUES
            //                  (
            //                    :BTC-ID-BATCH
            //                   ,:BTC-PROTOCOL
            //                   ,:BTC-DT-INS-DB
            //                   ,:BTC-USER-INS
            //                   ,:BTC-COD-BATCH-TYPE
            //                   ,:BTC-COD-COMP-ANIA
            //                   ,:BTC-COD-BATCH-STATE
            //                   ,:BTC-DT-START-DB
            //                    :IND-BTC-DT-START
            //                   ,:BTC-DT-END-DB
            //                    :IND-BTC-DT-END
            //                   ,:BTC-USER-START
            //                    :IND-BTC-USER-START
            //                   ,:BTC-ID-RICH
            //                    :IND-BTC-ID-RICH
            //                   ,:BTC-DT-EFF-DB
            //                    :IND-BTC-DT-EFF
            //                   ,:BTC-DATA-BATCH-VCHAR
            //                    :IND-BTC-DATA-BATCH
            //                  )
            //           END-EXEC
            btcBatchDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A330-UPDATE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330Update() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_BATCH SET
        //                   COD_BATCH_STATE        = :IABV0002-STATE-CURRENT
        //                  ,DT_START               = :BTC-DT-START-DB
        //                                            :IND-BTC-DT-START
        //                  ,DT_END                 = :BTC-DT-END-DB
        //                                            :IND-BTC-DT-END
        //                  ,USER_START             = :BTC-USER-START
        //                                            :IND-BTC-USER-START
        //                WHERE   ID_BATCH = :BTC-ID-BATCH
        //           END-EXEC.
        btcBatchDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BTC-COD-BATCH-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BTC-COD-BATCH-STATE
            btcBatch.setBtcCodBatchState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A360-OPEN-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursor() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR    THRU A305-EX
        a305DeclareCursor();
        // COB_CODE: EXEC SQL OPEN CUR-BTC      END-EXEC
        btcBatchDao.openCurBtc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-BTC
        //           END-EXEC.
        btcBatchDao.closeCurBtc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirst() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR    THRU A360-EX.
        a360OpenCursor();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT      THRU A390-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT      THRU A390-EX
            a390FetchNext();
        }
    }

    /**Original name: A390-FETCH-NEXT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNext() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BTC
        //           INTO
        //                :BTC-ID-BATCH
        //               ,:BTC-PROTOCOL
        //               ,:BTC-DT-INS-DB
        //               ,:BTC-USER-INS
        //               ,:BTC-COD-BATCH-TYPE
        //               ,:BTC-COD-COMP-ANIA
        //               ,:BTC-COD-BATCH-STATE
        //               ,:BTC-DT-START-DB
        //                :IND-BTC-DT-START
        //               ,:BTC-DT-END-DB
        //                :IND-BTC-DT-END
        //               ,:BTC-USER-START
        //                :IND-BTC-USER-START
        //               ,:BTC-ID-RICH
        //                :IND-BTC-ID-RICH
        //               ,:BTC-DT-EFF-DB
        //                :IND-BTC-DT-EFF
        //               ,:BTC-DATA-BATCH-VCHAR
        //                :IND-BTC-DATA-BATCH
        //           END-EXEC.
        btcBatchDao.fetchCurBtc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: W001-WHERE-CONDITION-01<br>
	 * <pre>*****************************************************************
	 *  WHERE CONDITION 01
	 * *****************************************************************</pre>*/
    private void w001WhereCondition01() {
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND
        //                TO AREA-WHERE-CONDITION
        ws.setAreaWhereConditionFormatted(idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: EVALUATE TRUE
        //              WHEN SELECT-X-TP-BATCH
        //                   PERFORM W100-WH-SELECT-TP-BATCH THRU W100-EX
        //              WHEN SELECT-X-DT-EFFETTO
        //                   PERFORM W200-WH-SELECT-DT-EFFETTO THRU W200-EX
        //              WHEN SELECT-X-DT-EFFETTO-MAX
        //                   PERFORM W300-WH-SELECT-DT-EFFETTO-MAX THRU W300-EX
        //              WHEN SELECT-X-DT-EFFETTO-MIN
        //                   PERFORM W400-WH-SELECT-DT-EFFETTO-MIN THRU W400-EX
        //              WHEN SELECT-X-DT-INSERIM-MAX
        //                   PERFORM W500-WH-SELECT-DT-INSERIM-MAX THRU W500-EX
        //              WHEN SELECT-X-DT-INSERIM-MIN
        //                   PERFORM W600-WH-SELECT-DT-INSERIM-MIN THRU W600-EX
        //              WHEN SELECT-ALL-BATCH
        //                   PERFORM W700-WH-ELABORA-ALL-BATCH     THRU W700-EX
        //              WHEN OTHER
        //                 CONTINUE
        //           END-EVALUATE.
        switch (ws.getFlagOperazione().getFlagOperazione()) {

            case FlagOperazione.X_TP_BATCH:// COB_CODE: PERFORM W100-WH-SELECT-TP-BATCH THRU W100-EX
                w100WhSelectTpBatch();
                break;

            case FlagOperazione.X_DT_EFFETTO:// COB_CODE: PERFORM W200-WH-SELECT-DT-EFFETTO THRU W200-EX
                w200WhSelectDtEffetto();
                break;

            case FlagOperazione.X_DT_EFFETTO_MAX:// COB_CODE: PERFORM W300-WH-SELECT-DT-EFFETTO-MAX THRU W300-EX
                w300WhSelectDtEffettoMax();
                break;

            case FlagOperazione.X_DT_EFFETTO_MIN:// COB_CODE: PERFORM W400-WH-SELECT-DT-EFFETTO-MIN THRU W400-EX
                w400WhSelectDtEffettoMin();
                break;

            case FlagOperazione.X_DT_INSERIM_MAX:// COB_CODE: PERFORM W500-WH-SELECT-DT-INSERIM-MAX THRU W500-EX
                w500WhSelectDtInserimMax();
                break;

            case FlagOperazione.X_DT_INSERIM_MIN:// COB_CODE: PERFORM W600-WH-SELECT-DT-INSERIM-MIN THRU W600-EX
                w600WhSelectDtInserimMin();
                break;

            case FlagOperazione.ALL_BATCH:// COB_CODE: PERFORM W700-WH-ELABORA-ALL-BATCH     THRU W700-EX
                w700WhElaboraAllBatch();
                break;

            default:// COB_CODE: CONTINUE
            //continue
                break;
        }
    }

    /**Original name: W002-WHERE-CONDITION-02<br>
	 * <pre>*****************************************************************
	 *  WHERE CONDITION 02
	 * *****************************************************************</pre>*/
    private void w002WhereCondition02() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_BATCH SET
        //                   COD_BATCH_STATE = :IABV0002-STATE-CURRENT
        //                WHERE   ID_RICH    = :BTC-ID-RICH
        //           END-EXEC.
        btcBatchDao.updateRec1(iabv0002.getIabv0002StateCurrent(), btcBatch.getBtcIdRich().getBtcIdRich());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BTC-COD-BATCH-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BTC-COD-BATCH-STATE
            btcBatch.setBtcCodBatchState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: W100-WH-SELECT-TP-BATCH<br>
	 * <pre>*****************************************************************
	 *  WHERE CONDITION - SELECT X TIPOLOGIA BATCH
	 * *****************************************************************</pre>*/
    private void w100WhSelectTpBatch() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //              SELECT
        //                ID_BATCH
        //                ,PROTOCOL
        //                ,DT_INS
        //                ,USER_INS
        //                ,COD_BATCH_TYPE
        //                ,COD_COMP_ANIA
        //                ,COD_BATCH_STATE
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,ID_RICH
        //                ,DT_EFF
        //                ,DATA_BATCH
        //             INTO
        //                :BTC-ID-BATCH
        //               ,:BTC-PROTOCOL
        //               ,:BTC-DT-INS-DB
        //               ,:BTC-USER-INS
        //               ,:BTC-COD-BATCH-TYPE
        //               ,:BTC-COD-COMP-ANIA
        //               ,:BTC-COD-BATCH-STATE
        //               ,:BTC-DT-START-DB
        //                :IND-BTC-DT-START
        //               ,:BTC-DT-END-DB
        //                :IND-BTC-DT-END
        //               ,:BTC-USER-START
        //                :IND-BTC-USER-START
        //               ,:BTC-ID-RICH
        //                :IND-BTC-ID-RICH
        //               ,:BTC-DT-EFF-DB
        //                :IND-BTC-DT-EFF
        //               ,:BTC-DATA-BATCH-VCHAR
        //                :IND-BTC-DATA-BATCH
        //              FROM BTC_BATCH
        //              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
        //                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
        //                AND COD_BATCH_STATE IN  (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                        )
        //              ORDER BY DT_INS DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        btcBatchDao.selectRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: W200-WH-SELECT-DT-EFFETTO<br>
	 * <pre>*****************************************************************
	 *  WHERE CONDITION - SELECT X TIPOLOGIA BATCH E DATA EFFETTO
	 * *****************************************************************</pre>*/
    private void w200WhSelectDtEffetto() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //              SELECT
        //                ID_BATCH
        //                ,PROTOCOL
        //                ,DT_INS
        //                ,USER_INS
        //                ,COD_BATCH_TYPE
        //                ,COD_COMP_ANIA
        //                ,COD_BATCH_STATE
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,ID_RICH
        //                ,DT_EFF
        //                ,DATA_BATCH
        //             INTO
        //                :BTC-ID-BATCH
        //               ,:BTC-PROTOCOL
        //               ,:BTC-DT-INS-DB
        //               ,:BTC-USER-INS
        //               ,:BTC-COD-BATCH-TYPE
        //               ,:BTC-COD-COMP-ANIA
        //               ,:BTC-COD-BATCH-STATE
        //               ,:BTC-DT-START-DB
        //                :IND-BTC-DT-START
        //               ,:BTC-DT-END-DB
        //                :IND-BTC-DT-END
        //               ,:BTC-USER-START
        //                :IND-BTC-USER-START
        //               ,:BTC-ID-RICH
        //                :IND-BTC-ID-RICH
        //               ,:BTC-DT-EFF-DB
        //                :IND-BTC-DT-EFF
        //               ,:BTC-DATA-BATCH-VCHAR
        //                :IND-BTC-DATA-BATCH
        //              FROM BTC_BATCH
        //              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
        //                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
        //                AND DT_EFF         = :BTC-DT-EFF-DB
        //                AND COD_BATCH_STATE IN  (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                        )
        //              ORDER BY DT_INS DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        btcBatchDao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: W300-WH-SELECT-DT-EFFETTO-MAX<br>
	 * <pre>*****************************************************************
	 *  WHERE CONDITION - SELECT X TIPOLOGIA BATCH E DATA EFFETTO MAX
	 * *****************************************************************</pre>*/
    private void w300WhSelectDtEffettoMax() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: EXEC SQL
        //              SELECT
        //                ID_BATCH
        //                ,PROTOCOL
        //                ,DT_INS
        //                ,USER_INS
        //                ,COD_BATCH_TYPE
        //                ,COD_COMP_ANIA
        //                ,COD_BATCH_STATE
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,ID_RICH
        //                ,DT_EFF
        //                ,DATA_BATCH
        //             INTO
        //                :BTC-ID-BATCH
        //               ,:BTC-PROTOCOL
        //               ,:BTC-DT-INS-DB
        //               ,:BTC-USER-INS
        //               ,:BTC-COD-BATCH-TYPE
        //               ,:BTC-COD-COMP-ANIA
        //               ,:BTC-COD-BATCH-STATE
        //               ,:BTC-DT-START-DB
        //                :IND-BTC-DT-START
        //               ,:BTC-DT-END-DB
        //                :IND-BTC-DT-END
        //               ,:BTC-USER-START
        //                :IND-BTC-USER-START
        //               ,:BTC-ID-RICH
        //                :IND-BTC-ID-RICH
        //               ,:BTC-DT-EFF-DB
        //                :IND-BTC-DT-EFF
        //               ,:BTC-DATA-BATCH-VCHAR
        //                :IND-BTC-DATA-BATCH
        //              FROM BTC_BATCH
        //              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
        //                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
        //                AND COD_BATCH_STATE IN  (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                        )
        //              ORDER BY DT_EFF DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        btcBatchDao.selectRec2(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: W400-WH-SELECT-DT-EFFETTO-MIN<br>
	 * <pre>*****************************************************************
	 *  WHERE CONDITION - SELECT X TIPOLOGIA BATCH E DATA EFFETTO MIN
	 * *****************************************************************</pre>*/
    private void w400WhSelectDtEffettoMin() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: EXEC SQL
        //              SELECT
        //                ID_BATCH
        //                ,PROTOCOL
        //                ,DT_INS
        //                ,USER_INS
        //                ,COD_BATCH_TYPE
        //                ,COD_COMP_ANIA
        //                ,COD_BATCH_STATE
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,ID_RICH
        //                ,DT_EFF
        //                ,DATA_BATCH
        //             INTO
        //                :BTC-ID-BATCH
        //               ,:BTC-PROTOCOL
        //               ,:BTC-DT-INS-DB
        //               ,:BTC-USER-INS
        //               ,:BTC-COD-BATCH-TYPE
        //               ,:BTC-COD-COMP-ANIA
        //               ,:BTC-COD-BATCH-STATE
        //               ,:BTC-DT-START-DB
        //                :IND-BTC-DT-START
        //               ,:BTC-DT-END-DB
        //                :IND-BTC-DT-END
        //               ,:BTC-USER-START
        //                :IND-BTC-USER-START
        //               ,:BTC-ID-RICH
        //                :IND-BTC-ID-RICH
        //               ,:BTC-DT-EFF-DB
        //                :IND-BTC-DT-EFF
        //               ,:BTC-DATA-BATCH-VCHAR
        //                :IND-BTC-DATA-BATCH
        //              FROM BTC_BATCH
        //              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
        //                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
        //                AND COD_BATCH_STATE IN  (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                        )
        //              ORDER BY DT_EFF
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        btcBatchDao.selectRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: W500-WH-SELECT-DT-INSERIM-MAX<br>
	 * <pre>*****************************************************************
	 *  WHERE CONDITION - SELECT X TIPOLOGIA BATCH E DATA INSERIM. MAX
	 * *****************************************************************</pre>*/
    private void w500WhSelectDtInserimMax() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: EXEC SQL
        //              SELECT
        //                ID_BATCH
        //                ,PROTOCOL
        //                ,DT_INS
        //                ,USER_INS
        //                ,COD_BATCH_TYPE
        //                ,COD_COMP_ANIA
        //                ,COD_BATCH_STATE
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,ID_RICH
        //                ,DT_EFF
        //                ,DATA_BATCH
        //             INTO
        //                :BTC-ID-BATCH
        //               ,:BTC-PROTOCOL
        //               ,:BTC-DT-INS-DB
        //               ,:BTC-USER-INS
        //               ,:BTC-COD-BATCH-TYPE
        //               ,:BTC-COD-COMP-ANIA
        //               ,:BTC-COD-BATCH-STATE
        //               ,:BTC-DT-START-DB
        //                :IND-BTC-DT-START
        //               ,:BTC-DT-END-DB
        //                :IND-BTC-DT-END
        //               ,:BTC-USER-START
        //                :IND-BTC-USER-START
        //               ,:BTC-ID-RICH
        //                :IND-BTC-ID-RICH
        //               ,:BTC-DT-EFF-DB
        //                :IND-BTC-DT-EFF
        //               ,:BTC-DATA-BATCH-VCHAR
        //                :IND-BTC-DATA-BATCH
        //              FROM BTC_BATCH
        //              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
        //                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
        //                AND COD_BATCH_STATE IN  (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                        )
        //              ORDER BY DT_INS DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        btcBatchDao.selectRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: W600-WH-SELECT-DT-INSERIM-MIN<br>
	 * <pre>*****************************************************************
	 *  WHERE CONDITION - SELECT X TIPOLOGIA BATCH E DATA INSERIM. MIN
	 * *****************************************************************</pre>*/
    private void w600WhSelectDtInserimMin() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: EXEC SQL
        //              SELECT
        //                ID_BATCH
        //                ,PROTOCOL
        //                ,DT_INS
        //                ,USER_INS
        //                ,COD_BATCH_TYPE
        //                ,COD_COMP_ANIA
        //                ,COD_BATCH_STATE
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,ID_RICH
        //                ,DT_EFF
        //                ,DATA_BATCH
        //             INTO
        //                :BTC-ID-BATCH
        //               ,:BTC-PROTOCOL
        //               ,:BTC-DT-INS-DB
        //               ,:BTC-USER-INS
        //               ,:BTC-COD-BATCH-TYPE
        //               ,:BTC-COD-COMP-ANIA
        //               ,:BTC-COD-BATCH-STATE
        //               ,:BTC-DT-START-DB
        //                :IND-BTC-DT-START
        //               ,:BTC-DT-END-DB
        //                :IND-BTC-DT-END
        //               ,:BTC-USER-START
        //                :IND-BTC-USER-START
        //               ,:BTC-ID-RICH
        //                :IND-BTC-ID-RICH
        //               ,:BTC-DT-EFF-DB
        //                :IND-BTC-DT-EFF
        //               ,:BTC-DATA-BATCH-VCHAR
        //                :IND-BTC-DATA-BATCH
        //              FROM BTC_BATCH
        //              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
        //                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
        //                AND COD_BATCH_STATE IN  (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                        )
        //              ORDER BY DT_INS
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        btcBatchDao.selectRec4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: W700-WH-ELABORA-ALL-BATCH<br>
	 * <pre>*****************************************************************
	 *  WHERE CONDITION - ELABORA X TIPOLOGIA BATCH
	 * *****************************************************************</pre>*/
    private void w700WhElaboraAllBatch() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST            THRU A380-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT             THRU A390-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A320-INSERT                 THRU A320-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A330-UPDATE                 THRU A330-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST            THRU A380-EX
            a380FetchFirst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT             THRU A390-EX
            a390FetchNext();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A320-INSERT                 THRU A320-EX
            a320Insert();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A330-UPDATE                 THRU A330-EX
            a330Update();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-BTC-DT-START = -1
        //              MOVE HIGH-VALUES TO BTC-DT-START-NULL
        //           END-IF
        if (ws.getIndBtcBatch().getDescDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BTC-DT-START-NULL
            btcBatch.getBtcDtStart().setBtcDtStartNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcDtStart.Len.BTC_DT_START_NULL));
        }
        // COB_CODE: IF IND-BTC-DT-END = -1
        //              MOVE HIGH-VALUES TO BTC-DT-END-NULL
        //           END-IF
        if (ws.getIndBtcBatch().getTipoDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BTC-DT-END-NULL
            btcBatch.getBtcDtEnd().setBtcDtEndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcDtEnd.Len.BTC_DT_END_NULL));
        }
        // COB_CODE: IF IND-BTC-USER-START = -1
        //              MOVE HIGH-VALUES TO BTC-USER-START-NULL
        //           END-IF
        if (ws.getIndBtcBatch().getLunghezzaDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BTC-USER-START-NULL
            btcBatch.setBtcUserStart(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatch.Len.BTC_USER_START));
        }
        // COB_CODE: IF IND-BTC-ID-RICH = -1
        //              MOVE HIGH-VALUES TO BTC-ID-RICH-NULL
        //           END-IF
        if (ws.getIndBtcBatch().getPrecisioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BTC-ID-RICH-NULL
            btcBatch.getBtcIdRich().setBtcIdRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcIdRich.Len.BTC_ID_RICH_NULL));
        }
        // COB_CODE: IF IND-BTC-DATA-BATCH = -1
        //              MOVE HIGH-VALUES TO BTC-DATA-BATCH
        //           END-IF.
        if (ws.getIndBtcBatch().getFormattazioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BTC-DATA-BATCH
            btcBatch.setBtcDataBatch(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatch.Len.BTC_DATA_BATCH));
        }
        // COB_CODE: IF IND-BTC-DT-EFF = -1
        //              MOVE HIGH-VALUES TO BTC-DT-EFF-NULL
        //           END-IF.
        if (ws.getIndBtcBatch().getCodDominio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BTC-DT-EFF-NULL
            btcBatch.getBtcDtEff().setBtcDtEffNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcDtEff.Len.BTC_DT_EFF_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES<br>
	 * <pre>*****************************************************************</pre>*/
    private void z150ValorizzaDataServices() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF BTC-DT-START-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BTC-DT-START
        //           ELSE
        //              MOVE 0 TO IND-BTC-DT-START
        //           END-IF
        if (Characters.EQ_HIGH.test(btcBatch.getBtcDtStart().getBtcDtStartNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BTC-DT-START
            ws.getIndBtcBatch().setFlContiguousData(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BTC-DT-START
            ws.getIndBtcBatch().setFlContiguousData(((short)0));
        }
        // COB_CODE: IF BTC-DT-END-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BTC-DT-END
        //           ELSE
        //              MOVE 0 TO IND-BTC-DT-END
        //           END-IF
        if (Characters.EQ_HIGH.test(btcBatch.getBtcDtEnd().getBtcDtEndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BTC-DT-END
            ws.getIndBtcBatch().setTipoDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BTC-DT-END
            ws.getIndBtcBatch().setTipoDato(((short)0));
        }
        // COB_CODE: IF BTC-USER-START-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BTC-USER-START
        //           ELSE
        //              MOVE 0 TO IND-BTC-USER-START
        //           END-IF
        if (Characters.EQ_HIGH.test(btcBatch.getBtcUserStart(), BtcBatch.Len.BTC_USER_START)) {
            // COB_CODE: MOVE -1 TO IND-BTC-USER-START
            ws.getIndBtcBatch().setLunghezzaDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BTC-USER-START
            ws.getIndBtcBatch().setLunghezzaDato(((short)0));
        }
        // COB_CODE: IF BTC-ID-RICH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BTC-ID-RICH
        //           ELSE
        //              MOVE 0 TO IND-BTC-ID-RICH
        //           END-IF
        if (Characters.EQ_HIGH.test(btcBatch.getBtcIdRich().getBtcIdRichNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BTC-ID-RICH
            ws.getIndBtcBatch().setPrecisioneDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BTC-ID-RICH
            ws.getIndBtcBatch().setPrecisioneDato(((short)0));
        }
        // COB_CODE: IF BTC-DATA-BATCH = HIGH-VALUES
        //              MOVE -1 TO IND-BTC-DATA-BATCH
        //           ELSE
        //              MOVE 0 TO IND-BTC-DATA-BATCH
        //           END-IF.
        if (Characters.EQ_HIGH.test(btcBatch.getBtcDataBatch(), BtcBatch.Len.BTC_DATA_BATCH)) {
            // COB_CODE: MOVE -1 TO IND-BTC-DATA-BATCH
            ws.getIndBtcBatch().setFormattazioneDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BTC-DATA-BATCH
            ws.getIndBtcBatch().setFormattazioneDato(((short)0));
        }
        // COB_CODE: IF BTC-DT-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BTC-DT-EFF
        //           ELSE
        //              MOVE 0 TO IND-BTC-DT-EFF
        //           END-IF.
        if (Characters.EQ_HIGH.test(btcBatch.getBtcDtEff().getBtcDtEffNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BTC-DT-EFF
            ws.getIndBtcBatch().setTypeRecord(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BTC-DT-EFF
            ws.getIndBtcBatch().setTypeRecord(((short)0));
        }
    }

    /**Original name: Z400-SEQ<br>*/
    private void z400Seq() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ID_BATCH
        //              INTO :BTC-ID-BATCH
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE BTC-DT-INS TO WS-TIMESTAMP-N
        ws.getIdsv0010().setWsTimestampN(TruncAbs.toLong(btcBatch.getBtcDtIns(), 18));
        // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
        z701TsNToX();
        // COB_CODE: MOVE WS-TIMESTAMP-X   TO BTC-DT-INS-DB
        ws.getBtcBatchDb().setInsDb(ws.getIdsv0010().getWsTimestampX());
        // COB_CODE: IF IND-BTC-DT-START = 0
        //               MOVE WS-TIMESTAMP-X      TO BTC-DT-START-DB
        //           END-IF
        if (ws.getIndBtcBatch().getDescDato() == 0) {
            // COB_CODE: MOVE BTC-DT-START TO WS-TIMESTAMP-N
            ws.getIdsv0010().setWsTimestampN(TruncAbs.toLong(btcBatch.getBtcDtStart().getBtcDtStart(), 18));
            // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
            z701TsNToX();
            // COB_CODE: MOVE WS-TIMESTAMP-X      TO BTC-DT-START-DB
            ws.getBtcBatchDb().setStartDb(ws.getIdsv0010().getWsTimestampX());
        }
        // COB_CODE: IF IND-BTC-DT-END = 0
        //               MOVE WS-TIMESTAMP-X      TO BTC-DT-END-DB
        //           END-IF.
        if (ws.getIndBtcBatch().getTipoDato() == 0) {
            // COB_CODE: MOVE BTC-DT-END TO WS-TIMESTAMP-N
            ws.getIdsv0010().setWsTimestampN(TruncAbs.toLong(btcBatch.getBtcDtEnd().getBtcDtEnd(), 18));
            // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
            z701TsNToX();
            // COB_CODE: MOVE WS-TIMESTAMP-X      TO BTC-DT-END-DB
            ws.getBtcBatchDb().setEndDb(ws.getIdsv0010().getWsTimestampX());
        }
        // COB_CODE: IF IND-BTC-DT-EFF = 0
        //               MOVE WS-DATE-X      TO BTC-DT-EFF-DB
        //           END-IF.
        if (ws.getIndBtcBatch().getCodDominio() == 0) {
            // COB_CODE: MOVE BTC-DT-EFF     TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(btcBatch.getBtcDtEff().getBtcDtEff(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO BTC-DT-EFF-DB
            ws.getBtcBatchDb().setEffDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE BTC-DT-INS-DB TO WS-TIMESTAMP-X
        ws.getIdsv0010().setWsTimestampX(ws.getBtcBatchDb().getInsDb());
        // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
        z801TsXToN();
        // COB_CODE: MOVE WS-TIMESTAMP-N      TO BTC-DT-INS
        btcBatch.setBtcDtIns(ws.getIdsv0010().getWsTimestampN());
        // COB_CODE: IF IND-BTC-DT-START = 0
        //               MOVE WS-TIMESTAMP-N      TO BTC-DT-START
        //           END-IF
        if (ws.getIndBtcBatch().getDescDato() == 0) {
            // COB_CODE: MOVE BTC-DT-START-DB TO WS-TIMESTAMP-X
            ws.getIdsv0010().setWsTimestampX(ws.getBtcBatchDb().getStartDb());
            // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
            z801TsXToN();
            // COB_CODE: MOVE WS-TIMESTAMP-N      TO BTC-DT-START
            btcBatch.getBtcDtStart().setBtcDtStart(ws.getIdsv0010().getWsTimestampN());
        }
        // COB_CODE: IF IND-BTC-DT-END = 0
        //               MOVE WS-TIMESTAMP-N      TO BTC-DT-END
        //           END-IF.
        if (ws.getIndBtcBatch().getTipoDato() == 0) {
            // COB_CODE: MOVE BTC-DT-END-DB TO WS-TIMESTAMP-X
            ws.getIdsv0010().setWsTimestampX(ws.getBtcBatchDb().getEndDb());
            // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
            z801TsXToN();
            // COB_CODE: MOVE WS-TIMESTAMP-N      TO BTC-DT-END
            btcBatch.getBtcDtEnd().setBtcDtEnd(ws.getIdsv0010().getWsTimestampN());
        }
        // COB_CODE: IF IND-BTC-DT-EFF = 0
        //               MOVE WS-DATE-N      TO BTC-DT-EFF
        //           END-IF.
        if (ws.getIndBtcBatch().getCodDominio() == 0) {
            // COB_CODE: MOVE BTC-DT-EFF-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBtcBatchDb().getEffDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO BTC-DT-EFF
            btcBatch.getBtcDtEff().setBtcDtEff(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF BTC-DATA-BATCH
        //                       TO BTC-DATA-BATCH-LEN.
        btcBatch.setBtcDataBatchLen(((short)BtcBatch.Len.BTC_DATA_BATCH));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z701-TS-N-TO-X<br>*/
    private void z701TsNToX() {
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(1:4)
        //                TO WS-TIMESTAMP-X(1:4)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(5:2)
        //                TO WS-TIMESTAMP-X(6:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(7:2)
        //                TO WS-TIMESTAMP-X(9:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(9:2)
        //                TO WS-TIMESTAMP-X(12:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((9) - 1, 10), 12, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(11:2)
        //                TO WS-TIMESTAMP-X(15:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((11) - 1, 12), 15, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(13:2)
        //                TO WS-TIMESTAMP-X(18:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((13) - 1, 14), 18, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(15:4)
        //                TO WS-TIMESTAMP-X(21:4)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((15) - 1, 18), 21, 4));
        // COB_CODE: MOVE '00'
        //                TO WS-TIMESTAMP-X(25:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "00", 25, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-TIMESTAMP-X(5:1)
        //                   WS-TIMESTAMP-X(8:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "-", 5, 1));
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "-", 8, 1));
        // COB_CODE: MOVE SPACE
        //                TO WS-TIMESTAMP-X(11:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), LiteralGenerator.create(Types.SPACE_CHAR, 1), 11, 1));
        // COB_CODE: MOVE ':'
        //                TO WS-TIMESTAMP-X(14:1)
        //                   WS-TIMESTAMP-X(17:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ":", 14, 1));
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ":", 17, 1));
        // COB_CODE: MOVE '.'
        //                TO WS-TIMESTAMP-X(20:1).
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ".", 20, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    /**Original name: Z801-TS-X-TO-N<br>*/
    private void z801TsXToN() {
        // COB_CODE: MOVE WS-TIMESTAMP-X(1:4)
        //                TO WS-STR-TIMESTAMP-N(1:4)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-TIMESTAMP-X(6:2)
        //                TO WS-STR-TIMESTAMP-N(5:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(9:2)
        //                TO WS-STR-TIMESTAMP-N(7:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((9) - 1, 10), 7, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(12:2)
        //                TO WS-STR-TIMESTAMP-N(9:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((12) - 1, 13), 9, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(15:2)
        //                TO WS-STR-TIMESTAMP-N(11:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((15) - 1, 16), 11, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(18:2)
        //                TO WS-STR-TIMESTAMP-N(13:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((18) - 1, 19), 13, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(21:4)
        //                TO WS-STR-TIMESTAMP-N(15:4).
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((21) - 1, 24), 15, 4));
    }

    @Override
    public int getBtcCodBatchType() {
        return btcBatch.getBtcCodBatchType();
    }

    @Override
    public void setBtcCodBatchType(int btcCodBatchType) {
        this.btcBatch.setBtcCodBatchType(btcCodBatchType);
    }

    @Override
    public int getBtcCodCompAnia() {
        return btcBatch.getBtcCodCompAnia();
    }

    @Override
    public void setBtcCodCompAnia(int btcCodCompAnia) {
        this.btcBatch.setBtcCodCompAnia(btcCodCompAnia);
    }

    @Override
    public String getBtcProtocol() {
        return btcBatch.getBtcProtocol();
    }

    @Override
    public void setBtcProtocol(String btcProtocol) {
        this.btcBatch.setBtcProtocol(btcProtocol);
    }

    @Override
    public char getCodBatchState() {
        return btcBatch.getBtcCodBatchState();
    }

    @Override
    public void setCodBatchState(char codBatchState) {
        this.btcBatch.setBtcCodBatchState(codBatchState);
    }

    @Override
    public String getDataBatchVchar() {
        return btcBatch.getBtcDataBatchVcharFormatted();
    }

    @Override
    public void setDataBatchVchar(String dataBatchVchar) {
        this.btcBatch.setBtcDataBatchVcharFormatted(dataBatchVchar);
    }

    @Override
    public String getDataBatchVcharObj() {
        if (ws.getIndBtcBatch().getFormattazioneDato() >= 0) {
            return getDataBatchVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDataBatchVcharObj(String dataBatchVcharObj) {
        if (dataBatchVcharObj != null) {
            setDataBatchVchar(dataBatchVcharObj);
            ws.getIndBtcBatch().setFormattazioneDato(((short)0));
        }
        else {
            ws.getIndBtcBatch().setFormattazioneDato(((short)-1));
        }
    }

    @Override
    public String getDtEffDb() {
        return ws.getBtcBatchDb().getEffDb();
    }

    @Override
    public void setDtEffDb(String dtEffDb) {
        this.ws.getBtcBatchDb().setEffDb(dtEffDb);
    }

    @Override
    public String getDtEffDbObj() {
        if (ws.getIndBtcBatch().getCodDominio() >= 0) {
            return getDtEffDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEffDbObj(String dtEffDbObj) {
        if (dtEffDbObj != null) {
            setDtEffDb(dtEffDbObj);
            ws.getIndBtcBatch().setTypeRecord(((short)0));
        }
        else {
            ws.getIndBtcBatch().setTypeRecord(((short)-1));
        }
    }

    @Override
    public String getDtEndDb() {
        return ws.getBtcBatchDb().getEndDb();
    }

    @Override
    public void setDtEndDb(String dtEndDb) {
        this.ws.getBtcBatchDb().setEndDb(dtEndDb);
    }

    @Override
    public String getDtEndDbObj() {
        if (ws.getIndBtcBatch().getTipoDato() >= 0) {
            return getDtEndDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndDbObj(String dtEndDbObj) {
        if (dtEndDbObj != null) {
            setDtEndDb(dtEndDbObj);
            ws.getIndBtcBatch().setTipoDato(((short)0));
        }
        else {
            ws.getIndBtcBatch().setTipoDato(((short)-1));
        }
    }

    @Override
    public String getDtInsDb() {
        return ws.getBtcBatchDb().getInsDb();
    }

    @Override
    public void setDtInsDb(String dtInsDb) {
        this.ws.getBtcBatchDb().setInsDb(dtInsDb);
    }

    @Override
    public String getDtStartDb() {
        return ws.getBtcBatchDb().getStartDb();
    }

    @Override
    public void setDtStartDb(String dtStartDb) {
        this.ws.getBtcBatchDb().setStartDb(dtStartDb);
    }

    @Override
    public String getDtStartDbObj() {
        if (ws.getIndBtcBatch().getDescDato() >= 0) {
            return getDtStartDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtStartDbObj(String dtStartDbObj) {
        if (dtStartDbObj != null) {
            setDtStartDb(dtStartDbObj);
            ws.getIndBtcBatch().setFlContiguousData(((short)0));
        }
        else {
            ws.getIndBtcBatch().setFlContiguousData(((short)-1));
        }
    }

    @Override
    public char getIabv0002State01() {
        return iabv0002.getIabv0002StateGlobal().getState01();
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        this.iabv0002.getIabv0002StateGlobal().setState01(iabv0002State01);
    }

    @Override
    public char getIabv0002State02() {
        return iabv0002.getIabv0002StateGlobal().getState02();
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        this.iabv0002.getIabv0002StateGlobal().setState02(iabv0002State02);
    }

    @Override
    public char getIabv0002State03() {
        return iabv0002.getIabv0002StateGlobal().getState03();
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        this.iabv0002.getIabv0002StateGlobal().setState03(iabv0002State03);
    }

    @Override
    public char getIabv0002State04() {
        return iabv0002.getIabv0002StateGlobal().getState04();
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        this.iabv0002.getIabv0002StateGlobal().setState04(iabv0002State04);
    }

    @Override
    public char getIabv0002State05() {
        return iabv0002.getIabv0002StateGlobal().getState05();
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        this.iabv0002.getIabv0002StateGlobal().setState05(iabv0002State05);
    }

    @Override
    public char getIabv0002State06() {
        return iabv0002.getIabv0002StateGlobal().getState06();
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        this.iabv0002.getIabv0002StateGlobal().setState06(iabv0002State06);
    }

    @Override
    public char getIabv0002State07() {
        return iabv0002.getIabv0002StateGlobal().getState07();
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        this.iabv0002.getIabv0002StateGlobal().setState07(iabv0002State07);
    }

    @Override
    public char getIabv0002State08() {
        return iabv0002.getIabv0002StateGlobal().getState08();
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        this.iabv0002.getIabv0002StateGlobal().setState08(iabv0002State08);
    }

    @Override
    public char getIabv0002State09() {
        return iabv0002.getIabv0002StateGlobal().getState09();
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        this.iabv0002.getIabv0002StateGlobal().setState09(iabv0002State09);
    }

    @Override
    public char getIabv0002State10() {
        return iabv0002.getIabv0002StateGlobal().getState10();
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        this.iabv0002.getIabv0002StateGlobal().setState10(iabv0002State10);
    }

    @Override
    public char getIabv0002StateCurrent() {
        return iabv0002.getIabv0002StateCurrent();
    }

    @Override
    public void setIabv0002StateCurrent(char iabv0002StateCurrent) {
        this.iabv0002.setIabv0002StateCurrent(iabv0002StateCurrent);
    }

    @Override
    public int getIdBatch() {
        return btcBatch.getBtcIdBatch();
    }

    @Override
    public void setIdBatch(int idBatch) {
        this.btcBatch.setBtcIdBatch(idBatch);
    }

    @Override
    public int getIdRich() {
        return btcBatch.getBtcIdRich().getBtcIdRich();
    }

    @Override
    public void setIdRich(int idRich) {
        this.btcBatch.getBtcIdRich().setBtcIdRich(idRich);
    }

    @Override
    public Integer getIdRichObj() {
        if (ws.getIndBtcBatch().getPrecisioneDato() >= 0) {
            return ((Integer)getIdRich());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRichObj(Integer idRichObj) {
        if (idRichObj != null) {
            setIdRich(((int)idRichObj));
            ws.getIndBtcBatch().setPrecisioneDato(((short)0));
        }
        else {
            ws.getIndBtcBatch().setPrecisioneDato(((short)-1));
        }
    }

    @Override
    public String getUserIns() {
        return btcBatch.getBtcUserIns();
    }

    @Override
    public void setUserIns(String userIns) {
        this.btcBatch.setBtcUserIns(userIns);
    }

    @Override
    public String getUserStart() {
        return btcBatch.getBtcUserStart();
    }

    @Override
    public void setUserStart(String userStart) {
        this.btcBatch.setBtcUserStart(userStart);
    }

    @Override
    public String getUserStartObj() {
        if (ws.getIndBtcBatch().getLunghezzaDato() >= 0) {
            return getUserStart();
        }
        else {
            return null;
        }
    }

    @Override
    public void setUserStartObj(String userStartObj) {
        if (userStartObj != null) {
            setUserStart(userStartObj);
            ws.getIndBtcBatch().setLunghezzaDato(((short)0));
        }
        else {
            ws.getIndBtcBatch().setLunghezzaDato(((short)-1));
        }
    }
}
