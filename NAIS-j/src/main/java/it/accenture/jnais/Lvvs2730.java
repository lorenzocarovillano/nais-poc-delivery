package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs2730Data;
import static java.lang.Math.abs;

/**Original name: LVVS2730<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2013.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS2730
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... IMPOSTA DI BOLLO
 *                   CALCOLO DELLA VARIABILE B_SW (IMPSWANNPOL)
 * **------------------------------------------------------------***</pre>*/
public class Lvvs2730 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs2730Data ws = new Lvvs2730Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS2730
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS2730_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs2730 getInstance() {
        return ((Lvvs2730)Programs.getInstance(Lvvs2730.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT
        //                                             WK-VAR-MOVI-COMUN.
        initIxIndici();
        initTabOutput();
        initWkVarMoviComun();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET COMUN-TROV-NO                 TO TRUE.
        ws.getFlagComunTrov().setNo();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-P58.
        initAreaIoP58();
        // COB_CODE: PERFORM VERIFICA-MOVIMENTO
        //              THRU VERIFICA-MOVIMENTO-EX
        verificaMovimento();
        //--> PERFORM DI CALCOLO ANNO CORRENTE
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //           *    AND IDSV0003-SUCCESSFUL-SQL
        //                       THRU S1150-CALCOLO-ANNO-EX
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            //    AND IDSV0003-SUCCESSFUL-SQL
            // COB_CODE: PERFORM S1150-CALCOLO-ANNO
            //              THRU S1150-CALCOLO-ANNO-EX
            s1150CalcoloAnno();
        }
        //--> PERFORM DI CALCOLO DELL'IMPORTO
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //           *    AND IDSV0003-SUCCESSFUL-SQL
        //                       THRU S1200-CALCOLO-IMPORTO-EX
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            //    AND IDSV0003-SUCCESSFUL-SQL
            // COB_CODE: PERFORM S1200-CALCOLO-IMPORTO
            //              THRU S1200-CALCOLO-IMPORTO-EX
            s1200CalcoloImporto();
        }
    }

    /**Original name: VERIFICA-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA MOVIMENTO
	 * ----------------------------------------------------------------*
	 * --> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
	 * --> IMMAGINI IN FASE DI COMUNICAZIONE
	 *     MOVE IVVC0213-TIPO-MOVIMENTO
	 *       TO WS-MOVIMENTO</pre>*/
    private void verificaMovimento() {
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG
        //             TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
        // COB_CODE:      IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
        //                   LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
        //                   LIQUI-RPP-TAKE-PROFIT
        //                OR LIQUI-RISTOT-INCAPIENZA
        //                OR LIQUI-RPP-REDDITO-PROGR
        //                OR LIQUI-RPP-BENEFICIO-CONTR
        //                OR LIQUI-RISTOT-INCAP
        //           *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
        //                   END-IF
        //                ELSE
        //           *-->    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //           *-->    RISPETTIVE AREE DCLGEN IN WORKING
        //                           SPACES OR LOW-VALUE OR HIGH-VALUE
        //                END-IF.
        if (ws.getWsMovimento().isLiquiRistotInd() || ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiScapol() || ws.getWsMovimento().isLiquiSinind() || ws.getWsMovimento().isLiquiRecind() || ws.getWsMovimento().isLiquiRppTakeProfit() || ws.getWsMovimento().isLiquiRistotIncapienza() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRppBeneficioContr() || ws.getWsMovimento().isLiquiRistotIncap()) {
            //--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
            // COB_CODE: PERFORM RECUP-MOVI-COMUN
            //              THRU RECUP-MOVI-COMUN-EX
            recupMoviComun();
            // COB_CODE:         IF COMUN-TROV-SI
            //                      THRU LETTURA-IMPST-BOLLO-EX
            //                   ELSE
            //           *-->       ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
            //           *-->       RISPETTIVE AREE DCLGEN IN WORKING
            //                              SPACES OR LOW-VALUE OR HIGH-VALUE
            //                   END-IF
            if (ws.getFlagComunTrov().isSi()) {
                // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
                //             TO WK-DATA-CPTZ-RIP
                ws.getWkVarMoviComun().setDataCptzRip(idsv0003.getDataCompetenza());
                // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                //             TO WK-DATA-EFF-RIP
                ws.getWkVarMoviComun().setDataEffRip(idsv0003.getDataInizioEffetto());
                // COB_CODE: MOVE WK-DATA-CPTZ-PREC
                //             TO IDSV0003-DATA-COMPETENZA
                idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getDataCptzPrec());
                // COB_CODE: MOVE WK-DATA-EFF-PREC
                //             TO IDSV0003-DATA-INIZIO-EFFETTO
                //                IDSV0003-DATA-FINE-EFFETTO
                idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getDataEffPrec());
                idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getDataEffPrec());
                //          UNTIL IX-TAB-P58 > 10
                // COB_CODE:         PERFORM VARYING IX-TAB-P58 FROM 1 BY 1
                //           *          UNTIL IX-TAB-P58 > 10
                //                      UNTIL IX-TAB-P58 > 75
                //                         THRU INIZIA-TOT-P58-EX
                //                   END-PERFORM
                ws.setIxTabP58(1);
                while (!(ws.getIxTabP58() > 75)) {
                    // COB_CODE: PERFORM INIZIA-TOT-P58
                    //              THRU INIZIA-TOT-P58-EX
                    iniziaTotP58();
                    ws.setIxTabP58(Trunc.toInt(ws.getIxTabP58() + 1, 9));
                }
                // COB_CODE: PERFORM LETTURA-IMPST-BOLLO
                //              THRU LETTURA-IMPST-BOLLO-EX
                letturaImpstBollo();
            }
            else {
                //-->       ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
                //-->       RISPETTIVE AREE DCLGEN IN WORKING
                // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
                //              THRU S1100-VALORIZZA-DCLGEN-EX
                //           VARYING IX-DCLGEN FROM 1 BY 1
                //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                //                   SPACES OR LOW-VALUE OR HIGH-VALUE
                ws.setIxDclgen(((short)1));
                while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
                    s1100ValorizzaDclgen();
                    ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
                }
            }
        }
        else {
            //-->    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
            //-->    RISPETTIVE AREE DCLGEN IN WORKING
            // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
            //              THRU S1100-VALORIZZA-DCLGEN-EX
            //           VARYING IX-DCLGEN FROM 1 BY 1
            //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
            //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
            //                   SPACES OR LOW-VALUE OR HIGH-VALUE
            ws.setIxDclgen(((short)1));
            while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
                s1100ValorizzaDclgen();
                ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
            }
        }
    }

    /**Original name: RECUP-MOVI-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Recupero il movimento di comunicazione
	 * ----------------------------------------------------------------*</pre>*/
    private void recupMoviComun() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET INIT-CUR-MOV               TO TRUE
        ws.getFlagCurMov().setInitCurMov();
        // COB_CODE: SET COMUN-TROV-NO              TO TRUE
        ws.getFlagComunTrov().setNo();
        // COB_CODE: INITIALIZE MOVI
        initMovi();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET  NO-ULTIMA-LETTURA         TO TRUE
        ws.getFlagUltimaLettura().setNoUltimaLettura();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-CUR-MOV
        //                      OR COMUN-TROV-SI
        //                 END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagCurMov().isFineCurMov() || ws.getFlagComunTrov().isSi())) {
            // COB_CODE: INITIALIZE MOVI
            initMovi();
            //
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                   TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR   TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL  LDBS6040   USING IDSV0003 MOVI
            //             ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                ldbs6040 = Ldbs6040.getInstance();
                ldbs6040.run(idsv0003, ws.getMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:           IF IDSV0003-SUCCESSFUL-RC
            //                         END-EVALUATE
            //                      ELSE
            //           *--> GESTIRE ERRORE
            //                         SET IDSV0003-INVALID-OPER   TO TRUE
            //                      END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:               EVALUATE TRUE
                //                             WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //           *-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                //           *-->     BOLLO IN INPUT
                //                               SET FINE-CUR-MOV TO TRUE
                //                             WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                //           *                   MOVE IDSV0003-BUFFER-DATI TO MOVI
                //           *      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                //                               END-IF
                //                             WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                                  SET IDSV0003-INVALID-OPER   TO TRUE
                //                         END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                        //-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                        //-->     BOLLO IN INPUT
                        // COB_CODE: SET FINE-CUR-MOV TO TRUE
                        ws.getFlagCurMov().setFineCurMov();
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                        //                   MOVE IDSV0003-BUFFER-DATI TO MOVI
                        //      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                        // COB_CODE: MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                        ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                        // COB_CODE: IF COMUN-RISPAR-IND
                        //           OR RISTO-INDIVI
                        //           OR VARIA-OPZION
                        //           OR SINIS-INDIVI
                        //           OR RECES-INDIVI
                        //           OR RPP-TAKE-PROFIT
                        //           OR COMUN-RISTOT-INCAPIENZA
                        //           OR RPP-REDDITO-PROGRAMMATO
                        //           OR RPP-BENEFICIO-CONTR
                        //           OR COMUN-RISTOT-INCAP
                        //              SET FINE-CUR-MOV   TO TRUE
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-IF
                        if (ws.getWsMovimento().isComunRisparInd() || ws.getWsMovimento().isRistoIndivi() || ws.getWsMovimento().isVariaOpzion() || ws.getWsMovimento().isSinisIndivi() || ws.getWsMovimento().isRecesIndivi() || ws.getWsMovimento().isRppTakeProfit() || ws.getWsMovimento().isComunRistotIncapienza() || ws.getWsMovimento().isRppRedditoProgrammato() || ws.getWsMovimento().isRppBeneficioContr() || ws.getWsMovimento().isComunRistotIncap()) {
                            // COB_CODE: MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                            ws.getWkVarMoviComun().setIdMoviComun(ws.getMovi().getMovIdMovi());
                            // COB_CODE: SET SI-ULTIMA-LETTURA  TO TRUE
                            ws.getFlagUltimaLettura().setSiUltimaLettura();
                            // COB_CODE: MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                            ws.getWkVarMoviComun().setDataEffPrec(ws.getMovi().getMovDtEff());
                            // COB_CODE: COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                            //                                     - 1
                            ws.getWkVarMoviComun().setDataCptzPrec(Trunc.toLong(ws.getMovi().getMovDsTsCptz() - 1, 18));
                            // COB_CODE: SET COMUN-TROV-SI   TO TRUE
                            ws.getFlagComunTrov().setSi();
                            // COB_CODE: PERFORM CLOSE-MOVI
                            //              THRU CLOSE-MOVI-EX
                            closeMovi();
                            // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                            ws.getFlagCurMov().setFineCurMov();
                        }
                        else {
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            idsv0003.getOperazione().setFetchNext();
                        }
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE LDBS6040
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                        // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                        // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE LDBS6040
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
        // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
    }

    /**Original name: CLOSE-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
	 * ----------------------------------------------------------------*</pre>*/
    private void closeMovi() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR            TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: CALL LDBS6040 USING IDSV0003 MOVI
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            ldbs6040 = Ldbs6040.getInstance();
            ldbs6040.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS6040
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
            // COB_CODE: MOVE 'ERRORE CALL LDBS6040 CLOSE'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS6040 CLOSE");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                TO TRUE
        //           END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CLOSE CURSORE LDBS6040'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CLOSE CURSORE LDBS6040");
            // COB_CODE: SET IDSV0003-INVALID-OPER
            //            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: LETTURA-IMPST-BOLLO<br>
	 * <pre>----------------------------------------------------------------*
	 *     RECUPERO IMPOSTA DI BOLLO DA MOVIMENTO DI COMUNICAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaImpstBollo() {
        Ldbse590 ldbse590 = null;
        // COB_CODE: SET FINE-LETT-P58-NO           TO TRUE
        ws.getFlagFineLetturaP58().setNo();
        // COB_CODE: INITIALIZE IMPST-BOLLO
        initImpstBollo();
        // COB_CODE: MOVE 0 TO DP58-ELE-P58-MAX
        ws.setDp58EleP58Max(((short)0));
        // COB_CODE: MOVE 0 TO IX-TAB-P58
        ws.setIxTabP58(0);
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-LETT-P58-SI
        //                 END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagFineLetturaP58().isSi())) {
            // COB_CODE: INITIALIZE IMPST-BOLLO
            initImpstBollo();
            //
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA    TO P58-ID-POLI
            ws.getImpstBollo().setP58IdPoli(ivvc0213.getIdPolizza());
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL  LDBSE590   USING IDSV0003 IMPST-BOLLO
            //             ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                ldbse590 = Ldbse590.getInstance();
                ldbse590.run(idsv0003, ws.getImpstBollo());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBSE590          TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbse590());
                // COB_CODE: MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSE590 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:           IF IDSV0003-SUCCESSFUL-RC
            //                         END-EVALUATE
            //                      ELSE
            //           *--> GESTIRE ERRORE
            //                         SET FINE-LETT-P58-SI TO TRUE
            //                      END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:               EVALUATE TRUE
                //                             WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //                               SET FINE-LETT-P58-SI TO TRUE
                //                                  SET IDSV0003-FETCH-NEXT   TO TRUE
                //                             WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                                  SET FINE-LETT-P58-SI TO TRUE
                //                         END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                        // COB_CODE: SET FINE-LETT-P58-SI TO TRUE
                        ws.getFlagFineLetturaP58().setSi();
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: code not available
                        ws.setIntRegister1(1);
                        ws.setDp58EleP58Max(Trunc.toShort(ws.getIntRegister1() + ws.getDp58EleP58Max(), 4));
                        ws.setIxTabP58(Trunc.toInt(abs(ws.getIntRegister1() + ws.getIxTabP58()), 9));
                        // COB_CODE: PERFORM VALORIZZA-OUTPUT-P58
                        //              THRU VALORIZZA-OUTPUT-P58-EX
                        valorizzaOutputP58();
                        // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                        idsv0003.getOperazione().setFetchNext();
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE LDBSE590
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbse590());
                        // COB_CODE: MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSE590 ERRORE CHIAMATA");
                        // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: SET FINE-LETT-P58-SI TO TRUE
                        ws.getFlagFineLetturaP58().setSi();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE LDBSE590
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbse590());
                // COB_CODE: MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSE590 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: SET FINE-LETT-P58-SI TO TRUE
                ws.getFlagFineLetturaP58().setSi();
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-IMPOSTA-BOLLO
        //                TO DP58-AREA-P58
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasImpostaBollo())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DP58-AREA-P58
            ws.setDp58AreaP58Formatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1150-CALCOLO-ANNO<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLO ANNO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1150CalcoloAnno() {
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //             TO WK-DATA
        ws.getWkDataSeparata().setWkData(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
        // COB_CODE: MOVE WK-ANNO
        //             TO WK-ANNO-INI
        //                WK-ANNO-FINE
        ws.getWkDataInizio().setAnnoIniFormatted(ws.getWkDataSeparata().getAnnoFormatted());
        ws.getWkDataFine().setAnnoFineFormatted(ws.getWkDataSeparata().getAnnoFormatted());
        // COB_CODE: MOVE 01
        //             TO WK-MESE-INI
        //                WK-GG-INI
        ws.getWkDataInizio().setMeseIni(((short)1));
        ws.getWkDataInizio().setGgIni(((short)1));
        // COB_CODE: MOVE 12
        //             TO WK-MESE-FINE
        ws.getWkDataFine().setMeseFine(((short)12));
        // COB_CODE: MOVE 31
        //             TO WK-GG-FINE.
        ws.getWkDataFine().setGgFine(((short)31));
        // COB_CODE: MOVE WK-DATA-INIZIO
        //             TO WK-DATA
        ws.getWkDataSeparata().setWkDataFromBuffer(ws.getWkDataInizio().getWkDataInizioBytes());
        // COB_CODE: MOVE WK-DATA
        //             TO WK-DATA-INIZIO-D.
        ws.setWkDataInizioD(ws.getWkDataSeparata().getWkData());
        // COB_CODE: MOVE WK-DATA-FINE
        //             TO WK-DATA
        ws.getWkDataSeparata().setWkDataFromBuffer(ws.getWkDataFine().getWkDataFineBytes());
        // COB_CODE: MOVE WK-DATA
        //             TO WK-DATA-FINE-D.
        ws.setWkDataFineD(ws.getWkDataSeparata().getWkData());
    }

    /**Original name: S1200-CALCOLO-IMPORTO<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLO IMPORTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200CalcoloImporto() {
        // COB_CODE: MOVE 0   TO IVVC0213-VAL-IMP-O
        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > DP58-ELE-P58-MAX
        //             END-IF
        //           END-PERFORM.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ws.getDp58EleP58Max())) {
            // COB_CODE: IF DP58-TP-CAUS-BOLLO(IX-DCLGEN)  = 'SW'
            //              AND DP58-DT-END-CALC(IX-DCLGEN) >= WK-DATA-INIZIO-D
            //              AND DP58-DT-END-CALC(IX-DCLGEN) <= WK-DATA-FINE-D
            //                          + DP58-IMPST-BOLLO-TOT-R(IX-DCLGEN)
            //           END-IF
            if (Conditions.eq(ws.getDp58TabP58(ws.getIxDclgen()).getLccvp581().getDati().getWp58TpCausBollo(), "SW") && ws.getDp58TabP58(ws.getIxDclgen()).getLccvp581().getDati().getWp58DtEndCalc() >= ws.getWkDataInizioD() && ws.getDp58TabP58(ws.getIxDclgen()).getLccvp581().getDati().getWp58DtEndCalc() <= ws.getWkDataFineD()) {
                // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O = IVVC0213-VAL-IMP-O
                //                       + DP58-IMPST-BOLLO-TOT-R(IX-DCLGEN)
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ivvc0213.getTabOutput().getValImpO().add(ws.getDp58TabP58(ws.getIxDclgen()).getLccvp581().getDati().getWp58ImpstBolloTotR()), 18, 7));
            }
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        //
        //    IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
        //       LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND
        // COB_CODE: IF COMUN-TROV-SI
        //                   IDSV0003-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getFlagComunTrov().isSi()) {
            // COB_CODE: MOVE WK-DATA-CPTZ-RIP
            //             TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getDataCptzRip());
            // COB_CODE: MOVE WK-DATA-EFF-RIP
            //             TO IDSV0003-DATA-INIZIO-EFFETTO
            //                IDSV0003-DATA-FINE-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getDataEffRip());
            idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getDataEffRip());
        }
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: VALORIZZA-OUTPUT-P58<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY IMPOSTA DI BOLLO
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVP583
	 *    ULTIMO AGG. 09 AGO 2013
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputP58() {
        // COB_CODE: MOVE P58-ID-IMPST-BOLLO
        //             TO (SF)-ID-PTF(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().setIdPtf(ws.getImpstBollo().getP58IdImpstBollo());
        // COB_CODE: MOVE P58-ID-IMPST-BOLLO
        //             TO (SF)-ID-IMPST-BOLLO(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58IdImpstBollo(ws.getImpstBollo().getP58IdImpstBollo());
        // COB_CODE: MOVE P58-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58CodCompAnia(ws.getImpstBollo().getP58CodCompAnia());
        // COB_CODE: MOVE P58-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58IdPoli(ws.getImpstBollo().getP58IdPoli());
        // COB_CODE: MOVE P58-IB-POLI
        //             TO (SF)-IB-POLI(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58IbPoli(ws.getImpstBollo().getP58IbPoli());
        // COB_CODE: IF P58-COD-FISC-NULL = HIGH-VALUES
        //                TO (SF)-COD-FISC-NULL(IX-TAB-P58)
        //           ELSE
        //                TO (SF)-COD-FISC(IX-TAB-P58)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getImpstBollo().getP58CodFiscFormatted())) {
            // COB_CODE: MOVE P58-COD-FISC-NULL
            //             TO (SF)-COD-FISC-NULL(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58CodFisc(ws.getImpstBollo().getP58CodFisc());
        }
        else {
            // COB_CODE: MOVE P58-COD-FISC
            //             TO (SF)-COD-FISC(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58CodFisc(ws.getImpstBollo().getP58CodFisc());
        }
        // COB_CODE: IF P58-COD-PART-IVA-NULL = HIGH-VALUES
        //                TO (SF)-COD-PART-IVA-NULL(IX-TAB-P58)
        //           ELSE
        //                TO (SF)-COD-PART-IVA(IX-TAB-P58)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getImpstBollo().getP58CodPartIvaFormatted())) {
            // COB_CODE: MOVE P58-COD-PART-IVA-NULL
            //             TO (SF)-COD-PART-IVA-NULL(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58CodPartIva(ws.getImpstBollo().getP58CodPartIva());
        }
        else {
            // COB_CODE: MOVE P58-COD-PART-IVA
            //             TO (SF)-COD-PART-IVA(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58CodPartIva(ws.getImpstBollo().getP58CodPartIva());
        }
        // COB_CODE: MOVE P58-ID-RAPP-ANA
        //             TO (SF)-ID-RAPP-ANA(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58IdRappAna(ws.getImpstBollo().getP58IdRappAna());
        // COB_CODE: MOVE P58-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58IdMoviCrz(ws.getImpstBollo().getP58IdMoviCrz());
        // COB_CODE: IF P58-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P58)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-P58)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getImpstBollo().getP58IdMoviChiu().getP58IdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE P58-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().getWp58IdMoviChiu().setDp58IdMoviChiuNull(ws.getImpstBollo().getP58IdMoviChiu().getP58IdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE P58-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().getWp58IdMoviChiu().setWp58IdMoviChiu(ws.getImpstBollo().getP58IdMoviChiu().getP58IdMoviChiu());
        }
        // COB_CODE: MOVE P58-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DtIniEff(ws.getImpstBollo().getP58DtIniEff());
        // COB_CODE: MOVE P58-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DtEndEff(ws.getImpstBollo().getP58DtEndEff());
        // COB_CODE: MOVE P58-DT-INI-CALC
        //             TO (SF)-DT-INI-CALC(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DtIniCalc(ws.getImpstBollo().getP58DtIniCalc());
        // COB_CODE: MOVE P58-DT-END-CALC
        //             TO (SF)-DT-END-CALC(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DtEndCalc(ws.getImpstBollo().getP58DtEndCalc());
        // COB_CODE: MOVE P58-TP-CAUS-BOLLO
        //             TO (SF)-TP-CAUS-BOLLO(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58TpCausBollo(ws.getImpstBollo().getP58TpCausBollo());
        // COB_CODE: MOVE P58-IMPST-BOLLO-DETT-C
        //             TO (SF)-IMPST-BOLLO-DETT-C(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58ImpstBolloDettC(Trunc.toDecimal(ws.getImpstBollo().getP58ImpstBolloDettC(), 15, 3));
        // COB_CODE: IF P58-IMPST-BOLLO-DETT-V-NULL = HIGH-VALUES
        //                TO (SF)-IMPST-BOLLO-DETT-V-NULL(IX-TAB-P58)
        //           ELSE
        //                TO (SF)-IMPST-BOLLO-DETT-V(IX-TAB-P58)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getImpstBollo().getP58ImpstBolloDettV().getP58ImpstBolloDettVNullFormatted())) {
            // COB_CODE: MOVE P58-IMPST-BOLLO-DETT-V-NULL
            //             TO (SF)-IMPST-BOLLO-DETT-V-NULL(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().getWp58ImpstBolloDettV().setDp58ImpstBolloDettVNull(ws.getImpstBollo().getP58ImpstBolloDettV().getP58ImpstBolloDettVNull());
        }
        else {
            // COB_CODE: MOVE P58-IMPST-BOLLO-DETT-V
            //             TO (SF)-IMPST-BOLLO-DETT-V(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().getWp58ImpstBolloDettV().setWp58ImpstBolloDettV(Trunc.toDecimal(ws.getImpstBollo().getP58ImpstBolloDettV().getP58ImpstBolloDettV(), 15, 3));
        }
        // COB_CODE: IF P58-IMPST-BOLLO-TOT-V-NULL = HIGH-VALUES
        //                TO (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-P58)
        //           ELSE
        //                TO (SF)-IMPST-BOLLO-TOT-V(IX-TAB-P58)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getImpstBollo().getP58ImpstBolloTotV().getP58ImpstBolloTotVNullFormatted())) {
            // COB_CODE: MOVE P58-IMPST-BOLLO-TOT-V-NULL
            //             TO (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().getWp58ImpstBolloTotV().setDp58ImpstBolloTotVNull(ws.getImpstBollo().getP58ImpstBolloTotV().getP58ImpstBolloTotVNull());
        }
        else {
            // COB_CODE: MOVE P58-IMPST-BOLLO-TOT-V
            //             TO (SF)-IMPST-BOLLO-TOT-V(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().getWp58ImpstBolloTotV().setWp58ImpstBolloTotV(Trunc.toDecimal(ws.getImpstBollo().getP58ImpstBolloTotV().getP58ImpstBolloTotV(), 15, 3));
        }
        // COB_CODE: MOVE P58-IMPST-BOLLO-TOT-R
        //             TO (SF)-IMPST-BOLLO-TOT-R(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58ImpstBolloTotR(Trunc.toDecimal(ws.getImpstBollo().getP58ImpstBolloTotR(), 15, 3));
        // COB_CODE: MOVE P58-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsRiga(ws.getImpstBollo().getP58DsRiga());
        // COB_CODE: MOVE P58-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsOperSql(ws.getImpstBollo().getP58DsOperSql());
        // COB_CODE: MOVE P58-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsVer(ws.getImpstBollo().getP58DsVer());
        // COB_CODE: MOVE P58-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsTsIniCptz(ws.getImpstBollo().getP58DsTsIniCptz());
        // COB_CODE: MOVE P58-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsTsEndCptz(ws.getImpstBollo().getP58DsTsEndCptz());
        // COB_CODE: MOVE P58-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsUtente(ws.getImpstBollo().getP58DsUtente());
        // COB_CODE: MOVE P58-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-P58).
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsStatoElab(ws.getImpstBollo().getP58DsStatoElab());
    }

    /**Original name: INIZIA-TOT-P58<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVP584
	 *    ULTIMO AGG. 09 AGO 2013
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotP58() {
        // COB_CODE: PERFORM INIZIA-ZEROES-P58 THRU INIZIA-ZEROES-P58-EX
        iniziaZeroesP58();
        // COB_CODE: PERFORM INIZIA-SPACES-P58 THRU INIZIA-SPACES-P58-EX
        iniziaSpacesP58();
        // COB_CODE: PERFORM INIZIA-NULL-P58 THRU INIZIA-NULL-P58-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVP584:line=14, because the code is unreachable.
    }

    /**Original name: INIZIA-ZEROES-P58<br>*/
    private void iniziaZeroesP58() {
        // COB_CODE: MOVE 0 TO (SF)-ID-IMPST-BOLLO(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58IdImpstBollo(0);
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58CodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-POLI(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58IdPoli(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-RAPP-ANA(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58IdRappAna(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58IdMoviCrz(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DtIniEff(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DtEndEff(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-INI-CALC(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DtIniCalc(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-END-CALC(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DtEndCalc(0);
        // COB_CODE: MOVE 0 TO (SF)-IMPST-BOLLO-DETT-C(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58ImpstBolloDettC(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: MOVE 0 TO (SF)-IMPST-BOLLO-TOT-R(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58ImpstBolloTotR(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: MOVE 0 TO (SF)-DS-RIGA(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsRiga(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsTsIniCptz(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-P58).
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsTsEndCptz(0);
    }

    /**Original name: INIZIA-SPACES-P58<br>*/
    private void iniziaSpacesP58() {
        // COB_CODE: MOVE SPACES TO (SF)-IB-POLI(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58IbPoli("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-CAUS-BOLLO(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58TpCausBollo("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsUtente("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-P58).
        ws.getDp58TabP58(ws.getIxTabP58()).getLccvp581().getDati().setWp58DsStatoElab(Types.SPACE_CHAR);
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabIso(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initWkVarMoviComun() {
        ws.getWkVarMoviComun().setIdMoviComun(0);
        ws.getWkVarMoviComun().setDataEffPrec(0);
        ws.getWkVarMoviComun().setDataCptzPrec(0);
        ws.getWkVarMoviComun().setDataEffRip(0);
        ws.getWkVarMoviComun().setDataCptzRip(0);
    }

    public void initAreaIoP58() {
        ws.setDp58EleP58Max(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs2730Data.DP58_TAB_P58_MAXOCCURS; idx0++) {
            ws.getDp58TabP58(idx0).getLccvp581().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDp58TabP58(idx0).getLccvp581().setIdPtf(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58IdImpstBollo(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58CodCompAnia(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58IdPoli(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58IbPoli("");
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58CodFisc("");
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58CodPartIva("");
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58IdRappAna(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58IdMoviCrz(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().getWp58IdMoviChiu().setWp58IdMoviChiu(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DtIniEff(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DtEndEff(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DtIniCalc(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DtEndCalc(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58TpCausBollo("");
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58ImpstBolloDettC(new AfDecimal(0, 15, 3));
            ws.getDp58TabP58(idx0).getLccvp581().getDati().getWp58ImpstBolloDettV().setWp58ImpstBolloDettV(new AfDecimal(0, 15, 3));
            ws.getDp58TabP58(idx0).getLccvp581().getDati().getWp58ImpstBolloTotV().setWp58ImpstBolloTotV(new AfDecimal(0, 15, 3));
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58ImpstBolloTotR(new AfDecimal(0, 15, 3));
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsRiga(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsOperSql(Types.SPACE_CHAR);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsVer(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsTsIniCptz(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsTsEndCptz(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsUtente("");
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsStatoElab(Types.SPACE_CHAR);
        }
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }

    public void initImpstBollo() {
        ws.getImpstBollo().setP58IdImpstBollo(0);
        ws.getImpstBollo().setP58CodCompAnia(0);
        ws.getImpstBollo().setP58IdPoli(0);
        ws.getImpstBollo().setP58IbPoli("");
        ws.getImpstBollo().setP58CodFisc("");
        ws.getImpstBollo().setP58CodPartIva("");
        ws.getImpstBollo().setP58IdRappAna(0);
        ws.getImpstBollo().setP58IdMoviCrz(0);
        ws.getImpstBollo().getP58IdMoviChiu().setP58IdMoviChiu(0);
        ws.getImpstBollo().setP58DtIniEff(0);
        ws.getImpstBollo().setP58DtEndEff(0);
        ws.getImpstBollo().setP58DtIniCalc(0);
        ws.getImpstBollo().setP58DtEndCalc(0);
        ws.getImpstBollo().setP58TpCausBollo("");
        ws.getImpstBollo().setP58ImpstBolloDettC(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().getP58ImpstBolloDettV().setP58ImpstBolloDettV(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().getP58ImpstBolloTotV().setP58ImpstBolloTotV(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().setP58ImpstBolloTotR(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().setP58DsRiga(0);
        ws.getImpstBollo().setP58DsOperSql(Types.SPACE_CHAR);
        ws.getImpstBollo().setP58DsVer(0);
        ws.getImpstBollo().setP58DsTsIniCptz(0);
        ws.getImpstBollo().setP58DsTsEndCptz(0);
        ws.getImpstBollo().setP58DsUtente("");
        ws.getImpstBollo().setP58DsStatoElab(Types.SPACE_CHAR);
    }
}
