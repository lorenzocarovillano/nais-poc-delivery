package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-COD-SUBAGE<br>
 * Variable: W-B03-COD-SUBAGE from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CodSubageLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CodSubageLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_COD_SUBAGE;
    }

    public void setwB03CodSubage(int wB03CodSubage) {
        writeIntAsPacked(Pos.W_B03_COD_SUBAGE, wB03CodSubage, Len.Int.W_B03_COD_SUBAGE);
    }

    /**Original name: W-B03-COD-SUBAGE<br>*/
    public int getwB03CodSubage() {
        return readPackedAsInt(Pos.W_B03_COD_SUBAGE, Len.Int.W_B03_COD_SUBAGE);
    }

    public byte[] getwB03CodSubageAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_COD_SUBAGE, Pos.W_B03_COD_SUBAGE);
        return buffer;
    }

    public void setwB03CodSubageNull(String wB03CodSubageNull) {
        writeString(Pos.W_B03_COD_SUBAGE_NULL, wB03CodSubageNull, Len.W_B03_COD_SUBAGE_NULL);
    }

    /**Original name: W-B03-COD-SUBAGE-NULL<br>*/
    public String getwB03CodSubageNull() {
        return readString(Pos.W_B03_COD_SUBAGE_NULL, Len.W_B03_COD_SUBAGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_COD_SUBAGE = 1;
        public static final int W_B03_COD_SUBAGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_COD_SUBAGE = 3;
        public static final int W_B03_COD_SUBAGE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_COD_SUBAGE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
