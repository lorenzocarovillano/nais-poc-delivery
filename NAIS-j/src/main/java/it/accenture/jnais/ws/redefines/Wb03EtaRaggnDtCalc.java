package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-ETA-RAGGN-DT-CALC<br>
 * Variable: WB03-ETA-RAGGN-DT-CALC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03EtaRaggnDtCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03EtaRaggnDtCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_ETA_RAGGN_DT_CALC;
    }

    public void setWb03EtaRaggnDtCalc(AfDecimal wb03EtaRaggnDtCalc) {
        writeDecimalAsPacked(Pos.WB03_ETA_RAGGN_DT_CALC, wb03EtaRaggnDtCalc.copy());
    }

    public void setWb03EtaRaggnDtCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_ETA_RAGGN_DT_CALC, Pos.WB03_ETA_RAGGN_DT_CALC);
    }

    /**Original name: WB03-ETA-RAGGN-DT-CALC<br>*/
    public AfDecimal getWb03EtaRaggnDtCalc() {
        return readPackedAsDecimal(Pos.WB03_ETA_RAGGN_DT_CALC, Len.Int.WB03_ETA_RAGGN_DT_CALC, Len.Fract.WB03_ETA_RAGGN_DT_CALC);
    }

    public byte[] getWb03EtaRaggnDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_ETA_RAGGN_DT_CALC, Pos.WB03_ETA_RAGGN_DT_CALC);
        return buffer;
    }

    public void setWb03EtaRaggnDtCalcNull(String wb03EtaRaggnDtCalcNull) {
        writeString(Pos.WB03_ETA_RAGGN_DT_CALC_NULL, wb03EtaRaggnDtCalcNull, Len.WB03_ETA_RAGGN_DT_CALC_NULL);
    }

    /**Original name: WB03-ETA-RAGGN-DT-CALC-NULL<br>*/
    public String getWb03EtaRaggnDtCalcNull() {
        return readString(Pos.WB03_ETA_RAGGN_DT_CALC_NULL, Len.WB03_ETA_RAGGN_DT_CALC_NULL);
    }

    public String getWb03EtaRaggnDtCalcNullFormatted() {
        return Functions.padBlanks(getWb03EtaRaggnDtCalcNull(), Len.WB03_ETA_RAGGN_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_ETA_RAGGN_DT_CALC = 1;
        public static final int WB03_ETA_RAGGN_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ETA_RAGGN_DT_CALC = 4;
        public static final int WB03_ETA_RAGGN_DT_CALC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ETA_RAGGN_DT_CALC = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_ETA_RAGGN_DT_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
