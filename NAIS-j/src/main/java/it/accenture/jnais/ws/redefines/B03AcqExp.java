package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ACQ-EXP<br>
 * Variable: B03-ACQ-EXP from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03AcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03AcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ACQ_EXP;
    }

    public void setB03AcqExp(AfDecimal b03AcqExp) {
        writeDecimalAsPacked(Pos.B03_ACQ_EXP, b03AcqExp.copy());
    }

    public void setB03AcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ACQ_EXP, Pos.B03_ACQ_EXP);
    }

    /**Original name: B03-ACQ-EXP<br>*/
    public AfDecimal getB03AcqExp() {
        return readPackedAsDecimal(Pos.B03_ACQ_EXP, Len.Int.B03_ACQ_EXP, Len.Fract.B03_ACQ_EXP);
    }

    public byte[] getB03AcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ACQ_EXP, Pos.B03_ACQ_EXP);
        return buffer;
    }

    public void setB03AcqExpNull(String b03AcqExpNull) {
        writeString(Pos.B03_ACQ_EXP_NULL, b03AcqExpNull, Len.B03_ACQ_EXP_NULL);
    }

    /**Original name: B03-ACQ-EXP-NULL<br>*/
    public String getB03AcqExpNull() {
        return readString(Pos.B03_ACQ_EXP_NULL, Len.B03_ACQ_EXP_NULL);
    }

    public String getB03AcqExpNullFormatted() {
        return Functions.padBlanks(getB03AcqExpNull(), Len.B03_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ACQ_EXP = 1;
        public static final int B03_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ACQ_EXP = 8;
        public static final int B03_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
