package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-DT-SCAD-COP<br>
 * Variable: P67-DT-SCAD-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67DtScadCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67DtScadCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_DT_SCAD_COP;
    }

    public void setP67DtScadCop(int p67DtScadCop) {
        writeIntAsPacked(Pos.P67_DT_SCAD_COP, p67DtScadCop, Len.Int.P67_DT_SCAD_COP);
    }

    public void setP67DtScadCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_DT_SCAD_COP, Pos.P67_DT_SCAD_COP);
    }

    /**Original name: P67-DT-SCAD-COP<br>*/
    public int getP67DtScadCop() {
        return readPackedAsInt(Pos.P67_DT_SCAD_COP, Len.Int.P67_DT_SCAD_COP);
    }

    public byte[] getP67DtScadCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_DT_SCAD_COP, Pos.P67_DT_SCAD_COP);
        return buffer;
    }

    public void setP67DtScadCopNull(String p67DtScadCopNull) {
        writeString(Pos.P67_DT_SCAD_COP_NULL, p67DtScadCopNull, Len.P67_DT_SCAD_COP_NULL);
    }

    /**Original name: P67-DT-SCAD-COP-NULL<br>*/
    public String getP67DtScadCopNull() {
        return readString(Pos.P67_DT_SCAD_COP_NULL, Len.P67_DT_SCAD_COP_NULL);
    }

    public String getP67DtScadCopNullFormatted() {
        return Functions.padBlanks(getP67DtScadCopNull(), Len.P67_DT_SCAD_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_DT_SCAD_COP = 1;
        public static final int P67_DT_SCAD_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_DT_SCAD_COP = 5;
        public static final int P67_DT_SCAD_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_DT_SCAD_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
