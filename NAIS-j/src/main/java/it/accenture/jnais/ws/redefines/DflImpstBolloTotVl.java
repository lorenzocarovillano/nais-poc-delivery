package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-BOLLO-TOT-VL<br>
 * Variable: DFL-IMPST-BOLLO-TOT-VL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstBolloTotVl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstBolloTotVl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_BOLLO_TOT_VL;
    }

    public void setDflImpstBolloTotVl(AfDecimal dflImpstBolloTotVl) {
        writeDecimalAsPacked(Pos.DFL_IMPST_BOLLO_TOT_VL, dflImpstBolloTotVl.copy());
    }

    public void setDflImpstBolloTotVlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_BOLLO_TOT_VL, Pos.DFL_IMPST_BOLLO_TOT_VL);
    }

    /**Original name: DFL-IMPST-BOLLO-TOT-VL<br>*/
    public AfDecimal getDflImpstBolloTotVl() {
        return readPackedAsDecimal(Pos.DFL_IMPST_BOLLO_TOT_VL, Len.Int.DFL_IMPST_BOLLO_TOT_VL, Len.Fract.DFL_IMPST_BOLLO_TOT_VL);
    }

    public byte[] getDflImpstBolloTotVlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_BOLLO_TOT_VL, Pos.DFL_IMPST_BOLLO_TOT_VL);
        return buffer;
    }

    public void setDflImpstBolloTotVlNull(String dflImpstBolloTotVlNull) {
        writeString(Pos.DFL_IMPST_BOLLO_TOT_VL_NULL, dflImpstBolloTotVlNull, Len.DFL_IMPST_BOLLO_TOT_VL_NULL);
    }

    /**Original name: DFL-IMPST-BOLLO-TOT-VL-NULL<br>*/
    public String getDflImpstBolloTotVlNull() {
        return readString(Pos.DFL_IMPST_BOLLO_TOT_VL_NULL, Len.DFL_IMPST_BOLLO_TOT_VL_NULL);
    }

    public String getDflImpstBolloTotVlNullFormatted() {
        return Functions.padBlanks(getDflImpstBolloTotVlNull(), Len.DFL_IMPST_BOLLO_TOT_VL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_TOT_VL = 1;
        public static final int DFL_IMPST_BOLLO_TOT_VL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_TOT_VL = 8;
        public static final int DFL_IMPST_BOLLO_TOT_VL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_TOT_VL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_TOT_VL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
