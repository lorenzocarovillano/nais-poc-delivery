package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.B01DtQtzIni;
import it.accenture.jnais.ws.redefines.B01DtValzzQuoDtCa;
import it.accenture.jnais.ws.redefines.B01IdRichEstrazAgg;
import it.accenture.jnais.ws.redefines.B01NumQuoDtCalc;
import it.accenture.jnais.ws.redefines.B01NumQuoIni;
import it.accenture.jnais.ws.redefines.B01PcInvst;
import it.accenture.jnais.ws.redefines.B01ValQuoDtCalc;
import it.accenture.jnais.ws.redefines.B01ValQuoIni;
import it.accenture.jnais.ws.redefines.B01ValQuoT;

/**Original name: BILA-FND-ESTR<br>
 * Variable: BILA-FND-ESTR from copybook IDBVB011<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BilaFndEstr extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: B01-ID-BILA-FND-ESTR
    private int b01IdBilaFndEstr = DefaultValues.INT_VAL;
    //Original name: B01-ID-BILA-TRCH-ESTR
    private int b01IdBilaTrchEstr = DefaultValues.INT_VAL;
    //Original name: B01-COD-COMP-ANIA
    private int b01CodCompAnia = DefaultValues.INT_VAL;
    //Original name: B01-ID-RICH-ESTRAZ-MAS
    private int b01IdRichEstrazMas = DefaultValues.INT_VAL;
    //Original name: B01-ID-RICH-ESTRAZ-AGG
    private B01IdRichEstrazAgg b01IdRichEstrazAgg = new B01IdRichEstrazAgg();
    //Original name: B01-DT-RIS
    private int b01DtRis = DefaultValues.INT_VAL;
    //Original name: B01-ID-POLI
    private int b01IdPoli = DefaultValues.INT_VAL;
    //Original name: B01-ID-ADES
    private int b01IdAdes = DefaultValues.INT_VAL;
    //Original name: B01-ID-GAR
    private int b01IdGar = DefaultValues.INT_VAL;
    //Original name: B01-ID-TRCH-DI-GAR
    private int b01IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: B01-COD-FND
    private String b01CodFnd = DefaultValues.stringVal(Len.B01_COD_FND);
    //Original name: B01-DT-QTZ-INI
    private B01DtQtzIni b01DtQtzIni = new B01DtQtzIni();
    //Original name: B01-NUM-QUO-INI
    private B01NumQuoIni b01NumQuoIni = new B01NumQuoIni();
    //Original name: B01-NUM-QUO-DT-CALC
    private B01NumQuoDtCalc b01NumQuoDtCalc = new B01NumQuoDtCalc();
    //Original name: B01-VAL-QUO-INI
    private B01ValQuoIni b01ValQuoIni = new B01ValQuoIni();
    //Original name: B01-DT-VALZZ-QUO-DT-CA
    private B01DtValzzQuoDtCa b01DtValzzQuoDtCa = new B01DtValzzQuoDtCa();
    //Original name: B01-VAL-QUO-DT-CALC
    private B01ValQuoDtCalc b01ValQuoDtCalc = new B01ValQuoDtCalc();
    //Original name: B01-VAL-QUO-T
    private B01ValQuoT b01ValQuoT = new B01ValQuoT();
    //Original name: B01-PC-INVST
    private B01PcInvst b01PcInvst = new B01PcInvst();
    //Original name: B01-DS-OPER-SQL
    private char b01DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: B01-DS-VER
    private int b01DsVer = DefaultValues.INT_VAL;
    //Original name: B01-DS-TS-CPTZ
    private long b01DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: B01-DS-UTENTE
    private String b01DsUtente = DefaultValues.stringVal(Len.B01_DS_UTENTE);
    //Original name: B01-DS-STATO-ELAB
    private char b01DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BILA_FND_ESTR;
    }

    @Override
    public void deserialize(byte[] buf) {
        setBilaFndEstrBytes(buf);
    }

    public void setBilaFndEstrBytes(byte[] buffer) {
        setBilaFndEstrBytes(buffer, 1);
    }

    public byte[] getBilaFndEstrBytes() {
        byte[] buffer = new byte[Len.BILA_FND_ESTR];
        return getBilaFndEstrBytes(buffer, 1);
    }

    public void setBilaFndEstrBytes(byte[] buffer, int offset) {
        int position = offset;
        b01IdBilaFndEstr = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B01_ID_BILA_FND_ESTR, 0);
        position += Len.B01_ID_BILA_FND_ESTR;
        b01IdBilaTrchEstr = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B01_ID_BILA_TRCH_ESTR, 0);
        position += Len.B01_ID_BILA_TRCH_ESTR;
        b01CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B01_COD_COMP_ANIA, 0);
        position += Len.B01_COD_COMP_ANIA;
        b01IdRichEstrazMas = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B01_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.B01_ID_RICH_ESTRAZ_MAS;
        b01IdRichEstrazAgg.setB01IdRichEstrazAggFromBuffer(buffer, position);
        position += B01IdRichEstrazAgg.Len.B01_ID_RICH_ESTRAZ_AGG;
        b01DtRis = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B01_DT_RIS, 0);
        position += Len.B01_DT_RIS;
        b01IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B01_ID_POLI, 0);
        position += Len.B01_ID_POLI;
        b01IdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B01_ID_ADES, 0);
        position += Len.B01_ID_ADES;
        b01IdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B01_ID_GAR, 0);
        position += Len.B01_ID_GAR;
        b01IdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B01_ID_TRCH_DI_GAR, 0);
        position += Len.B01_ID_TRCH_DI_GAR;
        b01CodFnd = MarshalByte.readString(buffer, position, Len.B01_COD_FND);
        position += Len.B01_COD_FND;
        b01DtQtzIni.setB01DtQtzIniFromBuffer(buffer, position);
        position += B01DtQtzIni.Len.B01_DT_QTZ_INI;
        b01NumQuoIni.setB01NumQuoIniFromBuffer(buffer, position);
        position += B01NumQuoIni.Len.B01_NUM_QUO_INI;
        b01NumQuoDtCalc.setB01NumQuoDtCalcFromBuffer(buffer, position);
        position += B01NumQuoDtCalc.Len.B01_NUM_QUO_DT_CALC;
        b01ValQuoIni.setB01ValQuoIniFromBuffer(buffer, position);
        position += B01ValQuoIni.Len.B01_VAL_QUO_INI;
        b01DtValzzQuoDtCa.setB01DtValzzQuoDtCaFromBuffer(buffer, position);
        position += B01DtValzzQuoDtCa.Len.B01_DT_VALZZ_QUO_DT_CA;
        b01ValQuoDtCalc.setB01ValQuoDtCalcFromBuffer(buffer, position);
        position += B01ValQuoDtCalc.Len.B01_VAL_QUO_DT_CALC;
        b01ValQuoT.setB01ValQuoTFromBuffer(buffer, position);
        position += B01ValQuoT.Len.B01_VAL_QUO_T;
        b01PcInvst.setB01PcInvstFromBuffer(buffer, position);
        position += B01PcInvst.Len.B01_PC_INVST;
        b01DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b01DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B01_DS_VER, 0);
        position += Len.B01_DS_VER;
        b01DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.B01_DS_TS_CPTZ, 0);
        position += Len.B01_DS_TS_CPTZ;
        b01DsUtente = MarshalByte.readString(buffer, position, Len.B01_DS_UTENTE);
        position += Len.B01_DS_UTENTE;
        b01DsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getBilaFndEstrBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, b01IdBilaFndEstr, Len.Int.B01_ID_BILA_FND_ESTR, 0);
        position += Len.B01_ID_BILA_FND_ESTR;
        MarshalByte.writeIntAsPacked(buffer, position, b01IdBilaTrchEstr, Len.Int.B01_ID_BILA_TRCH_ESTR, 0);
        position += Len.B01_ID_BILA_TRCH_ESTR;
        MarshalByte.writeIntAsPacked(buffer, position, b01CodCompAnia, Len.Int.B01_COD_COMP_ANIA, 0);
        position += Len.B01_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, b01IdRichEstrazMas, Len.Int.B01_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.B01_ID_RICH_ESTRAZ_MAS;
        b01IdRichEstrazAgg.getB01IdRichEstrazAggAsBuffer(buffer, position);
        position += B01IdRichEstrazAgg.Len.B01_ID_RICH_ESTRAZ_AGG;
        MarshalByte.writeIntAsPacked(buffer, position, b01DtRis, Len.Int.B01_DT_RIS, 0);
        position += Len.B01_DT_RIS;
        MarshalByte.writeIntAsPacked(buffer, position, b01IdPoli, Len.Int.B01_ID_POLI, 0);
        position += Len.B01_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, b01IdAdes, Len.Int.B01_ID_ADES, 0);
        position += Len.B01_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, b01IdGar, Len.Int.B01_ID_GAR, 0);
        position += Len.B01_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, b01IdTrchDiGar, Len.Int.B01_ID_TRCH_DI_GAR, 0);
        position += Len.B01_ID_TRCH_DI_GAR;
        MarshalByte.writeString(buffer, position, b01CodFnd, Len.B01_COD_FND);
        position += Len.B01_COD_FND;
        b01DtQtzIni.getB01DtQtzIniAsBuffer(buffer, position);
        position += B01DtQtzIni.Len.B01_DT_QTZ_INI;
        b01NumQuoIni.getB01NumQuoIniAsBuffer(buffer, position);
        position += B01NumQuoIni.Len.B01_NUM_QUO_INI;
        b01NumQuoDtCalc.getB01NumQuoDtCalcAsBuffer(buffer, position);
        position += B01NumQuoDtCalc.Len.B01_NUM_QUO_DT_CALC;
        b01ValQuoIni.getB01ValQuoIniAsBuffer(buffer, position);
        position += B01ValQuoIni.Len.B01_VAL_QUO_INI;
        b01DtValzzQuoDtCa.getB01DtValzzQuoDtCaAsBuffer(buffer, position);
        position += B01DtValzzQuoDtCa.Len.B01_DT_VALZZ_QUO_DT_CA;
        b01ValQuoDtCalc.getB01ValQuoDtCalcAsBuffer(buffer, position);
        position += B01ValQuoDtCalc.Len.B01_VAL_QUO_DT_CALC;
        b01ValQuoT.getB01ValQuoTAsBuffer(buffer, position);
        position += B01ValQuoT.Len.B01_VAL_QUO_T;
        b01PcInvst.getB01PcInvstAsBuffer(buffer, position);
        position += B01PcInvst.Len.B01_PC_INVST;
        MarshalByte.writeChar(buffer, position, b01DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, b01DsVer, Len.Int.B01_DS_VER, 0);
        position += Len.B01_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, b01DsTsCptz, Len.Int.B01_DS_TS_CPTZ, 0);
        position += Len.B01_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, b01DsUtente, Len.B01_DS_UTENTE);
        position += Len.B01_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, b01DsStatoElab);
        return buffer;
    }

    public void setB01IdBilaFndEstr(int b01IdBilaFndEstr) {
        this.b01IdBilaFndEstr = b01IdBilaFndEstr;
    }

    public int getB01IdBilaFndEstr() {
        return this.b01IdBilaFndEstr;
    }

    public void setB01IdBilaTrchEstr(int b01IdBilaTrchEstr) {
        this.b01IdBilaTrchEstr = b01IdBilaTrchEstr;
    }

    public int getB01IdBilaTrchEstr() {
        return this.b01IdBilaTrchEstr;
    }

    public void setB01CodCompAnia(int b01CodCompAnia) {
        this.b01CodCompAnia = b01CodCompAnia;
    }

    public int getB01CodCompAnia() {
        return this.b01CodCompAnia;
    }

    public void setB01IdRichEstrazMas(int b01IdRichEstrazMas) {
        this.b01IdRichEstrazMas = b01IdRichEstrazMas;
    }

    public int getB01IdRichEstrazMas() {
        return this.b01IdRichEstrazMas;
    }

    public void setB01DtRis(int b01DtRis) {
        this.b01DtRis = b01DtRis;
    }

    public int getB01DtRis() {
        return this.b01DtRis;
    }

    public void setB01IdPoli(int b01IdPoli) {
        this.b01IdPoli = b01IdPoli;
    }

    public int getB01IdPoli() {
        return this.b01IdPoli;
    }

    public void setB01IdAdes(int b01IdAdes) {
        this.b01IdAdes = b01IdAdes;
    }

    public int getB01IdAdes() {
        return this.b01IdAdes;
    }

    public void setB01IdGar(int b01IdGar) {
        this.b01IdGar = b01IdGar;
    }

    public int getB01IdGar() {
        return this.b01IdGar;
    }

    public void setB01IdTrchDiGar(int b01IdTrchDiGar) {
        this.b01IdTrchDiGar = b01IdTrchDiGar;
    }

    public int getB01IdTrchDiGar() {
        return this.b01IdTrchDiGar;
    }

    public void setB01CodFnd(String b01CodFnd) {
        this.b01CodFnd = Functions.subString(b01CodFnd, Len.B01_COD_FND);
    }

    public String getB01CodFnd() {
        return this.b01CodFnd;
    }

    public void setB01DsOperSql(char b01DsOperSql) {
        this.b01DsOperSql = b01DsOperSql;
    }

    public void setB01DsOperSqlFormatted(String b01DsOperSql) {
        setB01DsOperSql(Functions.charAt(b01DsOperSql, Types.CHAR_SIZE));
    }

    public char getB01DsOperSql() {
        return this.b01DsOperSql;
    }

    public void setB01DsVer(int b01DsVer) {
        this.b01DsVer = b01DsVer;
    }

    public int getB01DsVer() {
        return this.b01DsVer;
    }

    public void setB01DsTsCptz(long b01DsTsCptz) {
        this.b01DsTsCptz = b01DsTsCptz;
    }

    public long getB01DsTsCptz() {
        return this.b01DsTsCptz;
    }

    public void setB01DsUtente(String b01DsUtente) {
        this.b01DsUtente = Functions.subString(b01DsUtente, Len.B01_DS_UTENTE);
    }

    public String getB01DsUtente() {
        return this.b01DsUtente;
    }

    public void setB01DsStatoElab(char b01DsStatoElab) {
        this.b01DsStatoElab = b01DsStatoElab;
    }

    public void setB01DsStatoElabFormatted(String b01DsStatoElab) {
        setB01DsStatoElab(Functions.charAt(b01DsStatoElab, Types.CHAR_SIZE));
    }

    public char getB01DsStatoElab() {
        return this.b01DsStatoElab;
    }

    public B01DtQtzIni getB01DtQtzIni() {
        return b01DtQtzIni;
    }

    public B01DtValzzQuoDtCa getB01DtValzzQuoDtCa() {
        return b01DtValzzQuoDtCa;
    }

    public B01IdRichEstrazAgg getB01IdRichEstrazAgg() {
        return b01IdRichEstrazAgg;
    }

    public B01NumQuoDtCalc getB01NumQuoDtCalc() {
        return b01NumQuoDtCalc;
    }

    public B01NumQuoIni getB01NumQuoIni() {
        return b01NumQuoIni;
    }

    public B01PcInvst getB01PcInvst() {
        return b01PcInvst;
    }

    public B01ValQuoDtCalc getB01ValQuoDtCalc() {
        return b01ValQuoDtCalc;
    }

    public B01ValQuoIni getB01ValQuoIni() {
        return b01ValQuoIni;
    }

    public B01ValQuoT getB01ValQuoT() {
        return b01ValQuoT;
    }

    @Override
    public byte[] serialize() {
        return getBilaFndEstrBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int B01_ID_BILA_FND_ESTR = 5;
        public static final int B01_ID_BILA_TRCH_ESTR = 5;
        public static final int B01_COD_COMP_ANIA = 3;
        public static final int B01_ID_RICH_ESTRAZ_MAS = 5;
        public static final int B01_DT_RIS = 5;
        public static final int B01_ID_POLI = 5;
        public static final int B01_ID_ADES = 5;
        public static final int B01_ID_GAR = 5;
        public static final int B01_ID_TRCH_DI_GAR = 5;
        public static final int B01_COD_FND = 12;
        public static final int B01_DS_OPER_SQL = 1;
        public static final int B01_DS_VER = 5;
        public static final int B01_DS_TS_CPTZ = 10;
        public static final int B01_DS_UTENTE = 20;
        public static final int B01_DS_STATO_ELAB = 1;
        public static final int BILA_FND_ESTR = B01_ID_BILA_FND_ESTR + B01_ID_BILA_TRCH_ESTR + B01_COD_COMP_ANIA + B01_ID_RICH_ESTRAZ_MAS + B01IdRichEstrazAgg.Len.B01_ID_RICH_ESTRAZ_AGG + B01_DT_RIS + B01_ID_POLI + B01_ID_ADES + B01_ID_GAR + B01_ID_TRCH_DI_GAR + B01_COD_FND + B01DtQtzIni.Len.B01_DT_QTZ_INI + B01NumQuoIni.Len.B01_NUM_QUO_INI + B01NumQuoDtCalc.Len.B01_NUM_QUO_DT_CALC + B01ValQuoIni.Len.B01_VAL_QUO_INI + B01DtValzzQuoDtCa.Len.B01_DT_VALZZ_QUO_DT_CA + B01ValQuoDtCalc.Len.B01_VAL_QUO_DT_CALC + B01ValQuoT.Len.B01_VAL_QUO_T + B01PcInvst.Len.B01_PC_INVST + B01_DS_OPER_SQL + B01_DS_VER + B01_DS_TS_CPTZ + B01_DS_UTENTE + B01_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B01_ID_BILA_FND_ESTR = 9;
            public static final int B01_ID_BILA_TRCH_ESTR = 9;
            public static final int B01_COD_COMP_ANIA = 5;
            public static final int B01_ID_RICH_ESTRAZ_MAS = 9;
            public static final int B01_DT_RIS = 8;
            public static final int B01_ID_POLI = 9;
            public static final int B01_ID_ADES = 9;
            public static final int B01_ID_GAR = 9;
            public static final int B01_ID_TRCH_DI_GAR = 9;
            public static final int B01_DS_VER = 9;
            public static final int B01_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
