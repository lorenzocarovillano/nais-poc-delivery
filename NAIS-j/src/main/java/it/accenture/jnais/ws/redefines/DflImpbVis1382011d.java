package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-VIS-1382011D<br>
 * Variable: DFL-IMPB-VIS-1382011D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbVis1382011d extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbVis1382011d() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_VIS1382011D;
    }

    public void setDflImpbVis1382011d(AfDecimal dflImpbVis1382011d) {
        writeDecimalAsPacked(Pos.DFL_IMPB_VIS1382011D, dflImpbVis1382011d.copy());
    }

    public void setDflImpbVis1382011dFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_VIS1382011D, Pos.DFL_IMPB_VIS1382011D);
    }

    /**Original name: DFL-IMPB-VIS-1382011D<br>*/
    public AfDecimal getDflImpbVis1382011d() {
        return readPackedAsDecimal(Pos.DFL_IMPB_VIS1382011D, Len.Int.DFL_IMPB_VIS1382011D, Len.Fract.DFL_IMPB_VIS1382011D);
    }

    public byte[] getDflImpbVis1382011dAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_VIS1382011D, Pos.DFL_IMPB_VIS1382011D);
        return buffer;
    }

    public void setDflImpbVis1382011dNull(String dflImpbVis1382011dNull) {
        writeString(Pos.DFL_IMPB_VIS1382011D_NULL, dflImpbVis1382011dNull, Len.DFL_IMPB_VIS1382011D_NULL);
    }

    /**Original name: DFL-IMPB-VIS-1382011D-NULL<br>*/
    public String getDflImpbVis1382011dNull() {
        return readString(Pos.DFL_IMPB_VIS1382011D_NULL, Len.DFL_IMPB_VIS1382011D_NULL);
    }

    public String getDflImpbVis1382011dNullFormatted() {
        return Functions.padBlanks(getDflImpbVis1382011dNull(), Len.DFL_IMPB_VIS1382011D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS1382011D = 1;
        public static final int DFL_IMPB_VIS1382011D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS1382011D = 8;
        public static final int DFL_IMPB_VIS1382011D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS1382011D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS1382011D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
