package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ETA-AA-2O-ASSTO<br>
 * Variable: TGA-ETA-AA-2O-ASSTO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaEtaAa2oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaEtaAa2oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ETA_AA2O_ASSTO;
    }

    public void setTgaEtaAa2oAssto(short tgaEtaAa2oAssto) {
        writeShortAsPacked(Pos.TGA_ETA_AA2O_ASSTO, tgaEtaAa2oAssto, Len.Int.TGA_ETA_AA2O_ASSTO);
    }

    public void setTgaEtaAa2oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ETA_AA2O_ASSTO, Pos.TGA_ETA_AA2O_ASSTO);
    }

    /**Original name: TGA-ETA-AA-2O-ASSTO<br>*/
    public short getTgaEtaAa2oAssto() {
        return readPackedAsShort(Pos.TGA_ETA_AA2O_ASSTO, Len.Int.TGA_ETA_AA2O_ASSTO);
    }

    public byte[] getTgaEtaAa2oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ETA_AA2O_ASSTO, Pos.TGA_ETA_AA2O_ASSTO);
        return buffer;
    }

    public void setTgaEtaAa2oAsstoNull(String tgaEtaAa2oAsstoNull) {
        writeString(Pos.TGA_ETA_AA2O_ASSTO_NULL, tgaEtaAa2oAsstoNull, Len.TGA_ETA_AA2O_ASSTO_NULL);
    }

    /**Original name: TGA-ETA-AA-2O-ASSTO-NULL<br>*/
    public String getTgaEtaAa2oAsstoNull() {
        return readString(Pos.TGA_ETA_AA2O_ASSTO_NULL, Len.TGA_ETA_AA2O_ASSTO_NULL);
    }

    public String getTgaEtaAa2oAsstoNullFormatted() {
        return Functions.padBlanks(getTgaEtaAa2oAsstoNull(), Len.TGA_ETA_AA2O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ETA_AA2O_ASSTO = 1;
        public static final int TGA_ETA_AA2O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ETA_AA2O_ASSTO = 2;
        public static final int TGA_ETA_AA2O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ETA_AA2O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
