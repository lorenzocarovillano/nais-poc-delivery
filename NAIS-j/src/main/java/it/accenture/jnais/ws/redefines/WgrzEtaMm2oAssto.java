package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-ETA-MM-2O-ASSTO<br>
 * Variable: WGRZ-ETA-MM-2O-ASSTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzEtaMm2oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzEtaMm2oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_ETA_MM2O_ASSTO;
    }

    public void setWgrzEtaMm2oAssto(short wgrzEtaMm2oAssto) {
        writeShortAsPacked(Pos.WGRZ_ETA_MM2O_ASSTO, wgrzEtaMm2oAssto, Len.Int.WGRZ_ETA_MM2O_ASSTO);
    }

    public void setWgrzEtaMm2oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_ETA_MM2O_ASSTO, Pos.WGRZ_ETA_MM2O_ASSTO);
    }

    /**Original name: WGRZ-ETA-MM-2O-ASSTO<br>*/
    public short getWgrzEtaMm2oAssto() {
        return readPackedAsShort(Pos.WGRZ_ETA_MM2O_ASSTO, Len.Int.WGRZ_ETA_MM2O_ASSTO);
    }

    public byte[] getWgrzEtaMm2oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_ETA_MM2O_ASSTO, Pos.WGRZ_ETA_MM2O_ASSTO);
        return buffer;
    }

    public void initWgrzEtaMm2oAsstoSpaces() {
        fill(Pos.WGRZ_ETA_MM2O_ASSTO, Len.WGRZ_ETA_MM2O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWgrzEtaMm2oAsstoNull(String wgrzEtaMm2oAsstoNull) {
        writeString(Pos.WGRZ_ETA_MM2O_ASSTO_NULL, wgrzEtaMm2oAsstoNull, Len.WGRZ_ETA_MM2O_ASSTO_NULL);
    }

    /**Original name: WGRZ-ETA-MM-2O-ASSTO-NULL<br>*/
    public String getWgrzEtaMm2oAsstoNull() {
        return readString(Pos.WGRZ_ETA_MM2O_ASSTO_NULL, Len.WGRZ_ETA_MM2O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_ETA_MM2O_ASSTO = 1;
        public static final int WGRZ_ETA_MM2O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_ETA_MM2O_ASSTO = 2;
        public static final int WGRZ_ETA_MM2O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_ETA_MM2O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
