package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: AMGINF<br>
 * Variable: AMGINF from program LCCS0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Amginf {

    //==== PROPERTIES ====
    //Original name: AAAA-I
    private String aaaaI = DefaultValues.stringVal(Len.AAAA_I);
    //Original name: MM-I
    private short mmI = DefaultValues.SHORT_VAL;
    //Original name: GG-I
    private String ggI = DefaultValues.stringVal(Len.GG_I);

    //==== METHODS ====
    public void setAmginfBytes(byte[] buffer) {
        setAmginfBytes(buffer, 1);
    }

    public byte[] getAmginfBytes() {
        byte[] buffer = new byte[Len.AMGINF];
        return getAmginfBytes(buffer, 1);
    }

    public void setAmginfBytes(byte[] buffer, int offset) {
        int position = offset;
        aaaaI = MarshalByte.readFixedString(buffer, position, Len.AAAA_I);
        position += Len.AAAA_I;
        mmI = MarshalByte.readShort(buffer, position, Len.MM_I, SignType.NO_SIGN);
        position += Len.MM_I;
        ggI = MarshalByte.readFixedString(buffer, position, Len.GG_I);
    }

    public byte[] getAmginfBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aaaaI, Len.AAAA_I);
        position += Len.AAAA_I;
        MarshalByte.writeShort(buffer, position, mmI, Len.MM_I, SignType.NO_SIGN);
        position += Len.MM_I;
        MarshalByte.writeString(buffer, position, ggI, Len.GG_I);
        return buffer;
    }

    public void setAaaaI(short aaaaI) {
        this.aaaaI = NumericDisplay.asString(aaaaI, Len.AAAA_I);
    }

    public void setAaaaIFormatted(String aaaaI) {
        this.aaaaI = Trunc.toUnsignedNumeric(aaaaI, Len.AAAA_I);
    }

    public short getAaaaI() {
        return NumericDisplay.asShort(this.aaaaI);
    }

    public void setMmI(short mmI) {
        this.mmI = mmI;
    }

    public short getMmI() {
        return this.mmI;
    }

    public void setGgI(short ggI) {
        this.ggI = NumericDisplay.asString(ggI, Len.GG_I);
    }

    public void setGgIFormatted(String ggI) {
        this.ggI = Trunc.toUnsignedNumeric(ggI, Len.GG_I);
    }

    public short getGgI() {
        return NumericDisplay.asShort(this.ggI);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA_I = 4;
        public static final int GG_I = 2;
        public static final int MM_I = 2;
        public static final int AMGINF = AAAA_I + MM_I + GG_I;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
