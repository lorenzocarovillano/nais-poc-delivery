package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: DEC-TROVATO<br>
 * Variable: DEC-TROVATO from copybook IDSV0501<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class DecTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setDecTrovato(char decTrovato) {
        this.value = decTrovato;
    }

    public char getDecTrovato() {
        return this.value;
    }

    public boolean isDecTrovatoSi() {
        return value == SI;
    }

    public void setDecTrovatoSi() {
        value = SI;
    }

    public void setDecTrovatoNo() {
        value = NO;
    }
}
