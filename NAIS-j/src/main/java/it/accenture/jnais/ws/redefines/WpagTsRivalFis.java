package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-TS-RIVAL-FIS<br>
 * Variable: WPAG-TS-RIVAL-FIS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTsRivalFis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTsRivalFis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TS_RIVAL_FIS;
    }

    public void setWpagTsRivalFis(AfDecimal wpagTsRivalFis) {
        writeDecimalAsPacked(Pos.WPAG_TS_RIVAL_FIS, wpagTsRivalFis.copy());
    }

    public void setWpagTsRivalFisFormatted(String wpagTsRivalFis) {
        setWpagTsRivalFis(PicParser.display(new PicParams("S9(3)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_TS_RIVAL_FIS + Len.Fract.WPAG_TS_RIVAL_FIS, Len.Fract.WPAG_TS_RIVAL_FIS, wpagTsRivalFis));
    }

    public void setWpagTsRivalFisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TS_RIVAL_FIS, Pos.WPAG_TS_RIVAL_FIS);
    }

    /**Original name: WPAG-TS-RIVAL-FIS<br>*/
    public AfDecimal getWpagTsRivalFis() {
        return readPackedAsDecimal(Pos.WPAG_TS_RIVAL_FIS, Len.Int.WPAG_TS_RIVAL_FIS, Len.Fract.WPAG_TS_RIVAL_FIS);
    }

    public byte[] getWpagTsRivalFisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TS_RIVAL_FIS, Pos.WPAG_TS_RIVAL_FIS);
        return buffer;
    }

    public void initWpagTsRivalFisSpaces() {
        fill(Pos.WPAG_TS_RIVAL_FIS, Len.WPAG_TS_RIVAL_FIS, Types.SPACE_CHAR);
    }

    public void setWpagTsRivalFisNull(String wpagTsRivalFisNull) {
        writeString(Pos.WPAG_TS_RIVAL_FIS_NULL, wpagTsRivalFisNull, Len.WPAG_TS_RIVAL_FIS_NULL);
    }

    /**Original name: WPAG-TS-RIVAL-FIS-NULL<br>*/
    public String getWpagTsRivalFisNull() {
        return readString(Pos.WPAG_TS_RIVAL_FIS_NULL, Len.WPAG_TS_RIVAL_FIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TS_RIVAL_FIS = 1;
        public static final int WPAG_TS_RIVAL_FIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TS_RIVAL_FIS = 4;
        public static final int WPAG_TS_RIVAL_FIS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_TS_RIVAL_FIS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_TS_RIVAL_FIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
