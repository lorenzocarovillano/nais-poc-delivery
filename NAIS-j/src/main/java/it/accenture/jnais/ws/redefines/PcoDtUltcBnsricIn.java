package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-BNSRIC-IN<br>
 * Variable: PCO-DT-ULTC-BNSRIC-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcBnsricIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcBnsricIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_BNSRIC_IN;
    }

    public void setPcoDtUltcBnsricIn(int pcoDtUltcBnsricIn) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_BNSRIC_IN, pcoDtUltcBnsricIn, Len.Int.PCO_DT_ULTC_BNSRIC_IN);
    }

    public void setPcoDtUltcBnsricInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_BNSRIC_IN, Pos.PCO_DT_ULTC_BNSRIC_IN);
    }

    /**Original name: PCO-DT-ULTC-BNSRIC-IN<br>*/
    public int getPcoDtUltcBnsricIn() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_BNSRIC_IN, Len.Int.PCO_DT_ULTC_BNSRIC_IN);
    }

    public byte[] getPcoDtUltcBnsricInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_BNSRIC_IN, Pos.PCO_DT_ULTC_BNSRIC_IN);
        return buffer;
    }

    public void initPcoDtUltcBnsricInHighValues() {
        fill(Pos.PCO_DT_ULTC_BNSRIC_IN, Len.PCO_DT_ULTC_BNSRIC_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcBnsricInNull(String pcoDtUltcBnsricInNull) {
        writeString(Pos.PCO_DT_ULTC_BNSRIC_IN_NULL, pcoDtUltcBnsricInNull, Len.PCO_DT_ULTC_BNSRIC_IN_NULL);
    }

    /**Original name: PCO-DT-ULTC-BNSRIC-IN-NULL<br>*/
    public String getPcoDtUltcBnsricInNull() {
        return readString(Pos.PCO_DT_ULTC_BNSRIC_IN_NULL, Len.PCO_DT_ULTC_BNSRIC_IN_NULL);
    }

    public String getPcoDtUltcBnsricInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltcBnsricInNull(), Len.PCO_DT_ULTC_BNSRIC_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_BNSRIC_IN = 1;
        public static final int PCO_DT_ULTC_BNSRIC_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_BNSRIC_IN = 5;
        public static final int PCO_DT_ULTC_BNSRIC_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_BNSRIC_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
