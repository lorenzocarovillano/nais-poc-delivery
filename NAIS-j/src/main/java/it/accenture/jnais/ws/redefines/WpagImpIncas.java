package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-INCAS<br>
 * Variable: WPAG-IMP-INCAS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpIncas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpIncas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_INCAS;
    }

    public void setWpagImpIncas(AfDecimal wpagImpIncas) {
        writeDecimalAsPacked(Pos.WPAG_IMP_INCAS, wpagImpIncas.copy());
    }

    public void setWpagImpIncasFormatted(String wpagImpIncas) {
        setWpagImpIncas(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_INCAS + Len.Fract.WPAG_IMP_INCAS, Len.Fract.WPAG_IMP_INCAS, wpagImpIncas));
    }

    public void setWpagImpIncasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_INCAS, Pos.WPAG_IMP_INCAS);
    }

    /**Original name: WPAG-IMP-INCAS<br>*/
    public AfDecimal getWpagImpIncas() {
        return readPackedAsDecimal(Pos.WPAG_IMP_INCAS, Len.Int.WPAG_IMP_INCAS, Len.Fract.WPAG_IMP_INCAS);
    }

    public byte[] getWpagImpIncasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_INCAS, Pos.WPAG_IMP_INCAS);
        return buffer;
    }

    public void initWpagImpIncasSpaces() {
        fill(Pos.WPAG_IMP_INCAS, Len.WPAG_IMP_INCAS, Types.SPACE_CHAR);
    }

    public void setWpagImpIncasNull(String wpagImpIncasNull) {
        writeString(Pos.WPAG_IMP_INCAS_NULL, wpagImpIncasNull, Len.WPAG_IMP_INCAS_NULL);
    }

    /**Original name: WPAG-IMP-INCAS-NULL<br>*/
    public String getWpagImpIncasNull() {
        return readString(Pos.WPAG_IMP_INCAS_NULL, Len.WPAG_IMP_INCAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_INCAS = 1;
        public static final int WPAG_IMP_INCAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_INCAS = 8;
        public static final int WPAG_IMP_INCAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_INCAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_INCAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
