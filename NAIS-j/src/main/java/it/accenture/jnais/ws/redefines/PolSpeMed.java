package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-SPE-MED<br>
 * Variable: POL-SPE-MED from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolSpeMed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolSpeMed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_SPE_MED;
    }

    public void setPolSpeMed(AfDecimal polSpeMed) {
        writeDecimalAsPacked(Pos.POL_SPE_MED, polSpeMed.copy());
    }

    public void setPolSpeMedFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_SPE_MED, Pos.POL_SPE_MED);
    }

    /**Original name: POL-SPE-MED<br>*/
    public AfDecimal getPolSpeMed() {
        return readPackedAsDecimal(Pos.POL_SPE_MED, Len.Int.POL_SPE_MED, Len.Fract.POL_SPE_MED);
    }

    public byte[] getPolSpeMedAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_SPE_MED, Pos.POL_SPE_MED);
        return buffer;
    }

    public void setPolSpeMedNull(String polSpeMedNull) {
        writeString(Pos.POL_SPE_MED_NULL, polSpeMedNull, Len.POL_SPE_MED_NULL);
    }

    /**Original name: POL-SPE-MED-NULL<br>*/
    public String getPolSpeMedNull() {
        return readString(Pos.POL_SPE_MED_NULL, Len.POL_SPE_MED_NULL);
    }

    public String getPolSpeMedNullFormatted() {
        return Functions.padBlanks(getPolSpeMedNull(), Len.POL_SPE_MED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_SPE_MED = 1;
        public static final int POL_SPE_MED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_SPE_MED = 8;
        public static final int POL_SPE_MED_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_SPE_MED = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int POL_SPE_MED = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
