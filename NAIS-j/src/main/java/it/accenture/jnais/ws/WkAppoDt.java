package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-APPO-DT<br>
 * Variable: WK-APPO-DT from program LCCS0029<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkAppoDt {

    //==== PROPERTIES ====
    //Original name: WK-APPO-AAAA
    private String aaaa = DefaultValues.stringVal(Len.AAAA);
    //Original name: WK-APPO-MM
    private String mm = DefaultValues.stringVal(Len.MM);
    //Original name: WK-APPO-GG
    private String gg = DefaultValues.stringVal(Len.GG);

    //==== METHODS ====
    public void setWkAppoDtFormatted(String data) {
        byte[] buffer = new byte[Len.WK_APPO_DT];
        MarshalByte.writeString(buffer, 1, data, Len.WK_APPO_DT);
        setWkAppoDtBytes(buffer, 1);
    }

    public byte[] getWkAppoDtBytes() {
        byte[] buffer = new byte[Len.WK_APPO_DT];
        return getWkAppoDtBytes(buffer, 1);
    }

    public void setWkAppoDtBytes(byte[] buffer, int offset) {
        int position = offset;
        aaaa = MarshalByte.readFixedString(buffer, position, Len.AAAA);
        position += Len.AAAA;
        mm = MarshalByte.readFixedString(buffer, position, Len.MM);
        position += Len.MM;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
    }

    public byte[] getWkAppoDtBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aaaa, Len.AAAA);
        position += Len.AAAA;
        MarshalByte.writeString(buffer, position, mm, Len.MM);
        position += Len.MM;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        return buffer;
    }

    public String getAaaaFormatted() {
        return this.aaaa;
    }

    public String getMmFormatted() {
        return this.mm;
    }

    public void setGg(short gg) {
        this.gg = NumericDisplay.asString(gg, Len.GG);
    }

    public short getGg() {
        return NumericDisplay.asShort(this.gg);
    }

    public String getGgFormatted() {
        return this.gg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WK_APPO_DT = AAAA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
