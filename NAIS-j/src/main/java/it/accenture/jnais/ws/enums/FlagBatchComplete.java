package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-BATCH-COMPLETE<br>
 * Variable: FLAG-BATCH-COMPLETE from program IABS0130<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagBatchComplete {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_BATCH_COMPLETE);
    public static final String COMPLETE = "CO";
    public static final String NOT_COMPLETE = "NC";

    //==== METHODS ====
    public void setFlagBatchComplete(String flagBatchComplete) {
        this.value = Functions.subString(flagBatchComplete, Len.FLAG_BATCH_COMPLETE);
    }

    public String getFlagBatchComplete() {
        return this.value;
    }

    public boolean isBatchComplete() {
        return value.equals(COMPLETE);
    }

    public void setComplete() {
        value = COMPLETE;
    }

    public void setNotComplete() {
        value = NOT_COMPLETE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_BATCH_COMPLETE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
