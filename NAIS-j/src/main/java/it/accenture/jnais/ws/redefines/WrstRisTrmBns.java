package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-TRM-BNS<br>
 * Variable: WRST-RIS-TRM-BNS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisTrmBns extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisTrmBns() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_TRM_BNS;
    }

    public void setWrstRisTrmBns(AfDecimal wrstRisTrmBns) {
        writeDecimalAsPacked(Pos.WRST_RIS_TRM_BNS, wrstRisTrmBns.copy());
    }

    public void setWrstRisTrmBnsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_TRM_BNS, Pos.WRST_RIS_TRM_BNS);
    }

    /**Original name: WRST-RIS-TRM-BNS<br>*/
    public AfDecimal getWrstRisTrmBns() {
        return readPackedAsDecimal(Pos.WRST_RIS_TRM_BNS, Len.Int.WRST_RIS_TRM_BNS, Len.Fract.WRST_RIS_TRM_BNS);
    }

    public byte[] getWrstRisTrmBnsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_TRM_BNS, Pos.WRST_RIS_TRM_BNS);
        return buffer;
    }

    public void initWrstRisTrmBnsSpaces() {
        fill(Pos.WRST_RIS_TRM_BNS, Len.WRST_RIS_TRM_BNS, Types.SPACE_CHAR);
    }

    public void setWrstRisTrmBnsNull(String wrstRisTrmBnsNull) {
        writeString(Pos.WRST_RIS_TRM_BNS_NULL, wrstRisTrmBnsNull, Len.WRST_RIS_TRM_BNS_NULL);
    }

    /**Original name: WRST-RIS-TRM-BNS-NULL<br>*/
    public String getWrstRisTrmBnsNull() {
        return readString(Pos.WRST_RIS_TRM_BNS_NULL, Len.WRST_RIS_TRM_BNS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_TRM_BNS = 1;
        public static final int WRST_RIS_TRM_BNS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_TRM_BNS = 8;
        public static final int WRST_RIS_TRM_BNS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_TRM_BNS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_TRM_BNS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
