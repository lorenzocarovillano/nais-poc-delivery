package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCO-IMP-ARROT-PRE<br>
 * Variable: DCO-IMP-ARROT-PRE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DcoImpArrotPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DcoImpArrotPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DCO_IMP_ARROT_PRE;
    }

    public void setDcoImpArrotPre(AfDecimal dcoImpArrotPre) {
        writeDecimalAsPacked(Pos.DCO_IMP_ARROT_PRE, dcoImpArrotPre.copy());
    }

    public void setDcoImpArrotPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DCO_IMP_ARROT_PRE, Pos.DCO_IMP_ARROT_PRE);
    }

    /**Original name: DCO-IMP-ARROT-PRE<br>*/
    public AfDecimal getDcoImpArrotPre() {
        return readPackedAsDecimal(Pos.DCO_IMP_ARROT_PRE, Len.Int.DCO_IMP_ARROT_PRE, Len.Fract.DCO_IMP_ARROT_PRE);
    }

    public byte[] getDcoImpArrotPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DCO_IMP_ARROT_PRE, Pos.DCO_IMP_ARROT_PRE);
        return buffer;
    }

    public void setDcoImpArrotPreNull(String dcoImpArrotPreNull) {
        writeString(Pos.DCO_IMP_ARROT_PRE_NULL, dcoImpArrotPreNull, Len.DCO_IMP_ARROT_PRE_NULL);
    }

    /**Original name: DCO-IMP-ARROT-PRE-NULL<br>*/
    public String getDcoImpArrotPreNull() {
        return readString(Pos.DCO_IMP_ARROT_PRE_NULL, Len.DCO_IMP_ARROT_PRE_NULL);
    }

    public String getDcoImpArrotPreNullFormatted() {
        return Functions.padBlanks(getDcoImpArrotPreNull(), Len.DCO_IMP_ARROT_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DCO_IMP_ARROT_PRE = 1;
        public static final int DCO_IMP_ARROT_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_IMP_ARROT_PRE = 8;
        public static final int DCO_IMP_ARROT_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_IMP_ARROT_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DCO_IMP_ARROT_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
