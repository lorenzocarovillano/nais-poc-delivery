package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPOL-DIR-VERS-AGG<br>
 * Variable: WPOL-DIR-VERS-AGG from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDirVersAgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDirVersAgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DIR_VERS_AGG;
    }

    public void setWpolDirVersAgg(AfDecimal wpolDirVersAgg) {
        writeDecimalAsPacked(Pos.WPOL_DIR_VERS_AGG, wpolDirVersAgg.copy());
    }

    public void setWpolDirVersAggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DIR_VERS_AGG, Pos.WPOL_DIR_VERS_AGG);
    }

    /**Original name: WPOL-DIR-VERS-AGG<br>*/
    public AfDecimal getWpolDirVersAgg() {
        return readPackedAsDecimal(Pos.WPOL_DIR_VERS_AGG, Len.Int.WPOL_DIR_VERS_AGG, Len.Fract.WPOL_DIR_VERS_AGG);
    }

    public byte[] getWpolDirVersAggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DIR_VERS_AGG, Pos.WPOL_DIR_VERS_AGG);
        return buffer;
    }

    public void setWpolDirVersAggNull(String wpolDirVersAggNull) {
        writeString(Pos.WPOL_DIR_VERS_AGG_NULL, wpolDirVersAggNull, Len.WPOL_DIR_VERS_AGG_NULL);
    }

    /**Original name: WPOL-DIR-VERS-AGG-NULL<br>*/
    public String getWpolDirVersAggNull() {
        return readString(Pos.WPOL_DIR_VERS_AGG_NULL, Len.WPOL_DIR_VERS_AGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DIR_VERS_AGG = 1;
        public static final int WPOL_DIR_VERS_AGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DIR_VERS_AGG = 8;
        public static final int WPOL_DIR_VERS_AGG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPOL_DIR_VERS_AGG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DIR_VERS_AGG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
