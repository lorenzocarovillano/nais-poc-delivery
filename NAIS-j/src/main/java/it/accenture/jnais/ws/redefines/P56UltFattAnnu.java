package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-ULT-FATT-ANNU<br>
 * Variable: P56-ULT-FATT-ANNU from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56UltFattAnnu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56UltFattAnnu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_ULT_FATT_ANNU;
    }

    public void setP56UltFattAnnu(AfDecimal p56UltFattAnnu) {
        writeDecimalAsPacked(Pos.P56_ULT_FATT_ANNU, p56UltFattAnnu.copy());
    }

    public void setP56UltFattAnnuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_ULT_FATT_ANNU, Pos.P56_ULT_FATT_ANNU);
    }

    /**Original name: P56-ULT-FATT-ANNU<br>*/
    public AfDecimal getP56UltFattAnnu() {
        return readPackedAsDecimal(Pos.P56_ULT_FATT_ANNU, Len.Int.P56_ULT_FATT_ANNU, Len.Fract.P56_ULT_FATT_ANNU);
    }

    public byte[] getP56UltFattAnnuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_ULT_FATT_ANNU, Pos.P56_ULT_FATT_ANNU);
        return buffer;
    }

    public void setP56UltFattAnnuNull(String p56UltFattAnnuNull) {
        writeString(Pos.P56_ULT_FATT_ANNU_NULL, p56UltFattAnnuNull, Len.P56_ULT_FATT_ANNU_NULL);
    }

    /**Original name: P56-ULT-FATT-ANNU-NULL<br>*/
    public String getP56UltFattAnnuNull() {
        return readString(Pos.P56_ULT_FATT_ANNU_NULL, Len.P56_ULT_FATT_ANNU_NULL);
    }

    public String getP56UltFattAnnuNullFormatted() {
        return Functions.padBlanks(getP56UltFattAnnuNull(), Len.P56_ULT_FATT_ANNU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_ULT_FATT_ANNU = 1;
        public static final int P56_ULT_FATT_ANNU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_ULT_FATT_ANNU = 8;
        public static final int P56_ULT_FATT_ANNU_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_ULT_FATT_ANNU = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_ULT_FATT_ANNU = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
