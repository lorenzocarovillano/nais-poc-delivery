package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-ID-MOVI-CHIU<br>
 * Variable: P67-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_ID_MOVI_CHIU;
    }

    public void setP67IdMoviChiu(int p67IdMoviChiu) {
        writeIntAsPacked(Pos.P67_ID_MOVI_CHIU, p67IdMoviChiu, Len.Int.P67_ID_MOVI_CHIU);
    }

    public void setP67IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_ID_MOVI_CHIU, Pos.P67_ID_MOVI_CHIU);
    }

    /**Original name: P67-ID-MOVI-CHIU<br>*/
    public int getP67IdMoviChiu() {
        return readPackedAsInt(Pos.P67_ID_MOVI_CHIU, Len.Int.P67_ID_MOVI_CHIU);
    }

    public byte[] getP67IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_ID_MOVI_CHIU, Pos.P67_ID_MOVI_CHIU);
        return buffer;
    }

    public void setP67IdMoviChiuNull(String p67IdMoviChiuNull) {
        writeString(Pos.P67_ID_MOVI_CHIU_NULL, p67IdMoviChiuNull, Len.P67_ID_MOVI_CHIU_NULL);
    }

    /**Original name: P67-ID-MOVI-CHIU-NULL<br>*/
    public String getP67IdMoviChiuNull() {
        return readString(Pos.P67_ID_MOVI_CHIU_NULL, Len.P67_ID_MOVI_CHIU_NULL);
    }

    public String getP67IdMoviChiuNullFormatted() {
        return Functions.padBlanks(getP67IdMoviChiuNull(), Len.P67_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_ID_MOVI_CHIU = 1;
        public static final int P67_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_ID_MOVI_CHIU = 5;
        public static final int P67_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
