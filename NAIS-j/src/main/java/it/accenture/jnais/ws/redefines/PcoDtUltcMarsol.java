package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-MARSOL<br>
 * Variable: PCO-DT-ULTC-MARSOL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcMarsol extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcMarsol() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_MARSOL;
    }

    public void setPcoDtUltcMarsol(int pcoDtUltcMarsol) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_MARSOL, pcoDtUltcMarsol, Len.Int.PCO_DT_ULTC_MARSOL);
    }

    public void setPcoDtUltcMarsolFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_MARSOL, Pos.PCO_DT_ULTC_MARSOL);
    }

    /**Original name: PCO-DT-ULTC-MARSOL<br>*/
    public int getPcoDtUltcMarsol() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_MARSOL, Len.Int.PCO_DT_ULTC_MARSOL);
    }

    public byte[] getPcoDtUltcMarsolAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_MARSOL, Pos.PCO_DT_ULTC_MARSOL);
        return buffer;
    }

    public void initPcoDtUltcMarsolHighValues() {
        fill(Pos.PCO_DT_ULTC_MARSOL, Len.PCO_DT_ULTC_MARSOL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcMarsolNull(String pcoDtUltcMarsolNull) {
        writeString(Pos.PCO_DT_ULTC_MARSOL_NULL, pcoDtUltcMarsolNull, Len.PCO_DT_ULTC_MARSOL_NULL);
    }

    /**Original name: PCO-DT-ULTC-MARSOL-NULL<br>*/
    public String getPcoDtUltcMarsolNull() {
        return readString(Pos.PCO_DT_ULTC_MARSOL_NULL, Len.PCO_DT_ULTC_MARSOL_NULL);
    }

    public String getPcoDtUltcMarsolNullFormatted() {
        return Functions.padBlanks(getPcoDtUltcMarsolNull(), Len.PCO_DT_ULTC_MARSOL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_MARSOL = 1;
        public static final int PCO_DT_ULTC_MARSOL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_MARSOL = 5;
        public static final int PCO_DT_ULTC_MARSOL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_MARSOL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
