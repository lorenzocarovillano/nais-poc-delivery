package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-IMP-VOLO<br>
 * Variable: TIT-IMP-VOLO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_IMP_VOLO;
    }

    public void setTitImpVolo(AfDecimal titImpVolo) {
        writeDecimalAsPacked(Pos.TIT_IMP_VOLO, titImpVolo.copy());
    }

    public void setTitImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_IMP_VOLO, Pos.TIT_IMP_VOLO);
    }

    /**Original name: TIT-IMP-VOLO<br>*/
    public AfDecimal getTitImpVolo() {
        return readPackedAsDecimal(Pos.TIT_IMP_VOLO, Len.Int.TIT_IMP_VOLO, Len.Fract.TIT_IMP_VOLO);
    }

    public byte[] getTitImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_IMP_VOLO, Pos.TIT_IMP_VOLO);
        return buffer;
    }

    public void setTitImpVoloNull(String titImpVoloNull) {
        writeString(Pos.TIT_IMP_VOLO_NULL, titImpVoloNull, Len.TIT_IMP_VOLO_NULL);
    }

    /**Original name: TIT-IMP-VOLO-NULL<br>*/
    public String getTitImpVoloNull() {
        return readString(Pos.TIT_IMP_VOLO_NULL, Len.TIT_IMP_VOLO_NULL);
    }

    public String getTitImpVoloNullFormatted() {
        return Functions.padBlanks(getTitImpVoloNull(), Len.TIT_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_IMP_VOLO = 1;
        public static final int TIT_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_IMP_VOLO = 8;
        public static final int TIT_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
