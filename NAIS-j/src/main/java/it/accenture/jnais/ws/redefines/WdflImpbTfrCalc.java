package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-TFR-CALC<br>
 * Variable: WDFL-IMPB-TFR-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbTfrCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbTfrCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_TFR_CALC;
    }

    public void setWdflImpbTfrCalc(AfDecimal wdflImpbTfrCalc) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_TFR_CALC, wdflImpbTfrCalc.copy());
    }

    public void setWdflImpbTfrCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_TFR_CALC, Pos.WDFL_IMPB_TFR_CALC);
    }

    /**Original name: WDFL-IMPB-TFR-CALC<br>*/
    public AfDecimal getWdflImpbTfrCalc() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_TFR_CALC, Len.Int.WDFL_IMPB_TFR_CALC, Len.Fract.WDFL_IMPB_TFR_CALC);
    }

    public byte[] getWdflImpbTfrCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_TFR_CALC, Pos.WDFL_IMPB_TFR_CALC);
        return buffer;
    }

    public void setWdflImpbTfrCalcNull(String wdflImpbTfrCalcNull) {
        writeString(Pos.WDFL_IMPB_TFR_CALC_NULL, wdflImpbTfrCalcNull, Len.WDFL_IMPB_TFR_CALC_NULL);
    }

    /**Original name: WDFL-IMPB-TFR-CALC-NULL<br>*/
    public String getWdflImpbTfrCalcNull() {
        return readString(Pos.WDFL_IMPB_TFR_CALC_NULL, Len.WDFL_IMPB_TFR_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_TFR_CALC = 1;
        public static final int WDFL_IMPB_TFR_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_TFR_CALC = 8;
        public static final int WDFL_IMPB_TFR_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_TFR_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_TFR_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
