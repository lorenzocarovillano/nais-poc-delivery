package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-DT-DEC<br>
 * Variable: WK-DT-DEC from program LCCS0320<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkDtDec {

    //==== PROPERTIES ====
    //Original name: WK-DT-DEC-AAAA
    private String aaaa = "0000";
    //Original name: WK-DT-DEC-MM
    private String mm = "00";
    //Original name: WK-DT-DEC-GG
    private String gg = "00";

    //==== METHODS ====
    public void setWkDtDecFormatted(String data) {
        byte[] buffer = new byte[Len.WK_DT_DEC];
        MarshalByte.writeString(buffer, 1, data, Len.WK_DT_DEC);
        setWkDtDecBytes(buffer, 1);
    }

    public void setWkDtDecBytes(byte[] buffer, int offset) {
        int position = offset;
        aaaa = MarshalByte.readFixedString(buffer, position, Len.AAAA);
        position += Len.AAAA;
        mm = MarshalByte.readFixedString(buffer, position, Len.MM);
        position += Len.MM;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
    }

    public short getGg() {
        return NumericDisplay.asShort(this.gg);
    }

    public String getGgFormatted() {
        return this.gg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WK_DT_DEC = AAAA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
