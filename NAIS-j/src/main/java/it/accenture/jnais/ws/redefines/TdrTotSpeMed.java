package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-SPE-MED<br>
 * Variable: TDR-TOT-SPE-MED from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotSpeMed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotSpeMed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_SPE_MED;
    }

    public void setTdrTotSpeMed(AfDecimal tdrTotSpeMed) {
        writeDecimalAsPacked(Pos.TDR_TOT_SPE_MED, tdrTotSpeMed.copy());
    }

    public void setTdrTotSpeMedFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_SPE_MED, Pos.TDR_TOT_SPE_MED);
    }

    /**Original name: TDR-TOT-SPE-MED<br>*/
    public AfDecimal getTdrTotSpeMed() {
        return readPackedAsDecimal(Pos.TDR_TOT_SPE_MED, Len.Int.TDR_TOT_SPE_MED, Len.Fract.TDR_TOT_SPE_MED);
    }

    public byte[] getTdrTotSpeMedAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_SPE_MED, Pos.TDR_TOT_SPE_MED);
        return buffer;
    }

    public void setTdrTotSpeMedNull(String tdrTotSpeMedNull) {
        writeString(Pos.TDR_TOT_SPE_MED_NULL, tdrTotSpeMedNull, Len.TDR_TOT_SPE_MED_NULL);
    }

    /**Original name: TDR-TOT-SPE-MED-NULL<br>*/
    public String getTdrTotSpeMedNull() {
        return readString(Pos.TDR_TOT_SPE_MED_NULL, Len.TDR_TOT_SPE_MED_NULL);
    }

    public String getTdrTotSpeMedNullFormatted() {
        return Functions.padBlanks(getTdrTotSpeMedNull(), Len.TDR_TOT_SPE_MED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SPE_MED = 1;
        public static final int TDR_TOT_SPE_MED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SPE_MED = 8;
        public static final int TDR_TOT_SPE_MED_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SPE_MED = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SPE_MED = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
