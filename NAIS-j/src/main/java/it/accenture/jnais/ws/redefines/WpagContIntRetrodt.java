package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-INT-RETRODT<br>
 * Variable: WPAG-CONT-INT-RETRODT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContIntRetrodt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContIntRetrodt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_INT_RETRODT;
    }

    public void setWpagContIntRetrodt(AfDecimal wpagContIntRetrodt) {
        writeDecimalAsPacked(Pos.WPAG_CONT_INT_RETRODT, wpagContIntRetrodt.copy());
    }

    public void setWpagContIntRetrodtFormatted(String wpagContIntRetrodt) {
        setWpagContIntRetrodt(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_INT_RETRODT + Len.Fract.WPAG_CONT_INT_RETRODT, Len.Fract.WPAG_CONT_INT_RETRODT, wpagContIntRetrodt));
    }

    public void setWpagContIntRetrodtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_INT_RETRODT, Pos.WPAG_CONT_INT_RETRODT);
    }

    /**Original name: WPAG-CONT-INT-RETRODT<br>*/
    public AfDecimal getWpagContIntRetrodt() {
        return readPackedAsDecimal(Pos.WPAG_CONT_INT_RETRODT, Len.Int.WPAG_CONT_INT_RETRODT, Len.Fract.WPAG_CONT_INT_RETRODT);
    }

    public byte[] getWpagContIntRetrodtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_INT_RETRODT, Pos.WPAG_CONT_INT_RETRODT);
        return buffer;
    }

    public void initWpagContIntRetrodtSpaces() {
        fill(Pos.WPAG_CONT_INT_RETRODT, Len.WPAG_CONT_INT_RETRODT, Types.SPACE_CHAR);
    }

    public void setWpagContIntRetrodtNull(String wpagContIntRetrodtNull) {
        writeString(Pos.WPAG_CONT_INT_RETRODT_NULL, wpagContIntRetrodtNull, Len.WPAG_CONT_INT_RETRODT_NULL);
    }

    /**Original name: WPAG-CONT-INT-RETRODT-NULL<br>*/
    public String getWpagContIntRetrodtNull() {
        return readString(Pos.WPAG_CONT_INT_RETRODT_NULL, Len.WPAG_CONT_INT_RETRODT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_INT_RETRODT = 1;
        public static final int WPAG_CONT_INT_RETRODT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_INT_RETRODT = 8;
        public static final int WPAG_CONT_INT_RETRODT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_INT_RETRODT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_INT_RETRODT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
