package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PC-RIP-PRE<br>
 * Variable: WTGA-PC-RIP-PRE from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPcRipPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPcRipPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PC_RIP_PRE;
    }

    public void setWtgaPcRipPre(AfDecimal wtgaPcRipPre) {
        writeDecimalAsPacked(Pos.WTGA_PC_RIP_PRE, wtgaPcRipPre.copy());
    }

    public void setWtgaPcRipPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PC_RIP_PRE, Pos.WTGA_PC_RIP_PRE);
    }

    /**Original name: WTGA-PC-RIP-PRE<br>*/
    public AfDecimal getWtgaPcRipPre() {
        return readPackedAsDecimal(Pos.WTGA_PC_RIP_PRE, Len.Int.WTGA_PC_RIP_PRE, Len.Fract.WTGA_PC_RIP_PRE);
    }

    public byte[] getWtgaPcRipPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PC_RIP_PRE, Pos.WTGA_PC_RIP_PRE);
        return buffer;
    }

    public void initWtgaPcRipPreSpaces() {
        fill(Pos.WTGA_PC_RIP_PRE, Len.WTGA_PC_RIP_PRE, Types.SPACE_CHAR);
    }

    public void setWtgaPcRipPreNull(String wtgaPcRipPreNull) {
        writeString(Pos.WTGA_PC_RIP_PRE_NULL, wtgaPcRipPreNull, Len.WTGA_PC_RIP_PRE_NULL);
    }

    /**Original name: WTGA-PC-RIP-PRE-NULL<br>*/
    public String getWtgaPcRipPreNull() {
        return readString(Pos.WTGA_PC_RIP_PRE_NULL, Len.WTGA_PC_RIP_PRE_NULL);
    }

    public String getWtgaPcRipPreNullFormatted() {
        return Functions.padBlanks(getWtgaPcRipPreNull(), Len.WTGA_PC_RIP_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PC_RIP_PRE = 1;
        public static final int WTGA_PC_RIP_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PC_RIP_PRE = 4;
        public static final int WTGA_PC_RIP_PRE_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PC_RIP_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PC_RIP_PRE = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
