package it.accenture.jnais.ws;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IDettTitCont1;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndTitCont;
import it.accenture.jnais.copy.TitCont;
import it.accenture.jnais.copy.TitContDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS2970<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs2970Data implements IDettTitCont1 {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-TIT-CONT
    private IndTitCont indTitCont = new IndTitCont();
    //Original name: TIT-CONT-DB
    private TitContDb titContDb = new TitContDb();
    //Original name: TIT-CONT
    private TitCont titCont = new TitCont();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    @Override
    public AfDecimal getDtcAcqExp() {
        throw new FieldNotMappedException("dtcAcqExp");
    }

    @Override
    public void setDtcAcqExp(AfDecimal dtcAcqExp) {
        throw new FieldNotMappedException("dtcAcqExp");
    }

    @Override
    public AfDecimal getDtcAcqExpObj() {
        return getDtcAcqExp();
    }

    @Override
    public void setDtcAcqExpObj(AfDecimal dtcAcqExpObj) {
        setDtcAcqExp(new AfDecimal(dtcAcqExpObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcCarAcq() {
        throw new FieldNotMappedException("dtcCarAcq");
    }

    @Override
    public void setDtcCarAcq(AfDecimal dtcCarAcq) {
        throw new FieldNotMappedException("dtcCarAcq");
    }

    @Override
    public AfDecimal getDtcCarAcqObj() {
        return getDtcCarAcq();
    }

    @Override
    public void setDtcCarAcqObj(AfDecimal dtcCarAcqObj) {
        setDtcCarAcq(new AfDecimal(dtcCarAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcCarGest() {
        throw new FieldNotMappedException("dtcCarGest");
    }

    @Override
    public void setDtcCarGest(AfDecimal dtcCarGest) {
        throw new FieldNotMappedException("dtcCarGest");
    }

    @Override
    public AfDecimal getDtcCarGestObj() {
        return getDtcCarGest();
    }

    @Override
    public void setDtcCarGestObj(AfDecimal dtcCarGestObj) {
        setDtcCarGest(new AfDecimal(dtcCarGestObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcCarIas() {
        throw new FieldNotMappedException("dtcCarIas");
    }

    @Override
    public void setDtcCarIas(AfDecimal dtcCarIas) {
        throw new FieldNotMappedException("dtcCarIas");
    }

    @Override
    public AfDecimal getDtcCarIasObj() {
        return getDtcCarIas();
    }

    @Override
    public void setDtcCarIasObj(AfDecimal dtcCarIasObj) {
        setDtcCarIas(new AfDecimal(dtcCarIasObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcCarInc() {
        throw new FieldNotMappedException("dtcCarInc");
    }

    @Override
    public void setDtcCarInc(AfDecimal dtcCarInc) {
        throw new FieldNotMappedException("dtcCarInc");
    }

    @Override
    public AfDecimal getDtcCarIncObj() {
        return getDtcCarInc();
    }

    @Override
    public void setDtcCarIncObj(AfDecimal dtcCarIncObj) {
        setDtcCarInc(new AfDecimal(dtcCarIncObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcCnbtAntirac() {
        throw new FieldNotMappedException("dtcCnbtAntirac");
    }

    @Override
    public void setDtcCnbtAntirac(AfDecimal dtcCnbtAntirac) {
        throw new FieldNotMappedException("dtcCnbtAntirac");
    }

    @Override
    public AfDecimal getDtcCnbtAntiracObj() {
        return getDtcCnbtAntirac();
    }

    @Override
    public void setDtcCnbtAntiracObj(AfDecimal dtcCnbtAntiracObj) {
        setDtcCnbtAntirac(new AfDecimal(dtcCnbtAntiracObj, 15, 3));
    }

    @Override
    public int getDtcCodCompAnia() {
        throw new FieldNotMappedException("dtcCodCompAnia");
    }

    @Override
    public void setDtcCodCompAnia(int dtcCodCompAnia) {
        throw new FieldNotMappedException("dtcCodCompAnia");
    }

    @Override
    public String getDtcCodDvs() {
        throw new FieldNotMappedException("dtcCodDvs");
    }

    @Override
    public void setDtcCodDvs(String dtcCodDvs) {
        throw new FieldNotMappedException("dtcCodDvs");
    }

    @Override
    public String getDtcCodDvsObj() {
        return getDtcCodDvs();
    }

    @Override
    public void setDtcCodDvsObj(String dtcCodDvsObj) {
        setDtcCodDvs(dtcCodDvsObj);
    }

    @Override
    public String getDtcCodTari() {
        throw new FieldNotMappedException("dtcCodTari");
    }

    @Override
    public void setDtcCodTari(String dtcCodTari) {
        throw new FieldNotMappedException("dtcCodTari");
    }

    @Override
    public String getDtcCodTariObj() {
        return getDtcCodTari();
    }

    @Override
    public void setDtcCodTariObj(String dtcCodTariObj) {
        setDtcCodTari(dtcCodTariObj);
    }

    @Override
    public AfDecimal getDtcCommisInter() {
        throw new FieldNotMappedException("dtcCommisInter");
    }

    @Override
    public void setDtcCommisInter(AfDecimal dtcCommisInter) {
        throw new FieldNotMappedException("dtcCommisInter");
    }

    @Override
    public AfDecimal getDtcCommisInterObj() {
        return getDtcCommisInter();
    }

    @Override
    public void setDtcCommisInterObj(AfDecimal dtcCommisInterObj) {
        setDtcCommisInter(new AfDecimal(dtcCommisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcDir() {
        throw new FieldNotMappedException("dtcDir");
    }

    @Override
    public void setDtcDir(AfDecimal dtcDir) {
        throw new FieldNotMappedException("dtcDir");
    }

    @Override
    public AfDecimal getDtcDirObj() {
        return getDtcDir();
    }

    @Override
    public void setDtcDirObj(AfDecimal dtcDirObj) {
        setDtcDir(new AfDecimal(dtcDirObj, 15, 3));
    }

    @Override
    public char getDtcDsOperSql() {
        throw new FieldNotMappedException("dtcDsOperSql");
    }

    @Override
    public void setDtcDsOperSql(char dtcDsOperSql) {
        throw new FieldNotMappedException("dtcDsOperSql");
    }

    @Override
    public long getDtcDsRiga() {
        throw new FieldNotMappedException("dtcDsRiga");
    }

    @Override
    public void setDtcDsRiga(long dtcDsRiga) {
        throw new FieldNotMappedException("dtcDsRiga");
    }

    @Override
    public char getDtcDsStatoElab() {
        throw new FieldNotMappedException("dtcDsStatoElab");
    }

    @Override
    public void setDtcDsStatoElab(char dtcDsStatoElab) {
        throw new FieldNotMappedException("dtcDsStatoElab");
    }

    @Override
    public long getDtcDsTsEndCptz() {
        throw new FieldNotMappedException("dtcDsTsEndCptz");
    }

    @Override
    public void setDtcDsTsEndCptz(long dtcDsTsEndCptz) {
        throw new FieldNotMappedException("dtcDsTsEndCptz");
    }

    @Override
    public long getDtcDsTsIniCptz() {
        throw new FieldNotMappedException("dtcDsTsIniCptz");
    }

    @Override
    public void setDtcDsTsIniCptz(long dtcDsTsIniCptz) {
        throw new FieldNotMappedException("dtcDsTsIniCptz");
    }

    @Override
    public String getDtcDsUtente() {
        throw new FieldNotMappedException("dtcDsUtente");
    }

    @Override
    public void setDtcDsUtente(String dtcDsUtente) {
        throw new FieldNotMappedException("dtcDsUtente");
    }

    @Override
    public int getDtcDsVer() {
        throw new FieldNotMappedException("dtcDsVer");
    }

    @Override
    public void setDtcDsVer(int dtcDsVer) {
        throw new FieldNotMappedException("dtcDsVer");
    }

    @Override
    public String getDtcDtEndCopDb() {
        throw new FieldNotMappedException("dtcDtEndCopDb");
    }

    @Override
    public void setDtcDtEndCopDb(String dtcDtEndCopDb) {
        throw new FieldNotMappedException("dtcDtEndCopDb");
    }

    @Override
    public String getDtcDtEndCopDbObj() {
        return getDtcDtEndCopDb();
    }

    @Override
    public void setDtcDtEndCopDbObj(String dtcDtEndCopDbObj) {
        setDtcDtEndCopDb(dtcDtEndCopDbObj);
    }

    @Override
    public String getDtcDtEndEffDb() {
        throw new FieldNotMappedException("dtcDtEndEffDb");
    }

    @Override
    public void setDtcDtEndEffDb(String dtcDtEndEffDb) {
        throw new FieldNotMappedException("dtcDtEndEffDb");
    }

    @Override
    public String getDtcDtEsiTitDb() {
        throw new FieldNotMappedException("dtcDtEsiTitDb");
    }

    @Override
    public void setDtcDtEsiTitDb(String dtcDtEsiTitDb) {
        throw new FieldNotMappedException("dtcDtEsiTitDb");
    }

    @Override
    public String getDtcDtEsiTitDbObj() {
        return getDtcDtEsiTitDb();
    }

    @Override
    public void setDtcDtEsiTitDbObj(String dtcDtEsiTitDbObj) {
        setDtcDtEsiTitDb(dtcDtEsiTitDbObj);
    }

    @Override
    public String getDtcDtIniCopDb() {
        throw new FieldNotMappedException("dtcDtIniCopDb");
    }

    @Override
    public void setDtcDtIniCopDb(String dtcDtIniCopDb) {
        throw new FieldNotMappedException("dtcDtIniCopDb");
    }

    @Override
    public String getDtcDtIniCopDbObj() {
        return getDtcDtIniCopDb();
    }

    @Override
    public void setDtcDtIniCopDbObj(String dtcDtIniCopDbObj) {
        setDtcDtIniCopDb(dtcDtIniCopDbObj);
    }

    @Override
    public String getDtcDtIniEffDb() {
        throw new FieldNotMappedException("dtcDtIniEffDb");
    }

    @Override
    public void setDtcDtIniEffDb(String dtcDtIniEffDb) {
        throw new FieldNotMappedException("dtcDtIniEffDb");
    }

    @Override
    public int getDtcFrqMovi() {
        throw new FieldNotMappedException("dtcFrqMovi");
    }

    @Override
    public void setDtcFrqMovi(int dtcFrqMovi) {
        throw new FieldNotMappedException("dtcFrqMovi");
    }

    @Override
    public Integer getDtcFrqMoviObj() {
        return ((Integer)getDtcFrqMovi());
    }

    @Override
    public void setDtcFrqMoviObj(Integer dtcFrqMoviObj) {
        setDtcFrqMovi(((int)dtcFrqMoviObj));
    }

    @Override
    public int getDtcIdDettTitCont() {
        throw new FieldNotMappedException("dtcIdDettTitCont");
    }

    @Override
    public void setDtcIdDettTitCont(int dtcIdDettTitCont) {
        throw new FieldNotMappedException("dtcIdDettTitCont");
    }

    @Override
    public int getDtcIdMoviChiu() {
        throw new FieldNotMappedException("dtcIdMoviChiu");
    }

    @Override
    public void setDtcIdMoviChiu(int dtcIdMoviChiu) {
        throw new FieldNotMappedException("dtcIdMoviChiu");
    }

    @Override
    public Integer getDtcIdMoviChiuObj() {
        return ((Integer)getDtcIdMoviChiu());
    }

    @Override
    public void setDtcIdMoviChiuObj(Integer dtcIdMoviChiuObj) {
        setDtcIdMoviChiu(((int)dtcIdMoviChiuObj));
    }

    @Override
    public int getDtcIdMoviCrz() {
        throw new FieldNotMappedException("dtcIdMoviCrz");
    }

    @Override
    public void setDtcIdMoviCrz(int dtcIdMoviCrz) {
        throw new FieldNotMappedException("dtcIdMoviCrz");
    }

    @Override
    public int getDtcIdOgg() {
        throw new FieldNotMappedException("dtcIdOgg");
    }

    @Override
    public void setDtcIdOgg(int dtcIdOgg) {
        throw new FieldNotMappedException("dtcIdOgg");
    }

    @Override
    public int getDtcIdTitCont() {
        throw new FieldNotMappedException("dtcIdTitCont");
    }

    @Override
    public void setDtcIdTitCont(int dtcIdTitCont) {
        throw new FieldNotMappedException("dtcIdTitCont");
    }

    @Override
    public AfDecimal getDtcImpAder() {
        throw new FieldNotMappedException("dtcImpAder");
    }

    @Override
    public void setDtcImpAder(AfDecimal dtcImpAder) {
        throw new FieldNotMappedException("dtcImpAder");
    }

    @Override
    public AfDecimal getDtcImpAderObj() {
        return getDtcImpAder();
    }

    @Override
    public void setDtcImpAderObj(AfDecimal dtcImpAderObj) {
        setDtcImpAder(new AfDecimal(dtcImpAderObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcImpAz() {
        throw new FieldNotMappedException("dtcImpAz");
    }

    @Override
    public void setDtcImpAz(AfDecimal dtcImpAz) {
        throw new FieldNotMappedException("dtcImpAz");
    }

    @Override
    public AfDecimal getDtcImpAzObj() {
        return getDtcImpAz();
    }

    @Override
    public void setDtcImpAzObj(AfDecimal dtcImpAzObj) {
        setDtcImpAz(new AfDecimal(dtcImpAzObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcImpTfr() {
        throw new FieldNotMappedException("dtcImpTfr");
    }

    @Override
    public void setDtcImpTfr(AfDecimal dtcImpTfr) {
        throw new FieldNotMappedException("dtcImpTfr");
    }

    @Override
    public AfDecimal getDtcImpTfrObj() {
        return getDtcImpTfr();
    }

    @Override
    public void setDtcImpTfrObj(AfDecimal dtcImpTfrObj) {
        setDtcImpTfr(new AfDecimal(dtcImpTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcImpTfrStrc() {
        throw new FieldNotMappedException("dtcImpTfrStrc");
    }

    @Override
    public void setDtcImpTfrStrc(AfDecimal dtcImpTfrStrc) {
        throw new FieldNotMappedException("dtcImpTfrStrc");
    }

    @Override
    public AfDecimal getDtcImpTfrStrcObj() {
        return getDtcImpTfrStrc();
    }

    @Override
    public void setDtcImpTfrStrcObj(AfDecimal dtcImpTfrStrcObj) {
        setDtcImpTfrStrc(new AfDecimal(dtcImpTfrStrcObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcImpTrasfe() {
        throw new FieldNotMappedException("dtcImpTrasfe");
    }

    @Override
    public void setDtcImpTrasfe(AfDecimal dtcImpTrasfe) {
        throw new FieldNotMappedException("dtcImpTrasfe");
    }

    @Override
    public AfDecimal getDtcImpTrasfeObj() {
        return getDtcImpTrasfe();
    }

    @Override
    public void setDtcImpTrasfeObj(AfDecimal dtcImpTrasfeObj) {
        setDtcImpTrasfe(new AfDecimal(dtcImpTrasfeObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcImpVolo() {
        throw new FieldNotMappedException("dtcImpVolo");
    }

    @Override
    public void setDtcImpVolo(AfDecimal dtcImpVolo) {
        throw new FieldNotMappedException("dtcImpVolo");
    }

    @Override
    public AfDecimal getDtcImpVoloObj() {
        return getDtcImpVolo();
    }

    @Override
    public void setDtcImpVoloObj(AfDecimal dtcImpVoloObj) {
        setDtcImpVolo(new AfDecimal(dtcImpVoloObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcIntrFraz() {
        throw new FieldNotMappedException("dtcIntrFraz");
    }

    @Override
    public void setDtcIntrFraz(AfDecimal dtcIntrFraz) {
        throw new FieldNotMappedException("dtcIntrFraz");
    }

    @Override
    public AfDecimal getDtcIntrFrazObj() {
        return getDtcIntrFraz();
    }

    @Override
    public void setDtcIntrFrazObj(AfDecimal dtcIntrFrazObj) {
        setDtcIntrFraz(new AfDecimal(dtcIntrFrazObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcIntrMora() {
        throw new FieldNotMappedException("dtcIntrMora");
    }

    @Override
    public void setDtcIntrMora(AfDecimal dtcIntrMora) {
        throw new FieldNotMappedException("dtcIntrMora");
    }

    @Override
    public AfDecimal getDtcIntrMoraObj() {
        return getDtcIntrMora();
    }

    @Override
    public void setDtcIntrMoraObj(AfDecimal dtcIntrMoraObj) {
        setDtcIntrMora(new AfDecimal(dtcIntrMoraObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcIntrRetdt() {
        throw new FieldNotMappedException("dtcIntrRetdt");
    }

    @Override
    public void setDtcIntrRetdt(AfDecimal dtcIntrRetdt) {
        throw new FieldNotMappedException("dtcIntrRetdt");
    }

    @Override
    public AfDecimal getDtcIntrRetdtObj() {
        return getDtcIntrRetdt();
    }

    @Override
    public void setDtcIntrRetdtObj(AfDecimal dtcIntrRetdtObj) {
        setDtcIntrRetdt(new AfDecimal(dtcIntrRetdtObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcIntrRiat() {
        throw new FieldNotMappedException("dtcIntrRiat");
    }

    @Override
    public void setDtcIntrRiat(AfDecimal dtcIntrRiat) {
        throw new FieldNotMappedException("dtcIntrRiat");
    }

    @Override
    public AfDecimal getDtcIntrRiatObj() {
        return getDtcIntrRiat();
    }

    @Override
    public void setDtcIntrRiatObj(AfDecimal dtcIntrRiatObj) {
        setDtcIntrRiat(new AfDecimal(dtcIntrRiatObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcManfeeAntic() {
        throw new FieldNotMappedException("dtcManfeeAntic");
    }

    @Override
    public void setDtcManfeeAntic(AfDecimal dtcManfeeAntic) {
        throw new FieldNotMappedException("dtcManfeeAntic");
    }

    @Override
    public AfDecimal getDtcManfeeAnticObj() {
        return getDtcManfeeAntic();
    }

    @Override
    public void setDtcManfeeAnticObj(AfDecimal dtcManfeeAnticObj) {
        setDtcManfeeAntic(new AfDecimal(dtcManfeeAnticObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcManfeeRec() {
        throw new FieldNotMappedException("dtcManfeeRec");
    }

    @Override
    public void setDtcManfeeRec(AfDecimal dtcManfeeRec) {
        throw new FieldNotMappedException("dtcManfeeRec");
    }

    @Override
    public AfDecimal getDtcManfeeRecObj() {
        return getDtcManfeeRec();
    }

    @Override
    public void setDtcManfeeRecObj(AfDecimal dtcManfeeRecObj) {
        setDtcManfeeRec(new AfDecimal(dtcManfeeRecObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcManfeeRicor() {
        throw new FieldNotMappedException("dtcManfeeRicor");
    }

    @Override
    public void setDtcManfeeRicor(AfDecimal dtcManfeeRicor) {
        throw new FieldNotMappedException("dtcManfeeRicor");
    }

    @Override
    public AfDecimal getDtcManfeeRicorObj() {
        return getDtcManfeeRicor();
    }

    @Override
    public void setDtcManfeeRicorObj(AfDecimal dtcManfeeRicorObj) {
        setDtcManfeeRicor(new AfDecimal(dtcManfeeRicorObj, 15, 3));
    }

    @Override
    public int getDtcNumGgRitardoPag() {
        throw new FieldNotMappedException("dtcNumGgRitardoPag");
    }

    @Override
    public void setDtcNumGgRitardoPag(int dtcNumGgRitardoPag) {
        throw new FieldNotMappedException("dtcNumGgRitardoPag");
    }

    @Override
    public Integer getDtcNumGgRitardoPagObj() {
        return ((Integer)getDtcNumGgRitardoPag());
    }

    @Override
    public void setDtcNumGgRitardoPagObj(Integer dtcNumGgRitardoPagObj) {
        setDtcNumGgRitardoPag(((int)dtcNumGgRitardoPagObj));
    }

    @Override
    public int getDtcNumGgRival() {
        throw new FieldNotMappedException("dtcNumGgRival");
    }

    @Override
    public void setDtcNumGgRival(int dtcNumGgRival) {
        throw new FieldNotMappedException("dtcNumGgRival");
    }

    @Override
    public Integer getDtcNumGgRivalObj() {
        return ((Integer)getDtcNumGgRival());
    }

    @Override
    public void setDtcNumGgRivalObj(Integer dtcNumGgRivalObj) {
        setDtcNumGgRival(((int)dtcNumGgRivalObj));
    }

    @Override
    public AfDecimal getDtcPreNet() {
        throw new FieldNotMappedException("dtcPreNet");
    }

    @Override
    public void setDtcPreNet(AfDecimal dtcPreNet) {
        throw new FieldNotMappedException("dtcPreNet");
    }

    @Override
    public AfDecimal getDtcPreNetObj() {
        return getDtcPreNet();
    }

    @Override
    public void setDtcPreNetObj(AfDecimal dtcPreNetObj) {
        setDtcPreNet(new AfDecimal(dtcPreNetObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcPrePpIas() {
        throw new FieldNotMappedException("dtcPrePpIas");
    }

    @Override
    public void setDtcPrePpIas(AfDecimal dtcPrePpIas) {
        throw new FieldNotMappedException("dtcPrePpIas");
    }

    @Override
    public AfDecimal getDtcPrePpIasObj() {
        return getDtcPrePpIas();
    }

    @Override
    public void setDtcPrePpIasObj(AfDecimal dtcPrePpIasObj) {
        setDtcPrePpIas(new AfDecimal(dtcPrePpIasObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcPreSoloRsh() {
        throw new FieldNotMappedException("dtcPreSoloRsh");
    }

    @Override
    public void setDtcPreSoloRsh(AfDecimal dtcPreSoloRsh) {
        throw new FieldNotMappedException("dtcPreSoloRsh");
    }

    @Override
    public AfDecimal getDtcPreSoloRshObj() {
        return getDtcPreSoloRsh();
    }

    @Override
    public void setDtcPreSoloRshObj(AfDecimal dtcPreSoloRshObj) {
        setDtcPreSoloRsh(new AfDecimal(dtcPreSoloRshObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcPreTot() {
        throw new FieldNotMappedException("dtcPreTot");
    }

    @Override
    public void setDtcPreTot(AfDecimal dtcPreTot) {
        throw new FieldNotMappedException("dtcPreTot");
    }

    @Override
    public AfDecimal getDtcPreTotObj() {
        return getDtcPreTot();
    }

    @Override
    public void setDtcPreTotObj(AfDecimal dtcPreTotObj) {
        setDtcPreTot(new AfDecimal(dtcPreTotObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcProvAcq1aa() {
        throw new FieldNotMappedException("dtcProvAcq1aa");
    }

    @Override
    public void setDtcProvAcq1aa(AfDecimal dtcProvAcq1aa) {
        throw new FieldNotMappedException("dtcProvAcq1aa");
    }

    @Override
    public AfDecimal getDtcProvAcq1aaObj() {
        return getDtcProvAcq1aa();
    }

    @Override
    public void setDtcProvAcq1aaObj(AfDecimal dtcProvAcq1aaObj) {
        setDtcProvAcq1aa(new AfDecimal(dtcProvAcq1aaObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcProvAcq2aa() {
        throw new FieldNotMappedException("dtcProvAcq2aa");
    }

    @Override
    public void setDtcProvAcq2aa(AfDecimal dtcProvAcq2aa) {
        throw new FieldNotMappedException("dtcProvAcq2aa");
    }

    @Override
    public AfDecimal getDtcProvAcq2aaObj() {
        return getDtcProvAcq2aa();
    }

    @Override
    public void setDtcProvAcq2aaObj(AfDecimal dtcProvAcq2aaObj) {
        setDtcProvAcq2aa(new AfDecimal(dtcProvAcq2aaObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcProvDaRec() {
        throw new FieldNotMappedException("dtcProvDaRec");
    }

    @Override
    public void setDtcProvDaRec(AfDecimal dtcProvDaRec) {
        throw new FieldNotMappedException("dtcProvDaRec");
    }

    @Override
    public AfDecimal getDtcProvDaRecObj() {
        return getDtcProvDaRec();
    }

    @Override
    public void setDtcProvDaRecObj(AfDecimal dtcProvDaRecObj) {
        setDtcProvDaRec(new AfDecimal(dtcProvDaRecObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcProvInc() {
        throw new FieldNotMappedException("dtcProvInc");
    }

    @Override
    public void setDtcProvInc(AfDecimal dtcProvInc) {
        throw new FieldNotMappedException("dtcProvInc");
    }

    @Override
    public AfDecimal getDtcProvIncObj() {
        return getDtcProvInc();
    }

    @Override
    public void setDtcProvIncObj(AfDecimal dtcProvIncObj) {
        setDtcProvInc(new AfDecimal(dtcProvIncObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcProvRicor() {
        throw new FieldNotMappedException("dtcProvRicor");
    }

    @Override
    public void setDtcProvRicor(AfDecimal dtcProvRicor) {
        throw new FieldNotMappedException("dtcProvRicor");
    }

    @Override
    public AfDecimal getDtcProvRicorObj() {
        return getDtcProvRicor();
    }

    @Override
    public void setDtcProvRicorObj(AfDecimal dtcProvRicorObj) {
        setDtcProvRicor(new AfDecimal(dtcProvRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcRemunAss() {
        throw new FieldNotMappedException("dtcRemunAss");
    }

    @Override
    public void setDtcRemunAss(AfDecimal dtcRemunAss) {
        throw new FieldNotMappedException("dtcRemunAss");
    }

    @Override
    public AfDecimal getDtcRemunAssObj() {
        return getDtcRemunAss();
    }

    @Override
    public void setDtcRemunAssObj(AfDecimal dtcRemunAssObj) {
        setDtcRemunAss(new AfDecimal(dtcRemunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSoprAlt() {
        throw new FieldNotMappedException("dtcSoprAlt");
    }

    @Override
    public void setDtcSoprAlt(AfDecimal dtcSoprAlt) {
        throw new FieldNotMappedException("dtcSoprAlt");
    }

    @Override
    public AfDecimal getDtcSoprAltObj() {
        return getDtcSoprAlt();
    }

    @Override
    public void setDtcSoprAltObj(AfDecimal dtcSoprAltObj) {
        setDtcSoprAlt(new AfDecimal(dtcSoprAltObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSoprProf() {
        throw new FieldNotMappedException("dtcSoprProf");
    }

    @Override
    public void setDtcSoprProf(AfDecimal dtcSoprProf) {
        throw new FieldNotMappedException("dtcSoprProf");
    }

    @Override
    public AfDecimal getDtcSoprProfObj() {
        return getDtcSoprProf();
    }

    @Override
    public void setDtcSoprProfObj(AfDecimal dtcSoprProfObj) {
        setDtcSoprProf(new AfDecimal(dtcSoprProfObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSoprSan() {
        throw new FieldNotMappedException("dtcSoprSan");
    }

    @Override
    public void setDtcSoprSan(AfDecimal dtcSoprSan) {
        throw new FieldNotMappedException("dtcSoprSan");
    }

    @Override
    public AfDecimal getDtcSoprSanObj() {
        return getDtcSoprSan();
    }

    @Override
    public void setDtcSoprSanObj(AfDecimal dtcSoprSanObj) {
        setDtcSoprSan(new AfDecimal(dtcSoprSanObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSoprSpo() {
        throw new FieldNotMappedException("dtcSoprSpo");
    }

    @Override
    public void setDtcSoprSpo(AfDecimal dtcSoprSpo) {
        throw new FieldNotMappedException("dtcSoprSpo");
    }

    @Override
    public AfDecimal getDtcSoprSpoObj() {
        return getDtcSoprSpo();
    }

    @Override
    public void setDtcSoprSpoObj(AfDecimal dtcSoprSpoObj) {
        setDtcSoprSpo(new AfDecimal(dtcSoprSpoObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSoprTec() {
        throw new FieldNotMappedException("dtcSoprTec");
    }

    @Override
    public void setDtcSoprTec(AfDecimal dtcSoprTec) {
        throw new FieldNotMappedException("dtcSoprTec");
    }

    @Override
    public AfDecimal getDtcSoprTecObj() {
        return getDtcSoprTec();
    }

    @Override
    public void setDtcSoprTecObj(AfDecimal dtcSoprTecObj) {
        setDtcSoprTec(new AfDecimal(dtcSoprTecObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSpeAge() {
        throw new FieldNotMappedException("dtcSpeAge");
    }

    @Override
    public void setDtcSpeAge(AfDecimal dtcSpeAge) {
        throw new FieldNotMappedException("dtcSpeAge");
    }

    @Override
    public AfDecimal getDtcSpeAgeObj() {
        return getDtcSpeAge();
    }

    @Override
    public void setDtcSpeAgeObj(AfDecimal dtcSpeAgeObj) {
        setDtcSpeAge(new AfDecimal(dtcSpeAgeObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSpeMed() {
        throw new FieldNotMappedException("dtcSpeMed");
    }

    @Override
    public void setDtcSpeMed(AfDecimal dtcSpeMed) {
        throw new FieldNotMappedException("dtcSpeMed");
    }

    @Override
    public AfDecimal getDtcSpeMedObj() {
        return getDtcSpeMed();
    }

    @Override
    public void setDtcSpeMedObj(AfDecimal dtcSpeMedObj) {
        setDtcSpeMed(new AfDecimal(dtcSpeMedObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcTax() {
        throw new FieldNotMappedException("dtcTax");
    }

    @Override
    public void setDtcTax(AfDecimal dtcTax) {
        throw new FieldNotMappedException("dtcTax");
    }

    @Override
    public AfDecimal getDtcTaxObj() {
        return getDtcTax();
    }

    @Override
    public void setDtcTaxObj(AfDecimal dtcTaxObj) {
        setDtcTax(new AfDecimal(dtcTaxObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcTotIntrPrest() {
        throw new FieldNotMappedException("dtcTotIntrPrest");
    }

    @Override
    public void setDtcTotIntrPrest(AfDecimal dtcTotIntrPrest) {
        throw new FieldNotMappedException("dtcTotIntrPrest");
    }

    @Override
    public AfDecimal getDtcTotIntrPrestObj() {
        return getDtcTotIntrPrest();
    }

    @Override
    public void setDtcTotIntrPrestObj(AfDecimal dtcTotIntrPrestObj) {
        setDtcTotIntrPrest(new AfDecimal(dtcTotIntrPrestObj, 15, 3));
    }

    @Override
    public String getDtcTpOgg() {
        throw new FieldNotMappedException("dtcTpOgg");
    }

    @Override
    public void setDtcTpOgg(String dtcTpOgg) {
        throw new FieldNotMappedException("dtcTpOgg");
    }

    @Override
    public String getDtcTpRgmFisc() {
        throw new FieldNotMappedException("dtcTpRgmFisc");
    }

    @Override
    public void setDtcTpRgmFisc(String dtcTpRgmFisc) {
        throw new FieldNotMappedException("dtcTpRgmFisc");
    }

    @Override
    public String getDtcTpStatTit() {
        throw new FieldNotMappedException("dtcTpStatTit");
    }

    @Override
    public void setDtcTpStatTit(String dtcTpStatTit) {
        throw new FieldNotMappedException("dtcTpStatTit");
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndTitCont getIndTitCont() {
        return indTitCont;
    }

    @Override
    public int getLdbv0371IdOgg() {
        throw new FieldNotMappedException("ldbv0371IdOgg");
    }

    @Override
    public void setLdbv0371IdOgg(int ldbv0371IdOgg) {
        throw new FieldNotMappedException("ldbv0371IdOgg");
    }

    @Override
    public String getLdbv0371TpOgg() {
        throw new FieldNotMappedException("ldbv0371TpOgg");
    }

    @Override
    public void setLdbv0371TpOgg(String ldbv0371TpOgg) {
        throw new FieldNotMappedException("ldbv0371TpOgg");
    }

    @Override
    public String getLdbv0371TpPreTit() {
        throw new FieldNotMappedException("ldbv0371TpPreTit");
    }

    @Override
    public void setLdbv0371TpPreTit(String ldbv0371TpPreTit) {
        throw new FieldNotMappedException("ldbv0371TpPreTit");
    }

    @Override
    public String getLdbv0371TpStatTit1() {
        throw new FieldNotMappedException("ldbv0371TpStatTit1");
    }

    @Override
    public void setLdbv0371TpStatTit1(String ldbv0371TpStatTit1) {
        throw new FieldNotMappedException("ldbv0371TpStatTit1");
    }

    @Override
    public String getLdbv0371TpStatTit2() {
        throw new FieldNotMappedException("ldbv0371TpStatTit2");
    }

    @Override
    public void setLdbv0371TpStatTit2(String ldbv0371TpStatTit2) {
        throw new FieldNotMappedException("ldbv0371TpStatTit2");
    }

    @Override
    public int getLdbv2971IdOgg() {
        throw new FieldNotMappedException("ldbv2971IdOgg");
    }

    @Override
    public void setLdbv2971IdOgg(int ldbv2971IdOgg) {
        throw new FieldNotMappedException("ldbv2971IdOgg");
    }

    @Override
    public String getLdbv2971TpOgg() {
        throw new FieldNotMappedException("ldbv2971TpOgg");
    }

    @Override
    public void setLdbv2971TpOgg(String ldbv2971TpOgg) {
        throw new FieldNotMappedException("ldbv2971TpOgg");
    }

    @Override
    public String getLdbv3101DtADb() {
        throw new FieldNotMappedException("ldbv3101DtADb");
    }

    @Override
    public void setLdbv3101DtADb(String ldbv3101DtADb) {
        throw new FieldNotMappedException("ldbv3101DtADb");
    }

    @Override
    public String getLdbv3101DtDaDb() {
        throw new FieldNotMappedException("ldbv3101DtDaDb");
    }

    @Override
    public void setLdbv3101DtDaDb(String ldbv3101DtDaDb) {
        throw new FieldNotMappedException("ldbv3101DtDaDb");
    }

    @Override
    public int getLdbv3101IdOgg() {
        throw new FieldNotMappedException("ldbv3101IdOgg");
    }

    @Override
    public void setLdbv3101IdOgg(int ldbv3101IdOgg) {
        throw new FieldNotMappedException("ldbv3101IdOgg");
    }

    @Override
    public String getLdbv3101TpOgg() {
        throw new FieldNotMappedException("ldbv3101TpOgg");
    }

    @Override
    public void setLdbv3101TpOgg(String ldbv3101TpOgg) {
        throw new FieldNotMappedException("ldbv3101TpOgg");
    }

    @Override
    public String getLdbv3101TpStaTit() {
        throw new FieldNotMappedException("ldbv3101TpStaTit");
    }

    @Override
    public void setLdbv3101TpStaTit(String ldbv3101TpStaTit) {
        throw new FieldNotMappedException("ldbv3101TpStaTit");
    }

    @Override
    public String getLdbv3101TpTit() {
        throw new FieldNotMappedException("ldbv3101TpTit");
    }

    @Override
    public void setLdbv3101TpTit(String ldbv3101TpTit) {
        throw new FieldNotMappedException("ldbv3101TpTit");
    }

    @Override
    public String getLdbv7141DtADb() {
        throw new FieldNotMappedException("ldbv7141DtADb");
    }

    @Override
    public void setLdbv7141DtADb(String ldbv7141DtADb) {
        throw new FieldNotMappedException("ldbv7141DtADb");
    }

    @Override
    public String getLdbv7141DtDaDb() {
        throw new FieldNotMappedException("ldbv7141DtDaDb");
    }

    @Override
    public void setLdbv7141DtDaDb(String ldbv7141DtDaDb) {
        throw new FieldNotMappedException("ldbv7141DtDaDb");
    }

    @Override
    public int getLdbv7141IdOgg() {
        throw new FieldNotMappedException("ldbv7141IdOgg");
    }

    @Override
    public void setLdbv7141IdOgg(int ldbv7141IdOgg) {
        throw new FieldNotMappedException("ldbv7141IdOgg");
    }

    @Override
    public String getLdbv7141TpOgg() {
        throw new FieldNotMappedException("ldbv7141TpOgg");
    }

    @Override
    public void setLdbv7141TpOgg(String ldbv7141TpOgg) {
        throw new FieldNotMappedException("ldbv7141TpOgg");
    }

    @Override
    public String getLdbv7141TpStaTit1() {
        throw new FieldNotMappedException("ldbv7141TpStaTit1");
    }

    @Override
    public void setLdbv7141TpStaTit1(String ldbv7141TpStaTit1) {
        throw new FieldNotMappedException("ldbv7141TpStaTit1");
    }

    @Override
    public String getLdbv7141TpStaTit2() {
        throw new FieldNotMappedException("ldbv7141TpStaTit2");
    }

    @Override
    public void setLdbv7141TpStaTit2(String ldbv7141TpStaTit2) {
        throw new FieldNotMappedException("ldbv7141TpStaTit2");
    }

    @Override
    public String getLdbv7141TpTit() {
        throw new FieldNotMappedException("ldbv7141TpTit");
    }

    @Override
    public void setLdbv7141TpTit(String ldbv7141TpTit) {
        throw new FieldNotMappedException("ldbv7141TpTit");
    }

    @Override
    public int getLdbvd601IdOgg() {
        throw new FieldNotMappedException("ldbvd601IdOgg");
    }

    @Override
    public void setLdbvd601IdOgg(int ldbvd601IdOgg) {
        throw new FieldNotMappedException("ldbvd601IdOgg");
    }

    @Override
    public String getLdbvd601TpOgg() {
        throw new FieldNotMappedException("ldbvd601TpOgg");
    }

    @Override
    public void setLdbvd601TpOgg(String ldbvd601TpOgg) {
        throw new FieldNotMappedException("ldbvd601TpOgg");
    }

    @Override
    public String getLdbvd601TpStaTit() {
        throw new FieldNotMappedException("ldbvd601TpStaTit");
    }

    @Override
    public void setLdbvd601TpStaTit(String ldbvd601TpStaTit) {
        throw new FieldNotMappedException("ldbvd601TpStaTit");
    }

    @Override
    public String getLdbvd601TpTit() {
        throw new FieldNotMappedException("ldbvd601TpTit");
    }

    @Override
    public void setLdbvd601TpTit(String ldbvd601TpTit) {
        throw new FieldNotMappedException("ldbvd601TpTit");
    }

    @Override
    public int getTitCodCompAnia() {
        throw new FieldNotMappedException("titCodCompAnia");
    }

    @Override
    public void setTitCodCompAnia(int titCodCompAnia) {
        throw new FieldNotMappedException("titCodCompAnia");
    }

    @Override
    public String getTitCodDvs() {
        throw new FieldNotMappedException("titCodDvs");
    }

    @Override
    public void setTitCodDvs(String titCodDvs) {
        throw new FieldNotMappedException("titCodDvs");
    }

    @Override
    public String getTitCodDvsObj() {
        return getTitCodDvs();
    }

    @Override
    public void setTitCodDvsObj(String titCodDvsObj) {
        setTitCodDvs(titCodDvsObj);
    }

    @Override
    public String getTitCodIban() {
        throw new FieldNotMappedException("titCodIban");
    }

    @Override
    public void setTitCodIban(String titCodIban) {
        throw new FieldNotMappedException("titCodIban");
    }

    @Override
    public String getTitCodIbanObj() {
        return getTitCodIban();
    }

    @Override
    public void setTitCodIbanObj(String titCodIbanObj) {
        setTitCodIban(titCodIbanObj);
    }

    public TitCont getTitCont() {
        return titCont;
    }

    public TitContDb getTitContDb() {
        return titContDb;
    }

    @Override
    public char getTitDsOperSql() {
        throw new FieldNotMappedException("titDsOperSql");
    }

    @Override
    public void setTitDsOperSql(char titDsOperSql) {
        throw new FieldNotMappedException("titDsOperSql");
    }

    @Override
    public long getTitDsRiga() {
        throw new FieldNotMappedException("titDsRiga");
    }

    @Override
    public void setTitDsRiga(long titDsRiga) {
        throw new FieldNotMappedException("titDsRiga");
    }

    @Override
    public char getTitDsStatoElab() {
        throw new FieldNotMappedException("titDsStatoElab");
    }

    @Override
    public void setTitDsStatoElab(char titDsStatoElab) {
        throw new FieldNotMappedException("titDsStatoElab");
    }

    @Override
    public long getTitDsTsEndCptz() {
        throw new FieldNotMappedException("titDsTsEndCptz");
    }

    @Override
    public void setTitDsTsEndCptz(long titDsTsEndCptz) {
        throw new FieldNotMappedException("titDsTsEndCptz");
    }

    @Override
    public long getTitDsTsIniCptz() {
        throw new FieldNotMappedException("titDsTsIniCptz");
    }

    @Override
    public void setTitDsTsIniCptz(long titDsTsIniCptz) {
        throw new FieldNotMappedException("titDsTsIniCptz");
    }

    @Override
    public String getTitDsUtente() {
        throw new FieldNotMappedException("titDsUtente");
    }

    @Override
    public void setTitDsUtente(String titDsUtente) {
        throw new FieldNotMappedException("titDsUtente");
    }

    @Override
    public int getTitDsVer() {
        throw new FieldNotMappedException("titDsVer");
    }

    @Override
    public void setTitDsVer(int titDsVer) {
        throw new FieldNotMappedException("titDsVer");
    }

    @Override
    public String getTitDtApplzMoraDb() {
        throw new FieldNotMappedException("titDtApplzMoraDb");
    }

    @Override
    public void setTitDtApplzMoraDb(String titDtApplzMoraDb) {
        throw new FieldNotMappedException("titDtApplzMoraDb");
    }

    @Override
    public String getTitDtApplzMoraDbObj() {
        return getTitDtApplzMoraDb();
    }

    @Override
    public void setTitDtApplzMoraDbObj(String titDtApplzMoraDbObj) {
        setTitDtApplzMoraDb(titDtApplzMoraDbObj);
    }

    @Override
    public String getTitDtCambioVltDb() {
        throw new FieldNotMappedException("titDtCambioVltDb");
    }

    @Override
    public void setTitDtCambioVltDb(String titDtCambioVltDb) {
        throw new FieldNotMappedException("titDtCambioVltDb");
    }

    @Override
    public String getTitDtCambioVltDbObj() {
        return getTitDtCambioVltDb();
    }

    @Override
    public void setTitDtCambioVltDbObj(String titDtCambioVltDbObj) {
        setTitDtCambioVltDb(titDtCambioVltDbObj);
    }

    @Override
    public String getTitDtCertFiscDb() {
        throw new FieldNotMappedException("titDtCertFiscDb");
    }

    @Override
    public void setTitDtCertFiscDb(String titDtCertFiscDb) {
        throw new FieldNotMappedException("titDtCertFiscDb");
    }

    @Override
    public String getTitDtCertFiscDbObj() {
        return getTitDtCertFiscDb();
    }

    @Override
    public void setTitDtCertFiscDbObj(String titDtCertFiscDbObj) {
        setTitDtCertFiscDb(titDtCertFiscDbObj);
    }

    @Override
    public String getTitDtEmisTitDb() {
        throw new FieldNotMappedException("titDtEmisTitDb");
    }

    @Override
    public void setTitDtEmisTitDb(String titDtEmisTitDb) {
        throw new FieldNotMappedException("titDtEmisTitDb");
    }

    @Override
    public String getTitDtEmisTitDbObj() {
        return getTitDtEmisTitDb();
    }

    @Override
    public void setTitDtEmisTitDbObj(String titDtEmisTitDbObj) {
        setTitDtEmisTitDb(titDtEmisTitDbObj);
    }

    @Override
    public String getTitDtEndCopDb() {
        throw new FieldNotMappedException("titDtEndCopDb");
    }

    @Override
    public void setTitDtEndCopDb(String titDtEndCopDb) {
        throw new FieldNotMappedException("titDtEndCopDb");
    }

    @Override
    public String getTitDtEndCopDbObj() {
        return getTitDtEndCopDb();
    }

    @Override
    public void setTitDtEndCopDbObj(String titDtEndCopDbObj) {
        setTitDtEndCopDb(titDtEndCopDbObj);
    }

    @Override
    public String getTitDtEndEffDb() {
        throw new FieldNotMappedException("titDtEndEffDb");
    }

    @Override
    public void setTitDtEndEffDb(String titDtEndEffDb) {
        throw new FieldNotMappedException("titDtEndEffDb");
    }

    @Override
    public String getTitDtEsiTitDb() {
        throw new FieldNotMappedException("titDtEsiTitDb");
    }

    @Override
    public void setTitDtEsiTitDb(String titDtEsiTitDb) {
        throw new FieldNotMappedException("titDtEsiTitDb");
    }

    @Override
    public String getTitDtEsiTitDbObj() {
        return getTitDtEsiTitDb();
    }

    @Override
    public void setTitDtEsiTitDbObj(String titDtEsiTitDbObj) {
        setTitDtEsiTitDb(titDtEsiTitDbObj);
    }

    @Override
    public String getTitDtIniCopDb() {
        return titContDb.getIniCopDb();
    }

    @Override
    public void setTitDtIniCopDb(String titDtIniCopDb) {
        this.titContDb.setIniCopDb(titDtIniCopDb);
    }

    @Override
    public String getTitDtIniCopDbObj() {
        if (indTitCont.getDtIniCop() >= 0) {
            return getTitDtIniCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtIniCopDbObj(String titDtIniCopDbObj) {
        if (titDtIniCopDbObj != null) {
            setTitDtIniCopDb(titDtIniCopDbObj);
            indTitCont.setDtIniCop(((short)0));
        }
        else {
            indTitCont.setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getTitDtIniEffDb() {
        throw new FieldNotMappedException("titDtIniEffDb");
    }

    @Override
    public void setTitDtIniEffDb(String titDtIniEffDb) {
        throw new FieldNotMappedException("titDtIniEffDb");
    }

    @Override
    public String getTitDtRichAddRidDb() {
        throw new FieldNotMappedException("titDtRichAddRidDb");
    }

    @Override
    public void setTitDtRichAddRidDb(String titDtRichAddRidDb) {
        throw new FieldNotMappedException("titDtRichAddRidDb");
    }

    @Override
    public String getTitDtRichAddRidDbObj() {
        return getTitDtRichAddRidDb();
    }

    @Override
    public void setTitDtRichAddRidDbObj(String titDtRichAddRidDbObj) {
        setTitDtRichAddRidDb(titDtRichAddRidDbObj);
    }

    @Override
    public String getTitDtVltDb() {
        throw new FieldNotMappedException("titDtVltDb");
    }

    @Override
    public void setTitDtVltDb(String titDtVltDb) {
        throw new FieldNotMappedException("titDtVltDb");
    }

    @Override
    public String getTitDtVltDbObj() {
        return getTitDtVltDb();
    }

    @Override
    public void setTitDtVltDbObj(String titDtVltDbObj) {
        setTitDtVltDb(titDtVltDbObj);
    }

    @Override
    public String getTitEstrCntCorrAdd() {
        throw new FieldNotMappedException("titEstrCntCorrAdd");
    }

    @Override
    public void setTitEstrCntCorrAdd(String titEstrCntCorrAdd) {
        throw new FieldNotMappedException("titEstrCntCorrAdd");
    }

    @Override
    public String getTitEstrCntCorrAddObj() {
        return getTitEstrCntCorrAdd();
    }

    @Override
    public void setTitEstrCntCorrAddObj(String titEstrCntCorrAddObj) {
        setTitEstrCntCorrAdd(titEstrCntCorrAddObj);
    }

    @Override
    public char getTitFlForzDtVlt() {
        throw new FieldNotMappedException("titFlForzDtVlt");
    }

    @Override
    public void setTitFlForzDtVlt(char titFlForzDtVlt) {
        throw new FieldNotMappedException("titFlForzDtVlt");
    }

    @Override
    public Character getTitFlForzDtVltObj() {
        return ((Character)getTitFlForzDtVlt());
    }

    @Override
    public void setTitFlForzDtVltObj(Character titFlForzDtVltObj) {
        setTitFlForzDtVlt(((char)titFlForzDtVltObj));
    }

    @Override
    public char getTitFlIncAutogen() {
        throw new FieldNotMappedException("titFlIncAutogen");
    }

    @Override
    public void setTitFlIncAutogen(char titFlIncAutogen) {
        throw new FieldNotMappedException("titFlIncAutogen");
    }

    @Override
    public Character getTitFlIncAutogenObj() {
        return ((Character)getTitFlIncAutogen());
    }

    @Override
    public void setTitFlIncAutogenObj(Character titFlIncAutogenObj) {
        setTitFlIncAutogen(((char)titFlIncAutogenObj));
    }

    @Override
    public char getTitFlMora() {
        throw new FieldNotMappedException("titFlMora");
    }

    @Override
    public void setTitFlMora(char titFlMora) {
        throw new FieldNotMappedException("titFlMora");
    }

    @Override
    public Character getTitFlMoraObj() {
        return ((Character)getTitFlMora());
    }

    @Override
    public void setTitFlMoraObj(Character titFlMoraObj) {
        setTitFlMora(((char)titFlMoraObj));
    }

    @Override
    public char getTitFlSoll() {
        throw new FieldNotMappedException("titFlSoll");
    }

    @Override
    public void setTitFlSoll(char titFlSoll) {
        throw new FieldNotMappedException("titFlSoll");
    }

    @Override
    public Character getTitFlSollObj() {
        return ((Character)getTitFlSoll());
    }

    @Override
    public void setTitFlSollObj(Character titFlSollObj) {
        setTitFlSoll(((char)titFlSollObj));
    }

    @Override
    public char getTitFlTitDaReinvst() {
        throw new FieldNotMappedException("titFlTitDaReinvst");
    }

    @Override
    public void setTitFlTitDaReinvst(char titFlTitDaReinvst) {
        throw new FieldNotMappedException("titFlTitDaReinvst");
    }

    @Override
    public Character getTitFlTitDaReinvstObj() {
        return ((Character)getTitFlTitDaReinvst());
    }

    @Override
    public void setTitFlTitDaReinvstObj(Character titFlTitDaReinvstObj) {
        setTitFlTitDaReinvst(((char)titFlTitDaReinvstObj));
    }

    @Override
    public int getTitFraz() {
        return titCont.getTitFraz().getTitFraz();
    }

    @Override
    public void setTitFraz(int titFraz) {
        this.titCont.getTitFraz().setTitFraz(titFraz);
    }

    @Override
    public Integer getTitFrazObj() {
        if (indTitCont.getFraz() >= 0) {
            return ((Integer)getTitFraz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFrazObj(Integer titFrazObj) {
        if (titFrazObj != null) {
            setTitFraz(((int)titFrazObj));
            indTitCont.setFraz(((short)0));
        }
        else {
            indTitCont.setFraz(((short)-1));
        }
    }

    @Override
    public String getTitIbRich() {
        throw new FieldNotMappedException("titIbRich");
    }

    @Override
    public void setTitIbRich(String titIbRich) {
        throw new FieldNotMappedException("titIbRich");
    }

    @Override
    public String getTitIbRichObj() {
        return getTitIbRich();
    }

    @Override
    public void setTitIbRichObj(String titIbRichObj) {
        setTitIbRich(titIbRichObj);
    }

    @Override
    public int getTitIdMoviChiu() {
        throw new FieldNotMappedException("titIdMoviChiu");
    }

    @Override
    public void setTitIdMoviChiu(int titIdMoviChiu) {
        throw new FieldNotMappedException("titIdMoviChiu");
    }

    @Override
    public Integer getTitIdMoviChiuObj() {
        return ((Integer)getTitIdMoviChiu());
    }

    @Override
    public void setTitIdMoviChiuObj(Integer titIdMoviChiuObj) {
        setTitIdMoviChiu(((int)titIdMoviChiuObj));
    }

    @Override
    public int getTitIdMoviCrz() {
        throw new FieldNotMappedException("titIdMoviCrz");
    }

    @Override
    public void setTitIdMoviCrz(int titIdMoviCrz) {
        throw new FieldNotMappedException("titIdMoviCrz");
    }

    @Override
    public int getTitIdOgg() {
        throw new FieldNotMappedException("titIdOgg");
    }

    @Override
    public void setTitIdOgg(int titIdOgg) {
        throw new FieldNotMappedException("titIdOgg");
    }

    @Override
    public int getTitIdRappAna() {
        throw new FieldNotMappedException("titIdRappAna");
    }

    @Override
    public void setTitIdRappAna(int titIdRappAna) {
        throw new FieldNotMappedException("titIdRappAna");
    }

    @Override
    public Integer getTitIdRappAnaObj() {
        return ((Integer)getTitIdRappAna());
    }

    @Override
    public void setTitIdRappAnaObj(Integer titIdRappAnaObj) {
        setTitIdRappAna(((int)titIdRappAnaObj));
    }

    @Override
    public int getTitIdRappRete() {
        throw new FieldNotMappedException("titIdRappRete");
    }

    @Override
    public void setTitIdRappRete(int titIdRappRete) {
        throw new FieldNotMappedException("titIdRappRete");
    }

    @Override
    public Integer getTitIdRappReteObj() {
        return ((Integer)getTitIdRappRete());
    }

    @Override
    public void setTitIdRappReteObj(Integer titIdRappReteObj) {
        setTitIdRappRete(((int)titIdRappReteObj));
    }

    @Override
    public int getTitIdTitCont() {
        throw new FieldNotMappedException("titIdTitCont");
    }

    @Override
    public void setTitIdTitCont(int titIdTitCont) {
        throw new FieldNotMappedException("titIdTitCont");
    }

    @Override
    public AfDecimal getTitImpAder() {
        throw new FieldNotMappedException("titImpAder");
    }

    @Override
    public void setTitImpAder(AfDecimal titImpAder) {
        throw new FieldNotMappedException("titImpAder");
    }

    @Override
    public AfDecimal getTitImpAderObj() {
        return getTitImpAder();
    }

    @Override
    public void setTitImpAderObj(AfDecimal titImpAderObj) {
        setTitImpAder(new AfDecimal(titImpAderObj, 15, 3));
    }

    @Override
    public AfDecimal getTitImpAz() {
        throw new FieldNotMappedException("titImpAz");
    }

    @Override
    public void setTitImpAz(AfDecimal titImpAz) {
        throw new FieldNotMappedException("titImpAz");
    }

    @Override
    public AfDecimal getTitImpAzObj() {
        return getTitImpAz();
    }

    @Override
    public void setTitImpAzObj(AfDecimal titImpAzObj) {
        setTitImpAz(new AfDecimal(titImpAzObj, 15, 3));
    }

    @Override
    public AfDecimal getTitImpPag() {
        throw new FieldNotMappedException("titImpPag");
    }

    @Override
    public void setTitImpPag(AfDecimal titImpPag) {
        throw new FieldNotMappedException("titImpPag");
    }

    @Override
    public AfDecimal getTitImpPagObj() {
        return getTitImpPag();
    }

    @Override
    public void setTitImpPagObj(AfDecimal titImpPagObj) {
        setTitImpPag(new AfDecimal(titImpPagObj, 15, 3));
    }

    @Override
    public AfDecimal getTitImpTfr() {
        throw new FieldNotMappedException("titImpTfr");
    }

    @Override
    public void setTitImpTfr(AfDecimal titImpTfr) {
        throw new FieldNotMappedException("titImpTfr");
    }

    @Override
    public AfDecimal getTitImpTfrObj() {
        return getTitImpTfr();
    }

    @Override
    public void setTitImpTfrObj(AfDecimal titImpTfrObj) {
        setTitImpTfr(new AfDecimal(titImpTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getTitImpTfrStrc() {
        throw new FieldNotMappedException("titImpTfrStrc");
    }

    @Override
    public void setTitImpTfrStrc(AfDecimal titImpTfrStrc) {
        throw new FieldNotMappedException("titImpTfrStrc");
    }

    @Override
    public AfDecimal getTitImpTfrStrcObj() {
        return getTitImpTfrStrc();
    }

    @Override
    public void setTitImpTfrStrcObj(AfDecimal titImpTfrStrcObj) {
        setTitImpTfrStrc(new AfDecimal(titImpTfrStrcObj, 15, 3));
    }

    @Override
    public AfDecimal getTitImpTrasfe() {
        throw new FieldNotMappedException("titImpTrasfe");
    }

    @Override
    public void setTitImpTrasfe(AfDecimal titImpTrasfe) {
        throw new FieldNotMappedException("titImpTrasfe");
    }

    @Override
    public AfDecimal getTitImpTrasfeObj() {
        return getTitImpTrasfe();
    }

    @Override
    public void setTitImpTrasfeObj(AfDecimal titImpTrasfeObj) {
        setTitImpTrasfe(new AfDecimal(titImpTrasfeObj, 15, 3));
    }

    @Override
    public AfDecimal getTitImpVolo() {
        throw new FieldNotMappedException("titImpVolo");
    }

    @Override
    public void setTitImpVolo(AfDecimal titImpVolo) {
        throw new FieldNotMappedException("titImpVolo");
    }

    @Override
    public AfDecimal getTitImpVoloObj() {
        return getTitImpVolo();
    }

    @Override
    public void setTitImpVoloObj(AfDecimal titImpVoloObj) {
        setTitImpVolo(new AfDecimal(titImpVoloObj, 15, 3));
    }

    @Override
    public int getTitNumRatAccorpate() {
        return titCont.getTitNumRatAccorpate().getTitNumRatAccorpate();
    }

    @Override
    public void setTitNumRatAccorpate(int titNumRatAccorpate) {
        this.titCont.getTitNumRatAccorpate().setTitNumRatAccorpate(titNumRatAccorpate);
    }

    @Override
    public Integer getTitNumRatAccorpateObj() {
        if (indTitCont.getNumRatAccorpate() >= 0) {
            return ((Integer)getTitNumRatAccorpate());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitNumRatAccorpateObj(Integer titNumRatAccorpateObj) {
        if (titNumRatAccorpateObj != null) {
            setTitNumRatAccorpate(((int)titNumRatAccorpateObj));
            indTitCont.setNumRatAccorpate(((short)0));
        }
        else {
            indTitCont.setNumRatAccorpate(((short)-1));
        }
    }

    @Override
    public int getTitProgTit() {
        throw new FieldNotMappedException("titProgTit");
    }

    @Override
    public void setTitProgTit(int titProgTit) {
        throw new FieldNotMappedException("titProgTit");
    }

    @Override
    public Integer getTitProgTitObj() {
        return ((Integer)getTitProgTit());
    }

    @Override
    public void setTitProgTitObj(Integer titProgTitObj) {
        setTitProgTit(((int)titProgTitObj));
    }

    @Override
    public AfDecimal getTitTotAcqExp() {
        throw new FieldNotMappedException("titTotAcqExp");
    }

    @Override
    public void setTitTotAcqExp(AfDecimal titTotAcqExp) {
        throw new FieldNotMappedException("titTotAcqExp");
    }

    @Override
    public AfDecimal getTitTotAcqExpObj() {
        return getTitTotAcqExp();
    }

    @Override
    public void setTitTotAcqExpObj(AfDecimal titTotAcqExpObj) {
        setTitTotAcqExp(new AfDecimal(titTotAcqExpObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotCarAcq() {
        throw new FieldNotMappedException("titTotCarAcq");
    }

    @Override
    public void setTitTotCarAcq(AfDecimal titTotCarAcq) {
        throw new FieldNotMappedException("titTotCarAcq");
    }

    @Override
    public AfDecimal getTitTotCarAcqObj() {
        return getTitTotCarAcq();
    }

    @Override
    public void setTitTotCarAcqObj(AfDecimal titTotCarAcqObj) {
        setTitTotCarAcq(new AfDecimal(titTotCarAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotCarGest() {
        throw new FieldNotMappedException("titTotCarGest");
    }

    @Override
    public void setTitTotCarGest(AfDecimal titTotCarGest) {
        throw new FieldNotMappedException("titTotCarGest");
    }

    @Override
    public AfDecimal getTitTotCarGestObj() {
        return getTitTotCarGest();
    }

    @Override
    public void setTitTotCarGestObj(AfDecimal titTotCarGestObj) {
        setTitTotCarGest(new AfDecimal(titTotCarGestObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotCarIas() {
        throw new FieldNotMappedException("titTotCarIas");
    }

    @Override
    public void setTitTotCarIas(AfDecimal titTotCarIas) {
        throw new FieldNotMappedException("titTotCarIas");
    }

    @Override
    public AfDecimal getTitTotCarIasObj() {
        return getTitTotCarIas();
    }

    @Override
    public void setTitTotCarIasObj(AfDecimal titTotCarIasObj) {
        setTitTotCarIas(new AfDecimal(titTotCarIasObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotCarInc() {
        throw new FieldNotMappedException("titTotCarInc");
    }

    @Override
    public void setTitTotCarInc(AfDecimal titTotCarInc) {
        throw new FieldNotMappedException("titTotCarInc");
    }

    @Override
    public AfDecimal getTitTotCarIncObj() {
        return getTitTotCarInc();
    }

    @Override
    public void setTitTotCarIncObj(AfDecimal titTotCarIncObj) {
        setTitTotCarInc(new AfDecimal(titTotCarIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotCnbtAntirac() {
        throw new FieldNotMappedException("titTotCnbtAntirac");
    }

    @Override
    public void setTitTotCnbtAntirac(AfDecimal titTotCnbtAntirac) {
        throw new FieldNotMappedException("titTotCnbtAntirac");
    }

    @Override
    public AfDecimal getTitTotCnbtAntiracObj() {
        return getTitTotCnbtAntirac();
    }

    @Override
    public void setTitTotCnbtAntiracObj(AfDecimal titTotCnbtAntiracObj) {
        setTitTotCnbtAntirac(new AfDecimal(titTotCnbtAntiracObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotCommisInter() {
        throw new FieldNotMappedException("titTotCommisInter");
    }

    @Override
    public void setTitTotCommisInter(AfDecimal titTotCommisInter) {
        throw new FieldNotMappedException("titTotCommisInter");
    }

    @Override
    public AfDecimal getTitTotCommisInterObj() {
        return getTitTotCommisInter();
    }

    @Override
    public void setTitTotCommisInterObj(AfDecimal titTotCommisInterObj) {
        setTitTotCommisInter(new AfDecimal(titTotCommisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotDir() {
        throw new FieldNotMappedException("titTotDir");
    }

    @Override
    public void setTitTotDir(AfDecimal titTotDir) {
        throw new FieldNotMappedException("titTotDir");
    }

    @Override
    public AfDecimal getTitTotDirObj() {
        return getTitTotDir();
    }

    @Override
    public void setTitTotDirObj(AfDecimal titTotDirObj) {
        setTitTotDir(new AfDecimal(titTotDirObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotIntrFraz() {
        throw new FieldNotMappedException("titTotIntrFraz");
    }

    @Override
    public void setTitTotIntrFraz(AfDecimal titTotIntrFraz) {
        throw new FieldNotMappedException("titTotIntrFraz");
    }

    @Override
    public AfDecimal getTitTotIntrFrazObj() {
        return getTitTotIntrFraz();
    }

    @Override
    public void setTitTotIntrFrazObj(AfDecimal titTotIntrFrazObj) {
        setTitTotIntrFraz(new AfDecimal(titTotIntrFrazObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotIntrMora() {
        throw new FieldNotMappedException("titTotIntrMora");
    }

    @Override
    public void setTitTotIntrMora(AfDecimal titTotIntrMora) {
        throw new FieldNotMappedException("titTotIntrMora");
    }

    @Override
    public AfDecimal getTitTotIntrMoraObj() {
        return getTitTotIntrMora();
    }

    @Override
    public void setTitTotIntrMoraObj(AfDecimal titTotIntrMoraObj) {
        setTitTotIntrMora(new AfDecimal(titTotIntrMoraObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotIntrPrest() {
        throw new FieldNotMappedException("titTotIntrPrest");
    }

    @Override
    public void setTitTotIntrPrest(AfDecimal titTotIntrPrest) {
        throw new FieldNotMappedException("titTotIntrPrest");
    }

    @Override
    public AfDecimal getTitTotIntrPrestObj() {
        return getTitTotIntrPrest();
    }

    @Override
    public void setTitTotIntrPrestObj(AfDecimal titTotIntrPrestObj) {
        setTitTotIntrPrest(new AfDecimal(titTotIntrPrestObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotIntrRetdt() {
        throw new FieldNotMappedException("titTotIntrRetdt");
    }

    @Override
    public void setTitTotIntrRetdt(AfDecimal titTotIntrRetdt) {
        throw new FieldNotMappedException("titTotIntrRetdt");
    }

    @Override
    public AfDecimal getTitTotIntrRetdtObj() {
        return getTitTotIntrRetdt();
    }

    @Override
    public void setTitTotIntrRetdtObj(AfDecimal titTotIntrRetdtObj) {
        setTitTotIntrRetdt(new AfDecimal(titTotIntrRetdtObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotIntrRiat() {
        throw new FieldNotMappedException("titTotIntrRiat");
    }

    @Override
    public void setTitTotIntrRiat(AfDecimal titTotIntrRiat) {
        throw new FieldNotMappedException("titTotIntrRiat");
    }

    @Override
    public AfDecimal getTitTotIntrRiatObj() {
        return getTitTotIntrRiat();
    }

    @Override
    public void setTitTotIntrRiatObj(AfDecimal titTotIntrRiatObj) {
        setTitTotIntrRiat(new AfDecimal(titTotIntrRiatObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotManfeeAntic() {
        throw new FieldNotMappedException("titTotManfeeAntic");
    }

    @Override
    public void setTitTotManfeeAntic(AfDecimal titTotManfeeAntic) {
        throw new FieldNotMappedException("titTotManfeeAntic");
    }

    @Override
    public AfDecimal getTitTotManfeeAnticObj() {
        return getTitTotManfeeAntic();
    }

    @Override
    public void setTitTotManfeeAnticObj(AfDecimal titTotManfeeAnticObj) {
        setTitTotManfeeAntic(new AfDecimal(titTotManfeeAnticObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotManfeeRec() {
        throw new FieldNotMappedException("titTotManfeeRec");
    }

    @Override
    public void setTitTotManfeeRec(AfDecimal titTotManfeeRec) {
        throw new FieldNotMappedException("titTotManfeeRec");
    }

    @Override
    public AfDecimal getTitTotManfeeRecObj() {
        return getTitTotManfeeRec();
    }

    @Override
    public void setTitTotManfeeRecObj(AfDecimal titTotManfeeRecObj) {
        setTitTotManfeeRec(new AfDecimal(titTotManfeeRecObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotManfeeRicor() {
        throw new FieldNotMappedException("titTotManfeeRicor");
    }

    @Override
    public void setTitTotManfeeRicor(AfDecimal titTotManfeeRicor) {
        throw new FieldNotMappedException("titTotManfeeRicor");
    }

    @Override
    public AfDecimal getTitTotManfeeRicorObj() {
        return getTitTotManfeeRicor();
    }

    @Override
    public void setTitTotManfeeRicorObj(AfDecimal titTotManfeeRicorObj) {
        setTitTotManfeeRicor(new AfDecimal(titTotManfeeRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotPreNet() {
        throw new FieldNotMappedException("titTotPreNet");
    }

    @Override
    public void setTitTotPreNet(AfDecimal titTotPreNet) {
        throw new FieldNotMappedException("titTotPreNet");
    }

    @Override
    public AfDecimal getTitTotPreNetObj() {
        return getTitTotPreNet();
    }

    @Override
    public void setTitTotPreNetObj(AfDecimal titTotPreNetObj) {
        setTitTotPreNet(new AfDecimal(titTotPreNetObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotPrePpIas() {
        throw new FieldNotMappedException("titTotPrePpIas");
    }

    @Override
    public void setTitTotPrePpIas(AfDecimal titTotPrePpIas) {
        throw new FieldNotMappedException("titTotPrePpIas");
    }

    @Override
    public AfDecimal getTitTotPrePpIasObj() {
        return getTitTotPrePpIas();
    }

    @Override
    public void setTitTotPrePpIasObj(AfDecimal titTotPrePpIasObj) {
        setTitTotPrePpIas(new AfDecimal(titTotPrePpIasObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotPreSoloRsh() {
        throw new FieldNotMappedException("titTotPreSoloRsh");
    }

    @Override
    public void setTitTotPreSoloRsh(AfDecimal titTotPreSoloRsh) {
        throw new FieldNotMappedException("titTotPreSoloRsh");
    }

    @Override
    public AfDecimal getTitTotPreSoloRshObj() {
        return getTitTotPreSoloRsh();
    }

    @Override
    public void setTitTotPreSoloRshObj(AfDecimal titTotPreSoloRshObj) {
        setTitTotPreSoloRsh(new AfDecimal(titTotPreSoloRshObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotPreTot() {
        throw new FieldNotMappedException("titTotPreTot");
    }

    @Override
    public void setTitTotPreTot(AfDecimal titTotPreTot) {
        throw new FieldNotMappedException("titTotPreTot");
    }

    @Override
    public AfDecimal getTitTotPreTotObj() {
        return getTitTotPreTot();
    }

    @Override
    public void setTitTotPreTotObj(AfDecimal titTotPreTotObj) {
        setTitTotPreTot(new AfDecimal(titTotPreTotObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotProvAcq1aa() {
        throw new FieldNotMappedException("titTotProvAcq1aa");
    }

    @Override
    public void setTitTotProvAcq1aa(AfDecimal titTotProvAcq1aa) {
        throw new FieldNotMappedException("titTotProvAcq1aa");
    }

    @Override
    public AfDecimal getTitTotProvAcq1aaObj() {
        return getTitTotProvAcq1aa();
    }

    @Override
    public void setTitTotProvAcq1aaObj(AfDecimal titTotProvAcq1aaObj) {
        setTitTotProvAcq1aa(new AfDecimal(titTotProvAcq1aaObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotProvAcq2aa() {
        throw new FieldNotMappedException("titTotProvAcq2aa");
    }

    @Override
    public void setTitTotProvAcq2aa(AfDecimal titTotProvAcq2aa) {
        throw new FieldNotMappedException("titTotProvAcq2aa");
    }

    @Override
    public AfDecimal getTitTotProvAcq2aaObj() {
        return getTitTotProvAcq2aa();
    }

    @Override
    public void setTitTotProvAcq2aaObj(AfDecimal titTotProvAcq2aaObj) {
        setTitTotProvAcq2aa(new AfDecimal(titTotProvAcq2aaObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotProvDaRec() {
        throw new FieldNotMappedException("titTotProvDaRec");
    }

    @Override
    public void setTitTotProvDaRec(AfDecimal titTotProvDaRec) {
        throw new FieldNotMappedException("titTotProvDaRec");
    }

    @Override
    public AfDecimal getTitTotProvDaRecObj() {
        return getTitTotProvDaRec();
    }

    @Override
    public void setTitTotProvDaRecObj(AfDecimal titTotProvDaRecObj) {
        setTitTotProvDaRec(new AfDecimal(titTotProvDaRecObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotProvInc() {
        throw new FieldNotMappedException("titTotProvInc");
    }

    @Override
    public void setTitTotProvInc(AfDecimal titTotProvInc) {
        throw new FieldNotMappedException("titTotProvInc");
    }

    @Override
    public AfDecimal getTitTotProvIncObj() {
        return getTitTotProvInc();
    }

    @Override
    public void setTitTotProvIncObj(AfDecimal titTotProvIncObj) {
        setTitTotProvInc(new AfDecimal(titTotProvIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotProvRicor() {
        throw new FieldNotMappedException("titTotProvRicor");
    }

    @Override
    public void setTitTotProvRicor(AfDecimal titTotProvRicor) {
        throw new FieldNotMappedException("titTotProvRicor");
    }

    @Override
    public AfDecimal getTitTotProvRicorObj() {
        return getTitTotProvRicor();
    }

    @Override
    public void setTitTotProvRicorObj(AfDecimal titTotProvRicorObj) {
        setTitTotProvRicor(new AfDecimal(titTotProvRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotRemunAss() {
        throw new FieldNotMappedException("titTotRemunAss");
    }

    @Override
    public void setTitTotRemunAss(AfDecimal titTotRemunAss) {
        throw new FieldNotMappedException("titTotRemunAss");
    }

    @Override
    public AfDecimal getTitTotRemunAssObj() {
        return getTitTotRemunAss();
    }

    @Override
    public void setTitTotRemunAssObj(AfDecimal titTotRemunAssObj) {
        setTitTotRemunAss(new AfDecimal(titTotRemunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotSoprAlt() {
        throw new FieldNotMappedException("titTotSoprAlt");
    }

    @Override
    public void setTitTotSoprAlt(AfDecimal titTotSoprAlt) {
        throw new FieldNotMappedException("titTotSoprAlt");
    }

    @Override
    public AfDecimal getTitTotSoprAltObj() {
        return getTitTotSoprAlt();
    }

    @Override
    public void setTitTotSoprAltObj(AfDecimal titTotSoprAltObj) {
        setTitTotSoprAlt(new AfDecimal(titTotSoprAltObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotSoprProf() {
        throw new FieldNotMappedException("titTotSoprProf");
    }

    @Override
    public void setTitTotSoprProf(AfDecimal titTotSoprProf) {
        throw new FieldNotMappedException("titTotSoprProf");
    }

    @Override
    public AfDecimal getTitTotSoprProfObj() {
        return getTitTotSoprProf();
    }

    @Override
    public void setTitTotSoprProfObj(AfDecimal titTotSoprProfObj) {
        setTitTotSoprProf(new AfDecimal(titTotSoprProfObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotSoprSan() {
        throw new FieldNotMappedException("titTotSoprSan");
    }

    @Override
    public void setTitTotSoprSan(AfDecimal titTotSoprSan) {
        throw new FieldNotMappedException("titTotSoprSan");
    }

    @Override
    public AfDecimal getTitTotSoprSanObj() {
        return getTitTotSoprSan();
    }

    @Override
    public void setTitTotSoprSanObj(AfDecimal titTotSoprSanObj) {
        setTitTotSoprSan(new AfDecimal(titTotSoprSanObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotSoprSpo() {
        throw new FieldNotMappedException("titTotSoprSpo");
    }

    @Override
    public void setTitTotSoprSpo(AfDecimal titTotSoprSpo) {
        throw new FieldNotMappedException("titTotSoprSpo");
    }

    @Override
    public AfDecimal getTitTotSoprSpoObj() {
        return getTitTotSoprSpo();
    }

    @Override
    public void setTitTotSoprSpoObj(AfDecimal titTotSoprSpoObj) {
        setTitTotSoprSpo(new AfDecimal(titTotSoprSpoObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotSoprTec() {
        throw new FieldNotMappedException("titTotSoprTec");
    }

    @Override
    public void setTitTotSoprTec(AfDecimal titTotSoprTec) {
        throw new FieldNotMappedException("titTotSoprTec");
    }

    @Override
    public AfDecimal getTitTotSoprTecObj() {
        return getTitTotSoprTec();
    }

    @Override
    public void setTitTotSoprTecObj(AfDecimal titTotSoprTecObj) {
        setTitTotSoprTec(new AfDecimal(titTotSoprTecObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotSpeAge() {
        throw new FieldNotMappedException("titTotSpeAge");
    }

    @Override
    public void setTitTotSpeAge(AfDecimal titTotSpeAge) {
        throw new FieldNotMappedException("titTotSpeAge");
    }

    @Override
    public AfDecimal getTitTotSpeAgeObj() {
        return getTitTotSpeAge();
    }

    @Override
    public void setTitTotSpeAgeObj(AfDecimal titTotSpeAgeObj) {
        setTitTotSpeAge(new AfDecimal(titTotSpeAgeObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotSpeMed() {
        throw new FieldNotMappedException("titTotSpeMed");
    }

    @Override
    public void setTitTotSpeMed(AfDecimal titTotSpeMed) {
        throw new FieldNotMappedException("titTotSpeMed");
    }

    @Override
    public AfDecimal getTitTotSpeMedObj() {
        return getTitTotSpeMed();
    }

    @Override
    public void setTitTotSpeMedObj(AfDecimal titTotSpeMedObj) {
        setTitTotSpeMed(new AfDecimal(titTotSpeMedObj, 15, 3));
    }

    @Override
    public AfDecimal getTitTotTax() {
        throw new FieldNotMappedException("titTotTax");
    }

    @Override
    public void setTitTotTax(AfDecimal titTotTax) {
        throw new FieldNotMappedException("titTotTax");
    }

    @Override
    public AfDecimal getTitTotTaxObj() {
        return getTitTotTax();
    }

    @Override
    public void setTitTotTaxObj(AfDecimal titTotTaxObj) {
        setTitTotTax(new AfDecimal(titTotTaxObj, 15, 3));
    }

    @Override
    public String getTitTpCausDispStor() {
        throw new FieldNotMappedException("titTpCausDispStor");
    }

    @Override
    public void setTitTpCausDispStor(String titTpCausDispStor) {
        throw new FieldNotMappedException("titTpCausDispStor");
    }

    @Override
    public String getTitTpCausDispStorObj() {
        return getTitTpCausDispStor();
    }

    @Override
    public void setTitTpCausDispStorObj(String titTpCausDispStorObj) {
        setTitTpCausDispStor(titTpCausDispStorObj);
    }

    @Override
    public String getTitTpCausRimb() {
        throw new FieldNotMappedException("titTpCausRimb");
    }

    @Override
    public void setTitTpCausRimb(String titTpCausRimb) {
        throw new FieldNotMappedException("titTpCausRimb");
    }

    @Override
    public String getTitTpCausRimbObj() {
        return getTitTpCausRimb();
    }

    @Override
    public void setTitTpCausRimbObj(String titTpCausRimbObj) {
        setTitTpCausRimb(titTpCausRimbObj);
    }

    @Override
    public int getTitTpCausStor() {
        throw new FieldNotMappedException("titTpCausStor");
    }

    @Override
    public void setTitTpCausStor(int titTpCausStor) {
        throw new FieldNotMappedException("titTpCausStor");
    }

    @Override
    public Integer getTitTpCausStorObj() {
        return ((Integer)getTitTpCausStor());
    }

    @Override
    public void setTitTpCausStorObj(Integer titTpCausStorObj) {
        setTitTpCausStor(((int)titTpCausStorObj));
    }

    @Override
    public String getTitTpEsiRid() {
        throw new FieldNotMappedException("titTpEsiRid");
    }

    @Override
    public void setTitTpEsiRid(String titTpEsiRid) {
        throw new FieldNotMappedException("titTpEsiRid");
    }

    @Override
    public String getTitTpEsiRidObj() {
        return getTitTpEsiRid();
    }

    @Override
    public void setTitTpEsiRidObj(String titTpEsiRidObj) {
        setTitTpEsiRid(titTpEsiRidObj);
    }

    @Override
    public String getTitTpMezPagAdd() {
        throw new FieldNotMappedException("titTpMezPagAdd");
    }

    @Override
    public void setTitTpMezPagAdd(String titTpMezPagAdd) {
        throw new FieldNotMappedException("titTpMezPagAdd");
    }

    @Override
    public String getTitTpMezPagAddObj() {
        return getTitTpMezPagAdd();
    }

    @Override
    public void setTitTpMezPagAddObj(String titTpMezPagAddObj) {
        setTitTpMezPagAdd(titTpMezPagAddObj);
    }

    @Override
    public String getTitTpOgg() {
        throw new FieldNotMappedException("titTpOgg");
    }

    @Override
    public void setTitTpOgg(String titTpOgg) {
        throw new FieldNotMappedException("titTpOgg");
    }

    @Override
    public String getTitTpPreTit() {
        throw new FieldNotMappedException("titTpPreTit");
    }

    @Override
    public void setTitTpPreTit(String titTpPreTit) {
        throw new FieldNotMappedException("titTpPreTit");
    }

    @Override
    public String getTitTpStatTit() {
        throw new FieldNotMappedException("titTpStatTit");
    }

    @Override
    public void setTitTpStatTit(String titTpStatTit) {
        throw new FieldNotMappedException("titTpStatTit");
    }

    @Override
    public String getTitTpTit() {
        throw new FieldNotMappedException("titTpTit");
    }

    @Override
    public void setTitTpTit(String titTpTit) {
        throw new FieldNotMappedException("titTpTit");
    }

    @Override
    public String getTitTpTitMigraz() {
        throw new FieldNotMappedException("titTpTitMigraz");
    }

    @Override
    public void setTitTpTitMigraz(String titTpTitMigraz) {
        throw new FieldNotMappedException("titTpTitMigraz");
    }

    @Override
    public String getTitTpTitMigrazObj() {
        return getTitTpTitMigraz();
    }

    @Override
    public void setTitTpTitMigrazObj(String titTpTitMigrazObj) {
        setTitTpTitMigraz(titTpTitMigrazObj);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
