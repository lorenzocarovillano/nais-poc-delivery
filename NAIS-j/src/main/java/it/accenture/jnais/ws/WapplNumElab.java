package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WAPPL-NUM-ELAB<br>
 * Variable: WAPPL-NUM-ELAB from program LOAS0820<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WapplNumElab extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WAPPL-ELAB-INIZIATA
    private char elabIniziata = DefaultValues.CHAR_VAL;
    public static final char INIZIO = '1';

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WAPPL_NUM_ELAB;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWapplNumElabBytes(buf);
    }

    public String getWapplNumElabFormatted() {
        return String.valueOf(getElabIniziata());
    }

    public void setWapplNumElabBytes(byte[] buffer) {
        setWapplNumElabBytes(buffer, 1);
    }

    public byte[] getWapplNumElabBytes() {
        byte[] buffer = new byte[Len.WAPPL_NUM_ELAB];
        return getWapplNumElabBytes(buffer, 1);
    }

    public void setWapplNumElabBytes(byte[] buffer, int offset) {
        int position = offset;
        elabIniziata = MarshalByte.readChar(buffer, position);
    }

    public byte[] getWapplNumElabBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, elabIniziata);
        return buffer;
    }

    public void setElabIniziata(char elabIniziata) {
        this.elabIniziata = elabIniziata;
    }

    public void setElabIniziataFormatted(String elabIniziata) {
        setElabIniziata(Functions.charAt(elabIniziata, Types.CHAR_SIZE));
    }

    public char getElabIniziata() {
        return this.elabIniziata;
    }

    public boolean isInizio() {
        return elabIniziata == INIZIO;
    }

    @Override
    public byte[] serialize() {
        return getWapplNumElabBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELAB_INIZIATA = 1;
        public static final int WAPPL_NUM_ELAB = ELAB_INIZIATA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
