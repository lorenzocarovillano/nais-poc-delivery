package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B04-VAL-PC<br>
 * Variable: B04-VAL-PC from program LLBS0266<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B04ValPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B04ValPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B04_VAL_PC;
    }

    public void setB04ValPc(AfDecimal b04ValPc) {
        writeDecimalAsPacked(Pos.B04_VAL_PC, b04ValPc.copy());
    }

    public void setB04ValPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B04_VAL_PC, Pos.B04_VAL_PC);
    }

    /**Original name: B04-VAL-PC<br>*/
    public AfDecimal getB04ValPc() {
        return readPackedAsDecimal(Pos.B04_VAL_PC, Len.Int.B04_VAL_PC, Len.Fract.B04_VAL_PC);
    }

    public byte[] getB04ValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B04_VAL_PC, Pos.B04_VAL_PC);
        return buffer;
    }

    public void setB04ValPcNull(String b04ValPcNull) {
        writeString(Pos.B04_VAL_PC_NULL, b04ValPcNull, Len.B04_VAL_PC_NULL);
    }

    /**Original name: B04-VAL-PC-NULL<br>*/
    public String getB04ValPcNull() {
        return readString(Pos.B04_VAL_PC_NULL, Len.B04_VAL_PC_NULL);
    }

    public String getB04ValPcNullFormatted() {
        return Functions.padBlanks(getB04ValPcNull(), Len.B04_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B04_VAL_PC = 1;
        public static final int B04_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B04_VAL_PC = 8;
        public static final int B04_VAL_PC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B04_VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B04_VAL_PC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
