package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-PC-RISC-PARZ<br>
 * Variable: S089-PC-RISC-PARZ from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089PcRiscParz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089PcRiscParz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_PC_RISC_PARZ;
    }

    public void setWlquPcRiscParz(AfDecimal wlquPcRiscParz) {
        writeDecimalAsPacked(Pos.S089_PC_RISC_PARZ, wlquPcRiscParz.copy());
    }

    public void setWlquPcRiscParzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_PC_RISC_PARZ, Pos.S089_PC_RISC_PARZ);
    }

    /**Original name: WLQU-PC-RISC-PARZ<br>*/
    public AfDecimal getWlquPcRiscParz() {
        return readPackedAsDecimal(Pos.S089_PC_RISC_PARZ, Len.Int.WLQU_PC_RISC_PARZ, Len.Fract.WLQU_PC_RISC_PARZ);
    }

    public byte[] getWlquPcRiscParzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_PC_RISC_PARZ, Pos.S089_PC_RISC_PARZ);
        return buffer;
    }

    public void initWlquPcRiscParzSpaces() {
        fill(Pos.S089_PC_RISC_PARZ, Len.S089_PC_RISC_PARZ, Types.SPACE_CHAR);
    }

    public void setWlquPcRiscParzNull(String wlquPcRiscParzNull) {
        writeString(Pos.S089_PC_RISC_PARZ_NULL, wlquPcRiscParzNull, Len.WLQU_PC_RISC_PARZ_NULL);
    }

    /**Original name: WLQU-PC-RISC-PARZ-NULL<br>*/
    public String getWlquPcRiscParzNull() {
        return readString(Pos.S089_PC_RISC_PARZ_NULL, Len.WLQU_PC_RISC_PARZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_PC_RISC_PARZ = 1;
        public static final int S089_PC_RISC_PARZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_PC_RISC_PARZ = 7;
        public static final int WLQU_PC_RISC_PARZ_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_PC_RISC_PARZ = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_PC_RISC_PARZ = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
