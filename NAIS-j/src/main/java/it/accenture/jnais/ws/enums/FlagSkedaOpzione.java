package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-SKEDA-OPZIONE<br>
 * Variable: FLAG-SKEDA-OPZIONE from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagSkedaOpzione {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagSkedaOpzione(char flagSkedaOpzione) {
        this.value = flagSkedaOpzione;
    }

    public char getFlagSkedaOpzione() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
