package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: E12-CUM-PRSTZ<br>
 * Variable: E12-CUM-PRSTZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E12CumPrstz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E12CumPrstz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E12_CUM_PRSTZ;
    }

    public void setE12CumPrstz(AfDecimal e12CumPrstz) {
        writeDecimalAsPacked(Pos.E12_CUM_PRSTZ, e12CumPrstz.copy());
    }

    public void setE12CumPrstzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E12_CUM_PRSTZ, Pos.E12_CUM_PRSTZ);
    }

    /**Original name: E12-CUM-PRSTZ<br>*/
    public AfDecimal getE12CumPrstz() {
        return readPackedAsDecimal(Pos.E12_CUM_PRSTZ, Len.Int.E12_CUM_PRSTZ, Len.Fract.E12_CUM_PRSTZ);
    }

    public byte[] getE12CumPrstzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E12_CUM_PRSTZ, Pos.E12_CUM_PRSTZ);
        return buffer;
    }

    public void setE12CumPrstzNull(String e12CumPrstzNull) {
        writeString(Pos.E12_CUM_PRSTZ_NULL, e12CumPrstzNull, Len.E12_CUM_PRSTZ_NULL);
    }

    /**Original name: E12-CUM-PRSTZ-NULL<br>*/
    public String getE12CumPrstzNull() {
        return readString(Pos.E12_CUM_PRSTZ_NULL, Len.E12_CUM_PRSTZ_NULL);
    }

    public String getE12CumPrstzNullFormatted() {
        return Functions.padBlanks(getE12CumPrstzNull(), Len.E12_CUM_PRSTZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E12_CUM_PRSTZ = 1;
        public static final int E12_CUM_PRSTZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E12_CUM_PRSTZ = 8;
        public static final int E12_CUM_PRSTZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E12_CUM_PRSTZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int E12_CUM_PRSTZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
