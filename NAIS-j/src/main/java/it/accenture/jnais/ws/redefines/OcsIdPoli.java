package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCS-ID-POLI<br>
 * Variable: OCS-ID-POLI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcsIdPoli extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcsIdPoli() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCS_ID_POLI;
    }

    public void setOcsIdPoli(int ocsIdPoli) {
        writeIntAsPacked(Pos.OCS_ID_POLI, ocsIdPoli, Len.Int.OCS_ID_POLI);
    }

    public void setOcsIdPoliFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCS_ID_POLI, Pos.OCS_ID_POLI);
    }

    /**Original name: OCS-ID-POLI<br>*/
    public int getOcsIdPoli() {
        return readPackedAsInt(Pos.OCS_ID_POLI, Len.Int.OCS_ID_POLI);
    }

    public byte[] getOcsIdPoliAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCS_ID_POLI, Pos.OCS_ID_POLI);
        return buffer;
    }

    public void setOcsIdPoliNull(String ocsIdPoliNull) {
        writeString(Pos.OCS_ID_POLI_NULL, ocsIdPoliNull, Len.OCS_ID_POLI_NULL);
    }

    /**Original name: OCS-ID-POLI-NULL<br>*/
    public String getOcsIdPoliNull() {
        return readString(Pos.OCS_ID_POLI_NULL, Len.OCS_ID_POLI_NULL);
    }

    public String getOcsIdPoliNullFormatted() {
        return Functions.padBlanks(getOcsIdPoliNull(), Len.OCS_ID_POLI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCS_ID_POLI = 1;
        public static final int OCS_ID_POLI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCS_ID_POLI = 5;
        public static final int OCS_ID_POLI_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCS_ID_POLI = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
