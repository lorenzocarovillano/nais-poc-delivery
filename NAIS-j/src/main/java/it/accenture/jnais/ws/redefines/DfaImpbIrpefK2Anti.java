package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPB-IRPEF-K2-ANTI<br>
 * Variable: DFA-IMPB-IRPEF-K2-ANTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpbIrpefK2Anti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpbIrpefK2Anti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPB_IRPEF_K2_ANTI;
    }

    public void setDfaImpbIrpefK2Anti(AfDecimal dfaImpbIrpefK2Anti) {
        writeDecimalAsPacked(Pos.DFA_IMPB_IRPEF_K2_ANTI, dfaImpbIrpefK2Anti.copy());
    }

    public void setDfaImpbIrpefK2AntiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPB_IRPEF_K2_ANTI, Pos.DFA_IMPB_IRPEF_K2_ANTI);
    }

    /**Original name: DFA-IMPB-IRPEF-K2-ANTI<br>*/
    public AfDecimal getDfaImpbIrpefK2Anti() {
        return readPackedAsDecimal(Pos.DFA_IMPB_IRPEF_K2_ANTI, Len.Int.DFA_IMPB_IRPEF_K2_ANTI, Len.Fract.DFA_IMPB_IRPEF_K2_ANTI);
    }

    public byte[] getDfaImpbIrpefK2AntiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPB_IRPEF_K2_ANTI, Pos.DFA_IMPB_IRPEF_K2_ANTI);
        return buffer;
    }

    public void setDfaImpbIrpefK2AntiNull(String dfaImpbIrpefK2AntiNull) {
        writeString(Pos.DFA_IMPB_IRPEF_K2_ANTI_NULL, dfaImpbIrpefK2AntiNull, Len.DFA_IMPB_IRPEF_K2_ANTI_NULL);
    }

    /**Original name: DFA-IMPB-IRPEF-K2-ANTI-NULL<br>*/
    public String getDfaImpbIrpefK2AntiNull() {
        return readString(Pos.DFA_IMPB_IRPEF_K2_ANTI_NULL, Len.DFA_IMPB_IRPEF_K2_ANTI_NULL);
    }

    public String getDfaImpbIrpefK2AntiNullFormatted() {
        return Functions.padBlanks(getDfaImpbIrpefK2AntiNull(), Len.DFA_IMPB_IRPEF_K2_ANTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IRPEF_K2_ANTI = 1;
        public static final int DFA_IMPB_IRPEF_K2_ANTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IRPEF_K2_ANTI = 8;
        public static final int DFA_IMPB_IRPEF_K2_ANTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IRPEF_K2_ANTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IRPEF_K2_ANTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
