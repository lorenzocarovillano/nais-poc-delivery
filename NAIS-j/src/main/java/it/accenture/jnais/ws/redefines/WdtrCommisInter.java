package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-COMMIS-INTER<br>
 * Variable: WDTR-COMMIS-INTER from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_COMMIS_INTER;
    }

    public void setWdtrCommisInter(AfDecimal wdtrCommisInter) {
        writeDecimalAsPacked(Pos.WDTR_COMMIS_INTER, wdtrCommisInter.copy());
    }

    /**Original name: WDTR-COMMIS-INTER<br>*/
    public AfDecimal getWdtrCommisInter() {
        return readPackedAsDecimal(Pos.WDTR_COMMIS_INTER, Len.Int.WDTR_COMMIS_INTER, Len.Fract.WDTR_COMMIS_INTER);
    }

    public void setWdtrCommisInterNull(String wdtrCommisInterNull) {
        writeString(Pos.WDTR_COMMIS_INTER_NULL, wdtrCommisInterNull, Len.WDTR_COMMIS_INTER_NULL);
    }

    /**Original name: WDTR-COMMIS-INTER-NULL<br>*/
    public String getWdtrCommisInterNull() {
        return readString(Pos.WDTR_COMMIS_INTER_NULL, Len.WDTR_COMMIS_INTER_NULL);
    }

    public String getWdtrCommisInterNullFormatted() {
        return Functions.padBlanks(getWdtrCommisInterNull(), Len.WDTR_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_COMMIS_INTER = 1;
        public static final int WDTR_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_COMMIS_INTER = 8;
        public static final int WDTR_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
