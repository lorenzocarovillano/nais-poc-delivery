package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-PREMIO-RISC<br>
 * Variable: WPAG-CONT-PREMIO-RISC from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContPremioRisc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContPremioRisc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_PREMIO_RISC;
    }

    public void setWpagContPremioRisc(AfDecimal wpagContPremioRisc) {
        writeDecimalAsPacked(Pos.WPAG_CONT_PREMIO_RISC, wpagContPremioRisc.copy());
    }

    public void setWpagContPremioRiscFormatted(String wpagContPremioRisc) {
        setWpagContPremioRisc(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_PREMIO_RISC + Len.Fract.WPAG_CONT_PREMIO_RISC, Len.Fract.WPAG_CONT_PREMIO_RISC, wpagContPremioRisc));
    }

    public void setWpagContPremioRiscFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_PREMIO_RISC, Pos.WPAG_CONT_PREMIO_RISC);
    }

    /**Original name: WPAG-CONT-PREMIO-RISC<br>*/
    public AfDecimal getWpagContPremioRisc() {
        return readPackedAsDecimal(Pos.WPAG_CONT_PREMIO_RISC, Len.Int.WPAG_CONT_PREMIO_RISC, Len.Fract.WPAG_CONT_PREMIO_RISC);
    }

    public byte[] getWpagContPremioRiscAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_PREMIO_RISC, Pos.WPAG_CONT_PREMIO_RISC);
        return buffer;
    }

    public void initWpagContPremioRiscSpaces() {
        fill(Pos.WPAG_CONT_PREMIO_RISC, Len.WPAG_CONT_PREMIO_RISC, Types.SPACE_CHAR);
    }

    public void setWpagContPremioRiscNull(String wpagContPremioRiscNull) {
        writeString(Pos.WPAG_CONT_PREMIO_RISC_NULL, wpagContPremioRiscNull, Len.WPAG_CONT_PREMIO_RISC_NULL);
    }

    /**Original name: WPAG-CONT-PREMIO-RISC-NULL<br>*/
    public String getWpagContPremioRiscNull() {
        return readString(Pos.WPAG_CONT_PREMIO_RISC_NULL, Len.WPAG_CONT_PREMIO_RISC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_PREMIO_RISC = 1;
        public static final int WPAG_CONT_PREMIO_RISC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_PREMIO_RISC = 8;
        public static final int WPAG_CONT_PREMIO_RISC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_PREMIO_RISC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_PREMIO_RISC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
