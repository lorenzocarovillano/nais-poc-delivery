package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: POG-VAL-PC<br>
 * Variable: POG-VAL-PC from program LDBS1130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PogValPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PogValPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POG_VAL_PC;
    }

    public void setPogValPc(AfDecimal pogValPc) {
        writeDecimalAsPacked(Pos.POG_VAL_PC, pogValPc.copy());
    }

    public void setPogValPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POG_VAL_PC, Pos.POG_VAL_PC);
    }

    /**Original name: POG-VAL-PC<br>*/
    public AfDecimal getPogValPc() {
        return readPackedAsDecimal(Pos.POG_VAL_PC, Len.Int.POG_VAL_PC, Len.Fract.POG_VAL_PC);
    }

    public byte[] getPogValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POG_VAL_PC, Pos.POG_VAL_PC);
        return buffer;
    }

    public void setPogValPcNull(String pogValPcNull) {
        writeString(Pos.POG_VAL_PC_NULL, pogValPcNull, Len.POG_VAL_PC_NULL);
    }

    /**Original name: POG-VAL-PC-NULL<br>*/
    public String getPogValPcNull() {
        return readString(Pos.POG_VAL_PC_NULL, Len.POG_VAL_PC_NULL);
    }

    public String getPogValPcNullFormatted() {
        return Functions.padBlanks(getPogValPcNull(), Len.POG_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POG_VAL_PC = 1;
        public static final int POG_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POG_VAL_PC = 8;
        public static final int POG_VAL_PC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POG_VAL_PC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int POG_VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
