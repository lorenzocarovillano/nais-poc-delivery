package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-RENDTO-RETR<br>
 * Variable: L3421-RENDTO-RETR from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421RendtoRetr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421RendtoRetr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_RENDTO_RETR;
    }

    public void setL3421RendtoRetrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_RENDTO_RETR, Pos.L3421_RENDTO_RETR);
    }

    /**Original name: L3421-RENDTO-RETR<br>*/
    public AfDecimal getL3421RendtoRetr() {
        return readPackedAsDecimal(Pos.L3421_RENDTO_RETR, Len.Int.L3421_RENDTO_RETR, Len.Fract.L3421_RENDTO_RETR);
    }

    public byte[] getL3421RendtoRetrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_RENDTO_RETR, Pos.L3421_RENDTO_RETR);
        return buffer;
    }

    /**Original name: L3421-RENDTO-RETR-NULL<br>*/
    public String getL3421RendtoRetrNull() {
        return readString(Pos.L3421_RENDTO_RETR_NULL, Len.L3421_RENDTO_RETR_NULL);
    }

    public String getL3421RendtoRetrNullFormatted() {
        return Functions.padBlanks(getL3421RendtoRetrNull(), Len.L3421_RENDTO_RETR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_RENDTO_RETR = 1;
        public static final int L3421_RENDTO_RETR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_RENDTO_RETR = 8;
        public static final int L3421_RENDTO_RETR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_RENDTO_RETR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_RENDTO_RETR = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
