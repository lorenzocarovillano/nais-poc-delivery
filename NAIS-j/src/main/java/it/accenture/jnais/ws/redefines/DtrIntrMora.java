package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-INTR-MORA<br>
 * Variable: DTR-INTR-MORA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrIntrMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrIntrMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_INTR_MORA;
    }

    public void setDtrIntrMora(AfDecimal dtrIntrMora) {
        writeDecimalAsPacked(Pos.DTR_INTR_MORA, dtrIntrMora.copy());
    }

    public void setDtrIntrMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_INTR_MORA, Pos.DTR_INTR_MORA);
    }

    /**Original name: DTR-INTR-MORA<br>*/
    public AfDecimal getDtrIntrMora() {
        return readPackedAsDecimal(Pos.DTR_INTR_MORA, Len.Int.DTR_INTR_MORA, Len.Fract.DTR_INTR_MORA);
    }

    public byte[] getDtrIntrMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_INTR_MORA, Pos.DTR_INTR_MORA);
        return buffer;
    }

    public void setDtrIntrMoraNull(String dtrIntrMoraNull) {
        writeString(Pos.DTR_INTR_MORA_NULL, dtrIntrMoraNull, Len.DTR_INTR_MORA_NULL);
    }

    /**Original name: DTR-INTR-MORA-NULL<br>*/
    public String getDtrIntrMoraNull() {
        return readString(Pos.DTR_INTR_MORA_NULL, Len.DTR_INTR_MORA_NULL);
    }

    public String getDtrIntrMoraNullFormatted() {
        return Functions.padBlanks(getDtrIntrMoraNull(), Len.DTR_INTR_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_INTR_MORA = 1;
        public static final int DTR_INTR_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_INTR_MORA = 8;
        public static final int DTR_INTR_MORA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_INTR_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_INTR_MORA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
