package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-TABELLA-ERRORI<br>
 * Variables: WK-TABELLA-ERRORI from program IEAS9900<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WkTabellaErrori {

    //==== PROPERTIES ====
    //Original name: WK-COD-ERRORE
    private String codErrore = DefaultValues.stringVal(Len.COD_ERRORE);
    //Original name: WK-ID-GRAVITA-ERRORE
    private String idGravitaErrore = DefaultValues.stringVal(Len.ID_GRAVITA_ERRORE);
    //Original name: WK-LIV-GRAVITA
    private int livGravita = DefaultValues.INT_VAL;
    //Original name: WK-TIPO-TRATT-FE
    private char tipoTrattFe = DefaultValues.CHAR_VAL;
    //Original name: WK-DESC-ERRORE-BREVE
    private String descErroreBreve = DefaultValues.stringVal(Len.DESC_ERRORE_BREVE);
    //Original name: WK-DESC-ERRORE-ESTESA
    private String descErroreEstesa = DefaultValues.stringVal(Len.DESC_ERRORE_ESTESA);
    //Original name: WK-COD-ERRORE-990
    private String codErrore990 = DefaultValues.stringVal(Len.COD_ERRORE990);
    //Original name: WK-LABEL-ERR-990
    private String labelErr990 = DefaultValues.stringVal(Len.LABEL_ERR990);

    //==== METHODS ====
    public void setCodErroreFormatted(String codErrore) {
        this.codErrore = Trunc.toUnsignedNumeric(codErrore, Len.COD_ERRORE);
    }

    public int getCodErrore() {
        return NumericDisplay.asInt(this.codErrore);
    }

    public void setIdGravitaErroreFormatted(String idGravitaErrore) {
        this.idGravitaErrore = Trunc.toUnsignedNumeric(idGravitaErrore, Len.ID_GRAVITA_ERRORE);
    }

    public int getIdGravitaErrore() {
        return NumericDisplay.asInt(this.idGravitaErrore);
    }

    public String getIdGravitaErroreFormatted() {
        return this.idGravitaErrore;
    }

    public void setLivGravita(int livGravita) {
        this.livGravita = livGravita;
    }

    public int getLivGravita() {
        return this.livGravita;
    }

    public void setTipoTrattFe(char tipoTrattFe) {
        this.tipoTrattFe = tipoTrattFe;
    }

    public char getTipoTrattFe() {
        return this.tipoTrattFe;
    }

    public void setDescErroreBreve(String descErroreBreve) {
        this.descErroreBreve = Functions.subString(descErroreBreve, Len.DESC_ERRORE_BREVE);
    }

    public String getDescErroreBreve() {
        return this.descErroreBreve;
    }

    public void setDescErroreEstesa(String descErroreEstesa) {
        this.descErroreEstesa = Functions.subString(descErroreEstesa, Len.DESC_ERRORE_ESTESA);
    }

    public String getDescErroreEstesa() {
        return this.descErroreEstesa;
    }

    public void setCodErrore990Formatted(String codErrore990) {
        this.codErrore990 = Trunc.toUnsignedNumeric(codErrore990, Len.COD_ERRORE990);
    }

    public int getCodErrore990() {
        return NumericDisplay.asInt(this.codErrore990);
    }

    public String getCodErrore990Formatted() {
        return this.codErrore990;
    }

    public void setLabelErr990(String labelErr990) {
        this.labelErr990 = Functions.subString(labelErr990, Len.LABEL_ERR990);
    }

    public String getLabelErr990() {
        return this.labelErr990;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_ERRORE = 6;
        public static final int ID_GRAVITA_ERRORE = 9;
        public static final int DESC_ERRORE_BREVE = 100;
        public static final int DESC_ERRORE_ESTESA = 200;
        public static final int COD_ERRORE990 = 6;
        public static final int LABEL_ERR990 = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
