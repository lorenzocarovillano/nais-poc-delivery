package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WISO-ALQ-IS<br>
 * Variable: WISO-ALQ-IS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoAlqIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoAlqIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_ALQ_IS;
    }

    public void setWisoAlqIs(AfDecimal wisoAlqIs) {
        writeDecimalAsPacked(Pos.WISO_ALQ_IS, wisoAlqIs.copy());
    }

    public void setWisoAlqIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_ALQ_IS, Pos.WISO_ALQ_IS);
    }

    /**Original name: WISO-ALQ-IS<br>*/
    public AfDecimal getWisoAlqIs() {
        return readPackedAsDecimal(Pos.WISO_ALQ_IS, Len.Int.WISO_ALQ_IS, Len.Fract.WISO_ALQ_IS);
    }

    public byte[] getWisoAlqIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_ALQ_IS, Pos.WISO_ALQ_IS);
        return buffer;
    }

    public void initWisoAlqIsSpaces() {
        fill(Pos.WISO_ALQ_IS, Len.WISO_ALQ_IS, Types.SPACE_CHAR);
    }

    public void setWisoAlqIsNull(String wisoAlqIsNull) {
        writeString(Pos.WISO_ALQ_IS_NULL, wisoAlqIsNull, Len.WISO_ALQ_IS_NULL);
    }

    /**Original name: WISO-ALQ-IS-NULL<br>*/
    public String getWisoAlqIsNull() {
        return readString(Pos.WISO_ALQ_IS_NULL, Len.WISO_ALQ_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_ALQ_IS = 1;
        public static final int WISO_ALQ_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_ALQ_IS = 4;
        public static final int WISO_ALQ_IS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WISO_ALQ_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_ALQ_IS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
