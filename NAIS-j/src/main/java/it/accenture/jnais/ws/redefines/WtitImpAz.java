package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-IMP-AZ<br>
 * Variable: WTIT-IMP-AZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_IMP_AZ;
    }

    public void setWtitImpAz(AfDecimal wtitImpAz) {
        writeDecimalAsPacked(Pos.WTIT_IMP_AZ, wtitImpAz.copy());
    }

    public void setWtitImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_IMP_AZ, Pos.WTIT_IMP_AZ);
    }

    /**Original name: WTIT-IMP-AZ<br>*/
    public AfDecimal getWtitImpAz() {
        return readPackedAsDecimal(Pos.WTIT_IMP_AZ, Len.Int.WTIT_IMP_AZ, Len.Fract.WTIT_IMP_AZ);
    }

    public byte[] getWtitImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_IMP_AZ, Pos.WTIT_IMP_AZ);
        return buffer;
    }

    public void initWtitImpAzSpaces() {
        fill(Pos.WTIT_IMP_AZ, Len.WTIT_IMP_AZ, Types.SPACE_CHAR);
    }

    public void setWtitImpAzNull(String wtitImpAzNull) {
        writeString(Pos.WTIT_IMP_AZ_NULL, wtitImpAzNull, Len.WTIT_IMP_AZ_NULL);
    }

    /**Original name: WTIT-IMP-AZ-NULL<br>*/
    public String getWtitImpAzNull() {
        return readString(Pos.WTIT_IMP_AZ_NULL, Len.WTIT_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_AZ = 1;
        public static final int WTIT_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_AZ = 8;
        public static final int WTIT_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
