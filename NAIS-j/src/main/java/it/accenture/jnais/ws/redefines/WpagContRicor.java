package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-RICOR<br>
 * Variable: WPAG-CONT-RICOR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_RICOR;
    }

    public void setWpagContRicor(AfDecimal wpagContRicor) {
        writeDecimalAsPacked(Pos.WPAG_CONT_RICOR, wpagContRicor.copy());
    }

    public void setWpagContRicorFormatted(String wpagContRicor) {
        setWpagContRicor(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_RICOR + Len.Fract.WPAG_CONT_RICOR, Len.Fract.WPAG_CONT_RICOR, wpagContRicor));
    }

    public void setWpagContRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_RICOR, Pos.WPAG_CONT_RICOR);
    }

    /**Original name: WPAG-CONT-RICOR<br>*/
    public AfDecimal getWpagContRicor() {
        return readPackedAsDecimal(Pos.WPAG_CONT_RICOR, Len.Int.WPAG_CONT_RICOR, Len.Fract.WPAG_CONT_RICOR);
    }

    public byte[] getWpagContRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_RICOR, Pos.WPAG_CONT_RICOR);
        return buffer;
    }

    public void initWpagContRicorSpaces() {
        fill(Pos.WPAG_CONT_RICOR, Len.WPAG_CONT_RICOR, Types.SPACE_CHAR);
    }

    public void setWpagContRicorNull(String wpagContRicorNull) {
        writeString(Pos.WPAG_CONT_RICOR_NULL, wpagContRicorNull, Len.WPAG_CONT_RICOR_NULL);
    }

    /**Original name: WPAG-CONT-RICOR-NULL<br>*/
    public String getWpagContRicorNull() {
        return readString(Pos.WPAG_CONT_RICOR_NULL, Len.WPAG_CONT_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_RICOR = 1;
        public static final int WPAG_CONT_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_RICOR = 8;
        public static final int WPAG_CONT_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
