package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-DUR-GG<br>
 * Variable: WTGA-DUR-GG from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaDurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaDurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_DUR_GG;
    }

    public void setWtgaDurGg(int wtgaDurGg) {
        writeIntAsPacked(Pos.WTGA_DUR_GG, wtgaDurGg, Len.Int.WTGA_DUR_GG);
    }

    public void setWtgaDurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_DUR_GG, Pos.WTGA_DUR_GG);
    }

    /**Original name: WTGA-DUR-GG<br>*/
    public int getWtgaDurGg() {
        return readPackedAsInt(Pos.WTGA_DUR_GG, Len.Int.WTGA_DUR_GG);
    }

    public byte[] getWtgaDurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_DUR_GG, Pos.WTGA_DUR_GG);
        return buffer;
    }

    public void initWtgaDurGgSpaces() {
        fill(Pos.WTGA_DUR_GG, Len.WTGA_DUR_GG, Types.SPACE_CHAR);
    }

    public void setWtgaDurGgNull(String wtgaDurGgNull) {
        writeString(Pos.WTGA_DUR_GG_NULL, wtgaDurGgNull, Len.WTGA_DUR_GG_NULL);
    }

    /**Original name: WTGA-DUR-GG-NULL<br>*/
    public String getWtgaDurGgNull() {
        return readString(Pos.WTGA_DUR_GG_NULL, Len.WTGA_DUR_GG_NULL);
    }

    public String getWtgaDurGgNullFormatted() {
        return Functions.padBlanks(getWtgaDurGgNull(), Len.WTGA_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_DUR_GG = 1;
        public static final int WTGA_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_DUR_GG = 3;
        public static final int WTGA_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
