package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-AA-REN-CER<br>
 * Variable: WB03-AA-REN-CER from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03AaRenCer extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03AaRenCer() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_AA_REN_CER;
    }

    public void setWb03AaRenCer(int wb03AaRenCer) {
        writeIntAsPacked(Pos.WB03_AA_REN_CER, wb03AaRenCer, Len.Int.WB03_AA_REN_CER);
    }

    public void setWb03AaRenCerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_AA_REN_CER, Pos.WB03_AA_REN_CER);
    }

    /**Original name: WB03-AA-REN-CER<br>*/
    public int getWb03AaRenCer() {
        return readPackedAsInt(Pos.WB03_AA_REN_CER, Len.Int.WB03_AA_REN_CER);
    }

    public byte[] getWb03AaRenCerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_AA_REN_CER, Pos.WB03_AA_REN_CER);
        return buffer;
    }

    public void setWb03AaRenCerNull(String wb03AaRenCerNull) {
        writeString(Pos.WB03_AA_REN_CER_NULL, wb03AaRenCerNull, Len.WB03_AA_REN_CER_NULL);
    }

    /**Original name: WB03-AA-REN-CER-NULL<br>*/
    public String getWb03AaRenCerNull() {
        return readString(Pos.WB03_AA_REN_CER_NULL, Len.WB03_AA_REN_CER_NULL);
    }

    public String getWb03AaRenCerNullFormatted() {
        return Functions.padBlanks(getWb03AaRenCerNull(), Len.WB03_AA_REN_CER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_AA_REN_CER = 1;
        public static final int WB03_AA_REN_CER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_AA_REN_CER = 3;
        public static final int WB03_AA_REN_CER_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_AA_REN_CER = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
