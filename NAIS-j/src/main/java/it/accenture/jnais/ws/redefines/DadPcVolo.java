package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-PC-VOLO<br>
 * Variable: DAD-PC-VOLO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadPcVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadPcVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_PC_VOLO;
    }

    public void setDadPcVolo(AfDecimal dadPcVolo) {
        writeDecimalAsPacked(Pos.DAD_PC_VOLO, dadPcVolo.copy());
    }

    public void setDadPcVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_PC_VOLO, Pos.DAD_PC_VOLO);
    }

    /**Original name: DAD-PC-VOLO<br>*/
    public AfDecimal getDadPcVolo() {
        return readPackedAsDecimal(Pos.DAD_PC_VOLO, Len.Int.DAD_PC_VOLO, Len.Fract.DAD_PC_VOLO);
    }

    public byte[] getDadPcVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_PC_VOLO, Pos.DAD_PC_VOLO);
        return buffer;
    }

    public void setDadPcVoloNull(String dadPcVoloNull) {
        writeString(Pos.DAD_PC_VOLO_NULL, dadPcVoloNull, Len.DAD_PC_VOLO_NULL);
    }

    /**Original name: DAD-PC-VOLO-NULL<br>*/
    public String getDadPcVoloNull() {
        return readString(Pos.DAD_PC_VOLO_NULL, Len.DAD_PC_VOLO_NULL);
    }

    public String getDadPcVoloNullFormatted() {
        return Functions.padBlanks(getDadPcVoloNull(), Len.DAD_PC_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_PC_VOLO = 1;
        public static final int DAD_PC_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_PC_VOLO = 4;
        public static final int DAD_PC_VOLO_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_PC_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DAD_PC_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
