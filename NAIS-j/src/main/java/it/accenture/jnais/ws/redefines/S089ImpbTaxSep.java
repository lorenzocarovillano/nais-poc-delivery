package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPB-TAX-SEP<br>
 * Variable: S089-IMPB-TAX-SEP from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpbTaxSep extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpbTaxSep() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPB_TAX_SEP;
    }

    public void setWlquImpbTaxSep(AfDecimal wlquImpbTaxSep) {
        writeDecimalAsPacked(Pos.S089_IMPB_TAX_SEP, wlquImpbTaxSep.copy());
    }

    public void setWlquImpbTaxSepFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPB_TAX_SEP, Pos.S089_IMPB_TAX_SEP);
    }

    /**Original name: WLQU-IMPB-TAX-SEP<br>*/
    public AfDecimal getWlquImpbTaxSep() {
        return readPackedAsDecimal(Pos.S089_IMPB_TAX_SEP, Len.Int.WLQU_IMPB_TAX_SEP, Len.Fract.WLQU_IMPB_TAX_SEP);
    }

    public byte[] getWlquImpbTaxSepAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPB_TAX_SEP, Pos.S089_IMPB_TAX_SEP);
        return buffer;
    }

    public void initWlquImpbTaxSepSpaces() {
        fill(Pos.S089_IMPB_TAX_SEP, Len.S089_IMPB_TAX_SEP, Types.SPACE_CHAR);
    }

    public void setWlquImpbTaxSepNull(String wlquImpbTaxSepNull) {
        writeString(Pos.S089_IMPB_TAX_SEP_NULL, wlquImpbTaxSepNull, Len.WLQU_IMPB_TAX_SEP_NULL);
    }

    /**Original name: WLQU-IMPB-TAX-SEP-NULL<br>*/
    public String getWlquImpbTaxSepNull() {
        return readString(Pos.S089_IMPB_TAX_SEP_NULL, Len.WLQU_IMPB_TAX_SEP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPB_TAX_SEP = 1;
        public static final int S089_IMPB_TAX_SEP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPB_TAX_SEP = 8;
        public static final int WLQU_IMPB_TAX_SEP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_TAX_SEP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_TAX_SEP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
