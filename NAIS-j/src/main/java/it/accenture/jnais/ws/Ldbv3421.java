package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Ldbv3421TgaOutput;

/**Original name: LDBV3421<br>
 * Variable: LDBV3421 from copybook LDBV3421<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv3421 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV3421-ID-POLI
    private int ldbv3421IdPoli = DefaultValues.INT_VAL;
    //Original name: LDBV3421-ID-ADES
    private int ldbv3421IdAdes = DefaultValues.INT_VAL;
    //Original name: LDBV3421-ID-GAR
    private int ldbv3421IdGar = DefaultValues.INT_VAL;
    //Original name: LDBV3421-TP-OGG
    private String ldbv3421TpOgg = DefaultValues.stringVal(Len.LDBV3421_TP_OGG);
    //Original name: LDBV3421-TP-STAT-BUS-1
    private String ldbv3421TpStatBus1 = DefaultValues.stringVal(Len.LDBV3421_TP_STAT_BUS1);
    //Original name: LDBV3421-TP-STAT-BUS-2
    private String ldbv3421TpStatBus2 = DefaultValues.stringVal(Len.LDBV3421_TP_STAT_BUS2);
    //Original name: LDBV3421-TGA-OUTPUT
    private Ldbv3421TgaOutput ldbv3421TgaOutput = new Ldbv3421TgaOutput();
    //Original name: L3421-TP-STAT-BUS
    private String l3421TpStatBus = DefaultValues.stringVal(Len.L3421_TP_STAT_BUS);
    //Original name: L3421-TP-CAUS
    private String l3421TpCaus = DefaultValues.stringVal(Len.L3421_TP_CAUS);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV3421;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv3421Bytes(buf);
    }

    public void setLdbv3421Bytes(byte[] buffer) {
        setLdbv3421Bytes(buffer, 1);
    }

    public byte[] getLdbv3421Bytes() {
        byte[] buffer = new byte[Len.LDBV3421];
        return getLdbv3421Bytes(buffer, 1);
    }

    public void setLdbv3421Bytes(byte[] buffer, int offset) {
        int position = offset;
        setLdbv3421DatiInputBytes(buffer, position);
        position += Len.LDBV3421_DATI_INPUT;
        ldbv3421TgaOutput.setLdbv3421TgaOutputBytes(buffer, position);
        position += Ldbv3421TgaOutput.Len.LDBV3421_TGA_OUTPUT;
        setLdbv3421StbOutputBytes(buffer, position);
    }

    public byte[] getLdbv3421Bytes(byte[] buffer, int offset) {
        int position = offset;
        getLdbv3421DatiInputBytes(buffer, position);
        position += Len.LDBV3421_DATI_INPUT;
        ldbv3421TgaOutput.getLdbv3421TgaOutputBytes(buffer, position);
        position += Ldbv3421TgaOutput.Len.LDBV3421_TGA_OUTPUT;
        getLdbv3421StbOutputBytes(buffer, position);
        return buffer;
    }

    public void setLdbv3421DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv3421IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV3421_ID_POLI, 0);
        position += Len.LDBV3421_ID_POLI;
        ldbv3421IdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV3421_ID_ADES, 0);
        position += Len.LDBV3421_ID_ADES;
        ldbv3421IdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV3421_ID_GAR, 0);
        position += Len.LDBV3421_ID_GAR;
        ldbv3421TpOgg = MarshalByte.readString(buffer, position, Len.LDBV3421_TP_OGG);
        position += Len.LDBV3421_TP_OGG;
        ldbv3421TpStatBus1 = MarshalByte.readString(buffer, position, Len.LDBV3421_TP_STAT_BUS1);
        position += Len.LDBV3421_TP_STAT_BUS1;
        ldbv3421TpStatBus2 = MarshalByte.readString(buffer, position, Len.LDBV3421_TP_STAT_BUS2);
    }

    public byte[] getLdbv3421DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv3421IdPoli, Len.Int.LDBV3421_ID_POLI, 0);
        position += Len.LDBV3421_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv3421IdAdes, Len.Int.LDBV3421_ID_ADES, 0);
        position += Len.LDBV3421_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv3421IdGar, Len.Int.LDBV3421_ID_GAR, 0);
        position += Len.LDBV3421_ID_GAR;
        MarshalByte.writeString(buffer, position, ldbv3421TpOgg, Len.LDBV3421_TP_OGG);
        position += Len.LDBV3421_TP_OGG;
        MarshalByte.writeString(buffer, position, ldbv3421TpStatBus1, Len.LDBV3421_TP_STAT_BUS1);
        position += Len.LDBV3421_TP_STAT_BUS1;
        MarshalByte.writeString(buffer, position, ldbv3421TpStatBus2, Len.LDBV3421_TP_STAT_BUS2);
        return buffer;
    }

    public void setLdbv3421IdPoli(int ldbv3421IdPoli) {
        this.ldbv3421IdPoli = ldbv3421IdPoli;
    }

    public int getLdbv3421IdPoli() {
        return this.ldbv3421IdPoli;
    }

    public void setLdbv3421IdAdes(int ldbv3421IdAdes) {
        this.ldbv3421IdAdes = ldbv3421IdAdes;
    }

    public int getLdbv3421IdAdes() {
        return this.ldbv3421IdAdes;
    }

    public void setLdbv3421IdGar(int ldbv3421IdGar) {
        this.ldbv3421IdGar = ldbv3421IdGar;
    }

    public int getLdbv3421IdGar() {
        return this.ldbv3421IdGar;
    }

    public void setLdbv3421TpOgg(String ldbv3421TpOgg) {
        this.ldbv3421TpOgg = Functions.subString(ldbv3421TpOgg, Len.LDBV3421_TP_OGG);
    }

    public String getLdbv3421TpOgg() {
        return this.ldbv3421TpOgg;
    }

    public void setLdbv3421TpStatBus1(String ldbv3421TpStatBus1) {
        this.ldbv3421TpStatBus1 = Functions.subString(ldbv3421TpStatBus1, Len.LDBV3421_TP_STAT_BUS1);
    }

    public String getLdbv3421TpStatBus1() {
        return this.ldbv3421TpStatBus1;
    }

    public void setLdbv3421TpStatBus2(String ldbv3421TpStatBus2) {
        this.ldbv3421TpStatBus2 = Functions.subString(ldbv3421TpStatBus2, Len.LDBV3421_TP_STAT_BUS2);
    }

    public String getLdbv3421TpStatBus2() {
        return this.ldbv3421TpStatBus2;
    }

    public void setLdbv3421StbOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        l3421TpStatBus = MarshalByte.readString(buffer, position, Len.L3421_TP_STAT_BUS);
        position += Len.L3421_TP_STAT_BUS;
        l3421TpCaus = MarshalByte.readString(buffer, position, Len.L3421_TP_CAUS);
    }

    public byte[] getLdbv3421StbOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, l3421TpStatBus, Len.L3421_TP_STAT_BUS);
        position += Len.L3421_TP_STAT_BUS;
        MarshalByte.writeString(buffer, position, l3421TpCaus, Len.L3421_TP_CAUS);
        return buffer;
    }

    public void setL3421TpStatBus(String l3421TpStatBus) {
        this.l3421TpStatBus = Functions.subString(l3421TpStatBus, Len.L3421_TP_STAT_BUS);
    }

    public String getL3421TpStatBus() {
        return this.l3421TpStatBus;
    }

    public void setL3421TpCaus(String l3421TpCaus) {
        this.l3421TpCaus = Functions.subString(l3421TpCaus, Len.L3421_TP_CAUS);
    }

    public String getL3421TpCaus() {
        return this.l3421TpCaus;
    }

    public Ldbv3421TgaOutput getLdbv3421TgaOutput() {
        return ldbv3421TgaOutput;
    }

    @Override
    public byte[] serialize() {
        return getLdbv3421Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV3421_ID_POLI = 5;
        public static final int LDBV3421_ID_ADES = 5;
        public static final int LDBV3421_ID_GAR = 5;
        public static final int LDBV3421_TP_OGG = 2;
        public static final int LDBV3421_TP_STAT_BUS1 = 2;
        public static final int LDBV3421_TP_STAT_BUS2 = 2;
        public static final int LDBV3421_DATI_INPUT = LDBV3421_ID_POLI + LDBV3421_ID_ADES + LDBV3421_ID_GAR + LDBV3421_TP_OGG + LDBV3421_TP_STAT_BUS1 + LDBV3421_TP_STAT_BUS2;
        public static final int L3421_TP_STAT_BUS = 2;
        public static final int L3421_TP_CAUS = 2;
        public static final int LDBV3421_STB_OUTPUT = L3421_TP_STAT_BUS + L3421_TP_CAUS;
        public static final int LDBV3421 = LDBV3421_DATI_INPUT + Ldbv3421TgaOutput.Len.LDBV3421_TGA_OUTPUT + LDBV3421_STB_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV3421_ID_POLI = 9;
            public static final int LDBV3421_ID_ADES = 9;
            public static final int LDBV3421_ID_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
