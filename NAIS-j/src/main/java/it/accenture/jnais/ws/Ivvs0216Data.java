package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.AdesIvvs0216;
import it.accenture.jnais.copy.AreaLdbv1131;
import it.accenture.jnais.copy.Bnfic;
import it.accenture.jnais.copy.BnficrLiq;
import it.accenture.jnais.copy.CnbtNded;
import it.accenture.jnais.copy.DColl;
import it.accenture.jnais.copy.DCrist;
import it.accenture.jnais.copy.DettQuest;
import it.accenture.jnais.copy.DettTitCont;
import it.accenture.jnais.copy.DFiscAdes;
import it.accenture.jnais.copy.DfltAdes;
import it.accenture.jnais.copy.DForzLiq;
import it.accenture.jnais.copy.EstPoliCpiPr;
import it.accenture.jnais.copy.EstRappAna;
import it.accenture.jnais.copy.EstTrchDiGar;
import it.accenture.jnais.copy.GarIvvs0216;
import it.accenture.jnais.copy.GarLiq;
import it.accenture.jnais.copy.Iabc0010;
import it.accenture.jnais.copy.Idsi0021Area;
import it.accenture.jnais.copy.Idso0021;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.ImpstSost;
import it.accenture.jnais.copy.Ivvc0218Ivvs0216;
import it.accenture.jnais.copy.Ivvc0221;
import it.accenture.jnais.copy.Lccvgrzz;
import it.accenture.jnais.copy.Lccvpco1;
import it.accenture.jnais.copy.Ldbv0011;
import it.accenture.jnais.copy.Ldbv1291;
import it.accenture.jnais.copy.Ldbv1471;
import it.accenture.jnais.copy.Ldbv2171;
import it.accenture.jnais.copy.Ldbv2321;
import it.accenture.jnais.copy.Ldbv5001;
import it.accenture.jnais.copy.Liq;
import it.accenture.jnais.copy.Movi;
import it.accenture.jnais.copy.MoviFinrio;
import it.accenture.jnais.copy.OggCollg;
import it.accenture.jnais.copy.ParamComp;
import it.accenture.jnais.copy.ParamMovi;
import it.accenture.jnais.copy.PercLiq;
import it.accenture.jnais.copy.Poli;
import it.accenture.jnais.copy.Prest;
import it.accenture.jnais.copy.ProvDiGar;
import it.accenture.jnais.copy.Quest;
import it.accenture.jnais.copy.RappAna;
import it.accenture.jnais.copy.RappRete;
import it.accenture.jnais.copy.ReinvstPoliLq;
import it.accenture.jnais.copy.Rich;
import it.accenture.jnais.copy.RichEst;
import it.accenture.jnais.copy.RichInvstFnd;
import it.accenture.jnais.copy.RisDiTrch;
import it.accenture.jnais.copy.SoprDiGar;
import it.accenture.jnais.copy.StraDiInvst;
import it.accenture.jnais.copy.TitCont;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.copy.TrchLiq;
import it.accenture.jnais.copy.VincPeg;
import it.accenture.jnais.copy.WpcoDati;
import it.accenture.jnais.ws.enums.FlagGestione;
import it.accenture.jnais.ws.enums.FlagIdTrovato;
import it.accenture.jnais.ws.enums.FlagSkedaOpzione;
import it.accenture.jnais.ws.enums.FlagVarInChiaro;
import it.accenture.jnais.ws.enums.WcomFlagOverflow;
import it.accenture.jnais.ws.enums.WkAppAssicurato;
import it.accenture.jnais.ws.enums.WkCalcoli;
import it.accenture.jnais.ws.enums.WkCallCalcolo;
import it.accenture.jnais.ws.enums.WkCodLivelloFlag;
import it.accenture.jnais.ws.enums.WkContesto;
import it.accenture.jnais.ws.enums.WkContestoPog;
import it.accenture.jnais.ws.enums.WkDerogaOk;
import it.accenture.jnais.ws.enums.WkErrore;
import it.accenture.jnais.ws.enums.WkFindCampo;
import it.accenture.jnais.ws.enums.WkFindFiglia;
import it.accenture.jnais.ws.enums.WkFindIntern;
import it.accenture.jnais.ws.enums.WkFindLetto;
import it.accenture.jnais.ws.enums.WkGestioneCall;
import it.accenture.jnais.ws.enums.WkIdCodLivFlag;
import it.accenture.jnais.ws.enums.WkIdLivelloFlag;
import it.accenture.jnais.ws.enums.WkLeturaMmv;
import it.accenture.jnais.ws.enums.WkOutScritto;
import it.accenture.jnais.ws.enums.WksTrovatoTabbIvvs0216;
import it.accenture.jnais.ws.enums.WkValoreRis;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.occurs.We12TabEstTrch;
import it.accenture.jnais.ws.occurs.Wk1TabStringheTot;
import it.accenture.jnais.ws.occurs.WksAreaTabb;
import it.accenture.jnais.ws.occurs.WtgaTabTran;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IVVS0216<br>
 * Generated as a class for rule WS.<br>*/
public class Ivvs0216Data {

    //==== PROPERTIES ====
    public static final int WK1_TAB_STRINGHE_TOT_MAXOCCURS = 75;
    public static final int WKS_AREA_TABB_MAXOCCURS = 60;
    public static final int WE12_TAB_EST_TRCH_MAXOCCURS = 20;
    public static final int WTGA_TAB_TRAN_MAXOCCURS = 20;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "IVVS0216";
    //Original name: WKS-INDICI
    private WksIndici wksIndici = new WksIndici();
    //Original name: LCCVGRZZ
    private Lccvgrzz lccvgrzz = new Lccvgrzz();
    //Original name: IX-APPO-OVERF
    private int ixAppoOverf = 0;
    //Original name: WK-APPO-ERR
    private String wkAppoErr = "";
    //Original name: WK-APPO-TP-OGG
    private String wkAppoTpOgg = "";
    //Original name: WK-COD-CAN
    private int wkCodCan = 0;
    //Original name: WKS-AREA-TABB-APPO
    private String wksAreaTabbAppo = "";
    //Original name: WKS-APPO-IMP
    private AfDecimal wksAppoImp = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);
    //Original name: WKS-APPO-PER
    private AfDecimal wksAppoPer = new AfDecimal(DefaultValues.DEC_VAL, 11, 6);
    //Original name: WKS-APPO-DATA
    private String wksAppoData = DefaultValues.stringVal(Len.WKS_APPO_DATA);
    //Original name: WKS-APPO-STR
    private String wksAppoStr = DefaultValues.stringVal(Len.WKS_APPO_STR);
    //Original name: WKS-APPO-STOR
    private String wksAppoStor = DefaultValues.stringVal(Len.WKS_APPO_STOR);
    //Original name: WKS-COD-SOPR
    private String wksCodSopr = DefaultValues.stringVal(Len.WKS_COD_SOPR);
    //Original name: WK-ID-ASSIC
    private int wkIdAssic = DefaultValues.INT_VAL;
    //Original name: WK-ID-OGG
    private int wkIdOgg = DefaultValues.INT_VAL;
    //Original name: WK-TP-OGG
    private String wkTpOgg = "";
    //Original name: WK-COD-VARIABILE
    private String wkCodVariabile = "";
    //Original name: FXT-ADDRESS
    private int fxtAddress = DefaultValues.BIN_INT_VAL;
    //Original name: VXG-ADDRESS
    private int vxgAddress = DefaultValues.BIN_INT_VAL;
    //Original name: WK-APPO-LUNGHEZZA
    private int wkAppoLunghezza = DefaultValues.INT_VAL;
    //Original name: WK-LUNGHEZZA-TOT
    private int wkLunghezzaTot = DefaultValues.INT_VAL;
    //Original name: WK1-MAX-TAB-STRING
    private short wk1MaxTabString = DefaultValues.BIN_SHORT_VAL;
    //Original name: WK1-TAB-STRINGHE-TOT
    private Wk1TabStringheTot[] wk1TabStringheTot = new Wk1TabStringheTot[WK1_TAB_STRINGHE_TOT_MAXOCCURS];
    //Original name: IVVC0501-OUTPUT
    private Ivvc0501Output ivvc0501Output = new Ivvc0501Output();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0216 ivvc0218 = new Ivvc0218Ivvs0216();
    //Original name: AREA-IO-CALCOLI
    private Ivvc0213 ivvc0213 = new Ivvc0213();
    //Original name: IVVC0221
    private Ivvc0221 ivvc0221 = new Ivvc0221();
    /**Original name: FLAG-SKEDA-OPZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *   FLAGS
	 * ----------------------------------------------------------------*</pre>*/
    private FlagSkedaOpzione flagSkedaOpzione = new FlagSkedaOpzione();
    //Original name: ADES
    private AdesIvvs0216 ades = new AdesIvvs0216();
    //Original name: BNFIC
    private Bnfic bnfic = new Bnfic();
    //Original name: IMPST-SOST
    private ImpstSost impstSost = new ImpstSost();
    //Original name: CNBT-NDED
    private CnbtNded cnbtNded = new CnbtNded();
    //Original name: D-COLL
    private DColl dColl = new DColl();
    //Original name: D-FISC-ADES
    private DFiscAdes dFiscAdes = new DFiscAdes();
    //Original name: DETT-QUEST
    private DettQuest dettQuest = new DettQuest();
    //Original name: DETT-TIT-CONT
    private DettTitCont dettTitCont = new DettTitCont();
    //Original name: GAR
    private GarIvvs0216 gar = new GarIvvs0216();
    //Original name: MOVI
    private Movi movi = new Movi();
    //Original name: PARAM-COMP
    private ParamComp paramComp = new ParamComp();
    //Original name: PARAM-MOVI
    private ParamMovi paramMovi = new ParamMovi();
    //Original name: PARAM-OGG
    private ParamOgg paramOgg = new ParamOgg();
    //Original name: POLI
    private Poli poli = new Poli();
    //Original name: PROV-DI-GAR
    private ProvDiGar provDiGar = new ProvDiGar();
    //Original name: QUEST
    private Quest quest = new Quest();
    //Original name: RAPP-ANA
    private RappAna rappAna = new RappAna();
    //Original name: EST-RAPP-ANA
    private EstRappAna estRappAna = new EstRappAna();
    //Original name: RAPP-RETE
    private RappRete rappRete = new RappRete();
    //Original name: RICH
    private Rich rich = new Rich();
    //Original name: RIS-DI-TRCH
    private RisDiTrch risDiTrch = new RisDiTrch();
    //Original name: SOPR-DI-GAR
    private SoprDiGar soprDiGar = new SoprDiGar();
    //Original name: STRA-DI-INVST
    private StraDiInvst straDiInvst = new StraDiInvst();
    //Original name: TIT-CONT
    private TitCont titCont = new TitCont();
    //Original name: TRCH-DI-GAR
    private TrchDiGarIvvs0216 trchDiGar = new TrchDiGarIvvs0216();
    //Original name: BNFICR-LIQ
    private BnficrLiq bnficrLiq = new BnficrLiq();
    //Original name: LIQ
    private Liq liq = new Liq();
    //Original name: DFLT-ADES
    private DfltAdes dfltAdes = new DfltAdes();
    //Original name: PREST
    private Prest prest = new Prest();
    //Original name: MOVI-FINRIO
    private MoviFinrio moviFinrio = new MoviFinrio();
    //Original name: RICH-INVST-FND
    private RichInvstFnd richInvstFnd = new RichInvstFnd();
    //Original name: OGG-COLLG
    private OggCollg oggCollg = new OggCollg();
    //Original name: TRCH-LIQ
    private TrchLiq trchLiq = new TrchLiq();
    //Original name: PERC-LIQ
    private PercLiq percLiq = new PercLiq();
    //Original name: GAR-LIQ
    private GarLiq garLiq = new GarLiq();
    //Original name: D-FORZ-LIQ
    private DForzLiq dForzLiq = new DForzLiq();
    //Original name: COMP-QUEST
    private CompQuest compQuest = new CompQuest();
    //Original name: DOMANDA-COLLG
    private DomandaCollg domandaCollg = new DomandaCollg();
    //Original name: EST-TRCH-DI-GAR
    private EstTrchDiGar estTrchDiGar = new EstTrchDiGar();
    //Original name: REINVST-POLI-LQ
    private ReinvstPoliLq reinvstPoliLq = new ReinvstPoliLq();
    //Original name: VINC-PEG
    private VincPeg vincPeg = new VincPeg();
    //Original name: D-CRIST
    private DCrist dCrist = new DCrist();
    //Original name: EST-POLI-CPI-PR
    private EstPoliCpiPr estPoliCpiPr = new EstPoliCpiPr();
    //Original name: RICH-EST
    private RichEst richEst = new RichEst();
    //Original name: AREA-IDSV0141
    private Idss0140Link areaIdsv0141 = new Idss0140Link();
    /**Original name: WCOM-FLAG-OVERFLOW<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY INPUT PER LA LETTURA IN PTF
	 * ----------------------------------------------------------------*</pre>*/
    private WcomFlagOverflow wcomFlagOverflow = new WcomFlagOverflow();
    //Original name: WK-COD-LIVELLO-FLAG
    private WkCodLivelloFlag wkCodLivelloFlag = new WkCodLivelloFlag();
    //Original name: WK-ID-COD-LIV-FLAG
    private WkIdCodLivFlag wkIdCodLivFlag = new WkIdCodLivFlag();
    //Original name: WK-ID-LIVELLO-FLAG
    private WkIdLivelloFlag wkIdLivelloFlag = new WkIdLivelloFlag();
    //Original name: WK-CALCOLI
    private WkCalcoli wkCalcoli = new WkCalcoli();
    //Original name: WK-FIND-CAMPO
    private WkFindCampo wkFindCampo = new WkFindCampo();
    //Original name: WK-FIND-INTERN
    private WkFindIntern wkFindIntern = new WkFindIntern();
    //Original name: WK-FIND-LETTO
    private WkFindLetto wkFindLetto = new WkFindLetto();
    //Original name: WK-FIND-FIGLIA
    private WkFindFiglia wkFindFiglia = new WkFindFiglia();
    //Original name: WK-DEROGA-OK
    private WkDerogaOk wkDerogaOk = new WkDerogaOk();
    //Original name: WK-GESTIONE-CALL
    private WkGestioneCall wkGestioneCall = new WkGestioneCall();
    //Original name: WK-CONTESTO
    private WkContesto wkContesto = new WkContesto();
    //Original name: WK-LETURA-MMV
    private WkLeturaMmv wkLeturaMmv = new WkLeturaMmv();
    //Original name: WK-CONTESTO-POG
    private WkContestoPog wkContestoPog = new WkContestoPog();
    //Original name: WK-CALL-CALCOLO
    private WkCallCalcolo wkCallCalcolo = new WkCallCalcolo();
    //Original name: WK-APP-ASSICURATO
    private WkAppAssicurato wkAppAssicurato = new WkAppAssicurato();
    //Original name: WK-ERRORE
    private WkErrore wkErrore = new WkErrore();
    //Original name: WK-VALORE-RIS
    private WkValoreRis wkValoreRis = new WkValoreRis();
    //Original name: WK-OUT-SCRITTO
    private WkOutScritto wkOutScritto = new WkOutScritto();
    //Original name: WK-COD-DOM
    private String wkCodDom = "";
    //Original name: WK-TP-QUEST
    private String wkTpQuest = "";
    //Original name: FLAG-ID-TROVATO
    private FlagIdTrovato flagIdTrovato = new FlagIdTrovato();
    //Original name: FLAG-GESTIONE
    private FlagGestione flagGestione = new FlagGestione();
    //Original name: FLAG-VAR-IN-CHIARO
    private FlagVarInChiaro flagVarInChiaro = new FlagVarInChiaro();
    //Original name: IABC0010
    private Iabc0010 iabc0010 = new Iabc0010();
    //Original name: IX-ADDRESS
    private int ixAddress = DefaultValues.BIN_INT_VAL;
    //Original name: WK-ELE-MAX-ACTU
    private int wkEleMaxActu = DefaultValues.INT_VAL;
    //Original name: WK-IND-SERV
    private long wkIndServ = DefaultValues.BIN_LONG_VAL;
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IDSI0021-AREA
    private Idsi0021Area idsi0021Area = new Idsi0021Area();
    //Original name: IDSO0021
    private Idso0021 idso0021 = new Idso0021();
    //Original name: V1391-AREA-ACTU
    private V1391AreaActu v1391AreaActu = new V1391AreaActu();
    //Original name: LDBV1471
    private Ldbv1471 ldbv1471 = new Ldbv1471();
    //Original name: LDBV1291
    private Ldbv1291 ldbv1291 = new Ldbv1291();
    //Original name: LDBV2171
    private Ldbv2171 ldbv2171 = new Ldbv2171();
    //Original name: LDBV5001
    private Ldbv5001 ldbv5001 = new Ldbv5001();
    //Original name: LDBV2321
    private Ldbv2321 ldbv2321 = new Ldbv2321();
    //Original name: LDBV0011
    private Ldbv0011 ldbv0011 = new Ldbv0011();
    //Original name: AREA-LDBV1131
    private AreaLdbv1131 areaLdbv1131 = new AreaLdbv1131();
    //Original name: AREA-CALL
    private AreaCall areaCall = new AreaCall();
    /**Original name: WKS-NOME-TABELLA<br>
	 * <pre>----------------------------------------------------------------*
	 *     MODULI CHIAMATI
	 * ----------------------------------------------------------------*</pre>*/
    private String wksNomeTabella = DefaultValues.stringVal(Len.WKS_NOME_TABELLA);
    //Original name: PGM-CHIAMATO
    private String pgmChiamato = "";
    //Original name: PGM-CALCOLI
    private String pgmCalcoli = "";
    //Original name: WK-PARAM-OGG-APPO
    private String wkParamOggAppo = "";
    //Original name: WKS-ELE-MAX-TABB
    private short wksEleMaxTabb = DefaultValues.SHORT_VAL;
    //Original name: WKS-AREA-TABB
    private WksAreaTabb[] wksAreaTabb = new WksAreaTabb[WKS_AREA_TABB_MAXOCCURS];
    //Original name: WKS-TROVATO-TABB
    private WksTrovatoTabbIvvs0216 wksTrovatoTabb = new WksTrovatoTabbIvvs0216();
    //Original name: WTGA-ELE-TRAN-MAX
    private short wtgaEleTranMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTGA-TAB-TRAN
    private WtgaTabTran[] wtgaTabTran = new WtgaTabTran[WTGA_TAB_TRAN_MAXOCCURS];
    //Original name: WPCO-ELE-PCO-MAX
    private short wpcoElePcoMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPCO1
    private Lccvpco1 lccvpco1 = new Lccvpco1();
    //Original name: WE12-ELE-ESTRA-MAX
    private short we12EleEstraMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WE12-TAB-EST-TRCH
    private We12TabEstTrch[] we12TabEstTrch = new We12TabEstTrch[WE12_TAB_EST_TRCH_MAXOCCURS];
    //Original name: IDSV0102-SEARCH-INDEX
    private int idsv0102SearchIndex = 1;
    //Original name: IDSV0102A-SEARCH-INDEX
    private int idsv0102aSearchIndex = 1;
    //Original name: INT-REGISTER1
    private int intRegister1 = DefaultValues.INT_VAL;

    //==== CONSTRUCTORS ====
    public Ivvs0216Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wk1TabStringheTotIdx = 1; wk1TabStringheTotIdx <= WK1_TAB_STRINGHE_TOT_MAXOCCURS; wk1TabStringheTotIdx++) {
            wk1TabStringheTot[wk1TabStringheTotIdx - 1] = new Wk1TabStringheTot();
        }
        for (int wksAreaTabbIdx = 1; wksAreaTabbIdx <= WKS_AREA_TABB_MAXOCCURS; wksAreaTabbIdx++) {
            wksAreaTabb[wksAreaTabbIdx - 1] = new WksAreaTabb();
        }
        for (int wtgaTabTranIdx = 1; wtgaTabTranIdx <= WTGA_TAB_TRAN_MAXOCCURS; wtgaTabTranIdx++) {
            wtgaTabTran[wtgaTabTranIdx - 1] = new WtgaTabTran();
        }
        for (int we12TabEstTrchIdx = 1; we12TabEstTrchIdx <= WE12_TAB_EST_TRCH_MAXOCCURS; we12TabEstTrchIdx++) {
            we12TabEstTrch[we12TabEstTrchIdx - 1] = new We12TabEstTrch();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setIxAppoOverf(int ixAppoOverf) {
        this.ixAppoOverf = ixAppoOverf;
    }

    public int getIxAppoOverf() {
        return this.ixAppoOverf;
    }

    public void setWkAppoErr(String wkAppoErr) {
        this.wkAppoErr = Functions.subString(wkAppoErr, Len.WK_APPO_ERR);
    }

    public String getWkAppoErr() {
        return this.wkAppoErr;
    }

    public String getWkAppoErrFormatted() {
        return Functions.padBlanks(getWkAppoErr(), Len.WK_APPO_ERR);
    }

    public void setWkAppoTpOgg(String wkAppoTpOgg) {
        this.wkAppoTpOgg = Functions.subString(wkAppoTpOgg, Len.WK_APPO_TP_OGG);
    }

    public String getWkAppoTpOgg() {
        return this.wkAppoTpOgg;
    }

    public int getWkCodCan() {
        return this.wkCodCan;
    }

    public void setWksAreaTabbAppo(String wksAreaTabbAppo) {
        this.wksAreaTabbAppo = Functions.subString(wksAreaTabbAppo, Len.WKS_AREA_TABB_APPO);
    }

    public String getWksAreaTabbAppo() {
        return this.wksAreaTabbAppo;
    }

    public String getWksAreaTabbAppoFormatted() {
        return Functions.padBlanks(getWksAreaTabbAppo(), Len.WKS_AREA_TABB_APPO);
    }

    public void setWksAppoImp(AfDecimal wksAppoImp) {
        this.wksAppoImp.assign(wksAppoImp);
    }

    public AfDecimal getWksAppoImp() {
        return this.wksAppoImp.copy();
    }

    public void setWksAppoPer(AfDecimal wksAppoPer) {
        this.wksAppoPer.assign(wksAppoPer);
    }

    public AfDecimal getWksAppoPer() {
        return this.wksAppoPer.copy();
    }

    public void setWksAppoData(int wksAppoData) {
        this.wksAppoData = NumericDisplay.asString(wksAppoData, Len.WKS_APPO_DATA);
    }

    public int getWksAppoData() {
        return NumericDisplay.asInt(this.wksAppoData);
    }

    public String getWksAppoDataFormatted() {
        return this.wksAppoData;
    }

    public void setWksAppoStr(String wksAppoStr) {
        this.wksAppoStr = Functions.subString(wksAppoStr, Len.WKS_APPO_STR);
    }

    public String getWksAppoStr() {
        return this.wksAppoStr;
    }

    public void setWksAppoStor(String wksAppoStor) {
        this.wksAppoStor = Functions.subString(wksAppoStor, Len.WKS_APPO_STOR);
    }

    public String getWksAppoStor() {
        return this.wksAppoStor;
    }

    public void setWksCodSopr(String wksCodSopr) {
        this.wksCodSopr = Functions.subString(wksCodSopr, Len.WKS_COD_SOPR);
    }

    public String getWksCodSopr() {
        return this.wksCodSopr;
    }

    public void setWkIdAssic(int wkIdAssic) {
        this.wkIdAssic = wkIdAssic;
    }

    public int getWkIdAssic() {
        return this.wkIdAssic;
    }

    public void setWkIdOgg(int wkIdOgg) {
        this.wkIdOgg = wkIdOgg;
    }

    public int getWkIdOgg() {
        return this.wkIdOgg;
    }

    public void setWkTpOgg(String wkTpOgg) {
        this.wkTpOgg = Functions.subString(wkTpOgg, Len.WK_TP_OGG);
    }

    public String getWkTpOgg() {
        return this.wkTpOgg;
    }

    public void setWkCodVariabile(String wkCodVariabile) {
        this.wkCodVariabile = Functions.subString(wkCodVariabile, Len.WK_COD_VARIABILE);
    }

    public String getWkCodVariabile() {
        return this.wkCodVariabile;
    }

    public String getAreaPointerFormatted() {
        return MarshalByteExt.bufferToStr(getAreaPointerBytes());
    }

    /**Original name: AREA-POINTER<br>*/
    public byte[] getAreaPointerBytes() {
        byte[] buffer = new byte[Len.AREA_POINTER];
        return getAreaPointerBytes(buffer, 1);
    }

    public byte[] getAreaPointerBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, fxtAddress);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, vxgAddress);
        return buffer;
    }

    public void setFxtAddress(int fxtAddress) {
        this.fxtAddress = fxtAddress;
    }

    public int getFxtAddress() {
        return this.fxtAddress;
    }

    public void setVxgAddress(int vxgAddress) {
        this.vxgAddress = vxgAddress;
    }

    public int getVxgAddress() {
        return this.vxgAddress;
    }

    public void setWkAppoLunghezza(int wkAppoLunghezza) {
        this.wkAppoLunghezza = wkAppoLunghezza;
    }

    public int getWkAppoLunghezza() {
        return this.wkAppoLunghezza;
    }

    public void setWkLunghezzaTot(int wkLunghezzaTot) {
        this.wkLunghezzaTot = wkLunghezzaTot;
    }

    public int getWkLunghezzaTot() {
        return this.wkLunghezzaTot;
    }

    public void setWk1MaxTabString(short wk1MaxTabString) {
        this.wk1MaxTabString = wk1MaxTabString;
    }

    public short getWk1MaxTabString() {
        return this.wk1MaxTabString;
    }

    public void setWkCodDom(String wkCodDom) {
        this.wkCodDom = Functions.subString(wkCodDom, Len.WK_COD_DOM);
    }

    public String getWkCodDom() {
        return this.wkCodDom;
    }

    public void setWkTpQuest(String wkTpQuest) {
        this.wkTpQuest = Functions.subString(wkTpQuest, Len.WK_TP_QUEST);
    }

    public String getWkTpQuest() {
        return this.wkTpQuest;
    }

    public void setIxAddress(int ixAddress) {
        this.ixAddress = ixAddress;
    }

    public int getIxAddress() {
        return this.ixAddress;
    }

    public void setWkEleMaxActu(int wkEleMaxActu) {
        this.wkEleMaxActu = wkEleMaxActu;
    }

    public int getWkEleMaxActu() {
        return this.wkEleMaxActu;
    }

    public void setWkIndServ(long wkIndServ) {
        this.wkIndServ = wkIndServ;
    }

    public long getWkIndServ() {
        return this.wkIndServ;
    }

    /**Original name: INPUT-IDSS0020<br>
	 * <pre>   fine COPY IDSV0006</pre>*/
    public byte[] getInputIdss0020Bytes() {
        byte[] buffer = new byte[Len.INPUT_IDSS0020];
        return getInputIdss0020Bytes(buffer, 1);
    }

    public byte[] getInputIdss0020Bytes(byte[] buffer, int offset) {
        int position = offset;
        idsi0021Area.getIdsi0021AreaBytes(buffer, position);
        return buffer;
    }

    public void initInputIdss0020Spaces() {
        idsi0021Area.initIdsi0021AreaSpaces();
    }

    public void setOutputIdss0020Bytes(byte[] buffer) {
        setOutputIdss0020Bytes(buffer, 1);
    }

    public void setOutputIdss0020Bytes(byte[] buffer, int offset) {
        int position = offset;
        idso0021.setIdso0021AreaBytes(buffer, position);
    }

    public void initOutputIdss0020Spaces() {
        idso0021.initIdso0021Spaces();
    }

    public void setWksNomeTabella(String wksNomeTabella) {
        this.wksNomeTabella = Functions.subString(wksNomeTabella, Len.WKS_NOME_TABELLA);
    }

    public String getWksNomeTabella() {
        return this.wksNomeTabella;
    }

    public String getWksNomeTabellaFormatted() {
        return Functions.padBlanks(getWksNomeTabella(), Len.WKS_NOME_TABELLA);
    }

    public void setPgmChiamato(String pgmChiamato) {
        this.pgmChiamato = Functions.subString(pgmChiamato, Len.PGM_CHIAMATO);
    }

    public String getPgmChiamato() {
        return this.pgmChiamato;
    }

    public String getPgmChiamatoFormatted() {
        return Functions.padBlanks(getPgmChiamato(), Len.PGM_CHIAMATO);
    }

    public void setPgmCalcoli(String pgmCalcoli) {
        this.pgmCalcoli = Functions.subString(pgmCalcoli, Len.PGM_CALCOLI);
    }

    public String getPgmCalcoli() {
        return this.pgmCalcoli;
    }

    public void setWkParamOggAppo(String wkParamOggAppo) {
        this.wkParamOggAppo = Functions.subString(wkParamOggAppo, Len.WK_PARAM_OGG_APPO);
    }

    public String getWkParamOggAppo() {
        return this.wkParamOggAppo;
    }

    public String getWkParamOggAppoFormatted() {
        return Functions.padBlanks(getWkParamOggAppo(), Len.WK_PARAM_OGG_APPO);
    }

    public void setWksEleMaxTabb(short wksEleMaxTabb) {
        this.wksEleMaxTabb = wksEleMaxTabb;
    }

    public short getWksEleMaxTabb() {
        return this.wksEleMaxTabb;
    }

    public String getWtgaAreaTrancheFormatted() {
        return MarshalByteExt.bufferToStr(getWtgaAreaTrancheBytes());
    }

    /**Original name: WTGA-AREA-TRANCHE<br>
	 * <pre>--  AREA TRANCHE DI GARANZIA</pre>*/
    public byte[] getWtgaAreaTrancheBytes() {
        byte[] buffer = new byte[Len.WTGA_AREA_TRANCHE];
        return getWtgaAreaTrancheBytes(buffer, 1);
    }

    public byte[] getWtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtgaEleTranMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTGA_TAB_TRAN_MAXOCCURS; idx++) {
            wtgaTabTran[idx - 1].getTabTranBytes(buffer, position);
            position += WtgaTabTran.Len.TAB_TRAN;
        }
        return buffer;
    }

    public void setWtgaEleTranMax(short wtgaEleTranMax) {
        this.wtgaEleTranMax = wtgaEleTranMax;
    }

    public short getWtgaEleTranMax() {
        return this.wtgaEleTranMax;
    }

    public String getWpcoAreaParaComFormatted() {
        return MarshalByteExt.bufferToStr(getWpcoAreaParaComBytes());
    }

    /**Original name: WPCO-AREA-PARA-COM<br>*/
    public byte[] getWpcoAreaParaComBytes() {
        byte[] buffer = new byte[Len.WPCO_AREA_PARA_COM];
        return getWpcoAreaParaComBytes(buffer, 1);
    }

    public byte[] getWpcoAreaParaComBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpcoElePcoMax);
        position += Types.SHORT_SIZE;
        getWpcoTabPcoBytes(buffer, position);
        return buffer;
    }

    public void setWpcoElePcoMax(short wpcoElePcoMax) {
        this.wpcoElePcoMax = wpcoElePcoMax;
    }

    public short getWpcoElePcoMax() {
        return this.wpcoElePcoMax;
    }

    public byte[] getWpcoTabPcoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvpco1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        lccvpco1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public String getWe12AreaEstraFormatted() {
        return MarshalByteExt.bufferToStr(getWe12AreaEstraBytes());
    }

    /**Original name: WE12-AREA-ESTRA<br>*/
    public byte[] getWe12AreaEstraBytes() {
        byte[] buffer = new byte[Len.WE12_AREA_ESTRA];
        return getWe12AreaEstraBytes(buffer, 1);
    }

    public byte[] getWe12AreaEstraBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, we12EleEstraMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WE12_TAB_EST_TRCH_MAXOCCURS; idx++) {
            we12TabEstTrch[idx - 1].getWe12TabEstTrchBytes(buffer, position);
            position += We12TabEstTrch.Len.WE12_TAB_EST_TRCH;
        }
        return buffer;
    }

    public void setWe12EleEstraMax(short we12EleEstraMax) {
        this.we12EleEstraMax = we12EleEstraMax;
    }

    public short getWe12EleEstraMax() {
        return this.we12EleEstraMax;
    }

    public void setIdsv0102SearchIndex(int idsv0102SearchIndex) {
        this.idsv0102SearchIndex = idsv0102SearchIndex;
    }

    public int getIdsv0102SearchIndex() {
        return this.idsv0102SearchIndex;
    }

    public void setIdsv0102aSearchIndex(int idsv0102aSearchIndex) {
        this.idsv0102aSearchIndex = idsv0102aSearchIndex;
    }

    public int getIdsv0102aSearchIndex() {
        return this.idsv0102aSearchIndex;
    }

    public void setIntRegister1(int intRegister1) {
        this.intRegister1 = intRegister1;
    }

    public int getIntRegister1() {
        return this.intRegister1;
    }

    public AdesIvvs0216 getAdes() {
        return ades;
    }

    public AreaCall getAreaCall() {
        return areaCall;
    }

    public Idss0140Link getAreaIdsv0141() {
        return areaIdsv0141;
    }

    public AreaLdbv1131 getAreaLdbv1131() {
        return areaLdbv1131;
    }

    public Bnfic getBnfic() {
        return bnfic;
    }

    public BnficrLiq getBnficrLiq() {
        return bnficrLiq;
    }

    public CnbtNded getCnbtNded() {
        return cnbtNded;
    }

    public CompQuest getCompQuest() {
        return compQuest;
    }

    public DettQuest getDettQuest() {
        return dettQuest;
    }

    public DettTitCont getDettTitCont() {
        return dettTitCont;
    }

    public DfltAdes getDfltAdes() {
        return dfltAdes;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public DomandaCollg getDomandaCollg() {
        return domandaCollg;
    }

    public EstPoliCpiPr getEstPoliCpiPr() {
        return estPoliCpiPr;
    }

    public EstRappAna getEstRappAna() {
        return estRappAna;
    }

    public EstTrchDiGar getEstTrchDiGar() {
        return estTrchDiGar;
    }

    public FlagGestione getFlagGestione() {
        return flagGestione;
    }

    public FlagIdTrovato getFlagIdTrovato() {
        return flagIdTrovato;
    }

    public FlagSkedaOpzione getFlagSkedaOpzione() {
        return flagSkedaOpzione;
    }

    public FlagVarInChiaro getFlagVarInChiaro() {
        return flagVarInChiaro;
    }

    public GarIvvs0216 getGar() {
        return gar;
    }

    public GarLiq getGarLiq() {
        return garLiq;
    }

    public Iabc0010 getIabc0010() {
        return iabc0010;
    }

    public Idsi0021Area getIdsi0021Area() {
        return idsi0021Area;
    }

    public Idso0021 getIdso0021() {
        return idso0021;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public ImpstSost getImpstSost() {
        return impstSost;
    }

    public Ivvc0213 getIvvc0213() {
        return ivvc0213;
    }

    public Ivvc0218Ivvs0216 getIvvc0218() {
        return ivvc0218;
    }

    public Ivvc0221 getIvvc0221() {
        return ivvc0221;
    }

    public Ivvc0501Output getIvvc0501Output() {
        return ivvc0501Output;
    }

    public Lccvgrzz getLccvgrzz() {
        return lccvgrzz;
    }

    public Lccvpco1 getLccvpco1() {
        return lccvpco1;
    }

    public Ldbv0011 getLdbv0011() {
        return ldbv0011;
    }

    public Ldbv1291 getLdbv1291() {
        return ldbv1291;
    }

    public Ldbv1471 getLdbv1471() {
        return ldbv1471;
    }

    public Ldbv2171 getLdbv2171() {
        return ldbv2171;
    }

    public Ldbv2321 getLdbv2321() {
        return ldbv2321;
    }

    public Ldbv5001 getLdbv5001() {
        return ldbv5001;
    }

    public Liq getLiq() {
        return liq;
    }

    public Movi getMovi() {
        return movi;
    }

    public MoviFinrio getMoviFinrio() {
        return moviFinrio;
    }

    public OggCollg getOggCollg() {
        return oggCollg;
    }

    public ParamComp getParamComp() {
        return paramComp;
    }

    public ParamMovi getParamMovi() {
        return paramMovi;
    }

    public ParamOgg getParamOgg() {
        return paramOgg;
    }

    public PercLiq getPercLiq() {
        return percLiq;
    }

    public Poli getPoli() {
        return poli;
    }

    public Prest getPrest() {
        return prest;
    }

    public ProvDiGar getProvDiGar() {
        return provDiGar;
    }

    public Quest getQuest() {
        return quest;
    }

    public RappAna getRappAna() {
        return rappAna;
    }

    public RappRete getRappRete() {
        return rappRete;
    }

    public ReinvstPoliLq getReinvstPoliLq() {
        return reinvstPoliLq;
    }

    public Rich getRich() {
        return rich;
    }

    public RichEst getRichEst() {
        return richEst;
    }

    public RichInvstFnd getRichInvstFnd() {
        return richInvstFnd;
    }

    public RisDiTrch getRisDiTrch() {
        return risDiTrch;
    }

    public SoprDiGar getSoprDiGar() {
        return soprDiGar;
    }

    public StraDiInvst getStraDiInvst() {
        return straDiInvst;
    }

    public TitCont getTitCont() {
        return titCont;
    }

    public TrchDiGarIvvs0216 getTrchDiGar() {
        return trchDiGar;
    }

    public TrchLiq getTrchLiq() {
        return trchLiq;
    }

    public V1391AreaActu getV1391AreaActu() {
        return v1391AreaActu;
    }

    public VincPeg getVincPeg() {
        return vincPeg;
    }

    public WcomFlagOverflow getWcomFlagOverflow() {
        return wcomFlagOverflow;
    }

    public We12TabEstTrch getWe12TabEstTrch(int idx) {
        return we12TabEstTrch[idx - 1];
    }

    public Wk1TabStringheTot getWk1TabStringheTot(int idx) {
        return wk1TabStringheTot[idx - 1];
    }

    public WkAppAssicurato getWkAppAssicurato() {
        return wkAppAssicurato;
    }

    public WkCalcoli getWkCalcoli() {
        return wkCalcoli;
    }

    public WkCallCalcolo getWkCallCalcolo() {
        return wkCallCalcolo;
    }

    public WkCodLivelloFlag getWkCodLivelloFlag() {
        return wkCodLivelloFlag;
    }

    public WkContesto getWkContesto() {
        return wkContesto;
    }

    public WkContestoPog getWkContestoPog() {
        return wkContestoPog;
    }

    public WkDerogaOk getWkDerogaOk() {
        return wkDerogaOk;
    }

    public WkErrore getWkErrore() {
        return wkErrore;
    }

    public WkFindCampo getWkFindCampo() {
        return wkFindCampo;
    }

    public WkFindFiglia getWkFindFiglia() {
        return wkFindFiglia;
    }

    public WkFindIntern getWkFindIntern() {
        return wkFindIntern;
    }

    public WkFindLetto getWkFindLetto() {
        return wkFindLetto;
    }

    public WkGestioneCall getWkGestioneCall() {
        return wkGestioneCall;
    }

    public WkIdCodLivFlag getWkIdCodLivFlag() {
        return wkIdCodLivFlag;
    }

    public WkIdLivelloFlag getWkIdLivelloFlag() {
        return wkIdLivelloFlag;
    }

    public WkLeturaMmv getWkLeturaMmv() {
        return wkLeturaMmv;
    }

    public WkOutScritto getWkOutScritto() {
        return wkOutScritto;
    }

    public WkValoreRis getWkValoreRis() {
        return wkValoreRis;
    }

    public WksAreaTabb getWksAreaTabb(int idx) {
        return wksAreaTabb[idx - 1];
    }

    public WksIndici getWksIndici() {
        return wksIndici;
    }

    public WksTrovatoTabbIvvs0216 getWksTrovatoTabb() {
        return wksTrovatoTabb;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WtgaTabTran getWtgaTabTran(int idx) {
        return wtgaTabTran[idx - 1];
    }

    public DColl getdColl() {
        return dColl;
    }

    public DCrist getdCrist() {
        return dCrist;
    }

    public DFiscAdes getdFiscAdes() {
        return dFiscAdes;
    }

    public DForzLiq getdForzLiq() {
        return dForzLiq;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WKS_APPO_DATA = 8;
        public static final int WKS_APPO_STR = 12;
        public static final int WKS_APPO_STOR = 3;
        public static final int WKS_COD_SOPR = 12;
        public static final int WK_ELE_VARIABILI_MAX = 3;
        public static final int WKS_NOME_TABELLA = 30;
        public static final int WKS_NOME_TABELLA1 = 30;
        public static final int GG_DIFF = 5;
        public static final int WS_DT_EFF_APPO1 = 8;
        public static final int WK_APPO_TP_OGG = 2;
        public static final int PGM_CHIAMATO = 8;
        public static final int WK_COD_DOM = 20;
        public static final int WK_TP_QUEST = 2;
        public static final int INPUT_IDSS0020 = Idsi0021Area.Len.IDSI0021_AREA;
        public static final int WKS_AREA_TABB_APPO = 2500;
        public static final int WK_PARAM_OGG_APPO = 60000;
        public static final int WK_TP_OGG = 2;
        public static final int PGM_CALCOLI = 8;
        public static final int WK_COD_VARIABILE = 12;
        public static final int WK_APPO_ERR = 200;
        public static final int WTGA_ELE_TRAN_MAX = 2;
        public static final int WTGA_AREA_TRANCHE = WTGA_ELE_TRAN_MAX + Ivvs0216Data.WTGA_TAB_TRAN_MAXOCCURS * WtgaTabTran.Len.TAB_TRAN;
        public static final int FXT_ADDRESS = 4;
        public static final int VXG_ADDRESS = 4;
        public static final int AREA_POINTER = FXT_ADDRESS + VXG_ADDRESS;
        public static final int WPCO_ELE_PCO_MAX = 2;
        public static final int WPCO_TAB_PCO = WpolStatus.Len.STATUS + WpcoDati.Len.DATI;
        public static final int WPCO_AREA_PARA_COM = WPCO_ELE_PCO_MAX + WPCO_TAB_PCO;
        public static final int WE12_ELE_ESTRA_MAX = 2;
        public static final int WE12_AREA_ESTRA = WE12_ELE_ESTRA_MAX + Ivvs0216Data.WE12_TAB_EST_TRCH_MAXOCCURS * We12TabEstTrch.Len.WE12_TAB_EST_TRCH;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
