package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PRE-CONT<br>
 * Variable: W-B03-PRE-CONT from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03PreContLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03PreContLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PRE_CONT;
    }

    public void setwB03PreCont(AfDecimal wB03PreCont) {
        writeDecimalAsPacked(Pos.W_B03_PRE_CONT, wB03PreCont.copy());
    }

    /**Original name: W-B03-PRE-CONT<br>*/
    public AfDecimal getwB03PreCont() {
        return readPackedAsDecimal(Pos.W_B03_PRE_CONT, Len.Int.W_B03_PRE_CONT, Len.Fract.W_B03_PRE_CONT);
    }

    public byte[] getwB03PreContAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PRE_CONT, Pos.W_B03_PRE_CONT);
        return buffer;
    }

    public void setwB03PreContNull(String wB03PreContNull) {
        writeString(Pos.W_B03_PRE_CONT_NULL, wB03PreContNull, Len.W_B03_PRE_CONT_NULL);
    }

    /**Original name: W-B03-PRE-CONT-NULL<br>*/
    public String getwB03PreContNull() {
        return readString(Pos.W_B03_PRE_CONT_NULL, Len.W_B03_PRE_CONT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_CONT = 1;
        public static final int W_B03_PRE_CONT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_CONT = 8;
        public static final int W_B03_PRE_CONT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_CONT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_CONT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
