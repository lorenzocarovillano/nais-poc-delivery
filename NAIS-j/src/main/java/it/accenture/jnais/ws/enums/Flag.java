package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG<br>
 * Variable: FLAG from program LOAS0320<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Flag {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG);
    public static final String TROVATO = "SI";
    public static final String NON_TROVATO = "NO";

    //==== METHODS ====
    public void setFlag(String flag) {
        this.value = Functions.subString(flag, Len.FLAG);
    }

    public String getFlag() {
        return this.value;
    }

    public boolean isTrovato() {
        return value.equals(TROVATO);
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public void setNonTrovato() {
        value = NON_TROVATO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
