package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ISPC0040-FLAG-RISCHIO-COMUNE<br>
 * Variable: ISPC0040-FLAG-RISCHIO-COMUNE from copybook ISPC0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0040FlagRischioComune {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = '1';
    public static final char NO = '0';

    //==== METHODS ====
    public void setFlagRischioComune(char flagRischioComune) {
        this.value = flagRischioComune;
    }

    public char getFlagRischioComune() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_RISCHIO_COMUNE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
