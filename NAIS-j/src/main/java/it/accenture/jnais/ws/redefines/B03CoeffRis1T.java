package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-COEFF-RIS-1-T<br>
 * Variable: B03-COEFF-RIS-1-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CoeffRis1T extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CoeffRis1T() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_COEFF_RIS1_T;
    }

    public void setB03CoeffRis1T(AfDecimal b03CoeffRis1T) {
        writeDecimalAsPacked(Pos.B03_COEFF_RIS1_T, b03CoeffRis1T.copy());
    }

    public void setB03CoeffRis1TFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_COEFF_RIS1_T, Pos.B03_COEFF_RIS1_T);
    }

    /**Original name: B03-COEFF-RIS-1-T<br>*/
    public AfDecimal getB03CoeffRis1T() {
        return readPackedAsDecimal(Pos.B03_COEFF_RIS1_T, Len.Int.B03_COEFF_RIS1_T, Len.Fract.B03_COEFF_RIS1_T);
    }

    public byte[] getB03CoeffRis1TAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_COEFF_RIS1_T, Pos.B03_COEFF_RIS1_T);
        return buffer;
    }

    public void setB03CoeffRis1TNull(String b03CoeffRis1TNull) {
        writeString(Pos.B03_COEFF_RIS1_T_NULL, b03CoeffRis1TNull, Len.B03_COEFF_RIS1_T_NULL);
    }

    /**Original name: B03-COEFF-RIS-1-T-NULL<br>*/
    public String getB03CoeffRis1TNull() {
        return readString(Pos.B03_COEFF_RIS1_T_NULL, Len.B03_COEFF_RIS1_T_NULL);
    }

    public String getB03CoeffRis1TNullFormatted() {
        return Functions.padBlanks(getB03CoeffRis1TNull(), Len.B03_COEFF_RIS1_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_COEFF_RIS1_T = 1;
        public static final int B03_COEFF_RIS1_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_COEFF_RIS1_T = 8;
        public static final int B03_COEFF_RIS1_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_COEFF_RIS1_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_COEFF_RIS1_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
