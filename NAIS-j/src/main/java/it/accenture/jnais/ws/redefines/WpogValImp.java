package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPOG-VAL-IMP<br>
 * Variable: WPOG-VAL-IMP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpogValImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpogValImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOG_VAL_IMP;
    }

    public void setWpogValImp(AfDecimal wpogValImp) {
        writeDecimalAsPacked(Pos.WPOG_VAL_IMP, wpogValImp.copy());
    }

    public void setWpogValImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOG_VAL_IMP, Pos.WPOG_VAL_IMP);
    }

    /**Original name: WPOG-VAL-IMP<br>*/
    public AfDecimal getWpogValImp() {
        return readPackedAsDecimal(Pos.WPOG_VAL_IMP, Len.Int.WPOG_VAL_IMP, Len.Fract.WPOG_VAL_IMP);
    }

    public byte[] getWpogValImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOG_VAL_IMP, Pos.WPOG_VAL_IMP);
        return buffer;
    }

    public void initWpogValImpSpaces() {
        fill(Pos.WPOG_VAL_IMP, Len.WPOG_VAL_IMP, Types.SPACE_CHAR);
    }

    public void setWpogValImpNull(String wpogValImpNull) {
        writeString(Pos.WPOG_VAL_IMP_NULL, wpogValImpNull, Len.WPOG_VAL_IMP_NULL);
    }

    /**Original name: WPOG-VAL-IMP-NULL<br>*/
    public String getWpogValImpNull() {
        return readString(Pos.WPOG_VAL_IMP_NULL, Len.WPOG_VAL_IMP_NULL);
    }

    public String getDpogValImpNullFormatted() {
        return Functions.padBlanks(getWpogValImpNull(), Len.WPOG_VAL_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOG_VAL_IMP = 1;
        public static final int WPOG_VAL_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOG_VAL_IMP = 8;
        public static final int WPOG_VAL_IMP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPOG_VAL_IMP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOG_VAL_IMP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
