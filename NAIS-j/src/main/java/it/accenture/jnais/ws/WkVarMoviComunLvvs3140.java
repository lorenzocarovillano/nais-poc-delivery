package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-VAR-MOVI-COMUN<br>
 * Variable: WK-VAR-MOVI-COMUN from program LVVS3140<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVarMoviComunLvvs3140 {

    //==== PROPERTIES ====
    /**Original name: WK-DATA-EFF-PREC<br>
	 * <pre>    05 WK-ID-MOVI-COMUN             PIC S9(09) COMP-3.</pre>*/
    private int effPrec = DefaultValues.INT_VAL;
    //Original name: WK-DATA-CPTZ-PREC
    private long cptzPrec = DefaultValues.LONG_VAL;
    //Original name: WK-DATA-EFF-RIP
    private int effRip = DefaultValues.INT_VAL;
    //Original name: WK-DATA-CPTZ-RIP
    private long cptzRip = DefaultValues.LONG_VAL;

    //==== METHODS ====
    public void setEffPrec(int effPrec) {
        this.effPrec = effPrec;
    }

    public int getEffPrec() {
        return this.effPrec;
    }

    public void setCptzPrec(long cptzPrec) {
        this.cptzPrec = cptzPrec;
    }

    public long getCptzPrec() {
        return this.cptzPrec;
    }

    public void setEffRip(int effRip) {
        this.effRip = effRip;
    }

    public int getEffRip() {
        return this.effRip;
    }

    public void setCptzRip(long cptzRip) {
        this.cptzRip = cptzRip;
    }

    public long getCptzRip() {
        return this.cptzRip;
    }
}
