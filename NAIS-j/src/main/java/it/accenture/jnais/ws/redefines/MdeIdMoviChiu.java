package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MDE-ID-MOVI-CHIU<br>
 * Variable: MDE-ID-MOVI-CHIU from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MdeIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MdeIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MDE_ID_MOVI_CHIU;
    }

    public void setMdeIdMoviChiu(int mdeIdMoviChiu) {
        writeIntAsPacked(Pos.MDE_ID_MOVI_CHIU, mdeIdMoviChiu, Len.Int.MDE_ID_MOVI_CHIU);
    }

    public void setMdeIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MDE_ID_MOVI_CHIU, Pos.MDE_ID_MOVI_CHIU);
    }

    /**Original name: MDE-ID-MOVI-CHIU<br>*/
    public int getMdeIdMoviChiu() {
        return readPackedAsInt(Pos.MDE_ID_MOVI_CHIU, Len.Int.MDE_ID_MOVI_CHIU);
    }

    public byte[] getMdeIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MDE_ID_MOVI_CHIU, Pos.MDE_ID_MOVI_CHIU);
        return buffer;
    }

    public void setMdeIdMoviChiuNull(String mdeIdMoviChiuNull) {
        writeString(Pos.MDE_ID_MOVI_CHIU_NULL, mdeIdMoviChiuNull, Len.MDE_ID_MOVI_CHIU_NULL);
    }

    /**Original name: MDE-ID-MOVI-CHIU-NULL<br>*/
    public String getMdeIdMoviChiuNull() {
        return readString(Pos.MDE_ID_MOVI_CHIU_NULL, Len.MDE_ID_MOVI_CHIU_NULL);
    }

    public String getMdeIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getMdeIdMoviChiuNull(), Len.MDE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MDE_ID_MOVI_CHIU = 1;
        public static final int MDE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MDE_ID_MOVI_CHIU = 5;
        public static final int MDE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MDE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
