package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RES-PRE-ATT-EFFLQ<br>
 * Variable: WDFL-RES-PRE-ATT-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflResPreAttEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflResPreAttEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RES_PRE_ATT_EFFLQ;
    }

    public void setWdflResPreAttEfflq(AfDecimal wdflResPreAttEfflq) {
        writeDecimalAsPacked(Pos.WDFL_RES_PRE_ATT_EFFLQ, wdflResPreAttEfflq.copy());
    }

    public void setWdflResPreAttEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RES_PRE_ATT_EFFLQ, Pos.WDFL_RES_PRE_ATT_EFFLQ);
    }

    /**Original name: WDFL-RES-PRE-ATT-EFFLQ<br>*/
    public AfDecimal getWdflResPreAttEfflq() {
        return readPackedAsDecimal(Pos.WDFL_RES_PRE_ATT_EFFLQ, Len.Int.WDFL_RES_PRE_ATT_EFFLQ, Len.Fract.WDFL_RES_PRE_ATT_EFFLQ);
    }

    public byte[] getWdflResPreAttEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RES_PRE_ATT_EFFLQ, Pos.WDFL_RES_PRE_ATT_EFFLQ);
        return buffer;
    }

    public void setWdflResPreAttEfflqNull(String wdflResPreAttEfflqNull) {
        writeString(Pos.WDFL_RES_PRE_ATT_EFFLQ_NULL, wdflResPreAttEfflqNull, Len.WDFL_RES_PRE_ATT_EFFLQ_NULL);
    }

    /**Original name: WDFL-RES-PRE-ATT-EFFLQ-NULL<br>*/
    public String getWdflResPreAttEfflqNull() {
        return readString(Pos.WDFL_RES_PRE_ATT_EFFLQ_NULL, Len.WDFL_RES_PRE_ATT_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRE_ATT_EFFLQ = 1;
        public static final int WDFL_RES_PRE_ATT_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRE_ATT_EFFLQ = 8;
        public static final int WDFL_RES_PRE_ATT_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRE_ATT_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRE_ATT_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
