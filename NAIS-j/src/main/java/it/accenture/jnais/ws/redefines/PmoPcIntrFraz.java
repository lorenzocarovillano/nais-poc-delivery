package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-PC-INTR-FRAZ<br>
 * Variable: PMO-PC-INTR-FRAZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoPcIntrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoPcIntrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_PC_INTR_FRAZ;
    }

    public void setPmoPcIntrFraz(AfDecimal pmoPcIntrFraz) {
        writeDecimalAsPacked(Pos.PMO_PC_INTR_FRAZ, pmoPcIntrFraz.copy());
    }

    public void setPmoPcIntrFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_PC_INTR_FRAZ, Pos.PMO_PC_INTR_FRAZ);
    }

    /**Original name: PMO-PC-INTR-FRAZ<br>*/
    public AfDecimal getPmoPcIntrFraz() {
        return readPackedAsDecimal(Pos.PMO_PC_INTR_FRAZ, Len.Int.PMO_PC_INTR_FRAZ, Len.Fract.PMO_PC_INTR_FRAZ);
    }

    public byte[] getPmoPcIntrFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_PC_INTR_FRAZ, Pos.PMO_PC_INTR_FRAZ);
        return buffer;
    }

    public void initPmoPcIntrFrazHighValues() {
        fill(Pos.PMO_PC_INTR_FRAZ, Len.PMO_PC_INTR_FRAZ, Types.HIGH_CHAR_VAL);
    }

    public void setPmoPcIntrFrazNull(String pmoPcIntrFrazNull) {
        writeString(Pos.PMO_PC_INTR_FRAZ_NULL, pmoPcIntrFrazNull, Len.PMO_PC_INTR_FRAZ_NULL);
    }

    /**Original name: PMO-PC-INTR-FRAZ-NULL<br>*/
    public String getPmoPcIntrFrazNull() {
        return readString(Pos.PMO_PC_INTR_FRAZ_NULL, Len.PMO_PC_INTR_FRAZ_NULL);
    }

    public String getPmoPcIntrFrazNullFormatted() {
        return Functions.padBlanks(getPmoPcIntrFrazNull(), Len.PMO_PC_INTR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_PC_INTR_FRAZ = 1;
        public static final int PMO_PC_INTR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_PC_INTR_FRAZ = 4;
        public static final int PMO_PC_INTR_FRAZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_PC_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_PC_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
