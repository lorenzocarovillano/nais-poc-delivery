package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-DT-VALZZ-CALC<br>
 * Variable: VAS-DT-VALZZ-CALC from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasDtValzzCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasDtValzzCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_DT_VALZZ_CALC;
    }

    public void setVasDtValzzCalc(int vasDtValzzCalc) {
        writeIntAsPacked(Pos.VAS_DT_VALZZ_CALC, vasDtValzzCalc, Len.Int.VAS_DT_VALZZ_CALC);
    }

    public void setVasDtValzzCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_DT_VALZZ_CALC, Pos.VAS_DT_VALZZ_CALC);
    }

    /**Original name: VAS-DT-VALZZ-CALC<br>*/
    public int getVasDtValzzCalc() {
        return readPackedAsInt(Pos.VAS_DT_VALZZ_CALC, Len.Int.VAS_DT_VALZZ_CALC);
    }

    public byte[] getVasDtValzzCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_DT_VALZZ_CALC, Pos.VAS_DT_VALZZ_CALC);
        return buffer;
    }

    public void setVasDtValzzCalcNull(String vasDtValzzCalcNull) {
        writeString(Pos.VAS_DT_VALZZ_CALC_NULL, vasDtValzzCalcNull, Len.VAS_DT_VALZZ_CALC_NULL);
    }

    /**Original name: VAS-DT-VALZZ-CALC-NULL<br>*/
    public String getVasDtValzzCalcNull() {
        return readString(Pos.VAS_DT_VALZZ_CALC_NULL, Len.VAS_DT_VALZZ_CALC_NULL);
    }

    public String getVasDtValzzCalcNullFormatted() {
        return Functions.padBlanks(getVasDtValzzCalcNull(), Len.VAS_DT_VALZZ_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_DT_VALZZ_CALC = 1;
        public static final int VAS_DT_VALZZ_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_DT_VALZZ_CALC = 5;
        public static final int VAS_DT_VALZZ_CALC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_DT_VALZZ_CALC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
