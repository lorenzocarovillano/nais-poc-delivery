package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-NUM-RAT-PIAN<br>
 * Variable: WADE-NUM-RAT-PIAN from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeNumRatPian extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeNumRatPian() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_NUM_RAT_PIAN;
    }

    public void setWadeNumRatPian(AfDecimal wadeNumRatPian) {
        writeDecimalAsPacked(Pos.WADE_NUM_RAT_PIAN, wadeNumRatPian.copy());
    }

    public void setWadeNumRatPianFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_NUM_RAT_PIAN, Pos.WADE_NUM_RAT_PIAN);
    }

    /**Original name: WADE-NUM-RAT-PIAN<br>*/
    public AfDecimal getWadeNumRatPian() {
        return readPackedAsDecimal(Pos.WADE_NUM_RAT_PIAN, Len.Int.WADE_NUM_RAT_PIAN, Len.Fract.WADE_NUM_RAT_PIAN);
    }

    public byte[] getWadeNumRatPianAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_NUM_RAT_PIAN, Pos.WADE_NUM_RAT_PIAN);
        return buffer;
    }

    public void initWadeNumRatPianSpaces() {
        fill(Pos.WADE_NUM_RAT_PIAN, Len.WADE_NUM_RAT_PIAN, Types.SPACE_CHAR);
    }

    public void setWadeNumRatPianNull(String wadeNumRatPianNull) {
        writeString(Pos.WADE_NUM_RAT_PIAN_NULL, wadeNumRatPianNull, Len.WADE_NUM_RAT_PIAN_NULL);
    }

    /**Original name: WADE-NUM-RAT-PIAN-NULL<br>*/
    public String getWadeNumRatPianNull() {
        return readString(Pos.WADE_NUM_RAT_PIAN_NULL, Len.WADE_NUM_RAT_PIAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_NUM_RAT_PIAN = 1;
        public static final int WADE_NUM_RAT_PIAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_NUM_RAT_PIAN = 7;
        public static final int WADE_NUM_RAT_PIAN_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_NUM_RAT_PIAN = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_NUM_RAT_PIAN = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
