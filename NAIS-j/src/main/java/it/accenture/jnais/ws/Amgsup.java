package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: AMGSUP<br>
 * Variable: AMGSUP from program LCCS0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Amgsup {

    //==== PROPERTIES ====
    //Original name: AAAA-S
    private String aaaaS = DefaultValues.stringVal(Len.AAAA_S);
    //Original name: MM-S
    private String mmS = DefaultValues.stringVal(Len.MM_S);
    //Original name: GG-S
    private String ggS = DefaultValues.stringVal(Len.GG_S);

    //==== METHODS ====
    public void setAmgsupBytes(byte[] buffer) {
        setAmgsupBytes(buffer, 1);
    }

    public byte[] getAmgsupBytes() {
        byte[] buffer = new byte[Len.AMGSUP];
        return getAmgsupBytes(buffer, 1);
    }

    public void setAmgsupBytes(byte[] buffer, int offset) {
        int position = offset;
        aaaaS = MarshalByte.readFixedString(buffer, position, Len.AAAA_S);
        position += Len.AAAA_S;
        mmS = MarshalByte.readFixedString(buffer, position, Len.MM_S);
        position += Len.MM_S;
        ggS = MarshalByte.readFixedString(buffer, position, Len.GG_S);
    }

    public byte[] getAmgsupBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aaaaS, Len.AAAA_S);
        position += Len.AAAA_S;
        MarshalByte.writeString(buffer, position, mmS, Len.MM_S);
        position += Len.MM_S;
        MarshalByte.writeString(buffer, position, ggS, Len.GG_S);
        return buffer;
    }

    public void setAaaaS(short aaaaS) {
        this.aaaaS = NumericDisplay.asString(aaaaS, Len.AAAA_S);
    }

    public void setAaaaSFormatted(String aaaaS) {
        this.aaaaS = Trunc.toUnsignedNumeric(aaaaS, Len.AAAA_S);
    }

    public short getAaaaS() {
        return NumericDisplay.asShort(this.aaaaS);
    }

    public void setMmSFormatted(String mmS) {
        this.mmS = Trunc.toUnsignedNumeric(mmS, Len.MM_S);
    }

    public short getMmS() {
        return NumericDisplay.asShort(this.mmS);
    }

    public void setGgS(short ggS) {
        this.ggS = NumericDisplay.asString(ggS, Len.GG_S);
    }

    public void setGgSFormatted(String ggS) {
        this.ggS = Trunc.toUnsignedNumeric(ggS, Len.GG_S);
    }

    public short getGgS() {
        return NumericDisplay.asShort(this.ggS);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA_S = 4;
        public static final int MM_S = 2;
        public static final int GG_S = 2;
        public static final int AMGSUP = AAAA_S + MM_S + GG_S;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
