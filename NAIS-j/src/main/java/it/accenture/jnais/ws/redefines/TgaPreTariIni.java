package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-TARI-INI<br>
 * Variable: TGA-PRE-TARI-INI from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPreTariIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPreTariIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_TARI_INI;
    }

    public void setTgaPreTariIni(AfDecimal tgaPreTariIni) {
        writeDecimalAsPacked(Pos.TGA_PRE_TARI_INI, tgaPreTariIni.copy());
    }

    public void setTgaPreTariIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_TARI_INI, Pos.TGA_PRE_TARI_INI);
    }

    /**Original name: TGA-PRE-TARI-INI<br>*/
    public AfDecimal getTgaPreTariIni() {
        return readPackedAsDecimal(Pos.TGA_PRE_TARI_INI, Len.Int.TGA_PRE_TARI_INI, Len.Fract.TGA_PRE_TARI_INI);
    }

    public byte[] getTgaPreTariIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_TARI_INI, Pos.TGA_PRE_TARI_INI);
        return buffer;
    }

    public void setTgaPreTariIniNull(String tgaPreTariIniNull) {
        writeString(Pos.TGA_PRE_TARI_INI_NULL, tgaPreTariIniNull, Len.TGA_PRE_TARI_INI_NULL);
    }

    /**Original name: TGA-PRE-TARI-INI-NULL<br>*/
    public String getTgaPreTariIniNull() {
        return readString(Pos.TGA_PRE_TARI_INI_NULL, Len.TGA_PRE_TARI_INI_NULL);
    }

    public String getTgaPreTariIniNullFormatted() {
        return Functions.padBlanks(getTgaPreTariIniNull(), Len.TGA_PRE_TARI_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_TARI_INI = 1;
        public static final int TGA_PRE_TARI_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_TARI_INI = 8;
        public static final int TGA_PRE_TARI_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_TARI_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_TARI_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
