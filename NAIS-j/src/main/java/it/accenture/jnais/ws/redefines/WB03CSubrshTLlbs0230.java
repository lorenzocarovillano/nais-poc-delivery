package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-C-SUBRSH-T<br>
 * Variable: W-B03-C-SUBRSH-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CSubrshTLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CSubrshTLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_C_SUBRSH_T;
    }

    public void setwB03CSubrshT(AfDecimal wB03CSubrshT) {
        writeDecimalAsPacked(Pos.W_B03_C_SUBRSH_T, wB03CSubrshT.copy());
    }

    /**Original name: W-B03-C-SUBRSH-T<br>*/
    public AfDecimal getwB03CSubrshT() {
        return readPackedAsDecimal(Pos.W_B03_C_SUBRSH_T, Len.Int.W_B03_C_SUBRSH_T, Len.Fract.W_B03_C_SUBRSH_T);
    }

    public byte[] getwB03CSubrshTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_C_SUBRSH_T, Pos.W_B03_C_SUBRSH_T);
        return buffer;
    }

    public void setwB03CSubrshTNull(String wB03CSubrshTNull) {
        writeString(Pos.W_B03_C_SUBRSH_T_NULL, wB03CSubrshTNull, Len.W_B03_C_SUBRSH_T_NULL);
    }

    /**Original name: W-B03-C-SUBRSH-T-NULL<br>*/
    public String getwB03CSubrshTNull() {
        return readString(Pos.W_B03_C_SUBRSH_T_NULL, Len.W_B03_C_SUBRSH_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_C_SUBRSH_T = 1;
        public static final int W_B03_C_SUBRSH_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_C_SUBRSH_T = 8;
        public static final int W_B03_C_SUBRSH_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_C_SUBRSH_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_C_SUBRSH_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
