package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMPB-VIS-END2000<br>
 * Variable: L3421-IMPB-VIS-END2000 from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpbVisEnd2000 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpbVisEnd2000() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMPB_VIS_END2000;
    }

    public void setL3421ImpbVisEnd2000FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMPB_VIS_END2000, Pos.L3421_IMPB_VIS_END2000);
    }

    /**Original name: L3421-IMPB-VIS-END2000<br>*/
    public AfDecimal getL3421ImpbVisEnd2000() {
        return readPackedAsDecimal(Pos.L3421_IMPB_VIS_END2000, Len.Int.L3421_IMPB_VIS_END2000, Len.Fract.L3421_IMPB_VIS_END2000);
    }

    public byte[] getL3421ImpbVisEnd2000AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMPB_VIS_END2000, Pos.L3421_IMPB_VIS_END2000);
        return buffer;
    }

    /**Original name: L3421-IMPB-VIS-END2000-NULL<br>*/
    public String getL3421ImpbVisEnd2000Null() {
        return readString(Pos.L3421_IMPB_VIS_END2000_NULL, Len.L3421_IMPB_VIS_END2000_NULL);
    }

    public String getL3421ImpbVisEnd2000NullFormatted() {
        return Functions.padBlanks(getL3421ImpbVisEnd2000Null(), Len.L3421_IMPB_VIS_END2000_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMPB_VIS_END2000 = 1;
        public static final int L3421_IMPB_VIS_END2000_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMPB_VIS_END2000 = 8;
        public static final int L3421_IMPB_VIS_END2000_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMPB_VIS_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMPB_VIS_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
