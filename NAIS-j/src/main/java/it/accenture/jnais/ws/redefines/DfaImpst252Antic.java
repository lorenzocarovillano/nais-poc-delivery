package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPST-252-ANTIC<br>
 * Variable: DFA-IMPST-252-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpst252Antic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpst252Antic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPST252_ANTIC;
    }

    public void setDfaImpst252Antic(AfDecimal dfaImpst252Antic) {
        writeDecimalAsPacked(Pos.DFA_IMPST252_ANTIC, dfaImpst252Antic.copy());
    }

    public void setDfaImpst252AnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPST252_ANTIC, Pos.DFA_IMPST252_ANTIC);
    }

    /**Original name: DFA-IMPST-252-ANTIC<br>*/
    public AfDecimal getDfaImpst252Antic() {
        return readPackedAsDecimal(Pos.DFA_IMPST252_ANTIC, Len.Int.DFA_IMPST252_ANTIC, Len.Fract.DFA_IMPST252_ANTIC);
    }

    public byte[] getDfaImpst252AnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPST252_ANTIC, Pos.DFA_IMPST252_ANTIC);
        return buffer;
    }

    public void setDfaImpst252AnticNull(String dfaImpst252AnticNull) {
        writeString(Pos.DFA_IMPST252_ANTIC_NULL, dfaImpst252AnticNull, Len.DFA_IMPST252_ANTIC_NULL);
    }

    /**Original name: DFA-IMPST-252-ANTIC-NULL<br>*/
    public String getDfaImpst252AnticNull() {
        return readString(Pos.DFA_IMPST252_ANTIC_NULL, Len.DFA_IMPST252_ANTIC_NULL);
    }

    public String getDfaImpst252AnticNullFormatted() {
        return Functions.padBlanks(getDfaImpst252AnticNull(), Len.DFA_IMPST252_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPST252_ANTIC = 1;
        public static final int DFA_IMPST252_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPST252_ANTIC = 8;
        public static final int DFA_IMPST252_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPST252_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPST252_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
