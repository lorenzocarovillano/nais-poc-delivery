package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0003-IDENTITA-CHIAMANTE<br>
 * Variable: IDSV0003-IDENTITA-CHIAMANTE from copybook IDSV0003<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0003IdentitaChiamante {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.IDENTITA_CHIAMANTE);
    public static final String PTF_NEWLIFE = "LPF";
    public static final String REFACTORING = "REF";
    public static final String CONVERSIONE = "CON";

    //==== METHODS ====
    public void setIdentitaChiamante(String identitaChiamante) {
        this.value = Functions.subString(identitaChiamante, Len.IDENTITA_CHIAMANTE);
    }

    public String getIdentitaChiamante() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDENTITA_CHIAMANTE = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
