package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.DettRich;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Ldbv4511;
import it.accenture.jnais.copy.Llbv0000;
import it.accenture.jnais.copy.Rich;
import it.accenture.jnais.ws.enums.WsFlagCiclo;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LLBS0269<br>
 * Generated as a class for rule WS.<br>*/
public class Llbs0269Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI DI WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LLBS0269";
    //Original name: LLBV0000
    private Llbv0000 llbv0000 = new Llbv0000();
    /**Original name: WK-TS-DATA-MIN<br>
	 * <pre>----------------------------------------------------------------*
	 *     DEFINIZIONE DI INDICI
	 * ----------------------------------------------------------------*
	 * 01  IX-INDICI.
	 * ----------------------------------------------------------------*
	 *     VARIABILI DI WORKING
	 * ----------------------------------------------------------------*
	 * 01  WS-VARIABILI-WORKING.</pre>*/
    private long wkTsDataMin = 190001010000000000L;
    /**Original name: WS-FLAG-CICLO<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG
	 * ----------------------------------------------------------------*</pre>*/
    private WsFlagCiclo wsFlagCiclo = new WsFlagCiclo();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: RICH
    private Rich rich = new Rich();
    //Original name: DETT-RICH
    private DettRich dettRich = new DettRich();
    //Original name: LDBV4511
    private Ldbv4511 ldbv4511 = new Ldbv4511();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public long getWkTsDataMin() {
        return this.wkTsDataMin;
    }

    public DettRich getDettRich() {
        return dettRich;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Ldbv4511 getLdbv4511() {
        return ldbv4511;
    }

    public Llbv0000 getLlbv0000() {
        return llbv0000;
    }

    public Rich getRich() {
        return rich;
    }

    public WsFlagCiclo getWsFlagCiclo() {
        return wsFlagCiclo;
    }
}
