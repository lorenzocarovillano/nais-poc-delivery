package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-DT-RICH-ADD-RID<br>
 * Variable: WTIT-DT-RICH-ADD-RID from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitDtRichAddRid extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitDtRichAddRid() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_DT_RICH_ADD_RID;
    }

    public void setWtitDtRichAddRid(int wtitDtRichAddRid) {
        writeIntAsPacked(Pos.WTIT_DT_RICH_ADD_RID, wtitDtRichAddRid, Len.Int.WTIT_DT_RICH_ADD_RID);
    }

    public void setWtitDtRichAddRidFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_DT_RICH_ADD_RID, Pos.WTIT_DT_RICH_ADD_RID);
    }

    /**Original name: WTIT-DT-RICH-ADD-RID<br>*/
    public int getWtitDtRichAddRid() {
        return readPackedAsInt(Pos.WTIT_DT_RICH_ADD_RID, Len.Int.WTIT_DT_RICH_ADD_RID);
    }

    public byte[] getWtitDtRichAddRidAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_DT_RICH_ADD_RID, Pos.WTIT_DT_RICH_ADD_RID);
        return buffer;
    }

    public void initWtitDtRichAddRidSpaces() {
        fill(Pos.WTIT_DT_RICH_ADD_RID, Len.WTIT_DT_RICH_ADD_RID, Types.SPACE_CHAR);
    }

    public void setWtitDtRichAddRidNull(String wtitDtRichAddRidNull) {
        writeString(Pos.WTIT_DT_RICH_ADD_RID_NULL, wtitDtRichAddRidNull, Len.WTIT_DT_RICH_ADD_RID_NULL);
    }

    /**Original name: WTIT-DT-RICH-ADD-RID-NULL<br>*/
    public String getWtitDtRichAddRidNull() {
        return readString(Pos.WTIT_DT_RICH_ADD_RID_NULL, Len.WTIT_DT_RICH_ADD_RID_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_DT_RICH_ADD_RID = 1;
        public static final int WTIT_DT_RICH_ADD_RID_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_DT_RICH_ADD_RID = 5;
        public static final int WTIT_DT_RICH_ADD_RID_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_DT_RICH_ADD_RID = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
