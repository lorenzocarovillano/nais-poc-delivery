package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-TS-PP<br>
 * Variable: WB03-TS-PP from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03TsPp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03TsPp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_TS_PP;
    }

    public void setWb03TsPpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_TS_PP, Pos.WB03_TS_PP);
    }

    /**Original name: WB03-TS-PP<br>*/
    public AfDecimal getWb03TsPp() {
        return readPackedAsDecimal(Pos.WB03_TS_PP, Len.Int.WB03_TS_PP, Len.Fract.WB03_TS_PP);
    }

    public byte[] getWb03TsPpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_TS_PP, Pos.WB03_TS_PP);
        return buffer;
    }

    public void setWb03TsPpNull(String wb03TsPpNull) {
        writeString(Pos.WB03_TS_PP_NULL, wb03TsPpNull, Len.WB03_TS_PP_NULL);
    }

    /**Original name: WB03-TS-PP-NULL<br>*/
    public String getWb03TsPpNull() {
        return readString(Pos.WB03_TS_PP_NULL, Len.WB03_TS_PP_NULL);
    }

    public String getWb03TsPpNullFormatted() {
        return Functions.padBlanks(getWb03TsPpNull(), Len.WB03_TS_PP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_TS_PP = 1;
        public static final int WB03_TS_PP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_TS_PP = 8;
        public static final int WB03_TS_PP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_TS_PP = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_TS_PP = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
