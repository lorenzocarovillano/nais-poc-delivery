package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.DfaAaCnbzK1;
import it.accenture.jnais.ws.redefines.DfaAaCnbzK2;
import it.accenture.jnais.ws.redefines.DfaAaCnbzK3;
import it.accenture.jnais.ws.redefines.DfaAlqPrvr;
import it.accenture.jnais.ws.redefines.DfaAlqTfr;
import it.accenture.jnais.ws.redefines.DfaAnzCnbtvaCarass;
import it.accenture.jnais.ws.redefines.DfaAnzCnbtvaCarazi;
import it.accenture.jnais.ws.redefines.DfaAnzSrvz;
import it.accenture.jnais.ws.redefines.DfaCnbtEcc4x100K1;
import it.accenture.jnais.ws.redefines.DfaCommisDiTrasfe;
import it.accenture.jnais.ws.redefines.DfaCreditoIs;
import it.accenture.jnais.ws.redefines.DfaDt1aCnbz;
import it.accenture.jnais.ws.redefines.DfaDtAccnsRappFnd;
import it.accenture.jnais.ws.redefines.DfaDtCessazione;
import it.accenture.jnais.ws.redefines.DfaIdMoviChiu;
import it.accenture.jnais.ws.redefines.DfaImpb252Antic;
import it.accenture.jnais.ws.redefines.DfaImpb252K3;
import it.accenture.jnais.ws.redefines.DfaImpbIrpefK1Anti;
import it.accenture.jnais.ws.redefines.DfaImpbIrpefK2Anti;
import it.accenture.jnais.ws.redefines.DfaImpbIsK2;
import it.accenture.jnais.ws.redefines.DfaImpbIsK2Antic;
import it.accenture.jnais.ws.redefines.DfaImpbIsK3;
import it.accenture.jnais.ws.redefines.DfaImpbIsK3Antic;
import it.accenture.jnais.ws.redefines.DfaImpbTfrAntic;
import it.accenture.jnais.ws.redefines.DfaImpbVis;
import it.accenture.jnais.ws.redefines.DfaImpbVisAntic;
import it.accenture.jnais.ws.redefines.DfaImpCnbtAzK1;
import it.accenture.jnais.ws.redefines.DfaImpCnbtAzK2;
import it.accenture.jnais.ws.redefines.DfaImpCnbtAzK3;
import it.accenture.jnais.ws.redefines.DfaImpCnbtIscK1;
import it.accenture.jnais.ws.redefines.DfaImpCnbtIscK2;
import it.accenture.jnais.ws.redefines.DfaImpCnbtIscK3;
import it.accenture.jnais.ws.redefines.DfaImpCnbtNdedK1;
import it.accenture.jnais.ws.redefines.DfaImpCnbtNdedK2;
import it.accenture.jnais.ws.redefines.DfaImpCnbtNdedK3;
import it.accenture.jnais.ws.redefines.DfaImpCnbtTfrK1;
import it.accenture.jnais.ws.redefines.DfaImpCnbtTfrK2;
import it.accenture.jnais.ws.redefines.DfaImpCnbtTfrK3;
import it.accenture.jnais.ws.redefines.DfaImpCnbtVolK1;
import it.accenture.jnais.ws.redefines.DfaImpCnbtVolK2;
import it.accenture.jnais.ws.redefines.DfaImpCnbtVolK3;
import it.accenture.jnais.ws.redefines.DfaImpEseImpstTfr;
import it.accenture.jnais.ws.redefines.DfaImpNetTrasferito;
import it.accenture.jnais.ws.redefines.DfaImpst252Antic;
import it.accenture.jnais.ws.redefines.DfaImpst252K3;
import it.accenture.jnais.ws.redefines.DfaImpstIrpefK1Ant;
import it.accenture.jnais.ws.redefines.DfaImpstIrpefK2Ant;
import it.accenture.jnais.ws.redefines.DfaImpstSostK2;
import it.accenture.jnais.ws.redefines.DfaImpstSostK2Anti;
import it.accenture.jnais.ws.redefines.DfaImpstSostK3;
import it.accenture.jnais.ws.redefines.DfaImpstSostK3Anti;
import it.accenture.jnais.ws.redefines.DfaImpstTfrAntic;
import it.accenture.jnais.ws.redefines.DfaImpstVis;
import it.accenture.jnais.ws.redefines.DfaImpstVisAntic;
import it.accenture.jnais.ws.redefines.DfaMatuK1;
import it.accenture.jnais.ws.redefines.DfaMatuK2;
import it.accenture.jnais.ws.redefines.DfaMatuK3;
import it.accenture.jnais.ws.redefines.DfaMatuResK1;
import it.accenture.jnais.ws.redefines.DfaMmCnbzK1;
import it.accenture.jnais.ws.redefines.DfaMmCnbzK2;
import it.accenture.jnais.ws.redefines.DfaMmCnbzK3;
import it.accenture.jnais.ws.redefines.DfaOnerTrasfe;
import it.accenture.jnais.ws.redefines.DfaPcEseImpstTfr;
import it.accenture.jnais.ws.redefines.DfaPcTfr;
import it.accenture.jnais.ws.redefines.DfaRedtTassAbbatK2;
import it.accenture.jnais.ws.redefines.DfaRedtTassAbbatK3;
import it.accenture.jnais.ws.redefines.DfaRidzTfr;
import it.accenture.jnais.ws.redefines.DfaRidzTfrSuAntic;
import it.accenture.jnais.ws.redefines.DfaTotAntic;
import it.accenture.jnais.ws.redefines.DfaTotImpst;
import it.accenture.jnais.ws.redefines.DfaUltCommisTrasfe;

/**Original name: D-FISC-ADES<br>
 * Variable: D-FISC-ADES from copybook IDBVDFA1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DFiscAdesIdbsdfa0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: DFA-ID-D-FISC-ADES
    private int dfaIdDFiscAdes = DefaultValues.INT_VAL;
    //Original name: DFA-ID-ADES
    private int dfaIdAdes = DefaultValues.INT_VAL;
    //Original name: DFA-ID-MOVI-CRZ
    private int dfaIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: DFA-ID-MOVI-CHIU
    private DfaIdMoviChiu dfaIdMoviChiu = new DfaIdMoviChiu();
    //Original name: DFA-DT-INI-EFF
    private int dfaDtIniEff = DefaultValues.INT_VAL;
    //Original name: DFA-DT-END-EFF
    private int dfaDtEndEff = DefaultValues.INT_VAL;
    //Original name: DFA-COD-COMP-ANIA
    private int dfaCodCompAnia = DefaultValues.INT_VAL;
    //Original name: DFA-TP-ISC-FND
    private String dfaTpIscFnd = DefaultValues.stringVal(Len.DFA_TP_ISC_FND);
    //Original name: DFA-DT-ACCNS-RAPP-FND
    private DfaDtAccnsRappFnd dfaDtAccnsRappFnd = new DfaDtAccnsRappFnd();
    //Original name: DFA-IMP-CNBT-AZ-K1
    private DfaImpCnbtAzK1 dfaImpCnbtAzK1 = new DfaImpCnbtAzK1();
    //Original name: DFA-IMP-CNBT-ISC-K1
    private DfaImpCnbtIscK1 dfaImpCnbtIscK1 = new DfaImpCnbtIscK1();
    //Original name: DFA-IMP-CNBT-TFR-K1
    private DfaImpCnbtTfrK1 dfaImpCnbtTfrK1 = new DfaImpCnbtTfrK1();
    //Original name: DFA-IMP-CNBT-VOL-K1
    private DfaImpCnbtVolK1 dfaImpCnbtVolK1 = new DfaImpCnbtVolK1();
    //Original name: DFA-IMP-CNBT-AZ-K2
    private DfaImpCnbtAzK2 dfaImpCnbtAzK2 = new DfaImpCnbtAzK2();
    //Original name: DFA-IMP-CNBT-ISC-K2
    private DfaImpCnbtIscK2 dfaImpCnbtIscK2 = new DfaImpCnbtIscK2();
    //Original name: DFA-IMP-CNBT-TFR-K2
    private DfaImpCnbtTfrK2 dfaImpCnbtTfrK2 = new DfaImpCnbtTfrK2();
    //Original name: DFA-IMP-CNBT-VOL-K2
    private DfaImpCnbtVolK2 dfaImpCnbtVolK2 = new DfaImpCnbtVolK2();
    //Original name: DFA-MATU-K1
    private DfaMatuK1 dfaMatuK1 = new DfaMatuK1();
    //Original name: DFA-MATU-RES-K1
    private DfaMatuResK1 dfaMatuResK1 = new DfaMatuResK1();
    //Original name: DFA-MATU-K2
    private DfaMatuK2 dfaMatuK2 = new DfaMatuK2();
    //Original name: DFA-IMPB-VIS
    private DfaImpbVis dfaImpbVis = new DfaImpbVis();
    //Original name: DFA-IMPST-VIS
    private DfaImpstVis dfaImpstVis = new DfaImpstVis();
    //Original name: DFA-IMPB-IS-K2
    private DfaImpbIsK2 dfaImpbIsK2 = new DfaImpbIsK2();
    //Original name: DFA-IMPST-SOST-K2
    private DfaImpstSostK2 dfaImpstSostK2 = new DfaImpstSostK2();
    //Original name: DFA-RIDZ-TFR
    private DfaRidzTfr dfaRidzTfr = new DfaRidzTfr();
    //Original name: DFA-PC-TFR
    private DfaPcTfr dfaPcTfr = new DfaPcTfr();
    //Original name: DFA-ALQ-TFR
    private DfaAlqTfr dfaAlqTfr = new DfaAlqTfr();
    //Original name: DFA-TOT-ANTIC
    private DfaTotAntic dfaTotAntic = new DfaTotAntic();
    //Original name: DFA-IMPB-TFR-ANTIC
    private DfaImpbTfrAntic dfaImpbTfrAntic = new DfaImpbTfrAntic();
    //Original name: DFA-IMPST-TFR-ANTIC
    private DfaImpstTfrAntic dfaImpstTfrAntic = new DfaImpstTfrAntic();
    //Original name: DFA-RIDZ-TFR-SU-ANTIC
    private DfaRidzTfrSuAntic dfaRidzTfrSuAntic = new DfaRidzTfrSuAntic();
    //Original name: DFA-IMPB-VIS-ANTIC
    private DfaImpbVisAntic dfaImpbVisAntic = new DfaImpbVisAntic();
    //Original name: DFA-IMPST-VIS-ANTIC
    private DfaImpstVisAntic dfaImpstVisAntic = new DfaImpstVisAntic();
    //Original name: DFA-IMPB-IS-K2-ANTIC
    private DfaImpbIsK2Antic dfaImpbIsK2Antic = new DfaImpbIsK2Antic();
    //Original name: DFA-IMPST-SOST-K2-ANTI
    private DfaImpstSostK2Anti dfaImpstSostK2Anti = new DfaImpstSostK2Anti();
    //Original name: DFA-ULT-COMMIS-TRASFE
    private DfaUltCommisTrasfe dfaUltCommisTrasfe = new DfaUltCommisTrasfe();
    //Original name: DFA-COD-DVS
    private String dfaCodDvs = DefaultValues.stringVal(Len.DFA_COD_DVS);
    //Original name: DFA-ALQ-PRVR
    private DfaAlqPrvr dfaAlqPrvr = new DfaAlqPrvr();
    //Original name: DFA-PC-ESE-IMPST-TFR
    private DfaPcEseImpstTfr dfaPcEseImpstTfr = new DfaPcEseImpstTfr();
    //Original name: DFA-IMP-ESE-IMPST-TFR
    private DfaImpEseImpstTfr dfaImpEseImpstTfr = new DfaImpEseImpstTfr();
    //Original name: DFA-ANZ-CNBTVA-CARASS
    private DfaAnzCnbtvaCarass dfaAnzCnbtvaCarass = new DfaAnzCnbtvaCarass();
    //Original name: DFA-ANZ-CNBTVA-CARAZI
    private DfaAnzCnbtvaCarazi dfaAnzCnbtvaCarazi = new DfaAnzCnbtvaCarazi();
    //Original name: DFA-ANZ-SRVZ
    private DfaAnzSrvz dfaAnzSrvz = new DfaAnzSrvz();
    //Original name: DFA-IMP-CNBT-NDED-K1
    private DfaImpCnbtNdedK1 dfaImpCnbtNdedK1 = new DfaImpCnbtNdedK1();
    //Original name: DFA-IMP-CNBT-NDED-K2
    private DfaImpCnbtNdedK2 dfaImpCnbtNdedK2 = new DfaImpCnbtNdedK2();
    //Original name: DFA-IMP-CNBT-NDED-K3
    private DfaImpCnbtNdedK3 dfaImpCnbtNdedK3 = new DfaImpCnbtNdedK3();
    //Original name: DFA-IMP-CNBT-AZ-K3
    private DfaImpCnbtAzK3 dfaImpCnbtAzK3 = new DfaImpCnbtAzK3();
    //Original name: DFA-IMP-CNBT-ISC-K3
    private DfaImpCnbtIscK3 dfaImpCnbtIscK3 = new DfaImpCnbtIscK3();
    //Original name: DFA-IMP-CNBT-TFR-K3
    private DfaImpCnbtTfrK3 dfaImpCnbtTfrK3 = new DfaImpCnbtTfrK3();
    //Original name: DFA-IMP-CNBT-VOL-K3
    private DfaImpCnbtVolK3 dfaImpCnbtVolK3 = new DfaImpCnbtVolK3();
    //Original name: DFA-MATU-K3
    private DfaMatuK3 dfaMatuK3 = new DfaMatuK3();
    //Original name: DFA-IMPB-252-ANTIC
    private DfaImpb252Antic dfaImpb252Antic = new DfaImpb252Antic();
    //Original name: DFA-IMPST-252-ANTIC
    private DfaImpst252Antic dfaImpst252Antic = new DfaImpst252Antic();
    //Original name: DFA-DT-1A-CNBZ
    private DfaDt1aCnbz dfaDt1aCnbz = new DfaDt1aCnbz();
    //Original name: DFA-COMMIS-DI-TRASFE
    private DfaCommisDiTrasfe dfaCommisDiTrasfe = new DfaCommisDiTrasfe();
    //Original name: DFA-AA-CNBZ-K1
    private DfaAaCnbzK1 dfaAaCnbzK1 = new DfaAaCnbzK1();
    //Original name: DFA-AA-CNBZ-K2
    private DfaAaCnbzK2 dfaAaCnbzK2 = new DfaAaCnbzK2();
    //Original name: DFA-AA-CNBZ-K3
    private DfaAaCnbzK3 dfaAaCnbzK3 = new DfaAaCnbzK3();
    //Original name: DFA-MM-CNBZ-K1
    private DfaMmCnbzK1 dfaMmCnbzK1 = new DfaMmCnbzK1();
    //Original name: DFA-MM-CNBZ-K2
    private DfaMmCnbzK2 dfaMmCnbzK2 = new DfaMmCnbzK2();
    //Original name: DFA-MM-CNBZ-K3
    private DfaMmCnbzK3 dfaMmCnbzK3 = new DfaMmCnbzK3();
    //Original name: DFA-FL-APPLZ-NEWFIS
    private char dfaFlApplzNewfis = DefaultValues.CHAR_VAL;
    //Original name: DFA-REDT-TASS-ABBAT-K3
    private DfaRedtTassAbbatK3 dfaRedtTassAbbatK3 = new DfaRedtTassAbbatK3();
    //Original name: DFA-CNBT-ECC-4X100-K1
    private DfaCnbtEcc4x100K1 dfaCnbtEcc4x100K1 = new DfaCnbtEcc4x100K1();
    //Original name: DFA-DS-RIGA
    private long dfaDsRiga = DefaultValues.LONG_VAL;
    //Original name: DFA-DS-OPER-SQL
    private char dfaDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: DFA-DS-VER
    private int dfaDsVer = DefaultValues.INT_VAL;
    //Original name: DFA-DS-TS-INI-CPTZ
    private long dfaDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: DFA-DS-TS-END-CPTZ
    private long dfaDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: DFA-DS-UTENTE
    private String dfaDsUtente = DefaultValues.stringVal(Len.DFA_DS_UTENTE);
    //Original name: DFA-DS-STATO-ELAB
    private char dfaDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: DFA-CREDITO-IS
    private DfaCreditoIs dfaCreditoIs = new DfaCreditoIs();
    //Original name: DFA-REDT-TASS-ABBAT-K2
    private DfaRedtTassAbbatK2 dfaRedtTassAbbatK2 = new DfaRedtTassAbbatK2();
    //Original name: DFA-IMPB-IS-K3
    private DfaImpbIsK3 dfaImpbIsK3 = new DfaImpbIsK3();
    //Original name: DFA-IMPST-SOST-K3
    private DfaImpstSostK3 dfaImpstSostK3 = new DfaImpstSostK3();
    //Original name: DFA-IMPB-252-K3
    private DfaImpb252K3 dfaImpb252K3 = new DfaImpb252K3();
    //Original name: DFA-IMPST-252-K3
    private DfaImpst252K3 dfaImpst252K3 = new DfaImpst252K3();
    //Original name: DFA-IMPB-IS-K3-ANTIC
    private DfaImpbIsK3Antic dfaImpbIsK3Antic = new DfaImpbIsK3Antic();
    //Original name: DFA-IMPST-SOST-K3-ANTI
    private DfaImpstSostK3Anti dfaImpstSostK3Anti = new DfaImpstSostK3Anti();
    //Original name: DFA-IMPB-IRPEF-K1-ANTI
    private DfaImpbIrpefK1Anti dfaImpbIrpefK1Anti = new DfaImpbIrpefK1Anti();
    //Original name: DFA-IMPST-IRPEF-K1-ANT
    private DfaImpstIrpefK1Ant dfaImpstIrpefK1Ant = new DfaImpstIrpefK1Ant();
    //Original name: DFA-IMPB-IRPEF-K2-ANTI
    private DfaImpbIrpefK2Anti dfaImpbIrpefK2Anti = new DfaImpbIrpefK2Anti();
    //Original name: DFA-IMPST-IRPEF-K2-ANT
    private DfaImpstIrpefK2Ant dfaImpstIrpefK2Ant = new DfaImpstIrpefK2Ant();
    //Original name: DFA-DT-CESSAZIONE
    private DfaDtCessazione dfaDtCessazione = new DfaDtCessazione();
    //Original name: DFA-TOT-IMPST
    private DfaTotImpst dfaTotImpst = new DfaTotImpst();
    //Original name: DFA-ONER-TRASFE
    private DfaOnerTrasfe dfaOnerTrasfe = new DfaOnerTrasfe();
    //Original name: DFA-IMP-NET-TRASFERITO
    private DfaImpNetTrasferito dfaImpNetTrasferito = new DfaImpNetTrasferito();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.D_FISC_ADES;
    }

    @Override
    public void deserialize(byte[] buf) {
        setdFiscAdesBytes(buf);
    }

    public void setdFiscAdesFormatted(String data) {
        byte[] buffer = new byte[Len.D_FISC_ADES];
        MarshalByte.writeString(buffer, 1, data, Len.D_FISC_ADES);
        setdFiscAdesBytes(buffer, 1);
    }

    public String getdFiscAdesFormatted() {
        return MarshalByteExt.bufferToStr(getdFiscAdesBytes());
    }

    public void setdFiscAdesBytes(byte[] buffer) {
        setdFiscAdesBytes(buffer, 1);
    }

    public byte[] getdFiscAdesBytes() {
        byte[] buffer = new byte[Len.D_FISC_ADES];
        return getdFiscAdesBytes(buffer, 1);
    }

    public void setdFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        dfaIdDFiscAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFA_ID_D_FISC_ADES, 0);
        position += Len.DFA_ID_D_FISC_ADES;
        dfaIdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFA_ID_ADES, 0);
        position += Len.DFA_ID_ADES;
        dfaIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFA_ID_MOVI_CRZ, 0);
        position += Len.DFA_ID_MOVI_CRZ;
        dfaIdMoviChiu.setDfaIdMoviChiuFromBuffer(buffer, position);
        position += DfaIdMoviChiu.Len.DFA_ID_MOVI_CHIU;
        dfaDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFA_DT_INI_EFF, 0);
        position += Len.DFA_DT_INI_EFF;
        dfaDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFA_DT_END_EFF, 0);
        position += Len.DFA_DT_END_EFF;
        dfaCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFA_COD_COMP_ANIA, 0);
        position += Len.DFA_COD_COMP_ANIA;
        dfaTpIscFnd = MarshalByte.readString(buffer, position, Len.DFA_TP_ISC_FND);
        position += Len.DFA_TP_ISC_FND;
        dfaDtAccnsRappFnd.setDfaDtAccnsRappFndFromBuffer(buffer, position);
        position += DfaDtAccnsRappFnd.Len.DFA_DT_ACCNS_RAPP_FND;
        dfaImpCnbtAzK1.setDfaImpCnbtAzK1FromBuffer(buffer, position);
        position += DfaImpCnbtAzK1.Len.DFA_IMP_CNBT_AZ_K1;
        dfaImpCnbtIscK1.setDfaImpCnbtIscK1FromBuffer(buffer, position);
        position += DfaImpCnbtIscK1.Len.DFA_IMP_CNBT_ISC_K1;
        dfaImpCnbtTfrK1.setDfaImpCnbtTfrK1FromBuffer(buffer, position);
        position += DfaImpCnbtTfrK1.Len.DFA_IMP_CNBT_TFR_K1;
        dfaImpCnbtVolK1.setDfaImpCnbtVolK1FromBuffer(buffer, position);
        position += DfaImpCnbtVolK1.Len.DFA_IMP_CNBT_VOL_K1;
        dfaImpCnbtAzK2.setDfaImpCnbtAzK2FromBuffer(buffer, position);
        position += DfaImpCnbtAzK2.Len.DFA_IMP_CNBT_AZ_K2;
        dfaImpCnbtIscK2.setDfaImpCnbtIscK2FromBuffer(buffer, position);
        position += DfaImpCnbtIscK2.Len.DFA_IMP_CNBT_ISC_K2;
        dfaImpCnbtTfrK2.setDfaImpCnbtTfrK2FromBuffer(buffer, position);
        position += DfaImpCnbtTfrK2.Len.DFA_IMP_CNBT_TFR_K2;
        dfaImpCnbtVolK2.setDfaImpCnbtVolK2FromBuffer(buffer, position);
        position += DfaImpCnbtVolK2.Len.DFA_IMP_CNBT_VOL_K2;
        dfaMatuK1.setDfaMatuK1FromBuffer(buffer, position);
        position += DfaMatuK1.Len.DFA_MATU_K1;
        dfaMatuResK1.setDfaMatuResK1FromBuffer(buffer, position);
        position += DfaMatuResK1.Len.DFA_MATU_RES_K1;
        dfaMatuK2.setDfaMatuK2FromBuffer(buffer, position);
        position += DfaMatuK2.Len.DFA_MATU_K2;
        dfaImpbVis.setDfaImpbVisFromBuffer(buffer, position);
        position += DfaImpbVis.Len.DFA_IMPB_VIS;
        dfaImpstVis.setDfaImpstVisFromBuffer(buffer, position);
        position += DfaImpstVis.Len.DFA_IMPST_VIS;
        dfaImpbIsK2.setDfaImpbIsK2FromBuffer(buffer, position);
        position += DfaImpbIsK2.Len.DFA_IMPB_IS_K2;
        dfaImpstSostK2.setDfaImpstSostK2FromBuffer(buffer, position);
        position += DfaImpstSostK2.Len.DFA_IMPST_SOST_K2;
        dfaRidzTfr.setDfaRidzTfrFromBuffer(buffer, position);
        position += DfaRidzTfr.Len.DFA_RIDZ_TFR;
        dfaPcTfr.setDfaPcTfrFromBuffer(buffer, position);
        position += DfaPcTfr.Len.DFA_PC_TFR;
        dfaAlqTfr.setDfaAlqTfrFromBuffer(buffer, position);
        position += DfaAlqTfr.Len.DFA_ALQ_TFR;
        dfaTotAntic.setDfaTotAnticFromBuffer(buffer, position);
        position += DfaTotAntic.Len.DFA_TOT_ANTIC;
        dfaImpbTfrAntic.setDfaImpbTfrAnticFromBuffer(buffer, position);
        position += DfaImpbTfrAntic.Len.DFA_IMPB_TFR_ANTIC;
        dfaImpstTfrAntic.setDfaImpstTfrAnticFromBuffer(buffer, position);
        position += DfaImpstTfrAntic.Len.DFA_IMPST_TFR_ANTIC;
        dfaRidzTfrSuAntic.setDfaRidzTfrSuAnticFromBuffer(buffer, position);
        position += DfaRidzTfrSuAntic.Len.DFA_RIDZ_TFR_SU_ANTIC;
        dfaImpbVisAntic.setDfaImpbVisAnticFromBuffer(buffer, position);
        position += DfaImpbVisAntic.Len.DFA_IMPB_VIS_ANTIC;
        dfaImpstVisAntic.setDfaImpstVisAnticFromBuffer(buffer, position);
        position += DfaImpstVisAntic.Len.DFA_IMPST_VIS_ANTIC;
        dfaImpbIsK2Antic.setDfaImpbIsK2AnticFromBuffer(buffer, position);
        position += DfaImpbIsK2Antic.Len.DFA_IMPB_IS_K2_ANTIC;
        dfaImpstSostK2Anti.setDfaImpstSostK2AntiFromBuffer(buffer, position);
        position += DfaImpstSostK2Anti.Len.DFA_IMPST_SOST_K2_ANTI;
        dfaUltCommisTrasfe.setDfaUltCommisTrasfeFromBuffer(buffer, position);
        position += DfaUltCommisTrasfe.Len.DFA_ULT_COMMIS_TRASFE;
        dfaCodDvs = MarshalByte.readString(buffer, position, Len.DFA_COD_DVS);
        position += Len.DFA_COD_DVS;
        dfaAlqPrvr.setDfaAlqPrvrFromBuffer(buffer, position);
        position += DfaAlqPrvr.Len.DFA_ALQ_PRVR;
        dfaPcEseImpstTfr.setDfaPcEseImpstTfrFromBuffer(buffer, position);
        position += DfaPcEseImpstTfr.Len.DFA_PC_ESE_IMPST_TFR;
        dfaImpEseImpstTfr.setDfaImpEseImpstTfrFromBuffer(buffer, position);
        position += DfaImpEseImpstTfr.Len.DFA_IMP_ESE_IMPST_TFR;
        dfaAnzCnbtvaCarass.setDfaAnzCnbtvaCarassFromBuffer(buffer, position);
        position += DfaAnzCnbtvaCarass.Len.DFA_ANZ_CNBTVA_CARASS;
        dfaAnzCnbtvaCarazi.setDfaAnzCnbtvaCaraziFromBuffer(buffer, position);
        position += DfaAnzCnbtvaCarazi.Len.DFA_ANZ_CNBTVA_CARAZI;
        dfaAnzSrvz.setDfaAnzSrvzFromBuffer(buffer, position);
        position += DfaAnzSrvz.Len.DFA_ANZ_SRVZ;
        dfaImpCnbtNdedK1.setDfaImpCnbtNdedK1FromBuffer(buffer, position);
        position += DfaImpCnbtNdedK1.Len.DFA_IMP_CNBT_NDED_K1;
        dfaImpCnbtNdedK2.setDfaImpCnbtNdedK2FromBuffer(buffer, position);
        position += DfaImpCnbtNdedK2.Len.DFA_IMP_CNBT_NDED_K2;
        dfaImpCnbtNdedK3.setDfaImpCnbtNdedK3FromBuffer(buffer, position);
        position += DfaImpCnbtNdedK3.Len.DFA_IMP_CNBT_NDED_K3;
        dfaImpCnbtAzK3.setDfaImpCnbtAzK3FromBuffer(buffer, position);
        position += DfaImpCnbtAzK3.Len.DFA_IMP_CNBT_AZ_K3;
        dfaImpCnbtIscK3.setDfaImpCnbtIscK3FromBuffer(buffer, position);
        position += DfaImpCnbtIscK3.Len.DFA_IMP_CNBT_ISC_K3;
        dfaImpCnbtTfrK3.setDfaImpCnbtTfrK3FromBuffer(buffer, position);
        position += DfaImpCnbtTfrK3.Len.DFA_IMP_CNBT_TFR_K3;
        dfaImpCnbtVolK3.setDfaImpCnbtVolK3FromBuffer(buffer, position);
        position += DfaImpCnbtVolK3.Len.DFA_IMP_CNBT_VOL_K3;
        dfaMatuK3.setDfaMatuK3FromBuffer(buffer, position);
        position += DfaMatuK3.Len.DFA_MATU_K3;
        dfaImpb252Antic.setDfaImpb252AnticFromBuffer(buffer, position);
        position += DfaImpb252Antic.Len.DFA_IMPB252_ANTIC;
        dfaImpst252Antic.setDfaImpst252AnticFromBuffer(buffer, position);
        position += DfaImpst252Antic.Len.DFA_IMPST252_ANTIC;
        dfaDt1aCnbz.setDfaDt1aCnbzFromBuffer(buffer, position);
        position += DfaDt1aCnbz.Len.DFA_DT1A_CNBZ;
        dfaCommisDiTrasfe.setDfaCommisDiTrasfeFromBuffer(buffer, position);
        position += DfaCommisDiTrasfe.Len.DFA_COMMIS_DI_TRASFE;
        dfaAaCnbzK1.setDfaAaCnbzK1FromBuffer(buffer, position);
        position += DfaAaCnbzK1.Len.DFA_AA_CNBZ_K1;
        dfaAaCnbzK2.setDfaAaCnbzK2FromBuffer(buffer, position);
        position += DfaAaCnbzK2.Len.DFA_AA_CNBZ_K2;
        dfaAaCnbzK3.setDfaAaCnbzK3FromBuffer(buffer, position);
        position += DfaAaCnbzK3.Len.DFA_AA_CNBZ_K3;
        dfaMmCnbzK1.setDfaMmCnbzK1FromBuffer(buffer, position);
        position += DfaMmCnbzK1.Len.DFA_MM_CNBZ_K1;
        dfaMmCnbzK2.setDfaMmCnbzK2FromBuffer(buffer, position);
        position += DfaMmCnbzK2.Len.DFA_MM_CNBZ_K2;
        dfaMmCnbzK3.setDfaMmCnbzK3FromBuffer(buffer, position);
        position += DfaMmCnbzK3.Len.DFA_MM_CNBZ_K3;
        dfaFlApplzNewfis = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dfaRedtTassAbbatK3.setDfaRedtTassAbbatK3FromBuffer(buffer, position);
        position += DfaRedtTassAbbatK3.Len.DFA_REDT_TASS_ABBAT_K3;
        dfaCnbtEcc4x100K1.setDfaCnbtEcc4x100K1FromBuffer(buffer, position);
        position += DfaCnbtEcc4x100K1.Len.DFA_CNBT_ECC4X100_K1;
        dfaDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DFA_DS_RIGA, 0);
        position += Len.DFA_DS_RIGA;
        dfaDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dfaDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFA_DS_VER, 0);
        position += Len.DFA_DS_VER;
        dfaDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DFA_DS_TS_INI_CPTZ, 0);
        position += Len.DFA_DS_TS_INI_CPTZ;
        dfaDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DFA_DS_TS_END_CPTZ, 0);
        position += Len.DFA_DS_TS_END_CPTZ;
        dfaDsUtente = MarshalByte.readString(buffer, position, Len.DFA_DS_UTENTE);
        position += Len.DFA_DS_UTENTE;
        dfaDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dfaCreditoIs.setDfaCreditoIsFromBuffer(buffer, position);
        position += DfaCreditoIs.Len.DFA_CREDITO_IS;
        dfaRedtTassAbbatK2.setDfaRedtTassAbbatK2FromBuffer(buffer, position);
        position += DfaRedtTassAbbatK2.Len.DFA_REDT_TASS_ABBAT_K2;
        dfaImpbIsK3.setDfaImpbIsK3FromBuffer(buffer, position);
        position += DfaImpbIsK3.Len.DFA_IMPB_IS_K3;
        dfaImpstSostK3.setDfaImpstSostK3FromBuffer(buffer, position);
        position += DfaImpstSostK3.Len.DFA_IMPST_SOST_K3;
        dfaImpb252K3.setDfaImpb252K3FromBuffer(buffer, position);
        position += DfaImpb252K3.Len.DFA_IMPB252_K3;
        dfaImpst252K3.setDfaImpst252K3FromBuffer(buffer, position);
        position += DfaImpst252K3.Len.DFA_IMPST252_K3;
        dfaImpbIsK3Antic.setDfaImpbIsK3AnticFromBuffer(buffer, position);
        position += DfaImpbIsK3Antic.Len.DFA_IMPB_IS_K3_ANTIC;
        dfaImpstSostK3Anti.setDfaImpstSostK3AntiFromBuffer(buffer, position);
        position += DfaImpstSostK3Anti.Len.DFA_IMPST_SOST_K3_ANTI;
        dfaImpbIrpefK1Anti.setDfaImpbIrpefK1AntiFromBuffer(buffer, position);
        position += DfaImpbIrpefK1Anti.Len.DFA_IMPB_IRPEF_K1_ANTI;
        dfaImpstIrpefK1Ant.setDfaImpstIrpefK1AntFromBuffer(buffer, position);
        position += DfaImpstIrpefK1Ant.Len.DFA_IMPST_IRPEF_K1_ANT;
        dfaImpbIrpefK2Anti.setDfaImpbIrpefK2AntiFromBuffer(buffer, position);
        position += DfaImpbIrpefK2Anti.Len.DFA_IMPB_IRPEF_K2_ANTI;
        dfaImpstIrpefK2Ant.setDfaImpstIrpefK2AntFromBuffer(buffer, position);
        position += DfaImpstIrpefK2Ant.Len.DFA_IMPST_IRPEF_K2_ANT;
        dfaDtCessazione.setDfaDtCessazioneFromBuffer(buffer, position);
        position += DfaDtCessazione.Len.DFA_DT_CESSAZIONE;
        dfaTotImpst.setDfaTotImpstFromBuffer(buffer, position);
        position += DfaTotImpst.Len.DFA_TOT_IMPST;
        dfaOnerTrasfe.setDfaOnerTrasfeFromBuffer(buffer, position);
        position += DfaOnerTrasfe.Len.DFA_ONER_TRASFE;
        dfaImpNetTrasferito.setDfaImpNetTrasferitoFromBuffer(buffer, position);
    }

    public byte[] getdFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, dfaIdDFiscAdes, Len.Int.DFA_ID_D_FISC_ADES, 0);
        position += Len.DFA_ID_D_FISC_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, dfaIdAdes, Len.Int.DFA_ID_ADES, 0);
        position += Len.DFA_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, dfaIdMoviCrz, Len.Int.DFA_ID_MOVI_CRZ, 0);
        position += Len.DFA_ID_MOVI_CRZ;
        dfaIdMoviChiu.getDfaIdMoviChiuAsBuffer(buffer, position);
        position += DfaIdMoviChiu.Len.DFA_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dfaDtIniEff, Len.Int.DFA_DT_INI_EFF, 0);
        position += Len.DFA_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dfaDtEndEff, Len.Int.DFA_DT_END_EFF, 0);
        position += Len.DFA_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dfaCodCompAnia, Len.Int.DFA_COD_COMP_ANIA, 0);
        position += Len.DFA_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, dfaTpIscFnd, Len.DFA_TP_ISC_FND);
        position += Len.DFA_TP_ISC_FND;
        dfaDtAccnsRappFnd.getDfaDtAccnsRappFndAsBuffer(buffer, position);
        position += DfaDtAccnsRappFnd.Len.DFA_DT_ACCNS_RAPP_FND;
        dfaImpCnbtAzK1.getDfaImpCnbtAzK1AsBuffer(buffer, position);
        position += DfaImpCnbtAzK1.Len.DFA_IMP_CNBT_AZ_K1;
        dfaImpCnbtIscK1.getDfaImpCnbtIscK1AsBuffer(buffer, position);
        position += DfaImpCnbtIscK1.Len.DFA_IMP_CNBT_ISC_K1;
        dfaImpCnbtTfrK1.getDfaImpCnbtTfrK1AsBuffer(buffer, position);
        position += DfaImpCnbtTfrK1.Len.DFA_IMP_CNBT_TFR_K1;
        dfaImpCnbtVolK1.getDfaImpCnbtVolK1AsBuffer(buffer, position);
        position += DfaImpCnbtVolK1.Len.DFA_IMP_CNBT_VOL_K1;
        dfaImpCnbtAzK2.getDfaImpCnbtAzK2AsBuffer(buffer, position);
        position += DfaImpCnbtAzK2.Len.DFA_IMP_CNBT_AZ_K2;
        dfaImpCnbtIscK2.getDfaImpCnbtIscK2AsBuffer(buffer, position);
        position += DfaImpCnbtIscK2.Len.DFA_IMP_CNBT_ISC_K2;
        dfaImpCnbtTfrK2.getDfaImpCnbtTfrK2AsBuffer(buffer, position);
        position += DfaImpCnbtTfrK2.Len.DFA_IMP_CNBT_TFR_K2;
        dfaImpCnbtVolK2.getDfaImpCnbtVolK2AsBuffer(buffer, position);
        position += DfaImpCnbtVolK2.Len.DFA_IMP_CNBT_VOL_K2;
        dfaMatuK1.getDfaMatuK1AsBuffer(buffer, position);
        position += DfaMatuK1.Len.DFA_MATU_K1;
        dfaMatuResK1.getDfaMatuResK1AsBuffer(buffer, position);
        position += DfaMatuResK1.Len.DFA_MATU_RES_K1;
        dfaMatuK2.getDfaMatuK2AsBuffer(buffer, position);
        position += DfaMatuK2.Len.DFA_MATU_K2;
        dfaImpbVis.getDfaImpbVisAsBuffer(buffer, position);
        position += DfaImpbVis.Len.DFA_IMPB_VIS;
        dfaImpstVis.getDfaImpstVisAsBuffer(buffer, position);
        position += DfaImpstVis.Len.DFA_IMPST_VIS;
        dfaImpbIsK2.getDfaImpbIsK2AsBuffer(buffer, position);
        position += DfaImpbIsK2.Len.DFA_IMPB_IS_K2;
        dfaImpstSostK2.getDfaImpstSostK2AsBuffer(buffer, position);
        position += DfaImpstSostK2.Len.DFA_IMPST_SOST_K2;
        dfaRidzTfr.getDfaRidzTfrAsBuffer(buffer, position);
        position += DfaRidzTfr.Len.DFA_RIDZ_TFR;
        dfaPcTfr.getDfaPcTfrAsBuffer(buffer, position);
        position += DfaPcTfr.Len.DFA_PC_TFR;
        dfaAlqTfr.getDfaAlqTfrAsBuffer(buffer, position);
        position += DfaAlqTfr.Len.DFA_ALQ_TFR;
        dfaTotAntic.getDfaTotAnticAsBuffer(buffer, position);
        position += DfaTotAntic.Len.DFA_TOT_ANTIC;
        dfaImpbTfrAntic.getDfaImpbTfrAnticAsBuffer(buffer, position);
        position += DfaImpbTfrAntic.Len.DFA_IMPB_TFR_ANTIC;
        dfaImpstTfrAntic.getDfaImpstTfrAnticAsBuffer(buffer, position);
        position += DfaImpstTfrAntic.Len.DFA_IMPST_TFR_ANTIC;
        dfaRidzTfrSuAntic.getDfaRidzTfrSuAnticAsBuffer(buffer, position);
        position += DfaRidzTfrSuAntic.Len.DFA_RIDZ_TFR_SU_ANTIC;
        dfaImpbVisAntic.getDfaImpbVisAnticAsBuffer(buffer, position);
        position += DfaImpbVisAntic.Len.DFA_IMPB_VIS_ANTIC;
        dfaImpstVisAntic.getDfaImpstVisAnticAsBuffer(buffer, position);
        position += DfaImpstVisAntic.Len.DFA_IMPST_VIS_ANTIC;
        dfaImpbIsK2Antic.getDfaImpbIsK2AnticAsBuffer(buffer, position);
        position += DfaImpbIsK2Antic.Len.DFA_IMPB_IS_K2_ANTIC;
        dfaImpstSostK2Anti.getDfaImpstSostK2AntiAsBuffer(buffer, position);
        position += DfaImpstSostK2Anti.Len.DFA_IMPST_SOST_K2_ANTI;
        dfaUltCommisTrasfe.getDfaUltCommisTrasfeAsBuffer(buffer, position);
        position += DfaUltCommisTrasfe.Len.DFA_ULT_COMMIS_TRASFE;
        MarshalByte.writeString(buffer, position, dfaCodDvs, Len.DFA_COD_DVS);
        position += Len.DFA_COD_DVS;
        dfaAlqPrvr.getDfaAlqPrvrAsBuffer(buffer, position);
        position += DfaAlqPrvr.Len.DFA_ALQ_PRVR;
        dfaPcEseImpstTfr.getDfaPcEseImpstTfrAsBuffer(buffer, position);
        position += DfaPcEseImpstTfr.Len.DFA_PC_ESE_IMPST_TFR;
        dfaImpEseImpstTfr.getDfaImpEseImpstTfrAsBuffer(buffer, position);
        position += DfaImpEseImpstTfr.Len.DFA_IMP_ESE_IMPST_TFR;
        dfaAnzCnbtvaCarass.getDfaAnzCnbtvaCarassAsBuffer(buffer, position);
        position += DfaAnzCnbtvaCarass.Len.DFA_ANZ_CNBTVA_CARASS;
        dfaAnzCnbtvaCarazi.getDfaAnzCnbtvaCaraziAsBuffer(buffer, position);
        position += DfaAnzCnbtvaCarazi.Len.DFA_ANZ_CNBTVA_CARAZI;
        dfaAnzSrvz.getDfaAnzSrvzAsBuffer(buffer, position);
        position += DfaAnzSrvz.Len.DFA_ANZ_SRVZ;
        dfaImpCnbtNdedK1.getDfaImpCnbtNdedK1AsBuffer(buffer, position);
        position += DfaImpCnbtNdedK1.Len.DFA_IMP_CNBT_NDED_K1;
        dfaImpCnbtNdedK2.getDfaImpCnbtNdedK2AsBuffer(buffer, position);
        position += DfaImpCnbtNdedK2.Len.DFA_IMP_CNBT_NDED_K2;
        dfaImpCnbtNdedK3.getDfaImpCnbtNdedK3AsBuffer(buffer, position);
        position += DfaImpCnbtNdedK3.Len.DFA_IMP_CNBT_NDED_K3;
        dfaImpCnbtAzK3.getDfaImpCnbtAzK3AsBuffer(buffer, position);
        position += DfaImpCnbtAzK3.Len.DFA_IMP_CNBT_AZ_K3;
        dfaImpCnbtIscK3.getDfaImpCnbtIscK3AsBuffer(buffer, position);
        position += DfaImpCnbtIscK3.Len.DFA_IMP_CNBT_ISC_K3;
        dfaImpCnbtTfrK3.getDfaImpCnbtTfrK3AsBuffer(buffer, position);
        position += DfaImpCnbtTfrK3.Len.DFA_IMP_CNBT_TFR_K3;
        dfaImpCnbtVolK3.getDfaImpCnbtVolK3AsBuffer(buffer, position);
        position += DfaImpCnbtVolK3.Len.DFA_IMP_CNBT_VOL_K3;
        dfaMatuK3.getDfaMatuK3AsBuffer(buffer, position);
        position += DfaMatuK3.Len.DFA_MATU_K3;
        dfaImpb252Antic.getDfaImpb252AnticAsBuffer(buffer, position);
        position += DfaImpb252Antic.Len.DFA_IMPB252_ANTIC;
        dfaImpst252Antic.getDfaImpst252AnticAsBuffer(buffer, position);
        position += DfaImpst252Antic.Len.DFA_IMPST252_ANTIC;
        dfaDt1aCnbz.getDfaDt1aCnbzAsBuffer(buffer, position);
        position += DfaDt1aCnbz.Len.DFA_DT1A_CNBZ;
        dfaCommisDiTrasfe.getDfaCommisDiTrasfeAsBuffer(buffer, position);
        position += DfaCommisDiTrasfe.Len.DFA_COMMIS_DI_TRASFE;
        dfaAaCnbzK1.getDfaAaCnbzK1AsBuffer(buffer, position);
        position += DfaAaCnbzK1.Len.DFA_AA_CNBZ_K1;
        dfaAaCnbzK2.getDfaAaCnbzK2AsBuffer(buffer, position);
        position += DfaAaCnbzK2.Len.DFA_AA_CNBZ_K2;
        dfaAaCnbzK3.getDfaAaCnbzK3AsBuffer(buffer, position);
        position += DfaAaCnbzK3.Len.DFA_AA_CNBZ_K3;
        dfaMmCnbzK1.getDfaMmCnbzK1AsBuffer(buffer, position);
        position += DfaMmCnbzK1.Len.DFA_MM_CNBZ_K1;
        dfaMmCnbzK2.getDfaMmCnbzK2AsBuffer(buffer, position);
        position += DfaMmCnbzK2.Len.DFA_MM_CNBZ_K2;
        dfaMmCnbzK3.getDfaMmCnbzK3AsBuffer(buffer, position);
        position += DfaMmCnbzK3.Len.DFA_MM_CNBZ_K3;
        MarshalByte.writeChar(buffer, position, dfaFlApplzNewfis);
        position += Types.CHAR_SIZE;
        dfaRedtTassAbbatK3.getDfaRedtTassAbbatK3AsBuffer(buffer, position);
        position += DfaRedtTassAbbatK3.Len.DFA_REDT_TASS_ABBAT_K3;
        dfaCnbtEcc4x100K1.getDfaCnbtEcc4x100K1AsBuffer(buffer, position);
        position += DfaCnbtEcc4x100K1.Len.DFA_CNBT_ECC4X100_K1;
        MarshalByte.writeLongAsPacked(buffer, position, dfaDsRiga, Len.Int.DFA_DS_RIGA, 0);
        position += Len.DFA_DS_RIGA;
        MarshalByte.writeChar(buffer, position, dfaDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dfaDsVer, Len.Int.DFA_DS_VER, 0);
        position += Len.DFA_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dfaDsTsIniCptz, Len.Int.DFA_DS_TS_INI_CPTZ, 0);
        position += Len.DFA_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dfaDsTsEndCptz, Len.Int.DFA_DS_TS_END_CPTZ, 0);
        position += Len.DFA_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dfaDsUtente, Len.DFA_DS_UTENTE);
        position += Len.DFA_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dfaDsStatoElab);
        position += Types.CHAR_SIZE;
        dfaCreditoIs.getDfaCreditoIsAsBuffer(buffer, position);
        position += DfaCreditoIs.Len.DFA_CREDITO_IS;
        dfaRedtTassAbbatK2.getDfaRedtTassAbbatK2AsBuffer(buffer, position);
        position += DfaRedtTassAbbatK2.Len.DFA_REDT_TASS_ABBAT_K2;
        dfaImpbIsK3.getDfaImpbIsK3AsBuffer(buffer, position);
        position += DfaImpbIsK3.Len.DFA_IMPB_IS_K3;
        dfaImpstSostK3.getDfaImpstSostK3AsBuffer(buffer, position);
        position += DfaImpstSostK3.Len.DFA_IMPST_SOST_K3;
        dfaImpb252K3.getDfaImpb252K3AsBuffer(buffer, position);
        position += DfaImpb252K3.Len.DFA_IMPB252_K3;
        dfaImpst252K3.getDfaImpst252K3AsBuffer(buffer, position);
        position += DfaImpst252K3.Len.DFA_IMPST252_K3;
        dfaImpbIsK3Antic.getDfaImpbIsK3AnticAsBuffer(buffer, position);
        position += DfaImpbIsK3Antic.Len.DFA_IMPB_IS_K3_ANTIC;
        dfaImpstSostK3Anti.getDfaImpstSostK3AntiAsBuffer(buffer, position);
        position += DfaImpstSostK3Anti.Len.DFA_IMPST_SOST_K3_ANTI;
        dfaImpbIrpefK1Anti.getDfaImpbIrpefK1AntiAsBuffer(buffer, position);
        position += DfaImpbIrpefK1Anti.Len.DFA_IMPB_IRPEF_K1_ANTI;
        dfaImpstIrpefK1Ant.getDfaImpstIrpefK1AntAsBuffer(buffer, position);
        position += DfaImpstIrpefK1Ant.Len.DFA_IMPST_IRPEF_K1_ANT;
        dfaImpbIrpefK2Anti.getDfaImpbIrpefK2AntiAsBuffer(buffer, position);
        position += DfaImpbIrpefK2Anti.Len.DFA_IMPB_IRPEF_K2_ANTI;
        dfaImpstIrpefK2Ant.getDfaImpstIrpefK2AntAsBuffer(buffer, position);
        position += DfaImpstIrpefK2Ant.Len.DFA_IMPST_IRPEF_K2_ANT;
        dfaDtCessazione.getDfaDtCessazioneAsBuffer(buffer, position);
        position += DfaDtCessazione.Len.DFA_DT_CESSAZIONE;
        dfaTotImpst.getDfaTotImpstAsBuffer(buffer, position);
        position += DfaTotImpst.Len.DFA_TOT_IMPST;
        dfaOnerTrasfe.getDfaOnerTrasfeAsBuffer(buffer, position);
        position += DfaOnerTrasfe.Len.DFA_ONER_TRASFE;
        dfaImpNetTrasferito.getDfaImpNetTrasferitoAsBuffer(buffer, position);
        return buffer;
    }

    public void setDfaIdDFiscAdes(int dfaIdDFiscAdes) {
        this.dfaIdDFiscAdes = dfaIdDFiscAdes;
    }

    public int getDfaIdDFiscAdes() {
        return this.dfaIdDFiscAdes;
    }

    public void setDfaIdAdes(int dfaIdAdes) {
        this.dfaIdAdes = dfaIdAdes;
    }

    public int getDfaIdAdes() {
        return this.dfaIdAdes;
    }

    public void setDfaIdMoviCrz(int dfaIdMoviCrz) {
        this.dfaIdMoviCrz = dfaIdMoviCrz;
    }

    public int getDfaIdMoviCrz() {
        return this.dfaIdMoviCrz;
    }

    public void setDfaDtIniEff(int dfaDtIniEff) {
        this.dfaDtIniEff = dfaDtIniEff;
    }

    public int getDfaDtIniEff() {
        return this.dfaDtIniEff;
    }

    public void setDfaDtEndEff(int dfaDtEndEff) {
        this.dfaDtEndEff = dfaDtEndEff;
    }

    public int getDfaDtEndEff() {
        return this.dfaDtEndEff;
    }

    public void setDfaCodCompAnia(int dfaCodCompAnia) {
        this.dfaCodCompAnia = dfaCodCompAnia;
    }

    public int getDfaCodCompAnia() {
        return this.dfaCodCompAnia;
    }

    public void setDfaTpIscFnd(String dfaTpIscFnd) {
        this.dfaTpIscFnd = Functions.subString(dfaTpIscFnd, Len.DFA_TP_ISC_FND);
    }

    public String getDfaTpIscFnd() {
        return this.dfaTpIscFnd;
    }

    public String getDfaTpIscFndFormatted() {
        return Functions.padBlanks(getDfaTpIscFnd(), Len.DFA_TP_ISC_FND);
    }

    public void setDfaCodDvs(String dfaCodDvs) {
        this.dfaCodDvs = Functions.subString(dfaCodDvs, Len.DFA_COD_DVS);
    }

    public String getDfaCodDvs() {
        return this.dfaCodDvs;
    }

    public String getDfaCodDvsFormatted() {
        return Functions.padBlanks(getDfaCodDvs(), Len.DFA_COD_DVS);
    }

    public void setDfaFlApplzNewfis(char dfaFlApplzNewfis) {
        this.dfaFlApplzNewfis = dfaFlApplzNewfis;
    }

    public char getDfaFlApplzNewfis() {
        return this.dfaFlApplzNewfis;
    }

    public void setDfaDsRiga(long dfaDsRiga) {
        this.dfaDsRiga = dfaDsRiga;
    }

    public long getDfaDsRiga() {
        return this.dfaDsRiga;
    }

    public void setDfaDsOperSql(char dfaDsOperSql) {
        this.dfaDsOperSql = dfaDsOperSql;
    }

    public void setDfaDsOperSqlFormatted(String dfaDsOperSql) {
        setDfaDsOperSql(Functions.charAt(dfaDsOperSql, Types.CHAR_SIZE));
    }

    public char getDfaDsOperSql() {
        return this.dfaDsOperSql;
    }

    public void setDfaDsVer(int dfaDsVer) {
        this.dfaDsVer = dfaDsVer;
    }

    public int getDfaDsVer() {
        return this.dfaDsVer;
    }

    public void setDfaDsTsIniCptz(long dfaDsTsIniCptz) {
        this.dfaDsTsIniCptz = dfaDsTsIniCptz;
    }

    public long getDfaDsTsIniCptz() {
        return this.dfaDsTsIniCptz;
    }

    public void setDfaDsTsEndCptz(long dfaDsTsEndCptz) {
        this.dfaDsTsEndCptz = dfaDsTsEndCptz;
    }

    public long getDfaDsTsEndCptz() {
        return this.dfaDsTsEndCptz;
    }

    public void setDfaDsUtente(String dfaDsUtente) {
        this.dfaDsUtente = Functions.subString(dfaDsUtente, Len.DFA_DS_UTENTE);
    }

    public String getDfaDsUtente() {
        return this.dfaDsUtente;
    }

    public void setDfaDsStatoElab(char dfaDsStatoElab) {
        this.dfaDsStatoElab = dfaDsStatoElab;
    }

    public void setDfaDsStatoElabFormatted(String dfaDsStatoElab) {
        setDfaDsStatoElab(Functions.charAt(dfaDsStatoElab, Types.CHAR_SIZE));
    }

    public char getDfaDsStatoElab() {
        return this.dfaDsStatoElab;
    }

    public DfaAaCnbzK1 getDfaAaCnbzK1() {
        return dfaAaCnbzK1;
    }

    public DfaAaCnbzK2 getDfaAaCnbzK2() {
        return dfaAaCnbzK2;
    }

    public DfaAaCnbzK3 getDfaAaCnbzK3() {
        return dfaAaCnbzK3;
    }

    public DfaAlqPrvr getDfaAlqPrvr() {
        return dfaAlqPrvr;
    }

    public DfaAlqTfr getDfaAlqTfr() {
        return dfaAlqTfr;
    }

    public DfaAnzCnbtvaCarass getDfaAnzCnbtvaCarass() {
        return dfaAnzCnbtvaCarass;
    }

    public DfaAnzCnbtvaCarazi getDfaAnzCnbtvaCarazi() {
        return dfaAnzCnbtvaCarazi;
    }

    public DfaAnzSrvz getDfaAnzSrvz() {
        return dfaAnzSrvz;
    }

    public DfaCnbtEcc4x100K1 getDfaCnbtEcc4x100K1() {
        return dfaCnbtEcc4x100K1;
    }

    public DfaCommisDiTrasfe getDfaCommisDiTrasfe() {
        return dfaCommisDiTrasfe;
    }

    public DfaCreditoIs getDfaCreditoIs() {
        return dfaCreditoIs;
    }

    public DfaDt1aCnbz getDfaDt1aCnbz() {
        return dfaDt1aCnbz;
    }

    public DfaDtAccnsRappFnd getDfaDtAccnsRappFnd() {
        return dfaDtAccnsRappFnd;
    }

    public DfaDtCessazione getDfaDtCessazione() {
        return dfaDtCessazione;
    }

    public DfaIdMoviChiu getDfaIdMoviChiu() {
        return dfaIdMoviChiu;
    }

    public DfaImpCnbtAzK1 getDfaImpCnbtAzK1() {
        return dfaImpCnbtAzK1;
    }

    public DfaImpCnbtAzK2 getDfaImpCnbtAzK2() {
        return dfaImpCnbtAzK2;
    }

    public DfaImpCnbtAzK3 getDfaImpCnbtAzK3() {
        return dfaImpCnbtAzK3;
    }

    public DfaImpCnbtIscK1 getDfaImpCnbtIscK1() {
        return dfaImpCnbtIscK1;
    }

    public DfaImpCnbtIscK2 getDfaImpCnbtIscK2() {
        return dfaImpCnbtIscK2;
    }

    public DfaImpCnbtIscK3 getDfaImpCnbtIscK3() {
        return dfaImpCnbtIscK3;
    }

    public DfaImpCnbtNdedK1 getDfaImpCnbtNdedK1() {
        return dfaImpCnbtNdedK1;
    }

    public DfaImpCnbtNdedK2 getDfaImpCnbtNdedK2() {
        return dfaImpCnbtNdedK2;
    }

    public DfaImpCnbtNdedK3 getDfaImpCnbtNdedK3() {
        return dfaImpCnbtNdedK3;
    }

    public DfaImpCnbtTfrK1 getDfaImpCnbtTfrK1() {
        return dfaImpCnbtTfrK1;
    }

    public DfaImpCnbtTfrK2 getDfaImpCnbtTfrK2() {
        return dfaImpCnbtTfrK2;
    }

    public DfaImpCnbtTfrK3 getDfaImpCnbtTfrK3() {
        return dfaImpCnbtTfrK3;
    }

    public DfaImpCnbtVolK1 getDfaImpCnbtVolK1() {
        return dfaImpCnbtVolK1;
    }

    public DfaImpCnbtVolK2 getDfaImpCnbtVolK2() {
        return dfaImpCnbtVolK2;
    }

    public DfaImpCnbtVolK3 getDfaImpCnbtVolK3() {
        return dfaImpCnbtVolK3;
    }

    public DfaImpEseImpstTfr getDfaImpEseImpstTfr() {
        return dfaImpEseImpstTfr;
    }

    public DfaImpNetTrasferito getDfaImpNetTrasferito() {
        return dfaImpNetTrasferito;
    }

    public DfaImpb252Antic getDfaImpb252Antic() {
        return dfaImpb252Antic;
    }

    public DfaImpb252K3 getDfaImpb252K3() {
        return dfaImpb252K3;
    }

    public DfaImpbIrpefK1Anti getDfaImpbIrpefK1Anti() {
        return dfaImpbIrpefK1Anti;
    }

    public DfaImpbIrpefK2Anti getDfaImpbIrpefK2Anti() {
        return dfaImpbIrpefK2Anti;
    }

    public DfaImpbIsK2 getDfaImpbIsK2() {
        return dfaImpbIsK2;
    }

    public DfaImpbIsK2Antic getDfaImpbIsK2Antic() {
        return dfaImpbIsK2Antic;
    }

    public DfaImpbIsK3 getDfaImpbIsK3() {
        return dfaImpbIsK3;
    }

    public DfaImpbIsK3Antic getDfaImpbIsK3Antic() {
        return dfaImpbIsK3Antic;
    }

    public DfaImpbTfrAntic getDfaImpbTfrAntic() {
        return dfaImpbTfrAntic;
    }

    public DfaImpbVis getDfaImpbVis() {
        return dfaImpbVis;
    }

    public DfaImpbVisAntic getDfaImpbVisAntic() {
        return dfaImpbVisAntic;
    }

    public DfaImpst252Antic getDfaImpst252Antic() {
        return dfaImpst252Antic;
    }

    public DfaImpst252K3 getDfaImpst252K3() {
        return dfaImpst252K3;
    }

    public DfaImpstIrpefK1Ant getDfaImpstIrpefK1Ant() {
        return dfaImpstIrpefK1Ant;
    }

    public DfaImpstIrpefK2Ant getDfaImpstIrpefK2Ant() {
        return dfaImpstIrpefK2Ant;
    }

    public DfaImpstSostK2 getDfaImpstSostK2() {
        return dfaImpstSostK2;
    }

    public DfaImpstSostK2Anti getDfaImpstSostK2Anti() {
        return dfaImpstSostK2Anti;
    }

    public DfaImpstSostK3 getDfaImpstSostK3() {
        return dfaImpstSostK3;
    }

    public DfaImpstSostK3Anti getDfaImpstSostK3Anti() {
        return dfaImpstSostK3Anti;
    }

    public DfaImpstTfrAntic getDfaImpstTfrAntic() {
        return dfaImpstTfrAntic;
    }

    public DfaImpstVis getDfaImpstVis() {
        return dfaImpstVis;
    }

    public DfaImpstVisAntic getDfaImpstVisAntic() {
        return dfaImpstVisAntic;
    }

    public DfaMatuK1 getDfaMatuK1() {
        return dfaMatuK1;
    }

    public DfaMatuK2 getDfaMatuK2() {
        return dfaMatuK2;
    }

    public DfaMatuK3 getDfaMatuK3() {
        return dfaMatuK3;
    }

    public DfaMatuResK1 getDfaMatuResK1() {
        return dfaMatuResK1;
    }

    public DfaMmCnbzK1 getDfaMmCnbzK1() {
        return dfaMmCnbzK1;
    }

    public DfaMmCnbzK2 getDfaMmCnbzK2() {
        return dfaMmCnbzK2;
    }

    public DfaMmCnbzK3 getDfaMmCnbzK3() {
        return dfaMmCnbzK3;
    }

    public DfaOnerTrasfe getDfaOnerTrasfe() {
        return dfaOnerTrasfe;
    }

    public DfaPcEseImpstTfr getDfaPcEseImpstTfr() {
        return dfaPcEseImpstTfr;
    }

    public DfaPcTfr getDfaPcTfr() {
        return dfaPcTfr;
    }

    public DfaRedtTassAbbatK2 getDfaRedtTassAbbatK2() {
        return dfaRedtTassAbbatK2;
    }

    public DfaRedtTassAbbatK3 getDfaRedtTassAbbatK3() {
        return dfaRedtTassAbbatK3;
    }

    public DfaRidzTfr getDfaRidzTfr() {
        return dfaRidzTfr;
    }

    public DfaRidzTfrSuAntic getDfaRidzTfrSuAntic() {
        return dfaRidzTfrSuAntic;
    }

    public DfaTotAntic getDfaTotAntic() {
        return dfaTotAntic;
    }

    public DfaTotImpst getDfaTotImpst() {
        return dfaTotImpst;
    }

    public DfaUltCommisTrasfe getDfaUltCommisTrasfe() {
        return dfaUltCommisTrasfe;
    }

    @Override
    public byte[] serialize() {
        return getdFiscAdesBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_ID_D_FISC_ADES = 5;
        public static final int DFA_ID_ADES = 5;
        public static final int DFA_ID_MOVI_CRZ = 5;
        public static final int DFA_DT_INI_EFF = 5;
        public static final int DFA_DT_END_EFF = 5;
        public static final int DFA_COD_COMP_ANIA = 3;
        public static final int DFA_TP_ISC_FND = 2;
        public static final int DFA_COD_DVS = 20;
        public static final int DFA_FL_APPLZ_NEWFIS = 1;
        public static final int DFA_DS_RIGA = 6;
        public static final int DFA_DS_OPER_SQL = 1;
        public static final int DFA_DS_VER = 5;
        public static final int DFA_DS_TS_INI_CPTZ = 10;
        public static final int DFA_DS_TS_END_CPTZ = 10;
        public static final int DFA_DS_UTENTE = 20;
        public static final int DFA_DS_STATO_ELAB = 1;
        public static final int D_FISC_ADES = DFA_ID_D_FISC_ADES + DFA_ID_ADES + DFA_ID_MOVI_CRZ + DfaIdMoviChiu.Len.DFA_ID_MOVI_CHIU + DFA_DT_INI_EFF + DFA_DT_END_EFF + DFA_COD_COMP_ANIA + DFA_TP_ISC_FND + DfaDtAccnsRappFnd.Len.DFA_DT_ACCNS_RAPP_FND + DfaImpCnbtAzK1.Len.DFA_IMP_CNBT_AZ_K1 + DfaImpCnbtIscK1.Len.DFA_IMP_CNBT_ISC_K1 + DfaImpCnbtTfrK1.Len.DFA_IMP_CNBT_TFR_K1 + DfaImpCnbtVolK1.Len.DFA_IMP_CNBT_VOL_K1 + DfaImpCnbtAzK2.Len.DFA_IMP_CNBT_AZ_K2 + DfaImpCnbtIscK2.Len.DFA_IMP_CNBT_ISC_K2 + DfaImpCnbtTfrK2.Len.DFA_IMP_CNBT_TFR_K2 + DfaImpCnbtVolK2.Len.DFA_IMP_CNBT_VOL_K2 + DfaMatuK1.Len.DFA_MATU_K1 + DfaMatuResK1.Len.DFA_MATU_RES_K1 + DfaMatuK2.Len.DFA_MATU_K2 + DfaImpbVis.Len.DFA_IMPB_VIS + DfaImpstVis.Len.DFA_IMPST_VIS + DfaImpbIsK2.Len.DFA_IMPB_IS_K2 + DfaImpstSostK2.Len.DFA_IMPST_SOST_K2 + DfaRidzTfr.Len.DFA_RIDZ_TFR + DfaPcTfr.Len.DFA_PC_TFR + DfaAlqTfr.Len.DFA_ALQ_TFR + DfaTotAntic.Len.DFA_TOT_ANTIC + DfaImpbTfrAntic.Len.DFA_IMPB_TFR_ANTIC + DfaImpstTfrAntic.Len.DFA_IMPST_TFR_ANTIC + DfaRidzTfrSuAntic.Len.DFA_RIDZ_TFR_SU_ANTIC + DfaImpbVisAntic.Len.DFA_IMPB_VIS_ANTIC + DfaImpstVisAntic.Len.DFA_IMPST_VIS_ANTIC + DfaImpbIsK2Antic.Len.DFA_IMPB_IS_K2_ANTIC + DfaImpstSostK2Anti.Len.DFA_IMPST_SOST_K2_ANTI + DfaUltCommisTrasfe.Len.DFA_ULT_COMMIS_TRASFE + DFA_COD_DVS + DfaAlqPrvr.Len.DFA_ALQ_PRVR + DfaPcEseImpstTfr.Len.DFA_PC_ESE_IMPST_TFR + DfaImpEseImpstTfr.Len.DFA_IMP_ESE_IMPST_TFR + DfaAnzCnbtvaCarass.Len.DFA_ANZ_CNBTVA_CARASS + DfaAnzCnbtvaCarazi.Len.DFA_ANZ_CNBTVA_CARAZI + DfaAnzSrvz.Len.DFA_ANZ_SRVZ + DfaImpCnbtNdedK1.Len.DFA_IMP_CNBT_NDED_K1 + DfaImpCnbtNdedK2.Len.DFA_IMP_CNBT_NDED_K2 + DfaImpCnbtNdedK3.Len.DFA_IMP_CNBT_NDED_K3 + DfaImpCnbtAzK3.Len.DFA_IMP_CNBT_AZ_K3 + DfaImpCnbtIscK3.Len.DFA_IMP_CNBT_ISC_K3 + DfaImpCnbtTfrK3.Len.DFA_IMP_CNBT_TFR_K3 + DfaImpCnbtVolK3.Len.DFA_IMP_CNBT_VOL_K3 + DfaMatuK3.Len.DFA_MATU_K3 + DfaImpb252Antic.Len.DFA_IMPB252_ANTIC + DfaImpst252Antic.Len.DFA_IMPST252_ANTIC + DfaDt1aCnbz.Len.DFA_DT1A_CNBZ + DfaCommisDiTrasfe.Len.DFA_COMMIS_DI_TRASFE + DfaAaCnbzK1.Len.DFA_AA_CNBZ_K1 + DfaAaCnbzK2.Len.DFA_AA_CNBZ_K2 + DfaAaCnbzK3.Len.DFA_AA_CNBZ_K3 + DfaMmCnbzK1.Len.DFA_MM_CNBZ_K1 + DfaMmCnbzK2.Len.DFA_MM_CNBZ_K2 + DfaMmCnbzK3.Len.DFA_MM_CNBZ_K3 + DFA_FL_APPLZ_NEWFIS + DfaRedtTassAbbatK3.Len.DFA_REDT_TASS_ABBAT_K3 + DfaCnbtEcc4x100K1.Len.DFA_CNBT_ECC4X100_K1 + DFA_DS_RIGA + DFA_DS_OPER_SQL + DFA_DS_VER + DFA_DS_TS_INI_CPTZ + DFA_DS_TS_END_CPTZ + DFA_DS_UTENTE + DFA_DS_STATO_ELAB + DfaCreditoIs.Len.DFA_CREDITO_IS + DfaRedtTassAbbatK2.Len.DFA_REDT_TASS_ABBAT_K2 + DfaImpbIsK3.Len.DFA_IMPB_IS_K3 + DfaImpstSostK3.Len.DFA_IMPST_SOST_K3 + DfaImpb252K3.Len.DFA_IMPB252_K3 + DfaImpst252K3.Len.DFA_IMPST252_K3 + DfaImpbIsK3Antic.Len.DFA_IMPB_IS_K3_ANTIC + DfaImpstSostK3Anti.Len.DFA_IMPST_SOST_K3_ANTI + DfaImpbIrpefK1Anti.Len.DFA_IMPB_IRPEF_K1_ANTI + DfaImpstIrpefK1Ant.Len.DFA_IMPST_IRPEF_K1_ANT + DfaImpbIrpefK2Anti.Len.DFA_IMPB_IRPEF_K2_ANTI + DfaImpstIrpefK2Ant.Len.DFA_IMPST_IRPEF_K2_ANT + DfaDtCessazione.Len.DFA_DT_CESSAZIONE + DfaTotImpst.Len.DFA_TOT_IMPST + DfaOnerTrasfe.Len.DFA_ONER_TRASFE + DfaImpNetTrasferito.Len.DFA_IMP_NET_TRASFERITO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_ID_D_FISC_ADES = 9;
            public static final int DFA_ID_ADES = 9;
            public static final int DFA_ID_MOVI_CRZ = 9;
            public static final int DFA_DT_INI_EFF = 8;
            public static final int DFA_DT_END_EFF = 8;
            public static final int DFA_COD_COMP_ANIA = 5;
            public static final int DFA_DS_RIGA = 10;
            public static final int DFA_DS_VER = 9;
            public static final int DFA_DS_TS_INI_CPTZ = 18;
            public static final int DFA_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
