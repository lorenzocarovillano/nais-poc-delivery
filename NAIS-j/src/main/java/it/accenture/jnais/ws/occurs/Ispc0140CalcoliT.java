package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ISPC0140-CALCOLI-T<br>
 * Variables: ISPC0140-CALCOLI-T from copybook ISPC0140<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0140CalcoliT {

    //==== PROPERTIES ====
    public static final int AREA_TP_PREMIO_T_MAXOCCURS = 3;
    //Original name: ISPC0140-O-TIPO-LIVELLO-T
    private char oTipoLivelloT = DefaultValues.CHAR_VAL;
    //Original name: ISPC0140-O-CODICE-LIVELLO-T
    private String oCodiceLivelloT = DefaultValues.stringVal(Len.O_CODICE_LIVELLO_T);
    //Original name: ISPC0140-PRE-NUM-MAX-ELE-T
    private String preNumMaxEleT = DefaultValues.stringVal(Len.PRE_NUM_MAX_ELE_T);
    //Original name: ISPC0140-AREA-TP-PREMIO-T
    private Ispc0140AreaTpPremioT[] areaTpPremioT = new Ispc0140AreaTpPremioT[AREA_TP_PREMIO_T_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ispc0140CalcoliT() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int areaTpPremioTIdx = 1; areaTpPremioTIdx <= AREA_TP_PREMIO_T_MAXOCCURS; areaTpPremioTIdx++) {
            areaTpPremioT[areaTpPremioTIdx - 1] = new Ispc0140AreaTpPremioT();
        }
    }

    public void setCalcoliTBytes(byte[] buffer, int offset) {
        int position = offset;
        oTipoLivelloT = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        oCodiceLivelloT = MarshalByte.readString(buffer, position, Len.O_CODICE_LIVELLO_T);
        position += Len.O_CODICE_LIVELLO_T;
        preNumMaxEleT = MarshalByte.readFixedString(buffer, position, Len.PRE_NUM_MAX_ELE_T);
        position += Len.PRE_NUM_MAX_ELE_T;
        for (int idx = 1; idx <= AREA_TP_PREMIO_T_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                areaTpPremioT[idx - 1].setAreaTpPremioTBytes(buffer, position);
                position += Ispc0140AreaTpPremioT.Len.AREA_TP_PREMIO_T;
            }
            else {
                areaTpPremioT[idx - 1].initAreaTpPremioTSpaces();
                position += Ispc0140AreaTpPremioT.Len.AREA_TP_PREMIO_T;
            }
        }
    }

    public byte[] getCalcoliTBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, oTipoLivelloT);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, oCodiceLivelloT, Len.O_CODICE_LIVELLO_T);
        position += Len.O_CODICE_LIVELLO_T;
        MarshalByte.writeString(buffer, position, preNumMaxEleT, Len.PRE_NUM_MAX_ELE_T);
        position += Len.PRE_NUM_MAX_ELE_T;
        for (int idx = 1; idx <= AREA_TP_PREMIO_T_MAXOCCURS; idx++) {
            areaTpPremioT[idx - 1].getAreaTpPremioTBytes(buffer, position);
            position += Ispc0140AreaTpPremioT.Len.AREA_TP_PREMIO_T;
        }
        return buffer;
    }

    public void initCalcoliTSpaces() {
        oTipoLivelloT = Types.SPACE_CHAR;
        oCodiceLivelloT = "";
        preNumMaxEleT = "";
        for (int idx = 1; idx <= AREA_TP_PREMIO_T_MAXOCCURS; idx++) {
            areaTpPremioT[idx - 1].initAreaTpPremioTSpaces();
        }
    }

    public void setoTipoLivelloT(char oTipoLivelloT) {
        this.oTipoLivelloT = oTipoLivelloT;
    }

    public char getoTipoLivelloT() {
        return this.oTipoLivelloT;
    }

    public void setoCodiceLivelloT(String oCodiceLivelloT) {
        this.oCodiceLivelloT = Functions.subString(oCodiceLivelloT, Len.O_CODICE_LIVELLO_T);
    }

    public String getoCodiceLivelloT() {
        return this.oCodiceLivelloT;
    }

    public short getIspc0140PreNumMaxEleT() {
        return NumericDisplay.asShort(this.preNumMaxEleT);
    }

    public Ispc0140AreaTpPremioT getAreaTpPremioT(int idx) {
        return areaTpPremioT[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int O_TIPO_LIVELLO_T = 1;
        public static final int O_CODICE_LIVELLO_T = 12;
        public static final int PRE_NUM_MAX_ELE_T = 3;
        public static final int CALCOLI_T = O_TIPO_LIVELLO_T + O_CODICE_LIVELLO_T + PRE_NUM_MAX_ELE_T + Ispc0140CalcoliT.AREA_TP_PREMIO_T_MAXOCCURS * Ispc0140AreaTpPremioT.Len.AREA_TP_PREMIO_T;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
