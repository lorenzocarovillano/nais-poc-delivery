package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-PROV-INC<br>
 * Variable: PVT-PROV-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_PROV_INC;
    }

    public void setPvtProvInc(AfDecimal pvtProvInc) {
        writeDecimalAsPacked(Pos.PVT_PROV_INC, pvtProvInc.copy());
    }

    public void setPvtProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_PROV_INC, Pos.PVT_PROV_INC);
    }

    /**Original name: PVT-PROV-INC<br>*/
    public AfDecimal getPvtProvInc() {
        return readPackedAsDecimal(Pos.PVT_PROV_INC, Len.Int.PVT_PROV_INC, Len.Fract.PVT_PROV_INC);
    }

    public byte[] getPvtProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_PROV_INC, Pos.PVT_PROV_INC);
        return buffer;
    }

    public void setPvtProvIncNull(String pvtProvIncNull) {
        writeString(Pos.PVT_PROV_INC_NULL, pvtProvIncNull, Len.PVT_PROV_INC_NULL);
    }

    /**Original name: PVT-PROV-INC-NULL<br>*/
    public String getPvtProvIncNull() {
        return readString(Pos.PVT_PROV_INC_NULL, Len.PVT_PROV_INC_NULL);
    }

    public String getPvtProvIncNullFormatted() {
        return Functions.padBlanks(getPvtProvIncNull(), Len.PVT_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_PROV_INC = 1;
        public static final int PVT_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_PROV_INC = 8;
        public static final int PVT_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PVT_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
