package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-NUM-PRE-PATT<br>
 * Variable: W-B03-NUM-PRE-PATT from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03NumPrePattLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03NumPrePattLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_NUM_PRE_PATT;
    }

    public void setwB03NumPrePatt(int wB03NumPrePatt) {
        writeIntAsPacked(Pos.W_B03_NUM_PRE_PATT, wB03NumPrePatt, Len.Int.W_B03_NUM_PRE_PATT);
    }

    /**Original name: W-B03-NUM-PRE-PATT<br>*/
    public int getwB03NumPrePatt() {
        return readPackedAsInt(Pos.W_B03_NUM_PRE_PATT, Len.Int.W_B03_NUM_PRE_PATT);
    }

    public byte[] getwB03NumPrePattAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_NUM_PRE_PATT, Pos.W_B03_NUM_PRE_PATT);
        return buffer;
    }

    public void setwB03NumPrePattNull(String wB03NumPrePattNull) {
        writeString(Pos.W_B03_NUM_PRE_PATT_NULL, wB03NumPrePattNull, Len.W_B03_NUM_PRE_PATT_NULL);
    }

    /**Original name: W-B03-NUM-PRE-PATT-NULL<br>*/
    public String getwB03NumPrePattNull() {
        return readString(Pos.W_B03_NUM_PRE_PATT_NULL, Len.W_B03_NUM_PRE_PATT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_NUM_PRE_PATT = 1;
        public static final int W_B03_NUM_PRE_PATT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_NUM_PRE_PATT = 3;
        public static final int W_B03_NUM_PRE_PATT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_NUM_PRE_PATT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
