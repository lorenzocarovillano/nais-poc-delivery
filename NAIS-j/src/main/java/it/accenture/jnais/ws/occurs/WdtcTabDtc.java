package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvdtc1;
import it.accenture.jnais.copy.WdtcDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WDTC-TAB-DTC<br>
 * Variables: WDTC-TAB-DTC from copybook LCCVDTCA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WdtcTabDtc {

    //==== PROPERTIES ====
    //Original name: LCCVDTC1
    private Lccvdtc1 lccvdtc1 = new Lccvdtc1();

    //==== METHODS ====
    public void setWdtcTabDtcBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdtc1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdtc1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdtc1.Len.Int.ID_PTF, 0));
        position += Lccvdtc1.Len.ID_PTF;
        lccvdtc1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdtcTabDtcBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdtc1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdtc1.getIdPtf(), Lccvdtc1.Len.Int.ID_PTF, 0);
        position += Lccvdtc1.Len.ID_PTF;
        lccvdtc1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWdtcTabDtcSpaces() {
        lccvdtc1.initLccvdtc1Spaces();
    }

    public Lccvdtc1 getLccvdtc1() {
        return lccvdtc1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_TAB_DTC = WpolStatus.Len.STATUS + Lccvdtc1.Len.ID_PTF + WdtcDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
