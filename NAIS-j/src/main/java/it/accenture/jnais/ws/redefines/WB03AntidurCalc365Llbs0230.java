package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-ANTIDUR-CALC-365<br>
 * Variable: W-B03-ANTIDUR-CALC-365 from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03AntidurCalc365Llbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03AntidurCalc365Llbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_ANTIDUR_CALC365;
    }

    public void setwB03AntidurCalc365(AfDecimal wB03AntidurCalc365) {
        writeDecimalAsPacked(Pos.W_B03_ANTIDUR_CALC365, wB03AntidurCalc365.copy());
    }

    /**Original name: W-B03-ANTIDUR-CALC-365<br>*/
    public AfDecimal getwB03AntidurCalc365() {
        return readPackedAsDecimal(Pos.W_B03_ANTIDUR_CALC365, Len.Int.W_B03_ANTIDUR_CALC365, Len.Fract.W_B03_ANTIDUR_CALC365);
    }

    public byte[] getwB03AntidurCalc365AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_ANTIDUR_CALC365, Pos.W_B03_ANTIDUR_CALC365);
        return buffer;
    }

    public void setwB03AntidurCalc365Null(String wB03AntidurCalc365Null) {
        writeString(Pos.W_B03_ANTIDUR_CALC365_NULL, wB03AntidurCalc365Null, Len.W_B03_ANTIDUR_CALC365_NULL);
    }

    /**Original name: W-B03-ANTIDUR-CALC-365-NULL<br>*/
    public String getwB03AntidurCalc365Null() {
        return readString(Pos.W_B03_ANTIDUR_CALC365_NULL, Len.W_B03_ANTIDUR_CALC365_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_ANTIDUR_CALC365 = 1;
        public static final int W_B03_ANTIDUR_CALC365_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_ANTIDUR_CALC365 = 6;
        public static final int W_B03_ANTIDUR_CALC365_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_ANTIDUR_CALC365 = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_ANTIDUR_CALC365 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
