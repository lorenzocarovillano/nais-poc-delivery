package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-INCAS<br>
 * Variable: WPAG-CONT-INCAS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContIncas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContIncas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_INCAS;
    }

    public void setWpagContIncas(AfDecimal wpagContIncas) {
        writeDecimalAsPacked(Pos.WPAG_CONT_INCAS, wpagContIncas.copy());
    }

    public void setWpagContIncasFormatted(String wpagContIncas) {
        setWpagContIncas(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_INCAS + Len.Fract.WPAG_CONT_INCAS, Len.Fract.WPAG_CONT_INCAS, wpagContIncas));
    }

    public void setWpagContIncasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_INCAS, Pos.WPAG_CONT_INCAS);
    }

    /**Original name: WPAG-CONT-INCAS<br>*/
    public AfDecimal getWpagContIncas() {
        return readPackedAsDecimal(Pos.WPAG_CONT_INCAS, Len.Int.WPAG_CONT_INCAS, Len.Fract.WPAG_CONT_INCAS);
    }

    public byte[] getWpagContIncasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_INCAS, Pos.WPAG_CONT_INCAS);
        return buffer;
    }

    public void initWpagContIncasSpaces() {
        fill(Pos.WPAG_CONT_INCAS, Len.WPAG_CONT_INCAS, Types.SPACE_CHAR);
    }

    public void setWpagContIncasNull(String wpagContIncasNull) {
        writeString(Pos.WPAG_CONT_INCAS_NULL, wpagContIncasNull, Len.WPAG_CONT_INCAS_NULL);
    }

    /**Original name: WPAG-CONT-INCAS-NULL<br>*/
    public String getWpagContIncasNull() {
        return readString(Pos.WPAG_CONT_INCAS_NULL, Len.WPAG_CONT_INCAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_INCAS = 1;
        public static final int WPAG_CONT_INCAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_INCAS = 8;
        public static final int WPAG_CONT_INCAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_INCAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_INCAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
