package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-LIQ<br>
 * Variable: WK-LIQ from program LVVS0640<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkLiq {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_LIQ);
    public static final String NO = "NO";
    public static final String SI = "SI";

    //==== METHODS ====
    public void setWkLiq(String wkLiq) {
        this.value = Functions.subString(wkLiq, Len.WK_LIQ);
    }

    public String getWkLiq() {
        return this.value;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_LIQ = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
