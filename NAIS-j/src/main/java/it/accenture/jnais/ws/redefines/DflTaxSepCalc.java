package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-TAX-SEP-CALC<br>
 * Variable: DFL-TAX-SEP-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflTaxSepCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflTaxSepCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_TAX_SEP_CALC;
    }

    public void setDflTaxSepCalc(AfDecimal dflTaxSepCalc) {
        writeDecimalAsPacked(Pos.DFL_TAX_SEP_CALC, dflTaxSepCalc.copy());
    }

    public void setDflTaxSepCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_TAX_SEP_CALC, Pos.DFL_TAX_SEP_CALC);
    }

    /**Original name: DFL-TAX-SEP-CALC<br>*/
    public AfDecimal getDflTaxSepCalc() {
        return readPackedAsDecimal(Pos.DFL_TAX_SEP_CALC, Len.Int.DFL_TAX_SEP_CALC, Len.Fract.DFL_TAX_SEP_CALC);
    }

    public byte[] getDflTaxSepCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_TAX_SEP_CALC, Pos.DFL_TAX_SEP_CALC);
        return buffer;
    }

    public void setDflTaxSepCalcNull(String dflTaxSepCalcNull) {
        writeString(Pos.DFL_TAX_SEP_CALC_NULL, dflTaxSepCalcNull, Len.DFL_TAX_SEP_CALC_NULL);
    }

    /**Original name: DFL-TAX-SEP-CALC-NULL<br>*/
    public String getDflTaxSepCalcNull() {
        return readString(Pos.DFL_TAX_SEP_CALC_NULL, Len.DFL_TAX_SEP_CALC_NULL);
    }

    public String getDflTaxSepCalcNullFormatted() {
        return Functions.padBlanks(getDflTaxSepCalcNull(), Len.DFL_TAX_SEP_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_TAX_SEP_CALC = 1;
        public static final int DFL_TAX_SEP_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_TAX_SEP_CALC = 8;
        public static final int DFL_TAX_SEP_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_TAX_SEP_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_TAX_SEP_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
