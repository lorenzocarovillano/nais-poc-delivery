package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-252-CALC<br>
 * Variable: WDFL-IMPST-252-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpst252Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpst252Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST252_CALC;
    }

    public void setWdflImpst252Calc(AfDecimal wdflImpst252Calc) {
        writeDecimalAsPacked(Pos.WDFL_IMPST252_CALC, wdflImpst252Calc.copy());
    }

    public void setWdflImpst252CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST252_CALC, Pos.WDFL_IMPST252_CALC);
    }

    /**Original name: WDFL-IMPST-252-CALC<br>*/
    public AfDecimal getWdflImpst252Calc() {
        return readPackedAsDecimal(Pos.WDFL_IMPST252_CALC, Len.Int.WDFL_IMPST252_CALC, Len.Fract.WDFL_IMPST252_CALC);
    }

    public byte[] getWdflImpst252CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST252_CALC, Pos.WDFL_IMPST252_CALC);
        return buffer;
    }

    public void setWdflImpst252CalcNull(String wdflImpst252CalcNull) {
        writeString(Pos.WDFL_IMPST252_CALC_NULL, wdflImpst252CalcNull, Len.WDFL_IMPST252_CALC_NULL);
    }

    /**Original name: WDFL-IMPST-252-CALC-NULL<br>*/
    public String getWdflImpst252CalcNull() {
        return readString(Pos.WDFL_IMPST252_CALC_NULL, Len.WDFL_IMPST252_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST252_CALC = 1;
        public static final int WDFL_IMPST252_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST252_CALC = 8;
        public static final int WDFL_IMPST252_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST252_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST252_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
