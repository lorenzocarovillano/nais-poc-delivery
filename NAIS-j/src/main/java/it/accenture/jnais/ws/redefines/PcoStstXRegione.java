package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-STST-X-REGIONE<br>
 * Variable: PCO-STST-X-REGIONE from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoStstXRegione extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoStstXRegione() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_STST_X_REGIONE;
    }

    public void setPcoStstXRegione(int pcoStstXRegione) {
        writeIntAsPacked(Pos.PCO_STST_X_REGIONE, pcoStstXRegione, Len.Int.PCO_STST_X_REGIONE);
    }

    public void setPcoStstXRegioneFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_STST_X_REGIONE, Pos.PCO_STST_X_REGIONE);
    }

    /**Original name: PCO-STST-X-REGIONE<br>*/
    public int getPcoStstXRegione() {
        return readPackedAsInt(Pos.PCO_STST_X_REGIONE, Len.Int.PCO_STST_X_REGIONE);
    }

    public byte[] getPcoStstXRegioneAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_STST_X_REGIONE, Pos.PCO_STST_X_REGIONE);
        return buffer;
    }

    public void initPcoStstXRegioneHighValues() {
        fill(Pos.PCO_STST_X_REGIONE, Len.PCO_STST_X_REGIONE, Types.HIGH_CHAR_VAL);
    }

    public void setPcoStstXRegioneNull(String pcoStstXRegioneNull) {
        writeString(Pos.PCO_STST_X_REGIONE_NULL, pcoStstXRegioneNull, Len.PCO_STST_X_REGIONE_NULL);
    }

    /**Original name: PCO-STST-X-REGIONE-NULL<br>*/
    public String getPcoStstXRegioneNull() {
        return readString(Pos.PCO_STST_X_REGIONE_NULL, Len.PCO_STST_X_REGIONE_NULL);
    }

    public String getPcoStstXRegioneNullFormatted() {
        return Functions.padBlanks(getPcoStstXRegioneNull(), Len.PCO_STST_X_REGIONE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_STST_X_REGIONE = 1;
        public static final int PCO_STST_X_REGIONE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_STST_X_REGIONE = 5;
        public static final int PCO_STST_X_REGIONE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_STST_X_REGIONE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
