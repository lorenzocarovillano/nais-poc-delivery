package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpagPercFondo;

/**Original name: WPAG-TAB-ASSET<br>
 * Variables: WPAG-TAB-ASSET from copybook LVEC0268<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpagTabAsset {

    //==== PROPERTIES ====
    //Original name: WPAG-COD-FONDO
    private String wpagCodFondo = DefaultValues.stringVal(Len.WPAG_COD_FONDO);
    //Original name: WPAG-TP-FND
    private char wpagTpFnd = DefaultValues.CHAR_VAL;
    //Original name: WPAG-PERC-FONDO
    private WpagPercFondo wpagPercFondo = new WpagPercFondo();
    //Original name: WPAG-COD-TARI
    private String wpagCodTari = DefaultValues.stringVal(Len.WPAG_COD_TARI);

    //==== METHODS ====
    public void setWpagTabAssetBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagCodFondo = MarshalByte.readString(buffer, position, Len.WPAG_COD_FONDO);
        position += Len.WPAG_COD_FONDO;
        wpagTpFnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpagPercFondo.setWpagPercFondoFromBuffer(buffer, position);
        position += WpagPercFondo.Len.WPAG_PERC_FONDO;
        wpagCodTari = MarshalByte.readString(buffer, position, Len.WPAG_COD_TARI);
    }

    public byte[] getWpagTabAssetBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wpagCodFondo, Len.WPAG_COD_FONDO);
        position += Len.WPAG_COD_FONDO;
        MarshalByte.writeChar(buffer, position, wpagTpFnd);
        position += Types.CHAR_SIZE;
        wpagPercFondo.getWpagPercFondoAsBuffer(buffer, position);
        position += WpagPercFondo.Len.WPAG_PERC_FONDO;
        MarshalByte.writeString(buffer, position, wpagCodTari, Len.WPAG_COD_TARI);
        return buffer;
    }

    public void initWpagTabAssetSpaces() {
        wpagCodFondo = "";
        wpagTpFnd = Types.SPACE_CHAR;
        wpagPercFondo.initWpagPercFondoSpaces();
        wpagCodTari = "";
    }

    public void setWpagCodFondo(String wpagCodFondo) {
        this.wpagCodFondo = Functions.subString(wpagCodFondo, Len.WPAG_COD_FONDO);
    }

    public String getWpagCodFondo() {
        return this.wpagCodFondo;
    }

    public void setWpagTpFnd(char wpagTpFnd) {
        this.wpagTpFnd = wpagTpFnd;
    }

    public char getWpagTpFnd() {
        return this.wpagTpFnd;
    }

    public void setWpagCodTari(String wpagCodTari) {
        this.wpagCodTari = Functions.subString(wpagCodTari, Len.WPAG_COD_TARI);
    }

    public String getWpagCodTari() {
        return this.wpagCodTari;
    }

    public WpagPercFondo getWpagPercFondo() {
        return wpagPercFondo;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_COD_FONDO = 20;
        public static final int WPAG_TP_FND = 1;
        public static final int WPAG_COD_TARI = 12;
        public static final int WPAG_TAB_ASSET = WPAG_COD_FONDO + WPAG_TP_FND + WpagPercFondo.Len.WPAG_PERC_FONDO + WPAG_COD_TARI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
