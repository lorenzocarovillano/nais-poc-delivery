package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Ieav9903;
import it.accenture.jnais.ws.ptr.AreaProductServicesIsps0140;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program ISPS0140<br>
 * Generated as a class for rule WS.<br>*/
public class Isps0140Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI DI WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "ISPS0140";
    //Original name: MQ02-ADDRESS
    private Mq01Address mq02Address = new Mq01Address();
    //Original name: IX-TAB-ERR
    private short ixTabErr = ((short)0);
    /**Original name: AREA-PRODUCT-SERVICES<br>
	 * <pre>----------------------------------------------------------------*
	 *  CHIAMATA AL SERVIZIO INFRASTRUTTURALE PER COLLEGAMENTO AL
	 *  SERVIZI DI PRODOTTO
	 * ----------------------------------------------------------------*</pre>*/
    private AreaProductServicesIsps0140 areaProductServices = new AreaProductServicesIsps0140();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: IEAV9903
    private Ieav9903 ieav9903 = new Ieav9903();
    /**Original name: INTERF-MQSERIES<br>
	 * <pre>----------------------------------------------------------------*
	 *  MODULI CHIAMATI                                                *
	 * ----------------------------------------------------------------*</pre>*/
    private String interfMqseries = "IJCSMQ02";

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setIxTabErr(short ixTabErr) {
        this.ixTabErr = ixTabErr;
    }

    public short getIxTabErr() {
        return this.ixTabErr;
    }

    public String getInterfMqseries() {
        return this.interfMqseries;
    }

    public AreaProductServicesIsps0140 getAreaProductServices() {
        return areaProductServices;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Ieav9903 getIeav9903() {
        return ieav9903;
    }

    public Mq01Address getMq02Address() {
        return mq02Address;
    }
}
