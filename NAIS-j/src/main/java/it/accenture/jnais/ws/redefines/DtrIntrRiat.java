package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-INTR-RIAT<br>
 * Variable: DTR-INTR-RIAT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrIntrRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrIntrRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_INTR_RIAT;
    }

    public void setDtrIntrRiat(AfDecimal dtrIntrRiat) {
        writeDecimalAsPacked(Pos.DTR_INTR_RIAT, dtrIntrRiat.copy());
    }

    public void setDtrIntrRiatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_INTR_RIAT, Pos.DTR_INTR_RIAT);
    }

    /**Original name: DTR-INTR-RIAT<br>*/
    public AfDecimal getDtrIntrRiat() {
        return readPackedAsDecimal(Pos.DTR_INTR_RIAT, Len.Int.DTR_INTR_RIAT, Len.Fract.DTR_INTR_RIAT);
    }

    public byte[] getDtrIntrRiatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_INTR_RIAT, Pos.DTR_INTR_RIAT);
        return buffer;
    }

    public void setDtrIntrRiatNull(String dtrIntrRiatNull) {
        writeString(Pos.DTR_INTR_RIAT_NULL, dtrIntrRiatNull, Len.DTR_INTR_RIAT_NULL);
    }

    /**Original name: DTR-INTR-RIAT-NULL<br>*/
    public String getDtrIntrRiatNull() {
        return readString(Pos.DTR_INTR_RIAT_NULL, Len.DTR_INTR_RIAT_NULL);
    }

    public String getDtrIntrRiatNullFormatted() {
        return Functions.padBlanks(getDtrIntrRiatNull(), Len.DTR_INTR_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_INTR_RIAT = 1;
        public static final int DTR_INTR_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_INTR_RIAT = 8;
        public static final int DTR_INTR_RIAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_INTR_RIAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
