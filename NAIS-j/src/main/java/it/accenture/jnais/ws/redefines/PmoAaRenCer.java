package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-AA-REN-CER<br>
 * Variable: PMO-AA-REN-CER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoAaRenCer extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoAaRenCer() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_AA_REN_CER;
    }

    public void setPmoAaRenCer(int pmoAaRenCer) {
        writeIntAsPacked(Pos.PMO_AA_REN_CER, pmoAaRenCer, Len.Int.PMO_AA_REN_CER);
    }

    public void setPmoAaRenCerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_AA_REN_CER, Pos.PMO_AA_REN_CER);
    }

    /**Original name: PMO-AA-REN-CER<br>*/
    public int getPmoAaRenCer() {
        return readPackedAsInt(Pos.PMO_AA_REN_CER, Len.Int.PMO_AA_REN_CER);
    }

    public byte[] getPmoAaRenCerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_AA_REN_CER, Pos.PMO_AA_REN_CER);
        return buffer;
    }

    public void initPmoAaRenCerHighValues() {
        fill(Pos.PMO_AA_REN_CER, Len.PMO_AA_REN_CER, Types.HIGH_CHAR_VAL);
    }

    public void setPmoAaRenCerNull(String pmoAaRenCerNull) {
        writeString(Pos.PMO_AA_REN_CER_NULL, pmoAaRenCerNull, Len.PMO_AA_REN_CER_NULL);
    }

    /**Original name: PMO-AA-REN-CER-NULL<br>*/
    public String getPmoAaRenCerNull() {
        return readString(Pos.PMO_AA_REN_CER_NULL, Len.PMO_AA_REN_CER_NULL);
    }

    public String getPmoAaRenCerNullFormatted() {
        return Functions.padBlanks(getPmoAaRenCerNull(), Len.PMO_AA_REN_CER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_AA_REN_CER = 1;
        public static final int PMO_AA_REN_CER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_AA_REN_CER = 3;
        public static final int PMO_AA_REN_CER_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_AA_REN_CER = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
