package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-FAIVL<br>
 * Variable: RST-RIS-FAIVL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisFaivl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisFaivl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_FAIVL;
    }

    public void setRstRisFaivl(AfDecimal rstRisFaivl) {
        writeDecimalAsPacked(Pos.RST_RIS_FAIVL, rstRisFaivl.copy());
    }

    public void setRstRisFaivlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_FAIVL, Pos.RST_RIS_FAIVL);
    }

    /**Original name: RST-RIS-FAIVL<br>*/
    public AfDecimal getRstRisFaivl() {
        return readPackedAsDecimal(Pos.RST_RIS_FAIVL, Len.Int.RST_RIS_FAIVL, Len.Fract.RST_RIS_FAIVL);
    }

    public byte[] getRstRisFaivlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_FAIVL, Pos.RST_RIS_FAIVL);
        return buffer;
    }

    public void setRstRisFaivlNull(String rstRisFaivlNull) {
        writeString(Pos.RST_RIS_FAIVL_NULL, rstRisFaivlNull, Len.RST_RIS_FAIVL_NULL);
    }

    /**Original name: RST-RIS-FAIVL-NULL<br>*/
    public String getRstRisFaivlNull() {
        return readString(Pos.RST_RIS_FAIVL_NULL, Len.RST_RIS_FAIVL_NULL);
    }

    public String getRstRisFaivlNullFormatted() {
        return Functions.padBlanks(getRstRisFaivlNull(), Len.RST_RIS_FAIVL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_FAIVL = 1;
        public static final int RST_RIS_FAIVL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_FAIVL = 8;
        public static final int RST_RIS_FAIVL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_FAIVL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_FAIVL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
