package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.StbIdMoviChiu;

/**Original name: STAT-OGG-BUS<br>
 * Variable: STAT-OGG-BUS from copybook IDBVSTB1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class StatOggBusIdbsstb0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: STB-ID-STAT-OGG-BUS
    private int stbIdStatOggBus = DefaultValues.INT_VAL;
    //Original name: STB-ID-OGG
    private int stbIdOgg = DefaultValues.INT_VAL;
    //Original name: STB-TP-OGG
    private String stbTpOgg = DefaultValues.stringVal(Len.STB_TP_OGG);
    //Original name: STB-ID-MOVI-CRZ
    private int stbIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: STB-ID-MOVI-CHIU
    private StbIdMoviChiu stbIdMoviChiu = new StbIdMoviChiu();
    //Original name: STB-DT-INI-EFF
    private int stbDtIniEff = DefaultValues.INT_VAL;
    //Original name: STB-DT-END-EFF
    private int stbDtEndEff = DefaultValues.INT_VAL;
    //Original name: STB-COD-COMP-ANIA
    private int stbCodCompAnia = DefaultValues.INT_VAL;
    //Original name: STB-TP-STAT-BUS
    private String stbTpStatBus = DefaultValues.stringVal(Len.STB_TP_STAT_BUS);
    //Original name: STB-TP-CAUS
    private String stbTpCaus = DefaultValues.stringVal(Len.STB_TP_CAUS);
    //Original name: STB-DS-RIGA
    private long stbDsRiga = DefaultValues.LONG_VAL;
    //Original name: STB-DS-OPER-SQL
    private char stbDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: STB-DS-VER
    private int stbDsVer = DefaultValues.INT_VAL;
    //Original name: STB-DS-TS-INI-CPTZ
    private long stbDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: STB-DS-TS-END-CPTZ
    private long stbDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: STB-DS-UTENTE
    private String stbDsUtente = DefaultValues.stringVal(Len.STB_DS_UTENTE);
    //Original name: STB-DS-STATO-ELAB
    private char stbDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.STAT_OGG_BUS;
    }

    @Override
    public void deserialize(byte[] buf) {
        setStatOggBusBytes(buf);
    }

    public void setStatOggBusFormatted(String data) {
        byte[] buffer = new byte[Len.STAT_OGG_BUS];
        MarshalByte.writeString(buffer, 1, data, Len.STAT_OGG_BUS);
        setStatOggBusBytes(buffer, 1);
    }

    public String getStatOggBusFormatted() {
        return MarshalByteExt.bufferToStr(getStatOggBusBytes());
    }

    public void setStatOggBusBytes(byte[] buffer) {
        setStatOggBusBytes(buffer, 1);
    }

    public byte[] getStatOggBusBytes() {
        byte[] buffer = new byte[Len.STAT_OGG_BUS];
        return getStatOggBusBytes(buffer, 1);
    }

    public void setStatOggBusBytes(byte[] buffer, int offset) {
        int position = offset;
        stbIdStatOggBus = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STB_ID_STAT_OGG_BUS, 0);
        position += Len.STB_ID_STAT_OGG_BUS;
        stbIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STB_ID_OGG, 0);
        position += Len.STB_ID_OGG;
        stbTpOgg = MarshalByte.readString(buffer, position, Len.STB_TP_OGG);
        position += Len.STB_TP_OGG;
        stbIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STB_ID_MOVI_CRZ, 0);
        position += Len.STB_ID_MOVI_CRZ;
        stbIdMoviChiu.setStbIdMoviChiuFromBuffer(buffer, position);
        position += StbIdMoviChiu.Len.STB_ID_MOVI_CHIU;
        stbDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STB_DT_INI_EFF, 0);
        position += Len.STB_DT_INI_EFF;
        stbDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STB_DT_END_EFF, 0);
        position += Len.STB_DT_END_EFF;
        stbCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STB_COD_COMP_ANIA, 0);
        position += Len.STB_COD_COMP_ANIA;
        stbTpStatBus = MarshalByte.readString(buffer, position, Len.STB_TP_STAT_BUS);
        position += Len.STB_TP_STAT_BUS;
        stbTpCaus = MarshalByte.readString(buffer, position, Len.STB_TP_CAUS);
        position += Len.STB_TP_CAUS;
        stbDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.STB_DS_RIGA, 0);
        position += Len.STB_DS_RIGA;
        stbDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        stbDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STB_DS_VER, 0);
        position += Len.STB_DS_VER;
        stbDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.STB_DS_TS_INI_CPTZ, 0);
        position += Len.STB_DS_TS_INI_CPTZ;
        stbDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.STB_DS_TS_END_CPTZ, 0);
        position += Len.STB_DS_TS_END_CPTZ;
        stbDsUtente = MarshalByte.readString(buffer, position, Len.STB_DS_UTENTE);
        position += Len.STB_DS_UTENTE;
        stbDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getStatOggBusBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, stbIdStatOggBus, Len.Int.STB_ID_STAT_OGG_BUS, 0);
        position += Len.STB_ID_STAT_OGG_BUS;
        MarshalByte.writeIntAsPacked(buffer, position, stbIdOgg, Len.Int.STB_ID_OGG, 0);
        position += Len.STB_ID_OGG;
        MarshalByte.writeString(buffer, position, stbTpOgg, Len.STB_TP_OGG);
        position += Len.STB_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, stbIdMoviCrz, Len.Int.STB_ID_MOVI_CRZ, 0);
        position += Len.STB_ID_MOVI_CRZ;
        stbIdMoviChiu.getStbIdMoviChiuAsBuffer(buffer, position);
        position += StbIdMoviChiu.Len.STB_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, stbDtIniEff, Len.Int.STB_DT_INI_EFF, 0);
        position += Len.STB_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, stbDtEndEff, Len.Int.STB_DT_END_EFF, 0);
        position += Len.STB_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, stbCodCompAnia, Len.Int.STB_COD_COMP_ANIA, 0);
        position += Len.STB_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, stbTpStatBus, Len.STB_TP_STAT_BUS);
        position += Len.STB_TP_STAT_BUS;
        MarshalByte.writeString(buffer, position, stbTpCaus, Len.STB_TP_CAUS);
        position += Len.STB_TP_CAUS;
        MarshalByte.writeLongAsPacked(buffer, position, stbDsRiga, Len.Int.STB_DS_RIGA, 0);
        position += Len.STB_DS_RIGA;
        MarshalByte.writeChar(buffer, position, stbDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, stbDsVer, Len.Int.STB_DS_VER, 0);
        position += Len.STB_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, stbDsTsIniCptz, Len.Int.STB_DS_TS_INI_CPTZ, 0);
        position += Len.STB_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, stbDsTsEndCptz, Len.Int.STB_DS_TS_END_CPTZ, 0);
        position += Len.STB_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, stbDsUtente, Len.STB_DS_UTENTE);
        position += Len.STB_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, stbDsStatoElab);
        return buffer;
    }

    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        this.stbIdStatOggBus = stbIdStatOggBus;
    }

    public int getStbIdStatOggBus() {
        return this.stbIdStatOggBus;
    }

    public void setStbIdOgg(int stbIdOgg) {
        this.stbIdOgg = stbIdOgg;
    }

    public int getStbIdOgg() {
        return this.stbIdOgg;
    }

    public void setStbTpOgg(String stbTpOgg) {
        this.stbTpOgg = Functions.subString(stbTpOgg, Len.STB_TP_OGG);
    }

    public String getStbTpOgg() {
        return this.stbTpOgg;
    }

    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        this.stbIdMoviCrz = stbIdMoviCrz;
    }

    public int getStbIdMoviCrz() {
        return this.stbIdMoviCrz;
    }

    public void setStbDtIniEff(int stbDtIniEff) {
        this.stbDtIniEff = stbDtIniEff;
    }

    public int getStbDtIniEff() {
        return this.stbDtIniEff;
    }

    public void setStbDtEndEff(int stbDtEndEff) {
        this.stbDtEndEff = stbDtEndEff;
    }

    public int getStbDtEndEff() {
        return this.stbDtEndEff;
    }

    public void setStbCodCompAnia(int stbCodCompAnia) {
        this.stbCodCompAnia = stbCodCompAnia;
    }

    public int getStbCodCompAnia() {
        return this.stbCodCompAnia;
    }

    public void setStbTpStatBus(String stbTpStatBus) {
        this.stbTpStatBus = Functions.subString(stbTpStatBus, Len.STB_TP_STAT_BUS);
    }

    public String getStbTpStatBus() {
        return this.stbTpStatBus;
    }

    public void setStbTpCaus(String stbTpCaus) {
        this.stbTpCaus = Functions.subString(stbTpCaus, Len.STB_TP_CAUS);
    }

    public String getStbTpCaus() {
        return this.stbTpCaus;
    }

    public void setStbDsRiga(long stbDsRiga) {
        this.stbDsRiga = stbDsRiga;
    }

    public long getStbDsRiga() {
        return this.stbDsRiga;
    }

    public void setStbDsOperSql(char stbDsOperSql) {
        this.stbDsOperSql = stbDsOperSql;
    }

    public void setStbDsOperSqlFormatted(String stbDsOperSql) {
        setStbDsOperSql(Functions.charAt(stbDsOperSql, Types.CHAR_SIZE));
    }

    public char getStbDsOperSql() {
        return this.stbDsOperSql;
    }

    public void setStbDsVer(int stbDsVer) {
        this.stbDsVer = stbDsVer;
    }

    public int getStbDsVer() {
        return this.stbDsVer;
    }

    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        this.stbDsTsIniCptz = stbDsTsIniCptz;
    }

    public long getStbDsTsIniCptz() {
        return this.stbDsTsIniCptz;
    }

    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        this.stbDsTsEndCptz = stbDsTsEndCptz;
    }

    public long getStbDsTsEndCptz() {
        return this.stbDsTsEndCptz;
    }

    public void setStbDsUtente(String stbDsUtente) {
        this.stbDsUtente = Functions.subString(stbDsUtente, Len.STB_DS_UTENTE);
    }

    public String getStbDsUtente() {
        return this.stbDsUtente;
    }

    public void setStbDsStatoElab(char stbDsStatoElab) {
        this.stbDsStatoElab = stbDsStatoElab;
    }

    public void setStbDsStatoElabFormatted(String stbDsStatoElab) {
        setStbDsStatoElab(Functions.charAt(stbDsStatoElab, Types.CHAR_SIZE));
    }

    public char getStbDsStatoElab() {
        return this.stbDsStatoElab;
    }

    public StbIdMoviChiu getStbIdMoviChiu() {
        return stbIdMoviChiu;
    }

    @Override
    public byte[] serialize() {
        return getStatOggBusBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int STB_ID_STAT_OGG_BUS = 5;
        public static final int STB_ID_OGG = 5;
        public static final int STB_TP_OGG = 2;
        public static final int STB_ID_MOVI_CRZ = 5;
        public static final int STB_DT_INI_EFF = 5;
        public static final int STB_DT_END_EFF = 5;
        public static final int STB_COD_COMP_ANIA = 3;
        public static final int STB_TP_STAT_BUS = 2;
        public static final int STB_TP_CAUS = 2;
        public static final int STB_DS_RIGA = 6;
        public static final int STB_DS_OPER_SQL = 1;
        public static final int STB_DS_VER = 5;
        public static final int STB_DS_TS_INI_CPTZ = 10;
        public static final int STB_DS_TS_END_CPTZ = 10;
        public static final int STB_DS_UTENTE = 20;
        public static final int STB_DS_STATO_ELAB = 1;
        public static final int STAT_OGG_BUS = STB_ID_STAT_OGG_BUS + STB_ID_OGG + STB_TP_OGG + STB_ID_MOVI_CRZ + StbIdMoviChiu.Len.STB_ID_MOVI_CHIU + STB_DT_INI_EFF + STB_DT_END_EFF + STB_COD_COMP_ANIA + STB_TP_STAT_BUS + STB_TP_CAUS + STB_DS_RIGA + STB_DS_OPER_SQL + STB_DS_VER + STB_DS_TS_INI_CPTZ + STB_DS_TS_END_CPTZ + STB_DS_UTENTE + STB_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int STB_ID_STAT_OGG_BUS = 9;
            public static final int STB_ID_OGG = 9;
            public static final int STB_ID_MOVI_CRZ = 9;
            public static final int STB_DT_INI_EFF = 8;
            public static final int STB_DT_END_EFF = 8;
            public static final int STB_COD_COMP_ANIA = 5;
            public static final int STB_DS_RIGA = 10;
            public static final int STB_DS_VER = 9;
            public static final int STB_DS_TS_INI_CPTZ = 18;
            public static final int STB_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
