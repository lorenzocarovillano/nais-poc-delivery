package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-MANFEE-ANTIC<br>
 * Variable: WTDR-TOT-MANFEE-ANTIC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_MANFEE_ANTIC;
    }

    public void setWtdrTotManfeeAntic(AfDecimal wtdrTotManfeeAntic) {
        writeDecimalAsPacked(Pos.WTDR_TOT_MANFEE_ANTIC, wtdrTotManfeeAntic.copy());
    }

    /**Original name: WTDR-TOT-MANFEE-ANTIC<br>*/
    public AfDecimal getWtdrTotManfeeAntic() {
        return readPackedAsDecimal(Pos.WTDR_TOT_MANFEE_ANTIC, Len.Int.WTDR_TOT_MANFEE_ANTIC, Len.Fract.WTDR_TOT_MANFEE_ANTIC);
    }

    public void setWtdrTotManfeeAnticNull(String wtdrTotManfeeAnticNull) {
        writeString(Pos.WTDR_TOT_MANFEE_ANTIC_NULL, wtdrTotManfeeAnticNull, Len.WTDR_TOT_MANFEE_ANTIC_NULL);
    }

    /**Original name: WTDR-TOT-MANFEE-ANTIC-NULL<br>*/
    public String getWtdrTotManfeeAnticNull() {
        return readString(Pos.WTDR_TOT_MANFEE_ANTIC_NULL, Len.WTDR_TOT_MANFEE_ANTIC_NULL);
    }

    public String getWtdrTotManfeeAnticNullFormatted() {
        return Functions.padBlanks(getWtdrTotManfeeAnticNull(), Len.WTDR_TOT_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_MANFEE_ANTIC = 1;
        public static final int WTDR_TOT_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_MANFEE_ANTIC = 8;
        public static final int WTDR_TOT_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
