package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PRE-PP-ULT<br>
 * Variable: L3421-PRE-PP-ULT from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PrePpUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PrePpUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PRE_PP_ULT;
    }

    public void setL3421PrePpUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PRE_PP_ULT, Pos.L3421_PRE_PP_ULT);
    }

    /**Original name: L3421-PRE-PP-ULT<br>*/
    public AfDecimal getL3421PrePpUlt() {
        return readPackedAsDecimal(Pos.L3421_PRE_PP_ULT, Len.Int.L3421_PRE_PP_ULT, Len.Fract.L3421_PRE_PP_ULT);
    }

    public byte[] getL3421PrePpUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PRE_PP_ULT, Pos.L3421_PRE_PP_ULT);
        return buffer;
    }

    /**Original name: L3421-PRE-PP-ULT-NULL<br>*/
    public String getL3421PrePpUltNull() {
        return readString(Pos.L3421_PRE_PP_ULT_NULL, Len.L3421_PRE_PP_ULT_NULL);
    }

    public String getL3421PrePpUltNullFormatted() {
        return Functions.padBlanks(getL3421PrePpUltNull(), Len.L3421_PRE_PP_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PRE_PP_ULT = 1;
        public static final int L3421_PRE_PP_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PRE_PP_ULT = 8;
        public static final int L3421_PRE_PP_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PRE_PP_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PRE_PP_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
