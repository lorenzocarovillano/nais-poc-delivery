package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WISO-ID-MOVI-CHIU<br>
 * Variable: WISO-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_ID_MOVI_CHIU;
    }

    public void setWisoIdMoviChiu(int wisoIdMoviChiu) {
        writeIntAsPacked(Pos.WISO_ID_MOVI_CHIU, wisoIdMoviChiu, Len.Int.WISO_ID_MOVI_CHIU);
    }

    public void setWisoIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_ID_MOVI_CHIU, Pos.WISO_ID_MOVI_CHIU);
    }

    /**Original name: WISO-ID-MOVI-CHIU<br>*/
    public int getWisoIdMoviChiu() {
        return readPackedAsInt(Pos.WISO_ID_MOVI_CHIU, Len.Int.WISO_ID_MOVI_CHIU);
    }

    public byte[] getWisoIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_ID_MOVI_CHIU, Pos.WISO_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWisoIdMoviChiuSpaces() {
        fill(Pos.WISO_ID_MOVI_CHIU, Len.WISO_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWisoIdMoviChiuNull(String wisoIdMoviChiuNull) {
        writeString(Pos.WISO_ID_MOVI_CHIU_NULL, wisoIdMoviChiuNull, Len.WISO_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WISO-ID-MOVI-CHIU-NULL<br>*/
    public String getWisoIdMoviChiuNull() {
        return readString(Pos.WISO_ID_MOVI_CHIU_NULL, Len.WISO_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_ID_MOVI_CHIU = 1;
        public static final int WISO_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_ID_MOVI_CHIU = 5;
        public static final int WISO_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
