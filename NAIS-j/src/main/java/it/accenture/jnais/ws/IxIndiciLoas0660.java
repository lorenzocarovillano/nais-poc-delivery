package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LOAS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLoas0660 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-PMO
    private short tabPmo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-GRZ
    private short tabGrz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-TGA
    private short tabTga = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-POG
    private short tabPog = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-AREA-ISPC0211
    private short areaIspc0211 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-COMP-ISPC0211
    private short compIspc0211 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-AREA-SCHEDA
    private short areaScheda = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-VAR
    private short tabVar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ERR
    private short tabErr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-FRAZ
    private short fraz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ISPS0211
    private short tabIsps0211 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-VAR-P
    private short tabVarP = ((short)0);
    //Original name: IX-TAB-VAR-T
    private short tabVarT = ((short)0);
    //Original name: IX-AREA-SCHEDA-P
    private short areaSchedaP = ((short)0);
    //Original name: IX-AREA-SCHEDA-T
    private short areaSchedaT = ((short)0);

    //==== METHODS ====
    public void setTabPmo(short tabPmo) {
        this.tabPmo = tabPmo;
    }

    public short getTabPmo() {
        return this.tabPmo;
    }

    public void setTabGrz(short tabGrz) {
        this.tabGrz = tabGrz;
    }

    public short getTabGrz() {
        return this.tabGrz;
    }

    public void setTabTga(short tabTga) {
        this.tabTga = tabTga;
    }

    public short getTabTga() {
        return this.tabTga;
    }

    public void setTabPog(short tabPog) {
        this.tabPog = tabPog;
    }

    public short getTabPog() {
        return this.tabPog;
    }

    public void setAreaIspc0211(short areaIspc0211) {
        this.areaIspc0211 = areaIspc0211;
    }

    public short getAreaIspc0211() {
        return this.areaIspc0211;
    }

    public void setCompIspc0211(short compIspc0211) {
        this.compIspc0211 = compIspc0211;
    }

    public short getCompIspc0211() {
        return this.compIspc0211;
    }

    public void setAreaScheda(short areaScheda) {
        this.areaScheda = areaScheda;
    }

    public short getAreaScheda() {
        return this.areaScheda;
    }

    public void setTabVar(short tabVar) {
        this.tabVar = tabVar;
    }

    public short getTabVar() {
        return this.tabVar;
    }

    public void setTabErr(short tabErr) {
        this.tabErr = tabErr;
    }

    public short getTabErr() {
        return this.tabErr;
    }

    public void setFraz(short fraz) {
        this.fraz = fraz;
    }

    public short getFraz() {
        return this.fraz;
    }

    public void setTabIsps0211(short tabIsps0211) {
        this.tabIsps0211 = tabIsps0211;
    }

    public short getTabIsps0211() {
        return this.tabIsps0211;
    }

    public void setTabVarP(short tabVarP) {
        this.tabVarP = tabVarP;
    }

    public short getTabVarP() {
        return this.tabVarP;
    }

    public void setTabVarT(short tabVarT) {
        this.tabVarT = tabVarT;
    }

    public short getTabVarT() {
        return this.tabVarT;
    }

    public void setAreaSchedaP(short areaSchedaP) {
        this.areaSchedaP = areaSchedaP;
    }

    public short getAreaSchedaP() {
        return this.areaSchedaP;
    }

    public void setAreaSchedaT(short areaSchedaT) {
        this.areaSchedaT = areaSchedaT;
    }

    public short getAreaSchedaT() {
        return this.areaSchedaT;
    }
}
