package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-FINE-MOV<br>
 * Variable: WK-FINE-MOV from program LOAS0820<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFineMov {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkFineMov(char wkFineMov) {
        this.value = wkFineMov;
    }

    public char getWkFineMov() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
