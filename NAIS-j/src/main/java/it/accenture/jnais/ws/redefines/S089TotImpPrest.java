package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMP-PREST<br>
 * Variable: S089-TOT-IMP-PREST from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMP_PREST;
    }

    public void setWlquTotImpPrest(AfDecimal wlquTotImpPrest) {
        writeDecimalAsPacked(Pos.S089_TOT_IMP_PREST, wlquTotImpPrest.copy());
    }

    public void setWlquTotImpPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMP_PREST, Pos.S089_TOT_IMP_PREST);
    }

    /**Original name: WLQU-TOT-IMP-PREST<br>*/
    public AfDecimal getWlquTotImpPrest() {
        return readPackedAsDecimal(Pos.S089_TOT_IMP_PREST, Len.Int.WLQU_TOT_IMP_PREST, Len.Fract.WLQU_TOT_IMP_PREST);
    }

    public byte[] getWlquTotImpPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMP_PREST, Pos.S089_TOT_IMP_PREST);
        return buffer;
    }

    public void initWlquTotImpPrestSpaces() {
        fill(Pos.S089_TOT_IMP_PREST, Len.S089_TOT_IMP_PREST, Types.SPACE_CHAR);
    }

    public void setWlquTotImpPrestNull(String wlquTotImpPrestNull) {
        writeString(Pos.S089_TOT_IMP_PREST_NULL, wlquTotImpPrestNull, Len.WLQU_TOT_IMP_PREST_NULL);
    }

    /**Original name: WLQU-TOT-IMP-PREST-NULL<br>*/
    public String getWlquTotImpPrestNull() {
        return readString(Pos.S089_TOT_IMP_PREST_NULL, Len.WLQU_TOT_IMP_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_PREST = 1;
        public static final int S089_TOT_IMP_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_PREST = 8;
        public static final int WLQU_TOT_IMP_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
