package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-QTZ-SP-Z-COUP-DT-C<br>
 * Variable: B03-QTZ-SP-Z-COUP-DT-C from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03QtzSpZCoupDtC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03QtzSpZCoupDtC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_QTZ_SP_Z_COUP_DT_C;
    }

    public void setB03QtzSpZCoupDtC(AfDecimal b03QtzSpZCoupDtC) {
        writeDecimalAsPacked(Pos.B03_QTZ_SP_Z_COUP_DT_C, b03QtzSpZCoupDtC.copy());
    }

    public void setB03QtzSpZCoupDtCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_QTZ_SP_Z_COUP_DT_C, Pos.B03_QTZ_SP_Z_COUP_DT_C);
    }

    /**Original name: B03-QTZ-SP-Z-COUP-DT-C<br>*/
    public AfDecimal getB03QtzSpZCoupDtC() {
        return readPackedAsDecimal(Pos.B03_QTZ_SP_Z_COUP_DT_C, Len.Int.B03_QTZ_SP_Z_COUP_DT_C, Len.Fract.B03_QTZ_SP_Z_COUP_DT_C);
    }

    public byte[] getB03QtzSpZCoupDtCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_QTZ_SP_Z_COUP_DT_C, Pos.B03_QTZ_SP_Z_COUP_DT_C);
        return buffer;
    }

    public void setB03QtzSpZCoupDtCNull(String b03QtzSpZCoupDtCNull) {
        writeString(Pos.B03_QTZ_SP_Z_COUP_DT_C_NULL, b03QtzSpZCoupDtCNull, Len.B03_QTZ_SP_Z_COUP_DT_C_NULL);
    }

    /**Original name: B03-QTZ-SP-Z-COUP-DT-C-NULL<br>*/
    public String getB03QtzSpZCoupDtCNull() {
        return readString(Pos.B03_QTZ_SP_Z_COUP_DT_C_NULL, Len.B03_QTZ_SP_Z_COUP_DT_C_NULL);
    }

    public String getB03QtzSpZCoupDtCNullFormatted() {
        return Functions.padBlanks(getB03QtzSpZCoupDtCNull(), Len.B03_QTZ_SP_Z_COUP_DT_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_QTZ_SP_Z_COUP_DT_C = 1;
        public static final int B03_QTZ_SP_Z_COUP_DT_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_QTZ_SP_Z_COUP_DT_C = 7;
        public static final int B03_QTZ_SP_Z_COUP_DT_C_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_QTZ_SP_Z_COUP_DT_C = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_QTZ_SP_Z_COUP_DT_C = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
