package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-SNDNLQ<br>
 * Variable: WPCO-DT-ULT-BOLL-SNDNLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollSndnlq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollSndnlq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_SNDNLQ;
    }

    public void setWpcoDtUltBollSndnlq(int wpcoDtUltBollSndnlq) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_SNDNLQ, wpcoDtUltBollSndnlq, Len.Int.WPCO_DT_ULT_BOLL_SNDNLQ);
    }

    public void setDpcoDtUltBollSndnlqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_SNDNLQ, Pos.WPCO_DT_ULT_BOLL_SNDNLQ);
    }

    /**Original name: WPCO-DT-ULT-BOLL-SNDNLQ<br>*/
    public int getWpcoDtUltBollSndnlq() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_SNDNLQ, Len.Int.WPCO_DT_ULT_BOLL_SNDNLQ);
    }

    public byte[] getWpcoDtUltBollSndnlqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_SNDNLQ, Pos.WPCO_DT_ULT_BOLL_SNDNLQ);
        return buffer;
    }

    public void setWpcoDtUltBollSndnlqNull(String wpcoDtUltBollSndnlqNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_SNDNLQ_NULL, wpcoDtUltBollSndnlqNull, Len.WPCO_DT_ULT_BOLL_SNDNLQ_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-SNDNLQ-NULL<br>*/
    public String getWpcoDtUltBollSndnlqNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_SNDNLQ_NULL, Len.WPCO_DT_ULT_BOLL_SNDNLQ_NULL);
    }

    public String getWpcoDtUltBollSndnlqNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollSndnlqNull(), Len.WPCO_DT_ULT_BOLL_SNDNLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_SNDNLQ = 1;
        public static final int WPCO_DT_ULT_BOLL_SNDNLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_SNDNLQ = 5;
        public static final int WPCO_DT_ULT_BOLL_SNDNLQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_SNDNLQ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
