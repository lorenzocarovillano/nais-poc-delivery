package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PC-CAR-MOR<br>
 * Variable: B03-PC-CAR-MOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PcCarMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PcCarMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PC_CAR_MOR;
    }

    public void setB03PcCarMor(AfDecimal b03PcCarMor) {
        writeDecimalAsPacked(Pos.B03_PC_CAR_MOR, b03PcCarMor.copy());
    }

    public void setB03PcCarMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PC_CAR_MOR, Pos.B03_PC_CAR_MOR);
    }

    /**Original name: B03-PC-CAR-MOR<br>*/
    public AfDecimal getB03PcCarMor() {
        return readPackedAsDecimal(Pos.B03_PC_CAR_MOR, Len.Int.B03_PC_CAR_MOR, Len.Fract.B03_PC_CAR_MOR);
    }

    public byte[] getB03PcCarMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PC_CAR_MOR, Pos.B03_PC_CAR_MOR);
        return buffer;
    }

    public void setB03PcCarMorNull(String b03PcCarMorNull) {
        writeString(Pos.B03_PC_CAR_MOR_NULL, b03PcCarMorNull, Len.B03_PC_CAR_MOR_NULL);
    }

    /**Original name: B03-PC-CAR-MOR-NULL<br>*/
    public String getB03PcCarMorNull() {
        return readString(Pos.B03_PC_CAR_MOR_NULL, Len.B03_PC_CAR_MOR_NULL);
    }

    public String getB03PcCarMorNullFormatted() {
        return Functions.padBlanks(getB03PcCarMorNull(), Len.B03_PC_CAR_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PC_CAR_MOR = 1;
        public static final int B03_PC_CAR_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PC_CAR_MOR = 4;
        public static final int B03_PC_CAR_MOR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PC_CAR_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PC_CAR_MOR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
