package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPST-IRPEF-K2-ANT<br>
 * Variable: DFA-IMPST-IRPEF-K2-ANT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpstIrpefK2Ant extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpstIrpefK2Ant() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPST_IRPEF_K2_ANT;
    }

    public void setDfaImpstIrpefK2Ant(AfDecimal dfaImpstIrpefK2Ant) {
        writeDecimalAsPacked(Pos.DFA_IMPST_IRPEF_K2_ANT, dfaImpstIrpefK2Ant.copy());
    }

    public void setDfaImpstIrpefK2AntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPST_IRPEF_K2_ANT, Pos.DFA_IMPST_IRPEF_K2_ANT);
    }

    /**Original name: DFA-IMPST-IRPEF-K2-ANT<br>*/
    public AfDecimal getDfaImpstIrpefK2Ant() {
        return readPackedAsDecimal(Pos.DFA_IMPST_IRPEF_K2_ANT, Len.Int.DFA_IMPST_IRPEF_K2_ANT, Len.Fract.DFA_IMPST_IRPEF_K2_ANT);
    }

    public byte[] getDfaImpstIrpefK2AntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPST_IRPEF_K2_ANT, Pos.DFA_IMPST_IRPEF_K2_ANT);
        return buffer;
    }

    public void setDfaImpstIrpefK2AntNull(String dfaImpstIrpefK2AntNull) {
        writeString(Pos.DFA_IMPST_IRPEF_K2_ANT_NULL, dfaImpstIrpefK2AntNull, Len.DFA_IMPST_IRPEF_K2_ANT_NULL);
    }

    /**Original name: DFA-IMPST-IRPEF-K2-ANT-NULL<br>*/
    public String getDfaImpstIrpefK2AntNull() {
        return readString(Pos.DFA_IMPST_IRPEF_K2_ANT_NULL, Len.DFA_IMPST_IRPEF_K2_ANT_NULL);
    }

    public String getDfaImpstIrpefK2AntNullFormatted() {
        return Functions.padBlanks(getDfaImpstIrpefK2AntNull(), Len.DFA_IMPST_IRPEF_K2_ANT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_IRPEF_K2_ANT = 1;
        public static final int DFA_IMPST_IRPEF_K2_ANT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_IRPEF_K2_ANT = 8;
        public static final int DFA_IMPST_IRPEF_K2_ANT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_IRPEF_K2_ANT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_IRPEF_K2_ANT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
