package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-RINN-COLL<br>
 * Variable: PCO-DT-ULT-RINN-COLL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltRinnColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltRinnColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_RINN_COLL;
    }

    public void setPcoDtUltRinnColl(int pcoDtUltRinnColl) {
        writeIntAsPacked(Pos.PCO_DT_ULT_RINN_COLL, pcoDtUltRinnColl, Len.Int.PCO_DT_ULT_RINN_COLL);
    }

    public void setPcoDtUltRinnCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_RINN_COLL, Pos.PCO_DT_ULT_RINN_COLL);
    }

    /**Original name: PCO-DT-ULT-RINN-COLL<br>*/
    public int getPcoDtUltRinnColl() {
        return readPackedAsInt(Pos.PCO_DT_ULT_RINN_COLL, Len.Int.PCO_DT_ULT_RINN_COLL);
    }

    public byte[] getPcoDtUltRinnCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_RINN_COLL, Pos.PCO_DT_ULT_RINN_COLL);
        return buffer;
    }

    public void initPcoDtUltRinnCollHighValues() {
        fill(Pos.PCO_DT_ULT_RINN_COLL, Len.PCO_DT_ULT_RINN_COLL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltRinnCollNull(String pcoDtUltRinnCollNull) {
        writeString(Pos.PCO_DT_ULT_RINN_COLL_NULL, pcoDtUltRinnCollNull, Len.PCO_DT_ULT_RINN_COLL_NULL);
    }

    /**Original name: PCO-DT-ULT-RINN-COLL-NULL<br>*/
    public String getPcoDtUltRinnCollNull() {
        return readString(Pos.PCO_DT_ULT_RINN_COLL_NULL, Len.PCO_DT_ULT_RINN_COLL_NULL);
    }

    public String getPcoDtUltRinnCollNullFormatted() {
        return Functions.padBlanks(getPcoDtUltRinnCollNull(), Len.PCO_DT_ULT_RINN_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RINN_COLL = 1;
        public static final int PCO_DT_ULT_RINN_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RINN_COLL = 5;
        public static final int PCO_DT_ULT_RINN_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_RINN_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
