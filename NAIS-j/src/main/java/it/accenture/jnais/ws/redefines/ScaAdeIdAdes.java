package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: SCA-ADE-ID-ADES<br>
 * Variable: SCA-ADE-ID-ADES from program LLBM0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class ScaAdeIdAdes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public ScaAdeIdAdes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SCA_ADE_ID_ADES;
    }

    public void setScaAdeIdAdesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SCA_ADE_ID_ADES, Pos.SCA_ADE_ID_ADES);
    }

    /**Original name: SCA-ADE-ID-ADES-ALF<br>*/
    public String getScaAdeIdAdesAlf() {
        return readString(Pos.SCA_ADE_ID_ADES_ALF, Len.SCA_ADE_ID_ADES_ALF);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SCA_ADE_ID_ADES = 1;
        public static final int SCA_ADE_ID_ADES_ALF = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SCA_ADE_ID_ADES = 9;
        public static final int SCA_ADE_ID_ADES_ALF = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
