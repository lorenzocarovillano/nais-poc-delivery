package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-PRE-NET<br>
 * Variable: DTC-PRE-NET from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcPreNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcPreNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_PRE_NET;
    }

    public void setDtcPreNet(AfDecimal dtcPreNet) {
        writeDecimalAsPacked(Pos.DTC_PRE_NET, dtcPreNet.copy());
    }

    public void setDtcPreNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_PRE_NET, Pos.DTC_PRE_NET);
    }

    /**Original name: DTC-PRE-NET<br>*/
    public AfDecimal getDtcPreNet() {
        return readPackedAsDecimal(Pos.DTC_PRE_NET, Len.Int.DTC_PRE_NET, Len.Fract.DTC_PRE_NET);
    }

    public byte[] getDtcPreNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_PRE_NET, Pos.DTC_PRE_NET);
        return buffer;
    }

    public void setDtcPreNetNull(String dtcPreNetNull) {
        writeString(Pos.DTC_PRE_NET_NULL, dtcPreNetNull, Len.DTC_PRE_NET_NULL);
    }

    /**Original name: DTC-PRE-NET-NULL<br>*/
    public String getDtcPreNetNull() {
        return readString(Pos.DTC_PRE_NET_NULL, Len.DTC_PRE_NET_NULL);
    }

    public String getDtcPreNetNullFormatted() {
        return Functions.padBlanks(getDtcPreNetNull(), Len.DTC_PRE_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_PRE_NET = 1;
        public static final int DTC_PRE_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_PRE_NET = 8;
        public static final int DTC_PRE_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_PRE_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_PRE_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
