package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IABV0006-GEST-ROTTURA-IBO<br>
 * Variable: IABV0006-GEST-ROTTURA-IBO from copybook IABV0006<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0006GestRotturaIbo {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char ARCH_ROTTURA_IBO = 'A';
    public static final char APPL_ROTTURA_IBO = 'B';

    //==== METHODS ====
    public void setGestRotturaIbo(char gestRotturaIbo) {
        this.value = gestRotturaIbo;
    }

    public char getGestRotturaIbo() {
        return this.value;
    }

    public void setIabv0006GestApplRotturaIbo() {
        value = APPL_ROTTURA_IBO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GEST_ROTTURA_IBO = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
