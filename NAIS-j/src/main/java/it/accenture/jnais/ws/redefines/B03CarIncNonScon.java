package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CAR-INC-NON-SCON<br>
 * Variable: B03-CAR-INC-NON-SCON from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CarIncNonScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CarIncNonScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CAR_INC_NON_SCON;
    }

    public void setB03CarIncNonScon(AfDecimal b03CarIncNonScon) {
        writeDecimalAsPacked(Pos.B03_CAR_INC_NON_SCON, b03CarIncNonScon.copy());
    }

    public void setB03CarIncNonSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CAR_INC_NON_SCON, Pos.B03_CAR_INC_NON_SCON);
    }

    /**Original name: B03-CAR-INC-NON-SCON<br>*/
    public AfDecimal getB03CarIncNonScon() {
        return readPackedAsDecimal(Pos.B03_CAR_INC_NON_SCON, Len.Int.B03_CAR_INC_NON_SCON, Len.Fract.B03_CAR_INC_NON_SCON);
    }

    public byte[] getB03CarIncNonSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CAR_INC_NON_SCON, Pos.B03_CAR_INC_NON_SCON);
        return buffer;
    }

    public void setB03CarIncNonSconNull(String b03CarIncNonSconNull) {
        writeString(Pos.B03_CAR_INC_NON_SCON_NULL, b03CarIncNonSconNull, Len.B03_CAR_INC_NON_SCON_NULL);
    }

    /**Original name: B03-CAR-INC-NON-SCON-NULL<br>*/
    public String getB03CarIncNonSconNull() {
        return readString(Pos.B03_CAR_INC_NON_SCON_NULL, Len.B03_CAR_INC_NON_SCON_NULL);
    }

    public String getB03CarIncNonSconNullFormatted() {
        return Functions.padBlanks(getB03CarIncNonSconNull(), Len.B03_CAR_INC_NON_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CAR_INC_NON_SCON = 1;
        public static final int B03_CAR_INC_NON_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CAR_INC_NON_SCON = 8;
        public static final int B03_CAR_INC_NON_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CAR_INC_NON_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CAR_INC_NON_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
