package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-RIS-ACQ-T<br>
 * Variable: B03-RIS-ACQ-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03RisAcqT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03RisAcqT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_RIS_ACQ_T;
    }

    public void setB03RisAcqT(AfDecimal b03RisAcqT) {
        writeDecimalAsPacked(Pos.B03_RIS_ACQ_T, b03RisAcqT.copy());
    }

    public void setB03RisAcqTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_RIS_ACQ_T, Pos.B03_RIS_ACQ_T);
    }

    /**Original name: B03-RIS-ACQ-T<br>*/
    public AfDecimal getB03RisAcqT() {
        return readPackedAsDecimal(Pos.B03_RIS_ACQ_T, Len.Int.B03_RIS_ACQ_T, Len.Fract.B03_RIS_ACQ_T);
    }

    public byte[] getB03RisAcqTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_RIS_ACQ_T, Pos.B03_RIS_ACQ_T);
        return buffer;
    }

    public void setB03RisAcqTNull(String b03RisAcqTNull) {
        writeString(Pos.B03_RIS_ACQ_T_NULL, b03RisAcqTNull, Len.B03_RIS_ACQ_T_NULL);
    }

    /**Original name: B03-RIS-ACQ-T-NULL<br>*/
    public String getB03RisAcqTNull() {
        return readString(Pos.B03_RIS_ACQ_T_NULL, Len.B03_RIS_ACQ_T_NULL);
    }

    public String getB03RisAcqTNullFormatted() {
        return Functions.padBlanks(getB03RisAcqTNull(), Len.B03_RIS_ACQ_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_RIS_ACQ_T = 1;
        public static final int B03_RIS_ACQ_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_RIS_ACQ_T = 8;
        public static final int B03_RIS_ACQ_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_RIS_ACQ_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_RIS_ACQ_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
