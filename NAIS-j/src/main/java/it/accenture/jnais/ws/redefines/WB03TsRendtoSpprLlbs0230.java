package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-TS-RENDTO-SPPR<br>
 * Variable: W-B03-TS-RENDTO-SPPR from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03TsRendtoSpprLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03TsRendtoSpprLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_TS_RENDTO_SPPR;
    }

    public void setwB03TsRendtoSppr(AfDecimal wB03TsRendtoSppr) {
        writeDecimalAsPacked(Pos.W_B03_TS_RENDTO_SPPR, wB03TsRendtoSppr.copy());
    }

    /**Original name: W-B03-TS-RENDTO-SPPR<br>*/
    public AfDecimal getwB03TsRendtoSppr() {
        return readPackedAsDecimal(Pos.W_B03_TS_RENDTO_SPPR, Len.Int.W_B03_TS_RENDTO_SPPR, Len.Fract.W_B03_TS_RENDTO_SPPR);
    }

    public byte[] getwB03TsRendtoSpprAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_TS_RENDTO_SPPR, Pos.W_B03_TS_RENDTO_SPPR);
        return buffer;
    }

    public void setwB03TsRendtoSpprNull(String wB03TsRendtoSpprNull) {
        writeString(Pos.W_B03_TS_RENDTO_SPPR_NULL, wB03TsRendtoSpprNull, Len.W_B03_TS_RENDTO_SPPR_NULL);
    }

    /**Original name: W-B03-TS-RENDTO-SPPR-NULL<br>*/
    public String getwB03TsRendtoSpprNull() {
        return readString(Pos.W_B03_TS_RENDTO_SPPR_NULL, Len.W_B03_TS_RENDTO_SPPR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_TS_RENDTO_SPPR = 1;
        public static final int W_B03_TS_RENDTO_SPPR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_TS_RENDTO_SPPR = 8;
        public static final int W_B03_TS_RENDTO_SPPR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_TS_RENDTO_SPPR = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_TS_RENDTO_SPPR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
