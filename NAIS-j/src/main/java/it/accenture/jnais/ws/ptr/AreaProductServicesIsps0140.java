package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.notifier.ChangeNotifier;
import com.bphx.ctu.af.core.notifier.IValueChangeListener;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;

/**Original name: AREA-PRODUCT-SERVICES<br>
 * Variable: AREA-PRODUCT-SERVICES from copybook IJCCMQ02<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class AreaProductServicesIsps0140 extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int IJCCMQ02_CAR_MAXOCCURS = 1000000;
    private int ijccmq02CarListenerSize = IJCCMQ02_CAR_MAXOCCURS;
    private IValueChangeListener ijccmq02CarListener = new Ijccmq02CarListener();
    public static final int IJCCMQ02_ELE_ERRORI_MAXOCCURS = 10;
    public static final int IJCCMQ02_ADDRESSES_MAXOCCURS = 5;
    private Pos pos = new Pos(this);
    public static final char IJCCMQ02_ON_LINE = 'O';
    public static final char IJCCMQ02_BATCH = 'B';
    public static final char IJCCMQ02_BATCH_INFR = 'I';
    public static final char IJCCMQ02_SERVICE_INVOCATION = 'S';
    public static final char IJCCMQ02_DATA_REQUEST = 'D';
    public static final String IJCCMQ02_ESITO_OK = "OK";
    public static final String IJCCMQ02_ESITO_KO = "KO";
    private ChangeNotifier ijccmq02LengthDatiServizioNotifier = new ChangeNotifier();
    public static final String IJCCMQ00_NO_DEBUG = "0";
    public static final String IJCCMQ00_DEBUG_BASSO = "1";
    public static final String IJCCMQ00_DEBUG_MEDIO = "2";
    public static final String IJCCMQ00_DEBUG_ELEVATO = "3";
    public static final String IJCCMQ00_DEBUG_ESASPERATO = "4";
    private static final String[] IJCCMQ00_ANY_APPL_DBG = new String[] {"1", "2", "3", "4"};
    public static final String IJCCMQ00_ARCH_BATCH_DBG = "5";
    public static final String IJCCMQ00_COM_COB_JAV_DBG = "6";
    public static final String IJCCMQ00_STRESS_TEST_DBG = "7";
    public static final String IJCCMQ00_BUSINESS_DBG = "8";
    public static final String IJCCMQ00_TOT_TUNING_DBG = "9";
    private static final String[] IJCCMQ00_ANY_TUNING_DBG = new String[] {"5", "6", "7", "8", "9"};

    //==== CONSTRUCTORS ====
    public AreaProductServicesIsps0140() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return getAreaProductServicesSize();
    }

    public int getAreaProductServicesSize() {
        return Len.IJCCMQ02_HEADER + getIjccmq02AreaDatiServizioSize();
    }

    public void initAreaProductServicesLowValues() {
        getIjccmq02LengthDatiServizioNotifier().notifyMaxOccursListeners();
        fill(Pos.AREA_PRODUCT_SERVICES, getAreaProductServicesSize(), Types.LOW_CHAR_VAL);
    }

    public void setIjccmq02ModalitaEsecutiva(char ijccmq02ModalitaEsecutiva) {
        writeChar(Pos.IJCCMQ02_MODALITA_ESECUTIVA, ijccmq02ModalitaEsecutiva);
    }

    /**Original name: IJCCMQ02-MODALITA-ESECUTIVA<br>*/
    public char getIjccmq02ModalitaEsecutiva() {
        return readChar(Pos.IJCCMQ02_MODALITA_ESECUTIVA);
    }

    public void setIjccmq02JavaServiceName(String ijccmq02JavaServiceName) {
        writeString(Pos.IJCCMQ02_JAVA_SERVICE_NAME, ijccmq02JavaServiceName, Len.IJCCMQ02_JAVA_SERVICE_NAME);
    }

    /**Original name: IJCCMQ02-JAVA-SERVICE-NAME<br>*/
    public String getIjccmq02JavaServiceName() {
        return readString(Pos.IJCCMQ02_JAVA_SERVICE_NAME, Len.IJCCMQ02_JAVA_SERVICE_NAME);
    }

    public void setIjccmq02Esito(String ijccmq02Esito) {
        writeString(Pos.IJCCMQ02_ESITO, ijccmq02Esito, Len.IJCCMQ02_ESITO);
    }

    /**Original name: IJCCMQ02-ESITO<br>*/
    public String getIjccmq02Esito() {
        return readString(Pos.IJCCMQ02_ESITO, Len.IJCCMQ02_ESITO);
    }

    public boolean isIjccmq02EsitoOk() {
        return getIjccmq02Esito().equals(IJCCMQ02_ESITO_OK);
    }

    public void setIjccmq02EsitoOk() {
        setIjccmq02Esito(IJCCMQ02_ESITO_OK);
    }

    public void setIjccmq02LengthDatiServizio(int ijccmq02LengthDatiServizio) {
        writeBinaryInt(Pos.IJCCMQ02_LENGTH_DATI_SERVIZIO, ijccmq02LengthDatiServizio);
        this.ijccmq02LengthDatiServizioNotifier.setValue(ijccmq02LengthDatiServizio);
    }

    public int getIjccmq02LengthDatiServizio() {
        return readBinaryInt(Pos.IJCCMQ02_LENGTH_DATI_SERVIZIO);
    }

    /**Original name: IJCCMQ02-MAX-ELE-ERRORI<br>*/
    public short getIjccmq02MaxEleErrori() {
        return readBinaryShort(Pos.IJCCMQ02_MAX_ELE_ERRORI);
    }

    /**Original name: IJCCMQ02-DESC-ERRORE<br>*/
    public String getIjccmq02DescErrore(int ijccmq02DescErroreIdx) {
        int position = Pos.ijccmq02DescErrore(ijccmq02DescErroreIdx - 1);
        return readString(position, Len.IJCCMQ02_DESC_ERRORE);
    }

    public String getIjccmq02CodErroreFormatted(int ijccmq02CodErroreIdx) {
        int position = Pos.ijccmq02CodErrore(ijccmq02CodErroreIdx - 1);
        return readFixedString(position, Len.IJCCMQ02_COD_ERRORE);
    }

    public String getIjccmq02LivGravitaBeFormatted(int ijccmq02LivGravitaBeIdx) {
        int position = Pos.ijccmq02LivGravitaBe(ijccmq02LivGravitaBeIdx - 1);
        return readFixedString(position, Len.IJCCMQ02_LIV_GRAVITA_BE);
    }

    public void setIjccmq02AreaAddressesBytes(byte[] buffer) {
        setIjccmq02AreaAddressesBytes(buffer, 1);
    }

    public void setIjccmq02AreaAddressesBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.IJCCMQ02_AREA_ADDRESSES, Pos.IJCCMQ02_AREA_ADDRESSES);
    }

    public void setIjccmq02UserName(String ijccmq02UserName) {
        writeString(Pos.IJCCMQ02_USER_NAME, ijccmq02UserName, Len.IJCCMQ02_USER_NAME);
    }

    /**Original name: IJCCMQ02-USER-NAME<br>*/
    public String getIjccmq02UserName() {
        return readString(Pos.IJCCMQ02_USER_NAME, Len.IJCCMQ02_USER_NAME);
    }

    public void setIjccmq00LivelloDebugFormatted(String ijccmq00LivelloDebug) {
        writeString(Pos.IJCCMQ00_LIVELLO_DEBUG, Trunc.toUnsignedNumeric(ijccmq00LivelloDebug, Len.IJCCMQ00_LIVELLO_DEBUG), Len.IJCCMQ00_LIVELLO_DEBUG);
    }

    /**Original name: IJCCMQ00-LIVELLO-DEBUG<br>*/
    public short getIjccmq00LivelloDebug() {
        return readNumDispUnsignedShort(Pos.IJCCMQ00_LIVELLO_DEBUG, Len.IJCCMQ00_LIVELLO_DEBUG);
    }

    public int getIjccmq02AreaDatiServizioSize() {
        return Types.CHAR_SIZE * ijccmq02CarListenerSize;
    }

    public void setIjccmq02AreaDatiServizioBytes(byte[] buffer) {
        setIjccmq02AreaDatiServizioBytes(buffer, 1);
    }

    /**Original name: IJCCMQ02-AREA-DATI-SERVIZIO<br>*/
    public byte[] getIjccmq02AreaDatiServizioBytes() {
        byte[] buffer = new byte[getIjccmq02AreaDatiServizioSize()];
        return getIjccmq02AreaDatiServizioBytes(buffer, 1);
    }

    public void setIjccmq02AreaDatiServizioBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, getIjccmq02AreaDatiServizioSize(), Pos.IJCCMQ02_AREA_DATI_SERVIZIO);
    }

    public byte[] getIjccmq02AreaDatiServizioBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, getIjccmq02AreaDatiServizioSize(), Pos.IJCCMQ02_AREA_DATI_SERVIZIO);
        return buffer;
    }

    public IValueChangeListener getIjccmq02CarListener() {
        return ijccmq02CarListener;
    }

    public ChangeNotifier getIjccmq02LengthDatiServizioNotifier() {
        return ijccmq02LengthDatiServizioNotifier;
    }

    public void notifyListeners() {
        getIjccmq02LengthDatiServizioNotifier().setValue(getIjccmq02LengthDatiServizio());
    }

    //==== INNER CLASSES ====
    /**Original name: IJCCMQ02-CAR<br>*/
    public class Ijccmq02CarListener implements IValueChangeListener {

        //==== METHODS ====
        public void change() {
            ijccmq02CarListenerSize = IJCCMQ02_CAR_MAXOCCURS;
        }

        public void change(int value) {
            ijccmq02CarListenerSize = value < 1 ? 0 : (value > IJCCMQ02_CAR_MAXOCCURS ? IJCCMQ02_CAR_MAXOCCURS : value);
        }
    }

    public static class Pos {

        //==== PROPERTIES ====
        public static final int AREA_PRODUCT_SERVICES = 1;
        public static final int IJCCMQ02_HEADER = AREA_PRODUCT_SERVICES;
        public static final int IJCCMQ02_MODALITA_ESECUTIVA = IJCCMQ02_HEADER;
        public static final int IJCCMQ02_JAVA_SERVICE_NAME = IJCCMQ02_MODALITA_ESECUTIVA + Len.IJCCMQ02_MODALITA_ESECUTIVA;
        public static final int IJCCMQ02_INFO_FRAMES = IJCCMQ02_JAVA_SERVICE_NAME + Len.IJCCMQ02_JAVA_SERVICE_NAME;
        public static final int IJCCMQ02_ID_APPLICATION = IJCCMQ02_INFO_FRAMES;
        public static final int IJCCMQ02_TOT_NUM_FRAMES = IJCCMQ02_ID_APPLICATION + Len.IJCCMQ02_ID_APPLICATION;
        public static final int IJCCMQ02_NUM_FRAME = IJCCMQ02_TOT_NUM_FRAMES + Len.IJCCMQ02_TOT_NUM_FRAMES;
        public static final int IJCCMQ02_LENGTH_DATA_FRAME = IJCCMQ02_NUM_FRAME + Len.IJCCMQ02_NUM_FRAME;
        public static final int IJCCMQ02_CALL_METHOD = IJCCMQ02_LENGTH_DATA_FRAME + Len.IJCCMQ02_LENGTH_DATA_FRAME;
        public static final int IJCCMQ02_ESITO = IJCCMQ02_CALL_METHOD + Len.IJCCMQ02_CALL_METHOD;
        public static final int IJCCMQ02_LENGTH_DATI_SERVIZIO = IJCCMQ02_ESITO + Len.IJCCMQ02_ESITO;
        public static final int IJCCMQ02_MAX_ELE_ERRORI = IJCCMQ02_LENGTH_DATI_SERVIZIO + Len.IJCCMQ02_LENGTH_DATI_SERVIZIO;
        public static final int IJCCMQ02_TAB_ERRORI_FRONT_END = IJCCMQ02_MAX_ELE_ERRORI + Len.IJCCMQ02_MAX_ELE_ERRORI;
        public static final int IJCCMQ02_LENGTH_DATI_OUTPUT = ijccmq02TipoTrattFe(IJCCMQ02_ELE_ERRORI_MAXOCCURS - 1) + Len.IJCCMQ02_TIPO_TRATT_FE;
        public static final int IJCCMQ02_VAR_AMBIENTE = IJCCMQ02_LENGTH_DATI_OUTPUT + Len.IJCCMQ02_LENGTH_DATI_OUTPUT;
        public static final int IJCCMQ02_AREA_ADDRESSES = IJCCMQ02_VAR_AMBIENTE + Len.IJCCMQ02_VAR_AMBIENTE;
        public static final int IJCCMQ02_USER_NAME = ijccmq02Address(IJCCMQ02_ADDRESSES_MAXOCCURS - 1) + Len.IJCCMQ02_ADDRESS;
        public static final int FLR1 = IJCCMQ02_USER_NAME + Len.IJCCMQ02_USER_NAME;
        public static final int IJCCMQ00_LIVELLO_DEBUG = FLR1 + Len.FLR1;
        public static final int IJCCMQ02_AREA_DATI_SERVIZIO = IJCCMQ00_LIVELLO_DEBUG + Len.IJCCMQ00_LIVELLO_DEBUG;
        private AreaProductServicesIsps0140 outer;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        public Pos(AreaProductServicesIsps0140 outer) {
            this.outer = outer;
        }

        //==== METHODS ====
        public static int ijccmq02EleErrori(int idx) {
            return IJCCMQ02_TAB_ERRORI_FRONT_END + idx * Len.IJCCMQ02_ELE_ERRORI;
        }

        public static int ijccmq02DescErrore(int idx) {
            return ijccmq02EleErrori(idx);
        }

        public static int ijccmq02CodErrore(int idx) {
            return ijccmq02DescErrore(idx) + Len.IJCCMQ02_DESC_ERRORE;
        }

        public static int ijccmq02LivGravitaBe(int idx) {
            return ijccmq02CodErrore(idx) + Len.IJCCMQ02_COD_ERRORE;
        }

        public static int ijccmq02TipoTrattFe(int idx) {
            return ijccmq02LivGravitaBe(idx) + Len.IJCCMQ02_LIV_GRAVITA_BE;
        }

        public static int ijccmq02Addresses(int idx) {
            return IJCCMQ02_AREA_ADDRESSES + idx * Len.IJCCMQ02_ADDRESSES;
        }

        public static int ijccmq02AddressType(int idx) {
            return ijccmq02Addresses(idx);
        }

        public static int ijccmq02Address(int idx) {
            return ijccmq02AddressType(idx) + Len.IJCCMQ02_ADDRESS_TYPE;
        }

        public static int ijccmq02Car(int idx) {
            return IJCCMQ02_AREA_DATI_SERVIZIO + idx * Len.IJCCMQ02_CAR;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int IJCCMQ02_MODALITA_ESECUTIVA = 1;
        public static final int IJCCMQ02_JAVA_SERVICE_NAME = 100;
        public static final int IJCCMQ02_ID_APPLICATION = 26;
        public static final int IJCCMQ02_TOT_NUM_FRAMES = 2;
        public static final int IJCCMQ02_NUM_FRAME = 2;
        public static final int IJCCMQ02_LENGTH_DATA_FRAME = 4;
        public static final int IJCCMQ02_CALL_METHOD = 1;
        public static final int IJCCMQ02_ESITO = 2;
        public static final int IJCCMQ02_LENGTH_DATI_SERVIZIO = 4;
        public static final int IJCCMQ02_MAX_ELE_ERRORI = 2;
        public static final int IJCCMQ02_DESC_ERRORE = 200;
        public static final int IJCCMQ02_COD_ERRORE = 6;
        public static final int IJCCMQ02_LIV_GRAVITA_BE = 1;
        public static final int IJCCMQ02_TIPO_TRATT_FE = 1;
        public static final int IJCCMQ02_ELE_ERRORI = IJCCMQ02_DESC_ERRORE + IJCCMQ02_COD_ERRORE + IJCCMQ02_LIV_GRAVITA_BE + IJCCMQ02_TIPO_TRATT_FE;
        public static final int IJCCMQ02_LENGTH_DATI_OUTPUT = 4;
        public static final int IJCCMQ02_VAR_AMBIENTE = 224;
        public static final int IJCCMQ02_ADDRESS_TYPE = 1;
        public static final int IJCCMQ02_ADDRESS = 4;
        public static final int IJCCMQ02_ADDRESSES = IJCCMQ02_ADDRESS_TYPE + IJCCMQ02_ADDRESS;
        public static final int IJCCMQ02_USER_NAME = 20;
        public static final int FLR1 = 2;
        public static final int IJCCMQ00_LIVELLO_DEBUG = 1;
        public static final int IJCCMQ02_CAR = 1;
        public static final int IJCCMQ02_INFO_FRAMES = IJCCMQ02_ID_APPLICATION + IJCCMQ02_TOT_NUM_FRAMES + IJCCMQ02_NUM_FRAME + IJCCMQ02_LENGTH_DATA_FRAME + IJCCMQ02_CALL_METHOD;
        public static final int IJCCMQ02_TAB_ERRORI_FRONT_END = AreaProductServicesIsps0140.IJCCMQ02_ELE_ERRORI_MAXOCCURS * IJCCMQ02_ELE_ERRORI;
        public static final int IJCCMQ02_AREA_ADDRESSES = AreaProductServicesIsps0140.IJCCMQ02_ADDRESSES_MAXOCCURS * IJCCMQ02_ADDRESSES;
        public static final int IJCCMQ02_HEADER = IJCCMQ02_MODALITA_ESECUTIVA + IJCCMQ02_JAVA_SERVICE_NAME + IJCCMQ02_INFO_FRAMES + IJCCMQ02_ESITO + IJCCMQ02_LENGTH_DATI_SERVIZIO + IJCCMQ02_MAX_ELE_ERRORI + IJCCMQ02_TAB_ERRORI_FRONT_END + IJCCMQ02_LENGTH_DATI_OUTPUT + IJCCMQ02_VAR_AMBIENTE + IJCCMQ02_AREA_ADDRESSES + IJCCMQ02_USER_NAME + IJCCMQ00_LIVELLO_DEBUG + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
