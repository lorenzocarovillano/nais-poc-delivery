package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-IMP-TRASFE<br>
 * Variable: WTIT-IMP-TRASFE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitImpTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitImpTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_IMP_TRASFE;
    }

    public void setWtitImpTrasfe(AfDecimal wtitImpTrasfe) {
        writeDecimalAsPacked(Pos.WTIT_IMP_TRASFE, wtitImpTrasfe.copy());
    }

    public void setWtitImpTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_IMP_TRASFE, Pos.WTIT_IMP_TRASFE);
    }

    /**Original name: WTIT-IMP-TRASFE<br>*/
    public AfDecimal getWtitImpTrasfe() {
        return readPackedAsDecimal(Pos.WTIT_IMP_TRASFE, Len.Int.WTIT_IMP_TRASFE, Len.Fract.WTIT_IMP_TRASFE);
    }

    public byte[] getWtitImpTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_IMP_TRASFE, Pos.WTIT_IMP_TRASFE);
        return buffer;
    }

    public void initWtitImpTrasfeSpaces() {
        fill(Pos.WTIT_IMP_TRASFE, Len.WTIT_IMP_TRASFE, Types.SPACE_CHAR);
    }

    public void setWtitImpTrasfeNull(String wtitImpTrasfeNull) {
        writeString(Pos.WTIT_IMP_TRASFE_NULL, wtitImpTrasfeNull, Len.WTIT_IMP_TRASFE_NULL);
    }

    /**Original name: WTIT-IMP-TRASFE-NULL<br>*/
    public String getWtitImpTrasfeNull() {
        return readString(Pos.WTIT_IMP_TRASFE_NULL, Len.WTIT_IMP_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_TRASFE = 1;
        public static final int WTIT_IMP_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_TRASFE = 8;
        public static final int WTIT_IMP_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
