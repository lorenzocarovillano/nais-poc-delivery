package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-TIPO-OPERAZIONE<br>
 * Variable: WCOM-TIPO-OPERAZIONE from copybook LCCC0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomTipoOperazione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TIPO_OPERAZIONE);
    public static final String PREPARA_MAPPA = "00";
    public static final String CONVALIDA = "01";
    public static final String CALCOLI = "02";
    public static final String CTR_RICALCOLO = "03";
    public static final String EOC_CON_PREVENTIVO = "04";
    public static final String ACQUIS_RICH_EST = "08";

    //==== METHODS ====
    public void setTipoOperazione(String tipoOperazione) {
        this.value = Functions.subString(tipoOperazione, Len.TIPO_OPERAZIONE);
    }

    public String getTipoOperazione() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_OPERAZIONE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
