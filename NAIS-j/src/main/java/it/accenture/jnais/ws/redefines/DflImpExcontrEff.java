package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMP-EXCONTR-EFF<br>
 * Variable: DFL-IMP-EXCONTR-EFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpExcontrEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpExcontrEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMP_EXCONTR_EFF;
    }

    public void setDflImpExcontrEff(AfDecimal dflImpExcontrEff) {
        writeDecimalAsPacked(Pos.DFL_IMP_EXCONTR_EFF, dflImpExcontrEff.copy());
    }

    public void setDflImpExcontrEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMP_EXCONTR_EFF, Pos.DFL_IMP_EXCONTR_EFF);
    }

    /**Original name: DFL-IMP-EXCONTR-EFF<br>*/
    public AfDecimal getDflImpExcontrEff() {
        return readPackedAsDecimal(Pos.DFL_IMP_EXCONTR_EFF, Len.Int.DFL_IMP_EXCONTR_EFF, Len.Fract.DFL_IMP_EXCONTR_EFF);
    }

    public byte[] getDflImpExcontrEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMP_EXCONTR_EFF, Pos.DFL_IMP_EXCONTR_EFF);
        return buffer;
    }

    public void setDflImpExcontrEffNull(String dflImpExcontrEffNull) {
        writeString(Pos.DFL_IMP_EXCONTR_EFF_NULL, dflImpExcontrEffNull, Len.DFL_IMP_EXCONTR_EFF_NULL);
    }

    /**Original name: DFL-IMP-EXCONTR-EFF-NULL<br>*/
    public String getDflImpExcontrEffNull() {
        return readString(Pos.DFL_IMP_EXCONTR_EFF_NULL, Len.DFL_IMP_EXCONTR_EFF_NULL);
    }

    public String getDflImpExcontrEffNullFormatted() {
        return Functions.padBlanks(getDflImpExcontrEffNull(), Len.DFL_IMP_EXCONTR_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMP_EXCONTR_EFF = 1;
        public static final int DFL_IMP_EXCONTR_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMP_EXCONTR_EFF = 8;
        public static final int DFL_IMP_EXCONTR_EFF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMP_EXCONTR_EFF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMP_EXCONTR_EFF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
