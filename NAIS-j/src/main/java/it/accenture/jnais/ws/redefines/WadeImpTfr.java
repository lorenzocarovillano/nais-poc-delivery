package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-IMP-TFR<br>
 * Variable: WADE-IMP-TFR from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_IMP_TFR;
    }

    public void setWadeImpTfr(AfDecimal wadeImpTfr) {
        writeDecimalAsPacked(Pos.WADE_IMP_TFR, wadeImpTfr.copy());
    }

    public void setWadeImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_IMP_TFR, Pos.WADE_IMP_TFR);
    }

    /**Original name: WADE-IMP-TFR<br>*/
    public AfDecimal getWadeImpTfr() {
        return readPackedAsDecimal(Pos.WADE_IMP_TFR, Len.Int.WADE_IMP_TFR, Len.Fract.WADE_IMP_TFR);
    }

    public byte[] getWadeImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_IMP_TFR, Pos.WADE_IMP_TFR);
        return buffer;
    }

    public void initWadeImpTfrSpaces() {
        fill(Pos.WADE_IMP_TFR, Len.WADE_IMP_TFR, Types.SPACE_CHAR);
    }

    public void setWadeImpTfrNull(String wadeImpTfrNull) {
        writeString(Pos.WADE_IMP_TFR_NULL, wadeImpTfrNull, Len.WADE_IMP_TFR_NULL);
    }

    /**Original name: WADE-IMP-TFR-NULL<br>*/
    public String getWadeImpTfrNull() {
        return readString(Pos.WADE_IMP_TFR_NULL, Len.WADE_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_IMP_TFR = 1;
        public static final int WADE_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_IMP_TFR = 8;
        public static final int WADE_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
