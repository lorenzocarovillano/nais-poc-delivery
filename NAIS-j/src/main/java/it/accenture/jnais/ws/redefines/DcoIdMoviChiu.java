package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCO-ID-MOVI-CHIU<br>
 * Variable: DCO-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DcoIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DcoIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DCO_ID_MOVI_CHIU;
    }

    public void setDcoIdMoviChiu(int dcoIdMoviChiu) {
        writeIntAsPacked(Pos.DCO_ID_MOVI_CHIU, dcoIdMoviChiu, Len.Int.DCO_ID_MOVI_CHIU);
    }

    public void setDcoIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DCO_ID_MOVI_CHIU, Pos.DCO_ID_MOVI_CHIU);
    }

    /**Original name: DCO-ID-MOVI-CHIU<br>*/
    public int getDcoIdMoviChiu() {
        return readPackedAsInt(Pos.DCO_ID_MOVI_CHIU, Len.Int.DCO_ID_MOVI_CHIU);
    }

    public byte[] getDcoIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DCO_ID_MOVI_CHIU, Pos.DCO_ID_MOVI_CHIU);
        return buffer;
    }

    public void setDcoIdMoviChiuNull(String dcoIdMoviChiuNull) {
        writeString(Pos.DCO_ID_MOVI_CHIU_NULL, dcoIdMoviChiuNull, Len.DCO_ID_MOVI_CHIU_NULL);
    }

    /**Original name: DCO-ID-MOVI-CHIU-NULL<br>*/
    public String getDcoIdMoviChiuNull() {
        return readString(Pos.DCO_ID_MOVI_CHIU_NULL, Len.DCO_ID_MOVI_CHIU_NULL);
    }

    public String getDcoIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getDcoIdMoviChiuNull(), Len.DCO_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DCO_ID_MOVI_CHIU = 1;
        public static final int DCO_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_ID_MOVI_CHIU = 5;
        public static final int DCO_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
