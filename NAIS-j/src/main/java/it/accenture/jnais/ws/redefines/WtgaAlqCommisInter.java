package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ALQ-COMMIS-INTER<br>
 * Variable: WTGA-ALQ-COMMIS-INTER from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaAlqCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaAlqCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ALQ_COMMIS_INTER;
    }

    public void setWtgaAlqCommisInter(AfDecimal wtgaAlqCommisInter) {
        writeDecimalAsPacked(Pos.WTGA_ALQ_COMMIS_INTER, wtgaAlqCommisInter.copy());
    }

    public void setWtgaAlqCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ALQ_COMMIS_INTER, Pos.WTGA_ALQ_COMMIS_INTER);
    }

    /**Original name: WTGA-ALQ-COMMIS-INTER<br>*/
    public AfDecimal getWtgaAlqCommisInter() {
        return readPackedAsDecimal(Pos.WTGA_ALQ_COMMIS_INTER, Len.Int.WTGA_ALQ_COMMIS_INTER, Len.Fract.WTGA_ALQ_COMMIS_INTER);
    }

    public byte[] getWtgaAlqCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ALQ_COMMIS_INTER, Pos.WTGA_ALQ_COMMIS_INTER);
        return buffer;
    }

    public void initWtgaAlqCommisInterSpaces() {
        fill(Pos.WTGA_ALQ_COMMIS_INTER, Len.WTGA_ALQ_COMMIS_INTER, Types.SPACE_CHAR);
    }

    public void setWtgaAlqCommisInterNull(String wtgaAlqCommisInterNull) {
        writeString(Pos.WTGA_ALQ_COMMIS_INTER_NULL, wtgaAlqCommisInterNull, Len.WTGA_ALQ_COMMIS_INTER_NULL);
    }

    /**Original name: WTGA-ALQ-COMMIS-INTER-NULL<br>*/
    public String getWtgaAlqCommisInterNull() {
        return readString(Pos.WTGA_ALQ_COMMIS_INTER_NULL, Len.WTGA_ALQ_COMMIS_INTER_NULL);
    }

    public String getWtgaAlqCommisInterNullFormatted() {
        return Functions.padBlanks(getWtgaAlqCommisInterNull(), Len.WTGA_ALQ_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_COMMIS_INTER = 1;
        public static final int WTGA_ALQ_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_COMMIS_INTER = 4;
        public static final int WTGA_ALQ_COMMIS_INTER_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
