package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.SdiDtFis;
import it.accenture.jnais.ws.redefines.SdiFrqValut;
import it.accenture.jnais.ws.redefines.SdiIdMoviChiu;
import it.accenture.jnais.ws.redefines.SdiModGest;
import it.accenture.jnais.ws.redefines.SdiPcProtezione;

/**Original name: STRA-DI-INVST<br>
 * Variable: STRA-DI-INVST from copybook IDBVSDI1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class StraDiInvstIdbssdi0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: SDI-ID-STRA-DI-INVST
    private int sdiIdStraDiInvst = DefaultValues.INT_VAL;
    //Original name: SDI-ID-OGG
    private int sdiIdOgg = DefaultValues.INT_VAL;
    //Original name: SDI-TP-OGG
    private String sdiTpOgg = DefaultValues.stringVal(Len.SDI_TP_OGG);
    //Original name: SDI-ID-MOVI-CRZ
    private int sdiIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: SDI-ID-MOVI-CHIU
    private SdiIdMoviChiu sdiIdMoviChiu = new SdiIdMoviChiu();
    //Original name: SDI-DT-INI-EFF
    private int sdiDtIniEff = DefaultValues.INT_VAL;
    //Original name: SDI-DT-END-EFF
    private int sdiDtEndEff = DefaultValues.INT_VAL;
    //Original name: SDI-COD-COMP-ANIA
    private int sdiCodCompAnia = DefaultValues.INT_VAL;
    //Original name: SDI-COD-STRA
    private String sdiCodStra = DefaultValues.stringVal(Len.SDI_COD_STRA);
    //Original name: SDI-MOD-GEST
    private SdiModGest sdiModGest = new SdiModGest();
    //Original name: SDI-VAR-RIFTO
    private String sdiVarRifto = DefaultValues.stringVal(Len.SDI_VAR_RIFTO);
    //Original name: SDI-VAL-VAR-RIFTO
    private String sdiValVarRifto = DefaultValues.stringVal(Len.SDI_VAL_VAR_RIFTO);
    //Original name: SDI-DT-FIS
    private SdiDtFis sdiDtFis = new SdiDtFis();
    //Original name: SDI-FRQ-VALUT
    private SdiFrqValut sdiFrqValut = new SdiFrqValut();
    //Original name: SDI-FL-INI-END-PER
    private char sdiFlIniEndPer = DefaultValues.CHAR_VAL;
    //Original name: SDI-DS-RIGA
    private long sdiDsRiga = DefaultValues.LONG_VAL;
    //Original name: SDI-DS-OPER-SQL
    private char sdiDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: SDI-DS-VER
    private int sdiDsVer = DefaultValues.INT_VAL;
    //Original name: SDI-DS-TS-INI-CPTZ
    private long sdiDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: SDI-DS-TS-END-CPTZ
    private long sdiDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: SDI-DS-UTENTE
    private String sdiDsUtente = DefaultValues.stringVal(Len.SDI_DS_UTENTE);
    //Original name: SDI-DS-STATO-ELAB
    private char sdiDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: SDI-TP-STRA
    private String sdiTpStra = DefaultValues.stringVal(Len.SDI_TP_STRA);
    //Original name: SDI-TP-PROVZA-STRA
    private String sdiTpProvzaStra = DefaultValues.stringVal(Len.SDI_TP_PROVZA_STRA);
    //Original name: SDI-PC-PROTEZIONE
    private SdiPcProtezione sdiPcProtezione = new SdiPcProtezione();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.STRA_DI_INVST;
    }

    @Override
    public void deserialize(byte[] buf) {
        setStraDiInvstBytes(buf);
    }

    public void setStraDiInvstFormatted(String data) {
        byte[] buffer = new byte[Len.STRA_DI_INVST];
        MarshalByte.writeString(buffer, 1, data, Len.STRA_DI_INVST);
        setStraDiInvstBytes(buffer, 1);
    }

    public String getStraDiInvstFormatted() {
        return MarshalByteExt.bufferToStr(getStraDiInvstBytes());
    }

    public void setStraDiInvstBytes(byte[] buffer) {
        setStraDiInvstBytes(buffer, 1);
    }

    public byte[] getStraDiInvstBytes() {
        byte[] buffer = new byte[Len.STRA_DI_INVST];
        return getStraDiInvstBytes(buffer, 1);
    }

    public void setStraDiInvstBytes(byte[] buffer, int offset) {
        int position = offset;
        sdiIdStraDiInvst = MarshalByte.readPackedAsInt(buffer, position, Len.Int.SDI_ID_STRA_DI_INVST, 0);
        position += Len.SDI_ID_STRA_DI_INVST;
        sdiIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.SDI_ID_OGG, 0);
        position += Len.SDI_ID_OGG;
        sdiTpOgg = MarshalByte.readString(buffer, position, Len.SDI_TP_OGG);
        position += Len.SDI_TP_OGG;
        sdiIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.SDI_ID_MOVI_CRZ, 0);
        position += Len.SDI_ID_MOVI_CRZ;
        sdiIdMoviChiu.setSdiIdMoviChiuFromBuffer(buffer, position);
        position += SdiIdMoviChiu.Len.SDI_ID_MOVI_CHIU;
        sdiDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.SDI_DT_INI_EFF, 0);
        position += Len.SDI_DT_INI_EFF;
        sdiDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.SDI_DT_END_EFF, 0);
        position += Len.SDI_DT_END_EFF;
        sdiCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.SDI_COD_COMP_ANIA, 0);
        position += Len.SDI_COD_COMP_ANIA;
        sdiCodStra = MarshalByte.readString(buffer, position, Len.SDI_COD_STRA);
        position += Len.SDI_COD_STRA;
        sdiModGest.setSdiModGestFromBuffer(buffer, position);
        position += SdiModGest.Len.SDI_MOD_GEST;
        sdiVarRifto = MarshalByte.readString(buffer, position, Len.SDI_VAR_RIFTO);
        position += Len.SDI_VAR_RIFTO;
        sdiValVarRifto = MarshalByte.readString(buffer, position, Len.SDI_VAL_VAR_RIFTO);
        position += Len.SDI_VAL_VAR_RIFTO;
        sdiDtFis.setSdiDtFisFromBuffer(buffer, position);
        position += SdiDtFis.Len.SDI_DT_FIS;
        sdiFrqValut.setSdiFrqValutFromBuffer(buffer, position);
        position += SdiFrqValut.Len.SDI_FRQ_VALUT;
        sdiFlIniEndPer = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        sdiDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.SDI_DS_RIGA, 0);
        position += Len.SDI_DS_RIGA;
        sdiDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        sdiDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.SDI_DS_VER, 0);
        position += Len.SDI_DS_VER;
        sdiDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.SDI_DS_TS_INI_CPTZ, 0);
        position += Len.SDI_DS_TS_INI_CPTZ;
        sdiDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.SDI_DS_TS_END_CPTZ, 0);
        position += Len.SDI_DS_TS_END_CPTZ;
        sdiDsUtente = MarshalByte.readString(buffer, position, Len.SDI_DS_UTENTE);
        position += Len.SDI_DS_UTENTE;
        sdiDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        sdiTpStra = MarshalByte.readString(buffer, position, Len.SDI_TP_STRA);
        position += Len.SDI_TP_STRA;
        sdiTpProvzaStra = MarshalByte.readString(buffer, position, Len.SDI_TP_PROVZA_STRA);
        position += Len.SDI_TP_PROVZA_STRA;
        sdiPcProtezione.setSdiPcProtezioneFromBuffer(buffer, position);
    }

    public byte[] getStraDiInvstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, sdiIdStraDiInvst, Len.Int.SDI_ID_STRA_DI_INVST, 0);
        position += Len.SDI_ID_STRA_DI_INVST;
        MarshalByte.writeIntAsPacked(buffer, position, sdiIdOgg, Len.Int.SDI_ID_OGG, 0);
        position += Len.SDI_ID_OGG;
        MarshalByte.writeString(buffer, position, sdiTpOgg, Len.SDI_TP_OGG);
        position += Len.SDI_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, sdiIdMoviCrz, Len.Int.SDI_ID_MOVI_CRZ, 0);
        position += Len.SDI_ID_MOVI_CRZ;
        sdiIdMoviChiu.getSdiIdMoviChiuAsBuffer(buffer, position);
        position += SdiIdMoviChiu.Len.SDI_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, sdiDtIniEff, Len.Int.SDI_DT_INI_EFF, 0);
        position += Len.SDI_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, sdiDtEndEff, Len.Int.SDI_DT_END_EFF, 0);
        position += Len.SDI_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, sdiCodCompAnia, Len.Int.SDI_COD_COMP_ANIA, 0);
        position += Len.SDI_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, sdiCodStra, Len.SDI_COD_STRA);
        position += Len.SDI_COD_STRA;
        sdiModGest.getSdiModGestAsBuffer(buffer, position);
        position += SdiModGest.Len.SDI_MOD_GEST;
        MarshalByte.writeString(buffer, position, sdiVarRifto, Len.SDI_VAR_RIFTO);
        position += Len.SDI_VAR_RIFTO;
        MarshalByte.writeString(buffer, position, sdiValVarRifto, Len.SDI_VAL_VAR_RIFTO);
        position += Len.SDI_VAL_VAR_RIFTO;
        sdiDtFis.getSdiDtFisAsBuffer(buffer, position);
        position += SdiDtFis.Len.SDI_DT_FIS;
        sdiFrqValut.getSdiFrqValutAsBuffer(buffer, position);
        position += SdiFrqValut.Len.SDI_FRQ_VALUT;
        MarshalByte.writeChar(buffer, position, sdiFlIniEndPer);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, sdiDsRiga, Len.Int.SDI_DS_RIGA, 0);
        position += Len.SDI_DS_RIGA;
        MarshalByte.writeChar(buffer, position, sdiDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, sdiDsVer, Len.Int.SDI_DS_VER, 0);
        position += Len.SDI_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, sdiDsTsIniCptz, Len.Int.SDI_DS_TS_INI_CPTZ, 0);
        position += Len.SDI_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, sdiDsTsEndCptz, Len.Int.SDI_DS_TS_END_CPTZ, 0);
        position += Len.SDI_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, sdiDsUtente, Len.SDI_DS_UTENTE);
        position += Len.SDI_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, sdiDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, sdiTpStra, Len.SDI_TP_STRA);
        position += Len.SDI_TP_STRA;
        MarshalByte.writeString(buffer, position, sdiTpProvzaStra, Len.SDI_TP_PROVZA_STRA);
        position += Len.SDI_TP_PROVZA_STRA;
        sdiPcProtezione.getSdiPcProtezioneAsBuffer(buffer, position);
        return buffer;
    }

    public void setSdiIdStraDiInvst(int sdiIdStraDiInvst) {
        this.sdiIdStraDiInvst = sdiIdStraDiInvst;
    }

    public int getSdiIdStraDiInvst() {
        return this.sdiIdStraDiInvst;
    }

    public void setSdiIdOgg(int sdiIdOgg) {
        this.sdiIdOgg = sdiIdOgg;
    }

    public int getSdiIdOgg() {
        return this.sdiIdOgg;
    }

    public void setSdiTpOgg(String sdiTpOgg) {
        this.sdiTpOgg = Functions.subString(sdiTpOgg, Len.SDI_TP_OGG);
    }

    public String getSdiTpOgg() {
        return this.sdiTpOgg;
    }

    public void setSdiIdMoviCrz(int sdiIdMoviCrz) {
        this.sdiIdMoviCrz = sdiIdMoviCrz;
    }

    public int getSdiIdMoviCrz() {
        return this.sdiIdMoviCrz;
    }

    public void setSdiDtIniEff(int sdiDtIniEff) {
        this.sdiDtIniEff = sdiDtIniEff;
    }

    public int getSdiDtIniEff() {
        return this.sdiDtIniEff;
    }

    public void setSdiDtEndEff(int sdiDtEndEff) {
        this.sdiDtEndEff = sdiDtEndEff;
    }

    public int getSdiDtEndEff() {
        return this.sdiDtEndEff;
    }

    public void setSdiCodCompAnia(int sdiCodCompAnia) {
        this.sdiCodCompAnia = sdiCodCompAnia;
    }

    public int getSdiCodCompAnia() {
        return this.sdiCodCompAnia;
    }

    public void setSdiCodStra(String sdiCodStra) {
        this.sdiCodStra = Functions.subString(sdiCodStra, Len.SDI_COD_STRA);
    }

    public String getSdiCodStra() {
        return this.sdiCodStra;
    }

    public void setSdiVarRifto(String sdiVarRifto) {
        this.sdiVarRifto = Functions.subString(sdiVarRifto, Len.SDI_VAR_RIFTO);
    }

    public String getSdiVarRifto() {
        return this.sdiVarRifto;
    }

    public String getSdiVarRiftoFormatted() {
        return Functions.padBlanks(getSdiVarRifto(), Len.SDI_VAR_RIFTO);
    }

    public void setSdiValVarRifto(String sdiValVarRifto) {
        this.sdiValVarRifto = Functions.subString(sdiValVarRifto, Len.SDI_VAL_VAR_RIFTO);
    }

    public String getSdiValVarRifto() {
        return this.sdiValVarRifto;
    }

    public String getSdiValVarRiftoFormatted() {
        return Functions.padBlanks(getSdiValVarRifto(), Len.SDI_VAL_VAR_RIFTO);
    }

    public void setSdiFlIniEndPer(char sdiFlIniEndPer) {
        this.sdiFlIniEndPer = sdiFlIniEndPer;
    }

    public char getSdiFlIniEndPer() {
        return this.sdiFlIniEndPer;
    }

    public void setSdiDsRiga(long sdiDsRiga) {
        this.sdiDsRiga = sdiDsRiga;
    }

    public long getSdiDsRiga() {
        return this.sdiDsRiga;
    }

    public void setSdiDsOperSql(char sdiDsOperSql) {
        this.sdiDsOperSql = sdiDsOperSql;
    }

    public void setSdiDsOperSqlFormatted(String sdiDsOperSql) {
        setSdiDsOperSql(Functions.charAt(sdiDsOperSql, Types.CHAR_SIZE));
    }

    public char getSdiDsOperSql() {
        return this.sdiDsOperSql;
    }

    public void setSdiDsVer(int sdiDsVer) {
        this.sdiDsVer = sdiDsVer;
    }

    public int getSdiDsVer() {
        return this.sdiDsVer;
    }

    public void setSdiDsTsIniCptz(long sdiDsTsIniCptz) {
        this.sdiDsTsIniCptz = sdiDsTsIniCptz;
    }

    public long getSdiDsTsIniCptz() {
        return this.sdiDsTsIniCptz;
    }

    public void setSdiDsTsEndCptz(long sdiDsTsEndCptz) {
        this.sdiDsTsEndCptz = sdiDsTsEndCptz;
    }

    public long getSdiDsTsEndCptz() {
        return this.sdiDsTsEndCptz;
    }

    public void setSdiDsUtente(String sdiDsUtente) {
        this.sdiDsUtente = Functions.subString(sdiDsUtente, Len.SDI_DS_UTENTE);
    }

    public String getSdiDsUtente() {
        return this.sdiDsUtente;
    }

    public void setSdiDsStatoElab(char sdiDsStatoElab) {
        this.sdiDsStatoElab = sdiDsStatoElab;
    }

    public void setSdiDsStatoElabFormatted(String sdiDsStatoElab) {
        setSdiDsStatoElab(Functions.charAt(sdiDsStatoElab, Types.CHAR_SIZE));
    }

    public char getSdiDsStatoElab() {
        return this.sdiDsStatoElab;
    }

    public void setSdiTpStra(String sdiTpStra) {
        this.sdiTpStra = Functions.subString(sdiTpStra, Len.SDI_TP_STRA);
    }

    public String getSdiTpStra() {
        return this.sdiTpStra;
    }

    public String getSdiTpStraFormatted() {
        return Functions.padBlanks(getSdiTpStra(), Len.SDI_TP_STRA);
    }

    public void setSdiTpProvzaStra(String sdiTpProvzaStra) {
        this.sdiTpProvzaStra = Functions.subString(sdiTpProvzaStra, Len.SDI_TP_PROVZA_STRA);
    }

    public String getSdiTpProvzaStra() {
        return this.sdiTpProvzaStra;
    }

    public String getSdiTpProvzaStraFormatted() {
        return Functions.padBlanks(getSdiTpProvzaStra(), Len.SDI_TP_PROVZA_STRA);
    }

    public SdiDtFis getSdiDtFis() {
        return sdiDtFis;
    }

    public SdiFrqValut getSdiFrqValut() {
        return sdiFrqValut;
    }

    public SdiIdMoviChiu getSdiIdMoviChiu() {
        return sdiIdMoviChiu;
    }

    public SdiModGest getSdiModGest() {
        return sdiModGest;
    }

    public SdiPcProtezione getSdiPcProtezione() {
        return sdiPcProtezione;
    }

    @Override
    public byte[] serialize() {
        return getStraDiInvstBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int SDI_ID_STRA_DI_INVST = 5;
        public static final int SDI_ID_OGG = 5;
        public static final int SDI_TP_OGG = 2;
        public static final int SDI_ID_MOVI_CRZ = 5;
        public static final int SDI_DT_INI_EFF = 5;
        public static final int SDI_DT_END_EFF = 5;
        public static final int SDI_COD_COMP_ANIA = 3;
        public static final int SDI_COD_STRA = 30;
        public static final int SDI_VAR_RIFTO = 12;
        public static final int SDI_VAL_VAR_RIFTO = 12;
        public static final int SDI_FL_INI_END_PER = 1;
        public static final int SDI_DS_RIGA = 6;
        public static final int SDI_DS_OPER_SQL = 1;
        public static final int SDI_DS_VER = 5;
        public static final int SDI_DS_TS_INI_CPTZ = 10;
        public static final int SDI_DS_TS_END_CPTZ = 10;
        public static final int SDI_DS_UTENTE = 20;
        public static final int SDI_DS_STATO_ELAB = 1;
        public static final int SDI_TP_STRA = 2;
        public static final int SDI_TP_PROVZA_STRA = 2;
        public static final int STRA_DI_INVST = SDI_ID_STRA_DI_INVST + SDI_ID_OGG + SDI_TP_OGG + SDI_ID_MOVI_CRZ + SdiIdMoviChiu.Len.SDI_ID_MOVI_CHIU + SDI_DT_INI_EFF + SDI_DT_END_EFF + SDI_COD_COMP_ANIA + SDI_COD_STRA + SdiModGest.Len.SDI_MOD_GEST + SDI_VAR_RIFTO + SDI_VAL_VAR_RIFTO + SdiDtFis.Len.SDI_DT_FIS + SdiFrqValut.Len.SDI_FRQ_VALUT + SDI_FL_INI_END_PER + SDI_DS_RIGA + SDI_DS_OPER_SQL + SDI_DS_VER + SDI_DS_TS_INI_CPTZ + SDI_DS_TS_END_CPTZ + SDI_DS_UTENTE + SDI_DS_STATO_ELAB + SDI_TP_STRA + SDI_TP_PROVZA_STRA + SdiPcProtezione.Len.SDI_PC_PROTEZIONE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SDI_ID_STRA_DI_INVST = 9;
            public static final int SDI_ID_OGG = 9;
            public static final int SDI_ID_MOVI_CRZ = 9;
            public static final int SDI_DT_INI_EFF = 8;
            public static final int SDI_DT_END_EFF = 8;
            public static final int SDI_COD_COMP_ANIA = 5;
            public static final int SDI_DS_RIGA = 10;
            public static final int SDI_DS_VER = 9;
            public static final int SDI_DS_TS_INI_CPTZ = 18;
            public static final int SDI_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
