package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-RSP-CL<br>
 * Variable: PCO-DT-ULT-BOLL-RSP-CL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollRspCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollRspCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_RSP_CL;
    }

    public void setPcoDtUltBollRspCl(int pcoDtUltBollRspCl) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_RSP_CL, pcoDtUltBollRspCl, Len.Int.PCO_DT_ULT_BOLL_RSP_CL);
    }

    public void setPcoDtUltBollRspClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RSP_CL, Pos.PCO_DT_ULT_BOLL_RSP_CL);
    }

    /**Original name: PCO-DT-ULT-BOLL-RSP-CL<br>*/
    public int getPcoDtUltBollRspCl() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_RSP_CL, Len.Int.PCO_DT_ULT_BOLL_RSP_CL);
    }

    public byte[] getPcoDtUltBollRspClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RSP_CL, Pos.PCO_DT_ULT_BOLL_RSP_CL);
        return buffer;
    }

    public void initPcoDtUltBollRspClHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_RSP_CL, Len.PCO_DT_ULT_BOLL_RSP_CL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollRspClNull(String pcoDtUltBollRspClNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_RSP_CL_NULL, pcoDtUltBollRspClNull, Len.PCO_DT_ULT_BOLL_RSP_CL_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-RSP-CL-NULL<br>*/
    public String getPcoDtUltBollRspClNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_RSP_CL_NULL, Len.PCO_DT_ULT_BOLL_RSP_CL_NULL);
    }

    public String getPcoDtUltBollRspClNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollRspClNull(), Len.PCO_DT_ULT_BOLL_RSP_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RSP_CL = 1;
        public static final int PCO_DT_ULT_BOLL_RSP_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RSP_CL = 5;
        public static final int PCO_DT_ULT_BOLL_RSP_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_RSP_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
