package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMP-LRD-LIQTO<br>
 * Variable: S089-TOT-IMP-LRD-LIQTO from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpLrdLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpLrdLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMP_LRD_LIQTO;
    }

    public void setWlquTotImpLrdLiqto(AfDecimal wlquTotImpLrdLiqto) {
        writeDecimalAsPacked(Pos.S089_TOT_IMP_LRD_LIQTO, wlquTotImpLrdLiqto.copy());
    }

    public void setWlquTotImpLrdLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMP_LRD_LIQTO, Pos.S089_TOT_IMP_LRD_LIQTO);
    }

    /**Original name: WLQU-TOT-IMP-LRD-LIQTO<br>*/
    public AfDecimal getWlquTotImpLrdLiqto() {
        return readPackedAsDecimal(Pos.S089_TOT_IMP_LRD_LIQTO, Len.Int.WLQU_TOT_IMP_LRD_LIQTO, Len.Fract.WLQU_TOT_IMP_LRD_LIQTO);
    }

    public byte[] getWlquTotImpLrdLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMP_LRD_LIQTO, Pos.S089_TOT_IMP_LRD_LIQTO);
        return buffer;
    }

    public void initWlquTotImpLrdLiqtoSpaces() {
        fill(Pos.S089_TOT_IMP_LRD_LIQTO, Len.S089_TOT_IMP_LRD_LIQTO, Types.SPACE_CHAR);
    }

    public void setWlquTotImpLrdLiqtoNull(String wlquTotImpLrdLiqtoNull) {
        writeString(Pos.S089_TOT_IMP_LRD_LIQTO_NULL, wlquTotImpLrdLiqtoNull, Len.WLQU_TOT_IMP_LRD_LIQTO_NULL);
    }

    /**Original name: WLQU-TOT-IMP-LRD-LIQTO-NULL<br>*/
    public String getWlquTotImpLrdLiqtoNull() {
        return readString(Pos.S089_TOT_IMP_LRD_LIQTO_NULL, Len.WLQU_TOT_IMP_LRD_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_LRD_LIQTO = 1;
        public static final int S089_TOT_IMP_LRD_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_LRD_LIQTO = 8;
        public static final int WLQU_TOT_IMP_LRD_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_LRD_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_LRD_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
