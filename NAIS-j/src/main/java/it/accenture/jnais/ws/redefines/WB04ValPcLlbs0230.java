package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B04-VAL-PC<br>
 * Variable: W-B04-VAL-PC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB04ValPcLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB04ValPcLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B04_VAL_PC;
    }

    public void setwB04ValPc(AfDecimal wB04ValPc) {
        writeDecimalAsPacked(Pos.W_B04_VAL_PC, wB04ValPc.copy());
    }

    /**Original name: W-B04-VAL-PC<br>*/
    public AfDecimal getwB04ValPc() {
        return readPackedAsDecimal(Pos.W_B04_VAL_PC, Len.Int.W_B04_VAL_PC, Len.Fract.W_B04_VAL_PC);
    }

    public byte[] getwB04ValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B04_VAL_PC, Pos.W_B04_VAL_PC);
        return buffer;
    }

    public void setwB04ValPcNull(String wB04ValPcNull) {
        writeString(Pos.W_B04_VAL_PC_NULL, wB04ValPcNull, Len.W_B04_VAL_PC_NULL);
    }

    /**Original name: W-B04-VAL-PC-NULL<br>*/
    public String getwB04ValPcNull() {
        return readString(Pos.W_B04_VAL_PC_NULL, Len.W_B04_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B04_VAL_PC = 1;
        public static final int W_B04_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B04_VAL_PC = 8;
        public static final int W_B04_VAL_PC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B04_VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B04_VAL_PC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
