package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-DUR-MM<br>
 * Variable: TGA-DUR-MM from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaDurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaDurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_DUR_MM;
    }

    public void setTgaDurMm(int tgaDurMm) {
        writeIntAsPacked(Pos.TGA_DUR_MM, tgaDurMm, Len.Int.TGA_DUR_MM);
    }

    public void setTgaDurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_DUR_MM, Pos.TGA_DUR_MM);
    }

    /**Original name: TGA-DUR-MM<br>*/
    public int getTgaDurMm() {
        return readPackedAsInt(Pos.TGA_DUR_MM, Len.Int.TGA_DUR_MM);
    }

    public byte[] getTgaDurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_DUR_MM, Pos.TGA_DUR_MM);
        return buffer;
    }

    public void setTgaDurMmNull(String tgaDurMmNull) {
        writeString(Pos.TGA_DUR_MM_NULL, tgaDurMmNull, Len.TGA_DUR_MM_NULL);
    }

    /**Original name: TGA-DUR-MM-NULL<br>*/
    public String getTgaDurMmNull() {
        return readString(Pos.TGA_DUR_MM_NULL, Len.TGA_DUR_MM_NULL);
    }

    public String getTgaDurMmNullFormatted() {
        return Functions.padBlanks(getTgaDurMmNull(), Len.TGA_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_DUR_MM = 1;
        public static final int TGA_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_DUR_MM = 3;
        public static final int TGA_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
