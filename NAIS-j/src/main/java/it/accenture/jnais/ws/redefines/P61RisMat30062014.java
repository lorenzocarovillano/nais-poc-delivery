package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P61-RIS-MAT-30062014<br>
 * Variable: P61-RIS-MAT-30062014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P61RisMat30062014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P61RisMat30062014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P61_RIS_MAT30062014;
    }

    public void setP61RisMat30062014(AfDecimal p61RisMat30062014) {
        writeDecimalAsPacked(Pos.P61_RIS_MAT30062014, p61RisMat30062014.copy());
    }

    public void setP61RisMat30062014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P61_RIS_MAT30062014, Pos.P61_RIS_MAT30062014);
    }

    /**Original name: P61-RIS-MAT-30062014<br>*/
    public AfDecimal getP61RisMat30062014() {
        return readPackedAsDecimal(Pos.P61_RIS_MAT30062014, Len.Int.P61_RIS_MAT30062014, Len.Fract.P61_RIS_MAT30062014);
    }

    public byte[] getP61RisMat30062014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P61_RIS_MAT30062014, Pos.P61_RIS_MAT30062014);
        return buffer;
    }

    public void setP61RisMat30062014Null(String p61RisMat30062014Null) {
        writeString(Pos.P61_RIS_MAT30062014_NULL, p61RisMat30062014Null, Len.P61_RIS_MAT30062014_NULL);
    }

    /**Original name: P61-RIS-MAT-30062014-NULL<br>*/
    public String getP61RisMat30062014Null() {
        return readString(Pos.P61_RIS_MAT30062014_NULL, Len.P61_RIS_MAT30062014_NULL);
    }

    public String getP61RisMat30062014NullFormatted() {
        return Functions.padBlanks(getP61RisMat30062014Null(), Len.P61_RIS_MAT30062014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P61_RIS_MAT30062014 = 1;
        public static final int P61_RIS_MAT30062014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_RIS_MAT30062014 = 8;
        public static final int P61_RIS_MAT30062014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_RIS_MAT30062014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P61_RIS_MAT30062014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
