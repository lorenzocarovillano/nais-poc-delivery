package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;

/**Original name: IDSV0045-STR-INP-OUT<br>
 * Variables: IDSV0045-STR-INP-OUT from program IDSS0020<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idsv0045StrInpOut {

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_STR_DATO = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
