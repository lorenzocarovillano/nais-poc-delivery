package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ACT-TP-GARANZIA<br>
 * Variable: ACT-TP-GARANZIA from copybook LCCC0006<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class ActTpGaranzia {

    //==== PROPERTIES ====
    private String value = "0";
    public static final String BASE_FLD = "1";
    public static final String COMPLEM = "2";
    public static final String OPZIONE = "3";
    public static final String ACCESSORIA = "4";
    public static final String DIFFERIMENTO = "5";
    public static final String CEDOLA = "6";

    //==== METHODS ====
    public void setActTpGaranzia(short actTpGaranzia) {
        this.value = NumericDisplay.asString(actTpGaranzia, Len.ACT_TP_GARANZIA);
    }

    public void setActTpGaranziaFormatted(String actTpGaranzia) {
        this.value = Trunc.toUnsignedNumeric(actTpGaranzia, Len.ACT_TP_GARANZIA);
    }

    public short getActTpGaranzia() {
        return NumericDisplay.asShort(this.value);
    }

    public String getActTpGaranziaFormatted() {
        return this.value;
    }

    public boolean isBaseFld() {
        return getActTpGaranziaFormatted().equals(BASE_FLD);
    }

    public void setTpGarBase() {
        setActTpGaranziaFormatted(BASE_FLD);
    }

    public boolean isTpGarComplem() {
        return getActTpGaranziaFormatted().equals(COMPLEM);
    }

    public void setTpGarComplem() {
        setActTpGaranziaFormatted(COMPLEM);
    }

    public boolean isTpGarAccessoria() {
        return getActTpGaranziaFormatted().equals(ACCESSORIA);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ACT_TP_GARANZIA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
