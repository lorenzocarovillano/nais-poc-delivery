package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.AreaLdbv1131;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Lccvpmoz;
import it.accenture.jnais.copy.Ldbvg351;
import it.accenture.jnais.copy.ParamMovi;
import it.accenture.jnais.copy.ParamOggLoas0800;
import it.accenture.jnais.copy.StatOggBus;
import it.accenture.jnais.ws.enums.PmoTrov;
import it.accenture.jnais.ws.enums.WkPmoTrovato;
import it.accenture.jnais.ws.enums.WkTpOpzioni;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTpCaus;
import it.accenture.jnais.ws.enums.WsTpFrmAssva;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0320<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0320Data {

    //==== PROPERTIES ====
    //Original name: WK-PGM
    private String wkPgm = "LCCS0320";
    //Original name: LDBSG350
    private String ldbsg350 = "LDBSG350";
    //Original name: WK-INDICI
    private WkIndici wkIndici = new WkIndici();
    //Original name: PARAM-MOVI
    private ParamMovi paramMovi = new ParamMovi();
    //Original name: LDBVG351
    private Ldbvg351 ldbvg351 = new Ldbvg351();
    //Original name: STAT-OGG-BUS
    private StatOggBus statOggBus = new StatOggBus();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: AREA-IO-LCCS0029
    private AreaIoLccs0029 areaIoLccs0029 = new AreaIoLccs0029();
    //Original name: IO-A2K-LCCC0003
    private IoA2kLccc0003 ioA2kLccc0003 = new IoA2kLccc0003();
    //Original name: AREA-LDBV1131
    private AreaLdbv1131 areaLdbv1131 = new AreaLdbv1131();
    /**Original name: WK-LABEL-ERR<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*
	 *  --> Area di appoggio per i messaggi di errore</pre>*/
    private String wkLabelErr = "";
    //Original name: IN-RCODE
    private String inRcode = DefaultValues.stringVal(Len.IN_RCODE);
    //Original name: WK-DT-CALCOLATA
    private String wkDtCalcolata = "00000000";
    //Original name: WK-DT-LIMITE
    private String wkDtLimite = "00000000";
    //Original name: WK-APPO-DT-RICOR-SUCC
    private String wkAppoDtRicorSucc = "00000000";
    //Original name: WK-APPO-DT-NUM
    private String wkAppoDtNum = "00000000";
    //Original name: WK-DT-DEC
    private WkDtDec wkDtDec = new WkDtDec();
    //Original name: WK-DT-CALC
    private WkDtCalc wkDtCalc = new WkDtCalc();
    //Original name: WK-COD-OPZIONE
    private String wkCodOpzione = "";
    //Original name: FILLER-WK-STR-OPZIONE
    private String flr1 = "";
    //Original name: WK-TP-OPZIONI
    private WkTpOpzioni wkTpOpzioni = new WkTpOpzioni();
    /**Original name: WK-PMO-TROVATO<br>
	 * <pre> --> Ricerca Occorrenza Parametro Movimento</pre>*/
    private WkPmoTrovato wkPmoTrovato = new WkPmoTrovato();
    /**Original name: WK-RICERCA-PERIODADEG<br>
	 * <pre> --> Ricerca Occorrenza Parametro OGGETTO</pre>*/
    private boolean wkRicercaPeriodadeg = false;
    //Original name: WK-FRQ-MOVI
    private int wkFrqMovi = DefaultValues.INT_VAL;
    //Original name: WK-DT-RICOR-SUCC
    private int wkDtRicorSucc = DefaultValues.INT_VAL;
    //Original name: PMO-TROV
    private PmoTrov pmoTrov = new PmoTrov();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    /**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    /**Original name: WS-TP-FRM-ASSVA<br>
	 * <pre>*****************************************************************
	 *     TP_FRM_ASSVA (Forma Assicurativa)
	 * *****************************************************************</pre>*/
    private WsTpFrmAssva wsTpFrmAssva = new WsTpFrmAssva();
    /**Original name: WS-TP-CAUS<br>
	 * <pre>*****************************************************************
	 *     TP_CAUS (Tipo Causale)
	 * *****************************************************************</pre>*/
    private WsTpCaus wsTpCaus = new WsTpCaus();
    //Original name: LCCVPMOZ
    private Lccvpmoz lccvpmoz = new Lccvpmoz();
    //Original name: PARAM-OGG
    private ParamOggLoas0800 paramOgg = new ParamOggLoas0800();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getLdbsg350() {
        return this.ldbsg350;
    }

    public void setWkLabelErr(String wkLabelErr) {
        this.wkLabelErr = Functions.subString(wkLabelErr, Len.WK_LABEL_ERR);
    }

    public String getWkLabelErr() {
        return this.wkLabelErr;
    }

    public void setInRcodeFromBuffer(byte[] buffer) {
        inRcode = MarshalByte.readFixedString(buffer, 1, Len.IN_RCODE);
    }

    public String getInRcodeFormatted() {
        return this.inRcode;
    }

    public void setWkDtCalcolataFormatted(String wkDtCalcolata) {
        this.wkDtCalcolata = Trunc.toUnsignedNumeric(wkDtCalcolata, Len.WK_DT_CALCOLATA);
    }

    public void setWkDtCalcolataFromBuffer(byte[] buffer) {
        wkDtCalcolata = MarshalByte.readFixedString(buffer, 1, Len.WK_DT_CALCOLATA);
    }

    public int getWkDtCalcolata() {
        return NumericDisplay.asInt(this.wkDtCalcolata);
    }

    public String getWkDtCalcolataFormatted() {
        return this.wkDtCalcolata;
    }

    public void setWkDtLimiteFormatted(String wkDtLimite) {
        this.wkDtLimite = Trunc.toUnsignedNumeric(wkDtLimite, Len.WK_DT_LIMITE);
    }

    public int getWkDtLimite() {
        return NumericDisplay.asInt(this.wkDtLimite);
    }

    public void setWkAppoDtRicorSucc(int wkAppoDtRicorSucc) {
        this.wkAppoDtRicorSucc = NumericDisplay.asString(wkAppoDtRicorSucc, Len.WK_APPO_DT_RICOR_SUCC);
    }

    public void setWkAppoDtRicorSuccFormatted(String wkAppoDtRicorSucc) {
        this.wkAppoDtRicorSucc = Trunc.toUnsignedNumeric(wkAppoDtRicorSucc, Len.WK_APPO_DT_RICOR_SUCC);
    }

    public int getWkAppoDtRicorSucc() {
        return NumericDisplay.asInt(this.wkAppoDtRicorSucc);
    }

    public void setWkAppoDtNum(int wkAppoDtNum) {
        this.wkAppoDtNum = NumericDisplay.asString(wkAppoDtNum, Len.WK_APPO_DT_NUM);
    }

    public void setWkAppoDtNumFormatted(String wkAppoDtNum) {
        this.wkAppoDtNum = Trunc.toUnsignedNumeric(wkAppoDtNum, Len.WK_APPO_DT_NUM);
    }

    public int getWkAppoDtNum() {
        return NumericDisplay.asInt(this.wkAppoDtNum);
    }

    public String getWkAppoDtNumFormatted() {
        return this.wkAppoDtNum;
    }

    public void setWkStrOpzioneFormatted(String data) {
        byte[] buffer = new byte[Len.WK_STR_OPZIONE];
        MarshalByte.writeString(buffer, 1, data, Len.WK_STR_OPZIONE);
        setWkStrOpzioneBytes(buffer, 1);
    }

    public void setWkStrOpzioneBytes(byte[] buffer, int offset) {
        int position = offset;
        wkCodOpzione = MarshalByte.readString(buffer, position, Len.WK_COD_OPZIONE);
        position += Len.WK_COD_OPZIONE;
        flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
    }

    public void setWkCodOpzione(String wkCodOpzione) {
        this.wkCodOpzione = Functions.subString(wkCodOpzione, Len.WK_COD_OPZIONE);
    }

    public String getWkCodOpzione() {
        return this.wkCodOpzione;
    }

    public void setFlr1(String flr1) {
        this.flr1 = Functions.subString(flr1, Len.FLR1);
    }

    public String getFlr1() {
        return this.flr1;
    }

    public void setWkRicercaPeriodadeg(boolean wkRicercaPeriodadeg) {
        this.wkRicercaPeriodadeg = wkRicercaPeriodadeg;
    }

    public boolean isWkRicercaPeriodadeg() {
        return this.wkRicercaPeriodadeg;
    }

    public void setWkFrqMovi(int wkFrqMovi) {
        this.wkFrqMovi = wkFrqMovi;
    }

    public int getWkFrqMovi() {
        return this.wkFrqMovi;
    }

    public void setWkDtRicorSucc(int wkDtRicorSucc) {
        this.wkDtRicorSucc = wkDtRicorSucc;
    }

    public int getWkDtRicorSucc() {
        return this.wkDtRicorSucc;
    }

    public AreaIoLccs0029 getAreaIoLccs0029() {
        return areaIoLccs0029;
    }

    public AreaLdbv1131 getAreaLdbv1131() {
        return areaLdbv1131;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public IoA2kLccc0003 getIoA2kLccc0003() {
        return ioA2kLccc0003;
    }

    public Lccvpmoz getLccvpmoz() {
        return lccvpmoz;
    }

    public Ldbvg351 getLdbvg351() {
        return ldbvg351;
    }

    public ParamMovi getParamMovi() {
        return paramMovi;
    }

    public ParamOggLoas0800 getParamOgg() {
        return paramOgg;
    }

    public PmoTrov getPmoTrov() {
        return pmoTrov;
    }

    public StatOggBus getStatOggBus() {
        return statOggBus;
    }

    public WkDtCalc getWkDtCalc() {
        return wkDtCalc;
    }

    public WkDtDec getWkDtDec() {
        return wkDtDec;
    }

    public WkIndici getWkIndici() {
        return wkIndici;
    }

    public WkPmoTrovato getWkPmoTrovato() {
        return wkPmoTrovato;
    }

    public WkTpOpzioni getWkTpOpzioni() {
        return wkTpOpzioni;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTpCaus getWsTpCaus() {
        return wsTpCaus;
    }

    public WsTpFrmAssva getWsTpFrmAssva() {
        return wsTpFrmAssva;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IN_RCODE = 2;
        public static final int WK_DT_CALCOLATA = 8;
        public static final int WK_DT_LIMITE = 8;
        public static final int WK_APPO_DT_RICOR_SUCC = 8;
        public static final int WK_APPO_DT_NUM = 8;
        public static final int WK_LABEL_ERR = 30;
        public static final int WK_COD_OPZIONE = 2;
        public static final int FLR1 = 10;
        public static final int WK_STR_OPZIONE = WK_COD_OPZIONE + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
