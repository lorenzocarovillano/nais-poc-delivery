package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-MM-PAG-PRE-UNI<br>
 * Variable: WGRZ-MM-PAG-PRE-UNI from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzMmPagPreUni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzMmPagPreUni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_MM_PAG_PRE_UNI;
    }

    public void setWgrzMmPagPreUni(int wgrzMmPagPreUni) {
        writeIntAsPacked(Pos.WGRZ_MM_PAG_PRE_UNI, wgrzMmPagPreUni, Len.Int.WGRZ_MM_PAG_PRE_UNI);
    }

    public void setWgrzMmPagPreUniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_MM_PAG_PRE_UNI, Pos.WGRZ_MM_PAG_PRE_UNI);
    }

    /**Original name: WGRZ-MM-PAG-PRE-UNI<br>*/
    public int getWgrzMmPagPreUni() {
        return readPackedAsInt(Pos.WGRZ_MM_PAG_PRE_UNI, Len.Int.WGRZ_MM_PAG_PRE_UNI);
    }

    public byte[] getWgrzMmPagPreUniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_MM_PAG_PRE_UNI, Pos.WGRZ_MM_PAG_PRE_UNI);
        return buffer;
    }

    public void initWgrzMmPagPreUniSpaces() {
        fill(Pos.WGRZ_MM_PAG_PRE_UNI, Len.WGRZ_MM_PAG_PRE_UNI, Types.SPACE_CHAR);
    }

    public void setWgrzMmPagPreUniNull(String wgrzMmPagPreUniNull) {
        writeString(Pos.WGRZ_MM_PAG_PRE_UNI_NULL, wgrzMmPagPreUniNull, Len.WGRZ_MM_PAG_PRE_UNI_NULL);
    }

    /**Original name: WGRZ-MM-PAG-PRE-UNI-NULL<br>*/
    public String getWgrzMmPagPreUniNull() {
        return readString(Pos.WGRZ_MM_PAG_PRE_UNI_NULL, Len.WGRZ_MM_PAG_PRE_UNI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_MM_PAG_PRE_UNI = 1;
        public static final int WGRZ_MM_PAG_PRE_UNI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_MM_PAG_PRE_UNI = 3;
        public static final int WGRZ_MM_PAG_PRE_UNI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_MM_PAG_PRE_UNI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
