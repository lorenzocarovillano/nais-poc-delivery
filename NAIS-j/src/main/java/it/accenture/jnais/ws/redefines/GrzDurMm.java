package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-DUR-MM<br>
 * Variable: GRZ-DUR-MM from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzDurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzDurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_DUR_MM;
    }

    public void setGrzDurMm(int grzDurMm) {
        writeIntAsPacked(Pos.GRZ_DUR_MM, grzDurMm, Len.Int.GRZ_DUR_MM);
    }

    public void setGrzDurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_DUR_MM, Pos.GRZ_DUR_MM);
    }

    /**Original name: GRZ-DUR-MM<br>*/
    public int getGrzDurMm() {
        return readPackedAsInt(Pos.GRZ_DUR_MM, Len.Int.GRZ_DUR_MM);
    }

    public byte[] getGrzDurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_DUR_MM, Pos.GRZ_DUR_MM);
        return buffer;
    }

    public void setGrzDurMmNull(String grzDurMmNull) {
        writeString(Pos.GRZ_DUR_MM_NULL, grzDurMmNull, Len.GRZ_DUR_MM_NULL);
    }

    /**Original name: GRZ-DUR-MM-NULL<br>*/
    public String getGrzDurMmNull() {
        return readString(Pos.GRZ_DUR_MM_NULL, Len.GRZ_DUR_MM_NULL);
    }

    public String getGrzDurMmNullFormatted() {
        return Functions.padBlanks(getGrzDurMmNull(), Len.GRZ_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_DUR_MM = 1;
        public static final int GRZ_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_DUR_MM = 3;
        public static final int GRZ_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
