package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-QTZ-SP-Z-OPZ-DT-CA<br>
 * Variable: W-B03-QTZ-SP-Z-OPZ-DT-CA from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03QtzSpZOpzDtCaLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03QtzSpZOpzDtCaLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_QTZ_SP_Z_OPZ_DT_CA;
    }

    public void setwB03QtzSpZOpzDtCa(AfDecimal wB03QtzSpZOpzDtCa) {
        writeDecimalAsPacked(Pos.W_B03_QTZ_SP_Z_OPZ_DT_CA, wB03QtzSpZOpzDtCa.copy());
    }

    /**Original name: W-B03-QTZ-SP-Z-OPZ-DT-CA<br>*/
    public AfDecimal getwB03QtzSpZOpzDtCa() {
        return readPackedAsDecimal(Pos.W_B03_QTZ_SP_Z_OPZ_DT_CA, Len.Int.W_B03_QTZ_SP_Z_OPZ_DT_CA, Len.Fract.W_B03_QTZ_SP_Z_OPZ_DT_CA);
    }

    public byte[] getwB03QtzSpZOpzDtCaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_QTZ_SP_Z_OPZ_DT_CA, Pos.W_B03_QTZ_SP_Z_OPZ_DT_CA);
        return buffer;
    }

    public void setwB03QtzSpZOpzDtCaNull(String wB03QtzSpZOpzDtCaNull) {
        writeString(Pos.W_B03_QTZ_SP_Z_OPZ_DT_CA_NULL, wB03QtzSpZOpzDtCaNull, Len.W_B03_QTZ_SP_Z_OPZ_DT_CA_NULL);
    }

    /**Original name: W-B03-QTZ-SP-Z-OPZ-DT-CA-NULL<br>*/
    public String getwB03QtzSpZOpzDtCaNull() {
        return readString(Pos.W_B03_QTZ_SP_Z_OPZ_DT_CA_NULL, Len.W_B03_QTZ_SP_Z_OPZ_DT_CA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_QTZ_SP_Z_OPZ_DT_CA = 1;
        public static final int W_B03_QTZ_SP_Z_OPZ_DT_CA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_QTZ_SP_Z_OPZ_DT_CA = 7;
        public static final int W_B03_QTZ_SP_Z_OPZ_DT_CA_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_QTZ_SP_Z_OPZ_DT_CA = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_QTZ_SP_Z_OPZ_DT_CA = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
