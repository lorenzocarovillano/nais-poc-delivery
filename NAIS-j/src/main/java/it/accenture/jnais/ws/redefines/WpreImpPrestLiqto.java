package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPRE-IMP-PREST-LIQTO<br>
 * Variable: WPRE-IMP-PREST-LIQTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreImpPrestLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpreImpPrestLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_IMP_PREST_LIQTO;
    }

    public void setWpreImpPrestLiqto(AfDecimal wpreImpPrestLiqto) {
        writeDecimalAsPacked(Pos.WPRE_IMP_PREST_LIQTO, wpreImpPrestLiqto.copy());
    }

    public void setWpreImpPrestLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_IMP_PREST_LIQTO, Pos.WPRE_IMP_PREST_LIQTO);
    }

    /**Original name: WPRE-IMP-PREST-LIQTO<br>*/
    public AfDecimal getWpreImpPrestLiqto() {
        return readPackedAsDecimal(Pos.WPRE_IMP_PREST_LIQTO, Len.Int.WPRE_IMP_PREST_LIQTO, Len.Fract.WPRE_IMP_PREST_LIQTO);
    }

    public byte[] getWpreImpPrestLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_IMP_PREST_LIQTO, Pos.WPRE_IMP_PREST_LIQTO);
        return buffer;
    }

    public void initWpreImpPrestLiqtoSpaces() {
        fill(Pos.WPRE_IMP_PREST_LIQTO, Len.WPRE_IMP_PREST_LIQTO, Types.SPACE_CHAR);
    }

    public void setWpreImpPrestLiqtoNull(String wpreImpPrestLiqtoNull) {
        writeString(Pos.WPRE_IMP_PREST_LIQTO_NULL, wpreImpPrestLiqtoNull, Len.WPRE_IMP_PREST_LIQTO_NULL);
    }

    /**Original name: WPRE-IMP-PREST-LIQTO-NULL<br>*/
    public String getWpreImpPrestLiqtoNull() {
        return readString(Pos.WPRE_IMP_PREST_LIQTO_NULL, Len.WPRE_IMP_PREST_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_IMP_PREST_LIQTO = 1;
        public static final int WPRE_IMP_PREST_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_IMP_PREST_LIQTO = 8;
        public static final int WPRE_IMP_PREST_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPRE_IMP_PREST_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_IMP_PREST_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
