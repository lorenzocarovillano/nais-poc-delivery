package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP85-RENDTO-RETR<br>
 * Variable: WP85-RENDTO-RETR from program LRGS0660<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp85RendtoRetr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp85RendtoRetr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP85_RENDTO_RETR;
    }

    public void setWp85RendtoRetr(AfDecimal wp85RendtoRetr) {
        writeDecimalAsPacked(Pos.WP85_RENDTO_RETR, wp85RendtoRetr.copy());
    }

    /**Original name: WP85-RENDTO-RETR<br>*/
    public AfDecimal getWp85RendtoRetr() {
        return readPackedAsDecimal(Pos.WP85_RENDTO_RETR, Len.Int.WP85_RENDTO_RETR, Len.Fract.WP85_RENDTO_RETR);
    }

    public void setWp85RendtoRetrNull(String wp85RendtoRetrNull) {
        writeString(Pos.WP85_RENDTO_RETR_NULL, wp85RendtoRetrNull, Len.WP85_RENDTO_RETR_NULL);
    }

    /**Original name: WP85-RENDTO-RETR-NULL<br>*/
    public String getWp85RendtoRetrNull() {
        return readString(Pos.WP85_RENDTO_RETR_NULL, Len.WP85_RENDTO_RETR_NULL);
    }

    public String getWp85RendtoRetrNullFormatted() {
        return Functions.padBlanks(getWp85RendtoRetrNull(), Len.WP85_RENDTO_RETR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP85_RENDTO_RETR = 1;
        public static final int WP85_RENDTO_RETR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP85_RENDTO_RETR = 8;
        public static final int WP85_RENDTO_RETR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP85_RENDTO_RETR = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP85_RENDTO_RETR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
