package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvdco1;
import it.accenture.jnais.copy.WdcoDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WDCO-AREA-DT-COLLETTIVA<br>
 * Variable: WDCO-AREA-DT-COLLETTIVA from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WdcoAreaDtCollettiva extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WDCO-ELE-COLL-MAX
    private short wdcoEleCollMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDCO1
    private Lccvdco1 lccvdco1 = new Lccvdco1();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_AREA_DT_COLLETTIVA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWdcoAreaDtCollettivaBytes(buf);
    }

    public String getWdcoAreaDtCollettivaFormatted() {
        return MarshalByteExt.bufferToStr(getWdcoAreaDtCollettivaBytes());
    }

    public void setWdcoAreaDtCollettivaBytes(byte[] buffer) {
        setWdcoAreaDtCollettivaBytes(buffer, 1);
    }

    public byte[] getWdcoAreaDtCollettivaBytes() {
        byte[] buffer = new byte[Len.WDCO_AREA_DT_COLLETTIVA];
        return getWdcoAreaDtCollettivaBytes(buffer, 1);
    }

    public void setWdcoAreaDtCollettivaBytes(byte[] buffer, int offset) {
        int position = offset;
        wdcoEleCollMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWdcoTabCollBytes(buffer, position);
    }

    public byte[] getWdcoAreaDtCollettivaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdcoEleCollMax);
        position += Types.SHORT_SIZE;
        getWdcoTabCollBytes(buffer, position);
        return buffer;
    }

    public void setWdcoEleCollMax(short wdcoEleCollMax) {
        this.wdcoEleCollMax = wdcoEleCollMax;
    }

    public short getWdcoEleCollMax() {
        return this.wdcoEleCollMax;
    }

    public void setWdcoTabCollBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdco1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdco1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdco1.Len.Int.ID_PTF, 0));
        position += Lccvdco1.Len.ID_PTF;
        lccvdco1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdcoTabCollBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdco1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdco1.getIdPtf(), Lccvdco1.Len.Int.ID_PTF, 0);
        position += Lccvdco1.Len.ID_PTF;
        lccvdco1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    @Override
    public byte[] serialize() {
        return getWdcoAreaDtCollettivaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_ELE_COLL_MAX = 2;
        public static final int WDCO_TAB_COLL = WpolStatus.Len.STATUS + Lccvdco1.Len.ID_PTF + WdcoDati.Len.DATI;
        public static final int WDCO_AREA_DT_COLLETTIVA = WDCO_ELE_COLL_MAX + WDCO_TAB_COLL;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
