package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0590-TAB-PROD<br>
 * Variables: ISPC0590-TAB-PROD from copybook ISPC0590<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0590TabProd {

    //==== PROPERTIES ====
    //Original name: ISPC0590-COD-TARI
    private String codTari = DefaultValues.stringVal(Len.COD_TARI);
    //Original name: ISPC0590-TASSO-TECNICO
    private AfDecimal tassoTecnico = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);

    //==== METHODS ====
    public void setTabProdBytes(byte[] buffer, int offset) {
        int position = offset;
        codTari = MarshalByte.readString(buffer, position, Len.COD_TARI);
        position += Len.COD_TARI;
        tassoTecnico.assign(MarshalByte.readDecimal(buffer, position, Len.Int.TASSO_TECNICO, Len.Fract.TASSO_TECNICO, SignType.NO_SIGN));
    }

    public byte[] getTabProdBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codTari, Len.COD_TARI);
        position += Len.COD_TARI;
        MarshalByte.writeDecimal(buffer, position, tassoTecnico.copy(), SignType.NO_SIGN);
        return buffer;
    }

    public void initTabProdSpaces() {
        codTari = "";
        tassoTecnico.setNaN();
    }

    public void setCodTari(String codTari) {
        this.codTari = Functions.subString(codTari, Len.COD_TARI);
    }

    public String getCodTari() {
        return this.codTari;
    }

    public void setTassoTecnico(AfDecimal tassoTecnico) {
        this.tassoTecnico.assign(tassoTecnico);
    }

    public AfDecimal getTassoTecnico() {
        return this.tassoTecnico.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_TARI = 12;
        public static final int TASSO_TECNICO = 14;
        public static final int TAB_PROD = COD_TARI + TASSO_TECNICO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TASSO_TECNICO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TASSO_TECNICO = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
