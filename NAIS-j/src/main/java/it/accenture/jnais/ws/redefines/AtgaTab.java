package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: ATGA-TAB<br>
 * Variable: ATGA-TAB from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AtgaTab extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_TRAN_MAXOCCURS = 1250;
    public static final char ATGA_ST_ADD = 'A';
    public static final char ATGA_ST_MOD = 'M';
    public static final char ATGA_ST_INV = 'I';
    public static final char ATGA_ST_DEL = 'D';
    public static final char ATGA_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public AtgaTab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ATGA_TAB;
    }

    public String getAtgaTabFormatted() {
        return readFixedString(Pos.ATGA_TAB, Len.ATGA_TAB);
    }

    public void setAtgaTabBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ATGA_TAB, Pos.ATGA_TAB);
    }

    public byte[] getAtgaTabBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ATGA_TAB, Pos.ATGA_TAB);
        return buffer;
    }

    public void setTabTranBytes(int tabTranIdx, byte[] buffer) {
        setTabTranBytes(tabTranIdx, buffer, 1);
    }

    public void setTabTranBytes(int tabTranIdx, byte[] buffer, int offset) {
        int position = Pos.atgaTabTran(tabTranIdx - 1);
        setBytes(buffer, offset, Len.TAB_TRAN, position);
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.atgaStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: ATGA-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA TRCH_DI_GAR
	 *    ALIAS TGA
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.atgaStatus(statusIdx - 1);
        return readChar(position);
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.atgaIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: ATGA-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.atgaIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdTrchDiGar(int idTrchDiGarIdx, int idTrchDiGar) {
        int position = Pos.atgaIdTrchDiGar(idTrchDiGarIdx - 1);
        writeIntAsPacked(position, idTrchDiGar, Len.Int.ID_TRCH_DI_GAR);
    }

    /**Original name: ATGA-ID-TRCH-DI-GAR<br>*/
    public int getIdTrchDiGar(int idTrchDiGarIdx) {
        int position = Pos.atgaIdTrchDiGar(idTrchDiGarIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_TRCH_DI_GAR);
    }

    public void setIdGar(int idGarIdx, int idGar) {
        int position = Pos.atgaIdGar(idGarIdx - 1);
        writeIntAsPacked(position, idGar, Len.Int.ID_GAR);
    }

    /**Original name: ATGA-ID-GAR<br>*/
    public int getIdGar(int idGarIdx) {
        int position = Pos.atgaIdGar(idGarIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_GAR);
    }

    public void setIdAdes(int idAdesIdx, int idAdes) {
        int position = Pos.atgaIdAdes(idAdesIdx - 1);
        writeIntAsPacked(position, idAdes, Len.Int.ID_ADES);
    }

    /**Original name: ATGA-ID-ADES<br>*/
    public int getIdAdes(int idAdesIdx) {
        int position = Pos.atgaIdAdes(idAdesIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_ADES);
    }

    public void setIdPoli(int idPoliIdx, int idPoli) {
        int position = Pos.atgaIdPoli(idPoliIdx - 1);
        writeIntAsPacked(position, idPoli, Len.Int.ID_POLI);
    }

    /**Original name: ATGA-ID-POLI<br>*/
    public int getIdPoli(int idPoliIdx) {
        int position = Pos.atgaIdPoli(idPoliIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_POLI);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.atgaIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: ATGA-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.atgaIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.atgaIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: ATGA-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.atgaIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.atgaDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: ATGA-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.atgaDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.atgaDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: ATGA-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.atgaDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.atgaCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: ATGA-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.atgaCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setDtDecor(int dtDecorIdx, int dtDecor) {
        int position = Pos.atgaDtDecor(dtDecorIdx - 1);
        writeIntAsPacked(position, dtDecor, Len.Int.DT_DECOR);
    }

    /**Original name: ATGA-DT-DECOR<br>*/
    public int getDtDecor(int dtDecorIdx) {
        int position = Pos.atgaDtDecor(dtDecorIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_DECOR);
    }

    public void setDtScad(int dtScadIdx, int dtScad) {
        int position = Pos.atgaDtScad(dtScadIdx - 1);
        writeIntAsPacked(position, dtScad, Len.Int.DT_SCAD);
    }

    /**Original name: ATGA-DT-SCAD<br>*/
    public int getDtScad(int dtScadIdx) {
        int position = Pos.atgaDtScad(dtScadIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_SCAD);
    }

    public void setIbOgg(int ibOggIdx, String ibOgg) {
        int position = Pos.atgaIbOgg(ibOggIdx - 1);
        writeString(position, ibOgg, Len.IB_OGG);
    }

    /**Original name: ATGA-IB-OGG<br>*/
    public String getIbOgg(int ibOggIdx) {
        int position = Pos.atgaIbOgg(ibOggIdx - 1);
        return readString(position, Len.IB_OGG);
    }

    public void setTpRgmFisc(int tpRgmFiscIdx, String tpRgmFisc) {
        int position = Pos.atgaTpRgmFisc(tpRgmFiscIdx - 1);
        writeString(position, tpRgmFisc, Len.TP_RGM_FISC);
    }

    /**Original name: ATGA-TP-RGM-FISC<br>*/
    public String getTpRgmFisc(int tpRgmFiscIdx) {
        int position = Pos.atgaTpRgmFisc(tpRgmFiscIdx - 1);
        return readString(position, Len.TP_RGM_FISC);
    }

    public void setDtEmis(int dtEmisIdx, int dtEmis) {
        int position = Pos.atgaDtEmis(dtEmisIdx - 1);
        writeIntAsPacked(position, dtEmis, Len.Int.DT_EMIS);
    }

    /**Original name: ATGA-DT-EMIS<br>*/
    public int getDtEmis(int dtEmisIdx) {
        int position = Pos.atgaDtEmis(dtEmisIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_EMIS);
    }

    public void setTpTrch(int tpTrchIdx, String tpTrch) {
        int position = Pos.atgaTpTrch(tpTrchIdx - 1);
        writeString(position, tpTrch, Len.TP_TRCH);
    }

    /**Original name: ATGA-TP-TRCH<br>*/
    public String getTpTrch(int tpTrchIdx) {
        int position = Pos.atgaTpTrch(tpTrchIdx - 1);
        return readString(position, Len.TP_TRCH);
    }

    public void setDurAa(int durAaIdx, int durAa) {
        int position = Pos.atgaDurAa(durAaIdx - 1);
        writeIntAsPacked(position, durAa, Len.Int.DUR_AA);
    }

    /**Original name: ATGA-DUR-AA<br>*/
    public int getDurAa(int durAaIdx) {
        int position = Pos.atgaDurAa(durAaIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_AA);
    }

    public void setDurMm(int durMmIdx, int durMm) {
        int position = Pos.atgaDurMm(durMmIdx - 1);
        writeIntAsPacked(position, durMm, Len.Int.DUR_MM);
    }

    /**Original name: ATGA-DUR-MM<br>*/
    public int getDurMm(int durMmIdx) {
        int position = Pos.atgaDurMm(durMmIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_MM);
    }

    public void setDurGg(int durGgIdx, int durGg) {
        int position = Pos.atgaDurGg(durGgIdx - 1);
        writeIntAsPacked(position, durGg, Len.Int.DUR_GG);
    }

    /**Original name: ATGA-DUR-GG<br>*/
    public int getDurGg(int durGgIdx) {
        int position = Pos.atgaDurGg(durGgIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_GG);
    }

    public void setPreCasoMor(int preCasoMorIdx, AfDecimal preCasoMor) {
        int position = Pos.atgaPreCasoMor(preCasoMorIdx - 1);
        writeDecimalAsPacked(position, preCasoMor.copy());
    }

    /**Original name: ATGA-PRE-CASO-MOR<br>*/
    public AfDecimal getPreCasoMor(int preCasoMorIdx) {
        int position = Pos.atgaPreCasoMor(preCasoMorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_CASO_MOR, Len.Fract.PRE_CASO_MOR);
    }

    public void setPcIntrRiat(int pcIntrRiatIdx, AfDecimal pcIntrRiat) {
        int position = Pos.atgaPcIntrRiat(pcIntrRiatIdx - 1);
        writeDecimalAsPacked(position, pcIntrRiat.copy());
    }

    /**Original name: ATGA-PC-INTR-RIAT<br>*/
    public AfDecimal getPcIntrRiat(int pcIntrRiatIdx) {
        int position = Pos.atgaPcIntrRiat(pcIntrRiatIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_INTR_RIAT, Len.Fract.PC_INTR_RIAT);
    }

    public void setImpBnsAntic(int impBnsAnticIdx, AfDecimal impBnsAntic) {
        int position = Pos.atgaImpBnsAntic(impBnsAnticIdx - 1);
        writeDecimalAsPacked(position, impBnsAntic.copy());
    }

    /**Original name: ATGA-IMP-BNS-ANTIC<br>*/
    public AfDecimal getImpBnsAntic(int impBnsAnticIdx) {
        int position = Pos.atgaImpBnsAntic(impBnsAnticIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_BNS_ANTIC, Len.Fract.IMP_BNS_ANTIC);
    }

    public void setPreIniNet(int preIniNetIdx, AfDecimal preIniNet) {
        int position = Pos.atgaPreIniNet(preIniNetIdx - 1);
        writeDecimalAsPacked(position, preIniNet.copy());
    }

    /**Original name: ATGA-PRE-INI-NET<br>*/
    public AfDecimal getPreIniNet(int preIniNetIdx) {
        int position = Pos.atgaPreIniNet(preIniNetIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_INI_NET, Len.Fract.PRE_INI_NET);
    }

    public void setPrePpIni(int prePpIniIdx, AfDecimal prePpIni) {
        int position = Pos.atgaPrePpIni(prePpIniIdx - 1);
        writeDecimalAsPacked(position, prePpIni.copy());
    }

    /**Original name: ATGA-PRE-PP-INI<br>*/
    public AfDecimal getPrePpIni(int prePpIniIdx) {
        int position = Pos.atgaPrePpIni(prePpIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_PP_INI, Len.Fract.PRE_PP_INI);
    }

    public void setPrePpUlt(int prePpUltIdx, AfDecimal prePpUlt) {
        int position = Pos.atgaPrePpUlt(prePpUltIdx - 1);
        writeDecimalAsPacked(position, prePpUlt.copy());
    }

    /**Original name: ATGA-PRE-PP-ULT<br>*/
    public AfDecimal getPrePpUlt(int prePpUltIdx) {
        int position = Pos.atgaPrePpUlt(prePpUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_PP_ULT, Len.Fract.PRE_PP_ULT);
    }

    public void setPreTariIni(int preTariIniIdx, AfDecimal preTariIni) {
        int position = Pos.atgaPreTariIni(preTariIniIdx - 1);
        writeDecimalAsPacked(position, preTariIni.copy());
    }

    /**Original name: ATGA-PRE-TARI-INI<br>*/
    public AfDecimal getPreTariIni(int preTariIniIdx) {
        int position = Pos.atgaPreTariIni(preTariIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_TARI_INI, Len.Fract.PRE_TARI_INI);
    }

    public void setPreTariUlt(int preTariUltIdx, AfDecimal preTariUlt) {
        int position = Pos.atgaPreTariUlt(preTariUltIdx - 1);
        writeDecimalAsPacked(position, preTariUlt.copy());
    }

    /**Original name: ATGA-PRE-TARI-ULT<br>*/
    public AfDecimal getPreTariUlt(int preTariUltIdx) {
        int position = Pos.atgaPreTariUlt(preTariUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_TARI_ULT, Len.Fract.PRE_TARI_ULT);
    }

    public void setPreInvrioIni(int preInvrioIniIdx, AfDecimal preInvrioIni) {
        int position = Pos.atgaPreInvrioIni(preInvrioIniIdx - 1);
        writeDecimalAsPacked(position, preInvrioIni.copy());
    }

    /**Original name: ATGA-PRE-INVRIO-INI<br>*/
    public AfDecimal getPreInvrioIni(int preInvrioIniIdx) {
        int position = Pos.atgaPreInvrioIni(preInvrioIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_INVRIO_INI, Len.Fract.PRE_INVRIO_INI);
    }

    public void setPreInvrioUlt(int preInvrioUltIdx, AfDecimal preInvrioUlt) {
        int position = Pos.atgaPreInvrioUlt(preInvrioUltIdx - 1);
        writeDecimalAsPacked(position, preInvrioUlt.copy());
    }

    /**Original name: ATGA-PRE-INVRIO-ULT<br>*/
    public AfDecimal getPreInvrioUlt(int preInvrioUltIdx) {
        int position = Pos.atgaPreInvrioUlt(preInvrioUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_INVRIO_ULT, Len.Fract.PRE_INVRIO_ULT);
    }

    public void setPreRivto(int preRivtoIdx, AfDecimal preRivto) {
        int position = Pos.atgaPreRivto(preRivtoIdx - 1);
        writeDecimalAsPacked(position, preRivto.copy());
    }

    /**Original name: ATGA-PRE-RIVTO<br>*/
    public AfDecimal getPreRivto(int preRivtoIdx) {
        int position = Pos.atgaPreRivto(preRivtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_RIVTO, Len.Fract.PRE_RIVTO);
    }

    public void setImpSoprProf(int impSoprProfIdx, AfDecimal impSoprProf) {
        int position = Pos.atgaImpSoprProf(impSoprProfIdx - 1);
        writeDecimalAsPacked(position, impSoprProf.copy());
    }

    /**Original name: ATGA-IMP-SOPR-PROF<br>*/
    public AfDecimal getImpSoprProf(int impSoprProfIdx) {
        int position = Pos.atgaImpSoprProf(impSoprProfIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_PROF, Len.Fract.IMP_SOPR_PROF);
    }

    public void setImpSoprSan(int impSoprSanIdx, AfDecimal impSoprSan) {
        int position = Pos.atgaImpSoprSan(impSoprSanIdx - 1);
        writeDecimalAsPacked(position, impSoprSan.copy());
    }

    /**Original name: ATGA-IMP-SOPR-SAN<br>*/
    public AfDecimal getImpSoprSan(int impSoprSanIdx) {
        int position = Pos.atgaImpSoprSan(impSoprSanIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_SAN, Len.Fract.IMP_SOPR_SAN);
    }

    public void setImpSoprSpo(int impSoprSpoIdx, AfDecimal impSoprSpo) {
        int position = Pos.atgaImpSoprSpo(impSoprSpoIdx - 1);
        writeDecimalAsPacked(position, impSoprSpo.copy());
    }

    /**Original name: ATGA-IMP-SOPR-SPO<br>*/
    public AfDecimal getImpSoprSpo(int impSoprSpoIdx) {
        int position = Pos.atgaImpSoprSpo(impSoprSpoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_SPO, Len.Fract.IMP_SOPR_SPO);
    }

    public void setImpSoprTec(int impSoprTecIdx, AfDecimal impSoprTec) {
        int position = Pos.atgaImpSoprTec(impSoprTecIdx - 1);
        writeDecimalAsPacked(position, impSoprTec.copy());
    }

    /**Original name: ATGA-IMP-SOPR-TEC<br>*/
    public AfDecimal getImpSoprTec(int impSoprTecIdx) {
        int position = Pos.atgaImpSoprTec(impSoprTecIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_TEC, Len.Fract.IMP_SOPR_TEC);
    }

    public void setImpAltSopr(int impAltSoprIdx, AfDecimal impAltSopr) {
        int position = Pos.atgaImpAltSopr(impAltSoprIdx - 1);
        writeDecimalAsPacked(position, impAltSopr.copy());
    }

    /**Original name: ATGA-IMP-ALT-SOPR<br>*/
    public AfDecimal getImpAltSopr(int impAltSoprIdx) {
        int position = Pos.atgaImpAltSopr(impAltSoprIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_ALT_SOPR, Len.Fract.IMP_ALT_SOPR);
    }

    public void setPreStab(int preStabIdx, AfDecimal preStab) {
        int position = Pos.atgaPreStab(preStabIdx - 1);
        writeDecimalAsPacked(position, preStab.copy());
    }

    /**Original name: ATGA-PRE-STAB<br>*/
    public AfDecimal getPreStab(int preStabIdx) {
        int position = Pos.atgaPreStab(preStabIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_STAB, Len.Fract.PRE_STAB);
    }

    public void setDtEffStab(int dtEffStabIdx, int dtEffStab) {
        int position = Pos.atgaDtEffStab(dtEffStabIdx - 1);
        writeIntAsPacked(position, dtEffStab, Len.Int.DT_EFF_STAB);
    }

    /**Original name: ATGA-DT-EFF-STAB<br>*/
    public int getDtEffStab(int dtEffStabIdx) {
        int position = Pos.atgaDtEffStab(dtEffStabIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_EFF_STAB);
    }

    public void setTsRivalFis(int tsRivalFisIdx, AfDecimal tsRivalFis) {
        int position = Pos.atgaTsRivalFis(tsRivalFisIdx - 1);
        writeDecimalAsPacked(position, tsRivalFis.copy());
    }

    /**Original name: ATGA-TS-RIVAL-FIS<br>*/
    public AfDecimal getTsRivalFis(int tsRivalFisIdx) {
        int position = Pos.atgaTsRivalFis(tsRivalFisIdx - 1);
        return readPackedAsDecimal(position, Len.Int.TS_RIVAL_FIS, Len.Fract.TS_RIVAL_FIS);
    }

    public void setTsRivalIndiciz(int tsRivalIndicizIdx, AfDecimal tsRivalIndiciz) {
        int position = Pos.atgaTsRivalIndiciz(tsRivalIndicizIdx - 1);
        writeDecimalAsPacked(position, tsRivalIndiciz.copy());
    }

    /**Original name: ATGA-TS-RIVAL-INDICIZ<br>*/
    public AfDecimal getTsRivalIndiciz(int tsRivalIndicizIdx) {
        int position = Pos.atgaTsRivalIndiciz(tsRivalIndicizIdx - 1);
        return readPackedAsDecimal(position, Len.Int.TS_RIVAL_INDICIZ, Len.Fract.TS_RIVAL_INDICIZ);
    }

    public void setOldTsTec(int oldTsTecIdx, AfDecimal oldTsTec) {
        int position = Pos.atgaOldTsTec(oldTsTecIdx - 1);
        writeDecimalAsPacked(position, oldTsTec.copy());
    }

    /**Original name: ATGA-OLD-TS-TEC<br>*/
    public AfDecimal getOldTsTec(int oldTsTecIdx) {
        int position = Pos.atgaOldTsTec(oldTsTecIdx - 1);
        return readPackedAsDecimal(position, Len.Int.OLD_TS_TEC, Len.Fract.OLD_TS_TEC);
    }

    public void setRatLrd(int ratLrdIdx, AfDecimal ratLrd) {
        int position = Pos.atgaRatLrd(ratLrdIdx - 1);
        writeDecimalAsPacked(position, ratLrd.copy());
    }

    /**Original name: ATGA-RAT-LRD<br>*/
    public AfDecimal getRatLrd(int ratLrdIdx) {
        int position = Pos.atgaRatLrd(ratLrdIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RAT_LRD, Len.Fract.RAT_LRD);
    }

    public void setPreLrd(int preLrdIdx, AfDecimal preLrd) {
        int position = Pos.atgaPreLrd(preLrdIdx - 1);
        writeDecimalAsPacked(position, preLrd.copy());
    }

    /**Original name: ATGA-PRE-LRD<br>*/
    public AfDecimal getPreLrd(int preLrdIdx) {
        int position = Pos.atgaPreLrd(preLrdIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_LRD, Len.Fract.PRE_LRD);
    }

    public void setPrstzIni(int prstzIniIdx, AfDecimal prstzIni) {
        int position = Pos.atgaPrstzIni(prstzIniIdx - 1);
        writeDecimalAsPacked(position, prstzIni.copy());
    }

    /**Original name: ATGA-PRSTZ-INI<br>*/
    public AfDecimal getPrstzIni(int prstzIniIdx) {
        int position = Pos.atgaPrstzIni(prstzIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI, Len.Fract.PRSTZ_INI);
    }

    public void setPrstzUlt(int prstzUltIdx, AfDecimal prstzUlt) {
        int position = Pos.atgaPrstzUlt(prstzUltIdx - 1);
        writeDecimalAsPacked(position, prstzUlt.copy());
    }

    /**Original name: ATGA-PRSTZ-ULT<br>*/
    public AfDecimal getPrstzUlt(int prstzUltIdx) {
        int position = Pos.atgaPrstzUlt(prstzUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_ULT, Len.Fract.PRSTZ_ULT);
    }

    public void setCptInOpzRivto(int cptInOpzRivtoIdx, AfDecimal cptInOpzRivto) {
        int position = Pos.atgaCptInOpzRivto(cptInOpzRivtoIdx - 1);
        writeDecimalAsPacked(position, cptInOpzRivto.copy());
    }

    /**Original name: ATGA-CPT-IN-OPZ-RIVTO<br>*/
    public AfDecimal getCptInOpzRivto(int cptInOpzRivtoIdx) {
        int position = Pos.atgaCptInOpzRivto(cptInOpzRivtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.CPT_IN_OPZ_RIVTO, Len.Fract.CPT_IN_OPZ_RIVTO);
    }

    public void setPrstzIniStab(int prstzIniStabIdx, AfDecimal prstzIniStab) {
        int position = Pos.atgaPrstzIniStab(prstzIniStabIdx - 1);
        writeDecimalAsPacked(position, prstzIniStab.copy());
    }

    /**Original name: ATGA-PRSTZ-INI-STAB<br>*/
    public AfDecimal getPrstzIniStab(int prstzIniStabIdx) {
        int position = Pos.atgaPrstzIniStab(prstzIniStabIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI_STAB, Len.Fract.PRSTZ_INI_STAB);
    }

    public void setCptRshMor(int cptRshMorIdx, AfDecimal cptRshMor) {
        int position = Pos.atgaCptRshMor(cptRshMorIdx - 1);
        writeDecimalAsPacked(position, cptRshMor.copy());
    }

    /**Original name: ATGA-CPT-RSH-MOR<br>*/
    public AfDecimal getCptRshMor(int cptRshMorIdx) {
        int position = Pos.atgaCptRshMor(cptRshMorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.CPT_RSH_MOR, Len.Fract.CPT_RSH_MOR);
    }

    public void setPrstzRidIni(int prstzRidIniIdx, AfDecimal prstzRidIni) {
        int position = Pos.atgaPrstzRidIni(prstzRidIniIdx - 1);
        writeDecimalAsPacked(position, prstzRidIni.copy());
    }

    /**Original name: ATGA-PRSTZ-RID-INI<br>*/
    public AfDecimal getPrstzRidIni(int prstzRidIniIdx) {
        int position = Pos.atgaPrstzRidIni(prstzRidIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_RID_INI, Len.Fract.PRSTZ_RID_INI);
    }

    public void setFlCarCont(int flCarContIdx, char flCarCont) {
        int position = Pos.atgaFlCarCont(flCarContIdx - 1);
        writeChar(position, flCarCont);
    }

    /**Original name: ATGA-FL-CAR-CONT<br>*/
    public char getFlCarCont(int flCarContIdx) {
        int position = Pos.atgaFlCarCont(flCarContIdx - 1);
        return readChar(position);
    }

    public void setBnsGiaLiqto(int bnsGiaLiqtoIdx, AfDecimal bnsGiaLiqto) {
        int position = Pos.atgaBnsGiaLiqto(bnsGiaLiqtoIdx - 1);
        writeDecimalAsPacked(position, bnsGiaLiqto.copy());
    }

    /**Original name: ATGA-BNS-GIA-LIQTO<br>*/
    public AfDecimal getBnsGiaLiqto(int bnsGiaLiqtoIdx) {
        int position = Pos.atgaBnsGiaLiqto(bnsGiaLiqtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.BNS_GIA_LIQTO, Len.Fract.BNS_GIA_LIQTO);
    }

    public void setImpBns(int impBnsIdx, AfDecimal impBns) {
        int position = Pos.atgaImpBns(impBnsIdx - 1);
        writeDecimalAsPacked(position, impBns.copy());
    }

    /**Original name: ATGA-IMP-BNS<br>*/
    public AfDecimal getImpBns(int impBnsIdx) {
        int position = Pos.atgaImpBns(impBnsIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_BNS, Len.Fract.IMP_BNS);
    }

    public void setCodDvs(int codDvsIdx, String codDvs) {
        int position = Pos.atgaCodDvs(codDvsIdx - 1);
        writeString(position, codDvs, Len.COD_DVS);
    }

    /**Original name: ATGA-COD-DVS<br>*/
    public String getCodDvs(int codDvsIdx) {
        int position = Pos.atgaCodDvs(codDvsIdx - 1);
        return readString(position, Len.COD_DVS);
    }

    public void setPrstzIniNewfis(int prstzIniNewfisIdx, AfDecimal prstzIniNewfis) {
        int position = Pos.atgaPrstzIniNewfis(prstzIniNewfisIdx - 1);
        writeDecimalAsPacked(position, prstzIniNewfis.copy());
    }

    /**Original name: ATGA-PRSTZ-INI-NEWFIS<br>*/
    public AfDecimal getPrstzIniNewfis(int prstzIniNewfisIdx) {
        int position = Pos.atgaPrstzIniNewfis(prstzIniNewfisIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI_NEWFIS, Len.Fract.PRSTZ_INI_NEWFIS);
    }

    public void setImpScon(int impSconIdx, AfDecimal impScon) {
        int position = Pos.atgaImpScon(impSconIdx - 1);
        writeDecimalAsPacked(position, impScon.copy());
    }

    /**Original name: ATGA-IMP-SCON<br>*/
    public AfDecimal getImpScon(int impSconIdx) {
        int position = Pos.atgaImpScon(impSconIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SCON, Len.Fract.IMP_SCON);
    }

    public void setAlqScon(int alqSconIdx, AfDecimal alqScon) {
        int position = Pos.atgaAlqScon(alqSconIdx - 1);
        writeDecimalAsPacked(position, alqScon.copy());
    }

    /**Original name: ATGA-ALQ-SCON<br>*/
    public AfDecimal getAlqScon(int alqSconIdx) {
        int position = Pos.atgaAlqScon(alqSconIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_SCON, Len.Fract.ALQ_SCON);
    }

    public void setImpCarAcq(int impCarAcqIdx, AfDecimal impCarAcq) {
        int position = Pos.atgaImpCarAcq(impCarAcqIdx - 1);
        writeDecimalAsPacked(position, impCarAcq.copy());
    }

    /**Original name: ATGA-IMP-CAR-ACQ<br>*/
    public AfDecimal getImpCarAcq(int impCarAcqIdx) {
        int position = Pos.atgaImpCarAcq(impCarAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_CAR_ACQ, Len.Fract.IMP_CAR_ACQ);
    }

    public void setImpCarInc(int impCarIncIdx, AfDecimal impCarInc) {
        int position = Pos.atgaImpCarInc(impCarIncIdx - 1);
        writeDecimalAsPacked(position, impCarInc.copy());
    }

    /**Original name: ATGA-IMP-CAR-INC<br>*/
    public AfDecimal getImpCarInc(int impCarIncIdx) {
        int position = Pos.atgaImpCarInc(impCarIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_CAR_INC, Len.Fract.IMP_CAR_INC);
    }

    public void setImpCarGest(int impCarGestIdx, AfDecimal impCarGest) {
        int position = Pos.atgaImpCarGest(impCarGestIdx - 1);
        writeDecimalAsPacked(position, impCarGest.copy());
    }

    /**Original name: ATGA-IMP-CAR-GEST<br>*/
    public AfDecimal getImpCarGest(int impCarGestIdx) {
        int position = Pos.atgaImpCarGest(impCarGestIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_CAR_GEST, Len.Fract.IMP_CAR_GEST);
    }

    public void setEtaAa1oAssto(int etaAa1oAsstoIdx, short etaAa1oAssto) {
        int position = Pos.atgaEtaAa1oAssto(etaAa1oAsstoIdx - 1);
        writeShortAsPacked(position, etaAa1oAssto, Len.Int.ETA_AA1O_ASSTO);
    }

    /**Original name: ATGA-ETA-AA-1O-ASSTO<br>*/
    public short getEtaAa1oAssto(int etaAa1oAsstoIdx) {
        int position = Pos.atgaEtaAa1oAssto(etaAa1oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_AA1O_ASSTO);
    }

    public void setEtaMm1oAssto(int etaMm1oAsstoIdx, short etaMm1oAssto) {
        int position = Pos.atgaEtaMm1oAssto(etaMm1oAsstoIdx - 1);
        writeShortAsPacked(position, etaMm1oAssto, Len.Int.ETA_MM1O_ASSTO);
    }

    /**Original name: ATGA-ETA-MM-1O-ASSTO<br>*/
    public short getEtaMm1oAssto(int etaMm1oAsstoIdx) {
        int position = Pos.atgaEtaMm1oAssto(etaMm1oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_MM1O_ASSTO);
    }

    public void setEtaAa2oAssto(int etaAa2oAsstoIdx, short etaAa2oAssto) {
        int position = Pos.atgaEtaAa2oAssto(etaAa2oAsstoIdx - 1);
        writeShortAsPacked(position, etaAa2oAssto, Len.Int.ETA_AA2O_ASSTO);
    }

    /**Original name: ATGA-ETA-AA-2O-ASSTO<br>*/
    public short getEtaAa2oAssto(int etaAa2oAsstoIdx) {
        int position = Pos.atgaEtaAa2oAssto(etaAa2oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_AA2O_ASSTO);
    }

    public void setEtaMm2oAssto(int etaMm2oAsstoIdx, short etaMm2oAssto) {
        int position = Pos.atgaEtaMm2oAssto(etaMm2oAsstoIdx - 1);
        writeShortAsPacked(position, etaMm2oAssto, Len.Int.ETA_MM2O_ASSTO);
    }

    /**Original name: ATGA-ETA-MM-2O-ASSTO<br>*/
    public short getEtaMm2oAssto(int etaMm2oAsstoIdx) {
        int position = Pos.atgaEtaMm2oAssto(etaMm2oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_MM2O_ASSTO);
    }

    public void setEtaAa3oAssto(int etaAa3oAsstoIdx, short etaAa3oAssto) {
        int position = Pos.atgaEtaAa3oAssto(etaAa3oAsstoIdx - 1);
        writeShortAsPacked(position, etaAa3oAssto, Len.Int.ETA_AA3O_ASSTO);
    }

    /**Original name: ATGA-ETA-AA-3O-ASSTO<br>*/
    public short getEtaAa3oAssto(int etaAa3oAsstoIdx) {
        int position = Pos.atgaEtaAa3oAssto(etaAa3oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_AA3O_ASSTO);
    }

    public void setEtaMm3oAssto(int etaMm3oAsstoIdx, short etaMm3oAssto) {
        int position = Pos.atgaEtaMm3oAssto(etaMm3oAsstoIdx - 1);
        writeShortAsPacked(position, etaMm3oAssto, Len.Int.ETA_MM3O_ASSTO);
    }

    /**Original name: ATGA-ETA-MM-3O-ASSTO<br>*/
    public short getEtaMm3oAssto(int etaMm3oAsstoIdx) {
        int position = Pos.atgaEtaMm3oAssto(etaMm3oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_MM3O_ASSTO);
    }

    public void setRendtoLrd(int rendtoLrdIdx, AfDecimal rendtoLrd) {
        int position = Pos.atgaRendtoLrd(rendtoLrdIdx - 1);
        writeDecimalAsPacked(position, rendtoLrd.copy());
    }

    /**Original name: ATGA-RENDTO-LRD<br>*/
    public AfDecimal getRendtoLrd(int rendtoLrdIdx) {
        int position = Pos.atgaRendtoLrd(rendtoLrdIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RENDTO_LRD, Len.Fract.RENDTO_LRD);
    }

    public void setPcRetr(int pcRetrIdx, AfDecimal pcRetr) {
        int position = Pos.atgaPcRetr(pcRetrIdx - 1);
        writeDecimalAsPacked(position, pcRetr.copy());
    }

    /**Original name: ATGA-PC-RETR<br>*/
    public AfDecimal getPcRetr(int pcRetrIdx) {
        int position = Pos.atgaPcRetr(pcRetrIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_RETR, Len.Fract.PC_RETR);
    }

    public void setRendtoRetr(int rendtoRetrIdx, AfDecimal rendtoRetr) {
        int position = Pos.atgaRendtoRetr(rendtoRetrIdx - 1);
        writeDecimalAsPacked(position, rendtoRetr.copy());
    }

    /**Original name: ATGA-RENDTO-RETR<br>*/
    public AfDecimal getRendtoRetr(int rendtoRetrIdx) {
        int position = Pos.atgaRendtoRetr(rendtoRetrIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RENDTO_RETR, Len.Fract.RENDTO_RETR);
    }

    public void setMinGarto(int minGartoIdx, AfDecimal minGarto) {
        int position = Pos.atgaMinGarto(minGartoIdx - 1);
        writeDecimalAsPacked(position, minGarto.copy());
    }

    /**Original name: ATGA-MIN-GARTO<br>*/
    public AfDecimal getMinGarto(int minGartoIdx) {
        int position = Pos.atgaMinGarto(minGartoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MIN_GARTO, Len.Fract.MIN_GARTO);
    }

    public void setMinTrnut(int minTrnutIdx, AfDecimal minTrnut) {
        int position = Pos.atgaMinTrnut(minTrnutIdx - 1);
        writeDecimalAsPacked(position, minTrnut.copy());
    }

    /**Original name: ATGA-MIN-TRNUT<br>*/
    public AfDecimal getMinTrnut(int minTrnutIdx) {
        int position = Pos.atgaMinTrnut(minTrnutIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MIN_TRNUT, Len.Fract.MIN_TRNUT);
    }

    public void setPreAttDiTrch(int preAttDiTrchIdx, AfDecimal preAttDiTrch) {
        int position = Pos.atgaPreAttDiTrch(preAttDiTrchIdx - 1);
        writeDecimalAsPacked(position, preAttDiTrch.copy());
    }

    /**Original name: ATGA-PRE-ATT-DI-TRCH<br>*/
    public AfDecimal getPreAttDiTrch(int preAttDiTrchIdx) {
        int position = Pos.atgaPreAttDiTrch(preAttDiTrchIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_ATT_DI_TRCH, Len.Fract.PRE_ATT_DI_TRCH);
    }

    public void setMatuEnd2000(int matuEnd2000Idx, AfDecimal matuEnd2000) {
        int position = Pos.atgaMatuEnd2000(matuEnd2000Idx - 1);
        writeDecimalAsPacked(position, matuEnd2000.copy());
    }

    /**Original name: ATGA-MATU-END2000<br>*/
    public AfDecimal getMatuEnd2000(int matuEnd2000Idx) {
        int position = Pos.atgaMatuEnd2000(matuEnd2000Idx - 1);
        return readPackedAsDecimal(position, Len.Int.MATU_END2000, Len.Fract.MATU_END2000);
    }

    public void setAbbTotIni(int abbTotIniIdx, AfDecimal abbTotIni) {
        int position = Pos.atgaAbbTotIni(abbTotIniIdx - 1);
        writeDecimalAsPacked(position, abbTotIni.copy());
    }

    /**Original name: ATGA-ABB-TOT-INI<br>*/
    public AfDecimal getAbbTotIni(int abbTotIniIdx) {
        int position = Pos.atgaAbbTotIni(abbTotIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ABB_TOT_INI, Len.Fract.ABB_TOT_INI);
    }

    public void setAbbTotUlt(int abbTotUltIdx, AfDecimal abbTotUlt) {
        int position = Pos.atgaAbbTotUlt(abbTotUltIdx - 1);
        writeDecimalAsPacked(position, abbTotUlt.copy());
    }

    /**Original name: ATGA-ABB-TOT-ULT<br>*/
    public AfDecimal getAbbTotUlt(int abbTotUltIdx) {
        int position = Pos.atgaAbbTotUlt(abbTotUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ABB_TOT_ULT, Len.Fract.ABB_TOT_ULT);
    }

    public void setAbbAnnuUlt(int abbAnnuUltIdx, AfDecimal abbAnnuUlt) {
        int position = Pos.atgaAbbAnnuUlt(abbAnnuUltIdx - 1);
        writeDecimalAsPacked(position, abbAnnuUlt.copy());
    }

    /**Original name: ATGA-ABB-ANNU-ULT<br>*/
    public AfDecimal getAbbAnnuUlt(int abbAnnuUltIdx) {
        int position = Pos.atgaAbbAnnuUlt(abbAnnuUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ABB_ANNU_ULT, Len.Fract.ABB_ANNU_ULT);
    }

    public void setDurAbb(int durAbbIdx, int durAbb) {
        int position = Pos.atgaDurAbb(durAbbIdx - 1);
        writeIntAsPacked(position, durAbb, Len.Int.DUR_ABB);
    }

    /**Original name: ATGA-DUR-ABB<br>*/
    public int getDurAbb(int durAbbIdx) {
        int position = Pos.atgaDurAbb(durAbbIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_ABB);
    }

    public void setTpAdegAbb(int tpAdegAbbIdx, char tpAdegAbb) {
        int position = Pos.atgaTpAdegAbb(tpAdegAbbIdx - 1);
        writeChar(position, tpAdegAbb);
    }

    /**Original name: ATGA-TP-ADEG-ABB<br>*/
    public char getTpAdegAbb(int tpAdegAbbIdx) {
        int position = Pos.atgaTpAdegAbb(tpAdegAbbIdx - 1);
        return readChar(position);
    }

    public void setModCalc(int modCalcIdx, String modCalc) {
        int position = Pos.atgaModCalc(modCalcIdx - 1);
        writeString(position, modCalc, Len.MOD_CALC);
    }

    /**Original name: ATGA-MOD-CALC<br>*/
    public String getModCalc(int modCalcIdx) {
        int position = Pos.atgaModCalc(modCalcIdx - 1);
        return readString(position, Len.MOD_CALC);
    }

    public void setImpAz(int impAzIdx, AfDecimal impAz) {
        int position = Pos.atgaImpAz(impAzIdx - 1);
        writeDecimalAsPacked(position, impAz.copy());
    }

    /**Original name: ATGA-IMP-AZ<br>*/
    public AfDecimal getImpAz(int impAzIdx) {
        int position = Pos.atgaImpAz(impAzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_AZ, Len.Fract.IMP_AZ);
    }

    public void setImpAder(int impAderIdx, AfDecimal impAder) {
        int position = Pos.atgaImpAder(impAderIdx - 1);
        writeDecimalAsPacked(position, impAder.copy());
    }

    /**Original name: ATGA-IMP-ADER<br>*/
    public AfDecimal getImpAder(int impAderIdx) {
        int position = Pos.atgaImpAder(impAderIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_ADER, Len.Fract.IMP_ADER);
    }

    public void setImpTfr(int impTfrIdx, AfDecimal impTfr) {
        int position = Pos.atgaImpTfr(impTfrIdx - 1);
        writeDecimalAsPacked(position, impTfr.copy());
    }

    /**Original name: ATGA-IMP-TFR<br>*/
    public AfDecimal getImpTfr(int impTfrIdx) {
        int position = Pos.atgaImpTfr(impTfrIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_TFR, Len.Fract.IMP_TFR);
    }

    public void setImpVolo(int impVoloIdx, AfDecimal impVolo) {
        int position = Pos.atgaImpVolo(impVoloIdx - 1);
        writeDecimalAsPacked(position, impVolo.copy());
    }

    /**Original name: ATGA-IMP-VOLO<br>*/
    public AfDecimal getImpVolo(int impVoloIdx) {
        int position = Pos.atgaImpVolo(impVoloIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_VOLO, Len.Fract.IMP_VOLO);
    }

    public void setVisEnd2000(int visEnd2000Idx, AfDecimal visEnd2000) {
        int position = Pos.atgaVisEnd2000(visEnd2000Idx - 1);
        writeDecimalAsPacked(position, visEnd2000.copy());
    }

    /**Original name: ATGA-VIS-END2000<br>*/
    public AfDecimal getVisEnd2000(int visEnd2000Idx) {
        int position = Pos.atgaVisEnd2000(visEnd2000Idx - 1);
        return readPackedAsDecimal(position, Len.Int.VIS_END2000, Len.Fract.VIS_END2000);
    }

    public void setDtVldtProd(int dtVldtProdIdx, int dtVldtProd) {
        int position = Pos.atgaDtVldtProd(dtVldtProdIdx - 1);
        writeIntAsPacked(position, dtVldtProd, Len.Int.DT_VLDT_PROD);
    }

    /**Original name: ATGA-DT-VLDT-PROD<br>*/
    public int getDtVldtProd(int dtVldtProdIdx) {
        int position = Pos.atgaDtVldtProd(dtVldtProdIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_VLDT_PROD);
    }

    public void setDtIniValTar(int dtIniValTarIdx, int dtIniValTar) {
        int position = Pos.atgaDtIniValTar(dtIniValTarIdx - 1);
        writeIntAsPacked(position, dtIniValTar, Len.Int.DT_INI_VAL_TAR);
    }

    /**Original name: ATGA-DT-INI-VAL-TAR<br>*/
    public int getDtIniValTar(int dtIniValTarIdx) {
        int position = Pos.atgaDtIniValTar(dtIniValTarIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_VAL_TAR);
    }

    public void setImpbVisEnd2000(int impbVisEnd2000Idx, AfDecimal impbVisEnd2000) {
        int position = Pos.atgaImpbVisEnd2000(impbVisEnd2000Idx - 1);
        writeDecimalAsPacked(position, impbVisEnd2000.copy());
    }

    /**Original name: ATGA-IMPB-VIS-END2000<br>*/
    public AfDecimal getImpbVisEnd2000(int impbVisEnd2000Idx) {
        int position = Pos.atgaImpbVisEnd2000(impbVisEnd2000Idx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_VIS_END2000, Len.Fract.IMPB_VIS_END2000);
    }

    public void setRenIniTsTec0(int renIniTsTec0Idx, AfDecimal renIniTsTec0) {
        int position = Pos.atgaRenIniTsTec0(renIniTsTec0Idx - 1);
        writeDecimalAsPacked(position, renIniTsTec0.copy());
    }

    /**Original name: ATGA-REN-INI-TS-TEC-0<br>*/
    public AfDecimal getRenIniTsTec0(int renIniTsTec0Idx) {
        int position = Pos.atgaRenIniTsTec0(renIniTsTec0Idx - 1);
        return readPackedAsDecimal(position, Len.Int.REN_INI_TS_TEC0, Len.Fract.REN_INI_TS_TEC0);
    }

    public void setPcRipPre(int pcRipPreIdx, AfDecimal pcRipPre) {
        int position = Pos.atgaPcRipPre(pcRipPreIdx - 1);
        writeDecimalAsPacked(position, pcRipPre.copy());
    }

    /**Original name: ATGA-PC-RIP-PRE<br>*/
    public AfDecimal getPcRipPre(int pcRipPreIdx) {
        int position = Pos.atgaPcRipPre(pcRipPreIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_RIP_PRE, Len.Fract.PC_RIP_PRE);
    }

    public void setFlImportiForz(int flImportiForzIdx, char flImportiForz) {
        int position = Pos.atgaFlImportiForz(flImportiForzIdx - 1);
        writeChar(position, flImportiForz);
    }

    /**Original name: ATGA-FL-IMPORTI-FORZ<br>*/
    public char getFlImportiForz(int flImportiForzIdx) {
        int position = Pos.atgaFlImportiForz(flImportiForzIdx - 1);
        return readChar(position);
    }

    public void setPrstzIniNforz(int prstzIniNforzIdx, AfDecimal prstzIniNforz) {
        int position = Pos.atgaPrstzIniNforz(prstzIniNforzIdx - 1);
        writeDecimalAsPacked(position, prstzIniNforz.copy());
    }

    /**Original name: ATGA-PRSTZ-INI-NFORZ<br>*/
    public AfDecimal getPrstzIniNforz(int prstzIniNforzIdx) {
        int position = Pos.atgaPrstzIniNforz(prstzIniNforzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI_NFORZ, Len.Fract.PRSTZ_INI_NFORZ);
    }

    public void setVisEnd2000Nforz(int visEnd2000NforzIdx, AfDecimal visEnd2000Nforz) {
        int position = Pos.atgaVisEnd2000Nforz(visEnd2000NforzIdx - 1);
        writeDecimalAsPacked(position, visEnd2000Nforz.copy());
    }

    /**Original name: ATGA-VIS-END2000-NFORZ<br>*/
    public AfDecimal getVisEnd2000Nforz(int visEnd2000NforzIdx) {
        int position = Pos.atgaVisEnd2000Nforz(visEnd2000NforzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VIS_END2000_NFORZ, Len.Fract.VIS_END2000_NFORZ);
    }

    public void setIntrMora(int intrMoraIdx, AfDecimal intrMora) {
        int position = Pos.atgaIntrMora(intrMoraIdx - 1);
        writeDecimalAsPacked(position, intrMora.copy());
    }

    /**Original name: ATGA-INTR-MORA<br>*/
    public AfDecimal getIntrMora(int intrMoraIdx) {
        int position = Pos.atgaIntrMora(intrMoraIdx - 1);
        return readPackedAsDecimal(position, Len.Int.INTR_MORA, Len.Fract.INTR_MORA);
    }

    public void setManfeeAntic(int manfeeAnticIdx, AfDecimal manfeeAntic) {
        int position = Pos.atgaManfeeAntic(manfeeAnticIdx - 1);
        writeDecimalAsPacked(position, manfeeAntic.copy());
    }

    /**Original name: ATGA-MANFEE-ANTIC<br>*/
    public AfDecimal getManfeeAntic(int manfeeAnticIdx) {
        int position = Pos.atgaManfeeAntic(manfeeAnticIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MANFEE_ANTIC, Len.Fract.MANFEE_ANTIC);
    }

    public void setManfeeRicor(int manfeeRicorIdx, AfDecimal manfeeRicor) {
        int position = Pos.atgaManfeeRicor(manfeeRicorIdx - 1);
        writeDecimalAsPacked(position, manfeeRicor.copy());
    }

    /**Original name: ATGA-MANFEE-RICOR<br>*/
    public AfDecimal getManfeeRicor(int manfeeRicorIdx) {
        int position = Pos.atgaManfeeRicor(manfeeRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MANFEE_RICOR, Len.Fract.MANFEE_RICOR);
    }

    public void setPreUniRivto(int preUniRivtoIdx, AfDecimal preUniRivto) {
        int position = Pos.atgaPreUniRivto(preUniRivtoIdx - 1);
        writeDecimalAsPacked(position, preUniRivto.copy());
    }

    /**Original name: ATGA-PRE-UNI-RIVTO<br>*/
    public AfDecimal getPreUniRivto(int preUniRivtoIdx) {
        int position = Pos.atgaPreUniRivto(preUniRivtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_UNI_RIVTO, Len.Fract.PRE_UNI_RIVTO);
    }

    public void setProv1aaAcq(int prov1aaAcqIdx, AfDecimal prov1aaAcq) {
        int position = Pos.atgaProv1aaAcq(prov1aaAcqIdx - 1);
        writeDecimalAsPacked(position, prov1aaAcq.copy());
    }

    /**Original name: ATGA-PROV-1AA-ACQ<br>*/
    public AfDecimal getProv1aaAcq(int prov1aaAcqIdx) {
        int position = Pos.atgaProv1aaAcq(prov1aaAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV1AA_ACQ, Len.Fract.PROV1AA_ACQ);
    }

    public void setProv2aaAcq(int prov2aaAcqIdx, AfDecimal prov2aaAcq) {
        int position = Pos.atgaProv2aaAcq(prov2aaAcqIdx - 1);
        writeDecimalAsPacked(position, prov2aaAcq.copy());
    }

    /**Original name: ATGA-PROV-2AA-ACQ<br>*/
    public AfDecimal getProv2aaAcq(int prov2aaAcqIdx) {
        int position = Pos.atgaProv2aaAcq(prov2aaAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV2AA_ACQ, Len.Fract.PROV2AA_ACQ);
    }

    public void setProvRicor(int provRicorIdx, AfDecimal provRicor) {
        int position = Pos.atgaProvRicor(provRicorIdx - 1);
        writeDecimalAsPacked(position, provRicor.copy());
    }

    /**Original name: ATGA-PROV-RICOR<br>*/
    public AfDecimal getProvRicor(int provRicorIdx) {
        int position = Pos.atgaProvRicor(provRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV_RICOR, Len.Fract.PROV_RICOR);
    }

    public void setProvInc(int provIncIdx, AfDecimal provInc) {
        int position = Pos.atgaProvInc(provIncIdx - 1);
        writeDecimalAsPacked(position, provInc.copy());
    }

    /**Original name: ATGA-PROV-INC<br>*/
    public AfDecimal getProvInc(int provIncIdx) {
        int position = Pos.atgaProvInc(provIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV_INC, Len.Fract.PROV_INC);
    }

    public void setAlqProvAcq(int alqProvAcqIdx, AfDecimal alqProvAcq) {
        int position = Pos.atgaAlqProvAcq(alqProvAcqIdx - 1);
        writeDecimalAsPacked(position, alqProvAcq.copy());
    }

    /**Original name: ATGA-ALQ-PROV-ACQ<br>*/
    public AfDecimal getAlqProvAcq(int alqProvAcqIdx) {
        int position = Pos.atgaAlqProvAcq(alqProvAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_PROV_ACQ, Len.Fract.ALQ_PROV_ACQ);
    }

    public void setAlqProvInc(int alqProvIncIdx, AfDecimal alqProvInc) {
        int position = Pos.atgaAlqProvInc(alqProvIncIdx - 1);
        writeDecimalAsPacked(position, alqProvInc.copy());
    }

    /**Original name: ATGA-ALQ-PROV-INC<br>*/
    public AfDecimal getAlqProvInc(int alqProvIncIdx) {
        int position = Pos.atgaAlqProvInc(alqProvIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_PROV_INC, Len.Fract.ALQ_PROV_INC);
    }

    public void setAlqProvRicor(int alqProvRicorIdx, AfDecimal alqProvRicor) {
        int position = Pos.atgaAlqProvRicor(alqProvRicorIdx - 1);
        writeDecimalAsPacked(position, alqProvRicor.copy());
    }

    /**Original name: ATGA-ALQ-PROV-RICOR<br>*/
    public AfDecimal getAlqProvRicor(int alqProvRicorIdx) {
        int position = Pos.atgaAlqProvRicor(alqProvRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_PROV_RICOR, Len.Fract.ALQ_PROV_RICOR);
    }

    public void setImpbProvAcq(int impbProvAcqIdx, AfDecimal impbProvAcq) {
        int position = Pos.atgaImpbProvAcq(impbProvAcqIdx - 1);
        writeDecimalAsPacked(position, impbProvAcq.copy());
    }

    /**Original name: ATGA-IMPB-PROV-ACQ<br>*/
    public AfDecimal getImpbProvAcq(int impbProvAcqIdx) {
        int position = Pos.atgaImpbProvAcq(impbProvAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_PROV_ACQ, Len.Fract.IMPB_PROV_ACQ);
    }

    public void setImpbProvInc(int impbProvIncIdx, AfDecimal impbProvInc) {
        int position = Pos.atgaImpbProvInc(impbProvIncIdx - 1);
        writeDecimalAsPacked(position, impbProvInc.copy());
    }

    /**Original name: ATGA-IMPB-PROV-INC<br>*/
    public AfDecimal getImpbProvInc(int impbProvIncIdx) {
        int position = Pos.atgaImpbProvInc(impbProvIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_PROV_INC, Len.Fract.IMPB_PROV_INC);
    }

    public void setImpbProvRicor(int impbProvRicorIdx, AfDecimal impbProvRicor) {
        int position = Pos.atgaImpbProvRicor(impbProvRicorIdx - 1);
        writeDecimalAsPacked(position, impbProvRicor.copy());
    }

    /**Original name: ATGA-IMPB-PROV-RICOR<br>*/
    public AfDecimal getImpbProvRicor(int impbProvRicorIdx) {
        int position = Pos.atgaImpbProvRicor(impbProvRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_PROV_RICOR, Len.Fract.IMPB_PROV_RICOR);
    }

    public void setFlProvForz(int flProvForzIdx, char flProvForz) {
        int position = Pos.atgaFlProvForz(flProvForzIdx - 1);
        writeChar(position, flProvForz);
    }

    /**Original name: ATGA-FL-PROV-FORZ<br>*/
    public char getFlProvForz(int flProvForzIdx) {
        int position = Pos.atgaFlProvForz(flProvForzIdx - 1);
        return readChar(position);
    }

    public void setPrstzAggIni(int prstzAggIniIdx, AfDecimal prstzAggIni) {
        int position = Pos.atgaPrstzAggIni(prstzAggIniIdx - 1);
        writeDecimalAsPacked(position, prstzAggIni.copy());
    }

    /**Original name: ATGA-PRSTZ-AGG-INI<br>*/
    public AfDecimal getPrstzAggIni(int prstzAggIniIdx) {
        int position = Pos.atgaPrstzAggIni(prstzAggIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_AGG_INI, Len.Fract.PRSTZ_AGG_INI);
    }

    public void setIncrPre(int incrPreIdx, AfDecimal incrPre) {
        int position = Pos.atgaIncrPre(incrPreIdx - 1);
        writeDecimalAsPacked(position, incrPre.copy());
    }

    /**Original name: ATGA-INCR-PRE<br>*/
    public AfDecimal getIncrPre(int incrPreIdx) {
        int position = Pos.atgaIncrPre(incrPreIdx - 1);
        return readPackedAsDecimal(position, Len.Int.INCR_PRE, Len.Fract.INCR_PRE);
    }

    public void setIncrPrstz(int incrPrstzIdx, AfDecimal incrPrstz) {
        int position = Pos.atgaIncrPrstz(incrPrstzIdx - 1);
        writeDecimalAsPacked(position, incrPrstz.copy());
    }

    /**Original name: ATGA-INCR-PRSTZ<br>*/
    public AfDecimal getIncrPrstz(int incrPrstzIdx) {
        int position = Pos.atgaIncrPrstz(incrPrstzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.INCR_PRSTZ, Len.Fract.INCR_PRSTZ);
    }

    public void setDtUltAdegPrePr(int dtUltAdegPrePrIdx, int dtUltAdegPrePr) {
        int position = Pos.atgaDtUltAdegPrePr(dtUltAdegPrePrIdx - 1);
        writeIntAsPacked(position, dtUltAdegPrePr, Len.Int.DT_ULT_ADEG_PRE_PR);
    }

    /**Original name: ATGA-DT-ULT-ADEG-PRE-PR<br>*/
    public int getDtUltAdegPrePr(int dtUltAdegPrePrIdx) {
        int position = Pos.atgaDtUltAdegPrePr(dtUltAdegPrePrIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_ULT_ADEG_PRE_PR);
    }

    public void setPrstzAggUlt(int prstzAggUltIdx, AfDecimal prstzAggUlt) {
        int position = Pos.atgaPrstzAggUlt(prstzAggUltIdx - 1);
        writeDecimalAsPacked(position, prstzAggUlt.copy());
    }

    /**Original name: ATGA-PRSTZ-AGG-ULT<br>*/
    public AfDecimal getPrstzAggUlt(int prstzAggUltIdx) {
        int position = Pos.atgaPrstzAggUlt(prstzAggUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_AGG_ULT, Len.Fract.PRSTZ_AGG_ULT);
    }

    public void setTsRivalNet(int tsRivalNetIdx, AfDecimal tsRivalNet) {
        int position = Pos.atgaTsRivalNet(tsRivalNetIdx - 1);
        writeDecimalAsPacked(position, tsRivalNet.copy());
    }

    /**Original name: ATGA-TS-RIVAL-NET<br>*/
    public AfDecimal getTsRivalNet(int tsRivalNetIdx) {
        int position = Pos.atgaTsRivalNet(tsRivalNetIdx - 1);
        return readPackedAsDecimal(position, Len.Int.TS_RIVAL_NET, Len.Fract.TS_RIVAL_NET);
    }

    public void setPrePattuito(int prePattuitoIdx, AfDecimal prePattuito) {
        int position = Pos.atgaPrePattuito(prePattuitoIdx - 1);
        writeDecimalAsPacked(position, prePattuito.copy());
    }

    /**Original name: ATGA-PRE-PATTUITO<br>*/
    public AfDecimal getPrePattuito(int prePattuitoIdx) {
        int position = Pos.atgaPrePattuito(prePattuitoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_PATTUITO, Len.Fract.PRE_PATTUITO);
    }

    public void setTpRival(int tpRivalIdx, String tpRival) {
        int position = Pos.atgaTpRival(tpRivalIdx - 1);
        writeString(position, tpRival, Len.TP_RIVAL);
    }

    /**Original name: ATGA-TP-RIVAL<br>*/
    public String getTpRival(int tpRivalIdx) {
        int position = Pos.atgaTpRival(tpRivalIdx - 1);
        return readString(position, Len.TP_RIVAL);
    }

    public void setRisMat(int risMatIdx, AfDecimal risMat) {
        int position = Pos.atgaRisMat(risMatIdx - 1);
        writeDecimalAsPacked(position, risMat.copy());
    }

    /**Original name: ATGA-RIS-MAT<br>*/
    public AfDecimal getRisMat(int risMatIdx) {
        int position = Pos.atgaRisMat(risMatIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RIS_MAT, Len.Fract.RIS_MAT);
    }

    public void setCptMinScad(int cptMinScadIdx, AfDecimal cptMinScad) {
        int position = Pos.atgaCptMinScad(cptMinScadIdx - 1);
        writeDecimalAsPacked(position, cptMinScad.copy());
    }

    /**Original name: ATGA-CPT-MIN-SCAD<br>*/
    public AfDecimal getCptMinScad(int cptMinScadIdx) {
        int position = Pos.atgaCptMinScad(cptMinScadIdx - 1);
        return readPackedAsDecimal(position, Len.Int.CPT_MIN_SCAD, Len.Fract.CPT_MIN_SCAD);
    }

    public void setCommisGest(int commisGestIdx, AfDecimal commisGest) {
        int position = Pos.atgaCommisGest(commisGestIdx - 1);
        writeDecimalAsPacked(position, commisGest.copy());
    }

    /**Original name: ATGA-COMMIS-GEST<br>*/
    public AfDecimal getCommisGest(int commisGestIdx) {
        int position = Pos.atgaCommisGest(commisGestIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COMMIS_GEST, Len.Fract.COMMIS_GEST);
    }

    public void setTpManfeeAppl(int tpManfeeApplIdx, String tpManfeeAppl) {
        int position = Pos.atgaTpManfeeAppl(tpManfeeApplIdx - 1);
        writeString(position, tpManfeeAppl, Len.TP_MANFEE_APPL);
    }

    /**Original name: ATGA-TP-MANFEE-APPL<br>*/
    public String getTpManfeeAppl(int tpManfeeApplIdx) {
        int position = Pos.atgaTpManfeeAppl(tpManfeeApplIdx - 1);
        return readString(position, Len.TP_MANFEE_APPL);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.atgaDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: ATGA-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.atgaDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.atgaDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: ATGA-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.atgaDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.atgaDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: ATGA-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.atgaDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.atgaDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: ATGA-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.atgaDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.atgaDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: ATGA-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.atgaDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.atgaDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: ATGA-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.atgaDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.atgaDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: ATGA-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.atgaDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setPcCommisGest(int pcCommisGestIdx, AfDecimal pcCommisGest) {
        int position = Pos.atgaPcCommisGest(pcCommisGestIdx - 1);
        writeDecimalAsPacked(position, pcCommisGest.copy());
    }

    /**Original name: ATGA-PC-COMMIS-GEST<br>*/
    public AfDecimal getPcCommisGest(int pcCommisGestIdx) {
        int position = Pos.atgaPcCommisGest(pcCommisGestIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_COMMIS_GEST, Len.Fract.PC_COMMIS_GEST);
    }

    public void setNumGgRival(int numGgRivalIdx, int numGgRival) {
        int position = Pos.atgaNumGgRival(numGgRivalIdx - 1);
        writeIntAsPacked(position, numGgRival, Len.Int.NUM_GG_RIVAL);
    }

    /**Original name: ATGA-NUM-GG-RIVAL<br>*/
    public int getNumGgRival(int numGgRivalIdx) {
        int position = Pos.atgaNumGgRival(numGgRivalIdx - 1);
        return readPackedAsInt(position, Len.Int.NUM_GG_RIVAL);
    }

    public void setImpTrasfe(int impTrasfeIdx, AfDecimal impTrasfe) {
        int position = Pos.atgaImpTrasfe(impTrasfeIdx - 1);
        writeDecimalAsPacked(position, impTrasfe.copy());
    }

    /**Original name: ATGA-IMP-TRASFE<br>*/
    public AfDecimal getImpTrasfe(int impTrasfeIdx) {
        int position = Pos.atgaImpTrasfe(impTrasfeIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_TRASFE, Len.Fract.IMP_TRASFE);
    }

    public void setImpTfrStrc(int impTfrStrcIdx, AfDecimal impTfrStrc) {
        int position = Pos.atgaImpTfrStrc(impTfrStrcIdx - 1);
        writeDecimalAsPacked(position, impTfrStrc.copy());
    }

    /**Original name: ATGA-IMP-TFR-STRC<br>*/
    public AfDecimal getImpTfrStrc(int impTfrStrcIdx) {
        int position = Pos.atgaImpTfrStrc(impTfrStrcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_TFR_STRC, Len.Fract.IMP_TFR_STRC);
    }

    public void setAcqExp(int acqExpIdx, AfDecimal acqExp) {
        int position = Pos.atgaAcqExp(acqExpIdx - 1);
        writeDecimalAsPacked(position, acqExp.copy());
    }

    /**Original name: ATGA-ACQ-EXP<br>*/
    public AfDecimal getAcqExp(int acqExpIdx) {
        int position = Pos.atgaAcqExp(acqExpIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ACQ_EXP, Len.Fract.ACQ_EXP);
    }

    public void setRemunAss(int remunAssIdx, AfDecimal remunAss) {
        int position = Pos.atgaRemunAss(remunAssIdx - 1);
        writeDecimalAsPacked(position, remunAss.copy());
    }

    /**Original name: ATGA-REMUN-ASS<br>*/
    public AfDecimal getRemunAss(int remunAssIdx) {
        int position = Pos.atgaRemunAss(remunAssIdx - 1);
        return readPackedAsDecimal(position, Len.Int.REMUN_ASS, Len.Fract.REMUN_ASS);
    }

    public void setCommisInter(int commisInterIdx, AfDecimal commisInter) {
        int position = Pos.atgaCommisInter(commisInterIdx - 1);
        writeDecimalAsPacked(position, commisInter.copy());
    }

    /**Original name: ATGA-COMMIS-INTER<br>*/
    public AfDecimal getCommisInter(int commisInterIdx) {
        int position = Pos.atgaCommisInter(commisInterIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COMMIS_INTER, Len.Fract.COMMIS_INTER);
    }

    public void setAlqRemunAss(int alqRemunAssIdx, AfDecimal alqRemunAss) {
        int position = Pos.atgaAlqRemunAss(alqRemunAssIdx - 1);
        writeDecimalAsPacked(position, alqRemunAss.copy());
    }

    /**Original name: ATGA-ALQ-REMUN-ASS<br>*/
    public AfDecimal getAlqRemunAss(int alqRemunAssIdx) {
        int position = Pos.atgaAlqRemunAss(alqRemunAssIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_REMUN_ASS, Len.Fract.ALQ_REMUN_ASS);
    }

    public void setAlqCommisInter(int alqCommisInterIdx, AfDecimal alqCommisInter) {
        int position = Pos.atgaAlqCommisInter(alqCommisInterIdx - 1);
        writeDecimalAsPacked(position, alqCommisInter.copy());
    }

    /**Original name: ATGA-ALQ-COMMIS-INTER<br>*/
    public AfDecimal getAlqCommisInter(int alqCommisInterIdx) {
        int position = Pos.atgaAlqCommisInter(alqCommisInterIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_COMMIS_INTER, Len.Fract.ALQ_COMMIS_INTER);
    }

    public void setImpbRemunAss(int impbRemunAssIdx, AfDecimal impbRemunAss) {
        int position = Pos.atgaImpbRemunAss(impbRemunAssIdx - 1);
        writeDecimalAsPacked(position, impbRemunAss.copy());
    }

    /**Original name: ATGA-IMPB-REMUN-ASS<br>*/
    public AfDecimal getImpbRemunAss(int impbRemunAssIdx) {
        int position = Pos.atgaImpbRemunAss(impbRemunAssIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_REMUN_ASS, Len.Fract.IMPB_REMUN_ASS);
    }

    public void setImpbCommisInter(int impbCommisInterIdx, AfDecimal impbCommisInter) {
        int position = Pos.atgaImpbCommisInter(impbCommisInterIdx - 1);
        writeDecimalAsPacked(position, impbCommisInter.copy());
    }

    /**Original name: ATGA-IMPB-COMMIS-INTER<br>*/
    public AfDecimal getImpbCommisInter(int impbCommisInterIdx) {
        int position = Pos.atgaImpbCommisInter(impbCommisInterIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_COMMIS_INTER, Len.Fract.IMPB_COMMIS_INTER);
    }

    public void setCosRunAssva(int cosRunAssvaIdx, AfDecimal cosRunAssva) {
        int position = Pos.atgaCosRunAssva(cosRunAssvaIdx - 1);
        writeDecimalAsPacked(position, cosRunAssva.copy());
    }

    /**Original name: ATGA-COS-RUN-ASSVA<br>*/
    public AfDecimal getCosRunAssva(int cosRunAssvaIdx) {
        int position = Pos.atgaCosRunAssva(cosRunAssvaIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COS_RUN_ASSVA, Len.Fract.COS_RUN_ASSVA);
    }

    public void setCosRunAssvaIdc(int cosRunAssvaIdcIdx, AfDecimal cosRunAssvaIdc) {
        int position = Pos.atgaCosRunAssvaIdc(cosRunAssvaIdcIdx - 1);
        writeDecimalAsPacked(position, cosRunAssvaIdc.copy());
    }

    /**Original name: ATGA-COS-RUN-ASSVA-IDC<br>*/
    public AfDecimal getCosRunAssvaIdc(int cosRunAssvaIdcIdx) {
        int position = Pos.atgaCosRunAssvaIdc(cosRunAssvaIdcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COS_RUN_ASSVA_IDC, Len.Fract.COS_RUN_ASSVA_IDC);
    }

    public void setRestoTab(String restoTab) {
        writeString(Pos.RESTO_TAB, restoTab, Len.RESTO_TAB);
    }

    /**Original name: ATGA-RESTO-TAB<br>*/
    public String getRestoTab() {
        return readString(Pos.RESTO_TAB, Len.RESTO_TAB);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ATGA_TAB = 1;
        public static final int ATGA_TAB_R = 1;
        public static final int FLR1 = ATGA_TAB_R;
        public static final int RESTO_TAB = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int atgaTabTran(int idx) {
            return ATGA_TAB + idx * Len.TAB_TRAN;
        }

        public static int atgaStatus(int idx) {
            return atgaTabTran(idx);
        }

        public static int atgaIdPtf(int idx) {
            return atgaStatus(idx) + Len.STATUS;
        }

        public static int atgaDati(int idx) {
            return atgaIdPtf(idx) + Len.ID_PTF;
        }

        public static int atgaIdTrchDiGar(int idx) {
            return atgaDati(idx);
        }

        public static int atgaIdGar(int idx) {
            return atgaIdTrchDiGar(idx) + Len.ID_TRCH_DI_GAR;
        }

        public static int atgaIdAdes(int idx) {
            return atgaIdGar(idx) + Len.ID_GAR;
        }

        public static int atgaIdPoli(int idx) {
            return atgaIdAdes(idx) + Len.ID_ADES;
        }

        public static int atgaIdMoviCrz(int idx) {
            return atgaIdPoli(idx) + Len.ID_POLI;
        }

        public static int atgaIdMoviChiu(int idx) {
            return atgaIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int atgaIdMoviChiuNull(int idx) {
            return atgaIdMoviChiu(idx);
        }

        public static int atgaDtIniEff(int idx) {
            return atgaIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int atgaDtEndEff(int idx) {
            return atgaDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int atgaCodCompAnia(int idx) {
            return atgaDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int atgaDtDecor(int idx) {
            return atgaCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int atgaDtScad(int idx) {
            return atgaDtDecor(idx) + Len.DT_DECOR;
        }

        public static int atgaDtScadNull(int idx) {
            return atgaDtScad(idx);
        }

        public static int atgaIbOgg(int idx) {
            return atgaDtScad(idx) + Len.DT_SCAD;
        }

        public static int atgaIbOggNull(int idx) {
            return atgaIbOgg(idx);
        }

        public static int atgaTpRgmFisc(int idx) {
            return atgaIbOgg(idx) + Len.IB_OGG;
        }

        public static int atgaDtEmis(int idx) {
            return atgaTpRgmFisc(idx) + Len.TP_RGM_FISC;
        }

        public static int atgaDtEmisNull(int idx) {
            return atgaDtEmis(idx);
        }

        public static int atgaTpTrch(int idx) {
            return atgaDtEmis(idx) + Len.DT_EMIS;
        }

        public static int atgaDurAa(int idx) {
            return atgaTpTrch(idx) + Len.TP_TRCH;
        }

        public static int atgaDurAaNull(int idx) {
            return atgaDurAa(idx);
        }

        public static int atgaDurMm(int idx) {
            return atgaDurAa(idx) + Len.DUR_AA;
        }

        public static int atgaDurMmNull(int idx) {
            return atgaDurMm(idx);
        }

        public static int atgaDurGg(int idx) {
            return atgaDurMm(idx) + Len.DUR_MM;
        }

        public static int atgaDurGgNull(int idx) {
            return atgaDurGg(idx);
        }

        public static int atgaPreCasoMor(int idx) {
            return atgaDurGg(idx) + Len.DUR_GG;
        }

        public static int atgaPreCasoMorNull(int idx) {
            return atgaPreCasoMor(idx);
        }

        public static int atgaPcIntrRiat(int idx) {
            return atgaPreCasoMor(idx) + Len.PRE_CASO_MOR;
        }

        public static int atgaPcIntrRiatNull(int idx) {
            return atgaPcIntrRiat(idx);
        }

        public static int atgaImpBnsAntic(int idx) {
            return atgaPcIntrRiat(idx) + Len.PC_INTR_RIAT;
        }

        public static int atgaImpBnsAnticNull(int idx) {
            return atgaImpBnsAntic(idx);
        }

        public static int atgaPreIniNet(int idx) {
            return atgaImpBnsAntic(idx) + Len.IMP_BNS_ANTIC;
        }

        public static int atgaPreIniNetNull(int idx) {
            return atgaPreIniNet(idx);
        }

        public static int atgaPrePpIni(int idx) {
            return atgaPreIniNet(idx) + Len.PRE_INI_NET;
        }

        public static int atgaPrePpIniNull(int idx) {
            return atgaPrePpIni(idx);
        }

        public static int atgaPrePpUlt(int idx) {
            return atgaPrePpIni(idx) + Len.PRE_PP_INI;
        }

        public static int atgaPrePpUltNull(int idx) {
            return atgaPrePpUlt(idx);
        }

        public static int atgaPreTariIni(int idx) {
            return atgaPrePpUlt(idx) + Len.PRE_PP_ULT;
        }

        public static int atgaPreTariIniNull(int idx) {
            return atgaPreTariIni(idx);
        }

        public static int atgaPreTariUlt(int idx) {
            return atgaPreTariIni(idx) + Len.PRE_TARI_INI;
        }

        public static int atgaPreTariUltNull(int idx) {
            return atgaPreTariUlt(idx);
        }

        public static int atgaPreInvrioIni(int idx) {
            return atgaPreTariUlt(idx) + Len.PRE_TARI_ULT;
        }

        public static int atgaPreInvrioIniNull(int idx) {
            return atgaPreInvrioIni(idx);
        }

        public static int atgaPreInvrioUlt(int idx) {
            return atgaPreInvrioIni(idx) + Len.PRE_INVRIO_INI;
        }

        public static int atgaPreInvrioUltNull(int idx) {
            return atgaPreInvrioUlt(idx);
        }

        public static int atgaPreRivto(int idx) {
            return atgaPreInvrioUlt(idx) + Len.PRE_INVRIO_ULT;
        }

        public static int atgaPreRivtoNull(int idx) {
            return atgaPreRivto(idx);
        }

        public static int atgaImpSoprProf(int idx) {
            return atgaPreRivto(idx) + Len.PRE_RIVTO;
        }

        public static int atgaImpSoprProfNull(int idx) {
            return atgaImpSoprProf(idx);
        }

        public static int atgaImpSoprSan(int idx) {
            return atgaImpSoprProf(idx) + Len.IMP_SOPR_PROF;
        }

        public static int atgaImpSoprSanNull(int idx) {
            return atgaImpSoprSan(idx);
        }

        public static int atgaImpSoprSpo(int idx) {
            return atgaImpSoprSan(idx) + Len.IMP_SOPR_SAN;
        }

        public static int atgaImpSoprSpoNull(int idx) {
            return atgaImpSoprSpo(idx);
        }

        public static int atgaImpSoprTec(int idx) {
            return atgaImpSoprSpo(idx) + Len.IMP_SOPR_SPO;
        }

        public static int atgaImpSoprTecNull(int idx) {
            return atgaImpSoprTec(idx);
        }

        public static int atgaImpAltSopr(int idx) {
            return atgaImpSoprTec(idx) + Len.IMP_SOPR_TEC;
        }

        public static int atgaImpAltSoprNull(int idx) {
            return atgaImpAltSopr(idx);
        }

        public static int atgaPreStab(int idx) {
            return atgaImpAltSopr(idx) + Len.IMP_ALT_SOPR;
        }

        public static int atgaPreStabNull(int idx) {
            return atgaPreStab(idx);
        }

        public static int atgaDtEffStab(int idx) {
            return atgaPreStab(idx) + Len.PRE_STAB;
        }

        public static int atgaDtEffStabNull(int idx) {
            return atgaDtEffStab(idx);
        }

        public static int atgaTsRivalFis(int idx) {
            return atgaDtEffStab(idx) + Len.DT_EFF_STAB;
        }

        public static int atgaTsRivalFisNull(int idx) {
            return atgaTsRivalFis(idx);
        }

        public static int atgaTsRivalIndiciz(int idx) {
            return atgaTsRivalFis(idx) + Len.TS_RIVAL_FIS;
        }

        public static int atgaTsRivalIndicizNull(int idx) {
            return atgaTsRivalIndiciz(idx);
        }

        public static int atgaOldTsTec(int idx) {
            return atgaTsRivalIndiciz(idx) + Len.TS_RIVAL_INDICIZ;
        }

        public static int atgaOldTsTecNull(int idx) {
            return atgaOldTsTec(idx);
        }

        public static int atgaRatLrd(int idx) {
            return atgaOldTsTec(idx) + Len.OLD_TS_TEC;
        }

        public static int atgaRatLrdNull(int idx) {
            return atgaRatLrd(idx);
        }

        public static int atgaPreLrd(int idx) {
            return atgaRatLrd(idx) + Len.RAT_LRD;
        }

        public static int atgaPreLrdNull(int idx) {
            return atgaPreLrd(idx);
        }

        public static int atgaPrstzIni(int idx) {
            return atgaPreLrd(idx) + Len.PRE_LRD;
        }

        public static int atgaPrstzIniNull(int idx) {
            return atgaPrstzIni(idx);
        }

        public static int atgaPrstzUlt(int idx) {
            return atgaPrstzIni(idx) + Len.PRSTZ_INI;
        }

        public static int atgaPrstzUltNull(int idx) {
            return atgaPrstzUlt(idx);
        }

        public static int atgaCptInOpzRivto(int idx) {
            return atgaPrstzUlt(idx) + Len.PRSTZ_ULT;
        }

        public static int atgaCptInOpzRivtoNull(int idx) {
            return atgaCptInOpzRivto(idx);
        }

        public static int atgaPrstzIniStab(int idx) {
            return atgaCptInOpzRivto(idx) + Len.CPT_IN_OPZ_RIVTO;
        }

        public static int atgaPrstzIniStabNull(int idx) {
            return atgaPrstzIniStab(idx);
        }

        public static int atgaCptRshMor(int idx) {
            return atgaPrstzIniStab(idx) + Len.PRSTZ_INI_STAB;
        }

        public static int atgaCptRshMorNull(int idx) {
            return atgaCptRshMor(idx);
        }

        public static int atgaPrstzRidIni(int idx) {
            return atgaCptRshMor(idx) + Len.CPT_RSH_MOR;
        }

        public static int atgaPrstzRidIniNull(int idx) {
            return atgaPrstzRidIni(idx);
        }

        public static int atgaFlCarCont(int idx) {
            return atgaPrstzRidIni(idx) + Len.PRSTZ_RID_INI;
        }

        public static int atgaFlCarContNull(int idx) {
            return atgaFlCarCont(idx);
        }

        public static int atgaBnsGiaLiqto(int idx) {
            return atgaFlCarCont(idx) + Len.FL_CAR_CONT;
        }

        public static int atgaBnsGiaLiqtoNull(int idx) {
            return atgaBnsGiaLiqto(idx);
        }

        public static int atgaImpBns(int idx) {
            return atgaBnsGiaLiqto(idx) + Len.BNS_GIA_LIQTO;
        }

        public static int atgaImpBnsNull(int idx) {
            return atgaImpBns(idx);
        }

        public static int atgaCodDvs(int idx) {
            return atgaImpBns(idx) + Len.IMP_BNS;
        }

        public static int atgaPrstzIniNewfis(int idx) {
            return atgaCodDvs(idx) + Len.COD_DVS;
        }

        public static int atgaPrstzIniNewfisNull(int idx) {
            return atgaPrstzIniNewfis(idx);
        }

        public static int atgaImpScon(int idx) {
            return atgaPrstzIniNewfis(idx) + Len.PRSTZ_INI_NEWFIS;
        }

        public static int atgaImpSconNull(int idx) {
            return atgaImpScon(idx);
        }

        public static int atgaAlqScon(int idx) {
            return atgaImpScon(idx) + Len.IMP_SCON;
        }

        public static int atgaAlqSconNull(int idx) {
            return atgaAlqScon(idx);
        }

        public static int atgaImpCarAcq(int idx) {
            return atgaAlqScon(idx) + Len.ALQ_SCON;
        }

        public static int atgaImpCarAcqNull(int idx) {
            return atgaImpCarAcq(idx);
        }

        public static int atgaImpCarInc(int idx) {
            return atgaImpCarAcq(idx) + Len.IMP_CAR_ACQ;
        }

        public static int atgaImpCarIncNull(int idx) {
            return atgaImpCarInc(idx);
        }

        public static int atgaImpCarGest(int idx) {
            return atgaImpCarInc(idx) + Len.IMP_CAR_INC;
        }

        public static int atgaImpCarGestNull(int idx) {
            return atgaImpCarGest(idx);
        }

        public static int atgaEtaAa1oAssto(int idx) {
            return atgaImpCarGest(idx) + Len.IMP_CAR_GEST;
        }

        public static int atgaEtaAa1oAsstoNull(int idx) {
            return atgaEtaAa1oAssto(idx);
        }

        public static int atgaEtaMm1oAssto(int idx) {
            return atgaEtaAa1oAssto(idx) + Len.ETA_AA1O_ASSTO;
        }

        public static int atgaEtaMm1oAsstoNull(int idx) {
            return atgaEtaMm1oAssto(idx);
        }

        public static int atgaEtaAa2oAssto(int idx) {
            return atgaEtaMm1oAssto(idx) + Len.ETA_MM1O_ASSTO;
        }

        public static int atgaEtaAa2oAsstoNull(int idx) {
            return atgaEtaAa2oAssto(idx);
        }

        public static int atgaEtaMm2oAssto(int idx) {
            return atgaEtaAa2oAssto(idx) + Len.ETA_AA2O_ASSTO;
        }

        public static int atgaEtaMm2oAsstoNull(int idx) {
            return atgaEtaMm2oAssto(idx);
        }

        public static int atgaEtaAa3oAssto(int idx) {
            return atgaEtaMm2oAssto(idx) + Len.ETA_MM2O_ASSTO;
        }

        public static int atgaEtaAa3oAsstoNull(int idx) {
            return atgaEtaAa3oAssto(idx);
        }

        public static int atgaEtaMm3oAssto(int idx) {
            return atgaEtaAa3oAssto(idx) + Len.ETA_AA3O_ASSTO;
        }

        public static int atgaEtaMm3oAsstoNull(int idx) {
            return atgaEtaMm3oAssto(idx);
        }

        public static int atgaRendtoLrd(int idx) {
            return atgaEtaMm3oAssto(idx) + Len.ETA_MM3O_ASSTO;
        }

        public static int atgaRendtoLrdNull(int idx) {
            return atgaRendtoLrd(idx);
        }

        public static int atgaPcRetr(int idx) {
            return atgaRendtoLrd(idx) + Len.RENDTO_LRD;
        }

        public static int atgaPcRetrNull(int idx) {
            return atgaPcRetr(idx);
        }

        public static int atgaRendtoRetr(int idx) {
            return atgaPcRetr(idx) + Len.PC_RETR;
        }

        public static int atgaRendtoRetrNull(int idx) {
            return atgaRendtoRetr(idx);
        }

        public static int atgaMinGarto(int idx) {
            return atgaRendtoRetr(idx) + Len.RENDTO_RETR;
        }

        public static int atgaMinGartoNull(int idx) {
            return atgaMinGarto(idx);
        }

        public static int atgaMinTrnut(int idx) {
            return atgaMinGarto(idx) + Len.MIN_GARTO;
        }

        public static int atgaMinTrnutNull(int idx) {
            return atgaMinTrnut(idx);
        }

        public static int atgaPreAttDiTrch(int idx) {
            return atgaMinTrnut(idx) + Len.MIN_TRNUT;
        }

        public static int atgaPreAttDiTrchNull(int idx) {
            return atgaPreAttDiTrch(idx);
        }

        public static int atgaMatuEnd2000(int idx) {
            return atgaPreAttDiTrch(idx) + Len.PRE_ATT_DI_TRCH;
        }

        public static int atgaMatuEnd2000Null(int idx) {
            return atgaMatuEnd2000(idx);
        }

        public static int atgaAbbTotIni(int idx) {
            return atgaMatuEnd2000(idx) + Len.MATU_END2000;
        }

        public static int atgaAbbTotIniNull(int idx) {
            return atgaAbbTotIni(idx);
        }

        public static int atgaAbbTotUlt(int idx) {
            return atgaAbbTotIni(idx) + Len.ABB_TOT_INI;
        }

        public static int atgaAbbTotUltNull(int idx) {
            return atgaAbbTotUlt(idx);
        }

        public static int atgaAbbAnnuUlt(int idx) {
            return atgaAbbTotUlt(idx) + Len.ABB_TOT_ULT;
        }

        public static int atgaAbbAnnuUltNull(int idx) {
            return atgaAbbAnnuUlt(idx);
        }

        public static int atgaDurAbb(int idx) {
            return atgaAbbAnnuUlt(idx) + Len.ABB_ANNU_ULT;
        }

        public static int atgaDurAbbNull(int idx) {
            return atgaDurAbb(idx);
        }

        public static int atgaTpAdegAbb(int idx) {
            return atgaDurAbb(idx) + Len.DUR_ABB;
        }

        public static int atgaTpAdegAbbNull(int idx) {
            return atgaTpAdegAbb(idx);
        }

        public static int atgaModCalc(int idx) {
            return atgaTpAdegAbb(idx) + Len.TP_ADEG_ABB;
        }

        public static int atgaModCalcNull(int idx) {
            return atgaModCalc(idx);
        }

        public static int atgaImpAz(int idx) {
            return atgaModCalc(idx) + Len.MOD_CALC;
        }

        public static int atgaImpAzNull(int idx) {
            return atgaImpAz(idx);
        }

        public static int atgaImpAder(int idx) {
            return atgaImpAz(idx) + Len.IMP_AZ;
        }

        public static int atgaImpAderNull(int idx) {
            return atgaImpAder(idx);
        }

        public static int atgaImpTfr(int idx) {
            return atgaImpAder(idx) + Len.IMP_ADER;
        }

        public static int atgaImpTfrNull(int idx) {
            return atgaImpTfr(idx);
        }

        public static int atgaImpVolo(int idx) {
            return atgaImpTfr(idx) + Len.IMP_TFR;
        }

        public static int atgaImpVoloNull(int idx) {
            return atgaImpVolo(idx);
        }

        public static int atgaVisEnd2000(int idx) {
            return atgaImpVolo(idx) + Len.IMP_VOLO;
        }

        public static int atgaVisEnd2000Null(int idx) {
            return atgaVisEnd2000(idx);
        }

        public static int atgaDtVldtProd(int idx) {
            return atgaVisEnd2000(idx) + Len.VIS_END2000;
        }

        public static int atgaDtVldtProdNull(int idx) {
            return atgaDtVldtProd(idx);
        }

        public static int atgaDtIniValTar(int idx) {
            return atgaDtVldtProd(idx) + Len.DT_VLDT_PROD;
        }

        public static int atgaDtIniValTarNull(int idx) {
            return atgaDtIniValTar(idx);
        }

        public static int atgaImpbVisEnd2000(int idx) {
            return atgaDtIniValTar(idx) + Len.DT_INI_VAL_TAR;
        }

        public static int atgaImpbVisEnd2000Null(int idx) {
            return atgaImpbVisEnd2000(idx);
        }

        public static int atgaRenIniTsTec0(int idx) {
            return atgaImpbVisEnd2000(idx) + Len.IMPB_VIS_END2000;
        }

        public static int atgaRenIniTsTec0Null(int idx) {
            return atgaRenIniTsTec0(idx);
        }

        public static int atgaPcRipPre(int idx) {
            return atgaRenIniTsTec0(idx) + Len.REN_INI_TS_TEC0;
        }

        public static int atgaPcRipPreNull(int idx) {
            return atgaPcRipPre(idx);
        }

        public static int atgaFlImportiForz(int idx) {
            return atgaPcRipPre(idx) + Len.PC_RIP_PRE;
        }

        public static int atgaFlImportiForzNull(int idx) {
            return atgaFlImportiForz(idx);
        }

        public static int atgaPrstzIniNforz(int idx) {
            return atgaFlImportiForz(idx) + Len.FL_IMPORTI_FORZ;
        }

        public static int atgaPrstzIniNforzNull(int idx) {
            return atgaPrstzIniNforz(idx);
        }

        public static int atgaVisEnd2000Nforz(int idx) {
            return atgaPrstzIniNforz(idx) + Len.PRSTZ_INI_NFORZ;
        }

        public static int atgaVisEnd2000NforzNull(int idx) {
            return atgaVisEnd2000Nforz(idx);
        }

        public static int atgaIntrMora(int idx) {
            return atgaVisEnd2000Nforz(idx) + Len.VIS_END2000_NFORZ;
        }

        public static int atgaIntrMoraNull(int idx) {
            return atgaIntrMora(idx);
        }

        public static int atgaManfeeAntic(int idx) {
            return atgaIntrMora(idx) + Len.INTR_MORA;
        }

        public static int atgaManfeeAnticNull(int idx) {
            return atgaManfeeAntic(idx);
        }

        public static int atgaManfeeRicor(int idx) {
            return atgaManfeeAntic(idx) + Len.MANFEE_ANTIC;
        }

        public static int atgaManfeeRicorNull(int idx) {
            return atgaManfeeRicor(idx);
        }

        public static int atgaPreUniRivto(int idx) {
            return atgaManfeeRicor(idx) + Len.MANFEE_RICOR;
        }

        public static int atgaPreUniRivtoNull(int idx) {
            return atgaPreUniRivto(idx);
        }

        public static int atgaProv1aaAcq(int idx) {
            return atgaPreUniRivto(idx) + Len.PRE_UNI_RIVTO;
        }

        public static int atgaProv1aaAcqNull(int idx) {
            return atgaProv1aaAcq(idx);
        }

        public static int atgaProv2aaAcq(int idx) {
            return atgaProv1aaAcq(idx) + Len.PROV1AA_ACQ;
        }

        public static int atgaProv2aaAcqNull(int idx) {
            return atgaProv2aaAcq(idx);
        }

        public static int atgaProvRicor(int idx) {
            return atgaProv2aaAcq(idx) + Len.PROV2AA_ACQ;
        }

        public static int atgaProvRicorNull(int idx) {
            return atgaProvRicor(idx);
        }

        public static int atgaProvInc(int idx) {
            return atgaProvRicor(idx) + Len.PROV_RICOR;
        }

        public static int atgaProvIncNull(int idx) {
            return atgaProvInc(idx);
        }

        public static int atgaAlqProvAcq(int idx) {
            return atgaProvInc(idx) + Len.PROV_INC;
        }

        public static int atgaAlqProvAcqNull(int idx) {
            return atgaAlqProvAcq(idx);
        }

        public static int atgaAlqProvInc(int idx) {
            return atgaAlqProvAcq(idx) + Len.ALQ_PROV_ACQ;
        }

        public static int atgaAlqProvIncNull(int idx) {
            return atgaAlqProvInc(idx);
        }

        public static int atgaAlqProvRicor(int idx) {
            return atgaAlqProvInc(idx) + Len.ALQ_PROV_INC;
        }

        public static int atgaAlqProvRicorNull(int idx) {
            return atgaAlqProvRicor(idx);
        }

        public static int atgaImpbProvAcq(int idx) {
            return atgaAlqProvRicor(idx) + Len.ALQ_PROV_RICOR;
        }

        public static int atgaImpbProvAcqNull(int idx) {
            return atgaImpbProvAcq(idx);
        }

        public static int atgaImpbProvInc(int idx) {
            return atgaImpbProvAcq(idx) + Len.IMPB_PROV_ACQ;
        }

        public static int atgaImpbProvIncNull(int idx) {
            return atgaImpbProvInc(idx);
        }

        public static int atgaImpbProvRicor(int idx) {
            return atgaImpbProvInc(idx) + Len.IMPB_PROV_INC;
        }

        public static int atgaImpbProvRicorNull(int idx) {
            return atgaImpbProvRicor(idx);
        }

        public static int atgaFlProvForz(int idx) {
            return atgaImpbProvRicor(idx) + Len.IMPB_PROV_RICOR;
        }

        public static int atgaFlProvForzNull(int idx) {
            return atgaFlProvForz(idx);
        }

        public static int atgaPrstzAggIni(int idx) {
            return atgaFlProvForz(idx) + Len.FL_PROV_FORZ;
        }

        public static int atgaPrstzAggIniNull(int idx) {
            return atgaPrstzAggIni(idx);
        }

        public static int atgaIncrPre(int idx) {
            return atgaPrstzAggIni(idx) + Len.PRSTZ_AGG_INI;
        }

        public static int atgaIncrPreNull(int idx) {
            return atgaIncrPre(idx);
        }

        public static int atgaIncrPrstz(int idx) {
            return atgaIncrPre(idx) + Len.INCR_PRE;
        }

        public static int atgaIncrPrstzNull(int idx) {
            return atgaIncrPrstz(idx);
        }

        public static int atgaDtUltAdegPrePr(int idx) {
            return atgaIncrPrstz(idx) + Len.INCR_PRSTZ;
        }

        public static int atgaDtUltAdegPrePrNull(int idx) {
            return atgaDtUltAdegPrePr(idx);
        }

        public static int atgaPrstzAggUlt(int idx) {
            return atgaDtUltAdegPrePr(idx) + Len.DT_ULT_ADEG_PRE_PR;
        }

        public static int atgaPrstzAggUltNull(int idx) {
            return atgaPrstzAggUlt(idx);
        }

        public static int atgaTsRivalNet(int idx) {
            return atgaPrstzAggUlt(idx) + Len.PRSTZ_AGG_ULT;
        }

        public static int atgaTsRivalNetNull(int idx) {
            return atgaTsRivalNet(idx);
        }

        public static int atgaPrePattuito(int idx) {
            return atgaTsRivalNet(idx) + Len.TS_RIVAL_NET;
        }

        public static int atgaPrePattuitoNull(int idx) {
            return atgaPrePattuito(idx);
        }

        public static int atgaTpRival(int idx) {
            return atgaPrePattuito(idx) + Len.PRE_PATTUITO;
        }

        public static int atgaTpRivalNull(int idx) {
            return atgaTpRival(idx);
        }

        public static int atgaRisMat(int idx) {
            return atgaTpRival(idx) + Len.TP_RIVAL;
        }

        public static int atgaRisMatNull(int idx) {
            return atgaRisMat(idx);
        }

        public static int atgaCptMinScad(int idx) {
            return atgaRisMat(idx) + Len.RIS_MAT;
        }

        public static int atgaCptMinScadNull(int idx) {
            return atgaCptMinScad(idx);
        }

        public static int atgaCommisGest(int idx) {
            return atgaCptMinScad(idx) + Len.CPT_MIN_SCAD;
        }

        public static int atgaCommisGestNull(int idx) {
            return atgaCommisGest(idx);
        }

        public static int atgaTpManfeeAppl(int idx) {
            return atgaCommisGest(idx) + Len.COMMIS_GEST;
        }

        public static int atgaTpManfeeApplNull(int idx) {
            return atgaTpManfeeAppl(idx);
        }

        public static int atgaDsRiga(int idx) {
            return atgaTpManfeeAppl(idx) + Len.TP_MANFEE_APPL;
        }

        public static int atgaDsOperSql(int idx) {
            return atgaDsRiga(idx) + Len.DS_RIGA;
        }

        public static int atgaDsVer(int idx) {
            return atgaDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int atgaDsTsIniCptz(int idx) {
            return atgaDsVer(idx) + Len.DS_VER;
        }

        public static int atgaDsTsEndCptz(int idx) {
            return atgaDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int atgaDsUtente(int idx) {
            return atgaDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int atgaDsStatoElab(int idx) {
            return atgaDsUtente(idx) + Len.DS_UTENTE;
        }

        public static int atgaPcCommisGest(int idx) {
            return atgaDsStatoElab(idx) + Len.DS_STATO_ELAB;
        }

        public static int atgaPcCommisGestNull(int idx) {
            return atgaPcCommisGest(idx);
        }

        public static int atgaNumGgRival(int idx) {
            return atgaPcCommisGest(idx) + Len.PC_COMMIS_GEST;
        }

        public static int atgaNumGgRivalNull(int idx) {
            return atgaNumGgRival(idx);
        }

        public static int atgaImpTrasfe(int idx) {
            return atgaNumGgRival(idx) + Len.NUM_GG_RIVAL;
        }

        public static int atgaImpTrasfeNull(int idx) {
            return atgaImpTrasfe(idx);
        }

        public static int atgaImpTfrStrc(int idx) {
            return atgaImpTrasfe(idx) + Len.IMP_TRASFE;
        }

        public static int atgaImpTfrStrcNull(int idx) {
            return atgaImpTfrStrc(idx);
        }

        public static int atgaAcqExp(int idx) {
            return atgaImpTfrStrc(idx) + Len.IMP_TFR_STRC;
        }

        public static int atgaAcqExpNull(int idx) {
            return atgaAcqExp(idx);
        }

        public static int atgaRemunAss(int idx) {
            return atgaAcqExp(idx) + Len.ACQ_EXP;
        }

        public static int atgaRemunAssNull(int idx) {
            return atgaRemunAss(idx);
        }

        public static int atgaCommisInter(int idx) {
            return atgaRemunAss(idx) + Len.REMUN_ASS;
        }

        public static int atgaCommisInterNull(int idx) {
            return atgaCommisInter(idx);
        }

        public static int atgaAlqRemunAss(int idx) {
            return atgaCommisInter(idx) + Len.COMMIS_INTER;
        }

        public static int atgaAlqRemunAssNull(int idx) {
            return atgaAlqRemunAss(idx);
        }

        public static int atgaAlqCommisInter(int idx) {
            return atgaAlqRemunAss(idx) + Len.ALQ_REMUN_ASS;
        }

        public static int atgaAlqCommisInterNull(int idx) {
            return atgaAlqCommisInter(idx);
        }

        public static int atgaImpbRemunAss(int idx) {
            return atgaAlqCommisInter(idx) + Len.ALQ_COMMIS_INTER;
        }

        public static int atgaImpbRemunAssNull(int idx) {
            return atgaImpbRemunAss(idx);
        }

        public static int atgaImpbCommisInter(int idx) {
            return atgaImpbRemunAss(idx) + Len.IMPB_REMUN_ASS;
        }

        public static int atgaImpbCommisInterNull(int idx) {
            return atgaImpbCommisInter(idx);
        }

        public static int atgaCosRunAssva(int idx) {
            return atgaImpbCommisInter(idx) + Len.IMPB_COMMIS_INTER;
        }

        public static int atgaCosRunAssvaNull(int idx) {
            return atgaCosRunAssva(idx);
        }

        public static int atgaCosRunAssvaIdc(int idx) {
            return atgaCosRunAssva(idx) + Len.COS_RUN_ASSVA;
        }

        public static int atgaCosRunAssvaIdcNull(int idx) {
            return atgaCosRunAssvaIdc(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_TRCH_DI_GAR = 5;
        public static final int ID_GAR = 5;
        public static final int ID_ADES = 5;
        public static final int ID_POLI = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int DT_DECOR = 5;
        public static final int DT_SCAD = 5;
        public static final int IB_OGG = 40;
        public static final int TP_RGM_FISC = 2;
        public static final int DT_EMIS = 5;
        public static final int TP_TRCH = 2;
        public static final int DUR_AA = 3;
        public static final int DUR_MM = 3;
        public static final int DUR_GG = 3;
        public static final int PRE_CASO_MOR = 8;
        public static final int PC_INTR_RIAT = 4;
        public static final int IMP_BNS_ANTIC = 8;
        public static final int PRE_INI_NET = 8;
        public static final int PRE_PP_INI = 8;
        public static final int PRE_PP_ULT = 8;
        public static final int PRE_TARI_INI = 8;
        public static final int PRE_TARI_ULT = 8;
        public static final int PRE_INVRIO_INI = 8;
        public static final int PRE_INVRIO_ULT = 8;
        public static final int PRE_RIVTO = 8;
        public static final int IMP_SOPR_PROF = 8;
        public static final int IMP_SOPR_SAN = 8;
        public static final int IMP_SOPR_SPO = 8;
        public static final int IMP_SOPR_TEC = 8;
        public static final int IMP_ALT_SOPR = 8;
        public static final int PRE_STAB = 8;
        public static final int DT_EFF_STAB = 5;
        public static final int TS_RIVAL_FIS = 8;
        public static final int TS_RIVAL_INDICIZ = 8;
        public static final int OLD_TS_TEC = 8;
        public static final int RAT_LRD = 8;
        public static final int PRE_LRD = 8;
        public static final int PRSTZ_INI = 8;
        public static final int PRSTZ_ULT = 8;
        public static final int CPT_IN_OPZ_RIVTO = 8;
        public static final int PRSTZ_INI_STAB = 8;
        public static final int CPT_RSH_MOR = 8;
        public static final int PRSTZ_RID_INI = 8;
        public static final int FL_CAR_CONT = 1;
        public static final int BNS_GIA_LIQTO = 8;
        public static final int IMP_BNS = 8;
        public static final int COD_DVS = 20;
        public static final int PRSTZ_INI_NEWFIS = 8;
        public static final int IMP_SCON = 8;
        public static final int ALQ_SCON = 4;
        public static final int IMP_CAR_ACQ = 8;
        public static final int IMP_CAR_INC = 8;
        public static final int IMP_CAR_GEST = 8;
        public static final int ETA_AA1O_ASSTO = 2;
        public static final int ETA_MM1O_ASSTO = 2;
        public static final int ETA_AA2O_ASSTO = 2;
        public static final int ETA_MM2O_ASSTO = 2;
        public static final int ETA_AA3O_ASSTO = 2;
        public static final int ETA_MM3O_ASSTO = 2;
        public static final int RENDTO_LRD = 8;
        public static final int PC_RETR = 4;
        public static final int RENDTO_RETR = 8;
        public static final int MIN_GARTO = 8;
        public static final int MIN_TRNUT = 8;
        public static final int PRE_ATT_DI_TRCH = 8;
        public static final int MATU_END2000 = 8;
        public static final int ABB_TOT_INI = 8;
        public static final int ABB_TOT_ULT = 8;
        public static final int ABB_ANNU_ULT = 8;
        public static final int DUR_ABB = 4;
        public static final int TP_ADEG_ABB = 1;
        public static final int MOD_CALC = 2;
        public static final int IMP_AZ = 8;
        public static final int IMP_ADER = 8;
        public static final int IMP_TFR = 8;
        public static final int IMP_VOLO = 8;
        public static final int VIS_END2000 = 8;
        public static final int DT_VLDT_PROD = 5;
        public static final int DT_INI_VAL_TAR = 5;
        public static final int IMPB_VIS_END2000 = 8;
        public static final int REN_INI_TS_TEC0 = 8;
        public static final int PC_RIP_PRE = 4;
        public static final int FL_IMPORTI_FORZ = 1;
        public static final int PRSTZ_INI_NFORZ = 8;
        public static final int VIS_END2000_NFORZ = 8;
        public static final int INTR_MORA = 8;
        public static final int MANFEE_ANTIC = 8;
        public static final int MANFEE_RICOR = 8;
        public static final int PRE_UNI_RIVTO = 8;
        public static final int PROV1AA_ACQ = 8;
        public static final int PROV2AA_ACQ = 8;
        public static final int PROV_RICOR = 8;
        public static final int PROV_INC = 8;
        public static final int ALQ_PROV_ACQ = 4;
        public static final int ALQ_PROV_INC = 4;
        public static final int ALQ_PROV_RICOR = 4;
        public static final int IMPB_PROV_ACQ = 8;
        public static final int IMPB_PROV_INC = 8;
        public static final int IMPB_PROV_RICOR = 8;
        public static final int FL_PROV_FORZ = 1;
        public static final int PRSTZ_AGG_INI = 8;
        public static final int INCR_PRE = 8;
        public static final int INCR_PRSTZ = 8;
        public static final int DT_ULT_ADEG_PRE_PR = 5;
        public static final int PRSTZ_AGG_ULT = 8;
        public static final int TS_RIVAL_NET = 8;
        public static final int PRE_PATTUITO = 8;
        public static final int TP_RIVAL = 2;
        public static final int RIS_MAT = 8;
        public static final int CPT_MIN_SCAD = 8;
        public static final int COMMIS_GEST = 8;
        public static final int TP_MANFEE_APPL = 2;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int PC_COMMIS_GEST = 4;
        public static final int NUM_GG_RIVAL = 3;
        public static final int IMP_TRASFE = 8;
        public static final int IMP_TFR_STRC = 8;
        public static final int ACQ_EXP = 8;
        public static final int REMUN_ASS = 8;
        public static final int COMMIS_INTER = 8;
        public static final int ALQ_REMUN_ASS = 4;
        public static final int ALQ_COMMIS_INTER = 4;
        public static final int IMPB_REMUN_ASS = 8;
        public static final int IMPB_COMMIS_INTER = 8;
        public static final int COS_RUN_ASSVA = 8;
        public static final int COS_RUN_ASSVA_IDC = 8;
        public static final int DATI = ID_TRCH_DI_GAR + ID_GAR + ID_ADES + ID_POLI + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + DT_DECOR + DT_SCAD + IB_OGG + TP_RGM_FISC + DT_EMIS + TP_TRCH + DUR_AA + DUR_MM + DUR_GG + PRE_CASO_MOR + PC_INTR_RIAT + IMP_BNS_ANTIC + PRE_INI_NET + PRE_PP_INI + PRE_PP_ULT + PRE_TARI_INI + PRE_TARI_ULT + PRE_INVRIO_INI + PRE_INVRIO_ULT + PRE_RIVTO + IMP_SOPR_PROF + IMP_SOPR_SAN + IMP_SOPR_SPO + IMP_SOPR_TEC + IMP_ALT_SOPR + PRE_STAB + DT_EFF_STAB + TS_RIVAL_FIS + TS_RIVAL_INDICIZ + OLD_TS_TEC + RAT_LRD + PRE_LRD + PRSTZ_INI + PRSTZ_ULT + CPT_IN_OPZ_RIVTO + PRSTZ_INI_STAB + CPT_RSH_MOR + PRSTZ_RID_INI + FL_CAR_CONT + BNS_GIA_LIQTO + IMP_BNS + COD_DVS + PRSTZ_INI_NEWFIS + IMP_SCON + ALQ_SCON + IMP_CAR_ACQ + IMP_CAR_INC + IMP_CAR_GEST + ETA_AA1O_ASSTO + ETA_MM1O_ASSTO + ETA_AA2O_ASSTO + ETA_MM2O_ASSTO + ETA_AA3O_ASSTO + ETA_MM3O_ASSTO + RENDTO_LRD + PC_RETR + RENDTO_RETR + MIN_GARTO + MIN_TRNUT + PRE_ATT_DI_TRCH + MATU_END2000 + ABB_TOT_INI + ABB_TOT_ULT + ABB_ANNU_ULT + DUR_ABB + TP_ADEG_ABB + MOD_CALC + IMP_AZ + IMP_ADER + IMP_TFR + IMP_VOLO + VIS_END2000 + DT_VLDT_PROD + DT_INI_VAL_TAR + IMPB_VIS_END2000 + REN_INI_TS_TEC0 + PC_RIP_PRE + FL_IMPORTI_FORZ + PRSTZ_INI_NFORZ + VIS_END2000_NFORZ + INTR_MORA + MANFEE_ANTIC + MANFEE_RICOR + PRE_UNI_RIVTO + PROV1AA_ACQ + PROV2AA_ACQ + PROV_RICOR + PROV_INC + ALQ_PROV_ACQ + ALQ_PROV_INC + ALQ_PROV_RICOR + IMPB_PROV_ACQ + IMPB_PROV_INC + IMPB_PROV_RICOR + FL_PROV_FORZ + PRSTZ_AGG_INI + INCR_PRE + INCR_PRSTZ + DT_ULT_ADEG_PRE_PR + PRSTZ_AGG_ULT + TS_RIVAL_NET + PRE_PATTUITO + TP_RIVAL + RIS_MAT + CPT_MIN_SCAD + COMMIS_GEST + TP_MANFEE_APPL + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + PC_COMMIS_GEST + NUM_GG_RIVAL + IMP_TRASFE + IMP_TFR_STRC + ACQ_EXP + REMUN_ASS + COMMIS_INTER + ALQ_REMUN_ASS + ALQ_COMMIS_INTER + IMPB_REMUN_ASS + IMPB_COMMIS_INTER + COS_RUN_ASSVA + COS_RUN_ASSVA_IDC;
        public static final int TAB_TRAN = STATUS + ID_PTF + DATI;
        public static final int FLR1 = 911;
        public static final int ATGA_TAB = AtgaTab.TAB_TRAN_MAXOCCURS * TAB_TRAN;
        public static final int RESTO_TAB = 1137839;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_TRCH_DI_GAR = 9;
            public static final int ID_GAR = 9;
            public static final int ID_ADES = 9;
            public static final int ID_POLI = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_DECOR = 8;
            public static final int DT_SCAD = 8;
            public static final int DT_EMIS = 8;
            public static final int DUR_AA = 5;
            public static final int DUR_MM = 5;
            public static final int DUR_GG = 5;
            public static final int PRE_CASO_MOR = 12;
            public static final int PC_INTR_RIAT = 3;
            public static final int IMP_BNS_ANTIC = 12;
            public static final int PRE_INI_NET = 12;
            public static final int PRE_PP_INI = 12;
            public static final int PRE_PP_ULT = 12;
            public static final int PRE_TARI_INI = 12;
            public static final int PRE_TARI_ULT = 12;
            public static final int PRE_INVRIO_INI = 12;
            public static final int PRE_INVRIO_ULT = 12;
            public static final int PRE_RIVTO = 12;
            public static final int IMP_SOPR_PROF = 12;
            public static final int IMP_SOPR_SAN = 12;
            public static final int IMP_SOPR_SPO = 12;
            public static final int IMP_SOPR_TEC = 12;
            public static final int IMP_ALT_SOPR = 12;
            public static final int PRE_STAB = 12;
            public static final int DT_EFF_STAB = 8;
            public static final int TS_RIVAL_FIS = 5;
            public static final int TS_RIVAL_INDICIZ = 5;
            public static final int OLD_TS_TEC = 5;
            public static final int RAT_LRD = 12;
            public static final int PRE_LRD = 12;
            public static final int PRSTZ_INI = 12;
            public static final int PRSTZ_ULT = 12;
            public static final int CPT_IN_OPZ_RIVTO = 12;
            public static final int PRSTZ_INI_STAB = 12;
            public static final int CPT_RSH_MOR = 12;
            public static final int PRSTZ_RID_INI = 12;
            public static final int BNS_GIA_LIQTO = 12;
            public static final int IMP_BNS = 12;
            public static final int PRSTZ_INI_NEWFIS = 12;
            public static final int IMP_SCON = 12;
            public static final int ALQ_SCON = 3;
            public static final int IMP_CAR_ACQ = 12;
            public static final int IMP_CAR_INC = 12;
            public static final int IMP_CAR_GEST = 12;
            public static final int ETA_AA1O_ASSTO = 3;
            public static final int ETA_MM1O_ASSTO = 3;
            public static final int ETA_AA2O_ASSTO = 3;
            public static final int ETA_MM2O_ASSTO = 3;
            public static final int ETA_AA3O_ASSTO = 3;
            public static final int ETA_MM3O_ASSTO = 3;
            public static final int RENDTO_LRD = 5;
            public static final int PC_RETR = 3;
            public static final int RENDTO_RETR = 5;
            public static final int MIN_GARTO = 5;
            public static final int MIN_TRNUT = 5;
            public static final int PRE_ATT_DI_TRCH = 12;
            public static final int MATU_END2000 = 12;
            public static final int ABB_TOT_INI = 12;
            public static final int ABB_TOT_ULT = 12;
            public static final int ABB_ANNU_ULT = 12;
            public static final int DUR_ABB = 6;
            public static final int IMP_AZ = 12;
            public static final int IMP_ADER = 12;
            public static final int IMP_TFR = 12;
            public static final int IMP_VOLO = 12;
            public static final int VIS_END2000 = 12;
            public static final int DT_VLDT_PROD = 8;
            public static final int DT_INI_VAL_TAR = 8;
            public static final int IMPB_VIS_END2000 = 12;
            public static final int REN_INI_TS_TEC0 = 12;
            public static final int PC_RIP_PRE = 3;
            public static final int PRSTZ_INI_NFORZ = 12;
            public static final int VIS_END2000_NFORZ = 12;
            public static final int INTR_MORA = 12;
            public static final int MANFEE_ANTIC = 12;
            public static final int MANFEE_RICOR = 12;
            public static final int PRE_UNI_RIVTO = 12;
            public static final int PROV1AA_ACQ = 12;
            public static final int PROV2AA_ACQ = 12;
            public static final int PROV_RICOR = 12;
            public static final int PROV_INC = 12;
            public static final int ALQ_PROV_ACQ = 3;
            public static final int ALQ_PROV_INC = 3;
            public static final int ALQ_PROV_RICOR = 3;
            public static final int IMPB_PROV_ACQ = 12;
            public static final int IMPB_PROV_INC = 12;
            public static final int IMPB_PROV_RICOR = 12;
            public static final int PRSTZ_AGG_INI = 12;
            public static final int INCR_PRE = 12;
            public static final int INCR_PRSTZ = 12;
            public static final int DT_ULT_ADEG_PRE_PR = 8;
            public static final int PRSTZ_AGG_ULT = 12;
            public static final int TS_RIVAL_NET = 5;
            public static final int PRE_PATTUITO = 12;
            public static final int RIS_MAT = 12;
            public static final int CPT_MIN_SCAD = 12;
            public static final int COMMIS_GEST = 12;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int PC_COMMIS_GEST = 3;
            public static final int NUM_GG_RIVAL = 5;
            public static final int IMP_TRASFE = 12;
            public static final int IMP_TFR_STRC = 12;
            public static final int ACQ_EXP = 12;
            public static final int REMUN_ASS = 12;
            public static final int COMMIS_INTER = 12;
            public static final int ALQ_REMUN_ASS = 3;
            public static final int ALQ_COMMIS_INTER = 3;
            public static final int IMPB_REMUN_ASS = 12;
            public static final int IMPB_COMMIS_INTER = 12;
            public static final int COS_RUN_ASSVA = 12;
            public static final int COS_RUN_ASSVA_IDC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_CASO_MOR = 3;
            public static final int PC_INTR_RIAT = 3;
            public static final int IMP_BNS_ANTIC = 3;
            public static final int PRE_INI_NET = 3;
            public static final int PRE_PP_INI = 3;
            public static final int PRE_PP_ULT = 3;
            public static final int PRE_TARI_INI = 3;
            public static final int PRE_TARI_ULT = 3;
            public static final int PRE_INVRIO_INI = 3;
            public static final int PRE_INVRIO_ULT = 3;
            public static final int PRE_RIVTO = 3;
            public static final int IMP_SOPR_PROF = 3;
            public static final int IMP_SOPR_SAN = 3;
            public static final int IMP_SOPR_SPO = 3;
            public static final int IMP_SOPR_TEC = 3;
            public static final int IMP_ALT_SOPR = 3;
            public static final int PRE_STAB = 3;
            public static final int TS_RIVAL_FIS = 9;
            public static final int TS_RIVAL_INDICIZ = 9;
            public static final int OLD_TS_TEC = 9;
            public static final int RAT_LRD = 3;
            public static final int PRE_LRD = 3;
            public static final int PRSTZ_INI = 3;
            public static final int PRSTZ_ULT = 3;
            public static final int CPT_IN_OPZ_RIVTO = 3;
            public static final int PRSTZ_INI_STAB = 3;
            public static final int CPT_RSH_MOR = 3;
            public static final int PRSTZ_RID_INI = 3;
            public static final int BNS_GIA_LIQTO = 3;
            public static final int IMP_BNS = 3;
            public static final int PRSTZ_INI_NEWFIS = 3;
            public static final int IMP_SCON = 3;
            public static final int ALQ_SCON = 3;
            public static final int IMP_CAR_ACQ = 3;
            public static final int IMP_CAR_INC = 3;
            public static final int IMP_CAR_GEST = 3;
            public static final int RENDTO_LRD = 9;
            public static final int PC_RETR = 3;
            public static final int RENDTO_RETR = 9;
            public static final int MIN_GARTO = 9;
            public static final int MIN_TRNUT = 9;
            public static final int PRE_ATT_DI_TRCH = 3;
            public static final int MATU_END2000 = 3;
            public static final int ABB_TOT_INI = 3;
            public static final int ABB_TOT_ULT = 3;
            public static final int ABB_ANNU_ULT = 3;
            public static final int IMP_AZ = 3;
            public static final int IMP_ADER = 3;
            public static final int IMP_TFR = 3;
            public static final int IMP_VOLO = 3;
            public static final int VIS_END2000 = 3;
            public static final int IMPB_VIS_END2000 = 3;
            public static final int REN_INI_TS_TEC0 = 3;
            public static final int PC_RIP_PRE = 3;
            public static final int PRSTZ_INI_NFORZ = 3;
            public static final int VIS_END2000_NFORZ = 3;
            public static final int INTR_MORA = 3;
            public static final int MANFEE_ANTIC = 3;
            public static final int MANFEE_RICOR = 3;
            public static final int PRE_UNI_RIVTO = 3;
            public static final int PROV1AA_ACQ = 3;
            public static final int PROV2AA_ACQ = 3;
            public static final int PROV_RICOR = 3;
            public static final int PROV_INC = 3;
            public static final int ALQ_PROV_ACQ = 3;
            public static final int ALQ_PROV_INC = 3;
            public static final int ALQ_PROV_RICOR = 3;
            public static final int IMPB_PROV_ACQ = 3;
            public static final int IMPB_PROV_INC = 3;
            public static final int IMPB_PROV_RICOR = 3;
            public static final int PRSTZ_AGG_INI = 3;
            public static final int INCR_PRE = 3;
            public static final int INCR_PRSTZ = 3;
            public static final int PRSTZ_AGG_ULT = 3;
            public static final int TS_RIVAL_NET = 9;
            public static final int PRE_PATTUITO = 3;
            public static final int RIS_MAT = 3;
            public static final int CPT_MIN_SCAD = 3;
            public static final int COMMIS_GEST = 3;
            public static final int PC_COMMIS_GEST = 3;
            public static final int IMP_TRASFE = 3;
            public static final int IMP_TFR_STRC = 3;
            public static final int ACQ_EXP = 3;
            public static final int REMUN_ASS = 3;
            public static final int COMMIS_INTER = 3;
            public static final int ALQ_REMUN_ASS = 3;
            public static final int ALQ_COMMIS_INTER = 3;
            public static final int IMPB_REMUN_ASS = 3;
            public static final int IMPB_COMMIS_INTER = 3;
            public static final int COS_RUN_ASSVA = 3;
            public static final int COS_RUN_ASSVA_IDC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
