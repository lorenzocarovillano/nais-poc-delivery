package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-INVRIO-ULT<br>
 * Variable: TGA-PRE-INVRIO-ULT from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPreInvrioUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPreInvrioUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_INVRIO_ULT;
    }

    public void setTgaPreInvrioUlt(AfDecimal tgaPreInvrioUlt) {
        writeDecimalAsPacked(Pos.TGA_PRE_INVRIO_ULT, tgaPreInvrioUlt.copy());
    }

    public void setTgaPreInvrioUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_INVRIO_ULT, Pos.TGA_PRE_INVRIO_ULT);
    }

    /**Original name: TGA-PRE-INVRIO-ULT<br>*/
    public AfDecimal getTgaPreInvrioUlt() {
        return readPackedAsDecimal(Pos.TGA_PRE_INVRIO_ULT, Len.Int.TGA_PRE_INVRIO_ULT, Len.Fract.TGA_PRE_INVRIO_ULT);
    }

    public byte[] getTgaPreInvrioUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_INVRIO_ULT, Pos.TGA_PRE_INVRIO_ULT);
        return buffer;
    }

    public void setTgaPreInvrioUltNull(String tgaPreInvrioUltNull) {
        writeString(Pos.TGA_PRE_INVRIO_ULT_NULL, tgaPreInvrioUltNull, Len.TGA_PRE_INVRIO_ULT_NULL);
    }

    /**Original name: TGA-PRE-INVRIO-ULT-NULL<br>*/
    public String getTgaPreInvrioUltNull() {
        return readString(Pos.TGA_PRE_INVRIO_ULT_NULL, Len.TGA_PRE_INVRIO_ULT_NULL);
    }

    public String getTgaPreInvrioUltNullFormatted() {
        return Functions.padBlanks(getTgaPreInvrioUltNull(), Len.TGA_PRE_INVRIO_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_INVRIO_ULT = 1;
        public static final int TGA_PRE_INVRIO_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_INVRIO_ULT = 8;
        public static final int TGA_PRE_INVRIO_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_INVRIO_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_INVRIO_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
