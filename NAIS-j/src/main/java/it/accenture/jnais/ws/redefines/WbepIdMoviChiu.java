package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WBEP-ID-MOVI-CHIU<br>
 * Variable: WBEP-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbepIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbepIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEP_ID_MOVI_CHIU;
    }

    public void setWbepIdMoviChiu(int wbepIdMoviChiu) {
        writeIntAsPacked(Pos.WBEP_ID_MOVI_CHIU, wbepIdMoviChiu, Len.Int.WBEP_ID_MOVI_CHIU);
    }

    public void setWbepIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEP_ID_MOVI_CHIU, Pos.WBEP_ID_MOVI_CHIU);
    }

    /**Original name: WBEP-ID-MOVI-CHIU<br>*/
    public int getWbepIdMoviChiu() {
        return readPackedAsInt(Pos.WBEP_ID_MOVI_CHIU, Len.Int.WBEP_ID_MOVI_CHIU);
    }

    public byte[] getWbepIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEP_ID_MOVI_CHIU, Pos.WBEP_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWbepIdMoviChiuSpaces() {
        fill(Pos.WBEP_ID_MOVI_CHIU, Len.WBEP_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWbepIdMoviChiuNull(String wbepIdMoviChiuNull) {
        writeString(Pos.WBEP_ID_MOVI_CHIU_NULL, wbepIdMoviChiuNull, Len.WBEP_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WBEP-ID-MOVI-CHIU-NULL<br>*/
    public String getWbepIdMoviChiuNull() {
        return readString(Pos.WBEP_ID_MOVI_CHIU_NULL, Len.WBEP_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEP_ID_MOVI_CHIU = 1;
        public static final int WBEP_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEP_ID_MOVI_CHIU = 5;
        public static final int WBEP_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEP_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
