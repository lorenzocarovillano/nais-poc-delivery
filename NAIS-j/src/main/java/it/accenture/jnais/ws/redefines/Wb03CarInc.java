package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CAR-INC<br>
 * Variable: WB03-CAR-INC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CAR_INC;
    }

    public void setWb03CarInc(AfDecimal wb03CarInc) {
        writeDecimalAsPacked(Pos.WB03_CAR_INC, wb03CarInc.copy());
    }

    public void setWb03CarIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CAR_INC, Pos.WB03_CAR_INC);
    }

    /**Original name: WB03-CAR-INC<br>*/
    public AfDecimal getWb03CarInc() {
        return readPackedAsDecimal(Pos.WB03_CAR_INC, Len.Int.WB03_CAR_INC, Len.Fract.WB03_CAR_INC);
    }

    public byte[] getWb03CarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CAR_INC, Pos.WB03_CAR_INC);
        return buffer;
    }

    public void setWb03CarIncNull(String wb03CarIncNull) {
        writeString(Pos.WB03_CAR_INC_NULL, wb03CarIncNull, Len.WB03_CAR_INC_NULL);
    }

    /**Original name: WB03-CAR-INC-NULL<br>*/
    public String getWb03CarIncNull() {
        return readString(Pos.WB03_CAR_INC_NULL, Len.WB03_CAR_INC_NULL);
    }

    public String getWb03CarIncNullFormatted() {
        return Functions.padBlanks(getWb03CarIncNull(), Len.WB03_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CAR_INC = 1;
        public static final int WB03_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CAR_INC = 8;
        public static final int WB03_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
