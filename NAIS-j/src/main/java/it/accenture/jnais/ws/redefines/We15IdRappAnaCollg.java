package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WE15-ID-RAPP-ANA-COLLG<br>
 * Variable: WE15-ID-RAPP-ANA-COLLG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class We15IdRappAnaCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public We15IdRappAnaCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WE15_ID_RAPP_ANA_COLLG;
    }

    public void setWe15IdRappAnaCollg(int we15IdRappAnaCollg) {
        writeIntAsPacked(Pos.WE15_ID_RAPP_ANA_COLLG, we15IdRappAnaCollg, Len.Int.WE15_ID_RAPP_ANA_COLLG);
    }

    public void setWe15IdRappAnaCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WE15_ID_RAPP_ANA_COLLG, Pos.WE15_ID_RAPP_ANA_COLLG);
    }

    /**Original name: WE15-ID-RAPP-ANA-COLLG<br>*/
    public int getWe15IdRappAnaCollg() {
        return readPackedAsInt(Pos.WE15_ID_RAPP_ANA_COLLG, Len.Int.WE15_ID_RAPP_ANA_COLLG);
    }

    public byte[] getWe15IdRappAnaCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WE15_ID_RAPP_ANA_COLLG, Pos.WE15_ID_RAPP_ANA_COLLG);
        return buffer;
    }

    public void initWe15IdRappAnaCollgSpaces() {
        fill(Pos.WE15_ID_RAPP_ANA_COLLG, Len.WE15_ID_RAPP_ANA_COLLG, Types.SPACE_CHAR);
    }

    public void setWe15IdRappAnaCollgNull(String we15IdRappAnaCollgNull) {
        writeString(Pos.WE15_ID_RAPP_ANA_COLLG_NULL, we15IdRappAnaCollgNull, Len.WE15_ID_RAPP_ANA_COLLG_NULL);
    }

    /**Original name: WE15-ID-RAPP-ANA-COLLG-NULL<br>*/
    public String getWe15IdRappAnaCollgNull() {
        return readString(Pos.WE15_ID_RAPP_ANA_COLLG_NULL, Len.WE15_ID_RAPP_ANA_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WE15_ID_RAPP_ANA_COLLG = 1;
        public static final int WE15_ID_RAPP_ANA_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WE15_ID_RAPP_ANA_COLLG = 5;
        public static final int WE15_ID_RAPP_ANA_COLLG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WE15_ID_RAPP_ANA_COLLG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
