package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-COS-RUN-ASSVA-IDC<br>
 * Variable: L3421-COS-RUN-ASSVA-IDC from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421CosRunAssvaIdc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421CosRunAssvaIdc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_COS_RUN_ASSVA_IDC;
    }

    public void setL3421CosRunAssvaIdcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_COS_RUN_ASSVA_IDC, Pos.L3421_COS_RUN_ASSVA_IDC);
    }

    /**Original name: L3421-COS-RUN-ASSVA-IDC<br>*/
    public AfDecimal getL3421CosRunAssvaIdc() {
        return readPackedAsDecimal(Pos.L3421_COS_RUN_ASSVA_IDC, Len.Int.L3421_COS_RUN_ASSVA_IDC, Len.Fract.L3421_COS_RUN_ASSVA_IDC);
    }

    public byte[] getL3421CosRunAssvaIdcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_COS_RUN_ASSVA_IDC, Pos.L3421_COS_RUN_ASSVA_IDC);
        return buffer;
    }

    /**Original name: L3421-COS-RUN-ASSVA-IDC-NULL<br>*/
    public String getL3421CosRunAssvaIdcNull() {
        return readString(Pos.L3421_COS_RUN_ASSVA_IDC_NULL, Len.L3421_COS_RUN_ASSVA_IDC_NULL);
    }

    public String getL3421CosRunAssvaIdcNullFormatted() {
        return Functions.padBlanks(getL3421CosRunAssvaIdcNull(), Len.L3421_COS_RUN_ASSVA_IDC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_COS_RUN_ASSVA_IDC = 1;
        public static final int L3421_COS_RUN_ASSVA_IDC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_COS_RUN_ASSVA_IDC = 8;
        public static final int L3421_COS_RUN_ASSVA_IDC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_COS_RUN_ASSVA_IDC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_COS_RUN_ASSVA_IDC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
