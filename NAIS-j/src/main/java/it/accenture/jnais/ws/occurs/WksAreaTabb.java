package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WKS-AREA-TABB<br>
 * Variables: WKS-AREA-TABB from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WksAreaTabb {

    //==== PROPERTIES ====
    public static final int ELEMENTS_STR_DATO_MAXOCCURS = 250;
    //Original name: WKS-NOME-TABB
    private String nomeTabb = DefaultValues.stringVal(Len.NOME_TABB);
    //Original name: WKS-ELE-MAX-DATO
    private short eleMaxDato = DefaultValues.SHORT_VAL;
    //Original name: WKS-ELEMENTS-STR-DATO
    private WksElementsStrDato[] elementsStrDato = new WksElementsStrDato[ELEMENTS_STR_DATO_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WksAreaTabb() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int elementsStrDatoIdx = 1; elementsStrDatoIdx <= ELEMENTS_STR_DATO_MAXOCCURS; elementsStrDatoIdx++) {
            elementsStrDato[elementsStrDatoIdx - 1] = new WksElementsStrDato();
        }
    }

    public void setNomeTabb(String nomeTabb) {
        this.nomeTabb = Functions.subString(nomeTabb, Len.NOME_TABB);
    }

    public String getNomeTabb() {
        return this.nomeTabb;
    }

    public void setEleMaxDato(short eleMaxDato) {
        this.eleMaxDato = eleMaxDato;
    }

    public short getEleMaxDato() {
        return this.eleMaxDato;
    }

    public WksElementsStrDato getElementsStrDato(int idx) {
        return elementsStrDato[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NOME_TABB = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
