package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-EFF-RIDZ<br>
 * Variable: B03-DT-EFF-RIDZ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtEffRidz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtEffRidz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_EFF_RIDZ;
    }

    public void setB03DtEffRidz(int b03DtEffRidz) {
        writeIntAsPacked(Pos.B03_DT_EFF_RIDZ, b03DtEffRidz, Len.Int.B03_DT_EFF_RIDZ);
    }

    public void setB03DtEffRidzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_EFF_RIDZ, Pos.B03_DT_EFF_RIDZ);
    }

    /**Original name: B03-DT-EFF-RIDZ<br>*/
    public int getB03DtEffRidz() {
        return readPackedAsInt(Pos.B03_DT_EFF_RIDZ, Len.Int.B03_DT_EFF_RIDZ);
    }

    public byte[] getB03DtEffRidzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_EFF_RIDZ, Pos.B03_DT_EFF_RIDZ);
        return buffer;
    }

    public void setB03DtEffRidzNull(String b03DtEffRidzNull) {
        writeString(Pos.B03_DT_EFF_RIDZ_NULL, b03DtEffRidzNull, Len.B03_DT_EFF_RIDZ_NULL);
    }

    /**Original name: B03-DT-EFF-RIDZ-NULL<br>*/
    public String getB03DtEffRidzNull() {
        return readString(Pos.B03_DT_EFF_RIDZ_NULL, Len.B03_DT_EFF_RIDZ_NULL);
    }

    public String getB03DtEffRidzNullFormatted() {
        return Functions.padBlanks(getB03DtEffRidzNull(), Len.B03_DT_EFF_RIDZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_EFF_RIDZ = 1;
        public static final int B03_DT_EFF_RIDZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_EFF_RIDZ = 5;
        public static final int B03_DT_EFF_RIDZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_EFF_RIDZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
