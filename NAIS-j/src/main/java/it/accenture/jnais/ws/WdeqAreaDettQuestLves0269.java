package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WdeqTabDettQuest;

/**Original name: WDEQ-AREA-DETT-QUEST<br>
 * Variable: WDEQ-AREA-DETT-QUEST from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WdeqAreaDettQuestLves0269 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_DETT_QUEST_MAXOCCURS = 180;
    //Original name: WDEQ-ELE-DETT-QUEST-MAX
    private short eleDettQuestMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WDEQ-TAB-DETT-QUEST
    private WdeqTabDettQuest[] tabDettQuest = new WdeqTabDettQuest[TAB_DETT_QUEST_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WdeqAreaDettQuestLves0269() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDEQ_AREA_DETT_QUEST;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWdeqAreaDettQuestBytes(buf);
    }

    public void init() {
        for (int tabDettQuestIdx = 1; tabDettQuestIdx <= TAB_DETT_QUEST_MAXOCCURS; tabDettQuestIdx++) {
            tabDettQuest[tabDettQuestIdx - 1] = new WdeqTabDettQuest();
        }
    }

    public String getWdeqAreaDettQuestFormatted() {
        return MarshalByteExt.bufferToStr(getWdeqAreaDettQuestBytes());
    }

    public void setWdeqAreaDettQuestBytes(byte[] buffer) {
        setWdeqAreaDettQuestBytes(buffer, 1);
    }

    public byte[] getWdeqAreaDettQuestBytes() {
        byte[] buffer = new byte[Len.WDEQ_AREA_DETT_QUEST];
        return getWdeqAreaDettQuestBytes(buffer, 1);
    }

    public void setWdeqAreaDettQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        eleDettQuestMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_DETT_QUEST_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabDettQuest[idx - 1].setWdeqTabDettQuestBytes(buffer, position);
                position += WdeqTabDettQuest.Len.WDEQ_TAB_DETT_QUEST;
            }
            else {
                tabDettQuest[idx - 1].initWdeqTabDettQuestSpaces();
                position += WdeqTabDettQuest.Len.WDEQ_TAB_DETT_QUEST;
            }
        }
    }

    public byte[] getWdeqAreaDettQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleDettQuestMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_DETT_QUEST_MAXOCCURS; idx++) {
            tabDettQuest[idx - 1].getWdeqTabDettQuestBytes(buffer, position);
            position += WdeqTabDettQuest.Len.WDEQ_TAB_DETT_QUEST;
        }
        return buffer;
    }

    public void setEleDettQuestMax(short eleDettQuestMax) {
        this.eleDettQuestMax = eleDettQuestMax;
    }

    public short getEleDettQuestMax() {
        return this.eleDettQuestMax;
    }

    public WdeqTabDettQuest getTabDettQuest(int idx) {
        return tabDettQuest[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWdeqAreaDettQuestBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_DETT_QUEST_MAX = 2;
        public static final int WDEQ_AREA_DETT_QUEST = ELE_DETT_QUEST_MAX + WdeqAreaDettQuestLves0269.TAB_DETT_QUEST_MAXOCCURS * WdeqTabDettQuest.Len.WDEQ_TAB_DETT_QUEST;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
