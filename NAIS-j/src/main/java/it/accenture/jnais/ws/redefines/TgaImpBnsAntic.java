package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-BNS-ANTIC<br>
 * Variable: TGA-IMP-BNS-ANTIC from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpBnsAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpBnsAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_BNS_ANTIC;
    }

    public void setTgaImpBnsAntic(AfDecimal tgaImpBnsAntic) {
        writeDecimalAsPacked(Pos.TGA_IMP_BNS_ANTIC, tgaImpBnsAntic.copy());
    }

    public void setTgaImpBnsAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_BNS_ANTIC, Pos.TGA_IMP_BNS_ANTIC);
    }

    /**Original name: TGA-IMP-BNS-ANTIC<br>*/
    public AfDecimal getTgaImpBnsAntic() {
        return readPackedAsDecimal(Pos.TGA_IMP_BNS_ANTIC, Len.Int.TGA_IMP_BNS_ANTIC, Len.Fract.TGA_IMP_BNS_ANTIC);
    }

    public byte[] getTgaImpBnsAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_BNS_ANTIC, Pos.TGA_IMP_BNS_ANTIC);
        return buffer;
    }

    public void setTgaImpBnsAnticNull(String tgaImpBnsAnticNull) {
        writeString(Pos.TGA_IMP_BNS_ANTIC_NULL, tgaImpBnsAnticNull, Len.TGA_IMP_BNS_ANTIC_NULL);
    }

    /**Original name: TGA-IMP-BNS-ANTIC-NULL<br>*/
    public String getTgaImpBnsAnticNull() {
        return readString(Pos.TGA_IMP_BNS_ANTIC_NULL, Len.TGA_IMP_BNS_ANTIC_NULL);
    }

    public String getTgaImpBnsAnticNullFormatted() {
        return Functions.padBlanks(getTgaImpBnsAnticNull(), Len.TGA_IMP_BNS_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_BNS_ANTIC = 1;
        public static final int TGA_IMP_BNS_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_BNS_ANTIC = 8;
        public static final int TGA_IMP_BNS_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_BNS_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_BNS_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
