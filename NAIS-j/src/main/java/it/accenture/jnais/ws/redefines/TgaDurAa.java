package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-DUR-AA<br>
 * Variable: TGA-DUR-AA from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaDurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaDurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_DUR_AA;
    }

    public void setTgaDurAa(int tgaDurAa) {
        writeIntAsPacked(Pos.TGA_DUR_AA, tgaDurAa, Len.Int.TGA_DUR_AA);
    }

    public void setTgaDurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_DUR_AA, Pos.TGA_DUR_AA);
    }

    /**Original name: TGA-DUR-AA<br>*/
    public int getTgaDurAa() {
        return readPackedAsInt(Pos.TGA_DUR_AA, Len.Int.TGA_DUR_AA);
    }

    public byte[] getTgaDurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_DUR_AA, Pos.TGA_DUR_AA);
        return buffer;
    }

    public void setTgaDurAaNull(String tgaDurAaNull) {
        writeString(Pos.TGA_DUR_AA_NULL, tgaDurAaNull, Len.TGA_DUR_AA_NULL);
    }

    /**Original name: TGA-DUR-AA-NULL<br>*/
    public String getTgaDurAaNull() {
        return readString(Pos.TGA_DUR_AA_NULL, Len.TGA_DUR_AA_NULL);
    }

    public String getTgaDurAaNullFormatted() {
        return Functions.padBlanks(getTgaDurAaNull(), Len.TGA_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_DUR_AA = 1;
        public static final int TGA_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_DUR_AA = 3;
        public static final int TGA_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
