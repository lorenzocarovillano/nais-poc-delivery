package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-SOPR-TEC<br>
 * Variable: WTIT-TOT-SOPR-TEC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_SOPR_TEC;
    }

    public void setWtitTotSoprTec(AfDecimal wtitTotSoprTec) {
        writeDecimalAsPacked(Pos.WTIT_TOT_SOPR_TEC, wtitTotSoprTec.copy());
    }

    public void setWtitTotSoprTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_SOPR_TEC, Pos.WTIT_TOT_SOPR_TEC);
    }

    /**Original name: WTIT-TOT-SOPR-TEC<br>*/
    public AfDecimal getWtitTotSoprTec() {
        return readPackedAsDecimal(Pos.WTIT_TOT_SOPR_TEC, Len.Int.WTIT_TOT_SOPR_TEC, Len.Fract.WTIT_TOT_SOPR_TEC);
    }

    public byte[] getWtitTotSoprTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_SOPR_TEC, Pos.WTIT_TOT_SOPR_TEC);
        return buffer;
    }

    public void initWtitTotSoprTecSpaces() {
        fill(Pos.WTIT_TOT_SOPR_TEC, Len.WTIT_TOT_SOPR_TEC, Types.SPACE_CHAR);
    }

    public void setWtitTotSoprTecNull(String wtitTotSoprTecNull) {
        writeString(Pos.WTIT_TOT_SOPR_TEC_NULL, wtitTotSoprTecNull, Len.WTIT_TOT_SOPR_TEC_NULL);
    }

    /**Original name: WTIT-TOT-SOPR-TEC-NULL<br>*/
    public String getWtitTotSoprTecNull() {
        return readString(Pos.WTIT_TOT_SOPR_TEC_NULL, Len.WTIT_TOT_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SOPR_TEC = 1;
        public static final int WTIT_TOT_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SOPR_TEC = 8;
        public static final int WTIT_TOT_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
