package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-EC-UL-IND<br>
 * Variable: WPCO-DT-ULT-EC-UL-IND from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEcUlInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEcUlInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_EC_UL_IND;
    }

    public void setWpcoDtUltEcUlInd(int wpcoDtUltEcUlInd) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_EC_UL_IND, wpcoDtUltEcUlInd, Len.Int.WPCO_DT_ULT_EC_UL_IND);
    }

    public void setDpcoDtUltEcUlIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_EC_UL_IND, Pos.WPCO_DT_ULT_EC_UL_IND);
    }

    /**Original name: WPCO-DT-ULT-EC-UL-IND<br>*/
    public int getWpcoDtUltEcUlInd() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_EC_UL_IND, Len.Int.WPCO_DT_ULT_EC_UL_IND);
    }

    public byte[] getWpcoDtUltEcUlIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_EC_UL_IND, Pos.WPCO_DT_ULT_EC_UL_IND);
        return buffer;
    }

    public void setWpcoDtUltEcUlIndNull(String wpcoDtUltEcUlIndNull) {
        writeString(Pos.WPCO_DT_ULT_EC_UL_IND_NULL, wpcoDtUltEcUlIndNull, Len.WPCO_DT_ULT_EC_UL_IND_NULL);
    }

    /**Original name: WPCO-DT-ULT-EC-UL-IND-NULL<br>*/
    public String getWpcoDtUltEcUlIndNull() {
        return readString(Pos.WPCO_DT_ULT_EC_UL_IND_NULL, Len.WPCO_DT_ULT_EC_UL_IND_NULL);
    }

    public String getWpcoDtUltEcUlIndNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEcUlIndNull(), Len.WPCO_DT_ULT_EC_UL_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_UL_IND = 1;
        public static final int WPCO_DT_ULT_EC_UL_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_UL_IND = 5;
        public static final int WPCO_DT_ULT_EC_UL_IND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_EC_UL_IND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
