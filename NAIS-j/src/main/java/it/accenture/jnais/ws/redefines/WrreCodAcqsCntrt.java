package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRRE-COD-ACQS-CNTRT<br>
 * Variable: WRRE-COD-ACQS-CNTRT from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrreCodAcqsCntrt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrreCodAcqsCntrt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRRE_COD_ACQS_CNTRT;
    }

    public void setWrreCodAcqsCntrt(int wrreCodAcqsCntrt) {
        writeIntAsPacked(Pos.WRRE_COD_ACQS_CNTRT, wrreCodAcqsCntrt, Len.Int.WRRE_COD_ACQS_CNTRT);
    }

    public void setWrreCodAcqsCntrtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRRE_COD_ACQS_CNTRT, Pos.WRRE_COD_ACQS_CNTRT);
    }

    /**Original name: WRRE-COD-ACQS-CNTRT<br>*/
    public int getWrreCodAcqsCntrt() {
        return readPackedAsInt(Pos.WRRE_COD_ACQS_CNTRT, Len.Int.WRRE_COD_ACQS_CNTRT);
    }

    public byte[] getWrreCodAcqsCntrtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRRE_COD_ACQS_CNTRT, Pos.WRRE_COD_ACQS_CNTRT);
        return buffer;
    }

    public void initWrreCodAcqsCntrtSpaces() {
        fill(Pos.WRRE_COD_ACQS_CNTRT, Len.WRRE_COD_ACQS_CNTRT, Types.SPACE_CHAR);
    }

    public void setWrreCodAcqsCntrtNull(String wrreCodAcqsCntrtNull) {
        writeString(Pos.WRRE_COD_ACQS_CNTRT_NULL, wrreCodAcqsCntrtNull, Len.WRRE_COD_ACQS_CNTRT_NULL);
    }

    /**Original name: WRRE-COD-ACQS-CNTRT-NULL<br>*/
    public String getWrreCodAcqsCntrtNull() {
        return readString(Pos.WRRE_COD_ACQS_CNTRT_NULL, Len.WRRE_COD_ACQS_CNTRT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRRE_COD_ACQS_CNTRT = 1;
        public static final int WRRE_COD_ACQS_CNTRT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRRE_COD_ACQS_CNTRT = 3;
        public static final int WRRE_COD_ACQS_CNTRT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRRE_COD_ACQS_CNTRT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
