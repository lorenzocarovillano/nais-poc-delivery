package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Area0140Operazione;

/**Original name: AREA-X-IABS0140<br>
 * Variable: AREA-X-IABS0140 from program IABS0140<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AreaXIabs0140 {

    //==== PROPERTIES ====
    //Original name: AREA0140-DATABASE-NAME
    private String databaseName = DefaultValues.stringVal(Len.DATABASE_NAME);
    //Original name: AREA0140-USER
    private String user = DefaultValues.stringVal(Len.USER);
    //Original name: AREA0140-PASSWORD
    private String password = DefaultValues.stringVal(Len.PASSWORD);
    /**Original name: AREA0140-OPERAZIONE<br>
	 * <pre> -- CAMPI OPERAZIONE</pre>*/
    private Area0140Operazione operazione = new Area0140Operazione();

    //==== METHODS ====
    public void setAreaXIabs0140Formatted(String data) {
        byte[] buffer = new byte[Len.AREA_X_IABS0140];
        MarshalByte.writeString(buffer, 1, data, Len.AREA_X_IABS0140);
        setAreaXIabs0140Bytes(buffer, 1);
    }

    public String getAreaXIabs0140Formatted() {
        return MarshalByteExt.bufferToStr(getAreaXIabs0140Bytes());
    }

    public byte[] getAreaXIabs0140Bytes() {
        byte[] buffer = new byte[Len.AREA_X_IABS0140];
        return getAreaXIabs0140Bytes(buffer, 1);
    }

    public void setAreaXIabs0140Bytes(byte[] buffer, int offset) {
        int position = offset;
        databaseName = MarshalByte.readString(buffer, position, Len.DATABASE_NAME);
        position += Len.DATABASE_NAME;
        user = MarshalByte.readString(buffer, position, Len.USER);
        position += Len.USER;
        password = MarshalByte.readString(buffer, position, Len.PASSWORD);
        position += Len.PASSWORD;
        operazione.setOperazione(MarshalByte.readString(buffer, position, Area0140Operazione.Len.OPERAZIONE));
    }

    public byte[] getAreaXIabs0140Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, databaseName, Len.DATABASE_NAME);
        position += Len.DATABASE_NAME;
        MarshalByte.writeString(buffer, position, user, Len.USER);
        position += Len.USER;
        MarshalByte.writeString(buffer, position, password, Len.PASSWORD);
        position += Len.PASSWORD;
        MarshalByte.writeString(buffer, position, operazione.getOperazione(), Area0140Operazione.Len.OPERAZIONE);
        return buffer;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = Functions.subString(databaseName, Len.DATABASE_NAME);
    }

    public String getDatabaseName() {
        return this.databaseName;
    }

    public String getDatabaseNameFormatted() {
        return Functions.padBlanks(getDatabaseName(), Len.DATABASE_NAME);
    }

    public void setUser(String user) {
        this.user = Functions.subString(user, Len.USER);
    }

    public String getUser() {
        return this.user;
    }

    public String getUserFormatted() {
        return Functions.padBlanks(getUser(), Len.USER);
    }

    public void setPassword(String password) {
        this.password = Functions.subString(password, Len.PASSWORD);
    }

    public String getPassword() {
        return this.password;
    }

    public String getPasswordFormatted() {
        return Functions.padBlanks(getPassword(), Len.PASSWORD);
    }

    public Area0140Operazione getOperazione() {
        return operazione;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DATABASE_NAME = 10;
        public static final int USER = 8;
        public static final int PASSWORD = 8;
        public static final int AREA_X_IABS0140 = DATABASE_NAME + USER + PASSWORD + Area0140Operazione.Len.OPERAZIONE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
