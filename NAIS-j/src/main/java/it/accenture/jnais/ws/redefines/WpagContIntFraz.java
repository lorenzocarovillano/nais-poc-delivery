package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-INT-FRAZ<br>
 * Variable: WPAG-CONT-INT-FRAZ from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContIntFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContIntFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_INT_FRAZ;
    }

    public void setWpagContIntFraz(AfDecimal wpagContIntFraz) {
        writeDecimalAsPacked(Pos.WPAG_CONT_INT_FRAZ, wpagContIntFraz.copy());
    }

    public void setWpagContIntFrazFormatted(String wpagContIntFraz) {
        setWpagContIntFraz(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_INT_FRAZ + Len.Fract.WPAG_CONT_INT_FRAZ, Len.Fract.WPAG_CONT_INT_FRAZ, wpagContIntFraz));
    }

    public void setWpagContIntFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_INT_FRAZ, Pos.WPAG_CONT_INT_FRAZ);
    }

    /**Original name: WPAG-CONT-INT-FRAZ<br>*/
    public AfDecimal getWpagContIntFraz() {
        return readPackedAsDecimal(Pos.WPAG_CONT_INT_FRAZ, Len.Int.WPAG_CONT_INT_FRAZ, Len.Fract.WPAG_CONT_INT_FRAZ);
    }

    public byte[] getWpagContIntFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_INT_FRAZ, Pos.WPAG_CONT_INT_FRAZ);
        return buffer;
    }

    public void initWpagContIntFrazSpaces() {
        fill(Pos.WPAG_CONT_INT_FRAZ, Len.WPAG_CONT_INT_FRAZ, Types.SPACE_CHAR);
    }

    public void setWpagContIntFrazNull(String wpagContIntFrazNull) {
        writeString(Pos.WPAG_CONT_INT_FRAZ_NULL, wpagContIntFrazNull, Len.WPAG_CONT_INT_FRAZ_NULL);
    }

    /**Original name: WPAG-CONT-INT-FRAZ-NULL<br>*/
    public String getWpagContIntFrazNull() {
        return readString(Pos.WPAG_CONT_INT_FRAZ_NULL, Len.WPAG_CONT_INT_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_INT_FRAZ = 1;
        public static final int WPAG_CONT_INT_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_INT_FRAZ = 8;
        public static final int WPAG_CONT_INT_FRAZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_INT_FRAZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_INT_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
