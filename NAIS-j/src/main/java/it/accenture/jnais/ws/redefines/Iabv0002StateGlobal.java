package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;

/**Original name: IABV0002-STATE-GLOBAL<br>
 * Variable: IABV0002-STATE-GLOBAL from program IABS0030<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Iabv0002StateGlobal extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELE_MAXOCCURS = 10;

    //==== CONSTRUCTORS ====
    public Iabv0002StateGlobal() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IABV0002_STATE_GLOBAL;
    }

    public void setIabv0002StateGlobalBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.IABV0002_STATE_GLOBAL, Pos.IABV0002_STATE_GLOBAL);
    }

    public byte[] getIabv0002StateGlobalBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.IABV0002_STATE_GLOBAL, Pos.IABV0002_STATE_GLOBAL);
        return buffer;
    }

    public void setState01(char state01) {
        writeChar(Pos.STATE01, state01);
    }

    /**Original name: IABV0002-STATE-01<br>*/
    public char getState01() {
        return readChar(Pos.STATE01);
    }

    public void setState02(char state02) {
        writeChar(Pos.STATE02, state02);
    }

    /**Original name: IABV0002-STATE-02<br>*/
    public char getState02() {
        return readChar(Pos.STATE02);
    }

    public void setState03(char state03) {
        writeChar(Pos.STATE03, state03);
    }

    /**Original name: IABV0002-STATE-03<br>*/
    public char getState03() {
        return readChar(Pos.STATE03);
    }

    public void setState04(char state04) {
        writeChar(Pos.STATE04, state04);
    }

    /**Original name: IABV0002-STATE-04<br>*/
    public char getState04() {
        return readChar(Pos.STATE04);
    }

    public void setState05(char state05) {
        writeChar(Pos.STATE05, state05);
    }

    /**Original name: IABV0002-STATE-05<br>*/
    public char getState05() {
        return readChar(Pos.STATE05);
    }

    public void setState06(char state06) {
        writeChar(Pos.STATE06, state06);
    }

    /**Original name: IABV0002-STATE-06<br>*/
    public char getState06() {
        return readChar(Pos.STATE06);
    }

    public void setState07(char state07) {
        writeChar(Pos.STATE07, state07);
    }

    /**Original name: IABV0002-STATE-07<br>*/
    public char getState07() {
        return readChar(Pos.STATE07);
    }

    public void setState08(char state08) {
        writeChar(Pos.STATE08, state08);
    }

    /**Original name: IABV0002-STATE-08<br>*/
    public char getState08() {
        return readChar(Pos.STATE08);
    }

    public void setState09(char state09) {
        writeChar(Pos.STATE09, state09);
    }

    /**Original name: IABV0002-STATE-09<br>*/
    public char getState09() {
        return readChar(Pos.STATE09);
    }

    public void setState10(char state10) {
        writeChar(Pos.STATE10, state10);
    }

    /**Original name: IABV0002-STATE-10<br>*/
    public char getState10() {
        return readChar(Pos.STATE10);
    }

    public void setIabv0002StateEle(int iabv0002StateEleIdx, char iabv0002StateEle) {
        int position = Pos.iabv0002StateEle(iabv0002StateEleIdx - 1);
        writeChar(position, iabv0002StateEle);
    }

    /**Original name: IABV0002-STATE-ELE<br>*/
    public char getIabv0002StateEle(int iabv0002StateEleIdx) {
        int position = Pos.iabv0002StateEle(iabv0002StateEleIdx - 1);
        return readChar(position);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int IABV0002_STATE_GLOBAL = 1;
        public static final int STATE01 = IABV0002_STATE_GLOBAL;
        public static final int STATE02 = STATE01 + Len.STATE01;
        public static final int STATE03 = STATE02 + Len.STATE02;
        public static final int STATE04 = STATE03 + Len.STATE03;
        public static final int STATE05 = STATE04 + Len.STATE04;
        public static final int STATE06 = STATE05 + Len.STATE05;
        public static final int STATE07 = STATE06 + Len.STATE06;
        public static final int STATE08 = STATE07 + Len.STATE07;
        public static final int STATE09 = STATE08 + Len.STATE08;
        public static final int STATE10 = STATE09 + Len.STATE09;
        public static final int IABV0002_STATE_TABLE = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int iabv0002StateEle(int idx) {
            return IABV0002_STATE_TABLE + idx * Len.ELE;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATE01 = 1;
        public static final int STATE02 = 1;
        public static final int STATE03 = 1;
        public static final int STATE04 = 1;
        public static final int STATE05 = 1;
        public static final int STATE06 = 1;
        public static final int STATE07 = 1;
        public static final int STATE08 = 1;
        public static final int STATE09 = 1;
        public static final int ELE = 1;
        public static final int STATE10 = 1;
        public static final int IABV0002_STATE_GLOBAL = STATE01 + STATE02 + STATE03 + STATE04 + STATE05 + STATE06 + STATE07 + STATE08 + STATE09 + STATE10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
