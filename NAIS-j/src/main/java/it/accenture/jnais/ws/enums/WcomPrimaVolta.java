package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WCOM-PRIMA-VOLTA<br>
 * Variable: WCOM-PRIMA-VOLTA from copybook LCCC0261<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomPrimaVolta {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setPrimaVolta(char primaVolta) {
        this.value = primaVolta;
    }

    public char getPrimaVolta() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PRIMA_VOLTA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
