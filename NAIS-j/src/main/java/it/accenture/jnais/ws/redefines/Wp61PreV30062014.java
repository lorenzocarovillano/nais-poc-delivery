package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP61-PRE-V-30062014<br>
 * Variable: WP61-PRE-V-30062014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp61PreV30062014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp61PreV30062014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP61_PRE_V30062014;
    }

    public void setWp61PreV30062014(AfDecimal wp61PreV30062014) {
        writeDecimalAsPacked(Pos.WP61_PRE_V30062014, wp61PreV30062014.copy());
    }

    public void setWp61PreV30062014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP61_PRE_V30062014, Pos.WP61_PRE_V30062014);
    }

    /**Original name: WP61-PRE-V-30062014<br>*/
    public AfDecimal getWp61PreV30062014() {
        return readPackedAsDecimal(Pos.WP61_PRE_V30062014, Len.Int.WP61_PRE_V30062014, Len.Fract.WP61_PRE_V30062014);
    }

    public byte[] getWp61PreV30062014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP61_PRE_V30062014, Pos.WP61_PRE_V30062014);
        return buffer;
    }

    public void setWp61PreV30062014Null(String wp61PreV30062014Null) {
        writeString(Pos.WP61_PRE_V30062014_NULL, wp61PreV30062014Null, Len.WP61_PRE_V30062014_NULL);
    }

    /**Original name: WP61-PRE-V-30062014-NULL<br>*/
    public String getWp61PreV30062014Null() {
        return readString(Pos.WP61_PRE_V30062014_NULL, Len.WP61_PRE_V30062014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP61_PRE_V30062014 = 1;
        public static final int WP61_PRE_V30062014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_PRE_V30062014 = 8;
        public static final int WP61_PRE_V30062014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP61_PRE_V30062014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_PRE_V30062014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
