package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-STB-TROVATO<br>
 * Variable: WK-STB-TROVATO from program LOAS0310<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkStbTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkStbTrovato(char wkStbTrovato) {
        this.value = wkStbTrovato;
    }

    public char getWkStbTrovato() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
