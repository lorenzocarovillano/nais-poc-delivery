package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WVAS-NUM-QUO<br>
 * Variable: WVAS-NUM-QUO from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasNumQuo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasNumQuo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_NUM_QUO;
    }

    public void setWvasNumQuo(AfDecimal wvasNumQuo) {
        writeDecimalAsPacked(Pos.WVAS_NUM_QUO, wvasNumQuo.copy());
    }

    /**Original name: WVAS-NUM-QUO<br>*/
    public AfDecimal getWvasNumQuo() {
        return readPackedAsDecimal(Pos.WVAS_NUM_QUO, Len.Int.WVAS_NUM_QUO, Len.Fract.WVAS_NUM_QUO);
    }

    public void setWvasNumQuoNull(String wvasNumQuoNull) {
        writeString(Pos.WVAS_NUM_QUO_NULL, wvasNumQuoNull, Len.WVAS_NUM_QUO_NULL);
    }

    /**Original name: WVAS-NUM-QUO-NULL<br>*/
    public String getWvasNumQuoNull() {
        return readString(Pos.WVAS_NUM_QUO_NULL, Len.WVAS_NUM_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_NUM_QUO = 1;
        public static final int WVAS_NUM_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_NUM_QUO = 7;
        public static final int WVAS_NUM_QUO_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_NUM_QUO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WVAS_NUM_QUO = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
