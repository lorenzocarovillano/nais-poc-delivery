package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-COEFF-RIS-1-T<br>
 * Variable: WB03-COEFF-RIS-1-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CoeffRis1T extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CoeffRis1T() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_COEFF_RIS1_T;
    }

    public void setWb03CoeffRis1T(AfDecimal wb03CoeffRis1T) {
        writeDecimalAsPacked(Pos.WB03_COEFF_RIS1_T, wb03CoeffRis1T.copy());
    }

    public void setWb03CoeffRis1TFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_COEFF_RIS1_T, Pos.WB03_COEFF_RIS1_T);
    }

    /**Original name: WB03-COEFF-RIS-1-T<br>*/
    public AfDecimal getWb03CoeffRis1T() {
        return readPackedAsDecimal(Pos.WB03_COEFF_RIS1_T, Len.Int.WB03_COEFF_RIS1_T, Len.Fract.WB03_COEFF_RIS1_T);
    }

    public byte[] getWb03CoeffRis1TAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_COEFF_RIS1_T, Pos.WB03_COEFF_RIS1_T);
        return buffer;
    }

    public void setWb03CoeffRis1TNull(String wb03CoeffRis1TNull) {
        writeString(Pos.WB03_COEFF_RIS1_T_NULL, wb03CoeffRis1TNull, Len.WB03_COEFF_RIS1_T_NULL);
    }

    /**Original name: WB03-COEFF-RIS-1-T-NULL<br>*/
    public String getWb03CoeffRis1TNull() {
        return readString(Pos.WB03_COEFF_RIS1_T_NULL, Len.WB03_COEFF_RIS1_T_NULL);
    }

    public String getWb03CoeffRis1TNullFormatted() {
        return Functions.padBlanks(getWb03CoeffRis1TNull(), Len.WB03_COEFF_RIS1_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_COEFF_RIS1_T = 1;
        public static final int WB03_COEFF_RIS1_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_COEFF_RIS1_T = 8;
        public static final int WB03_COEFF_RIS1_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_COEFF_RIS1_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_COEFF_RIS1_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
