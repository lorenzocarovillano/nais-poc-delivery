package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WOPZ-GAR-POLIZZA<br>
 * Variables: WOPZ-GAR-POLIZZA from copybook IVVC0217<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WopzGarPolizza {

    //==== PROPERTIES ====
    //Original name: WOPZ-GAR-CODICE
    private String wopzGarCodice = DefaultValues.stringVal(Len.WOPZ_GAR_CODICE);

    //==== METHODS ====
    public void setGarPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        wopzGarCodice = MarshalByte.readString(buffer, position, Len.WOPZ_GAR_CODICE);
    }

    public byte[] getGarPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wopzGarCodice, Len.WOPZ_GAR_CODICE);
        return buffer;
    }

    public void initGarPolizzaSpaces() {
        wopzGarCodice = "";
    }

    public void setWopzGarCodice(String wopzGarCodice) {
        this.wopzGarCodice = Functions.subString(wopzGarCodice, Len.WOPZ_GAR_CODICE);
    }

    public String getWopzGarCodice() {
        return this.wopzGarCodice;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WOPZ_GAR_CODICE = 12;
        public static final int GAR_POLIZZA = WOPZ_GAR_CODICE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
