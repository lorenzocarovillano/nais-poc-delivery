package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RDF-NUM-QUO-CDGTOT-FNZ<br>
 * Variable: RDF-NUM-QUO-CDGTOT-FNZ from program IDBSRDF0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RdfNumQuoCdgtotFnz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RdfNumQuoCdgtotFnz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RDF_NUM_QUO_CDGTOT_FNZ;
    }

    public void setRdfNumQuoCdgtotFnz(AfDecimal rdfNumQuoCdgtotFnz) {
        writeDecimalAsPacked(Pos.RDF_NUM_QUO_CDGTOT_FNZ, rdfNumQuoCdgtotFnz.copy());
    }

    public void setRdfNumQuoCdgtotFnzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RDF_NUM_QUO_CDGTOT_FNZ, Pos.RDF_NUM_QUO_CDGTOT_FNZ);
    }

    /**Original name: RDF-NUM-QUO-CDGTOT-FNZ<br>*/
    public AfDecimal getRdfNumQuoCdgtotFnz() {
        return readPackedAsDecimal(Pos.RDF_NUM_QUO_CDGTOT_FNZ, Len.Int.RDF_NUM_QUO_CDGTOT_FNZ, Len.Fract.RDF_NUM_QUO_CDGTOT_FNZ);
    }

    public byte[] getRdfNumQuoCdgtotFnzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RDF_NUM_QUO_CDGTOT_FNZ, Pos.RDF_NUM_QUO_CDGTOT_FNZ);
        return buffer;
    }

    public void setRdfNumQuoCdgtotFnzNull(String rdfNumQuoCdgtotFnzNull) {
        writeString(Pos.RDF_NUM_QUO_CDGTOT_FNZ_NULL, rdfNumQuoCdgtotFnzNull, Len.RDF_NUM_QUO_CDGTOT_FNZ_NULL);
    }

    /**Original name: RDF-NUM-QUO-CDGTOT-FNZ-NULL<br>*/
    public String getRdfNumQuoCdgtotFnzNull() {
        return readString(Pos.RDF_NUM_QUO_CDGTOT_FNZ_NULL, Len.RDF_NUM_QUO_CDGTOT_FNZ_NULL);
    }

    public String getRdfNumQuoCdgtotFnzNullFormatted() {
        return Functions.padBlanks(getRdfNumQuoCdgtotFnzNull(), Len.RDF_NUM_QUO_CDGTOT_FNZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RDF_NUM_QUO_CDGTOT_FNZ = 1;
        public static final int RDF_NUM_QUO_CDGTOT_FNZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_NUM_QUO_CDGTOT_FNZ = 7;
        public static final int RDF_NUM_QUO_CDGTOT_FNZ_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_NUM_QUO_CDGTOT_FNZ = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RDF_NUM_QUO_CDGTOT_FNZ = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
