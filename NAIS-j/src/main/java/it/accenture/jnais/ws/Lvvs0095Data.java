package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Ldbv2441;
import it.accenture.jnais.ws.enums.WkPolizza;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0095;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0095<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0095Data {

    //==== PROPERTIES ====
    public static final int DTGA_TAB_TRAN_MAXOCCURS = 1250;
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-GG-APP<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private int wkGgApp = 0;
    //Original name: WK-DATA-OUT
    private String wkDataOut = "00000000";
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    /**Original name: FORMATO<br>
	 * <pre>-- AREA ROUTINE PER IL CALCOLO</pre>*/
    private char formato = DefaultValues.CHAR_VAL;
    //Original name: DATA-INFERIORE
    private DataInferioreLccs0490 dataInferiore = new DataInferioreLccs0490();
    //Original name: DATA-SUPERIORE
    private DataSuperioreLccs0490 dataSuperiore = new DataSuperioreLccs0490();
    //Original name: GG-DIFF
    private String ggDiff = DefaultValues.stringVal(Len.GG_DIFF);
    //Original name: CODICE-RITORNO
    private char codiceRitorno = DefaultValues.CHAR_VAL;
    //Original name: WK-POLIZZA
    private WkPolizza wkPolizza = new WkPolizza();
    //Original name: LDBV2971
    private Ldbv2971 ldbv2971 = new Ldbv2971();
    //Original name: LDBV2961
    private Ldbv2441 ldbv2961 = new Ldbv2441();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>    COPY LCCVXMV0.
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0095 wsMovimento = new WsMovimentoLvvs0095();
    //Original name: STAT-OGG-BUS
    private StatOggBusIdbsstb0 statOggBus = new StatOggBusIdbsstb0();
    //Original name: DTGA-ELE-TGA-MAX
    private short dtgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTGA-TAB-TRAN
    private LazyArrayCopy<W1tgaTabTran> dtgaTabTran = new LazyArrayCopy<W1tgaTabTran>(new W1tgaTabTran(), 1, DTGA_TAB_TRAN_MAXOCCURS);
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-TGA
    private short ixTabTga = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkGgApp(int wkGgApp) {
        this.wkGgApp = wkGgApp;
    }

    public int getWkGgApp() {
        return this.wkGgApp;
    }

    public void setWkDataOut(int wkDataOut) {
        this.wkDataOut = NumericDisplay.asString(wkDataOut, Len.WK_DATA_OUT);
    }

    public void setWkDataOutFormatted(String wkDataOut) {
        this.wkDataOut = Trunc.toUnsignedNumeric(wkDataOut, Len.WK_DATA_OUT);
    }

    public int getWkDataOut() {
        return NumericDisplay.asInt(this.wkDataOut);
    }

    public String getWkDataOutFormatted() {
        return this.wkDataOut;
    }

    public void setFormato(char formato) {
        this.formato = formato;
    }

    public void setFormatoFormatted(String formato) {
        setFormato(Functions.charAt(formato, Types.CHAR_SIZE));
    }

    public void setFormatoFromBuffer(byte[] buffer) {
        formato = MarshalByte.readChar(buffer, 1);
    }

    public char getFormato() {
        return this.formato;
    }

    public void setGgDiffFromBuffer(byte[] buffer) {
        ggDiff = MarshalByte.readFixedString(buffer, 1, Len.GG_DIFF);
    }

    public int getGgDiff() {
        return NumericDisplay.asInt(this.ggDiff);
    }

    public String getGgDiffFormatted() {
        return this.ggDiff;
    }

    public void setCodiceRitorno(char codiceRitorno) {
        this.codiceRitorno = codiceRitorno;
    }

    public void setCodiceRitornoFromBuffer(byte[] buffer) {
        codiceRitorno = MarshalByte.readChar(buffer, 1);
    }

    public char getCodiceRitorno() {
        return this.codiceRitorno;
    }

    public void setDtgaAreaTgaFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TGA];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TGA);
        setDtgaAreaTgaBytes(buffer, 1);
    }

    public void setDtgaAreaTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleTgaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DTGA_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dtgaTabTran.get(idx - 1).setW1tgaTabTranBytes(buffer, position);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN;
            }
            else {
                W1tgaTabTran temp_dtgaTabTran = new W1tgaTabTran();
                temp_dtgaTabTran.initW1tgaTabTranSpaces();
                getDtgaTabTranObj().fill(temp_dtgaTabTran);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN * (DTGA_TAB_TRAN_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public void setDtgaEleTgaMax(short dtgaEleTgaMax) {
        this.dtgaEleTgaMax = dtgaEleTgaMax;
    }

    public short getDtgaEleTgaMax() {
        return this.dtgaEleTgaMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabTga(short ixTabTga) {
        this.ixTabTga = ixTabTga;
    }

    public short getIxTabTga() {
        return this.ixTabTga;
    }

    public DataInferioreLccs0490 getDataInferiore() {
        return dataInferiore;
    }

    public DataSuperioreLccs0490 getDataSuperiore() {
        return dataSuperiore;
    }

    public W1tgaTabTran getDtgaTabTran(int idx) {
        return dtgaTabTran.get(idx - 1);
    }

    public LazyArrayCopy<W1tgaTabTran> getDtgaTabTranObj() {
        return dtgaTabTran;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Ldbv2441 getLdbv2961() {
        return ldbv2961;
    }

    public Ldbv2971 getLdbv2971() {
        return ldbv2971;
    }

    public StatOggBusIdbsstb0 getStatOggBus() {
        return statOggBus;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    public WkPolizza getWkPolizza() {
        return wkPolizza;
    }

    public WsMovimentoLvvs0095 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_DATA_OUT = 8;
        public static final int GG_DIFF = 5;
        public static final int DTGA_ELE_TGA_MAX = 2;
        public static final int DTGA_AREA_TGA = DTGA_ELE_TGA_MAX + Lvvs0095Data.DTGA_TAB_TRAN_MAXOCCURS * W1tgaTabTran.Len.W1TGA_TAB_TRAN;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
