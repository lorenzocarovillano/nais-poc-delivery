package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-FRAZ-DECR-CPT<br>
 * Variable: B03-FRAZ-DECR-CPT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03FrazDecrCpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03FrazDecrCpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_FRAZ_DECR_CPT;
    }

    public void setB03FrazDecrCpt(int b03FrazDecrCpt) {
        writeIntAsPacked(Pos.B03_FRAZ_DECR_CPT, b03FrazDecrCpt, Len.Int.B03_FRAZ_DECR_CPT);
    }

    public void setB03FrazDecrCptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_FRAZ_DECR_CPT, Pos.B03_FRAZ_DECR_CPT);
    }

    /**Original name: B03-FRAZ-DECR-CPT<br>*/
    public int getB03FrazDecrCpt() {
        return readPackedAsInt(Pos.B03_FRAZ_DECR_CPT, Len.Int.B03_FRAZ_DECR_CPT);
    }

    public byte[] getB03FrazDecrCptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_FRAZ_DECR_CPT, Pos.B03_FRAZ_DECR_CPT);
        return buffer;
    }

    public void setB03FrazDecrCptNull(String b03FrazDecrCptNull) {
        writeString(Pos.B03_FRAZ_DECR_CPT_NULL, b03FrazDecrCptNull, Len.B03_FRAZ_DECR_CPT_NULL);
    }

    /**Original name: B03-FRAZ-DECR-CPT-NULL<br>*/
    public String getB03FrazDecrCptNull() {
        return readString(Pos.B03_FRAZ_DECR_CPT_NULL, Len.B03_FRAZ_DECR_CPT_NULL);
    }

    public String getB03FrazDecrCptNullFormatted() {
        return Functions.padBlanks(getB03FrazDecrCptNull(), Len.B03_FRAZ_DECR_CPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_FRAZ_DECR_CPT = 1;
        public static final int B03_FRAZ_DECR_CPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_FRAZ_DECR_CPT = 3;
        public static final int B03_FRAZ_DECR_CPT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_FRAZ_DECR_CPT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
