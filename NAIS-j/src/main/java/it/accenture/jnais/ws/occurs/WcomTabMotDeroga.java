package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WCOM-TAB-MOT-DEROGA<br>
 * Variables: WCOM-TAB-MOT-DEROGA from copybook LCCC0001<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WcomTabMotDeroga {

    //==== PROPERTIES ====
    //Original name: WCOM-STATUS
    private WpolStatus status = new WpolStatus();
    //Original name: WCOM-ID-MOT-DEROGA
    private int idMotDeroga = DefaultValues.INT_VAL;
    //Original name: WCOM-COD-ERR
    private String codErr = DefaultValues.stringVal(Len.COD_ERR);
    //Original name: WCOM-TP-ERR
    private String tpErr = DefaultValues.stringVal(Len.TP_ERR);
    //Original name: WCOM-DESCRIZIONE-ERR
    private String descrizioneErr = DefaultValues.stringVal(Len.DESCRIZIONE_ERR);
    //Original name: WCOM-TP-MOT-DEROGA
    private String tpMotDeroga = DefaultValues.stringVal(Len.TP_MOT_DEROGA);
    //Original name: WCOM-COD-LIV-AUTORIZZATIVO
    private int codLivAutorizzativo = DefaultValues.INT_VAL;
    //Original name: WCOM-IDC-FORZABILITA
    private char idcForzabilita = DefaultValues.CHAR_VAL;
    //Original name: WCOM-MOT-DER-DT-EFFETTO
    private int motDerDtEffetto = DefaultValues.INT_VAL;
    //Original name: WCOM-MOT-DER-DT-COMPETENZA
    private int motDerDtCompetenza = DefaultValues.INT_VAL;
    //Original name: WCOM-MOT-DER-DS-VER
    private int motDerDsVer = DefaultValues.INT_VAL;
    //Original name: WCOM-MOT-DER-STEP-ELAB
    private char motDerStepElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setWcomTabMotDerogaBytes(byte[] buffer) {
        setTabMotDerogaBytes(buffer, 1);
    }

    public byte[] getWcomTabMotDerogaBytes() {
        byte[] buffer = new byte[Len.TAB_MOT_DEROGA];
        return getTabMotDerogaBytes(buffer, 1);
    }

    public void setTabMotDerogaBytes(byte[] buffer, int offset) {
        int position = offset;
        status.setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        idMotDeroga = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOT_DEROGA, 0);
        position += Len.ID_MOT_DEROGA;
        codErr = MarshalByte.readString(buffer, position, Len.COD_ERR);
        position += Len.COD_ERR;
        tpErr = MarshalByte.readString(buffer, position, Len.TP_ERR);
        position += Len.TP_ERR;
        descrizioneErr = MarshalByte.readString(buffer, position, Len.DESCRIZIONE_ERR);
        position += Len.DESCRIZIONE_ERR;
        tpMotDeroga = MarshalByte.readString(buffer, position, Len.TP_MOT_DEROGA);
        position += Len.TP_MOT_DEROGA;
        codLivAutorizzativo = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_LIV_AUTORIZZATIVO, 0);
        position += Len.COD_LIV_AUTORIZZATIVO;
        idcForzabilita = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        motDerDtEffetto = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOT_DER_DT_EFFETTO, 0);
        position += Len.MOT_DER_DT_EFFETTO;
        motDerDtCompetenza = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOT_DER_DT_COMPETENZA, 0);
        position += Len.MOT_DER_DT_COMPETENZA;
        motDerDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOT_DER_DS_VER, 0);
        position += Len.MOT_DER_DS_VER;
        motDerStepElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getTabMotDerogaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, status.getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, idMotDeroga, Len.Int.ID_MOT_DEROGA, 0);
        position += Len.ID_MOT_DEROGA;
        MarshalByte.writeString(buffer, position, codErr, Len.COD_ERR);
        position += Len.COD_ERR;
        MarshalByte.writeString(buffer, position, tpErr, Len.TP_ERR);
        position += Len.TP_ERR;
        MarshalByte.writeString(buffer, position, descrizioneErr, Len.DESCRIZIONE_ERR);
        position += Len.DESCRIZIONE_ERR;
        MarshalByte.writeString(buffer, position, tpMotDeroga, Len.TP_MOT_DEROGA);
        position += Len.TP_MOT_DEROGA;
        MarshalByte.writeIntAsPacked(buffer, position, codLivAutorizzativo, Len.Int.COD_LIV_AUTORIZZATIVO, 0);
        position += Len.COD_LIV_AUTORIZZATIVO;
        MarshalByte.writeChar(buffer, position, idcForzabilita);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, motDerDtEffetto, Len.Int.MOT_DER_DT_EFFETTO, 0);
        position += Len.MOT_DER_DT_EFFETTO;
        MarshalByte.writeIntAsPacked(buffer, position, motDerDtCompetenza, Len.Int.MOT_DER_DT_COMPETENZA, 0);
        position += Len.MOT_DER_DT_COMPETENZA;
        MarshalByte.writeIntAsPacked(buffer, position, motDerDsVer, Len.Int.MOT_DER_DS_VER, 0);
        position += Len.MOT_DER_DS_VER;
        MarshalByte.writeChar(buffer, position, motDerStepElab);
        return buffer;
    }

    public void initTabMotDerogaSpaces() {
        status.setStatus(Types.SPACE_CHAR);
        idMotDeroga = Types.INVALID_INT_VAL;
        codErr = "";
        tpErr = "";
        descrizioneErr = "";
        tpMotDeroga = "";
        codLivAutorizzativo = Types.INVALID_INT_VAL;
        idcForzabilita = Types.SPACE_CHAR;
        motDerDtEffetto = Types.INVALID_INT_VAL;
        motDerDtCompetenza = Types.INVALID_INT_VAL;
        motDerDsVer = Types.INVALID_INT_VAL;
        motDerStepElab = Types.SPACE_CHAR;
    }

    public void setIdMotDeroga(int idMotDeroga) {
        this.idMotDeroga = idMotDeroga;
    }

    public int getIdMotDeroga() {
        return this.idMotDeroga;
    }

    public void setCodErr(String codErr) {
        this.codErr = Functions.subString(codErr, Len.COD_ERR);
    }

    public String getCodErr() {
        return this.codErr;
    }

    public void setTpErr(String tpErr) {
        this.tpErr = Functions.subString(tpErr, Len.TP_ERR);
    }

    public String getTpErr() {
        return this.tpErr;
    }

    public void setDescrizioneErr(String descrizioneErr) {
        this.descrizioneErr = Functions.subString(descrizioneErr, Len.DESCRIZIONE_ERR);
    }

    public String getDescrizioneErr() {
        return this.descrizioneErr;
    }

    public void setTpMotDeroga(String tpMotDeroga) {
        this.tpMotDeroga = Functions.subString(tpMotDeroga, Len.TP_MOT_DEROGA);
    }

    public String getTpMotDeroga() {
        return this.tpMotDeroga;
    }

    public void setCodLivAutorizzativo(int codLivAutorizzativo) {
        this.codLivAutorizzativo = codLivAutorizzativo;
    }

    public int getCodLivAutorizzativo() {
        return this.codLivAutorizzativo;
    }

    public void setIdcForzabilita(char idcForzabilita) {
        this.idcForzabilita = idcForzabilita;
    }

    public char getIdcForzabilita() {
        return this.idcForzabilita;
    }

    public void setMotDerDtEffetto(int motDerDtEffetto) {
        this.motDerDtEffetto = motDerDtEffetto;
    }

    public int getMotDerDtEffetto() {
        return this.motDerDtEffetto;
    }

    public void setMotDerDtCompetenza(int motDerDtCompetenza) {
        this.motDerDtCompetenza = motDerDtCompetenza;
    }

    public int getMotDerDtCompetenza() {
        return this.motDerDtCompetenza;
    }

    public void setMotDerDsVer(int motDerDsVer) {
        this.motDerDsVer = motDerDsVer;
    }

    public int getMotDerDsVer() {
        return this.motDerDsVer;
    }

    public void setMotDerStepElab(char motDerStepElab) {
        this.motDerStepElab = motDerStepElab;
    }

    public char getMotDerStepElab() {
        return this.motDerStepElab;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_MOT_DEROGA = 5;
        public static final int COD_ERR = 12;
        public static final int TP_ERR = 2;
        public static final int DESCRIZIONE_ERR = 50;
        public static final int TP_MOT_DEROGA = 2;
        public static final int COD_LIV_AUTORIZZATIVO = 3;
        public static final int IDC_FORZABILITA = 1;
        public static final int MOT_DER_DT_EFFETTO = 5;
        public static final int MOT_DER_DT_COMPETENZA = 5;
        public static final int MOT_DER_DS_VER = 5;
        public static final int MOT_DER_STEP_ELAB = 1;
        public static final int TAB_MOT_DEROGA = WpolStatus.Len.STATUS + ID_MOT_DEROGA + COD_ERR + TP_ERR + DESCRIZIONE_ERR + TP_MOT_DEROGA + COD_LIV_AUTORIZZATIVO + IDC_FORZABILITA + MOT_DER_DT_EFFETTO + MOT_DER_DT_COMPETENZA + MOT_DER_DS_VER + MOT_DER_STEP_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_MOT_DEROGA = 9;
            public static final int COD_LIV_AUTORIZZATIVO = 5;
            public static final int MOT_DER_DT_EFFETTO = 8;
            public static final int MOT_DER_DT_COMPETENZA = 8;
            public static final int MOT_DER_DS_VER = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
