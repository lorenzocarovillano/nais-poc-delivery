package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-IS-EFFLQ<br>
 * Variable: DFL-IMPB-IS-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbIsEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbIsEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_IS_EFFLQ;
    }

    public void setDflImpbIsEfflq(AfDecimal dflImpbIsEfflq) {
        writeDecimalAsPacked(Pos.DFL_IMPB_IS_EFFLQ, dflImpbIsEfflq.copy());
    }

    public void setDflImpbIsEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_IS_EFFLQ, Pos.DFL_IMPB_IS_EFFLQ);
    }

    /**Original name: DFL-IMPB-IS-EFFLQ<br>*/
    public AfDecimal getDflImpbIsEfflq() {
        return readPackedAsDecimal(Pos.DFL_IMPB_IS_EFFLQ, Len.Int.DFL_IMPB_IS_EFFLQ, Len.Fract.DFL_IMPB_IS_EFFLQ);
    }

    public byte[] getDflImpbIsEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_IS_EFFLQ, Pos.DFL_IMPB_IS_EFFLQ);
        return buffer;
    }

    public void setDflImpbIsEfflqNull(String dflImpbIsEfflqNull) {
        writeString(Pos.DFL_IMPB_IS_EFFLQ_NULL, dflImpbIsEfflqNull, Len.DFL_IMPB_IS_EFFLQ_NULL);
    }

    /**Original name: DFL-IMPB-IS-EFFLQ-NULL<br>*/
    public String getDflImpbIsEfflqNull() {
        return readString(Pos.DFL_IMPB_IS_EFFLQ_NULL, Len.DFL_IMPB_IS_EFFLQ_NULL);
    }

    public String getDflImpbIsEfflqNullFormatted() {
        return Functions.padBlanks(getDflImpbIsEfflqNull(), Len.DFL_IMPB_IS_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_IS_EFFLQ = 1;
        public static final int DFL_IMPB_IS_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_IS_EFFLQ = 8;
        public static final int DFL_IMPB_IS_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_IS_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_IS_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
