package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L23-NUM-PROVV-SEQ<br>
 * Variable: L23-NUM-PROVV-SEQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L23NumProvvSeq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L23NumProvvSeq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L23_NUM_PROVV_SEQ;
    }

    public void setL23NumProvvSeq(int l23NumProvvSeq) {
        writeIntAsPacked(Pos.L23_NUM_PROVV_SEQ, l23NumProvvSeq, Len.Int.L23_NUM_PROVV_SEQ);
    }

    public void setL23NumProvvSeqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L23_NUM_PROVV_SEQ, Pos.L23_NUM_PROVV_SEQ);
    }

    /**Original name: L23-NUM-PROVV-SEQ<br>*/
    public int getL23NumProvvSeq() {
        return readPackedAsInt(Pos.L23_NUM_PROVV_SEQ, Len.Int.L23_NUM_PROVV_SEQ);
    }

    public byte[] getL23NumProvvSeqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L23_NUM_PROVV_SEQ, Pos.L23_NUM_PROVV_SEQ);
        return buffer;
    }

    public void setL23NumProvvSeqNull(String l23NumProvvSeqNull) {
        writeString(Pos.L23_NUM_PROVV_SEQ_NULL, l23NumProvvSeqNull, Len.L23_NUM_PROVV_SEQ_NULL);
    }

    /**Original name: L23-NUM-PROVV-SEQ-NULL<br>*/
    public String getL23NumProvvSeqNull() {
        return readString(Pos.L23_NUM_PROVV_SEQ_NULL, Len.L23_NUM_PROVV_SEQ_NULL);
    }

    public String getL23NumProvvSeqNullFormatted() {
        return Functions.padBlanks(getL23NumProvvSeqNull(), Len.L23_NUM_PROVV_SEQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L23_NUM_PROVV_SEQ = 1;
        public static final int L23_NUM_PROVV_SEQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L23_NUM_PROVV_SEQ = 5;
        public static final int L23_NUM_PROVV_SEQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L23_NUM_PROVV_SEQ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
