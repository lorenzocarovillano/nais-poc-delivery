package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-VARIABILI<br>
 * Variable: WK-VARIABILI from program LCCS0023<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVariabiliLccs0023 {

    //==== PROPERTIES ====
    //Original name: WK-TABELLA
    private String wkTabella = DefaultValues.stringVal(Len.WK_TABELLA);
    //Original name: WS-TP-GRAVITA-MOVIMENTO
    private String wsTpGravitaMovimento = DefaultValues.stringVal(Len.WS_TP_GRAVITA_MOVIMENTO);
    //Original name: WK-DT-EFF
    private int wkDtEff = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public void setWsTpGravitaMovimento(String wsTpGravitaMovimento) {
        this.wsTpGravitaMovimento = Functions.subString(wsTpGravitaMovimento, Len.WS_TP_GRAVITA_MOVIMENTO);
    }

    public String getWsTpGravitaMovimento() {
        return this.wsTpGravitaMovimento;
    }

    public void setWkDtEff(int wkDtEff) {
        this.wkDtEff = wkDtEff;
    }

    public int getWkDtEff() {
        return this.wkDtEff;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TABELLA = 20;
        public static final int WS_TP_GRAVITA_MOVIMENTO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
