package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPB-TFR<br>
 * Variable: TCL-IMPB-TFR from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpbTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpbTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPB_TFR;
    }

    public void setTclImpbTfr(AfDecimal tclImpbTfr) {
        writeDecimalAsPacked(Pos.TCL_IMPB_TFR, tclImpbTfr.copy());
    }

    public void setTclImpbTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPB_TFR, Pos.TCL_IMPB_TFR);
    }

    /**Original name: TCL-IMPB-TFR<br>*/
    public AfDecimal getTclImpbTfr() {
        return readPackedAsDecimal(Pos.TCL_IMPB_TFR, Len.Int.TCL_IMPB_TFR, Len.Fract.TCL_IMPB_TFR);
    }

    public byte[] getTclImpbTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPB_TFR, Pos.TCL_IMPB_TFR);
        return buffer;
    }

    public void setTclImpbTfrNull(String tclImpbTfrNull) {
        writeString(Pos.TCL_IMPB_TFR_NULL, tclImpbTfrNull, Len.TCL_IMPB_TFR_NULL);
    }

    /**Original name: TCL-IMPB-TFR-NULL<br>*/
    public String getTclImpbTfrNull() {
        return readString(Pos.TCL_IMPB_TFR_NULL, Len.TCL_IMPB_TFR_NULL);
    }

    public String getTclImpbTfrNullFormatted() {
        return Functions.padBlanks(getTclImpbTfrNull(), Len.TCL_IMPB_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_TFR = 1;
        public static final int TCL_IMPB_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_TFR = 8;
        public static final int TCL_IMPB_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
