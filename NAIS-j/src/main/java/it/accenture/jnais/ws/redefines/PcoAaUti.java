package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-AA-UTI<br>
 * Variable: PCO-AA-UTI from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoAaUti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoAaUti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_AA_UTI;
    }

    public void setPcoAaUti(short pcoAaUti) {
        writeShortAsPacked(Pos.PCO_AA_UTI, pcoAaUti, Len.Int.PCO_AA_UTI);
    }

    public void setPcoAaUtiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_AA_UTI, Pos.PCO_AA_UTI);
    }

    /**Original name: PCO-AA-UTI<br>*/
    public short getPcoAaUti() {
        return readPackedAsShort(Pos.PCO_AA_UTI, Len.Int.PCO_AA_UTI);
    }

    public byte[] getPcoAaUtiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_AA_UTI, Pos.PCO_AA_UTI);
        return buffer;
    }

    public void initPcoAaUtiHighValues() {
        fill(Pos.PCO_AA_UTI, Len.PCO_AA_UTI, Types.HIGH_CHAR_VAL);
    }

    public void setPcoAaUtiNull(String pcoAaUtiNull) {
        writeString(Pos.PCO_AA_UTI_NULL, pcoAaUtiNull, Len.PCO_AA_UTI_NULL);
    }

    /**Original name: PCO-AA-UTI-NULL<br>*/
    public String getPcoAaUtiNull() {
        return readString(Pos.PCO_AA_UTI_NULL, Len.PCO_AA_UTI_NULL);
    }

    public String getPcoAaUtiNullFormatted() {
        return Functions.padBlanks(getPcoAaUtiNull(), Len.PCO_AA_UTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_AA_UTI = 1;
        public static final int PCO_AA_UTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_AA_UTI = 3;
        public static final int PCO_AA_UTI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_AA_UTI = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
