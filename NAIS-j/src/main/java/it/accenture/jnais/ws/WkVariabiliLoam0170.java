package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-VARIABILI<br>
 * Variable: WK-VARIABILI from program LOAM0170<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVariabiliLoam0170 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-PMO
    private short ixTabPmo = ((short)0);
    //Original name: WK-ID-POLI
    private int wkIdPoli = 0;
    //Original name: WK-ID-ADES
    private int wkIdAdes = 0;
    //Original name: WK-DATA-APPO-AA
    private String wkDataAppoAa = DefaultValues.stringVal(Len.WK_DATA_APPO_AA);
    //Original name: WK-DATA-APPO-MM
    private String wkDataAppoMm = DefaultValues.stringVal(Len.WK_DATA_APPO_MM);
    //Original name: WK-DATA-APPO-GG
    private String wkDataAppoGg = DefaultValues.stringVal(Len.WK_DATA_APPO_GG);

    //==== METHODS ====
    public void setIxTabPmo(short ixTabPmo) {
        this.ixTabPmo = ixTabPmo;
    }

    public short getIxTabPmo() {
        return this.ixTabPmo;
    }

    public void setWkIdPoli(int wkIdPoli) {
        this.wkIdPoli = wkIdPoli;
    }

    public void setWkIdPoliFromBuffer(byte[] buffer) {
        wkIdPoli = MarshalByte.readPackedAsInt(buffer, 1, Len.Int.WK_ID_POLI, 0);
    }

    public int getWkIdPoli() {
        return this.wkIdPoli;
    }

    public String getWkIdPoliFormatted() {
        return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.PACKED)).format(getWkIdPoli()).toString();
    }

    public void setWkIdAdes(int wkIdAdes) {
        this.wkIdAdes = wkIdAdes;
    }

    public void setWkIdAdesFromBuffer(byte[] buffer) {
        wkIdAdes = MarshalByte.readPackedAsInt(buffer, 1, Len.Int.WK_ID_ADES, 0);
    }

    public int getWkIdAdes() {
        return this.wkIdAdes;
    }

    public String getWkIdAdesFormatted() {
        return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.PACKED)).format(getWkIdAdes()).toString();
    }

    /**Original name: WK-DATA-APPO<br>*/
    public byte[] getWkDataAppoBytes() {
        byte[] buffer = new byte[Len.WK_DATA_APPO];
        return getWkDataAppoBytes(buffer, 1);
    }

    public byte[] getWkDataAppoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wkDataAppoAa, Len.WK_DATA_APPO_AA);
        position += Len.WK_DATA_APPO_AA;
        MarshalByte.writeString(buffer, position, wkDataAppoMm, Len.WK_DATA_APPO_MM);
        position += Len.WK_DATA_APPO_MM;
        MarshalByte.writeString(buffer, position, wkDataAppoGg, Len.WK_DATA_APPO_GG);
        return buffer;
    }

    public void setWkDataAppoAa(short wkDataAppoAa) {
        this.wkDataAppoAa = NumericDisplay.asString(wkDataAppoAa, Len.WK_DATA_APPO_AA);
    }

    public void setWkDataAppoAaFormatted(String wkDataAppoAa) {
        this.wkDataAppoAa = Trunc.toUnsignedNumeric(wkDataAppoAa, Len.WK_DATA_APPO_AA);
    }

    public short getWkDataAppoAa() {
        return NumericDisplay.asShort(this.wkDataAppoAa);
    }

    public void setWkDataAppoMm(short wkDataAppoMm) {
        this.wkDataAppoMm = NumericDisplay.asString(wkDataAppoMm, Len.WK_DATA_APPO_MM);
    }

    public short getWkDataAppoMm() {
        return NumericDisplay.asShort(this.wkDataAppoMm);
    }

    public void setWkDataAppoGg(short wkDataAppoGg) {
        this.wkDataAppoGg = NumericDisplay.asString(wkDataAppoGg, Len.WK_DATA_APPO_GG);
    }

    public short getWkDataAppoGg() {
        return NumericDisplay.asShort(this.wkDataAppoGg);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_DATA_APPO_AA = 4;
        public static final int WK_DATA_APPO_MM = 2;
        public static final int WK_DATA_APPO_GG = 2;
        public static final int WK_DATA_APPO = WK_DATA_APPO_AA + WK_DATA_APPO_MM + WK_DATA_APPO_GG;
        public static final int WK_ID_POLI = 5;
        public static final int WK_ID_ADES = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_ID_POLI = 9;
            public static final int WK_ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
