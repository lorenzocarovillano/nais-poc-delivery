package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B01-VAL-QUO-T<br>
 * Variable: W-B01-VAL-QUO-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB01ValQuoT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB01ValQuoT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B01_VAL_QUO_T;
    }

    public void setwB01ValQuoT(AfDecimal wB01ValQuoT) {
        writeDecimalAsPacked(Pos.W_B01_VAL_QUO_T, wB01ValQuoT.copy());
    }

    /**Original name: W-B01-VAL-QUO-T<br>*/
    public AfDecimal getwB01ValQuoT() {
        return readPackedAsDecimal(Pos.W_B01_VAL_QUO_T, Len.Int.W_B01_VAL_QUO_T, Len.Fract.W_B01_VAL_QUO_T);
    }

    public byte[] getwB01ValQuoTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B01_VAL_QUO_T, Pos.W_B01_VAL_QUO_T);
        return buffer;
    }

    public void setwB01ValQuoTNull(String wB01ValQuoTNull) {
        writeString(Pos.W_B01_VAL_QUO_T_NULL, wB01ValQuoTNull, Len.W_B01_VAL_QUO_T_NULL);
    }

    /**Original name: W-B01-VAL-QUO-T-NULL<br>*/
    public String getwB01ValQuoTNull() {
        return readString(Pos.W_B01_VAL_QUO_T_NULL, Len.W_B01_VAL_QUO_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B01_VAL_QUO_T = 1;
        public static final int W_B01_VAL_QUO_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B01_VAL_QUO_T = 7;
        public static final int W_B01_VAL_QUO_T_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B01_VAL_QUO_T = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B01_VAL_QUO_T = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
