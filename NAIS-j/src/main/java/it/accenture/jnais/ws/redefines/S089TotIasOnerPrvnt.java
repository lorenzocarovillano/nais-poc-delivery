package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IAS-ONER-PRVNT<br>
 * Variable: S089-TOT-IAS-ONER-PRVNT from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotIasOnerPrvnt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotIasOnerPrvnt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IAS_ONER_PRVNT;
    }

    public void setWlquTotIasOnerPrvnt(AfDecimal wlquTotIasOnerPrvnt) {
        writeDecimalAsPacked(Pos.S089_TOT_IAS_ONER_PRVNT, wlquTotIasOnerPrvnt.copy());
    }

    public void setWlquTotIasOnerPrvntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IAS_ONER_PRVNT, Pos.S089_TOT_IAS_ONER_PRVNT);
    }

    /**Original name: WLQU-TOT-IAS-ONER-PRVNT<br>*/
    public AfDecimal getWlquTotIasOnerPrvnt() {
        return readPackedAsDecimal(Pos.S089_TOT_IAS_ONER_PRVNT, Len.Int.WLQU_TOT_IAS_ONER_PRVNT, Len.Fract.WLQU_TOT_IAS_ONER_PRVNT);
    }

    public byte[] getWlquTotIasOnerPrvntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IAS_ONER_PRVNT, Pos.S089_TOT_IAS_ONER_PRVNT);
        return buffer;
    }

    public void initWlquTotIasOnerPrvntSpaces() {
        fill(Pos.S089_TOT_IAS_ONER_PRVNT, Len.S089_TOT_IAS_ONER_PRVNT, Types.SPACE_CHAR);
    }

    public void setWlquTotIasOnerPrvntNull(String wlquTotIasOnerPrvntNull) {
        writeString(Pos.S089_TOT_IAS_ONER_PRVNT_NULL, wlquTotIasOnerPrvntNull, Len.WLQU_TOT_IAS_ONER_PRVNT_NULL);
    }

    /**Original name: WLQU-TOT-IAS-ONER-PRVNT-NULL<br>*/
    public String getWlquTotIasOnerPrvntNull() {
        return readString(Pos.S089_TOT_IAS_ONER_PRVNT_NULL, Len.WLQU_TOT_IAS_ONER_PRVNT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IAS_ONER_PRVNT = 1;
        public static final int S089_TOT_IAS_ONER_PRVNT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IAS_ONER_PRVNT = 8;
        public static final int WLQU_TOT_IAS_ONER_PRVNT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IAS_ONER_PRVNT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IAS_ONER_PRVNT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
