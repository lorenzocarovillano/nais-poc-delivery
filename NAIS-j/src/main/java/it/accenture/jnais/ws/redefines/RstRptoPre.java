package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RPTO-PRE<br>
 * Variable: RST-RPTO-PRE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRptoPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRptoPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RPTO_PRE;
    }

    public void setRstRptoPre(AfDecimal rstRptoPre) {
        writeDecimalAsPacked(Pos.RST_RPTO_PRE, rstRptoPre.copy());
    }

    public void setRstRptoPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RPTO_PRE, Pos.RST_RPTO_PRE);
    }

    /**Original name: RST-RPTO-PRE<br>*/
    public AfDecimal getRstRptoPre() {
        return readPackedAsDecimal(Pos.RST_RPTO_PRE, Len.Int.RST_RPTO_PRE, Len.Fract.RST_RPTO_PRE);
    }

    public byte[] getRstRptoPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RPTO_PRE, Pos.RST_RPTO_PRE);
        return buffer;
    }

    public void setRstRptoPreNull(String rstRptoPreNull) {
        writeString(Pos.RST_RPTO_PRE_NULL, rstRptoPreNull, Len.RST_RPTO_PRE_NULL);
    }

    /**Original name: RST-RPTO-PRE-NULL<br>*/
    public String getRstRptoPreNull() {
        return readString(Pos.RST_RPTO_PRE_NULL, Len.RST_RPTO_PRE_NULL);
    }

    public String getRstRptoPreNullFormatted() {
        return Functions.padBlanks(getRstRptoPreNull(), Len.RST_RPTO_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RPTO_PRE = 1;
        public static final int RST_RPTO_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RPTO_PRE = 8;
        public static final int RST_RPTO_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RPTO_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RPTO_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
