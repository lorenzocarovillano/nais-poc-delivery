package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-SOPR-PROF<br>
 * Variable: TIT-TOT-SOPR-PROF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_SOPR_PROF;
    }

    public void setTitTotSoprProf(AfDecimal titTotSoprProf) {
        writeDecimalAsPacked(Pos.TIT_TOT_SOPR_PROF, titTotSoprProf.copy());
    }

    public void setTitTotSoprProfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_SOPR_PROF, Pos.TIT_TOT_SOPR_PROF);
    }

    /**Original name: TIT-TOT-SOPR-PROF<br>*/
    public AfDecimal getTitTotSoprProf() {
        return readPackedAsDecimal(Pos.TIT_TOT_SOPR_PROF, Len.Int.TIT_TOT_SOPR_PROF, Len.Fract.TIT_TOT_SOPR_PROF);
    }

    public byte[] getTitTotSoprProfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_SOPR_PROF, Pos.TIT_TOT_SOPR_PROF);
        return buffer;
    }

    public void setTitTotSoprProfNull(String titTotSoprProfNull) {
        writeString(Pos.TIT_TOT_SOPR_PROF_NULL, titTotSoprProfNull, Len.TIT_TOT_SOPR_PROF_NULL);
    }

    /**Original name: TIT-TOT-SOPR-PROF-NULL<br>*/
    public String getTitTotSoprProfNull() {
        return readString(Pos.TIT_TOT_SOPR_PROF_NULL, Len.TIT_TOT_SOPR_PROF_NULL);
    }

    public String getTitTotSoprProfNullFormatted() {
        return Functions.padBlanks(getTitTotSoprProfNull(), Len.TIT_TOT_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SOPR_PROF = 1;
        public static final int TIT_TOT_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SOPR_PROF = 8;
        public static final int TIT_TOT_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
