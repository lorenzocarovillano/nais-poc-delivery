package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CAR-ACQ-PRECONTATO<br>
 * Variable: WB03-CAR-ACQ-PRECONTATO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CarAcqPrecontato extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CarAcqPrecontato() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CAR_ACQ_PRECONTATO;
    }

    public void setWb03CarAcqPrecontatoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CAR_ACQ_PRECONTATO, Pos.WB03_CAR_ACQ_PRECONTATO);
    }

    /**Original name: WB03-CAR-ACQ-PRECONTATO<br>*/
    public AfDecimal getWb03CarAcqPrecontato() {
        return readPackedAsDecimal(Pos.WB03_CAR_ACQ_PRECONTATO, Len.Int.WB03_CAR_ACQ_PRECONTATO, Len.Fract.WB03_CAR_ACQ_PRECONTATO);
    }

    public byte[] getWb03CarAcqPrecontatoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CAR_ACQ_PRECONTATO, Pos.WB03_CAR_ACQ_PRECONTATO);
        return buffer;
    }

    public void setWb03CarAcqPrecontatoNull(String wb03CarAcqPrecontatoNull) {
        writeString(Pos.WB03_CAR_ACQ_PRECONTATO_NULL, wb03CarAcqPrecontatoNull, Len.WB03_CAR_ACQ_PRECONTATO_NULL);
    }

    /**Original name: WB03-CAR-ACQ-PRECONTATO-NULL<br>*/
    public String getWb03CarAcqPrecontatoNull() {
        return readString(Pos.WB03_CAR_ACQ_PRECONTATO_NULL, Len.WB03_CAR_ACQ_PRECONTATO_NULL);
    }

    public String getWb03CarAcqPrecontatoNullFormatted() {
        return Functions.padBlanks(getWb03CarAcqPrecontatoNull(), Len.WB03_CAR_ACQ_PRECONTATO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CAR_ACQ_PRECONTATO = 1;
        public static final int WB03_CAR_ACQ_PRECONTATO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CAR_ACQ_PRECONTATO = 8;
        public static final int WB03_CAR_ACQ_PRECONTATO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CAR_ACQ_PRECONTATO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CAR_ACQ_PRECONTATO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
