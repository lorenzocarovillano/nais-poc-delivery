package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-IMP-BNS-DA-SCO<br>
 * Variable: WPMO-IMP-BNS-DA-SCO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoImpBnsDaSco extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoImpBnsDaSco() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_IMP_BNS_DA_SCO;
    }

    public void setWpmoImpBnsDaSco(AfDecimal wpmoImpBnsDaSco) {
        writeDecimalAsPacked(Pos.WPMO_IMP_BNS_DA_SCO, wpmoImpBnsDaSco.copy());
    }

    public void setWpmoImpBnsDaScoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_IMP_BNS_DA_SCO, Pos.WPMO_IMP_BNS_DA_SCO);
    }

    /**Original name: WPMO-IMP-BNS-DA-SCO<br>*/
    public AfDecimal getWpmoImpBnsDaSco() {
        return readPackedAsDecimal(Pos.WPMO_IMP_BNS_DA_SCO, Len.Int.WPMO_IMP_BNS_DA_SCO, Len.Fract.WPMO_IMP_BNS_DA_SCO);
    }

    public byte[] getWpmoImpBnsDaScoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_IMP_BNS_DA_SCO, Pos.WPMO_IMP_BNS_DA_SCO);
        return buffer;
    }

    public void initWpmoImpBnsDaScoSpaces() {
        fill(Pos.WPMO_IMP_BNS_DA_SCO, Len.WPMO_IMP_BNS_DA_SCO, Types.SPACE_CHAR);
    }

    public void setWpmoImpBnsDaScoNull(String wpmoImpBnsDaScoNull) {
        writeString(Pos.WPMO_IMP_BNS_DA_SCO_NULL, wpmoImpBnsDaScoNull, Len.WPMO_IMP_BNS_DA_SCO_NULL);
    }

    /**Original name: WPMO-IMP-BNS-DA-SCO-NULL<br>*/
    public String getWpmoImpBnsDaScoNull() {
        return readString(Pos.WPMO_IMP_BNS_DA_SCO_NULL, Len.WPMO_IMP_BNS_DA_SCO_NULL);
    }

    public String getWpmoImpBnsDaScoNullFormatted() {
        return Functions.padBlanks(getWpmoImpBnsDaScoNull(), Len.WPMO_IMP_BNS_DA_SCO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_IMP_BNS_DA_SCO = 1;
        public static final int WPMO_IMP_BNS_DA_SCO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_IMP_BNS_DA_SCO = 8;
        public static final int WPMO_IMP_BNS_DA_SCO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_IMP_BNS_DA_SCO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_IMP_BNS_DA_SCO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
