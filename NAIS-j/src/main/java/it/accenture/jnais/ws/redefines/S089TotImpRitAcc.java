package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMP-RIT-ACC<br>
 * Variable: S089-TOT-IMP-RIT-ACC from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpRitAcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpRitAcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMP_RIT_ACC;
    }

    public void setWlquTotImpRitAcc(AfDecimal wlquTotImpRitAcc) {
        writeDecimalAsPacked(Pos.S089_TOT_IMP_RIT_ACC, wlquTotImpRitAcc.copy());
    }

    public void setWlquTotImpRitAccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMP_RIT_ACC, Pos.S089_TOT_IMP_RIT_ACC);
    }

    /**Original name: WLQU-TOT-IMP-RIT-ACC<br>*/
    public AfDecimal getWlquTotImpRitAcc() {
        return readPackedAsDecimal(Pos.S089_TOT_IMP_RIT_ACC, Len.Int.WLQU_TOT_IMP_RIT_ACC, Len.Fract.WLQU_TOT_IMP_RIT_ACC);
    }

    public byte[] getWlquTotImpRitAccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMP_RIT_ACC, Pos.S089_TOT_IMP_RIT_ACC);
        return buffer;
    }

    public void initWlquTotImpRitAccSpaces() {
        fill(Pos.S089_TOT_IMP_RIT_ACC, Len.S089_TOT_IMP_RIT_ACC, Types.SPACE_CHAR);
    }

    public void setWlquTotImpRitAccNull(String wlquTotImpRitAccNull) {
        writeString(Pos.S089_TOT_IMP_RIT_ACC_NULL, wlquTotImpRitAccNull, Len.WLQU_TOT_IMP_RIT_ACC_NULL);
    }

    /**Original name: WLQU-TOT-IMP-RIT-ACC-NULL<br>*/
    public String getWlquTotImpRitAccNull() {
        return readString(Pos.S089_TOT_IMP_RIT_ACC_NULL, Len.WLQU_TOT_IMP_RIT_ACC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_RIT_ACC = 1;
        public static final int S089_TOT_IMP_RIT_ACC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_RIT_ACC = 8;
        public static final int WLQU_TOT_IMP_RIT_ACC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_RIT_ACC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_RIT_ACC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
