package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: E12-ACCPRE-PAG<br>
 * Variable: E12-ACCPRE-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E12AccprePag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E12AccprePag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E12_ACCPRE_PAG;
    }

    public void setE12AccprePag(AfDecimal e12AccprePag) {
        writeDecimalAsPacked(Pos.E12_ACCPRE_PAG, e12AccprePag.copy());
    }

    public void setE12AccprePagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E12_ACCPRE_PAG, Pos.E12_ACCPRE_PAG);
    }

    /**Original name: E12-ACCPRE-PAG<br>*/
    public AfDecimal getE12AccprePag() {
        return readPackedAsDecimal(Pos.E12_ACCPRE_PAG, Len.Int.E12_ACCPRE_PAG, Len.Fract.E12_ACCPRE_PAG);
    }

    public byte[] getE12AccprePagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E12_ACCPRE_PAG, Pos.E12_ACCPRE_PAG);
        return buffer;
    }

    public void setE12AccprePagNull(String e12AccprePagNull) {
        writeString(Pos.E12_ACCPRE_PAG_NULL, e12AccprePagNull, Len.E12_ACCPRE_PAG_NULL);
    }

    /**Original name: E12-ACCPRE-PAG-NULL<br>*/
    public String getE12AccprePagNull() {
        return readString(Pos.E12_ACCPRE_PAG_NULL, Len.E12_ACCPRE_PAG_NULL);
    }

    public String getE12AccprePagNullFormatted() {
        return Functions.padBlanks(getE12AccprePagNull(), Len.E12_ACCPRE_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E12_ACCPRE_PAG = 1;
        public static final int E12_ACCPRE_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E12_ACCPRE_PAG = 8;
        public static final int E12_ACCPRE_PAG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E12_ACCPRE_PAG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int E12_ACCPRE_PAG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
