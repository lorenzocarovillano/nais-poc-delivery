package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.BtcParallelismDb;
import it.accenture.jnais.copy.Iabv0004;
import it.accenture.jnais.copy.Idsv0014;
import it.accenture.jnais.copy.IndGravitaErrore;
import it.accenture.jnais.ws.enums.WkFineLoop;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IABS0130<br>
 * Generated as a class for rule WS.<br>*/
public class Iabs0130Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WS-PROG-PROTOCOL
    private int wsProgProtocol = 0;
    //Original name: WK-FINE-LOOP
    private WkFineLoop wkFineLoop = new WkFineLoop();
    //Original name: COMODO-COD-COMP-ANIA
    private String comodoCodCompAnia = DefaultValues.stringVal(Len.COMODO_COD_COMP_ANIA);
    //Original name: COMODO-PROG-PROTOCOL
    private String comodoProgProtocol = DefaultValues.stringVal(Len.COMODO_PROG_PROTOCOL);
    //Original name: WK-PROG-PROTOCOL
    private int wkProgProtocol = DefaultValues.BIN_INT_VAL;
    //Original name: WS-BUFFER-WHERE-COND-IABS0130
    private WsBufferWhereCondIabs0130 wsBufferWhereCondIabs0130 = new WsBufferWhereCondIabs0130();
    //Original name: IDSV0014
    private Idsv0014 idsv0014 = new Idsv0014();
    //Original name: IABV0004
    private Iabv0004 iabv0004 = new Iabv0004();
    //Original name: IND-BTC-PARALLELISM
    private IndGravitaErrore indBtcParallelism = new IndGravitaErrore();
    //Original name: BTC-PARALLELISM-DB
    private BtcParallelismDb btcParallelismDb = new BtcParallelismDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWsProgProtocol(int wsProgProtocol) {
        this.wsProgProtocol = wsProgProtocol;
    }

    public int getWsProgProtocol() {
        return this.wsProgProtocol;
    }

    public void setComodoCodCompAnia(int comodoCodCompAnia) {
        this.comodoCodCompAnia = NumericDisplay.asString(comodoCodCompAnia, Len.COMODO_COD_COMP_ANIA);
    }

    public int getComodoCodCompAnia() {
        return NumericDisplay.asInt(this.comodoCodCompAnia);
    }

    public String getComodoCodCompAniaFormatted() {
        return this.comodoCodCompAnia;
    }

    public String getComodoCodCompAniaAsString() {
        return getComodoCodCompAniaFormatted();
    }

    public void setComodoProgProtocol(int comodoProgProtocol) {
        this.comodoProgProtocol = NumericDisplay.asString(comodoProgProtocol, Len.COMODO_PROG_PROTOCOL);
    }

    public int getComodoProgProtocol() {
        return NumericDisplay.asInt(this.comodoProgProtocol);
    }

    public String getComodoProgProtocolFormatted() {
        return this.comodoProgProtocol;
    }

    public String getComodoProgProtocolAsString() {
        return getComodoProgProtocolFormatted();
    }

    public void setWkProgProtocol(int wkProgProtocol) {
        this.wkProgProtocol = wkProgProtocol;
    }

    public int getWkProgProtocol() {
        return this.wkProgProtocol;
    }

    public BtcParallelismDb getBtcParallelismDb() {
        return btcParallelismDb;
    }

    public Iabv0004 getIabv0004() {
        return iabv0004;
    }

    public Idsv0014 getIdsv0014() {
        return idsv0014;
    }

    public IndGravitaErrore getIndBtcParallelism() {
        return indBtcParallelism;
    }

    public WkFineLoop getWkFineLoop() {
        return wkFineLoop;
    }

    public WsBufferWhereCondIabs0130 getWsBufferWhereCondIabs0130() {
        return wsBufferWhereCondIabs0130;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int COMODO_COD_COMP_ANIA = 9;
        public static final int COMODO_PROG_PROTOCOL = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
