package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-FRQ-COSTI-ATT<br>
 * Variable: PCO-FRQ-COSTI-ATT from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoFrqCostiAtt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoFrqCostiAtt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_FRQ_COSTI_ATT;
    }

    public void setPcoFrqCostiAtt(int pcoFrqCostiAtt) {
        writeIntAsPacked(Pos.PCO_FRQ_COSTI_ATT, pcoFrqCostiAtt, Len.Int.PCO_FRQ_COSTI_ATT);
    }

    public void setPcoFrqCostiAttFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_FRQ_COSTI_ATT, Pos.PCO_FRQ_COSTI_ATT);
    }

    /**Original name: PCO-FRQ-COSTI-ATT<br>*/
    public int getPcoFrqCostiAtt() {
        return readPackedAsInt(Pos.PCO_FRQ_COSTI_ATT, Len.Int.PCO_FRQ_COSTI_ATT);
    }

    public byte[] getPcoFrqCostiAttAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_FRQ_COSTI_ATT, Pos.PCO_FRQ_COSTI_ATT);
        return buffer;
    }

    public void initPcoFrqCostiAttHighValues() {
        fill(Pos.PCO_FRQ_COSTI_ATT, Len.PCO_FRQ_COSTI_ATT, Types.HIGH_CHAR_VAL);
    }

    public void setPcoFrqCostiAttNull(String pcoFrqCostiAttNull) {
        writeString(Pos.PCO_FRQ_COSTI_ATT_NULL, pcoFrqCostiAttNull, Len.PCO_FRQ_COSTI_ATT_NULL);
    }

    /**Original name: PCO-FRQ-COSTI-ATT-NULL<br>*/
    public String getPcoFrqCostiAttNull() {
        return readString(Pos.PCO_FRQ_COSTI_ATT_NULL, Len.PCO_FRQ_COSTI_ATT_NULL);
    }

    public String getPcoFrqCostiAttNullFormatted() {
        return Functions.padBlanks(getPcoFrqCostiAttNull(), Len.PCO_FRQ_COSTI_ATT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_FRQ_COSTI_ATT = 1;
        public static final int PCO_FRQ_COSTI_ATT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_FRQ_COSTI_ATT = 3;
        public static final int PCO_FRQ_COSTI_ATT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_FRQ_COSTI_ATT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
