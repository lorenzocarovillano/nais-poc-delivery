package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-PROV-RICOR<br>
 * Variable: PVT-PROV-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_PROV_RICOR;
    }

    public void setPvtProvRicor(AfDecimal pvtProvRicor) {
        writeDecimalAsPacked(Pos.PVT_PROV_RICOR, pvtProvRicor.copy());
    }

    public void setPvtProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_PROV_RICOR, Pos.PVT_PROV_RICOR);
    }

    /**Original name: PVT-PROV-RICOR<br>*/
    public AfDecimal getPvtProvRicor() {
        return readPackedAsDecimal(Pos.PVT_PROV_RICOR, Len.Int.PVT_PROV_RICOR, Len.Fract.PVT_PROV_RICOR);
    }

    public byte[] getPvtProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_PROV_RICOR, Pos.PVT_PROV_RICOR);
        return buffer;
    }

    public void setPvtProvRicorNull(String pvtProvRicorNull) {
        writeString(Pos.PVT_PROV_RICOR_NULL, pvtProvRicorNull, Len.PVT_PROV_RICOR_NULL);
    }

    /**Original name: PVT-PROV-RICOR-NULL<br>*/
    public String getPvtProvRicorNull() {
        return readString(Pos.PVT_PROV_RICOR_NULL, Len.PVT_PROV_RICOR_NULL);
    }

    public String getPvtProvRicorNullFormatted() {
        return Functions.padBlanks(getPvtProvRicorNull(), Len.PVT_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_PROV_RICOR = 1;
        public static final int PVT_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_PROV_RICOR = 8;
        public static final int PVT_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PVT_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
