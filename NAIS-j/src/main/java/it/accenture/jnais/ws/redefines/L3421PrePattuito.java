package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PRE-PATTUITO<br>
 * Variable: L3421-PRE-PATTUITO from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PrePattuito extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PrePattuito() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PRE_PATTUITO;
    }

    public void setL3421PrePattuitoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PRE_PATTUITO, Pos.L3421_PRE_PATTUITO);
    }

    /**Original name: L3421-PRE-PATTUITO<br>*/
    public AfDecimal getL3421PrePattuito() {
        return readPackedAsDecimal(Pos.L3421_PRE_PATTUITO, Len.Int.L3421_PRE_PATTUITO, Len.Fract.L3421_PRE_PATTUITO);
    }

    public byte[] getL3421PrePattuitoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PRE_PATTUITO, Pos.L3421_PRE_PATTUITO);
        return buffer;
    }

    /**Original name: L3421-PRE-PATTUITO-NULL<br>*/
    public String getL3421PrePattuitoNull() {
        return readString(Pos.L3421_PRE_PATTUITO_NULL, Len.L3421_PRE_PATTUITO_NULL);
    }

    public String getL3421PrePattuitoNullFormatted() {
        return Functions.padBlanks(getL3421PrePattuitoNull(), Len.L3421_PRE_PATTUITO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PRE_PATTUITO = 1;
        public static final int L3421_PRE_PATTUITO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PRE_PATTUITO = 8;
        public static final int L3421_PRE_PATTUITO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PRE_PATTUITO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PRE_PATTUITO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
