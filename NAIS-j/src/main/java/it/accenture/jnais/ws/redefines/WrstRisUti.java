package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-UTI<br>
 * Variable: WRST-RIS-UTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisUti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisUti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_UTI;
    }

    public void setWrstRisUti(AfDecimal wrstRisUti) {
        writeDecimalAsPacked(Pos.WRST_RIS_UTI, wrstRisUti.copy());
    }

    public void setWrstRisUtiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_UTI, Pos.WRST_RIS_UTI);
    }

    /**Original name: WRST-RIS-UTI<br>*/
    public AfDecimal getWrstRisUti() {
        return readPackedAsDecimal(Pos.WRST_RIS_UTI, Len.Int.WRST_RIS_UTI, Len.Fract.WRST_RIS_UTI);
    }

    public byte[] getWrstRisUtiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_UTI, Pos.WRST_RIS_UTI);
        return buffer;
    }

    public void initWrstRisUtiSpaces() {
        fill(Pos.WRST_RIS_UTI, Len.WRST_RIS_UTI, Types.SPACE_CHAR);
    }

    public void setWrstRisUtiNull(String wrstRisUtiNull) {
        writeString(Pos.WRST_RIS_UTI_NULL, wrstRisUtiNull, Len.WRST_RIS_UTI_NULL);
    }

    /**Original name: WRST-RIS-UTI-NULL<br>*/
    public String getWrstRisUtiNull() {
        return readString(Pos.WRST_RIS_UTI_NULL, Len.WRST_RIS_UTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_UTI = 1;
        public static final int WRST_RIS_UTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_UTI = 8;
        public static final int WRST_RIS_UTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_UTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_UTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
