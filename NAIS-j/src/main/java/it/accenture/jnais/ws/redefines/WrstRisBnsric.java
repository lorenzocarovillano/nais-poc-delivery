package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-BNSRIC<br>
 * Variable: WRST-RIS-BNSRIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisBnsric extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisBnsric() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_BNSRIC;
    }

    public void setWrstRisBnsric(AfDecimal wrstRisBnsric) {
        writeDecimalAsPacked(Pos.WRST_RIS_BNSRIC, wrstRisBnsric.copy());
    }

    public void setWrstRisBnsricFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_BNSRIC, Pos.WRST_RIS_BNSRIC);
    }

    /**Original name: WRST-RIS-BNSRIC<br>*/
    public AfDecimal getWrstRisBnsric() {
        return readPackedAsDecimal(Pos.WRST_RIS_BNSRIC, Len.Int.WRST_RIS_BNSRIC, Len.Fract.WRST_RIS_BNSRIC);
    }

    public byte[] getWrstRisBnsricAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_BNSRIC, Pos.WRST_RIS_BNSRIC);
        return buffer;
    }

    public void initWrstRisBnsricSpaces() {
        fill(Pos.WRST_RIS_BNSRIC, Len.WRST_RIS_BNSRIC, Types.SPACE_CHAR);
    }

    public void setWrstRisBnsricNull(String wrstRisBnsricNull) {
        writeString(Pos.WRST_RIS_BNSRIC_NULL, wrstRisBnsricNull, Len.WRST_RIS_BNSRIC_NULL);
    }

    /**Original name: WRST-RIS-BNSRIC-NULL<br>*/
    public String getWrstRisBnsricNull() {
        return readString(Pos.WRST_RIS_BNSRIC_NULL, Len.WRST_RIS_BNSRIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_BNSRIC = 1;
        public static final int WRST_RIS_BNSRIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_BNSRIC = 8;
        public static final int WRST_RIS_BNSRIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_BNSRIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_BNSRIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
