package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-CAR-ACQ<br>
 * Variable: L3421-IMP-CAR-ACQ from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_CAR_ACQ;
    }

    public void setL3421ImpCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_CAR_ACQ, Pos.L3421_IMP_CAR_ACQ);
    }

    /**Original name: L3421-IMP-CAR-ACQ<br>*/
    public AfDecimal getL3421ImpCarAcq() {
        return readPackedAsDecimal(Pos.L3421_IMP_CAR_ACQ, Len.Int.L3421_IMP_CAR_ACQ, Len.Fract.L3421_IMP_CAR_ACQ);
    }

    public byte[] getL3421ImpCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_CAR_ACQ, Pos.L3421_IMP_CAR_ACQ);
        return buffer;
    }

    /**Original name: L3421-IMP-CAR-ACQ-NULL<br>*/
    public String getL3421ImpCarAcqNull() {
        return readString(Pos.L3421_IMP_CAR_ACQ_NULL, Len.L3421_IMP_CAR_ACQ_NULL);
    }

    public String getL3421ImpCarAcqNullFormatted() {
        return Functions.padBlanks(getL3421ImpCarAcqNull(), Len.L3421_IMP_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_CAR_ACQ = 1;
        public static final int L3421_IMP_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_CAR_ACQ = 8;
        public static final int L3421_IMP_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
