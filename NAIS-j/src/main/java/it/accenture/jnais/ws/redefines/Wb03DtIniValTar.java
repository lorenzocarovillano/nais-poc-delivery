package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-INI-VAL-TAR<br>
 * Variable: WB03-DT-INI-VAL-TAR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtIniValTar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtIniValTar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_INI_VAL_TAR;
    }

    public void setWb03DtIniValTar(int wb03DtIniValTar) {
        writeIntAsPacked(Pos.WB03_DT_INI_VAL_TAR, wb03DtIniValTar, Len.Int.WB03_DT_INI_VAL_TAR);
    }

    public void setWb03DtIniValTarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_INI_VAL_TAR, Pos.WB03_DT_INI_VAL_TAR);
    }

    /**Original name: WB03-DT-INI-VAL-TAR<br>*/
    public int getWb03DtIniValTar() {
        return readPackedAsInt(Pos.WB03_DT_INI_VAL_TAR, Len.Int.WB03_DT_INI_VAL_TAR);
    }

    public byte[] getWb03DtIniValTarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_INI_VAL_TAR, Pos.WB03_DT_INI_VAL_TAR);
        return buffer;
    }

    public void setWb03DtIniValTarNull(String wb03DtIniValTarNull) {
        writeString(Pos.WB03_DT_INI_VAL_TAR_NULL, wb03DtIniValTarNull, Len.WB03_DT_INI_VAL_TAR_NULL);
    }

    /**Original name: WB03-DT-INI-VAL-TAR-NULL<br>*/
    public String getWb03DtIniValTarNull() {
        return readString(Pos.WB03_DT_INI_VAL_TAR_NULL, Len.WB03_DT_INI_VAL_TAR_NULL);
    }

    public String getWb03DtIniValTarNullFormatted() {
        return Functions.padBlanks(getWb03DtIniValTarNull(), Len.WB03_DT_INI_VAL_TAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_INI_VAL_TAR = 1;
        public static final int WB03_DT_INI_VAL_TAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_INI_VAL_TAR = 5;
        public static final int WB03_DT_INI_VAL_TAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_INI_VAL_TAR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
