package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: S234-DATI-OUTPUT<br>
 * Variable: S234-DATI-OUTPUT from program LCCS0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class S234DatiOutput extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: S234-ID-OGG-PTF-EOC
    private int idOggPtfEoc = DefaultValues.INT_VAL;
    //Original name: S234-IB-OGG-PTF-EOC
    private String ibOggPtfEoc = DefaultValues.stringVal(Len.IB_OGG_PTF_EOC);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S234_DATI_OUTPUT;
    }

    @Override
    public void deserialize(byte[] buf) {
        setS234DatiOutputBytes(buf);
    }

    public String getS234DatiOutputFormatted() {
        return MarshalByteExt.bufferToStr(getS234DatiOutputBytes());
    }

    public void setS234DatiOutputBytes(byte[] buffer) {
        setS234DatiOutputBytes(buffer, 1);
    }

    public byte[] getS234DatiOutputBytes() {
        byte[] buffer = new byte[Len.S234_DATI_OUTPUT];
        return getS234DatiOutputBytes(buffer, 1);
    }

    public void setS234DatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        idOggPtfEoc = MarshalByte.readInt(buffer, position, Len.ID_OGG_PTF_EOC);
        position += Len.ID_OGG_PTF_EOC;
        ibOggPtfEoc = MarshalByte.readString(buffer, position, Len.IB_OGG_PTF_EOC);
    }

    public byte[] getS234DatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeInt(buffer, position, idOggPtfEoc, Len.ID_OGG_PTF_EOC);
        position += Len.ID_OGG_PTF_EOC;
        MarshalByte.writeString(buffer, position, ibOggPtfEoc, Len.IB_OGG_PTF_EOC);
        return buffer;
    }

    public void setIdOggPtfEoc(int idOggPtfEoc) {
        this.idOggPtfEoc = idOggPtfEoc;
    }

    public int getIdOggPtfEoc() {
        return this.idOggPtfEoc;
    }

    public void setIbOggPtfEoc(String ibOggPtfEoc) {
        this.ibOggPtfEoc = Functions.subString(ibOggPtfEoc, Len.IB_OGG_PTF_EOC);
    }

    public String getIbOggPtfEoc() {
        return this.ibOggPtfEoc;
    }

    @Override
    public byte[] serialize() {
        return getS234DatiOutputBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG_PTF_EOC = 9;
        public static final int IB_OGG_PTF_EOC = 40;
        public static final int S234_DATI_OUTPUT = ID_OGG_PTF_EOC + IB_OGG_PTF_EOC;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
