package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ESTRAZ-FUG<br>
 * Variable: PCO-DT-ULT-ESTRAZ-FUG from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEstrazFug extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEstrazFug() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ESTRAZ_FUG;
    }

    public void setPcoDtUltEstrazFug(int pcoDtUltEstrazFug) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ESTRAZ_FUG, pcoDtUltEstrazFug, Len.Int.PCO_DT_ULT_ESTRAZ_FUG);
    }

    public void setPcoDtUltEstrazFugFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ESTRAZ_FUG, Pos.PCO_DT_ULT_ESTRAZ_FUG);
    }

    /**Original name: PCO-DT-ULT-ESTRAZ-FUG<br>*/
    public int getPcoDtUltEstrazFug() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ESTRAZ_FUG, Len.Int.PCO_DT_ULT_ESTRAZ_FUG);
    }

    public byte[] getPcoDtUltEstrazFugAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ESTRAZ_FUG, Pos.PCO_DT_ULT_ESTRAZ_FUG);
        return buffer;
    }

    public void initPcoDtUltEstrazFugHighValues() {
        fill(Pos.PCO_DT_ULT_ESTRAZ_FUG, Len.PCO_DT_ULT_ESTRAZ_FUG, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEstrazFugNull(String pcoDtUltEstrazFugNull) {
        writeString(Pos.PCO_DT_ULT_ESTRAZ_FUG_NULL, pcoDtUltEstrazFugNull, Len.PCO_DT_ULT_ESTRAZ_FUG_NULL);
    }

    /**Original name: PCO-DT-ULT-ESTRAZ-FUG-NULL<br>*/
    public String getPcoDtUltEstrazFugNull() {
        return readString(Pos.PCO_DT_ULT_ESTRAZ_FUG_NULL, Len.PCO_DT_ULT_ESTRAZ_FUG_NULL);
    }

    public String getPcoDtUltEstrazFugNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEstrazFugNull(), Len.PCO_DT_ULT_ESTRAZ_FUG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ESTRAZ_FUG = 1;
        public static final int PCO_DT_ULT_ESTRAZ_FUG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ESTRAZ_FUG = 5;
        public static final int PCO_DT_ULT_ESTRAZ_FUG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ESTRAZ_FUG = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
