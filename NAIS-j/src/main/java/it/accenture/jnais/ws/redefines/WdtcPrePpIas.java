package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-PRE-PP-IAS<br>
 * Variable: WDTC-PRE-PP-IAS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcPrePpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcPrePpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_PRE_PP_IAS;
    }

    public void setWdtcPrePpIas(AfDecimal wdtcPrePpIas) {
        writeDecimalAsPacked(Pos.WDTC_PRE_PP_IAS, wdtcPrePpIas.copy());
    }

    public void setWdtcPrePpIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_PRE_PP_IAS, Pos.WDTC_PRE_PP_IAS);
    }

    /**Original name: WDTC-PRE-PP-IAS<br>*/
    public AfDecimal getWdtcPrePpIas() {
        return readPackedAsDecimal(Pos.WDTC_PRE_PP_IAS, Len.Int.WDTC_PRE_PP_IAS, Len.Fract.WDTC_PRE_PP_IAS);
    }

    public byte[] getWdtcPrePpIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_PRE_PP_IAS, Pos.WDTC_PRE_PP_IAS);
        return buffer;
    }

    public void initWdtcPrePpIasSpaces() {
        fill(Pos.WDTC_PRE_PP_IAS, Len.WDTC_PRE_PP_IAS, Types.SPACE_CHAR);
    }

    public void setWdtcPrePpIasNull(String wdtcPrePpIasNull) {
        writeString(Pos.WDTC_PRE_PP_IAS_NULL, wdtcPrePpIasNull, Len.WDTC_PRE_PP_IAS_NULL);
    }

    /**Original name: WDTC-PRE-PP-IAS-NULL<br>*/
    public String getWdtcPrePpIasNull() {
        return readString(Pos.WDTC_PRE_PP_IAS_NULL, Len.WDTC_PRE_PP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_PRE_PP_IAS = 1;
        public static final int WDTC_PRE_PP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_PRE_PP_IAS = 8;
        public static final int WDTC_PRE_PP_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_PRE_PP_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_PRE_PP_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
