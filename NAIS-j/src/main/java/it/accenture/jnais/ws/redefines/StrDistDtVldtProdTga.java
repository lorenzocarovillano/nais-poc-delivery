package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: STR-DIST-DT-VLDT-PROD-TGA<br>
 * Variable: STR-DIST-DT-VLDT-PROD-TGA from program IVVS0211<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class StrDistDtVldtProdTga extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int DIST_TAB_INFO_MAXOCCURS = 300;

    //==== CONSTRUCTORS ====
    public StrDistDtVldtProdTga() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.STR_DIST_DT_VLDT_PROD_TGA;
    }

    public String getStrDistDtVldtProdTgaFormatted() {
        return readFixedString(Pos.STR_DIST_DT_VLDT_PROD_TGA, Len.STR_DIST_DT_VLDT_PROD_TGA);
    }

    public void setDistTabDtVldtProd(int distTabDtVldtProdIdx, int distTabDtVldtProd) {
        int position = Pos.distTabDtVldtProd(distTabDtVldtProdIdx - 1);
        writeIntAsPacked(position, distTabDtVldtProd, Len.Int.DIST_TAB_DT_VLDT_PROD);
    }

    /**Original name: DIST-TAB-DT-VLDT-PROD<br>*/
    public int getDistTabDtVldtProd(int distTabDtVldtProdIdx) {
        int position = Pos.distTabDtVldtProd(distTabDtVldtProdIdx - 1);
        return readPackedAsInt(position, Len.Int.DIST_TAB_DT_VLDT_PROD);
    }

    public void setStrResDistDtVldtProdTga(String strResDistDtVldtProdTga) {
        writeString(Pos.STR_RES_DIST_DT_VLDT_PROD_TGA, strResDistDtVldtProdTga, Len.STR_RES_DIST_DT_VLDT_PROD_TGA);
    }

    /**Original name: STR-RES-DIST-DT-VLDT-PROD-TGA<br>*/
    public String getStrResDistDtVldtProdTga() {
        return readString(Pos.STR_RES_DIST_DT_VLDT_PROD_TGA, Len.STR_RES_DIST_DT_VLDT_PROD_TGA);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int STR_DIST_DT_VLDT_PROD_TGA = 1;
        public static final int STR_DIST_DT_VLDT_PROD_TGA_R = 1;
        public static final int FLR1 = STR_DIST_DT_VLDT_PROD_TGA_R;
        public static final int STR_RES_DIST_DT_VLDT_PROD_TGA = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int distTabInfo(int idx) {
            return STR_DIST_DT_VLDT_PROD_TGA + idx * Len.DIST_TAB_INFO;
        }

        public static int distTabDtVldtProd(int idx) {
            return distTabInfo(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DIST_TAB_DT_VLDT_PROD = 5;
        public static final int DIST_TAB_INFO = DIST_TAB_DT_VLDT_PROD;
        public static final int FLR1 = 5;
        public static final int STR_DIST_DT_VLDT_PROD_TGA = StrDistDtVldtProdTga.DIST_TAB_INFO_MAXOCCURS * DIST_TAB_INFO;
        public static final int STR_RES_DIST_DT_VLDT_PROD_TGA = 1495;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DIST_TAB_DT_VLDT_PROD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
