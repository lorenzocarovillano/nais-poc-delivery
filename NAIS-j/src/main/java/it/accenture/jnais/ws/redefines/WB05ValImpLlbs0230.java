package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B05-VAL-IMP<br>
 * Variable: W-B05-VAL-IMP from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB05ValImpLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB05ValImpLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B05_VAL_IMP;
    }

    public void setwB05ValImp(AfDecimal wB05ValImp) {
        writeDecimalAsPacked(Pos.W_B05_VAL_IMP, wB05ValImp.copy());
    }

    /**Original name: W-B05-VAL-IMP<br>*/
    public AfDecimal getwB05ValImp() {
        return readPackedAsDecimal(Pos.W_B05_VAL_IMP, Len.Int.W_B05_VAL_IMP, Len.Fract.W_B05_VAL_IMP);
    }

    public byte[] getwB05ValImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B05_VAL_IMP, Pos.W_B05_VAL_IMP);
        return buffer;
    }

    public void setwB05ValImpNull(String wB05ValImpNull) {
        writeString(Pos.W_B05_VAL_IMP_NULL, wB05ValImpNull, Len.W_B05_VAL_IMP_NULL);
    }

    /**Original name: W-B05-VAL-IMP-NULL<br>*/
    public String getwB05ValImpNull() {
        return readString(Pos.W_B05_VAL_IMP_NULL, Len.W_B05_VAL_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B05_VAL_IMP = 1;
        public static final int W_B05_VAL_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B05_VAL_IMP = 10;
        public static final int W_B05_VAL_IMP_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B05_VAL_IMP = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B05_VAL_IMP = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
