package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-OPERAZIONE<br>
 * Variable: FLAG-OPERAZIONE from program IABS0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagOperazione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_OPERAZIONE);
    public static final String X_TP_BATCH = "TB";
    public static final String X_DT_EFFETTO = "DT-EFF";
    public static final String X_DT_EFFETTO_MAX = "MAXEFF";
    public static final String X_DT_EFFETTO_MIN = "MINEFF";
    public static final String X_DT_INSERIM_MAX = "MAXINS";
    public static final String X_DT_INSERIM_MIN = "MININS";
    public static final String ALL_BATCH = "ALLBTC";

    //==== METHODS ====
    public void setFlagOperazione(String flagOperazione) {
        this.value = Functions.subString(flagOperazione, Len.FLAG_OPERAZIONE);
    }

    public String getFlagOperazione() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_OPERAZIONE = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
