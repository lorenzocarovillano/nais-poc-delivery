package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMP-LRD-LIQTO<br>
 * Variable: WBEL-IMP-LRD-LIQTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpLrdLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpLrdLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMP_LRD_LIQTO;
    }

    public void setWbelImpLrdLiqto(AfDecimal wbelImpLrdLiqto) {
        writeDecimalAsPacked(Pos.WBEL_IMP_LRD_LIQTO, wbelImpLrdLiqto.copy());
    }

    public void setWbelImpLrdLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMP_LRD_LIQTO, Pos.WBEL_IMP_LRD_LIQTO);
    }

    /**Original name: WBEL-IMP-LRD-LIQTO<br>*/
    public AfDecimal getWbelImpLrdLiqto() {
        return readPackedAsDecimal(Pos.WBEL_IMP_LRD_LIQTO, Len.Int.WBEL_IMP_LRD_LIQTO, Len.Fract.WBEL_IMP_LRD_LIQTO);
    }

    public byte[] getWbelImpLrdLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMP_LRD_LIQTO, Pos.WBEL_IMP_LRD_LIQTO);
        return buffer;
    }

    public void initWbelImpLrdLiqtoSpaces() {
        fill(Pos.WBEL_IMP_LRD_LIQTO, Len.WBEL_IMP_LRD_LIQTO, Types.SPACE_CHAR);
    }

    public void setWbelImpLrdLiqtoNull(String wbelImpLrdLiqtoNull) {
        writeString(Pos.WBEL_IMP_LRD_LIQTO_NULL, wbelImpLrdLiqtoNull, Len.WBEL_IMP_LRD_LIQTO_NULL);
    }

    /**Original name: WBEL-IMP-LRD-LIQTO-NULL<br>*/
    public String getWbelImpLrdLiqtoNull() {
        return readString(Pos.WBEL_IMP_LRD_LIQTO_NULL, Len.WBEL_IMP_LRD_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMP_LRD_LIQTO = 1;
        public static final int WBEL_IMP_LRD_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMP_LRD_LIQTO = 8;
        public static final int WBEL_IMP_LRD_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMP_LRD_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMP_LRD_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
