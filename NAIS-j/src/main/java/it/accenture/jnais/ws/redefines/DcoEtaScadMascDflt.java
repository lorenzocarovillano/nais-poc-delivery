package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCO-ETA-SCAD-MASC-DFLT<br>
 * Variable: DCO-ETA-SCAD-MASC-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DcoEtaScadMascDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DcoEtaScadMascDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DCO_ETA_SCAD_MASC_DFLT;
    }

    public void setDcoEtaScadMascDflt(int dcoEtaScadMascDflt) {
        writeIntAsPacked(Pos.DCO_ETA_SCAD_MASC_DFLT, dcoEtaScadMascDflt, Len.Int.DCO_ETA_SCAD_MASC_DFLT);
    }

    public void setDcoEtaScadMascDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DCO_ETA_SCAD_MASC_DFLT, Pos.DCO_ETA_SCAD_MASC_DFLT);
    }

    /**Original name: DCO-ETA-SCAD-MASC-DFLT<br>*/
    public int getDcoEtaScadMascDflt() {
        return readPackedAsInt(Pos.DCO_ETA_SCAD_MASC_DFLT, Len.Int.DCO_ETA_SCAD_MASC_DFLT);
    }

    public byte[] getDcoEtaScadMascDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DCO_ETA_SCAD_MASC_DFLT, Pos.DCO_ETA_SCAD_MASC_DFLT);
        return buffer;
    }

    public void setDcoEtaScadMascDfltNull(String dcoEtaScadMascDfltNull) {
        writeString(Pos.DCO_ETA_SCAD_MASC_DFLT_NULL, dcoEtaScadMascDfltNull, Len.DCO_ETA_SCAD_MASC_DFLT_NULL);
    }

    /**Original name: DCO-ETA-SCAD-MASC-DFLT-NULL<br>*/
    public String getDcoEtaScadMascDfltNull() {
        return readString(Pos.DCO_ETA_SCAD_MASC_DFLT_NULL, Len.DCO_ETA_SCAD_MASC_DFLT_NULL);
    }

    public String getDcoEtaScadMascDfltNullFormatted() {
        return Functions.padBlanks(getDcoEtaScadMascDfltNull(), Len.DCO_ETA_SCAD_MASC_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DCO_ETA_SCAD_MASC_DFLT = 1;
        public static final int DCO_ETA_SCAD_MASC_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_ETA_SCAD_MASC_DFLT = 3;
        public static final int DCO_ETA_SCAD_MASC_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_ETA_SCAD_MASC_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
