package it.accenture.jnais.ws.enums;

/**Original name: WCOM-STEP-SUCC<br>
 * Variable: WCOM-STEP-SUCC from copybook LCCC0261<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomStepSucc {

    //==== PROPERTIES ====
    private char value = 'Y';
    public static final char SI = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setStepSucc(char stepSucc) {
        this.value = stepSucc;
    }

    public char getStepSucc() {
        return this.value;
    }

    public void setWcomStepSuccSi() {
        value = SI;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int STEP_SUCC = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
