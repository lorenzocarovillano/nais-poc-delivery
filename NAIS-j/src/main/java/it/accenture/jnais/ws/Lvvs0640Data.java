package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import it.accenture.jnais.ws.enums.WkLiq;
import it.accenture.jnais.ws.occurs.S089TabLiq;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0640<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0640Data {

    //==== PROPERTIES ====
    public static final int DLQU_TAB_LIQ_MAXOCCURS = 30;
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: WK-LIQ
    private WkLiq wkLiq = new WkLiq();
    //Original name: DLQU-ELE-LIQ-MAX
    private short dlquEleLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DLQU-TAB-LIQ
    private S089TabLiq[] dlquTabLiq = new S089TabLiq[DLQU_TAB_LIQ_MAXOCCURS];
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-LQU
    private short ixTabLqu = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0640Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dlquTabLiqIdx = 1; dlquTabLiqIdx <= DLQU_TAB_LIQ_MAXOCCURS; dlquTabLiqIdx++) {
            dlquTabLiq[dlquTabLiqIdx - 1] = new S089TabLiq();
        }
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setDlquEleLiqMax(short dlquEleLiqMax) {
        this.dlquEleLiqMax = dlquEleLiqMax;
    }

    public short getDlquEleLiqMax() {
        return this.dlquEleLiqMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabLqu(short ixTabLqu) {
        this.ixTabLqu = ixTabLqu;
    }

    public short getIxTabLqu() {
        return this.ixTabLqu;
    }

    public S089TabLiq getDlquTabLiq(int idx) {
        return dlquTabLiq[idx - 1];
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    public WkLiq getWkLiq() {
        return wkLiq;
    }
}
