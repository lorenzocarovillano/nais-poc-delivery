package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0024-TP-MOVI-SOSP<br>
 * Variable: LCCC0024-TP-MOVI-SOSP from program LCCS0024<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Lccc0024TpMoviSosp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Lccc0024TpMoviSosp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0024_TP_MOVI_SOSP;
    }

    public void setLccc0024TpMoviSosp(int lccc0024TpMoviSosp) {
        writeIntAsPacked(Pos.LCCC0024_TP_MOVI_SOSP, lccc0024TpMoviSosp, Len.Int.LCCC0024_TP_MOVI_SOSP);
    }

    public void setLccc0024TpMoviSospFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LCCC0024_TP_MOVI_SOSP, Pos.LCCC0024_TP_MOVI_SOSP);
    }

    /**Original name: LCCC0024-TP-MOVI-SOSP<br>*/
    public int getLccc0024TpMoviSosp() {
        return readPackedAsInt(Pos.LCCC0024_TP_MOVI_SOSP, Len.Int.LCCC0024_TP_MOVI_SOSP);
    }

    public byte[] getLccc0024TpMoviSospAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LCCC0024_TP_MOVI_SOSP, Pos.LCCC0024_TP_MOVI_SOSP);
        return buffer;
    }

    public void initLccc0024TpMoviSospSpaces() {
        fill(Pos.LCCC0024_TP_MOVI_SOSP, Len.LCCC0024_TP_MOVI_SOSP, Types.SPACE_CHAR);
    }

    /**Original name: LCCC0024-TP-MOVI-SOSP-NULL<br>*/
    public String getLccc0024TpMoviSospNull() {
        return readString(Pos.LCCC0024_TP_MOVI_SOSP_NULL, Len.LCCC0024_TP_MOVI_SOSP_NULL);
    }

    public String getLccc0024TpMoviSospNullFormatted() {
        return Functions.padBlanks(getLccc0024TpMoviSospNull(), Len.LCCC0024_TP_MOVI_SOSP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LCCC0024_TP_MOVI_SOSP = 1;
        public static final int LCCC0024_TP_MOVI_SOSP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0024_TP_MOVI_SOSP = 3;
        public static final int LCCC0024_TP_MOVI_SOSP_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LCCC0024_TP_MOVI_SOSP = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
