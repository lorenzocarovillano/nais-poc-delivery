package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-AA-CNBZ-K1<br>
 * Variable: WDFA-AA-CNBZ-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaAaCnbzK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaAaCnbzK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_AA_CNBZ_K1;
    }

    public void setWdfaAaCnbzK1(short wdfaAaCnbzK1) {
        writeShortAsPacked(Pos.WDFA_AA_CNBZ_K1, wdfaAaCnbzK1, Len.Int.WDFA_AA_CNBZ_K1);
    }

    public void setWdfaAaCnbzK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_AA_CNBZ_K1, Pos.WDFA_AA_CNBZ_K1);
    }

    /**Original name: WDFA-AA-CNBZ-K1<br>*/
    public short getWdfaAaCnbzK1() {
        return readPackedAsShort(Pos.WDFA_AA_CNBZ_K1, Len.Int.WDFA_AA_CNBZ_K1);
    }

    public byte[] getWdfaAaCnbzK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_AA_CNBZ_K1, Pos.WDFA_AA_CNBZ_K1);
        return buffer;
    }

    public void setWdfaAaCnbzK1Null(String wdfaAaCnbzK1Null) {
        writeString(Pos.WDFA_AA_CNBZ_K1_NULL, wdfaAaCnbzK1Null, Len.WDFA_AA_CNBZ_K1_NULL);
    }

    /**Original name: WDFA-AA-CNBZ-K1-NULL<br>*/
    public String getWdfaAaCnbzK1Null() {
        return readString(Pos.WDFA_AA_CNBZ_K1_NULL, Len.WDFA_AA_CNBZ_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_AA_CNBZ_K1 = 1;
        public static final int WDFA_AA_CNBZ_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_AA_CNBZ_K1 = 3;
        public static final int WDFA_AA_CNBZ_K1_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_AA_CNBZ_K1 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
