package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-CPT-IN-OPZ-RIVTO<br>
 * Variable: TGA-CPT-IN-OPZ-RIVTO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaCptInOpzRivto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaCptInOpzRivto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_CPT_IN_OPZ_RIVTO;
    }

    public void setTgaCptInOpzRivto(AfDecimal tgaCptInOpzRivto) {
        writeDecimalAsPacked(Pos.TGA_CPT_IN_OPZ_RIVTO, tgaCptInOpzRivto.copy());
    }

    public void setTgaCptInOpzRivtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_CPT_IN_OPZ_RIVTO, Pos.TGA_CPT_IN_OPZ_RIVTO);
    }

    /**Original name: TGA-CPT-IN-OPZ-RIVTO<br>*/
    public AfDecimal getTgaCptInOpzRivto() {
        return readPackedAsDecimal(Pos.TGA_CPT_IN_OPZ_RIVTO, Len.Int.TGA_CPT_IN_OPZ_RIVTO, Len.Fract.TGA_CPT_IN_OPZ_RIVTO);
    }

    public byte[] getTgaCptInOpzRivtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_CPT_IN_OPZ_RIVTO, Pos.TGA_CPT_IN_OPZ_RIVTO);
        return buffer;
    }

    public void setTgaCptInOpzRivtoNull(String tgaCptInOpzRivtoNull) {
        writeString(Pos.TGA_CPT_IN_OPZ_RIVTO_NULL, tgaCptInOpzRivtoNull, Len.TGA_CPT_IN_OPZ_RIVTO_NULL);
    }

    /**Original name: TGA-CPT-IN-OPZ-RIVTO-NULL<br>*/
    public String getTgaCptInOpzRivtoNull() {
        return readString(Pos.TGA_CPT_IN_OPZ_RIVTO_NULL, Len.TGA_CPT_IN_OPZ_RIVTO_NULL);
    }

    public String getTgaCptInOpzRivtoNullFormatted() {
        return Functions.padBlanks(getTgaCptInOpzRivtoNull(), Len.TGA_CPT_IN_OPZ_RIVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_CPT_IN_OPZ_RIVTO = 1;
        public static final int TGA_CPT_IN_OPZ_RIVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_CPT_IN_OPZ_RIVTO = 8;
        public static final int TGA_CPT_IN_OPZ_RIVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_CPT_IN_OPZ_RIVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_CPT_IN_OPZ_RIVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
