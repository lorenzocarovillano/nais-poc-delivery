package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndParamOgg;
import it.accenture.jnais.copy.ParamDiCalcDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS1650<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs1650Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-PARAM-DI-CALC
    private IndParamOgg indParamDiCalc = new IndParamOgg();
    //Original name: PARAM-DI-CALC-DB
    private ParamDiCalcDb paramDiCalcDb = new ParamDiCalcDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndParamOgg getIndParamDiCalc() {
        return indParamDiCalc;
    }

    public ParamDiCalcDb getParamDiCalcDb() {
        return paramDiCalcDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
