package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-RIS-MAT-CHIU-PREC<br>
 * Variable: WB03-RIS-MAT-CHIU-PREC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03RisMatChiuPrec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03RisMatChiuPrec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_RIS_MAT_CHIU_PREC;
    }

    public void setWb03RisMatChiuPrec(AfDecimal wb03RisMatChiuPrec) {
        writeDecimalAsPacked(Pos.WB03_RIS_MAT_CHIU_PREC, wb03RisMatChiuPrec.copy());
    }

    public void setWb03RisMatChiuPrecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_RIS_MAT_CHIU_PREC, Pos.WB03_RIS_MAT_CHIU_PREC);
    }

    /**Original name: WB03-RIS-MAT-CHIU-PREC<br>*/
    public AfDecimal getWb03RisMatChiuPrec() {
        return readPackedAsDecimal(Pos.WB03_RIS_MAT_CHIU_PREC, Len.Int.WB03_RIS_MAT_CHIU_PREC, Len.Fract.WB03_RIS_MAT_CHIU_PREC);
    }

    public byte[] getWb03RisMatChiuPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_RIS_MAT_CHIU_PREC, Pos.WB03_RIS_MAT_CHIU_PREC);
        return buffer;
    }

    public void setWb03RisMatChiuPrecNull(String wb03RisMatChiuPrecNull) {
        writeString(Pos.WB03_RIS_MAT_CHIU_PREC_NULL, wb03RisMatChiuPrecNull, Len.WB03_RIS_MAT_CHIU_PREC_NULL);
    }

    /**Original name: WB03-RIS-MAT-CHIU-PREC-NULL<br>*/
    public String getWb03RisMatChiuPrecNull() {
        return readString(Pos.WB03_RIS_MAT_CHIU_PREC_NULL, Len.WB03_RIS_MAT_CHIU_PREC_NULL);
    }

    public String getWb03RisMatChiuPrecNullFormatted() {
        return Functions.padBlanks(getWb03RisMatChiuPrecNull(), Len.WB03_RIS_MAT_CHIU_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_RIS_MAT_CHIU_PREC = 1;
        public static final int WB03_RIS_MAT_CHIU_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_RIS_MAT_CHIU_PREC = 8;
        public static final int WB03_RIS_MAT_CHIU_PREC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_RIS_MAT_CHIU_PREC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_RIS_MAT_CHIU_PREC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
