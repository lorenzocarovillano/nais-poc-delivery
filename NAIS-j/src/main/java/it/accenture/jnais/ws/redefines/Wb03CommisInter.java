package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-COMMIS-INTER<br>
 * Variable: WB03-COMMIS-INTER from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_COMMIS_INTER;
    }

    public void setWb03CommisInter(AfDecimal wb03CommisInter) {
        writeDecimalAsPacked(Pos.WB03_COMMIS_INTER, wb03CommisInter.copy());
    }

    public void setWb03CommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_COMMIS_INTER, Pos.WB03_COMMIS_INTER);
    }

    /**Original name: WB03-COMMIS-INTER<br>*/
    public AfDecimal getWb03CommisInter() {
        return readPackedAsDecimal(Pos.WB03_COMMIS_INTER, Len.Int.WB03_COMMIS_INTER, Len.Fract.WB03_COMMIS_INTER);
    }

    public byte[] getWb03CommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_COMMIS_INTER, Pos.WB03_COMMIS_INTER);
        return buffer;
    }

    public void setWb03CommisInterNull(String wb03CommisInterNull) {
        writeString(Pos.WB03_COMMIS_INTER_NULL, wb03CommisInterNull, Len.WB03_COMMIS_INTER_NULL);
    }

    /**Original name: WB03-COMMIS-INTER-NULL<br>*/
    public String getWb03CommisInterNull() {
        return readString(Pos.WB03_COMMIS_INTER_NULL, Len.WB03_COMMIS_INTER_NULL);
    }

    public String getWb03CommisInterNullFormatted() {
        return Functions.padBlanks(getWb03CommisInterNull(), Len.WB03_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_COMMIS_INTER = 1;
        public static final int WB03_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_COMMIS_INTER = 8;
        public static final int WB03_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
