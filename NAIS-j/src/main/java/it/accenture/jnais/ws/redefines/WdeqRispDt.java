package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WDEQ-RISP-DT<br>
 * Variable: WDEQ-RISP-DT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdeqRispDt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdeqRispDt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDEQ_RISP_DT;
    }

    public void setWdeqRispDt(int wdeqRispDt) {
        writeIntAsPacked(Pos.WDEQ_RISP_DT, wdeqRispDt, Len.Int.WDEQ_RISP_DT);
    }

    public void setWdeqRispDtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDEQ_RISP_DT, Pos.WDEQ_RISP_DT);
    }

    /**Original name: WDEQ-RISP-DT<br>*/
    public int getWdeqRispDt() {
        return readPackedAsInt(Pos.WDEQ_RISP_DT, Len.Int.WDEQ_RISP_DT);
    }

    public byte[] getWdeqRispDtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDEQ_RISP_DT, Pos.WDEQ_RISP_DT);
        return buffer;
    }

    public void initWdeqRispDtSpaces() {
        fill(Pos.WDEQ_RISP_DT, Len.WDEQ_RISP_DT, Types.SPACE_CHAR);
    }

    public void setWdeqRispDtNull(String wdeqRispDtNull) {
        writeString(Pos.WDEQ_RISP_DT_NULL, wdeqRispDtNull, Len.WDEQ_RISP_DT_NULL);
    }

    /**Original name: WDEQ-RISP-DT-NULL<br>*/
    public String getWdeqRispDtNull() {
        return readString(Pos.WDEQ_RISP_DT_NULL, Len.WDEQ_RISP_DT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDEQ_RISP_DT = 1;
        public static final int WDEQ_RISP_DT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDEQ_RISP_DT = 5;
        public static final int WDEQ_RISP_DT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDEQ_RISP_DT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
