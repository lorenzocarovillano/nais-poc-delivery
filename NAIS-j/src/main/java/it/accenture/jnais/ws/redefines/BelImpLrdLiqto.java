package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-IMP-LRD-LIQTO<br>
 * Variable: BEL-IMP-LRD-LIQTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelImpLrdLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelImpLrdLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_IMP_LRD_LIQTO;
    }

    public void setBelImpLrdLiqto(AfDecimal belImpLrdLiqto) {
        writeDecimalAsPacked(Pos.BEL_IMP_LRD_LIQTO, belImpLrdLiqto.copy());
    }

    public void setBelImpLrdLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_IMP_LRD_LIQTO, Pos.BEL_IMP_LRD_LIQTO);
    }

    /**Original name: BEL-IMP-LRD-LIQTO<br>*/
    public AfDecimal getBelImpLrdLiqto() {
        return readPackedAsDecimal(Pos.BEL_IMP_LRD_LIQTO, Len.Int.BEL_IMP_LRD_LIQTO, Len.Fract.BEL_IMP_LRD_LIQTO);
    }

    public byte[] getBelImpLrdLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_IMP_LRD_LIQTO, Pos.BEL_IMP_LRD_LIQTO);
        return buffer;
    }

    public void setBelImpLrdLiqtoNull(String belImpLrdLiqtoNull) {
        writeString(Pos.BEL_IMP_LRD_LIQTO_NULL, belImpLrdLiqtoNull, Len.BEL_IMP_LRD_LIQTO_NULL);
    }

    /**Original name: BEL-IMP-LRD-LIQTO-NULL<br>*/
    public String getBelImpLrdLiqtoNull() {
        return readString(Pos.BEL_IMP_LRD_LIQTO_NULL, Len.BEL_IMP_LRD_LIQTO_NULL);
    }

    public String getBelImpLrdLiqtoNullFormatted() {
        return Functions.padBlanks(getBelImpLrdLiqtoNull(), Len.BEL_IMP_LRD_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_IMP_LRD_LIQTO = 1;
        public static final int BEL_IMP_LRD_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_IMP_LRD_LIQTO = 8;
        public static final int BEL_IMP_LRD_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_IMP_LRD_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_IMP_LRD_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
