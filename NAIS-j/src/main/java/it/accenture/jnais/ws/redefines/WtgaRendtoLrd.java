package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-RENDTO-LRD<br>
 * Variable: WTGA-RENDTO-LRD from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaRendtoLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaRendtoLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_RENDTO_LRD;
    }

    public void setWtgaRendtoLrd(AfDecimal wtgaRendtoLrd) {
        writeDecimalAsPacked(Pos.WTGA_RENDTO_LRD, wtgaRendtoLrd.copy());
    }

    public void setWtgaRendtoLrdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_RENDTO_LRD, Pos.WTGA_RENDTO_LRD);
    }

    /**Original name: WTGA-RENDTO-LRD<br>*/
    public AfDecimal getWtgaRendtoLrd() {
        return readPackedAsDecimal(Pos.WTGA_RENDTO_LRD, Len.Int.WTGA_RENDTO_LRD, Len.Fract.WTGA_RENDTO_LRD);
    }

    public byte[] getWtgaRendtoLrdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_RENDTO_LRD, Pos.WTGA_RENDTO_LRD);
        return buffer;
    }

    public void initWtgaRendtoLrdSpaces() {
        fill(Pos.WTGA_RENDTO_LRD, Len.WTGA_RENDTO_LRD, Types.SPACE_CHAR);
    }

    public void setWtgaRendtoLrdNull(String wtgaRendtoLrdNull) {
        writeString(Pos.WTGA_RENDTO_LRD_NULL, wtgaRendtoLrdNull, Len.WTGA_RENDTO_LRD_NULL);
    }

    /**Original name: WTGA-RENDTO-LRD-NULL<br>*/
    public String getWtgaRendtoLrdNull() {
        return readString(Pos.WTGA_RENDTO_LRD_NULL, Len.WTGA_RENDTO_LRD_NULL);
    }

    public String getWtgaRendtoLrdNullFormatted() {
        return Functions.padBlanks(getWtgaRendtoLrdNull(), Len.WTGA_RENDTO_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_RENDTO_LRD = 1;
        public static final int WTGA_RENDTO_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_RENDTO_LRD = 8;
        public static final int WTGA_RENDTO_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_RENDTO_LRD = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_RENDTO_LRD = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
