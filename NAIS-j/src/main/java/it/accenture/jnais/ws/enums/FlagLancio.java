package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-LANCIO<br>
 * Variable: FLAG-LANCIO from program LRGM0380<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagLancio {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_LANCIO);
    public static final String SI = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setFlagLancio(String flagLancio) {
        this.value = Functions.subString(flagLancio, Len.FLAG_LANCIO);
    }

    public String getFlagLancio() {
        return this.value;
    }

    public boolean isSi() {
        return value.equals(SI);
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_LANCIO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
