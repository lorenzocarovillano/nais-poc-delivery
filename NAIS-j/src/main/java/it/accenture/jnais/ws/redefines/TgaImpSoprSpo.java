package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-SOPR-SPO<br>
 * Variable: TGA-IMP-SOPR-SPO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_SOPR_SPO;
    }

    public void setTgaImpSoprSpo(AfDecimal tgaImpSoprSpo) {
        writeDecimalAsPacked(Pos.TGA_IMP_SOPR_SPO, tgaImpSoprSpo.copy());
    }

    public void setTgaImpSoprSpoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_SOPR_SPO, Pos.TGA_IMP_SOPR_SPO);
    }

    /**Original name: TGA-IMP-SOPR-SPO<br>*/
    public AfDecimal getTgaImpSoprSpo() {
        return readPackedAsDecimal(Pos.TGA_IMP_SOPR_SPO, Len.Int.TGA_IMP_SOPR_SPO, Len.Fract.TGA_IMP_SOPR_SPO);
    }

    public byte[] getTgaImpSoprSpoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_SOPR_SPO, Pos.TGA_IMP_SOPR_SPO);
        return buffer;
    }

    public void setTgaImpSoprSpoNull(String tgaImpSoprSpoNull) {
        writeString(Pos.TGA_IMP_SOPR_SPO_NULL, tgaImpSoprSpoNull, Len.TGA_IMP_SOPR_SPO_NULL);
    }

    /**Original name: TGA-IMP-SOPR-SPO-NULL<br>*/
    public String getTgaImpSoprSpoNull() {
        return readString(Pos.TGA_IMP_SOPR_SPO_NULL, Len.TGA_IMP_SOPR_SPO_NULL);
    }

    public String getTgaImpSoprSpoNullFormatted() {
        return Functions.padBlanks(getTgaImpSoprSpoNull(), Len.TGA_IMP_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_SOPR_SPO = 1;
        public static final int TGA_IMP_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_SOPR_SPO = 8;
        public static final int TGA_IMP_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
