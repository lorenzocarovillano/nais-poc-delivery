package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WpmoTabParamMov;

/**Original name: AREA-MAIN<br>
 * Variable: AREA-MAIN from program LOAS0310<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaMainLoas0310 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int WPMO_TAB_PARAM_MOV_MAXOCCURS = 50;
    //Original name: WPMO-ELE-PARAM-MOV-MAX
    private short wpmoEleParamMovMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPMO-TAB-PARAM-MOV
    private WpmoTabParamMov[] wpmoTabParamMov = new WpmoTabParamMov[WPMO_TAB_PARAM_MOV_MAXOCCURS];
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza = new WpolAreaPolizzaLccs0005();
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLoas0800 wadeAreaAdesione = new WadeAreaAdesioneLoas0800();
    //Original name: WMOV-AREA-MOVIMENTO
    private WmovAreaMovimento wmovAreaMovimento = new WmovAreaMovimento();

    //==== CONSTRUCTORS ====
    public AreaMainLoas0310() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_MAIN;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaMainBytes(buf);
    }

    public void init() {
        for (int wpmoTabParamMovIdx = 1; wpmoTabParamMovIdx <= WPMO_TAB_PARAM_MOV_MAXOCCURS; wpmoTabParamMovIdx++) {
            wpmoTabParamMov[wpmoTabParamMovIdx - 1] = new WpmoTabParamMov();
        }
    }

    public String getAreaMainFormatted() {
        return MarshalByteExt.bufferToStr(getAreaMainBytes());
    }

    public void setAreaMainBytes(byte[] buffer) {
        setAreaMainBytes(buffer, 1);
    }

    public byte[] getAreaMainBytes() {
        byte[] buffer = new byte[Len.AREA_MAIN];
        return getAreaMainBytes(buffer, 1);
    }

    public void setAreaMainBytes(byte[] buffer, int offset) {
        int position = offset;
        setWpmoAreaParamMoviBytes(buffer, position);
        position += Len.WPMO_AREA_PARAM_MOVI;
        wpolAreaPolizza.setWpolAreaPolizzaBytes(buffer, position);
        position += WpolAreaPolizzaLccs0005.Len.WPOL_AREA_POLIZZA;
        wadeAreaAdesione.setWadeAreaAdesioneBytes(buffer, position);
        position += WadeAreaAdesioneLoas0800.Len.WADE_AREA_ADESIONE;
        wmovAreaMovimento.setWmovAreaMovimentoBytes(buffer, position);
    }

    public byte[] getAreaMainBytes(byte[] buffer, int offset) {
        int position = offset;
        getWpmoAreaParamMoviBytes(buffer, position);
        position += Len.WPMO_AREA_PARAM_MOVI;
        wpolAreaPolizza.getWpolAreaPolizzaBytes(buffer, position);
        position += WpolAreaPolizzaLccs0005.Len.WPOL_AREA_POLIZZA;
        wadeAreaAdesione.getWadeAreaAdesioneBytes(buffer, position);
        position += WadeAreaAdesioneLoas0800.Len.WADE_AREA_ADESIONE;
        wmovAreaMovimento.getWmovAreaMovimentoBytes(buffer, position);
        return buffer;
    }

    public String getWpmoAreaParamMoviFormatted() {
        return MarshalByteExt.bufferToStr(getWpmoAreaParamMoviBytes());
    }

    /**Original name: WPMO-AREA-PARAM-MOVI<br>
	 * <pre> -- Area parametro movimento</pre>*/
    public byte[] getWpmoAreaParamMoviBytes() {
        byte[] buffer = new byte[Len.WPMO_AREA_PARAM_MOVI];
        return getWpmoAreaParamMoviBytes(buffer, 1);
    }

    public void setWpmoAreaParamMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        wpmoEleParamMovMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPMO_TAB_PARAM_MOV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpmoTabParamMov[idx - 1].setWpmoTabParamMovBytes(buffer, position);
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
            else {
                wpmoTabParamMov[idx - 1].initWpmoTabParamMovSpaces();
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
        }
    }

    public byte[] getWpmoAreaParamMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpmoEleParamMovMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPMO_TAB_PARAM_MOV_MAXOCCURS; idx++) {
            wpmoTabParamMov[idx - 1].getWpmoTabParamMovBytes(buffer, position);
            position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
        }
        return buffer;
    }

    public void setWpmoEleParamMovMax(short wpmoEleParamMovMax) {
        this.wpmoEleParamMovMax = wpmoEleParamMovMax;
    }

    public short getWpmoEleParamMovMax() {
        return this.wpmoEleParamMovMax;
    }

    public WadeAreaAdesioneLoas0800 getWadeAreaAdesione() {
        return wadeAreaAdesione;
    }

    public WmovAreaMovimento getWmovAreaMovimento() {
        return wmovAreaMovimento;
    }

    public WpmoTabParamMov getWpmoTabParamMov(int idx) {
        return wpmoTabParamMov[idx - 1];
    }

    public WpolAreaPolizzaLccs0005 getWpolAreaPolizza() {
        return wpolAreaPolizza;
    }

    @Override
    public byte[] serialize() {
        return getAreaMainBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_ELE_PARAM_MOV_MAX = 2;
        public static final int WPMO_AREA_PARAM_MOVI = WPMO_ELE_PARAM_MOV_MAX + AreaMainLoas0310.WPMO_TAB_PARAM_MOV_MAXOCCURS * WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
        public static final int AREA_MAIN = WPMO_AREA_PARAM_MOVI + WpolAreaPolizzaLccs0005.Len.WPOL_AREA_POLIZZA + WadeAreaAdesioneLoas0800.Len.WADE_AREA_ADESIONE + WmovAreaMovimento.Len.WMOV_AREA_MOVIMENTO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
