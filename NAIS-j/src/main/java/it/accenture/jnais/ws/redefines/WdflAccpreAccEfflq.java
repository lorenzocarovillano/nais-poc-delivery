package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ACCPRE-ACC-EFFLQ<br>
 * Variable: WDFL-ACCPRE-ACC-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAccpreAccEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAccpreAccEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ACCPRE_ACC_EFFLQ;
    }

    public void setWdflAccpreAccEfflq(AfDecimal wdflAccpreAccEfflq) {
        writeDecimalAsPacked(Pos.WDFL_ACCPRE_ACC_EFFLQ, wdflAccpreAccEfflq.copy());
    }

    public void setWdflAccpreAccEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ACCPRE_ACC_EFFLQ, Pos.WDFL_ACCPRE_ACC_EFFLQ);
    }

    /**Original name: WDFL-ACCPRE-ACC-EFFLQ<br>*/
    public AfDecimal getWdflAccpreAccEfflq() {
        return readPackedAsDecimal(Pos.WDFL_ACCPRE_ACC_EFFLQ, Len.Int.WDFL_ACCPRE_ACC_EFFLQ, Len.Fract.WDFL_ACCPRE_ACC_EFFLQ);
    }

    public byte[] getWdflAccpreAccEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ACCPRE_ACC_EFFLQ, Pos.WDFL_ACCPRE_ACC_EFFLQ);
        return buffer;
    }

    public void setWdflAccpreAccEfflqNull(String wdflAccpreAccEfflqNull) {
        writeString(Pos.WDFL_ACCPRE_ACC_EFFLQ_NULL, wdflAccpreAccEfflqNull, Len.WDFL_ACCPRE_ACC_EFFLQ_NULL);
    }

    /**Original name: WDFL-ACCPRE-ACC-EFFLQ-NULL<br>*/
    public String getWdflAccpreAccEfflqNull() {
        return readString(Pos.WDFL_ACCPRE_ACC_EFFLQ_NULL, Len.WDFL_ACCPRE_ACC_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_ACC_EFFLQ = 1;
        public static final int WDFL_ACCPRE_ACC_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_ACC_EFFLQ = 8;
        public static final int WDFL_ACCPRE_ACC_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_ACC_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_ACC_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
