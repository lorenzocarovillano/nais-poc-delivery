package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-PRE-NET-IND<br>
 * Variable: WADE-PRE-NET-IND from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadePreNetInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadePreNetInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_PRE_NET_IND;
    }

    public void setWadePreNetInd(AfDecimal wadePreNetInd) {
        writeDecimalAsPacked(Pos.WADE_PRE_NET_IND, wadePreNetInd.copy());
    }

    public void setWadePreNetIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_PRE_NET_IND, Pos.WADE_PRE_NET_IND);
    }

    /**Original name: WADE-PRE-NET-IND<br>*/
    public AfDecimal getWadePreNetInd() {
        return readPackedAsDecimal(Pos.WADE_PRE_NET_IND, Len.Int.WADE_PRE_NET_IND, Len.Fract.WADE_PRE_NET_IND);
    }

    public byte[] getWadePreNetIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_PRE_NET_IND, Pos.WADE_PRE_NET_IND);
        return buffer;
    }

    public void initWadePreNetIndSpaces() {
        fill(Pos.WADE_PRE_NET_IND, Len.WADE_PRE_NET_IND, Types.SPACE_CHAR);
    }

    public void setWadePreNetIndNull(String wadePreNetIndNull) {
        writeString(Pos.WADE_PRE_NET_IND_NULL, wadePreNetIndNull, Len.WADE_PRE_NET_IND_NULL);
    }

    /**Original name: WADE-PRE-NET-IND-NULL<br>*/
    public String getWadePreNetIndNull() {
        return readString(Pos.WADE_PRE_NET_IND_NULL, Len.WADE_PRE_NET_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_PRE_NET_IND = 1;
        public static final int WADE_PRE_NET_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_PRE_NET_IND = 8;
        public static final int WADE_PRE_NET_IND_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_PRE_NET_IND = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_PRE_NET_IND = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
