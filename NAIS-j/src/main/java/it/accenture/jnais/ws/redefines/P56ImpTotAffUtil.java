package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-IMP-TOT-AFF-UTIL<br>
 * Variable: P56-IMP-TOT-AFF-UTIL from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56ImpTotAffUtil extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56ImpTotAffUtil() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_IMP_TOT_AFF_UTIL;
    }

    public void setP56ImpTotAffUtil(AfDecimal p56ImpTotAffUtil) {
        writeDecimalAsPacked(Pos.P56_IMP_TOT_AFF_UTIL, p56ImpTotAffUtil.copy());
    }

    public void setP56ImpTotAffUtilFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_IMP_TOT_AFF_UTIL, Pos.P56_IMP_TOT_AFF_UTIL);
    }

    /**Original name: P56-IMP-TOT-AFF-UTIL<br>*/
    public AfDecimal getP56ImpTotAffUtil() {
        return readPackedAsDecimal(Pos.P56_IMP_TOT_AFF_UTIL, Len.Int.P56_IMP_TOT_AFF_UTIL, Len.Fract.P56_IMP_TOT_AFF_UTIL);
    }

    public byte[] getP56ImpTotAffUtilAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_IMP_TOT_AFF_UTIL, Pos.P56_IMP_TOT_AFF_UTIL);
        return buffer;
    }

    public void setP56ImpTotAffUtilNull(String p56ImpTotAffUtilNull) {
        writeString(Pos.P56_IMP_TOT_AFF_UTIL_NULL, p56ImpTotAffUtilNull, Len.P56_IMP_TOT_AFF_UTIL_NULL);
    }

    /**Original name: P56-IMP-TOT-AFF-UTIL-NULL<br>*/
    public String getP56ImpTotAffUtilNull() {
        return readString(Pos.P56_IMP_TOT_AFF_UTIL_NULL, Len.P56_IMP_TOT_AFF_UTIL_NULL);
    }

    public String getP56ImpTotAffUtilNullFormatted() {
        return Functions.padBlanks(getP56ImpTotAffUtilNull(), Len.P56_IMP_TOT_AFF_UTIL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_IMP_TOT_AFF_UTIL = 1;
        public static final int P56_IMP_TOT_AFF_UTIL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_IMP_TOT_AFF_UTIL = 8;
        public static final int P56_IMP_TOT_AFF_UTIL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_IMP_TOT_AFF_UTIL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_IMP_TOT_AFF_UTIL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
