package it.accenture.jnais.ws;

import it.accenture.jnais.copy.DettTitContDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndDettTitCont;
import it.accenture.jnais.copy.Ldbvf111Input;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBSF980<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbsf980Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-DETT-TIT-CONT
    private IndDettTitCont indDettTitCont = new IndDettTitCont();
    //Original name: DETT-TIT-CONT-DB
    private DettTitContDb dettTitContDb = new DettTitContDb();
    //Original name: LDBVF981
    private Ldbvf111Input ldbvf981 = new Ldbvf111Input();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public DettTitContDb getDettTitContDb() {
        return dettTitContDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndDettTitCont getIndDettTitCont() {
        return indDettTitCont;
    }

    public Ldbvf111Input getLdbvf981() {
        return ldbvf981;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
