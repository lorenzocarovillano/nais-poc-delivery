package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CPT-RIASTO-ECC<br>
 * Variable: WB03-CPT-RIASTO-ECC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CptRiastoEcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CptRiastoEcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CPT_RIASTO_ECC;
    }

    public void setWb03CptRiastoEccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CPT_RIASTO_ECC, Pos.WB03_CPT_RIASTO_ECC);
    }

    /**Original name: WB03-CPT-RIASTO-ECC<br>*/
    public AfDecimal getWb03CptRiastoEcc() {
        return readPackedAsDecimal(Pos.WB03_CPT_RIASTO_ECC, Len.Int.WB03_CPT_RIASTO_ECC, Len.Fract.WB03_CPT_RIASTO_ECC);
    }

    public byte[] getWb03CptRiastoEccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CPT_RIASTO_ECC, Pos.WB03_CPT_RIASTO_ECC);
        return buffer;
    }

    public void setWb03CptRiastoEccNull(String wb03CptRiastoEccNull) {
        writeString(Pos.WB03_CPT_RIASTO_ECC_NULL, wb03CptRiastoEccNull, Len.WB03_CPT_RIASTO_ECC_NULL);
    }

    /**Original name: WB03-CPT-RIASTO-ECC-NULL<br>*/
    public String getWb03CptRiastoEccNull() {
        return readString(Pos.WB03_CPT_RIASTO_ECC_NULL, Len.WB03_CPT_RIASTO_ECC_NULL);
    }

    public String getWb03CptRiastoEccNullFormatted() {
        return Functions.padBlanks(getWb03CptRiastoEccNull(), Len.WB03_CPT_RIASTO_ECC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CPT_RIASTO_ECC = 1;
        public static final int WB03_CPT_RIASTO_ECC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CPT_RIASTO_ECC = 8;
        public static final int WB03_CPT_RIASTO_ECC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CPT_RIASTO_ECC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CPT_RIASTO_ECC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
