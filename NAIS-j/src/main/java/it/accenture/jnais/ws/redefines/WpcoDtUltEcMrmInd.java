package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-EC-MRM-IND<br>
 * Variable: WPCO-DT-ULT-EC-MRM-IND from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEcMrmInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEcMrmInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_EC_MRM_IND;
    }

    public void setWpcoDtUltEcMrmInd(int wpcoDtUltEcMrmInd) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_EC_MRM_IND, wpcoDtUltEcMrmInd, Len.Int.WPCO_DT_ULT_EC_MRM_IND);
    }

    public void setDpcoDtUltEcMrmIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_EC_MRM_IND, Pos.WPCO_DT_ULT_EC_MRM_IND);
    }

    /**Original name: WPCO-DT-ULT-EC-MRM-IND<br>*/
    public int getWpcoDtUltEcMrmInd() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_EC_MRM_IND, Len.Int.WPCO_DT_ULT_EC_MRM_IND);
    }

    public byte[] getWpcoDtUltEcMrmIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_EC_MRM_IND, Pos.WPCO_DT_ULT_EC_MRM_IND);
        return buffer;
    }

    public void setWpcoDtUltEcMrmIndNull(String wpcoDtUltEcMrmIndNull) {
        writeString(Pos.WPCO_DT_ULT_EC_MRM_IND_NULL, wpcoDtUltEcMrmIndNull, Len.WPCO_DT_ULT_EC_MRM_IND_NULL);
    }

    /**Original name: WPCO-DT-ULT-EC-MRM-IND-NULL<br>*/
    public String getWpcoDtUltEcMrmIndNull() {
        return readString(Pos.WPCO_DT_ULT_EC_MRM_IND_NULL, Len.WPCO_DT_ULT_EC_MRM_IND_NULL);
    }

    public String getWpcoDtUltEcMrmIndNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEcMrmIndNull(), Len.WPCO_DT_ULT_EC_MRM_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_MRM_IND = 1;
        public static final int WPCO_DT_ULT_EC_MRM_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_MRM_IND = 5;
        public static final int WPCO_DT_ULT_EC_MRM_IND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_EC_MRM_IND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
