package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRSTZ-RID-INI<br>
 * Variable: TGA-PRSTZ-RID-INI from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPrstzRidIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPrstzRidIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRSTZ_RID_INI;
    }

    public void setTgaPrstzRidIni(AfDecimal tgaPrstzRidIni) {
        writeDecimalAsPacked(Pos.TGA_PRSTZ_RID_INI, tgaPrstzRidIni.copy());
    }

    public void setTgaPrstzRidIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRSTZ_RID_INI, Pos.TGA_PRSTZ_RID_INI);
    }

    /**Original name: TGA-PRSTZ-RID-INI<br>*/
    public AfDecimal getTgaPrstzRidIni() {
        return readPackedAsDecimal(Pos.TGA_PRSTZ_RID_INI, Len.Int.TGA_PRSTZ_RID_INI, Len.Fract.TGA_PRSTZ_RID_INI);
    }

    public byte[] getTgaPrstzRidIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRSTZ_RID_INI, Pos.TGA_PRSTZ_RID_INI);
        return buffer;
    }

    public void setTgaPrstzRidIniNull(String tgaPrstzRidIniNull) {
        writeString(Pos.TGA_PRSTZ_RID_INI_NULL, tgaPrstzRidIniNull, Len.TGA_PRSTZ_RID_INI_NULL);
    }

    /**Original name: TGA-PRSTZ-RID-INI-NULL<br>*/
    public String getTgaPrstzRidIniNull() {
        return readString(Pos.TGA_PRSTZ_RID_INI_NULL, Len.TGA_PRSTZ_RID_INI_NULL);
    }

    public String getTgaPrstzRidIniNullFormatted() {
        return Functions.padBlanks(getTgaPrstzRidIniNull(), Len.TGA_PRSTZ_RID_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_RID_INI = 1;
        public static final int TGA_PRSTZ_RID_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_RID_INI = 8;
        public static final int TGA_PRSTZ_RID_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_RID_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_RID_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
