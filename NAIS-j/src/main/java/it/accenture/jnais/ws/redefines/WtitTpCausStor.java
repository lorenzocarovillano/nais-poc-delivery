package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-TP-CAUS-STOR<br>
 * Variable: WTIT-TP-CAUS-STOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTpCausStor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTpCausStor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TP_CAUS_STOR;
    }

    public void setWtitTpCausStor(int wtitTpCausStor) {
        writeIntAsPacked(Pos.WTIT_TP_CAUS_STOR, wtitTpCausStor, Len.Int.WTIT_TP_CAUS_STOR);
    }

    public void setWtitTpCausStorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TP_CAUS_STOR, Pos.WTIT_TP_CAUS_STOR);
    }

    /**Original name: WTIT-TP-CAUS-STOR<br>*/
    public int getWtitTpCausStor() {
        return readPackedAsInt(Pos.WTIT_TP_CAUS_STOR, Len.Int.WTIT_TP_CAUS_STOR);
    }

    public byte[] getWtitTpCausStorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TP_CAUS_STOR, Pos.WTIT_TP_CAUS_STOR);
        return buffer;
    }

    public void initWtitTpCausStorSpaces() {
        fill(Pos.WTIT_TP_CAUS_STOR, Len.WTIT_TP_CAUS_STOR, Types.SPACE_CHAR);
    }

    public void setWtitTpCausStorNull(String wtitTpCausStorNull) {
        writeString(Pos.WTIT_TP_CAUS_STOR_NULL, wtitTpCausStorNull, Len.WTIT_TP_CAUS_STOR_NULL);
    }

    /**Original name: WTIT-TP-CAUS-STOR-NULL<br>*/
    public String getWtitTpCausStorNull() {
        return readString(Pos.WTIT_TP_CAUS_STOR_NULL, Len.WTIT_TP_CAUS_STOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TP_CAUS_STOR = 1;
        public static final int WTIT_TP_CAUS_STOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TP_CAUS_STOR = 3;
        public static final int WTIT_TP_CAUS_STOR_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TP_CAUS_STOR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
