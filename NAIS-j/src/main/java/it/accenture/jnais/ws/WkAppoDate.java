package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-APPO-DATE<br>
 * Variable: WK-APPO-DATE from program LVVS0116<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkAppoDate {

    //==== PROPERTIES ====
    //Original name: WK-APPO-DATA-INIZIO-EFFETTO
    private int inizioEffetto = DefaultValues.INT_VAL;
    //Original name: WK-APPO-DATA-FINE-EFFETTO
    private int fineEffetto = DefaultValues.INT_VAL;
    //Original name: WK-APPO-DATA-COMPETENZA
    private long competenza = DefaultValues.LONG_VAL;

    //==== METHODS ====
    public void setInizioEffetto(int inizioEffetto) {
        this.inizioEffetto = inizioEffetto;
    }

    public int getInizioEffetto() {
        return this.inizioEffetto;
    }

    public void setFineEffetto(int fineEffetto) {
        this.fineEffetto = fineEffetto;
    }

    public int getFineEffetto() {
        return this.fineEffetto;
    }

    public void setCompetenza(long competenza) {
        this.competenza = competenza;
    }

    public long getCompetenza() {
        return this.competenza;
    }
}
