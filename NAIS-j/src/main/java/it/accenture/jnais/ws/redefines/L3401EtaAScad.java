package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-ETA-A-SCAD<br>
 * Variable: L3401-ETA-A-SCAD from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401EtaAScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401EtaAScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_ETA_A_SCAD;
    }

    public void setL3401EtaAScad(int l3401EtaAScad) {
        writeIntAsPacked(Pos.L3401_ETA_A_SCAD, l3401EtaAScad, Len.Int.L3401_ETA_A_SCAD);
    }

    public void setL3401EtaAScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_ETA_A_SCAD, Pos.L3401_ETA_A_SCAD);
    }

    /**Original name: L3401-ETA-A-SCAD<br>*/
    public int getL3401EtaAScad() {
        return readPackedAsInt(Pos.L3401_ETA_A_SCAD, Len.Int.L3401_ETA_A_SCAD);
    }

    public byte[] getL3401EtaAScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_ETA_A_SCAD, Pos.L3401_ETA_A_SCAD);
        return buffer;
    }

    /**Original name: L3401-ETA-A-SCAD-NULL<br>*/
    public String getL3401EtaAScadNull() {
        return readString(Pos.L3401_ETA_A_SCAD_NULL, Len.L3401_ETA_A_SCAD_NULL);
    }

    public String getL3401EtaAScadNullFormatted() {
        return Functions.padBlanks(getL3401EtaAScadNull(), Len.L3401_ETA_A_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_ETA_A_SCAD = 1;
        public static final int L3401_ETA_A_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_ETA_A_SCAD = 3;
        public static final int L3401_ETA_A_SCAD_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_ETA_A_SCAD = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
