package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: LOR-TIPO-OGGETTO<br>
 * Variable: LOR-TIPO-OGGETTO from program IEAS9700<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LorTipoOggetto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LorTipoOggetto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LOR_TIPO_OGGETTO;
    }

    public void setLorTipoOggetto(short lorTipoOggetto) {
        writeShortAsPacked(Pos.LOR_TIPO_OGGETTO, lorTipoOggetto, Len.Int.LOR_TIPO_OGGETTO);
    }

    public void setLorTipoOggettoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LOR_TIPO_OGGETTO, Pos.LOR_TIPO_OGGETTO);
    }

    /**Original name: LOR-TIPO-OGGETTO<br>*/
    public short getLorTipoOggetto() {
        return readPackedAsShort(Pos.LOR_TIPO_OGGETTO, Len.Int.LOR_TIPO_OGGETTO);
    }

    public byte[] getLorTipoOggettoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LOR_TIPO_OGGETTO, Pos.LOR_TIPO_OGGETTO);
        return buffer;
    }

    public void initLorTipoOggettoHighValues() {
        fill(Pos.LOR_TIPO_OGGETTO, Len.LOR_TIPO_OGGETTO, Types.HIGH_CHAR_VAL);
    }

    public void setLorTipoOggettoNull(String lorTipoOggettoNull) {
        writeString(Pos.LOR_TIPO_OGGETTO_NULL, lorTipoOggettoNull, Len.LOR_TIPO_OGGETTO_NULL);
    }

    /**Original name: LOR-TIPO-OGGETTO-NULL<br>*/
    public String getLorTipoOggettoNull() {
        return readString(Pos.LOR_TIPO_OGGETTO_NULL, Len.LOR_TIPO_OGGETTO_NULL);
    }

    public String getLorTipoOggettoNullFormatted() {
        return Functions.padBlanks(getLorTipoOggettoNull(), Len.LOR_TIPO_OGGETTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LOR_TIPO_OGGETTO = 1;
        public static final int LOR_TIPO_OGGETTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LOR_TIPO_OGGETTO = 2;
        public static final int LOR_TIPO_OGGETTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LOR_TIPO_OGGETTO = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
