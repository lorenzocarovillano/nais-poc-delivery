package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-SOPR-ALT<br>
 * Variable: DTR-SOPR-ALT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrSoprAlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrSoprAlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_SOPR_ALT;
    }

    public void setDtrSoprAlt(AfDecimal dtrSoprAlt) {
        writeDecimalAsPacked(Pos.DTR_SOPR_ALT, dtrSoprAlt.copy());
    }

    public void setDtrSoprAltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_SOPR_ALT, Pos.DTR_SOPR_ALT);
    }

    /**Original name: DTR-SOPR-ALT<br>*/
    public AfDecimal getDtrSoprAlt() {
        return readPackedAsDecimal(Pos.DTR_SOPR_ALT, Len.Int.DTR_SOPR_ALT, Len.Fract.DTR_SOPR_ALT);
    }

    public byte[] getDtrSoprAltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_SOPR_ALT, Pos.DTR_SOPR_ALT);
        return buffer;
    }

    public void setDtrSoprAltNull(String dtrSoprAltNull) {
        writeString(Pos.DTR_SOPR_ALT_NULL, dtrSoprAltNull, Len.DTR_SOPR_ALT_NULL);
    }

    /**Original name: DTR-SOPR-ALT-NULL<br>*/
    public String getDtrSoprAltNull() {
        return readString(Pos.DTR_SOPR_ALT_NULL, Len.DTR_SOPR_ALT_NULL);
    }

    public String getDtrSoprAltNullFormatted() {
        return Functions.padBlanks(getDtrSoprAltNull(), Len.DTR_SOPR_ALT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_SOPR_ALT = 1;
        public static final int DTR_SOPR_ALT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_SOPR_ALT = 8;
        public static final int DTR_SOPR_ALT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_SOPR_ALT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_SOPR_ALT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
