package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-CPT-DT-STAB<br>
 * Variable: W-B03-CPT-DT-STAB from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CptDtStabLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CptDtStabLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_CPT_DT_STAB;
    }

    public void setwB03CptDtStab(AfDecimal wB03CptDtStab) {
        writeDecimalAsPacked(Pos.W_B03_CPT_DT_STAB, wB03CptDtStab.copy());
    }

    /**Original name: W-B03-CPT-DT-STAB<br>*/
    public AfDecimal getwB03CptDtStab() {
        return readPackedAsDecimal(Pos.W_B03_CPT_DT_STAB, Len.Int.W_B03_CPT_DT_STAB, Len.Fract.W_B03_CPT_DT_STAB);
    }

    public byte[] getwB03CptDtStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_CPT_DT_STAB, Pos.W_B03_CPT_DT_STAB);
        return buffer;
    }

    public void setwB03CptDtStabNull(String wB03CptDtStabNull) {
        writeString(Pos.W_B03_CPT_DT_STAB_NULL, wB03CptDtStabNull, Len.W_B03_CPT_DT_STAB_NULL);
    }

    /**Original name: W-B03-CPT-DT-STAB-NULL<br>*/
    public String getwB03CptDtStabNull() {
        return readString(Pos.W_B03_CPT_DT_STAB_NULL, Len.W_B03_CPT_DT_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_CPT_DT_STAB = 1;
        public static final int W_B03_CPT_DT_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_CPT_DT_STAB = 8;
        public static final int W_B03_CPT_DT_STAB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_CPT_DT_STAB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_CPT_DT_STAB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
