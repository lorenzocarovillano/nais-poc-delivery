package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP01-DT-EST-FINANZ<br>
 * Variable: WP01-DT-EST-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp01DtEstFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp01DtEstFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP01_DT_EST_FINANZ;
    }

    public void setWp01DtEstFinanz(int wp01DtEstFinanz) {
        writeIntAsPacked(Pos.WP01_DT_EST_FINANZ, wp01DtEstFinanz, Len.Int.WP01_DT_EST_FINANZ);
    }

    public void setWp01DtEstFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP01_DT_EST_FINANZ, Pos.WP01_DT_EST_FINANZ);
    }

    /**Original name: WP01-DT-EST-FINANZ<br>*/
    public int getWp01DtEstFinanz() {
        return readPackedAsInt(Pos.WP01_DT_EST_FINANZ, Len.Int.WP01_DT_EST_FINANZ);
    }

    public byte[] getWp01DtEstFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP01_DT_EST_FINANZ, Pos.WP01_DT_EST_FINANZ);
        return buffer;
    }

    public void setWp01DtEstFinanzNull(String wp01DtEstFinanzNull) {
        writeString(Pos.WP01_DT_EST_FINANZ_NULL, wp01DtEstFinanzNull, Len.WP01_DT_EST_FINANZ_NULL);
    }

    /**Original name: WP01-DT-EST-FINANZ-NULL<br>*/
    public String getWp01DtEstFinanzNull() {
        return readString(Pos.WP01_DT_EST_FINANZ_NULL, Len.WP01_DT_EST_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP01_DT_EST_FINANZ = 1;
        public static final int WP01_DT_EST_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP01_DT_EST_FINANZ = 5;
        public static final int WP01_DT_EST_FINANZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP01_DT_EST_FINANZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
