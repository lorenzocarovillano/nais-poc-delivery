package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B05-VAL-PC<br>
 * Variable: B05-VAL-PC from program LLBS0250<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B05ValPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B05ValPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B05_VAL_PC;
    }

    public void setB05ValPc(AfDecimal b05ValPc) {
        writeDecimalAsPacked(Pos.B05_VAL_PC, b05ValPc.copy());
    }

    public void setB05ValPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B05_VAL_PC, Pos.B05_VAL_PC);
    }

    /**Original name: B05-VAL-PC<br>*/
    public AfDecimal getB05ValPc() {
        return readPackedAsDecimal(Pos.B05_VAL_PC, Len.Int.B05_VAL_PC, Len.Fract.B05_VAL_PC);
    }

    public byte[] getB05ValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B05_VAL_PC, Pos.B05_VAL_PC);
        return buffer;
    }

    public void setB05ValPcNull(String b05ValPcNull) {
        writeString(Pos.B05_VAL_PC_NULL, b05ValPcNull, Len.B05_VAL_PC_NULL);
    }

    /**Original name: B05-VAL-PC-NULL<br>*/
    public String getB05ValPcNull() {
        return readString(Pos.B05_VAL_PC_NULL, Len.B05_VAL_PC_NULL);
    }

    public String getB05ValPcNullFormatted() {
        return Functions.padBlanks(getB05ValPcNull(), Len.B05_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B05_VAL_PC = 1;
        public static final int B05_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B05_VAL_PC = 8;
        public static final int B05_VAL_PC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B05_VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B05_VAL_PC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
