package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-ID-MOVI-CHIU<br>
 * Variable: WGRZ-ID-MOVI-CHIU from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_ID_MOVI_CHIU;
    }

    public void setWgrzIdMoviChiu(int wgrzIdMoviChiu) {
        writeIntAsPacked(Pos.WGRZ_ID_MOVI_CHIU, wgrzIdMoviChiu, Len.Int.WGRZ_ID_MOVI_CHIU);
    }

    public void setWgrzIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_ID_MOVI_CHIU, Pos.WGRZ_ID_MOVI_CHIU);
    }

    /**Original name: WGRZ-ID-MOVI-CHIU<br>*/
    public int getWgrzIdMoviChiu() {
        return readPackedAsInt(Pos.WGRZ_ID_MOVI_CHIU, Len.Int.WGRZ_ID_MOVI_CHIU);
    }

    public byte[] getWgrzIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_ID_MOVI_CHIU, Pos.WGRZ_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWgrzIdMoviChiuSpaces() {
        fill(Pos.WGRZ_ID_MOVI_CHIU, Len.WGRZ_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWgrzIdMoviChiuNull(String wgrzIdMoviChiuNull) {
        writeString(Pos.WGRZ_ID_MOVI_CHIU_NULL, wgrzIdMoviChiuNull, Len.WGRZ_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WGRZ-ID-MOVI-CHIU-NULL<br>*/
    public String getWgrzIdMoviChiuNull() {
        return readString(Pos.WGRZ_ID_MOVI_CHIU_NULL, Len.WGRZ_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_ID_MOVI_CHIU = 1;
        public static final int WGRZ_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_ID_MOVI_CHIU = 5;
        public static final int WGRZ_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
