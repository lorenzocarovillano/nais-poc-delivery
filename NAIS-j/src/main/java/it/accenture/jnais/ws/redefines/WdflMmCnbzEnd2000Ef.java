package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFL-MM-CNBZ-END2000-EF<br>
 * Variable: WDFL-MM-CNBZ-END2000-EF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflMmCnbzEnd2000Ef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflMmCnbzEnd2000Ef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_MM_CNBZ_END2000_EF;
    }

    public void setWdflMmCnbzEnd2000Ef(int wdflMmCnbzEnd2000Ef) {
        writeIntAsPacked(Pos.WDFL_MM_CNBZ_END2000_EF, wdflMmCnbzEnd2000Ef, Len.Int.WDFL_MM_CNBZ_END2000_EF);
    }

    public void setWdflMmCnbzEnd2000EfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_MM_CNBZ_END2000_EF, Pos.WDFL_MM_CNBZ_END2000_EF);
    }

    /**Original name: WDFL-MM-CNBZ-END2000-EF<br>*/
    public int getWdflMmCnbzEnd2000Ef() {
        return readPackedAsInt(Pos.WDFL_MM_CNBZ_END2000_EF, Len.Int.WDFL_MM_CNBZ_END2000_EF);
    }

    public byte[] getWdflMmCnbzEnd2000EfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_MM_CNBZ_END2000_EF, Pos.WDFL_MM_CNBZ_END2000_EF);
        return buffer;
    }

    public void setWdflMmCnbzEnd2000EfNull(String wdflMmCnbzEnd2000EfNull) {
        writeString(Pos.WDFL_MM_CNBZ_END2000_EF_NULL, wdflMmCnbzEnd2000EfNull, Len.WDFL_MM_CNBZ_END2000_EF_NULL);
    }

    /**Original name: WDFL-MM-CNBZ-END2000-EF-NULL<br>*/
    public String getWdflMmCnbzEnd2000EfNull() {
        return readString(Pos.WDFL_MM_CNBZ_END2000_EF_NULL, Len.WDFL_MM_CNBZ_END2000_EF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_MM_CNBZ_END2000_EF = 1;
        public static final int WDFL_MM_CNBZ_END2000_EF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_MM_CNBZ_END2000_EF = 3;
        public static final int WDFL_MM_CNBZ_END2000_EF_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_MM_CNBZ_END2000_EF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
