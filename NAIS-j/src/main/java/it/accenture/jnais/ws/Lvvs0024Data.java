package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.occurs.WtgaTabTran;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0024<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0024Data {

    //==== PROPERTIES ====
    public static final int DTGA_TAB_TRAN_MAXOCCURS = 20;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0024";
    //Original name: WK-DURA-AA
    private int wkDuraAa = DefaultValues.INT_VAL;
    //Original name: WK-DURA-MM
    private int wkDuraMm = DefaultValues.INT_VAL;
    //Original name: WK-APPO-MESI
    private int wkAppoMesi = DefaultValues.INT_VAL;
    //Original name: WK-APPO-ANNI
    private int wkAppoAnni = DefaultValues.INT_VAL;
    //Original name: DTGA-ELE-TGA-MAX
    private short dtgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTGA-TAB-TRAN
    private WtgaTabTran[] dtgaTabTran = new WtgaTabTran[DTGA_TAB_TRAN_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-TGA
    private short ixTabTga = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0024Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dtgaTabTranIdx = 1; dtgaTabTranIdx <= DTGA_TAB_TRAN_MAXOCCURS; dtgaTabTranIdx++) {
            dtgaTabTran[dtgaTabTranIdx - 1] = new WtgaTabTran();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkDuraAa(int wkDuraAa) {
        this.wkDuraAa = wkDuraAa;
    }

    public int getWkDuraAa() {
        return this.wkDuraAa;
    }

    public void setWkDuraMm(int wkDuraMm) {
        this.wkDuraMm = wkDuraMm;
    }

    public int getWkDuraMm() {
        return this.wkDuraMm;
    }

    public void setWkAppoMesi(int wkAppoMesi) {
        this.wkAppoMesi = wkAppoMesi;
    }

    public int getWkAppoMesi() {
        return this.wkAppoMesi;
    }

    public void setWkAppoAnni(int wkAppoAnni) {
        this.wkAppoAnni = wkAppoAnni;
    }

    public int getWkAppoAnni() {
        return this.wkAppoAnni;
    }

    public void setDtgaAreaTrancheFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TRANCHE];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TRANCHE);
        setDtgaAreaTrancheBytes(buffer, 1);
    }

    public void setDtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleTgaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DTGA_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dtgaTabTran[idx - 1].setTabTranBytes(buffer, position);
                position += WtgaTabTran.Len.TAB_TRAN;
            }
            else {
                dtgaTabTran[idx - 1].initTabTranSpaces();
                position += WtgaTabTran.Len.TAB_TRAN;
            }
        }
    }

    public void setDtgaEleTgaMax(short dtgaEleTgaMax) {
        this.dtgaEleTgaMax = dtgaEleTgaMax;
    }

    public short getDtgaEleTgaMax() {
        return this.dtgaEleTgaMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabTga(short ixTabTga) {
        this.ixTabTga = ixTabTga;
    }

    public short getIxTabTga() {
        return this.ixTabTga;
    }

    public WtgaTabTran getDtgaTabTran(int idx) {
        return dtgaTabTran[idx - 1];
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_APPO_MESI = 9;
        public static final int WK_APPO_ANNI = 9;
        public static final int DTGA_ELE_TGA_MAX = 2;
        public static final int DTGA_AREA_TRANCHE = DTGA_ELE_TGA_MAX + Lvvs0024Data.DTGA_TAB_TRAN_MAXOCCURS * WtgaTabTran.Len.TAB_TRAN;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
