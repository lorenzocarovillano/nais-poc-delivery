package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-PROV-ACQ-2AA<br>
 * Variable: WTIT-TOT-PROV-ACQ-2AA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotProvAcq2aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotProvAcq2aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_PROV_ACQ2AA;
    }

    public void setWtitTotProvAcq2aa(AfDecimal wtitTotProvAcq2aa) {
        writeDecimalAsPacked(Pos.WTIT_TOT_PROV_ACQ2AA, wtitTotProvAcq2aa.copy());
    }

    public void setWtitTotProvAcq2aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_PROV_ACQ2AA, Pos.WTIT_TOT_PROV_ACQ2AA);
    }

    /**Original name: WTIT-TOT-PROV-ACQ-2AA<br>*/
    public AfDecimal getWtitTotProvAcq2aa() {
        return readPackedAsDecimal(Pos.WTIT_TOT_PROV_ACQ2AA, Len.Int.WTIT_TOT_PROV_ACQ2AA, Len.Fract.WTIT_TOT_PROV_ACQ2AA);
    }

    public byte[] getWtitTotProvAcq2aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_PROV_ACQ2AA, Pos.WTIT_TOT_PROV_ACQ2AA);
        return buffer;
    }

    public void initWtitTotProvAcq2aaSpaces() {
        fill(Pos.WTIT_TOT_PROV_ACQ2AA, Len.WTIT_TOT_PROV_ACQ2AA, Types.SPACE_CHAR);
    }

    public void setWtitTotProvAcq2aaNull(String wtitTotProvAcq2aaNull) {
        writeString(Pos.WTIT_TOT_PROV_ACQ2AA_NULL, wtitTotProvAcq2aaNull, Len.WTIT_TOT_PROV_ACQ2AA_NULL);
    }

    /**Original name: WTIT-TOT-PROV-ACQ-2AA-NULL<br>*/
    public String getWtitTotProvAcq2aaNull() {
        return readString(Pos.WTIT_TOT_PROV_ACQ2AA_NULL, Len.WTIT_TOT_PROV_ACQ2AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PROV_ACQ2AA = 1;
        public static final int WTIT_TOT_PROV_ACQ2AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PROV_ACQ2AA = 8;
        public static final int WTIT_TOT_PROV_ACQ2AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PROV_ACQ2AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PROV_ACQ2AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
