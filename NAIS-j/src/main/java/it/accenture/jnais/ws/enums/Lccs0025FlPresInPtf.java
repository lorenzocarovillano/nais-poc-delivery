package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LCCS0025-FL-PRES-IN-PTF<br>
 * Variable: LCCS0025-FL-PRES-IN-PTF from copybook LCCC0025<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccs0025FlPresInPtf {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlPresInPtf(char flPresInPtf) {
        this.value = flPresInPtf;
    }

    public char getFlPresInPtf() {
        return this.value;
    }

    public boolean isLccs0025FlPresPtfSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_PRES_IN_PTF = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
