package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-FINE-GUIDE<br>
 * Variable: FLAG-FINE-GUIDE from program LOAS9000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagFineGuide {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char YES = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagFineGuide(char flagFineGuide) {
        this.value = flagFineGuide;
    }

    public char getFlagFineGuide() {
        return this.value;
    }

    public boolean isYes() {
        return value == YES;
    }

    public void setYes() {
        value = YES;
    }

    public void setNo() {
        value = NO;
    }
}
