package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.FlagComunTrov;
import it.accenture.jnais.ws.enums.FlagCurMov;
import it.accenture.jnais.ws.enums.FlagUltimaLettura;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0101;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS1010<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs1010Data {

    //==== PROPERTIES ====
    //Original name: WK-CALL-PGM
    private String wkCallPgm = DefaultValues.stringVal(Len.WK_CALL_PGM);
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0101 wsMovimento = new WsMovimentoLvvs0101();
    //Original name: MOVI
    private MoviLdbs1530 movi = new MoviLdbs1530();
    //Original name: EST-TRCH-DI-GAR
    private EstTrchDiGarIdbse120 estTrchDiGar = new EstTrchDiGarIdbse120();
    //Original name: WK-DT-INI-EFF-TEMP
    private int wkDtIniEffTemp = DefaultValues.INT_VAL;
    //Original name: WK-DT-CPTZ-TEMP
    private long wkDtCptzTemp = DefaultValues.LONG_VAL;
    //Original name: WK-DATA-EFF-PREC
    private int wkDataEffPrec = DefaultValues.INT_VAL;
    //Original name: WK-DATA-CPTZ-PREC
    private long wkDataCptzPrec = DefaultValues.LONG_VAL;
    //Original name: FLAG-ULTIMA-LETTURA
    private FlagUltimaLettura flagUltimaLettura = new FlagUltimaLettura();
    //Original name: FLAG-CUR-MOV
    private FlagCurMov flagCurMov = new FlagCurMov();
    //Original name: FLAG-COMUN-TROV
    private FlagComunTrov flagComunTrov = new FlagComunTrov();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkDtIniEffTemp(int wkDtIniEffTemp) {
        this.wkDtIniEffTemp = wkDtIniEffTemp;
    }

    public int getWkDtIniEffTemp() {
        return this.wkDtIniEffTemp;
    }

    public void setWkDtCptzTemp(long wkDtCptzTemp) {
        this.wkDtCptzTemp = wkDtCptzTemp;
    }

    public long getWkDtCptzTemp() {
        return this.wkDtCptzTemp;
    }

    public void setWkDataEffPrec(int wkDataEffPrec) {
        this.wkDataEffPrec = wkDataEffPrec;
    }

    public int getWkDataEffPrec() {
        return this.wkDataEffPrec;
    }

    public void setWkDataCptzPrec(long wkDataCptzPrec) {
        this.wkDataCptzPrec = wkDataCptzPrec;
    }

    public long getWkDataCptzPrec() {
        return this.wkDataCptzPrec;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public EstTrchDiGarIdbse120 getEstTrchDiGar() {
        return estTrchDiGar;
    }

    public FlagComunTrov getFlagComunTrov() {
        return flagComunTrov;
    }

    public FlagCurMov getFlagCurMov() {
        return flagCurMov;
    }

    public FlagUltimaLettura getFlagUltimaLettura() {
        return flagUltimaLettura;
    }

    public MoviLdbs1530 getMovi() {
        return movi;
    }

    public WsMovimentoLvvs0101 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
