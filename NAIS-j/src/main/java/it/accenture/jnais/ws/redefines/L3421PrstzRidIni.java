package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PRSTZ-RID-INI<br>
 * Variable: L3421-PRSTZ-RID-INI from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PrstzRidIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PrstzRidIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PRSTZ_RID_INI;
    }

    public void setL3421PrstzRidIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PRSTZ_RID_INI, Pos.L3421_PRSTZ_RID_INI);
    }

    /**Original name: L3421-PRSTZ-RID-INI<br>*/
    public AfDecimal getL3421PrstzRidIni() {
        return readPackedAsDecimal(Pos.L3421_PRSTZ_RID_INI, Len.Int.L3421_PRSTZ_RID_INI, Len.Fract.L3421_PRSTZ_RID_INI);
    }

    public byte[] getL3421PrstzRidIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PRSTZ_RID_INI, Pos.L3421_PRSTZ_RID_INI);
        return buffer;
    }

    /**Original name: L3421-PRSTZ-RID-INI-NULL<br>*/
    public String getL3421PrstzRidIniNull() {
        return readString(Pos.L3421_PRSTZ_RID_INI_NULL, Len.L3421_PRSTZ_RID_INI_NULL);
    }

    public String getL3421PrstzRidIniNullFormatted() {
        return Functions.padBlanks(getL3421PrstzRidIniNull(), Len.L3421_PRSTZ_RID_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PRSTZ_RID_INI = 1;
        public static final int L3421_PRSTZ_RID_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PRSTZ_RID_INI = 8;
        public static final int L3421_PRSTZ_RID_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PRSTZ_RID_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PRSTZ_RID_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
