package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-STOR-I<br>
 * Variable: PCO-DT-ULT-BOLL-STOR-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollStorI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollStorI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_STOR_I;
    }

    public void setPcoDtUltBollStorI(int pcoDtUltBollStorI) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_STOR_I, pcoDtUltBollStorI, Len.Int.PCO_DT_ULT_BOLL_STOR_I);
    }

    public void setPcoDtUltBollStorIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_STOR_I, Pos.PCO_DT_ULT_BOLL_STOR_I);
    }

    /**Original name: PCO-DT-ULT-BOLL-STOR-I<br>*/
    public int getPcoDtUltBollStorI() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_STOR_I, Len.Int.PCO_DT_ULT_BOLL_STOR_I);
    }

    public byte[] getPcoDtUltBollStorIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_STOR_I, Pos.PCO_DT_ULT_BOLL_STOR_I);
        return buffer;
    }

    public void initPcoDtUltBollStorIHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_STOR_I, Len.PCO_DT_ULT_BOLL_STOR_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollStorINull(String pcoDtUltBollStorINull) {
        writeString(Pos.PCO_DT_ULT_BOLL_STOR_I_NULL, pcoDtUltBollStorINull, Len.PCO_DT_ULT_BOLL_STOR_I_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-STOR-I-NULL<br>*/
    public String getPcoDtUltBollStorINull() {
        return readString(Pos.PCO_DT_ULT_BOLL_STOR_I_NULL, Len.PCO_DT_ULT_BOLL_STOR_I_NULL);
    }

    public String getPcoDtUltBollStorINullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollStorINull(), Len.PCO_DT_ULT_BOLL_STOR_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_STOR_I = 1;
        public static final int PCO_DT_ULT_BOLL_STOR_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_STOR_I = 5;
        public static final int PCO_DT_ULT_BOLL_STOR_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_STOR_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
