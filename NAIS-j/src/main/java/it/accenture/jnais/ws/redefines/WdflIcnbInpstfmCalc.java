package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ICNB-INPSTFM-CALC<br>
 * Variable: WDFL-ICNB-INPSTFM-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIcnbInpstfmCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIcnbInpstfmCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ICNB_INPSTFM_CALC;
    }

    public void setWdflIcnbInpstfmCalc(AfDecimal wdflIcnbInpstfmCalc) {
        writeDecimalAsPacked(Pos.WDFL_ICNB_INPSTFM_CALC, wdflIcnbInpstfmCalc.copy());
    }

    public void setWdflIcnbInpstfmCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ICNB_INPSTFM_CALC, Pos.WDFL_ICNB_INPSTFM_CALC);
    }

    /**Original name: WDFL-ICNB-INPSTFM-CALC<br>*/
    public AfDecimal getWdflIcnbInpstfmCalc() {
        return readPackedAsDecimal(Pos.WDFL_ICNB_INPSTFM_CALC, Len.Int.WDFL_ICNB_INPSTFM_CALC, Len.Fract.WDFL_ICNB_INPSTFM_CALC);
    }

    public byte[] getWdflIcnbInpstfmCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ICNB_INPSTFM_CALC, Pos.WDFL_ICNB_INPSTFM_CALC);
        return buffer;
    }

    public void setWdflIcnbInpstfmCalcNull(String wdflIcnbInpstfmCalcNull) {
        writeString(Pos.WDFL_ICNB_INPSTFM_CALC_NULL, wdflIcnbInpstfmCalcNull, Len.WDFL_ICNB_INPSTFM_CALC_NULL);
    }

    /**Original name: WDFL-ICNB-INPSTFM-CALC-NULL<br>*/
    public String getWdflIcnbInpstfmCalcNull() {
        return readString(Pos.WDFL_ICNB_INPSTFM_CALC_NULL, Len.WDFL_ICNB_INPSTFM_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ICNB_INPSTFM_CALC = 1;
        public static final int WDFL_ICNB_INPSTFM_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ICNB_INPSTFM_CALC = 8;
        public static final int WDFL_ICNB_INPSTFM_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ICNB_INPSTFM_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ICNB_INPSTFM_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
