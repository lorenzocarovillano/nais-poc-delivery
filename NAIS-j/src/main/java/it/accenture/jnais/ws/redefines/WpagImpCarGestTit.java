package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPAG-IMP-CAR-GEST-TIT<br>
 * Variable: WPAG-IMP-CAR-GEST-TIT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpCarGestTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpCarGestTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_CAR_GEST_TIT;
    }

    public void setWpagImpCarGestTit(AfDecimal wpagImpCarGestTit) {
        writeDecimalAsPacked(Pos.WPAG_IMP_CAR_GEST_TIT, wpagImpCarGestTit.copy());
    }

    public void setWpagImpCarGestTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_CAR_GEST_TIT, Pos.WPAG_IMP_CAR_GEST_TIT);
    }

    /**Original name: WPAG-IMP-CAR-GEST-TIT<br>*/
    public AfDecimal getWpagImpCarGestTit() {
        return readPackedAsDecimal(Pos.WPAG_IMP_CAR_GEST_TIT, Len.Int.WPAG_IMP_CAR_GEST_TIT, Len.Fract.WPAG_IMP_CAR_GEST_TIT);
    }

    public byte[] getWpagImpCarGestTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_CAR_GEST_TIT, Pos.WPAG_IMP_CAR_GEST_TIT);
        return buffer;
    }

    public void initWpagImpCarGestTitSpaces() {
        fill(Pos.WPAG_IMP_CAR_GEST_TIT, Len.WPAG_IMP_CAR_GEST_TIT, Types.SPACE_CHAR);
    }

    public void setWpagImpCarGestTitNull(String wpagImpCarGestTitNull) {
        writeString(Pos.WPAG_IMP_CAR_GEST_TIT_NULL, wpagImpCarGestTitNull, Len.WPAG_IMP_CAR_GEST_TIT_NULL);
    }

    /**Original name: WPAG-IMP-CAR-GEST-TIT-NULL<br>*/
    public String getWpagImpCarGestTitNull() {
        return readString(Pos.WPAG_IMP_CAR_GEST_TIT_NULL, Len.WPAG_IMP_CAR_GEST_TIT_NULL);
    }

    public String getWpagImpCarGestTitNullFormatted() {
        return Functions.padBlanks(getWpagImpCarGestTitNull(), Len.WPAG_IMP_CAR_GEST_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_GEST_TIT = 1;
        public static final int WPAG_IMP_CAR_GEST_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_GEST_TIT = 8;
        public static final int WPAG_IMP_CAR_GEST_TIT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_GEST_TIT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_GEST_TIT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
