package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CPT-IN-OPZ-RIVTO<br>
 * Variable: WPAG-CPT-IN-OPZ-RIVTO from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagCptInOpzRivto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagCptInOpzRivto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CPT_IN_OPZ_RIVTO;
    }

    public void setWpagCptInOpzRivto(AfDecimal wpagCptInOpzRivto) {
        writeDecimalAsPacked(Pos.WPAG_CPT_IN_OPZ_RIVTO, wpagCptInOpzRivto.copy());
    }

    public void setWpagCptInOpzRivtoFormatted(String wpagCptInOpzRivto) {
        setWpagCptInOpzRivto(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CPT_IN_OPZ_RIVTO + Len.Fract.WPAG_CPT_IN_OPZ_RIVTO, Len.Fract.WPAG_CPT_IN_OPZ_RIVTO, wpagCptInOpzRivto));
    }

    public void setWpagCptInOpzRivtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CPT_IN_OPZ_RIVTO, Pos.WPAG_CPT_IN_OPZ_RIVTO);
    }

    /**Original name: WPAG-CPT-IN-OPZ-RIVTO<br>*/
    public AfDecimal getWpagCptInOpzRivto() {
        return readPackedAsDecimal(Pos.WPAG_CPT_IN_OPZ_RIVTO, Len.Int.WPAG_CPT_IN_OPZ_RIVTO, Len.Fract.WPAG_CPT_IN_OPZ_RIVTO);
    }

    public byte[] getWpagCptInOpzRivtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CPT_IN_OPZ_RIVTO, Pos.WPAG_CPT_IN_OPZ_RIVTO);
        return buffer;
    }

    public void initWpagCptInOpzRivtoSpaces() {
        fill(Pos.WPAG_CPT_IN_OPZ_RIVTO, Len.WPAG_CPT_IN_OPZ_RIVTO, Types.SPACE_CHAR);
    }

    public void setWpagCptInOpzRivtoNull(String wpagCptInOpzRivtoNull) {
        writeString(Pos.WPAG_CPT_IN_OPZ_RIVTO_NULL, wpagCptInOpzRivtoNull, Len.WPAG_CPT_IN_OPZ_RIVTO_NULL);
    }

    /**Original name: WPAG-CPT-IN-OPZ-RIVTO-NULL<br>*/
    public String getWpagCptInOpzRivtoNull() {
        return readString(Pos.WPAG_CPT_IN_OPZ_RIVTO_NULL, Len.WPAG_CPT_IN_OPZ_RIVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CPT_IN_OPZ_RIVTO = 1;
        public static final int WPAG_CPT_IN_OPZ_RIVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CPT_IN_OPZ_RIVTO = 8;
        public static final int WPAG_CPT_IN_OPZ_RIVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CPT_IN_OPZ_RIVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CPT_IN_OPZ_RIVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
