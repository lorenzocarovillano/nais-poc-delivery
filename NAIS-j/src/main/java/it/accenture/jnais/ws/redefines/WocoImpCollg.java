package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-IMP-COLLG<br>
 * Variable: WOCO-IMP-COLLG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoImpCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoImpCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_IMP_COLLG;
    }

    public void setWocoImpCollg(AfDecimal wocoImpCollg) {
        writeDecimalAsPacked(Pos.WOCO_IMP_COLLG, wocoImpCollg.copy());
    }

    public void setWocoImpCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_IMP_COLLG, Pos.WOCO_IMP_COLLG);
    }

    /**Original name: WOCO-IMP-COLLG<br>*/
    public AfDecimal getWocoImpCollg() {
        return readPackedAsDecimal(Pos.WOCO_IMP_COLLG, Len.Int.WOCO_IMP_COLLG, Len.Fract.WOCO_IMP_COLLG);
    }

    public byte[] getWocoImpCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_IMP_COLLG, Pos.WOCO_IMP_COLLG);
        return buffer;
    }

    public void initWocoImpCollgSpaces() {
        fill(Pos.WOCO_IMP_COLLG, Len.WOCO_IMP_COLLG, Types.SPACE_CHAR);
    }

    public void setWocoImpCollgNull(String wocoImpCollgNull) {
        writeString(Pos.WOCO_IMP_COLLG_NULL, wocoImpCollgNull, Len.WOCO_IMP_COLLG_NULL);
    }

    /**Original name: WOCO-IMP-COLLG-NULL<br>*/
    public String getWocoImpCollgNull() {
        return readString(Pos.WOCO_IMP_COLLG_NULL, Len.WOCO_IMP_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_IMP_COLLG = 1;
        public static final int WOCO_IMP_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_IMP_COLLG = 8;
        public static final int WOCO_IMP_COLLG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_IMP_COLLG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_IMP_COLLG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
