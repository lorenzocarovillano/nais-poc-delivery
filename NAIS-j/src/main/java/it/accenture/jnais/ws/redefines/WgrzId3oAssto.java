package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-ID-3O-ASSTO<br>
 * Variable: WGRZ-ID-3O-ASSTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzId3oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzId3oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_ID3O_ASSTO;
    }

    public void setWgrzId3oAssto(int wgrzId3oAssto) {
        writeIntAsPacked(Pos.WGRZ_ID3O_ASSTO, wgrzId3oAssto, Len.Int.WGRZ_ID3O_ASSTO);
    }

    public void setWgrzId3oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_ID3O_ASSTO, Pos.WGRZ_ID3O_ASSTO);
    }

    /**Original name: WGRZ-ID-3O-ASSTO<br>*/
    public int getWgrzId3oAssto() {
        return readPackedAsInt(Pos.WGRZ_ID3O_ASSTO, Len.Int.WGRZ_ID3O_ASSTO);
    }

    public byte[] getWgrzId3oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_ID3O_ASSTO, Pos.WGRZ_ID3O_ASSTO);
        return buffer;
    }

    public void initWgrzId3oAsstoSpaces() {
        fill(Pos.WGRZ_ID3O_ASSTO, Len.WGRZ_ID3O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWgrzId3oAsstoNull(String wgrzId3oAsstoNull) {
        writeString(Pos.WGRZ_ID3O_ASSTO_NULL, wgrzId3oAsstoNull, Len.WGRZ_ID3O_ASSTO_NULL);
    }

    /**Original name: WGRZ-ID-3O-ASSTO-NULL<br>*/
    public String getWgrzId3oAsstoNull() {
        return readString(Pos.WGRZ_ID3O_ASSTO_NULL, Len.WGRZ_ID3O_ASSTO_NULL);
    }

    public String getWgrzId3oAsstoNullFormatted() {
        return Functions.padBlanks(getWgrzId3oAsstoNull(), Len.WGRZ_ID3O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_ID3O_ASSTO = 1;
        public static final int WGRZ_ID3O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_ID3O_ASSTO = 5;
        public static final int WGRZ_ID3O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_ID3O_ASSTO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
