package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Ldbv1661;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;
import it.accenture.jnais.ws.occurs.WgrzTabGar;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0035<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0035Data {

    //==== PROPERTIES ====
    public static final int DGRZ_TAB_GAR_MAXOCCURS = 20;
    public static final int DTGA_TAB_TRAN_MAXOCCURS = 1250;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0035";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-TP-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkTpOgg = DefaultValues.stringVal(Len.WK_TP_OGG);
    /**Original name: FORMATO<br>
	 * <pre>-- AREA ROUTINE PER IL CALCOLO</pre>*/
    private char formato = DefaultValues.CHAR_VAL;
    //Original name: DATA-INFERIORE
    private DataInferioreLccs0490 dataInferiore = new DataInferioreLccs0490();
    //Original name: DATA-SUPERIORE
    private DataSuperioreLccs0490 dataSuperiore = new DataSuperioreLccs0490();
    //Original name: GG-DIFF
    private String ggDiff = DefaultValues.stringVal(Len.GG_DIFF);
    //Original name: CODICE-RITORNO
    private char codiceRitorno = DefaultValues.CHAR_VAL;
    //Original name: WK-DATA-INFERIORE
    private String wkDataInferiore = "00000000";
    //Original name: LDBV1661
    private Ldbv1661 ldbv1661 = new Ldbv1661();
    //Original name: DTGA-ELE-TGA-MAX
    private short dtgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTGA-TAB-TRAN
    private LazyArrayCopy<W1tgaTabTran> dtgaTabTran = new LazyArrayCopy<W1tgaTabTran>(new W1tgaTabTran(), 1, DTGA_TAB_TRAN_MAXOCCURS);
    //Original name: DGRZ-ELE-GAR-MAX
    private short dgrzEleGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DGRZ-TAB-GAR
    private WgrzTabGar[] dgrzTabGar = new WgrzTabGar[DGRZ_TAB_GAR_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-TGA
    private short ixTabTga = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0035Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dgrzTabGarIdx = 1; dgrzTabGarIdx <= DGRZ_TAB_GAR_MAXOCCURS; dgrzTabGarIdx++) {
            dgrzTabGar[dgrzTabGarIdx - 1] = new WgrzTabGar();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkTpOgg(String wkTpOgg) {
        this.wkTpOgg = Functions.subString(wkTpOgg, Len.WK_TP_OGG);
    }

    public String getWkTpOgg() {
        return this.wkTpOgg;
    }

    public void setFormato(char formato) {
        this.formato = formato;
    }

    public void setFormatoFormatted(String formato) {
        setFormato(Functions.charAt(formato, Types.CHAR_SIZE));
    }

    public void setFormatoFromBuffer(byte[] buffer) {
        formato = MarshalByte.readChar(buffer, 1);
    }

    public char getFormato() {
        return this.formato;
    }

    public void setGgDiffFromBuffer(byte[] buffer) {
        ggDiff = MarshalByte.readFixedString(buffer, 1, Len.GG_DIFF);
    }

    public int getGgDiff() {
        return NumericDisplay.asInt(this.ggDiff);
    }

    public String getGgDiffFormatted() {
        return this.ggDiff;
    }

    public void setCodiceRitorno(char codiceRitorno) {
        this.codiceRitorno = codiceRitorno;
    }

    public void setCodiceRitornoFromBuffer(byte[] buffer) {
        codiceRitorno = MarshalByte.readChar(buffer, 1);
    }

    public char getCodiceRitorno() {
        return this.codiceRitorno;
    }

    public void setWkDataInferiore(int wkDataInferiore) {
        this.wkDataInferiore = NumericDisplay.asString(wkDataInferiore, Len.WK_DATA_INFERIORE);
    }

    public int getWkDataInferiore() {
        return NumericDisplay.asInt(this.wkDataInferiore);
    }

    public String getWkDataInferioreFormatted() {
        return this.wkDataInferiore;
    }

    public void setDtgaAreaTgaFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TGA];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TGA);
        setDtgaAreaTgaBytes(buffer, 1);
    }

    public void setDtgaAreaTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleTgaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DTGA_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dtgaTabTran.get(idx - 1).setW1tgaTabTranBytes(buffer, position);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN;
            }
            else {
                W1tgaTabTran temp_dtgaTabTran = new W1tgaTabTran();
                temp_dtgaTabTran.initW1tgaTabTranSpaces();
                getDtgaTabTranObj().fill(temp_dtgaTabTran);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN * (DTGA_TAB_TRAN_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public void setDtgaEleTgaMax(short dtgaEleTgaMax) {
        this.dtgaEleTgaMax = dtgaEleTgaMax;
    }

    public short getDtgaEleTgaMax() {
        return this.dtgaEleTgaMax;
    }

    public void setDgrzAreaGarFormatted(String data) {
        byte[] buffer = new byte[Len.DGRZ_AREA_GAR];
        MarshalByte.writeString(buffer, 1, data, Len.DGRZ_AREA_GAR);
        setDgrzAreaGarBytes(buffer, 1);
    }

    public void setDgrzAreaGarBytes(byte[] buffer, int offset) {
        int position = offset;
        dgrzEleGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DGRZ_TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dgrzTabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                dgrzTabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public void setDgrzEleGarMax(short dgrzEleGarMax) {
        this.dgrzEleGarMax = dgrzEleGarMax;
    }

    public short getDgrzEleGarMax() {
        return this.dgrzEleGarMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabTga(short ixTabTga) {
        this.ixTabTga = ixTabTga;
    }

    public short getIxTabTga() {
        return this.ixTabTga;
    }

    public DataInferioreLccs0490 getDataInferiore() {
        return dataInferiore;
    }

    public DataSuperioreLccs0490 getDataSuperiore() {
        return dataSuperiore;
    }

    public WgrzTabGar getDgrzTabGar(int idx) {
        return dgrzTabGar[idx - 1];
    }

    public W1tgaTabTran getDtgaTabTran(int idx) {
        return dtgaTabTran.get(idx - 1);
    }

    public LazyArrayCopy<W1tgaTabTran> getDtgaTabTranObj() {
        return dtgaTabTran;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Ldbv1661 getLdbv1661() {
        return ldbv1661;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TP_OGG = 2;
        public static final int GG_DIFF = 5;
        public static final int WK_DATA_INFERIORE = 8;
        public static final int DTGA_ELE_TGA_MAX = 2;
        public static final int DTGA_AREA_TGA = DTGA_ELE_TGA_MAX + Lvvs0035Data.DTGA_TAB_TRAN_MAXOCCURS * W1tgaTabTran.Len.W1TGA_TAB_TRAN;
        public static final int DGRZ_ELE_GAR_MAX = 2;
        public static final int DGRZ_AREA_GAR = DGRZ_ELE_GAR_MAX + Lvvs0035Data.DGRZ_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
