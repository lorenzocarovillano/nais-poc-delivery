package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDAD-DT-DECOR-DFLT<br>
 * Variable: WDAD-DT-DECOR-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadDtDecorDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadDtDecorDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_DT_DECOR_DFLT;
    }

    public void setWdadDtDecorDflt(int wdadDtDecorDflt) {
        writeIntAsPacked(Pos.WDAD_DT_DECOR_DFLT, wdadDtDecorDflt, Len.Int.WDAD_DT_DECOR_DFLT);
    }

    public void setWdadDtDecorDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_DT_DECOR_DFLT, Pos.WDAD_DT_DECOR_DFLT);
    }

    /**Original name: WDAD-DT-DECOR-DFLT<br>*/
    public int getWdadDtDecorDflt() {
        return readPackedAsInt(Pos.WDAD_DT_DECOR_DFLT, Len.Int.WDAD_DT_DECOR_DFLT);
    }

    public byte[] getWdadDtDecorDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_DT_DECOR_DFLT, Pos.WDAD_DT_DECOR_DFLT);
        return buffer;
    }

    public void setWdadDtDecorDfltNull(String wdadDtDecorDfltNull) {
        writeString(Pos.WDAD_DT_DECOR_DFLT_NULL, wdadDtDecorDfltNull, Len.WDAD_DT_DECOR_DFLT_NULL);
    }

    /**Original name: WDAD-DT-DECOR-DFLT-NULL<br>*/
    public String getWdadDtDecorDfltNull() {
        return readString(Pos.WDAD_DT_DECOR_DFLT_NULL, Len.WDAD_DT_DECOR_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_DT_DECOR_DFLT = 1;
        public static final int WDAD_DT_DECOR_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_DT_DECOR_DFLT = 5;
        public static final int WDAD_DT_DECOR_DFLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_DT_DECOR_DFLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
