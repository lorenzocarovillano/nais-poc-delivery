package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRSTZ-INI-STAB<br>
 * Variable: TGA-PRSTZ-INI-STAB from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPrstzIniStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPrstzIniStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRSTZ_INI_STAB;
    }

    public void setTgaPrstzIniStab(AfDecimal tgaPrstzIniStab) {
        writeDecimalAsPacked(Pos.TGA_PRSTZ_INI_STAB, tgaPrstzIniStab.copy());
    }

    public void setTgaPrstzIniStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRSTZ_INI_STAB, Pos.TGA_PRSTZ_INI_STAB);
    }

    /**Original name: TGA-PRSTZ-INI-STAB<br>*/
    public AfDecimal getTgaPrstzIniStab() {
        return readPackedAsDecimal(Pos.TGA_PRSTZ_INI_STAB, Len.Int.TGA_PRSTZ_INI_STAB, Len.Fract.TGA_PRSTZ_INI_STAB);
    }

    public byte[] getTgaPrstzIniStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRSTZ_INI_STAB, Pos.TGA_PRSTZ_INI_STAB);
        return buffer;
    }

    public void setTgaPrstzIniStabNull(String tgaPrstzIniStabNull) {
        writeString(Pos.TGA_PRSTZ_INI_STAB_NULL, tgaPrstzIniStabNull, Len.TGA_PRSTZ_INI_STAB_NULL);
    }

    /**Original name: TGA-PRSTZ-INI-STAB-NULL<br>*/
    public String getTgaPrstzIniStabNull() {
        return readString(Pos.TGA_PRSTZ_INI_STAB_NULL, Len.TGA_PRSTZ_INI_STAB_NULL);
    }

    public String getTgaPrstzIniStabNullFormatted() {
        return Functions.padBlanks(getTgaPrstzIniStabNull(), Len.TGA_PRSTZ_INI_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_INI_STAB = 1;
        public static final int TGA_PRSTZ_INI_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_INI_STAB = 8;
        public static final int TGA_PRSTZ_INI_STAB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_INI_STAB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_INI_STAB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
