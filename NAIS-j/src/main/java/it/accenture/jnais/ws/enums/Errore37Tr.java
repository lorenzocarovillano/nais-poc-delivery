package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: ERRORE-37-TR<br>
 * Variable: ERRORE-37-TR from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Errore37Tr {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setErrore37Tr(char errore37Tr) {
        this.value = errore37Tr;
    }

    public char getErrore37Tr() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
