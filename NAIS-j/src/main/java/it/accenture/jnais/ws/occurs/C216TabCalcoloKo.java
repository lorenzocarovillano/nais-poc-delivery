package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: C216-TAB-CALCOLO-KO<br>
 * Variables: C216-TAB-CALCOLO-KO from copybook IVVC0215<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class C216TabCalcoloKo {

    //==== PROPERTIES ====
    //Original name: C216-VAR-CALCOLO-KO
    private String calcoloKo = DefaultValues.stringVal(Len.CALCOLO_KO);
    //Original name: C216-VAR-CALC-SP
    private char calcSp = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setTabCalcoloKoBytes(byte[] buffer, int offset) {
        int position = offset;
        calcoloKo = MarshalByte.readString(buffer, position, Len.CALCOLO_KO);
        position += Len.CALCOLO_KO;
        calcSp = MarshalByte.readChar(buffer, position);
    }

    public byte[] getTabCalcoloKoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, calcoloKo, Len.CALCOLO_KO);
        position += Len.CALCOLO_KO;
        MarshalByte.writeChar(buffer, position, calcSp);
        return buffer;
    }

    public void initTabCalcoloKoSpaces() {
        calcoloKo = "";
        calcSp = Types.SPACE_CHAR;
    }

    public void setCalcoloKo(String calcoloKo) {
        this.calcoloKo = Functions.subString(calcoloKo, Len.CALCOLO_KO);
    }

    public String getCalcoloKo() {
        return this.calcoloKo;
    }

    public void setCalcSp(char calcSp) {
        this.calcSp = calcSp;
    }

    public char getCalcSp() {
        return this.calcSp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CALCOLO_KO = 19;
        public static final int CALC_SP = 1;
        public static final int TAB_CALCOLO_KO = CALCOLO_KO + CALC_SP;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
