package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-SCAD-INTMD<br>
 * Variable: WB03-DT-SCAD-INTMD from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtScadIntmd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtScadIntmd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_SCAD_INTMD;
    }

    public void setWb03DtScadIntmdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_SCAD_INTMD, Pos.WB03_DT_SCAD_INTMD);
    }

    /**Original name: WB03-DT-SCAD-INTMD<br>*/
    public int getWb03DtScadIntmd() {
        return readPackedAsInt(Pos.WB03_DT_SCAD_INTMD, Len.Int.WB03_DT_SCAD_INTMD);
    }

    public byte[] getWb03DtScadIntmdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_SCAD_INTMD, Pos.WB03_DT_SCAD_INTMD);
        return buffer;
    }

    public void setWb03DtScadIntmdNull(String wb03DtScadIntmdNull) {
        writeString(Pos.WB03_DT_SCAD_INTMD_NULL, wb03DtScadIntmdNull, Len.WB03_DT_SCAD_INTMD_NULL);
    }

    /**Original name: WB03-DT-SCAD-INTMD-NULL<br>*/
    public String getWb03DtScadIntmdNull() {
        return readString(Pos.WB03_DT_SCAD_INTMD_NULL, Len.WB03_DT_SCAD_INTMD_NULL);
    }

    public String getWb03DtScadIntmdNullFormatted() {
        return Functions.padBlanks(getWb03DtScadIntmdNull(), Len.WB03_DT_SCAD_INTMD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_SCAD_INTMD = 1;
        public static final int WB03_DT_SCAD_INTMD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_SCAD_INTMD = 5;
        public static final int WB03_DT_SCAD_INTMD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_SCAD_INTMD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
