package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ALQ-CNBT-INPSTFM-C<br>
 * Variable: DFL-ALQ-CNBT-INPSTFM-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAlqCnbtInpstfmC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAlqCnbtInpstfmC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ALQ_CNBT_INPSTFM_C;
    }

    public void setDflAlqCnbtInpstfmC(AfDecimal dflAlqCnbtInpstfmC) {
        writeDecimalAsPacked(Pos.DFL_ALQ_CNBT_INPSTFM_C, dflAlqCnbtInpstfmC.copy());
    }

    public void setDflAlqCnbtInpstfmCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ALQ_CNBT_INPSTFM_C, Pos.DFL_ALQ_CNBT_INPSTFM_C);
    }

    /**Original name: DFL-ALQ-CNBT-INPSTFM-C<br>*/
    public AfDecimal getDflAlqCnbtInpstfmC() {
        return readPackedAsDecimal(Pos.DFL_ALQ_CNBT_INPSTFM_C, Len.Int.DFL_ALQ_CNBT_INPSTFM_C, Len.Fract.DFL_ALQ_CNBT_INPSTFM_C);
    }

    public byte[] getDflAlqCnbtInpstfmCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ALQ_CNBT_INPSTFM_C, Pos.DFL_ALQ_CNBT_INPSTFM_C);
        return buffer;
    }

    public void setDflAlqCnbtInpstfmCNull(String dflAlqCnbtInpstfmCNull) {
        writeString(Pos.DFL_ALQ_CNBT_INPSTFM_C_NULL, dflAlqCnbtInpstfmCNull, Len.DFL_ALQ_CNBT_INPSTFM_C_NULL);
    }

    /**Original name: DFL-ALQ-CNBT-INPSTFM-C-NULL<br>*/
    public String getDflAlqCnbtInpstfmCNull() {
        return readString(Pos.DFL_ALQ_CNBT_INPSTFM_C_NULL, Len.DFL_ALQ_CNBT_INPSTFM_C_NULL);
    }

    public String getDflAlqCnbtInpstfmCNullFormatted() {
        return Functions.padBlanks(getDflAlqCnbtInpstfmCNull(), Len.DFL_ALQ_CNBT_INPSTFM_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_CNBT_INPSTFM_C = 1;
        public static final int DFL_ALQ_CNBT_INPSTFM_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_CNBT_INPSTFM_C = 4;
        public static final int DFL_ALQ_CNBT_INPSTFM_C_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_CNBT_INPSTFM_C = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_CNBT_INPSTFM_C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
