package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LOAS0820<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLoas0820 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-PMO
    private short pmo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-GRZ
    private short grz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-TGA
    private short tga = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setPmo(short pmo) {
        this.pmo = pmo;
    }

    public short getPmo() {
        return this.pmo;
    }

    public void setGrz(short grz) {
        this.grz = grz;
    }

    public short getGrz() {
        return this.grz;
    }

    public void setTga(short tga) {
        this.tga = tga;
    }

    public short getTga() {
        return this.tga;
    }
}
