package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-MOVIMENTO<br>
 * Variable: WS-MOVIMENTO from copybook LCCVXMVZ<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsMovimentoLvvs0039 {

    //==== PROPERTIES ====
    private String value = "00000";
    public static final String CREAZ_COLLET = "01001";
    public static final String REVIS_COLLET = "01002";
    public static final String STORN_COLLET = "01003";
    public static final String ATTIV_COLLET = "01004";
    public static final String DATI_DEFAULT = "01005";
    public static final String EMISS_POLIZZA = "01006";
    public static final String PRENOT_RICALC = "01007";
    public static final String CREAZ_INDIVI = "01011";
    public static final String REVIS_INDIVI = "01012";
    public static final String STORN_INDIVI = "01013";
    public static final String REVOC_INDIVI = "01014";
    public static final String ATTIV_INDIVI = "01015";
    public static final String CREAZ_INDIVI_REINV = "01016";
    public static final String CREAZ_INDIVI_REINV_LIQ = "01017";
    public static final String STAMPA_PROP_POLI = "01018";
    public static final String TRFOR_INDIVI = "01021";
    public static final String REVIS_TRFOR_INDIVI = "01022";
    public static final String STORN_TRFOR_INDIVI = "01023";
    public static final String REVOC_TRFOR_INDIVI = "01024";
    public static final String ATTIV_TRFOR_INDIVI = "01025";
    public static final String CREAZ_ADESIO = "01031";
    public static final String REVIS_ADESIO = "01032";
    public static final String STORN_ADESIO = "01033";
    public static final String REVOC_ADESIO = "01034";
    public static final String ATTIV_ADESIO = "01035";
    public static final String PREN_CREAZ_ADES = "01036";
    public static final String TRASF_ADESIO = "01041";
    public static final String TRASF_REVADE = "01042";
    public static final String STORN_TRADES = "01043";
    public static final String REVOC_TRADES = "01044";
    public static final String ATTIV_TRADES = "01045";
    public static final String INCLU_GARANZ = "01051";
    public static final String REVIS_GARANZ = "01052";
    public static final String STORN_GARANZ = "01053";
    public static final String ATTIV_GARANZ = "01054";
    public static final String INCLU_GARANZ_COMPLEM = "01055";
    public static final String REVIS_GARANZ_COMPLEM = "01056";
    public static final String INCLU_TRANCH = "01061";
    public static final String REVIS_TRANCH = "01062";
    public static final String STORN_TRANCH = "01063";
    public static final String VERSAM_AGGIUNTIVO = "01065";
    public static final String VERSAM_AGGIUNTIVO_REINV = "01066";
    public static final String RINNOVO_TRANCHE = "01075";
    public static final String CREAZ_PREVEN = "01081";
    public static final String MODIF_PREVEN = "01082";
    public static final String ANNUL_PREVEN = "01083";

    //==== METHODS ====
    public void setWsMovimentoFormatted(String wsMovimento) {
        this.value = Trunc.toUnsignedNumeric(wsMovimento, Len.WS_MOVIMENTO);
    }

    public int getWsMovimento() {
        return NumericDisplay.asInt(this.value);
    }

    public String getWsMovimentoFormatted() {
        return this.value;
    }

    public boolean isCreazIndivi() {
        return getWsMovimentoFormatted().equals(CREAZ_INDIVI);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_MOVIMENTO = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
