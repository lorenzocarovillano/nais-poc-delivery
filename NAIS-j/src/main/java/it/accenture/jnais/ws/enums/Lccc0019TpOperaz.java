package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0019-TP-OPERAZ<br>
 * Variable: LCCC0019-TP-OPERAZ from copybook LCCC0019<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0019TpOperaz {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TP_OPERAZ);
    public static final String LETTURA_VPL = "00";
    public static final String AGGIORNA_VPL = "01";

    //==== METHODS ====
    public void setTpOperaz(String tpOperaz) {
        this.value = Functions.subString(tpOperaz, Len.TP_OPERAZ);
    }

    public String getTpOperaz() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OPERAZ = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
