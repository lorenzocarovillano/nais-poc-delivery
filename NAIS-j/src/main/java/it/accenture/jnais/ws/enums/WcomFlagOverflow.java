package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-FLAG-OVERFLOW<br>
 * Variable: WCOM-FLAG-OVERFLOW from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomFlagOverflow {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WCOM_FLAG_OVERFLOW);
    public static final String YES = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setWcomFlagOverflow(String wcomFlagOverflow) {
        this.value = Functions.subString(wcomFlagOverflow, Len.WCOM_FLAG_OVERFLOW);
    }

    public String getWcomFlagOverflow() {
        return this.value;
    }

    public boolean isYes() {
        return value.equals(YES);
    }

    public void setYes() {
        value = YES;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WCOM_FLAG_OVERFLOW = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
