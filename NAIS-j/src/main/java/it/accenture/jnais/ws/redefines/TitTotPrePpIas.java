package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-PRE-PP-IAS<br>
 * Variable: TIT-TOT-PRE-PP-IAS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotPrePpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotPrePpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_PRE_PP_IAS;
    }

    public void setTitTotPrePpIas(AfDecimal titTotPrePpIas) {
        writeDecimalAsPacked(Pos.TIT_TOT_PRE_PP_IAS, titTotPrePpIas.copy());
    }

    public void setTitTotPrePpIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_PRE_PP_IAS, Pos.TIT_TOT_PRE_PP_IAS);
    }

    /**Original name: TIT-TOT-PRE-PP-IAS<br>*/
    public AfDecimal getTitTotPrePpIas() {
        return readPackedAsDecimal(Pos.TIT_TOT_PRE_PP_IAS, Len.Int.TIT_TOT_PRE_PP_IAS, Len.Fract.TIT_TOT_PRE_PP_IAS);
    }

    public byte[] getTitTotPrePpIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_PRE_PP_IAS, Pos.TIT_TOT_PRE_PP_IAS);
        return buffer;
    }

    public void setTitTotPrePpIasNull(String titTotPrePpIasNull) {
        writeString(Pos.TIT_TOT_PRE_PP_IAS_NULL, titTotPrePpIasNull, Len.TIT_TOT_PRE_PP_IAS_NULL);
    }

    /**Original name: TIT-TOT-PRE-PP-IAS-NULL<br>*/
    public String getTitTotPrePpIasNull() {
        return readString(Pos.TIT_TOT_PRE_PP_IAS_NULL, Len.TIT_TOT_PRE_PP_IAS_NULL);
    }

    public String getTitTotPrePpIasNullFormatted() {
        return Functions.padBlanks(getTitTotPrePpIasNull(), Len.TIT_TOT_PRE_PP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PRE_PP_IAS = 1;
        public static final int TIT_TOT_PRE_PP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PRE_PP_IAS = 8;
        public static final int TIT_TOT_PRE_PP_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PRE_PP_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PRE_PP_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
