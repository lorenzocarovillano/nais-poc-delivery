package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-MOVI-NON-INVES<br>
 * Variable: WRST-RIS-MOVI-NON-INVES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisMoviNonInves extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisMoviNonInves() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_MOVI_NON_INVES;
    }

    public void setWrstRisMoviNonInves(AfDecimal wrstRisMoviNonInves) {
        writeDecimalAsPacked(Pos.WRST_RIS_MOVI_NON_INVES, wrstRisMoviNonInves.copy());
    }

    public void setWrstRisMoviNonInvesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_MOVI_NON_INVES, Pos.WRST_RIS_MOVI_NON_INVES);
    }

    /**Original name: WRST-RIS-MOVI-NON-INVES<br>*/
    public AfDecimal getWrstRisMoviNonInves() {
        return readPackedAsDecimal(Pos.WRST_RIS_MOVI_NON_INVES, Len.Int.WRST_RIS_MOVI_NON_INVES, Len.Fract.WRST_RIS_MOVI_NON_INVES);
    }

    public byte[] getWrstRisMoviNonInvesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_MOVI_NON_INVES, Pos.WRST_RIS_MOVI_NON_INVES);
        return buffer;
    }

    public void initWrstRisMoviNonInvesSpaces() {
        fill(Pos.WRST_RIS_MOVI_NON_INVES, Len.WRST_RIS_MOVI_NON_INVES, Types.SPACE_CHAR);
    }

    public void setWrstRisMoviNonInvesNull(String wrstRisMoviNonInvesNull) {
        writeString(Pos.WRST_RIS_MOVI_NON_INVES_NULL, wrstRisMoviNonInvesNull, Len.WRST_RIS_MOVI_NON_INVES_NULL);
    }

    /**Original name: WRST-RIS-MOVI-NON-INVES-NULL<br>*/
    public String getWrstRisMoviNonInvesNull() {
        return readString(Pos.WRST_RIS_MOVI_NON_INVES_NULL, Len.WRST_RIS_MOVI_NON_INVES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_MOVI_NON_INVES = 1;
        public static final int WRST_RIS_MOVI_NON_INVES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_MOVI_NON_INVES = 8;
        public static final int WRST_RIS_MOVI_NON_INVES_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_MOVI_NON_INVES = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_MOVI_NON_INVES = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
