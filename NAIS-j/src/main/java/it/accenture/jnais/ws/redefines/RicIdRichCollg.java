package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIC-ID-RICH-COLLG<br>
 * Variable: RIC-ID-RICH-COLLG from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RicIdRichCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RicIdRichCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIC_ID_RICH_COLLG;
    }

    public void setRicIdRichCollg(int ricIdRichCollg) {
        writeIntAsPacked(Pos.RIC_ID_RICH_COLLG, ricIdRichCollg, Len.Int.RIC_ID_RICH_COLLG);
    }

    public void setRicIdRichCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIC_ID_RICH_COLLG, Pos.RIC_ID_RICH_COLLG);
    }

    /**Original name: RIC-ID-RICH-COLLG<br>*/
    public int getRicIdRichCollg() {
        return readPackedAsInt(Pos.RIC_ID_RICH_COLLG, Len.Int.RIC_ID_RICH_COLLG);
    }

    public byte[] getRicIdRichCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIC_ID_RICH_COLLG, Pos.RIC_ID_RICH_COLLG);
        return buffer;
    }

    public void setRicIdRichCollgNull(String ricIdRichCollgNull) {
        writeString(Pos.RIC_ID_RICH_COLLG_NULL, ricIdRichCollgNull, Len.RIC_ID_RICH_COLLG_NULL);
    }

    /**Original name: RIC-ID-RICH-COLLG-NULL<br>*/
    public String getRicIdRichCollgNull() {
        return readString(Pos.RIC_ID_RICH_COLLG_NULL, Len.RIC_ID_RICH_COLLG_NULL);
    }

    public String getRicIdRichCollgNullFormatted() {
        return Functions.padBlanks(getRicIdRichCollgNull(), Len.RIC_ID_RICH_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIC_ID_RICH_COLLG = 1;
        public static final int RIC_ID_RICH_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIC_ID_RICH_COLLG = 5;
        public static final int RIC_ID_RICH_COLLG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIC_ID_RICH_COLLG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
