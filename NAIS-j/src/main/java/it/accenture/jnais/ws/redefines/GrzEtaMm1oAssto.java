package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-ETA-MM-1O-ASSTO<br>
 * Variable: GRZ-ETA-MM-1O-ASSTO from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzEtaMm1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzEtaMm1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_ETA_MM1O_ASSTO;
    }

    public void setGrzEtaMm1oAssto(short grzEtaMm1oAssto) {
        writeShortAsPacked(Pos.GRZ_ETA_MM1O_ASSTO, grzEtaMm1oAssto, Len.Int.GRZ_ETA_MM1O_ASSTO);
    }

    public void setGrzEtaMm1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_ETA_MM1O_ASSTO, Pos.GRZ_ETA_MM1O_ASSTO);
    }

    /**Original name: GRZ-ETA-MM-1O-ASSTO<br>*/
    public short getGrzEtaMm1oAssto() {
        return readPackedAsShort(Pos.GRZ_ETA_MM1O_ASSTO, Len.Int.GRZ_ETA_MM1O_ASSTO);
    }

    public byte[] getGrzEtaMm1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_ETA_MM1O_ASSTO, Pos.GRZ_ETA_MM1O_ASSTO);
        return buffer;
    }

    public void setGrzEtaMm1oAsstoNull(String grzEtaMm1oAsstoNull) {
        writeString(Pos.GRZ_ETA_MM1O_ASSTO_NULL, grzEtaMm1oAsstoNull, Len.GRZ_ETA_MM1O_ASSTO_NULL);
    }

    /**Original name: GRZ-ETA-MM-1O-ASSTO-NULL<br>*/
    public String getGrzEtaMm1oAsstoNull() {
        return readString(Pos.GRZ_ETA_MM1O_ASSTO_NULL, Len.GRZ_ETA_MM1O_ASSTO_NULL);
    }

    public String getGrzEtaMm1oAsstoNullFormatted() {
        return Functions.padBlanks(getGrzEtaMm1oAsstoNull(), Len.GRZ_ETA_MM1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_ETA_MM1O_ASSTO = 1;
        public static final int GRZ_ETA_MM1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_ETA_MM1O_ASSTO = 2;
        public static final int GRZ_ETA_MM1O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_ETA_MM1O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
