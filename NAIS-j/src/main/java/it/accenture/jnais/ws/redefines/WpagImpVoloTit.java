package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-VOLO-TIT<br>
 * Variable: WPAG-IMP-VOLO-TIT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpVoloTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpVoloTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_VOLO_TIT;
    }

    public void setWpagImpVoloTit(AfDecimal wpagImpVoloTit) {
        writeDecimalAsPacked(Pos.WPAG_IMP_VOLO_TIT, wpagImpVoloTit.copy());
    }

    public void setWpagImpVoloTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_VOLO_TIT, Pos.WPAG_IMP_VOLO_TIT);
    }

    /**Original name: WPAG-IMP-VOLO-TIT<br>*/
    public AfDecimal getWpagImpVoloTit() {
        return readPackedAsDecimal(Pos.WPAG_IMP_VOLO_TIT, Len.Int.WPAG_IMP_VOLO_TIT, Len.Fract.WPAG_IMP_VOLO_TIT);
    }

    public byte[] getWpagImpVoloTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_VOLO_TIT, Pos.WPAG_IMP_VOLO_TIT);
        return buffer;
    }

    public void initWpagImpVoloTitSpaces() {
        fill(Pos.WPAG_IMP_VOLO_TIT, Len.WPAG_IMP_VOLO_TIT, Types.SPACE_CHAR);
    }

    public void setWpagImpVoloTitNull(String wpagImpVoloTitNull) {
        writeString(Pos.WPAG_IMP_VOLO_TIT_NULL, wpagImpVoloTitNull, Len.WPAG_IMP_VOLO_TIT_NULL);
    }

    /**Original name: WPAG-IMP-VOLO-TIT-NULL<br>*/
    public String getWpagImpVoloTitNull() {
        return readString(Pos.WPAG_IMP_VOLO_TIT_NULL, Len.WPAG_IMP_VOLO_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_VOLO_TIT = 1;
        public static final int WPAG_IMP_VOLO_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_VOLO_TIT = 8;
        public static final int WPAG_IMP_VOLO_TIT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_VOLO_TIT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_VOLO_TIT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
