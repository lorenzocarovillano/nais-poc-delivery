package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-IMP-MOVTO<br>
 * Variable: VAS-IMP-MOVTO from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasImpMovto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasImpMovto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_IMP_MOVTO;
    }

    public void setVasImpMovto(AfDecimal vasImpMovto) {
        writeDecimalAsPacked(Pos.VAS_IMP_MOVTO, vasImpMovto.copy());
    }

    public void setVasImpMovtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_IMP_MOVTO, Pos.VAS_IMP_MOVTO);
    }

    /**Original name: VAS-IMP-MOVTO<br>*/
    public AfDecimal getVasImpMovto() {
        return readPackedAsDecimal(Pos.VAS_IMP_MOVTO, Len.Int.VAS_IMP_MOVTO, Len.Fract.VAS_IMP_MOVTO);
    }

    public byte[] getVasImpMovtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_IMP_MOVTO, Pos.VAS_IMP_MOVTO);
        return buffer;
    }

    public void setVasImpMovtoNull(String vasImpMovtoNull) {
        writeString(Pos.VAS_IMP_MOVTO_NULL, vasImpMovtoNull, Len.VAS_IMP_MOVTO_NULL);
    }

    /**Original name: VAS-IMP-MOVTO-NULL<br>*/
    public String getVasImpMovtoNull() {
        return readString(Pos.VAS_IMP_MOVTO_NULL, Len.VAS_IMP_MOVTO_NULL);
    }

    public String getVasImpMovtoNullFormatted() {
        return Functions.padBlanks(getVasImpMovtoNull(), Len.VAS_IMP_MOVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_IMP_MOVTO = 1;
        public static final int VAS_IMP_MOVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_IMP_MOVTO = 8;
        public static final int VAS_IMP_MOVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAS_IMP_MOVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_IMP_MOVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
