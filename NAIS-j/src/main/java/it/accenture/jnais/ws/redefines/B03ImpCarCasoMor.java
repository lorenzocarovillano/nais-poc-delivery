package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-IMP-CAR-CASO-MOR<br>
 * Variable: B03-IMP-CAR-CASO-MOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03ImpCarCasoMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03ImpCarCasoMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_IMP_CAR_CASO_MOR;
    }

    public void setB03ImpCarCasoMor(AfDecimal b03ImpCarCasoMor) {
        writeDecimalAsPacked(Pos.B03_IMP_CAR_CASO_MOR, b03ImpCarCasoMor.copy());
    }

    public void setB03ImpCarCasoMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_IMP_CAR_CASO_MOR, Pos.B03_IMP_CAR_CASO_MOR);
    }

    /**Original name: B03-IMP-CAR-CASO-MOR<br>*/
    public AfDecimal getB03ImpCarCasoMor() {
        return readPackedAsDecimal(Pos.B03_IMP_CAR_CASO_MOR, Len.Int.B03_IMP_CAR_CASO_MOR, Len.Fract.B03_IMP_CAR_CASO_MOR);
    }

    public byte[] getB03ImpCarCasoMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_IMP_CAR_CASO_MOR, Pos.B03_IMP_CAR_CASO_MOR);
        return buffer;
    }

    public void setB03ImpCarCasoMorNull(String b03ImpCarCasoMorNull) {
        writeString(Pos.B03_IMP_CAR_CASO_MOR_NULL, b03ImpCarCasoMorNull, Len.B03_IMP_CAR_CASO_MOR_NULL);
    }

    /**Original name: B03-IMP-CAR-CASO-MOR-NULL<br>*/
    public String getB03ImpCarCasoMorNull() {
        return readString(Pos.B03_IMP_CAR_CASO_MOR_NULL, Len.B03_IMP_CAR_CASO_MOR_NULL);
    }

    public String getB03ImpCarCasoMorNullFormatted() {
        return Functions.padBlanks(getB03ImpCarCasoMorNull(), Len.B03_IMP_CAR_CASO_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_IMP_CAR_CASO_MOR = 1;
        public static final int B03_IMP_CAR_CASO_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_IMP_CAR_CASO_MOR = 8;
        public static final int B03_IMP_CAR_CASO_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_IMP_CAR_CASO_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_IMP_CAR_CASO_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
