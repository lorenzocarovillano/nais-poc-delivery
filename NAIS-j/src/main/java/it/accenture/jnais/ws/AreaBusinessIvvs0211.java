package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0212;
import it.accenture.jnais.copy.Ivvc0217;
import it.accenture.jnais.copy.Ivvc0223;
import it.accenture.jnais.copy.Ivvc0224;
import it.accenture.jnais.copy.Lccvade1;
import it.accenture.jnais.copy.Lccvdad1;
import it.accenture.jnais.copy.Lccvdco1;
import it.accenture.jnais.copy.Lccvdfa1;
import it.accenture.jnais.copy.Lccvdfl1;
import it.accenture.jnais.copy.Lccviso1;
import it.accenture.jnais.copy.Lccvmov1;
import it.accenture.jnais.copy.Lccvp011;
import it.accenture.jnais.copy.Lccvp611;
import it.accenture.jnais.copy.Lccvp671;
import it.accenture.jnais.copy.Lccvpol1;
import it.accenture.jnais.copy.Lccvric1;
import it.accenture.jnais.copy.Lccvtcl1;
import it.accenture.jnais.copy.WadeDati;
import it.accenture.jnais.copy.WdadDati;
import it.accenture.jnais.copy.WdcoDati;
import it.accenture.jnais.copy.WdfaDati;
import it.accenture.jnais.copy.WdflDati;
import it.accenture.jnais.copy.WisoDati;
import it.accenture.jnais.copy.WmovDati;
import it.accenture.jnais.copy.Wp01Dati;
import it.accenture.jnais.copy.Wp61Dati;
import it.accenture.jnais.copy.Wp67Dati;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.copy.WricDati;
import it.accenture.jnais.copy.WtclDati;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.occurs.Ivvc0222TabTran;
import it.accenture.jnais.ws.occurs.S089TabLiq;
import it.accenture.jnais.ws.occurs.WbelTabBeneLiq;
import it.accenture.jnais.ws.occurs.WbepTabBeneficiari;
import it.accenture.jnais.ws.occurs.WcltTabClauTxt;
import it.accenture.jnais.ws.occurs.WdtcTabDtc;
import it.accenture.jnais.ws.occurs.We15TabE15;
import it.accenture.jnais.ws.occurs.WgrlTabGarLiq;
import it.accenture.jnais.ws.occurs.WgrzTabGar;
import it.accenture.jnais.ws.occurs.Wl23TabVincPeg;
import it.accenture.jnais.ws.occurs.Wl30TabReinvstPoli;
import it.accenture.jnais.ws.occurs.WmfzTabMoviFinrio;
import it.accenture.jnais.ws.occurs.WopzOpzioni;
import it.accenture.jnais.ws.occurs.Wp56TabQuestAdegVer;
import it.accenture.jnais.ws.occurs.Wp58TabImpstBol;
import it.accenture.jnais.ws.occurs.Wp86TabMotLiq;
import it.accenture.jnais.ws.occurs.Wp88TabServVal;
import it.accenture.jnais.ws.occurs.Wp89TabDservVal;
import it.accenture.jnais.ws.occurs.WpliTabPercLiq;
import it.accenture.jnais.ws.occurs.WpreTabPrestiti;
import it.accenture.jnais.ws.occurs.WpvtTabProvTr;
import it.accenture.jnais.ws.occurs.WqueTabQuest;
import it.accenture.jnais.ws.occurs.WranTabRappAnag;
import it.accenture.jnais.ws.occurs.WrreTabRappRete;
import it.accenture.jnais.ws.occurs.WrstTabRst;
import it.accenture.jnais.ws.occurs.WsdiTabStraInv;
import it.accenture.jnais.ws.occurs.WspgTabSpg;
import it.accenture.jnais.ws.occurs.WtgaTabTran;
import it.accenture.jnais.ws.occurs.WtitTabTitCont;

/**Original name: AREA-BUSINESS<br>
 * Variable: AREA-BUSINESS from program IVVS0211<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaBusinessIvvs0211 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int IVVC0222_TAB_TRAN_MAXOCCURS = 1250;
    public static final int WBEP_TAB_BENEFICIARI_MAXOCCURS = 20;
    public static final int WBEL_TAB_BENE_LIQ_MAXOCCURS = 15;
    public static final int WDTC_TAB_DTC_MAXOCCURS = 100;
    public static final int WGRZ_TAB_GAR_MAXOCCURS = 20;
    public static final int WGRL_TAB_GAR_LIQ_MAXOCCURS = 20;
    public static final int WLQU_TAB_LIQ_MAXOCCURS = 30;
    public static final int WMFZ_TAB_MOVI_FINRIO_MAXOCCURS = 10;
    public static final int WPLI_TAB_PERC_LIQ_MAXOCCURS = 30;
    public static final int WPRE_TAB_PRESTITI_MAXOCCURS = 10;
    public static final int WPVT_TAB_PROV_TR_MAXOCCURS = 10;
    public static final int WQUE_TAB_QUEST_MAXOCCURS = 12;
    public static final int WRAN_TAB_RAPP_ANAG_MAXOCCURS = 100;
    public static final int WE15_TAB_E15_MAXOCCURS = 100;
    public static final int WRRE_TAB_RAPP_RETE_MAXOCCURS = 10;
    public static final int WSPG_TAB_SPG_MAXOCCURS = 20;
    public static final int WSDI_TAB_STRA_INV_MAXOCCURS = 20;
    public static final int WTIT_TAB_TIT_CONT_MAXOCCURS = 200;
    public static final int WRST_TAB_RST_MAXOCCURS = 10;
    public static final int WL30_TAB_REINVST_POLI_MAXOCCURS = 40;
    public static final int WL23_TAB_VINC_PEG_MAXOCCURS = 10;
    public static final int WGOP_TAB_GAR_MAXOCCURS = 20;
    public static final int WTOP_TAB_TRAN_MAXOCCURS = 20;
    public static final int WCLT_TAB_CLAU_TXT_MAXOCCURS = 54;
    public static final int WP58_TAB_IMPST_BOL_MAXOCCURS = 75;
    public static final int WP86_TAB_MOT_LIQ_MAXOCCURS = 1;
    public static final int WP88_TAB_SERV_VAL_MAXOCCURS = 10;
    public static final int WP89_TAB_DSERV_VAL_MAXOCCURS = 350;
    public static final int WP56_TAB_QUEST_ADEG_VER_MAXOCCURS = 100;
    //Original name: WADE-ELE-ADES-MAX
    private short wadeEleAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVADE1
    private Lccvade1 lccvade1 = new Lccvade1();
    //Original name: WBEP-ELE-BENEF-MAX
    private short wbepEleBenefMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WBEP-TAB-BENEFICIARI
    private WbepTabBeneficiari[] wbepTabBeneficiari = new WbepTabBeneficiari[WBEP_TAB_BENEFICIARI_MAXOCCURS];
    //Original name: WBEL-ELE-BENEF-LIQ-MAX
    private short wbelEleBenefLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WBEL-TAB-BENE-LIQ
    private WbelTabBeneLiq[] wbelTabBeneLiq = new WbelTabBeneLiq[WBEL_TAB_BENE_LIQ_MAXOCCURS];
    //Original name: WDCO-ELE-COLL-MAX
    private short wdcoEleCollMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDCO1
    private Lccvdco1 lccvdco1 = new Lccvdco1();
    //Original name: WDFA-ELE-FISC-ADES-MAX
    private short wdfaEleFiscAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDFA1
    private Lccvdfa1 lccvdfa1 = new Lccvdfa1();
    //Original name: WDEQ-AREA-DETT-QUEST
    private WdeqAreaDettQuest wdeqAreaDettQuest = new WdeqAreaDettQuest();
    //Original name: WDTC-ELE-DETT-TIT-MAX
    private short wdtcEleDettTitMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WDTC-TAB-DTC
    private WdtcTabDtc[] wdtcTabDtc = new WdtcTabDtc[WDTC_TAB_DTC_MAXOCCURS];
    //Original name: WGRZ-ELE-GARANZIA-MAX
    private short wgrzEleGaranziaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WGRZ-TAB-GAR
    private WgrzTabGar[] wgrzTabGar = new WgrzTabGar[WGRZ_TAB_GAR_MAXOCCURS];
    //Original name: WGRL-ELE-GAR-LIQ-MAX
    private short wgrlEleGarLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WGRL-TAB-GAR-LIQ
    private WgrlTabGarLiq[] wgrlTabGarLiq = new WgrlTabGarLiq[WGRL_TAB_GAR_LIQ_MAXOCCURS];
    //Original name: WISO-ELE-IMP-SOST-MAX
    private short wisoEleImpSostMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVISO1
    private Lccviso1 lccviso1 = new Lccviso1();
    //Original name: WLQU-ELE-LIQ-MAX
    private short wlquEleLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WLQU-TAB-LIQ
    private S089TabLiq[] wlquTabLiq = new S089TabLiq[WLQU_TAB_LIQ_MAXOCCURS];
    //Original name: WMOV-ELE-MOVI-MAX
    private short wmovEleMoviMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVMOV1
    private Lccvmov1 lccvmov1 = new Lccvmov1();
    //Original name: WMFZ-ELE-MOVI-FINRIO-MAX
    private short wmfzEleMoviFinrioMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WMFZ-TAB-MOVI-FINRIO
    private WmfzTabMoviFinrio[] wmfzTabMoviFinrio = new WmfzTabMoviFinrio[WMFZ_TAB_MOVI_FINRIO_MAXOCCURS];
    //Original name: WPOG-AREA-PARAM-OGG
    private WpogAreaParamOgg wpogAreaParamOgg = new WpogAreaParamOgg();
    //Original name: WPMO-AREA-PARAM-MOV
    private WpmoAreaParamMov wpmoAreaParamMov = new WpmoAreaParamMov();
    //Original name: WPLI-ELE-PERC-LIQ-MAX
    private short wpliElePercLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPLI-TAB-PERC-LIQ
    private WpliTabPercLiq[] wpliTabPercLiq = new WpliTabPercLiq[WPLI_TAB_PERC_LIQ_MAXOCCURS];
    //Original name: WPOL-ELE-POLI-MAX
    private short wpolElePoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1 lccvpol1 = new Lccvpol1();
    //Original name: WPRE-ELE-PRESTITI-MAX
    private short wpreElePrestitiMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPRE-TAB-PRESTITI
    private WpreTabPrestiti[] wpreTabPrestiti = new WpreTabPrestiti[WPRE_TAB_PRESTITI_MAXOCCURS];
    //Original name: WPVT-ELE-PROV-MAX
    private short wpvtEleProvMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPVT-TAB-PROV-TR
    private WpvtTabProvTr[] wpvtTabProvTr = new WpvtTabProvTr[WPVT_TAB_PROV_TR_MAXOCCURS];
    //Original name: WQUE-ELE-QUEST-MAX
    private short wqueEleQuestMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WQUE-TAB-QUEST
    private WqueTabQuest[] wqueTabQuest = new WqueTabQuest[WQUE_TAB_QUEST_MAXOCCURS];
    //Original name: WRIC-ELE-RICH-MAX
    private short wricEleRichMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVRIC1
    private Lccvric1 lccvric1 = new Lccvric1();
    //Original name: WRDF-AREA-RICH-DISINV-FND
    private WrdfAreaRichDisinvFnd wrdfAreaRichDisinvFnd = new WrdfAreaRichDisinvFnd();
    //Original name: WRIF-AREA-RICH-INV-FND
    private WrifAreaRichInvFnd wrifAreaRichInvFnd = new WrifAreaRichInvFnd();
    //Original name: WRAN-ELE-RAPP-ANAG-MAX
    private short wranEleRappAnagMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRAN-TAB-RAPP-ANAG
    private WranTabRappAnag[] wranTabRappAnag = new WranTabRappAnag[WRAN_TAB_RAPP_ANAG_MAXOCCURS];
    //Original name: WE15-ELE-EST-RAPP-ANAG-MAX
    private short we15EleEstRappAnagMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WE15-TAB-E15
    private We15TabE15[] we15TabE15 = new We15TabE15[WE15_TAB_E15_MAXOCCURS];
    //Original name: WRRE-ELE-RAPP-RETE-MAX
    private short wrreEleRappReteMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRRE-TAB-RAPP-RETE
    private WrreTabRappRete[] wrreTabRappRete = new WrreTabRappRete[WRRE_TAB_RAPP_RETE_MAXOCCURS];
    //Original name: WSPG-ELE-SOPRAP-GAR-MAX
    private short wspgEleSoprapGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WSPG-TAB-SPG
    private WspgTabSpg[] wspgTabSpg = new WspgTabSpg[WSPG_TAB_SPG_MAXOCCURS];
    //Original name: WSDI-ELE-STRA-INV-MAX
    private short wsdiEleStraInvMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WSDI-TAB-STRA-INV
    private WsdiTabStraInv[] wsdiTabStraInv = new WsdiTabStraInv[WSDI_TAB_STRA_INV_MAXOCCURS];
    //Original name: WTIT-ELE-TIT-CONT-MAX
    private short wtitEleTitContMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTIT-TAB-TIT-CONT
    private WtitTabTitCont[] wtitTabTitCont = new WtitTabTitCont[WTIT_TAB_TIT_CONT_MAXOCCURS];
    //Original name: WTCL-ELE-TIT-LIQ-MAX
    private short wtclEleTitLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVTCL1
    private Lccvtcl1 lccvtcl1 = new Lccvtcl1();
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTrancheIvvs0211 wtgaAreaTranche = new WtgaAreaTrancheIvvs0211();
    //Original name: WTLI-AREA-TRCH-LIQ
    private WtliAreaTrchLiq wtliAreaTrchLiq = new WtliAreaTrchLiq();
    //Original name: WDAD-ELE-DFLT-ADES-MAX
    private short wdadEleDfltAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDAD1
    private Lccvdad1 lccvdad1 = new Lccvdad1();
    //Original name: WOCO-AREA-OGG-COLL
    private WocoAreaOggColl wocoAreaOggColl = new WocoAreaOggColl();
    //Original name: WDFL-ELE-DFL-MAX
    private short wdflEleDflMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDFL1
    private Lccvdfl1 lccvdfl1 = new Lccvdfl1();
    //Original name: WRST-ELE-RST-MAX
    private short wrstEleRstMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRST-TAB-RST
    private WrstTabRst[] wrstTabRst = new WrstTabRst[WRST_TAB_RST_MAXOCCURS];
    //Original name: WL19-AREA-QUOTE
    private Wl19AreaQuote wl19AreaQuote = new Wl19AreaQuote();
    //Original name: WL30-ELE-REINVST-POLI-MAX
    private short wl30EleReinvstPoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WL30-TAB-REINVST-POLI
    private Wl30TabReinvstPoli[] wl30TabReinvstPoli = new Wl30TabReinvstPoli[WL30_TAB_REINVST_POLI_MAXOCCURS];
    //Original name: WL23-ELE-VINC-PEG-MAX
    private short wl23EleVincPegMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WL23-TAB-VINC-PEG
    private Wl23TabVincPeg[] wl23TabVincPeg = new Wl23TabVincPeg[WL23_TAB_VINC_PEG_MAXOCCURS];
    //Original name: IVVC0217
    private Ivvc0217 ivvc0217 = new Ivvc0217();
    //Original name: WGOP-ELE-GARANZIA-OPZ-MAX
    private short wgopEleGaranziaOpzMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WGOP-TAB-GAR
    private WgrzTabGar[] wgopTabGar = new WgrzTabGar[WGOP_TAB_GAR_MAXOCCURS];
    //Original name: WTOP-ELE-TRAN-OPZ-MAX
    private short wtopEleTranOpzMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTOP-TAB-TRAN
    private WtgaTabTran[] wtopTabTran = new WtgaTabTran[WTOP_TAB_TRAN_MAXOCCURS];
    //Original name: IVVC0212
    private Ivvc0212 ivvc0212 = new Ivvc0212();
    //Original name: WCLT-ELE-CLAU-TXT-MAX
    private short wcltEleClauTxtMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WCLT-TAB-CLAU-TXT
    private WcltTabClauTxt[] wcltTabClauTxt = new WcltTabClauTxt[WCLT_TAB_CLAU_TXT_MAXOCCURS];
    //Original name: WP58-ELE-MAX-IMPST-BOL
    private short wp58EleMaxImpstBol = DefaultValues.BIN_SHORT_VAL;
    //Original name: WP58-TAB-IMPST-BOL
    private Wp58TabImpstBol[] wp58TabImpstBol = new Wp58TabImpstBol[WP58_TAB_IMPST_BOL_MAXOCCURS];
    //Original name: WP61-ELE-MAX-D-CRIST
    private short wp61EleMaxDCrist = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVP611
    private Lccvp611 lccvp611 = new Lccvp611();
    //Original name: WP67-ELE-MAX-EST-POLI-CPI-PR
    private short wp67EleMaxEstPoliCpiPr = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVP671
    private Lccvp671 lccvp671 = new Lccvp671();
    //Original name: WP01-ELE-MAX-RICH-EST
    private short wp01EleMaxRichEst = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVP011
    private Lccvp011 lccvp011 = new Lccvp011();
    //Original name: IVVC0222-ELE-TRCH-MAX
    private short ivvc0222EleTrchMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: IVVC0222-TAB-TRAN
    private LazyArrayCopy<Ivvc0222TabTran> ivvc0222TabTran = new LazyArrayCopy<Ivvc0222TabTran>(new Ivvc0222TabTran(), 1, IVVC0222_TAB_TRAN_MAXOCCURS);
    //Original name: WP86-ELE-MOT-LIQ-MAX
    private short wp86EleMotLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WP86-TAB-MOT-LIQ
    private Wp86TabMotLiq[] wp86TabMotLiq = new Wp86TabMotLiq[WP86_TAB_MOT_LIQ_MAXOCCURS];
    //Original name: IVVC0223
    private Ivvc0223 ivvc0223 = new Ivvc0223();
    /**Original name: WK-MOVI-ORIG<br>
	 * <pre>--  VARIABILE PER APPOGGIO MOVIMENTO DI ORIGINE (X LIQUIDAZIONE)</pre>*/
    private String wkMoviOrig = DefaultValues.stringVal(Len.WK_MOVI_ORIG);
    //Original name: WP88-ELE-SER-VAL-MAX
    private short wp88EleSerValMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WP88-TAB-SERV-VAL
    private Wp88TabServVal[] wp88TabServVal = new Wp88TabServVal[WP88_TAB_SERV_VAL_MAXOCCURS];
    //Original name: WP89-ELE-DSERV-VAL-MAX
    private short wp89EleDservValMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WP89-TAB-DSERV-VAL
    private Wp89TabDservVal[] wp89TabDservVal = new Wp89TabDservVal[WP89_TAB_DSERV_VAL_MAXOCCURS];
    //Original name: WP56-QUEST-ADEG-VER-MAX
    private short wp56QuestAdegVerMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WP56-TAB-QUEST-ADEG-VER
    private Wp56TabQuestAdegVer[] wp56TabQuestAdegVer = new Wp56TabQuestAdegVer[WP56_TAB_QUEST_ADEG_VER_MAXOCCURS];
    //Original name: IVVC0224
    private Ivvc0224 ivvc0224 = new Ivvc0224();

    //==== CONSTRUCTORS ====
    public AreaBusinessIvvs0211() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_BUSINESS;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaBusinessBytes(buf);
    }

    public void init() {
        for (int wbepTabBeneficiariIdx = 1; wbepTabBeneficiariIdx <= WBEP_TAB_BENEFICIARI_MAXOCCURS; wbepTabBeneficiariIdx++) {
            wbepTabBeneficiari[wbepTabBeneficiariIdx - 1] = new WbepTabBeneficiari();
        }
        for (int wbelTabBeneLiqIdx = 1; wbelTabBeneLiqIdx <= WBEL_TAB_BENE_LIQ_MAXOCCURS; wbelTabBeneLiqIdx++) {
            wbelTabBeneLiq[wbelTabBeneLiqIdx - 1] = new WbelTabBeneLiq();
        }
        for (int wdtcTabDtcIdx = 1; wdtcTabDtcIdx <= WDTC_TAB_DTC_MAXOCCURS; wdtcTabDtcIdx++) {
            wdtcTabDtc[wdtcTabDtcIdx - 1] = new WdtcTabDtc();
        }
        for (int wgrzTabGarIdx = 1; wgrzTabGarIdx <= WGRZ_TAB_GAR_MAXOCCURS; wgrzTabGarIdx++) {
            wgrzTabGar[wgrzTabGarIdx - 1] = new WgrzTabGar();
        }
        for (int wgrlTabGarLiqIdx = 1; wgrlTabGarLiqIdx <= WGRL_TAB_GAR_LIQ_MAXOCCURS; wgrlTabGarLiqIdx++) {
            wgrlTabGarLiq[wgrlTabGarLiqIdx - 1] = new WgrlTabGarLiq();
        }
        for (int wlquTabLiqIdx = 1; wlquTabLiqIdx <= WLQU_TAB_LIQ_MAXOCCURS; wlquTabLiqIdx++) {
            wlquTabLiq[wlquTabLiqIdx - 1] = new S089TabLiq();
        }
        for (int wmfzTabMoviFinrioIdx = 1; wmfzTabMoviFinrioIdx <= WMFZ_TAB_MOVI_FINRIO_MAXOCCURS; wmfzTabMoviFinrioIdx++) {
            wmfzTabMoviFinrio[wmfzTabMoviFinrioIdx - 1] = new WmfzTabMoviFinrio();
        }
        for (int wpliTabPercLiqIdx = 1; wpliTabPercLiqIdx <= WPLI_TAB_PERC_LIQ_MAXOCCURS; wpliTabPercLiqIdx++) {
            wpliTabPercLiq[wpliTabPercLiqIdx - 1] = new WpliTabPercLiq();
        }
        for (int wpreTabPrestitiIdx = 1; wpreTabPrestitiIdx <= WPRE_TAB_PRESTITI_MAXOCCURS; wpreTabPrestitiIdx++) {
            wpreTabPrestiti[wpreTabPrestitiIdx - 1] = new WpreTabPrestiti();
        }
        for (int wpvtTabProvTrIdx = 1; wpvtTabProvTrIdx <= WPVT_TAB_PROV_TR_MAXOCCURS; wpvtTabProvTrIdx++) {
            wpvtTabProvTr[wpvtTabProvTrIdx - 1] = new WpvtTabProvTr();
        }
        for (int wqueTabQuestIdx = 1; wqueTabQuestIdx <= WQUE_TAB_QUEST_MAXOCCURS; wqueTabQuestIdx++) {
            wqueTabQuest[wqueTabQuestIdx - 1] = new WqueTabQuest();
        }
        for (int wranTabRappAnagIdx = 1; wranTabRappAnagIdx <= WRAN_TAB_RAPP_ANAG_MAXOCCURS; wranTabRappAnagIdx++) {
            wranTabRappAnag[wranTabRappAnagIdx - 1] = new WranTabRappAnag();
        }
        for (int we15TabE15Idx = 1; we15TabE15Idx <= WE15_TAB_E15_MAXOCCURS; we15TabE15Idx++) {
            we15TabE15[we15TabE15Idx - 1] = new We15TabE15();
        }
        for (int wrreTabRappReteIdx = 1; wrreTabRappReteIdx <= WRRE_TAB_RAPP_RETE_MAXOCCURS; wrreTabRappReteIdx++) {
            wrreTabRappRete[wrreTabRappReteIdx - 1] = new WrreTabRappRete();
        }
        for (int wspgTabSpgIdx = 1; wspgTabSpgIdx <= WSPG_TAB_SPG_MAXOCCURS; wspgTabSpgIdx++) {
            wspgTabSpg[wspgTabSpgIdx - 1] = new WspgTabSpg();
        }
        for (int wsdiTabStraInvIdx = 1; wsdiTabStraInvIdx <= WSDI_TAB_STRA_INV_MAXOCCURS; wsdiTabStraInvIdx++) {
            wsdiTabStraInv[wsdiTabStraInvIdx - 1] = new WsdiTabStraInv();
        }
        for (int wtitTabTitContIdx = 1; wtitTabTitContIdx <= WTIT_TAB_TIT_CONT_MAXOCCURS; wtitTabTitContIdx++) {
            wtitTabTitCont[wtitTabTitContIdx - 1] = new WtitTabTitCont();
        }
        for (int wrstTabRstIdx = 1; wrstTabRstIdx <= WRST_TAB_RST_MAXOCCURS; wrstTabRstIdx++) {
            wrstTabRst[wrstTabRstIdx - 1] = new WrstTabRst();
        }
        for (int wl30TabReinvstPoliIdx = 1; wl30TabReinvstPoliIdx <= WL30_TAB_REINVST_POLI_MAXOCCURS; wl30TabReinvstPoliIdx++) {
            wl30TabReinvstPoli[wl30TabReinvstPoliIdx - 1] = new Wl30TabReinvstPoli();
        }
        for (int wl23TabVincPegIdx = 1; wl23TabVincPegIdx <= WL23_TAB_VINC_PEG_MAXOCCURS; wl23TabVincPegIdx++) {
            wl23TabVincPeg[wl23TabVincPegIdx - 1] = new Wl23TabVincPeg();
        }
        for (int wgopTabGarIdx = 1; wgopTabGarIdx <= WGOP_TAB_GAR_MAXOCCURS; wgopTabGarIdx++) {
            wgopTabGar[wgopTabGarIdx - 1] = new WgrzTabGar();
        }
        for (int wtopTabTranIdx = 1; wtopTabTranIdx <= WTOP_TAB_TRAN_MAXOCCURS; wtopTabTranIdx++) {
            wtopTabTran[wtopTabTranIdx - 1] = new WtgaTabTran();
        }
        for (int wcltTabClauTxtIdx = 1; wcltTabClauTxtIdx <= WCLT_TAB_CLAU_TXT_MAXOCCURS; wcltTabClauTxtIdx++) {
            wcltTabClauTxt[wcltTabClauTxtIdx - 1] = new WcltTabClauTxt();
        }
        for (int wp58TabImpstBolIdx = 1; wp58TabImpstBolIdx <= WP58_TAB_IMPST_BOL_MAXOCCURS; wp58TabImpstBolIdx++) {
            wp58TabImpstBol[wp58TabImpstBolIdx - 1] = new Wp58TabImpstBol();
        }
        for (int wp86TabMotLiqIdx = 1; wp86TabMotLiqIdx <= WP86_TAB_MOT_LIQ_MAXOCCURS; wp86TabMotLiqIdx++) {
            wp86TabMotLiq[wp86TabMotLiqIdx - 1] = new Wp86TabMotLiq();
        }
        for (int wp88TabServValIdx = 1; wp88TabServValIdx <= WP88_TAB_SERV_VAL_MAXOCCURS; wp88TabServValIdx++) {
            wp88TabServVal[wp88TabServValIdx - 1] = new Wp88TabServVal();
        }
        for (int wp89TabDservValIdx = 1; wp89TabDservValIdx <= WP89_TAB_DSERV_VAL_MAXOCCURS; wp89TabDservValIdx++) {
            wp89TabDservVal[wp89TabDservValIdx - 1] = new Wp89TabDservVal();
        }
        for (int wp56TabQuestAdegVerIdx = 1; wp56TabQuestAdegVerIdx <= WP56_TAB_QUEST_ADEG_VER_MAXOCCURS; wp56TabQuestAdegVerIdx++) {
            wp56TabQuestAdegVer[wp56TabQuestAdegVerIdx - 1] = new Wp56TabQuestAdegVer();
        }
    }

    public String getAreaBusinessFormatted() {
        return MarshalByteExt.bufferToStr(getAreaBusinessBytes());
    }

    public void setAreaBusinessBytes(byte[] buffer) {
        setAreaBusinessBytes(buffer, 1);
    }

    public byte[] getAreaBusinessBytes() {
        byte[] buffer = new byte[Len.AREA_BUSINESS];
        return getAreaBusinessBytes(buffer, 1);
    }

    public void setAreaBusinessBytes(byte[] buffer, int offset) {
        int position = offset;
        setWadeAreaAdesioneBytes(buffer, position);
        position += Len.WADE_AREA_ADESIONE;
        setWbepAreaBenefBytes(buffer, position);
        position += Len.WBEP_AREA_BENEF;
        setWbelAreaBenefLiqBytes(buffer, position);
        position += Len.WBEL_AREA_BENEF_LIQ;
        setWdcoAreaDtCollettivaBytes(buffer, position);
        position += Len.WDCO_AREA_DT_COLLETTIVA;
        setWdfaAreaDtFiscAdesBytes(buffer, position);
        position += Len.WDFA_AREA_DT_FISC_ADES;
        wdeqAreaDettQuest.setWdeqAreaDettQuestBytes(buffer, position);
        position += WdeqAreaDettQuest.Len.WDEQ_AREA_DETT_QUEST;
        setWdtcAreaDettTitContBytes(buffer, position);
        position += Len.WDTC_AREA_DETT_TIT_CONT;
        setWgrzAreaGaranziaBytes(buffer, position);
        position += Len.WGRZ_AREA_GARANZIA;
        setWgrlAreaGaranziaLiqBytes(buffer, position);
        position += Len.WGRL_AREA_GARANZIA_LIQ;
        setWisoAreaImpostaSostBytes(buffer, position);
        position += Len.WISO_AREA_IMPOSTA_SOST;
        setWlquAreaLiquidazioneBytes(buffer, position);
        position += Len.WLQU_AREA_LIQUIDAZIONE;
        setWmovAreaMovimentoBytes(buffer, position);
        position += Len.WMOV_AREA_MOVIMENTO;
        setWmfzAreaMoviFinrioBytes(buffer, position);
        position += Len.WMFZ_AREA_MOVI_FINRIO;
        wpogAreaParamOgg.setWpogAreaParamOggBytes(buffer, position);
        position += WpogAreaParamOgg.Len.WPOG_AREA_PARAM_OGG;
        wpmoAreaParamMov.setWpmoAreaParamMovBytes(buffer, position);
        position += WpmoAreaParamMov.Len.WPMO_AREA_PARAM_MOV;
        setWpliAreaPercLiqBytes(buffer, position);
        position += Len.WPLI_AREA_PERC_LIQ;
        setWpolAreaPolizzaBytes(buffer, position);
        position += Len.WPOL_AREA_POLIZZA;
        setWpreAreaPrestitiBytes(buffer, position);
        position += Len.WPRE_AREA_PRESTITI;
        setWpvtAreaProvBytes(buffer, position);
        position += Len.WPVT_AREA_PROV;
        setWqueAreaQuestBytes(buffer, position);
        position += Len.WQUE_AREA_QUEST;
        setWricAreaRichBytes(buffer, position);
        position += Len.WRIC_AREA_RICH;
        wrdfAreaRichDisinvFnd.setWrdfAreaRichDisinvFndBytes(buffer, position);
        position += WrdfAreaRichDisinvFnd.Len.WRDF_AREA_RICH_DISINV_FND;
        wrifAreaRichInvFnd.setWrifAreaRichInvFndBytes(buffer, position);
        position += WrifAreaRichInvFnd.Len.WRIF_AREA_RICH_INV_FND;
        setWranAreaRappAnagBytes(buffer, position);
        position += Len.WRAN_AREA_RAPP_ANAG;
        setWe15AreaEstRappAnagBytes(buffer, position);
        position += Len.WE15_AREA_EST_RAPP_ANAG;
        setWrreAreaRappReteBytes(buffer, position);
        position += Len.WRRE_AREA_RAPP_RETE;
        setWspgAreaSoprapGarBytes(buffer, position);
        position += Len.WSPG_AREA_SOPRAP_GAR;
        setWsdiAreaStraInvBytes(buffer, position);
        position += Len.WSDI_AREA_STRA_INV;
        setWtitAreaTitContBytes(buffer, position);
        position += Len.WTIT_AREA_TIT_CONT;
        setWtclAreaTitLiqBytes(buffer, position);
        position += Len.WTCL_AREA_TIT_LIQ;
        wtgaAreaTranche.setWtgaAreaTrancheBytes(buffer, position);
        position += WtgaAreaTrancheIvvs0211.Len.WTGA_AREA_TRANCHE;
        wtliAreaTrchLiq.setWtliAreaTrchLiqBytes(buffer, position);
        position += WtliAreaTrchLiq.Len.WTLI_AREA_TRCH_LIQ;
        setWdadAreaDefaultAdesBytes(buffer, position);
        position += Len.WDAD_AREA_DEFAULT_ADES;
        wocoAreaOggColl.setWocoAreaOggCollBytes(buffer, position);
        position += WocoAreaOggColl.Len.WOCO_AREA_OGG_COLL;
        setWdflAreaDflBytes(buffer, position);
        position += Len.WDFL_AREA_DFL;
        setWrstAreaRstBytes(buffer, position);
        position += Len.WRST_AREA_RST;
        wl19AreaQuote.setWl19AreaQuoteBytes(buffer, position);
        position += Wl19AreaQuote.Len.WL19_AREA_QUOTE;
        setWl30AreaReinvstPoliBytes(buffer, position);
        position += Len.WL30_AREA_REINVST_POLI;
        setWl23AreaVincPegBytes(buffer, position);
        position += Len.WL23_AREA_VINC_PEG;
        setWopzAreaOpzioniBytes(buffer, position);
        position += Len.WOPZ_AREA_OPZIONI;
        setWgopAreaGaranziaOpzBytes(buffer, position);
        position += Len.WGOP_AREA_GARANZIA_OPZ;
        setWtopAreaTrancheOpzBytes(buffer, position);
        position += Len.WTOP_AREA_TRANCHE_OPZ;
        setWcntAreaDatiContestBytes(buffer, position);
        position += Len.WCNT_AREA_DATI_CONTEST;
        setWcltAreaClauTxtBytes(buffer, position);
        position += Len.WCLT_AREA_CLAU_TXT;
        setWp58AreaImpostaBolloBytes(buffer, position);
        position += Len.WP58_AREA_IMPOSTA_BOLLO;
        setWp61AreaDCristBytes(buffer, position);
        position += Len.WP61_AREA_D_CRIST;
        setWp67AreaEstPoliCpiPrBytes(buffer, position);
        position += Len.WP67_AREA_EST_POLI_CPI_PR;
        setWp01AreaRichEstBytes(buffer, position);
        position += Len.WP01_AREA_RICH_EST;
        setIvvc0222AreaFndXTrancheBytes(buffer, position);
        position += Len.IVVC0222_AREA_FND_X_TRANCHE;
        setWp86AreaMotLiqBytes(buffer, position);
        position += Len.WP86_AREA_MOT_LIQ;
        setAreaIvvc0223Bytes(buffer, position);
        position += Len.AREA_IVVC0223;
        wkMoviOrig = MarshalByte.readFixedString(buffer, position, Len.WK_MOVI_ORIG);
        position += Len.WK_MOVI_ORIG;
        setWp88AreaServValBytes(buffer, position);
        position += Len.WP88_AREA_SERV_VAL;
        setWp89AreaDservValBytes(buffer, position);
        position += Len.WP89_AREA_DSERV_VAL;
        setWp56AreaQuestAdegVerBytes(buffer, position);
        position += Len.WP56_AREA_QUEST_ADEG_VER;
        setWcdgAreaCommisGestBytes(buffer, position);
    }

    public byte[] getAreaBusinessBytes(byte[] buffer, int offset) {
        int position = offset;
        getWadeAreaAdesioneBytes(buffer, position);
        position += Len.WADE_AREA_ADESIONE;
        getWbepAreaBenefBytes(buffer, position);
        position += Len.WBEP_AREA_BENEF;
        getWbelAreaBenefLiqBytes(buffer, position);
        position += Len.WBEL_AREA_BENEF_LIQ;
        getWdcoAreaDtCollettivaBytes(buffer, position);
        position += Len.WDCO_AREA_DT_COLLETTIVA;
        getWdfaAreaDtFiscAdesBytes(buffer, position);
        position += Len.WDFA_AREA_DT_FISC_ADES;
        wdeqAreaDettQuest.getWdeqAreaDettQuestBytes(buffer, position);
        position += WdeqAreaDettQuest.Len.WDEQ_AREA_DETT_QUEST;
        getWdtcAreaDettTitContBytes(buffer, position);
        position += Len.WDTC_AREA_DETT_TIT_CONT;
        getWgrzAreaGaranziaBytes(buffer, position);
        position += Len.WGRZ_AREA_GARANZIA;
        getWgrlAreaGaranziaLiqBytes(buffer, position);
        position += Len.WGRL_AREA_GARANZIA_LIQ;
        getWisoAreaImpostaSostBytes(buffer, position);
        position += Len.WISO_AREA_IMPOSTA_SOST;
        getWlquAreaLiquidazioneBytes(buffer, position);
        position += Len.WLQU_AREA_LIQUIDAZIONE;
        getWmovAreaMovimentoBytes(buffer, position);
        position += Len.WMOV_AREA_MOVIMENTO;
        getWmfzAreaMoviFinrioBytes(buffer, position);
        position += Len.WMFZ_AREA_MOVI_FINRIO;
        wpogAreaParamOgg.getWpogAreaParamOggBytes(buffer, position);
        position += WpogAreaParamOgg.Len.WPOG_AREA_PARAM_OGG;
        wpmoAreaParamMov.getWpmoAreaParamMovBytes(buffer, position);
        position += WpmoAreaParamMov.Len.WPMO_AREA_PARAM_MOV;
        getWpliAreaPercLiqBytes(buffer, position);
        position += Len.WPLI_AREA_PERC_LIQ;
        getWpolAreaPolizzaBytes(buffer, position);
        position += Len.WPOL_AREA_POLIZZA;
        getWpreAreaPrestitiBytes(buffer, position);
        position += Len.WPRE_AREA_PRESTITI;
        getWpvtAreaProvBytes(buffer, position);
        position += Len.WPVT_AREA_PROV;
        getWqueAreaQuestBytes(buffer, position);
        position += Len.WQUE_AREA_QUEST;
        getWricAreaRichBytes(buffer, position);
        position += Len.WRIC_AREA_RICH;
        wrdfAreaRichDisinvFnd.getWrdfAreaRichDisinvFndBytes(buffer, position);
        position += WrdfAreaRichDisinvFnd.Len.WRDF_AREA_RICH_DISINV_FND;
        wrifAreaRichInvFnd.getWrifAreaRichInvFndBytes(buffer, position);
        position += WrifAreaRichInvFnd.Len.WRIF_AREA_RICH_INV_FND;
        getWranAreaRappAnagBytes(buffer, position);
        position += Len.WRAN_AREA_RAPP_ANAG;
        getWe15AreaEstRappAnagBytes(buffer, position);
        position += Len.WE15_AREA_EST_RAPP_ANAG;
        getWrreAreaRappReteBytes(buffer, position);
        position += Len.WRRE_AREA_RAPP_RETE;
        getWspgAreaSoprapGarBytes(buffer, position);
        position += Len.WSPG_AREA_SOPRAP_GAR;
        getWsdiAreaStraInvBytes(buffer, position);
        position += Len.WSDI_AREA_STRA_INV;
        getWtitAreaTitContBytes(buffer, position);
        position += Len.WTIT_AREA_TIT_CONT;
        getWtclAreaTitLiqBytes(buffer, position);
        position += Len.WTCL_AREA_TIT_LIQ;
        wtgaAreaTranche.getWtgaAreaTrancheBytes(buffer, position);
        position += WtgaAreaTrancheIvvs0211.Len.WTGA_AREA_TRANCHE;
        wtliAreaTrchLiq.getWtliAreaTrchLiqBytes(buffer, position);
        position += WtliAreaTrchLiq.Len.WTLI_AREA_TRCH_LIQ;
        getWdadAreaDefaultAdesBytes(buffer, position);
        position += Len.WDAD_AREA_DEFAULT_ADES;
        wocoAreaOggColl.getWocoAreaOggCollBytes(buffer, position);
        position += WocoAreaOggColl.Len.WOCO_AREA_OGG_COLL;
        getWdflAreaDflBytes(buffer, position);
        position += Len.WDFL_AREA_DFL;
        getWrstAreaRstBytes(buffer, position);
        position += Len.WRST_AREA_RST;
        wl19AreaQuote.getWl19AreaQuoteBytes(buffer, position);
        position += Wl19AreaQuote.Len.WL19_AREA_QUOTE;
        getWl30AreaReinvstPoliBytes(buffer, position);
        position += Len.WL30_AREA_REINVST_POLI;
        getWl23AreaVincPegBytes(buffer, position);
        position += Len.WL23_AREA_VINC_PEG;
        getWopzAreaOpzioniBytes(buffer, position);
        position += Len.WOPZ_AREA_OPZIONI;
        getWgopAreaGaranziaOpzBytes(buffer, position);
        position += Len.WGOP_AREA_GARANZIA_OPZ;
        getWtopAreaTrancheOpzBytes(buffer, position);
        position += Len.WTOP_AREA_TRANCHE_OPZ;
        getWcntAreaDatiContestBytes(buffer, position);
        position += Len.WCNT_AREA_DATI_CONTEST;
        getWcltAreaClauTxtBytes(buffer, position);
        position += Len.WCLT_AREA_CLAU_TXT;
        getWp58AreaImpostaBolloBytes(buffer, position);
        position += Len.WP58_AREA_IMPOSTA_BOLLO;
        getWp61AreaDCristBytes(buffer, position);
        position += Len.WP61_AREA_D_CRIST;
        getWp67AreaEstPoliCpiPrBytes(buffer, position);
        position += Len.WP67_AREA_EST_POLI_CPI_PR;
        getWp01AreaRichEstBytes(buffer, position);
        position += Len.WP01_AREA_RICH_EST;
        getIvvc0222AreaFndXTrancheBytes(buffer, position);
        position += Len.IVVC0222_AREA_FND_X_TRANCHE;
        getWp86AreaMotLiqBytes(buffer, position);
        position += Len.WP86_AREA_MOT_LIQ;
        getAreaIvvc0223Bytes(buffer, position);
        position += Len.AREA_IVVC0223;
        MarshalByte.writeString(buffer, position, wkMoviOrig, Len.WK_MOVI_ORIG);
        position += Len.WK_MOVI_ORIG;
        getWp88AreaServValBytes(buffer, position);
        position += Len.WP88_AREA_SERV_VAL;
        getWp89AreaDservValBytes(buffer, position);
        position += Len.WP89_AREA_DSERV_VAL;
        getWp56AreaQuestAdegVerBytes(buffer, position);
        position += Len.WP56_AREA_QUEST_ADEG_VER;
        getWcdgAreaCommisGestBytes(buffer, position);
        return buffer;
    }

    public void setWadeAreaAdesioneFormatted(String data) {
        byte[] buffer = new byte[Len.WADE_AREA_ADESIONE];
        MarshalByte.writeString(buffer, 1, data, Len.WADE_AREA_ADESIONE);
        setWadeAreaAdesioneBytes(buffer, 1);
    }

    public void setWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        wadeEleAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWadeTabAdesBytes(buffer, position);
    }

    public byte[] getWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wadeEleAdesMax);
        position += Types.SHORT_SIZE;
        getWadeTabAdesBytes(buffer, position);
        return buffer;
    }

    public void setWadeEleAdesMax(short wadeEleAdesMax) {
        this.wadeEleAdesMax = wadeEleAdesMax;
    }

    public short getWadeEleAdesMax() {
        return this.wadeEleAdesMax;
    }

    public void setWadeTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvade1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvade1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvade1.Len.Int.ID_PTF, 0));
        position += Lccvade1.Len.ID_PTF;
        lccvade1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWadeTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvade1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvade1.getIdPtf(), Lccvade1.Len.Int.ID_PTF, 0);
        position += Lccvade1.Len.ID_PTF;
        lccvade1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWbepAreaBenefFormatted(String data) {
        byte[] buffer = new byte[Len.WBEP_AREA_BENEF];
        MarshalByte.writeString(buffer, 1, data, Len.WBEP_AREA_BENEF);
        setWbepAreaBenefBytes(buffer, 1);
    }

    public void setWbepAreaBenefBytes(byte[] buffer, int offset) {
        int position = offset;
        wbepEleBenefMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WBEP_TAB_BENEFICIARI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wbepTabBeneficiari[idx - 1].setWbepTabBeneficiariBytes(buffer, position);
                position += WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
            }
            else {
                wbepTabBeneficiari[idx - 1].initWbepTabBeneficiariSpaces();
                position += WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
            }
        }
    }

    public byte[] getWbepAreaBenefBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wbepEleBenefMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WBEP_TAB_BENEFICIARI_MAXOCCURS; idx++) {
            wbepTabBeneficiari[idx - 1].getWbepTabBeneficiariBytes(buffer, position);
            position += WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
        }
        return buffer;
    }

    public void setWbepEleBenefMax(short wbepEleBenefMax) {
        this.wbepEleBenefMax = wbepEleBenefMax;
    }

    public short getWbepEleBenefMax() {
        return this.wbepEleBenefMax;
    }

    public void setWbelAreaBenefLiqFormatted(String data) {
        byte[] buffer = new byte[Len.WBEL_AREA_BENEF_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.WBEL_AREA_BENEF_LIQ);
        setWbelAreaBenefLiqBytes(buffer, 1);
    }

    public void setWbelAreaBenefLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wbelEleBenefLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WBEL_TAB_BENE_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wbelTabBeneLiq[idx - 1].setWbelTabBeneLiqBytes(buffer, position);
                position += WbelTabBeneLiq.Len.WBEL_TAB_BENE_LIQ;
            }
            else {
                wbelTabBeneLiq[idx - 1].initWbelTabBeneLiqSpaces();
                position += WbelTabBeneLiq.Len.WBEL_TAB_BENE_LIQ;
            }
        }
    }

    public byte[] getWbelAreaBenefLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wbelEleBenefLiqMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WBEL_TAB_BENE_LIQ_MAXOCCURS; idx++) {
            wbelTabBeneLiq[idx - 1].getWbelTabBeneLiqBytes(buffer, position);
            position += WbelTabBeneLiq.Len.WBEL_TAB_BENE_LIQ;
        }
        return buffer;
    }

    public void setWbelEleBenefLiqMax(short wbelEleBenefLiqMax) {
        this.wbelEleBenefLiqMax = wbelEleBenefLiqMax;
    }

    public short getWbelEleBenefLiqMax() {
        return this.wbelEleBenefLiqMax;
    }

    public void setWdcoAreaDtCollettivaFormatted(String data) {
        byte[] buffer = new byte[Len.WDCO_AREA_DT_COLLETTIVA];
        MarshalByte.writeString(buffer, 1, data, Len.WDCO_AREA_DT_COLLETTIVA);
        setWdcoAreaDtCollettivaBytes(buffer, 1);
    }

    public void setWdcoAreaDtCollettivaBytes(byte[] buffer, int offset) {
        int position = offset;
        wdcoEleCollMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWdcoTabCollBytes(buffer, position);
    }

    public byte[] getWdcoAreaDtCollettivaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdcoEleCollMax);
        position += Types.SHORT_SIZE;
        getWdcoTabCollBytes(buffer, position);
        return buffer;
    }

    public void setWdcoEleCollMax(short wdcoEleCollMax) {
        this.wdcoEleCollMax = wdcoEleCollMax;
    }

    public short getWdcoEleCollMax() {
        return this.wdcoEleCollMax;
    }

    public void setWdcoTabCollBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdco1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdco1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdco1.Len.Int.ID_PTF, 0));
        position += Lccvdco1.Len.ID_PTF;
        lccvdco1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdcoTabCollBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdco1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdco1.getIdPtf(), Lccvdco1.Len.Int.ID_PTF, 0);
        position += Lccvdco1.Len.ID_PTF;
        lccvdco1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWdfaAreaDtFiscAdesFormatted(String data) {
        byte[] buffer = new byte[Len.WDFA_AREA_DT_FISC_ADES];
        MarshalByte.writeString(buffer, 1, data, Len.WDFA_AREA_DT_FISC_ADES);
        setWdfaAreaDtFiscAdesBytes(buffer, 1);
    }

    public void setWdfaAreaDtFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        wdfaEleFiscAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWdfaTabFiscAdesBytes(buffer, position);
    }

    public byte[] getWdfaAreaDtFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdfaEleFiscAdesMax);
        position += Types.SHORT_SIZE;
        getWdfaTabFiscAdesBytes(buffer, position);
        return buffer;
    }

    public void setWdfaEleFiscAdesMax(short wdfaEleFiscAdesMax) {
        this.wdfaEleFiscAdesMax = wdfaEleFiscAdesMax;
    }

    public short getWdfaEleFiscAdesMax() {
        return this.wdfaEleFiscAdesMax;
    }

    public void setWdfaTabFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdfa1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdfa1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdfa1.Len.Int.ID_PTF, 0));
        position += Lccvdfa1.Len.ID_PTF;
        lccvdfa1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdfaTabFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdfa1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdfa1.getIdPtf(), Lccvdfa1.Len.Int.ID_PTF, 0);
        position += Lccvdfa1.Len.ID_PTF;
        lccvdfa1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWdtcAreaDettTitContFormatted(String data) {
        byte[] buffer = new byte[Len.WDTC_AREA_DETT_TIT_CONT];
        MarshalByte.writeString(buffer, 1, data, Len.WDTC_AREA_DETT_TIT_CONT);
        setWdtcAreaDettTitContBytes(buffer, 1);
    }

    public void setWdtcAreaDettTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        wdtcEleDettTitMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WDTC_TAB_DTC_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wdtcTabDtc[idx - 1].setWdtcTabDtcBytes(buffer, position);
                position += WdtcTabDtc.Len.WDTC_TAB_DTC;
            }
            else {
                wdtcTabDtc[idx - 1].initWdtcTabDtcSpaces();
                position += WdtcTabDtc.Len.WDTC_TAB_DTC;
            }
        }
    }

    public byte[] getWdtcAreaDettTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdtcEleDettTitMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WDTC_TAB_DTC_MAXOCCURS; idx++) {
            wdtcTabDtc[idx - 1].getWdtcTabDtcBytes(buffer, position);
            position += WdtcTabDtc.Len.WDTC_TAB_DTC;
        }
        return buffer;
    }

    public void setWdtcEleDettTitMax(short wdtcEleDettTitMax) {
        this.wdtcEleDettTitMax = wdtcEleDettTitMax;
    }

    public short getWdtcEleDettTitMax() {
        return this.wdtcEleDettTitMax;
    }

    public void setWgrzAreaGaranziaFormatted(String data) {
        byte[] buffer = new byte[Len.WGRZ_AREA_GARANZIA];
        MarshalByte.writeString(buffer, 1, data, Len.WGRZ_AREA_GARANZIA);
        setWgrzAreaGaranziaBytes(buffer, 1);
    }

    public void setWgrzAreaGaranziaBytes(byte[] buffer, int offset) {
        int position = offset;
        wgrzEleGaranziaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGRZ_TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wgrzTabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                wgrzTabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public byte[] getWgrzAreaGaranziaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wgrzEleGaranziaMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGRZ_TAB_GAR_MAXOCCURS; idx++) {
            wgrzTabGar[idx - 1].getTabGarBytes(buffer, position);
            position += WgrzTabGar.Len.TAB_GAR;
        }
        return buffer;
    }

    public void setWgrzEleGaranziaMax(short wgrzEleGaranziaMax) {
        this.wgrzEleGaranziaMax = wgrzEleGaranziaMax;
    }

    public short getWgrzEleGaranziaMax() {
        return this.wgrzEleGaranziaMax;
    }

    public void setWgrlAreaGaranziaLiqFormatted(String data) {
        byte[] buffer = new byte[Len.WGRL_AREA_GARANZIA_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.WGRL_AREA_GARANZIA_LIQ);
        setWgrlAreaGaranziaLiqBytes(buffer, 1);
    }

    public void setWgrlAreaGaranziaLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wgrlEleGarLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGRL_TAB_GAR_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wgrlTabGarLiq[idx - 1].setWgrlTabGarLiqBytes(buffer, position);
                position += WgrlTabGarLiq.Len.WGRL_TAB_GAR_LIQ;
            }
            else {
                wgrlTabGarLiq[idx - 1].initWgrlTabGarLiqSpaces();
                position += WgrlTabGarLiq.Len.WGRL_TAB_GAR_LIQ;
            }
        }
    }

    public byte[] getWgrlAreaGaranziaLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wgrlEleGarLiqMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGRL_TAB_GAR_LIQ_MAXOCCURS; idx++) {
            wgrlTabGarLiq[idx - 1].getWgrlTabGarLiqBytes(buffer, position);
            position += WgrlTabGarLiq.Len.WGRL_TAB_GAR_LIQ;
        }
        return buffer;
    }

    public void setWgrlEleGarLiqMax(short wgrlEleGarLiqMax) {
        this.wgrlEleGarLiqMax = wgrlEleGarLiqMax;
    }

    public short getWgrlEleGarLiqMax() {
        return this.wgrlEleGarLiqMax;
    }

    public void setWisoAreaImpostaSostFormatted(String data) {
        byte[] buffer = new byte[Len.WISO_AREA_IMPOSTA_SOST];
        MarshalByte.writeString(buffer, 1, data, Len.WISO_AREA_IMPOSTA_SOST);
        setWisoAreaImpostaSostBytes(buffer, 1);
    }

    public void setWisoAreaImpostaSostBytes(byte[] buffer, int offset) {
        int position = offset;
        wisoEleImpSostMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWisoTabImpSostBytes(buffer, position);
    }

    public byte[] getWisoAreaImpostaSostBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wisoEleImpSostMax);
        position += Types.SHORT_SIZE;
        getWisoTabImpSostBytes(buffer, position);
        return buffer;
    }

    public void setWisoEleImpSostMax(short wisoEleImpSostMax) {
        this.wisoEleImpSostMax = wisoEleImpSostMax;
    }

    public short getWisoEleImpSostMax() {
        return this.wisoEleImpSostMax;
    }

    public void setWisoTabImpSostBytes(byte[] buffer, int offset) {
        int position = offset;
        lccviso1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccviso1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccviso1.Len.Int.ID_PTF, 0));
        position += Lccviso1.Len.ID_PTF;
        lccviso1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWisoTabImpSostBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccviso1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccviso1.getIdPtf(), Lccviso1.Len.Int.ID_PTF, 0);
        position += Lccviso1.Len.ID_PTF;
        lccviso1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWlquAreaLiquidazioneFormatted(String data) {
        byte[] buffer = new byte[Len.WLQU_AREA_LIQUIDAZIONE];
        MarshalByte.writeString(buffer, 1, data, Len.WLQU_AREA_LIQUIDAZIONE);
        setWlquAreaLiquidazioneBytes(buffer, 1);
    }

    public void setWlquAreaLiquidazioneBytes(byte[] buffer, int offset) {
        int position = offset;
        wlquEleLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WLQU_TAB_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wlquTabLiq[idx - 1].setWlquTabLiqBytes(buffer, position);
                position += S089TabLiq.Len.WLQU_TAB_LIQ;
            }
            else {
                wlquTabLiq[idx - 1].initWlquTabLiqSpaces();
                position += S089TabLiq.Len.WLQU_TAB_LIQ;
            }
        }
    }

    public byte[] getWlquAreaLiquidazioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wlquEleLiqMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WLQU_TAB_LIQ_MAXOCCURS; idx++) {
            wlquTabLiq[idx - 1].getWlquTabLiqBytes(buffer, position);
            position += S089TabLiq.Len.WLQU_TAB_LIQ;
        }
        return buffer;
    }

    public void setWlquEleLiqMax(short wlquEleLiqMax) {
        this.wlquEleLiqMax = wlquEleLiqMax;
    }

    public short getWlquEleLiqMax() {
        return this.wlquEleLiqMax;
    }

    public void setWmovAreaMovimentoFormatted(String data) {
        byte[] buffer = new byte[Len.WMOV_AREA_MOVIMENTO];
        MarshalByte.writeString(buffer, 1, data, Len.WMOV_AREA_MOVIMENTO);
        setWmovAreaMovimentoBytes(buffer, 1);
    }

    public void setWmovAreaMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        wmovEleMoviMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWmovTabMoviBytes(buffer, position);
    }

    public byte[] getWmovAreaMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wmovEleMoviMax);
        position += Types.SHORT_SIZE;
        getWmovTabMoviBytes(buffer, position);
        return buffer;
    }

    public void setWmovEleMoviMax(short wmovEleMoviMax) {
        this.wmovEleMoviMax = wmovEleMoviMax;
    }

    public short getWmovEleMoviMax() {
        return this.wmovEleMoviMax;
    }

    public void setWmovTabMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvmov1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvmov1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvmov1.Len.Int.ID_PTF, 0));
        position += Lccvmov1.Len.ID_PTF;
        lccvmov1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWmovTabMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvmov1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvmov1.getIdPtf(), Lccvmov1.Len.Int.ID_PTF, 0);
        position += Lccvmov1.Len.ID_PTF;
        lccvmov1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWmfzAreaMoviFinrioFormatted(String data) {
        byte[] buffer = new byte[Len.WMFZ_AREA_MOVI_FINRIO];
        MarshalByte.writeString(buffer, 1, data, Len.WMFZ_AREA_MOVI_FINRIO);
        setWmfzAreaMoviFinrioBytes(buffer, 1);
    }

    public void setWmfzAreaMoviFinrioBytes(byte[] buffer, int offset) {
        int position = offset;
        wmfzEleMoviFinrioMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WMFZ_TAB_MOVI_FINRIO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wmfzTabMoviFinrio[idx - 1].setWmfzTabMoviFinrioBytes(buffer, position);
                position += WmfzTabMoviFinrio.Len.WMFZ_TAB_MOVI_FINRIO;
            }
            else {
                wmfzTabMoviFinrio[idx - 1].initWmfzTabMoviFinrioSpaces();
                position += WmfzTabMoviFinrio.Len.WMFZ_TAB_MOVI_FINRIO;
            }
        }
    }

    public byte[] getWmfzAreaMoviFinrioBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wmfzEleMoviFinrioMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WMFZ_TAB_MOVI_FINRIO_MAXOCCURS; idx++) {
            wmfzTabMoviFinrio[idx - 1].getWmfzTabMoviFinrioBytes(buffer, position);
            position += WmfzTabMoviFinrio.Len.WMFZ_TAB_MOVI_FINRIO;
        }
        return buffer;
    }

    public void setWmfzEleMoviFinrioMax(short wmfzEleMoviFinrioMax) {
        this.wmfzEleMoviFinrioMax = wmfzEleMoviFinrioMax;
    }

    public short getWmfzEleMoviFinrioMax() {
        return this.wmfzEleMoviFinrioMax;
    }

    public void setWpliAreaPercLiqFormatted(String data) {
        byte[] buffer = new byte[Len.WPLI_AREA_PERC_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.WPLI_AREA_PERC_LIQ);
        setWpliAreaPercLiqBytes(buffer, 1);
    }

    public void setWpliAreaPercLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wpliElePercLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPLI_TAB_PERC_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpliTabPercLiq[idx - 1].setWpliTabPercLiqBytes(buffer, position);
                position += WpliTabPercLiq.Len.WPLI_TAB_PERC_LIQ;
            }
            else {
                wpliTabPercLiq[idx - 1].initWpliTabPercLiqSpaces();
                position += WpliTabPercLiq.Len.WPLI_TAB_PERC_LIQ;
            }
        }
    }

    public byte[] getWpliAreaPercLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpliElePercLiqMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPLI_TAB_PERC_LIQ_MAXOCCURS; idx++) {
            wpliTabPercLiq[idx - 1].getWpliTabPercLiqBytes(buffer, position);
            position += WpliTabPercLiq.Len.WPLI_TAB_PERC_LIQ;
        }
        return buffer;
    }

    public void setWpliElePercLiqMax(short wpliElePercLiqMax) {
        this.wpliElePercLiqMax = wpliElePercLiqMax;
    }

    public short getWpliElePercLiqMax() {
        return this.wpliElePercLiqMax;
    }

    public void setWpolAreaPolizzaFormatted(String data) {
        byte[] buffer = new byte[Len.WPOL_AREA_POLIZZA];
        MarshalByte.writeString(buffer, 1, data, Len.WPOL_AREA_POLIZZA);
        setWpolAreaPolizzaBytes(buffer, 1);
    }

    public void setWpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        wpolElePoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWpolTabPoliBytes(buffer, position);
    }

    public byte[] getWpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpolElePoliMax);
        position += Types.SHORT_SIZE;
        getWpolTabPoliBytes(buffer, position);
        return buffer;
    }

    public void setWpolElePoliMax(short wpolElePoliMax) {
        this.wpolElePoliMax = wpolElePoliMax;
    }

    public short getWpolElePoliMax() {
        return this.wpolElePoliMax;
    }

    public void setWpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1.Len.Int.ID_PTF, 0));
        position += Lccvpol1.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvpol1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvpol1.getIdPtf(), Lccvpol1.Len.Int.ID_PTF, 0);
        position += Lccvpol1.Len.ID_PTF;
        lccvpol1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWpreAreaPrestitiFormatted(String data) {
        byte[] buffer = new byte[Len.WPRE_AREA_PRESTITI];
        MarshalByte.writeString(buffer, 1, data, Len.WPRE_AREA_PRESTITI);
        setWpreAreaPrestitiBytes(buffer, 1);
    }

    public void setWpreAreaPrestitiBytes(byte[] buffer, int offset) {
        int position = offset;
        wpreElePrestitiMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPRE_TAB_PRESTITI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpreTabPrestiti[idx - 1].setWpreTabPrestitiBytes(buffer, position);
                position += WpreTabPrestiti.Len.WPRE_TAB_PRESTITI;
            }
            else {
                wpreTabPrestiti[idx - 1].initWpreTabPrestitiSpaces();
                position += WpreTabPrestiti.Len.WPRE_TAB_PRESTITI;
            }
        }
    }

    public byte[] getWpreAreaPrestitiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpreElePrestitiMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPRE_TAB_PRESTITI_MAXOCCURS; idx++) {
            wpreTabPrestiti[idx - 1].getWpreTabPrestitiBytes(buffer, position);
            position += WpreTabPrestiti.Len.WPRE_TAB_PRESTITI;
        }
        return buffer;
    }

    public void setWpreElePrestitiMax(short wpreElePrestitiMax) {
        this.wpreElePrestitiMax = wpreElePrestitiMax;
    }

    public short getWpreElePrestitiMax() {
        return this.wpreElePrestitiMax;
    }

    public void setWpvtAreaProvFormatted(String data) {
        byte[] buffer = new byte[Len.WPVT_AREA_PROV];
        MarshalByte.writeString(buffer, 1, data, Len.WPVT_AREA_PROV);
        setWpvtAreaProvBytes(buffer, 1);
    }

    public void setWpvtAreaProvBytes(byte[] buffer, int offset) {
        int position = offset;
        wpvtEleProvMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPVT_TAB_PROV_TR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpvtTabProvTr[idx - 1].setWpvtTabProvTrBytes(buffer, position);
                position += WpvtTabProvTr.Len.WPVT_TAB_PROV_TR;
            }
            else {
                wpvtTabProvTr[idx - 1].initWpvtTabProvTrSpaces();
                position += WpvtTabProvTr.Len.WPVT_TAB_PROV_TR;
            }
        }
    }

    public byte[] getWpvtAreaProvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpvtEleProvMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPVT_TAB_PROV_TR_MAXOCCURS; idx++) {
            wpvtTabProvTr[idx - 1].getWpvtTabProvTrBytes(buffer, position);
            position += WpvtTabProvTr.Len.WPVT_TAB_PROV_TR;
        }
        return buffer;
    }

    public void setWpvtEleProvMax(short wpvtEleProvMax) {
        this.wpvtEleProvMax = wpvtEleProvMax;
    }

    public short getWpvtEleProvMax() {
        return this.wpvtEleProvMax;
    }

    public void setWqueAreaQuestFormatted(String data) {
        byte[] buffer = new byte[Len.WQUE_AREA_QUEST];
        MarshalByte.writeString(buffer, 1, data, Len.WQUE_AREA_QUEST);
        setWqueAreaQuestBytes(buffer, 1);
    }

    public void setWqueAreaQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        wqueEleQuestMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WQUE_TAB_QUEST_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wqueTabQuest[idx - 1].setWqueTabQuestBytes(buffer, position);
                position += WqueTabQuest.Len.WQUE_TAB_QUEST;
            }
            else {
                wqueTabQuest[idx - 1].initWqueTabQuestSpaces();
                position += WqueTabQuest.Len.WQUE_TAB_QUEST;
            }
        }
    }

    public byte[] getWqueAreaQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wqueEleQuestMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WQUE_TAB_QUEST_MAXOCCURS; idx++) {
            wqueTabQuest[idx - 1].getWqueTabQuestBytes(buffer, position);
            position += WqueTabQuest.Len.WQUE_TAB_QUEST;
        }
        return buffer;
    }

    public void setWqueEleQuestMax(short wqueEleQuestMax) {
        this.wqueEleQuestMax = wqueEleQuestMax;
    }

    public short getWqueEleQuestMax() {
        return this.wqueEleQuestMax;
    }

    public void setWricAreaRichFormatted(String data) {
        byte[] buffer = new byte[Len.WRIC_AREA_RICH];
        MarshalByte.writeString(buffer, 1, data, Len.WRIC_AREA_RICH);
        setWricAreaRichBytes(buffer, 1);
    }

    public void setWricAreaRichBytes(byte[] buffer, int offset) {
        int position = offset;
        wricEleRichMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWricTabRichBytes(buffer, position);
    }

    public byte[] getWricAreaRichBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wricEleRichMax);
        position += Types.SHORT_SIZE;
        getWricTabRichBytes(buffer, position);
        return buffer;
    }

    public void setWricEleRichMax(short wricEleRichMax) {
        this.wricEleRichMax = wricEleRichMax;
    }

    public short getWricEleRichMax() {
        return this.wricEleRichMax;
    }

    public void setWricTabRichBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvric1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvric1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvric1.Len.Int.ID_PTF, 0));
        position += Lccvric1.Len.ID_PTF;
        lccvric1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWricTabRichBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvric1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvric1.getIdPtf(), Lccvric1.Len.Int.ID_PTF, 0);
        position += Lccvric1.Len.ID_PTF;
        lccvric1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWranAreaRappAnagFormatted(String data) {
        byte[] buffer = new byte[Len.WRAN_AREA_RAPP_ANAG];
        MarshalByte.writeString(buffer, 1, data, Len.WRAN_AREA_RAPP_ANAG);
        setWranAreaRappAnagBytes(buffer, 1);
    }

    public void setWranAreaRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        wranEleRappAnagMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRAN_TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wranTabRappAnag[idx - 1].setWranTabRappAnagBytes(buffer, position);
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
            else {
                wranTabRappAnag[idx - 1].initWranTabRappAnagSpaces();
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
        }
    }

    public byte[] getWranAreaRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wranEleRappAnagMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRAN_TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            wranTabRappAnag[idx - 1].getWranTabRappAnagBytes(buffer, position);
            position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
        }
        return buffer;
    }

    public void setWranEleRappAnagMax(short wranEleRappAnagMax) {
        this.wranEleRappAnagMax = wranEleRappAnagMax;
    }

    public short getWranEleRappAnagMax() {
        return this.wranEleRappAnagMax;
    }

    public void setWe15AreaEstRappAnagFormatted(String data) {
        byte[] buffer = new byte[Len.WE15_AREA_EST_RAPP_ANAG];
        MarshalByte.writeString(buffer, 1, data, Len.WE15_AREA_EST_RAPP_ANAG);
        setWe15AreaEstRappAnagBytes(buffer, 1);
    }

    public void setWe15AreaEstRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        we15EleEstRappAnagMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WE15_TAB_E15_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                we15TabE15[idx - 1].setWe15TabE15Bytes(buffer, position);
                position += We15TabE15.Len.WE15_TAB_E15;
            }
            else {
                we15TabE15[idx - 1].initWe15TabE15Spaces();
                position += We15TabE15.Len.WE15_TAB_E15;
            }
        }
    }

    public byte[] getWe15AreaEstRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, we15EleEstRappAnagMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WE15_TAB_E15_MAXOCCURS; idx++) {
            we15TabE15[idx - 1].getWe15TabE15Bytes(buffer, position);
            position += We15TabE15.Len.WE15_TAB_E15;
        }
        return buffer;
    }

    public void setWe15EleEstRappAnagMax(short we15EleEstRappAnagMax) {
        this.we15EleEstRappAnagMax = we15EleEstRappAnagMax;
    }

    public short getWe15EleEstRappAnagMax() {
        return this.we15EleEstRappAnagMax;
    }

    public void setWrreAreaRappReteFormatted(String data) {
        byte[] buffer = new byte[Len.WRRE_AREA_RAPP_RETE];
        MarshalByte.writeString(buffer, 1, data, Len.WRRE_AREA_RAPP_RETE);
        setWrreAreaRappReteBytes(buffer, 1);
    }

    public void setWrreAreaRappReteBytes(byte[] buffer, int offset) {
        int position = offset;
        wrreEleRappReteMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRRE_TAB_RAPP_RETE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wrreTabRappRete[idx - 1].setWrreTabRappReteBytes(buffer, position);
                position += WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
            }
            else {
                wrreTabRappRete[idx - 1].initWrreTabRappReteSpaces();
                position += WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
            }
        }
    }

    public byte[] getWrreAreaRappReteBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wrreEleRappReteMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRRE_TAB_RAPP_RETE_MAXOCCURS; idx++) {
            wrreTabRappRete[idx - 1].getTabRreBytes(buffer, position);
            position += WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
        }
        return buffer;
    }

    public void setWrreEleRappReteMax(short wrreEleRappReteMax) {
        this.wrreEleRappReteMax = wrreEleRappReteMax;
    }

    public short getWrreEleRappReteMax() {
        return this.wrreEleRappReteMax;
    }

    public void setWspgAreaSoprapGarFormatted(String data) {
        byte[] buffer = new byte[Len.WSPG_AREA_SOPRAP_GAR];
        MarshalByte.writeString(buffer, 1, data, Len.WSPG_AREA_SOPRAP_GAR);
        setWspgAreaSoprapGarBytes(buffer, 1);
    }

    public void setWspgAreaSoprapGarBytes(byte[] buffer, int offset) {
        int position = offset;
        wspgEleSoprapGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WSPG_TAB_SPG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wspgTabSpg[idx - 1].setWspgTabSpgBytes(buffer, position);
                position += WspgTabSpg.Len.WSPG_TAB_SPG;
            }
            else {
                wspgTabSpg[idx - 1].initWspgTabSpgSpaces();
                position += WspgTabSpg.Len.WSPG_TAB_SPG;
            }
        }
    }

    public byte[] getWspgAreaSoprapGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wspgEleSoprapGarMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WSPG_TAB_SPG_MAXOCCURS; idx++) {
            wspgTabSpg[idx - 1].getWspgTabSpgBytes(buffer, position);
            position += WspgTabSpg.Len.WSPG_TAB_SPG;
        }
        return buffer;
    }

    public void setWspgEleSoprapGarMax(short wspgEleSoprapGarMax) {
        this.wspgEleSoprapGarMax = wspgEleSoprapGarMax;
    }

    public short getWspgEleSoprapGarMax() {
        return this.wspgEleSoprapGarMax;
    }

    public void setWsdiAreaStraInvFormatted(String data) {
        byte[] buffer = new byte[Len.WSDI_AREA_STRA_INV];
        MarshalByte.writeString(buffer, 1, data, Len.WSDI_AREA_STRA_INV);
        setWsdiAreaStraInvBytes(buffer, 1);
    }

    public void setWsdiAreaStraInvBytes(byte[] buffer, int offset) {
        int position = offset;
        wsdiEleStraInvMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WSDI_TAB_STRA_INV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wsdiTabStraInv[idx - 1].setWsdiTabStraInvBytes(buffer, position);
                position += WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
            }
            else {
                wsdiTabStraInv[idx - 1].initWsdiTabStraInvSpaces();
                position += WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
            }
        }
    }

    public byte[] getWsdiAreaStraInvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wsdiEleStraInvMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WSDI_TAB_STRA_INV_MAXOCCURS; idx++) {
            wsdiTabStraInv[idx - 1].getWsdiTabStraInvBytes(buffer, position);
            position += WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
        }
        return buffer;
    }

    public void setWsdiEleStraInvMax(short wsdiEleStraInvMax) {
        this.wsdiEleStraInvMax = wsdiEleStraInvMax;
    }

    public short getWsdiEleStraInvMax() {
        return this.wsdiEleStraInvMax;
    }

    public void setWtitAreaTitContFormatted(String data) {
        byte[] buffer = new byte[Len.WTIT_AREA_TIT_CONT];
        MarshalByte.writeString(buffer, 1, data, Len.WTIT_AREA_TIT_CONT);
        setWtitAreaTitContBytes(buffer, 1);
    }

    public void setWtitAreaTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        wtitEleTitContMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTIT_TAB_TIT_CONT_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wtitTabTitCont[idx - 1].setWtitTabTitContBytes(buffer, position);
                position += WtitTabTitCont.Len.WTIT_TAB_TIT_CONT;
            }
            else {
                wtitTabTitCont[idx - 1].initWtitTabTitContSpaces();
                position += WtitTabTitCont.Len.WTIT_TAB_TIT_CONT;
            }
        }
    }

    public byte[] getWtitAreaTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtitEleTitContMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTIT_TAB_TIT_CONT_MAXOCCURS; idx++) {
            wtitTabTitCont[idx - 1].getWtitTabTitContBytes(buffer, position);
            position += WtitTabTitCont.Len.WTIT_TAB_TIT_CONT;
        }
        return buffer;
    }

    public void setWtitEleTitContMax(short wtitEleTitContMax) {
        this.wtitEleTitContMax = wtitEleTitContMax;
    }

    public short getWtitEleTitContMax() {
        return this.wtitEleTitContMax;
    }

    public void setWtclAreaTitLiqFormatted(String data) {
        byte[] buffer = new byte[Len.WTCL_AREA_TIT_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.WTCL_AREA_TIT_LIQ);
        setWtclAreaTitLiqBytes(buffer, 1);
    }

    public void setWtclAreaTitLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wtclEleTitLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWtclTabTitLiqBytes(buffer, position);
    }

    public byte[] getWtclAreaTitLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtclEleTitLiqMax);
        position += Types.SHORT_SIZE;
        getWtclTabTitLiqBytes(buffer, position);
        return buffer;
    }

    public void setWtclEleTitLiqMax(short wtclEleTitLiqMax) {
        this.wtclEleTitLiqMax = wtclEleTitLiqMax;
    }

    public short getWtclEleTitLiqMax() {
        return this.wtclEleTitLiqMax;
    }

    public void setWtclTabTitLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvtcl1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvtcl1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvtcl1.Len.Int.ID_PTF, 0));
        position += Lccvtcl1.Len.ID_PTF;
        lccvtcl1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWtclTabTitLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvtcl1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvtcl1.getIdPtf(), Lccvtcl1.Len.Int.ID_PTF, 0);
        position += Lccvtcl1.Len.ID_PTF;
        lccvtcl1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWdadAreaDefaultAdesFormatted(String data) {
        byte[] buffer = new byte[Len.WDAD_AREA_DEFAULT_ADES];
        MarshalByte.writeString(buffer, 1, data, Len.WDAD_AREA_DEFAULT_ADES);
        setWdadAreaDefaultAdesBytes(buffer, 1);
    }

    public void setWdadAreaDefaultAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        wdadEleDfltAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWdadTabDfltAdesBytes(buffer, position);
    }

    public byte[] getWdadAreaDefaultAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdadEleDfltAdesMax);
        position += Types.SHORT_SIZE;
        getWdadTabDfltAdesBytes(buffer, position);
        return buffer;
    }

    public void setWdadEleDfltAdesMax(short wdadEleDfltAdesMax) {
        this.wdadEleDfltAdesMax = wdadEleDfltAdesMax;
    }

    public short getWdadEleDfltAdesMax() {
        return this.wdadEleDfltAdesMax;
    }

    public void setWdadTabDfltAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdad1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdad1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdad1.Len.Int.ID_PTF, 0));
        position += Lccvdad1.Len.ID_PTF;
        lccvdad1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdadTabDfltAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdad1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdad1.getIdPtf(), Lccvdad1.Len.Int.ID_PTF, 0);
        position += Lccvdad1.Len.ID_PTF;
        lccvdad1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWdflAreaDflFormatted(String data) {
        byte[] buffer = new byte[Len.WDFL_AREA_DFL];
        MarshalByte.writeString(buffer, 1, data, Len.WDFL_AREA_DFL);
        setWdflAreaDflBytes(buffer, 1);
    }

    public void setWdflAreaDflBytes(byte[] buffer, int offset) {
        int position = offset;
        wdflEleDflMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWdflTabDflBytes(buffer, position);
    }

    public byte[] getWdflAreaDflBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdflEleDflMax);
        position += Types.SHORT_SIZE;
        getWdflTabDflBytes(buffer, position);
        return buffer;
    }

    public void setWdflEleDflMax(short wdflEleDflMax) {
        this.wdflEleDflMax = wdflEleDflMax;
    }

    public short getWdflEleDflMax() {
        return this.wdflEleDflMax;
    }

    public void setWdflTabDflBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdfl1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdfl1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdfl1.Len.Int.ID_PTF, 0));
        position += Lccvdfl1.Len.ID_PTF;
        lccvdfl1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdflTabDflBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdfl1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdfl1.getIdPtf(), Lccvdfl1.Len.Int.ID_PTF, 0);
        position += Lccvdfl1.Len.ID_PTF;
        lccvdfl1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWrstAreaRstFormatted(String data) {
        byte[] buffer = new byte[Len.WRST_AREA_RST];
        MarshalByte.writeString(buffer, 1, data, Len.WRST_AREA_RST);
        setWrstAreaRstBytes(buffer, 1);
    }

    public void setWrstAreaRstBytes(byte[] buffer, int offset) {
        int position = offset;
        wrstEleRstMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRST_TAB_RST_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wrstTabRst[idx - 1].setWrstTabRstBytes(buffer, position);
                position += WrstTabRst.Len.WRST_TAB_RST;
            }
            else {
                wrstTabRst[idx - 1].initWrstTabRstSpaces();
                position += WrstTabRst.Len.WRST_TAB_RST;
            }
        }
    }

    public byte[] getWrstAreaRstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wrstEleRstMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRST_TAB_RST_MAXOCCURS; idx++) {
            wrstTabRst[idx - 1].getWrstTabRstBytes(buffer, position);
            position += WrstTabRst.Len.WRST_TAB_RST;
        }
        return buffer;
    }

    public void setWrstEleRstMax(short wrstEleRstMax) {
        this.wrstEleRstMax = wrstEleRstMax;
    }

    public short getWrstEleRstMax() {
        return this.wrstEleRstMax;
    }

    public void setWl30AreaReinvstPoliFormatted(String data) {
        byte[] buffer = new byte[Len.WL30_AREA_REINVST_POLI];
        MarshalByte.writeString(buffer, 1, data, Len.WL30_AREA_REINVST_POLI);
        setWl30AreaReinvstPoliBytes(buffer, 1);
    }

    public void setWl30AreaReinvstPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        wl30EleReinvstPoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WL30_TAB_REINVST_POLI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wl30TabReinvstPoli[idx - 1].setWl30TabReinvstPoliBytes(buffer, position);
                position += Wl30TabReinvstPoli.Len.WL30_TAB_REINVST_POLI;
            }
            else {
                wl30TabReinvstPoli[idx - 1].initWl30TabReinvstPoliSpaces();
                position += Wl30TabReinvstPoli.Len.WL30_TAB_REINVST_POLI;
            }
        }
    }

    public byte[] getWl30AreaReinvstPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wl30EleReinvstPoliMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WL30_TAB_REINVST_POLI_MAXOCCURS; idx++) {
            wl30TabReinvstPoli[idx - 1].getWl30TabReinvstPoliBytes(buffer, position);
            position += Wl30TabReinvstPoli.Len.WL30_TAB_REINVST_POLI;
        }
        return buffer;
    }

    public void setWl30EleReinvstPoliMax(short wl30EleReinvstPoliMax) {
        this.wl30EleReinvstPoliMax = wl30EleReinvstPoliMax;
    }

    public short getWl30EleReinvstPoliMax() {
        return this.wl30EleReinvstPoliMax;
    }

    public void setWl23AreaVincPegFormatted(String data) {
        byte[] buffer = new byte[Len.WL23_AREA_VINC_PEG];
        MarshalByte.writeString(buffer, 1, data, Len.WL23_AREA_VINC_PEG);
        setWl23AreaVincPegBytes(buffer, 1);
    }

    public void setWl23AreaVincPegBytes(byte[] buffer, int offset) {
        int position = offset;
        wl23EleVincPegMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WL23_TAB_VINC_PEG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wl23TabVincPeg[idx - 1].setWl23TabVincPegBytes(buffer, position);
                position += Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;
            }
            else {
                wl23TabVincPeg[idx - 1].initWl23TabVincPegSpaces();
                position += Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;
            }
        }
    }

    public byte[] getWl23AreaVincPegBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wl23EleVincPegMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WL23_TAB_VINC_PEG_MAXOCCURS; idx++) {
            wl23TabVincPeg[idx - 1].getWl23TabVincPegBytes(buffer, position);
            position += Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;
        }
        return buffer;
    }

    public void setWl23EleVincPegMax(short wl23EleVincPegMax) {
        this.wl23EleVincPegMax = wl23EleVincPegMax;
    }

    public short getWl23EleVincPegMax() {
        return this.wl23EleVincPegMax;
    }

    public void setWopzAreaOpzioniFormatted(String data) {
        byte[] buffer = new byte[Len.WOPZ_AREA_OPZIONI];
        MarshalByte.writeString(buffer, 1, data, Len.WOPZ_AREA_OPZIONI);
        setWopzAreaOpzioniBytes(buffer, 1);
    }

    public void setWopzAreaOpzioniBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0217.eleMaxOpzioni = MarshalByte.readFixedString(buffer, position, Ivvc0217.Len.ELE_MAX_OPZIONI);
        position += Ivvc0217.Len.ELE_MAX_OPZIONI;
        for (int idx = 1; idx <= Ivvc0217.OPZIONI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                ivvc0217.getOpzioni(idx).setOpzioniBytes(buffer, position);
                position += WopzOpzioni.Len.OPZIONI;
            }
            else {
                ivvc0217.getOpzioni(idx).initOpzioniSpaces();
                position += WopzOpzioni.Len.OPZIONI;
            }
        }
    }

    public byte[] getWopzAreaOpzioniBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ivvc0217.eleMaxOpzioni, Ivvc0217.Len.ELE_MAX_OPZIONI);
        position += Ivvc0217.Len.ELE_MAX_OPZIONI;
        for (int idx = 1; idx <= Ivvc0217.OPZIONI_MAXOCCURS; idx++) {
            ivvc0217.getOpzioni(idx).getOpzioniBytes(buffer, position);
            position += WopzOpzioni.Len.OPZIONI;
        }
        return buffer;
    }

    public void setWgopAreaGaranziaOpzFormatted(String data) {
        byte[] buffer = new byte[Len.WGOP_AREA_GARANZIA_OPZ];
        MarshalByte.writeString(buffer, 1, data, Len.WGOP_AREA_GARANZIA_OPZ);
        setWgopAreaGaranziaOpzBytes(buffer, 1);
    }

    public void setWgopAreaGaranziaOpzBytes(byte[] buffer, int offset) {
        int position = offset;
        wgopEleGaranziaOpzMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGOP_TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wgopTabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                wgopTabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public byte[] getWgopAreaGaranziaOpzBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wgopEleGaranziaOpzMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGOP_TAB_GAR_MAXOCCURS; idx++) {
            wgopTabGar[idx - 1].getTabGarBytes(buffer, position);
            position += WgrzTabGar.Len.TAB_GAR;
        }
        return buffer;
    }

    public void setWgopEleGaranziaOpzMax(short wgopEleGaranziaOpzMax) {
        this.wgopEleGaranziaOpzMax = wgopEleGaranziaOpzMax;
    }

    public short getWgopEleGaranziaOpzMax() {
        return this.wgopEleGaranziaOpzMax;
    }

    public void setWtopAreaTrancheOpzFormatted(String data) {
        byte[] buffer = new byte[Len.WTOP_AREA_TRANCHE_OPZ];
        MarshalByte.writeString(buffer, 1, data, Len.WTOP_AREA_TRANCHE_OPZ);
        setWtopAreaTrancheOpzBytes(buffer, 1);
    }

    public void setWtopAreaTrancheOpzBytes(byte[] buffer, int offset) {
        int position = offset;
        wtopEleTranOpzMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTOP_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wtopTabTran[idx - 1].setTabTranBytes(buffer, position);
                position += WtgaTabTran.Len.TAB_TRAN;
            }
            else {
                wtopTabTran[idx - 1].initTabTranSpaces();
                position += WtgaTabTran.Len.TAB_TRAN;
            }
        }
    }

    public byte[] getWtopAreaTrancheOpzBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtopEleTranOpzMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTOP_TAB_TRAN_MAXOCCURS; idx++) {
            wtopTabTran[idx - 1].getTabTranBytes(buffer, position);
            position += WtgaTabTran.Len.TAB_TRAN;
        }
        return buffer;
    }

    public void setWtopEleTranOpzMax(short wtopEleTranOpzMax) {
        this.wtopEleTranOpzMax = wtopEleTranOpzMax;
    }

    public short getWtopEleTranOpzMax() {
        return this.wtopEleTranOpzMax;
    }

    public void setWcntAreaDatiContestFormatted(String data) {
        byte[] buffer = new byte[Len.WCNT_AREA_DATI_CONTEST];
        MarshalByte.writeString(buffer, 1, data, Len.WCNT_AREA_DATI_CONTEST);
        setWcntAreaDatiContestBytes(buffer, 1);
    }

    public void setWcntAreaDatiContestBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0212.setWcntAreaVariabiliContBytes(buffer, position);
    }

    public byte[] getWcntAreaDatiContestBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0212.getWcntAreaVariabiliContBytes(buffer, position);
        return buffer;
    }

    public void setWcltAreaClauTxtFormatted(String data) {
        byte[] buffer = new byte[Len.WCLT_AREA_CLAU_TXT];
        MarshalByte.writeString(buffer, 1, data, Len.WCLT_AREA_CLAU_TXT);
        setWcltAreaClauTxtBytes(buffer, 1);
    }

    public void setWcltAreaClauTxtBytes(byte[] buffer, int offset) {
        int position = offset;
        wcltEleClauTxtMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WCLT_TAB_CLAU_TXT_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wcltTabClauTxt[idx - 1].setWcltTabClauTxtBytes(buffer, position);
                position += WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;
            }
            else {
                wcltTabClauTxt[idx - 1].initWcltTabClauTxtSpaces();
                position += WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;
            }
        }
    }

    public byte[] getWcltAreaClauTxtBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wcltEleClauTxtMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WCLT_TAB_CLAU_TXT_MAXOCCURS; idx++) {
            wcltTabClauTxt[idx - 1].getWcltTabClauTxtBytes(buffer, position);
            position += WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;
        }
        return buffer;
    }

    public void setWcltEleClauTxtMax(short wcltEleClauTxtMax) {
        this.wcltEleClauTxtMax = wcltEleClauTxtMax;
    }

    public short getWcltEleClauTxtMax() {
        return this.wcltEleClauTxtMax;
    }

    public void setWp58AreaImpostaBolloFormatted(String data) {
        byte[] buffer = new byte[Len.WP58_AREA_IMPOSTA_BOLLO];
        MarshalByte.writeString(buffer, 1, data, Len.WP58_AREA_IMPOSTA_BOLLO);
        setWp58AreaImpostaBolloBytes(buffer, 1);
    }

    public void setWp58AreaImpostaBolloBytes(byte[] buffer, int offset) {
        int position = offset;
        wp58EleMaxImpstBol = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP58_TAB_IMPST_BOL_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wp58TabImpstBol[idx - 1].setWp58TabImpstBolBytes(buffer, position);
                position += Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
            }
            else {
                wp58TabImpstBol[idx - 1].initWp58TabImpstBolSpaces();
                position += Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
            }
        }
    }

    public byte[] getWp58AreaImpostaBolloBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp58EleMaxImpstBol);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP58_TAB_IMPST_BOL_MAXOCCURS; idx++) {
            wp58TabImpstBol[idx - 1].getWp58TabImpstBolBytes(buffer, position);
            position += Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
        }
        return buffer;
    }

    public void setWp58EleMaxImpstBol(short wp58EleMaxImpstBol) {
        this.wp58EleMaxImpstBol = wp58EleMaxImpstBol;
    }

    public short getWp58EleMaxImpstBol() {
        return this.wp58EleMaxImpstBol;
    }

    public void setWp61AreaDCristFormatted(String data) {
        byte[] buffer = new byte[Len.WP61_AREA_D_CRIST];
        MarshalByte.writeString(buffer, 1, data, Len.WP61_AREA_D_CRIST);
        setWp61AreaDCristBytes(buffer, 1);
    }

    public void setWp61AreaDCristBytes(byte[] buffer, int offset) {
        int position = offset;
        wp61EleMaxDCrist = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWp61TabDCristBytes(buffer, position);
    }

    public byte[] getWp61AreaDCristBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp61EleMaxDCrist);
        position += Types.SHORT_SIZE;
        getWp61TabDCristBytes(buffer, position);
        return buffer;
    }

    public void setWp61EleMaxDCrist(short wp61EleMaxDCrist) {
        this.wp61EleMaxDCrist = wp61EleMaxDCrist;
    }

    public short getWp61EleMaxDCrist() {
        return this.wp61EleMaxDCrist;
    }

    public void setWp61TabDCristBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp611.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp611.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp611.Len.Int.ID_PTF, 0));
        position += Lccvp611.Len.ID_PTF;
        lccvp611.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp61TabDCristBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp611.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp611.getIdPtf(), Lccvp611.Len.Int.ID_PTF, 0);
        position += Lccvp611.Len.ID_PTF;
        lccvp611.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWp67AreaEstPoliCpiPrFormatted(String data) {
        byte[] buffer = new byte[Len.WP67_AREA_EST_POLI_CPI_PR];
        MarshalByte.writeString(buffer, 1, data, Len.WP67_AREA_EST_POLI_CPI_PR);
        setWp67AreaEstPoliCpiPrBytes(buffer, 1);
    }

    public void setWp67AreaEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        wp67EleMaxEstPoliCpiPr = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWp67TabEstPoliCpiPrBytes(buffer, position);
    }

    public byte[] getWp67AreaEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp67EleMaxEstPoliCpiPr);
        position += Types.SHORT_SIZE;
        getWp67TabEstPoliCpiPrBytes(buffer, position);
        return buffer;
    }

    public void setWp67EleMaxEstPoliCpiPr(short wp67EleMaxEstPoliCpiPr) {
        this.wp67EleMaxEstPoliCpiPr = wp67EleMaxEstPoliCpiPr;
    }

    public short getWp67EleMaxEstPoliCpiPr() {
        return this.wp67EleMaxEstPoliCpiPr;
    }

    public void setWp67TabEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp671.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp671.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp671.Len.Int.ID_PTF, 0));
        position += Lccvp671.Len.ID_PTF;
        lccvp671.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp67TabEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp671.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp671.getIdPtf(), Lccvp671.Len.Int.ID_PTF, 0);
        position += Lccvp671.Len.ID_PTF;
        lccvp671.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWp01AreaRichEstFormatted(String data) {
        byte[] buffer = new byte[Len.WP01_AREA_RICH_EST];
        MarshalByte.writeString(buffer, 1, data, Len.WP01_AREA_RICH_EST);
        setWp01AreaRichEstBytes(buffer, 1);
    }

    public void setWp01AreaRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        wp01EleMaxRichEst = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWp01TabRichEstBytes(buffer, position);
    }

    public byte[] getWp01AreaRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp01EleMaxRichEst);
        position += Types.SHORT_SIZE;
        getWp01TabRichEstBytes(buffer, position);
        return buffer;
    }

    public void setWp01EleMaxRichEst(short wp01EleMaxRichEst) {
        this.wp01EleMaxRichEst = wp01EleMaxRichEst;
    }

    public short getWp01EleMaxRichEst() {
        return this.wp01EleMaxRichEst;
    }

    public void setWp01TabRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp011.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp011.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp011.Len.Int.ID_PTF, 0));
        position += Lccvp011.Len.ID_PTF;
        lccvp011.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp01TabRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp011.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp011.getIdPtf(), Lccvp011.Len.Int.ID_PTF, 0);
        position += Lccvp011.Len.ID_PTF;
        lccvp011.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setIvvc0222AreaFndXTrancheFormatted(String data) {
        byte[] buffer = new byte[Len.IVVC0222_AREA_FND_X_TRANCHE];
        MarshalByte.writeString(buffer, 1, data, Len.IVVC0222_AREA_FND_X_TRANCHE);
        setIvvc0222AreaFndXTrancheBytes(buffer, 1);
    }

    public void setIvvc0222AreaFndXTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0222EleTrchMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= IVVC0222_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                ivvc0222TabTran.get(idx - 1).setIvvc0222TabTranBytes(buffer, position);
                position += Ivvc0222TabTran.Len.IVVC0222_TAB_TRAN;
            }
            else {
                Ivvc0222TabTran temp_ivvc0222TabTran = new Ivvc0222TabTran();
                temp_ivvc0222TabTran.initIvvc0222TabTranSpaces();
                getIvvc0222TabTranObj().fill(temp_ivvc0222TabTran);
                position += Ivvc0222TabTran.Len.IVVC0222_TAB_TRAN * (IVVC0222_TAB_TRAN_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public byte[] getIvvc0222AreaFndXTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ivvc0222EleTrchMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= IVVC0222_TAB_TRAN_MAXOCCURS; idx++) {
            ivvc0222TabTran.get(idx - 1).getIvvc0222TabTranBytes(buffer, position);
            position += Ivvc0222TabTran.Len.IVVC0222_TAB_TRAN;
        }
        return buffer;
    }

    public void setIvvc0222EleTrchMax(short ivvc0222EleTrchMax) {
        this.ivvc0222EleTrchMax = ivvc0222EleTrchMax;
    }

    public short getIvvc0222EleTrchMax() {
        return this.ivvc0222EleTrchMax;
    }

    public void setWp86AreaMotLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wp86EleMotLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP86_TAB_MOT_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wp86TabMotLiq[idx - 1].setWp86TabMotLiqBytes(buffer, position);
                position += Wp86TabMotLiq.Len.WP86_TAB_MOT_LIQ;
            }
            else {
                wp86TabMotLiq[idx - 1].initWp86TabMotLiqSpaces();
                position += Wp86TabMotLiq.Len.WP86_TAB_MOT_LIQ;
            }
        }
    }

    public byte[] getWp86AreaMotLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp86EleMotLiqMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP86_TAB_MOT_LIQ_MAXOCCURS; idx++) {
            wp86TabMotLiq[idx - 1].getWp86TabMotLiqBytes(buffer, position);
            position += Wp86TabMotLiq.Len.WP86_TAB_MOT_LIQ;
        }
        return buffer;
    }

    public void setWp86EleMotLiqMax(short wp86EleMotLiqMax) {
        this.wp86EleMotLiqMax = wp86EleMotLiqMax;
    }

    public short getWp86EleMotLiqMax() {
        return this.wp86EleMotLiqMax;
    }

    public void setAreaIvvc0223Formatted(String data) {
        byte[] buffer = new byte[Len.AREA_IVVC0223];
        MarshalByte.writeString(buffer, 1, data, Len.AREA_IVVC0223);
        setAreaIvvc0223Bytes(buffer, 1);
    }

    public void setAreaIvvc0223Bytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0223.setAreaVariabiliGarBytes(buffer, position);
    }

    public byte[] getAreaIvvc0223Bytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0223.getAreaVariabiliGarBytes(buffer, position);
        return buffer;
    }

    public void setWkMoviOrigFormatted(String wkMoviOrig) {
        this.wkMoviOrig = Trunc.toUnsignedNumeric(wkMoviOrig, Len.WK_MOVI_ORIG);
    }

    public int getWkMoviOrig() {
        return NumericDisplay.asInt(this.wkMoviOrig);
    }

    public void setWp88AreaServValFormatted(String data) {
        byte[] buffer = new byte[Len.WP88_AREA_SERV_VAL];
        MarshalByte.writeString(buffer, 1, data, Len.WP88_AREA_SERV_VAL);
        setWp88AreaServValBytes(buffer, 1);
    }

    public void setWp88AreaServValBytes(byte[] buffer, int offset) {
        int position = offset;
        wp88EleSerValMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP88_TAB_SERV_VAL_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wp88TabServVal[idx - 1].setWp88TabServValBytes(buffer, position);
                position += Wp88TabServVal.Len.WP88_TAB_SERV_VAL;
            }
            else {
                wp88TabServVal[idx - 1].initWp88TabServValSpaces();
                position += Wp88TabServVal.Len.WP88_TAB_SERV_VAL;
            }
        }
    }

    public byte[] getWp88AreaServValBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp88EleSerValMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP88_TAB_SERV_VAL_MAXOCCURS; idx++) {
            wp88TabServVal[idx - 1].getWp88TabServValBytes(buffer, position);
            position += Wp88TabServVal.Len.WP88_TAB_SERV_VAL;
        }
        return buffer;
    }

    public void setWp88EleSerValMax(short wp88EleSerValMax) {
        this.wp88EleSerValMax = wp88EleSerValMax;
    }

    public short getWp88EleSerValMax() {
        return this.wp88EleSerValMax;
    }

    public void setWp89AreaDservValFormatted(String data) {
        byte[] buffer = new byte[Len.WP89_AREA_DSERV_VAL];
        MarshalByte.writeString(buffer, 1, data, Len.WP89_AREA_DSERV_VAL);
        setWp89AreaDservValBytes(buffer, 1);
    }

    public void setWp89AreaDservValBytes(byte[] buffer, int offset) {
        int position = offset;
        wp89EleDservValMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP89_TAB_DSERV_VAL_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wp89TabDservVal[idx - 1].setWp89TabDservValBytes(buffer, position);
                position += Wp89TabDservVal.Len.WP89_TAB_DSERV_VAL;
            }
            else {
                wp89TabDservVal[idx - 1].initWp89TabDservValSpaces();
                position += Wp89TabDservVal.Len.WP89_TAB_DSERV_VAL;
            }
        }
    }

    public byte[] getWp89AreaDservValBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp89EleDservValMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP89_TAB_DSERV_VAL_MAXOCCURS; idx++) {
            wp89TabDservVal[idx - 1].getWp89TabDservValBytes(buffer, position);
            position += Wp89TabDservVal.Len.WP89_TAB_DSERV_VAL;
        }
        return buffer;
    }

    public void setWp89EleDservValMax(short wp89EleDservValMax) {
        this.wp89EleDservValMax = wp89EleDservValMax;
    }

    public short getWp89EleDservValMax() {
        return this.wp89EleDservValMax;
    }

    public void setWp56AreaQuestAdegVerFormatted(String data) {
        byte[] buffer = new byte[Len.WP56_AREA_QUEST_ADEG_VER];
        MarshalByte.writeString(buffer, 1, data, Len.WP56_AREA_QUEST_ADEG_VER);
        setWp56AreaQuestAdegVerBytes(buffer, 1);
    }

    public void setWp56AreaQuestAdegVerBytes(byte[] buffer, int offset) {
        int position = offset;
        wp56QuestAdegVerMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP56_TAB_QUEST_ADEG_VER_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wp56TabQuestAdegVer[idx - 1].setWp56TabQuestAdegVerBytes(buffer, position);
                position += Wp56TabQuestAdegVer.Len.WP56_TAB_QUEST_ADEG_VER;
            }
            else {
                wp56TabQuestAdegVer[idx - 1].initWp56TabQuestAdegVerSpaces();
                position += Wp56TabQuestAdegVer.Len.WP56_TAB_QUEST_ADEG_VER;
            }
        }
    }

    public byte[] getWp56AreaQuestAdegVerBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp56QuestAdegVerMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP56_TAB_QUEST_ADEG_VER_MAXOCCURS; idx++) {
            wp56TabQuestAdegVer[idx - 1].getWp56TabQuestAdegVerBytes(buffer, position);
            position += Wp56TabQuestAdegVer.Len.WP56_TAB_QUEST_ADEG_VER;
        }
        return buffer;
    }

    public void setWp56QuestAdegVerMax(short wp56QuestAdegVerMax) {
        this.wp56QuestAdegVerMax = wp56QuestAdegVerMax;
    }

    public short getWp56QuestAdegVerMax() {
        return this.wp56QuestAdegVerMax;
    }

    public void setWcdgAreaCommisGestBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0224.setAreaCommisGestCalcBytes(buffer, position);
        position += Ivvc0224.Len.AREA_COMMIS_GEST_CALC;
        ivvc0224.setAreaCommisGestVvBytes(buffer, position);
        position += Ivvc0224.Len.AREA_COMMIS_GEST_VV;
        ivvc0224.setAreaCommisGestOutBytes(buffer, position);
    }

    public byte[] getWcdgAreaCommisGestBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0224.getAreaCommisGestCalcBytes(buffer, position);
        position += Ivvc0224.Len.AREA_COMMIS_GEST_CALC;
        ivvc0224.getAreaCommisGestVvBytes(buffer, position);
        position += Ivvc0224.Len.AREA_COMMIS_GEST_VV;
        ivvc0224.getAreaCommisGestOutBytes(buffer, position);
        return buffer;
    }

    public Ivvc0212 getIvvc0212() {
        return ivvc0212;
    }

    public Ivvc0217 getIvvc0217() {
        return ivvc0217;
    }

    public LazyArrayCopy<Ivvc0222TabTran> getIvvc0222TabTranObj() {
        return ivvc0222TabTran;
    }

    public Ivvc0223 getIvvc0223() {
        return ivvc0223;
    }

    public Ivvc0224 getIvvc0224() {
        return ivvc0224;
    }

    public Lccvade1 getLccvade1() {
        return lccvade1;
    }

    public Lccvdad1 getLccvdad1() {
        return lccvdad1;
    }

    public Lccvdco1 getLccvdco1() {
        return lccvdco1;
    }

    public Lccvdfa1 getLccvdfa1() {
        return lccvdfa1;
    }

    public Lccvdfl1 getLccvdfl1() {
        return lccvdfl1;
    }

    public Lccviso1 getLccviso1() {
        return lccviso1;
    }

    public Lccvmov1 getLccvmov1() {
        return lccvmov1;
    }

    public Lccvp011 getLccvp011() {
        return lccvp011;
    }

    public Lccvp611 getLccvp611() {
        return lccvp611;
    }

    public Lccvp671 getLccvp671() {
        return lccvp671;
    }

    public Lccvpol1 getLccvpol1() {
        return lccvpol1;
    }

    public Lccvric1 getLccvric1() {
        return lccvric1;
    }

    public Lccvtcl1 getLccvtcl1() {
        return lccvtcl1;
    }

    public WbelTabBeneLiq getWbelTabBeneLiq(int idx) {
        return wbelTabBeneLiq[idx - 1];
    }

    public WbepTabBeneficiari getWbepTabBeneficiari(int idx) {
        return wbepTabBeneficiari[idx - 1];
    }

    public WcltTabClauTxt getWcltTabClauTxt(int idx) {
        return wcltTabClauTxt[idx - 1];
    }

    public WdeqAreaDettQuest getWdeqAreaDettQuest() {
        return wdeqAreaDettQuest;
    }

    public WdtcTabDtc getWdtcTabDtc(int idx) {
        return wdtcTabDtc[idx - 1];
    }

    public We15TabE15 getWe15TabE15(int idx) {
        return we15TabE15[idx - 1];
    }

    public WgrzTabGar getWgopTabGar(int idx) {
        return wgopTabGar[idx - 1];
    }

    public WgrlTabGarLiq getWgrlTabGarLiq(int idx) {
        return wgrlTabGarLiq[idx - 1];
    }

    public WgrzTabGar getWgrzTabGar(int idx) {
        return wgrzTabGar[idx - 1];
    }

    public Wl19AreaQuote getWl19AreaQuote() {
        return wl19AreaQuote;
    }

    public Wl23TabVincPeg getWl23TabVincPeg(int idx) {
        return wl23TabVincPeg[idx - 1];
    }

    public Wl30TabReinvstPoli getWl30TabReinvstPoli(int idx) {
        return wl30TabReinvstPoli[idx - 1];
    }

    public S089TabLiq getWlquTabLiq(int idx) {
        return wlquTabLiq[idx - 1];
    }

    public WmfzTabMoviFinrio getWmfzTabMoviFinrio(int idx) {
        return wmfzTabMoviFinrio[idx - 1];
    }

    public WocoAreaOggColl getWocoAreaOggColl() {
        return wocoAreaOggColl;
    }

    public Wp58TabImpstBol getWp58TabImpstBol(int idx) {
        return wp58TabImpstBol[idx - 1];
    }

    public WpliTabPercLiq getWpliTabPercLiq(int idx) {
        return wpliTabPercLiq[idx - 1];
    }

    public WpmoAreaParamMov getWpmoAreaParamMov() {
        return wpmoAreaParamMov;
    }

    public WpogAreaParamOgg getWpogAreaParamOgg() {
        return wpogAreaParamOgg;
    }

    public WpreTabPrestiti getWpreTabPrestiti(int idx) {
        return wpreTabPrestiti[idx - 1];
    }

    public WpvtTabProvTr getWpvtTabProvTr(int idx) {
        return wpvtTabProvTr[idx - 1];
    }

    public WqueTabQuest getWqueTabQuest(int idx) {
        return wqueTabQuest[idx - 1];
    }

    public WranTabRappAnag getWranTabRappAnag(int idx) {
        return wranTabRappAnag[idx - 1];
    }

    public WrdfAreaRichDisinvFnd getWrdfAreaRichDisinvFnd() {
        return wrdfAreaRichDisinvFnd;
    }

    public WrifAreaRichInvFnd getWrifAreaRichInvFnd() {
        return wrifAreaRichInvFnd;
    }

    public WrreTabRappRete getWrreTabRappRete(int idx) {
        return wrreTabRappRete[idx - 1];
    }

    public WrstTabRst getWrstTabRst(int idx) {
        return wrstTabRst[idx - 1];
    }

    public WsdiTabStraInv getWsdiTabStraInv(int idx) {
        return wsdiTabStraInv[idx - 1];
    }

    public WspgTabSpg getWspgTabSpg(int idx) {
        return wspgTabSpg[idx - 1];
    }

    public WtgaAreaTrancheIvvs0211 getWtgaAreaTranche() {
        return wtgaAreaTranche;
    }

    public WtitTabTitCont getWtitTabTitCont(int idx) {
        return wtitTabTitCont[idx - 1];
    }

    public WtliAreaTrchLiq getWtliAreaTrchLiq() {
        return wtliAreaTrchLiq;
    }

    public WtgaTabTran getWtopTabTran(int idx) {
        return wtopTabTran[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getAreaBusinessBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_ELE_ADES_MAX = 2;
        public static final int WADE_TAB_ADES = WpolStatus.Len.STATUS + Lccvade1.Len.ID_PTF + WadeDati.Len.DATI;
        public static final int WADE_AREA_ADESIONE = WADE_ELE_ADES_MAX + WADE_TAB_ADES;
        public static final int WBEP_ELE_BENEF_MAX = 2;
        public static final int WBEP_AREA_BENEF = WBEP_ELE_BENEF_MAX + AreaBusinessIvvs0211.WBEP_TAB_BENEFICIARI_MAXOCCURS * WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
        public static final int WBEL_ELE_BENEF_LIQ_MAX = 2;
        public static final int WBEL_AREA_BENEF_LIQ = WBEL_ELE_BENEF_LIQ_MAX + AreaBusinessIvvs0211.WBEL_TAB_BENE_LIQ_MAXOCCURS * WbelTabBeneLiq.Len.WBEL_TAB_BENE_LIQ;
        public static final int WDCO_ELE_COLL_MAX = 2;
        public static final int WDCO_TAB_COLL = WpolStatus.Len.STATUS + Lccvdco1.Len.ID_PTF + WdcoDati.Len.DATI;
        public static final int WDCO_AREA_DT_COLLETTIVA = WDCO_ELE_COLL_MAX + WDCO_TAB_COLL;
        public static final int WDFA_ELE_FISC_ADES_MAX = 2;
        public static final int WDFA_TAB_FISC_ADES = WpolStatus.Len.STATUS + Lccvdfa1.Len.ID_PTF + WdfaDati.Len.DATI;
        public static final int WDFA_AREA_DT_FISC_ADES = WDFA_ELE_FISC_ADES_MAX + WDFA_TAB_FISC_ADES;
        public static final int WDTC_ELE_DETT_TIT_MAX = 2;
        public static final int WDTC_AREA_DETT_TIT_CONT = WDTC_ELE_DETT_TIT_MAX + AreaBusinessIvvs0211.WDTC_TAB_DTC_MAXOCCURS * WdtcTabDtc.Len.WDTC_TAB_DTC;
        public static final int WGRZ_ELE_GARANZIA_MAX = 2;
        public static final int WGRZ_AREA_GARANZIA = WGRZ_ELE_GARANZIA_MAX + AreaBusinessIvvs0211.WGRZ_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int WGRL_ELE_GAR_LIQ_MAX = 2;
        public static final int WGRL_AREA_GARANZIA_LIQ = WGRL_ELE_GAR_LIQ_MAX + AreaBusinessIvvs0211.WGRL_TAB_GAR_LIQ_MAXOCCURS * WgrlTabGarLiq.Len.WGRL_TAB_GAR_LIQ;
        public static final int WISO_ELE_IMP_SOST_MAX = 2;
        public static final int WISO_TAB_IMP_SOST = WpolStatus.Len.STATUS + Lccviso1.Len.ID_PTF + WisoDati.Len.DATI;
        public static final int WISO_AREA_IMPOSTA_SOST = WISO_ELE_IMP_SOST_MAX + WISO_TAB_IMP_SOST;
        public static final int WLQU_ELE_LIQ_MAX = 2;
        public static final int WLQU_AREA_LIQUIDAZIONE = WLQU_ELE_LIQ_MAX + AreaBusinessIvvs0211.WLQU_TAB_LIQ_MAXOCCURS * S089TabLiq.Len.WLQU_TAB_LIQ;
        public static final int WMOV_ELE_MOVI_MAX = 2;
        public static final int WMOV_TAB_MOVI = WpolStatus.Len.STATUS + Lccvmov1.Len.ID_PTF + WmovDati.Len.DATI;
        public static final int WMOV_AREA_MOVIMENTO = WMOV_ELE_MOVI_MAX + WMOV_TAB_MOVI;
        public static final int WMFZ_ELE_MOVI_FINRIO_MAX = 2;
        public static final int WMFZ_AREA_MOVI_FINRIO = WMFZ_ELE_MOVI_FINRIO_MAX + AreaBusinessIvvs0211.WMFZ_TAB_MOVI_FINRIO_MAXOCCURS * WmfzTabMoviFinrio.Len.WMFZ_TAB_MOVI_FINRIO;
        public static final int WPLI_ELE_PERC_LIQ_MAX = 2;
        public static final int WPLI_AREA_PERC_LIQ = WPLI_ELE_PERC_LIQ_MAX + AreaBusinessIvvs0211.WPLI_TAB_PERC_LIQ_MAXOCCURS * WpliTabPercLiq.Len.WPLI_TAB_PERC_LIQ;
        public static final int WPOL_ELE_POLI_MAX = 2;
        public static final int WPOL_TAB_POLI = WpolStatus.Len.STATUS + Lccvpol1.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int WPOL_AREA_POLIZZA = WPOL_ELE_POLI_MAX + WPOL_TAB_POLI;
        public static final int WPRE_ELE_PRESTITI_MAX = 2;
        public static final int WPRE_AREA_PRESTITI = WPRE_ELE_PRESTITI_MAX + AreaBusinessIvvs0211.WPRE_TAB_PRESTITI_MAXOCCURS * WpreTabPrestiti.Len.WPRE_TAB_PRESTITI;
        public static final int WPVT_ELE_PROV_MAX = 2;
        public static final int WPVT_AREA_PROV = WPVT_ELE_PROV_MAX + AreaBusinessIvvs0211.WPVT_TAB_PROV_TR_MAXOCCURS * WpvtTabProvTr.Len.WPVT_TAB_PROV_TR;
        public static final int WQUE_ELE_QUEST_MAX = 2;
        public static final int WQUE_AREA_QUEST = WQUE_ELE_QUEST_MAX + AreaBusinessIvvs0211.WQUE_TAB_QUEST_MAXOCCURS * WqueTabQuest.Len.WQUE_TAB_QUEST;
        public static final int WRIC_ELE_RICH_MAX = 2;
        public static final int WRIC_TAB_RICH = WpolStatus.Len.STATUS + Lccvric1.Len.ID_PTF + WricDati.Len.DATI;
        public static final int WRIC_AREA_RICH = WRIC_ELE_RICH_MAX + WRIC_TAB_RICH;
        public static final int WRAN_ELE_RAPP_ANAG_MAX = 2;
        public static final int WRAN_AREA_RAPP_ANAG = WRAN_ELE_RAPP_ANAG_MAX + AreaBusinessIvvs0211.WRAN_TAB_RAPP_ANAG_MAXOCCURS * WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
        public static final int WE15_ELE_EST_RAPP_ANAG_MAX = 2;
        public static final int WE15_AREA_EST_RAPP_ANAG = WE15_ELE_EST_RAPP_ANAG_MAX + AreaBusinessIvvs0211.WE15_TAB_E15_MAXOCCURS * We15TabE15.Len.WE15_TAB_E15;
        public static final int WRRE_ELE_RAPP_RETE_MAX = 2;
        public static final int WRRE_AREA_RAPP_RETE = WRRE_ELE_RAPP_RETE_MAX + AreaBusinessIvvs0211.WRRE_TAB_RAPP_RETE_MAXOCCURS * WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
        public static final int WSPG_ELE_SOPRAP_GAR_MAX = 2;
        public static final int WSPG_AREA_SOPRAP_GAR = WSPG_ELE_SOPRAP_GAR_MAX + AreaBusinessIvvs0211.WSPG_TAB_SPG_MAXOCCURS * WspgTabSpg.Len.WSPG_TAB_SPG;
        public static final int WSDI_ELE_STRA_INV_MAX = 2;
        public static final int WSDI_AREA_STRA_INV = WSDI_ELE_STRA_INV_MAX + AreaBusinessIvvs0211.WSDI_TAB_STRA_INV_MAXOCCURS * WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
        public static final int WTIT_ELE_TIT_CONT_MAX = 2;
        public static final int WTIT_AREA_TIT_CONT = WTIT_ELE_TIT_CONT_MAX + AreaBusinessIvvs0211.WTIT_TAB_TIT_CONT_MAXOCCURS * WtitTabTitCont.Len.WTIT_TAB_TIT_CONT;
        public static final int WTCL_ELE_TIT_LIQ_MAX = 2;
        public static final int WTCL_TAB_TIT_LIQ = WpolStatus.Len.STATUS + Lccvtcl1.Len.ID_PTF + WtclDati.Len.DATI;
        public static final int WTCL_AREA_TIT_LIQ = WTCL_ELE_TIT_LIQ_MAX + WTCL_TAB_TIT_LIQ;
        public static final int WDAD_ELE_DFLT_ADES_MAX = 2;
        public static final int WDAD_TAB_DFLT_ADES = WpolStatus.Len.STATUS + Lccvdad1.Len.ID_PTF + WdadDati.Len.DATI;
        public static final int WDAD_AREA_DEFAULT_ADES = WDAD_ELE_DFLT_ADES_MAX + WDAD_TAB_DFLT_ADES;
        public static final int WDFL_ELE_DFL_MAX = 2;
        public static final int WDFL_TAB_DFL = WpolStatus.Len.STATUS + Lccvdfl1.Len.ID_PTF + WdflDati.Len.DATI;
        public static final int WDFL_AREA_DFL = WDFL_ELE_DFL_MAX + WDFL_TAB_DFL;
        public static final int WRST_ELE_RST_MAX = 2;
        public static final int WRST_AREA_RST = WRST_ELE_RST_MAX + AreaBusinessIvvs0211.WRST_TAB_RST_MAXOCCURS * WrstTabRst.Len.WRST_TAB_RST;
        public static final int WL30_ELE_REINVST_POLI_MAX = 2;
        public static final int WL30_AREA_REINVST_POLI = WL30_ELE_REINVST_POLI_MAX + AreaBusinessIvvs0211.WL30_TAB_REINVST_POLI_MAXOCCURS * Wl30TabReinvstPoli.Len.WL30_TAB_REINVST_POLI;
        public static final int WL23_ELE_VINC_PEG_MAX = 2;
        public static final int WL23_AREA_VINC_PEG = WL23_ELE_VINC_PEG_MAX + AreaBusinessIvvs0211.WL23_TAB_VINC_PEG_MAXOCCURS * Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;
        public static final int WOPZ_AREA_OPZIONI = Ivvc0217.Len.ELE_MAX_OPZIONI + Ivvc0217.OPZIONI_MAXOCCURS * WopzOpzioni.Len.OPZIONI;
        public static final int WGOP_ELE_GARANZIA_OPZ_MAX = 2;
        public static final int WGOP_AREA_GARANZIA_OPZ = WGOP_ELE_GARANZIA_OPZ_MAX + AreaBusinessIvvs0211.WGOP_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int WTOP_ELE_TRAN_OPZ_MAX = 2;
        public static final int WTOP_AREA_TRANCHE_OPZ = WTOP_ELE_TRAN_OPZ_MAX + AreaBusinessIvvs0211.WTOP_TAB_TRAN_MAXOCCURS * WtgaTabTran.Len.TAB_TRAN;
        public static final int WCNT_AREA_DATI_CONTEST = Ivvc0212.Len.WCNT_AREA_VARIABILI_CONT;
        public static final int WCLT_ELE_CLAU_TXT_MAX = 2;
        public static final int WCLT_AREA_CLAU_TXT = WCLT_ELE_CLAU_TXT_MAX + AreaBusinessIvvs0211.WCLT_TAB_CLAU_TXT_MAXOCCURS * WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;
        public static final int WP58_ELE_MAX_IMPST_BOL = 2;
        public static final int WP58_AREA_IMPOSTA_BOLLO = WP58_ELE_MAX_IMPST_BOL + AreaBusinessIvvs0211.WP58_TAB_IMPST_BOL_MAXOCCURS * Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
        public static final int WP61_ELE_MAX_D_CRIST = 2;
        public static final int WP61_TAB_D_CRIST = WpolStatus.Len.STATUS + Lccvp611.Len.ID_PTF + Wp61Dati.Len.DATI;
        public static final int WP61_AREA_D_CRIST = WP61_ELE_MAX_D_CRIST + WP61_TAB_D_CRIST;
        public static final int WP67_ELE_MAX_EST_POLI_CPI_PR = 2;
        public static final int WP67_TAB_EST_POLI_CPI_PR = WpolStatus.Len.STATUS + Lccvp671.Len.ID_PTF + Wp67Dati.Len.DATI;
        public static final int WP67_AREA_EST_POLI_CPI_PR = WP67_ELE_MAX_EST_POLI_CPI_PR + WP67_TAB_EST_POLI_CPI_PR;
        public static final int WP01_ELE_MAX_RICH_EST = 2;
        public static final int WP01_TAB_RICH_EST = WpolStatus.Len.STATUS + Lccvp011.Len.ID_PTF + Wp01Dati.Len.DATI;
        public static final int WP01_AREA_RICH_EST = WP01_ELE_MAX_RICH_EST + WP01_TAB_RICH_EST;
        public static final int IVVC0222_ELE_TRCH_MAX = 2;
        public static final int IVVC0222_AREA_FND_X_TRANCHE = IVVC0222_ELE_TRCH_MAX + AreaBusinessIvvs0211.IVVC0222_TAB_TRAN_MAXOCCURS * Ivvc0222TabTran.Len.IVVC0222_TAB_TRAN;
        public static final int WP86_ELE_MOT_LIQ_MAX = 2;
        public static final int WP86_AREA_MOT_LIQ = WP86_ELE_MOT_LIQ_MAX + AreaBusinessIvvs0211.WP86_TAB_MOT_LIQ_MAXOCCURS * Wp86TabMotLiq.Len.WP86_TAB_MOT_LIQ;
        public static final int AREA_IVVC0223 = Ivvc0223.Len.AREA_VARIABILI_GAR;
        public static final int WK_MOVI_ORIG = 5;
        public static final int WP88_ELE_SER_VAL_MAX = 2;
        public static final int WP88_AREA_SERV_VAL = WP88_ELE_SER_VAL_MAX + AreaBusinessIvvs0211.WP88_TAB_SERV_VAL_MAXOCCURS * Wp88TabServVal.Len.WP88_TAB_SERV_VAL;
        public static final int WP89_ELE_DSERV_VAL_MAX = 2;
        public static final int WP89_AREA_DSERV_VAL = WP89_ELE_DSERV_VAL_MAX + AreaBusinessIvvs0211.WP89_TAB_DSERV_VAL_MAXOCCURS * Wp89TabDservVal.Len.WP89_TAB_DSERV_VAL;
        public static final int WP56_QUEST_ADEG_VER_MAX = 2;
        public static final int WP56_AREA_QUEST_ADEG_VER = WP56_QUEST_ADEG_VER_MAX + AreaBusinessIvvs0211.WP56_TAB_QUEST_ADEG_VER_MAXOCCURS * Wp56TabQuestAdegVer.Len.WP56_TAB_QUEST_ADEG_VER;
        public static final int WCDG_AREA_COMMIS_GEST = Ivvc0224.Len.AREA_COMMIS_GEST_CALC + Ivvc0224.Len.AREA_COMMIS_GEST_VV + Ivvc0224.Len.AREA_COMMIS_GEST_OUT;
        public static final int AREA_BUSINESS = WADE_AREA_ADESIONE + WBEP_AREA_BENEF + WBEL_AREA_BENEF_LIQ + WDCO_AREA_DT_COLLETTIVA + WDFA_AREA_DT_FISC_ADES + WdeqAreaDettQuest.Len.WDEQ_AREA_DETT_QUEST + WDTC_AREA_DETT_TIT_CONT + WGRZ_AREA_GARANZIA + WGRL_AREA_GARANZIA_LIQ + WISO_AREA_IMPOSTA_SOST + WLQU_AREA_LIQUIDAZIONE + WMOV_AREA_MOVIMENTO + WMFZ_AREA_MOVI_FINRIO + WpogAreaParamOgg.Len.WPOG_AREA_PARAM_OGG + WpmoAreaParamMov.Len.WPMO_AREA_PARAM_MOV + WPLI_AREA_PERC_LIQ + WPOL_AREA_POLIZZA + WPRE_AREA_PRESTITI + WPVT_AREA_PROV + WQUE_AREA_QUEST + WRIC_AREA_RICH + WrdfAreaRichDisinvFnd.Len.WRDF_AREA_RICH_DISINV_FND + WrifAreaRichInvFnd.Len.WRIF_AREA_RICH_INV_FND + WRAN_AREA_RAPP_ANAG + WE15_AREA_EST_RAPP_ANAG + WRRE_AREA_RAPP_RETE + WSPG_AREA_SOPRAP_GAR + WSDI_AREA_STRA_INV + WTIT_AREA_TIT_CONT + WTCL_AREA_TIT_LIQ + WtgaAreaTrancheIvvs0211.Len.WTGA_AREA_TRANCHE + WtliAreaTrchLiq.Len.WTLI_AREA_TRCH_LIQ + WDAD_AREA_DEFAULT_ADES + WocoAreaOggColl.Len.WOCO_AREA_OGG_COLL + WDFL_AREA_DFL + WRST_AREA_RST + Wl19AreaQuote.Len.WL19_AREA_QUOTE + WL30_AREA_REINVST_POLI + WL23_AREA_VINC_PEG + WOPZ_AREA_OPZIONI + WGOP_AREA_GARANZIA_OPZ + WTOP_AREA_TRANCHE_OPZ + WCNT_AREA_DATI_CONTEST + WCLT_AREA_CLAU_TXT + WP58_AREA_IMPOSTA_BOLLO + WP61_AREA_D_CRIST + WP67_AREA_EST_POLI_CPI_PR + WP01_AREA_RICH_EST + IVVC0222_AREA_FND_X_TRANCHE + WP86_AREA_MOT_LIQ + AREA_IVVC0223 + WK_MOVI_ORIG + WP88_AREA_SERV_VAL + WP89_AREA_DSERV_VAL + WP56_AREA_QUEST_ADEG_VER + WCDG_AREA_COMMIS_GEST;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
