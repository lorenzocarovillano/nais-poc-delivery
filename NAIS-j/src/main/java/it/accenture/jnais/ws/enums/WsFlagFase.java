package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-FLAG-FASE<br>
 * Variable: WS-FLAG-FASE from program IABS0130<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFlagFase {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WS_FLAG_FASE);
    public static final String CONTROLLO_INIZIALE = "CI";
    public static final String CONTROLLO_FINALE = "CF";
    public static final String INIZIALE = "FI";
    public static final String FINALE = "FF";

    //==== METHODS ====
    public void setWsFlagFase(String wsFlagFase) {
        this.value = Functions.subString(wsFlagFase, Len.WS_FLAG_FASE);
    }

    public String getWsFlagFase() {
        return this.value;
    }

    public void setWsFaseControlloIniziale() {
        value = CONTROLLO_INIZIALE;
    }

    public void setWsFaseControlloFinale() {
        value = CONTROLLO_FINALE;
    }

    public void setWsFaseIniziale() {
        value = INIZIALE;
    }

    public void setWsFaseFinale() {
        value = FINALE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_FLAG_FASE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
