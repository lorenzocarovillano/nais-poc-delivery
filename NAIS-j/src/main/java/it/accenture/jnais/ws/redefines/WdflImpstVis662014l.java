package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-VIS-662014L<br>
 * Variable: WDFL-IMPST-VIS-662014L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstVis662014l extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstVis662014l() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_VIS662014L;
    }

    public void setWdflImpstVis662014l(AfDecimal wdflImpstVis662014l) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_VIS662014L, wdflImpstVis662014l.copy());
    }

    public void setWdflImpstVis662014lFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_VIS662014L, Pos.WDFL_IMPST_VIS662014L);
    }

    /**Original name: WDFL-IMPST-VIS-662014L<br>*/
    public AfDecimal getWdflImpstVis662014l() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_VIS662014L, Len.Int.WDFL_IMPST_VIS662014L, Len.Fract.WDFL_IMPST_VIS662014L);
    }

    public byte[] getWdflImpstVis662014lAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_VIS662014L, Pos.WDFL_IMPST_VIS662014L);
        return buffer;
    }

    public void setWdflImpstVis662014lNull(String wdflImpstVis662014lNull) {
        writeString(Pos.WDFL_IMPST_VIS662014L_NULL, wdflImpstVis662014lNull, Len.WDFL_IMPST_VIS662014L_NULL);
    }

    /**Original name: WDFL-IMPST-VIS-662014L-NULL<br>*/
    public String getWdflImpstVis662014lNull() {
        return readString(Pos.WDFL_IMPST_VIS662014L_NULL, Len.WDFL_IMPST_VIS662014L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS662014L = 1;
        public static final int WDFL_IMPST_VIS662014L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS662014L = 8;
        public static final int WDFL_IMPST_VIS662014L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS662014L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS662014L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
