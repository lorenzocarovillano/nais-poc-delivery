package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-AA-CNBZ-K3<br>
 * Variable: WDFA-AA-CNBZ-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaAaCnbzK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaAaCnbzK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_AA_CNBZ_K3;
    }

    public void setWdfaAaCnbzK3(short wdfaAaCnbzK3) {
        writeShortAsPacked(Pos.WDFA_AA_CNBZ_K3, wdfaAaCnbzK3, Len.Int.WDFA_AA_CNBZ_K3);
    }

    public void setWdfaAaCnbzK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_AA_CNBZ_K3, Pos.WDFA_AA_CNBZ_K3);
    }

    /**Original name: WDFA-AA-CNBZ-K3<br>*/
    public short getWdfaAaCnbzK3() {
        return readPackedAsShort(Pos.WDFA_AA_CNBZ_K3, Len.Int.WDFA_AA_CNBZ_K3);
    }

    public byte[] getWdfaAaCnbzK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_AA_CNBZ_K3, Pos.WDFA_AA_CNBZ_K3);
        return buffer;
    }

    public void setWdfaAaCnbzK3Null(String wdfaAaCnbzK3Null) {
        writeString(Pos.WDFA_AA_CNBZ_K3_NULL, wdfaAaCnbzK3Null, Len.WDFA_AA_CNBZ_K3_NULL);
    }

    /**Original name: WDFA-AA-CNBZ-K3-NULL<br>*/
    public String getWdfaAaCnbzK3Null() {
        return readString(Pos.WDFA_AA_CNBZ_K3_NULL, Len.WDFA_AA_CNBZ_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_AA_CNBZ_K3 = 1;
        public static final int WDFA_AA_CNBZ_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_AA_CNBZ_K3 = 3;
        public static final int WDFA_AA_CNBZ_K3_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_AA_CNBZ_K3 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
