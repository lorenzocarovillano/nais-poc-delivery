package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-ACQ-2O-ANNO<br>
 * Variable: WPAG-RATA-ACQ-2O-ANNO from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataAcq2oAnno extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataAcq2oAnno() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_ACQ2O_ANNO;
    }

    public void setWpagRataAcq2oAnno(AfDecimal wpagRataAcq2oAnno) {
        writeDecimalAsPacked(Pos.WPAG_RATA_ACQ2O_ANNO, wpagRataAcq2oAnno.copy());
    }

    public void setWpagRataAcq2oAnnoFormatted(String wpagRataAcq2oAnno) {
        setWpagRataAcq2oAnno(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_ACQ2O_ANNO + Len.Fract.WPAG_RATA_ACQ2O_ANNO, Len.Fract.WPAG_RATA_ACQ2O_ANNO, wpagRataAcq2oAnno));
    }

    public void setWpagRataAcq2oAnnoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_ACQ2O_ANNO, Pos.WPAG_RATA_ACQ2O_ANNO);
    }

    /**Original name: WPAG-RATA-ACQ-2O-ANNO<br>*/
    public AfDecimal getWpagRataAcq2oAnno() {
        return readPackedAsDecimal(Pos.WPAG_RATA_ACQ2O_ANNO, Len.Int.WPAG_RATA_ACQ2O_ANNO, Len.Fract.WPAG_RATA_ACQ2O_ANNO);
    }

    public byte[] getWpagRataAcq2oAnnoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_ACQ2O_ANNO, Pos.WPAG_RATA_ACQ2O_ANNO);
        return buffer;
    }

    public void initWpagRataAcq2oAnnoSpaces() {
        fill(Pos.WPAG_RATA_ACQ2O_ANNO, Len.WPAG_RATA_ACQ2O_ANNO, Types.SPACE_CHAR);
    }

    public void setWpagRataAcq2oAnnoNull(String wpagRataAcq2oAnnoNull) {
        writeString(Pos.WPAG_RATA_ACQ2O_ANNO_NULL, wpagRataAcq2oAnnoNull, Len.WPAG_RATA_ACQ2O_ANNO_NULL);
    }

    /**Original name: WPAG-RATA-ACQ-2O-ANNO-NULL<br>*/
    public String getWpagRataAcq2oAnnoNull() {
        return readString(Pos.WPAG_RATA_ACQ2O_ANNO_NULL, Len.WPAG_RATA_ACQ2O_ANNO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_ACQ2O_ANNO = 1;
        public static final int WPAG_RATA_ACQ2O_ANNO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_ACQ2O_ANNO = 8;
        public static final int WPAG_RATA_ACQ2O_ANNO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_ACQ2O_ANNO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_ACQ2O_ANNO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
