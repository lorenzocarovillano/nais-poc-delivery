package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B01-ID-RICH-ESTRAZ-AGG<br>
 * Variable: W-B01-ID-RICH-ESTRAZ-AGG from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB01IdRichEstrazAgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB01IdRichEstrazAgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B01_ID_RICH_ESTRAZ_AGG;
    }

    public void setwB01IdRichEstrazAgg(int wB01IdRichEstrazAgg) {
        writeIntAsPacked(Pos.W_B01_ID_RICH_ESTRAZ_AGG, wB01IdRichEstrazAgg, Len.Int.W_B01_ID_RICH_ESTRAZ_AGG);
    }

    /**Original name: W-B01-ID-RICH-ESTRAZ-AGG<br>*/
    public int getwB01IdRichEstrazAgg() {
        return readPackedAsInt(Pos.W_B01_ID_RICH_ESTRAZ_AGG, Len.Int.W_B01_ID_RICH_ESTRAZ_AGG);
    }

    public byte[] getwB01IdRichEstrazAggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B01_ID_RICH_ESTRAZ_AGG, Pos.W_B01_ID_RICH_ESTRAZ_AGG);
        return buffer;
    }

    public void setwB01IdRichEstrazAggNull(String wB01IdRichEstrazAggNull) {
        writeString(Pos.W_B01_ID_RICH_ESTRAZ_AGG_NULL, wB01IdRichEstrazAggNull, Len.W_B01_ID_RICH_ESTRAZ_AGG_NULL);
    }

    /**Original name: W-B01-ID-RICH-ESTRAZ-AGG-NULL<br>*/
    public String getwB01IdRichEstrazAggNull() {
        return readString(Pos.W_B01_ID_RICH_ESTRAZ_AGG_NULL, Len.W_B01_ID_RICH_ESTRAZ_AGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B01_ID_RICH_ESTRAZ_AGG = 1;
        public static final int W_B01_ID_RICH_ESTRAZ_AGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B01_ID_RICH_ESTRAZ_AGG = 5;
        public static final int W_B01_ID_RICH_ESTRAZ_AGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B01_ID_RICH_ESTRAZ_AGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
