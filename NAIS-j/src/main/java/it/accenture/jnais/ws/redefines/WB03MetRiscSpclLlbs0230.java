package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: W-B03-MET-RISC-SPCL<br>
 * Variable: W-B03-MET-RISC-SPCL from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03MetRiscSpclLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03MetRiscSpclLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_MET_RISC_SPCL;
    }

    public void setwB03MetRiscSpcl(short wB03MetRiscSpcl) {
        writeShortAsPacked(Pos.W_B03_MET_RISC_SPCL, wB03MetRiscSpcl, Len.Int.W_B03_MET_RISC_SPCL);
    }

    /**Original name: W-B03-MET-RISC-SPCL<br>*/
    public short getwB03MetRiscSpcl() {
        return readPackedAsShort(Pos.W_B03_MET_RISC_SPCL, Len.Int.W_B03_MET_RISC_SPCL);
    }

    public byte[] getwB03MetRiscSpclAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_MET_RISC_SPCL, Pos.W_B03_MET_RISC_SPCL);
        return buffer;
    }

    public void setwB03MetRiscSpclNull(char wB03MetRiscSpclNull) {
        writeChar(Pos.W_B03_MET_RISC_SPCL_NULL, wB03MetRiscSpclNull);
    }

    /**Original name: W-B03-MET-RISC-SPCL-NULL<br>*/
    public char getwB03MetRiscSpclNull() {
        return readChar(Pos.W_B03_MET_RISC_SPCL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_MET_RISC_SPCL = 1;
        public static final int W_B03_MET_RISC_SPCL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_MET_RISC_SPCL = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_MET_RISC_SPCL = 1;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
