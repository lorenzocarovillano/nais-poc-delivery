package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDCO-DUR-AA-ADES-DFLT<br>
 * Variable: WDCO-DUR-AA-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdcoDurAaAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdcoDurAaAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_DUR_AA_ADES_DFLT;
    }

    public void setWdcoDurAaAdesDflt(int wdcoDurAaAdesDflt) {
        writeIntAsPacked(Pos.WDCO_DUR_AA_ADES_DFLT, wdcoDurAaAdesDflt, Len.Int.WDCO_DUR_AA_ADES_DFLT);
    }

    public void setWdcoDurAaAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDCO_DUR_AA_ADES_DFLT, Pos.WDCO_DUR_AA_ADES_DFLT);
    }

    /**Original name: WDCO-DUR-AA-ADES-DFLT<br>*/
    public int getWdcoDurAaAdesDflt() {
        return readPackedAsInt(Pos.WDCO_DUR_AA_ADES_DFLT, Len.Int.WDCO_DUR_AA_ADES_DFLT);
    }

    public byte[] getWdcoDurAaAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDCO_DUR_AA_ADES_DFLT, Pos.WDCO_DUR_AA_ADES_DFLT);
        return buffer;
    }

    public void setWdcoDurAaAdesDfltNull(String wdcoDurAaAdesDfltNull) {
        writeString(Pos.WDCO_DUR_AA_ADES_DFLT_NULL, wdcoDurAaAdesDfltNull, Len.WDCO_DUR_AA_ADES_DFLT_NULL);
    }

    /**Original name: WDCO-DUR-AA-ADES-DFLT-NULL<br>*/
    public String getWdcoDurAaAdesDfltNull() {
        return readString(Pos.WDCO_DUR_AA_ADES_DFLT_NULL, Len.WDCO_DUR_AA_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDCO_DUR_AA_ADES_DFLT = 1;
        public static final int WDCO_DUR_AA_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_DUR_AA_ADES_DFLT = 3;
        public static final int WDCO_DUR_AA_ADES_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_DUR_AA_ADES_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
