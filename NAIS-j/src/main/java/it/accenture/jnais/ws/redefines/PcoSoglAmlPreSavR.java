package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-SOGL-AML-PRE-SAV-R<br>
 * Variable: PCO-SOGL-AML-PRE-SAV-R from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoSoglAmlPreSavR extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoSoglAmlPreSavR() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_SOGL_AML_PRE_SAV_R;
    }

    public void setPcoSoglAmlPreSavR(AfDecimal pcoSoglAmlPreSavR) {
        writeDecimalAsPacked(Pos.PCO_SOGL_AML_PRE_SAV_R, pcoSoglAmlPreSavR.copy());
    }

    public void setPcoSoglAmlPreSavRFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_SOGL_AML_PRE_SAV_R, Pos.PCO_SOGL_AML_PRE_SAV_R);
    }

    /**Original name: PCO-SOGL-AML-PRE-SAV-R<br>*/
    public AfDecimal getPcoSoglAmlPreSavR() {
        return readPackedAsDecimal(Pos.PCO_SOGL_AML_PRE_SAV_R, Len.Int.PCO_SOGL_AML_PRE_SAV_R, Len.Fract.PCO_SOGL_AML_PRE_SAV_R);
    }

    public byte[] getPcoSoglAmlPreSavRAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_SOGL_AML_PRE_SAV_R, Pos.PCO_SOGL_AML_PRE_SAV_R);
        return buffer;
    }

    public void initPcoSoglAmlPreSavRHighValues() {
        fill(Pos.PCO_SOGL_AML_PRE_SAV_R, Len.PCO_SOGL_AML_PRE_SAV_R, Types.HIGH_CHAR_VAL);
    }

    public void setPcoSoglAmlPreSavRNull(String pcoSoglAmlPreSavRNull) {
        writeString(Pos.PCO_SOGL_AML_PRE_SAV_R_NULL, pcoSoglAmlPreSavRNull, Len.PCO_SOGL_AML_PRE_SAV_R_NULL);
    }

    /**Original name: PCO-SOGL-AML-PRE-SAV-R-NULL<br>*/
    public String getPcoSoglAmlPreSavRNull() {
        return readString(Pos.PCO_SOGL_AML_PRE_SAV_R_NULL, Len.PCO_SOGL_AML_PRE_SAV_R_NULL);
    }

    public String getPcoSoglAmlPreSavRNullFormatted() {
        return Functions.padBlanks(getPcoSoglAmlPreSavRNull(), Len.PCO_SOGL_AML_PRE_SAV_R_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_SOGL_AML_PRE_SAV_R = 1;
        public static final int PCO_SOGL_AML_PRE_SAV_R_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_SOGL_AML_PRE_SAV_R = 8;
        public static final int PCO_SOGL_AML_PRE_SAV_R_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_SOGL_AML_PRE_SAV_R = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_SOGL_AML_PRE_SAV_R = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
