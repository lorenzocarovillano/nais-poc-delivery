package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-REC-PROV<br>
 * Variable: OCO-REC-PROV from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoRecProv extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoRecProv() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_REC_PROV;
    }

    public void setOcoRecProv(AfDecimal ocoRecProv) {
        writeDecimalAsPacked(Pos.OCO_REC_PROV, ocoRecProv.copy());
    }

    public void setOcoRecProvFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_REC_PROV, Pos.OCO_REC_PROV);
    }

    /**Original name: OCO-REC-PROV<br>*/
    public AfDecimal getOcoRecProv() {
        return readPackedAsDecimal(Pos.OCO_REC_PROV, Len.Int.OCO_REC_PROV, Len.Fract.OCO_REC_PROV);
    }

    public byte[] getOcoRecProvAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_REC_PROV, Pos.OCO_REC_PROV);
        return buffer;
    }

    public void setOcoRecProvNull(String ocoRecProvNull) {
        writeString(Pos.OCO_REC_PROV_NULL, ocoRecProvNull, Len.OCO_REC_PROV_NULL);
    }

    /**Original name: OCO-REC-PROV-NULL<br>*/
    public String getOcoRecProvNull() {
        return readString(Pos.OCO_REC_PROV_NULL, Len.OCO_REC_PROV_NULL);
    }

    public String getOcoRecProvNullFormatted() {
        return Functions.padBlanks(getOcoRecProvNull(), Len.OCO_REC_PROV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_REC_PROV = 1;
        public static final int OCO_REC_PROV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_REC_PROV = 8;
        public static final int OCO_REC_PROV_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_REC_PROV = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_REC_PROV = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
