package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-SOPR-SPO<br>
 * Variable: TIT-TOT-SOPR-SPO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_SOPR_SPO;
    }

    public void setTitTotSoprSpo(AfDecimal titTotSoprSpo) {
        writeDecimalAsPacked(Pos.TIT_TOT_SOPR_SPO, titTotSoprSpo.copy());
    }

    public void setTitTotSoprSpoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_SOPR_SPO, Pos.TIT_TOT_SOPR_SPO);
    }

    /**Original name: TIT-TOT-SOPR-SPO<br>*/
    public AfDecimal getTitTotSoprSpo() {
        return readPackedAsDecimal(Pos.TIT_TOT_SOPR_SPO, Len.Int.TIT_TOT_SOPR_SPO, Len.Fract.TIT_TOT_SOPR_SPO);
    }

    public byte[] getTitTotSoprSpoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_SOPR_SPO, Pos.TIT_TOT_SOPR_SPO);
        return buffer;
    }

    public void setTitTotSoprSpoNull(String titTotSoprSpoNull) {
        writeString(Pos.TIT_TOT_SOPR_SPO_NULL, titTotSoprSpoNull, Len.TIT_TOT_SOPR_SPO_NULL);
    }

    /**Original name: TIT-TOT-SOPR-SPO-NULL<br>*/
    public String getTitTotSoprSpoNull() {
        return readString(Pos.TIT_TOT_SOPR_SPO_NULL, Len.TIT_TOT_SOPR_SPO_NULL);
    }

    public String getTitTotSoprSpoNullFormatted() {
        return Functions.padBlanks(getTitTotSoprSpoNull(), Len.TIT_TOT_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SOPR_SPO = 1;
        public static final int TIT_TOT_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SOPR_SPO = 8;
        public static final int TIT_TOT_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
