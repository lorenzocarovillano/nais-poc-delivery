package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvpvt1;
import it.accenture.jnais.copy.WpvtDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WPVT-TAB-PROV-TR<br>
 * Variables: WPVT-TAB-PROV-TR from copybook LCCVPVTA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpvtTabProvTr {

    //==== PROPERTIES ====
    //Original name: LCCVPVT1
    private Lccvpvt1 lccvpvt1 = new Lccvpvt1();

    //==== METHODS ====
    public void setWpvtTabProvTrBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpvt1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpvt1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpvt1.Len.Int.ID_PTF, 0));
        position += Lccvpvt1.Len.ID_PTF;
        lccvpvt1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWpvtTabProvTrBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvpvt1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvpvt1.getIdPtf(), Lccvpvt1.Len.Int.ID_PTF, 0);
        position += Lccvpvt1.Len.ID_PTF;
        lccvpvt1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWpvtTabProvTrSpaces() {
        lccvpvt1.initLccvpvt1Spaces();
    }

    public Lccvpvt1 getLccvpvt1() {
        return lccvpvt1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_TAB_PROV_TR = WpolStatus.Len.STATUS + Lccvpvt1.Len.ID_PTF + WpvtDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
