package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvdad1;
import it.accenture.jnais.copy.WdadDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WDAD-AREA-DEF-ADES<br>
 * Variable: WDAD-AREA-DEF-ADES from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WdadAreaDefAdes extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WDAD-ELE-DEF-ADES-MAX
    private short wdadEleDefAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDAD1
    private Lccvdad1 lccvdad1 = new Lccvdad1();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_AREA_DEF_ADES;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWdadAreaDefAdesBytes(buf);
    }

    public String getWdadAreaDefAdesFormatted() {
        return MarshalByteExt.bufferToStr(getWdadAreaDefAdesBytes());
    }

    public void setWdadAreaDefAdesBytes(byte[] buffer) {
        setWdadAreaDefAdesBytes(buffer, 1);
    }

    public byte[] getWdadAreaDefAdesBytes() {
        byte[] buffer = new byte[Len.WDAD_AREA_DEF_ADES];
        return getWdadAreaDefAdesBytes(buffer, 1);
    }

    public void setWdadAreaDefAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        wdadEleDefAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWdadTabDefAdesBytes(buffer, position);
    }

    public byte[] getWdadAreaDefAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdadEleDefAdesMax);
        position += Types.SHORT_SIZE;
        getWdadTabDefAdesBytes(buffer, position);
        return buffer;
    }

    public void setWdadEleDefAdesMax(short wdadEleDefAdesMax) {
        this.wdadEleDefAdesMax = wdadEleDefAdesMax;
    }

    public short getWdadEleDefAdesMax() {
        return this.wdadEleDefAdesMax;
    }

    public void setWdadTabDefAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdad1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdad1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdad1.Len.Int.ID_PTF, 0));
        position += Lccvdad1.Len.ID_PTF;
        lccvdad1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdadTabDefAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdad1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdad1.getIdPtf(), Lccvdad1.Len.Int.ID_PTF, 0);
        position += Lccvdad1.Len.ID_PTF;
        lccvdad1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    @Override
    public byte[] serialize() {
        return getWdadAreaDefAdesBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_ELE_DEF_ADES_MAX = 2;
        public static final int WDAD_TAB_DEF_ADES = WpolStatus.Len.STATUS + Lccvdad1.Len.ID_PTF + WdadDati.Len.DATI;
        public static final int WDAD_AREA_DEF_ADES = WDAD_ELE_DEF_ADES_MAX + WDAD_TAB_DEF_ADES;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
