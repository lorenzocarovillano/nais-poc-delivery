package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRSTZ-AGG-ULT<br>
 * Variable: WTGA-PRSTZ-AGG-ULT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPrstzAggUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPrstzAggUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRSTZ_AGG_ULT;
    }

    public void setWtgaPrstzAggUlt(AfDecimal wtgaPrstzAggUlt) {
        writeDecimalAsPacked(Pos.WTGA_PRSTZ_AGG_ULT, wtgaPrstzAggUlt.copy());
    }

    public void setWtgaPrstzAggUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRSTZ_AGG_ULT, Pos.WTGA_PRSTZ_AGG_ULT);
    }

    /**Original name: WTGA-PRSTZ-AGG-ULT<br>*/
    public AfDecimal getWtgaPrstzAggUlt() {
        return readPackedAsDecimal(Pos.WTGA_PRSTZ_AGG_ULT, Len.Int.WTGA_PRSTZ_AGG_ULT, Len.Fract.WTGA_PRSTZ_AGG_ULT);
    }

    public byte[] getWtgaPrstzAggUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRSTZ_AGG_ULT, Pos.WTGA_PRSTZ_AGG_ULT);
        return buffer;
    }

    public void initWtgaPrstzAggUltSpaces() {
        fill(Pos.WTGA_PRSTZ_AGG_ULT, Len.WTGA_PRSTZ_AGG_ULT, Types.SPACE_CHAR);
    }

    public void setWtgaPrstzAggUltNull(String wtgaPrstzAggUltNull) {
        writeString(Pos.WTGA_PRSTZ_AGG_ULT_NULL, wtgaPrstzAggUltNull, Len.WTGA_PRSTZ_AGG_ULT_NULL);
    }

    /**Original name: WTGA-PRSTZ-AGG-ULT-NULL<br>*/
    public String getWtgaPrstzAggUltNull() {
        return readString(Pos.WTGA_PRSTZ_AGG_ULT_NULL, Len.WTGA_PRSTZ_AGG_ULT_NULL);
    }

    public String getWtgaPrstzAggUltNullFormatted() {
        return Functions.padBlanks(getWtgaPrstzAggUltNull(), Len.WTGA_PRSTZ_AGG_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_AGG_ULT = 1;
        public static final int WTGA_PRSTZ_AGG_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_AGG_ULT = 8;
        public static final int WTGA_PRSTZ_AGG_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_AGG_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_AGG_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
