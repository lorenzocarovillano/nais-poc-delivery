package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-IMPST-PRVR<br>
 * Variable: LQU-IMPB-IMPST-PRVR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbImpstPrvr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbImpstPrvr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_IMPST_PRVR;
    }

    public void setLquImpbImpstPrvr(AfDecimal lquImpbImpstPrvr) {
        writeDecimalAsPacked(Pos.LQU_IMPB_IMPST_PRVR, lquImpbImpstPrvr.copy());
    }

    public void setLquImpbImpstPrvrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_IMPST_PRVR, Pos.LQU_IMPB_IMPST_PRVR);
    }

    /**Original name: LQU-IMPB-IMPST-PRVR<br>*/
    public AfDecimal getLquImpbImpstPrvr() {
        return readPackedAsDecimal(Pos.LQU_IMPB_IMPST_PRVR, Len.Int.LQU_IMPB_IMPST_PRVR, Len.Fract.LQU_IMPB_IMPST_PRVR);
    }

    public byte[] getLquImpbImpstPrvrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_IMPST_PRVR, Pos.LQU_IMPB_IMPST_PRVR);
        return buffer;
    }

    public void setLquImpbImpstPrvrNull(String lquImpbImpstPrvrNull) {
        writeString(Pos.LQU_IMPB_IMPST_PRVR_NULL, lquImpbImpstPrvrNull, Len.LQU_IMPB_IMPST_PRVR_NULL);
    }

    /**Original name: LQU-IMPB-IMPST-PRVR-NULL<br>*/
    public String getLquImpbImpstPrvrNull() {
        return readString(Pos.LQU_IMPB_IMPST_PRVR_NULL, Len.LQU_IMPB_IMPST_PRVR_NULL);
    }

    public String getLquImpbImpstPrvrNullFormatted() {
        return Functions.padBlanks(getLquImpbImpstPrvrNull(), Len.LQU_IMPB_IMPST_PRVR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_IMPST_PRVR = 1;
        public static final int LQU_IMPB_IMPST_PRVR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_IMPST_PRVR = 8;
        public static final int LQU_IMPB_IMPST_PRVR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_IMPST_PRVR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_IMPST_PRVR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
