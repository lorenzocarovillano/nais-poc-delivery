package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADA-LUNGHEZZA-DATO<br>
 * Variable: ADA-LUNGHEZZA-DATO from program IDSS0020<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdaLunghezzaDato extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdaLunghezzaDato() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADA_LUNGHEZZA_DATO;
    }

    public void setAdaLunghezzaDato(int adaLunghezzaDato) {
        writeIntAsPacked(Pos.ADA_LUNGHEZZA_DATO, adaLunghezzaDato, Len.Int.ADA_LUNGHEZZA_DATO);
    }

    public void setAdaLunghezzaDatoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADA_LUNGHEZZA_DATO, Pos.ADA_LUNGHEZZA_DATO);
    }

    /**Original name: ADA-LUNGHEZZA-DATO<br>*/
    public int getAdaLunghezzaDato() {
        return readPackedAsInt(Pos.ADA_LUNGHEZZA_DATO, Len.Int.ADA_LUNGHEZZA_DATO);
    }

    public byte[] getAdaLunghezzaDatoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADA_LUNGHEZZA_DATO, Pos.ADA_LUNGHEZZA_DATO);
        return buffer;
    }

    public void setAdaLunghezzaDatoNull(String adaLunghezzaDatoNull) {
        writeString(Pos.ADA_LUNGHEZZA_DATO_NULL, adaLunghezzaDatoNull, Len.ADA_LUNGHEZZA_DATO_NULL);
    }

    /**Original name: ADA-LUNGHEZZA-DATO-NULL<br>*/
    public String getAdaLunghezzaDatoNull() {
        return readString(Pos.ADA_LUNGHEZZA_DATO_NULL, Len.ADA_LUNGHEZZA_DATO_NULL);
    }

    public String getAdaLunghezzaDatoNullFormatted() {
        return Functions.padBlanks(getAdaLunghezzaDatoNull(), Len.ADA_LUNGHEZZA_DATO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADA_LUNGHEZZA_DATO = 1;
        public static final int ADA_LUNGHEZZA_DATO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADA_LUNGHEZZA_DATO = 3;
        public static final int ADA_LUNGHEZZA_DATO_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADA_LUNGHEZZA_DATO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
