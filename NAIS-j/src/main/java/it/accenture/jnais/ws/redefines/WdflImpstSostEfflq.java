package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-SOST-EFFLQ<br>
 * Variable: WDFL-IMPST-SOST-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstSostEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstSostEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_SOST_EFFLQ;
    }

    public void setWdflImpstSostEfflq(AfDecimal wdflImpstSostEfflq) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_SOST_EFFLQ, wdflImpstSostEfflq.copy());
    }

    public void setWdflImpstSostEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_SOST_EFFLQ, Pos.WDFL_IMPST_SOST_EFFLQ);
    }

    /**Original name: WDFL-IMPST-SOST-EFFLQ<br>*/
    public AfDecimal getWdflImpstSostEfflq() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_SOST_EFFLQ, Len.Int.WDFL_IMPST_SOST_EFFLQ, Len.Fract.WDFL_IMPST_SOST_EFFLQ);
    }

    public byte[] getWdflImpstSostEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_SOST_EFFLQ, Pos.WDFL_IMPST_SOST_EFFLQ);
        return buffer;
    }

    public void setWdflImpstSostEfflqNull(String wdflImpstSostEfflqNull) {
        writeString(Pos.WDFL_IMPST_SOST_EFFLQ_NULL, wdflImpstSostEfflqNull, Len.WDFL_IMPST_SOST_EFFLQ_NULL);
    }

    /**Original name: WDFL-IMPST-SOST-EFFLQ-NULL<br>*/
    public String getWdflImpstSostEfflqNull() {
        return readString(Pos.WDFL_IMPST_SOST_EFFLQ_NULL, Len.WDFL_IMPST_SOST_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_SOST_EFFLQ = 1;
        public static final int WDFL_IMPST_SOST_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_SOST_EFFLQ = 8;
        public static final int WDFL_IMPST_SOST_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_SOST_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_SOST_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
