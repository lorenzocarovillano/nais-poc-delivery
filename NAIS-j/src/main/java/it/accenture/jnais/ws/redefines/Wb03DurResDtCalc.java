package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-RES-DT-CALC<br>
 * Variable: WB03-DUR-RES-DT-CALC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DurResDtCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DurResDtCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR_RES_DT_CALC;
    }

    public void setWb03DurResDtCalc(AfDecimal wb03DurResDtCalc) {
        writeDecimalAsPacked(Pos.WB03_DUR_RES_DT_CALC, wb03DurResDtCalc.copy());
    }

    public void setWb03DurResDtCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR_RES_DT_CALC, Pos.WB03_DUR_RES_DT_CALC);
    }

    /**Original name: WB03-DUR-RES-DT-CALC<br>*/
    public AfDecimal getWb03DurResDtCalc() {
        return readPackedAsDecimal(Pos.WB03_DUR_RES_DT_CALC, Len.Int.WB03_DUR_RES_DT_CALC, Len.Fract.WB03_DUR_RES_DT_CALC);
    }

    public byte[] getWb03DurResDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR_RES_DT_CALC, Pos.WB03_DUR_RES_DT_CALC);
        return buffer;
    }

    public void setWb03DurResDtCalcNull(String wb03DurResDtCalcNull) {
        writeString(Pos.WB03_DUR_RES_DT_CALC_NULL, wb03DurResDtCalcNull, Len.WB03_DUR_RES_DT_CALC_NULL);
    }

    /**Original name: WB03-DUR-RES-DT-CALC-NULL<br>*/
    public String getWb03DurResDtCalcNull() {
        return readString(Pos.WB03_DUR_RES_DT_CALC_NULL, Len.WB03_DUR_RES_DT_CALC_NULL);
    }

    public String getWb03DurResDtCalcNullFormatted() {
        return Functions.padBlanks(getWb03DurResDtCalcNull(), Len.WB03_DUR_RES_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR_RES_DT_CALC = 1;
        public static final int WB03_DUR_RES_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR_RES_DT_CALC = 6;
        public static final int WB03_DUR_RES_DT_CALC_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR_RES_DT_CALC = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_DUR_RES_DT_CALC = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
