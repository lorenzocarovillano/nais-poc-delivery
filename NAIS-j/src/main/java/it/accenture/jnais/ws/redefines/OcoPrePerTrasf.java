package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-PRE-PER-TRASF<br>
 * Variable: OCO-PRE-PER-TRASF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoPrePerTrasf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoPrePerTrasf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_PRE_PER_TRASF;
    }

    public void setOcoPrePerTrasf(AfDecimal ocoPrePerTrasf) {
        writeDecimalAsPacked(Pos.OCO_PRE_PER_TRASF, ocoPrePerTrasf.copy());
    }

    public void setOcoPrePerTrasfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_PRE_PER_TRASF, Pos.OCO_PRE_PER_TRASF);
    }

    /**Original name: OCO-PRE-PER-TRASF<br>*/
    public AfDecimal getOcoPrePerTrasf() {
        return readPackedAsDecimal(Pos.OCO_PRE_PER_TRASF, Len.Int.OCO_PRE_PER_TRASF, Len.Fract.OCO_PRE_PER_TRASF);
    }

    public byte[] getOcoPrePerTrasfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_PRE_PER_TRASF, Pos.OCO_PRE_PER_TRASF);
        return buffer;
    }

    public void setOcoPrePerTrasfNull(String ocoPrePerTrasfNull) {
        writeString(Pos.OCO_PRE_PER_TRASF_NULL, ocoPrePerTrasfNull, Len.OCO_PRE_PER_TRASF_NULL);
    }

    /**Original name: OCO-PRE-PER-TRASF-NULL<br>*/
    public String getOcoPrePerTrasfNull() {
        return readString(Pos.OCO_PRE_PER_TRASF_NULL, Len.OCO_PRE_PER_TRASF_NULL);
    }

    public String getOcoPrePerTrasfNullFormatted() {
        return Functions.padBlanks(getOcoPrePerTrasfNull(), Len.OCO_PRE_PER_TRASF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_PRE_PER_TRASF = 1;
        public static final int OCO_PRE_PER_TRASF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_PRE_PER_TRASF = 8;
        public static final int OCO_PRE_PER_TRASF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_PRE_PER_TRASF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_PRE_PER_TRASF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
