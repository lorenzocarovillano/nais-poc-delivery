package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.Ivvv0212CtrlAutOper;
import it.accenture.jnais.ws.redefines.C216TabValP;
import it.accenture.jnais.ws.redefines.C216TabValT;

/**Original name: AREA-INPUT<br>
 * Variable: AREA-INPUT from program IVVS0216<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaInput extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int C216_CTRL_AUT_OPER_MAXOCCURS = 20;
    /**Original name: C216-NUM-MAX-LIVELLI-P<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA OUTPUT VALORIZZATORE VARIABILI
	 *    - area skeda per servizi di prodotto
	 *    - area variabili autonomia operativa
	 * ----------------------------------------------------------------*
	 * AREA SKEDA PER SERVIZI DI PRODOTTO
	 *     05 (SF)-NUM-MAX-LIVELLI              PIC 9(04) VALUE 300.</pre>*/
    private String c216NumMaxLivelliP = "0003";
    //Original name: C216-NUM-MAX-LIVELLI-T
    private String c216NumMaxLivelliT = "0750";
    //Original name: C216-NUM-MAX-VARIABILI-P
    private String c216NumMaxVariabiliP = "100";
    //Original name: C216-NUM-MAX-VARIABILI-T
    private String c216NumMaxVariabiliT = "075";
    //Original name: C216-DEE
    private String c216Dee = DefaultValues.stringVal(Len.C216_DEE);
    //Original name: C216-ELE-LIVELLO-MAX-P
    private short c216EleLivelloMaxP = DefaultValues.SHORT_VAL;
    //Original name: C216-TAB-VAL-P
    private C216TabValP c216TabValP = new C216TabValP();
    /**Original name: C216-ELE-LIVELLO-MAX-T<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA OUTPUT TRANCHE SCHEDE 'G' 'H'
	 * ----------------------------------------------------------------*</pre>*/
    private short c216EleLivelloMaxT = DefaultValues.SHORT_VAL;
    //Original name: C216-TAB-VAL-T
    private C216TabValT c216TabValT = new C216TabValT();
    //Original name: C216-ELE-CTRL-AUT-OPER-MAX
    private short c216EleCtrlAutOperMax = DefaultValues.SHORT_VAL;
    //Original name: C216-CTRL-AUT-OPER
    private Ivvv0212CtrlAutOper[] c216CtrlAutOper = new Ivvv0212CtrlAutOper[C216_CTRL_AUT_OPER_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public AreaInput() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_INPUT;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaInputBytes(buf);
    }

    public void init() {
        for (int c216CtrlAutOperIdx = 1; c216CtrlAutOperIdx <= C216_CTRL_AUT_OPER_MAXOCCURS; c216CtrlAutOperIdx++) {
            c216CtrlAutOper[c216CtrlAutOperIdx - 1] = new Ivvv0212CtrlAutOper();
        }
    }

    public String getAreaWorkIvvc0216Formatted() {
        return MarshalByteExt.bufferToStr(getAreaInputBytes());
    }

    public void setAreaInputBytes(byte[] buffer) {
        setAreaInputBytes(buffer, 1);
    }

    public byte[] getAreaInputBytes() {
        byte[] buffer = new byte[Len.AREA_INPUT];
        return getAreaInputBytes(buffer, 1);
    }

    public void setAreaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        c216NumMaxLivelliP = MarshalByte.readFixedString(buffer, position, Len.C216_NUM_MAX_LIVELLI_P);
        position += Len.C216_NUM_MAX_LIVELLI_P;
        c216NumMaxLivelliT = MarshalByte.readFixedString(buffer, position, Len.C216_NUM_MAX_LIVELLI_T);
        position += Len.C216_NUM_MAX_LIVELLI_T;
        c216NumMaxVariabiliP = MarshalByte.readFixedString(buffer, position, Len.C216_NUM_MAX_VARIABILI_P);
        position += Len.C216_NUM_MAX_VARIABILI_P;
        c216NumMaxVariabiliT = MarshalByte.readFixedString(buffer, position, Len.C216_NUM_MAX_VARIABILI_T);
        position += Len.C216_NUM_MAX_VARIABILI_T;
        c216Dee = MarshalByte.readString(buffer, position, Len.C216_DEE);
        position += Len.C216_DEE;
        c216EleLivelloMaxP = MarshalByte.readPackedAsShort(buffer, position, Len.Int.C216_ELE_LIVELLO_MAX_P, 0);
        position += Len.C216_ELE_LIVELLO_MAX_P;
        c216TabValP.setC216TabValPBytes(buffer, position);
        position += C216TabValP.Len.C216_TAB_VAL_P;
        c216EleLivelloMaxT = MarshalByte.readPackedAsShort(buffer, position, Len.Int.C216_ELE_LIVELLO_MAX_T, 0);
        position += Len.C216_ELE_LIVELLO_MAX_T;
        c216TabValT.setC216TabValTBytes(buffer, position);
        position += C216TabValT.Len.C216_TAB_VAL_T;
        setC216VarAutOperBytes(buffer, position);
    }

    public byte[] getAreaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, c216NumMaxLivelliP, Len.C216_NUM_MAX_LIVELLI_P);
        position += Len.C216_NUM_MAX_LIVELLI_P;
        MarshalByte.writeString(buffer, position, c216NumMaxLivelliT, Len.C216_NUM_MAX_LIVELLI_T);
        position += Len.C216_NUM_MAX_LIVELLI_T;
        MarshalByte.writeString(buffer, position, c216NumMaxVariabiliP, Len.C216_NUM_MAX_VARIABILI_P);
        position += Len.C216_NUM_MAX_VARIABILI_P;
        MarshalByte.writeString(buffer, position, c216NumMaxVariabiliT, Len.C216_NUM_MAX_VARIABILI_T);
        position += Len.C216_NUM_MAX_VARIABILI_T;
        MarshalByte.writeString(buffer, position, c216Dee, Len.C216_DEE);
        position += Len.C216_DEE;
        MarshalByte.writeShortAsPacked(buffer, position, c216EleLivelloMaxP, Len.Int.C216_ELE_LIVELLO_MAX_P, 0);
        position += Len.C216_ELE_LIVELLO_MAX_P;
        c216TabValP.getC216TabValPBytes(buffer, position);
        position += C216TabValP.Len.C216_TAB_VAL_P;
        MarshalByte.writeShortAsPacked(buffer, position, c216EleLivelloMaxT, Len.Int.C216_ELE_LIVELLO_MAX_T, 0);
        position += Len.C216_ELE_LIVELLO_MAX_T;
        c216TabValT.getC216TabValTBytes(buffer, position);
        position += C216TabValT.Len.C216_TAB_VAL_T;
        getC216VarAutOperBytes(buffer, position);
        return buffer;
    }

    public short getIvvc0216NumMaxLivelliP() {
        return NumericDisplay.asShort(this.c216NumMaxLivelliP);
    }

    public short getIvvc0216NumMaxLivelliT() {
        return NumericDisplay.asShort(this.c216NumMaxLivelliT);
    }

    public short getC216NumMaxVariabiliP() {
        return NumericDisplay.asShort(this.c216NumMaxVariabiliP);
    }

    public short getC216NumMaxVariabiliT() {
        return NumericDisplay.asShort(this.c216NumMaxVariabiliT);
    }

    public void setC216Dee(String c216Dee) {
        this.c216Dee = Functions.subString(c216Dee, Len.C216_DEE);
    }

    public String getC216Dee() {
        return this.c216Dee;
    }

    public void setC216EleLivelloMaxP(short c216EleLivelloMaxP) {
        this.c216EleLivelloMaxP = c216EleLivelloMaxP;
    }

    public short getC216EleLivelloMaxP() {
        return this.c216EleLivelloMaxP;
    }

    public void setC216EleLivelloMaxT(short c216EleLivelloMaxT) {
        this.c216EleLivelloMaxT = c216EleLivelloMaxT;
    }

    public short getC216EleLivelloMaxT() {
        return this.c216EleLivelloMaxT;
    }

    /**Original name: IVVC0216-VAR-AUT-OPER<br>
	 * <pre>AREA VARIABILI AUTONOMIA OPERATIVA</pre>*/
    public byte[] getIvvc0216VarAutOperBytes() {
        byte[] buffer = new byte[Len.C216_VAR_AUT_OPER];
        return getC216VarAutOperBytes(buffer, 1);
    }

    public void setC216VarAutOperBytes(byte[] buffer, int offset) {
        int position = offset;
        c216EleCtrlAutOperMax = MarshalByte.readPackedAsShort(buffer, position, Len.Int.C216_ELE_CTRL_AUT_OPER_MAX, 0);
        position += Len.C216_ELE_CTRL_AUT_OPER_MAX;
        for (int idx = 1; idx <= C216_CTRL_AUT_OPER_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                c216CtrlAutOper[idx - 1].setCtrlAutOperBytes(buffer, position);
                position += Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
            }
            else {
                c216CtrlAutOper[idx - 1].initCtrlAutOperSpaces();
                position += Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
            }
        }
    }

    public byte[] getC216VarAutOperBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeShortAsPacked(buffer, position, c216EleCtrlAutOperMax, Len.Int.C216_ELE_CTRL_AUT_OPER_MAX, 0);
        position += Len.C216_ELE_CTRL_AUT_OPER_MAX;
        for (int idx = 1; idx <= C216_CTRL_AUT_OPER_MAXOCCURS; idx++) {
            c216CtrlAutOper[idx - 1].getCtrlAutOperBytes(buffer, position);
            position += Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
        }
        return buffer;
    }

    public void setC216EleCtrlAutOperMax(short c216EleCtrlAutOperMax) {
        this.c216EleCtrlAutOperMax = c216EleCtrlAutOperMax;
    }

    public short getC216EleCtrlAutOperMax() {
        return this.c216EleCtrlAutOperMax;
    }

    public Ivvv0212CtrlAutOper getC216CtrlAutOper(int idx) {
        return c216CtrlAutOper[idx - 1];
    }

    public C216TabValP getC216TabValP() {
        return c216TabValP;
    }

    public C216TabValT getC216TabValT() {
        return c216TabValT;
    }

    @Override
    public byte[] serialize() {
        return getAreaInputBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int C216_NUM_MAX_LIVELLI_P = 4;
        public static final int C216_NUM_MAX_LIVELLI_T = 4;
        public static final int C216_NUM_MAX_VARIABILI_P = 3;
        public static final int C216_NUM_MAX_VARIABILI_T = 3;
        public static final int C216_DEE = 8;
        public static final int C216_ELE_LIVELLO_MAX_P = 3;
        public static final int C216_ELE_LIVELLO_MAX_T = 3;
        public static final int C216_ELE_CTRL_AUT_OPER_MAX = 3;
        public static final int C216_VAR_AUT_OPER = C216_ELE_CTRL_AUT_OPER_MAX + AreaInput.C216_CTRL_AUT_OPER_MAXOCCURS * Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
        public static final int AREA_INPUT = C216_NUM_MAX_LIVELLI_P + C216_NUM_MAX_LIVELLI_T + C216_NUM_MAX_VARIABILI_P + C216_NUM_MAX_VARIABILI_T + C216_DEE + C216_ELE_LIVELLO_MAX_P + C216TabValP.Len.C216_TAB_VAL_P + C216_ELE_LIVELLO_MAX_T + C216TabValT.Len.C216_TAB_VAL_T + C216_VAR_AUT_OPER;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int C216_ELE_LIVELLO_MAX_P = 4;
            public static final int C216_ELE_LIVELLO_MAX_T = 4;
            public static final int C216_ELE_CTRL_AUT_OPER_MAX = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
