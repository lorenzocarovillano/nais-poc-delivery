package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WSDI-ID-MOVI-CHIU<br>
 * Variable: WSDI-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsdiIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsdiIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSDI_ID_MOVI_CHIU;
    }

    public void setWsdiIdMoviChiu(int wsdiIdMoviChiu) {
        writeIntAsPacked(Pos.WSDI_ID_MOVI_CHIU, wsdiIdMoviChiu, Len.Int.WSDI_ID_MOVI_CHIU);
    }

    public void setWsdiIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSDI_ID_MOVI_CHIU, Pos.WSDI_ID_MOVI_CHIU);
    }

    /**Original name: WSDI-ID-MOVI-CHIU<br>*/
    public int getWsdiIdMoviChiu() {
        return readPackedAsInt(Pos.WSDI_ID_MOVI_CHIU, Len.Int.WSDI_ID_MOVI_CHIU);
    }

    public byte[] getWsdiIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSDI_ID_MOVI_CHIU, Pos.WSDI_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWsdiIdMoviChiuSpaces() {
        fill(Pos.WSDI_ID_MOVI_CHIU, Len.WSDI_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWsdiIdMoviChiuNull(String wsdiIdMoviChiuNull) {
        writeString(Pos.WSDI_ID_MOVI_CHIU_NULL, wsdiIdMoviChiuNull, Len.WSDI_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WSDI-ID-MOVI-CHIU-NULL<br>*/
    public String getWsdiIdMoviChiuNull() {
        return readString(Pos.WSDI_ID_MOVI_CHIU_NULL, Len.WSDI_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSDI_ID_MOVI_CHIU = 1;
        public static final int WSDI_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WSDI_ID_MOVI_CHIU = 5;
        public static final int WSDI_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WSDI_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
