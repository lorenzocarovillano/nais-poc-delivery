package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPST-SOST-K2-ANTI<br>
 * Variable: DFA-IMPST-SOST-K2-ANTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpstSostK2Anti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpstSostK2Anti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPST_SOST_K2_ANTI;
    }

    public void setDfaImpstSostK2Anti(AfDecimal dfaImpstSostK2Anti) {
        writeDecimalAsPacked(Pos.DFA_IMPST_SOST_K2_ANTI, dfaImpstSostK2Anti.copy());
    }

    public void setDfaImpstSostK2AntiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPST_SOST_K2_ANTI, Pos.DFA_IMPST_SOST_K2_ANTI);
    }

    /**Original name: DFA-IMPST-SOST-K2-ANTI<br>*/
    public AfDecimal getDfaImpstSostK2Anti() {
        return readPackedAsDecimal(Pos.DFA_IMPST_SOST_K2_ANTI, Len.Int.DFA_IMPST_SOST_K2_ANTI, Len.Fract.DFA_IMPST_SOST_K2_ANTI);
    }

    public byte[] getDfaImpstSostK2AntiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPST_SOST_K2_ANTI, Pos.DFA_IMPST_SOST_K2_ANTI);
        return buffer;
    }

    public void setDfaImpstSostK2AntiNull(String dfaImpstSostK2AntiNull) {
        writeString(Pos.DFA_IMPST_SOST_K2_ANTI_NULL, dfaImpstSostK2AntiNull, Len.DFA_IMPST_SOST_K2_ANTI_NULL);
    }

    /**Original name: DFA-IMPST-SOST-K2-ANTI-NULL<br>*/
    public String getDfaImpstSostK2AntiNull() {
        return readString(Pos.DFA_IMPST_SOST_K2_ANTI_NULL, Len.DFA_IMPST_SOST_K2_ANTI_NULL);
    }

    public String getDfaImpstSostK2AntiNullFormatted() {
        return Functions.padBlanks(getDfaImpstSostK2AntiNull(), Len.DFA_IMPST_SOST_K2_ANTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_SOST_K2_ANTI = 1;
        public static final int DFA_IMPST_SOST_K2_ANTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_SOST_K2_ANTI = 8;
        public static final int DFA_IMPST_SOST_K2_ANTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_SOST_K2_ANTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_SOST_K2_ANTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
