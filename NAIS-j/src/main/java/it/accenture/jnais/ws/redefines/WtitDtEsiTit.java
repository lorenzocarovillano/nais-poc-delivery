package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-DT-ESI-TIT<br>
 * Variable: WTIT-DT-ESI-TIT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitDtEsiTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitDtEsiTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_DT_ESI_TIT;
    }

    public void setWtitDtEsiTit(int wtitDtEsiTit) {
        writeIntAsPacked(Pos.WTIT_DT_ESI_TIT, wtitDtEsiTit, Len.Int.WTIT_DT_ESI_TIT);
    }

    public void setWtitDtEsiTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_DT_ESI_TIT, Pos.WTIT_DT_ESI_TIT);
    }

    /**Original name: WTIT-DT-ESI-TIT<br>*/
    public int getWtitDtEsiTit() {
        return readPackedAsInt(Pos.WTIT_DT_ESI_TIT, Len.Int.WTIT_DT_ESI_TIT);
    }

    public byte[] getWtitDtEsiTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_DT_ESI_TIT, Pos.WTIT_DT_ESI_TIT);
        return buffer;
    }

    public void initWtitDtEsiTitSpaces() {
        fill(Pos.WTIT_DT_ESI_TIT, Len.WTIT_DT_ESI_TIT, Types.SPACE_CHAR);
    }

    public void setWtitDtEsiTitNull(String wtitDtEsiTitNull) {
        writeString(Pos.WTIT_DT_ESI_TIT_NULL, wtitDtEsiTitNull, Len.WTIT_DT_ESI_TIT_NULL);
    }

    /**Original name: WTIT-DT-ESI-TIT-NULL<br>*/
    public String getWtitDtEsiTitNull() {
        return readString(Pos.WTIT_DT_ESI_TIT_NULL, Len.WTIT_DT_ESI_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_DT_ESI_TIT = 1;
        public static final int WTIT_DT_ESI_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_DT_ESI_TIT = 5;
        public static final int WTIT_DT_ESI_TIT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_DT_ESI_TIT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
