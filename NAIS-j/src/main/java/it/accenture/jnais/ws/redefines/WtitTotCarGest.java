package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-CAR-GEST<br>
 * Variable: WTIT-TOT-CAR-GEST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_CAR_GEST;
    }

    public void setWtitTotCarGest(AfDecimal wtitTotCarGest) {
        writeDecimalAsPacked(Pos.WTIT_TOT_CAR_GEST, wtitTotCarGest.copy());
    }

    public void setWtitTotCarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_CAR_GEST, Pos.WTIT_TOT_CAR_GEST);
    }

    /**Original name: WTIT-TOT-CAR-GEST<br>*/
    public AfDecimal getWtitTotCarGest() {
        return readPackedAsDecimal(Pos.WTIT_TOT_CAR_GEST, Len.Int.WTIT_TOT_CAR_GEST, Len.Fract.WTIT_TOT_CAR_GEST);
    }

    public byte[] getWtitTotCarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_CAR_GEST, Pos.WTIT_TOT_CAR_GEST);
        return buffer;
    }

    public void initWtitTotCarGestSpaces() {
        fill(Pos.WTIT_TOT_CAR_GEST, Len.WTIT_TOT_CAR_GEST, Types.SPACE_CHAR);
    }

    public void setWtitTotCarGestNull(String wtitTotCarGestNull) {
        writeString(Pos.WTIT_TOT_CAR_GEST_NULL, wtitTotCarGestNull, Len.WTIT_TOT_CAR_GEST_NULL);
    }

    /**Original name: WTIT-TOT-CAR-GEST-NULL<br>*/
    public String getWtitTotCarGestNull() {
        return readString(Pos.WTIT_TOT_CAR_GEST_NULL, Len.WTIT_TOT_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_CAR_GEST = 1;
        public static final int WTIT_TOT_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_CAR_GEST = 8;
        public static final int WTIT_TOT_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
