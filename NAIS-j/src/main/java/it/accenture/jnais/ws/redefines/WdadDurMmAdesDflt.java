package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDAD-DUR-MM-ADES-DFLT<br>
 * Variable: WDAD-DUR-MM-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadDurMmAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadDurMmAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_DUR_MM_ADES_DFLT;
    }

    public void setWdadDurMmAdesDflt(int wdadDurMmAdesDflt) {
        writeIntAsPacked(Pos.WDAD_DUR_MM_ADES_DFLT, wdadDurMmAdesDflt, Len.Int.WDAD_DUR_MM_ADES_DFLT);
    }

    public void setWdadDurMmAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_DUR_MM_ADES_DFLT, Pos.WDAD_DUR_MM_ADES_DFLT);
    }

    /**Original name: WDAD-DUR-MM-ADES-DFLT<br>*/
    public int getWdadDurMmAdesDflt() {
        return readPackedAsInt(Pos.WDAD_DUR_MM_ADES_DFLT, Len.Int.WDAD_DUR_MM_ADES_DFLT);
    }

    public byte[] getWdadDurMmAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_DUR_MM_ADES_DFLT, Pos.WDAD_DUR_MM_ADES_DFLT);
        return buffer;
    }

    public void setWdadDurMmAdesDfltNull(String wdadDurMmAdesDfltNull) {
        writeString(Pos.WDAD_DUR_MM_ADES_DFLT_NULL, wdadDurMmAdesDfltNull, Len.WDAD_DUR_MM_ADES_DFLT_NULL);
    }

    /**Original name: WDAD-DUR-MM-ADES-DFLT-NULL<br>*/
    public String getWdadDurMmAdesDfltNull() {
        return readString(Pos.WDAD_DUR_MM_ADES_DFLT_NULL, Len.WDAD_DUR_MM_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_DUR_MM_ADES_DFLT = 1;
        public static final int WDAD_DUR_MM_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_DUR_MM_ADES_DFLT = 3;
        public static final int WDAD_DUR_MM_ADES_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_DUR_MM_ADES_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
