package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: AREA-OUT-SCARTI<br>
 * Variable: AREA-OUT-SCARTI from program LLBS0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AreaOutScarti {

    //==== PROPERTIES ====
    //Original name: SCA-POL-IB-OGG
    private String polIbOgg = DefaultValues.stringVal(Len.POL_IB_OGG);
    //Original name: SCA-POL-ID-POLI
    private String polIdPoli = DefaultValues.stringVal(Len.POL_ID_POLI);
    //Original name: SCA-ADE-ID-ADES
    private String adeIdAdes = DefaultValues.stringVal(Len.ADE_ID_ADES);
    //Original name: FILLER-AREA-OUT-SCARTI
    private String flr1 = DefaultValues.stringVal(Len.FLR1);

    //==== METHODS ====
    public String getAreaOutScartiFormatted() {
        return MarshalByteExt.bufferToStr(getAreaOutScartiBytes());
    }

    public byte[] getAreaOutScartiBytes() {
        byte[] buffer = new byte[Len.AREA_OUT_SCARTI];
        return getAreaOutScartiBytes(buffer, 1);
    }

    public byte[] getAreaOutScartiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, polIbOgg, Len.POL_IB_OGG);
        position += Len.POL_IB_OGG;
        MarshalByte.writeString(buffer, position, polIdPoli, Len.POL_ID_POLI);
        position += Len.POL_ID_POLI;
        MarshalByte.writeString(buffer, position, adeIdAdes, Len.ADE_ID_ADES);
        position += Len.ADE_ID_ADES;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        return buffer;
    }

    public void setPolIbOgg(String polIbOgg) {
        this.polIbOgg = Functions.subString(polIbOgg, Len.POL_IB_OGG);
    }

    public String getPolIbOgg() {
        return this.polIbOgg;
    }

    public void setPolIdPoli(int polIdPoli) {
        this.polIdPoli = NumericDisplay.asString(polIdPoli, Len.POL_ID_POLI);
    }

    public void setPolIdPoliFormatted(String polIdPoli) {
        this.polIdPoli = Trunc.toUnsignedNumeric(polIdPoli, Len.POL_ID_POLI);
    }

    public int getPolIdPoli() {
        return NumericDisplay.asInt(this.polIdPoli);
    }

    public void setAdeIdAdes(int adeIdAdes) {
        this.adeIdAdes = NumericDisplay.asString(adeIdAdes, Len.ADE_ID_ADES);
    }

    public void setAdeIdAdesFormatted(String adeIdAdes) {
        this.adeIdAdes = Trunc.toUnsignedNumeric(adeIdAdes, Len.ADE_ID_ADES);
    }

    public int getAdeIdAdes() {
        return NumericDisplay.asInt(this.adeIdAdes);
    }

    public String getFlr1() {
        return this.flr1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_IB_OGG = 40;
        public static final int POL_ID_POLI = 9;
        public static final int ADE_ID_ADES = 9;
        public static final int FLR1 = 242;
        public static final int AREA_OUT_SCARTI = POL_IB_OGG + POL_ID_POLI + ADE_ID_ADES + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
