package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PRE-PP-INI<br>
 * Variable: W-B03-PRE-PP-INI from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03PrePpIniLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03PrePpIniLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PRE_PP_INI;
    }

    public void setwB03PrePpIni(AfDecimal wB03PrePpIni) {
        writeDecimalAsPacked(Pos.W_B03_PRE_PP_INI, wB03PrePpIni.copy());
    }

    /**Original name: W-B03-PRE-PP-INI<br>*/
    public AfDecimal getwB03PrePpIni() {
        return readPackedAsDecimal(Pos.W_B03_PRE_PP_INI, Len.Int.W_B03_PRE_PP_INI, Len.Fract.W_B03_PRE_PP_INI);
    }

    public byte[] getwB03PrePpIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PRE_PP_INI, Pos.W_B03_PRE_PP_INI);
        return buffer;
    }

    public void setwB03PrePpIniNull(String wB03PrePpIniNull) {
        writeString(Pos.W_B03_PRE_PP_INI_NULL, wB03PrePpIniNull, Len.W_B03_PRE_PP_INI_NULL);
    }

    /**Original name: W-B03-PRE-PP-INI-NULL<br>*/
    public String getwB03PrePpIniNull() {
        return readString(Pos.W_B03_PRE_PP_INI_NULL, Len.W_B03_PRE_PP_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_PP_INI = 1;
        public static final int W_B03_PRE_PP_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_PP_INI = 8;
        public static final int W_B03_PRE_PP_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_PP_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_PP_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
