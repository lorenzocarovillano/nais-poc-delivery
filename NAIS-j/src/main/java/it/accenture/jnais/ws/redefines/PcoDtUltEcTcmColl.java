package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-EC-TCM-COLL<br>
 * Variable: PCO-DT-ULT-EC-TCM-COLL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEcTcmColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEcTcmColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_EC_TCM_COLL;
    }

    public void setPcoDtUltEcTcmColl(int pcoDtUltEcTcmColl) {
        writeIntAsPacked(Pos.PCO_DT_ULT_EC_TCM_COLL, pcoDtUltEcTcmColl, Len.Int.PCO_DT_ULT_EC_TCM_COLL);
    }

    public void setPcoDtUltEcTcmCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_EC_TCM_COLL, Pos.PCO_DT_ULT_EC_TCM_COLL);
    }

    /**Original name: PCO-DT-ULT-EC-TCM-COLL<br>*/
    public int getPcoDtUltEcTcmColl() {
        return readPackedAsInt(Pos.PCO_DT_ULT_EC_TCM_COLL, Len.Int.PCO_DT_ULT_EC_TCM_COLL);
    }

    public byte[] getPcoDtUltEcTcmCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_EC_TCM_COLL, Pos.PCO_DT_ULT_EC_TCM_COLL);
        return buffer;
    }

    public void initPcoDtUltEcTcmCollHighValues() {
        fill(Pos.PCO_DT_ULT_EC_TCM_COLL, Len.PCO_DT_ULT_EC_TCM_COLL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEcTcmCollNull(String pcoDtUltEcTcmCollNull) {
        writeString(Pos.PCO_DT_ULT_EC_TCM_COLL_NULL, pcoDtUltEcTcmCollNull, Len.PCO_DT_ULT_EC_TCM_COLL_NULL);
    }

    /**Original name: PCO-DT-ULT-EC-TCM-COLL-NULL<br>*/
    public String getPcoDtUltEcTcmCollNull() {
        return readString(Pos.PCO_DT_ULT_EC_TCM_COLL_NULL, Len.PCO_DT_ULT_EC_TCM_COLL_NULL);
    }

    public String getPcoDtUltEcTcmCollNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEcTcmCollNull(), Len.PCO_DT_ULT_EC_TCM_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_TCM_COLL = 1;
        public static final int PCO_DT_ULT_EC_TCM_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_TCM_COLL = 5;
        public static final int PCO_DT_ULT_EC_TCM_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_EC_TCM_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
