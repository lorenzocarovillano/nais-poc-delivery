package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-REMUN-ASS<br>
 * Variable: DTC-REMUN-ASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_REMUN_ASS;
    }

    public void setDtcRemunAss(AfDecimal dtcRemunAss) {
        writeDecimalAsPacked(Pos.DTC_REMUN_ASS, dtcRemunAss.copy());
    }

    public void setDtcRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_REMUN_ASS, Pos.DTC_REMUN_ASS);
    }

    /**Original name: DTC-REMUN-ASS<br>*/
    public AfDecimal getDtcRemunAss() {
        return readPackedAsDecimal(Pos.DTC_REMUN_ASS, Len.Int.DTC_REMUN_ASS, Len.Fract.DTC_REMUN_ASS);
    }

    public byte[] getDtcRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_REMUN_ASS, Pos.DTC_REMUN_ASS);
        return buffer;
    }

    public void setDtcRemunAssNull(String dtcRemunAssNull) {
        writeString(Pos.DTC_REMUN_ASS_NULL, dtcRemunAssNull, Len.DTC_REMUN_ASS_NULL);
    }

    /**Original name: DTC-REMUN-ASS-NULL<br>*/
    public String getDtcRemunAssNull() {
        return readString(Pos.DTC_REMUN_ASS_NULL, Len.DTC_REMUN_ASS_NULL);
    }

    public String getDtcRemunAssNullFormatted() {
        return Functions.padBlanks(getDtcRemunAssNull(), Len.DTC_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_REMUN_ASS = 1;
        public static final int DTC_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_REMUN_ASS = 8;
        public static final int DTC_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
