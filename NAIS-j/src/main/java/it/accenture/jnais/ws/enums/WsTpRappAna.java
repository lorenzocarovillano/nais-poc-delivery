package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-RAPP-ANA<br>
 * Variable: WS-TP-RAPP-ANA from copybook LCCVXRA0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpRappAna {

    //==== PROPERTIES ====
    private String value = "";
    public static final String CONTRAENTE = "CO";
    public static final String ASSICURATO = "AS";
    public static final String ADERENTE = "AD";
    public static final String LEGALE_RAPPR = "LR";
    public static final String BENEFI_DISPO = "BD";
    public static final String BENEFI_LIQUI = "BL";
    public static final String VINCOLATARIO = "VI";
    public static final String PERCIP_LIQUI = "PL";
    public static final String CREDIT_PIGNO = "CP";
    public static final String REVERS_ASSIC = "RA";
    public static final String DELEGATARIO = "DE";
    public static final String PERCIP_DISPO = "PD";
    public static final String PERCIP_REND = "PR";
    public static final String CONTRAENTE_CE = "CE";
    public static final String PERCIP_CEDOLE = "PC";
    public static final String PERC_RISC_PARZ_PROG = "PP";
    public static final String PAGATORE = "PG";
    public static final String PAGATORE_TFR = "PT";
    public static final String PAGATORE_FND_CED = "PF";
    public static final String PAGAT_VERS_AGG = "PV";
    public static final String TUTORE = "TU";
    public static final String PAGATORE_PERFEZ = "PZ";
    public static final String PAGAT_RATE_SUCC = "PS";
    public static final String RAPP_LEG_PAGAT = "RP";
    public static final String PAGAT_PREMI_SUCC = "PA";
    public static final String TITOLARE_EFFETTIVO = "TE";
    public static final String TERZO_ESECUTORE = "TS";
    public static final String ALTRO_TERZO_ESEC = "TA";
    public static final String BENEFI_REDDITO_PROG = "B1";
    public static final String BENEFI_TAKE_PROFIT = "B2";
    public static final String ESECUTORE = "ES";
    public static final String TITOLARE_EFFETTIVO_BEN = "TB";

    //==== METHODS ====
    public void setWsTpRappAna(String wsTpRappAna) {
        this.value = Functions.subString(wsTpRappAna, Len.WS_TP_RAPP_ANA);
    }

    public String getWsTpRappAna() {
        return this.value;
    }

    public void setContraente() {
        value = CONTRAENTE;
    }

    public void setAssicurato() {
        value = ASSICURATO;
    }

    public void setAderente() {
        value = ADERENTE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_RAPP_ANA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
