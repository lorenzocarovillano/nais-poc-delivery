package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.AreaLdbve391I;

/**Original name: LDBVE391<br>
 * Variable: LDBVE391 from copybook LDBVE391<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbve391 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: AREA-LDBVE391-I
    private AreaLdbve391I areaLdbve391I = new AreaLdbve391I();
    //Original name: LDBVE391-IMP-BOLLO-DETT-C
    private AfDecimal ldbve391ImpBolloDettC = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LDBVE391-IMP-BOLLO-DETT-V
    private AfDecimal ldbve391ImpBolloDettV = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LDBVE391-IMP-BOLLO-TOT-V
    private AfDecimal ldbve391ImpBolloTotV = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LDBVE391-IMP-BOLLO-TOT-R
    private AfDecimal ldbve391ImpBolloTotR = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBVE391;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbve391Bytes(buf);
    }

    public String getLdbve391Formatted() {
        return MarshalByteExt.bufferToStr(getLdbve391Bytes());
    }

    public void setLdbve391Bytes(byte[] buffer) {
        setLdbve391Bytes(buffer, 1);
    }

    public byte[] getLdbve391Bytes() {
        byte[] buffer = new byte[Len.LDBVE391];
        return getLdbve391Bytes(buffer, 1);
    }

    public void setLdbve391Bytes(byte[] buffer, int offset) {
        int position = offset;
        areaLdbve391I.setAreaLdbve391IBytes(buffer, position);
        position += AreaLdbve391I.Len.AREA_LDBVE391_I;
        setAreaLdbve391OBytes(buffer, position);
    }

    public byte[] getLdbve391Bytes(byte[] buffer, int offset) {
        int position = offset;
        areaLdbve391I.getAreaLdbve391IBytes(buffer, position);
        position += AreaLdbve391I.Len.AREA_LDBVE391_I;
        getAreaLdbve391OBytes(buffer, position);
        return buffer;
    }

    public void setAreaLdbve391OBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbve391ImpBolloDettC.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LDBVE391_IMP_BOLLO_DETT_C, Len.Fract.LDBVE391_IMP_BOLLO_DETT_C));
        position += Len.LDBVE391_IMP_BOLLO_DETT_C;
        ldbve391ImpBolloDettV.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LDBVE391_IMP_BOLLO_DETT_V, Len.Fract.LDBVE391_IMP_BOLLO_DETT_V));
        position += Len.LDBVE391_IMP_BOLLO_DETT_V;
        ldbve391ImpBolloTotV.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LDBVE391_IMP_BOLLO_TOT_V, Len.Fract.LDBVE391_IMP_BOLLO_TOT_V));
        position += Len.LDBVE391_IMP_BOLLO_TOT_V;
        ldbve391ImpBolloTotR.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LDBVE391_IMP_BOLLO_TOT_R, Len.Fract.LDBVE391_IMP_BOLLO_TOT_R));
    }

    public byte[] getAreaLdbve391OBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimalAsPacked(buffer, position, ldbve391ImpBolloDettC.copy());
        position += Len.LDBVE391_IMP_BOLLO_DETT_C;
        MarshalByte.writeDecimalAsPacked(buffer, position, ldbve391ImpBolloDettV.copy());
        position += Len.LDBVE391_IMP_BOLLO_DETT_V;
        MarshalByte.writeDecimalAsPacked(buffer, position, ldbve391ImpBolloTotV.copy());
        position += Len.LDBVE391_IMP_BOLLO_TOT_V;
        MarshalByte.writeDecimalAsPacked(buffer, position, ldbve391ImpBolloTotR.copy());
        return buffer;
    }

    public void setLdbve391ImpBolloDettC(AfDecimal ldbve391ImpBolloDettC) {
        this.ldbve391ImpBolloDettC.assign(ldbve391ImpBolloDettC);
    }

    public AfDecimal getLdbve391ImpBolloDettC() {
        return this.ldbve391ImpBolloDettC.copy();
    }

    public void setLdbve391ImpBolloDettV(AfDecimal ldbve391ImpBolloDettV) {
        this.ldbve391ImpBolloDettV.assign(ldbve391ImpBolloDettV);
    }

    public AfDecimal getLdbve391ImpBolloDettV() {
        return this.ldbve391ImpBolloDettV.copy();
    }

    public void setLdbve391ImpBolloTotV(AfDecimal ldbve391ImpBolloTotV) {
        this.ldbve391ImpBolloTotV.assign(ldbve391ImpBolloTotV);
    }

    public AfDecimal getLdbve391ImpBolloTotV() {
        return this.ldbve391ImpBolloTotV.copy();
    }

    public void setLdbve391ImpBolloTotR(AfDecimal ldbve391ImpBolloTotR) {
        this.ldbve391ImpBolloTotR.assign(ldbve391ImpBolloTotR);
    }

    public AfDecimal getLdbve391ImpBolloTotR() {
        return this.ldbve391ImpBolloTotR.copy();
    }

    public AreaLdbve391I getAreaLdbve391I() {
        return areaLdbve391I;
    }

    @Override
    public byte[] serialize() {
        return getLdbve391Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBVE391_IMP_BOLLO_DETT_C = 8;
        public static final int LDBVE391_IMP_BOLLO_DETT_V = 8;
        public static final int LDBVE391_IMP_BOLLO_TOT_V = 8;
        public static final int LDBVE391_IMP_BOLLO_TOT_R = 8;
        public static final int AREA_LDBVE391_O = LDBVE391_IMP_BOLLO_DETT_C + LDBVE391_IMP_BOLLO_DETT_V + LDBVE391_IMP_BOLLO_TOT_V + LDBVE391_IMP_BOLLO_TOT_R;
        public static final int LDBVE391 = AreaLdbve391I.Len.AREA_LDBVE391_I + AREA_LDBVE391_O;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBVE391_IMP_BOLLO_DETT_C = 12;
            public static final int LDBVE391_IMP_BOLLO_DETT_V = 12;
            public static final int LDBVE391_IMP_BOLLO_TOT_V = 12;
            public static final int LDBVE391_IMP_BOLLO_TOT_R = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LDBVE391_IMP_BOLLO_DETT_C = 3;
            public static final int LDBVE391_IMP_BOLLO_DETT_V = 3;
            public static final int LDBVE391_IMP_BOLLO_TOT_V = 3;
            public static final int LDBVE391_IMP_BOLLO_TOT_R = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
