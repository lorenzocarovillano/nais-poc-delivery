package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMP-NET-LIQTO<br>
 * Variable: LQU-TOT-IMP-NET-LIQTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpNetLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpNetLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMP_NET_LIQTO;
    }

    public void setLquTotImpNetLiqto(AfDecimal lquTotImpNetLiqto) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMP_NET_LIQTO, lquTotImpNetLiqto.copy());
    }

    public void setLquTotImpNetLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMP_NET_LIQTO, Pos.LQU_TOT_IMP_NET_LIQTO);
    }

    /**Original name: LQU-TOT-IMP-NET-LIQTO<br>*/
    public AfDecimal getLquTotImpNetLiqto() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMP_NET_LIQTO, Len.Int.LQU_TOT_IMP_NET_LIQTO, Len.Fract.LQU_TOT_IMP_NET_LIQTO);
    }

    public byte[] getLquTotImpNetLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMP_NET_LIQTO, Pos.LQU_TOT_IMP_NET_LIQTO);
        return buffer;
    }

    public void setLquTotImpNetLiqtoNull(String lquTotImpNetLiqtoNull) {
        writeString(Pos.LQU_TOT_IMP_NET_LIQTO_NULL, lquTotImpNetLiqtoNull, Len.LQU_TOT_IMP_NET_LIQTO_NULL);
    }

    /**Original name: LQU-TOT-IMP-NET-LIQTO-NULL<br>*/
    public String getLquTotImpNetLiqtoNull() {
        return readString(Pos.LQU_TOT_IMP_NET_LIQTO_NULL, Len.LQU_TOT_IMP_NET_LIQTO_NULL);
    }

    public String getLquTotImpNetLiqtoNullFormatted() {
        return Functions.padBlanks(getLquTotImpNetLiqtoNull(), Len.LQU_TOT_IMP_NET_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_NET_LIQTO = 1;
        public static final int LQU_TOT_IMP_NET_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_NET_LIQTO = 8;
        public static final int LQU_TOT_IMP_NET_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_NET_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_NET_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
