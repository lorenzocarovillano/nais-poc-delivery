package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-PP-INI<br>
 * Variable: WTGA-PRE-PP-INI from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPrePpIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPrePpIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_PP_INI;
    }

    public void setWtgaPrePpIni(AfDecimal wtgaPrePpIni) {
        writeDecimalAsPacked(Pos.WTGA_PRE_PP_INI, wtgaPrePpIni.copy());
    }

    public void setWtgaPrePpIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_PP_INI, Pos.WTGA_PRE_PP_INI);
    }

    /**Original name: WTGA-PRE-PP-INI<br>*/
    public AfDecimal getWtgaPrePpIni() {
        return readPackedAsDecimal(Pos.WTGA_PRE_PP_INI, Len.Int.WTGA_PRE_PP_INI, Len.Fract.WTGA_PRE_PP_INI);
    }

    public byte[] getWtgaPrePpIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_PP_INI, Pos.WTGA_PRE_PP_INI);
        return buffer;
    }

    public void initWtgaPrePpIniSpaces() {
        fill(Pos.WTGA_PRE_PP_INI, Len.WTGA_PRE_PP_INI, Types.SPACE_CHAR);
    }

    public void setWtgaPrePpIniNull(String wtgaPrePpIniNull) {
        writeString(Pos.WTGA_PRE_PP_INI_NULL, wtgaPrePpIniNull, Len.WTGA_PRE_PP_INI_NULL);
    }

    /**Original name: WTGA-PRE-PP-INI-NULL<br>*/
    public String getWtgaPrePpIniNull() {
        return readString(Pos.WTGA_PRE_PP_INI_NULL, Len.WTGA_PRE_PP_INI_NULL);
    }

    public String getWtgaPrePpIniNullFormatted() {
        return Functions.padBlanks(getWtgaPrePpIniNull(), Len.WTGA_PRE_PP_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_PP_INI = 1;
        public static final int WTGA_PRE_PP_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_PP_INI = 8;
        public static final int WTGA_PRE_PP_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_PP_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_PP_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
