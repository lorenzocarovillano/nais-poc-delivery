package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0019-TIPO-AGG<br>
 * Variable: LCCC0019-TIPO-AGG from copybook LCCC0019<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0019TipoAgg {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TIPO_AGG);
    public static final String DEROGA_EV = "EV";
    public static final String DEROGA_SI = "SI";
    public static final String DEROGA_NO = "NO";
    public static final String DEROGA_ST = "SD";
    public static final String STORNO_SI = "ST";
    public static final String PRENOT_SI = "PR";
    public static final String STOPRE_SI = "SP";

    //==== METHODS ====
    public void setTipoAgg(String tipoAgg) {
        this.value = Functions.subString(tipoAgg, Len.TIPO_AGG);
    }

    public String getTipoAgg() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_AGG = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
