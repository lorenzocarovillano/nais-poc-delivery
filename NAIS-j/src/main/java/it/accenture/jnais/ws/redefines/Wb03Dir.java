package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DIR<br>
 * Variable: WB03-DIR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03Dir extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03Dir() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DIR;
    }

    public void setWb03DirFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DIR, Pos.WB03_DIR);
    }

    /**Original name: WB03-DIR<br>*/
    public AfDecimal getWb03Dir() {
        return readPackedAsDecimal(Pos.WB03_DIR, Len.Int.WB03_DIR, Len.Fract.WB03_DIR);
    }

    public byte[] getWb03DirAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DIR, Pos.WB03_DIR);
        return buffer;
    }

    public void setWb03DirNull(String wb03DirNull) {
        writeString(Pos.WB03_DIR_NULL, wb03DirNull, Len.WB03_DIR_NULL);
    }

    /**Original name: WB03-DIR-NULL<br>*/
    public String getWb03DirNull() {
        return readString(Pos.WB03_DIR_NULL, Len.WB03_DIR_NULL);
    }

    public String getWb03DirNullFormatted() {
        return Functions.padBlanks(getWb03DirNull(), Len.WB03_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DIR = 1;
        public static final int WB03_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DIR = 8;
        public static final int WB03_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
