package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-DT-CESSAZIONE<br>
 * Variable: WDFA-DT-CESSAZIONE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaDtCessazione extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaDtCessazione() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_DT_CESSAZIONE;
    }

    public void setWdfaDtCessazione(int wdfaDtCessazione) {
        writeIntAsPacked(Pos.WDFA_DT_CESSAZIONE, wdfaDtCessazione, Len.Int.WDFA_DT_CESSAZIONE);
    }

    public void setWdfaDtCessazioneFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_DT_CESSAZIONE, Pos.WDFA_DT_CESSAZIONE);
    }

    /**Original name: WDFA-DT-CESSAZIONE<br>*/
    public int getWdfaDtCessazione() {
        return readPackedAsInt(Pos.WDFA_DT_CESSAZIONE, Len.Int.WDFA_DT_CESSAZIONE);
    }

    public byte[] getWdfaDtCessazioneAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_DT_CESSAZIONE, Pos.WDFA_DT_CESSAZIONE);
        return buffer;
    }

    public void setWdfaDtCessazioneNull(String wdfaDtCessazioneNull) {
        writeString(Pos.WDFA_DT_CESSAZIONE_NULL, wdfaDtCessazioneNull, Len.WDFA_DT_CESSAZIONE_NULL);
    }

    /**Original name: WDFA-DT-CESSAZIONE-NULL<br>*/
    public String getWdfaDtCessazioneNull() {
        return readString(Pos.WDFA_DT_CESSAZIONE_NULL, Len.WDFA_DT_CESSAZIONE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_DT_CESSAZIONE = 1;
        public static final int WDFA_DT_CESSAZIONE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_DT_CESSAZIONE = 5;
        public static final int WDFA_DT_CESSAZIONE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_DT_CESSAZIONE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
