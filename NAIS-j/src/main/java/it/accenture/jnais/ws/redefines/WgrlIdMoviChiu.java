package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRL-ID-MOVI-CHIU<br>
 * Variable: WGRL-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrlIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrlIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRL_ID_MOVI_CHIU;
    }

    public void setWgrlIdMoviChiu(int wgrlIdMoviChiu) {
        writeIntAsPacked(Pos.WGRL_ID_MOVI_CHIU, wgrlIdMoviChiu, Len.Int.WGRL_ID_MOVI_CHIU);
    }

    public void setWgrlIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRL_ID_MOVI_CHIU, Pos.WGRL_ID_MOVI_CHIU);
    }

    /**Original name: WGRL-ID-MOVI-CHIU<br>*/
    public int getWgrlIdMoviChiu() {
        return readPackedAsInt(Pos.WGRL_ID_MOVI_CHIU, Len.Int.WGRL_ID_MOVI_CHIU);
    }

    public byte[] getWgrlIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRL_ID_MOVI_CHIU, Pos.WGRL_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWgrlIdMoviChiuSpaces() {
        fill(Pos.WGRL_ID_MOVI_CHIU, Len.WGRL_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWgrlIdMoviChiuNull(String wgrlIdMoviChiuNull) {
        writeString(Pos.WGRL_ID_MOVI_CHIU_NULL, wgrlIdMoviChiuNull, Len.WGRL_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WGRL-ID-MOVI-CHIU-NULL<br>*/
    public String getWgrlIdMoviChiuNull() {
        return readString(Pos.WGRL_ID_MOVI_CHIU_NULL, Len.WGRL_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRL_ID_MOVI_CHIU = 1;
        public static final int WGRL_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRL_ID_MOVI_CHIU = 5;
        public static final int WGRL_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRL_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
