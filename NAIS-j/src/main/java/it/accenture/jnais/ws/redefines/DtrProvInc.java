package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-PROV-INC<br>
 * Variable: DTR-PROV-INC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_PROV_INC;
    }

    public void setDtrProvInc(AfDecimal dtrProvInc) {
        writeDecimalAsPacked(Pos.DTR_PROV_INC, dtrProvInc.copy());
    }

    public void setDtrProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_PROV_INC, Pos.DTR_PROV_INC);
    }

    /**Original name: DTR-PROV-INC<br>*/
    public AfDecimal getDtrProvInc() {
        return readPackedAsDecimal(Pos.DTR_PROV_INC, Len.Int.DTR_PROV_INC, Len.Fract.DTR_PROV_INC);
    }

    public byte[] getDtrProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_PROV_INC, Pos.DTR_PROV_INC);
        return buffer;
    }

    public void setDtrProvIncNull(String dtrProvIncNull) {
        writeString(Pos.DTR_PROV_INC_NULL, dtrProvIncNull, Len.DTR_PROV_INC_NULL);
    }

    /**Original name: DTR-PROV-INC-NULL<br>*/
    public String getDtrProvIncNull() {
        return readString(Pos.DTR_PROV_INC_NULL, Len.DTR_PROV_INC_NULL);
    }

    public String getDtrProvIncNullFormatted() {
        return Functions.padBlanks(getDtrProvIncNull(), Len.DTR_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_PROV_INC = 1;
        public static final int DTR_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_PROV_INC = 8;
        public static final int DTR_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
