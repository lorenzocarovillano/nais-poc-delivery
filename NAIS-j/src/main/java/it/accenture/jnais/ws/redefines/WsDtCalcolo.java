package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WS-DT-CALCOLO<br>
 * Variable: WS-DT-CALCOLO from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsDtCalcolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsDtCalcolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_DT_CALCOLO;
    }

    public void setWsDtCalcolo(String wsDtCalcolo) {
        writeString(Pos.WS_DT_CALCOLO, wsDtCalcolo, Len.WS_DT_CALCOLO);
    }

    /**Original name: WS-DT-CALCOLO<br>*/
    public String getWsDtCalcolo() {
        return readString(Pos.WS_DT_CALCOLO, Len.WS_DT_CALCOLO);
    }

    /**Original name: WS-DT-CALCOLO-NUM<br>*/
    public int getWsDtCalcoloNum() {
        return readNumDispUnsignedInt(Pos.WS_DT_CALCOLO_NUM, Len.WS_DT_CALCOLO_NUM);
    }

    public String getWsDtCalcoloNumFormatted() {
        return readFixedString(Pos.WS_DT_CALCOLO_NUM, Len.WS_DT_CALCOLO_NUM);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_DT_CALCOLO = 1;
        public static final int WS_DT_CALCOLO_NUM = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_DT_CALCOLO = 8;
        public static final int WS_DT_CALCOLO_NUM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
