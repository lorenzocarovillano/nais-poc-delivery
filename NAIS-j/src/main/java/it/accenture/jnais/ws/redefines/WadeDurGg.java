package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WADE-DUR-GG<br>
 * Variable: WADE-DUR-GG from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeDurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeDurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_DUR_GG;
    }

    public void setWadeDurGg(int wadeDurGg) {
        writeIntAsPacked(Pos.WADE_DUR_GG, wadeDurGg, Len.Int.WADE_DUR_GG);
    }

    public void setWadeDurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_DUR_GG, Pos.WADE_DUR_GG);
    }

    /**Original name: WADE-DUR-GG<br>*/
    public int getWadeDurGg() {
        return readPackedAsInt(Pos.WADE_DUR_GG, Len.Int.WADE_DUR_GG);
    }

    public byte[] getWadeDurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_DUR_GG, Pos.WADE_DUR_GG);
        return buffer;
    }

    public void initWadeDurGgSpaces() {
        fill(Pos.WADE_DUR_GG, Len.WADE_DUR_GG, Types.SPACE_CHAR);
    }

    public void setWadeDurGgNull(String wadeDurGgNull) {
        writeString(Pos.WADE_DUR_GG_NULL, wadeDurGgNull, Len.WADE_DUR_GG_NULL);
    }

    /**Original name: WADE-DUR-GG-NULL<br>*/
    public String getWadeDurGgNull() {
        return readString(Pos.WADE_DUR_GG_NULL, Len.WADE_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_DUR_GG = 1;
        public static final int WADE_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_DUR_GG = 3;
        public static final int WADE_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
