package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-RIT-ACC-CALC<br>
 * Variable: WDFL-IMPB-RIT-ACC-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbRitAccCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbRitAccCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_RIT_ACC_CALC;
    }

    public void setWdflImpbRitAccCalc(AfDecimal wdflImpbRitAccCalc) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_RIT_ACC_CALC, wdflImpbRitAccCalc.copy());
    }

    public void setWdflImpbRitAccCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_RIT_ACC_CALC, Pos.WDFL_IMPB_RIT_ACC_CALC);
    }

    /**Original name: WDFL-IMPB-RIT-ACC-CALC<br>*/
    public AfDecimal getWdflImpbRitAccCalc() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_RIT_ACC_CALC, Len.Int.WDFL_IMPB_RIT_ACC_CALC, Len.Fract.WDFL_IMPB_RIT_ACC_CALC);
    }

    public byte[] getWdflImpbRitAccCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_RIT_ACC_CALC, Pos.WDFL_IMPB_RIT_ACC_CALC);
        return buffer;
    }

    public void setWdflImpbRitAccCalcNull(String wdflImpbRitAccCalcNull) {
        writeString(Pos.WDFL_IMPB_RIT_ACC_CALC_NULL, wdflImpbRitAccCalcNull, Len.WDFL_IMPB_RIT_ACC_CALC_NULL);
    }

    /**Original name: WDFL-IMPB-RIT-ACC-CALC-NULL<br>*/
    public String getWdflImpbRitAccCalcNull() {
        return readString(Pos.WDFL_IMPB_RIT_ACC_CALC_NULL, Len.WDFL_IMPB_RIT_ACC_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_RIT_ACC_CALC = 1;
        public static final int WDFL_IMPB_RIT_ACC_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_RIT_ACC_CALC = 8;
        public static final int WDFL_IMPB_RIT_ACC_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_RIT_ACC_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_RIT_ACC_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
