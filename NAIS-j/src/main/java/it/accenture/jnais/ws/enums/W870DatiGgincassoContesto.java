package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: W870-DATI-GGINCASSO-CONTESTO<br>
 * Variable: W870-DATI-GGINCASSO-CONTESTO from copybook LOAC0870<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class W870DatiGgincassoContesto {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.W870_DATI_GGINCASSO_CONTESTO);
    public static final String SI = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setW870DatiGgincassoContesto(String w870DatiGgincassoContesto) {
        this.value = Functions.subString(w870DatiGgincassoContesto, Len.W870_DATI_GGINCASSO_CONTESTO);
    }

    public String getW870DatiGgincassoContesto() {
        return this.value;
    }

    public void setW870GgcontestoContNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int W870_DATI_GGINCASSO_CONTESTO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
