package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-IMP-ADER<br>
 * Variable: DTC-IMP-ADER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_IMP_ADER;
    }

    public void setDtcImpAder(AfDecimal dtcImpAder) {
        writeDecimalAsPacked(Pos.DTC_IMP_ADER, dtcImpAder.copy());
    }

    public void setDtcImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_IMP_ADER, Pos.DTC_IMP_ADER);
    }

    /**Original name: DTC-IMP-ADER<br>*/
    public AfDecimal getDtcImpAder() {
        return readPackedAsDecimal(Pos.DTC_IMP_ADER, Len.Int.DTC_IMP_ADER, Len.Fract.DTC_IMP_ADER);
    }

    public byte[] getDtcImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_IMP_ADER, Pos.DTC_IMP_ADER);
        return buffer;
    }

    public void setDtcImpAderNull(String dtcImpAderNull) {
        writeString(Pos.DTC_IMP_ADER_NULL, dtcImpAderNull, Len.DTC_IMP_ADER_NULL);
    }

    /**Original name: DTC-IMP-ADER-NULL<br>*/
    public String getDtcImpAderNull() {
        return readString(Pos.DTC_IMP_ADER_NULL, Len.DTC_IMP_ADER_NULL);
    }

    public String getDtcImpAderNullFormatted() {
        return Functions.padBlanks(getDtcImpAderNull(), Len.DTC_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_IMP_ADER = 1;
        public static final int DTC_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_IMP_ADER = 8;
        public static final int DTC_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
