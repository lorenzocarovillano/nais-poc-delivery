package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-DT-ACCNS-RAPP-FND<br>
 * Variable: WDFA-DT-ACCNS-RAPP-FND from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaDtAccnsRappFnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaDtAccnsRappFnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_DT_ACCNS_RAPP_FND;
    }

    public void setWdfaDtAccnsRappFnd(int wdfaDtAccnsRappFnd) {
        writeIntAsPacked(Pos.WDFA_DT_ACCNS_RAPP_FND, wdfaDtAccnsRappFnd, Len.Int.WDFA_DT_ACCNS_RAPP_FND);
    }

    public void setWdfaDtAccnsRappFndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_DT_ACCNS_RAPP_FND, Pos.WDFA_DT_ACCNS_RAPP_FND);
    }

    /**Original name: WDFA-DT-ACCNS-RAPP-FND<br>*/
    public int getWdfaDtAccnsRappFnd() {
        return readPackedAsInt(Pos.WDFA_DT_ACCNS_RAPP_FND, Len.Int.WDFA_DT_ACCNS_RAPP_FND);
    }

    public byte[] getWdfaDtAccnsRappFndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_DT_ACCNS_RAPP_FND, Pos.WDFA_DT_ACCNS_RAPP_FND);
        return buffer;
    }

    public void setWdfaDtAccnsRappFndNull(String wdfaDtAccnsRappFndNull) {
        writeString(Pos.WDFA_DT_ACCNS_RAPP_FND_NULL, wdfaDtAccnsRappFndNull, Len.WDFA_DT_ACCNS_RAPP_FND_NULL);
    }

    /**Original name: WDFA-DT-ACCNS-RAPP-FND-NULL<br>*/
    public String getWdfaDtAccnsRappFndNull() {
        return readString(Pos.WDFA_DT_ACCNS_RAPP_FND_NULL, Len.WDFA_DT_ACCNS_RAPP_FND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_DT_ACCNS_RAPP_FND = 1;
        public static final int WDFA_DT_ACCNS_RAPP_FND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_DT_ACCNS_RAPP_FND = 5;
        public static final int WDFA_DT_ACCNS_RAPP_FND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_DT_ACCNS_RAPP_FND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
