package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-ALT-SOPR<br>
 * Variable: L3421-IMP-ALT-SOPR from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpAltSopr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpAltSopr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_ALT_SOPR;
    }

    public void setL3421ImpAltSoprFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_ALT_SOPR, Pos.L3421_IMP_ALT_SOPR);
    }

    /**Original name: L3421-IMP-ALT-SOPR<br>*/
    public AfDecimal getL3421ImpAltSopr() {
        return readPackedAsDecimal(Pos.L3421_IMP_ALT_SOPR, Len.Int.L3421_IMP_ALT_SOPR, Len.Fract.L3421_IMP_ALT_SOPR);
    }

    public byte[] getL3421ImpAltSoprAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_ALT_SOPR, Pos.L3421_IMP_ALT_SOPR);
        return buffer;
    }

    /**Original name: L3421-IMP-ALT-SOPR-NULL<br>*/
    public String getL3421ImpAltSoprNull() {
        return readString(Pos.L3421_IMP_ALT_SOPR_NULL, Len.L3421_IMP_ALT_SOPR_NULL);
    }

    public String getL3421ImpAltSoprNullFormatted() {
        return Functions.padBlanks(getL3421ImpAltSoprNull(), Len.L3421_IMP_ALT_SOPR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_ALT_SOPR = 1;
        public static final int L3421_IMP_ALT_SOPR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_ALT_SOPR = 8;
        public static final int L3421_IMP_ALT_SOPR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_ALT_SOPR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_ALT_SOPR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
