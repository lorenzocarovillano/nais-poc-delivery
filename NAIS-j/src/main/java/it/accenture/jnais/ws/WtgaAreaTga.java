package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.redefines.WtgaTabLlbs0230;

/**Original name: WTGA-AREA-TGA<br>
 * Variable: WTGA-AREA-TGA from program LLBS0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WtgaAreaTga {

    //==== PROPERTIES ====
    //Original name: WTGA-ELE-TGA-MAX
    private short wtgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTGA-TAB
    private WtgaTabLlbs0230 wtgaTab = new WtgaTabLlbs0230();

    //==== METHODS ====
    public String getWtgaAreaTgaFormatted() {
        return MarshalByteExt.bufferToStr(getWtgaAreaTgaBytes());
    }

    public byte[] getWtgaAreaTgaBytes() {
        byte[] buffer = new byte[Len.WTGA_AREA_TGA];
        return getWtgaAreaTgaBytes(buffer, 1);
    }

    public byte[] getWtgaAreaTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtgaEleTgaMax);
        position += Types.SHORT_SIZE;
        wtgaTab.getWtgaTabBytes(buffer, position);
        return buffer;
    }

    public void setWtgaEleTgaMax(short wtgaEleTgaMax) {
        this.wtgaEleTgaMax = wtgaEleTgaMax;
    }

    public short getWtgaEleTgaMax() {
        return this.wtgaEleTgaMax;
    }

    public WtgaTabLlbs0230 getWtgaTab() {
        return wtgaTab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ELE_TGA_MAX = 2;
        public static final int WTGA_AREA_TGA = WTGA_ELE_TGA_MAX + WtgaTabLlbs0230.Len.WTGA_TAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
