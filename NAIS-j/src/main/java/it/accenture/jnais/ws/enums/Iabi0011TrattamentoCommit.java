package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABI0011-TRATTAMENTO-COMMIT<br>
 * Variable: IABI0011-TRATTAMENTO-COMMIT from copybook IABI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabi0011TrattamentoCommit {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TRATTAMENTO_COMMIT);
    public static final String PARTIAL = "PARTIAL";
    public static final String FULL = "FULL";

    //==== METHODS ====
    public void setTrattamentoCommit(String trattamentoCommit) {
        this.value = Functions.subString(trattamentoCommit, Len.TRATTAMENTO_COMMIT);
    }

    public String getTrattamentoCommit() {
        return this.value;
    }

    public String getTrattamentoCommitFormatted() {
        return Functions.padBlanks(getTrattamentoCommit(), Len.TRATTAMENTO_COMMIT);
    }

    public boolean isPartial() {
        return value.equals(PARTIAL);
    }

    public boolean isFull() {
        return value.equals(FULL);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TRATTAMENTO_COMMIT = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
