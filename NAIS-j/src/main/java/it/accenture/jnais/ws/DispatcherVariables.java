package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Idsi0011Area;

/**Original name: DISPATCHER-VARIABLES<br>
 * Variable: DISPATCHER-VARIABLES from program IDSS0160<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DispatcherVariables extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IDSI0011-AREA
    private Idsi0011Area idsi0011Area = new Idsi0011Area();
    //Original name: IDSO0011-AREA
    private Idso0011Area idso0011Area = new Idso0011Area();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DISPATCHER_VARIABLES;
    }

    @Override
    public void deserialize(byte[] buf) {
        setDispatcherVariablesBytes(buf);
    }

    public String getDispatcherVariablesFormatted() {
        return MarshalByteExt.bufferToStr(getDispatcherVariablesBytes());
    }

    public void setDispatcherVariablesBytes(byte[] buffer) {
        setDispatcherVariablesBytes(buffer, 1);
    }

    public byte[] getDispatcherVariablesBytes() {
        byte[] buffer = new byte[Len.DISPATCHER_VARIABLES];
        return getDispatcherVariablesBytes(buffer, 1);
    }

    public void setDispatcherVariablesBytes(byte[] buffer, int offset) {
        int position = offset;
        idsi0011Area.setIdsi0011AreaBytes(buffer, position);
        position += Idsi0011Area.Len.IDSI0011_AREA;
        idso0011Area.setIdso0011AreaBytes(buffer, position);
    }

    public byte[] getDispatcherVariablesBytes(byte[] buffer, int offset) {
        int position = offset;
        idsi0011Area.getIdsi0011AreaBytes(buffer, position);
        position += Idsi0011Area.Len.IDSI0011_AREA;
        idso0011Area.getIdso0011AreaBytes(buffer, position);
        return buffer;
    }

    public Idsi0011Area getIdsi0011Area() {
        return idsi0011Area;
    }

    public Idso0011Area getIdso0011Area() {
        return idso0011Area;
    }

    @Override
    public byte[] serialize() {
        return getDispatcherVariablesBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DISPATCHER_VARIABLES = Idsi0011Area.Len.IDSI0011_AREA + Idso0011Area.Len.IDSO0011_AREA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
