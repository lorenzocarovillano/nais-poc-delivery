package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-TAX-SEP-EFFLQ<br>
 * Variable: WDFL-TAX-SEP-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflTaxSepEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflTaxSepEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_TAX_SEP_EFFLQ;
    }

    public void setWdflTaxSepEfflq(AfDecimal wdflTaxSepEfflq) {
        writeDecimalAsPacked(Pos.WDFL_TAX_SEP_EFFLQ, wdflTaxSepEfflq.copy());
    }

    public void setWdflTaxSepEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_TAX_SEP_EFFLQ, Pos.WDFL_TAX_SEP_EFFLQ);
    }

    /**Original name: WDFL-TAX-SEP-EFFLQ<br>*/
    public AfDecimal getWdflTaxSepEfflq() {
        return readPackedAsDecimal(Pos.WDFL_TAX_SEP_EFFLQ, Len.Int.WDFL_TAX_SEP_EFFLQ, Len.Fract.WDFL_TAX_SEP_EFFLQ);
    }

    public byte[] getWdflTaxSepEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_TAX_SEP_EFFLQ, Pos.WDFL_TAX_SEP_EFFLQ);
        return buffer;
    }

    public void setWdflTaxSepEfflqNull(String wdflTaxSepEfflqNull) {
        writeString(Pos.WDFL_TAX_SEP_EFFLQ_NULL, wdflTaxSepEfflqNull, Len.WDFL_TAX_SEP_EFFLQ_NULL);
    }

    /**Original name: WDFL-TAX-SEP-EFFLQ-NULL<br>*/
    public String getWdflTaxSepEfflqNull() {
        return readString(Pos.WDFL_TAX_SEP_EFFLQ_NULL, Len.WDFL_TAX_SEP_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_TAX_SEP_EFFLQ = 1;
        public static final int WDFL_TAX_SEP_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_TAX_SEP_EFFLQ = 8;
        public static final int WDFL_TAX_SEP_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_TAX_SEP_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_TAX_SEP_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
