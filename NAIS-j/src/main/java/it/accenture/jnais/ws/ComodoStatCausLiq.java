package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: COMODO-STAT-CAUS-LIQ<br>
 * Variable: COMODO-STAT-CAUS-LIQ from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ComodoStatCausLiq {

    //==== PROPERTIES ====
    //Original name: COM-TP-CAUS-01-LIQ
    private String caus01Liq = "ST";
    //Original name: COM-TP-CAUS-02-LIQ
    private String caus02Liq = "AR";
    //Original name: COM-TP-CAUS-03-LIQ
    private String caus03Liq = "RZ";
    //Original name: COM-TP-CAUS-04-LIQ
    private String caus04Liq = "RL";

    //==== METHODS ====
    public byte[] getComodoStatCausLiqBytes() {
        byte[] buffer = new byte[Len.COMODO_STAT_CAUS_LIQ];
        return getComodoStatCausLiqBytes(buffer, 1);
    }

    public byte[] getComodoStatCausLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, caus01Liq, Len.CAUS01_LIQ);
        position += Len.CAUS01_LIQ;
        MarshalByte.writeString(buffer, position, caus02Liq, Len.CAUS02_LIQ);
        position += Len.CAUS02_LIQ;
        MarshalByte.writeString(buffer, position, caus03Liq, Len.CAUS03_LIQ);
        position += Len.CAUS03_LIQ;
        MarshalByte.writeString(buffer, position, caus04Liq, Len.CAUS04_LIQ);
        return buffer;
    }

    public String getCaus01Liq() {
        return this.caus01Liq;
    }

    public String getCaus02Liq() {
        return this.caus02Liq;
    }

    public String getCaus03Liq() {
        return this.caus03Liq;
    }

    public String getCaus04Liq() {
        return this.caus04Liq;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CAUS01_LIQ = 2;
        public static final int CAUS02_LIQ = 2;
        public static final int CAUS03_LIQ = 2;
        public static final int CAUS04_LIQ = 2;
        public static final int COMODO_STAT_CAUS_LIQ = CAUS01_LIQ + CAUS02_LIQ + CAUS03_LIQ + CAUS04_LIQ;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
