package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-TAX<br>
 * Variable: WDTR-TAX from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_TAX;
    }

    public void setWdtrTax(AfDecimal wdtrTax) {
        writeDecimalAsPacked(Pos.WDTR_TAX, wdtrTax.copy());
    }

    /**Original name: WDTR-TAX<br>*/
    public AfDecimal getWdtrTax() {
        return readPackedAsDecimal(Pos.WDTR_TAX, Len.Int.WDTR_TAX, Len.Fract.WDTR_TAX);
    }

    public void setWdtrTaxNull(String wdtrTaxNull) {
        writeString(Pos.WDTR_TAX_NULL, wdtrTaxNull, Len.WDTR_TAX_NULL);
    }

    /**Original name: WDTR-TAX-NULL<br>*/
    public String getWdtrTaxNull() {
        return readString(Pos.WDTR_TAX_NULL, Len.WDTR_TAX_NULL);
    }

    public String getWdtrTaxNullFormatted() {
        return Functions.padBlanks(getWdtrTaxNull(), Len.WDTR_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_TAX = 1;
        public static final int WDTR_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_TAX = 8;
        public static final int WDTR_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
