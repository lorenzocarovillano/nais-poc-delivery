package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-TS-STAB-LIMITATA<br>
 * Variable: L3401-TS-STAB-LIMITATA from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401TsStabLimitata extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401TsStabLimitata() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_TS_STAB_LIMITATA;
    }

    public void setL3401TsStabLimitata(AfDecimal l3401TsStabLimitata) {
        writeDecimalAsPacked(Pos.L3401_TS_STAB_LIMITATA, l3401TsStabLimitata.copy());
    }

    public void setL3401TsStabLimitataFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_TS_STAB_LIMITATA, Pos.L3401_TS_STAB_LIMITATA);
    }

    /**Original name: L3401-TS-STAB-LIMITATA<br>*/
    public AfDecimal getL3401TsStabLimitata() {
        return readPackedAsDecimal(Pos.L3401_TS_STAB_LIMITATA, Len.Int.L3401_TS_STAB_LIMITATA, Len.Fract.L3401_TS_STAB_LIMITATA);
    }

    public byte[] getL3401TsStabLimitataAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_TS_STAB_LIMITATA, Pos.L3401_TS_STAB_LIMITATA);
        return buffer;
    }

    /**Original name: L3401-TS-STAB-LIMITATA-NULL<br>*/
    public String getL3401TsStabLimitataNull() {
        return readString(Pos.L3401_TS_STAB_LIMITATA_NULL, Len.L3401_TS_STAB_LIMITATA_NULL);
    }

    public String getL3401TsStabLimitataNullFormatted() {
        return Functions.padBlanks(getL3401TsStabLimitataNull(), Len.L3401_TS_STAB_LIMITATA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_TS_STAB_LIMITATA = 1;
        public static final int L3401_TS_STAB_LIMITATA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_TS_STAB_LIMITATA = 8;
        public static final int L3401_TS_STAB_LIMITATA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3401_TS_STAB_LIMITATA = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_TS_STAB_LIMITATA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
