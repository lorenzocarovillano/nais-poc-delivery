package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.redefines.WrdfTabella;

/**Original name: WRDF-AREA-RICH-DISINV-FND<br>
 * Variable: WRDF-AREA-RICH-DISINV-FND from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WrdfAreaRichDisinvFnd {

    //==== PROPERTIES ====
    //Original name: WRDF-ELE-RIC-INV-MAX
    private short wrdfEleRicInvMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRDF-TABELLA
    private WrdfTabella wrdfTabella = new WrdfTabella();

    //==== METHODS ====
    public void setWrdfAreaRichDisinvFndFormatted(String data) {
        byte[] buffer = new byte[Len.WRDF_AREA_RICH_DISINV_FND];
        MarshalByte.writeString(buffer, 1, data, Len.WRDF_AREA_RICH_DISINV_FND);
        setWrdfAreaRichDisinvFndBytes(buffer, 1);
    }

    public void setWrdfAreaRichDisinvFndBytes(byte[] buffer, int offset) {
        int position = offset;
        wrdfEleRicInvMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wrdfTabella.setWrdfTabellaBytes(buffer, position);
    }

    public byte[] getWrdfAreaRichDisinvFndBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wrdfEleRicInvMax);
        position += Types.SHORT_SIZE;
        wrdfTabella.getWrdfTabellaBytes(buffer, position);
        return buffer;
    }

    public void setWrdfEleRicInvMax(short wrdfEleRicInvMax) {
        this.wrdfEleRicInvMax = wrdfEleRicInvMax;
    }

    public short getWrdfEleRicInvMax() {
        return this.wrdfEleRicInvMax;
    }

    public WrdfTabella getWrdfTabella() {
        return wrdfTabella;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRDF_ELE_RIC_INV_MAX = 2;
        public static final int WRDF_AREA_RICH_DISINV_FND = WRDF_ELE_RIC_INV_MAX + WrdfTabella.Len.WRDF_TABELLA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
