package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-PER-PREMIO<br>
 * Variable: WS-TP-PER-PREMIO from program LOAS0670<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpPerPremio {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char UNICO = 'U';
    public static final char RICORRENTE = 'R';
    public static final char ANNUALE = 'A';

    //==== METHODS ====
    public void setWsTpPerPremio(char wsTpPerPremio) {
        this.value = wsTpPerPremio;
    }

    public void setWsTpPerPremioFormatted(String wsTpPerPremio) {
        setWsTpPerPremio(Functions.charAt(wsTpPerPremio, Types.CHAR_SIZE));
    }

    public char getWsTpPerPremio() {
        return this.value;
    }

    public boolean isUnico() {
        return value == UNICO;
    }

    public boolean isRicorrente() {
        return value == RICORRENTE;
    }

    public boolean isAnnuale() {
        return value == ANNUALE;
    }
}
