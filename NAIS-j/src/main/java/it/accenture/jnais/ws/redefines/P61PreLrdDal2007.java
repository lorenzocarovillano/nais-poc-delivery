package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P61-PRE-LRD-DAL2007<br>
 * Variable: P61-PRE-LRD-DAL2007 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P61PreLrdDal2007 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P61PreLrdDal2007() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P61_PRE_LRD_DAL2007;
    }

    public void setP61PreLrdDal2007(AfDecimal p61PreLrdDal2007) {
        writeDecimalAsPacked(Pos.P61_PRE_LRD_DAL2007, p61PreLrdDal2007.copy());
    }

    public void setP61PreLrdDal2007FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P61_PRE_LRD_DAL2007, Pos.P61_PRE_LRD_DAL2007);
    }

    /**Original name: P61-PRE-LRD-DAL2007<br>*/
    public AfDecimal getP61PreLrdDal2007() {
        return readPackedAsDecimal(Pos.P61_PRE_LRD_DAL2007, Len.Int.P61_PRE_LRD_DAL2007, Len.Fract.P61_PRE_LRD_DAL2007);
    }

    public byte[] getP61PreLrdDal2007AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P61_PRE_LRD_DAL2007, Pos.P61_PRE_LRD_DAL2007);
        return buffer;
    }

    public void setP61PreLrdDal2007Null(String p61PreLrdDal2007Null) {
        writeString(Pos.P61_PRE_LRD_DAL2007_NULL, p61PreLrdDal2007Null, Len.P61_PRE_LRD_DAL2007_NULL);
    }

    /**Original name: P61-PRE-LRD-DAL2007-NULL<br>*/
    public String getP61PreLrdDal2007Null() {
        return readString(Pos.P61_PRE_LRD_DAL2007_NULL, Len.P61_PRE_LRD_DAL2007_NULL);
    }

    public String getP61PreLrdDal2007NullFormatted() {
        return Functions.padBlanks(getP61PreLrdDal2007Null(), Len.P61_PRE_LRD_DAL2007_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P61_PRE_LRD_DAL2007 = 1;
        public static final int P61_PRE_LRD_DAL2007_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_PRE_LRD_DAL2007 = 8;
        public static final int P61_PRE_LRD_DAL2007_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_PRE_LRD_DAL2007 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P61_PRE_LRD_DAL2007 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
