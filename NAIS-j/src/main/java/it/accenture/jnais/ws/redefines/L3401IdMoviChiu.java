package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-ID-MOVI-CHIU<br>
 * Variable: L3401-ID-MOVI-CHIU from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_ID_MOVI_CHIU;
    }

    public void setL3401IdMoviChiu(int l3401IdMoviChiu) {
        writeIntAsPacked(Pos.L3401_ID_MOVI_CHIU, l3401IdMoviChiu, Len.Int.L3401_ID_MOVI_CHIU);
    }

    public void setL3401IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_ID_MOVI_CHIU, Pos.L3401_ID_MOVI_CHIU);
    }

    /**Original name: L3401-ID-MOVI-CHIU<br>*/
    public int getL3401IdMoviChiu() {
        return readPackedAsInt(Pos.L3401_ID_MOVI_CHIU, Len.Int.L3401_ID_MOVI_CHIU);
    }

    public byte[] getL3401IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_ID_MOVI_CHIU, Pos.L3401_ID_MOVI_CHIU);
        return buffer;
    }

    /**Original name: L3401-ID-MOVI-CHIU-NULL<br>*/
    public String getL3401IdMoviChiuNull() {
        return readString(Pos.L3401_ID_MOVI_CHIU_NULL, Len.L3401_ID_MOVI_CHIU_NULL);
    }

    public String getL3401IdMoviChiuNullFormatted() {
        return Functions.padBlanks(getL3401IdMoviChiuNull(), Len.L3401_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_ID_MOVI_CHIU = 1;
        public static final int L3401_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_ID_MOVI_CHIU = 5;
        public static final int L3401_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
