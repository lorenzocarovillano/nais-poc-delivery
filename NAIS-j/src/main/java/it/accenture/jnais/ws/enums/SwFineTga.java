package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-FINE-TGA<br>
 * Variable: SW-FINE-TGA from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwFineTga {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TGA = 'S';
    public static final char TGA_NO = 'N';

    //==== METHODS ====
    public void setSwFineTga(char swFineTga) {
        this.value = swFineTga;
    }

    public char getSwFineTga() {
        return this.value;
    }

    public void setTga() {
        value = TGA;
    }

    public boolean isTgaNo() {
        return value == TGA_NO;
    }

    public void setTgaNo() {
        value = TGA_NO;
    }
}
