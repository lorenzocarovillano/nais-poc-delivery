package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.BtcDtEff;
import it.accenture.jnais.ws.redefines.BtcDtEnd;
import it.accenture.jnais.ws.redefines.BtcDtStart;
import it.accenture.jnais.ws.redefines.BtcIdRich;

/**Original name: BTC-BATCH<br>
 * Variable: BTC-BATCH from copybook IDBVBTC1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BtcBatch extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: BTC-ID-BATCH
    private int btcIdBatch = DefaultValues.BIN_INT_VAL;
    //Original name: BTC-PROTOCOL
    private String btcProtocol = DefaultValues.stringVal(Len.BTC_PROTOCOL);
    //Original name: BTC-DT-INS
    private long btcDtIns = DefaultValues.LONG_VAL;
    //Original name: BTC-USER-INS
    private String btcUserIns = DefaultValues.stringVal(Len.BTC_USER_INS);
    //Original name: BTC-COD-BATCH-TYPE
    private int btcCodBatchType = DefaultValues.BIN_INT_VAL;
    //Original name: BTC-COD-COMP-ANIA
    private int btcCodCompAnia = DefaultValues.BIN_INT_VAL;
    //Original name: BTC-COD-BATCH-STATE
    private char btcCodBatchState = DefaultValues.CHAR_VAL;
    //Original name: BTC-DT-START
    private BtcDtStart btcDtStart = new BtcDtStart();
    //Original name: BTC-DT-END
    private BtcDtEnd btcDtEnd = new BtcDtEnd();
    //Original name: BTC-USER-START
    private String btcUserStart = DefaultValues.stringVal(Len.BTC_USER_START);
    //Original name: BTC-ID-RICH
    private BtcIdRich btcIdRich = new BtcIdRich();
    //Original name: BTC-DT-EFF
    private BtcDtEff btcDtEff = new BtcDtEff();
    //Original name: BTC-DATA-BATCH-LEN
    private short btcDataBatchLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: BTC-DATA-BATCH
    private String btcDataBatch = DefaultValues.stringVal(Len.BTC_DATA_BATCH);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_BATCH;
    }

    @Override
    public void deserialize(byte[] buf) {
        setBtcBatchBytes(buf);
    }

    public String getBtcBatchFormatted() {
        return MarshalByteExt.bufferToStr(getBtcBatchBytes());
    }

    public void setBtcBatchBytes(byte[] buffer) {
        setBtcBatchBytes(buffer, 1);
    }

    public byte[] getBtcBatchBytes() {
        byte[] buffer = new byte[Len.BTC_BATCH];
        return getBtcBatchBytes(buffer, 1);
    }

    public void setBtcBatchBytes(byte[] buffer, int offset) {
        int position = offset;
        btcIdBatch = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        btcProtocol = MarshalByte.readString(buffer, position, Len.BTC_PROTOCOL);
        position += Len.BTC_PROTOCOL;
        btcDtIns = MarshalByte.readPackedAsLong(buffer, position, Len.Int.BTC_DT_INS, 0);
        position += Len.BTC_DT_INS;
        btcUserIns = MarshalByte.readString(buffer, position, Len.BTC_USER_INS);
        position += Len.BTC_USER_INS;
        btcCodBatchType = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        btcCodCompAnia = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        btcCodBatchState = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        btcDtStart.setBtcDtStartFromBuffer(buffer, position);
        position += BtcDtStart.Len.BTC_DT_START;
        btcDtEnd.setBtcDtEndFromBuffer(buffer, position);
        position += BtcDtEnd.Len.BTC_DT_END;
        btcUserStart = MarshalByte.readString(buffer, position, Len.BTC_USER_START);
        position += Len.BTC_USER_START;
        btcIdRich.setBtcIdRichFromBuffer(buffer, position);
        position += Types.INT_SIZE;
        btcDtEff.setBtcDtEffFromBuffer(buffer, position);
        position += BtcDtEff.Len.BTC_DT_EFF;
        setBtcDataBatchVcharBytes(buffer, position);
    }

    public byte[] getBtcBatchBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, btcIdBatch);
        position += Types.INT_SIZE;
        MarshalByte.writeString(buffer, position, btcProtocol, Len.BTC_PROTOCOL);
        position += Len.BTC_PROTOCOL;
        MarshalByte.writeLongAsPacked(buffer, position, btcDtIns, Len.Int.BTC_DT_INS, 0);
        position += Len.BTC_DT_INS;
        MarshalByte.writeString(buffer, position, btcUserIns, Len.BTC_USER_INS);
        position += Len.BTC_USER_INS;
        MarshalByte.writeBinaryInt(buffer, position, btcCodBatchType);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, btcCodCompAnia);
        position += Types.INT_SIZE;
        MarshalByte.writeChar(buffer, position, btcCodBatchState);
        position += Types.CHAR_SIZE;
        btcDtStart.getBtcDtStartAsBuffer(buffer, position);
        position += BtcDtStart.Len.BTC_DT_START;
        btcDtEnd.getBtcDtEndAsBuffer(buffer, position);
        position += BtcDtEnd.Len.BTC_DT_END;
        MarshalByte.writeString(buffer, position, btcUserStart, Len.BTC_USER_START);
        position += Len.BTC_USER_START;
        btcIdRich.getBtcIdRichAsBuffer(buffer, position);
        position += Types.INT_SIZE;
        btcDtEff.getBtcDtEffAsBuffer(buffer, position);
        position += BtcDtEff.Len.BTC_DT_EFF;
        getBtcDataBatchVcharBytes(buffer, position);
        return buffer;
    }

    public void setBtcIdBatch(int btcIdBatch) {
        this.btcIdBatch = btcIdBatch;
    }

    public int getBtcIdBatch() {
        return this.btcIdBatch;
    }

    public void setBtcProtocol(String btcProtocol) {
        this.btcProtocol = Functions.subString(btcProtocol, Len.BTC_PROTOCOL);
    }

    public String getBtcProtocol() {
        return this.btcProtocol;
    }

    public void setBtcDtIns(long btcDtIns) {
        this.btcDtIns = btcDtIns;
    }

    public long getBtcDtIns() {
        return this.btcDtIns;
    }

    public void setBtcUserIns(String btcUserIns) {
        this.btcUserIns = Functions.subString(btcUserIns, Len.BTC_USER_INS);
    }

    public String getBtcUserIns() {
        return this.btcUserIns;
    }

    public void setBtcCodBatchType(int btcCodBatchType) {
        this.btcCodBatchType = btcCodBatchType;
    }

    public int getBtcCodBatchType() {
        return this.btcCodBatchType;
    }

    public void setBtcCodCompAnia(int btcCodCompAnia) {
        this.btcCodCompAnia = btcCodCompAnia;
    }

    public int getBtcCodCompAnia() {
        return this.btcCodCompAnia;
    }

    public void setBtcCodBatchState(char btcCodBatchState) {
        this.btcCodBatchState = btcCodBatchState;
    }

    public char getBtcCodBatchState() {
        return this.btcCodBatchState;
    }

    public void setBtcUserStart(String btcUserStart) {
        this.btcUserStart = Functions.subString(btcUserStart, Len.BTC_USER_START);
    }

    public String getBtcUserStart() {
        return this.btcUserStart;
    }

    public void setBtcDataBatchVcharFormatted(String data) {
        byte[] buffer = new byte[Len.BTC_DATA_BATCH_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.BTC_DATA_BATCH_VCHAR);
        setBtcDataBatchVcharBytes(buffer, 1);
    }

    public String getBtcDataBatchVcharFormatted() {
        return MarshalByteExt.bufferToStr(getBtcDataBatchVcharBytes());
    }

    /**Original name: BTC-DATA-BATCH-VCHAR<br>*/
    public byte[] getBtcDataBatchVcharBytes() {
        byte[] buffer = new byte[Len.BTC_DATA_BATCH_VCHAR];
        return getBtcDataBatchVcharBytes(buffer, 1);
    }

    public void setBtcDataBatchVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        btcDataBatchLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        btcDataBatch = MarshalByte.readString(buffer, position, Len.BTC_DATA_BATCH);
    }

    public byte[] getBtcDataBatchVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, btcDataBatchLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, btcDataBatch, Len.BTC_DATA_BATCH);
        return buffer;
    }

    public void setBtcDataBatchLen(short btcDataBatchLen) {
        this.btcDataBatchLen = btcDataBatchLen;
    }

    public short getBtcDataBatchLen() {
        return this.btcDataBatchLen;
    }

    public void setBtcDataBatch(String btcDataBatch) {
        this.btcDataBatch = Functions.subString(btcDataBatch, Len.BTC_DATA_BATCH);
    }

    public String getBtcDataBatch() {
        return this.btcDataBatch;
    }

    public BtcDtEff getBtcDtEff() {
        return btcDtEff;
    }

    public BtcDtEnd getBtcDtEnd() {
        return btcDtEnd;
    }

    public BtcDtStart getBtcDtStart() {
        return btcDtStart;
    }

    public BtcIdRich getBtcIdRich() {
        return btcIdRich;
    }

    @Override
    public byte[] serialize() {
        return getBtcBatchBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BTC_ID_BATCH = 4;
        public static final int BTC_PROTOCOL = 50;
        public static final int BTC_DT_INS = 10;
        public static final int BTC_USER_INS = 30;
        public static final int BTC_COD_BATCH_TYPE = 4;
        public static final int BTC_COD_COMP_ANIA = 4;
        public static final int BTC_COD_BATCH_STATE = 1;
        public static final int BTC_USER_START = 30;
        public static final int BTC_DATA_BATCH_LEN = 2;
        public static final int BTC_DATA_BATCH = 32000;
        public static final int BTC_DATA_BATCH_VCHAR = BTC_DATA_BATCH_LEN + BTC_DATA_BATCH;
        public static final int BTC_BATCH = BTC_ID_BATCH + BTC_PROTOCOL + BTC_DT_INS + BTC_USER_INS + BTC_COD_BATCH_TYPE + BTC_COD_COMP_ANIA + BTC_COD_BATCH_STATE + BtcDtStart.Len.BTC_DT_START + BtcDtEnd.Len.BTC_DT_END + BTC_USER_START + BtcIdRich.Len.BTC_ID_RICH + BtcDtEff.Len.BTC_DT_EFF + BTC_DATA_BATCH_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BTC_DT_INS = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
