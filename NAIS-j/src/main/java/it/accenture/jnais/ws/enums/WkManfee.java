package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-MANFEE<br>
 * Variable: WK-MANFEE from program LOAS0800<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkManfee {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char YES = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkManfee(char wkManfee) {
        this.value = wkManfee;
    }

    public char getWkManfee() {
        return this.value;
    }

    public boolean isYes() {
        return value == YES;
    }

    public void setYes() {
        value = YES;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
