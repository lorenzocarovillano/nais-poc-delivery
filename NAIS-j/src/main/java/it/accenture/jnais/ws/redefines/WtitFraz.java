package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-FRAZ<br>
 * Variable: WTIT-FRAZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_FRAZ;
    }

    public void setWtitFraz(int wtitFraz) {
        writeIntAsPacked(Pos.WTIT_FRAZ, wtitFraz, Len.Int.WTIT_FRAZ);
    }

    public void setWtitFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_FRAZ, Pos.WTIT_FRAZ);
    }

    /**Original name: WTIT-FRAZ<br>*/
    public int getWtitFraz() {
        return readPackedAsInt(Pos.WTIT_FRAZ, Len.Int.WTIT_FRAZ);
    }

    public byte[] getWtitFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_FRAZ, Pos.WTIT_FRAZ);
        return buffer;
    }

    public void initWtitFrazSpaces() {
        fill(Pos.WTIT_FRAZ, Len.WTIT_FRAZ, Types.SPACE_CHAR);
    }

    public void setWtitFrazNull(String wtitFrazNull) {
        writeString(Pos.WTIT_FRAZ_NULL, wtitFrazNull, Len.WTIT_FRAZ_NULL);
    }

    /**Original name: WTIT-FRAZ-NULL<br>*/
    public String getWtitFrazNull() {
        return readString(Pos.WTIT_FRAZ_NULL, Len.WTIT_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_FRAZ = 1;
        public static final int WTIT_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_FRAZ = 3;
        public static final int WTIT_FRAZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_FRAZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
