package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: C216-ERRORE-VALORIZZA<br>
 * Variable: C216-ERRORE-VALORIZZA from copybook IVVC0215<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class C216ErroreValorizza {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.ERRORE_VALORIZZA);
    public static final String SUCCESSFUL_RC = "00";
    public static final String VARIABILE_KO = "CA";

    //==== METHODS ====
    public void setErroreValorizza(String erroreValorizza) {
        this.value = Functions.subString(erroreValorizza, Len.ERRORE_VALORIZZA);
    }

    public String getErroreValorizza() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ERRORE_VALORIZZA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
