package it.accenture.jnais.ws;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IDettTitCont1;
import it.accenture.jnais.copy.DettTitCont;
import it.accenture.jnais.copy.DettTitContDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndDettTitCont;
import it.accenture.jnais.copy.IndTitCont;
import it.accenture.jnais.copy.TitCont;
import it.accenture.jnais.copy.TitContDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS0370<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs0370Data implements IDettTitCont1 {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-TIT-CONT
    private IndTitCont indTitCont = new IndTitCont();
    //Original name: IND-DETT-TIT-CONT
    private IndDettTitCont indDettTitCont = new IndDettTitCont();
    //Original name: TIT-CONT-DB
    private TitContDb titContDb = new TitContDb();
    //Original name: DETT-TIT-CONT-DB
    private DettTitContDb dettTitContDb = new DettTitContDb();
    //Original name: TIT-CONT
    private TitCont titCont = new TitCont();
    //Original name: DETT-TIT-CONT
    private DettTitCont dettTitCont = new DettTitCont();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public DettTitCont getDettTitCont() {
        return dettTitCont;
    }

    public DettTitContDb getDettTitContDb() {
        return dettTitContDb;
    }

    @Override
    public AfDecimal getDtcAcqExp() {
        return dettTitCont.getDtcAcqExp().getDtcAcqExp();
    }

    @Override
    public void setDtcAcqExp(AfDecimal dtcAcqExp) {
        this.dettTitCont.getDtcAcqExp().setDtcAcqExp(dtcAcqExp.copy());
    }

    @Override
    public AfDecimal getDtcAcqExpObj() {
        if (indDettTitCont.getAcqExp() >= 0) {
            return getDtcAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcAcqExpObj(AfDecimal dtcAcqExpObj) {
        if (dtcAcqExpObj != null) {
            setDtcAcqExp(new AfDecimal(dtcAcqExpObj, 15, 3));
            indDettTitCont.setAcqExp(((short)0));
        }
        else {
            indDettTitCont.setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCarAcq() {
        return dettTitCont.getDtcCarAcq().getDtcCarAcq();
    }

    @Override
    public void setDtcCarAcq(AfDecimal dtcCarAcq) {
        this.dettTitCont.getDtcCarAcq().setDtcCarAcq(dtcCarAcq.copy());
    }

    @Override
    public AfDecimal getDtcCarAcqObj() {
        if (indDettTitCont.getCarAcq() >= 0) {
            return getDtcCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCarAcqObj(AfDecimal dtcCarAcqObj) {
        if (dtcCarAcqObj != null) {
            setDtcCarAcq(new AfDecimal(dtcCarAcqObj, 15, 3));
            indDettTitCont.setCarAcq(((short)0));
        }
        else {
            indDettTitCont.setCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCarGest() {
        return dettTitCont.getDtcCarGest().getDtcCarGest();
    }

    @Override
    public void setDtcCarGest(AfDecimal dtcCarGest) {
        this.dettTitCont.getDtcCarGest().setDtcCarGest(dtcCarGest.copy());
    }

    @Override
    public AfDecimal getDtcCarGestObj() {
        if (indDettTitCont.getCarGest() >= 0) {
            return getDtcCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCarGestObj(AfDecimal dtcCarGestObj) {
        if (dtcCarGestObj != null) {
            setDtcCarGest(new AfDecimal(dtcCarGestObj, 15, 3));
            indDettTitCont.setCarGest(((short)0));
        }
        else {
            indDettTitCont.setCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCarIas() {
        return dettTitCont.getDtcCarIas().getDtcCarIas();
    }

    @Override
    public void setDtcCarIas(AfDecimal dtcCarIas) {
        this.dettTitCont.getDtcCarIas().setDtcCarIas(dtcCarIas.copy());
    }

    @Override
    public AfDecimal getDtcCarIasObj() {
        if (indDettTitCont.getCarIas() >= 0) {
            return getDtcCarIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCarIasObj(AfDecimal dtcCarIasObj) {
        if (dtcCarIasObj != null) {
            setDtcCarIas(new AfDecimal(dtcCarIasObj, 15, 3));
            indDettTitCont.setCarIas(((short)0));
        }
        else {
            indDettTitCont.setCarIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCarInc() {
        return dettTitCont.getDtcCarInc().getDtcCarInc();
    }

    @Override
    public void setDtcCarInc(AfDecimal dtcCarInc) {
        this.dettTitCont.getDtcCarInc().setDtcCarInc(dtcCarInc.copy());
    }

    @Override
    public AfDecimal getDtcCarIncObj() {
        if (indDettTitCont.getCarInc() >= 0) {
            return getDtcCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCarIncObj(AfDecimal dtcCarIncObj) {
        if (dtcCarIncObj != null) {
            setDtcCarInc(new AfDecimal(dtcCarIncObj, 15, 3));
            indDettTitCont.setCarInc(((short)0));
        }
        else {
            indDettTitCont.setCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCnbtAntirac() {
        return dettTitCont.getDtcCnbtAntirac().getDtcCnbtAntirac();
    }

    @Override
    public void setDtcCnbtAntirac(AfDecimal dtcCnbtAntirac) {
        this.dettTitCont.getDtcCnbtAntirac().setDtcCnbtAntirac(dtcCnbtAntirac.copy());
    }

    @Override
    public AfDecimal getDtcCnbtAntiracObj() {
        if (indDettTitCont.getCnbtAntirac() >= 0) {
            return getDtcCnbtAntirac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCnbtAntiracObj(AfDecimal dtcCnbtAntiracObj) {
        if (dtcCnbtAntiracObj != null) {
            setDtcCnbtAntirac(new AfDecimal(dtcCnbtAntiracObj, 15, 3));
            indDettTitCont.setCnbtAntirac(((short)0));
        }
        else {
            indDettTitCont.setCnbtAntirac(((short)-1));
        }
    }

    @Override
    public int getDtcCodCompAnia() {
        return dettTitCont.getDtcCodCompAnia();
    }

    @Override
    public void setDtcCodCompAnia(int dtcCodCompAnia) {
        this.dettTitCont.setDtcCodCompAnia(dtcCodCompAnia);
    }

    @Override
    public String getDtcCodDvs() {
        return dettTitCont.getDtcCodDvs();
    }

    @Override
    public void setDtcCodDvs(String dtcCodDvs) {
        this.dettTitCont.setDtcCodDvs(dtcCodDvs);
    }

    @Override
    public String getDtcCodDvsObj() {
        if (indDettTitCont.getCodDvs() >= 0) {
            return getDtcCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCodDvsObj(String dtcCodDvsObj) {
        if (dtcCodDvsObj != null) {
            setDtcCodDvs(dtcCodDvsObj);
            indDettTitCont.setCodDvs(((short)0));
        }
        else {
            indDettTitCont.setCodDvs(((short)-1));
        }
    }

    @Override
    public String getDtcCodTari() {
        return dettTitCont.getDtcCodTari();
    }

    @Override
    public void setDtcCodTari(String dtcCodTari) {
        this.dettTitCont.setDtcCodTari(dtcCodTari);
    }

    @Override
    public String getDtcCodTariObj() {
        if (indDettTitCont.getCodTari() >= 0) {
            return getDtcCodTari();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCodTariObj(String dtcCodTariObj) {
        if (dtcCodTariObj != null) {
            setDtcCodTari(dtcCodTariObj);
            indDettTitCont.setCodTari(((short)0));
        }
        else {
            indDettTitCont.setCodTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCommisInter() {
        return dettTitCont.getDtcCommisInter().getDtcCommisInter();
    }

    @Override
    public void setDtcCommisInter(AfDecimal dtcCommisInter) {
        this.dettTitCont.getDtcCommisInter().setDtcCommisInter(dtcCommisInter.copy());
    }

    @Override
    public AfDecimal getDtcCommisInterObj() {
        if (indDettTitCont.getCommisInter() >= 0) {
            return getDtcCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCommisInterObj(AfDecimal dtcCommisInterObj) {
        if (dtcCommisInterObj != null) {
            setDtcCommisInter(new AfDecimal(dtcCommisInterObj, 15, 3));
            indDettTitCont.setCommisInter(((short)0));
        }
        else {
            indDettTitCont.setCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcDir() {
        return dettTitCont.getDtcDir().getDtcDir();
    }

    @Override
    public void setDtcDir(AfDecimal dtcDir) {
        this.dettTitCont.getDtcDir().setDtcDir(dtcDir.copy());
    }

    @Override
    public AfDecimal getDtcDirObj() {
        if (indDettTitCont.getDir() >= 0) {
            return getDtcDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcDirObj(AfDecimal dtcDirObj) {
        if (dtcDirObj != null) {
            setDtcDir(new AfDecimal(dtcDirObj, 15, 3));
            indDettTitCont.setDir(((short)0));
        }
        else {
            indDettTitCont.setDir(((short)-1));
        }
    }

    @Override
    public char getDtcDsOperSql() {
        return dettTitCont.getDtcDsOperSql();
    }

    @Override
    public void setDtcDsOperSql(char dtcDsOperSql) {
        this.dettTitCont.setDtcDsOperSql(dtcDsOperSql);
    }

    @Override
    public long getDtcDsRiga() {
        return dettTitCont.getDtcDsRiga();
    }

    @Override
    public void setDtcDsRiga(long dtcDsRiga) {
        this.dettTitCont.setDtcDsRiga(dtcDsRiga);
    }

    @Override
    public char getDtcDsStatoElab() {
        return dettTitCont.getDtcDsStatoElab();
    }

    @Override
    public void setDtcDsStatoElab(char dtcDsStatoElab) {
        this.dettTitCont.setDtcDsStatoElab(dtcDsStatoElab);
    }

    @Override
    public long getDtcDsTsEndCptz() {
        return dettTitCont.getDtcDsTsEndCptz();
    }

    @Override
    public void setDtcDsTsEndCptz(long dtcDsTsEndCptz) {
        this.dettTitCont.setDtcDsTsEndCptz(dtcDsTsEndCptz);
    }

    @Override
    public long getDtcDsTsIniCptz() {
        return dettTitCont.getDtcDsTsIniCptz();
    }

    @Override
    public void setDtcDsTsIniCptz(long dtcDsTsIniCptz) {
        this.dettTitCont.setDtcDsTsIniCptz(dtcDsTsIniCptz);
    }

    @Override
    public String getDtcDsUtente() {
        return dettTitCont.getDtcDsUtente();
    }

    @Override
    public void setDtcDsUtente(String dtcDsUtente) {
        this.dettTitCont.setDtcDsUtente(dtcDsUtente);
    }

    @Override
    public int getDtcDsVer() {
        return dettTitCont.getDtcDsVer();
    }

    @Override
    public void setDtcDsVer(int dtcDsVer) {
        this.dettTitCont.setDtcDsVer(dtcDsVer);
    }

    @Override
    public String getDtcDtEndCopDb() {
        return dettTitContDb.getEndCopDb();
    }

    @Override
    public void setDtcDtEndCopDb(String dtcDtEndCopDb) {
        this.dettTitContDb.setEndCopDb(dtcDtEndCopDb);
    }

    @Override
    public String getDtcDtEndCopDbObj() {
        if (indDettTitCont.getDtEndCop() >= 0) {
            return getDtcDtEndCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcDtEndCopDbObj(String dtcDtEndCopDbObj) {
        if (dtcDtEndCopDbObj != null) {
            setDtcDtEndCopDb(dtcDtEndCopDbObj);
            indDettTitCont.setDtEndCop(((short)0));
        }
        else {
            indDettTitCont.setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getDtcDtEndEffDb() {
        return dettTitContDb.getEndEffDb();
    }

    @Override
    public void setDtcDtEndEffDb(String dtcDtEndEffDb) {
        this.dettTitContDb.setEndEffDb(dtcDtEndEffDb);
    }

    @Override
    public String getDtcDtEsiTitDb() {
        return dettTitContDb.getEsiTitDb();
    }

    @Override
    public void setDtcDtEsiTitDb(String dtcDtEsiTitDb) {
        this.dettTitContDb.setEsiTitDb(dtcDtEsiTitDb);
    }

    @Override
    public String getDtcDtEsiTitDbObj() {
        if (indDettTitCont.getDtEsiTit() >= 0) {
            return getDtcDtEsiTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcDtEsiTitDbObj(String dtcDtEsiTitDbObj) {
        if (dtcDtEsiTitDbObj != null) {
            setDtcDtEsiTitDb(dtcDtEsiTitDbObj);
            indDettTitCont.setDtEsiTit(((short)0));
        }
        else {
            indDettTitCont.setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getDtcDtIniCopDb() {
        return dettTitContDb.getIniCopDb();
    }

    @Override
    public void setDtcDtIniCopDb(String dtcDtIniCopDb) {
        this.dettTitContDb.setIniCopDb(dtcDtIniCopDb);
    }

    @Override
    public String getDtcDtIniCopDbObj() {
        if (indDettTitCont.getDtIniCop() >= 0) {
            return getDtcDtIniCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcDtIniCopDbObj(String dtcDtIniCopDbObj) {
        if (dtcDtIniCopDbObj != null) {
            setDtcDtIniCopDb(dtcDtIniCopDbObj);
            indDettTitCont.setDtIniCop(((short)0));
        }
        else {
            indDettTitCont.setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getDtcDtIniEffDb() {
        return dettTitContDb.getIniEffDb();
    }

    @Override
    public void setDtcDtIniEffDb(String dtcDtIniEffDb) {
        this.dettTitContDb.setIniEffDb(dtcDtIniEffDb);
    }

    @Override
    public int getDtcFrqMovi() {
        return dettTitCont.getDtcFrqMovi().getDtcFrqMovi();
    }

    @Override
    public void setDtcFrqMovi(int dtcFrqMovi) {
        this.dettTitCont.getDtcFrqMovi().setDtcFrqMovi(dtcFrqMovi);
    }

    @Override
    public Integer getDtcFrqMoviObj() {
        if (indDettTitCont.getFrqMovi() >= 0) {
            return ((Integer)getDtcFrqMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcFrqMoviObj(Integer dtcFrqMoviObj) {
        if (dtcFrqMoviObj != null) {
            setDtcFrqMovi(((int)dtcFrqMoviObj));
            indDettTitCont.setFrqMovi(((short)0));
        }
        else {
            indDettTitCont.setFrqMovi(((short)-1));
        }
    }

    @Override
    public int getDtcIdDettTitCont() {
        return dettTitCont.getDtcIdDettTitCont();
    }

    @Override
    public void setDtcIdDettTitCont(int dtcIdDettTitCont) {
        this.dettTitCont.setDtcIdDettTitCont(dtcIdDettTitCont);
    }

    @Override
    public int getDtcIdMoviChiu() {
        return dettTitCont.getDtcIdMoviChiu().getDtcIdMoviChiu();
    }

    @Override
    public void setDtcIdMoviChiu(int dtcIdMoviChiu) {
        this.dettTitCont.getDtcIdMoviChiu().setDtcIdMoviChiu(dtcIdMoviChiu);
    }

    @Override
    public Integer getDtcIdMoviChiuObj() {
        if (indDettTitCont.getIdMoviChiu() >= 0) {
            return ((Integer)getDtcIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcIdMoviChiuObj(Integer dtcIdMoviChiuObj) {
        if (dtcIdMoviChiuObj != null) {
            setDtcIdMoviChiu(((int)dtcIdMoviChiuObj));
            indDettTitCont.setIdMoviChiu(((short)0));
        }
        else {
            indDettTitCont.setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getDtcIdMoviCrz() {
        return dettTitCont.getDtcIdMoviCrz();
    }

    @Override
    public void setDtcIdMoviCrz(int dtcIdMoviCrz) {
        this.dettTitCont.setDtcIdMoviCrz(dtcIdMoviCrz);
    }

    @Override
    public int getDtcIdOgg() {
        return dettTitCont.getDtcIdOgg();
    }

    @Override
    public void setDtcIdOgg(int dtcIdOgg) {
        this.dettTitCont.setDtcIdOgg(dtcIdOgg);
    }

    @Override
    public int getDtcIdTitCont() {
        return dettTitCont.getDtcIdTitCont();
    }

    @Override
    public void setDtcIdTitCont(int dtcIdTitCont) {
        this.dettTitCont.setDtcIdTitCont(dtcIdTitCont);
    }

    @Override
    public AfDecimal getDtcImpAder() {
        return dettTitCont.getDtcImpAder().getDtcImpAder();
    }

    @Override
    public void setDtcImpAder(AfDecimal dtcImpAder) {
        this.dettTitCont.getDtcImpAder().setDtcImpAder(dtcImpAder.copy());
    }

    @Override
    public AfDecimal getDtcImpAderObj() {
        if (indDettTitCont.getImpAder() >= 0) {
            return getDtcImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpAderObj(AfDecimal dtcImpAderObj) {
        if (dtcImpAderObj != null) {
            setDtcImpAder(new AfDecimal(dtcImpAderObj, 15, 3));
            indDettTitCont.setImpAder(((short)0));
        }
        else {
            indDettTitCont.setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcImpAz() {
        return dettTitCont.getDtcImpAz().getDtcImpAz();
    }

    @Override
    public void setDtcImpAz(AfDecimal dtcImpAz) {
        this.dettTitCont.getDtcImpAz().setDtcImpAz(dtcImpAz.copy());
    }

    @Override
    public AfDecimal getDtcImpAzObj() {
        if (indDettTitCont.getImpAz() >= 0) {
            return getDtcImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpAzObj(AfDecimal dtcImpAzObj) {
        if (dtcImpAzObj != null) {
            setDtcImpAz(new AfDecimal(dtcImpAzObj, 15, 3));
            indDettTitCont.setImpAz(((short)0));
        }
        else {
            indDettTitCont.setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcImpTfr() {
        return dettTitCont.getDtcImpTfr().getDtcImpTfr();
    }

    @Override
    public void setDtcImpTfr(AfDecimal dtcImpTfr) {
        this.dettTitCont.getDtcImpTfr().setDtcImpTfr(dtcImpTfr.copy());
    }

    @Override
    public AfDecimal getDtcImpTfrObj() {
        if (indDettTitCont.getImpTfr() >= 0) {
            return getDtcImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpTfrObj(AfDecimal dtcImpTfrObj) {
        if (dtcImpTfrObj != null) {
            setDtcImpTfr(new AfDecimal(dtcImpTfrObj, 15, 3));
            indDettTitCont.setImpTfr(((short)0));
        }
        else {
            indDettTitCont.setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcImpTfrStrc() {
        return dettTitCont.getDtcImpTfrStrc().getDtcImpTfrStrc();
    }

    @Override
    public void setDtcImpTfrStrc(AfDecimal dtcImpTfrStrc) {
        this.dettTitCont.getDtcImpTfrStrc().setDtcImpTfrStrc(dtcImpTfrStrc.copy());
    }

    @Override
    public AfDecimal getDtcImpTfrStrcObj() {
        if (indDettTitCont.getImpTfrStrc() >= 0) {
            return getDtcImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpTfrStrcObj(AfDecimal dtcImpTfrStrcObj) {
        if (dtcImpTfrStrcObj != null) {
            setDtcImpTfrStrc(new AfDecimal(dtcImpTfrStrcObj, 15, 3));
            indDettTitCont.setImpTfrStrc(((short)0));
        }
        else {
            indDettTitCont.setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcImpTrasfe() {
        return dettTitCont.getDtcImpTrasfe().getDtcImpTrasfe();
    }

    @Override
    public void setDtcImpTrasfe(AfDecimal dtcImpTrasfe) {
        this.dettTitCont.getDtcImpTrasfe().setDtcImpTrasfe(dtcImpTrasfe.copy());
    }

    @Override
    public AfDecimal getDtcImpTrasfeObj() {
        if (indDettTitCont.getImpTrasfe() >= 0) {
            return getDtcImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpTrasfeObj(AfDecimal dtcImpTrasfeObj) {
        if (dtcImpTrasfeObj != null) {
            setDtcImpTrasfe(new AfDecimal(dtcImpTrasfeObj, 15, 3));
            indDettTitCont.setImpTrasfe(((short)0));
        }
        else {
            indDettTitCont.setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcImpVolo() {
        return dettTitCont.getDtcImpVolo().getDtcImpVolo();
    }

    @Override
    public void setDtcImpVolo(AfDecimal dtcImpVolo) {
        this.dettTitCont.getDtcImpVolo().setDtcImpVolo(dtcImpVolo.copy());
    }

    @Override
    public AfDecimal getDtcImpVoloObj() {
        if (indDettTitCont.getImpVolo() >= 0) {
            return getDtcImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpVoloObj(AfDecimal dtcImpVoloObj) {
        if (dtcImpVoloObj != null) {
            setDtcImpVolo(new AfDecimal(dtcImpVoloObj, 15, 3));
            indDettTitCont.setImpVolo(((short)0));
        }
        else {
            indDettTitCont.setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcIntrFraz() {
        return dettTitCont.getDtcIntrFraz().getDtcIntrFraz();
    }

    @Override
    public void setDtcIntrFraz(AfDecimal dtcIntrFraz) {
        this.dettTitCont.getDtcIntrFraz().setDtcIntrFraz(dtcIntrFraz.copy());
    }

    @Override
    public AfDecimal getDtcIntrFrazObj() {
        if (indDettTitCont.getIntrFraz() >= 0) {
            return getDtcIntrFraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcIntrFrazObj(AfDecimal dtcIntrFrazObj) {
        if (dtcIntrFrazObj != null) {
            setDtcIntrFraz(new AfDecimal(dtcIntrFrazObj, 15, 3));
            indDettTitCont.setIntrFraz(((short)0));
        }
        else {
            indDettTitCont.setIntrFraz(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcIntrMora() {
        return dettTitCont.getDtcIntrMora().getDtcIntrMora();
    }

    @Override
    public void setDtcIntrMora(AfDecimal dtcIntrMora) {
        this.dettTitCont.getDtcIntrMora().setDtcIntrMora(dtcIntrMora.copy());
    }

    @Override
    public AfDecimal getDtcIntrMoraObj() {
        if (indDettTitCont.getIntrMora() >= 0) {
            return getDtcIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcIntrMoraObj(AfDecimal dtcIntrMoraObj) {
        if (dtcIntrMoraObj != null) {
            setDtcIntrMora(new AfDecimal(dtcIntrMoraObj, 15, 3));
            indDettTitCont.setIntrMora(((short)0));
        }
        else {
            indDettTitCont.setIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcIntrRetdt() {
        return dettTitCont.getDtcIntrRetdt().getDtcIntrRetdt();
    }

    @Override
    public void setDtcIntrRetdt(AfDecimal dtcIntrRetdt) {
        this.dettTitCont.getDtcIntrRetdt().setDtcIntrRetdt(dtcIntrRetdt.copy());
    }

    @Override
    public AfDecimal getDtcIntrRetdtObj() {
        if (indDettTitCont.getIntrRetdt() >= 0) {
            return getDtcIntrRetdt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcIntrRetdtObj(AfDecimal dtcIntrRetdtObj) {
        if (dtcIntrRetdtObj != null) {
            setDtcIntrRetdt(new AfDecimal(dtcIntrRetdtObj, 15, 3));
            indDettTitCont.setIntrRetdt(((short)0));
        }
        else {
            indDettTitCont.setIntrRetdt(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcIntrRiat() {
        return dettTitCont.getDtcIntrRiat().getDtcIntrRiat();
    }

    @Override
    public void setDtcIntrRiat(AfDecimal dtcIntrRiat) {
        this.dettTitCont.getDtcIntrRiat().setDtcIntrRiat(dtcIntrRiat.copy());
    }

    @Override
    public AfDecimal getDtcIntrRiatObj() {
        if (indDettTitCont.getIntrRiat() >= 0) {
            return getDtcIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcIntrRiatObj(AfDecimal dtcIntrRiatObj) {
        if (dtcIntrRiatObj != null) {
            setDtcIntrRiat(new AfDecimal(dtcIntrRiatObj, 15, 3));
            indDettTitCont.setIntrRiat(((short)0));
        }
        else {
            indDettTitCont.setIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcManfeeAntic() {
        return dettTitCont.getDtcManfeeAntic().getDtcManfeeAntic();
    }

    @Override
    public void setDtcManfeeAntic(AfDecimal dtcManfeeAntic) {
        this.dettTitCont.getDtcManfeeAntic().setDtcManfeeAntic(dtcManfeeAntic.copy());
    }

    @Override
    public AfDecimal getDtcManfeeAnticObj() {
        if (indDettTitCont.getManfeeAntic() >= 0) {
            return getDtcManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcManfeeAnticObj(AfDecimal dtcManfeeAnticObj) {
        if (dtcManfeeAnticObj != null) {
            setDtcManfeeAntic(new AfDecimal(dtcManfeeAnticObj, 15, 3));
            indDettTitCont.setManfeeAntic(((short)0));
        }
        else {
            indDettTitCont.setManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcManfeeRec() {
        return dettTitCont.getDtcManfeeRec().getDtcManfeeRec();
    }

    @Override
    public void setDtcManfeeRec(AfDecimal dtcManfeeRec) {
        this.dettTitCont.getDtcManfeeRec().setDtcManfeeRec(dtcManfeeRec.copy());
    }

    @Override
    public AfDecimal getDtcManfeeRecObj() {
        if (indDettTitCont.getManfeeRec() >= 0) {
            return getDtcManfeeRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcManfeeRecObj(AfDecimal dtcManfeeRecObj) {
        if (dtcManfeeRecObj != null) {
            setDtcManfeeRec(new AfDecimal(dtcManfeeRecObj, 15, 3));
            indDettTitCont.setManfeeRec(((short)0));
        }
        else {
            indDettTitCont.setManfeeRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcManfeeRicor() {
        return dettTitCont.getDtcManfeeRicor().getDtcManfeeRicor();
    }

    @Override
    public void setDtcManfeeRicor(AfDecimal dtcManfeeRicor) {
        this.dettTitCont.getDtcManfeeRicor().setDtcManfeeRicor(dtcManfeeRicor.copy());
    }

    @Override
    public AfDecimal getDtcManfeeRicorObj() {
        if (indDettTitCont.getManfeeRicor() >= 0) {
            return getDtcManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcManfeeRicorObj(AfDecimal dtcManfeeRicorObj) {
        if (dtcManfeeRicorObj != null) {
            setDtcManfeeRicor(new AfDecimal(dtcManfeeRicorObj, 15, 3));
            indDettTitCont.setManfeeRicor(((short)0));
        }
        else {
            indDettTitCont.setManfeeRicor(((short)-1));
        }
    }

    @Override
    public int getDtcNumGgRitardoPag() {
        return dettTitCont.getDtcNumGgRitardoPag().getDtcNumGgRitardoPag();
    }

    @Override
    public void setDtcNumGgRitardoPag(int dtcNumGgRitardoPag) {
        this.dettTitCont.getDtcNumGgRitardoPag().setDtcNumGgRitardoPag(dtcNumGgRitardoPag);
    }

    @Override
    public Integer getDtcNumGgRitardoPagObj() {
        if (indDettTitCont.getNumGgRitardoPag() >= 0) {
            return ((Integer)getDtcNumGgRitardoPag());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcNumGgRitardoPagObj(Integer dtcNumGgRitardoPagObj) {
        if (dtcNumGgRitardoPagObj != null) {
            setDtcNumGgRitardoPag(((int)dtcNumGgRitardoPagObj));
            indDettTitCont.setNumGgRitardoPag(((short)0));
        }
        else {
            indDettTitCont.setNumGgRitardoPag(((short)-1));
        }
    }

    @Override
    public int getDtcNumGgRival() {
        return dettTitCont.getDtcNumGgRival().getDtcNumGgRival();
    }

    @Override
    public void setDtcNumGgRival(int dtcNumGgRival) {
        this.dettTitCont.getDtcNumGgRival().setDtcNumGgRival(dtcNumGgRival);
    }

    @Override
    public Integer getDtcNumGgRivalObj() {
        if (indDettTitCont.getNumGgRival() >= 0) {
            return ((Integer)getDtcNumGgRival());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcNumGgRivalObj(Integer dtcNumGgRivalObj) {
        if (dtcNumGgRivalObj != null) {
            setDtcNumGgRival(((int)dtcNumGgRivalObj));
            indDettTitCont.setNumGgRival(((short)0));
        }
        else {
            indDettTitCont.setNumGgRival(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcPreNet() {
        return dettTitCont.getDtcPreNet().getDtcPreNet();
    }

    @Override
    public void setDtcPreNet(AfDecimal dtcPreNet) {
        this.dettTitCont.getDtcPreNet().setDtcPreNet(dtcPreNet.copy());
    }

    @Override
    public AfDecimal getDtcPreNetObj() {
        if (indDettTitCont.getPreNet() >= 0) {
            return getDtcPreNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcPreNetObj(AfDecimal dtcPreNetObj) {
        if (dtcPreNetObj != null) {
            setDtcPreNet(new AfDecimal(dtcPreNetObj, 15, 3));
            indDettTitCont.setPreNet(((short)0));
        }
        else {
            indDettTitCont.setPreNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcPrePpIas() {
        return dettTitCont.getDtcPrePpIas().getDtcPrePpIas();
    }

    @Override
    public void setDtcPrePpIas(AfDecimal dtcPrePpIas) {
        this.dettTitCont.getDtcPrePpIas().setDtcPrePpIas(dtcPrePpIas.copy());
    }

    @Override
    public AfDecimal getDtcPrePpIasObj() {
        if (indDettTitCont.getPrePpIas() >= 0) {
            return getDtcPrePpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcPrePpIasObj(AfDecimal dtcPrePpIasObj) {
        if (dtcPrePpIasObj != null) {
            setDtcPrePpIas(new AfDecimal(dtcPrePpIasObj, 15, 3));
            indDettTitCont.setPrePpIas(((short)0));
        }
        else {
            indDettTitCont.setPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcPreSoloRsh() {
        return dettTitCont.getDtcPreSoloRsh().getDtcPreSoloRsh();
    }

    @Override
    public void setDtcPreSoloRsh(AfDecimal dtcPreSoloRsh) {
        this.dettTitCont.getDtcPreSoloRsh().setDtcPreSoloRsh(dtcPreSoloRsh.copy());
    }

    @Override
    public AfDecimal getDtcPreSoloRshObj() {
        if (indDettTitCont.getPreSoloRsh() >= 0) {
            return getDtcPreSoloRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcPreSoloRshObj(AfDecimal dtcPreSoloRshObj) {
        if (dtcPreSoloRshObj != null) {
            setDtcPreSoloRsh(new AfDecimal(dtcPreSoloRshObj, 15, 3));
            indDettTitCont.setPreSoloRsh(((short)0));
        }
        else {
            indDettTitCont.setPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcPreTot() {
        return dettTitCont.getDtcPreTot().getDtcPreTot();
    }

    @Override
    public void setDtcPreTot(AfDecimal dtcPreTot) {
        this.dettTitCont.getDtcPreTot().setDtcPreTot(dtcPreTot.copy());
    }

    @Override
    public AfDecimal getDtcPreTotObj() {
        if (indDettTitCont.getPreTot() >= 0) {
            return getDtcPreTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcPreTotObj(AfDecimal dtcPreTotObj) {
        if (dtcPreTotObj != null) {
            setDtcPreTot(new AfDecimal(dtcPreTotObj, 15, 3));
            indDettTitCont.setPreTot(((short)0));
        }
        else {
            indDettTitCont.setPreTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcProvAcq1aa() {
        return dettTitCont.getDtcProvAcq1aa().getDtcProvAcq1aa();
    }

    @Override
    public void setDtcProvAcq1aa(AfDecimal dtcProvAcq1aa) {
        this.dettTitCont.getDtcProvAcq1aa().setDtcProvAcq1aa(dtcProvAcq1aa.copy());
    }

    @Override
    public AfDecimal getDtcProvAcq1aaObj() {
        if (indDettTitCont.getProvAcq1aa() >= 0) {
            return getDtcProvAcq1aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcProvAcq1aaObj(AfDecimal dtcProvAcq1aaObj) {
        if (dtcProvAcq1aaObj != null) {
            setDtcProvAcq1aa(new AfDecimal(dtcProvAcq1aaObj, 15, 3));
            indDettTitCont.setProvAcq1aa(((short)0));
        }
        else {
            indDettTitCont.setProvAcq1aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcProvAcq2aa() {
        return dettTitCont.getDtcProvAcq2aa().getDtcProvAcq2aa();
    }

    @Override
    public void setDtcProvAcq2aa(AfDecimal dtcProvAcq2aa) {
        this.dettTitCont.getDtcProvAcq2aa().setDtcProvAcq2aa(dtcProvAcq2aa.copy());
    }

    @Override
    public AfDecimal getDtcProvAcq2aaObj() {
        if (indDettTitCont.getProvAcq2aa() >= 0) {
            return getDtcProvAcq2aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcProvAcq2aaObj(AfDecimal dtcProvAcq2aaObj) {
        if (dtcProvAcq2aaObj != null) {
            setDtcProvAcq2aa(new AfDecimal(dtcProvAcq2aaObj, 15, 3));
            indDettTitCont.setProvAcq2aa(((short)0));
        }
        else {
            indDettTitCont.setProvAcq2aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcProvDaRec() {
        return dettTitCont.getDtcProvDaRec().getDtcProvDaRec();
    }

    @Override
    public void setDtcProvDaRec(AfDecimal dtcProvDaRec) {
        this.dettTitCont.getDtcProvDaRec().setDtcProvDaRec(dtcProvDaRec.copy());
    }

    @Override
    public AfDecimal getDtcProvDaRecObj() {
        if (indDettTitCont.getProvDaRec() >= 0) {
            return getDtcProvDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcProvDaRecObj(AfDecimal dtcProvDaRecObj) {
        if (dtcProvDaRecObj != null) {
            setDtcProvDaRec(new AfDecimal(dtcProvDaRecObj, 15, 3));
            indDettTitCont.setProvDaRec(((short)0));
        }
        else {
            indDettTitCont.setProvDaRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcProvInc() {
        return dettTitCont.getDtcProvInc().getDtcProvInc();
    }

    @Override
    public void setDtcProvInc(AfDecimal dtcProvInc) {
        this.dettTitCont.getDtcProvInc().setDtcProvInc(dtcProvInc.copy());
    }

    @Override
    public AfDecimal getDtcProvIncObj() {
        if (indDettTitCont.getProvInc() >= 0) {
            return getDtcProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcProvIncObj(AfDecimal dtcProvIncObj) {
        if (dtcProvIncObj != null) {
            setDtcProvInc(new AfDecimal(dtcProvIncObj, 15, 3));
            indDettTitCont.setProvInc(((short)0));
        }
        else {
            indDettTitCont.setProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcProvRicor() {
        return dettTitCont.getDtcProvRicor().getDtcProvRicor();
    }

    @Override
    public void setDtcProvRicor(AfDecimal dtcProvRicor) {
        this.dettTitCont.getDtcProvRicor().setDtcProvRicor(dtcProvRicor.copy());
    }

    @Override
    public AfDecimal getDtcProvRicorObj() {
        if (indDettTitCont.getProvRicor() >= 0) {
            return getDtcProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcProvRicorObj(AfDecimal dtcProvRicorObj) {
        if (dtcProvRicorObj != null) {
            setDtcProvRicor(new AfDecimal(dtcProvRicorObj, 15, 3));
            indDettTitCont.setProvRicor(((short)0));
        }
        else {
            indDettTitCont.setProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcRemunAss() {
        return dettTitCont.getDtcRemunAss().getDtcRemunAss();
    }

    @Override
    public void setDtcRemunAss(AfDecimal dtcRemunAss) {
        this.dettTitCont.getDtcRemunAss().setDtcRemunAss(dtcRemunAss.copy());
    }

    @Override
    public AfDecimal getDtcRemunAssObj() {
        if (indDettTitCont.getRemunAss() >= 0) {
            return getDtcRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcRemunAssObj(AfDecimal dtcRemunAssObj) {
        if (dtcRemunAssObj != null) {
            setDtcRemunAss(new AfDecimal(dtcRemunAssObj, 15, 3));
            indDettTitCont.setRemunAss(((short)0));
        }
        else {
            indDettTitCont.setRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSoprAlt() {
        return dettTitCont.getDtcSoprAlt().getDtcSoprAlt();
    }

    @Override
    public void setDtcSoprAlt(AfDecimal dtcSoprAlt) {
        this.dettTitCont.getDtcSoprAlt().setDtcSoprAlt(dtcSoprAlt.copy());
    }

    @Override
    public AfDecimal getDtcSoprAltObj() {
        if (indDettTitCont.getSoprAlt() >= 0) {
            return getDtcSoprAlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSoprAltObj(AfDecimal dtcSoprAltObj) {
        if (dtcSoprAltObj != null) {
            setDtcSoprAlt(new AfDecimal(dtcSoprAltObj, 15, 3));
            indDettTitCont.setSoprAlt(((short)0));
        }
        else {
            indDettTitCont.setSoprAlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSoprProf() {
        return dettTitCont.getDtcSoprProf().getDtcSoprProf();
    }

    @Override
    public void setDtcSoprProf(AfDecimal dtcSoprProf) {
        this.dettTitCont.getDtcSoprProf().setDtcSoprProf(dtcSoprProf.copy());
    }

    @Override
    public AfDecimal getDtcSoprProfObj() {
        if (indDettTitCont.getSoprProf() >= 0) {
            return getDtcSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSoprProfObj(AfDecimal dtcSoprProfObj) {
        if (dtcSoprProfObj != null) {
            setDtcSoprProf(new AfDecimal(dtcSoprProfObj, 15, 3));
            indDettTitCont.setSoprProf(((short)0));
        }
        else {
            indDettTitCont.setSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSoprSan() {
        return dettTitCont.getDtcSoprSan().getDtcSoprSan();
    }

    @Override
    public void setDtcSoprSan(AfDecimal dtcSoprSan) {
        this.dettTitCont.getDtcSoprSan().setDtcSoprSan(dtcSoprSan.copy());
    }

    @Override
    public AfDecimal getDtcSoprSanObj() {
        if (indDettTitCont.getSoprSan() >= 0) {
            return getDtcSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSoprSanObj(AfDecimal dtcSoprSanObj) {
        if (dtcSoprSanObj != null) {
            setDtcSoprSan(new AfDecimal(dtcSoprSanObj, 15, 3));
            indDettTitCont.setSoprSan(((short)0));
        }
        else {
            indDettTitCont.setSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSoprSpo() {
        return dettTitCont.getDtcSoprSpo().getDtcSoprSpo();
    }

    @Override
    public void setDtcSoprSpo(AfDecimal dtcSoprSpo) {
        this.dettTitCont.getDtcSoprSpo().setDtcSoprSpo(dtcSoprSpo.copy());
    }

    @Override
    public AfDecimal getDtcSoprSpoObj() {
        if (indDettTitCont.getSoprSpo() >= 0) {
            return getDtcSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSoprSpoObj(AfDecimal dtcSoprSpoObj) {
        if (dtcSoprSpoObj != null) {
            setDtcSoprSpo(new AfDecimal(dtcSoprSpoObj, 15, 3));
            indDettTitCont.setSoprSpo(((short)0));
        }
        else {
            indDettTitCont.setSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSoprTec() {
        return dettTitCont.getDtcSoprTec().getDtcSoprTec();
    }

    @Override
    public void setDtcSoprTec(AfDecimal dtcSoprTec) {
        this.dettTitCont.getDtcSoprTec().setDtcSoprTec(dtcSoprTec.copy());
    }

    @Override
    public AfDecimal getDtcSoprTecObj() {
        if (indDettTitCont.getSoprTec() >= 0) {
            return getDtcSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSoprTecObj(AfDecimal dtcSoprTecObj) {
        if (dtcSoprTecObj != null) {
            setDtcSoprTec(new AfDecimal(dtcSoprTecObj, 15, 3));
            indDettTitCont.setSoprTec(((short)0));
        }
        else {
            indDettTitCont.setSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSpeAge() {
        return dettTitCont.getDtcSpeAge().getDtcSpeAge();
    }

    @Override
    public void setDtcSpeAge(AfDecimal dtcSpeAge) {
        this.dettTitCont.getDtcSpeAge().setDtcSpeAge(dtcSpeAge.copy());
    }

    @Override
    public AfDecimal getDtcSpeAgeObj() {
        if (indDettTitCont.getSpeAge() >= 0) {
            return getDtcSpeAge();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSpeAgeObj(AfDecimal dtcSpeAgeObj) {
        if (dtcSpeAgeObj != null) {
            setDtcSpeAge(new AfDecimal(dtcSpeAgeObj, 15, 3));
            indDettTitCont.setSpeAge(((short)0));
        }
        else {
            indDettTitCont.setSpeAge(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSpeMed() {
        return dettTitCont.getDtcSpeMed().getDtcSpeMed();
    }

    @Override
    public void setDtcSpeMed(AfDecimal dtcSpeMed) {
        this.dettTitCont.getDtcSpeMed().setDtcSpeMed(dtcSpeMed.copy());
    }

    @Override
    public AfDecimal getDtcSpeMedObj() {
        if (indDettTitCont.getSpeMed() >= 0) {
            return getDtcSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSpeMedObj(AfDecimal dtcSpeMedObj) {
        if (dtcSpeMedObj != null) {
            setDtcSpeMed(new AfDecimal(dtcSpeMedObj, 15, 3));
            indDettTitCont.setSpeMed(((short)0));
        }
        else {
            indDettTitCont.setSpeMed(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcTax() {
        return dettTitCont.getDtcTax().getDtcTax();
    }

    @Override
    public void setDtcTax(AfDecimal dtcTax) {
        this.dettTitCont.getDtcTax().setDtcTax(dtcTax.copy());
    }

    @Override
    public AfDecimal getDtcTaxObj() {
        if (indDettTitCont.getTax() >= 0) {
            return getDtcTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcTaxObj(AfDecimal dtcTaxObj) {
        if (dtcTaxObj != null) {
            setDtcTax(new AfDecimal(dtcTaxObj, 15, 3));
            indDettTitCont.setTax(((short)0));
        }
        else {
            indDettTitCont.setTax(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcTotIntrPrest() {
        return dettTitCont.getDtcTotIntrPrest().getDtcTotIntrPrest();
    }

    @Override
    public void setDtcTotIntrPrest(AfDecimal dtcTotIntrPrest) {
        this.dettTitCont.getDtcTotIntrPrest().setDtcTotIntrPrest(dtcTotIntrPrest.copy());
    }

    @Override
    public AfDecimal getDtcTotIntrPrestObj() {
        if (indDettTitCont.getTotIntrPrest() >= 0) {
            return getDtcTotIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcTotIntrPrestObj(AfDecimal dtcTotIntrPrestObj) {
        if (dtcTotIntrPrestObj != null) {
            setDtcTotIntrPrest(new AfDecimal(dtcTotIntrPrestObj, 15, 3));
            indDettTitCont.setTotIntrPrest(((short)0));
        }
        else {
            indDettTitCont.setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public String getDtcTpOgg() {
        return dettTitCont.getDtcTpOgg();
    }

    @Override
    public void setDtcTpOgg(String dtcTpOgg) {
        this.dettTitCont.setDtcTpOgg(dtcTpOgg);
    }

    @Override
    public String getDtcTpRgmFisc() {
        return dettTitCont.getDtcTpRgmFisc();
    }

    @Override
    public void setDtcTpRgmFisc(String dtcTpRgmFisc) {
        this.dettTitCont.setDtcTpRgmFisc(dtcTpRgmFisc);
    }

    @Override
    public String getDtcTpStatTit() {
        return dettTitCont.getDtcTpStatTit();
    }

    @Override
    public void setDtcTpStatTit(String dtcTpStatTit) {
        this.dettTitCont.setDtcTpStatTit(dtcTpStatTit);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndDettTitCont getIndDettTitCont() {
        return indDettTitCont;
    }

    public IndTitCont getIndTitCont() {
        return indTitCont;
    }

    @Override
    public int getLdbv0371IdOgg() {
        throw new FieldNotMappedException("ldbv0371IdOgg");
    }

    @Override
    public void setLdbv0371IdOgg(int ldbv0371IdOgg) {
        throw new FieldNotMappedException("ldbv0371IdOgg");
    }

    @Override
    public String getLdbv0371TpOgg() {
        throw new FieldNotMappedException("ldbv0371TpOgg");
    }

    @Override
    public void setLdbv0371TpOgg(String ldbv0371TpOgg) {
        throw new FieldNotMappedException("ldbv0371TpOgg");
    }

    @Override
    public String getLdbv0371TpPreTit() {
        throw new FieldNotMappedException("ldbv0371TpPreTit");
    }

    @Override
    public void setLdbv0371TpPreTit(String ldbv0371TpPreTit) {
        throw new FieldNotMappedException("ldbv0371TpPreTit");
    }

    @Override
    public String getLdbv0371TpStatTit1() {
        throw new FieldNotMappedException("ldbv0371TpStatTit1");
    }

    @Override
    public void setLdbv0371TpStatTit1(String ldbv0371TpStatTit1) {
        throw new FieldNotMappedException("ldbv0371TpStatTit1");
    }

    @Override
    public String getLdbv0371TpStatTit2() {
        throw new FieldNotMappedException("ldbv0371TpStatTit2");
    }

    @Override
    public void setLdbv0371TpStatTit2(String ldbv0371TpStatTit2) {
        throw new FieldNotMappedException("ldbv0371TpStatTit2");
    }

    @Override
    public int getLdbv2971IdOgg() {
        throw new FieldNotMappedException("ldbv2971IdOgg");
    }

    @Override
    public void setLdbv2971IdOgg(int ldbv2971IdOgg) {
        throw new FieldNotMappedException("ldbv2971IdOgg");
    }

    @Override
    public String getLdbv2971TpOgg() {
        throw new FieldNotMappedException("ldbv2971TpOgg");
    }

    @Override
    public void setLdbv2971TpOgg(String ldbv2971TpOgg) {
        throw new FieldNotMappedException("ldbv2971TpOgg");
    }

    @Override
    public String getLdbv3101DtADb() {
        throw new FieldNotMappedException("ldbv3101DtADb");
    }

    @Override
    public void setLdbv3101DtADb(String ldbv3101DtADb) {
        throw new FieldNotMappedException("ldbv3101DtADb");
    }

    @Override
    public String getLdbv3101DtDaDb() {
        throw new FieldNotMappedException("ldbv3101DtDaDb");
    }

    @Override
    public void setLdbv3101DtDaDb(String ldbv3101DtDaDb) {
        throw new FieldNotMappedException("ldbv3101DtDaDb");
    }

    @Override
    public int getLdbv3101IdOgg() {
        throw new FieldNotMappedException("ldbv3101IdOgg");
    }

    @Override
    public void setLdbv3101IdOgg(int ldbv3101IdOgg) {
        throw new FieldNotMappedException("ldbv3101IdOgg");
    }

    @Override
    public String getLdbv3101TpOgg() {
        throw new FieldNotMappedException("ldbv3101TpOgg");
    }

    @Override
    public void setLdbv3101TpOgg(String ldbv3101TpOgg) {
        throw new FieldNotMappedException("ldbv3101TpOgg");
    }

    @Override
    public String getLdbv3101TpStaTit() {
        throw new FieldNotMappedException("ldbv3101TpStaTit");
    }

    @Override
    public void setLdbv3101TpStaTit(String ldbv3101TpStaTit) {
        throw new FieldNotMappedException("ldbv3101TpStaTit");
    }

    @Override
    public String getLdbv3101TpTit() {
        throw new FieldNotMappedException("ldbv3101TpTit");
    }

    @Override
    public void setLdbv3101TpTit(String ldbv3101TpTit) {
        throw new FieldNotMappedException("ldbv3101TpTit");
    }

    @Override
    public String getLdbv7141DtADb() {
        throw new FieldNotMappedException("ldbv7141DtADb");
    }

    @Override
    public void setLdbv7141DtADb(String ldbv7141DtADb) {
        throw new FieldNotMappedException("ldbv7141DtADb");
    }

    @Override
    public String getLdbv7141DtDaDb() {
        throw new FieldNotMappedException("ldbv7141DtDaDb");
    }

    @Override
    public void setLdbv7141DtDaDb(String ldbv7141DtDaDb) {
        throw new FieldNotMappedException("ldbv7141DtDaDb");
    }

    @Override
    public int getLdbv7141IdOgg() {
        throw new FieldNotMappedException("ldbv7141IdOgg");
    }

    @Override
    public void setLdbv7141IdOgg(int ldbv7141IdOgg) {
        throw new FieldNotMappedException("ldbv7141IdOgg");
    }

    @Override
    public String getLdbv7141TpOgg() {
        throw new FieldNotMappedException("ldbv7141TpOgg");
    }

    @Override
    public void setLdbv7141TpOgg(String ldbv7141TpOgg) {
        throw new FieldNotMappedException("ldbv7141TpOgg");
    }

    @Override
    public String getLdbv7141TpStaTit1() {
        throw new FieldNotMappedException("ldbv7141TpStaTit1");
    }

    @Override
    public void setLdbv7141TpStaTit1(String ldbv7141TpStaTit1) {
        throw new FieldNotMappedException("ldbv7141TpStaTit1");
    }

    @Override
    public String getLdbv7141TpStaTit2() {
        throw new FieldNotMappedException("ldbv7141TpStaTit2");
    }

    @Override
    public void setLdbv7141TpStaTit2(String ldbv7141TpStaTit2) {
        throw new FieldNotMappedException("ldbv7141TpStaTit2");
    }

    @Override
    public String getLdbv7141TpTit() {
        throw new FieldNotMappedException("ldbv7141TpTit");
    }

    @Override
    public void setLdbv7141TpTit(String ldbv7141TpTit) {
        throw new FieldNotMappedException("ldbv7141TpTit");
    }

    @Override
    public int getLdbvd601IdOgg() {
        throw new FieldNotMappedException("ldbvd601IdOgg");
    }

    @Override
    public void setLdbvd601IdOgg(int ldbvd601IdOgg) {
        throw new FieldNotMappedException("ldbvd601IdOgg");
    }

    @Override
    public String getLdbvd601TpOgg() {
        throw new FieldNotMappedException("ldbvd601TpOgg");
    }

    @Override
    public void setLdbvd601TpOgg(String ldbvd601TpOgg) {
        throw new FieldNotMappedException("ldbvd601TpOgg");
    }

    @Override
    public String getLdbvd601TpStaTit() {
        throw new FieldNotMappedException("ldbvd601TpStaTit");
    }

    @Override
    public void setLdbvd601TpStaTit(String ldbvd601TpStaTit) {
        throw new FieldNotMappedException("ldbvd601TpStaTit");
    }

    @Override
    public String getLdbvd601TpTit() {
        throw new FieldNotMappedException("ldbvd601TpTit");
    }

    @Override
    public void setLdbvd601TpTit(String ldbvd601TpTit) {
        throw new FieldNotMappedException("ldbvd601TpTit");
    }

    @Override
    public int getTitCodCompAnia() {
        return titCont.getTitCodCompAnia();
    }

    @Override
    public void setTitCodCompAnia(int titCodCompAnia) {
        this.titCont.setTitCodCompAnia(titCodCompAnia);
    }

    @Override
    public String getTitCodDvs() {
        return titCont.getTitCodDvs();
    }

    @Override
    public void setTitCodDvs(String titCodDvs) {
        this.titCont.setTitCodDvs(titCodDvs);
    }

    @Override
    public String getTitCodDvsObj() {
        if (indTitCont.getCodDvs() >= 0) {
            return getTitCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitCodDvsObj(String titCodDvsObj) {
        if (titCodDvsObj != null) {
            setTitCodDvs(titCodDvsObj);
            indTitCont.setCodDvs(((short)0));
        }
        else {
            indTitCont.setCodDvs(((short)-1));
        }
    }

    @Override
    public String getTitCodIban() {
        return titCont.getTitCodIban();
    }

    @Override
    public void setTitCodIban(String titCodIban) {
        this.titCont.setTitCodIban(titCodIban);
    }

    @Override
    public String getTitCodIbanObj() {
        if (indTitCont.getCodIban() >= 0) {
            return getTitCodIban();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitCodIbanObj(String titCodIbanObj) {
        if (titCodIbanObj != null) {
            setTitCodIban(titCodIbanObj);
            indTitCont.setCodIban(((short)0));
        }
        else {
            indTitCont.setCodIban(((short)-1));
        }
    }

    public TitCont getTitCont() {
        return titCont;
    }

    public TitContDb getTitContDb() {
        return titContDb;
    }

    @Override
    public char getTitDsOperSql() {
        return titCont.getTitDsOperSql();
    }

    @Override
    public void setTitDsOperSql(char titDsOperSql) {
        this.titCont.setTitDsOperSql(titDsOperSql);
    }

    @Override
    public long getTitDsRiga() {
        return titCont.getTitDsRiga();
    }

    @Override
    public void setTitDsRiga(long titDsRiga) {
        this.titCont.setTitDsRiga(titDsRiga);
    }

    @Override
    public char getTitDsStatoElab() {
        return titCont.getTitDsStatoElab();
    }

    @Override
    public void setTitDsStatoElab(char titDsStatoElab) {
        this.titCont.setTitDsStatoElab(titDsStatoElab);
    }

    @Override
    public long getTitDsTsEndCptz() {
        return titCont.getTitDsTsEndCptz();
    }

    @Override
    public void setTitDsTsEndCptz(long titDsTsEndCptz) {
        this.titCont.setTitDsTsEndCptz(titDsTsEndCptz);
    }

    @Override
    public long getTitDsTsIniCptz() {
        return titCont.getTitDsTsIniCptz();
    }

    @Override
    public void setTitDsTsIniCptz(long titDsTsIniCptz) {
        this.titCont.setTitDsTsIniCptz(titDsTsIniCptz);
    }

    @Override
    public String getTitDsUtente() {
        return titCont.getTitDsUtente();
    }

    @Override
    public void setTitDsUtente(String titDsUtente) {
        this.titCont.setTitDsUtente(titDsUtente);
    }

    @Override
    public int getTitDsVer() {
        return titCont.getTitDsVer();
    }

    @Override
    public void setTitDsVer(int titDsVer) {
        this.titCont.setTitDsVer(titDsVer);
    }

    @Override
    public String getTitDtApplzMoraDb() {
        return titContDb.getApplzMoraDb();
    }

    @Override
    public void setTitDtApplzMoraDb(String titDtApplzMoraDb) {
        this.titContDb.setApplzMoraDb(titDtApplzMoraDb);
    }

    @Override
    public String getTitDtApplzMoraDbObj() {
        if (indTitCont.getDtApplzMora() >= 0) {
            return getTitDtApplzMoraDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtApplzMoraDbObj(String titDtApplzMoraDbObj) {
        if (titDtApplzMoraDbObj != null) {
            setTitDtApplzMoraDb(titDtApplzMoraDbObj);
            indTitCont.setDtApplzMora(((short)0));
        }
        else {
            indTitCont.setDtApplzMora(((short)-1));
        }
    }

    @Override
    public String getTitDtCambioVltDb() {
        return titContDb.getCambioVltDb();
    }

    @Override
    public void setTitDtCambioVltDb(String titDtCambioVltDb) {
        this.titContDb.setCambioVltDb(titDtCambioVltDb);
    }

    @Override
    public String getTitDtCambioVltDbObj() {
        if (indTitCont.getDtCambioVlt() >= 0) {
            return getTitDtCambioVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtCambioVltDbObj(String titDtCambioVltDbObj) {
        if (titDtCambioVltDbObj != null) {
            setTitDtCambioVltDb(titDtCambioVltDbObj);
            indTitCont.setDtCambioVlt(((short)0));
        }
        else {
            indTitCont.setDtCambioVlt(((short)-1));
        }
    }

    @Override
    public String getTitDtCertFiscDb() {
        return titContDb.getCertFiscDb();
    }

    @Override
    public void setTitDtCertFiscDb(String titDtCertFiscDb) {
        this.titContDb.setCertFiscDb(titDtCertFiscDb);
    }

    @Override
    public String getTitDtCertFiscDbObj() {
        if (indTitCont.getDtCertFisc() >= 0) {
            return getTitDtCertFiscDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtCertFiscDbObj(String titDtCertFiscDbObj) {
        if (titDtCertFiscDbObj != null) {
            setTitDtCertFiscDb(titDtCertFiscDbObj);
            indTitCont.setDtCertFisc(((short)0));
        }
        else {
            indTitCont.setDtCertFisc(((short)-1));
        }
    }

    @Override
    public String getTitDtEmisTitDb() {
        return titContDb.getEmisTitDb();
    }

    @Override
    public void setTitDtEmisTitDb(String titDtEmisTitDb) {
        this.titContDb.setEmisTitDb(titDtEmisTitDb);
    }

    @Override
    public String getTitDtEmisTitDbObj() {
        if (indTitCont.getDtEmisTit() >= 0) {
            return getTitDtEmisTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtEmisTitDbObj(String titDtEmisTitDbObj) {
        if (titDtEmisTitDbObj != null) {
            setTitDtEmisTitDb(titDtEmisTitDbObj);
            indTitCont.setDtEmisTit(((short)0));
        }
        else {
            indTitCont.setDtEmisTit(((short)-1));
        }
    }

    @Override
    public String getTitDtEndCopDb() {
        return titContDb.getEndCopDb();
    }

    @Override
    public void setTitDtEndCopDb(String titDtEndCopDb) {
        this.titContDb.setEndCopDb(titDtEndCopDb);
    }

    @Override
    public String getTitDtEndCopDbObj() {
        if (indTitCont.getDtEndCop() >= 0) {
            return getTitDtEndCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtEndCopDbObj(String titDtEndCopDbObj) {
        if (titDtEndCopDbObj != null) {
            setTitDtEndCopDb(titDtEndCopDbObj);
            indTitCont.setDtEndCop(((short)0));
        }
        else {
            indTitCont.setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getTitDtEndEffDb() {
        return titContDb.getEndEffDb();
    }

    @Override
    public void setTitDtEndEffDb(String titDtEndEffDb) {
        this.titContDb.setEndEffDb(titDtEndEffDb);
    }

    @Override
    public String getTitDtEsiTitDb() {
        return titContDb.getEsiTitDb();
    }

    @Override
    public void setTitDtEsiTitDb(String titDtEsiTitDb) {
        this.titContDb.setEsiTitDb(titDtEsiTitDb);
    }

    @Override
    public String getTitDtEsiTitDbObj() {
        if (indTitCont.getDtEsiTit() >= 0) {
            return getTitDtEsiTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtEsiTitDbObj(String titDtEsiTitDbObj) {
        if (titDtEsiTitDbObj != null) {
            setTitDtEsiTitDb(titDtEsiTitDbObj);
            indTitCont.setDtEsiTit(((short)0));
        }
        else {
            indTitCont.setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getTitDtIniCopDb() {
        return titContDb.getIniCopDb();
    }

    @Override
    public void setTitDtIniCopDb(String titDtIniCopDb) {
        this.titContDb.setIniCopDb(titDtIniCopDb);
    }

    @Override
    public String getTitDtIniCopDbObj() {
        if (indTitCont.getDtIniCop() >= 0) {
            return getTitDtIniCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtIniCopDbObj(String titDtIniCopDbObj) {
        if (titDtIniCopDbObj != null) {
            setTitDtIniCopDb(titDtIniCopDbObj);
            indTitCont.setDtIniCop(((short)0));
        }
        else {
            indTitCont.setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getTitDtIniEffDb() {
        return titContDb.getIniEffDb();
    }

    @Override
    public void setTitDtIniEffDb(String titDtIniEffDb) {
        this.titContDb.setIniEffDb(titDtIniEffDb);
    }

    @Override
    public String getTitDtRichAddRidDb() {
        return titContDb.getRichAddRidDb();
    }

    @Override
    public void setTitDtRichAddRidDb(String titDtRichAddRidDb) {
        this.titContDb.setRichAddRidDb(titDtRichAddRidDb);
    }

    @Override
    public String getTitDtRichAddRidDbObj() {
        if (indTitCont.getDtRichAddRid() >= 0) {
            return getTitDtRichAddRidDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtRichAddRidDbObj(String titDtRichAddRidDbObj) {
        if (titDtRichAddRidDbObj != null) {
            setTitDtRichAddRidDb(titDtRichAddRidDbObj);
            indTitCont.setDtRichAddRid(((short)0));
        }
        else {
            indTitCont.setDtRichAddRid(((short)-1));
        }
    }

    @Override
    public String getTitDtVltDb() {
        return titContDb.getVltDb();
    }

    @Override
    public void setTitDtVltDb(String titDtVltDb) {
        this.titContDb.setVltDb(titDtVltDb);
    }

    @Override
    public String getTitDtVltDbObj() {
        if (indTitCont.getDtVlt() >= 0) {
            return getTitDtVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtVltDbObj(String titDtVltDbObj) {
        if (titDtVltDbObj != null) {
            setTitDtVltDb(titDtVltDbObj);
            indTitCont.setDtVlt(((short)0));
        }
        else {
            indTitCont.setDtVlt(((short)-1));
        }
    }

    @Override
    public String getTitEstrCntCorrAdd() {
        return titCont.getTitEstrCntCorrAdd();
    }

    @Override
    public void setTitEstrCntCorrAdd(String titEstrCntCorrAdd) {
        this.titCont.setTitEstrCntCorrAdd(titEstrCntCorrAdd);
    }

    @Override
    public String getTitEstrCntCorrAddObj() {
        if (indTitCont.getEstrCntCorrAdd() >= 0) {
            return getTitEstrCntCorrAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitEstrCntCorrAddObj(String titEstrCntCorrAddObj) {
        if (titEstrCntCorrAddObj != null) {
            setTitEstrCntCorrAdd(titEstrCntCorrAddObj);
            indTitCont.setEstrCntCorrAdd(((short)0));
        }
        else {
            indTitCont.setEstrCntCorrAdd(((short)-1));
        }
    }

    @Override
    public char getTitFlForzDtVlt() {
        return titCont.getTitFlForzDtVlt();
    }

    @Override
    public void setTitFlForzDtVlt(char titFlForzDtVlt) {
        this.titCont.setTitFlForzDtVlt(titFlForzDtVlt);
    }

    @Override
    public Character getTitFlForzDtVltObj() {
        if (indTitCont.getFlForzDtVlt() >= 0) {
            return ((Character)getTitFlForzDtVlt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlForzDtVltObj(Character titFlForzDtVltObj) {
        if (titFlForzDtVltObj != null) {
            setTitFlForzDtVlt(((char)titFlForzDtVltObj));
            indTitCont.setFlForzDtVlt(((short)0));
        }
        else {
            indTitCont.setFlForzDtVlt(((short)-1));
        }
    }

    @Override
    public char getTitFlIncAutogen() {
        return titCont.getTitFlIncAutogen();
    }

    @Override
    public void setTitFlIncAutogen(char titFlIncAutogen) {
        this.titCont.setTitFlIncAutogen(titFlIncAutogen);
    }

    @Override
    public Character getTitFlIncAutogenObj() {
        if (indTitCont.getFlIncAutogen() >= 0) {
            return ((Character)getTitFlIncAutogen());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlIncAutogenObj(Character titFlIncAutogenObj) {
        if (titFlIncAutogenObj != null) {
            setTitFlIncAutogen(((char)titFlIncAutogenObj));
            indTitCont.setFlIncAutogen(((short)0));
        }
        else {
            indTitCont.setFlIncAutogen(((short)-1));
        }
    }

    @Override
    public char getTitFlMora() {
        return titCont.getTitFlMora();
    }

    @Override
    public void setTitFlMora(char titFlMora) {
        this.titCont.setTitFlMora(titFlMora);
    }

    @Override
    public Character getTitFlMoraObj() {
        if (indTitCont.getFlMora() >= 0) {
            return ((Character)getTitFlMora());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlMoraObj(Character titFlMoraObj) {
        if (titFlMoraObj != null) {
            setTitFlMora(((char)titFlMoraObj));
            indTitCont.setFlMora(((short)0));
        }
        else {
            indTitCont.setFlMora(((short)-1));
        }
    }

    @Override
    public char getTitFlSoll() {
        return titCont.getTitFlSoll();
    }

    @Override
    public void setTitFlSoll(char titFlSoll) {
        this.titCont.setTitFlSoll(titFlSoll);
    }

    @Override
    public Character getTitFlSollObj() {
        if (indTitCont.getFlSoll() >= 0) {
            return ((Character)getTitFlSoll());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlSollObj(Character titFlSollObj) {
        if (titFlSollObj != null) {
            setTitFlSoll(((char)titFlSollObj));
            indTitCont.setFlSoll(((short)0));
        }
        else {
            indTitCont.setFlSoll(((short)-1));
        }
    }

    @Override
    public char getTitFlTitDaReinvst() {
        return titCont.getTitFlTitDaReinvst();
    }

    @Override
    public void setTitFlTitDaReinvst(char titFlTitDaReinvst) {
        this.titCont.setTitFlTitDaReinvst(titFlTitDaReinvst);
    }

    @Override
    public Character getTitFlTitDaReinvstObj() {
        if (indTitCont.getFlTitDaReinvst() >= 0) {
            return ((Character)getTitFlTitDaReinvst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlTitDaReinvstObj(Character titFlTitDaReinvstObj) {
        if (titFlTitDaReinvstObj != null) {
            setTitFlTitDaReinvst(((char)titFlTitDaReinvstObj));
            indTitCont.setFlTitDaReinvst(((short)0));
        }
        else {
            indTitCont.setFlTitDaReinvst(((short)-1));
        }
    }

    @Override
    public int getTitFraz() {
        return titCont.getTitFraz().getTitFraz();
    }

    @Override
    public void setTitFraz(int titFraz) {
        this.titCont.getTitFraz().setTitFraz(titFraz);
    }

    @Override
    public Integer getTitFrazObj() {
        if (indTitCont.getFraz() >= 0) {
            return ((Integer)getTitFraz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFrazObj(Integer titFrazObj) {
        if (titFrazObj != null) {
            setTitFraz(((int)titFrazObj));
            indTitCont.setFraz(((short)0));
        }
        else {
            indTitCont.setFraz(((short)-1));
        }
    }

    @Override
    public String getTitIbRich() {
        return titCont.getTitIbRich();
    }

    @Override
    public void setTitIbRich(String titIbRich) {
        this.titCont.setTitIbRich(titIbRich);
    }

    @Override
    public String getTitIbRichObj() {
        if (indTitCont.getIbRich() >= 0) {
            return getTitIbRich();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIbRichObj(String titIbRichObj) {
        if (titIbRichObj != null) {
            setTitIbRich(titIbRichObj);
            indTitCont.setIbRich(((short)0));
        }
        else {
            indTitCont.setIbRich(((short)-1));
        }
    }

    @Override
    public int getTitIdMoviChiu() {
        return titCont.getTitIdMoviChiu().getTitIdMoviChiu();
    }

    @Override
    public void setTitIdMoviChiu(int titIdMoviChiu) {
        this.titCont.getTitIdMoviChiu().setTitIdMoviChiu(titIdMoviChiu);
    }

    @Override
    public Integer getTitIdMoviChiuObj() {
        if (indTitCont.getIdMoviChiu() >= 0) {
            return ((Integer)getTitIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIdMoviChiuObj(Integer titIdMoviChiuObj) {
        if (titIdMoviChiuObj != null) {
            setTitIdMoviChiu(((int)titIdMoviChiuObj));
            indTitCont.setIdMoviChiu(((short)0));
        }
        else {
            indTitCont.setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getTitIdMoviCrz() {
        return titCont.getTitIdMoviCrz();
    }

    @Override
    public void setTitIdMoviCrz(int titIdMoviCrz) {
        this.titCont.setTitIdMoviCrz(titIdMoviCrz);
    }

    @Override
    public int getTitIdOgg() {
        return titCont.getTitIdOgg();
    }

    @Override
    public void setTitIdOgg(int titIdOgg) {
        this.titCont.setTitIdOgg(titIdOgg);
    }

    @Override
    public int getTitIdRappAna() {
        return titCont.getTitIdRappAna().getTitIdRappAna();
    }

    @Override
    public void setTitIdRappAna(int titIdRappAna) {
        this.titCont.getTitIdRappAna().setTitIdRappAna(titIdRappAna);
    }

    @Override
    public Integer getTitIdRappAnaObj() {
        if (indTitCont.getIdRappAna() >= 0) {
            return ((Integer)getTitIdRappAna());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIdRappAnaObj(Integer titIdRappAnaObj) {
        if (titIdRappAnaObj != null) {
            setTitIdRappAna(((int)titIdRappAnaObj));
            indTitCont.setIdRappAna(((short)0));
        }
        else {
            indTitCont.setIdRappAna(((short)-1));
        }
    }

    @Override
    public int getTitIdRappRete() {
        return titCont.getTitIdRappRete().getTitIdRappRete();
    }

    @Override
    public void setTitIdRappRete(int titIdRappRete) {
        this.titCont.getTitIdRappRete().setTitIdRappRete(titIdRappRete);
    }

    @Override
    public Integer getTitIdRappReteObj() {
        if (indTitCont.getIdRappRete() >= 0) {
            return ((Integer)getTitIdRappRete());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIdRappReteObj(Integer titIdRappReteObj) {
        if (titIdRappReteObj != null) {
            setTitIdRappRete(((int)titIdRappReteObj));
            indTitCont.setIdRappRete(((short)0));
        }
        else {
            indTitCont.setIdRappRete(((short)-1));
        }
    }

    @Override
    public int getTitIdTitCont() {
        return titCont.getTitIdTitCont();
    }

    @Override
    public void setTitIdTitCont(int titIdTitCont) {
        this.titCont.setTitIdTitCont(titIdTitCont);
    }

    @Override
    public AfDecimal getTitImpAder() {
        return titCont.getTitImpAder().getTitImpAder();
    }

    @Override
    public void setTitImpAder(AfDecimal titImpAder) {
        this.titCont.getTitImpAder().setTitImpAder(titImpAder.copy());
    }

    @Override
    public AfDecimal getTitImpAderObj() {
        if (indTitCont.getImpAder() >= 0) {
            return getTitImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpAderObj(AfDecimal titImpAderObj) {
        if (titImpAderObj != null) {
            setTitImpAder(new AfDecimal(titImpAderObj, 15, 3));
            indTitCont.setImpAder(((short)0));
        }
        else {
            indTitCont.setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpAz() {
        return titCont.getTitImpAz().getTitImpAz();
    }

    @Override
    public void setTitImpAz(AfDecimal titImpAz) {
        this.titCont.getTitImpAz().setTitImpAz(titImpAz.copy());
    }

    @Override
    public AfDecimal getTitImpAzObj() {
        if (indTitCont.getImpAz() >= 0) {
            return getTitImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpAzObj(AfDecimal titImpAzObj) {
        if (titImpAzObj != null) {
            setTitImpAz(new AfDecimal(titImpAzObj, 15, 3));
            indTitCont.setImpAz(((short)0));
        }
        else {
            indTitCont.setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpPag() {
        return titCont.getTitImpPag().getTitImpPag();
    }

    @Override
    public void setTitImpPag(AfDecimal titImpPag) {
        this.titCont.getTitImpPag().setTitImpPag(titImpPag.copy());
    }

    @Override
    public AfDecimal getTitImpPagObj() {
        if (indTitCont.getImpPag() >= 0) {
            return getTitImpPag();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpPagObj(AfDecimal titImpPagObj) {
        if (titImpPagObj != null) {
            setTitImpPag(new AfDecimal(titImpPagObj, 15, 3));
            indTitCont.setImpPag(((short)0));
        }
        else {
            indTitCont.setImpPag(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpTfr() {
        return titCont.getTitImpTfr().getTitImpTfr();
    }

    @Override
    public void setTitImpTfr(AfDecimal titImpTfr) {
        this.titCont.getTitImpTfr().setTitImpTfr(titImpTfr.copy());
    }

    @Override
    public AfDecimal getTitImpTfrObj() {
        if (indTitCont.getImpTfr() >= 0) {
            return getTitImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpTfrObj(AfDecimal titImpTfrObj) {
        if (titImpTfrObj != null) {
            setTitImpTfr(new AfDecimal(titImpTfrObj, 15, 3));
            indTitCont.setImpTfr(((short)0));
        }
        else {
            indTitCont.setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpTfrStrc() {
        return titCont.getTitImpTfrStrc().getTitImpTfrStrc();
    }

    @Override
    public void setTitImpTfrStrc(AfDecimal titImpTfrStrc) {
        this.titCont.getTitImpTfrStrc().setTitImpTfrStrc(titImpTfrStrc.copy());
    }

    @Override
    public AfDecimal getTitImpTfrStrcObj() {
        if (indTitCont.getImpTfrStrc() >= 0) {
            return getTitImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpTfrStrcObj(AfDecimal titImpTfrStrcObj) {
        if (titImpTfrStrcObj != null) {
            setTitImpTfrStrc(new AfDecimal(titImpTfrStrcObj, 15, 3));
            indTitCont.setImpTfrStrc(((short)0));
        }
        else {
            indTitCont.setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpTrasfe() {
        return titCont.getTitImpTrasfe().getTitImpTrasfe();
    }

    @Override
    public void setTitImpTrasfe(AfDecimal titImpTrasfe) {
        this.titCont.getTitImpTrasfe().setTitImpTrasfe(titImpTrasfe.copy());
    }

    @Override
    public AfDecimal getTitImpTrasfeObj() {
        if (indTitCont.getImpTrasfe() >= 0) {
            return getTitImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpTrasfeObj(AfDecimal titImpTrasfeObj) {
        if (titImpTrasfeObj != null) {
            setTitImpTrasfe(new AfDecimal(titImpTrasfeObj, 15, 3));
            indTitCont.setImpTrasfe(((short)0));
        }
        else {
            indTitCont.setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpVolo() {
        return titCont.getTitImpVolo().getTitImpVolo();
    }

    @Override
    public void setTitImpVolo(AfDecimal titImpVolo) {
        this.titCont.getTitImpVolo().setTitImpVolo(titImpVolo.copy());
    }

    @Override
    public AfDecimal getTitImpVoloObj() {
        if (indTitCont.getImpVolo() >= 0) {
            return getTitImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpVoloObj(AfDecimal titImpVoloObj) {
        if (titImpVoloObj != null) {
            setTitImpVolo(new AfDecimal(titImpVoloObj, 15, 3));
            indTitCont.setImpVolo(((short)0));
        }
        else {
            indTitCont.setImpVolo(((short)-1));
        }
    }

    @Override
    public int getTitNumRatAccorpate() {
        return titCont.getTitNumRatAccorpate().getTitNumRatAccorpate();
    }

    @Override
    public void setTitNumRatAccorpate(int titNumRatAccorpate) {
        this.titCont.getTitNumRatAccorpate().setTitNumRatAccorpate(titNumRatAccorpate);
    }

    @Override
    public Integer getTitNumRatAccorpateObj() {
        if (indTitCont.getNumRatAccorpate() >= 0) {
            return ((Integer)getTitNumRatAccorpate());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitNumRatAccorpateObj(Integer titNumRatAccorpateObj) {
        if (titNumRatAccorpateObj != null) {
            setTitNumRatAccorpate(((int)titNumRatAccorpateObj));
            indTitCont.setNumRatAccorpate(((short)0));
        }
        else {
            indTitCont.setNumRatAccorpate(((short)-1));
        }
    }

    @Override
    public int getTitProgTit() {
        return titCont.getTitProgTit().getTitProgTit();
    }

    @Override
    public void setTitProgTit(int titProgTit) {
        this.titCont.getTitProgTit().setTitProgTit(titProgTit);
    }

    @Override
    public Integer getTitProgTitObj() {
        if (indTitCont.getProgTit() >= 0) {
            return ((Integer)getTitProgTit());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitProgTitObj(Integer titProgTitObj) {
        if (titProgTitObj != null) {
            setTitProgTit(((int)titProgTitObj));
            indTitCont.setProgTit(((short)0));
        }
        else {
            indTitCont.setProgTit(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotAcqExp() {
        return titCont.getTitTotAcqExp().getTitTotAcqExp();
    }

    @Override
    public void setTitTotAcqExp(AfDecimal titTotAcqExp) {
        this.titCont.getTitTotAcqExp().setTitTotAcqExp(titTotAcqExp.copy());
    }

    @Override
    public AfDecimal getTitTotAcqExpObj() {
        if (indTitCont.getTotAcqExp() >= 0) {
            return getTitTotAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotAcqExpObj(AfDecimal titTotAcqExpObj) {
        if (titTotAcqExpObj != null) {
            setTitTotAcqExp(new AfDecimal(titTotAcqExpObj, 15, 3));
            indTitCont.setTotAcqExp(((short)0));
        }
        else {
            indTitCont.setTotAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarAcq() {
        return titCont.getTitTotCarAcq().getTitTotCarAcq();
    }

    @Override
    public void setTitTotCarAcq(AfDecimal titTotCarAcq) {
        this.titCont.getTitTotCarAcq().setTitTotCarAcq(titTotCarAcq.copy());
    }

    @Override
    public AfDecimal getTitTotCarAcqObj() {
        if (indTitCont.getTotCarAcq() >= 0) {
            return getTitTotCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarAcqObj(AfDecimal titTotCarAcqObj) {
        if (titTotCarAcqObj != null) {
            setTitTotCarAcq(new AfDecimal(titTotCarAcqObj, 15, 3));
            indTitCont.setTotCarAcq(((short)0));
        }
        else {
            indTitCont.setTotCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarGest() {
        return titCont.getTitTotCarGest().getTitTotCarGest();
    }

    @Override
    public void setTitTotCarGest(AfDecimal titTotCarGest) {
        this.titCont.getTitTotCarGest().setTitTotCarGest(titTotCarGest.copy());
    }

    @Override
    public AfDecimal getTitTotCarGestObj() {
        if (indTitCont.getTotCarGest() >= 0) {
            return getTitTotCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarGestObj(AfDecimal titTotCarGestObj) {
        if (titTotCarGestObj != null) {
            setTitTotCarGest(new AfDecimal(titTotCarGestObj, 15, 3));
            indTitCont.setTotCarGest(((short)0));
        }
        else {
            indTitCont.setTotCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarIas() {
        return titCont.getTitTotCarIas().getTitTotCarIas();
    }

    @Override
    public void setTitTotCarIas(AfDecimal titTotCarIas) {
        this.titCont.getTitTotCarIas().setTitTotCarIas(titTotCarIas.copy());
    }

    @Override
    public AfDecimal getTitTotCarIasObj() {
        if (indTitCont.getTotCarIas() >= 0) {
            return getTitTotCarIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarIasObj(AfDecimal titTotCarIasObj) {
        if (titTotCarIasObj != null) {
            setTitTotCarIas(new AfDecimal(titTotCarIasObj, 15, 3));
            indTitCont.setTotCarIas(((short)0));
        }
        else {
            indTitCont.setTotCarIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarInc() {
        return titCont.getTitTotCarInc().getTitTotCarInc();
    }

    @Override
    public void setTitTotCarInc(AfDecimal titTotCarInc) {
        this.titCont.getTitTotCarInc().setTitTotCarInc(titTotCarInc.copy());
    }

    @Override
    public AfDecimal getTitTotCarIncObj() {
        if (indTitCont.getTotCarInc() >= 0) {
            return getTitTotCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarIncObj(AfDecimal titTotCarIncObj) {
        if (titTotCarIncObj != null) {
            setTitTotCarInc(new AfDecimal(titTotCarIncObj, 15, 3));
            indTitCont.setTotCarInc(((short)0));
        }
        else {
            indTitCont.setTotCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCnbtAntirac() {
        return titCont.getTitTotCnbtAntirac().getTitTotCnbtAntirac();
    }

    @Override
    public void setTitTotCnbtAntirac(AfDecimal titTotCnbtAntirac) {
        this.titCont.getTitTotCnbtAntirac().setTitTotCnbtAntirac(titTotCnbtAntirac.copy());
    }

    @Override
    public AfDecimal getTitTotCnbtAntiracObj() {
        if (indTitCont.getTotCnbtAntirac() >= 0) {
            return getTitTotCnbtAntirac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCnbtAntiracObj(AfDecimal titTotCnbtAntiracObj) {
        if (titTotCnbtAntiracObj != null) {
            setTitTotCnbtAntirac(new AfDecimal(titTotCnbtAntiracObj, 15, 3));
            indTitCont.setTotCnbtAntirac(((short)0));
        }
        else {
            indTitCont.setTotCnbtAntirac(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCommisInter() {
        return titCont.getTitTotCommisInter().getTitTotCommisInter();
    }

    @Override
    public void setTitTotCommisInter(AfDecimal titTotCommisInter) {
        this.titCont.getTitTotCommisInter().setTitTotCommisInter(titTotCommisInter.copy());
    }

    @Override
    public AfDecimal getTitTotCommisInterObj() {
        if (indTitCont.getTotCommisInter() >= 0) {
            return getTitTotCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCommisInterObj(AfDecimal titTotCommisInterObj) {
        if (titTotCommisInterObj != null) {
            setTitTotCommisInter(new AfDecimal(titTotCommisInterObj, 15, 3));
            indTitCont.setTotCommisInter(((short)0));
        }
        else {
            indTitCont.setTotCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotDir() {
        return titCont.getTitTotDir().getTitTotDir();
    }

    @Override
    public void setTitTotDir(AfDecimal titTotDir) {
        this.titCont.getTitTotDir().setTitTotDir(titTotDir.copy());
    }

    @Override
    public AfDecimal getTitTotDirObj() {
        if (indTitCont.getTotDir() >= 0) {
            return getTitTotDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotDirObj(AfDecimal titTotDirObj) {
        if (titTotDirObj != null) {
            setTitTotDir(new AfDecimal(titTotDirObj, 15, 3));
            indTitCont.setTotDir(((short)0));
        }
        else {
            indTitCont.setTotDir(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrFraz() {
        return titCont.getTitTotIntrFraz().getTitTotIntrFraz();
    }

    @Override
    public void setTitTotIntrFraz(AfDecimal titTotIntrFraz) {
        this.titCont.getTitTotIntrFraz().setTitTotIntrFraz(titTotIntrFraz.copy());
    }

    @Override
    public AfDecimal getTitTotIntrFrazObj() {
        if (indTitCont.getTotIntrFraz() >= 0) {
            return getTitTotIntrFraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrFrazObj(AfDecimal titTotIntrFrazObj) {
        if (titTotIntrFrazObj != null) {
            setTitTotIntrFraz(new AfDecimal(titTotIntrFrazObj, 15, 3));
            indTitCont.setTotIntrFraz(((short)0));
        }
        else {
            indTitCont.setTotIntrFraz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrMora() {
        return titCont.getTitTotIntrMora().getTitTotIntrMora();
    }

    @Override
    public void setTitTotIntrMora(AfDecimal titTotIntrMora) {
        this.titCont.getTitTotIntrMora().setTitTotIntrMora(titTotIntrMora.copy());
    }

    @Override
    public AfDecimal getTitTotIntrMoraObj() {
        if (indTitCont.getTotIntrMora() >= 0) {
            return getTitTotIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrMoraObj(AfDecimal titTotIntrMoraObj) {
        if (titTotIntrMoraObj != null) {
            setTitTotIntrMora(new AfDecimal(titTotIntrMoraObj, 15, 3));
            indTitCont.setTotIntrMora(((short)0));
        }
        else {
            indTitCont.setTotIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrPrest() {
        return titCont.getTitTotIntrPrest().getTitTotIntrPrest();
    }

    @Override
    public void setTitTotIntrPrest(AfDecimal titTotIntrPrest) {
        this.titCont.getTitTotIntrPrest().setTitTotIntrPrest(titTotIntrPrest.copy());
    }

    @Override
    public AfDecimal getTitTotIntrPrestObj() {
        if (indTitCont.getTotIntrPrest() >= 0) {
            return getTitTotIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrPrestObj(AfDecimal titTotIntrPrestObj) {
        if (titTotIntrPrestObj != null) {
            setTitTotIntrPrest(new AfDecimal(titTotIntrPrestObj, 15, 3));
            indTitCont.setTotIntrPrest(((short)0));
        }
        else {
            indTitCont.setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrRetdt() {
        return titCont.getTitTotIntrRetdt().getTitTotIntrRetdt();
    }

    @Override
    public void setTitTotIntrRetdt(AfDecimal titTotIntrRetdt) {
        this.titCont.getTitTotIntrRetdt().setTitTotIntrRetdt(titTotIntrRetdt.copy());
    }

    @Override
    public AfDecimal getTitTotIntrRetdtObj() {
        if (indTitCont.getTotIntrRetdt() >= 0) {
            return getTitTotIntrRetdt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrRetdtObj(AfDecimal titTotIntrRetdtObj) {
        if (titTotIntrRetdtObj != null) {
            setTitTotIntrRetdt(new AfDecimal(titTotIntrRetdtObj, 15, 3));
            indTitCont.setTotIntrRetdt(((short)0));
        }
        else {
            indTitCont.setTotIntrRetdt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrRiat() {
        return titCont.getTitTotIntrRiat().getTitTotIntrRiat();
    }

    @Override
    public void setTitTotIntrRiat(AfDecimal titTotIntrRiat) {
        this.titCont.getTitTotIntrRiat().setTitTotIntrRiat(titTotIntrRiat.copy());
    }

    @Override
    public AfDecimal getTitTotIntrRiatObj() {
        if (indTitCont.getTotIntrRiat() >= 0) {
            return getTitTotIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrRiatObj(AfDecimal titTotIntrRiatObj) {
        if (titTotIntrRiatObj != null) {
            setTitTotIntrRiat(new AfDecimal(titTotIntrRiatObj, 15, 3));
            indTitCont.setTotIntrRiat(((short)0));
        }
        else {
            indTitCont.setTotIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotManfeeAntic() {
        return titCont.getTitTotManfeeAntic().getTitTotManfeeAntic();
    }

    @Override
    public void setTitTotManfeeAntic(AfDecimal titTotManfeeAntic) {
        this.titCont.getTitTotManfeeAntic().setTitTotManfeeAntic(titTotManfeeAntic.copy());
    }

    @Override
    public AfDecimal getTitTotManfeeAnticObj() {
        if (indTitCont.getTotManfeeAntic() >= 0) {
            return getTitTotManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotManfeeAnticObj(AfDecimal titTotManfeeAnticObj) {
        if (titTotManfeeAnticObj != null) {
            setTitTotManfeeAntic(new AfDecimal(titTotManfeeAnticObj, 15, 3));
            indTitCont.setTotManfeeAntic(((short)0));
        }
        else {
            indTitCont.setTotManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotManfeeRec() {
        return titCont.getTitTotManfeeRec().getTitTotManfeeRec();
    }

    @Override
    public void setTitTotManfeeRec(AfDecimal titTotManfeeRec) {
        this.titCont.getTitTotManfeeRec().setTitTotManfeeRec(titTotManfeeRec.copy());
    }

    @Override
    public AfDecimal getTitTotManfeeRecObj() {
        if (indTitCont.getTotManfeeRec() >= 0) {
            return getTitTotManfeeRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotManfeeRecObj(AfDecimal titTotManfeeRecObj) {
        if (titTotManfeeRecObj != null) {
            setTitTotManfeeRec(new AfDecimal(titTotManfeeRecObj, 15, 3));
            indTitCont.setTotManfeeRec(((short)0));
        }
        else {
            indTitCont.setTotManfeeRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotManfeeRicor() {
        return titCont.getTitTotManfeeRicor().getTitTotManfeeRicor();
    }

    @Override
    public void setTitTotManfeeRicor(AfDecimal titTotManfeeRicor) {
        this.titCont.getTitTotManfeeRicor().setTitTotManfeeRicor(titTotManfeeRicor.copy());
    }

    @Override
    public AfDecimal getTitTotManfeeRicorObj() {
        if (indTitCont.getTotManfeeRicor() >= 0) {
            return getTitTotManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotManfeeRicorObj(AfDecimal titTotManfeeRicorObj) {
        if (titTotManfeeRicorObj != null) {
            setTitTotManfeeRicor(new AfDecimal(titTotManfeeRicorObj, 15, 3));
            indTitCont.setTotManfeeRicor(((short)0));
        }
        else {
            indTitCont.setTotManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPreNet() {
        return titCont.getTitTotPreNet().getTitTotPreNet();
    }

    @Override
    public void setTitTotPreNet(AfDecimal titTotPreNet) {
        this.titCont.getTitTotPreNet().setTitTotPreNet(titTotPreNet.copy());
    }

    @Override
    public AfDecimal getTitTotPreNetObj() {
        if (indTitCont.getTotPreNet() >= 0) {
            return getTitTotPreNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPreNetObj(AfDecimal titTotPreNetObj) {
        if (titTotPreNetObj != null) {
            setTitTotPreNet(new AfDecimal(titTotPreNetObj, 15, 3));
            indTitCont.setTotPreNet(((short)0));
        }
        else {
            indTitCont.setTotPreNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPrePpIas() {
        return titCont.getTitTotPrePpIas().getTitTotPrePpIas();
    }

    @Override
    public void setTitTotPrePpIas(AfDecimal titTotPrePpIas) {
        this.titCont.getTitTotPrePpIas().setTitTotPrePpIas(titTotPrePpIas.copy());
    }

    @Override
    public AfDecimal getTitTotPrePpIasObj() {
        if (indTitCont.getTotPrePpIas() >= 0) {
            return getTitTotPrePpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPrePpIasObj(AfDecimal titTotPrePpIasObj) {
        if (titTotPrePpIasObj != null) {
            setTitTotPrePpIas(new AfDecimal(titTotPrePpIasObj, 15, 3));
            indTitCont.setTotPrePpIas(((short)0));
        }
        else {
            indTitCont.setTotPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPreSoloRsh() {
        return titCont.getTitTotPreSoloRsh().getTitTotPreSoloRsh();
    }

    @Override
    public void setTitTotPreSoloRsh(AfDecimal titTotPreSoloRsh) {
        this.titCont.getTitTotPreSoloRsh().setTitTotPreSoloRsh(titTotPreSoloRsh.copy());
    }

    @Override
    public AfDecimal getTitTotPreSoloRshObj() {
        if (indTitCont.getTotPreSoloRsh() >= 0) {
            return getTitTotPreSoloRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPreSoloRshObj(AfDecimal titTotPreSoloRshObj) {
        if (titTotPreSoloRshObj != null) {
            setTitTotPreSoloRsh(new AfDecimal(titTotPreSoloRshObj, 15, 3));
            indTitCont.setTotPreSoloRsh(((short)0));
        }
        else {
            indTitCont.setTotPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPreTot() {
        return titCont.getTitTotPreTot().getTitTotPreTot();
    }

    @Override
    public void setTitTotPreTot(AfDecimal titTotPreTot) {
        this.titCont.getTitTotPreTot().setTitTotPreTot(titTotPreTot.copy());
    }

    @Override
    public AfDecimal getTitTotPreTotObj() {
        if (indTitCont.getTotPreTot() >= 0) {
            return getTitTotPreTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPreTotObj(AfDecimal titTotPreTotObj) {
        if (titTotPreTotObj != null) {
            setTitTotPreTot(new AfDecimal(titTotPreTotObj, 15, 3));
            indTitCont.setTotPreTot(((short)0));
        }
        else {
            indTitCont.setTotPreTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvAcq1aa() {
        return titCont.getTitTotProvAcq1aa().getTitTotProvAcq1aa();
    }

    @Override
    public void setTitTotProvAcq1aa(AfDecimal titTotProvAcq1aa) {
        this.titCont.getTitTotProvAcq1aa().setTitTotProvAcq1aa(titTotProvAcq1aa.copy());
    }

    @Override
    public AfDecimal getTitTotProvAcq1aaObj() {
        if (indTitCont.getTotProvAcq1aa() >= 0) {
            return getTitTotProvAcq1aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvAcq1aaObj(AfDecimal titTotProvAcq1aaObj) {
        if (titTotProvAcq1aaObj != null) {
            setTitTotProvAcq1aa(new AfDecimal(titTotProvAcq1aaObj, 15, 3));
            indTitCont.setTotProvAcq1aa(((short)0));
        }
        else {
            indTitCont.setTotProvAcq1aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvAcq2aa() {
        return titCont.getTitTotProvAcq2aa().getTitTotProvAcq2aa();
    }

    @Override
    public void setTitTotProvAcq2aa(AfDecimal titTotProvAcq2aa) {
        this.titCont.getTitTotProvAcq2aa().setTitTotProvAcq2aa(titTotProvAcq2aa.copy());
    }

    @Override
    public AfDecimal getTitTotProvAcq2aaObj() {
        if (indTitCont.getTotProvAcq2aa() >= 0) {
            return getTitTotProvAcq2aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvAcq2aaObj(AfDecimal titTotProvAcq2aaObj) {
        if (titTotProvAcq2aaObj != null) {
            setTitTotProvAcq2aa(new AfDecimal(titTotProvAcq2aaObj, 15, 3));
            indTitCont.setTotProvAcq2aa(((short)0));
        }
        else {
            indTitCont.setTotProvAcq2aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvDaRec() {
        return titCont.getTitTotProvDaRec().getTitTotProvDaRec();
    }

    @Override
    public void setTitTotProvDaRec(AfDecimal titTotProvDaRec) {
        this.titCont.getTitTotProvDaRec().setTitTotProvDaRec(titTotProvDaRec.copy());
    }

    @Override
    public AfDecimal getTitTotProvDaRecObj() {
        if (indTitCont.getTotProvDaRec() >= 0) {
            return getTitTotProvDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvDaRecObj(AfDecimal titTotProvDaRecObj) {
        if (titTotProvDaRecObj != null) {
            setTitTotProvDaRec(new AfDecimal(titTotProvDaRecObj, 15, 3));
            indTitCont.setTotProvDaRec(((short)0));
        }
        else {
            indTitCont.setTotProvDaRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvInc() {
        return titCont.getTitTotProvInc().getTitTotProvInc();
    }

    @Override
    public void setTitTotProvInc(AfDecimal titTotProvInc) {
        this.titCont.getTitTotProvInc().setTitTotProvInc(titTotProvInc.copy());
    }

    @Override
    public AfDecimal getTitTotProvIncObj() {
        if (indTitCont.getTotProvInc() >= 0) {
            return getTitTotProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvIncObj(AfDecimal titTotProvIncObj) {
        if (titTotProvIncObj != null) {
            setTitTotProvInc(new AfDecimal(titTotProvIncObj, 15, 3));
            indTitCont.setTotProvInc(((short)0));
        }
        else {
            indTitCont.setTotProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvRicor() {
        return titCont.getTitTotProvRicor().getTitTotProvRicor();
    }

    @Override
    public void setTitTotProvRicor(AfDecimal titTotProvRicor) {
        this.titCont.getTitTotProvRicor().setTitTotProvRicor(titTotProvRicor.copy());
    }

    @Override
    public AfDecimal getTitTotProvRicorObj() {
        if (indTitCont.getTotProvRicor() >= 0) {
            return getTitTotProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvRicorObj(AfDecimal titTotProvRicorObj) {
        if (titTotProvRicorObj != null) {
            setTitTotProvRicor(new AfDecimal(titTotProvRicorObj, 15, 3));
            indTitCont.setTotProvRicor(((short)0));
        }
        else {
            indTitCont.setTotProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotRemunAss() {
        return titCont.getTitTotRemunAss().getTitTotRemunAss();
    }

    @Override
    public void setTitTotRemunAss(AfDecimal titTotRemunAss) {
        this.titCont.getTitTotRemunAss().setTitTotRemunAss(titTotRemunAss.copy());
    }

    @Override
    public AfDecimal getTitTotRemunAssObj() {
        if (indTitCont.getTotRemunAss() >= 0) {
            return getTitTotRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotRemunAssObj(AfDecimal titTotRemunAssObj) {
        if (titTotRemunAssObj != null) {
            setTitTotRemunAss(new AfDecimal(titTotRemunAssObj, 15, 3));
            indTitCont.setTotRemunAss(((short)0));
        }
        else {
            indTitCont.setTotRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprAlt() {
        return titCont.getTitTotSoprAlt().getTitTotSoprAlt();
    }

    @Override
    public void setTitTotSoprAlt(AfDecimal titTotSoprAlt) {
        this.titCont.getTitTotSoprAlt().setTitTotSoprAlt(titTotSoprAlt.copy());
    }

    @Override
    public AfDecimal getTitTotSoprAltObj() {
        if (indTitCont.getTotSoprAlt() >= 0) {
            return getTitTotSoprAlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprAltObj(AfDecimal titTotSoprAltObj) {
        if (titTotSoprAltObj != null) {
            setTitTotSoprAlt(new AfDecimal(titTotSoprAltObj, 15, 3));
            indTitCont.setTotSoprAlt(((short)0));
        }
        else {
            indTitCont.setTotSoprAlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprProf() {
        return titCont.getTitTotSoprProf().getTitTotSoprProf();
    }

    @Override
    public void setTitTotSoprProf(AfDecimal titTotSoprProf) {
        this.titCont.getTitTotSoprProf().setTitTotSoprProf(titTotSoprProf.copy());
    }

    @Override
    public AfDecimal getTitTotSoprProfObj() {
        if (indTitCont.getTotSoprProf() >= 0) {
            return getTitTotSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprProfObj(AfDecimal titTotSoprProfObj) {
        if (titTotSoprProfObj != null) {
            setTitTotSoprProf(new AfDecimal(titTotSoprProfObj, 15, 3));
            indTitCont.setTotSoprProf(((short)0));
        }
        else {
            indTitCont.setTotSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprSan() {
        return titCont.getTitTotSoprSan().getTitTotSoprSan();
    }

    @Override
    public void setTitTotSoprSan(AfDecimal titTotSoprSan) {
        this.titCont.getTitTotSoprSan().setTitTotSoprSan(titTotSoprSan.copy());
    }

    @Override
    public AfDecimal getTitTotSoprSanObj() {
        if (indTitCont.getTotSoprSan() >= 0) {
            return getTitTotSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprSanObj(AfDecimal titTotSoprSanObj) {
        if (titTotSoprSanObj != null) {
            setTitTotSoprSan(new AfDecimal(titTotSoprSanObj, 15, 3));
            indTitCont.setTotSoprSan(((short)0));
        }
        else {
            indTitCont.setTotSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprSpo() {
        return titCont.getTitTotSoprSpo().getTitTotSoprSpo();
    }

    @Override
    public void setTitTotSoprSpo(AfDecimal titTotSoprSpo) {
        this.titCont.getTitTotSoprSpo().setTitTotSoprSpo(titTotSoprSpo.copy());
    }

    @Override
    public AfDecimal getTitTotSoprSpoObj() {
        if (indTitCont.getTotSoprSpo() >= 0) {
            return getTitTotSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprSpoObj(AfDecimal titTotSoprSpoObj) {
        if (titTotSoprSpoObj != null) {
            setTitTotSoprSpo(new AfDecimal(titTotSoprSpoObj, 15, 3));
            indTitCont.setTotSoprSpo(((short)0));
        }
        else {
            indTitCont.setTotSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprTec() {
        return titCont.getTitTotSoprTec().getTitTotSoprTec();
    }

    @Override
    public void setTitTotSoprTec(AfDecimal titTotSoprTec) {
        this.titCont.getTitTotSoprTec().setTitTotSoprTec(titTotSoprTec.copy());
    }

    @Override
    public AfDecimal getTitTotSoprTecObj() {
        if (indTitCont.getTotSoprTec() >= 0) {
            return getTitTotSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprTecObj(AfDecimal titTotSoprTecObj) {
        if (titTotSoprTecObj != null) {
            setTitTotSoprTec(new AfDecimal(titTotSoprTecObj, 15, 3));
            indTitCont.setTotSoprTec(((short)0));
        }
        else {
            indTitCont.setTotSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSpeAge() {
        return titCont.getTitTotSpeAge().getTitTotSpeAge();
    }

    @Override
    public void setTitTotSpeAge(AfDecimal titTotSpeAge) {
        this.titCont.getTitTotSpeAge().setTitTotSpeAge(titTotSpeAge.copy());
    }

    @Override
    public AfDecimal getTitTotSpeAgeObj() {
        if (indTitCont.getTotSpeAge() >= 0) {
            return getTitTotSpeAge();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSpeAgeObj(AfDecimal titTotSpeAgeObj) {
        if (titTotSpeAgeObj != null) {
            setTitTotSpeAge(new AfDecimal(titTotSpeAgeObj, 15, 3));
            indTitCont.setTotSpeAge(((short)0));
        }
        else {
            indTitCont.setTotSpeAge(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSpeMed() {
        return titCont.getTitTotSpeMed().getTitTotSpeMed();
    }

    @Override
    public void setTitTotSpeMed(AfDecimal titTotSpeMed) {
        this.titCont.getTitTotSpeMed().setTitTotSpeMed(titTotSpeMed.copy());
    }

    @Override
    public AfDecimal getTitTotSpeMedObj() {
        if (indTitCont.getTotSpeMed() >= 0) {
            return getTitTotSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSpeMedObj(AfDecimal titTotSpeMedObj) {
        if (titTotSpeMedObj != null) {
            setTitTotSpeMed(new AfDecimal(titTotSpeMedObj, 15, 3));
            indTitCont.setTotSpeMed(((short)0));
        }
        else {
            indTitCont.setTotSpeMed(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotTax() {
        return titCont.getTitTotTax().getTitTotTax();
    }

    @Override
    public void setTitTotTax(AfDecimal titTotTax) {
        this.titCont.getTitTotTax().setTitTotTax(titTotTax.copy());
    }

    @Override
    public AfDecimal getTitTotTaxObj() {
        if (indTitCont.getTotTax() >= 0) {
            return getTitTotTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotTaxObj(AfDecimal titTotTaxObj) {
        if (titTotTaxObj != null) {
            setTitTotTax(new AfDecimal(titTotTaxObj, 15, 3));
            indTitCont.setTotTax(((short)0));
        }
        else {
            indTitCont.setTotTax(((short)-1));
        }
    }

    @Override
    public String getTitTpCausDispStor() {
        return titCont.getTitTpCausDispStor();
    }

    @Override
    public void setTitTpCausDispStor(String titTpCausDispStor) {
        this.titCont.setTitTpCausDispStor(titTpCausDispStor);
    }

    @Override
    public String getTitTpCausDispStorObj() {
        if (indTitCont.getTpCausDispStor() >= 0) {
            return getTitTpCausDispStor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpCausDispStorObj(String titTpCausDispStorObj) {
        if (titTpCausDispStorObj != null) {
            setTitTpCausDispStor(titTpCausDispStorObj);
            indTitCont.setTpCausDispStor(((short)0));
        }
        else {
            indTitCont.setTpCausDispStor(((short)-1));
        }
    }

    @Override
    public String getTitTpCausRimb() {
        return titCont.getTitTpCausRimb();
    }

    @Override
    public void setTitTpCausRimb(String titTpCausRimb) {
        this.titCont.setTitTpCausRimb(titTpCausRimb);
    }

    @Override
    public String getTitTpCausRimbObj() {
        if (indTitCont.getTpCausRimb() >= 0) {
            return getTitTpCausRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpCausRimbObj(String titTpCausRimbObj) {
        if (titTpCausRimbObj != null) {
            setTitTpCausRimb(titTpCausRimbObj);
            indTitCont.setTpCausRimb(((short)0));
        }
        else {
            indTitCont.setTpCausRimb(((short)-1));
        }
    }

    @Override
    public int getTitTpCausStor() {
        return titCont.getTitTpCausStor().getTitTpCausStor();
    }

    @Override
    public void setTitTpCausStor(int titTpCausStor) {
        this.titCont.getTitTpCausStor().setTitTpCausStor(titTpCausStor);
    }

    @Override
    public Integer getTitTpCausStorObj() {
        if (indTitCont.getTpCausStor() >= 0) {
            return ((Integer)getTitTpCausStor());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpCausStorObj(Integer titTpCausStorObj) {
        if (titTpCausStorObj != null) {
            setTitTpCausStor(((int)titTpCausStorObj));
            indTitCont.setTpCausStor(((short)0));
        }
        else {
            indTitCont.setTpCausStor(((short)-1));
        }
    }

    @Override
    public String getTitTpEsiRid() {
        return titCont.getTitTpEsiRid();
    }

    @Override
    public void setTitTpEsiRid(String titTpEsiRid) {
        this.titCont.setTitTpEsiRid(titTpEsiRid);
    }

    @Override
    public String getTitTpEsiRidObj() {
        if (indTitCont.getTpEsiRid() >= 0) {
            return getTitTpEsiRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpEsiRidObj(String titTpEsiRidObj) {
        if (titTpEsiRidObj != null) {
            setTitTpEsiRid(titTpEsiRidObj);
            indTitCont.setTpEsiRid(((short)0));
        }
        else {
            indTitCont.setTpEsiRid(((short)-1));
        }
    }

    @Override
    public String getTitTpMezPagAdd() {
        return titCont.getTitTpMezPagAdd();
    }

    @Override
    public void setTitTpMezPagAdd(String titTpMezPagAdd) {
        this.titCont.setTitTpMezPagAdd(titTpMezPagAdd);
    }

    @Override
    public String getTitTpMezPagAddObj() {
        if (indTitCont.getTpMezPagAdd() >= 0) {
            return getTitTpMezPagAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpMezPagAddObj(String titTpMezPagAddObj) {
        if (titTpMezPagAddObj != null) {
            setTitTpMezPagAdd(titTpMezPagAddObj);
            indTitCont.setTpMezPagAdd(((short)0));
        }
        else {
            indTitCont.setTpMezPagAdd(((short)-1));
        }
    }

    @Override
    public String getTitTpOgg() {
        return titCont.getTitTpOgg();
    }

    @Override
    public void setTitTpOgg(String titTpOgg) {
        this.titCont.setTitTpOgg(titTpOgg);
    }

    @Override
    public String getTitTpPreTit() {
        return titCont.getTitTpPreTit();
    }

    @Override
    public void setTitTpPreTit(String titTpPreTit) {
        this.titCont.setTitTpPreTit(titTpPreTit);
    }

    @Override
    public String getTitTpStatTit() {
        return titCont.getTitTpStatTit();
    }

    @Override
    public void setTitTpStatTit(String titTpStatTit) {
        this.titCont.setTitTpStatTit(titTpStatTit);
    }

    @Override
    public String getTitTpTit() {
        return titCont.getTitTpTit();
    }

    @Override
    public void setTitTpTit(String titTpTit) {
        this.titCont.setTitTpTit(titTpTit);
    }

    @Override
    public String getTitTpTitMigraz() {
        return titCont.getTitTpTitMigraz();
    }

    @Override
    public void setTitTpTitMigraz(String titTpTitMigraz) {
        this.titCont.setTitTpTitMigraz(titTpTitMigraz);
    }

    @Override
    public String getTitTpTitMigrazObj() {
        if (indTitCont.getTpTitMigraz() >= 0) {
            return getTitTpTitMigraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpTitMigrazObj(String titTpTitMigrazObj) {
        if (titTpTitMigrazObj != null) {
            setTitTpTitMigraz(titTpTitMigrazObj);
            indTitCont.setTpTitMigraz(((short)0));
        }
        else {
            indTitCont.setTpTitMigraz(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
