package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-PERC-SOVRAP<br>
 * Variable: WPAG-PERC-SOVRAP from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPercSovrap extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPercSovrap() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PERC_SOVRAP;
    }

    public void setWpagPercSovrapFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PERC_SOVRAP, Pos.WPAG_PERC_SOVRAP);
    }

    public byte[] getWpagPercSovrapAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PERC_SOVRAP, Pos.WPAG_PERC_SOVRAP);
        return buffer;
    }

    public void initWpagPercSovrapSpaces() {
        fill(Pos.WPAG_PERC_SOVRAP, Len.WPAG_PERC_SOVRAP, Types.SPACE_CHAR);
    }

    public void setWpagPercSovrapNull(String wpagPercSovrapNull) {
        writeString(Pos.WPAG_PERC_SOVRAP_NULL, wpagPercSovrapNull, Len.WPAG_PERC_SOVRAP_NULL);
    }

    /**Original name: WPAG-PERC-SOVRAP-NULL<br>*/
    public String getWpagPercSovrapNull() {
        return readString(Pos.WPAG_PERC_SOVRAP_NULL, Len.WPAG_PERC_SOVRAP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PERC_SOVRAP = 1;
        public static final int WPAG_PERC_SOVRAP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PERC_SOVRAP = 4;
        public static final int WPAG_PERC_SOVRAP_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
