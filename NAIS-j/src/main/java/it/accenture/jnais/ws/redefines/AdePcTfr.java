package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-PC-TFR<br>
 * Variable: ADE-PC-TFR from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdePcTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdePcTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_PC_TFR;
    }

    public void setAdePcTfr(AfDecimal adePcTfr) {
        writeDecimalAsPacked(Pos.ADE_PC_TFR, adePcTfr.copy());
    }

    public void setAdePcTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_PC_TFR, Pos.ADE_PC_TFR);
    }

    /**Original name: ADE-PC-TFR<br>*/
    public AfDecimal getAdePcTfr() {
        return readPackedAsDecimal(Pos.ADE_PC_TFR, Len.Int.ADE_PC_TFR, Len.Fract.ADE_PC_TFR);
    }

    public byte[] getAdePcTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_PC_TFR, Pos.ADE_PC_TFR);
        return buffer;
    }

    public void setAdePcTfrNull(String adePcTfrNull) {
        writeString(Pos.ADE_PC_TFR_NULL, adePcTfrNull, Len.ADE_PC_TFR_NULL);
    }

    /**Original name: ADE-PC-TFR-NULL<br>*/
    public String getAdePcTfrNull() {
        return readString(Pos.ADE_PC_TFR_NULL, Len.ADE_PC_TFR_NULL);
    }

    public String getAdePcTfrNullFormatted() {
        return Functions.padBlanks(getAdePcTfrNull(), Len.ADE_PC_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_PC_TFR = 1;
        public static final int ADE_PC_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_PC_TFR = 4;
        public static final int ADE_PC_TFR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
