package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-PREMIO-RISC<br>
 * Variable: WPAG-RATA-PREMIO-RISC from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataPremioRisc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataPremioRisc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_PREMIO_RISC;
    }

    public void setWpagRataPremioRisc(AfDecimal wpagRataPremioRisc) {
        writeDecimalAsPacked(Pos.WPAG_RATA_PREMIO_RISC, wpagRataPremioRisc.copy());
    }

    public void setWpagRataPremioRiscFormatted(String wpagRataPremioRisc) {
        setWpagRataPremioRisc(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_PREMIO_RISC + Len.Fract.WPAG_RATA_PREMIO_RISC, Len.Fract.WPAG_RATA_PREMIO_RISC, wpagRataPremioRisc));
    }

    public void setWpagRataPremioRiscFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_PREMIO_RISC, Pos.WPAG_RATA_PREMIO_RISC);
    }

    /**Original name: WPAG-RATA-PREMIO-RISC<br>*/
    public AfDecimal getWpagRataPremioRisc() {
        return readPackedAsDecimal(Pos.WPAG_RATA_PREMIO_RISC, Len.Int.WPAG_RATA_PREMIO_RISC, Len.Fract.WPAG_RATA_PREMIO_RISC);
    }

    public byte[] getWpagRataPremioRiscAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_PREMIO_RISC, Pos.WPAG_RATA_PREMIO_RISC);
        return buffer;
    }

    public void initWpagRataPremioRiscSpaces() {
        fill(Pos.WPAG_RATA_PREMIO_RISC, Len.WPAG_RATA_PREMIO_RISC, Types.SPACE_CHAR);
    }

    public void setWpagRataPremioRiscNull(String wpagRataPremioRiscNull) {
        writeString(Pos.WPAG_RATA_PREMIO_RISC_NULL, wpagRataPremioRiscNull, Len.WPAG_RATA_PREMIO_RISC_NULL);
    }

    /**Original name: WPAG-RATA-PREMIO-RISC-NULL<br>*/
    public String getWpagRataPremioRiscNull() {
        return readString(Pos.WPAG_RATA_PREMIO_RISC_NULL, Len.WPAG_RATA_PREMIO_RISC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_PREMIO_RISC = 1;
        public static final int WPAG_RATA_PREMIO_RISC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_PREMIO_RISC = 8;
        public static final int WPAG_RATA_PREMIO_RISC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_PREMIO_RISC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_PREMIO_RISC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
