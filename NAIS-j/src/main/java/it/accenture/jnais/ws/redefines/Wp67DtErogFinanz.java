package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-DT-EROG-FINANZ<br>
 * Variable: WP67-DT-EROG-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67DtErogFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67DtErogFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_DT_EROG_FINANZ;
    }

    public void setWp67DtErogFinanz(int wp67DtErogFinanz) {
        writeIntAsPacked(Pos.WP67_DT_EROG_FINANZ, wp67DtErogFinanz, Len.Int.WP67_DT_EROG_FINANZ);
    }

    public void setWp67DtErogFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_DT_EROG_FINANZ, Pos.WP67_DT_EROG_FINANZ);
    }

    /**Original name: WP67-DT-EROG-FINANZ<br>*/
    public int getWp67DtErogFinanz() {
        return readPackedAsInt(Pos.WP67_DT_EROG_FINANZ, Len.Int.WP67_DT_EROG_FINANZ);
    }

    public byte[] getWp67DtErogFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_DT_EROG_FINANZ, Pos.WP67_DT_EROG_FINANZ);
        return buffer;
    }

    public void setWp67DtErogFinanzNull(String wp67DtErogFinanzNull) {
        writeString(Pos.WP67_DT_EROG_FINANZ_NULL, wp67DtErogFinanzNull, Len.WP67_DT_EROG_FINANZ_NULL);
    }

    /**Original name: WP67-DT-EROG-FINANZ-NULL<br>*/
    public String getWp67DtErogFinanzNull() {
        return readString(Pos.WP67_DT_EROG_FINANZ_NULL, Len.WP67_DT_EROG_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_DT_EROG_FINANZ = 1;
        public static final int WP67_DT_EROG_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_DT_EROG_FINANZ = 5;
        public static final int WP67_DT_EROG_FINANZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_DT_EROG_FINANZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
