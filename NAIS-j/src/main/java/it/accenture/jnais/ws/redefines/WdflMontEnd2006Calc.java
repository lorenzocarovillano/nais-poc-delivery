package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-MONT-END2006-CALC<br>
 * Variable: WDFL-MONT-END2006-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflMontEnd2006Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflMontEnd2006Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_MONT_END2006_CALC;
    }

    public void setWdflMontEnd2006Calc(AfDecimal wdflMontEnd2006Calc) {
        writeDecimalAsPacked(Pos.WDFL_MONT_END2006_CALC, wdflMontEnd2006Calc.copy());
    }

    public void setWdflMontEnd2006CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_MONT_END2006_CALC, Pos.WDFL_MONT_END2006_CALC);
    }

    /**Original name: WDFL-MONT-END2006-CALC<br>*/
    public AfDecimal getWdflMontEnd2006Calc() {
        return readPackedAsDecimal(Pos.WDFL_MONT_END2006_CALC, Len.Int.WDFL_MONT_END2006_CALC, Len.Fract.WDFL_MONT_END2006_CALC);
    }

    public byte[] getWdflMontEnd2006CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_MONT_END2006_CALC, Pos.WDFL_MONT_END2006_CALC);
        return buffer;
    }

    public void setWdflMontEnd2006CalcNull(String wdflMontEnd2006CalcNull) {
        writeString(Pos.WDFL_MONT_END2006_CALC_NULL, wdflMontEnd2006CalcNull, Len.WDFL_MONT_END2006_CALC_NULL);
    }

    /**Original name: WDFL-MONT-END2006-CALC-NULL<br>*/
    public String getWdflMontEnd2006CalcNull() {
        return readString(Pos.WDFL_MONT_END2006_CALC_NULL, Len.WDFL_MONT_END2006_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_END2006_CALC = 1;
        public static final int WDFL_MONT_END2006_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_END2006_CALC = 8;
        public static final int WDFL_MONT_END2006_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_END2006_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_END2006_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
