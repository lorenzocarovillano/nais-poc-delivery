package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-ETA-AA-3O-ASSTO<br>
 * Variable: WGRZ-ETA-AA-3O-ASSTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzEtaAa3oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzEtaAa3oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_ETA_AA3O_ASSTO;
    }

    public void setWgrzEtaAa3oAssto(short wgrzEtaAa3oAssto) {
        writeShortAsPacked(Pos.WGRZ_ETA_AA3O_ASSTO, wgrzEtaAa3oAssto, Len.Int.WGRZ_ETA_AA3O_ASSTO);
    }

    public void setWgrzEtaAa3oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_ETA_AA3O_ASSTO, Pos.WGRZ_ETA_AA3O_ASSTO);
    }

    /**Original name: WGRZ-ETA-AA-3O-ASSTO<br>*/
    public short getWgrzEtaAa3oAssto() {
        return readPackedAsShort(Pos.WGRZ_ETA_AA3O_ASSTO, Len.Int.WGRZ_ETA_AA3O_ASSTO);
    }

    public byte[] getWgrzEtaAa3oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_ETA_AA3O_ASSTO, Pos.WGRZ_ETA_AA3O_ASSTO);
        return buffer;
    }

    public void initWgrzEtaAa3oAsstoSpaces() {
        fill(Pos.WGRZ_ETA_AA3O_ASSTO, Len.WGRZ_ETA_AA3O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWgrzEtaAa3oAsstoNull(String wgrzEtaAa3oAsstoNull) {
        writeString(Pos.WGRZ_ETA_AA3O_ASSTO_NULL, wgrzEtaAa3oAsstoNull, Len.WGRZ_ETA_AA3O_ASSTO_NULL);
    }

    /**Original name: WGRZ-ETA-AA-3O-ASSTO-NULL<br>*/
    public String getWgrzEtaAa3oAsstoNull() {
        return readString(Pos.WGRZ_ETA_AA3O_ASSTO_NULL, Len.WGRZ_ETA_AA3O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_ETA_AA3O_ASSTO = 1;
        public static final int WGRZ_ETA_AA3O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_ETA_AA3O_ASSTO = 2;
        public static final int WGRZ_ETA_AA3O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_ETA_AA3O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
