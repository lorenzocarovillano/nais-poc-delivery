package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-TAX-SEP-CALC<br>
 * Variable: DFL-IMPB-TAX-SEP-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbTaxSepCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbTaxSepCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_TAX_SEP_CALC;
    }

    public void setDflImpbTaxSepCalc(AfDecimal dflImpbTaxSepCalc) {
        writeDecimalAsPacked(Pos.DFL_IMPB_TAX_SEP_CALC, dflImpbTaxSepCalc.copy());
    }

    public void setDflImpbTaxSepCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_TAX_SEP_CALC, Pos.DFL_IMPB_TAX_SEP_CALC);
    }

    /**Original name: DFL-IMPB-TAX-SEP-CALC<br>*/
    public AfDecimal getDflImpbTaxSepCalc() {
        return readPackedAsDecimal(Pos.DFL_IMPB_TAX_SEP_CALC, Len.Int.DFL_IMPB_TAX_SEP_CALC, Len.Fract.DFL_IMPB_TAX_SEP_CALC);
    }

    public byte[] getDflImpbTaxSepCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_TAX_SEP_CALC, Pos.DFL_IMPB_TAX_SEP_CALC);
        return buffer;
    }

    public void setDflImpbTaxSepCalcNull(String dflImpbTaxSepCalcNull) {
        writeString(Pos.DFL_IMPB_TAX_SEP_CALC_NULL, dflImpbTaxSepCalcNull, Len.DFL_IMPB_TAX_SEP_CALC_NULL);
    }

    /**Original name: DFL-IMPB-TAX-SEP-CALC-NULL<br>*/
    public String getDflImpbTaxSepCalcNull() {
        return readString(Pos.DFL_IMPB_TAX_SEP_CALC_NULL, Len.DFL_IMPB_TAX_SEP_CALC_NULL);
    }

    public String getDflImpbTaxSepCalcNullFormatted() {
        return Functions.padBlanks(getDflImpbTaxSepCalcNull(), Len.DFL_IMPB_TAX_SEP_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TAX_SEP_CALC = 1;
        public static final int DFL_IMPB_TAX_SEP_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TAX_SEP_CALC = 8;
        public static final int DFL_IMPB_TAX_SEP_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TAX_SEP_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TAX_SEP_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
