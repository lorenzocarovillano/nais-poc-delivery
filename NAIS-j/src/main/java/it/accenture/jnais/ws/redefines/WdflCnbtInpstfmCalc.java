package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-CNBT-INPSTFM-CALC<br>
 * Variable: WDFL-CNBT-INPSTFM-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflCnbtInpstfmCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflCnbtInpstfmCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_CNBT_INPSTFM_CALC;
    }

    public void setWdflCnbtInpstfmCalc(AfDecimal wdflCnbtInpstfmCalc) {
        writeDecimalAsPacked(Pos.WDFL_CNBT_INPSTFM_CALC, wdflCnbtInpstfmCalc.copy());
    }

    public void setWdflCnbtInpstfmCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_CNBT_INPSTFM_CALC, Pos.WDFL_CNBT_INPSTFM_CALC);
    }

    /**Original name: WDFL-CNBT-INPSTFM-CALC<br>*/
    public AfDecimal getWdflCnbtInpstfmCalc() {
        return readPackedAsDecimal(Pos.WDFL_CNBT_INPSTFM_CALC, Len.Int.WDFL_CNBT_INPSTFM_CALC, Len.Fract.WDFL_CNBT_INPSTFM_CALC);
    }

    public byte[] getWdflCnbtInpstfmCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_CNBT_INPSTFM_CALC, Pos.WDFL_CNBT_INPSTFM_CALC);
        return buffer;
    }

    public void setWdflCnbtInpstfmCalcNull(String wdflCnbtInpstfmCalcNull) {
        writeString(Pos.WDFL_CNBT_INPSTFM_CALC_NULL, wdflCnbtInpstfmCalcNull, Len.WDFL_CNBT_INPSTFM_CALC_NULL);
    }

    /**Original name: WDFL-CNBT-INPSTFM-CALC-NULL<br>*/
    public String getWdflCnbtInpstfmCalcNull() {
        return readString(Pos.WDFL_CNBT_INPSTFM_CALC_NULL, Len.WDFL_CNBT_INPSTFM_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_CNBT_INPSTFM_CALC = 1;
        public static final int WDFL_CNBT_INPSTFM_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_CNBT_INPSTFM_CALC = 8;
        public static final int WDFL_CNBT_INPSTFM_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_CNBT_INPSTFM_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_CNBT_INPSTFM_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
