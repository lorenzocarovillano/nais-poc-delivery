package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP85-RENDTO-LRD<br>
 * Variable: WP85-RENDTO-LRD from program LRGS0660<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp85RendtoLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp85RendtoLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP85_RENDTO_LRD;
    }

    public void setWp85RendtoLrd(AfDecimal wp85RendtoLrd) {
        writeDecimalAsPacked(Pos.WP85_RENDTO_LRD, wp85RendtoLrd.copy());
    }

    /**Original name: WP85-RENDTO-LRD<br>*/
    public AfDecimal getWp85RendtoLrd() {
        return readPackedAsDecimal(Pos.WP85_RENDTO_LRD, Len.Int.WP85_RENDTO_LRD, Len.Fract.WP85_RENDTO_LRD);
    }

    public void setWp85RendtoLrdNull(String wp85RendtoLrdNull) {
        writeString(Pos.WP85_RENDTO_LRD_NULL, wp85RendtoLrdNull, Len.WP85_RENDTO_LRD_NULL);
    }

    /**Original name: WP85-RENDTO-LRD-NULL<br>*/
    public String getWp85RendtoLrdNull() {
        return readString(Pos.WP85_RENDTO_LRD_NULL, Len.WP85_RENDTO_LRD_NULL);
    }

    public String getWp85RendtoLrdNullFormatted() {
        return Functions.padBlanks(getWp85RendtoLrdNull(), Len.WP85_RENDTO_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP85_RENDTO_LRD = 1;
        public static final int WP85_RENDTO_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP85_RENDTO_LRD = 8;
        public static final int WP85_RENDTO_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP85_RENDTO_LRD = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP85_RENDTO_LRD = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
