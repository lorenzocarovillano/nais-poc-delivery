package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-IS-CL<br>
 * Variable: WPCO-DT-ULTC-IS-CL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcIsCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcIsCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_IS_CL;
    }

    public void setWpcoDtUltcIsCl(int wpcoDtUltcIsCl) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_IS_CL, wpcoDtUltcIsCl, Len.Int.WPCO_DT_ULTC_IS_CL);
    }

    public void setDpcoDtUltcIsClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_IS_CL, Pos.WPCO_DT_ULTC_IS_CL);
    }

    /**Original name: WPCO-DT-ULTC-IS-CL<br>*/
    public int getWpcoDtUltcIsCl() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_IS_CL, Len.Int.WPCO_DT_ULTC_IS_CL);
    }

    public byte[] getWpcoDtUltcIsClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_IS_CL, Pos.WPCO_DT_ULTC_IS_CL);
        return buffer;
    }

    public void setWpcoDtUltcIsClNull(String wpcoDtUltcIsClNull) {
        writeString(Pos.WPCO_DT_ULTC_IS_CL_NULL, wpcoDtUltcIsClNull, Len.WPCO_DT_ULTC_IS_CL_NULL);
    }

    /**Original name: WPCO-DT-ULTC-IS-CL-NULL<br>*/
    public String getWpcoDtUltcIsClNull() {
        return readString(Pos.WPCO_DT_ULTC_IS_CL_NULL, Len.WPCO_DT_ULTC_IS_CL_NULL);
    }

    public String getWpcoDtUltcIsClNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcIsClNull(), Len.WPCO_DT_ULTC_IS_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_IS_CL = 1;
        public static final int WPCO_DT_ULTC_IS_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_IS_CL = 5;
        public static final int WPCO_DT_ULTC_IS_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_IS_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
