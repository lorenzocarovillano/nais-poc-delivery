package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-PC-1O-RAT<br>
 * Variable: GRZ-PC-1O-RAT from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzPc1oRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzPc1oRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_PC1O_RAT;
    }

    public void setGrzPc1oRat(AfDecimal grzPc1oRat) {
        writeDecimalAsPacked(Pos.GRZ_PC1O_RAT, grzPc1oRat.copy());
    }

    public void setGrzPc1oRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_PC1O_RAT, Pos.GRZ_PC1O_RAT);
    }

    /**Original name: GRZ-PC-1O-RAT<br>*/
    public AfDecimal getGrzPc1oRat() {
        return readPackedAsDecimal(Pos.GRZ_PC1O_RAT, Len.Int.GRZ_PC1O_RAT, Len.Fract.GRZ_PC1O_RAT);
    }

    public byte[] getGrzPc1oRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_PC1O_RAT, Pos.GRZ_PC1O_RAT);
        return buffer;
    }

    public void setGrzPc1oRatNull(String grzPc1oRatNull) {
        writeString(Pos.GRZ_PC1O_RAT_NULL, grzPc1oRatNull, Len.GRZ_PC1O_RAT_NULL);
    }

    /**Original name: GRZ-PC-1O-RAT-NULL<br>*/
    public String getGrzPc1oRatNull() {
        return readString(Pos.GRZ_PC1O_RAT_NULL, Len.GRZ_PC1O_RAT_NULL);
    }

    public String getGrzPc1oRatNullFormatted() {
        return Functions.padBlanks(getGrzPc1oRatNull(), Len.GRZ_PC1O_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_PC1O_RAT = 1;
        public static final int GRZ_PC1O_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_PC1O_RAT = 4;
        public static final int GRZ_PC1O_RAT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_PC1O_RAT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int GRZ_PC1O_RAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
