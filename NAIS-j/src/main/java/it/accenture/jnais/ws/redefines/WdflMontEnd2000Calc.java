package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-MONT-END2000-CALC<br>
 * Variable: WDFL-MONT-END2000-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflMontEnd2000Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflMontEnd2000Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_MONT_END2000_CALC;
    }

    public void setWdflMontEnd2000Calc(AfDecimal wdflMontEnd2000Calc) {
        writeDecimalAsPacked(Pos.WDFL_MONT_END2000_CALC, wdflMontEnd2000Calc.copy());
    }

    public void setWdflMontEnd2000CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_MONT_END2000_CALC, Pos.WDFL_MONT_END2000_CALC);
    }

    /**Original name: WDFL-MONT-END2000-CALC<br>*/
    public AfDecimal getWdflMontEnd2000Calc() {
        return readPackedAsDecimal(Pos.WDFL_MONT_END2000_CALC, Len.Int.WDFL_MONT_END2000_CALC, Len.Fract.WDFL_MONT_END2000_CALC);
    }

    public byte[] getWdflMontEnd2000CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_MONT_END2000_CALC, Pos.WDFL_MONT_END2000_CALC);
        return buffer;
    }

    public void setWdflMontEnd2000CalcNull(String wdflMontEnd2000CalcNull) {
        writeString(Pos.WDFL_MONT_END2000_CALC_NULL, wdflMontEnd2000CalcNull, Len.WDFL_MONT_END2000_CALC_NULL);
    }

    /**Original name: WDFL-MONT-END2000-CALC-NULL<br>*/
    public String getWdflMontEnd2000CalcNull() {
        return readString(Pos.WDFL_MONT_END2000_CALC_NULL, Len.WDFL_MONT_END2000_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_END2000_CALC = 1;
        public static final int WDFL_MONT_END2000_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_END2000_CALC = 8;
        public static final int WDFL_MONT_END2000_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_END2000_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_END2000_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
