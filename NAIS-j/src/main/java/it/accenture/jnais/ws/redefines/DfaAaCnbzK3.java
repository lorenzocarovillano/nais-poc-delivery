package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-AA-CNBZ-K3<br>
 * Variable: DFA-AA-CNBZ-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaAaCnbzK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaAaCnbzK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_AA_CNBZ_K3;
    }

    public void setDfaAaCnbzK3(short dfaAaCnbzK3) {
        writeShortAsPacked(Pos.DFA_AA_CNBZ_K3, dfaAaCnbzK3, Len.Int.DFA_AA_CNBZ_K3);
    }

    public void setDfaAaCnbzK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_AA_CNBZ_K3, Pos.DFA_AA_CNBZ_K3);
    }

    /**Original name: DFA-AA-CNBZ-K3<br>*/
    public short getDfaAaCnbzK3() {
        return readPackedAsShort(Pos.DFA_AA_CNBZ_K3, Len.Int.DFA_AA_CNBZ_K3);
    }

    public byte[] getDfaAaCnbzK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_AA_CNBZ_K3, Pos.DFA_AA_CNBZ_K3);
        return buffer;
    }

    public void setDfaAaCnbzK3Null(String dfaAaCnbzK3Null) {
        writeString(Pos.DFA_AA_CNBZ_K3_NULL, dfaAaCnbzK3Null, Len.DFA_AA_CNBZ_K3_NULL);
    }

    /**Original name: DFA-AA-CNBZ-K3-NULL<br>*/
    public String getDfaAaCnbzK3Null() {
        return readString(Pos.DFA_AA_CNBZ_K3_NULL, Len.DFA_AA_CNBZ_K3_NULL);
    }

    public String getDfaAaCnbzK3NullFormatted() {
        return Functions.padBlanks(getDfaAaCnbzK3Null(), Len.DFA_AA_CNBZ_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_AA_CNBZ_K3 = 1;
        public static final int DFA_AA_CNBZ_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_AA_CNBZ_K3 = 3;
        public static final int DFA_AA_CNBZ_K3_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_AA_CNBZ_K3 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
