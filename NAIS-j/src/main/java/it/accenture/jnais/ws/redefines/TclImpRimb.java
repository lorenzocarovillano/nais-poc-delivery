package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-RIMB<br>
 * Variable: TCL-IMP-RIMB from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_RIMB;
    }

    public void setTclImpRimb(AfDecimal tclImpRimb) {
        writeDecimalAsPacked(Pos.TCL_IMP_RIMB, tclImpRimb.copy());
    }

    public void setTclImpRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_RIMB, Pos.TCL_IMP_RIMB);
    }

    /**Original name: TCL-IMP-RIMB<br>*/
    public AfDecimal getTclImpRimb() {
        return readPackedAsDecimal(Pos.TCL_IMP_RIMB, Len.Int.TCL_IMP_RIMB, Len.Fract.TCL_IMP_RIMB);
    }

    public byte[] getTclImpRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_RIMB, Pos.TCL_IMP_RIMB);
        return buffer;
    }

    public void setTclImpRimbNull(String tclImpRimbNull) {
        writeString(Pos.TCL_IMP_RIMB_NULL, tclImpRimbNull, Len.TCL_IMP_RIMB_NULL);
    }

    /**Original name: TCL-IMP-RIMB-NULL<br>*/
    public String getTclImpRimbNull() {
        return readString(Pos.TCL_IMP_RIMB_NULL, Len.TCL_IMP_RIMB_NULL);
    }

    public String getTclImpRimbNullFormatted() {
        return Functions.padBlanks(getTclImpRimbNull(), Len.TCL_IMP_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_RIMB = 1;
        public static final int TCL_IMP_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_RIMB = 8;
        public static final int TCL_IMP_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
