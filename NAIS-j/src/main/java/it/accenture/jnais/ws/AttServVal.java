package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.P88IdMoviChiu;

/**Original name: ATT-SERV-VAL<br>
 * Variable: ATT-SERV-VAL from copybook IDBVP881<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AttServVal extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: P88-ID-ATT-SERV-VAL
    private int p88IdAttServVal = DefaultValues.INT_VAL;
    //Original name: P88-COD-COMP-ANIA
    private int p88CodCompAnia = DefaultValues.INT_VAL;
    //Original name: P88-ID-MOVI-CRZ
    private int p88IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: P88-ID-MOVI-CHIU
    private P88IdMoviChiu p88IdMoviChiu = new P88IdMoviChiu();
    //Original name: P88-DT-INI-EFF
    private int p88DtIniEff = DefaultValues.INT_VAL;
    //Original name: P88-DT-END-EFF
    private int p88DtEndEff = DefaultValues.INT_VAL;
    //Original name: P88-TP-SERV-VAL
    private String p88TpServVal = DefaultValues.stringVal(Len.P88_TP_SERV_VAL);
    //Original name: P88-ID-OGG
    private int p88IdOgg = DefaultValues.INT_VAL;
    //Original name: P88-TP-OGG
    private String p88TpOgg = DefaultValues.stringVal(Len.P88_TP_OGG);
    //Original name: P88-DS-RIGA
    private long p88DsRiga = DefaultValues.LONG_VAL;
    //Original name: P88-DS-OPER-SQL
    private char p88DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P88-DS-VER
    private int p88DsVer = DefaultValues.INT_VAL;
    //Original name: P88-DS-TS-INI-CPTZ
    private long p88DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: P88-DS-TS-END-CPTZ
    private long p88DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: P88-DS-UTENTE
    private String p88DsUtente = DefaultValues.stringVal(Len.P88_DS_UTENTE);
    //Original name: P88-DS-STATO-ELAB
    private char p88DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: P88-FL-ALL-FND
    private char p88FlAllFnd = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ATT_SERV_VAL;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAttServValBytes(buf);
    }

    public void setAttServValFormatted(String data) {
        byte[] buffer = new byte[Len.ATT_SERV_VAL];
        MarshalByte.writeString(buffer, 1, data, Len.ATT_SERV_VAL);
        setAttServValBytes(buffer, 1);
    }

    public String getAttServValFormatted() {
        return MarshalByteExt.bufferToStr(getAttServValBytes());
    }

    public void setAttServValBytes(byte[] buffer) {
        setAttServValBytes(buffer, 1);
    }

    public byte[] getAttServValBytes() {
        byte[] buffer = new byte[Len.ATT_SERV_VAL];
        return getAttServValBytes(buffer, 1);
    }

    public void setAttServValBytes(byte[] buffer, int offset) {
        int position = offset;
        p88IdAttServVal = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P88_ID_ATT_SERV_VAL, 0);
        position += Len.P88_ID_ATT_SERV_VAL;
        p88CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P88_COD_COMP_ANIA, 0);
        position += Len.P88_COD_COMP_ANIA;
        p88IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P88_ID_MOVI_CRZ, 0);
        position += Len.P88_ID_MOVI_CRZ;
        p88IdMoviChiu.setP88IdMoviChiuFromBuffer(buffer, position);
        position += P88IdMoviChiu.Len.P88_ID_MOVI_CHIU;
        p88DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P88_DT_INI_EFF, 0);
        position += Len.P88_DT_INI_EFF;
        p88DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P88_DT_END_EFF, 0);
        position += Len.P88_DT_END_EFF;
        p88TpServVal = MarshalByte.readString(buffer, position, Len.P88_TP_SERV_VAL);
        position += Len.P88_TP_SERV_VAL;
        p88IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P88_ID_OGG, 0);
        position += Len.P88_ID_OGG;
        p88TpOgg = MarshalByte.readString(buffer, position, Len.P88_TP_OGG);
        position += Len.P88_TP_OGG;
        p88DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P88_DS_RIGA, 0);
        position += Len.P88_DS_RIGA;
        p88DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p88DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P88_DS_VER, 0);
        position += Len.P88_DS_VER;
        p88DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P88_DS_TS_INI_CPTZ, 0);
        position += Len.P88_DS_TS_INI_CPTZ;
        p88DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P88_DS_TS_END_CPTZ, 0);
        position += Len.P88_DS_TS_END_CPTZ;
        p88DsUtente = MarshalByte.readString(buffer, position, Len.P88_DS_UTENTE);
        position += Len.P88_DS_UTENTE;
        p88DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p88FlAllFnd = MarshalByte.readChar(buffer, position);
    }

    public byte[] getAttServValBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, p88IdAttServVal, Len.Int.P88_ID_ATT_SERV_VAL, 0);
        position += Len.P88_ID_ATT_SERV_VAL;
        MarshalByte.writeIntAsPacked(buffer, position, p88CodCompAnia, Len.Int.P88_COD_COMP_ANIA, 0);
        position += Len.P88_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, p88IdMoviCrz, Len.Int.P88_ID_MOVI_CRZ, 0);
        position += Len.P88_ID_MOVI_CRZ;
        p88IdMoviChiu.getP88IdMoviChiuAsBuffer(buffer, position);
        position += P88IdMoviChiu.Len.P88_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, p88DtIniEff, Len.Int.P88_DT_INI_EFF, 0);
        position += Len.P88_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, p88DtEndEff, Len.Int.P88_DT_END_EFF, 0);
        position += Len.P88_DT_END_EFF;
        MarshalByte.writeString(buffer, position, p88TpServVal, Len.P88_TP_SERV_VAL);
        position += Len.P88_TP_SERV_VAL;
        MarshalByte.writeIntAsPacked(buffer, position, p88IdOgg, Len.Int.P88_ID_OGG, 0);
        position += Len.P88_ID_OGG;
        MarshalByte.writeString(buffer, position, p88TpOgg, Len.P88_TP_OGG);
        position += Len.P88_TP_OGG;
        MarshalByte.writeLongAsPacked(buffer, position, p88DsRiga, Len.Int.P88_DS_RIGA, 0);
        position += Len.P88_DS_RIGA;
        MarshalByte.writeChar(buffer, position, p88DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, p88DsVer, Len.Int.P88_DS_VER, 0);
        position += Len.P88_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, p88DsTsIniCptz, Len.Int.P88_DS_TS_INI_CPTZ, 0);
        position += Len.P88_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, p88DsTsEndCptz, Len.Int.P88_DS_TS_END_CPTZ, 0);
        position += Len.P88_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, p88DsUtente, Len.P88_DS_UTENTE);
        position += Len.P88_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, p88DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p88FlAllFnd);
        return buffer;
    }

    public void setP88IdAttServVal(int p88IdAttServVal) {
        this.p88IdAttServVal = p88IdAttServVal;
    }

    public int getP88IdAttServVal() {
        return this.p88IdAttServVal;
    }

    public void setP88CodCompAnia(int p88CodCompAnia) {
        this.p88CodCompAnia = p88CodCompAnia;
    }

    public int getP88CodCompAnia() {
        return this.p88CodCompAnia;
    }

    public void setP88IdMoviCrz(int p88IdMoviCrz) {
        this.p88IdMoviCrz = p88IdMoviCrz;
    }

    public int getP88IdMoviCrz() {
        return this.p88IdMoviCrz;
    }

    public void setP88DtIniEff(int p88DtIniEff) {
        this.p88DtIniEff = p88DtIniEff;
    }

    public int getP88DtIniEff() {
        return this.p88DtIniEff;
    }

    public void setP88DtEndEff(int p88DtEndEff) {
        this.p88DtEndEff = p88DtEndEff;
    }

    public int getP88DtEndEff() {
        return this.p88DtEndEff;
    }

    public void setP88TpServVal(String p88TpServVal) {
        this.p88TpServVal = Functions.subString(p88TpServVal, Len.P88_TP_SERV_VAL);
    }

    public String getP88TpServVal() {
        return this.p88TpServVal;
    }

    public void setP88IdOgg(int p88IdOgg) {
        this.p88IdOgg = p88IdOgg;
    }

    public int getP88IdOgg() {
        return this.p88IdOgg;
    }

    public void setP88TpOgg(String p88TpOgg) {
        this.p88TpOgg = Functions.subString(p88TpOgg, Len.P88_TP_OGG);
    }

    public String getP88TpOgg() {
        return this.p88TpOgg;
    }

    public void setP88DsRiga(long p88DsRiga) {
        this.p88DsRiga = p88DsRiga;
    }

    public long getP88DsRiga() {
        return this.p88DsRiga;
    }

    public void setP88DsOperSql(char p88DsOperSql) {
        this.p88DsOperSql = p88DsOperSql;
    }

    public void setP88DsOperSqlFormatted(String p88DsOperSql) {
        setP88DsOperSql(Functions.charAt(p88DsOperSql, Types.CHAR_SIZE));
    }

    public char getP88DsOperSql() {
        return this.p88DsOperSql;
    }

    public void setP88DsVer(int p88DsVer) {
        this.p88DsVer = p88DsVer;
    }

    public int getP88DsVer() {
        return this.p88DsVer;
    }

    public void setP88DsTsIniCptz(long p88DsTsIniCptz) {
        this.p88DsTsIniCptz = p88DsTsIniCptz;
    }

    public long getP88DsTsIniCptz() {
        return this.p88DsTsIniCptz;
    }

    public void setP88DsTsEndCptz(long p88DsTsEndCptz) {
        this.p88DsTsEndCptz = p88DsTsEndCptz;
    }

    public long getP88DsTsEndCptz() {
        return this.p88DsTsEndCptz;
    }

    public void setP88DsUtente(String p88DsUtente) {
        this.p88DsUtente = Functions.subString(p88DsUtente, Len.P88_DS_UTENTE);
    }

    public String getP88DsUtente() {
        return this.p88DsUtente;
    }

    public void setP88DsStatoElab(char p88DsStatoElab) {
        this.p88DsStatoElab = p88DsStatoElab;
    }

    public void setP88DsStatoElabFormatted(String p88DsStatoElab) {
        setP88DsStatoElab(Functions.charAt(p88DsStatoElab, Types.CHAR_SIZE));
    }

    public char getP88DsStatoElab() {
        return this.p88DsStatoElab;
    }

    public void setP88FlAllFnd(char p88FlAllFnd) {
        this.p88FlAllFnd = p88FlAllFnd;
    }

    public char getP88FlAllFnd() {
        return this.p88FlAllFnd;
    }

    public P88IdMoviChiu getP88IdMoviChiu() {
        return p88IdMoviChiu;
    }

    @Override
    public byte[] serialize() {
        return getAttServValBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P88_ID_ATT_SERV_VAL = 5;
        public static final int P88_COD_COMP_ANIA = 3;
        public static final int P88_ID_MOVI_CRZ = 5;
        public static final int P88_DT_INI_EFF = 5;
        public static final int P88_DT_END_EFF = 5;
        public static final int P88_TP_SERV_VAL = 2;
        public static final int P88_ID_OGG = 5;
        public static final int P88_TP_OGG = 2;
        public static final int P88_DS_RIGA = 6;
        public static final int P88_DS_OPER_SQL = 1;
        public static final int P88_DS_VER = 5;
        public static final int P88_DS_TS_INI_CPTZ = 10;
        public static final int P88_DS_TS_END_CPTZ = 10;
        public static final int P88_DS_UTENTE = 20;
        public static final int P88_DS_STATO_ELAB = 1;
        public static final int P88_FL_ALL_FND = 1;
        public static final int ATT_SERV_VAL = P88_ID_ATT_SERV_VAL + P88_COD_COMP_ANIA + P88_ID_MOVI_CRZ + P88IdMoviChiu.Len.P88_ID_MOVI_CHIU + P88_DT_INI_EFF + P88_DT_END_EFF + P88_TP_SERV_VAL + P88_ID_OGG + P88_TP_OGG + P88_DS_RIGA + P88_DS_OPER_SQL + P88_DS_VER + P88_DS_TS_INI_CPTZ + P88_DS_TS_END_CPTZ + P88_DS_UTENTE + P88_DS_STATO_ELAB + P88_FL_ALL_FND;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P88_ID_ATT_SERV_VAL = 9;
            public static final int P88_COD_COMP_ANIA = 5;
            public static final int P88_ID_MOVI_CRZ = 9;
            public static final int P88_DT_INI_EFF = 8;
            public static final int P88_DT_END_EFF = 8;
            public static final int P88_ID_OGG = 9;
            public static final int P88_DS_RIGA = 10;
            public static final int P88_DS_VER = 9;
            public static final int P88_DS_TS_INI_CPTZ = 18;
            public static final int P88_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
