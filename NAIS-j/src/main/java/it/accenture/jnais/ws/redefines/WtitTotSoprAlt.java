package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-SOPR-ALT<br>
 * Variable: WTIT-TOT-SOPR-ALT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotSoprAlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotSoprAlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_SOPR_ALT;
    }

    public void setWtitTotSoprAlt(AfDecimal wtitTotSoprAlt) {
        writeDecimalAsPacked(Pos.WTIT_TOT_SOPR_ALT, wtitTotSoprAlt.copy());
    }

    public void setWtitTotSoprAltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_SOPR_ALT, Pos.WTIT_TOT_SOPR_ALT);
    }

    /**Original name: WTIT-TOT-SOPR-ALT<br>*/
    public AfDecimal getWtitTotSoprAlt() {
        return readPackedAsDecimal(Pos.WTIT_TOT_SOPR_ALT, Len.Int.WTIT_TOT_SOPR_ALT, Len.Fract.WTIT_TOT_SOPR_ALT);
    }

    public byte[] getWtitTotSoprAltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_SOPR_ALT, Pos.WTIT_TOT_SOPR_ALT);
        return buffer;
    }

    public void initWtitTotSoprAltSpaces() {
        fill(Pos.WTIT_TOT_SOPR_ALT, Len.WTIT_TOT_SOPR_ALT, Types.SPACE_CHAR);
    }

    public void setWtitTotSoprAltNull(String wtitTotSoprAltNull) {
        writeString(Pos.WTIT_TOT_SOPR_ALT_NULL, wtitTotSoprAltNull, Len.WTIT_TOT_SOPR_ALT_NULL);
    }

    /**Original name: WTIT-TOT-SOPR-ALT-NULL<br>*/
    public String getWtitTotSoprAltNull() {
        return readString(Pos.WTIT_TOT_SOPR_ALT_NULL, Len.WTIT_TOT_SOPR_ALT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SOPR_ALT = 1;
        public static final int WTIT_TOT_SOPR_ALT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SOPR_ALT = 8;
        public static final int WTIT_TOT_SOPR_ALT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SOPR_ALT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SOPR_ALT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
