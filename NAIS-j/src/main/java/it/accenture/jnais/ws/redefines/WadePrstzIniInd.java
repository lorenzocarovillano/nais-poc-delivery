package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-PRSTZ-INI-IND<br>
 * Variable: WADE-PRSTZ-INI-IND from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadePrstzIniInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadePrstzIniInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_PRSTZ_INI_IND;
    }

    public void setWadePrstzIniInd(AfDecimal wadePrstzIniInd) {
        writeDecimalAsPacked(Pos.WADE_PRSTZ_INI_IND, wadePrstzIniInd.copy());
    }

    public void setWadePrstzIniIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_PRSTZ_INI_IND, Pos.WADE_PRSTZ_INI_IND);
    }

    /**Original name: WADE-PRSTZ-INI-IND<br>*/
    public AfDecimal getWadePrstzIniInd() {
        return readPackedAsDecimal(Pos.WADE_PRSTZ_INI_IND, Len.Int.WADE_PRSTZ_INI_IND, Len.Fract.WADE_PRSTZ_INI_IND);
    }

    public byte[] getWadePrstzIniIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_PRSTZ_INI_IND, Pos.WADE_PRSTZ_INI_IND);
        return buffer;
    }

    public void initWadePrstzIniIndSpaces() {
        fill(Pos.WADE_PRSTZ_INI_IND, Len.WADE_PRSTZ_INI_IND, Types.SPACE_CHAR);
    }

    public void setWadePrstzIniIndNull(String wadePrstzIniIndNull) {
        writeString(Pos.WADE_PRSTZ_INI_IND_NULL, wadePrstzIniIndNull, Len.WADE_PRSTZ_INI_IND_NULL);
    }

    /**Original name: WADE-PRSTZ-INI-IND-NULL<br>*/
    public String getWadePrstzIniIndNull() {
        return readString(Pos.WADE_PRSTZ_INI_IND_NULL, Len.WADE_PRSTZ_INI_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_PRSTZ_INI_IND = 1;
        public static final int WADE_PRSTZ_INI_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_PRSTZ_INI_IND = 8;
        public static final int WADE_PRSTZ_INI_IND_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_PRSTZ_INI_IND = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_PRSTZ_INI_IND = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
