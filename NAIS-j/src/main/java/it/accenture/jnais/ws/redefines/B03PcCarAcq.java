package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PC-CAR-ACQ<br>
 * Variable: B03-PC-CAR-ACQ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PcCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PcCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PC_CAR_ACQ;
    }

    public void setB03PcCarAcq(AfDecimal b03PcCarAcq) {
        writeDecimalAsPacked(Pos.B03_PC_CAR_ACQ, b03PcCarAcq.copy());
    }

    public void setB03PcCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PC_CAR_ACQ, Pos.B03_PC_CAR_ACQ);
    }

    /**Original name: B03-PC-CAR-ACQ<br>*/
    public AfDecimal getB03PcCarAcq() {
        return readPackedAsDecimal(Pos.B03_PC_CAR_ACQ, Len.Int.B03_PC_CAR_ACQ, Len.Fract.B03_PC_CAR_ACQ);
    }

    public byte[] getB03PcCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PC_CAR_ACQ, Pos.B03_PC_CAR_ACQ);
        return buffer;
    }

    public void setB03PcCarAcqNull(String b03PcCarAcqNull) {
        writeString(Pos.B03_PC_CAR_ACQ_NULL, b03PcCarAcqNull, Len.B03_PC_CAR_ACQ_NULL);
    }

    /**Original name: B03-PC-CAR-ACQ-NULL<br>*/
    public String getB03PcCarAcqNull() {
        return readString(Pos.B03_PC_CAR_ACQ_NULL, Len.B03_PC_CAR_ACQ_NULL);
    }

    public String getB03PcCarAcqNullFormatted() {
        return Functions.padBlanks(getB03PcCarAcqNull(), Len.B03_PC_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PC_CAR_ACQ = 1;
        public static final int B03_PC_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PC_CAR_ACQ = 4;
        public static final int B03_PC_CAR_ACQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PC_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PC_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
