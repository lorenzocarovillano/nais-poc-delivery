package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RIT-IRPEF-EFFLQ<br>
 * Variable: WDFL-RIT-IRPEF-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflRitIrpefEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflRitIrpefEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RIT_IRPEF_EFFLQ;
    }

    public void setWdflRitIrpefEfflq(AfDecimal wdflRitIrpefEfflq) {
        writeDecimalAsPacked(Pos.WDFL_RIT_IRPEF_EFFLQ, wdflRitIrpefEfflq.copy());
    }

    public void setWdflRitIrpefEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RIT_IRPEF_EFFLQ, Pos.WDFL_RIT_IRPEF_EFFLQ);
    }

    /**Original name: WDFL-RIT-IRPEF-EFFLQ<br>*/
    public AfDecimal getWdflRitIrpefEfflq() {
        return readPackedAsDecimal(Pos.WDFL_RIT_IRPEF_EFFLQ, Len.Int.WDFL_RIT_IRPEF_EFFLQ, Len.Fract.WDFL_RIT_IRPEF_EFFLQ);
    }

    public byte[] getWdflRitIrpefEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RIT_IRPEF_EFFLQ, Pos.WDFL_RIT_IRPEF_EFFLQ);
        return buffer;
    }

    public void setWdflRitIrpefEfflqNull(String wdflRitIrpefEfflqNull) {
        writeString(Pos.WDFL_RIT_IRPEF_EFFLQ_NULL, wdflRitIrpefEfflqNull, Len.WDFL_RIT_IRPEF_EFFLQ_NULL);
    }

    /**Original name: WDFL-RIT-IRPEF-EFFLQ-NULL<br>*/
    public String getWdflRitIrpefEfflqNull() {
        return readString(Pos.WDFL_RIT_IRPEF_EFFLQ_NULL, Len.WDFL_RIT_IRPEF_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_IRPEF_EFFLQ = 1;
        public static final int WDFL_RIT_IRPEF_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_IRPEF_EFFLQ = 8;
        public static final int WDFL_RIT_IRPEF_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_IRPEF_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_IRPEF_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
