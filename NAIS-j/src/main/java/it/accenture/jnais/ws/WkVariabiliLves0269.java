package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.WkFrazMm;
import it.accenture.jnais.ws.enums.WkTpRatPerf;
import it.accenture.jnais.ws.occurs.WkTabGar;
import it.accenture.jnais.ws.redefines.NewDtValQuote;
import it.accenture.jnais.ws.redefines.WkAppoDtLves0269;
import it.accenture.jnais.ws.redefines.WkAppoIas;
import it.accenture.jnais.ws.redefines.WkDiffPror;

/**Original name: WK-VARIABILI<br>
 * Variable: WK-VARIABILI from program LVES0269<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVariabiliLves0269 {

    //==== PROPERTIES ====
    public static final int WK_TAB_GAR_MAXOCCURS = 5;
    //Original name: WK-APPO-DT
    private WkAppoDtLves0269 wkAppoDt = new WkAppoDtLves0269();
    //Original name: NEW-DT-VAL-QUOTE
    private NewDtValQuote newDtValQuote = new NewDtValQuote();
    //Original name: WK-APPO-LUNGHEZZA
    private int wkAppoLunghezza = 0;
    //Original name: WK-ID-GAR
    private int wkIdGar = 0;
    //Original name: WK-DT-DECORRENZA
    private int wkDtDecorrenza = 0;
    //Original name: WK-PREMIO-NEG
    private AfDecimal wkPremioNeg = new AfDecimal(0, 15, 3);
    //Original name: WK-DIFF-PROR
    private WkDiffPror wkDiffPror = new WkDiffPror();
    //Original name: WK-APPO-IAS
    private WkAppoIas wkAppoIas = new WkAppoIas();
    //Original name: WK-PARAMETRO
    private String wkParametro = DefaultValues.stringVal(Len.WK_PARAMETRO);
    public static final String WK_PR_RATEANTIC = "RATEANTIC";
    /**Original name: WK-RATE-ANTIC<br>
	 * <pre>--  numero rate anticipate</pre>*/
    private long wkRateAntic = 0;
    /**Original name: WK-RATE-RECUP<br>
	 * <pre>--  numero rate da recuperare</pre>*/
    private long wkRateRecup = 0;
    //Original name: WK-VAL-VAR
    private long wkValVar = 0;
    //Original name: WK-DATA-LIMITE
    private int wkDataLimite = 0;
    //Original name: WK-DATA-COMPETENZA
    private int wkDataCompetenza = 0;
    //Original name: WK-FRQ-MOVI
    private String wkFrqMovi = DefaultValues.stringVal(Len.WK_FRQ_MOVI);
    //Original name: WK-FRAZ-MM
    private WkFrazMm wkFrazMm = new WkFrazMm();
    //Original name: WK-TP-RAT-PERF
    private WkTpRatPerf wkTpRatPerf = new WkTpRatPerf();
    //Original name: WK-TAB-GAR
    private WkTabGar[] wkTabGar = new WkTabGar[WK_TAB_GAR_MAXOCCURS];
    //Original name: WS-TOT-PREMIO-ANNUO
    private AfDecimal wsTotPremioAnnuo = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-COD-TARIFFA
    private String wsCodTariffa = DefaultValues.stringVal(Len.WS_COD_TARIFFA);
    //Original name: WK-FL-INC-AUTOGEN
    private char wkFlIncAutogen = DefaultValues.CHAR_VAL;

    //==== CONSTRUCTORS ====
    public WkVariabiliLves0269() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wkTabGarIdx = 1; wkTabGarIdx <= WK_TAB_GAR_MAXOCCURS; wkTabGarIdx++) {
            wkTabGar[wkTabGarIdx - 1] = new WkTabGar();
        }
    }

    public void setWkAppoLunghezza(int wkAppoLunghezza) {
        this.wkAppoLunghezza = wkAppoLunghezza;
    }

    public int getWkAppoLunghezza() {
        return this.wkAppoLunghezza;
    }

    public void setWkIdGar(int wkIdGar) {
        this.wkIdGar = wkIdGar;
    }

    public int getWkIdGar() {
        return this.wkIdGar;
    }

    public void setWkDtDecorrenza(int wkDtDecorrenza) {
        this.wkDtDecorrenza = wkDtDecorrenza;
    }

    public int getWkDtDecorrenza() {
        return this.wkDtDecorrenza;
    }

    public void setWkPremioNeg(AfDecimal wkPremioNeg) {
        this.wkPremioNeg.assign(wkPremioNeg);
    }

    public AfDecimal getWkPremioNeg() {
        return this.wkPremioNeg.copy();
    }

    public void setWkParametro(String wkParametro) {
        this.wkParametro = Functions.subString(wkParametro, Len.WK_PARAMETRO);
    }

    public String getWkParametro() {
        return this.wkParametro;
    }

    public void setWkPrRateantic() {
        wkParametro = WK_PR_RATEANTIC;
    }

    public void setWkRateAntic(long wkRateAntic) {
        this.wkRateAntic = wkRateAntic;
    }

    public long getWkRateAntic() {
        return this.wkRateAntic;
    }

    public void setWkRateRecup(long wkRateRecup) {
        this.wkRateRecup = wkRateRecup;
    }

    public long getWkRateRecup() {
        return this.wkRateRecup;
    }

    public void setWkValVar(long wkValVar) {
        this.wkValVar = wkValVar;
    }

    public long getWkValVar() {
        return this.wkValVar;
    }

    public void setWkDataLimite(int wkDataLimite) {
        this.wkDataLimite = wkDataLimite;
    }

    public int getWkDataLimite() {
        return this.wkDataLimite;
    }

    public void setWkDataCompetenza(int wkDataCompetenza) {
        this.wkDataCompetenza = wkDataCompetenza;
    }

    public void setWkDataCompetenzaFormatted(String wkDataCompetenza) {
        setWkDataCompetenza(PicParser.display(new PicParams("9(8)").setUsage(PicUsage.PACKED)).parseInt(wkDataCompetenza));
    }

    public int getWkDataCompetenza() {
        return this.wkDataCompetenza;
    }

    public void setWkFrqMovi(int wkFrqMovi) {
        this.wkFrqMovi = NumericDisplay.asString(wkFrqMovi, Len.WK_FRQ_MOVI);
    }

    public void setWkFrqMoviFormatted(String wkFrqMovi) {
        this.wkFrqMovi = Trunc.toUnsignedNumeric(wkFrqMovi, Len.WK_FRQ_MOVI);
    }

    public int getWkFrqMovi() {
        return NumericDisplay.asInt(this.wkFrqMovi);
    }

    public void setWsTotPremioAnnuo(AfDecimal wsTotPremioAnnuo) {
        this.wsTotPremioAnnuo.assign(wsTotPremioAnnuo);
    }

    public AfDecimal getWsTotPremioAnnuo() {
        return this.wsTotPremioAnnuo.copy();
    }

    public void setWsCodTariffa(String wsCodTariffa) {
        this.wsCodTariffa = Functions.subString(wsCodTariffa, Len.WS_COD_TARIFFA);
    }

    public String getWsCodTariffa() {
        return this.wsCodTariffa;
    }

    public void setWkFlIncAutogen(char wkFlIncAutogen) {
        this.wkFlIncAutogen = wkFlIncAutogen;
    }

    public void setWkFlIncAutogenFormatted(String wkFlIncAutogen) {
        setWkFlIncAutogen(Functions.charAt(wkFlIncAutogen, Types.CHAR_SIZE));
    }

    public char getWkFlIncAutogen() {
        return this.wkFlIncAutogen;
    }

    public NewDtValQuote getNewDtValQuote() {
        return newDtValQuote;
    }

    public WkAppoDtLves0269 getWkAppoDt() {
        return wkAppoDt;
    }

    public WkAppoIas getWkAppoIas() {
        return wkAppoIas;
    }

    public WkDiffPror getWkDiffPror() {
        return wkDiffPror;
    }

    public WkFrazMm getWkFrazMm() {
        return wkFrazMm;
    }

    public WkTabGar getWkTabGar(int idx) {
        return wkTabGar[idx - 1];
    }

    public WkTpRatPerf getWkTpRatPerf() {
        return wkTpRatPerf;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_PARAMETRO = 12;
        public static final int WK_FRQ_MOVI = 5;
        public static final int WS_COD_TARIFFA = 12;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
