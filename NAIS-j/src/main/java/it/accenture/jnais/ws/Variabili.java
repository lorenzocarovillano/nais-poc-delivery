package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: VARIABILI<br>
 * Variable: VARIABILI from program LCCS0450<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Variabili {

    //==== PROPERTIES ====
    //Original name: WK-NUMERO-QUOTE
    private AfDecimal wkNumeroQuote = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: WK-CONTROVALORE
    private AfDecimal wkControvalore = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-DT-RICOR-TRANCHE
    private WkDtRicorTranche wkDtRicorTranche = new WkDtRicorTranche();
    //Original name: WK-ANNO
    private String wkAnno = DefaultValues.stringVal(Len.WK_ANNO);
    //Original name: WK-LABEL
    private String wkLabel = DefaultValues.stringVal(Len.WK_LABEL);

    //==== METHODS ====
    public void setWkNumeroQuote(AfDecimal wkNumeroQuote) {
        this.wkNumeroQuote.assign(wkNumeroQuote);
    }

    public AfDecimal getWkNumeroQuote() {
        return this.wkNumeroQuote.copy();
    }

    public void setWkControvalore(AfDecimal wkControvalore) {
        this.wkControvalore.assign(wkControvalore);
    }

    public AfDecimal getWkControvalore() {
        return this.wkControvalore.copy();
    }

    public void setWkAnno(short wkAnno) {
        this.wkAnno = NumericDisplay.asString(wkAnno, Len.WK_ANNO);
    }

    public void setWkAnnoFormatted(String wkAnno) {
        this.wkAnno = Trunc.toUnsignedNumeric(wkAnno, Len.WK_ANNO);
    }

    public short getWkAnno() {
        return NumericDisplay.asShort(this.wkAnno);
    }

    public String getWkAnnoFormatted() {
        return this.wkAnno;
    }

    public void setWkLabel(String wkLabel) {
        this.wkLabel = Functions.subString(wkLabel, Len.WK_LABEL);
    }

    public String getWkLabel() {
        return this.wkLabel;
    }

    public WkDtRicorTranche getWkDtRicorTranche() {
        return wkDtRicorTranche;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_ANNO = 4;
        public static final int WK_LABEL = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
