package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-CNBT-VOL-K3<br>
 * Variable: DFA-IMP-CNBT-VOL-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpCnbtVolK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpCnbtVolK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_CNBT_VOL_K3;
    }

    public void setDfaImpCnbtVolK3(AfDecimal dfaImpCnbtVolK3) {
        writeDecimalAsPacked(Pos.DFA_IMP_CNBT_VOL_K3, dfaImpCnbtVolK3.copy());
    }

    public void setDfaImpCnbtVolK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_CNBT_VOL_K3, Pos.DFA_IMP_CNBT_VOL_K3);
    }

    /**Original name: DFA-IMP-CNBT-VOL-K3<br>*/
    public AfDecimal getDfaImpCnbtVolK3() {
        return readPackedAsDecimal(Pos.DFA_IMP_CNBT_VOL_K3, Len.Int.DFA_IMP_CNBT_VOL_K3, Len.Fract.DFA_IMP_CNBT_VOL_K3);
    }

    public byte[] getDfaImpCnbtVolK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_CNBT_VOL_K3, Pos.DFA_IMP_CNBT_VOL_K3);
        return buffer;
    }

    public void setDfaImpCnbtVolK3Null(String dfaImpCnbtVolK3Null) {
        writeString(Pos.DFA_IMP_CNBT_VOL_K3_NULL, dfaImpCnbtVolK3Null, Len.DFA_IMP_CNBT_VOL_K3_NULL);
    }

    /**Original name: DFA-IMP-CNBT-VOL-K3-NULL<br>*/
    public String getDfaImpCnbtVolK3Null() {
        return readString(Pos.DFA_IMP_CNBT_VOL_K3_NULL, Len.DFA_IMP_CNBT_VOL_K3_NULL);
    }

    public String getDfaImpCnbtVolK3NullFormatted() {
        return Functions.padBlanks(getDfaImpCnbtVolK3Null(), Len.DFA_IMP_CNBT_VOL_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_VOL_K3 = 1;
        public static final int DFA_IMP_CNBT_VOL_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_VOL_K3 = 8;
        public static final int DFA_IMP_CNBT_VOL_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_VOL_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_VOL_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
