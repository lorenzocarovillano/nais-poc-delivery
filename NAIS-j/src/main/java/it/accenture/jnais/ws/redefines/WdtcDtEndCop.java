package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WDTC-DT-END-COP<br>
 * Variable: WDTC-DT-END-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcDtEndCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcDtEndCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_DT_END_COP;
    }

    public void setWdtcDtEndCop(int wdtcDtEndCop) {
        writeIntAsPacked(Pos.WDTC_DT_END_COP, wdtcDtEndCop, Len.Int.WDTC_DT_END_COP);
    }

    public void setWdtcDtEndCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_DT_END_COP, Pos.WDTC_DT_END_COP);
    }

    /**Original name: WDTC-DT-END-COP<br>*/
    public int getWdtcDtEndCop() {
        return readPackedAsInt(Pos.WDTC_DT_END_COP, Len.Int.WDTC_DT_END_COP);
    }

    public byte[] getWdtcDtEndCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_DT_END_COP, Pos.WDTC_DT_END_COP);
        return buffer;
    }

    public void initWdtcDtEndCopSpaces() {
        fill(Pos.WDTC_DT_END_COP, Len.WDTC_DT_END_COP, Types.SPACE_CHAR);
    }

    public void setWdtcDtEndCopNull(String wdtcDtEndCopNull) {
        writeString(Pos.WDTC_DT_END_COP_NULL, wdtcDtEndCopNull, Len.WDTC_DT_END_COP_NULL);
    }

    /**Original name: WDTC-DT-END-COP-NULL<br>*/
    public String getWdtcDtEndCopNull() {
        return readString(Pos.WDTC_DT_END_COP_NULL, Len.WDTC_DT_END_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_DT_END_COP = 1;
        public static final int WDTC_DT_END_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_DT_END_COP = 5;
        public static final int WDTC_DT_END_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_DT_END_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
