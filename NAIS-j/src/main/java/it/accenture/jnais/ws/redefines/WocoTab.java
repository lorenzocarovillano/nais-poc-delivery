package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-TAB<br>
 * Variable: WOCO-TAB from program IVVS0211<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoTab extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_OGG_COLL_MAXOCCURS = 60;
    public static final char WOCO_ST_ADD = 'A';
    public static final char WOCO_ST_MOD = 'M';
    public static final char WOCO_ST_INV = 'I';
    public static final char WOCO_ST_DEL = 'D';
    public static final char WOCO_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public WocoTab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_TAB;
    }

    public String getWocoTabFormatted() {
        return readFixedString(Pos.WOCO_TAB, Len.WOCO_TAB);
    }

    public void setWocoTabBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_TAB, Pos.WOCO_TAB);
    }

    public byte[] getWocoTabBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_TAB, Pos.WOCO_TAB);
        return buffer;
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.wocoStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: WOCO-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA OGG_COLLG
	 *    ALIAS OCO
	 *    ULTIMO AGG. 09 LUG 2010
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.wocoStatus(statusIdx - 1);
        return readChar(position);
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.wocoIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: WOCO-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.wocoIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdOggCollg(int idOggCollgIdx, int idOggCollg) {
        int position = Pos.wocoIdOggCollg(idOggCollgIdx - 1);
        writeIntAsPacked(position, idOggCollg, Len.Int.ID_OGG_COLLG);
    }

    /**Original name: WOCO-ID-OGG-COLLG<br>*/
    public int getIdOggCollg(int idOggCollgIdx) {
        int position = Pos.wocoIdOggCollg(idOggCollgIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_OGG_COLLG);
    }

    public void setIdOggCoinv(int idOggCoinvIdx, int idOggCoinv) {
        int position = Pos.wocoIdOggCoinv(idOggCoinvIdx - 1);
        writeIntAsPacked(position, idOggCoinv, Len.Int.ID_OGG_COINV);
    }

    /**Original name: WOCO-ID-OGG-COINV<br>*/
    public int getIdOggCoinv(int idOggCoinvIdx) {
        int position = Pos.wocoIdOggCoinv(idOggCoinvIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_OGG_COINV);
    }

    public void setTpOggCoinv(int tpOggCoinvIdx, String tpOggCoinv) {
        int position = Pos.wocoTpOggCoinv(tpOggCoinvIdx - 1);
        writeString(position, tpOggCoinv, Len.TP_OGG_COINV);
    }

    /**Original name: WOCO-TP-OGG-COINV<br>*/
    public String getTpOggCoinv(int tpOggCoinvIdx) {
        int position = Pos.wocoTpOggCoinv(tpOggCoinvIdx - 1);
        return readString(position, Len.TP_OGG_COINV);
    }

    public void setIdOggDer(int idOggDerIdx, int idOggDer) {
        int position = Pos.wocoIdOggDer(idOggDerIdx - 1);
        writeIntAsPacked(position, idOggDer, Len.Int.ID_OGG_DER);
    }

    /**Original name: WOCO-ID-OGG-DER<br>*/
    public int getIdOggDer(int idOggDerIdx) {
        int position = Pos.wocoIdOggDer(idOggDerIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_OGG_DER);
    }

    public void setTpOggDer(int tpOggDerIdx, String tpOggDer) {
        int position = Pos.wocoTpOggDer(tpOggDerIdx - 1);
        writeString(position, tpOggDer, Len.TP_OGG_DER);
    }

    /**Original name: WOCO-TP-OGG-DER<br>*/
    public String getTpOggDer(int tpOggDerIdx) {
        int position = Pos.wocoTpOggDer(tpOggDerIdx - 1);
        return readString(position, Len.TP_OGG_DER);
    }

    public void setIbRiftoEstno(int ibRiftoEstnoIdx, String ibRiftoEstno) {
        int position = Pos.wocoIbRiftoEstno(ibRiftoEstnoIdx - 1);
        writeString(position, ibRiftoEstno, Len.IB_RIFTO_ESTNO);
    }

    /**Original name: WOCO-IB-RIFTO-ESTNO<br>*/
    public String getIbRiftoEstno(int ibRiftoEstnoIdx) {
        int position = Pos.wocoIbRiftoEstno(ibRiftoEstnoIdx - 1);
        return readString(position, Len.IB_RIFTO_ESTNO);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.wocoIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: WOCO-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.wocoIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.wocoIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: WOCO-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.wocoIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.wocoDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: WOCO-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.wocoDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.wocoDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: WOCO-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.wocoDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.wocoCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: WOCO-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wocoCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setCodProd(int codProdIdx, String codProd) {
        int position = Pos.wocoCodProd(codProdIdx - 1);
        writeString(position, codProd, Len.COD_PROD);
    }

    /**Original name: WOCO-COD-PROD<br>*/
    public String getCodProd(int codProdIdx) {
        int position = Pos.wocoCodProd(codProdIdx - 1);
        return readString(position, Len.COD_PROD);
    }

    public void setDtScad(int dtScadIdx, int dtScad) {
        int position = Pos.wocoDtScad(dtScadIdx - 1);
        writeIntAsPacked(position, dtScad, Len.Int.DT_SCAD);
    }

    /**Original name: WOCO-DT-SCAD<br>*/
    public int getDtScad(int dtScadIdx) {
        int position = Pos.wocoDtScad(dtScadIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_SCAD);
    }

    public void setTpCollgm(int tpCollgmIdx, String tpCollgm) {
        int position = Pos.wocoTpCollgm(tpCollgmIdx - 1);
        writeString(position, tpCollgm, Len.TP_COLLGM);
    }

    /**Original name: WOCO-TP-COLLGM<br>*/
    public String getTpCollgm(int tpCollgmIdx) {
        int position = Pos.wocoTpCollgm(tpCollgmIdx - 1);
        return readString(position, Len.TP_COLLGM);
    }

    public void setTpTrasf(int tpTrasfIdx, String tpTrasf) {
        int position = Pos.wocoTpTrasf(tpTrasfIdx - 1);
        writeString(position, tpTrasf, Len.TP_TRASF);
    }

    /**Original name: WOCO-TP-TRASF<br>*/
    public String getTpTrasf(int tpTrasfIdx) {
        int position = Pos.wocoTpTrasf(tpTrasfIdx - 1);
        return readString(position, Len.TP_TRASF);
    }

    public void setRecProv(int recProvIdx, AfDecimal recProv) {
        int position = Pos.wocoRecProv(recProvIdx - 1);
        writeDecimalAsPacked(position, recProv.copy());
    }

    /**Original name: WOCO-REC-PROV<br>*/
    public AfDecimal getRecProv(int recProvIdx) {
        int position = Pos.wocoRecProv(recProvIdx - 1);
        return readPackedAsDecimal(position, Len.Int.REC_PROV, Len.Fract.REC_PROV);
    }

    public void setDtDecor(int dtDecorIdx, int dtDecor) {
        int position = Pos.wocoDtDecor(dtDecorIdx - 1);
        writeIntAsPacked(position, dtDecor, Len.Int.DT_DECOR);
    }

    /**Original name: WOCO-DT-DECOR<br>*/
    public int getDtDecor(int dtDecorIdx) {
        int position = Pos.wocoDtDecor(dtDecorIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_DECOR);
    }

    public void setDtUltPrePag(int dtUltPrePagIdx, int dtUltPrePag) {
        int position = Pos.wocoDtUltPrePag(dtUltPrePagIdx - 1);
        writeIntAsPacked(position, dtUltPrePag, Len.Int.DT_ULT_PRE_PAG);
    }

    /**Original name: WOCO-DT-ULT-PRE-PAG<br>*/
    public int getDtUltPrePag(int dtUltPrePagIdx) {
        int position = Pos.wocoDtUltPrePag(dtUltPrePagIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_ULT_PRE_PAG);
    }

    public void setTotPre(int totPreIdx, AfDecimal totPre) {
        int position = Pos.wocoTotPre(totPreIdx - 1);
        writeDecimalAsPacked(position, totPre.copy());
    }

    /**Original name: WOCO-TOT-PRE<br>*/
    public AfDecimal getTotPre(int totPreIdx) {
        int position = Pos.wocoTotPre(totPreIdx - 1);
        return readPackedAsDecimal(position, Len.Int.TOT_PRE, Len.Fract.TOT_PRE);
    }

    public void setRisMat(int risMatIdx, AfDecimal risMat) {
        int position = Pos.wocoRisMat(risMatIdx - 1);
        writeDecimalAsPacked(position, risMat.copy());
    }

    /**Original name: WOCO-RIS-MAT<br>*/
    public AfDecimal getRisMat(int risMatIdx) {
        int position = Pos.wocoRisMat(risMatIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RIS_MAT, Len.Fract.RIS_MAT);
    }

    public void setRisZil(int risZilIdx, AfDecimal risZil) {
        int position = Pos.wocoRisZil(risZilIdx - 1);
        writeDecimalAsPacked(position, risZil.copy());
    }

    /**Original name: WOCO-RIS-ZIL<br>*/
    public AfDecimal getRisZil(int risZilIdx) {
        int position = Pos.wocoRisZil(risZilIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RIS_ZIL, Len.Fract.RIS_ZIL);
    }

    public void setImpTrasf(int impTrasfIdx, AfDecimal impTrasf) {
        int position = Pos.wocoImpTrasf(impTrasfIdx - 1);
        writeDecimalAsPacked(position, impTrasf.copy());
    }

    /**Original name: WOCO-IMP-TRASF<br>*/
    public AfDecimal getImpTrasf(int impTrasfIdx) {
        int position = Pos.wocoImpTrasf(impTrasfIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_TRASF, Len.Fract.IMP_TRASF);
    }

    public void setImpReinvst(int impReinvstIdx, AfDecimal impReinvst) {
        int position = Pos.wocoImpReinvst(impReinvstIdx - 1);
        writeDecimalAsPacked(position, impReinvst.copy());
    }

    /**Original name: WOCO-IMP-REINVST<br>*/
    public AfDecimal getImpReinvst(int impReinvstIdx) {
        int position = Pos.wocoImpReinvst(impReinvstIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_REINVST, Len.Fract.IMP_REINVST);
    }

    public void setPcReinvstRilievi(int pcReinvstRilieviIdx, AfDecimal pcReinvstRilievi) {
        int position = Pos.wocoPcReinvstRilievi(pcReinvstRilieviIdx - 1);
        writeDecimalAsPacked(position, pcReinvstRilievi.copy());
    }

    /**Original name: WOCO-PC-REINVST-RILIEVI<br>*/
    public AfDecimal getPcReinvstRilievi(int pcReinvstRilieviIdx) {
        int position = Pos.wocoPcReinvstRilievi(pcReinvstRilieviIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_REINVST_RILIEVI, Len.Fract.PC_REINVST_RILIEVI);
    }

    public void setIb2oRiftoEstno(int ib2oRiftoEstnoIdx, String ib2oRiftoEstno) {
        int position = Pos.wocoIb2oRiftoEstno(ib2oRiftoEstnoIdx - 1);
        writeString(position, ib2oRiftoEstno, Len.IB2O_RIFTO_ESTNO);
    }

    /**Original name: WOCO-IB-2O-RIFTO-ESTNO<br>*/
    public String getIb2oRiftoEstno(int ib2oRiftoEstnoIdx) {
        int position = Pos.wocoIb2oRiftoEstno(ib2oRiftoEstnoIdx - 1);
        return readString(position, Len.IB2O_RIFTO_ESTNO);
    }

    public void setIndLiqAggMan(int indLiqAggManIdx, char indLiqAggMan) {
        int position = Pos.wocoIndLiqAggMan(indLiqAggManIdx - 1);
        writeChar(position, indLiqAggMan);
    }

    /**Original name: WOCO-IND-LIQ-AGG-MAN<br>*/
    public char getIndLiqAggMan(int indLiqAggManIdx) {
        int position = Pos.wocoIndLiqAggMan(indLiqAggManIdx - 1);
        return readChar(position);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.wocoDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: WOCO-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.wocoDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.wocoDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: WOCO-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.wocoDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.wocoDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: WOCO-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.wocoDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.wocoDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: WOCO-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.wocoDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.wocoDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: WOCO-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.wocoDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.wocoDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: WOCO-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.wocoDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.wocoDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: WOCO-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.wocoDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setImpCollg(int impCollgIdx, AfDecimal impCollg) {
        int position = Pos.wocoImpCollg(impCollgIdx - 1);
        writeDecimalAsPacked(position, impCollg.copy());
    }

    /**Original name: WOCO-IMP-COLLG<br>*/
    public AfDecimal getImpCollg(int impCollgIdx) {
        int position = Pos.wocoImpCollg(impCollgIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_COLLG, Len.Fract.IMP_COLLG);
    }

    public void setPrePerTrasf(int prePerTrasfIdx, AfDecimal prePerTrasf) {
        int position = Pos.wocoPrePerTrasf(prePerTrasfIdx - 1);
        writeDecimalAsPacked(position, prePerTrasf.copy());
    }

    /**Original name: WOCO-PRE-PER-TRASF<br>*/
    public AfDecimal getPrePerTrasf(int prePerTrasfIdx) {
        int position = Pos.wocoPrePerTrasf(prePerTrasfIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_PER_TRASF, Len.Fract.PRE_PER_TRASF);
    }

    public void setCarAcq(int carAcqIdx, AfDecimal carAcq) {
        int position = Pos.wocoCarAcq(carAcqIdx - 1);
        writeDecimalAsPacked(position, carAcq.copy());
    }

    /**Original name: WOCO-CAR-ACQ<br>*/
    public AfDecimal getCarAcq(int carAcqIdx) {
        int position = Pos.wocoCarAcq(carAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.CAR_ACQ, Len.Fract.CAR_ACQ);
    }

    public void setPre1aAnnualita(int pre1aAnnualitaIdx, AfDecimal pre1aAnnualita) {
        int position = Pos.wocoPre1aAnnualita(pre1aAnnualitaIdx - 1);
        writeDecimalAsPacked(position, pre1aAnnualita.copy());
    }

    /**Original name: WOCO-PRE-1A-ANNUALITA<br>*/
    public AfDecimal getPre1aAnnualita(int pre1aAnnualitaIdx) {
        int position = Pos.wocoPre1aAnnualita(pre1aAnnualitaIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE1A_ANNUALITA, Len.Fract.PRE1A_ANNUALITA);
    }

    public void setImpTrasferito(int impTrasferitoIdx, AfDecimal impTrasferito) {
        int position = Pos.wocoImpTrasferito(impTrasferitoIdx - 1);
        writeDecimalAsPacked(position, impTrasferito.copy());
    }

    /**Original name: WOCO-IMP-TRASFERITO<br>*/
    public AfDecimal getImpTrasferito(int impTrasferitoIdx) {
        int position = Pos.wocoImpTrasferito(impTrasferitoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_TRASFERITO, Len.Fract.IMP_TRASFERITO);
    }

    public void setTpModAbbinamento(int tpModAbbinamentoIdx, String tpModAbbinamento) {
        int position = Pos.wocoTpModAbbinamento(tpModAbbinamentoIdx - 1);
        writeString(position, tpModAbbinamento, Len.TP_MOD_ABBINAMENTO);
    }

    /**Original name: WOCO-TP-MOD-ABBINAMENTO<br>*/
    public String getTpModAbbinamento(int tpModAbbinamentoIdx) {
        int position = Pos.wocoTpModAbbinamento(tpModAbbinamentoIdx - 1);
        return readString(position, Len.TP_MOD_ABBINAMENTO);
    }

    public void setPcPreTrasferito(int pcPreTrasferitoIdx, AfDecimal pcPreTrasferito) {
        int position = Pos.wocoPcPreTrasferito(pcPreTrasferitoIdx - 1);
        writeDecimalAsPacked(position, pcPreTrasferito.copy());
    }

    /**Original name: WOCO-PC-PRE-TRASFERITO<br>*/
    public AfDecimal getPcPreTrasferito(int pcPreTrasferitoIdx) {
        int position = Pos.wocoPcPreTrasferito(pcPreTrasferitoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_PRE_TRASFERITO, Len.Fract.PC_PRE_TRASFERITO);
    }

    public void setRestoTab(String restoTab) {
        writeString(Pos.RESTO_TAB, restoTab, Len.RESTO_TAB);
    }

    /**Original name: WOCO-RESTO-TAB<br>*/
    public String getRestoTab() {
        return readString(Pos.RESTO_TAB, Len.RESTO_TAB);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_TAB = 1;
        public static final int WOCO_TAB_R = 1;
        public static final int FLR1 = WOCO_TAB_R;
        public static final int RESTO_TAB = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wocoTabOggColl(int idx) {
            return WOCO_TAB + idx * Len.TAB_OGG_COLL;
        }

        public static int wocoStatus(int idx) {
            return wocoTabOggColl(idx);
        }

        public static int wocoIdPtf(int idx) {
            return wocoStatus(idx) + Len.STATUS;
        }

        public static int wocoDati(int idx) {
            return wocoIdPtf(idx) + Len.ID_PTF;
        }

        public static int wocoIdOggCollg(int idx) {
            return wocoDati(idx);
        }

        public static int wocoIdOggCoinv(int idx) {
            return wocoIdOggCollg(idx) + Len.ID_OGG_COLLG;
        }

        public static int wocoTpOggCoinv(int idx) {
            return wocoIdOggCoinv(idx) + Len.ID_OGG_COINV;
        }

        public static int wocoIdOggDer(int idx) {
            return wocoTpOggCoinv(idx) + Len.TP_OGG_COINV;
        }

        public static int wocoTpOggDer(int idx) {
            return wocoIdOggDer(idx) + Len.ID_OGG_DER;
        }

        public static int wocoIbRiftoEstno(int idx) {
            return wocoTpOggDer(idx) + Len.TP_OGG_DER;
        }

        public static int wocoIbRiftoEstnoNull(int idx) {
            return wocoIbRiftoEstno(idx);
        }

        public static int wocoIdMoviCrz(int idx) {
            return wocoIbRiftoEstno(idx) + Len.IB_RIFTO_ESTNO;
        }

        public static int wocoIdMoviChiu(int idx) {
            return wocoIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int wocoIdMoviChiuNull(int idx) {
            return wocoIdMoviChiu(idx);
        }

        public static int wocoDtIniEff(int idx) {
            return wocoIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int wocoDtEndEff(int idx) {
            return wocoDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int wocoCodCompAnia(int idx) {
            return wocoDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int wocoCodProd(int idx) {
            return wocoCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wocoDtScad(int idx) {
            return wocoCodProd(idx) + Len.COD_PROD;
        }

        public static int wocoDtScadNull(int idx) {
            return wocoDtScad(idx);
        }

        public static int wocoTpCollgm(int idx) {
            return wocoDtScad(idx) + Len.DT_SCAD;
        }

        public static int wocoTpTrasf(int idx) {
            return wocoTpCollgm(idx) + Len.TP_COLLGM;
        }

        public static int wocoTpTrasfNull(int idx) {
            return wocoTpTrasf(idx);
        }

        public static int wocoRecProv(int idx) {
            return wocoTpTrasf(idx) + Len.TP_TRASF;
        }

        public static int wocoRecProvNull(int idx) {
            return wocoRecProv(idx);
        }

        public static int wocoDtDecor(int idx) {
            return wocoRecProv(idx) + Len.REC_PROV;
        }

        public static int wocoDtDecorNull(int idx) {
            return wocoDtDecor(idx);
        }

        public static int wocoDtUltPrePag(int idx) {
            return wocoDtDecor(idx) + Len.DT_DECOR;
        }

        public static int wocoDtUltPrePagNull(int idx) {
            return wocoDtUltPrePag(idx);
        }

        public static int wocoTotPre(int idx) {
            return wocoDtUltPrePag(idx) + Len.DT_ULT_PRE_PAG;
        }

        public static int wocoTotPreNull(int idx) {
            return wocoTotPre(idx);
        }

        public static int wocoRisMat(int idx) {
            return wocoTotPre(idx) + Len.TOT_PRE;
        }

        public static int wocoRisMatNull(int idx) {
            return wocoRisMat(idx);
        }

        public static int wocoRisZil(int idx) {
            return wocoRisMat(idx) + Len.RIS_MAT;
        }

        public static int wocoRisZilNull(int idx) {
            return wocoRisZil(idx);
        }

        public static int wocoImpTrasf(int idx) {
            return wocoRisZil(idx) + Len.RIS_ZIL;
        }

        public static int wocoImpTrasfNull(int idx) {
            return wocoImpTrasf(idx);
        }

        public static int wocoImpReinvst(int idx) {
            return wocoImpTrasf(idx) + Len.IMP_TRASF;
        }

        public static int wocoImpReinvstNull(int idx) {
            return wocoImpReinvst(idx);
        }

        public static int wocoPcReinvstRilievi(int idx) {
            return wocoImpReinvst(idx) + Len.IMP_REINVST;
        }

        public static int wocoPcReinvstRilieviNull(int idx) {
            return wocoPcReinvstRilievi(idx);
        }

        public static int wocoIb2oRiftoEstno(int idx) {
            return wocoPcReinvstRilievi(idx) + Len.PC_REINVST_RILIEVI;
        }

        public static int wocoIb2oRiftoEstnoNull(int idx) {
            return wocoIb2oRiftoEstno(idx);
        }

        public static int wocoIndLiqAggMan(int idx) {
            return wocoIb2oRiftoEstno(idx) + Len.IB2O_RIFTO_ESTNO;
        }

        public static int wocoIndLiqAggManNull(int idx) {
            return wocoIndLiqAggMan(idx);
        }

        public static int wocoDsRiga(int idx) {
            return wocoIndLiqAggMan(idx) + Len.IND_LIQ_AGG_MAN;
        }

        public static int wocoDsOperSql(int idx) {
            return wocoDsRiga(idx) + Len.DS_RIGA;
        }

        public static int wocoDsVer(int idx) {
            return wocoDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int wocoDsTsIniCptz(int idx) {
            return wocoDsVer(idx) + Len.DS_VER;
        }

        public static int wocoDsTsEndCptz(int idx) {
            return wocoDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int wocoDsUtente(int idx) {
            return wocoDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int wocoDsStatoElab(int idx) {
            return wocoDsUtente(idx) + Len.DS_UTENTE;
        }

        public static int wocoImpCollg(int idx) {
            return wocoDsStatoElab(idx) + Len.DS_STATO_ELAB;
        }

        public static int wocoImpCollgNull(int idx) {
            return wocoImpCollg(idx);
        }

        public static int wocoPrePerTrasf(int idx) {
            return wocoImpCollg(idx) + Len.IMP_COLLG;
        }

        public static int wocoPrePerTrasfNull(int idx) {
            return wocoPrePerTrasf(idx);
        }

        public static int wocoCarAcq(int idx) {
            return wocoPrePerTrasf(idx) + Len.PRE_PER_TRASF;
        }

        public static int wocoCarAcqNull(int idx) {
            return wocoCarAcq(idx);
        }

        public static int wocoPre1aAnnualita(int idx) {
            return wocoCarAcq(idx) + Len.CAR_ACQ;
        }

        public static int wocoPre1aAnnualitaNull(int idx) {
            return wocoPre1aAnnualita(idx);
        }

        public static int wocoImpTrasferito(int idx) {
            return wocoPre1aAnnualita(idx) + Len.PRE1A_ANNUALITA;
        }

        public static int wocoImpTrasferitoNull(int idx) {
            return wocoImpTrasferito(idx);
        }

        public static int wocoTpModAbbinamento(int idx) {
            return wocoImpTrasferito(idx) + Len.IMP_TRASFERITO;
        }

        public static int wocoTpModAbbinamentoNull(int idx) {
            return wocoTpModAbbinamento(idx);
        }

        public static int wocoPcPreTrasferito(int idx) {
            return wocoTpModAbbinamento(idx) + Len.TP_MOD_ABBINAMENTO;
        }

        public static int wocoPcPreTrasferitoNull(int idx) {
            return wocoPcPreTrasferito(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_OGG_COLLG = 5;
        public static final int ID_OGG_COINV = 5;
        public static final int TP_OGG_COINV = 2;
        public static final int ID_OGG_DER = 5;
        public static final int TP_OGG_DER = 2;
        public static final int IB_RIFTO_ESTNO = 40;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int COD_PROD = 12;
        public static final int DT_SCAD = 5;
        public static final int TP_COLLGM = 2;
        public static final int TP_TRASF = 2;
        public static final int REC_PROV = 8;
        public static final int DT_DECOR = 5;
        public static final int DT_ULT_PRE_PAG = 5;
        public static final int TOT_PRE = 8;
        public static final int RIS_MAT = 8;
        public static final int RIS_ZIL = 8;
        public static final int IMP_TRASF = 8;
        public static final int IMP_REINVST = 8;
        public static final int PC_REINVST_RILIEVI = 4;
        public static final int IB2O_RIFTO_ESTNO = 40;
        public static final int IND_LIQ_AGG_MAN = 1;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int IMP_COLLG = 8;
        public static final int PRE_PER_TRASF = 8;
        public static final int CAR_ACQ = 8;
        public static final int PRE1A_ANNUALITA = 8;
        public static final int IMP_TRASFERITO = 8;
        public static final int TP_MOD_ABBINAMENTO = 2;
        public static final int PC_PRE_TRASFERITO = 4;
        public static final int DATI = ID_OGG_COLLG + ID_OGG_COINV + TP_OGG_COINV + ID_OGG_DER + TP_OGG_DER + IB_RIFTO_ESTNO + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + COD_PROD + DT_SCAD + TP_COLLGM + TP_TRASF + REC_PROV + DT_DECOR + DT_ULT_PRE_PAG + TOT_PRE + RIS_MAT + RIS_ZIL + IMP_TRASF + IMP_REINVST + PC_REINVST_RILIEVI + IB2O_RIFTO_ESTNO + IND_LIQ_AGG_MAN + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + IMP_COLLG + PRE_PER_TRASF + CAR_ACQ + PRE1A_ANNUALITA + IMP_TRASFERITO + TP_MOD_ABBINAMENTO + PC_PRE_TRASFERITO;
        public static final int TAB_OGG_COLL = STATUS + ID_PTF + DATI;
        public static final int FLR1 = 311;
        public static final int WOCO_TAB = WocoTab.TAB_OGG_COLL_MAXOCCURS * TAB_OGG_COLL;
        public static final int RESTO_TAB = 18349;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_OGG_COLLG = 9;
            public static final int ID_OGG_COINV = 9;
            public static final int ID_OGG_DER = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_SCAD = 8;
            public static final int REC_PROV = 12;
            public static final int DT_DECOR = 8;
            public static final int DT_ULT_PRE_PAG = 8;
            public static final int TOT_PRE = 12;
            public static final int RIS_MAT = 12;
            public static final int RIS_ZIL = 12;
            public static final int IMP_TRASF = 12;
            public static final int IMP_REINVST = 12;
            public static final int PC_REINVST_RILIEVI = 3;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int IMP_COLLG = 12;
            public static final int PRE_PER_TRASF = 12;
            public static final int CAR_ACQ = 12;
            public static final int PRE1A_ANNUALITA = 12;
            public static final int IMP_TRASFERITO = 12;
            public static final int PC_PRE_TRASFERITO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int REC_PROV = 3;
            public static final int TOT_PRE = 3;
            public static final int RIS_MAT = 3;
            public static final int RIS_ZIL = 3;
            public static final int IMP_TRASF = 3;
            public static final int IMP_REINVST = 3;
            public static final int PC_REINVST_RILIEVI = 3;
            public static final int IMP_COLLG = 3;
            public static final int PRE_PER_TRASF = 3;
            public static final int CAR_ACQ = 3;
            public static final int PRE1A_ANNUALITA = 3;
            public static final int IMP_TRASFERITO = 3;
            public static final int PC_PRE_TRASFERITO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
