package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POG-VAL-DT<br>
 * Variable: POG-VAL-DT from program LDBS1130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PogValDt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PogValDt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POG_VAL_DT;
    }

    public void setPogValDt(int pogValDt) {
        writeIntAsPacked(Pos.POG_VAL_DT, pogValDt, Len.Int.POG_VAL_DT);
    }

    public void setPogValDtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POG_VAL_DT, Pos.POG_VAL_DT);
    }

    /**Original name: POG-VAL-DT<br>*/
    public int getPogValDt() {
        return readPackedAsInt(Pos.POG_VAL_DT, Len.Int.POG_VAL_DT);
    }

    public byte[] getPogValDtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POG_VAL_DT, Pos.POG_VAL_DT);
        return buffer;
    }

    public void setPogValDtNull(String pogValDtNull) {
        writeString(Pos.POG_VAL_DT_NULL, pogValDtNull, Len.POG_VAL_DT_NULL);
    }

    /**Original name: POG-VAL-DT-NULL<br>*/
    public String getPogValDtNull() {
        return readString(Pos.POG_VAL_DT_NULL, Len.POG_VAL_DT_NULL);
    }

    public String getPogValDtNullFormatted() {
        return Functions.padBlanks(getPogValDtNull(), Len.POG_VAL_DT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POG_VAL_DT = 1;
        public static final int POG_VAL_DT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POG_VAL_DT = 5;
        public static final int POG_VAL_DT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POG_VAL_DT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
