package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTGZ-TRCH-E-IN<br>
 * Variable: WPCO-DT-ULTGZ-TRCH-E-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltgzTrchEIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltgzTrchEIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTGZ_TRCH_E_IN;
    }

    public void setWpcoDtUltgzTrchEIn(int wpcoDtUltgzTrchEIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULTGZ_TRCH_E_IN, wpcoDtUltgzTrchEIn, Len.Int.WPCO_DT_ULTGZ_TRCH_E_IN);
    }

    public void setDpcoDtUltgzTrchEInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTGZ_TRCH_E_IN, Pos.WPCO_DT_ULTGZ_TRCH_E_IN);
    }

    /**Original name: WPCO-DT-ULTGZ-TRCH-E-IN<br>*/
    public int getWpcoDtUltgzTrchEIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULTGZ_TRCH_E_IN, Len.Int.WPCO_DT_ULTGZ_TRCH_E_IN);
    }

    public byte[] getWpcoDtUltgzTrchEInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTGZ_TRCH_E_IN, Pos.WPCO_DT_ULTGZ_TRCH_E_IN);
        return buffer;
    }

    public void setWpcoDtUltgzTrchEInNull(String wpcoDtUltgzTrchEInNull) {
        writeString(Pos.WPCO_DT_ULTGZ_TRCH_E_IN_NULL, wpcoDtUltgzTrchEInNull, Len.WPCO_DT_ULTGZ_TRCH_E_IN_NULL);
    }

    /**Original name: WPCO-DT-ULTGZ-TRCH-E-IN-NULL<br>*/
    public String getWpcoDtUltgzTrchEInNull() {
        return readString(Pos.WPCO_DT_ULTGZ_TRCH_E_IN_NULL, Len.WPCO_DT_ULTGZ_TRCH_E_IN_NULL);
    }

    public String getWpcoDtUltgzTrchEInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltgzTrchEInNull(), Len.WPCO_DT_ULTGZ_TRCH_E_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTGZ_TRCH_E_IN = 1;
        public static final int WPCO_DT_ULTGZ_TRCH_E_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTGZ_TRCH_E_IN = 5;
        public static final int WPCO_DT_ULTGZ_TRCH_E_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTGZ_TRCH_E_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
