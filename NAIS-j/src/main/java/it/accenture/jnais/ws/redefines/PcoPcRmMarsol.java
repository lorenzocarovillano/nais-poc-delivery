package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-PC-RM-MARSOL<br>
 * Variable: PCO-PC-RM-MARSOL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoPcRmMarsol extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoPcRmMarsol() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_PC_RM_MARSOL;
    }

    public void setPcoPcRmMarsol(AfDecimal pcoPcRmMarsol) {
        writeDecimalAsPacked(Pos.PCO_PC_RM_MARSOL, pcoPcRmMarsol.copy());
    }

    public void setPcoPcRmMarsolFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_PC_RM_MARSOL, Pos.PCO_PC_RM_MARSOL);
    }

    /**Original name: PCO-PC-RM-MARSOL<br>*/
    public AfDecimal getPcoPcRmMarsol() {
        return readPackedAsDecimal(Pos.PCO_PC_RM_MARSOL, Len.Int.PCO_PC_RM_MARSOL, Len.Fract.PCO_PC_RM_MARSOL);
    }

    public byte[] getPcoPcRmMarsolAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_PC_RM_MARSOL, Pos.PCO_PC_RM_MARSOL);
        return buffer;
    }

    public void initPcoPcRmMarsolHighValues() {
        fill(Pos.PCO_PC_RM_MARSOL, Len.PCO_PC_RM_MARSOL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoPcRmMarsolNull(String pcoPcRmMarsolNull) {
        writeString(Pos.PCO_PC_RM_MARSOL_NULL, pcoPcRmMarsolNull, Len.PCO_PC_RM_MARSOL_NULL);
    }

    /**Original name: PCO-PC-RM-MARSOL-NULL<br>*/
    public String getPcoPcRmMarsolNull() {
        return readString(Pos.PCO_PC_RM_MARSOL_NULL, Len.PCO_PC_RM_MARSOL_NULL);
    }

    public String getPcoPcRmMarsolNullFormatted() {
        return Functions.padBlanks(getPcoPcRmMarsolNull(), Len.PCO_PC_RM_MARSOL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_PC_RM_MARSOL = 1;
        public static final int PCO_PC_RM_MARSOL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_PC_RM_MARSOL = 4;
        public static final int PCO_PC_RM_MARSOL_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_PC_RM_MARSOL = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_PC_RM_MARSOL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
