package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-EC-IL-IND<br>
 * Variable: WPCO-DT-ULT-EC-IL-IND from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEcIlInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEcIlInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_EC_IL_IND;
    }

    public void setWpcoDtUltEcIlInd(int wpcoDtUltEcIlInd) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_EC_IL_IND, wpcoDtUltEcIlInd, Len.Int.WPCO_DT_ULT_EC_IL_IND);
    }

    public void setDpcoDtUltEcIlIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_EC_IL_IND, Pos.WPCO_DT_ULT_EC_IL_IND);
    }

    /**Original name: WPCO-DT-ULT-EC-IL-IND<br>*/
    public int getWpcoDtUltEcIlInd() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_EC_IL_IND, Len.Int.WPCO_DT_ULT_EC_IL_IND);
    }

    public byte[] getWpcoDtUltEcIlIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_EC_IL_IND, Pos.WPCO_DT_ULT_EC_IL_IND);
        return buffer;
    }

    public void setWpcoDtUltEcIlIndNull(String wpcoDtUltEcIlIndNull) {
        writeString(Pos.WPCO_DT_ULT_EC_IL_IND_NULL, wpcoDtUltEcIlIndNull, Len.WPCO_DT_ULT_EC_IL_IND_NULL);
    }

    /**Original name: WPCO-DT-ULT-EC-IL-IND-NULL<br>*/
    public String getWpcoDtUltEcIlIndNull() {
        return readString(Pos.WPCO_DT_ULT_EC_IL_IND_NULL, Len.WPCO_DT_ULT_EC_IL_IND_NULL);
    }

    public String getWpcoDtUltEcIlIndNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEcIlIndNull(), Len.WPCO_DT_ULT_EC_IL_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_IL_IND = 1;
        public static final int WPCO_DT_ULT_EC_IL_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_IL_IND = 5;
        public static final int WPCO_DT_ULT_EC_IL_IND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_EC_IL_IND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
