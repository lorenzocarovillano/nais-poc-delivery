package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RDF-DT-DIS-CALC<br>
 * Variable: RDF-DT-DIS-CALC from program IDBSRDF0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RdfDtDisCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RdfDtDisCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RDF_DT_DIS_CALC;
    }

    public void setRdfDtDisCalc(int rdfDtDisCalc) {
        writeIntAsPacked(Pos.RDF_DT_DIS_CALC, rdfDtDisCalc, Len.Int.RDF_DT_DIS_CALC);
    }

    public void setRdfDtDisCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RDF_DT_DIS_CALC, Pos.RDF_DT_DIS_CALC);
    }

    /**Original name: RDF-DT-DIS-CALC<br>*/
    public int getRdfDtDisCalc() {
        return readPackedAsInt(Pos.RDF_DT_DIS_CALC, Len.Int.RDF_DT_DIS_CALC);
    }

    public byte[] getRdfDtDisCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RDF_DT_DIS_CALC, Pos.RDF_DT_DIS_CALC);
        return buffer;
    }

    public void setRdfDtDisCalcNull(String rdfDtDisCalcNull) {
        writeString(Pos.RDF_DT_DIS_CALC_NULL, rdfDtDisCalcNull, Len.RDF_DT_DIS_CALC_NULL);
    }

    /**Original name: RDF-DT-DIS-CALC-NULL<br>*/
    public String getRdfDtDisCalcNull() {
        return readString(Pos.RDF_DT_DIS_CALC_NULL, Len.RDF_DT_DIS_CALC_NULL);
    }

    public String getRdfDtDisCalcNullFormatted() {
        return Functions.padBlanks(getRdfDtDisCalcNull(), Len.RDF_DT_DIS_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RDF_DT_DIS_CALC = 1;
        public static final int RDF_DT_DIS_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_DT_DIS_CALC = 5;
        public static final int RDF_DT_DIS_CALC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_DT_DIS_CALC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
