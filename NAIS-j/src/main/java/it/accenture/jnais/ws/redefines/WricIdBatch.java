package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WRIC-ID-BATCH<br>
 * Variable: WRIC-ID-BATCH from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WricIdBatch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WricIdBatch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIC_ID_BATCH;
    }

    public void setWricIdBatch(int wricIdBatch) {
        writeIntAsPacked(Pos.WRIC_ID_BATCH, wricIdBatch, Len.Int.WRIC_ID_BATCH);
    }

    public void setWricIdBatchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIC_ID_BATCH, Pos.WRIC_ID_BATCH);
    }

    /**Original name: WRIC-ID-BATCH<br>*/
    public int getWricIdBatch() {
        return readPackedAsInt(Pos.WRIC_ID_BATCH, Len.Int.WRIC_ID_BATCH);
    }

    public byte[] getWricIdBatchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIC_ID_BATCH, Pos.WRIC_ID_BATCH);
        return buffer;
    }

    public void setWricIdBatchNull(String wricIdBatchNull) {
        writeString(Pos.WRIC_ID_BATCH_NULL, wricIdBatchNull, Len.WRIC_ID_BATCH_NULL);
    }

    /**Original name: WRIC-ID-BATCH-NULL<br>*/
    public String getWricIdBatchNull() {
        return readString(Pos.WRIC_ID_BATCH_NULL, Len.WRIC_ID_BATCH_NULL);
    }

    public String getWricIdBatchNullFormatted() {
        return Functions.padBlanks(getWricIdBatchNull(), Len.WRIC_ID_BATCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIC_ID_BATCH = 1;
        public static final int WRIC_ID_BATCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIC_ID_BATCH = 5;
        public static final int WRIC_ID_BATCH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIC_ID_BATCH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
