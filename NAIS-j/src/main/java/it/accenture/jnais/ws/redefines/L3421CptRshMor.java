package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-CPT-RSH-MOR<br>
 * Variable: L3421-CPT-RSH-MOR from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421CptRshMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421CptRshMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_CPT_RSH_MOR;
    }

    public void setL3421CptRshMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_CPT_RSH_MOR, Pos.L3421_CPT_RSH_MOR);
    }

    /**Original name: L3421-CPT-RSH-MOR<br>*/
    public AfDecimal getL3421CptRshMor() {
        return readPackedAsDecimal(Pos.L3421_CPT_RSH_MOR, Len.Int.L3421_CPT_RSH_MOR, Len.Fract.L3421_CPT_RSH_MOR);
    }

    public byte[] getL3421CptRshMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_CPT_RSH_MOR, Pos.L3421_CPT_RSH_MOR);
        return buffer;
    }

    /**Original name: L3421-CPT-RSH-MOR-NULL<br>*/
    public String getL3421CptRshMorNull() {
        return readString(Pos.L3421_CPT_RSH_MOR_NULL, Len.L3421_CPT_RSH_MOR_NULL);
    }

    public String getL3421CptRshMorNullFormatted() {
        return Functions.padBlanks(getL3421CptRshMorNull(), Len.L3421_CPT_RSH_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_CPT_RSH_MOR = 1;
        public static final int L3421_CPT_RSH_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_CPT_RSH_MOR = 8;
        public static final int L3421_CPT_RSH_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_CPT_RSH_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_CPT_RSH_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
