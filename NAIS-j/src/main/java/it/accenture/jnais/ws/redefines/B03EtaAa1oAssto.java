package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ETA-AA-1O-ASSTO<br>
 * Variable: B03-ETA-AA-1O-ASSTO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03EtaAa1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03EtaAa1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ETA_AA1O_ASSTO;
    }

    public void setB03EtaAa1oAssto(int b03EtaAa1oAssto) {
        writeIntAsPacked(Pos.B03_ETA_AA1O_ASSTO, b03EtaAa1oAssto, Len.Int.B03_ETA_AA1O_ASSTO);
    }

    public void setB03EtaAa1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ETA_AA1O_ASSTO, Pos.B03_ETA_AA1O_ASSTO);
    }

    /**Original name: B03-ETA-AA-1O-ASSTO<br>*/
    public int getB03EtaAa1oAssto() {
        return readPackedAsInt(Pos.B03_ETA_AA1O_ASSTO, Len.Int.B03_ETA_AA1O_ASSTO);
    }

    public byte[] getB03EtaAa1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ETA_AA1O_ASSTO, Pos.B03_ETA_AA1O_ASSTO);
        return buffer;
    }

    public void setB03EtaAa1oAsstoNull(String b03EtaAa1oAsstoNull) {
        writeString(Pos.B03_ETA_AA1O_ASSTO_NULL, b03EtaAa1oAsstoNull, Len.B03_ETA_AA1O_ASSTO_NULL);
    }

    /**Original name: B03-ETA-AA-1O-ASSTO-NULL<br>*/
    public String getB03EtaAa1oAsstoNull() {
        return readString(Pos.B03_ETA_AA1O_ASSTO_NULL, Len.B03_ETA_AA1O_ASSTO_NULL);
    }

    public String getB03EtaAa1oAsstoNullFormatted() {
        return Functions.padBlanks(getB03EtaAa1oAsstoNull(), Len.B03_ETA_AA1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ETA_AA1O_ASSTO = 1;
        public static final int B03_ETA_AA1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ETA_AA1O_ASSTO = 3;
        public static final int B03_ETA_AA1O_ASSTO_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ETA_AA1O_ASSTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
