package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-CAR-ACQ-TGA<br>
 * Variable: WPAG-IMP-CAR-ACQ-TGA from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpCarAcqTga extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpCarAcqTga() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_CAR_ACQ_TGA;
    }

    public void setWpagImpCarAcqTga(AfDecimal wpagImpCarAcqTga) {
        writeDecimalAsPacked(Pos.WPAG_IMP_CAR_ACQ_TGA, wpagImpCarAcqTga.copy());
    }

    public void setWpagImpCarAcqTgaFormatted(String wpagImpCarAcqTga) {
        setWpagImpCarAcqTga(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_CAR_ACQ_TGA + Len.Fract.WPAG_IMP_CAR_ACQ_TGA, Len.Fract.WPAG_IMP_CAR_ACQ_TGA, wpagImpCarAcqTga));
    }

    public void setWpagImpCarAcqTgaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_CAR_ACQ_TGA, Pos.WPAG_IMP_CAR_ACQ_TGA);
    }

    /**Original name: WPAG-IMP-CAR-ACQ-TGA<br>*/
    public AfDecimal getWpagImpCarAcqTga() {
        return readPackedAsDecimal(Pos.WPAG_IMP_CAR_ACQ_TGA, Len.Int.WPAG_IMP_CAR_ACQ_TGA, Len.Fract.WPAG_IMP_CAR_ACQ_TGA);
    }

    public byte[] getWpagImpCarAcqTgaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_CAR_ACQ_TGA, Pos.WPAG_IMP_CAR_ACQ_TGA);
        return buffer;
    }

    public void initWpagImpCarAcqTgaSpaces() {
        fill(Pos.WPAG_IMP_CAR_ACQ_TGA, Len.WPAG_IMP_CAR_ACQ_TGA, Types.SPACE_CHAR);
    }

    public void setWpagImpCarAcqTgaNull(String wpagImpCarAcqTgaNull) {
        writeString(Pos.WPAG_IMP_CAR_ACQ_TGA_NULL, wpagImpCarAcqTgaNull, Len.WPAG_IMP_CAR_ACQ_TGA_NULL);
    }

    /**Original name: WPAG-IMP-CAR-ACQ-TGA-NULL<br>*/
    public String getWpagImpCarAcqTgaNull() {
        return readString(Pos.WPAG_IMP_CAR_ACQ_TGA_NULL, Len.WPAG_IMP_CAR_ACQ_TGA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_ACQ_TGA = 1;
        public static final int WPAG_IMP_CAR_ACQ_TGA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_ACQ_TGA = 8;
        public static final int WPAG_IMP_CAR_ACQ_TGA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_ACQ_TGA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_ACQ_TGA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
