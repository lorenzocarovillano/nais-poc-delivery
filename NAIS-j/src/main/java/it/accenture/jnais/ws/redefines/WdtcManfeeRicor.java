package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-MANFEE-RICOR<br>
 * Variable: WDTC-MANFEE-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcManfeeRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcManfeeRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_MANFEE_RICOR;
    }

    public void setWdtcManfeeRicor(AfDecimal wdtcManfeeRicor) {
        writeDecimalAsPacked(Pos.WDTC_MANFEE_RICOR, wdtcManfeeRicor.copy());
    }

    public void setWdtcManfeeRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_MANFEE_RICOR, Pos.WDTC_MANFEE_RICOR);
    }

    /**Original name: WDTC-MANFEE-RICOR<br>*/
    public AfDecimal getWdtcManfeeRicor() {
        return readPackedAsDecimal(Pos.WDTC_MANFEE_RICOR, Len.Int.WDTC_MANFEE_RICOR, Len.Fract.WDTC_MANFEE_RICOR);
    }

    public byte[] getWdtcManfeeRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_MANFEE_RICOR, Pos.WDTC_MANFEE_RICOR);
        return buffer;
    }

    public void initWdtcManfeeRicorSpaces() {
        fill(Pos.WDTC_MANFEE_RICOR, Len.WDTC_MANFEE_RICOR, Types.SPACE_CHAR);
    }

    public void setWdtcManfeeRicorNull(String wdtcManfeeRicorNull) {
        writeString(Pos.WDTC_MANFEE_RICOR_NULL, wdtcManfeeRicorNull, Len.WDTC_MANFEE_RICOR_NULL);
    }

    /**Original name: WDTC-MANFEE-RICOR-NULL<br>*/
    public String getWdtcManfeeRicorNull() {
        return readString(Pos.WDTC_MANFEE_RICOR_NULL, Len.WDTC_MANFEE_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_MANFEE_RICOR = 1;
        public static final int WDTC_MANFEE_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_MANFEE_RICOR = 8;
        public static final int WDTC_MANFEE_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_MANFEE_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_MANFEE_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
