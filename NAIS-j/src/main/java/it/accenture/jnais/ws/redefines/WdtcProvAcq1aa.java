package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-PROV-ACQ-1AA<br>
 * Variable: WDTC-PROV-ACQ-1AA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcProvAcq1aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcProvAcq1aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_PROV_ACQ1AA;
    }

    public void setWdtcProvAcq1aa(AfDecimal wdtcProvAcq1aa) {
        writeDecimalAsPacked(Pos.WDTC_PROV_ACQ1AA, wdtcProvAcq1aa.copy());
    }

    public void setWdtcProvAcq1aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_PROV_ACQ1AA, Pos.WDTC_PROV_ACQ1AA);
    }

    /**Original name: WDTC-PROV-ACQ-1AA<br>*/
    public AfDecimal getWdtcProvAcq1aa() {
        return readPackedAsDecimal(Pos.WDTC_PROV_ACQ1AA, Len.Int.WDTC_PROV_ACQ1AA, Len.Fract.WDTC_PROV_ACQ1AA);
    }

    public byte[] getWdtcProvAcq1aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_PROV_ACQ1AA, Pos.WDTC_PROV_ACQ1AA);
        return buffer;
    }

    public void initWdtcProvAcq1aaSpaces() {
        fill(Pos.WDTC_PROV_ACQ1AA, Len.WDTC_PROV_ACQ1AA, Types.SPACE_CHAR);
    }

    public void setWdtcProvAcq1aaNull(String wdtcProvAcq1aaNull) {
        writeString(Pos.WDTC_PROV_ACQ1AA_NULL, wdtcProvAcq1aaNull, Len.WDTC_PROV_ACQ1AA_NULL);
    }

    /**Original name: WDTC-PROV-ACQ-1AA-NULL<br>*/
    public String getWdtcProvAcq1aaNull() {
        return readString(Pos.WDTC_PROV_ACQ1AA_NULL, Len.WDTC_PROV_ACQ1AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_PROV_ACQ1AA = 1;
        public static final int WDTC_PROV_ACQ1AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_PROV_ACQ1AA = 8;
        public static final int WDTC_PROV_ACQ1AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_PROV_ACQ1AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_PROV_ACQ1AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
