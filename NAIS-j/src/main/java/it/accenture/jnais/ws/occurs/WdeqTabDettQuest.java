package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvdeq1;
import it.accenture.jnais.copy.WdeqDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WDEQ-TAB-DETT-QUEST<br>
 * Variables: WDEQ-TAB-DETT-QUEST from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WdeqTabDettQuest {

    //==== PROPERTIES ====
    //Original name: LCCVDEQ1
    private Lccvdeq1 lccvdeq1 = new Lccvdeq1();

    //==== METHODS ====
    public void setVdeqTabDettQuestBytes(byte[] buffer) {
        setWdeqTabDettQuestBytes(buffer, 1);
    }

    public byte[] getTabDettQuestBytes() {
        byte[] buffer = new byte[Len.WDEQ_TAB_DETT_QUEST];
        return getWdeqTabDettQuestBytes(buffer, 1);
    }

    public void setWdeqTabDettQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdeq1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdeq1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdeq1.Len.Int.ID_PTF, 0));
        position += Lccvdeq1.Len.ID_PTF;
        lccvdeq1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdeqTabDettQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdeq1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdeq1.getIdPtf(), Lccvdeq1.Len.Int.ID_PTF, 0);
        position += Lccvdeq1.Len.ID_PTF;
        lccvdeq1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWdeqTabDettQuestSpaces() {
        lccvdeq1.initLccvdeq1Spaces();
    }

    public Lccvdeq1 getLccvdeq1() {
        return lccvdeq1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDEQ_TAB_DETT_QUEST = WpolStatus.Len.STATUS + Lccvdeq1.Len.ID_PTF + WdeqDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
