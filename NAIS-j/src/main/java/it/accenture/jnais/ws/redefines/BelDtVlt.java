package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-DT-VLT<br>
 * Variable: BEL-DT-VLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelDtVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelDtVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_DT_VLT;
    }

    public void setBelDtVlt(int belDtVlt) {
        writeIntAsPacked(Pos.BEL_DT_VLT, belDtVlt, Len.Int.BEL_DT_VLT);
    }

    public void setBelDtVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_DT_VLT, Pos.BEL_DT_VLT);
    }

    /**Original name: BEL-DT-VLT<br>*/
    public int getBelDtVlt() {
        return readPackedAsInt(Pos.BEL_DT_VLT, Len.Int.BEL_DT_VLT);
    }

    public byte[] getBelDtVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_DT_VLT, Pos.BEL_DT_VLT);
        return buffer;
    }

    public void setBelDtVltNull(String belDtVltNull) {
        writeString(Pos.BEL_DT_VLT_NULL, belDtVltNull, Len.BEL_DT_VLT_NULL);
    }

    /**Original name: BEL-DT-VLT-NULL<br>*/
    public String getBelDtVltNull() {
        return readString(Pos.BEL_DT_VLT_NULL, Len.BEL_DT_VLT_NULL);
    }

    public String getBelDtVltNullFormatted() {
        return Functions.padBlanks(getBelDtVltNull(), Len.BEL_DT_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_DT_VLT = 1;
        public static final int BEL_DT_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_DT_VLT = 5;
        public static final int BEL_DT_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_DT_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
