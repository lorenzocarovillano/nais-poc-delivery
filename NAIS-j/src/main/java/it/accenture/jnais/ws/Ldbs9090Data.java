package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IGarStatOggBus;
import it.accenture.jnais.copy.GarDb;
import it.accenture.jnais.copy.GarIvvs0216;
import it.accenture.jnais.copy.Idbvstb3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndGar;
import it.accenture.jnais.copy.StatOggBus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS9090<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs9090Data implements IGarStatOggBus {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-GAR
    private IndGar indGar = new IndGar();
    //Original name: IND-STB-ID-MOVI-CHIU
    private short indStbIdMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: GAR-DB
    private GarDb garDb = new GarDb();
    //Original name: IDBVSTB3
    private Idbvstb3 idbvstb3 = new Idbvstb3();
    //Original name: GAR
    private GarIvvs0216 gar = new GarIvvs0216();
    //Original name: STAT-OGG-BUS
    private StatOggBus statOggBus = new StatOggBus();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndStbIdMoviChiu(short indStbIdMoviChiu) {
        this.indStbIdMoviChiu = indStbIdMoviChiu;
    }

    public short getIndStbIdMoviChiu() {
        return this.indStbIdMoviChiu;
    }

    @Override
    public int getAaPagPreUni() {
        return gar.getGrzAaPagPreUni().getGrzAaPagPreUni();
    }

    @Override
    public void setAaPagPreUni(int aaPagPreUni) {
        this.gar.getGrzAaPagPreUni().setGrzAaPagPreUni(aaPagPreUni);
    }

    @Override
    public Integer getAaPagPreUniObj() {
        if (indGar.getAaPagPreUni() >= 0) {
            return ((Integer)getAaPagPreUni());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaPagPreUniObj(Integer aaPagPreUniObj) {
        if (aaPagPreUniObj != null) {
            setAaPagPreUni(((int)aaPagPreUniObj));
            indGar.setAaPagPreUni(((short)0));
        }
        else {
            indGar.setAaPagPreUni(((short)-1));
        }
    }

    @Override
    public String getAaRenCer() {
        return gar.getGrzAaRenCer();
    }

    @Override
    public void setAaRenCer(String aaRenCer) {
        this.gar.setGrzAaRenCer(aaRenCer);
    }

    @Override
    public String getAaRenCerObj() {
        if (indGar.getAaRenCer() >= 0) {
            return getAaRenCer();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaRenCerObj(String aaRenCerObj) {
        if (aaRenCerObj != null) {
            setAaRenCer(aaRenCerObj);
            indGar.setAaRenCer(((short)0));
        }
        else {
            indGar.setAaRenCer(((short)-1));
        }
    }

    @Override
    public int getAaStab() {
        return gar.getGrzAaStab().getGrzAaStab();
    }

    @Override
    public void setAaStab(int aaStab) {
        this.gar.getGrzAaStab().setGrzAaStab(aaStab);
    }

    @Override
    public Integer getAaStabObj() {
        if (indGar.getAaStab() >= 0) {
            return ((Integer)getAaStab());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaStabObj(Integer aaStabObj) {
        if (aaStabObj != null) {
            setAaStab(((int)aaStabObj));
            indGar.setAaStab(((short)0));
        }
        else {
            indGar.setAaStab(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return gar.getGrzCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.gar.setGrzCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodFnd() {
        return gar.getGrzCodFnd();
    }

    @Override
    public void setCodFnd(String codFnd) {
        this.gar.setGrzCodFnd(codFnd);
    }

    @Override
    public String getCodFndObj() {
        if (indGar.getCodFnd() >= 0) {
            return getCodFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFndObj(String codFndObj) {
        if (codFndObj != null) {
            setCodFnd(codFndObj);
            indGar.setCodFnd(((short)0));
        }
        else {
            indGar.setCodFnd(((short)-1));
        }
    }

    @Override
    public String getCodSez() {
        return gar.getGrzCodSez();
    }

    @Override
    public void setCodSez(String codSez) {
        this.gar.setGrzCodSez(codSez);
    }

    @Override
    public String getCodSezObj() {
        if (indGar.getCodSez() >= 0) {
            return getCodSez();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodSezObj(String codSezObj) {
        if (codSezObj != null) {
            setCodSez(codSezObj);
            indGar.setCodSez(((short)0));
        }
        else {
            indGar.setCodSez(((short)-1));
        }
    }

    @Override
    public String getCodTratRiass() {
        return gar.getGrzCodTratRiass();
    }

    @Override
    public void setCodTratRiass(String codTratRiass) {
        this.gar.setGrzCodTratRiass(codTratRiass);
    }

    @Override
    public String getCodTratRiassObj() {
        if (indGar.getCodTratRiass() >= 0) {
            return getCodTratRiass();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTratRiassObj(String codTratRiassObj) {
        if (codTratRiassObj != null) {
            setCodTratRiass(codTratRiassObj);
            indGar.setCodTratRiass(((short)0));
        }
        else {
            indGar.setCodTratRiass(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return gar.getGrzDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.gar.setGrzDsOperSql(dsOperSql);
    }

    @Override
    public long getDsRiga() {
        return gar.getGrzDsRiga();
    }

    @Override
    public void setDsRiga(long dsRiga) {
        this.gar.setGrzDsRiga(dsRiga);
    }

    @Override
    public char getDsStatoElab() {
        return gar.getGrzDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.gar.setGrzDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return gar.getGrzDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.gar.setGrzDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return gar.getGrzDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.gar.setGrzDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return gar.getGrzDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.gar.setGrzDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return gar.getGrzDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.gar.setGrzDsVer(dsVer);
    }

    @Override
    public String getDtDecorDb() {
        return garDb.getDecorDb();
    }

    @Override
    public void setDtDecorDb(String dtDecorDb) {
        this.garDb.setDecorDb(dtDecorDb);
    }

    @Override
    public String getDtDecorDbObj() {
        if (indGar.getDtDecor() >= 0) {
            return getDtDecorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorDbObj(String dtDecorDbObj) {
        if (dtDecorDbObj != null) {
            setDtDecorDb(dtDecorDbObj);
            indGar.setDtDecor(((short)0));
        }
        else {
            indGar.setDtDecor(((short)-1));
        }
    }

    @Override
    public String getDtEndCarzDb() {
        return garDb.getEndCarzDb();
    }

    @Override
    public void setDtEndCarzDb(String dtEndCarzDb) {
        this.garDb.setEndCarzDb(dtEndCarzDb);
    }

    @Override
    public String getDtEndCarzDbObj() {
        if (indGar.getDtEndCarz() >= 0) {
            return getDtEndCarzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndCarzDbObj(String dtEndCarzDbObj) {
        if (dtEndCarzDbObj != null) {
            setDtEndCarzDb(dtEndCarzDbObj);
            indGar.setDtEndCarz(((short)0));
        }
        else {
            indGar.setDtEndCarz(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return garDb.getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.garDb.setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return garDb.getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.garDb.setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtIniValTarDb() {
        return garDb.getIniValTarDb();
    }

    @Override
    public void setDtIniValTarDb(String dtIniValTarDb) {
        this.garDb.setIniValTarDb(dtIniValTarDb);
    }

    @Override
    public String getDtIniValTarDbObj() {
        if (indGar.getDtIniValTar() >= 0) {
            return getDtIniValTarDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniValTarDbObj(String dtIniValTarDbObj) {
        if (dtIniValTarDbObj != null) {
            setDtIniValTarDb(dtIniValTarDbObj);
            indGar.setDtIniValTar(((short)0));
        }
        else {
            indGar.setDtIniValTar(((short)-1));
        }
    }

    @Override
    public String getDtPrescDb() {
        return garDb.getPrescDb();
    }

    @Override
    public void setDtPrescDb(String dtPrescDb) {
        this.garDb.setPrescDb(dtPrescDb);
    }

    @Override
    public String getDtPrescDbObj() {
        if (indGar.getDtPresc() >= 0) {
            return getDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtPrescDbObj(String dtPrescDbObj) {
        if (dtPrescDbObj != null) {
            setDtPrescDb(dtPrescDbObj);
            indGar.setDtPresc(((short)0));
        }
        else {
            indGar.setDtPresc(((short)-1));
        }
    }

    @Override
    public String getDtScadDb() {
        return garDb.getScadDb();
    }

    @Override
    public void setDtScadDb(String dtScadDb) {
        this.garDb.setScadDb(dtScadDb);
    }

    @Override
    public String getDtScadDbObj() {
        if (indGar.getDtScad() >= 0) {
            return getDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadDbObj(String dtScadDbObj) {
        if (dtScadDbObj != null) {
            setDtScadDb(dtScadDbObj);
            indGar.setDtScad(((short)0));
        }
        else {
            indGar.setDtScad(((short)-1));
        }
    }

    @Override
    public String getDtVarzTpIasDb() {
        return garDb.getVarzTpIasDb();
    }

    @Override
    public void setDtVarzTpIasDb(String dtVarzTpIasDb) {
        this.garDb.setVarzTpIasDb(dtVarzTpIasDb);
    }

    @Override
    public String getDtVarzTpIasDbObj() {
        if (indGar.getDtVarzTpIas() >= 0) {
            return getDtVarzTpIasDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtVarzTpIasDbObj(String dtVarzTpIasDbObj) {
        if (dtVarzTpIasDbObj != null) {
            setDtVarzTpIasDb(dtVarzTpIasDbObj);
            indGar.setDtVarzTpIas(((short)0));
        }
        else {
            indGar.setDtVarzTpIas(((short)-1));
        }
    }

    @Override
    public int getDurAa() {
        return gar.getGrzDurAa().getGrzDurAa();
    }

    @Override
    public void setDurAa(int durAa) {
        this.gar.getGrzDurAa().setGrzDurAa(durAa);
    }

    @Override
    public Integer getDurAaObj() {
        if (indGar.getDurAa() >= 0) {
            return ((Integer)getDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        if (durAaObj != null) {
            setDurAa(((int)durAaObj));
            indGar.setDurAa(((short)0));
        }
        else {
            indGar.setDurAa(((short)-1));
        }
    }

    @Override
    public int getDurGg() {
        return gar.getGrzDurGg().getGrzDurGg();
    }

    @Override
    public void setDurGg(int durGg) {
        this.gar.getGrzDurGg().setGrzDurGg(durGg);
    }

    @Override
    public Integer getDurGgObj() {
        if (indGar.getDurGg() >= 0) {
            return ((Integer)getDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        if (durGgObj != null) {
            setDurGg(((int)durGgObj));
            indGar.setDurGg(((short)0));
        }
        else {
            indGar.setDurGg(((short)-1));
        }
    }

    @Override
    public int getDurMm() {
        return gar.getGrzDurMm().getGrzDurMm();
    }

    @Override
    public void setDurMm(int durMm) {
        this.gar.getGrzDurMm().setGrzDurMm(durMm);
    }

    @Override
    public Integer getDurMmObj() {
        if (indGar.getDurMm() >= 0) {
            return ((Integer)getDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        if (durMmObj != null) {
            setDurMm(((int)durMmObj));
            indGar.setDurMm(((short)0));
        }
        else {
            indGar.setDurMm(((short)-1));
        }
    }

    @Override
    public int getEtaAScad() {
        return gar.getGrzEtaAScad().getGrzEtaAScad();
    }

    @Override
    public void setEtaAScad(int etaAScad) {
        this.gar.getGrzEtaAScad().setGrzEtaAScad(etaAScad);
    }

    @Override
    public Integer getEtaAScadObj() {
        if (indGar.getEtaAScad() >= 0) {
            return ((Integer)getEtaAScad());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAScadObj(Integer etaAScadObj) {
        if (etaAScadObj != null) {
            setEtaAScad(((int)etaAScadObj));
            indGar.setEtaAScad(((short)0));
        }
        else {
            indGar.setEtaAScad(((short)-1));
        }
    }

    @Override
    public short getEtaAa1oAssto() {
        return gar.getGrzEtaAa1oAssto().getGrzEtaAa1oAssto();
    }

    @Override
    public void setEtaAa1oAssto(short etaAa1oAssto) {
        this.gar.getGrzEtaAa1oAssto().setGrzEtaAa1oAssto(etaAa1oAssto);
    }

    @Override
    public Short getEtaAa1oAsstoObj() {
        if (indGar.getEtaAa1oAssto() >= 0) {
            return ((Short)getEtaAa1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa1oAsstoObj(Short etaAa1oAsstoObj) {
        if (etaAa1oAsstoObj != null) {
            setEtaAa1oAssto(((short)etaAa1oAsstoObj));
            indGar.setEtaAa1oAssto(((short)0));
        }
        else {
            indGar.setEtaAa1oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaAa2oAssto() {
        return gar.getGrzEtaAa2oAssto().getGrzEtaAa2oAssto();
    }

    @Override
    public void setEtaAa2oAssto(short etaAa2oAssto) {
        this.gar.getGrzEtaAa2oAssto().setGrzEtaAa2oAssto(etaAa2oAssto);
    }

    @Override
    public Short getEtaAa2oAsstoObj() {
        if (indGar.getEtaAa2oAssto() >= 0) {
            return ((Short)getEtaAa2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa2oAsstoObj(Short etaAa2oAsstoObj) {
        if (etaAa2oAsstoObj != null) {
            setEtaAa2oAssto(((short)etaAa2oAsstoObj));
            indGar.setEtaAa2oAssto(((short)0));
        }
        else {
            indGar.setEtaAa2oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaAa3oAssto() {
        return gar.getGrzEtaAa3oAssto().getGrzEtaAa3oAssto();
    }

    @Override
    public void setEtaAa3oAssto(short etaAa3oAssto) {
        this.gar.getGrzEtaAa3oAssto().setGrzEtaAa3oAssto(etaAa3oAssto);
    }

    @Override
    public Short getEtaAa3oAsstoObj() {
        if (indGar.getEtaAa3oAssto() >= 0) {
            return ((Short)getEtaAa3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa3oAsstoObj(Short etaAa3oAsstoObj) {
        if (etaAa3oAsstoObj != null) {
            setEtaAa3oAssto(((short)etaAa3oAsstoObj));
            indGar.setEtaAa3oAssto(((short)0));
        }
        else {
            indGar.setEtaAa3oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm1oAssto() {
        return gar.getGrzEtaMm1oAssto().getGrzEtaMm1oAssto();
    }

    @Override
    public void setEtaMm1oAssto(short etaMm1oAssto) {
        this.gar.getGrzEtaMm1oAssto().setGrzEtaMm1oAssto(etaMm1oAssto);
    }

    @Override
    public Short getEtaMm1oAsstoObj() {
        if (indGar.getEtaMm1oAssto() >= 0) {
            return ((Short)getEtaMm1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm1oAsstoObj(Short etaMm1oAsstoObj) {
        if (etaMm1oAsstoObj != null) {
            setEtaMm1oAssto(((short)etaMm1oAsstoObj));
            indGar.setEtaMm1oAssto(((short)0));
        }
        else {
            indGar.setEtaMm1oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm2oAssto() {
        return gar.getGrzEtaMm2oAssto().getGrzEtaMm2oAssto();
    }

    @Override
    public void setEtaMm2oAssto(short etaMm2oAssto) {
        this.gar.getGrzEtaMm2oAssto().setGrzEtaMm2oAssto(etaMm2oAssto);
    }

    @Override
    public Short getEtaMm2oAsstoObj() {
        if (indGar.getEtaMm2oAssto() >= 0) {
            return ((Short)getEtaMm2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm2oAsstoObj(Short etaMm2oAsstoObj) {
        if (etaMm2oAsstoObj != null) {
            setEtaMm2oAssto(((short)etaMm2oAsstoObj));
            indGar.setEtaMm2oAssto(((short)0));
        }
        else {
            indGar.setEtaMm2oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm3oAssto() {
        return gar.getGrzEtaMm3oAssto().getGrzEtaMm3oAssto();
    }

    @Override
    public void setEtaMm3oAssto(short etaMm3oAssto) {
        this.gar.getGrzEtaMm3oAssto().setGrzEtaMm3oAssto(etaMm3oAssto);
    }

    @Override
    public Short getEtaMm3oAsstoObj() {
        if (indGar.getEtaMm3oAssto() >= 0) {
            return ((Short)getEtaMm3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm3oAsstoObj(Short etaMm3oAsstoObj) {
        if (etaMm3oAsstoObj != null) {
            setEtaMm3oAssto(((short)etaMm3oAsstoObj));
            indGar.setEtaMm3oAssto(((short)0));
        }
        else {
            indGar.setEtaMm3oAssto(((short)-1));
        }
    }

    @Override
    public int getFrazDecrCpt() {
        return gar.getGrzFrazDecrCpt().getGrzFrazDecrCpt();
    }

    @Override
    public void setFrazDecrCpt(int frazDecrCpt) {
        this.gar.getGrzFrazDecrCpt().setGrzFrazDecrCpt(frazDecrCpt);
    }

    @Override
    public Integer getFrazDecrCptObj() {
        if (indGar.getFrazDecrCpt() >= 0) {
            return ((Integer)getFrazDecrCpt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazDecrCptObj(Integer frazDecrCptObj) {
        if (frazDecrCptObj != null) {
            setFrazDecrCpt(((int)frazDecrCptObj));
            indGar.setFrazDecrCpt(((short)0));
        }
        else {
            indGar.setFrazDecrCpt(((short)-1));
        }
    }

    @Override
    public int getFrazIniErogRen() {
        return gar.getGrzFrazIniErogRen().getGrzFrazIniErogRen();
    }

    @Override
    public void setFrazIniErogRen(int frazIniErogRen) {
        this.gar.getGrzFrazIniErogRen().setGrzFrazIniErogRen(frazIniErogRen);
    }

    @Override
    public Integer getFrazIniErogRenObj() {
        if (indGar.getFrazIniErogRen() >= 0) {
            return ((Integer)getFrazIniErogRen());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazIniErogRenObj(Integer frazIniErogRenObj) {
        if (frazIniErogRenObj != null) {
            setFrazIniErogRen(((int)frazIniErogRenObj));
            indGar.setFrazIniErogRen(((short)0));
        }
        else {
            indGar.setFrazIniErogRen(((short)-1));
        }
    }

    public GarIvvs0216 getGar() {
        return gar;
    }

    public GarDb getGarDb() {
        return garDb;
    }

    @Override
    public String getGrzCodTari() {
        return gar.getGrzCodTari();
    }

    @Override
    public void setGrzCodTari(String grzCodTari) {
        this.gar.setGrzCodTari(grzCodTari);
    }

    @Override
    public int getGrzIdPoli() {
        return gar.getGrzIdPoli();
    }

    @Override
    public void setGrzIdPoli(int grzIdPoli) {
        this.gar.setGrzIdPoli(grzIdPoli);
    }

    @Override
    public String getIbOgg() {
        return gar.getGrzIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.gar.setGrzIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (indGar.getIbOgg() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            indGar.setIbOgg(((short)0));
        }
        else {
            indGar.setIbOgg(((short)-1));
        }
    }

    @Override
    public int getId1oAssto() {
        return gar.getGrzId1oAssto().getGrzId1oAssto();
    }

    @Override
    public void setId1oAssto(int id1oAssto) {
        this.gar.getGrzId1oAssto().setGrzId1oAssto(id1oAssto);
    }

    @Override
    public Integer getId1oAsstoObj() {
        if (indGar.getId1oAssto() >= 0) {
            return ((Integer)getId1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setId1oAsstoObj(Integer id1oAsstoObj) {
        if (id1oAsstoObj != null) {
            setId1oAssto(((int)id1oAsstoObj));
            indGar.setId1oAssto(((short)0));
        }
        else {
            indGar.setId1oAssto(((short)-1));
        }
    }

    @Override
    public int getId2oAssto() {
        return gar.getGrzId2oAssto().getGrzId2oAssto();
    }

    @Override
    public void setId2oAssto(int id2oAssto) {
        this.gar.getGrzId2oAssto().setGrzId2oAssto(id2oAssto);
    }

    @Override
    public Integer getId2oAsstoObj() {
        if (indGar.getId2oAssto() >= 0) {
            return ((Integer)getId2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setId2oAsstoObj(Integer id2oAsstoObj) {
        if (id2oAsstoObj != null) {
            setId2oAssto(((int)id2oAsstoObj));
            indGar.setId2oAssto(((short)0));
        }
        else {
            indGar.setId2oAssto(((short)-1));
        }
    }

    @Override
    public int getId3oAssto() {
        return gar.getGrzId3oAssto().getGrzId3oAssto();
    }

    @Override
    public void setId3oAssto(int id3oAssto) {
        this.gar.getGrzId3oAssto().setGrzId3oAssto(id3oAssto);
    }

    @Override
    public Integer getId3oAsstoObj() {
        if (indGar.getId3oAssto() >= 0) {
            return ((Integer)getId3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setId3oAsstoObj(Integer id3oAsstoObj) {
        if (id3oAsstoObj != null) {
            setId3oAssto(((int)id3oAsstoObj));
            indGar.setId3oAssto(((short)0));
        }
        else {
            indGar.setId3oAssto(((short)-1));
        }
    }

    @Override
    public int getIdAdes() {
        return gar.getGrzIdAdes().getGrzIdAdes();
    }

    @Override
    public void setIdAdes(int idAdes) {
        this.gar.getGrzIdAdes().setGrzIdAdes(idAdes);
    }

    @Override
    public Integer getIdAdesObj() {
        if (indGar.getIdAdes() >= 0) {
            return ((Integer)getIdAdes());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdAdesObj(Integer idAdesObj) {
        if (idAdesObj != null) {
            setIdAdes(((int)idAdesObj));
            indGar.setIdAdes(((short)0));
        }
        else {
            indGar.setIdAdes(((short)-1));
        }
    }

    @Override
    public int getIdGar() {
        return gar.getGrzIdGar();
    }

    @Override
    public void setIdGar(int idGar) {
        this.gar.setGrzIdGar(idGar);
    }

    @Override
    public int getIdMoviChiu() {
        return gar.getGrzIdMoviChiu().getGrzIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.gar.getGrzIdMoviChiu().setGrzIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (indGar.getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            indGar.setIdMoviChiu(((short)0));
        }
        else {
            indGar.setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return gar.getGrzIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.gar.setGrzIdMoviCrz(idMoviCrz);
    }

    public Idbvstb3 getIdbvstb3() {
        return idbvstb3;
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndGar getIndGar() {
        return indGar;
    }

    @Override
    public String getLdbv1351TpCaus01() {
        throw new FieldNotMappedException("ldbv1351TpCaus01");
    }

    @Override
    public void setLdbv1351TpCaus01(String ldbv1351TpCaus01) {
        throw new FieldNotMappedException("ldbv1351TpCaus01");
    }

    @Override
    public String getLdbv1351TpCaus02() {
        throw new FieldNotMappedException("ldbv1351TpCaus02");
    }

    @Override
    public void setLdbv1351TpCaus02(String ldbv1351TpCaus02) {
        throw new FieldNotMappedException("ldbv1351TpCaus02");
    }

    @Override
    public String getLdbv1351TpCaus03() {
        throw new FieldNotMappedException("ldbv1351TpCaus03");
    }

    @Override
    public void setLdbv1351TpCaus03(String ldbv1351TpCaus03) {
        throw new FieldNotMappedException("ldbv1351TpCaus03");
    }

    @Override
    public String getLdbv1351TpCaus04() {
        throw new FieldNotMappedException("ldbv1351TpCaus04");
    }

    @Override
    public void setLdbv1351TpCaus04(String ldbv1351TpCaus04) {
        throw new FieldNotMappedException("ldbv1351TpCaus04");
    }

    @Override
    public String getLdbv1351TpCaus05() {
        throw new FieldNotMappedException("ldbv1351TpCaus05");
    }

    @Override
    public void setLdbv1351TpCaus05(String ldbv1351TpCaus05) {
        throw new FieldNotMappedException("ldbv1351TpCaus05");
    }

    @Override
    public String getLdbv1351TpCaus06() {
        throw new FieldNotMappedException("ldbv1351TpCaus06");
    }

    @Override
    public void setLdbv1351TpCaus06(String ldbv1351TpCaus06) {
        throw new FieldNotMappedException("ldbv1351TpCaus06");
    }

    @Override
    public String getLdbv1351TpCaus07() {
        throw new FieldNotMappedException("ldbv1351TpCaus07");
    }

    @Override
    public void setLdbv1351TpCaus07(String ldbv1351TpCaus07) {
        throw new FieldNotMappedException("ldbv1351TpCaus07");
    }

    @Override
    public String getLdbv1351TpStatBus01() {
        throw new FieldNotMappedException("ldbv1351TpStatBus01");
    }

    @Override
    public void setLdbv1351TpStatBus01(String ldbv1351TpStatBus01) {
        throw new FieldNotMappedException("ldbv1351TpStatBus01");
    }

    @Override
    public String getLdbv1351TpStatBus02() {
        throw new FieldNotMappedException("ldbv1351TpStatBus02");
    }

    @Override
    public void setLdbv1351TpStatBus02(String ldbv1351TpStatBus02) {
        throw new FieldNotMappedException("ldbv1351TpStatBus02");
    }

    @Override
    public String getLdbv1351TpStatBus03() {
        throw new FieldNotMappedException("ldbv1351TpStatBus03");
    }

    @Override
    public void setLdbv1351TpStatBus03(String ldbv1351TpStatBus03) {
        throw new FieldNotMappedException("ldbv1351TpStatBus03");
    }

    @Override
    public String getLdbv1351TpStatBus04() {
        throw new FieldNotMappedException("ldbv1351TpStatBus04");
    }

    @Override
    public void setLdbv1351TpStatBus04(String ldbv1351TpStatBus04) {
        throw new FieldNotMappedException("ldbv1351TpStatBus04");
    }

    @Override
    public String getLdbv1351TpStatBus05() {
        throw new FieldNotMappedException("ldbv1351TpStatBus05");
    }

    @Override
    public void setLdbv1351TpStatBus05(String ldbv1351TpStatBus05) {
        throw new FieldNotMappedException("ldbv1351TpStatBus05");
    }

    @Override
    public String getLdbv1351TpStatBus06() {
        throw new FieldNotMappedException("ldbv1351TpStatBus06");
    }

    @Override
    public void setLdbv1351TpStatBus06(String ldbv1351TpStatBus06) {
        throw new FieldNotMappedException("ldbv1351TpStatBus06");
    }

    @Override
    public String getLdbv1351TpStatBus07() {
        throw new FieldNotMappedException("ldbv1351TpStatBus07");
    }

    @Override
    public void setLdbv1351TpStatBus07(String ldbv1351TpStatBus07) {
        throw new FieldNotMappedException("ldbv1351TpStatBus07");
    }

    @Override
    public int getLdbv1421IdPoli() {
        throw new FieldNotMappedException("ldbv1421IdPoli");
    }

    @Override
    public void setLdbv1421IdPoli(int ldbv1421IdPoli) {
        throw new FieldNotMappedException("ldbv1421IdPoli");
    }

    @Override
    public String getLdbv1421RamoBila1() {
        throw new FieldNotMappedException("ldbv1421RamoBila1");
    }

    @Override
    public void setLdbv1421RamoBila1(String ldbv1421RamoBila1) {
        throw new FieldNotMappedException("ldbv1421RamoBila1");
    }

    @Override
    public String getLdbv1421RamoBila2() {
        throw new FieldNotMappedException("ldbv1421RamoBila2");
    }

    @Override
    public void setLdbv1421RamoBila2(String ldbv1421RamoBila2) {
        throw new FieldNotMappedException("ldbv1421RamoBila2");
    }

    @Override
    public String getLdbv1421RamoBila3() {
        throw new FieldNotMappedException("ldbv1421RamoBila3");
    }

    @Override
    public void setLdbv1421RamoBila3(String ldbv1421RamoBila3) {
        throw new FieldNotMappedException("ldbv1421RamoBila3");
    }

    @Override
    public String getLdbv1421RamoBila4() {
        throw new FieldNotMappedException("ldbv1421RamoBila4");
    }

    @Override
    public void setLdbv1421RamoBila4(String ldbv1421RamoBila4) {
        throw new FieldNotMappedException("ldbv1421RamoBila4");
    }

    @Override
    public String getLdbv1421RamoBila5() {
        throw new FieldNotMappedException("ldbv1421RamoBila5");
    }

    @Override
    public void setLdbv1421RamoBila5(String ldbv1421RamoBila5) {
        throw new FieldNotMappedException("ldbv1421RamoBila5");
    }

    @Override
    public int getLdbv3401IdAdes() {
        throw new FieldNotMappedException("ldbv3401IdAdes");
    }

    @Override
    public void setLdbv3401IdAdes(int ldbv3401IdAdes) {
        throw new FieldNotMappedException("ldbv3401IdAdes");
    }

    @Override
    public int getLdbv3401IdPoli() {
        throw new FieldNotMappedException("ldbv3401IdPoli");
    }

    @Override
    public void setLdbv3401IdPoli(int ldbv3401IdPoli) {
        throw new FieldNotMappedException("ldbv3401IdPoli");
    }

    @Override
    public String getLdbv3401Ramo1() {
        throw new FieldNotMappedException("ldbv3401Ramo1");
    }

    @Override
    public void setLdbv3401Ramo1(String ldbv3401Ramo1) {
        throw new FieldNotMappedException("ldbv3401Ramo1");
    }

    @Override
    public String getLdbv3401Ramo2() {
        throw new FieldNotMappedException("ldbv3401Ramo2");
    }

    @Override
    public void setLdbv3401Ramo2(String ldbv3401Ramo2) {
        throw new FieldNotMappedException("ldbv3401Ramo2");
    }

    @Override
    public String getLdbv3401Ramo3() {
        throw new FieldNotMappedException("ldbv3401Ramo3");
    }

    @Override
    public void setLdbv3401Ramo3(String ldbv3401Ramo3) {
        throw new FieldNotMappedException("ldbv3401Ramo3");
    }

    @Override
    public String getLdbv3401Ramo4() {
        throw new FieldNotMappedException("ldbv3401Ramo4");
    }

    @Override
    public void setLdbv3401Ramo4(String ldbv3401Ramo4) {
        throw new FieldNotMappedException("ldbv3401Ramo4");
    }

    @Override
    public String getLdbv3401Ramo5() {
        throw new FieldNotMappedException("ldbv3401Ramo5");
    }

    @Override
    public void setLdbv3401Ramo5(String ldbv3401Ramo5) {
        throw new FieldNotMappedException("ldbv3401Ramo5");
    }

    @Override
    public String getLdbv3401Ramo6() {
        throw new FieldNotMappedException("ldbv3401Ramo6");
    }

    @Override
    public void setLdbv3401Ramo6(String ldbv3401Ramo6) {
        throw new FieldNotMappedException("ldbv3401Ramo6");
    }

    @Override
    public String getLdbv3401TpOgg() {
        throw new FieldNotMappedException("ldbv3401TpOgg");
    }

    @Override
    public void setLdbv3401TpOgg(String ldbv3401TpOgg) {
        throw new FieldNotMappedException("ldbv3401TpOgg");
    }

    @Override
    public String getLdbv3401TpStatBus1() {
        throw new FieldNotMappedException("ldbv3401TpStatBus1");
    }

    @Override
    public void setLdbv3401TpStatBus1(String ldbv3401TpStatBus1) {
        throw new FieldNotMappedException("ldbv3401TpStatBus1");
    }

    @Override
    public String getLdbv3401TpStatBus2() {
        throw new FieldNotMappedException("ldbv3401TpStatBus2");
    }

    @Override
    public void setLdbv3401TpStatBus2(String ldbv3401TpStatBus2) {
        throw new FieldNotMappedException("ldbv3401TpStatBus2");
    }

    @Override
    public int getLdbv3411IdAdes() {
        throw new FieldNotMappedException("ldbv3411IdAdes");
    }

    @Override
    public void setLdbv3411IdAdes(int ldbv3411IdAdes) {
        throw new FieldNotMappedException("ldbv3411IdAdes");
    }

    @Override
    public int getLdbv3411IdPoli() {
        throw new FieldNotMappedException("ldbv3411IdPoli");
    }

    @Override
    public void setLdbv3411IdPoli(int ldbv3411IdPoli) {
        throw new FieldNotMappedException("ldbv3411IdPoli");
    }

    @Override
    public String getLdbv3411TpOgg() {
        throw new FieldNotMappedException("ldbv3411TpOgg");
    }

    @Override
    public void setLdbv3411TpOgg(String ldbv3411TpOgg) {
        throw new FieldNotMappedException("ldbv3411TpOgg");
    }

    @Override
    public String getLdbv3411TpStatBus1() {
        throw new FieldNotMappedException("ldbv3411TpStatBus1");
    }

    @Override
    public void setLdbv3411TpStatBus1(String ldbv3411TpStatBus1) {
        throw new FieldNotMappedException("ldbv3411TpStatBus1");
    }

    @Override
    public String getLdbv3411TpStatBus2() {
        throw new FieldNotMappedException("ldbv3411TpStatBus2");
    }

    @Override
    public void setLdbv3411TpStatBus2(String ldbv3411TpStatBus2) {
        throw new FieldNotMappedException("ldbv3411TpStatBus2");
    }

    @Override
    public int getLdbv9091IdAdes() {
        throw new FieldNotMappedException("ldbv9091IdAdes");
    }

    @Override
    public void setLdbv9091IdAdes(int ldbv9091IdAdes) {
        throw new FieldNotMappedException("ldbv9091IdAdes");
    }

    @Override
    public int getLdbv9091IdPoli() {
        throw new FieldNotMappedException("ldbv9091IdPoli");
    }

    @Override
    public void setLdbv9091IdPoli(int ldbv9091IdPoli) {
        throw new FieldNotMappedException("ldbv9091IdPoli");
    }

    @Override
    public String getLdbv9091Ramo1() {
        throw new FieldNotMappedException("ldbv9091Ramo1");
    }

    @Override
    public void setLdbv9091Ramo1(String ldbv9091Ramo1) {
        throw new FieldNotMappedException("ldbv9091Ramo1");
    }

    @Override
    public String getLdbv9091Ramo2() {
        throw new FieldNotMappedException("ldbv9091Ramo2");
    }

    @Override
    public void setLdbv9091Ramo2(String ldbv9091Ramo2) {
        throw new FieldNotMappedException("ldbv9091Ramo2");
    }

    @Override
    public String getLdbv9091Ramo3() {
        throw new FieldNotMappedException("ldbv9091Ramo3");
    }

    @Override
    public void setLdbv9091Ramo3(String ldbv9091Ramo3) {
        throw new FieldNotMappedException("ldbv9091Ramo3");
    }

    @Override
    public String getLdbv9091Ramo4() {
        throw new FieldNotMappedException("ldbv9091Ramo4");
    }

    @Override
    public void setLdbv9091Ramo4(String ldbv9091Ramo4) {
        throw new FieldNotMappedException("ldbv9091Ramo4");
    }

    @Override
    public String getLdbv9091Ramo5() {
        throw new FieldNotMappedException("ldbv9091Ramo5");
    }

    @Override
    public void setLdbv9091Ramo5(String ldbv9091Ramo5) {
        throw new FieldNotMappedException("ldbv9091Ramo5");
    }

    @Override
    public String getLdbv9091Ramo6() {
        throw new FieldNotMappedException("ldbv9091Ramo6");
    }

    @Override
    public void setLdbv9091Ramo6(String ldbv9091Ramo6) {
        throw new FieldNotMappedException("ldbv9091Ramo6");
    }

    @Override
    public short getLdbv9091TpInvst1() {
        throw new FieldNotMappedException("ldbv9091TpInvst1");
    }

    @Override
    public void setLdbv9091TpInvst1(short ldbv9091TpInvst1) {
        throw new FieldNotMappedException("ldbv9091TpInvst1");
    }

    @Override
    public short getLdbv9091TpInvst2() {
        throw new FieldNotMappedException("ldbv9091TpInvst2");
    }

    @Override
    public void setLdbv9091TpInvst2(short ldbv9091TpInvst2) {
        throw new FieldNotMappedException("ldbv9091TpInvst2");
    }

    @Override
    public short getLdbv9091TpInvst3() {
        throw new FieldNotMappedException("ldbv9091TpInvst3");
    }

    @Override
    public void setLdbv9091TpInvst3(short ldbv9091TpInvst3) {
        throw new FieldNotMappedException("ldbv9091TpInvst3");
    }

    @Override
    public short getLdbv9091TpInvst4() {
        throw new FieldNotMappedException("ldbv9091TpInvst4");
    }

    @Override
    public void setLdbv9091TpInvst4(short ldbv9091TpInvst4) {
        throw new FieldNotMappedException("ldbv9091TpInvst4");
    }

    @Override
    public short getLdbv9091TpInvst5() {
        throw new FieldNotMappedException("ldbv9091TpInvst5");
    }

    @Override
    public void setLdbv9091TpInvst5(short ldbv9091TpInvst5) {
        throw new FieldNotMappedException("ldbv9091TpInvst5");
    }

    @Override
    public short getLdbv9091TpInvst6() {
        throw new FieldNotMappedException("ldbv9091TpInvst6");
    }

    @Override
    public void setLdbv9091TpInvst6(short ldbv9091TpInvst6) {
        throw new FieldNotMappedException("ldbv9091TpInvst6");
    }

    @Override
    public String getLdbv9091TpOgg() {
        throw new FieldNotMappedException("ldbv9091TpOgg");
    }

    @Override
    public void setLdbv9091TpOgg(String ldbv9091TpOgg) {
        throw new FieldNotMappedException("ldbv9091TpOgg");
    }

    @Override
    public String getLdbv9091TpStatBus1() {
        throw new FieldNotMappedException("ldbv9091TpStatBus1");
    }

    @Override
    public void setLdbv9091TpStatBus1(String ldbv9091TpStatBus1) {
        throw new FieldNotMappedException("ldbv9091TpStatBus1");
    }

    @Override
    public String getLdbv9091TpStatBus2() {
        throw new FieldNotMappedException("ldbv9091TpStatBus2");
    }

    @Override
    public void setLdbv9091TpStatBus2(String ldbv9091TpStatBus2) {
        throw new FieldNotMappedException("ldbv9091TpStatBus2");
    }

    @Override
    public short getMm1oRat() {
        return gar.getGrzMm1oRat().getGrzMm1oRat();
    }

    @Override
    public void setMm1oRat(short mm1oRat) {
        this.gar.getGrzMm1oRat().setGrzMm1oRat(mm1oRat);
    }

    @Override
    public Short getMm1oRatObj() {
        if (indGar.getMm1oRat() >= 0) {
            return ((Short)getMm1oRat());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMm1oRatObj(Short mm1oRatObj) {
        if (mm1oRatObj != null) {
            setMm1oRat(((short)mm1oRatObj));
            indGar.setMm1oRat(((short)0));
        }
        else {
            indGar.setMm1oRat(((short)-1));
        }
    }

    @Override
    public int getMmPagPreUni() {
        return gar.getGrzMmPagPreUni().getGrzMmPagPreUni();
    }

    @Override
    public void setMmPagPreUni(int mmPagPreUni) {
        this.gar.getGrzMmPagPreUni().setGrzMmPagPreUni(mmPagPreUni);
    }

    @Override
    public Integer getMmPagPreUniObj() {
        if (indGar.getMmPagPreUni() >= 0) {
            return ((Integer)getMmPagPreUni());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMmPagPreUniObj(Integer mmPagPreUniObj) {
        if (mmPagPreUniObj != null) {
            setMmPagPreUni(((int)mmPagPreUniObj));
            indGar.setMmPagPreUni(((short)0));
        }
        else {
            indGar.setMmPagPreUni(((short)-1));
        }
    }

    @Override
    public String getModPagGarcol() {
        return gar.getGrzModPagGarcol();
    }

    @Override
    public void setModPagGarcol(String modPagGarcol) {
        this.gar.setGrzModPagGarcol(modPagGarcol);
    }

    @Override
    public String getModPagGarcolObj() {
        if (indGar.getModPagGarcol() >= 0) {
            return getModPagGarcol();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModPagGarcolObj(String modPagGarcolObj) {
        if (modPagGarcolObj != null) {
            setModPagGarcol(modPagGarcolObj);
            indGar.setModPagGarcol(((short)0));
        }
        else {
            indGar.setModPagGarcol(((short)-1));
        }
    }

    @Override
    public int getNumAaPagPre() {
        return gar.getGrzNumAaPagPre().getGrzNumAaPagPre();
    }

    @Override
    public void setNumAaPagPre(int numAaPagPre) {
        this.gar.getGrzNumAaPagPre().setGrzNumAaPagPre(numAaPagPre);
    }

    @Override
    public Integer getNumAaPagPreObj() {
        if (indGar.getNumAaPagPre() >= 0) {
            return ((Integer)getNumAaPagPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumAaPagPreObj(Integer numAaPagPreObj) {
        if (numAaPagPreObj != null) {
            setNumAaPagPre(((int)numAaPagPreObj));
            indGar.setNumAaPagPre(((short)0));
        }
        else {
            indGar.setNumAaPagPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getPc1oRat() {
        return gar.getGrzPc1oRat().getGrzPc1oRat();
    }

    @Override
    public void setPc1oRat(AfDecimal pc1oRat) {
        this.gar.getGrzPc1oRat().setGrzPc1oRat(pc1oRat.copy());
    }

    @Override
    public AfDecimal getPc1oRatObj() {
        if (indGar.getPc1oRat() >= 0) {
            return getPc1oRat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPc1oRatObj(AfDecimal pc1oRatObj) {
        if (pc1oRatObj != null) {
            setPc1oRat(new AfDecimal(pc1oRatObj, 6, 3));
            indGar.setPc1oRat(((short)0));
        }
        else {
            indGar.setPc1oRat(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcOpz() {
        return gar.getGrzPcOpz().getGrzPcOpz();
    }

    @Override
    public void setPcOpz(AfDecimal pcOpz) {
        this.gar.getGrzPcOpz().setGrzPcOpz(pcOpz.copy());
    }

    @Override
    public AfDecimal getPcOpzObj() {
        if (indGar.getPcOpz() >= 0) {
            return getPcOpz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcOpzObj(AfDecimal pcOpzObj) {
        if (pcOpzObj != null) {
            setPcOpz(new AfDecimal(pcOpzObj, 6, 3));
            indGar.setPcOpz(((short)0));
        }
        else {
            indGar.setPcOpz(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRevrsb() {
        return gar.getGrzPcRevrsb().getGrzPcRevrsb();
    }

    @Override
    public void setPcRevrsb(AfDecimal pcRevrsb) {
        this.gar.getGrzPcRevrsb().setGrzPcRevrsb(pcRevrsb.copy());
    }

    @Override
    public AfDecimal getPcRevrsbObj() {
        if (indGar.getPcRevrsb() >= 0) {
            return getPcRevrsb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRevrsbObj(AfDecimal pcRevrsbObj) {
        if (pcRevrsbObj != null) {
            setPcRevrsb(new AfDecimal(pcRevrsbObj, 6, 3));
            indGar.setPcRevrsb(((short)0));
        }
        else {
            indGar.setPcRevrsb(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRipPre() {
        return gar.getGrzPcRipPre().getGrzPcRipPre();
    }

    @Override
    public void setPcRipPre(AfDecimal pcRipPre) {
        this.gar.getGrzPcRipPre().setGrzPcRipPre(pcRipPre.copy());
    }

    @Override
    public AfDecimal getPcRipPreObj() {
        if (indGar.getPcRipPre() >= 0) {
            return getPcRipPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRipPreObj(AfDecimal pcRipPreObj) {
        if (pcRipPreObj != null) {
            setPcRipPre(new AfDecimal(pcRipPreObj, 6, 3));
            indGar.setPcRipPre(((short)0));
        }
        else {
            indGar.setPcRipPre(((short)-1));
        }
    }

    @Override
    public String getRamoBila() {
        return gar.getGrzRamoBila();
    }

    @Override
    public void setRamoBila(String ramoBila) {
        this.gar.setGrzRamoBila(ramoBila);
    }

    @Override
    public String getRamoBilaObj() {
        if (indGar.getRamoBila() >= 0) {
            return getRamoBila();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRamoBilaObj(String ramoBilaObj) {
        if (ramoBilaObj != null) {
            setRamoBila(ramoBilaObj);
            indGar.setRamoBila(((short)0));
        }
        else {
            indGar.setRamoBila(((short)-1));
        }
    }

    @Override
    public char getRshInvst() {
        return gar.getGrzRshInvst();
    }

    @Override
    public void setRshInvst(char rshInvst) {
        this.gar.setGrzRshInvst(rshInvst);
    }

    @Override
    public Character getRshInvstObj() {
        if (indGar.getRshInvst() >= 0) {
            return ((Character)getRshInvst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRshInvstObj(Character rshInvstObj) {
        if (rshInvstObj != null) {
            setRshInvst(((char)rshInvstObj));
            indGar.setRshInvst(((short)0));
        }
        else {
            indGar.setRshInvst(((short)-1));
        }
    }

    public StatOggBus getStatOggBus() {
        return statOggBus;
    }

    @Override
    public int getStbCodCompAnia() {
        return statOggBus.getStbCodCompAnia();
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        this.statOggBus.setStbCodCompAnia(stbCodCompAnia);
    }

    @Override
    public char getStbDsOperSql() {
        return statOggBus.getStbDsOperSql();
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        this.statOggBus.setStbDsOperSql(stbDsOperSql);
    }

    @Override
    public long getStbDsRiga() {
        return statOggBus.getStbDsRiga();
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        this.statOggBus.setStbDsRiga(stbDsRiga);
    }

    @Override
    public char getStbDsStatoElab() {
        return statOggBus.getStbDsStatoElab();
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        this.statOggBus.setStbDsStatoElab(stbDsStatoElab);
    }

    @Override
    public long getStbDsTsEndCptz() {
        return statOggBus.getStbDsTsEndCptz();
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        this.statOggBus.setStbDsTsEndCptz(stbDsTsEndCptz);
    }

    @Override
    public long getStbDsTsIniCptz() {
        return statOggBus.getStbDsTsIniCptz();
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        this.statOggBus.setStbDsTsIniCptz(stbDsTsIniCptz);
    }

    @Override
    public String getStbDsUtente() {
        return statOggBus.getStbDsUtente();
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        this.statOggBus.setStbDsUtente(stbDsUtente);
    }

    @Override
    public int getStbDsVer() {
        return statOggBus.getStbDsVer();
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        this.statOggBus.setStbDsVer(stbDsVer);
    }

    @Override
    public String getStbDtEndEffDb() {
        return idbvstb3.getStbDtEndEffDb();
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        this.idbvstb3.setStbDtEndEffDb(stbDtEndEffDb);
    }

    @Override
    public String getStbDtIniEffDb() {
        return idbvstb3.getStbDtIniEffDb();
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        this.idbvstb3.setStbDtIniEffDb(stbDtIniEffDb);
    }

    @Override
    public int getStbIdMoviChiu() {
        return statOggBus.getStbIdMoviChiu().getStbIdMoviChiu();
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        this.statOggBus.getStbIdMoviChiu().setStbIdMoviChiu(stbIdMoviChiu);
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        if (getIndStbIdMoviChiu() >= 0) {
            return ((Integer)getStbIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        if (stbIdMoviChiuObj != null) {
            setStbIdMoviChiu(((int)stbIdMoviChiuObj));
            setIndStbIdMoviChiu(((short)0));
        }
        else {
            setIndStbIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getStbIdMoviCrz() {
        return statOggBus.getStbIdMoviCrz();
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        this.statOggBus.setStbIdMoviCrz(stbIdMoviCrz);
    }

    @Override
    public int getStbIdOgg() {
        return statOggBus.getStbIdOgg();
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        this.statOggBus.setStbIdOgg(stbIdOgg);
    }

    @Override
    public int getStbIdStatOggBus() {
        return statOggBus.getStbIdStatOggBus();
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        this.statOggBus.setStbIdStatOggBus(stbIdStatOggBus);
    }

    @Override
    public String getStbTpCaus() {
        return statOggBus.getStbTpCaus();
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        this.statOggBus.setStbTpCaus(stbTpCaus);
    }

    @Override
    public String getStbTpOgg() {
        return statOggBus.getStbTpOgg();
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        this.statOggBus.setStbTpOgg(stbTpOgg);
    }

    @Override
    public String getStbTpStatBus() {
        return statOggBus.getStbTpStatBus();
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        this.statOggBus.setStbTpStatBus(stbTpStatBus);
    }

    @Override
    public char getTpAdegPre() {
        return gar.getGrzTpAdegPre();
    }

    @Override
    public void setTpAdegPre(char tpAdegPre) {
        this.gar.setGrzTpAdegPre(tpAdegPre);
    }

    @Override
    public Character getTpAdegPreObj() {
        if (indGar.getTpAdegPre() >= 0) {
            return ((Character)getTpAdegPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAdegPreObj(Character tpAdegPreObj) {
        if (tpAdegPreObj != null) {
            setTpAdegPre(((char)tpAdegPreObj));
            indGar.setTpAdegPre(((short)0));
        }
        else {
            indGar.setTpAdegPre(((short)-1));
        }
    }

    @Override
    public String getTpCalcPrePrstz() {
        return gar.getGrzTpCalcPrePrstz();
    }

    @Override
    public void setTpCalcPrePrstz(String tpCalcPrePrstz) {
        this.gar.setGrzTpCalcPrePrstz(tpCalcPrePrstz);
    }

    @Override
    public String getTpCalcPrePrstzObj() {
        if (indGar.getTpCalcPrePrstz() >= 0) {
            return getTpCalcPrePrstz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCalcPrePrstzObj(String tpCalcPrePrstzObj) {
        if (tpCalcPrePrstzObj != null) {
            setTpCalcPrePrstz(tpCalcPrePrstzObj);
            indGar.setTpCalcPrePrstz(((short)0));
        }
        else {
            indGar.setTpCalcPrePrstz(((short)-1));
        }
    }

    @Override
    public String getTpCessRiass() {
        return gar.getGrzTpCessRiass();
    }

    @Override
    public void setTpCessRiass(String tpCessRiass) {
        this.gar.setGrzTpCessRiass(tpCessRiass);
    }

    @Override
    public String getTpCessRiassObj() {
        if (indGar.getTpCessRiass() >= 0) {
            return getTpCessRiass();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCessRiassObj(String tpCessRiassObj) {
        if (tpCessRiassObj != null) {
            setTpCessRiass(tpCessRiassObj);
            indGar.setTpCessRiass(((short)0));
        }
        else {
            indGar.setTpCessRiass(((short)-1));
        }
    }

    @Override
    public String getTpDtEmisRiass() {
        return gar.getGrzTpDtEmisRiass();
    }

    @Override
    public void setTpDtEmisRiass(String tpDtEmisRiass) {
        this.gar.setGrzTpDtEmisRiass(tpDtEmisRiass);
    }

    @Override
    public String getTpDtEmisRiassObj() {
        if (indGar.getTpDtEmisRiass() >= 0) {
            return getTpDtEmisRiass();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDtEmisRiassObj(String tpDtEmisRiassObj) {
        if (tpDtEmisRiassObj != null) {
            setTpDtEmisRiass(tpDtEmisRiassObj);
            indGar.setTpDtEmisRiass(((short)0));
        }
        else {
            indGar.setTpDtEmisRiass(((short)-1));
        }
    }

    @Override
    public String getTpDur() {
        return gar.getGrzTpDur();
    }

    @Override
    public void setTpDur(String tpDur) {
        this.gar.setGrzTpDur(tpDur);
    }

    @Override
    public String getTpDurObj() {
        if (indGar.getTpDur() >= 0) {
            return getTpDur();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDurObj(String tpDurObj) {
        if (tpDurObj != null) {
            setTpDur(tpDurObj);
            indGar.setTpDur(((short)0));
        }
        else {
            indGar.setTpDur(((short)-1));
        }
    }

    @Override
    public char getTpEmisPur() {
        return gar.getGrzTpEmisPur();
    }

    @Override
    public void setTpEmisPur(char tpEmisPur) {
        this.gar.setGrzTpEmisPur(tpEmisPur);
    }

    @Override
    public Character getTpEmisPurObj() {
        if (indGar.getTpEmisPur() >= 0) {
            return ((Character)getTpEmisPur());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpEmisPurObj(Character tpEmisPurObj) {
        if (tpEmisPurObj != null) {
            setTpEmisPur(((char)tpEmisPurObj));
            indGar.setTpEmisPur(((short)0));
        }
        else {
            indGar.setTpEmisPur(((short)-1));
        }
    }

    @Override
    public short getTpGar() {
        return gar.getGrzTpGar().getGrzTpGar();
    }

    @Override
    public void setTpGar(short tpGar) {
        this.gar.getGrzTpGar().setGrzTpGar(tpGar);
    }

    @Override
    public Short getTpGarObj() {
        if (indGar.getTpGar() >= 0) {
            return ((Short)getTpGar());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpGarObj(Short tpGarObj) {
        if (tpGarObj != null) {
            setTpGar(((short)tpGarObj));
            indGar.setTpGar(((short)0));
        }
        else {
            indGar.setTpGar(((short)-1));
        }
    }

    @Override
    public String getTpIas() {
        return gar.getGrzTpIas();
    }

    @Override
    public void setTpIas(String tpIas) {
        this.gar.setGrzTpIas(tpIas);
    }

    @Override
    public String getTpIasObj() {
        if (indGar.getTpIas() >= 0) {
            return getTpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpIasObj(String tpIasObj) {
        if (tpIasObj != null) {
            setTpIas(tpIasObj);
            indGar.setTpIas(((short)0));
        }
        else {
            indGar.setTpIas(((short)-1));
        }
    }

    @Override
    public short getTpInvst() {
        return gar.getGrzTpInvst().getGrzTpInvst();
    }

    @Override
    public void setTpInvst(short tpInvst) {
        this.gar.getGrzTpInvst().setGrzTpInvst(tpInvst);
    }

    @Override
    public Short getTpInvstObj() {
        if (indGar.getTpInvst() >= 0) {
            return ((Short)getTpInvst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpInvstObj(Short tpInvstObj) {
        if (tpInvstObj != null) {
            setTpInvst(((short)tpInvstObj));
            indGar.setTpInvst(((short)0));
        }
        else {
            indGar.setTpInvst(((short)-1));
        }
    }

    @Override
    public String getTpPcRip() {
        return gar.getGrzTpPcRip();
    }

    @Override
    public void setTpPcRip(String tpPcRip) {
        this.gar.setGrzTpPcRip(tpPcRip);
    }

    @Override
    public String getTpPcRipObj() {
        if (indGar.getTpPcRip() >= 0) {
            return getTpPcRip();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPcRipObj(String tpPcRipObj) {
        if (tpPcRipObj != null) {
            setTpPcRip(tpPcRipObj);
            indGar.setTpPcRip(((short)0));
        }
        else {
            indGar.setTpPcRip(((short)-1));
        }
    }

    @Override
    public String getTpPerPre() {
        return gar.getGrzTpPerPre();
    }

    @Override
    public void setTpPerPre(String tpPerPre) {
        this.gar.setGrzTpPerPre(tpPerPre);
    }

    @Override
    public String getTpPerPreObj() {
        if (indGar.getTpPerPre() >= 0) {
            return getTpPerPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPerPreObj(String tpPerPreObj) {
        if (tpPerPreObj != null) {
            setTpPerPre(tpPerPreObj);
            indGar.setTpPerPre(((short)0));
        }
        else {
            indGar.setTpPerPre(((short)-1));
        }
    }

    @Override
    public char getTpPre() {
        return gar.getGrzTpPre();
    }

    @Override
    public void setTpPre(char tpPre) {
        this.gar.setGrzTpPre(tpPre);
    }

    @Override
    public Character getTpPreObj() {
        if (indGar.getTpPre() >= 0) {
            return ((Character)getTpPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPreObj(Character tpPreObj) {
        if (tpPreObj != null) {
            setTpPre(((char)tpPreObj));
            indGar.setTpPre(((short)0));
        }
        else {
            indGar.setTpPre(((short)-1));
        }
    }

    @Override
    public String getTpPrstzAssta() {
        return gar.getGrzTpPrstzAssta();
    }

    @Override
    public void setTpPrstzAssta(String tpPrstzAssta) {
        this.gar.setGrzTpPrstzAssta(tpPrstzAssta);
    }

    @Override
    public String getTpPrstzAsstaObj() {
        if (indGar.getTpPrstzAssta() >= 0) {
            return getTpPrstzAssta();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPrstzAsstaObj(String tpPrstzAsstaObj) {
        if (tpPrstzAsstaObj != null) {
            setTpPrstzAssta(tpPrstzAsstaObj);
            indGar.setTpPrstzAssta(((short)0));
        }
        else {
            indGar.setTpPrstzAssta(((short)-1));
        }
    }

    @Override
    public String getTpRamoBila() {
        return gar.getGrzTpRamoBila();
    }

    @Override
    public void setTpRamoBila(String tpRamoBila) {
        this.gar.setGrzTpRamoBila(tpRamoBila);
    }

    @Override
    public String getTpRsh() {
        return gar.getGrzTpRsh();
    }

    @Override
    public void setTpRsh(String tpRsh) {
        this.gar.setGrzTpRsh(tpRsh);
    }

    @Override
    public String getTpRshObj() {
        if (indGar.getTpRsh() >= 0) {
            return getTpRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRshObj(String tpRshObj) {
        if (tpRshObj != null) {
            setTpRsh(tpRshObj);
            indGar.setTpRsh(((short)0));
        }
        else {
            indGar.setTpRsh(((short)-1));
        }
    }

    @Override
    public String getTpStab() {
        return gar.getGrzTpStab();
    }

    @Override
    public void setTpStab(String tpStab) {
        this.gar.setGrzTpStab(tpStab);
    }

    @Override
    public String getTpStabObj() {
        if (indGar.getTpStab() >= 0) {
            return getTpStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStabObj(String tpStabObj) {
        if (tpStabObj != null) {
            setTpStab(tpStabObj);
            indGar.setTpStab(((short)0));
        }
        else {
            indGar.setTpStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsStabLimitata() {
        return gar.getGrzTsStabLimitata().getGrzTsStabLimitata();
    }

    @Override
    public void setTsStabLimitata(AfDecimal tsStabLimitata) {
        this.gar.getGrzTsStabLimitata().setGrzTsStabLimitata(tsStabLimitata.copy());
    }

    @Override
    public AfDecimal getTsStabLimitataObj() {
        if (indGar.getTsStabLimitata() >= 0) {
            return getTsStabLimitata();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsStabLimitataObj(AfDecimal tsStabLimitataObj) {
        if (tsStabLimitataObj != null) {
            setTsStabLimitata(new AfDecimal(tsStabLimitataObj, 14, 9));
            indGar.setTsStabLimitata(((short)0));
        }
        else {
            indGar.setTsStabLimitata(((short)-1));
        }
    }

    @Override
    public int getWkIdAdesA() {
        throw new FieldNotMappedException("wkIdAdesA");
    }

    @Override
    public void setWkIdAdesA(int wkIdAdesA) {
        throw new FieldNotMappedException("wkIdAdesA");
    }

    @Override
    public int getWkIdAdesDa() {
        throw new FieldNotMappedException("wkIdAdesDa");
    }

    @Override
    public void setWkIdAdesDa(int wkIdAdesDa) {
        throw new FieldNotMappedException("wkIdAdesDa");
    }

    @Override
    public int getWkIdGarA() {
        throw new FieldNotMappedException("wkIdGarA");
    }

    @Override
    public void setWkIdGarA(int wkIdGarA) {
        throw new FieldNotMappedException("wkIdGarA");
    }

    @Override
    public int getWkIdGarDa() {
        throw new FieldNotMappedException("wkIdGarDa");
    }

    @Override
    public void setWkIdGarDa(int wkIdGarDa) {
        throw new FieldNotMappedException("wkIdGarDa");
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
