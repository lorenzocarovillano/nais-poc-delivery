package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.Lccs0025FlPresInPtf;
import it.accenture.jnais.ws.enums.Lccs0025FlTipoIb;

/**Original name: LCCC0025-AREA-COMUNICAZ<br>
 * Variable: LCCC0025-AREA-COMUNICAZ from program LCCS0025<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Lccc0025AreaComunicaz extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LCCS0025-FL-TIPO-IB
    private Lccs0025FlTipoIb flTipoIb = new Lccs0025FlTipoIb();
    //Original name: LCCS0025-IB
    private String ib = DefaultValues.stringVal(Len.IB);
    //Original name: LCCS0025-FL-PRES-IN-PTF
    private Lccs0025FlPresInPtf flPresInPtf = new Lccs0025FlPresInPtf();
    //Original name: LCCS0025-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0025_AREA_COMUNICAZ;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLccc0025AreaComunicazBytes(buf);
    }

    public String getLccc0025AreaComunicazFormatted() {
        return MarshalByteExt.bufferToStr(getLccc0025AreaComunicazBytes());
    }

    public void setLccc0025AreaComunicazBytes(byte[] buffer) {
        setLccc0025AreaComunicazBytes(buffer, 1);
    }

    public byte[] getLccc0025AreaComunicazBytes() {
        byte[] buffer = new byte[Len.LCCC0025_AREA_COMUNICAZ];
        return getLccc0025AreaComunicazBytes(buffer, 1);
    }

    public void setLccc0025AreaComunicazBytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
    }

    public byte[] getLccc0025AreaComunicazBytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        flTipoIb.setFlTipoIb(MarshalByte.readString(buffer, position, Lccs0025FlTipoIb.Len.FL_TIPO_IB));
        position += Lccs0025FlTipoIb.Len.FL_TIPO_IB;
        ib = MarshalByte.readString(buffer, position, Len.IB);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flTipoIb.getFlTipoIb(), Lccs0025FlTipoIb.Len.FL_TIPO_IB);
        position += Lccs0025FlTipoIb.Len.FL_TIPO_IB;
        MarshalByte.writeString(buffer, position, ib, Len.IB);
        return buffer;
    }

    public void setIb(String ib) {
        this.ib = Functions.subString(ib, Len.IB);
    }

    public String getIb() {
        return this.ib;
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        flPresInPtf.setFlPresInPtf(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, flPresInPtf.getFlPresInPtf());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public Lccs0025FlPresInPtf getFlPresInPtf() {
        return flPresInPtf;
    }

    public Lccs0025FlTipoIb getFlTipoIb() {
        return flTipoIb;
    }

    @Override
    public byte[] serialize() {
        return getLccc0025AreaComunicazBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IB = 40;
        public static final int DATI_INPUT = Lccs0025FlTipoIb.Len.FL_TIPO_IB + IB;
        public static final int ID_POLI = 5;
        public static final int DATI_OUTPUT = Lccs0025FlPresInPtf.Len.FL_PRES_IN_PTF + ID_POLI;
        public static final int LCCC0025_AREA_COMUNICAZ = DATI_INPUT + DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
