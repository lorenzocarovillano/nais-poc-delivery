package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCS0025-FL-TIPO-IB<br>
 * Variable: LCCS0025-FL-TIPO-IB from copybook LCCC0025<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccs0025FlTipoIb {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FL_TIPO_IB);
    public static final String PROP = "PR";
    public static final String POLI = "PO";

    //==== METHODS ====
    public void setFlTipoIb(String flTipoIb) {
        this.value = Functions.subString(flTipoIb, Len.FL_TIPO_IB);
    }

    public String getFlTipoIb() {
        return this.value;
    }

    public String getFlTipoIbFormatted() {
        return Functions.padBlanks(getFlTipoIb(), Len.FL_TIPO_IB);
    }

    public boolean isProp() {
        return value.equals(PROP);
    }

    public void setLccs0025FlIbProp() {
        value = PROP;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_TIPO_IB = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
