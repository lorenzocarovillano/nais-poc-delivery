package it.accenture.jnais.ws;

import it.accenture.jnais.copy.DCristDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndDCrist;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBSH650<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbsh650Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-D-CRIST
    private IndDCrist indDCrist = new IndDCrist();
    //Original name: D-CRIST-DB
    private DCristDb dCristDb = new DCristDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndDCrist getIndDCrist() {
        return indDCrist;
    }

    public DCristDb getdCristDb() {
        return dCristDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
