package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-INTR-MORA<br>
 * Variable: WTDR-TOT-INTR-MORA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotIntrMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotIntrMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_INTR_MORA;
    }

    public void setWtdrTotIntrMora(AfDecimal wtdrTotIntrMora) {
        writeDecimalAsPacked(Pos.WTDR_TOT_INTR_MORA, wtdrTotIntrMora.copy());
    }

    /**Original name: WTDR-TOT-INTR-MORA<br>*/
    public AfDecimal getWtdrTotIntrMora() {
        return readPackedAsDecimal(Pos.WTDR_TOT_INTR_MORA, Len.Int.WTDR_TOT_INTR_MORA, Len.Fract.WTDR_TOT_INTR_MORA);
    }

    public void setWtdrTotIntrMoraNull(String wtdrTotIntrMoraNull) {
        writeString(Pos.WTDR_TOT_INTR_MORA_NULL, wtdrTotIntrMoraNull, Len.WTDR_TOT_INTR_MORA_NULL);
    }

    /**Original name: WTDR-TOT-INTR-MORA-NULL<br>*/
    public String getWtdrTotIntrMoraNull() {
        return readString(Pos.WTDR_TOT_INTR_MORA_NULL, Len.WTDR_TOT_INTR_MORA_NULL);
    }

    public String getWtdrTotIntrMoraNullFormatted() {
        return Functions.padBlanks(getWtdrTotIntrMoraNull(), Len.WTDR_TOT_INTR_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_INTR_MORA = 1;
        public static final int WTDR_TOT_INTR_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_INTR_MORA = 8;
        public static final int WTDR_TOT_INTR_MORA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_INTR_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_INTR_MORA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
