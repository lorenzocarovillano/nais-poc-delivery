package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: SCA-POL-ID-POLI<br>
 * Variable: SCA-POL-ID-POLI from program LLBM0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class ScaPolIdPoli extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public ScaPolIdPoli() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SCA_POL_ID_POLI;
    }

    public void setScaPolIdPoli(int scaPolIdPoli) {
        writeInt(Pos.SCA_POL_ID_POLI, scaPolIdPoli, Len.Int.SCA_POL_ID_POLI, SignType.NO_SIGN);
    }

    public void setScaPolIdPoliFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SCA_POL_ID_POLI, Pos.SCA_POL_ID_POLI);
    }

    /**Original name: SCA-POL-ID-POLI<br>*/
    public int getScaPolIdPoli() {
        return readNumDispUnsignedInt(Pos.SCA_POL_ID_POLI, Len.SCA_POL_ID_POLI);
    }

    public String getScaPolIdPoliFormatted() {
        return readFixedString(Pos.SCA_POL_ID_POLI, Len.SCA_POL_ID_POLI);
    }

    /**Original name: SCA-POL-ID-POLI-ALF<br>*/
    public String getScaPolIdPoliAlf() {
        return readString(Pos.SCA_POL_ID_POLI_ALF, Len.SCA_POL_ID_POLI_ALF);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SCA_POL_ID_POLI = 1;
        public static final int SCA_POL_ID_POLI_ALF = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SCA_POL_ID_POLI = 9;
        public static final int SCA_POL_ID_POLI_ALF = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SCA_POL_ID_POLI = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
