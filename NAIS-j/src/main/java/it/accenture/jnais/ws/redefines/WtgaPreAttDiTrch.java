package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-ATT-DI-TRCH<br>
 * Variable: WTGA-PRE-ATT-DI-TRCH from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPreAttDiTrch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPreAttDiTrch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_ATT_DI_TRCH;
    }

    public void setWtgaPreAttDiTrch(AfDecimal wtgaPreAttDiTrch) {
        writeDecimalAsPacked(Pos.WTGA_PRE_ATT_DI_TRCH, wtgaPreAttDiTrch.copy());
    }

    public void setWtgaPreAttDiTrchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_ATT_DI_TRCH, Pos.WTGA_PRE_ATT_DI_TRCH);
    }

    /**Original name: WTGA-PRE-ATT-DI-TRCH<br>*/
    public AfDecimal getWtgaPreAttDiTrch() {
        return readPackedAsDecimal(Pos.WTGA_PRE_ATT_DI_TRCH, Len.Int.WTGA_PRE_ATT_DI_TRCH, Len.Fract.WTGA_PRE_ATT_DI_TRCH);
    }

    public byte[] getWtgaPreAttDiTrchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_ATT_DI_TRCH, Pos.WTGA_PRE_ATT_DI_TRCH);
        return buffer;
    }

    public void initWtgaPreAttDiTrchSpaces() {
        fill(Pos.WTGA_PRE_ATT_DI_TRCH, Len.WTGA_PRE_ATT_DI_TRCH, Types.SPACE_CHAR);
    }

    public void setWtgaPreAttDiTrchNull(String wtgaPreAttDiTrchNull) {
        writeString(Pos.WTGA_PRE_ATT_DI_TRCH_NULL, wtgaPreAttDiTrchNull, Len.WTGA_PRE_ATT_DI_TRCH_NULL);
    }

    /**Original name: WTGA-PRE-ATT-DI-TRCH-NULL<br>*/
    public String getWtgaPreAttDiTrchNull() {
        return readString(Pos.WTGA_PRE_ATT_DI_TRCH_NULL, Len.WTGA_PRE_ATT_DI_TRCH_NULL);
    }

    public String getWtgaPreAttDiTrchNullFormatted() {
        return Functions.padBlanks(getWtgaPreAttDiTrchNull(), Len.WTGA_PRE_ATT_DI_TRCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_ATT_DI_TRCH = 1;
        public static final int WTGA_PRE_ATT_DI_TRCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_ATT_DI_TRCH = 8;
        public static final int WTGA_PRE_ATT_DI_TRCH_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_ATT_DI_TRCH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_ATT_DI_TRCH = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
