package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WMFZ-DT-EFF-MOVI-FINRIO<br>
 * Variable: WMFZ-DT-EFF-MOVI-FINRIO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmfzDtEffMoviFinrio extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmfzDtEffMoviFinrio() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMFZ_DT_EFF_MOVI_FINRIO;
    }

    public void setWmfzDtEffMoviFinrio(int wmfzDtEffMoviFinrio) {
        writeIntAsPacked(Pos.WMFZ_DT_EFF_MOVI_FINRIO, wmfzDtEffMoviFinrio, Len.Int.WMFZ_DT_EFF_MOVI_FINRIO);
    }

    public void setWmfzDtEffMoviFinrioFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMFZ_DT_EFF_MOVI_FINRIO, Pos.WMFZ_DT_EFF_MOVI_FINRIO);
    }

    /**Original name: WMFZ-DT-EFF-MOVI-FINRIO<br>*/
    public int getWmfzDtEffMoviFinrio() {
        return readPackedAsInt(Pos.WMFZ_DT_EFF_MOVI_FINRIO, Len.Int.WMFZ_DT_EFF_MOVI_FINRIO);
    }

    public byte[] getWmfzDtEffMoviFinrioAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMFZ_DT_EFF_MOVI_FINRIO, Pos.WMFZ_DT_EFF_MOVI_FINRIO);
        return buffer;
    }

    public void initWmfzDtEffMoviFinrioSpaces() {
        fill(Pos.WMFZ_DT_EFF_MOVI_FINRIO, Len.WMFZ_DT_EFF_MOVI_FINRIO, Types.SPACE_CHAR);
    }

    public void setWmfzDtEffMoviFinrioNull(String wmfzDtEffMoviFinrioNull) {
        writeString(Pos.WMFZ_DT_EFF_MOVI_FINRIO_NULL, wmfzDtEffMoviFinrioNull, Len.WMFZ_DT_EFF_MOVI_FINRIO_NULL);
    }

    /**Original name: WMFZ-DT-EFF-MOVI-FINRIO-NULL<br>*/
    public String getWmfzDtEffMoviFinrioNull() {
        return readString(Pos.WMFZ_DT_EFF_MOVI_FINRIO_NULL, Len.WMFZ_DT_EFF_MOVI_FINRIO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMFZ_DT_EFF_MOVI_FINRIO = 1;
        public static final int WMFZ_DT_EFF_MOVI_FINRIO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMFZ_DT_EFF_MOVI_FINRIO = 5;
        public static final int WMFZ_DT_EFF_MOVI_FINRIO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMFZ_DT_EFF_MOVI_FINRIO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
