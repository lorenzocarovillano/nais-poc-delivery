package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RRE-COD-PNT-RETE-INI<br>
 * Variable: RRE-COD-PNT-RETE-INI from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RreCodPntReteIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RreCodPntReteIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RRE_COD_PNT_RETE_INI;
    }

    public void setRreCodPntReteIni(long rreCodPntReteIni) {
        writeLongAsPacked(Pos.RRE_COD_PNT_RETE_INI, rreCodPntReteIni, Len.Int.RRE_COD_PNT_RETE_INI);
    }

    public void setRreCodPntReteIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RRE_COD_PNT_RETE_INI, Pos.RRE_COD_PNT_RETE_INI);
    }

    /**Original name: RRE-COD-PNT-RETE-INI<br>*/
    public long getRreCodPntReteIni() {
        return readPackedAsLong(Pos.RRE_COD_PNT_RETE_INI, Len.Int.RRE_COD_PNT_RETE_INI);
    }

    public byte[] getRreCodPntReteIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RRE_COD_PNT_RETE_INI, Pos.RRE_COD_PNT_RETE_INI);
        return buffer;
    }

    public void setRreCodPntReteIniNull(String rreCodPntReteIniNull) {
        writeString(Pos.RRE_COD_PNT_RETE_INI_NULL, rreCodPntReteIniNull, Len.RRE_COD_PNT_RETE_INI_NULL);
    }

    /**Original name: RRE-COD-PNT-RETE-INI-NULL<br>*/
    public String getRreCodPntReteIniNull() {
        return readString(Pos.RRE_COD_PNT_RETE_INI_NULL, Len.RRE_COD_PNT_RETE_INI_NULL);
    }

    public String getRreCodPntReteIniNullFormatted() {
        return Functions.padBlanks(getRreCodPntReteIniNull(), Len.RRE_COD_PNT_RETE_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RRE_COD_PNT_RETE_INI = 1;
        public static final int RRE_COD_PNT_RETE_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RRE_COD_PNT_RETE_INI = 6;
        public static final int RRE_COD_PNT_RETE_INI_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RRE_COD_PNT_RETE_INI = 10;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
