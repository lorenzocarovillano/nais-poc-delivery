package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-SNDNLQ<br>
 * Variable: PCO-DT-ULT-BOLL-SNDNLQ from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollSndnlq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollSndnlq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_SNDNLQ;
    }

    public void setPcoDtUltBollSndnlq(int pcoDtUltBollSndnlq) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_SNDNLQ, pcoDtUltBollSndnlq, Len.Int.PCO_DT_ULT_BOLL_SNDNLQ);
    }

    public void setPcoDtUltBollSndnlqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_SNDNLQ, Pos.PCO_DT_ULT_BOLL_SNDNLQ);
    }

    /**Original name: PCO-DT-ULT-BOLL-SNDNLQ<br>*/
    public int getPcoDtUltBollSndnlq() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_SNDNLQ, Len.Int.PCO_DT_ULT_BOLL_SNDNLQ);
    }

    public byte[] getPcoDtUltBollSndnlqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_SNDNLQ, Pos.PCO_DT_ULT_BOLL_SNDNLQ);
        return buffer;
    }

    public void initPcoDtUltBollSndnlqHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_SNDNLQ, Len.PCO_DT_ULT_BOLL_SNDNLQ, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollSndnlqNull(String pcoDtUltBollSndnlqNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_SNDNLQ_NULL, pcoDtUltBollSndnlqNull, Len.PCO_DT_ULT_BOLL_SNDNLQ_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-SNDNLQ-NULL<br>*/
    public String getPcoDtUltBollSndnlqNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_SNDNLQ_NULL, Len.PCO_DT_ULT_BOLL_SNDNLQ_NULL);
    }

    public String getPcoDtUltBollSndnlqNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollSndnlqNull(), Len.PCO_DT_ULT_BOLL_SNDNLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_SNDNLQ = 1;
        public static final int PCO_DT_ULT_BOLL_SNDNLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_SNDNLQ = 5;
        public static final int PCO_DT_ULT_BOLL_SNDNLQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_SNDNLQ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
