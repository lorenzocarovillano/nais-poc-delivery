package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Lccc0090;
import org.apache.commons.lang3.ArrayUtils;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0090<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0090Data {

    //==== PROPERTIES ====
    /**Original name: DESCRIZ-ERR-DB2<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI DI WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private String descrizErrDb2 = "";
    //Original name: WS-CTRL-TABELLA
    private String wsCtrlTabella = DefaultValues.stringVal(Len.WS_CTRL_TABELLA);
    private static final String[] TAB_VALIDA = new String[] {"ADES", "ACC-COMM", "ANA-SOPR-AUT", "ANTIRICIC", "ASSICURATI", "AST-ALLOC", "ATT-SERV-VAL", "D-ATT-SERV-VAL", "BILA-FND-CALC", "BILA-FND-ESTR", "BILA-TRCH-CALC", "BILA-TRCH-ESTR", "BILA-TRCH-SOM-P", "BILA-VAR-CALC-P", "BILA-VAR-CALC-T", "BNFIC", "BNFICR-LIQ", "CLAU-TXT", "CNBT-NDED", "CONV-AZ", "D-CRIST", "DETT-ANTIC", "DETT-DEROGA", "DETT-QUEST", "DETT-TCONT-LIQ", "DETT-TIT-CONT", "DETT-TIT-DI-RAT", "DETT-TIT-COA-AG", "DETT-TRASFE", "DFLT-ADES", "DFLT-RINN-TRCH", "DOCTO-LIQ", "D-COLL", "D-FISC-ADES", "D-FORZ-LIQ", "EST-MOVI", "ESTR-CNT-COASS", "ESTENZ-CLAU-TXT", "EVE-SERV-VAL", "FNZ-F03-ACCOUNT", "FNZ-F03-ORDER", "FNZ-F05-ACCOUNT", "FNZ-F5A-ACC-PF", "FNZ-F5A-FUND-PF", "GAR", "GAR-LIQ", "IDEN-ESTNI", "IMPST-BOLLO", "IMPST-SOST", "LEG-ADES", "LIQ", "MATR-ELAB-BATCH", "MATR-OBBL-ATTB", "MOT-DEROGA", "MOVI", "MOVI-BATCH-SOSP", "MOVI-FINRIO", "NOTE-OGG", "OGG-BLOCCO", "OGG-COLLG", "OGG-DEROGA", "OGG-IN-COASS", "OGG-RIASS", "PARAM-CLAU-TXT", "PARAM-DI-CALC", "PARAM-MOVI", "PARAM-OGG", "PERC-LIQ", "POLI", "PREST", "PREV", "PROV-DI-GAR", "PROG-PROD-IVASS", "QUEST", "RAPP-ANA", "RAPP-RETE", "REGOLE-VEND", "REINVST-POLI-LQ", "RICH", "RICH-DIS-FND", "RICH-INVST-FND", "RIP-COASS-AGE", "RIS-DI-TRCH", "SOGG-ANTIRICIC", "SOPR-DETT-QUEST", "SOPR-DI-GAR", "STAT-OGG-BUS", "STAT-OGG-WF", "STRA-DI-INVST", "STRC-MOVI-SOSP", "TCONT-LIQ", "TIT-CONT", "TRANS-POLI", "TRANS-RAPP-ANA", "TRANS-REN", "TIT-RAT", "TRCH-DI-GAR", "TRCH-LIQ", "TS-BNS-SIN-PRE", "VAL-AST", "VAL-PLFD", "VAL-PMC", "VINC-PEG", "RICH-EST", "STAT-RICH-EST", "SEQ_ID_BATCH", "SEQ_ID_EXECUTION", "SEQ_ID_JOB", "SEQ_ID_MESSAGE", "SEQ_IVASS_TARI", "SEQ_IVASS_PROD", "SEQ_RIGA", "SEQ_SESSIONE", "SHARED_MEMORY", "TRA-COND-MODMIN", "TRA-VAL-MODMIN", "PRE-LRD-CONT", "SEQ-BILA-PROV-AMM-D", "SEQ-BILA-PROV-AMM-T", "D-CALC-SWITCH", "RIP-DI-RIASS", "TRCH-RIASTA", "QUEST-ADEG-VER", "RIBIL", "SERV-INV-MIFID", "PRESTAZ-PRE-ASS", "MOT-LIQ", "PDM-POLI-FORZ", "PDM-CONF-SCARTO", "PDM-CONF-GAP-IM", "PDM-CONF-ELAB", "AUT-DUE-DIL", "RIV-TRCH-ESTR", "RIV-VAR-CALC-P", "RIV-VAR-CALC-T", "FNZ-STANDBY", "FNZ-EXT-ACC-ID", "COSTI-IDD", "DETT-COSTI-IDD", "TERZO-REFERENTE", "CNTR-PTF-DIFF", "FNZ-MON-FL-QTD", "PDM-CONF-ELAB-B", "PDM-STRC-ELAB", "PDM-STAT-ELAB", "CNTSTR-CNT-CORR", "STRA-ACTV-PTF", "D-STRA-ACTV-PTF", "STAT-STRA-AP", "FNZ-EXT-ORDER-ID"};
    /**Original name: WS-CTRL-TABELLA-CLIENT<br>
	 * <pre>****************************************************
	 *  tabelle bnl
	 * ****************************************************</pre>*/
    private String wsCtrlTabellaClient = DefaultValues.stringVal(Len.WS_CTRL_TABELLA_CLIENT);
    private static final String[] TAB_CLIENT_VALIDA = new String[] {"EST-POLI", "EST-RAPP-ANA", "EST-TRCH-DI-GAR", "PDM-GAR-UL-ESTR", "PDM-TRCH-ESTR", "PDM-VAR-CALC-P", "PDM-VAR-CALC-T", "SEGMENTAZ-CLI"};
    //Original name: WS-ID-TABELLA
    private int wsIdTabella = DefaultValues.INT_VAL;
    //Original name: LCCC0090
    private Lccc0090 lccc0090 = new Lccc0090();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWsCtrlTabella(String wsCtrlTabella) {
        this.wsCtrlTabella = Functions.subString(wsCtrlTabella, Len.WS_CTRL_TABELLA);
    }

    public String getWsCtrlTabella() {
        return this.wsCtrlTabella;
    }

    public boolean isTabValida() {
        return ArrayUtils.contains(TAB_VALIDA, wsCtrlTabella);
    }

    public void setWsCtrlTabellaClient(String wsCtrlTabellaClient) {
        this.wsCtrlTabellaClient = Functions.subString(wsCtrlTabellaClient, Len.WS_CTRL_TABELLA_CLIENT);
    }

    public String getWsCtrlTabellaClient() {
        return this.wsCtrlTabellaClient;
    }

    public boolean isTabClientValida() {
        return ArrayUtils.contains(TAB_CLIENT_VALIDA, wsCtrlTabellaClient);
    }

    public int getWsIdTabella() {
        return this.wsIdTabella;
    }

    public void setWorkAreaBytes(byte[] buffer) {
        setWorkAreaBytes(buffer, 1);
    }

    /**Original name: WORK-AREA<br>
	 * <pre>----------------------------------------------------------------*
	 *     COMMAREA SERVIZIO
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getWorkAreaBytes() {
        byte[] buffer = new byte[Len.WORK_AREA];
        return getWorkAreaBytes(buffer, 1);
    }

    public void setWorkAreaBytes(byte[] buffer, int offset) {
        int position = offset;
        lccc0090.setDatiInputBytes(buffer, position);
        position += Lccc0090.Len.DATI_INPUT;
        lccc0090.setDatiOutputBytes(buffer, position);
    }

    public byte[] getWorkAreaBytes(byte[] buffer, int offset) {
        int position = offset;
        lccc0090.getDatiInputBytes(buffer, position);
        position += Lccc0090.Len.DATI_INPUT;
        lccc0090.getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public Lccc0090 getLccc0090() {
        return lccc0090;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_CTRL_TABELLA = 20;
        public static final int WS_CTRL_TABELLA_CLIENT = 20;
        public static final int WORK_AREA = Lccc0090.Len.DATI_INPUT + Lccc0090.Len.DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
