package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-DATA-X-12<br>
 * Variable: WK-DATA-X-12 from program LVVS0007<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkDataX12 {

    //==== PROPERTIES ====
    //Original name: WK-X-AA
    private String xAa = DefaultValues.stringVal(Len.X_AA);
    //Original name: WK-VIROGLA
    private char virogla = ',';
    //Original name: WK-X-GG
    private String xGg = DefaultValues.stringVal(Len.X_GG);

    //==== METHODS ====
    public void setxAa(String xAa) {
        this.xAa = Functions.subString(xAa, Len.X_AA);
    }

    public String getxAa() {
        return this.xAa;
    }

    public void setVirogla(char virogla) {
        this.virogla = virogla;
    }

    public char getVirogla() {
        return this.virogla;
    }

    public void setxGg(String xGg) {
        this.xGg = Functions.subString(xGg, Len.X_GG);
    }

    public String getxGg() {
        return this.xGg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int X_AA = 4;
        public static final int X_GG = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
