package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-TS-STAB-LIMITATA<br>
 * Variable: WGRZ-TS-STAB-LIMITATA from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzTsStabLimitata extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzTsStabLimitata() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_TS_STAB_LIMITATA;
    }

    public void setWgrzTsStabLimitata(AfDecimal wgrzTsStabLimitata) {
        writeDecimalAsPacked(Pos.WGRZ_TS_STAB_LIMITATA, wgrzTsStabLimitata.copy());
    }

    public void setWgrzTsStabLimitataFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_TS_STAB_LIMITATA, Pos.WGRZ_TS_STAB_LIMITATA);
    }

    /**Original name: WGRZ-TS-STAB-LIMITATA<br>*/
    public AfDecimal getWgrzTsStabLimitata() {
        return readPackedAsDecimal(Pos.WGRZ_TS_STAB_LIMITATA, Len.Int.WGRZ_TS_STAB_LIMITATA, Len.Fract.WGRZ_TS_STAB_LIMITATA);
    }

    public byte[] getWgrzTsStabLimitataAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_TS_STAB_LIMITATA, Pos.WGRZ_TS_STAB_LIMITATA);
        return buffer;
    }

    public void initWgrzTsStabLimitataSpaces() {
        fill(Pos.WGRZ_TS_STAB_LIMITATA, Len.WGRZ_TS_STAB_LIMITATA, Types.SPACE_CHAR);
    }

    public void setWgrzTsStabLimitataNull(String wgrzTsStabLimitataNull) {
        writeString(Pos.WGRZ_TS_STAB_LIMITATA_NULL, wgrzTsStabLimitataNull, Len.WGRZ_TS_STAB_LIMITATA_NULL);
    }

    /**Original name: WGRZ-TS-STAB-LIMITATA-NULL<br>*/
    public String getWgrzTsStabLimitataNull() {
        return readString(Pos.WGRZ_TS_STAB_LIMITATA_NULL, Len.WGRZ_TS_STAB_LIMITATA_NULL);
    }

    public String getWgrzTsStabLimitataNullFormatted() {
        return Functions.padBlanks(getWgrzTsStabLimitataNull(), Len.WGRZ_TS_STAB_LIMITATA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_TS_STAB_LIMITATA = 1;
        public static final int WGRZ_TS_STAB_LIMITATA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_TS_STAB_LIMITATA = 8;
        public static final int WGRZ_TS_STAB_LIMITATA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WGRZ_TS_STAB_LIMITATA = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_TS_STAB_LIMITATA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
