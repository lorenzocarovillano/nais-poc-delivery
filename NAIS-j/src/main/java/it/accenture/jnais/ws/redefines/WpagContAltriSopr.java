package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-ALTRI-SOPR<br>
 * Variable: WPAG-CONT-ALTRI-SOPR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContAltriSopr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContAltriSopr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_ALTRI_SOPR;
    }

    public void setWpagContAltriSopr(AfDecimal wpagContAltriSopr) {
        writeDecimalAsPacked(Pos.WPAG_CONT_ALTRI_SOPR, wpagContAltriSopr.copy());
    }

    public void setWpagContAltriSoprFormatted(String wpagContAltriSopr) {
        setWpagContAltriSopr(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_ALTRI_SOPR + Len.Fract.WPAG_CONT_ALTRI_SOPR, Len.Fract.WPAG_CONT_ALTRI_SOPR, wpagContAltriSopr));
    }

    public void setWpagContAltriSoprFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_ALTRI_SOPR, Pos.WPAG_CONT_ALTRI_SOPR);
    }

    /**Original name: WPAG-CONT-ALTRI-SOPR<br>*/
    public AfDecimal getWpagContAltriSopr() {
        return readPackedAsDecimal(Pos.WPAG_CONT_ALTRI_SOPR, Len.Int.WPAG_CONT_ALTRI_SOPR, Len.Fract.WPAG_CONT_ALTRI_SOPR);
    }

    public byte[] getWpagContAltriSoprAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_ALTRI_SOPR, Pos.WPAG_CONT_ALTRI_SOPR);
        return buffer;
    }

    public void initWpagContAltriSoprSpaces() {
        fill(Pos.WPAG_CONT_ALTRI_SOPR, Len.WPAG_CONT_ALTRI_SOPR, Types.SPACE_CHAR);
    }

    public void setWpagContAltriSoprNull(String wpagContAltriSoprNull) {
        writeString(Pos.WPAG_CONT_ALTRI_SOPR_NULL, wpagContAltriSoprNull, Len.WPAG_CONT_ALTRI_SOPR_NULL);
    }

    /**Original name: WPAG-CONT-ALTRI-SOPR-NULL<br>*/
    public String getWpagContAltriSoprNull() {
        return readString(Pos.WPAG_CONT_ALTRI_SOPR_NULL, Len.WPAG_CONT_ALTRI_SOPR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_ALTRI_SOPR = 1;
        public static final int WPAG_CONT_ALTRI_SOPR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_ALTRI_SOPR = 8;
        public static final int WPAG_CONT_ALTRI_SOPR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_ALTRI_SOPR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_ALTRI_SOPR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
