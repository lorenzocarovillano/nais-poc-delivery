package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: FL-GAR-RAMO-III<br>
 * Variable: FL-GAR-RAMO-III from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlGarRamoIii {

    //==== PROPERTIES ====
    private String value = "";
    public static final String SI_GAR_RAMO_III = "SI";
    public static final String NO_GAR_RAMO_III = "NO";

    //==== METHODS ====
    public void setFlGarRamoIii(String flGarRamoIii) {
        this.value = Functions.subString(flGarRamoIii, Len.FL_GAR_RAMO_III);
    }

    public String getFlGarRamoIii() {
        return this.value;
    }

    public boolean isSiGarRamoIii() {
        return value.equals(SI_GAR_RAMO_III);
    }

    public void setSiGarRamoIii() {
        value = SI_GAR_RAMO_III;
    }

    public void setNoGarRamoIii() {
        value = NO_GAR_RAMO_III;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_GAR_RAMO_III = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
