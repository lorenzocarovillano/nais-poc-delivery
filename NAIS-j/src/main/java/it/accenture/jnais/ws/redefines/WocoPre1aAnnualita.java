package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-PRE-1A-ANNUALITA<br>
 * Variable: WOCO-PRE-1A-ANNUALITA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoPre1aAnnualita extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoPre1aAnnualita() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_PRE1A_ANNUALITA;
    }

    public void setWocoPre1aAnnualita(AfDecimal wocoPre1aAnnualita) {
        writeDecimalAsPacked(Pos.WOCO_PRE1A_ANNUALITA, wocoPre1aAnnualita.copy());
    }

    public void setWocoPre1aAnnualitaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_PRE1A_ANNUALITA, Pos.WOCO_PRE1A_ANNUALITA);
    }

    /**Original name: WOCO-PRE-1A-ANNUALITA<br>*/
    public AfDecimal getWocoPre1aAnnualita() {
        return readPackedAsDecimal(Pos.WOCO_PRE1A_ANNUALITA, Len.Int.WOCO_PRE1A_ANNUALITA, Len.Fract.WOCO_PRE1A_ANNUALITA);
    }

    public byte[] getWocoPre1aAnnualitaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_PRE1A_ANNUALITA, Pos.WOCO_PRE1A_ANNUALITA);
        return buffer;
    }

    public void initWocoPre1aAnnualitaSpaces() {
        fill(Pos.WOCO_PRE1A_ANNUALITA, Len.WOCO_PRE1A_ANNUALITA, Types.SPACE_CHAR);
    }

    public void setWocoPre1aAnnualitaNull(String wocoPre1aAnnualitaNull) {
        writeString(Pos.WOCO_PRE1A_ANNUALITA_NULL, wocoPre1aAnnualitaNull, Len.WOCO_PRE1A_ANNUALITA_NULL);
    }

    /**Original name: WOCO-PRE-1A-ANNUALITA-NULL<br>*/
    public String getWocoPre1aAnnualitaNull() {
        return readString(Pos.WOCO_PRE1A_ANNUALITA_NULL, Len.WOCO_PRE1A_ANNUALITA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_PRE1A_ANNUALITA = 1;
        public static final int WOCO_PRE1A_ANNUALITA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_PRE1A_ANNUALITA = 8;
        public static final int WOCO_PRE1A_ANNUALITA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_PRE1A_ANNUALITA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_PRE1A_ANNUALITA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
