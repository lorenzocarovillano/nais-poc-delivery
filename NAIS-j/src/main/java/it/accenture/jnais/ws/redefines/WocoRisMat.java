package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-RIS-MAT<br>
 * Variable: WOCO-RIS-MAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoRisMat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoRisMat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_RIS_MAT;
    }

    public void setWocoRisMat(AfDecimal wocoRisMat) {
        writeDecimalAsPacked(Pos.WOCO_RIS_MAT, wocoRisMat.copy());
    }

    public void setWocoRisMatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_RIS_MAT, Pos.WOCO_RIS_MAT);
    }

    /**Original name: WOCO-RIS-MAT<br>*/
    public AfDecimal getWocoRisMat() {
        return readPackedAsDecimal(Pos.WOCO_RIS_MAT, Len.Int.WOCO_RIS_MAT, Len.Fract.WOCO_RIS_MAT);
    }

    public byte[] getWocoRisMatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_RIS_MAT, Pos.WOCO_RIS_MAT);
        return buffer;
    }

    public void initWocoRisMatSpaces() {
        fill(Pos.WOCO_RIS_MAT, Len.WOCO_RIS_MAT, Types.SPACE_CHAR);
    }

    public void setWocoRisMatNull(String wocoRisMatNull) {
        writeString(Pos.WOCO_RIS_MAT_NULL, wocoRisMatNull, Len.WOCO_RIS_MAT_NULL);
    }

    /**Original name: WOCO-RIS-MAT-NULL<br>*/
    public String getWocoRisMatNull() {
        return readString(Pos.WOCO_RIS_MAT_NULL, Len.WOCO_RIS_MAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_RIS_MAT = 1;
        public static final int WOCO_RIS_MAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_RIS_MAT = 8;
        public static final int WOCO_RIS_MAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_RIS_MAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_RIS_MAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
