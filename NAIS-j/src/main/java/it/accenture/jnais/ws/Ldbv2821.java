package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LDBV2821<br>
 * Variable: LDBV2821 from copybook LDBV2821<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv2821 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV2821-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV2821-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV2821-IMP-PREST
    private AfDecimal impPrest = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV2821;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv2821Bytes(buf);
    }

    public String getLdbv2821Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv2821Bytes());
    }

    public void setLdbv2821Bytes(byte[] buffer) {
        setLdbv2821Bytes(buffer, 1);
    }

    public byte[] getLdbv2821Bytes() {
        byte[] buffer = new byte[Len.LDBV2821];
        return getLdbv2821Bytes(buffer, 1);
    }

    public void setLdbv2821Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        impPrest.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_PREST, Len.Fract.IMP_PREST));
    }

    public byte[] getLdbv2821Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeDecimalAsPacked(buffer, position, impPrest.copy());
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setImpPrest(AfDecimal impPrest) {
        this.impPrest.assign(impPrest);
    }

    public AfDecimal getImpPrest() {
        return this.impPrest.copy();
    }

    @Override
    public byte[] serialize() {
        return getLdbv2821Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int IMP_PREST = 8;
        public static final int LDBV2821 = ID_OGG + TP_OGG + IMP_PREST;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int IMP_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMP_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
