package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idsv8888;
import it.accenture.jnais.copy.Ivvc0216;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvgrzz;
import it.accenture.jnais.copy.Ldbv1351;
import it.accenture.jnais.copy.WkTgaMax;
import it.accenture.jnais.ws.enums.FlagApp;
import it.accenture.jnais.ws.enums.FlagArea;
import it.accenture.jnais.ws.enums.FlagBufferDati;
import it.accenture.jnais.ws.enums.FlagDtVldtProd;
import it.accenture.jnais.ws.enums.FlagEsitoGaranzie;
import it.accenture.jnais.ws.enums.FlagEsitoTranche;
import it.accenture.jnais.ws.enums.FlagFineElab;
import it.accenture.jnais.ws.enums.FlagGestione;
import it.accenture.jnais.ws.enums.FlagRiass;
import it.accenture.jnais.ws.enums.FlagSkede;
import it.accenture.jnais.ws.enums.FlagStatCaus;
import it.accenture.jnais.ws.enums.FlagTipoGaranzia;
import it.accenture.jnais.ws.enums.FlagTipologiaScheda;
import it.accenture.jnais.ws.enums.FlagTipoTranche;
import it.accenture.jnais.ws.enums.FlagTrattatoValido;
import it.accenture.jnais.ws.enums.FlagTrattaTranche;
import it.accenture.jnais.ws.enums.FlagUnzipTrovato;
import it.accenture.jnais.ws.enums.FlagVersione;
import it.accenture.jnais.ws.enums.Idsv8888LivelloDebug;
import it.accenture.jnais.ws.enums.Ivvc0216FlgArea;
import it.accenture.jnais.ws.enums.WkGlovarlist;
import it.accenture.jnais.ws.enums.WkVarFunzTrovata;
import it.accenture.jnais.ws.enums.WkVarlist;
import it.accenture.jnais.ws.enums.WsTpTrch;
import it.accenture.jnais.ws.occurs.DistinctTabInfo;
import it.accenture.jnais.ws.redefines.C216TabValP;
import it.accenture.jnais.ws.redefines.C216TabValT;
import it.accenture.jnais.ws.redefines.StrDistDtVldtProdTga;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IVVS0211<br>
 * Generated as a class for rule WS.<br>*/
public class Ivvs0211Data {

    //==== PROPERTIES ====
    public static final int DISTINCT_TAB_INFO_MAXOCCURS = 20;
    //Original name: AREA-BUSINESS
    private AreaBusinessIvvs0211 areaBusiness = new AreaBusinessIvvs0211();
    /**Original name: IVVC0216-FLG-AREA<br>
	 * <pre>--  INPUT PER VALORIZZATORE</pre>*/
    private Ivvc0216FlgArea ivvc0216FlgArea = new Ivvc0216FlgArea();
    //Original name: IVVC0216
    private Ivvc0216 ivvc0216 = new Ivvc0216();
    //Original name: AREA-WORK-IVVC0216
    private AreaInput areaWorkIvvc0216 = new AreaInput();
    //Original name: AREA-WARNING-IVVC0216
    private Ivvc0215 ivvc0215 = new Ivvc0215();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: LDBV1351
    private Ldbv1351 ldbv1351 = new Ldbv1351();
    //Original name: LDBV1361
    private Ldbv1351 ldbv1361 = new Ldbv1351();
    //Original name: LCCV0021
    private Lccv0021 lccv0021 = new Lccv0021();
    //Original name: AREA-IVVV0212
    private AreaIoIvvs0212 areaIvvv0212 = new AreaIoIvvs0212();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001 = new AreaIdsv0001();
    //Original name: IDSV0003
    private Idsv0003 idsv0003 = new Idsv0003();
    /**Original name: STR-DIST-DT-VLDT-PROD-TGA<br>
	 * <pre>   fine COPY IDSV0006</pre>*/
    private StrDistDtVldtProdTga strDistDtVldtProdTga = new StrDistDtVldtProdTga();
    //Original name: DIST-SEARCH-IND
    private int distSearchInd = 1;
    /**Original name: WK-PGM<br>
	 * <pre>**************************************************************
	 *  NOMI PGM
	 * **************************************************************</pre>*/
    private String wkPgm = "IVVS0211";
    //Original name: PGM-LDBS1400
    private String pgmLdbs1400 = "LDBS1400";
    //Original name: PGM-LDBS0270
    private String pgmLdbs0270 = "LDBS0270";
    //Original name: PGM-LDBS8800
    private String pgmLdbs8800 = "LDBS8800";
    //Original name: PGM-LDBS1350
    private String pgmLdbs1350 = "LDBS1350";
    //Original name: PGM-LDBS1360
    private String pgmLdbs1360 = "LDBS1360";
    //Original name: WS-LIV-PRODOTTO
    private char wsLivProdotto = 'P';
    //Original name: WS-LIV-GARANZIA
    private char wsLivGaranzia = 'G';
    //Original name: IDSV8888
    private Idsv8888 idsv8888 = new Idsv8888();
    //Original name: GAR
    private Gar gar = new Gar();
    //Original name: TRCH-DI-GAR
    private TrchDiGar trchDiGar = new TrchDiGar();
    //Original name: MATR-VAL-VAR
    private MatrValVarLdbs1400 matrValVar = new MatrValVarLdbs1400();
    //Original name: VAR-FUNZ-DI-CALC
    private VarFunzDiCalcR varFunzDiCalc = new VarFunzDiCalcR();
    //Original name: VAR-FUNZ-DI-CALC-R
    private VarFunzDiCalcR varFunzDiCalcR = new VarFunzDiCalcR();
    /**Original name: WS-TP-TRCH<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY TIPOLOGICA TIPO TRANCHE
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_TRCH
	 * *****************************************************************</pre>*/
    private WsTpTrch wsTpTrch = new WsTpTrch();
    //Original name: IX-INDICI
    private IxIndiciIvvs0211 ixIndici = new IxIndiciIvvs0211();
    /**Original name: LIMITE-OPZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     LIMITI
	 * ----------------------------------------------------------------*</pre>*/
    private short limiteOpz = ((short)10);
    //Original name: LIMITE-TIPO-OPZ
    private short limiteTipoOpz = ((short)10);
    //Original name: LIMITE-VARIABILI-UNZIPPED
    private short limiteVariabiliUnzipped = ((short)100);
    //Original name: LCCVGRZZ
    private Lccvgrzz lccvgrzz = new Lccvgrzz();
    //Original name: WK-TGA-MAX
    private WkTgaMax wkTgaMax = new WkTgaMax();
    /**Original name: FLAG-AREA<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAGS
	 * ----------------------------------------------------------------*</pre>*/
    private FlagArea flagArea = new FlagArea();
    //Original name: FLAG-DT-VLDT-PROD
    private FlagDtVldtProd flagDtVldtProd = new FlagDtVldtProd();
    //Original name: FLAG-BUFFER-DATI
    private FlagBufferDati flagBufferDati = new FlagBufferDati();
    //Original name: FLAG-VERSIONE
    private FlagVersione flagVersione = new FlagVersione();
    //Original name: FLAG-TIPOLOGIA-SCHEDA
    private FlagTipologiaScheda flagTipologiaScheda = new FlagTipologiaScheda();
    //Original name: FLAG-GESTIONE
    private FlagGestione flagGestione = new FlagGestione();
    //Original name: FLAG-TIPO-TRANCHE
    private FlagTipoTranche flagTipoTranche = new FlagTipoTranche();
    //Original name: FLAG-TIPO-GARANZIA
    private FlagTipoGaranzia flagTipoGaranzia = new FlagTipoGaranzia();
    //Original name: FLAG-ESITO-GARANZIE
    private FlagEsitoGaranzie flagEsitoGaranzie = new FlagEsitoGaranzie();
    //Original name: FLAG-ESITO-TRANCHE
    private FlagEsitoTranche flagEsitoTranche = new FlagEsitoTranche();
    //Original name: FLAG-FINE-ELAB
    private FlagFineElab flagFineElab = new FlagFineElab();
    //Original name: FLAG-TRATTA-TRANCHE
    private FlagTrattaTranche flagTrattaTranche = new FlagTrattaTranche();
    //Original name: FLAG-SKEDE
    private FlagSkede flagSkede = new FlagSkede();
    //Original name: FLAG-STAT-CAUS
    private FlagStatCaus flagStatCaus = new FlagStatCaus();
    //Original name: FLAG-RIASS
    private FlagRiass flagRiass = new FlagRiass();
    //Original name: FLAG-APP
    private FlagApp flagApp = new FlagApp();
    //Original name: FLAG-TRATTATO-VALIDO
    private FlagTrattatoValido flagTrattatoValido = new FlagTrattatoValido();
    /**Original name: PRESENZA-VARIABILI<br>
	 * <pre>**************************************************************
	 *  COSTANTI
	 * **************************************************************</pre>*/
    private short presenzaVariabili = ((short)0);
    //Original name: PRESENZA-SERVIZIO
    private short presenzaServizio = ((short)1);
    //Original name: IVVC0211-LIVELLO-DEBUG
    private Idsv8888LivelloDebug ivvc0211LivelloDebug = new Idsv8888LivelloDebug();
    /**Original name: WS-SQLCODE<br>
	 * <pre>**************************************************************
	 *  COMODO
	 * **************************************************************</pre>*/
    private String wsSqlcode = DefaultValues.stringVal(Len.WS_SQLCODE);
    //Original name: WS-ALIAS
    private String wsAlias = DefaultValues.stringVal(Len.WS_ALIAS);
    /**Original name: VARIABILI-RIASS<br>
	 * <pre>--> Area di appoggio per RIASS</pre>*/
    private short variabiliRiass = DefaultValues.SHORT_VAL;
    //Original name: ELE-MAX-APP-COD-VAR
    private short eleMaxAppCodVar = DefaultValues.SHORT_VAL;
    //Original name: COMODO-STAT-CAUS
    private ComodoStatCaus comodoStatCaus = new ComodoStatCaus();
    //Original name: COMODO-STAT-CAUS-LIQ
    private ComodoStatCausLiq comodoStatCausLiq = new ComodoStatCausLiq();
    //Original name: WK-ID-POLI
    private String wkIdPoli = DefaultValues.stringVal(Len.WK_ID_POLI);
    //Original name: WK-ID-GAR
    private String wkIdGar = DefaultValues.stringVal(Len.WK_ID_GAR);
    //Original name: WK-CODICE-COMPAGNIA
    private String wkCodiceCompagnia = DefaultValues.stringVal(Len.WK_CODICE_COMPAGNIA);
    //Original name: WK-TIPO-MOVIMENTO
    private String wkTipoMovimento = DefaultValues.stringVal(Len.WK_TIPO_MOVIMENTO);
    //Original name: WK-CODICE-PRODOTTO-VFC
    private String wkCodiceProdottoVfc = DefaultValues.stringVal(Len.WK_CODICE_PRODOTTO_VFC);
    //Original name: WK-CODICE-TARIFFA-VFC
    private String wkCodiceTariffaVfc = DefaultValues.stringVal(Len.WK_CODICE_TARIFFA_VFC);
    //Original name: WK-CODICE-TARIFFA-AC5
    private String wkCodiceTariffaAc5 = DefaultValues.stringVal(Len.WK_CODICE_TARIFFA_AC5);
    //Original name: WK-DATA-INIZIO-VFC
    private String wkDataInizioVfc = DefaultValues.stringVal(Len.WK_DATA_INIZIO_VFC);
    //Original name: WK-NOME-FUNZIONE-VFC
    private String wkNomeFunzioneVfc = DefaultValues.stringVal(Len.WK_NOME_FUNZIONE_VFC);
    //Original name: WK-STEP-ELAB-VFC
    private String wkStepElabVfc = DefaultValues.stringVal(Len.WK_STEP_ELAB_VFC);
    //Original name: WK-GLOVARLIST
    private WkGlovarlist wkGlovarlist = new WkGlovarlist();
    //Original name: WK-VAR-FUNZ-TROVATA
    private WkVarFunzTrovata wkVarFunzTrovata = new WkVarFunzTrovata();
    //Original name: DISTINCT-TAB-INFO
    private DistinctTabInfo[] distinctTabInfo = new DistinctTabInfo[DISTINCT_TAB_INFO_MAXOCCURS];
    /**Original name: WK-VARLIST<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAGS
	 * ----------------------------------------------------------------*</pre>*/
    private WkVarlist wkVarlist = new WkVarlist();
    //Original name: UNZIP-STRUCTURE
    private UnzipStructure unzipStructure = new UnzipStructure();
    //Original name: FLAG-UNZIP-TROVATO
    private FlagUnzipTrovato flagUnzipTrovato = new FlagUnzipTrovato();
    //Original name: WK-ELE-VARIABILI-MAX
    private String wkEleVariabiliMax = DefaultValues.stringVal(Len.WK_ELE_VARIABILI_MAX);
    //Original name: AREA-IDSV0101
    private AreaIdsv0101 areaIdsv0101 = new AreaIdsv0101();
    //Original name: IVVC0211-SEARCH-IND
    private int ivvc0211SearchInd = 1;

    //==== CONSTRUCTORS ====
    public Ivvs0211Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int distinctTabInfoIdx = 1; distinctTabInfoIdx <= DISTINCT_TAB_INFO_MAXOCCURS; distinctTabInfoIdx++) {
            distinctTabInfo[distinctTabInfoIdx - 1] = new DistinctTabInfo();
        }
    }

    public String getAreaIvvc0216Formatted() {
        return MarshalByteExt.bufferToStr(getAreaIvvc0216Bytes());
    }

    /**Original name: AREA-IVVC0216<br>
	 * <pre>--  AREA SCHEDA VARIABILI</pre>*/
    public byte[] getAreaIvvc0216Bytes() {
        byte[] buffer = new byte[Len.AREA_IVVC0216];
        return getAreaIvvc0216Bytes(buffer, 1);
    }

    public byte[] getAreaIvvc0216Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ivvc0216.getC216Dee(), Ivvc0216.Len.C216_DEE);
        position += Ivvc0216.Len.C216_DEE;
        MarshalByte.writeShortAsPacked(buffer, position, ivvc0216.getC216EleLivelloMaxP(), Ivvc0216.Len.Int.C216_ELE_LIVELLO_MAX_P, 0);
        position += Ivvc0216.Len.C216_ELE_LIVELLO_MAX_P;
        ivvc0216.getC216TabValP().getC216TabValPBytes(buffer, position);
        position += C216TabValP.Len.C216_TAB_VAL_P;
        MarshalByte.writeShortAsPacked(buffer, position, ivvc0216.getC216EleLivelloMaxT(), Ivvc0216.Len.Int.C216_ELE_LIVELLO_MAX_T, 0);
        position += Ivvc0216.Len.C216_ELE_LIVELLO_MAX_T;
        ivvc0216.getC216TabValT().getC216TabValTBytes(buffer, position);
        position += C216TabValT.Len.C216_TAB_VAL_T;
        ivvc0216.getC216VarAutOperBytes(buffer, position);
        return buffer;
    }

    public void setDistSearchInd(int distSearchInd) {
        this.distSearchInd = distSearchInd;
    }

    public int getDistSearchInd() {
        return this.distSearchInd;
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getPgmLdbs1400() {
        return this.pgmLdbs1400;
    }

    public String getPgmLdbs1400Formatted() {
        return Functions.padBlanks(getPgmLdbs1400(), Len.PGM_LDBS1400);
    }

    public String getPgmLdbs0270() {
        return this.pgmLdbs0270;
    }

    public String getPgmLdbs0270Formatted() {
        return Functions.padBlanks(getPgmLdbs0270(), Len.PGM_LDBS0270);
    }

    public String getPgmLdbs8800() {
        return this.pgmLdbs8800;
    }

    public String getPgmLdbs8800Formatted() {
        return Functions.padBlanks(getPgmLdbs8800(), Len.PGM_LDBS8800);
    }

    public String getPgmLdbs1350() {
        return this.pgmLdbs1350;
    }

    public String getPgmLdbs1350Formatted() {
        return Functions.padBlanks(getPgmLdbs1350(), Len.PGM_LDBS1350);
    }

    public String getPgmLdbs1360() {
        return this.pgmLdbs1360;
    }

    public String getPgmLdbs1360Formatted() {
        return Functions.padBlanks(getPgmLdbs1360(), Len.PGM_LDBS1360);
    }

    public char getWsLivProdotto() {
        return this.wsLivProdotto;
    }

    public char getWsLivGaranzia() {
        return this.wsLivGaranzia;
    }

    public short getLimiteOpz() {
        return this.limiteOpz;
    }

    public short getLimiteTipoOpz() {
        return this.limiteTipoOpz;
    }

    public short getLimiteVariabiliUnzipped() {
        return this.limiteVariabiliUnzipped;
    }

    public short getPresenzaVariabili() {
        return this.presenzaVariabili;
    }

    public short getPresenzaServizio() {
        return this.presenzaServizio;
    }

    public void setWsSqlcode(long wsSqlcode) {
        this.wsSqlcode = PicFormatter.display("++(7)9").format(wsSqlcode).toString();
    }

    public long getWsSqlcode() {
        return PicParser.display("++(7)9").parseLong(this.wsSqlcode);
    }

    public String getWsSqlcodeFormatted() {
        return this.wsSqlcode;
    }

    public String getWsSqlcodeAsString() {
        return getWsSqlcodeFormatted();
    }

    public void setWsAlias(String wsAlias) {
        this.wsAlias = Functions.subString(wsAlias, Len.WS_ALIAS);
    }

    public String getWsAlias() {
        return this.wsAlias;
    }

    public void setVariabiliRiass(short variabiliRiass) {
        this.variabiliRiass = variabiliRiass;
    }

    public short getVariabiliRiass() {
        return this.variabiliRiass;
    }

    public void setEleMaxAppCodVar(short eleMaxAppCodVar) {
        this.eleMaxAppCodVar = eleMaxAppCodVar;
    }

    public short getEleMaxAppCodVar() {
        return this.eleMaxAppCodVar;
    }

    public void setWkIdPoli(int wkIdPoli) {
        this.wkIdPoli = NumericDisplay.asString(wkIdPoli, Len.WK_ID_POLI);
    }

    public int getWkIdPoli() {
        return NumericDisplay.asInt(this.wkIdPoli);
    }

    public String getWkIdPoliFormatted() {
        return this.wkIdPoli;
    }

    public String getWkIdPoliAsString() {
        return getWkIdPoliFormatted();
    }

    public void setWkIdGar(int wkIdGar) {
        this.wkIdGar = NumericDisplay.asString(wkIdGar, Len.WK_ID_GAR);
    }

    public int getWkIdGar() {
        return NumericDisplay.asInt(this.wkIdGar);
    }

    public String getWkIdGarFormatted() {
        return this.wkIdGar;
    }

    public String getWkIdGarAsString() {
        return getWkIdGarFormatted();
    }

    public void setWkCodiceCompagniaFormatted(String wkCodiceCompagnia) {
        this.wkCodiceCompagnia = Trunc.toUnsignedNumeric(wkCodiceCompagnia, Len.WK_CODICE_COMPAGNIA);
    }

    public int getWkCodiceCompagnia() {
        return NumericDisplay.asInt(this.wkCodiceCompagnia);
    }

    public String getWkCodiceCompagniaFormatted() {
        return this.wkCodiceCompagnia;
    }

    public String getWkCodiceCompagniaAsString() {
        return getWkCodiceCompagniaFormatted();
    }

    public void setWkTipoMovimentoFormatted(String wkTipoMovimento) {
        this.wkTipoMovimento = Trunc.toUnsignedNumeric(wkTipoMovimento, Len.WK_TIPO_MOVIMENTO);
    }

    public int getWkTipoMovimento() {
        return NumericDisplay.asInt(this.wkTipoMovimento);
    }

    public String getWkTipoMovimentoFormatted() {
        return this.wkTipoMovimento;
    }

    public String getWkTipoMovimentoAsString() {
        return getWkTipoMovimentoFormatted();
    }

    public void setWkCodiceProdottoVfc(String wkCodiceProdottoVfc) {
        this.wkCodiceProdottoVfc = Functions.subString(wkCodiceProdottoVfc, Len.WK_CODICE_PRODOTTO_VFC);
    }

    public String getWkCodiceProdottoVfc() {
        return this.wkCodiceProdottoVfc;
    }

    public String getWkCodiceProdottoVfcFormatted() {
        return Functions.padBlanks(getWkCodiceProdottoVfc(), Len.WK_CODICE_PRODOTTO_VFC);
    }

    public void setWkCodiceTariffaVfc(String wkCodiceTariffaVfc) {
        this.wkCodiceTariffaVfc = Functions.subString(wkCodiceTariffaVfc, Len.WK_CODICE_TARIFFA_VFC);
    }

    public String getWkCodiceTariffaVfc() {
        return this.wkCodiceTariffaVfc;
    }

    public String getWkCodiceTariffaVfcFormatted() {
        return Functions.padBlanks(getWkCodiceTariffaVfc(), Len.WK_CODICE_TARIFFA_VFC);
    }

    public void setWkCodiceTariffaAc5(String wkCodiceTariffaAc5) {
        this.wkCodiceTariffaAc5 = Functions.subString(wkCodiceTariffaAc5, Len.WK_CODICE_TARIFFA_AC5);
    }

    public String getWkCodiceTariffaAc5() {
        return this.wkCodiceTariffaAc5;
    }

    public String getWkCodiceTariffaAc5Formatted() {
        return Functions.padBlanks(getWkCodiceTariffaAc5(), Len.WK_CODICE_TARIFFA_AC5);
    }

    public void setWkDataInizioVfc(int wkDataInizioVfc) {
        this.wkDataInizioVfc = NumericDisplay.asString(wkDataInizioVfc, Len.WK_DATA_INIZIO_VFC);
    }

    public void setWkDataInizioVfcFormatted(String wkDataInizioVfc) {
        this.wkDataInizioVfc = Trunc.toUnsignedNumeric(wkDataInizioVfc, Len.WK_DATA_INIZIO_VFC);
    }

    public int getWkDataInizioVfc() {
        return NumericDisplay.asInt(this.wkDataInizioVfc);
    }

    public String getWkDataInizioVfcFormatted() {
        return this.wkDataInizioVfc;
    }

    public String getWkDataInizioVfcAsString() {
        return getWkDataInizioVfcFormatted();
    }

    public void setWkNomeFunzioneVfc(String wkNomeFunzioneVfc) {
        this.wkNomeFunzioneVfc = Functions.subString(wkNomeFunzioneVfc, Len.WK_NOME_FUNZIONE_VFC);
    }

    public String getWkNomeFunzioneVfc() {
        return this.wkNomeFunzioneVfc;
    }

    public String getWkNomeFunzioneVfcFormatted() {
        return Functions.padBlanks(getWkNomeFunzioneVfc(), Len.WK_NOME_FUNZIONE_VFC);
    }

    public void setWkStepElabVfc(String wkStepElabVfc) {
        this.wkStepElabVfc = Functions.subString(wkStepElabVfc, Len.WK_STEP_ELAB_VFC);
    }

    public String getWkStepElabVfc() {
        return this.wkStepElabVfc;
    }

    public String getWkStepElabVfcFormatted() {
        return Functions.padBlanks(getWkStepElabVfc(), Len.WK_STEP_ELAB_VFC);
    }

    public void setWkEleVariabiliMax(short wkEleVariabiliMax) {
        this.wkEleVariabiliMax = NumericDisplay.asString(wkEleVariabiliMax, Len.WK_ELE_VARIABILI_MAX);
    }

    public short getWkEleVariabiliMax() {
        return NumericDisplay.asShort(this.wkEleVariabiliMax);
    }

    public String getWkEleVariabiliMaxFormatted() {
        return this.wkEleVariabiliMax;
    }

    public String getWkEleVariabiliMaxAsString() {
        return getWkEleVariabiliMaxFormatted();
    }

    public void setIvvc0211SearchInd(int ivvc0211SearchInd) {
        this.ivvc0211SearchInd = ivvc0211SearchInd;
    }

    public int getIvvc0211SearchInd() {
        return this.ivvc0211SearchInd;
    }

    public AreaBusinessIvvs0211 getAreaBusiness() {
        return areaBusiness;
    }

    public AreaIdsv0001 getAreaIdsv0001() {
        return areaIdsv0001;
    }

    public AreaIdsv0101 getAreaIdsv0101() {
        return areaIdsv0101;
    }

    public AreaIoIvvs0212 getAreaIvvv0212() {
        return areaIvvv0212;
    }

    public AreaInput getAreaWorkIvvc0216() {
        return areaWorkIvvc0216;
    }

    public ComodoStatCaus getComodoStatCaus() {
        return comodoStatCaus;
    }

    public ComodoStatCausLiq getComodoStatCausLiq() {
        return comodoStatCausLiq;
    }

    public DistinctTabInfo getDistinctTabInfo(int idx) {
        return distinctTabInfo[idx - 1];
    }

    public FlagApp getFlagApp() {
        return flagApp;
    }

    public FlagArea getFlagArea() {
        return flagArea;
    }

    public FlagBufferDati getFlagBufferDati() {
        return flagBufferDati;
    }

    public FlagDtVldtProd getFlagDtVldtProd() {
        return flagDtVldtProd;
    }

    public FlagEsitoGaranzie getFlagEsitoGaranzie() {
        return flagEsitoGaranzie;
    }

    public FlagEsitoTranche getFlagEsitoTranche() {
        return flagEsitoTranche;
    }

    public FlagFineElab getFlagFineElab() {
        return flagFineElab;
    }

    public FlagGestione getFlagGestione() {
        return flagGestione;
    }

    public FlagRiass getFlagRiass() {
        return flagRiass;
    }

    public FlagSkede getFlagSkede() {
        return flagSkede;
    }

    public FlagStatCaus getFlagStatCaus() {
        return flagStatCaus;
    }

    public FlagTipoGaranzia getFlagTipoGaranzia() {
        return flagTipoGaranzia;
    }

    public FlagTipoTranche getFlagTipoTranche() {
        return flagTipoTranche;
    }

    public FlagTipologiaScheda getFlagTipologiaScheda() {
        return flagTipologiaScheda;
    }

    public FlagTrattaTranche getFlagTrattaTranche() {
        return flagTrattaTranche;
    }

    public FlagTrattatoValido getFlagTrattatoValido() {
        return flagTrattatoValido;
    }

    public FlagUnzipTrovato getFlagUnzipTrovato() {
        return flagUnzipTrovato;
    }

    public FlagVersione getFlagVersione() {
        return flagVersione;
    }

    public Gar getGar() {
        return gar;
    }

    public Idsv0003 getIdsv0003() {
        return idsv0003;
    }

    public Idsv8888 getIdsv8888() {
        return idsv8888;
    }

    public Idsv8888LivelloDebug getIvvc0211LivelloDebug() {
        return ivvc0211LivelloDebug;
    }

    public Ivvc0215 getIvvc0215() {
        return ivvc0215;
    }

    public Ivvc0216 getIvvc0216() {
        return ivvc0216;
    }

    public Ivvc0216FlgArea getIvvc0216FlgArea() {
        return ivvc0216FlgArea;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndiciIvvs0211 getIxIndici() {
        return ixIndici;
    }

    public Lccv0021 getLccv0021() {
        return lccv0021;
    }

    public Lccvgrzz getLccvgrzz() {
        return lccvgrzz;
    }

    public Ldbv1351 getLdbv1351() {
        return ldbv1351;
    }

    public Ldbv1351 getLdbv1361() {
        return ldbv1361;
    }

    public MatrValVarLdbs1400 getMatrValVar() {
        return matrValVar;
    }

    public StrDistDtVldtProdTga getStrDistDtVldtProdTga() {
        return strDistDtVldtProdTga;
    }

    public TrchDiGar getTrchDiGar() {
        return trchDiGar;
    }

    public UnzipStructure getUnzipStructure() {
        return unzipStructure;
    }

    public VarFunzDiCalcR getVarFunzDiCalc() {
        return varFunzDiCalc;
    }

    public VarFunzDiCalcR getVarFunzDiCalcR() {
        return varFunzDiCalcR;
    }

    public WkGlovarlist getWkGlovarlist() {
        return wkGlovarlist;
    }

    public WkTgaMax getWkTgaMax() {
        return wkTgaMax;
    }

    public WkVarFunzTrovata getWkVarFunzTrovata() {
        return wkVarFunzTrovata;
    }

    public WkVarlist getWkVarlist() {
        return wkVarlist;
    }

    public WsTpTrch getWsTpTrch() {
        return wsTpTrch;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_SQLCODE = 9;
        public static final int WS_ALIAS = 3;
        public static final int VARIABILI_RIASS = 3;
        public static final int ELE_MAX_APP_COD_VAR = 3;
        public static final int WK_ID_POLI = 9;
        public static final int WK_ID_GAR = 9;
        public static final int WK_CODICE_COMPAGNIA = 5;
        public static final int WK_TIPO_MOVIMENTO = 5;
        public static final int WK_CODICE_PRODOTTO_VFC = 12;
        public static final int WK_CODICE_TARIFFA_VFC = 12;
        public static final int WK_CODICE_TARIFFA_AC5 = 12;
        public static final int WK_DATA_INIZIO_VFC = 9;
        public static final int WK_NOME_FUNZIONE_VFC = 12;
        public static final int WK_STEP_ELAB_VFC = 20;
        public static final int WK_ELE_VARIABILI_MAX = 3;
        public static final int PGM_LDBS1400 = 8;
        public static final int PGM_LDBS1350 = 8;
        public static final int PGM_LDBS1360 = 8;
        public static final int PGM_LDBS8800 = 8;
        public static final int PGM_LDBS0270 = 8;
        public static final int AREA_IVVC0216 = Ivvc0216.Len.C216_DEE + Ivvc0216.Len.C216_ELE_LIVELLO_MAX_P + C216TabValP.Len.C216_TAB_VAL_P + Ivvc0216.Len.C216_ELE_LIVELLO_MAX_T + C216TabValT.Len.C216_TAB_VAL_T + Ivvc0216.Len.C216_VAR_AUT_OPER;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
