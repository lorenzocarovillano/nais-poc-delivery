package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccc0001Loas0110;
import it.accenture.jnais.copy.WcomDatiDeroghe;
import it.accenture.jnais.copy.WcomStati;
import it.accenture.jnais.copy.WcomTastiDaAbilitare;
import it.accenture.jnais.ws.enums.S0024ModificaDtdecor;
import it.accenture.jnais.ws.enums.S0024TpVisualizPag;
import it.accenture.jnais.ws.enums.WcomNavigabilita;
import it.accenture.jnais.ws.enums.WcomTipoOperazione;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: S0024-AREA-COMUNE<br>
 * Variable: S0024-AREA-COMUNE from program LOAS0110<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class S0024AreaComune extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LCCC0001
    private Lccc0001Loas0110 lccc0001 = new Lccc0001Loas0110();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S0024_AREA_COMUNE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setS0024AreaComuneBytes(buf);
    }

    public String getS0024AreaComuneFormatted() {
        return getS0024AreaStatiFormatted();
    }

    public void setS0024AreaComuneBytes(byte[] buffer) {
        setS0024AreaComuneBytes(buffer, 1);
    }

    public byte[] getS0024AreaComuneBytes() {
        byte[] buffer = new byte[Len.S0024_AREA_COMUNE];
        return getS0024AreaComuneBytes(buffer, 1);
    }

    public void setS0024AreaComuneBytes(byte[] buffer, int offset) {
        int position = offset;
        setS0024AreaStatiBytes(buffer, position);
    }

    public byte[] getS0024AreaComuneBytes(byte[] buffer, int offset) {
        int position = offset;
        getS0024AreaStatiBytes(buffer, position);
        return buffer;
    }

    public String getS0024AreaStatiFormatted() {
        return MarshalByteExt.bufferToStr(getS0024AreaStatiBytes());
    }

    public void setS0024AreaStatiBytes(byte[] buffer) {
        setS0024AreaStatiBytes(buffer, 1);
    }

    /**Original name: S0024-AREA-STATI<br>*/
    public byte[] getS0024AreaStatiBytes() {
        byte[] buffer = new byte[Len.S0024_AREA_STATI];
        return getS0024AreaStatiBytes(buffer, 1);
    }

    public void setS0024AreaStatiBytes(byte[] buffer, int offset) {
        int position = offset;
        lccc0001.getTipoOperazione().setTipoOperazione(MarshalByte.readString(buffer, position, WcomTipoOperazione.Len.TIPO_OPERAZIONE));
        position += WcomTipoOperazione.Len.TIPO_OPERAZIONE;
        lccc0001.getNavigabilita().setNavigabilita(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.getTastiDaAbilitare().setTastiDaAbilitareBytes(buffer, position);
        position += WcomTastiDaAbilitare.Len.TASTI_DA_ABILITARE;
        lccc0001.setDatiActuatorBytes(buffer, position);
        position += Lccc0001Loas0110.Len.DATI_ACTUATOR;
        lccc0001.getStati().setStatiBytes(buffer, position);
        position += WcomStati.Len.STATI;
        lccc0001.getDatiDeroghe().setDatiDerogheBytes(buffer, position);
        position += WcomDatiDeroghe.Len.DATI_DEROGHE;
        lccc0001.setMovimentiAnnullBytes(buffer, position);
        position += Lccc0001Loas0110.Len.MOVIMENTI_ANNULL;
        lccc0001.setIdMoviCrz(MarshalByte.readPackedAsInt(buffer, position, Lccc0001Loas0110.Len.Int.ID_MOVI_CRZ, 0));
        position += Lccc0001Loas0110.Len.ID_MOVI_CRZ;
        lccc0001.setFlagTariffaRischio(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.setAreaPlatfondBytes(buffer, position);
        position += Lccc0001Loas0110.Len.AREA_PLATFOND;
        lccc0001.getModificaDtdecor().setModificaDtdecor(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.getStatusDer().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.setIdRichEst(MarshalByte.readPackedAsInt(buffer, position, Lccc0001Loas0110.Len.Int.ID_RICH_EST, 0));
        position += Lccc0001Loas0110.Len.ID_RICH_EST;
        lccc0001.setCodiceIniziativa(MarshalByte.readString(buffer, position, Lccc0001Loas0110.Len.CODICE_INIZIATIVA));
        position += Lccc0001Loas0110.Len.CODICE_INIZIATIVA;
        lccc0001.getTpVisualizPag().setTpVisualizPag(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.setFlr1(MarshalByte.readString(buffer, position, Lccc0001Loas0110.Len.FLR1));
    }

    public byte[] getS0024AreaStatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, lccc0001.getTipoOperazione().getTipoOperazione(), WcomTipoOperazione.Len.TIPO_OPERAZIONE);
        position += WcomTipoOperazione.Len.TIPO_OPERAZIONE;
        MarshalByte.writeChar(buffer, position, lccc0001.getNavigabilita().getNavigabilita());
        position += Types.CHAR_SIZE;
        lccc0001.getTastiDaAbilitare().getTastiDaAbilitareBytes(buffer, position);
        position += WcomTastiDaAbilitare.Len.TASTI_DA_ABILITARE;
        lccc0001.getDatiActuatorBytes(buffer, position);
        position += Lccc0001Loas0110.Len.DATI_ACTUATOR;
        lccc0001.getStati().getStatiBytes(buffer, position);
        position += WcomStati.Len.STATI;
        lccc0001.getDatiDeroghe().getDatiDerogheBytes(buffer, position);
        position += WcomDatiDeroghe.Len.DATI_DEROGHE;
        lccc0001.getMovimentiAnnullBytes(buffer, position);
        position += Lccc0001Loas0110.Len.MOVIMENTI_ANNULL;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0001.getIdMoviCrz(), Lccc0001Loas0110.Len.Int.ID_MOVI_CRZ, 0);
        position += Lccc0001Loas0110.Len.ID_MOVI_CRZ;
        MarshalByte.writeChar(buffer, position, lccc0001.getFlagTariffaRischio());
        position += Types.CHAR_SIZE;
        lccc0001.getAreaPlatfondBytes(buffer, position);
        position += Lccc0001Loas0110.Len.AREA_PLATFOND;
        MarshalByte.writeChar(buffer, position, lccc0001.getModificaDtdecor().getModificaDtdecor());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, lccc0001.getStatusDer().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0001.getIdRichEst(), Lccc0001Loas0110.Len.Int.ID_RICH_EST, 0);
        position += Lccc0001Loas0110.Len.ID_RICH_EST;
        MarshalByte.writeString(buffer, position, lccc0001.getCodiceIniziativa(), Lccc0001Loas0110.Len.CODICE_INIZIATIVA);
        position += Lccc0001Loas0110.Len.CODICE_INIZIATIVA;
        MarshalByte.writeChar(buffer, position, lccc0001.getTpVisualizPag().getTpVisualizPag());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, lccc0001.getFlr1(), Lccc0001Loas0110.Len.FLR1);
        return buffer;
    }

    @Override
    public byte[] serialize() {
        return getS0024AreaComuneBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int S0024_AREA_STATI = WcomTipoOperazione.Len.TIPO_OPERAZIONE + WcomNavigabilita.Len.NAVIGABILITA + WcomTastiDaAbilitare.Len.TASTI_DA_ABILITARE + Lccc0001Loas0110.Len.DATI_ACTUATOR + WcomStati.Len.STATI + WcomDatiDeroghe.Len.DATI_DEROGHE + Lccc0001Loas0110.Len.MOVIMENTI_ANNULL + Lccc0001Loas0110.Len.ID_MOVI_CRZ + Lccc0001Loas0110.Len.FLAG_TARIFFA_RISCHIO + Lccc0001Loas0110.Len.AREA_PLATFOND + S0024ModificaDtdecor.Len.MODIFICA_DTDECOR + WpolStatus.Len.STATUS + Lccc0001Loas0110.Len.ID_RICH_EST + Lccc0001Loas0110.Len.CODICE_INIZIATIVA + S0024TpVisualizPag.Len.TP_VISUALIZ_PAG + Lccc0001Loas0110.Len.FLR1;
        public static final int S0024_AREA_COMUNE = S0024_AREA_STATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
