package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDAD-PC-AZ<br>
 * Variable: WDAD-PC-AZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadPcAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadPcAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_PC_AZ;
    }

    public void setWdadPcAz(AfDecimal wdadPcAz) {
        writeDecimalAsPacked(Pos.WDAD_PC_AZ, wdadPcAz.copy());
    }

    public void setWdadPcAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_PC_AZ, Pos.WDAD_PC_AZ);
    }

    /**Original name: WDAD-PC-AZ<br>*/
    public AfDecimal getWdadPcAz() {
        return readPackedAsDecimal(Pos.WDAD_PC_AZ, Len.Int.WDAD_PC_AZ, Len.Fract.WDAD_PC_AZ);
    }

    public byte[] getWdadPcAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_PC_AZ, Pos.WDAD_PC_AZ);
        return buffer;
    }

    public void setWdadPcAzNull(String wdadPcAzNull) {
        writeString(Pos.WDAD_PC_AZ_NULL, wdadPcAzNull, Len.WDAD_PC_AZ_NULL);
    }

    /**Original name: WDAD-PC-AZ-NULL<br>*/
    public String getWdadPcAzNull() {
        return readString(Pos.WDAD_PC_AZ_NULL, Len.WDAD_PC_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_PC_AZ = 1;
        public static final int WDAD_PC_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_PC_AZ = 4;
        public static final int WDAD_PC_AZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDAD_PC_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_PC_AZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
