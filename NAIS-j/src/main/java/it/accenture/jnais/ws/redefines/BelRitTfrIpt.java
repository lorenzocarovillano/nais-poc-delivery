package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-RIT-TFR-IPT<br>
 * Variable: BEL-RIT-TFR-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelRitTfrIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelRitTfrIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_RIT_TFR_IPT;
    }

    public void setBelRitTfrIpt(AfDecimal belRitTfrIpt) {
        writeDecimalAsPacked(Pos.BEL_RIT_TFR_IPT, belRitTfrIpt.copy());
    }

    public void setBelRitTfrIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_RIT_TFR_IPT, Pos.BEL_RIT_TFR_IPT);
    }

    /**Original name: BEL-RIT-TFR-IPT<br>*/
    public AfDecimal getBelRitTfrIpt() {
        return readPackedAsDecimal(Pos.BEL_RIT_TFR_IPT, Len.Int.BEL_RIT_TFR_IPT, Len.Fract.BEL_RIT_TFR_IPT);
    }

    public byte[] getBelRitTfrIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_RIT_TFR_IPT, Pos.BEL_RIT_TFR_IPT);
        return buffer;
    }

    public void setBelRitTfrIptNull(String belRitTfrIptNull) {
        writeString(Pos.BEL_RIT_TFR_IPT_NULL, belRitTfrIptNull, Len.BEL_RIT_TFR_IPT_NULL);
    }

    /**Original name: BEL-RIT-TFR-IPT-NULL<br>*/
    public String getBelRitTfrIptNull() {
        return readString(Pos.BEL_RIT_TFR_IPT_NULL, Len.BEL_RIT_TFR_IPT_NULL);
    }

    public String getBelRitTfrIptNullFormatted() {
        return Functions.padBlanks(getBelRitTfrIptNull(), Len.BEL_RIT_TFR_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_RIT_TFR_IPT = 1;
        public static final int BEL_RIT_TFR_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_RIT_TFR_IPT = 8;
        public static final int BEL_RIT_TFR_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_RIT_TFR_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_RIT_TFR_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
