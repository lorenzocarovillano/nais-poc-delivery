package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ALL-PERIODO<br>
 * Variable: ALL-PERIODO from program LCCS1900<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AllPeriodo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AllPeriodo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ALL_PERIODO;
    }

    public void setAllPeriodo(short allPeriodo) {
        writeShortAsPacked(Pos.ALL_PERIODO, allPeriodo, Len.Int.ALL_PERIODO);
    }

    public void setAllPeriodoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ALL_PERIODO, Pos.ALL_PERIODO);
    }

    /**Original name: ALL-PERIODO<br>*/
    public short getAllPeriodo() {
        return readPackedAsShort(Pos.ALL_PERIODO, Len.Int.ALL_PERIODO);
    }

    public byte[] getAllPeriodoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ALL_PERIODO, Pos.ALL_PERIODO);
        return buffer;
    }

    public void setAllPeriodoNull(String allPeriodoNull) {
        writeString(Pos.ALL_PERIODO_NULL, allPeriodoNull, Len.ALL_PERIODO_NULL);
    }

    /**Original name: ALL-PERIODO-NULL<br>*/
    public String getAllPeriodoNull() {
        return readString(Pos.ALL_PERIODO_NULL, Len.ALL_PERIODO_NULL);
    }

    public String getAllPeriodoNullFormatted() {
        return Functions.padBlanks(getAllPeriodoNull(), Len.ALL_PERIODO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ALL_PERIODO = 1;
        public static final int ALL_PERIODO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ALL_PERIODO = 2;
        public static final int ALL_PERIODO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ALL_PERIODO = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
