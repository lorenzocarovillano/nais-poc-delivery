package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LDBV4011<br>
 * Variable: LDBV4011 from copybook LDBV4011<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv4011 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV4011-ID-TIT-CONT
    private int idTitCont = DefaultValues.INT_VAL;
    //Original name: LDBV4011-DT-INI-COP
    private String dtIniCop = DefaultValues.stringVal(Len.DT_INI_COP);
    //Original name: LDBV4011-DT-END-COP
    private String dtEndCop = DefaultValues.stringVal(Len.DT_END_COP);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV4011;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv4011Bytes(buf);
    }

    public String getLdbv4011Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv4011Bytes());
    }

    public void setLdbv4011Bytes(byte[] buffer) {
        setLdbv4011Bytes(buffer, 1);
    }

    public byte[] getLdbv4011Bytes() {
        byte[] buffer = new byte[Len.LDBV4011];
        return getLdbv4011Bytes(buffer, 1);
    }

    public void setLdbv4011Bytes(byte[] buffer, int offset) {
        int position = offset;
        idTitCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_TIT_CONT, 0);
        position += Len.ID_TIT_CONT;
        dtIniCop = MarshalByte.readFixedString(buffer, position, Len.DT_INI_COP);
        position += Len.DT_INI_COP;
        dtEndCop = MarshalByte.readFixedString(buffer, position, Len.DT_END_COP);
    }

    public byte[] getLdbv4011Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idTitCont, Len.Int.ID_TIT_CONT, 0);
        position += Len.ID_TIT_CONT;
        MarshalByte.writeString(buffer, position, dtIniCop, Len.DT_INI_COP);
        position += Len.DT_INI_COP;
        MarshalByte.writeString(buffer, position, dtEndCop, Len.DT_END_COP);
        return buffer;
    }

    public void setIdTitCont(int idTitCont) {
        this.idTitCont = idTitCont;
    }

    public int getIdTitCont() {
        return this.idTitCont;
    }

    public void setDtIniCopFormatted(String dtIniCop) {
        this.dtIniCop = Trunc.toUnsignedNumeric(dtIniCop, Len.DT_INI_COP);
    }

    public int getDtIniCop() {
        return NumericDisplay.asInt(this.dtIniCop);
    }

    public String getDtIniCopFormatted() {
        return this.dtIniCop;
    }

    public void setDtEndCopFormatted(String dtEndCop) {
        this.dtEndCop = Trunc.toUnsignedNumeric(dtEndCop, Len.DT_END_COP);
    }

    public int getDtEndCop() {
        return NumericDisplay.asInt(this.dtEndCop);
    }

    public String getDtEndCopFormatted() {
        return this.dtEndCop;
    }

    @Override
    public byte[] serialize() {
        return getLdbv4011Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_TIT_CONT = 5;
        public static final int DT_INI_COP = 8;
        public static final int DT_END_COP = 8;
        public static final int LDBV4011 = ID_TIT_CONT + DT_INI_COP + DT_END_COP;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_TIT_CONT = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
