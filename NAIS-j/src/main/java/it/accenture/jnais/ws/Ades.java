package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.AdeCumCnbtCap;
import it.accenture.jnais.ws.redefines.AdeDtDecor;
import it.accenture.jnais.ws.redefines.AdeDtDecorPrestBan;
import it.accenture.jnais.ws.redefines.AdeDtEffVarzStatT;
import it.accenture.jnais.ws.redefines.AdeDtNovaRgmFisc;
import it.accenture.jnais.ws.redefines.AdeDtPresc;
import it.accenture.jnais.ws.redefines.AdeDtScad;
import it.accenture.jnais.ws.redefines.AdeDtUltConsCnbt;
import it.accenture.jnais.ws.redefines.AdeDtVarzTpIas;
import it.accenture.jnais.ws.redefines.AdeDurAa;
import it.accenture.jnais.ws.redefines.AdeDurGg;
import it.accenture.jnais.ws.redefines.AdeDurMm;
import it.accenture.jnais.ws.redefines.AdeEtaAScad;
import it.accenture.jnais.ws.redefines.AdeIdMoviChiu;
import it.accenture.jnais.ws.redefines.AdeImpAder;
import it.accenture.jnais.ws.redefines.AdeImpAz;
import it.accenture.jnais.ws.redefines.AdeImpbVisDaRec;
import it.accenture.jnais.ws.redefines.AdeImpGarCnbt;
import it.accenture.jnais.ws.redefines.AdeImpRecRitAcc;
import it.accenture.jnais.ws.redefines.AdeImpRecRitVis;
import it.accenture.jnais.ws.redefines.AdeImpTfr;
import it.accenture.jnais.ws.redefines.AdeImpVolo;
import it.accenture.jnais.ws.redefines.AdeNumRatPian;
import it.accenture.jnais.ws.redefines.AdePcAder;
import it.accenture.jnais.ws.redefines.AdePcAz;
import it.accenture.jnais.ws.redefines.AdePcTfr;
import it.accenture.jnais.ws.redefines.AdePcVolo;
import it.accenture.jnais.ws.redefines.AdePreLrdInd;
import it.accenture.jnais.ws.redefines.AdePreNetInd;
import it.accenture.jnais.ws.redefines.AdePrstzIniInd;
import it.accenture.jnais.ws.redefines.AdeRatLrdInd;

/**Original name: ADES<br>
 * Variable: ADES from copybook IDBVADE1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ades extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: ADE-ID-ADES
    private int adeIdAdes = DefaultValues.INT_VAL;
    //Original name: ADE-ID-POLI
    private int adeIdPoli = DefaultValues.INT_VAL;
    //Original name: ADE-ID-MOVI-CRZ
    private int adeIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: ADE-ID-MOVI-CHIU
    private AdeIdMoviChiu adeIdMoviChiu = new AdeIdMoviChiu();
    //Original name: ADE-DT-INI-EFF
    private int adeDtIniEff = DefaultValues.INT_VAL;
    //Original name: ADE-DT-END-EFF
    private int adeDtEndEff = DefaultValues.INT_VAL;
    //Original name: ADE-IB-PREV
    private String adeIbPrev = DefaultValues.stringVal(Len.ADE_IB_PREV);
    //Original name: ADE-IB-OGG
    private String adeIbOgg = DefaultValues.stringVal(Len.ADE_IB_OGG);
    //Original name: ADE-COD-COMP-ANIA
    private int adeCodCompAnia = DefaultValues.INT_VAL;
    //Original name: ADE-DT-DECOR
    private AdeDtDecor adeDtDecor = new AdeDtDecor();
    //Original name: ADE-DT-SCAD
    private AdeDtScad adeDtScad = new AdeDtScad();
    //Original name: ADE-ETA-A-SCAD
    private AdeEtaAScad adeEtaAScad = new AdeEtaAScad();
    //Original name: ADE-DUR-AA
    private AdeDurAa adeDurAa = new AdeDurAa();
    //Original name: ADE-DUR-MM
    private AdeDurMm adeDurMm = new AdeDurMm();
    //Original name: ADE-DUR-GG
    private AdeDurGg adeDurGg = new AdeDurGg();
    //Original name: ADE-TP-RGM-FISC
    private String adeTpRgmFisc = DefaultValues.stringVal(Len.ADE_TP_RGM_FISC);
    //Original name: ADE-TP-RIAT
    private String adeTpRiat = DefaultValues.stringVal(Len.ADE_TP_RIAT);
    //Original name: ADE-TP-MOD-PAG-TIT
    private String adeTpModPagTit = DefaultValues.stringVal(Len.ADE_TP_MOD_PAG_TIT);
    //Original name: ADE-TP-IAS
    private String adeTpIas = DefaultValues.stringVal(Len.ADE_TP_IAS);
    //Original name: ADE-DT-VARZ-TP-IAS
    private AdeDtVarzTpIas adeDtVarzTpIas = new AdeDtVarzTpIas();
    //Original name: ADE-PRE-NET-IND
    private AdePreNetInd adePreNetInd = new AdePreNetInd();
    //Original name: ADE-PRE-LRD-IND
    private AdePreLrdInd adePreLrdInd = new AdePreLrdInd();
    //Original name: ADE-RAT-LRD-IND
    private AdeRatLrdInd adeRatLrdInd = new AdeRatLrdInd();
    //Original name: ADE-PRSTZ-INI-IND
    private AdePrstzIniInd adePrstzIniInd = new AdePrstzIniInd();
    //Original name: ADE-FL-COINC-ASSTO
    private char adeFlCoincAssto = DefaultValues.CHAR_VAL;
    //Original name: ADE-IB-DFLT
    private String adeIbDflt = DefaultValues.stringVal(Len.ADE_IB_DFLT);
    //Original name: ADE-MOD-CALC
    private String adeModCalc = DefaultValues.stringVal(Len.ADE_MOD_CALC);
    //Original name: ADE-TP-FNT-CNBTVA
    private String adeTpFntCnbtva = DefaultValues.stringVal(Len.ADE_TP_FNT_CNBTVA);
    //Original name: ADE-IMP-AZ
    private AdeImpAz adeImpAz = new AdeImpAz();
    //Original name: ADE-IMP-ADER
    private AdeImpAder adeImpAder = new AdeImpAder();
    //Original name: ADE-IMP-TFR
    private AdeImpTfr adeImpTfr = new AdeImpTfr();
    //Original name: ADE-IMP-VOLO
    private AdeImpVolo adeImpVolo = new AdeImpVolo();
    //Original name: ADE-PC-AZ
    private AdePcAz adePcAz = new AdePcAz();
    //Original name: ADE-PC-ADER
    private AdePcAder adePcAder = new AdePcAder();
    //Original name: ADE-PC-TFR
    private AdePcTfr adePcTfr = new AdePcTfr();
    //Original name: ADE-PC-VOLO
    private AdePcVolo adePcVolo = new AdePcVolo();
    //Original name: ADE-DT-NOVA-RGM-FISC
    private AdeDtNovaRgmFisc adeDtNovaRgmFisc = new AdeDtNovaRgmFisc();
    //Original name: ADE-FL-ATTIV
    private char adeFlAttiv = DefaultValues.CHAR_VAL;
    //Original name: ADE-IMP-REC-RIT-VIS
    private AdeImpRecRitVis adeImpRecRitVis = new AdeImpRecRitVis();
    //Original name: ADE-IMP-REC-RIT-ACC
    private AdeImpRecRitAcc adeImpRecRitAcc = new AdeImpRecRitAcc();
    //Original name: ADE-FL-VARZ-STAT-TBGC
    private char adeFlVarzStatTbgc = DefaultValues.CHAR_VAL;
    //Original name: ADE-FL-PROVZA-MIGRAZ
    private char adeFlProvzaMigraz = DefaultValues.CHAR_VAL;
    //Original name: ADE-IMPB-VIS-DA-REC
    private AdeImpbVisDaRec adeImpbVisDaRec = new AdeImpbVisDaRec();
    //Original name: ADE-DT-DECOR-PREST-BAN
    private AdeDtDecorPrestBan adeDtDecorPrestBan = new AdeDtDecorPrestBan();
    //Original name: ADE-DT-EFF-VARZ-STAT-T
    private AdeDtEffVarzStatT adeDtEffVarzStatT = new AdeDtEffVarzStatT();
    //Original name: ADE-DS-RIGA
    private long adeDsRiga = DefaultValues.LONG_VAL;
    //Original name: ADE-DS-OPER-SQL
    private char adeDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: ADE-DS-VER
    private int adeDsVer = DefaultValues.INT_VAL;
    //Original name: ADE-DS-TS-INI-CPTZ
    private long adeDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: ADE-DS-TS-END-CPTZ
    private long adeDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: ADE-DS-UTENTE
    private String adeDsUtente = DefaultValues.stringVal(Len.ADE_DS_UTENTE);
    //Original name: ADE-DS-STATO-ELAB
    private char adeDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: ADE-CUM-CNBT-CAP
    private AdeCumCnbtCap adeCumCnbtCap = new AdeCumCnbtCap();
    //Original name: ADE-IMP-GAR-CNBT
    private AdeImpGarCnbt adeImpGarCnbt = new AdeImpGarCnbt();
    //Original name: ADE-DT-ULT-CONS-CNBT
    private AdeDtUltConsCnbt adeDtUltConsCnbt = new AdeDtUltConsCnbt();
    //Original name: ADE-IDEN-ISC-FND
    private String adeIdenIscFnd = DefaultValues.stringVal(Len.ADE_IDEN_ISC_FND);
    //Original name: ADE-NUM-RAT-PIAN
    private AdeNumRatPian adeNumRatPian = new AdeNumRatPian();
    //Original name: ADE-DT-PRESC
    private AdeDtPresc adeDtPresc = new AdeDtPresc();
    //Original name: ADE-CONCS-PREST
    private char adeConcsPrest = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADES;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAdesBytes(buf);
    }

    public void setAdesFormatted(String data) {
        byte[] buffer = new byte[Len.ADES];
        MarshalByte.writeString(buffer, 1, data, Len.ADES);
        setAdesBytes(buffer, 1);
    }

    public String getAdesFormatted() {
        return MarshalByteExt.bufferToStr(getAdesBytes());
    }

    public void setAdesBytes(byte[] buffer) {
        setAdesBytes(buffer, 1);
    }

    public byte[] getAdesBytes() {
        byte[] buffer = new byte[Len.ADES];
        return getAdesBytes(buffer, 1);
    }

    public void setAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        adeIdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ADE_ID_ADES, 0);
        position += Len.ADE_ID_ADES;
        adeIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ADE_ID_POLI, 0);
        position += Len.ADE_ID_POLI;
        adeIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ADE_ID_MOVI_CRZ, 0);
        position += Len.ADE_ID_MOVI_CRZ;
        adeIdMoviChiu.setAdeIdMoviChiuFromBuffer(buffer, position);
        position += AdeIdMoviChiu.Len.ADE_ID_MOVI_CHIU;
        adeDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ADE_DT_INI_EFF, 0);
        position += Len.ADE_DT_INI_EFF;
        adeDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ADE_DT_END_EFF, 0);
        position += Len.ADE_DT_END_EFF;
        adeIbPrev = MarshalByte.readString(buffer, position, Len.ADE_IB_PREV);
        position += Len.ADE_IB_PREV;
        adeIbOgg = MarshalByte.readString(buffer, position, Len.ADE_IB_OGG);
        position += Len.ADE_IB_OGG;
        adeCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ADE_COD_COMP_ANIA, 0);
        position += Len.ADE_COD_COMP_ANIA;
        adeDtDecor.setAdeDtDecorFromBuffer(buffer, position);
        position += AdeDtDecor.Len.ADE_DT_DECOR;
        adeDtScad.setAdeDtScadFromBuffer(buffer, position);
        position += AdeDtScad.Len.ADE_DT_SCAD;
        adeEtaAScad.setAdeEtaAScadFromBuffer(buffer, position);
        position += AdeEtaAScad.Len.ADE_ETA_A_SCAD;
        adeDurAa.setAdeDurAaFromBuffer(buffer, position);
        position += AdeDurAa.Len.ADE_DUR_AA;
        adeDurMm.setAdeDurMmFromBuffer(buffer, position);
        position += AdeDurMm.Len.ADE_DUR_MM;
        adeDurGg.setAdeDurGgFromBuffer(buffer, position);
        position += AdeDurGg.Len.ADE_DUR_GG;
        adeTpRgmFisc = MarshalByte.readString(buffer, position, Len.ADE_TP_RGM_FISC);
        position += Len.ADE_TP_RGM_FISC;
        adeTpRiat = MarshalByte.readString(buffer, position, Len.ADE_TP_RIAT);
        position += Len.ADE_TP_RIAT;
        adeTpModPagTit = MarshalByte.readString(buffer, position, Len.ADE_TP_MOD_PAG_TIT);
        position += Len.ADE_TP_MOD_PAG_TIT;
        adeTpIas = MarshalByte.readString(buffer, position, Len.ADE_TP_IAS);
        position += Len.ADE_TP_IAS;
        adeDtVarzTpIas.setAdeDtVarzTpIasFromBuffer(buffer, position);
        position += AdeDtVarzTpIas.Len.ADE_DT_VARZ_TP_IAS;
        adePreNetInd.setAdePreNetIndFromBuffer(buffer, position);
        position += AdePreNetInd.Len.ADE_PRE_NET_IND;
        adePreLrdInd.setAdePreLrdIndFromBuffer(buffer, position);
        position += AdePreLrdInd.Len.ADE_PRE_LRD_IND;
        adeRatLrdInd.setAdeRatLrdIndFromBuffer(buffer, position);
        position += AdeRatLrdInd.Len.ADE_RAT_LRD_IND;
        adePrstzIniInd.setAdePrstzIniIndFromBuffer(buffer, position);
        position += AdePrstzIniInd.Len.ADE_PRSTZ_INI_IND;
        adeFlCoincAssto = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        adeIbDflt = MarshalByte.readString(buffer, position, Len.ADE_IB_DFLT);
        position += Len.ADE_IB_DFLT;
        adeModCalc = MarshalByte.readString(buffer, position, Len.ADE_MOD_CALC);
        position += Len.ADE_MOD_CALC;
        adeTpFntCnbtva = MarshalByte.readString(buffer, position, Len.ADE_TP_FNT_CNBTVA);
        position += Len.ADE_TP_FNT_CNBTVA;
        adeImpAz.setAdeImpAzFromBuffer(buffer, position);
        position += AdeImpAz.Len.ADE_IMP_AZ;
        adeImpAder.setAdeImpAderFromBuffer(buffer, position);
        position += AdeImpAder.Len.ADE_IMP_ADER;
        adeImpTfr.setAdeImpTfrFromBuffer(buffer, position);
        position += AdeImpTfr.Len.ADE_IMP_TFR;
        adeImpVolo.setAdeImpVoloFromBuffer(buffer, position);
        position += AdeImpVolo.Len.ADE_IMP_VOLO;
        adePcAz.setAdePcAzFromBuffer(buffer, position);
        position += AdePcAz.Len.ADE_PC_AZ;
        adePcAder.setAdePcAderFromBuffer(buffer, position);
        position += AdePcAder.Len.ADE_PC_ADER;
        adePcTfr.setAdePcTfrFromBuffer(buffer, position);
        position += AdePcTfr.Len.ADE_PC_TFR;
        adePcVolo.setAdePcVoloFromBuffer(buffer, position);
        position += AdePcVolo.Len.ADE_PC_VOLO;
        adeDtNovaRgmFisc.setAdeDtNovaRgmFiscFromBuffer(buffer, position);
        position += AdeDtNovaRgmFisc.Len.ADE_DT_NOVA_RGM_FISC;
        adeFlAttiv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        adeImpRecRitVis.setAdeImpRecRitVisFromBuffer(buffer, position);
        position += AdeImpRecRitVis.Len.ADE_IMP_REC_RIT_VIS;
        adeImpRecRitAcc.setAdeImpRecRitAccFromBuffer(buffer, position);
        position += AdeImpRecRitAcc.Len.ADE_IMP_REC_RIT_ACC;
        adeFlVarzStatTbgc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        adeFlProvzaMigraz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        adeImpbVisDaRec.setAdeImpbVisDaRecFromBuffer(buffer, position);
        position += AdeImpbVisDaRec.Len.ADE_IMPB_VIS_DA_REC;
        adeDtDecorPrestBan.setAdeDtDecorPrestBanFromBuffer(buffer, position);
        position += AdeDtDecorPrestBan.Len.ADE_DT_DECOR_PREST_BAN;
        adeDtEffVarzStatT.setAdeDtEffVarzStatTFromBuffer(buffer, position);
        position += AdeDtEffVarzStatT.Len.ADE_DT_EFF_VARZ_STAT_T;
        adeDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ADE_DS_RIGA, 0);
        position += Len.ADE_DS_RIGA;
        adeDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        adeDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ADE_DS_VER, 0);
        position += Len.ADE_DS_VER;
        adeDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ADE_DS_TS_INI_CPTZ, 0);
        position += Len.ADE_DS_TS_INI_CPTZ;
        adeDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ADE_DS_TS_END_CPTZ, 0);
        position += Len.ADE_DS_TS_END_CPTZ;
        adeDsUtente = MarshalByte.readString(buffer, position, Len.ADE_DS_UTENTE);
        position += Len.ADE_DS_UTENTE;
        adeDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        adeCumCnbtCap.setAdeCumCnbtCapFromBuffer(buffer, position);
        position += AdeCumCnbtCap.Len.ADE_CUM_CNBT_CAP;
        adeImpGarCnbt.setAdeImpGarCnbtFromBuffer(buffer, position);
        position += AdeImpGarCnbt.Len.ADE_IMP_GAR_CNBT;
        adeDtUltConsCnbt.setAdeDtUltConsCnbtFromBuffer(buffer, position);
        position += AdeDtUltConsCnbt.Len.ADE_DT_ULT_CONS_CNBT;
        adeIdenIscFnd = MarshalByte.readString(buffer, position, Len.ADE_IDEN_ISC_FND);
        position += Len.ADE_IDEN_ISC_FND;
        adeNumRatPian.setAdeNumRatPianFromBuffer(buffer, position);
        position += AdeNumRatPian.Len.ADE_NUM_RAT_PIAN;
        adeDtPresc.setAdeDtPrescFromBuffer(buffer, position);
        position += AdeDtPresc.Len.ADE_DT_PRESC;
        adeConcsPrest = MarshalByte.readChar(buffer, position);
    }

    public byte[] getAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, adeIdAdes, Len.Int.ADE_ID_ADES, 0);
        position += Len.ADE_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, adeIdPoli, Len.Int.ADE_ID_POLI, 0);
        position += Len.ADE_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, adeIdMoviCrz, Len.Int.ADE_ID_MOVI_CRZ, 0);
        position += Len.ADE_ID_MOVI_CRZ;
        adeIdMoviChiu.getAdeIdMoviChiuAsBuffer(buffer, position);
        position += AdeIdMoviChiu.Len.ADE_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, adeDtIniEff, Len.Int.ADE_DT_INI_EFF, 0);
        position += Len.ADE_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, adeDtEndEff, Len.Int.ADE_DT_END_EFF, 0);
        position += Len.ADE_DT_END_EFF;
        MarshalByte.writeString(buffer, position, adeIbPrev, Len.ADE_IB_PREV);
        position += Len.ADE_IB_PREV;
        MarshalByte.writeString(buffer, position, adeIbOgg, Len.ADE_IB_OGG);
        position += Len.ADE_IB_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, adeCodCompAnia, Len.Int.ADE_COD_COMP_ANIA, 0);
        position += Len.ADE_COD_COMP_ANIA;
        adeDtDecor.getAdeDtDecorAsBuffer(buffer, position);
        position += AdeDtDecor.Len.ADE_DT_DECOR;
        adeDtScad.getAdeDtScadAsBuffer(buffer, position);
        position += AdeDtScad.Len.ADE_DT_SCAD;
        adeEtaAScad.getAdeEtaAScadAsBuffer(buffer, position);
        position += AdeEtaAScad.Len.ADE_ETA_A_SCAD;
        adeDurAa.getAdeDurAaAsBuffer(buffer, position);
        position += AdeDurAa.Len.ADE_DUR_AA;
        adeDurMm.getAdeDurMmAsBuffer(buffer, position);
        position += AdeDurMm.Len.ADE_DUR_MM;
        adeDurGg.getAdeDurGgAsBuffer(buffer, position);
        position += AdeDurGg.Len.ADE_DUR_GG;
        MarshalByte.writeString(buffer, position, adeTpRgmFisc, Len.ADE_TP_RGM_FISC);
        position += Len.ADE_TP_RGM_FISC;
        MarshalByte.writeString(buffer, position, adeTpRiat, Len.ADE_TP_RIAT);
        position += Len.ADE_TP_RIAT;
        MarshalByte.writeString(buffer, position, adeTpModPagTit, Len.ADE_TP_MOD_PAG_TIT);
        position += Len.ADE_TP_MOD_PAG_TIT;
        MarshalByte.writeString(buffer, position, adeTpIas, Len.ADE_TP_IAS);
        position += Len.ADE_TP_IAS;
        adeDtVarzTpIas.getAdeDtVarzTpIasAsBuffer(buffer, position);
        position += AdeDtVarzTpIas.Len.ADE_DT_VARZ_TP_IAS;
        adePreNetInd.getAdePreNetIndAsBuffer(buffer, position);
        position += AdePreNetInd.Len.ADE_PRE_NET_IND;
        adePreLrdInd.getAdePreLrdIndAsBuffer(buffer, position);
        position += AdePreLrdInd.Len.ADE_PRE_LRD_IND;
        adeRatLrdInd.getAdeRatLrdIndAsBuffer(buffer, position);
        position += AdeRatLrdInd.Len.ADE_RAT_LRD_IND;
        adePrstzIniInd.getAdePrstzIniIndAsBuffer(buffer, position);
        position += AdePrstzIniInd.Len.ADE_PRSTZ_INI_IND;
        MarshalByte.writeChar(buffer, position, adeFlCoincAssto);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, adeIbDflt, Len.ADE_IB_DFLT);
        position += Len.ADE_IB_DFLT;
        MarshalByte.writeString(buffer, position, adeModCalc, Len.ADE_MOD_CALC);
        position += Len.ADE_MOD_CALC;
        MarshalByte.writeString(buffer, position, adeTpFntCnbtva, Len.ADE_TP_FNT_CNBTVA);
        position += Len.ADE_TP_FNT_CNBTVA;
        adeImpAz.getAdeImpAzAsBuffer(buffer, position);
        position += AdeImpAz.Len.ADE_IMP_AZ;
        adeImpAder.getAdeImpAderAsBuffer(buffer, position);
        position += AdeImpAder.Len.ADE_IMP_ADER;
        adeImpTfr.getAdeImpTfrAsBuffer(buffer, position);
        position += AdeImpTfr.Len.ADE_IMP_TFR;
        adeImpVolo.getAdeImpVoloAsBuffer(buffer, position);
        position += AdeImpVolo.Len.ADE_IMP_VOLO;
        adePcAz.getAdePcAzAsBuffer(buffer, position);
        position += AdePcAz.Len.ADE_PC_AZ;
        adePcAder.getAdePcAderAsBuffer(buffer, position);
        position += AdePcAder.Len.ADE_PC_ADER;
        adePcTfr.getAdePcTfrAsBuffer(buffer, position);
        position += AdePcTfr.Len.ADE_PC_TFR;
        adePcVolo.getAdePcVoloAsBuffer(buffer, position);
        position += AdePcVolo.Len.ADE_PC_VOLO;
        adeDtNovaRgmFisc.getAdeDtNovaRgmFiscAsBuffer(buffer, position);
        position += AdeDtNovaRgmFisc.Len.ADE_DT_NOVA_RGM_FISC;
        MarshalByte.writeChar(buffer, position, adeFlAttiv);
        position += Types.CHAR_SIZE;
        adeImpRecRitVis.getAdeImpRecRitVisAsBuffer(buffer, position);
        position += AdeImpRecRitVis.Len.ADE_IMP_REC_RIT_VIS;
        adeImpRecRitAcc.getAdeImpRecRitAccAsBuffer(buffer, position);
        position += AdeImpRecRitAcc.Len.ADE_IMP_REC_RIT_ACC;
        MarshalByte.writeChar(buffer, position, adeFlVarzStatTbgc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, adeFlProvzaMigraz);
        position += Types.CHAR_SIZE;
        adeImpbVisDaRec.getAdeImpbVisDaRecAsBuffer(buffer, position);
        position += AdeImpbVisDaRec.Len.ADE_IMPB_VIS_DA_REC;
        adeDtDecorPrestBan.getAdeDtDecorPrestBanAsBuffer(buffer, position);
        position += AdeDtDecorPrestBan.Len.ADE_DT_DECOR_PREST_BAN;
        adeDtEffVarzStatT.getAdeDtEffVarzStatTAsBuffer(buffer, position);
        position += AdeDtEffVarzStatT.Len.ADE_DT_EFF_VARZ_STAT_T;
        MarshalByte.writeLongAsPacked(buffer, position, adeDsRiga, Len.Int.ADE_DS_RIGA, 0);
        position += Len.ADE_DS_RIGA;
        MarshalByte.writeChar(buffer, position, adeDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, adeDsVer, Len.Int.ADE_DS_VER, 0);
        position += Len.ADE_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, adeDsTsIniCptz, Len.Int.ADE_DS_TS_INI_CPTZ, 0);
        position += Len.ADE_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, adeDsTsEndCptz, Len.Int.ADE_DS_TS_END_CPTZ, 0);
        position += Len.ADE_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, adeDsUtente, Len.ADE_DS_UTENTE);
        position += Len.ADE_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, adeDsStatoElab);
        position += Types.CHAR_SIZE;
        adeCumCnbtCap.getAdeCumCnbtCapAsBuffer(buffer, position);
        position += AdeCumCnbtCap.Len.ADE_CUM_CNBT_CAP;
        adeImpGarCnbt.getAdeImpGarCnbtAsBuffer(buffer, position);
        position += AdeImpGarCnbt.Len.ADE_IMP_GAR_CNBT;
        adeDtUltConsCnbt.getAdeDtUltConsCnbtAsBuffer(buffer, position);
        position += AdeDtUltConsCnbt.Len.ADE_DT_ULT_CONS_CNBT;
        MarshalByte.writeString(buffer, position, adeIdenIscFnd, Len.ADE_IDEN_ISC_FND);
        position += Len.ADE_IDEN_ISC_FND;
        adeNumRatPian.getAdeNumRatPianAsBuffer(buffer, position);
        position += AdeNumRatPian.Len.ADE_NUM_RAT_PIAN;
        adeDtPresc.getAdeDtPrescAsBuffer(buffer, position);
        position += AdeDtPresc.Len.ADE_DT_PRESC;
        MarshalByte.writeChar(buffer, position, adeConcsPrest);
        return buffer;
    }

    public void setAdeIdAdes(int adeIdAdes) {
        this.adeIdAdes = adeIdAdes;
    }

    public int getAdeIdAdes() {
        return this.adeIdAdes;
    }

    public String getAdeIdAdesFormatted() {
        return PicFormatter.display(new PicParams("S9(9)V").setUsage(PicUsage.PACKED)).format(getAdeIdAdes()).toString();
    }

    public void setAdeIdPoli(int adeIdPoli) {
        this.adeIdPoli = adeIdPoli;
    }

    public int getAdeIdPoli() {
        return this.adeIdPoli;
    }

    public void setAdeIdMoviCrz(int adeIdMoviCrz) {
        this.adeIdMoviCrz = adeIdMoviCrz;
    }

    public int getAdeIdMoviCrz() {
        return this.adeIdMoviCrz;
    }

    public void setAdeDtIniEff(int adeDtIniEff) {
        this.adeDtIniEff = adeDtIniEff;
    }

    public int getAdeDtIniEff() {
        return this.adeDtIniEff;
    }

    public void setAdeDtEndEff(int adeDtEndEff) {
        this.adeDtEndEff = adeDtEndEff;
    }

    public int getAdeDtEndEff() {
        return this.adeDtEndEff;
    }

    public void setAdeIbPrev(String adeIbPrev) {
        this.adeIbPrev = Functions.subString(adeIbPrev, Len.ADE_IB_PREV);
    }

    public String getAdeIbPrev() {
        return this.adeIbPrev;
    }

    public void setAdeIbOgg(String adeIbOgg) {
        this.adeIbOgg = Functions.subString(adeIbOgg, Len.ADE_IB_OGG);
    }

    public String getAdeIbOgg() {
        return this.adeIbOgg;
    }

    public void setAdeCodCompAnia(int adeCodCompAnia) {
        this.adeCodCompAnia = adeCodCompAnia;
    }

    public int getAdeCodCompAnia() {
        return this.adeCodCompAnia;
    }

    public void setAdeTpRgmFisc(String adeTpRgmFisc) {
        this.adeTpRgmFisc = Functions.subString(adeTpRgmFisc, Len.ADE_TP_RGM_FISC);
    }

    public String getAdeTpRgmFisc() {
        return this.adeTpRgmFisc;
    }

    public void setAdeTpRiat(String adeTpRiat) {
        this.adeTpRiat = Functions.subString(adeTpRiat, Len.ADE_TP_RIAT);
    }

    public String getAdeTpRiat() {
        return this.adeTpRiat;
    }

    public String getAdeTpRiatFormatted() {
        return Functions.padBlanks(getAdeTpRiat(), Len.ADE_TP_RIAT);
    }

    public void setAdeTpModPagTit(String adeTpModPagTit) {
        this.adeTpModPagTit = Functions.subString(adeTpModPagTit, Len.ADE_TP_MOD_PAG_TIT);
    }

    public String getAdeTpModPagTit() {
        return this.adeTpModPagTit;
    }

    public void setAdeTpIas(String adeTpIas) {
        this.adeTpIas = Functions.subString(adeTpIas, Len.ADE_TP_IAS);
    }

    public String getAdeTpIas() {
        return this.adeTpIas;
    }

    public String getAdeTpIasFormatted() {
        return Functions.padBlanks(getAdeTpIas(), Len.ADE_TP_IAS);
    }

    public void setAdeFlCoincAssto(char adeFlCoincAssto) {
        this.adeFlCoincAssto = adeFlCoincAssto;
    }

    public char getAdeFlCoincAssto() {
        return this.adeFlCoincAssto;
    }

    public void setAdeIbDflt(String adeIbDflt) {
        this.adeIbDflt = Functions.subString(adeIbDflt, Len.ADE_IB_DFLT);
    }

    public String getAdeIbDflt() {
        return this.adeIbDflt;
    }

    public void setAdeModCalc(String adeModCalc) {
        this.adeModCalc = Functions.subString(adeModCalc, Len.ADE_MOD_CALC);
    }

    public String getAdeModCalc() {
        return this.adeModCalc;
    }

    public String getAdeModCalcFormatted() {
        return Functions.padBlanks(getAdeModCalc(), Len.ADE_MOD_CALC);
    }

    public void setAdeTpFntCnbtva(String adeTpFntCnbtva) {
        this.adeTpFntCnbtva = Functions.subString(adeTpFntCnbtva, Len.ADE_TP_FNT_CNBTVA);
    }

    public String getAdeTpFntCnbtva() {
        return this.adeTpFntCnbtva;
    }

    public String getAdeTpFntCnbtvaFormatted() {
        return Functions.padBlanks(getAdeTpFntCnbtva(), Len.ADE_TP_FNT_CNBTVA);
    }

    public void setAdeFlAttiv(char adeFlAttiv) {
        this.adeFlAttiv = adeFlAttiv;
    }

    public char getAdeFlAttiv() {
        return this.adeFlAttiv;
    }

    public void setAdeFlVarzStatTbgc(char adeFlVarzStatTbgc) {
        this.adeFlVarzStatTbgc = adeFlVarzStatTbgc;
    }

    public char getAdeFlVarzStatTbgc() {
        return this.adeFlVarzStatTbgc;
    }

    public void setAdeFlProvzaMigraz(char adeFlProvzaMigraz) {
        this.adeFlProvzaMigraz = adeFlProvzaMigraz;
    }

    public char getAdeFlProvzaMigraz() {
        return this.adeFlProvzaMigraz;
    }

    public void setAdeDsRiga(long adeDsRiga) {
        this.adeDsRiga = adeDsRiga;
    }

    public long getAdeDsRiga() {
        return this.adeDsRiga;
    }

    public void setAdeDsOperSql(char adeDsOperSql) {
        this.adeDsOperSql = adeDsOperSql;
    }

    public void setAdeDsOperSqlFormatted(String adeDsOperSql) {
        setAdeDsOperSql(Functions.charAt(adeDsOperSql, Types.CHAR_SIZE));
    }

    public char getAdeDsOperSql() {
        return this.adeDsOperSql;
    }

    public void setAdeDsVer(int adeDsVer) {
        this.adeDsVer = adeDsVer;
    }

    public int getAdeDsVer() {
        return this.adeDsVer;
    }

    public void setAdeDsTsIniCptz(long adeDsTsIniCptz) {
        this.adeDsTsIniCptz = adeDsTsIniCptz;
    }

    public long getAdeDsTsIniCptz() {
        return this.adeDsTsIniCptz;
    }

    public void setAdeDsTsEndCptz(long adeDsTsEndCptz) {
        this.adeDsTsEndCptz = adeDsTsEndCptz;
    }

    public long getAdeDsTsEndCptz() {
        return this.adeDsTsEndCptz;
    }

    public void setAdeDsUtente(String adeDsUtente) {
        this.adeDsUtente = Functions.subString(adeDsUtente, Len.ADE_DS_UTENTE);
    }

    public String getAdeDsUtente() {
        return this.adeDsUtente;
    }

    public void setAdeDsStatoElab(char adeDsStatoElab) {
        this.adeDsStatoElab = adeDsStatoElab;
    }

    public void setAdeDsStatoElabFormatted(String adeDsStatoElab) {
        setAdeDsStatoElab(Functions.charAt(adeDsStatoElab, Types.CHAR_SIZE));
    }

    public char getAdeDsStatoElab() {
        return this.adeDsStatoElab;
    }

    public void setAdeIdenIscFnd(String adeIdenIscFnd) {
        this.adeIdenIscFnd = Functions.subString(adeIdenIscFnd, Len.ADE_IDEN_ISC_FND);
    }

    public String getAdeIdenIscFnd() {
        return this.adeIdenIscFnd;
    }

    public void setAdeConcsPrest(char adeConcsPrest) {
        this.adeConcsPrest = adeConcsPrest;
    }

    public char getAdeConcsPrest() {
        return this.adeConcsPrest;
    }

    public AdeCumCnbtCap getAdeCumCnbtCap() {
        return adeCumCnbtCap;
    }

    public AdeDtDecor getAdeDtDecor() {
        return adeDtDecor;
    }

    public AdeDtDecorPrestBan getAdeDtDecorPrestBan() {
        return adeDtDecorPrestBan;
    }

    public AdeDtEffVarzStatT getAdeDtEffVarzStatT() {
        return adeDtEffVarzStatT;
    }

    public AdeDtNovaRgmFisc getAdeDtNovaRgmFisc() {
        return adeDtNovaRgmFisc;
    }

    public AdeDtPresc getAdeDtPresc() {
        return adeDtPresc;
    }

    public AdeDtScad getAdeDtScad() {
        return adeDtScad;
    }

    public AdeDtUltConsCnbt getAdeDtUltConsCnbt() {
        return adeDtUltConsCnbt;
    }

    public AdeDtVarzTpIas getAdeDtVarzTpIas() {
        return adeDtVarzTpIas;
    }

    public AdeDurAa getAdeDurAa() {
        return adeDurAa;
    }

    public AdeDurGg getAdeDurGg() {
        return adeDurGg;
    }

    public AdeDurMm getAdeDurMm() {
        return adeDurMm;
    }

    public AdeEtaAScad getAdeEtaAScad() {
        return adeEtaAScad;
    }

    public AdeIdMoviChiu getAdeIdMoviChiu() {
        return adeIdMoviChiu;
    }

    public AdeImpAder getAdeImpAder() {
        return adeImpAder;
    }

    public AdeImpAz getAdeImpAz() {
        return adeImpAz;
    }

    public AdeImpGarCnbt getAdeImpGarCnbt() {
        return adeImpGarCnbt;
    }

    public AdeImpRecRitAcc getAdeImpRecRitAcc() {
        return adeImpRecRitAcc;
    }

    public AdeImpRecRitVis getAdeImpRecRitVis() {
        return adeImpRecRitVis;
    }

    public AdeImpTfr getAdeImpTfr() {
        return adeImpTfr;
    }

    public AdeImpVolo getAdeImpVolo() {
        return adeImpVolo;
    }

    public AdeImpbVisDaRec getAdeImpbVisDaRec() {
        return adeImpbVisDaRec;
    }

    public AdeNumRatPian getAdeNumRatPian() {
        return adeNumRatPian;
    }

    public AdePcAder getAdePcAder() {
        return adePcAder;
    }

    public AdePcAz getAdePcAz() {
        return adePcAz;
    }

    public AdePcTfr getAdePcTfr() {
        return adePcTfr;
    }

    public AdePcVolo getAdePcVolo() {
        return adePcVolo;
    }

    public AdePreLrdInd getAdePreLrdInd() {
        return adePreLrdInd;
    }

    public AdePreNetInd getAdePreNetInd() {
        return adePreNetInd;
    }

    public AdePrstzIniInd getAdePrstzIniInd() {
        return adePrstzIniInd;
    }

    public AdeRatLrdInd getAdeRatLrdInd() {
        return adeRatLrdInd;
    }

    @Override
    public byte[] serialize() {
        return getAdesBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_ID_ADES = 5;
        public static final int ADE_ID_POLI = 5;
        public static final int ADE_ID_MOVI_CRZ = 5;
        public static final int ADE_DT_INI_EFF = 5;
        public static final int ADE_DT_END_EFF = 5;
        public static final int ADE_IB_PREV = 40;
        public static final int ADE_IB_OGG = 40;
        public static final int ADE_COD_COMP_ANIA = 3;
        public static final int ADE_TP_RGM_FISC = 2;
        public static final int ADE_TP_RIAT = 20;
        public static final int ADE_TP_MOD_PAG_TIT = 2;
        public static final int ADE_TP_IAS = 2;
        public static final int ADE_FL_COINC_ASSTO = 1;
        public static final int ADE_IB_DFLT = 40;
        public static final int ADE_MOD_CALC = 2;
        public static final int ADE_TP_FNT_CNBTVA = 2;
        public static final int ADE_FL_ATTIV = 1;
        public static final int ADE_FL_VARZ_STAT_TBGC = 1;
        public static final int ADE_FL_PROVZA_MIGRAZ = 1;
        public static final int ADE_DS_RIGA = 6;
        public static final int ADE_DS_OPER_SQL = 1;
        public static final int ADE_DS_VER = 5;
        public static final int ADE_DS_TS_INI_CPTZ = 10;
        public static final int ADE_DS_TS_END_CPTZ = 10;
        public static final int ADE_DS_UTENTE = 20;
        public static final int ADE_DS_STATO_ELAB = 1;
        public static final int ADE_IDEN_ISC_FND = 40;
        public static final int ADE_CONCS_PREST = 1;
        public static final int ADES = ADE_ID_ADES + ADE_ID_POLI + ADE_ID_MOVI_CRZ + AdeIdMoviChiu.Len.ADE_ID_MOVI_CHIU + ADE_DT_INI_EFF + ADE_DT_END_EFF + ADE_IB_PREV + ADE_IB_OGG + ADE_COD_COMP_ANIA + AdeDtDecor.Len.ADE_DT_DECOR + AdeDtScad.Len.ADE_DT_SCAD + AdeEtaAScad.Len.ADE_ETA_A_SCAD + AdeDurAa.Len.ADE_DUR_AA + AdeDurMm.Len.ADE_DUR_MM + AdeDurGg.Len.ADE_DUR_GG + ADE_TP_RGM_FISC + ADE_TP_RIAT + ADE_TP_MOD_PAG_TIT + ADE_TP_IAS + AdeDtVarzTpIas.Len.ADE_DT_VARZ_TP_IAS + AdePreNetInd.Len.ADE_PRE_NET_IND + AdePreLrdInd.Len.ADE_PRE_LRD_IND + AdeRatLrdInd.Len.ADE_RAT_LRD_IND + AdePrstzIniInd.Len.ADE_PRSTZ_INI_IND + ADE_FL_COINC_ASSTO + ADE_IB_DFLT + ADE_MOD_CALC + ADE_TP_FNT_CNBTVA + AdeImpAz.Len.ADE_IMP_AZ + AdeImpAder.Len.ADE_IMP_ADER + AdeImpTfr.Len.ADE_IMP_TFR + AdeImpVolo.Len.ADE_IMP_VOLO + AdePcAz.Len.ADE_PC_AZ + AdePcAder.Len.ADE_PC_ADER + AdePcTfr.Len.ADE_PC_TFR + AdePcVolo.Len.ADE_PC_VOLO + AdeDtNovaRgmFisc.Len.ADE_DT_NOVA_RGM_FISC + ADE_FL_ATTIV + AdeImpRecRitVis.Len.ADE_IMP_REC_RIT_VIS + AdeImpRecRitAcc.Len.ADE_IMP_REC_RIT_ACC + ADE_FL_VARZ_STAT_TBGC + ADE_FL_PROVZA_MIGRAZ + AdeImpbVisDaRec.Len.ADE_IMPB_VIS_DA_REC + AdeDtDecorPrestBan.Len.ADE_DT_DECOR_PREST_BAN + AdeDtEffVarzStatT.Len.ADE_DT_EFF_VARZ_STAT_T + ADE_DS_RIGA + ADE_DS_OPER_SQL + ADE_DS_VER + ADE_DS_TS_INI_CPTZ + ADE_DS_TS_END_CPTZ + ADE_DS_UTENTE + ADE_DS_STATO_ELAB + AdeCumCnbtCap.Len.ADE_CUM_CNBT_CAP + AdeImpGarCnbt.Len.ADE_IMP_GAR_CNBT + AdeDtUltConsCnbt.Len.ADE_DT_ULT_CONS_CNBT + ADE_IDEN_ISC_FND + AdeNumRatPian.Len.ADE_NUM_RAT_PIAN + AdeDtPresc.Len.ADE_DT_PRESC + ADE_CONCS_PREST;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_ID_ADES = 9;
            public static final int ADE_ID_POLI = 9;
            public static final int ADE_ID_MOVI_CRZ = 9;
            public static final int ADE_DT_INI_EFF = 8;
            public static final int ADE_DT_END_EFF = 8;
            public static final int ADE_COD_COMP_ANIA = 5;
            public static final int ADE_DS_RIGA = 10;
            public static final int ADE_DS_VER = 9;
            public static final int ADE_DS_TS_INI_CPTZ = 18;
            public static final int ADE_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
