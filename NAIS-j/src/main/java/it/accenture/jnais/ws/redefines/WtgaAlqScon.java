package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ALQ-SCON<br>
 * Variable: WTGA-ALQ-SCON from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaAlqScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaAlqScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ALQ_SCON;
    }

    public void setWtgaAlqScon(AfDecimal wtgaAlqScon) {
        writeDecimalAsPacked(Pos.WTGA_ALQ_SCON, wtgaAlqScon.copy());
    }

    public void setWtgaAlqSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ALQ_SCON, Pos.WTGA_ALQ_SCON);
    }

    /**Original name: WTGA-ALQ-SCON<br>*/
    public AfDecimal getWtgaAlqScon() {
        return readPackedAsDecimal(Pos.WTGA_ALQ_SCON, Len.Int.WTGA_ALQ_SCON, Len.Fract.WTGA_ALQ_SCON);
    }

    public byte[] getWtgaAlqSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ALQ_SCON, Pos.WTGA_ALQ_SCON);
        return buffer;
    }

    public void initWtgaAlqSconSpaces() {
        fill(Pos.WTGA_ALQ_SCON, Len.WTGA_ALQ_SCON, Types.SPACE_CHAR);
    }

    public void setWtgaAlqSconNull(String wtgaAlqSconNull) {
        writeString(Pos.WTGA_ALQ_SCON_NULL, wtgaAlqSconNull, Len.WTGA_ALQ_SCON_NULL);
    }

    /**Original name: WTGA-ALQ-SCON-NULL<br>*/
    public String getWtgaAlqSconNull() {
        return readString(Pos.WTGA_ALQ_SCON_NULL, Len.WTGA_ALQ_SCON_NULL);
    }

    public String getWtgaAlqSconNullFormatted() {
        return Functions.padBlanks(getWtgaAlqSconNull(), Len.WTGA_ALQ_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_SCON = 1;
        public static final int WTGA_ALQ_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_SCON = 4;
        public static final int WTGA_ALQ_SCON_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_SCON = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
