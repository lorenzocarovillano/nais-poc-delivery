package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: SPG-ID-MOVI-CHIU<br>
 * Variable: SPG-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SpgIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public SpgIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SPG_ID_MOVI_CHIU;
    }

    public void setSpgIdMoviChiu(int spgIdMoviChiu) {
        writeIntAsPacked(Pos.SPG_ID_MOVI_CHIU, spgIdMoviChiu, Len.Int.SPG_ID_MOVI_CHIU);
    }

    public void setSpgIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SPG_ID_MOVI_CHIU, Pos.SPG_ID_MOVI_CHIU);
    }

    /**Original name: SPG-ID-MOVI-CHIU<br>*/
    public int getSpgIdMoviChiu() {
        return readPackedAsInt(Pos.SPG_ID_MOVI_CHIU, Len.Int.SPG_ID_MOVI_CHIU);
    }

    public byte[] getSpgIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.SPG_ID_MOVI_CHIU, Pos.SPG_ID_MOVI_CHIU);
        return buffer;
    }

    public void setSpgIdMoviChiuNull(String spgIdMoviChiuNull) {
        writeString(Pos.SPG_ID_MOVI_CHIU_NULL, spgIdMoviChiuNull, Len.SPG_ID_MOVI_CHIU_NULL);
    }

    /**Original name: SPG-ID-MOVI-CHIU-NULL<br>*/
    public String getSpgIdMoviChiuNull() {
        return readString(Pos.SPG_ID_MOVI_CHIU_NULL, Len.SPG_ID_MOVI_CHIU_NULL);
    }

    public String getSpgIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getSpgIdMoviChiuNull(), Len.SPG_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SPG_ID_MOVI_CHIU = 1;
        public static final int SPG_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SPG_ID_MOVI_CHIU = 5;
        public static final int SPG_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SPG_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
