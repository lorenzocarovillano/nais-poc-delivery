package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-MM-CNBZ-END2006-EF<br>
 * Variable: DFL-MM-CNBZ-END2006-EF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflMmCnbzEnd2006Ef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflMmCnbzEnd2006Ef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_MM_CNBZ_END2006_EF;
    }

    public void setDflMmCnbzEnd2006Ef(int dflMmCnbzEnd2006Ef) {
        writeIntAsPacked(Pos.DFL_MM_CNBZ_END2006_EF, dflMmCnbzEnd2006Ef, Len.Int.DFL_MM_CNBZ_END2006_EF);
    }

    public void setDflMmCnbzEnd2006EfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_MM_CNBZ_END2006_EF, Pos.DFL_MM_CNBZ_END2006_EF);
    }

    /**Original name: DFL-MM-CNBZ-END2006-EF<br>*/
    public int getDflMmCnbzEnd2006Ef() {
        return readPackedAsInt(Pos.DFL_MM_CNBZ_END2006_EF, Len.Int.DFL_MM_CNBZ_END2006_EF);
    }

    public byte[] getDflMmCnbzEnd2006EfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_MM_CNBZ_END2006_EF, Pos.DFL_MM_CNBZ_END2006_EF);
        return buffer;
    }

    public void setDflMmCnbzEnd2006EfNull(String dflMmCnbzEnd2006EfNull) {
        writeString(Pos.DFL_MM_CNBZ_END2006_EF_NULL, dflMmCnbzEnd2006EfNull, Len.DFL_MM_CNBZ_END2006_EF_NULL);
    }

    /**Original name: DFL-MM-CNBZ-END2006-EF-NULL<br>*/
    public String getDflMmCnbzEnd2006EfNull() {
        return readString(Pos.DFL_MM_CNBZ_END2006_EF_NULL, Len.DFL_MM_CNBZ_END2006_EF_NULL);
    }

    public String getDflMmCnbzEnd2006EfNullFormatted() {
        return Functions.padBlanks(getDflMmCnbzEnd2006EfNull(), Len.DFL_MM_CNBZ_END2006_EF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_MM_CNBZ_END2006_EF = 1;
        public static final int DFL_MM_CNBZ_END2006_EF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_MM_CNBZ_END2006_EF = 3;
        public static final int DFL_MM_CNBZ_END2006_EF_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_MM_CNBZ_END2006_EF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
