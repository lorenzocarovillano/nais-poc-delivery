package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PROV-ACQ-RICOR<br>
 * Variable: W-B03-PROV-ACQ-RICOR from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03ProvAcqRicorLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03ProvAcqRicorLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PROV_ACQ_RICOR;
    }

    public void setwB03ProvAcqRicor(AfDecimal wB03ProvAcqRicor) {
        writeDecimalAsPacked(Pos.W_B03_PROV_ACQ_RICOR, wB03ProvAcqRicor.copy());
    }

    /**Original name: W-B03-PROV-ACQ-RICOR<br>*/
    public AfDecimal getwB03ProvAcqRicor() {
        return readPackedAsDecimal(Pos.W_B03_PROV_ACQ_RICOR, Len.Int.W_B03_PROV_ACQ_RICOR, Len.Fract.W_B03_PROV_ACQ_RICOR);
    }

    public byte[] getwB03ProvAcqRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PROV_ACQ_RICOR, Pos.W_B03_PROV_ACQ_RICOR);
        return buffer;
    }

    public void setwB03ProvAcqRicorNull(String wB03ProvAcqRicorNull) {
        writeString(Pos.W_B03_PROV_ACQ_RICOR_NULL, wB03ProvAcqRicorNull, Len.W_B03_PROV_ACQ_RICOR_NULL);
    }

    /**Original name: W-B03-PROV-ACQ-RICOR-NULL<br>*/
    public String getwB03ProvAcqRicorNull() {
        return readString(Pos.W_B03_PROV_ACQ_RICOR_NULL, Len.W_B03_PROV_ACQ_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PROV_ACQ_RICOR = 1;
        public static final int W_B03_PROV_ACQ_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PROV_ACQ_RICOR = 8;
        public static final int W_B03_PROV_ACQ_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PROV_ACQ_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PROV_ACQ_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
