package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-PR-AUT<br>
 * Variable: PCO-DT-ULT-ELAB-PR-AUT from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabPrAut extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabPrAut() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_PR_AUT;
    }

    public void setPcoDtUltElabPrAut(int pcoDtUltElabPrAut) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_PR_AUT, pcoDtUltElabPrAut, Len.Int.PCO_DT_ULT_ELAB_PR_AUT);
    }

    public void setPcoDtUltElabPrAutFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_PR_AUT, Pos.PCO_DT_ULT_ELAB_PR_AUT);
    }

    /**Original name: PCO-DT-ULT-ELAB-PR-AUT<br>*/
    public int getPcoDtUltElabPrAut() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_PR_AUT, Len.Int.PCO_DT_ULT_ELAB_PR_AUT);
    }

    public byte[] getPcoDtUltElabPrAutAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_PR_AUT, Pos.PCO_DT_ULT_ELAB_PR_AUT);
        return buffer;
    }

    public void initPcoDtUltElabPrAutHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_PR_AUT, Len.PCO_DT_ULT_ELAB_PR_AUT, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabPrAutNull(String pcoDtUltElabPrAutNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_PR_AUT_NULL, pcoDtUltElabPrAutNull, Len.PCO_DT_ULT_ELAB_PR_AUT_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-PR-AUT-NULL<br>*/
    public String getPcoDtUltElabPrAutNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_PR_AUT_NULL, Len.PCO_DT_ULT_ELAB_PR_AUT_NULL);
    }

    public String getPcoDtUltElabPrAutNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabPrAutNull(), Len.PCO_DT_ULT_ELAB_PR_AUT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_PR_AUT = 1;
        public static final int PCO_DT_ULT_ELAB_PR_AUT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_PR_AUT = 5;
        public static final int PCO_DT_ULT_ELAB_PR_AUT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_PR_AUT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
