package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.AdesDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndDettTitCont;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS5560<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs5560Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-POLI
    private IndDettTitCont indPoli = new IndDettTitCont();
    //Original name: POLI-DB
    private AdesDb poliDb = new AdesDb();
    //Original name: LDBV5561-COD-SOGG
    private String ldbv5561CodSogg = DefaultValues.stringVal(Len.LDBV5561_COD_SOGG);

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setLdbv5561Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV5561];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV5561);
        setLdbv5561Bytes(buffer, 1);
    }

    public String getLdbv5561Formatted() {
        return getLdbv5561CodSoggFormatted();
    }

    public void setLdbv5561Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv5561CodSogg = MarshalByte.readString(buffer, position, Len.LDBV5561_COD_SOGG);
    }

    public void setLdbv5561CodSogg(String ldbv5561CodSogg) {
        this.ldbv5561CodSogg = Functions.subString(ldbv5561CodSogg, Len.LDBV5561_COD_SOGG);
    }

    public String getLdbv5561CodSogg() {
        return this.ldbv5561CodSogg;
    }

    public String getLdbv5561CodSoggFormatted() {
        return Functions.padBlanks(getLdbv5561CodSogg(), Len.LDBV5561_COD_SOGG);
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndDettTitCont getIndPoli() {
        return indPoli;
    }

    public AdesDb getPoliDb() {
        return poliDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int LDBV5561_COD_SOGG = 20;
        public static final int LDBV5561 = LDBV5561_COD_SOGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
