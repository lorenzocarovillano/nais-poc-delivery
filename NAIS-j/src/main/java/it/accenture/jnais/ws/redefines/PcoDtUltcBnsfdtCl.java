package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-BNSFDT-CL<br>
 * Variable: PCO-DT-ULTC-BNSFDT-CL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcBnsfdtCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcBnsfdtCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_BNSFDT_CL;
    }

    public void setPcoDtUltcBnsfdtCl(int pcoDtUltcBnsfdtCl) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_BNSFDT_CL, pcoDtUltcBnsfdtCl, Len.Int.PCO_DT_ULTC_BNSFDT_CL);
    }

    public void setPcoDtUltcBnsfdtClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_BNSFDT_CL, Pos.PCO_DT_ULTC_BNSFDT_CL);
    }

    /**Original name: PCO-DT-ULTC-BNSFDT-CL<br>*/
    public int getPcoDtUltcBnsfdtCl() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_BNSFDT_CL, Len.Int.PCO_DT_ULTC_BNSFDT_CL);
    }

    public byte[] getPcoDtUltcBnsfdtClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_BNSFDT_CL, Pos.PCO_DT_ULTC_BNSFDT_CL);
        return buffer;
    }

    public void initPcoDtUltcBnsfdtClHighValues() {
        fill(Pos.PCO_DT_ULTC_BNSFDT_CL, Len.PCO_DT_ULTC_BNSFDT_CL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcBnsfdtClNull(String pcoDtUltcBnsfdtClNull) {
        writeString(Pos.PCO_DT_ULTC_BNSFDT_CL_NULL, pcoDtUltcBnsfdtClNull, Len.PCO_DT_ULTC_BNSFDT_CL_NULL);
    }

    /**Original name: PCO-DT-ULTC-BNSFDT-CL-NULL<br>*/
    public String getPcoDtUltcBnsfdtClNull() {
        return readString(Pos.PCO_DT_ULTC_BNSFDT_CL_NULL, Len.PCO_DT_ULTC_BNSFDT_CL_NULL);
    }

    public String getPcoDtUltcBnsfdtClNullFormatted() {
        return Functions.padBlanks(getPcoDtUltcBnsfdtClNull(), Len.PCO_DT_ULTC_BNSFDT_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_BNSFDT_CL = 1;
        public static final int PCO_DT_ULTC_BNSFDT_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_BNSFDT_CL = 5;
        public static final int PCO_DT_ULTC_BNSFDT_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_BNSFDT_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
