package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ETA-MM-1O-ASSTO<br>
 * Variable: B03-ETA-MM-1O-ASSTO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03EtaMm1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03EtaMm1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ETA_MM1O_ASSTO;
    }

    public void setB03EtaMm1oAssto(int b03EtaMm1oAssto) {
        writeIntAsPacked(Pos.B03_ETA_MM1O_ASSTO, b03EtaMm1oAssto, Len.Int.B03_ETA_MM1O_ASSTO);
    }

    public void setB03EtaMm1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ETA_MM1O_ASSTO, Pos.B03_ETA_MM1O_ASSTO);
    }

    /**Original name: B03-ETA-MM-1O-ASSTO<br>*/
    public int getB03EtaMm1oAssto() {
        return readPackedAsInt(Pos.B03_ETA_MM1O_ASSTO, Len.Int.B03_ETA_MM1O_ASSTO);
    }

    public byte[] getB03EtaMm1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ETA_MM1O_ASSTO, Pos.B03_ETA_MM1O_ASSTO);
        return buffer;
    }

    public void setB03EtaMm1oAsstoNull(String b03EtaMm1oAsstoNull) {
        writeString(Pos.B03_ETA_MM1O_ASSTO_NULL, b03EtaMm1oAsstoNull, Len.B03_ETA_MM1O_ASSTO_NULL);
    }

    /**Original name: B03-ETA-MM-1O-ASSTO-NULL<br>*/
    public String getB03EtaMm1oAsstoNull() {
        return readString(Pos.B03_ETA_MM1O_ASSTO_NULL, Len.B03_ETA_MM1O_ASSTO_NULL);
    }

    public String getB03EtaMm1oAsstoNullFormatted() {
        return Functions.padBlanks(getB03EtaMm1oAsstoNull(), Len.B03_ETA_MM1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ETA_MM1O_ASSTO = 1;
        public static final int B03_ETA_MM1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ETA_MM1O_ASSTO = 3;
        public static final int B03_ETA_MM1O_ASSTO_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ETA_MM1O_ASSTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
