package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndGravitaErrore;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS0470<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs0470Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-MOVI
    private IndGravitaErrore indMovi = new IndGravitaErrore();
    //Original name: MOV-DT-EFF-DB
    private String movDtEffDb = DefaultValues.stringVal(Len.MOV_DT_EFF_DB);
    //Original name: LDBV0471-ID-RICH
    private int ldbv0471IdRich = DefaultValues.INT_VAL;

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setMovDtEffDb(String movDtEffDb) {
        this.movDtEffDb = Functions.subString(movDtEffDb, Len.MOV_DT_EFF_DB);
    }

    public String getMovDtEffDb() {
        return this.movDtEffDb;
    }

    public void setLdbv0471Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV0471];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV0471);
        setLdbv0471Bytes(buffer, 1);
    }

    public String getLdbv0471Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv0471Bytes());
    }

    /**Original name: LDBV0471<br>*/
    public byte[] getLdbv0471Bytes() {
        byte[] buffer = new byte[Len.LDBV0471];
        return getLdbv0471Bytes(buffer, 1);
    }

    public void setLdbv0471Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv0471IdRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV0471_ID_RICH, 0);
    }

    public byte[] getLdbv0471Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv0471IdRich, Len.Int.LDBV0471_ID_RICH, 0);
        return buffer;
    }

    public void setLdbv0471IdRich(int ldbv0471IdRich) {
        this.ldbv0471IdRich = ldbv0471IdRich;
    }

    public int getLdbv0471IdRich() {
        return this.ldbv0471IdRich;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndGravitaErrore getIndMovi() {
        return indMovi;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MOV_DT_EFF_DB = 10;
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int LDBV0471_ID_RICH = 5;
        public static final int LDBV0471 = LDBV0471_ID_RICH;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV0471_ID_RICH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
