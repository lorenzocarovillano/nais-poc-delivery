package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-RINN-TAC<br>
 * Variable: WPCO-DT-ULT-RINN-TAC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltRinnTac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltRinnTac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_RINN_TAC;
    }

    public void setWpcoDtUltRinnTac(int wpcoDtUltRinnTac) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_RINN_TAC, wpcoDtUltRinnTac, Len.Int.WPCO_DT_ULT_RINN_TAC);
    }

    public void setDpcoDtUltRinnTacFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_RINN_TAC, Pos.WPCO_DT_ULT_RINN_TAC);
    }

    /**Original name: WPCO-DT-ULT-RINN-TAC<br>*/
    public int getWpcoDtUltRinnTac() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_RINN_TAC, Len.Int.WPCO_DT_ULT_RINN_TAC);
    }

    public byte[] getWpcoDtUltRinnTacAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_RINN_TAC, Pos.WPCO_DT_ULT_RINN_TAC);
        return buffer;
    }

    public void setWpcoDtUltRinnTacNull(String wpcoDtUltRinnTacNull) {
        writeString(Pos.WPCO_DT_ULT_RINN_TAC_NULL, wpcoDtUltRinnTacNull, Len.WPCO_DT_ULT_RINN_TAC_NULL);
    }

    /**Original name: WPCO-DT-ULT-RINN-TAC-NULL<br>*/
    public String getWpcoDtUltRinnTacNull() {
        return readString(Pos.WPCO_DT_ULT_RINN_TAC_NULL, Len.WPCO_DT_ULT_RINN_TAC_NULL);
    }

    public String getWpcoDtUltRinnTacNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltRinnTacNull(), Len.WPCO_DT_ULT_RINN_TAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RINN_TAC = 1;
        public static final int WPCO_DT_ULT_RINN_TAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RINN_TAC = 5;
        public static final int WPCO_DT_ULT_RINN_TAC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_RINN_TAC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
