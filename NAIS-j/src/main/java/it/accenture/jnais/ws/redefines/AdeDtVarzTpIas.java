package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-DT-VARZ-TP-IAS<br>
 * Variable: ADE-DT-VARZ-TP-IAS from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeDtVarzTpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeDtVarzTpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_DT_VARZ_TP_IAS;
    }

    public void setAdeDtVarzTpIas(int adeDtVarzTpIas) {
        writeIntAsPacked(Pos.ADE_DT_VARZ_TP_IAS, adeDtVarzTpIas, Len.Int.ADE_DT_VARZ_TP_IAS);
    }

    public void setAdeDtVarzTpIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_DT_VARZ_TP_IAS, Pos.ADE_DT_VARZ_TP_IAS);
    }

    /**Original name: ADE-DT-VARZ-TP-IAS<br>*/
    public int getAdeDtVarzTpIas() {
        return readPackedAsInt(Pos.ADE_DT_VARZ_TP_IAS, Len.Int.ADE_DT_VARZ_TP_IAS);
    }

    public byte[] getAdeDtVarzTpIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_DT_VARZ_TP_IAS, Pos.ADE_DT_VARZ_TP_IAS);
        return buffer;
    }

    public void setAdeDtVarzTpIasNull(String adeDtVarzTpIasNull) {
        writeString(Pos.ADE_DT_VARZ_TP_IAS_NULL, adeDtVarzTpIasNull, Len.ADE_DT_VARZ_TP_IAS_NULL);
    }

    /**Original name: ADE-DT-VARZ-TP-IAS-NULL<br>*/
    public String getAdeDtVarzTpIasNull() {
        return readString(Pos.ADE_DT_VARZ_TP_IAS_NULL, Len.ADE_DT_VARZ_TP_IAS_NULL);
    }

    public String getAdeDtVarzTpIasNullFormatted() {
        return Functions.padBlanks(getAdeDtVarzTpIasNull(), Len.ADE_DT_VARZ_TP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_DT_VARZ_TP_IAS = 1;
        public static final int ADE_DT_VARZ_TP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_DT_VARZ_TP_IAS = 5;
        public static final int ADE_DT_VARZ_TP_IAS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_DT_VARZ_TP_IAS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
