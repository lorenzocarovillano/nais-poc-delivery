package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-RIS-MAT-CHIU-PREC<br>
 * Variable: W-B03-RIS-MAT-CHIU-PREC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03RisMatChiuPrecLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03RisMatChiuPrecLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_RIS_MAT_CHIU_PREC;
    }

    public void setwB03RisMatChiuPrec(AfDecimal wB03RisMatChiuPrec) {
        writeDecimalAsPacked(Pos.W_B03_RIS_MAT_CHIU_PREC, wB03RisMatChiuPrec.copy());
    }

    /**Original name: W-B03-RIS-MAT-CHIU-PREC<br>*/
    public AfDecimal getwB03RisMatChiuPrec() {
        return readPackedAsDecimal(Pos.W_B03_RIS_MAT_CHIU_PREC, Len.Int.W_B03_RIS_MAT_CHIU_PREC, Len.Fract.W_B03_RIS_MAT_CHIU_PREC);
    }

    public byte[] getwB03RisMatChiuPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_RIS_MAT_CHIU_PREC, Pos.W_B03_RIS_MAT_CHIU_PREC);
        return buffer;
    }

    public void setwB03RisMatChiuPrecNull(String wB03RisMatChiuPrecNull) {
        writeString(Pos.W_B03_RIS_MAT_CHIU_PREC_NULL, wB03RisMatChiuPrecNull, Len.W_B03_RIS_MAT_CHIU_PREC_NULL);
    }

    /**Original name: W-B03-RIS-MAT-CHIU-PREC-NULL<br>*/
    public String getwB03RisMatChiuPrecNull() {
        return readString(Pos.W_B03_RIS_MAT_CHIU_PREC_NULL, Len.W_B03_RIS_MAT_CHIU_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_MAT_CHIU_PREC = 1;
        public static final int W_B03_RIS_MAT_CHIU_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_MAT_CHIU_PREC = 8;
        public static final int W_B03_RIS_MAT_CHIU_PREC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_MAT_CHIU_PREC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_MAT_CHIU_PREC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
