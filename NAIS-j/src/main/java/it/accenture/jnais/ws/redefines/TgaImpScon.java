package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-SCON<br>
 * Variable: TGA-IMP-SCON from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_SCON;
    }

    public void setTgaImpScon(AfDecimal tgaImpScon) {
        writeDecimalAsPacked(Pos.TGA_IMP_SCON, tgaImpScon.copy());
    }

    public void setTgaImpSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_SCON, Pos.TGA_IMP_SCON);
    }

    /**Original name: TGA-IMP-SCON<br>*/
    public AfDecimal getTgaImpScon() {
        return readPackedAsDecimal(Pos.TGA_IMP_SCON, Len.Int.TGA_IMP_SCON, Len.Fract.TGA_IMP_SCON);
    }

    public byte[] getTgaImpSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_SCON, Pos.TGA_IMP_SCON);
        return buffer;
    }

    public void setTgaImpSconNull(String tgaImpSconNull) {
        writeString(Pos.TGA_IMP_SCON_NULL, tgaImpSconNull, Len.TGA_IMP_SCON_NULL);
    }

    /**Original name: TGA-IMP-SCON-NULL<br>*/
    public String getTgaImpSconNull() {
        return readString(Pos.TGA_IMP_SCON_NULL, Len.TGA_IMP_SCON_NULL);
    }

    public String getTgaImpSconNullFormatted() {
        return Functions.padBlanks(getTgaImpSconNull(), Len.TGA_IMP_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_SCON = 1;
        public static final int TGA_IMP_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_SCON = 8;
        public static final int TGA_IMP_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
