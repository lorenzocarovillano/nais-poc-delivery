package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-DT-X<br>
 * Variable: WS-DT-X from program LDBM0250<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDtX {

    //==== PROPERTIES ====
    //Original name: WS-DT-X-AA
    private String aa = DefaultValues.stringVal(Len.AA);
    //Original name: FILLER-WS-DT-X
    private char flr1 = '-';
    //Original name: WS-DT-X-MM
    private String mm = DefaultValues.stringVal(Len.MM);
    //Original name: FILLER-WS-DT-X-1
    private char flr2 = '-';
    //Original name: WS-DT-X-GG
    private String gg = DefaultValues.stringVal(Len.GG);

    //==== METHODS ====
    public String getWsDtXFormatted() {
        return MarshalByteExt.bufferToStr(getWsDtXBytes());
    }

    public byte[] getWsDtXBytes() {
        byte[] buffer = new byte[Len.WS_DT_X];
        return getWsDtXBytes(buffer, 1);
    }

    public byte[] getWsDtXBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aa, Len.AA);
        position += Len.AA;
        MarshalByte.writeChar(buffer, position, flr1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, mm, Len.MM);
        position += Len.MM;
        MarshalByte.writeChar(buffer, position, flr2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        return buffer;
    }

    public void setAaFormatted(String aa) {
        this.aa = Trunc.toUnsignedNumeric(aa, Len.AA);
    }

    public short getAa() {
        return NumericDisplay.asShort(this.aa);
    }

    public char getFlr1() {
        return this.flr1;
    }

    public void setMmFormatted(String mm) {
        this.mm = Trunc.toUnsignedNumeric(mm, Len.MM);
    }

    public short getMm() {
        return NumericDisplay.asShort(this.mm);
    }

    public char getFlr2() {
        return this.flr2;
    }

    public void setGgFormatted(String gg) {
        this.gg = Trunc.toUnsignedNumeric(gg, Len.GG);
    }

    public short getGg() {
        return NumericDisplay.asShort(this.gg);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int FLR1 = 1;
        public static final int WS_DT_X = AA + MM + GG + 2 * FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
