package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-RAT-LRD-IND<br>
 * Variable: ADE-RAT-LRD-IND from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeRatLrdInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeRatLrdInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_RAT_LRD_IND;
    }

    public void setAdeRatLrdInd(AfDecimal adeRatLrdInd) {
        writeDecimalAsPacked(Pos.ADE_RAT_LRD_IND, adeRatLrdInd.copy());
    }

    public void setAdeRatLrdIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_RAT_LRD_IND, Pos.ADE_RAT_LRD_IND);
    }

    /**Original name: ADE-RAT-LRD-IND<br>*/
    public AfDecimal getAdeRatLrdInd() {
        return readPackedAsDecimal(Pos.ADE_RAT_LRD_IND, Len.Int.ADE_RAT_LRD_IND, Len.Fract.ADE_RAT_LRD_IND);
    }

    public byte[] getAdeRatLrdIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_RAT_LRD_IND, Pos.ADE_RAT_LRD_IND);
        return buffer;
    }

    public void setAdeRatLrdIndNull(String adeRatLrdIndNull) {
        writeString(Pos.ADE_RAT_LRD_IND_NULL, adeRatLrdIndNull, Len.ADE_RAT_LRD_IND_NULL);
    }

    /**Original name: ADE-RAT-LRD-IND-NULL<br>*/
    public String getAdeRatLrdIndNull() {
        return readString(Pos.ADE_RAT_LRD_IND_NULL, Len.ADE_RAT_LRD_IND_NULL);
    }

    public String getAdeRatLrdIndNullFormatted() {
        return Functions.padBlanks(getAdeRatLrdIndNull(), Len.ADE_RAT_LRD_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_RAT_LRD_IND = 1;
        public static final int ADE_RAT_LRD_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_RAT_LRD_IND = 8;
        public static final int ADE_RAT_LRD_IND_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_RAT_LRD_IND = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_RAT_LRD_IND = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
