package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvmov1Lvvs0011;
import it.accenture.jnais.copy.WmovDati;
import it.accenture.jnais.ws.enums.FlLiqui;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs2800;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3420<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3420Data {

    //==== PROPERTIES ====
    /**Original name: FL-LIQUI<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private FlLiqui flLiqui = new FlLiqui();
    //Original name: DMOV-ELE-MOV-MAX
    private short dmovEleMovMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVMOV1
    private Lccvmov1Lvvs0011 lccvmov1 = new Lccvmov1Lvvs0011();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs2800 wsMovimento = new WsMovimentoLvvs2800();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setDmovIoMovFormatted(String data) {
        byte[] buffer = new byte[Len.DMOV_IO_MOV];
        MarshalByte.writeString(buffer, 1, data, Len.DMOV_IO_MOV);
        setDmovIoMovBytes(buffer, 1);
    }

    public void setDmovIoMovBytes(byte[] buffer, int offset) {
        int position = offset;
        setDmovAreaMovBytes(buffer, position);
    }

    public void setDmovAreaMovBytes(byte[] buffer, int offset) {
        int position = offset;
        dmovEleMovMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDmovTabMovBytes(buffer, position);
    }

    public void setDmovEleMovMax(short dmovEleMovMax) {
        this.dmovEleMovMax = dmovEleMovMax;
    }

    public short getDmovEleMovMax() {
        return this.dmovEleMovMax;
    }

    public void setDmovTabMovBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvmov1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvmov1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvmov1Lvvs0011.Len.Int.ID_PTF, 0));
        position += Lccvmov1Lvvs0011.Len.ID_PTF;
        lccvmov1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public FlLiqui getFlLiqui() {
        return flLiqui;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvmov1Lvvs0011 getLccvmov1() {
        return lccvmov1;
    }

    public WsMovimentoLvvs2800 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DMOV_ELE_MOV_MAX = 2;
        public static final int DMOV_TAB_MOV = WpolStatus.Len.STATUS + Lccvmov1Lvvs0011.Len.ID_PTF + WmovDati.Len.DATI;
        public static final int DMOV_AREA_MOV = DMOV_ELE_MOV_MAX + DMOV_TAB_MOV;
        public static final int DMOV_IO_MOV = DMOV_AREA_MOV;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
