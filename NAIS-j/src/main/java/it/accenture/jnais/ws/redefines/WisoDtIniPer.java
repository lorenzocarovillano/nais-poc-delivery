package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WISO-DT-INI-PER<br>
 * Variable: WISO-DT-INI-PER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoDtIniPer extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoDtIniPer() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_DT_INI_PER;
    }

    public void setWisoDtIniPer(int wisoDtIniPer) {
        writeIntAsPacked(Pos.WISO_DT_INI_PER, wisoDtIniPer, Len.Int.WISO_DT_INI_PER);
    }

    public void setWisoDtIniPerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_DT_INI_PER, Pos.WISO_DT_INI_PER);
    }

    /**Original name: WISO-DT-INI-PER<br>*/
    public int getWisoDtIniPer() {
        return readPackedAsInt(Pos.WISO_DT_INI_PER, Len.Int.WISO_DT_INI_PER);
    }

    public byte[] getWisoDtIniPerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_DT_INI_PER, Pos.WISO_DT_INI_PER);
        return buffer;
    }

    public void initWisoDtIniPerSpaces() {
        fill(Pos.WISO_DT_INI_PER, Len.WISO_DT_INI_PER, Types.SPACE_CHAR);
    }

    public void setWisoDtIniPerNull(String wisoDtIniPerNull) {
        writeString(Pos.WISO_DT_INI_PER_NULL, wisoDtIniPerNull, Len.WISO_DT_INI_PER_NULL);
    }

    /**Original name: WISO-DT-INI-PER-NULL<br>*/
    public String getWisoDtIniPerNull() {
        return readString(Pos.WISO_DT_INI_PER_NULL, Len.WISO_DT_INI_PER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_DT_INI_PER = 1;
        public static final int WISO_DT_INI_PER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_DT_INI_PER = 5;
        public static final int WISO_DT_INI_PER_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_DT_INI_PER = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
