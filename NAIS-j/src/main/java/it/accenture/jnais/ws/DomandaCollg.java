package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DOMANDA-COLLG<br>
 * Variable: DOMANDA-COLLG from copybook IDBVQ051<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DomandaCollg extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: Q05-ID-COMP-QUEST-PAD
    private int idCompQuestPad = DefaultValues.INT_VAL;
    //Original name: Q05-ID-COMP-QUEST
    private int idCompQuest = DefaultValues.INT_VAL;
    //Original name: Q05-DT-INI-EFF
    private int dtIniEff = DefaultValues.INT_VAL;
    //Original name: Q05-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: Q05-DT-END-EFF
    private int dtEndEff = DefaultValues.INT_VAL;
    //Original name: Q05-VAL-RISP-LEN
    private short valRispLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: Q05-VAL-RISP
    private String valRisp = DefaultValues.stringVal(Len.VAL_RISP);
    //Original name: Q05-TP-GERARCHIA
    private String tpGerarchia = DefaultValues.stringVal(Len.TP_GERARCHIA);
    //Original name: Q05-DFLT-VISUAL
    private char dfltVisual = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DOMANDA_COLLG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setDomandaCollgBytes(buf);
    }

    public String getDomandaCollgFormatted() {
        return MarshalByteExt.bufferToStr(getDomandaCollgBytes());
    }

    public void setDomandaCollgBytes(byte[] buffer) {
        setDomandaCollgBytes(buffer, 1);
    }

    public byte[] getDomandaCollgBytes() {
        byte[] buffer = new byte[Len.DOMANDA_COLLG];
        return getDomandaCollgBytes(buffer, 1);
    }

    public void setDomandaCollgBytes(byte[] buffer, int offset) {
        int position = offset;
        idCompQuestPad = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_COMP_QUEST_PAD, 0);
        position += Len.ID_COMP_QUEST_PAD;
        idCompQuest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_COMP_QUEST, 0);
        position += Len.ID_COMP_QUEST;
        dtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        dtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        setValRispVcharBytes(buffer, position);
        position += Len.VAL_RISP_VCHAR;
        tpGerarchia = MarshalByte.readString(buffer, position, Len.TP_GERARCHIA);
        position += Len.TP_GERARCHIA;
        dfltVisual = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDomandaCollgBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idCompQuestPad, Len.Int.ID_COMP_QUEST_PAD, 0);
        position += Len.ID_COMP_QUEST_PAD;
        MarshalByte.writeIntAsPacked(buffer, position, idCompQuest, Len.Int.ID_COMP_QUEST, 0);
        position += Len.ID_COMP_QUEST;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniEff, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, dtEndEff, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        getValRispVcharBytes(buffer, position);
        position += Len.VAL_RISP_VCHAR;
        MarshalByte.writeString(buffer, position, tpGerarchia, Len.TP_GERARCHIA);
        position += Len.TP_GERARCHIA;
        MarshalByte.writeChar(buffer, position, dfltVisual);
        return buffer;
    }

    public void setIdCompQuestPad(int idCompQuestPad) {
        this.idCompQuestPad = idCompQuestPad;
    }

    public int getIdCompQuestPad() {
        return this.idCompQuestPad;
    }

    public void setIdCompQuest(int idCompQuest) {
        this.idCompQuest = idCompQuest;
    }

    public int getIdCompQuest() {
        return this.idCompQuest;
    }

    public void setDtIniEff(int dtIniEff) {
        this.dtIniEff = dtIniEff;
    }

    public int getDtIniEff() {
        return this.dtIniEff;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setDtEndEff(int dtEndEff) {
        this.dtEndEff = dtEndEff;
    }

    public int getDtEndEff() {
        return this.dtEndEff;
    }

    public void setValRispVcharFormatted(String data) {
        byte[] buffer = new byte[Len.VAL_RISP_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.VAL_RISP_VCHAR);
        setValRispVcharBytes(buffer, 1);
    }

    public String getValRispVcharFormatted() {
        return MarshalByteExt.bufferToStr(getValRispVcharBytes());
    }

    /**Original name: Q05-VAL-RISP-VCHAR<br>*/
    public byte[] getValRispVcharBytes() {
        byte[] buffer = new byte[Len.VAL_RISP_VCHAR];
        return getValRispVcharBytes(buffer, 1);
    }

    public void setValRispVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        valRispLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        valRisp = MarshalByte.readString(buffer, position, Len.VAL_RISP);
    }

    public byte[] getValRispVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, valRispLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, valRisp, Len.VAL_RISP);
        return buffer;
    }

    public void setValRispLen(short valRispLen) {
        this.valRispLen = valRispLen;
    }

    public short getValRispLen() {
        return this.valRispLen;
    }

    public void setValRisp(String valRisp) {
        this.valRisp = Functions.subString(valRisp, Len.VAL_RISP);
    }

    public String getValRisp() {
        return this.valRisp;
    }

    public void setTpGerarchia(String tpGerarchia) {
        this.tpGerarchia = Functions.subString(tpGerarchia, Len.TP_GERARCHIA);
    }

    public String getTpGerarchia() {
        return this.tpGerarchia;
    }

    public void setDfltVisual(char dfltVisual) {
        this.dfltVisual = dfltVisual;
    }

    public char getDfltVisual() {
        return this.dfltVisual;
    }

    @Override
    public byte[] serialize() {
        return getDomandaCollgBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_COMP_QUEST_PAD = 5;
        public static final int ID_COMP_QUEST = 5;
        public static final int DT_INI_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int DT_END_EFF = 5;
        public static final int VAL_RISP_LEN = 2;
        public static final int VAL_RISP = 250;
        public static final int VAL_RISP_VCHAR = VAL_RISP_LEN + VAL_RISP;
        public static final int TP_GERARCHIA = 2;
        public static final int DFLT_VISUAL = 1;
        public static final int DOMANDA_COLLG = ID_COMP_QUEST_PAD + ID_COMP_QUEST + DT_INI_EFF + COD_COMP_ANIA + DT_END_EFF + VAL_RISP_VCHAR + TP_GERARCHIA + DFLT_VISUAL;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_COMP_QUEST_PAD = 9;
            public static final int ID_COMP_QUEST = 9;
            public static final int DT_INI_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_END_EFF = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
