package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.redefines.Wl19Tabella;

/**Original name: WL19-AREA-QUOTE<br>
 * Variable: WL19-AREA-QUOTE from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Wl19AreaQuote {

    //==== PROPERTIES ====
    /**Original name: WL19-ELE-FND-MAX<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY 7 PER LA GESTIONE DELLE OCCURS
	 * ----------------------------------------------------------------*</pre>*/
    private short wl19EleFndMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WL19-TABELLA
    private Wl19Tabella wl19Tabella = new Wl19Tabella();

    //==== METHODS ====
    public void setWl19AreaQuoteFormatted(String data) {
        byte[] buffer = new byte[Len.WL19_AREA_QUOTE];
        MarshalByte.writeString(buffer, 1, data, Len.WL19_AREA_QUOTE);
        setWl19AreaQuoteBytes(buffer, 1);
    }

    public void setWl19AreaQuoteBytes(byte[] buffer, int offset) {
        int position = offset;
        wl19EleFndMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wl19Tabella.setWl19TabellaBytes(buffer, position);
    }

    public byte[] getWl19AreaQuoteBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wl19EleFndMax);
        position += Types.SHORT_SIZE;
        wl19Tabella.getWl19TabellaBytes(buffer, position);
        return buffer;
    }

    public void setWl19EleFndMax(short wl19EleFndMax) {
        this.wl19EleFndMax = wl19EleFndMax;
    }

    public short getWl19EleFndMax() {
        return this.wl19EleFndMax;
    }

    public Wl19Tabella getWl19Tabella() {
        return wl19Tabella;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WL19_ELE_FND_MAX = 2;
        public static final int WL19_AREA_QUOTE = WL19_ELE_FND_MAX + Wl19Tabella.Len.WL19_TABELLA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
