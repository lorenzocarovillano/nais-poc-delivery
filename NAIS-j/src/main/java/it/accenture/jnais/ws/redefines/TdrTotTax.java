package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-TAX<br>
 * Variable: TDR-TOT-TAX from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_TAX;
    }

    public void setTdrTotTax(AfDecimal tdrTotTax) {
        writeDecimalAsPacked(Pos.TDR_TOT_TAX, tdrTotTax.copy());
    }

    public void setTdrTotTaxFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_TAX, Pos.TDR_TOT_TAX);
    }

    /**Original name: TDR-TOT-TAX<br>*/
    public AfDecimal getTdrTotTax() {
        return readPackedAsDecimal(Pos.TDR_TOT_TAX, Len.Int.TDR_TOT_TAX, Len.Fract.TDR_TOT_TAX);
    }

    public byte[] getTdrTotTaxAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_TAX, Pos.TDR_TOT_TAX);
        return buffer;
    }

    public void setTdrTotTaxNull(String tdrTotTaxNull) {
        writeString(Pos.TDR_TOT_TAX_NULL, tdrTotTaxNull, Len.TDR_TOT_TAX_NULL);
    }

    /**Original name: TDR-TOT-TAX-NULL<br>*/
    public String getTdrTotTaxNull() {
        return readString(Pos.TDR_TOT_TAX_NULL, Len.TDR_TOT_TAX_NULL);
    }

    public String getTdrTotTaxNullFormatted() {
        return Functions.padBlanks(getTdrTotTaxNull(), Len.TDR_TOT_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_TAX = 1;
        public static final int TDR_TOT_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_TAX = 8;
        public static final int TDR_TOT_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
