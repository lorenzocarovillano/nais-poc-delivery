package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-SPE-AGE<br>
 * Variable: DTC-SPE-AGE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcSpeAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcSpeAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_SPE_AGE;
    }

    public void setDtcSpeAge(AfDecimal dtcSpeAge) {
        writeDecimalAsPacked(Pos.DTC_SPE_AGE, dtcSpeAge.copy());
    }

    public void setDtcSpeAgeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_SPE_AGE, Pos.DTC_SPE_AGE);
    }

    /**Original name: DTC-SPE-AGE<br>*/
    public AfDecimal getDtcSpeAge() {
        return readPackedAsDecimal(Pos.DTC_SPE_AGE, Len.Int.DTC_SPE_AGE, Len.Fract.DTC_SPE_AGE);
    }

    public byte[] getDtcSpeAgeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_SPE_AGE, Pos.DTC_SPE_AGE);
        return buffer;
    }

    public void setDtcSpeAgeNull(String dtcSpeAgeNull) {
        writeString(Pos.DTC_SPE_AGE_NULL, dtcSpeAgeNull, Len.DTC_SPE_AGE_NULL);
    }

    /**Original name: DTC-SPE-AGE-NULL<br>*/
    public String getDtcSpeAgeNull() {
        return readString(Pos.DTC_SPE_AGE_NULL, Len.DTC_SPE_AGE_NULL);
    }

    public String getDtcSpeAgeNullFormatted() {
        return Functions.padBlanks(getDtcSpeAgeNull(), Len.DTC_SPE_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_SPE_AGE = 1;
        public static final int DTC_SPE_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_SPE_AGE = 8;
        public static final int DTC_SPE_AGE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_SPE_AGE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_SPE_AGE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
