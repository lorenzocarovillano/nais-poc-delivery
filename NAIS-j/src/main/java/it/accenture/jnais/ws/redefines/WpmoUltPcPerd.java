package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-ULT-PC-PERD<br>
 * Variable: WPMO-ULT-PC-PERD from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoUltPcPerd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoUltPcPerd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_ULT_PC_PERD;
    }

    public void setWpmoUltPcPerd(AfDecimal wpmoUltPcPerd) {
        writeDecimalAsPacked(Pos.WPMO_ULT_PC_PERD, wpmoUltPcPerd.copy());
    }

    public void setWpmoUltPcPerdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_ULT_PC_PERD, Pos.WPMO_ULT_PC_PERD);
    }

    /**Original name: WPMO-ULT-PC-PERD<br>*/
    public AfDecimal getWpmoUltPcPerd() {
        return readPackedAsDecimal(Pos.WPMO_ULT_PC_PERD, Len.Int.WPMO_ULT_PC_PERD, Len.Fract.WPMO_ULT_PC_PERD);
    }

    public byte[] getWpmoUltPcPerdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_ULT_PC_PERD, Pos.WPMO_ULT_PC_PERD);
        return buffer;
    }

    public void initWpmoUltPcPerdSpaces() {
        fill(Pos.WPMO_ULT_PC_PERD, Len.WPMO_ULT_PC_PERD, Types.SPACE_CHAR);
    }

    public void setWpmoUltPcPerdNull(String wpmoUltPcPerdNull) {
        writeString(Pos.WPMO_ULT_PC_PERD_NULL, wpmoUltPcPerdNull, Len.WPMO_ULT_PC_PERD_NULL);
    }

    /**Original name: WPMO-ULT-PC-PERD-NULL<br>*/
    public String getWpmoUltPcPerdNull() {
        return readString(Pos.WPMO_ULT_PC_PERD_NULL, Len.WPMO_ULT_PC_PERD_NULL);
    }

    public String getWpmoUltPcPerdNullFormatted() {
        return Functions.padBlanks(getWpmoUltPcPerdNull(), Len.WPMO_ULT_PC_PERD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_ULT_PC_PERD = 1;
        public static final int WPMO_ULT_PC_PERD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_ULT_PC_PERD = 4;
        public static final int WPMO_ULT_PC_PERD_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_ULT_PC_PERD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_ULT_PC_PERD = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
