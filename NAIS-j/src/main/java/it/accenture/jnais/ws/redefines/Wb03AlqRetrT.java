package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-ALQ-RETR-T<br>
 * Variable: WB03-ALQ-RETR-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03AlqRetrT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03AlqRetrT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_ALQ_RETR_T;
    }

    public void setWb03AlqRetrTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_ALQ_RETR_T, Pos.WB03_ALQ_RETR_T);
    }

    /**Original name: WB03-ALQ-RETR-T<br>*/
    public AfDecimal getWb03AlqRetrT() {
        return readPackedAsDecimal(Pos.WB03_ALQ_RETR_T, Len.Int.WB03_ALQ_RETR_T, Len.Fract.WB03_ALQ_RETR_T);
    }

    public byte[] getWb03AlqRetrTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_ALQ_RETR_T, Pos.WB03_ALQ_RETR_T);
        return buffer;
    }

    public void setWb03AlqRetrTNull(String wb03AlqRetrTNull) {
        writeString(Pos.WB03_ALQ_RETR_T_NULL, wb03AlqRetrTNull, Len.WB03_ALQ_RETR_T_NULL);
    }

    /**Original name: WB03-ALQ-RETR-T-NULL<br>*/
    public String getWb03AlqRetrTNull() {
        return readString(Pos.WB03_ALQ_RETR_T_NULL, Len.WB03_ALQ_RETR_T_NULL);
    }

    public String getWb03AlqRetrTNullFormatted() {
        return Functions.padBlanks(getWb03AlqRetrTNull(), Len.WB03_ALQ_RETR_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_ALQ_RETR_T = 1;
        public static final int WB03_ALQ_RETR_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ALQ_RETR_T = 4;
        public static final int WB03_ALQ_RETR_T_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ALQ_RETR_T = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_ALQ_RETR_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
