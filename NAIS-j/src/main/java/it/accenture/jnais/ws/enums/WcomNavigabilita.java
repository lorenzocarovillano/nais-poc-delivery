package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WCOM-NAVIGABILITA<br>
 * Variable: WCOM-NAVIGABILITA from copybook LCCC0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomNavigabilita {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char AMMESSA = 'A';
    public static final char NON_AMMESSA = 'N';
    public static final char EMBEDDED_MOD = 'E';
    public static final char EMBEDDED_READ_ONLY = 'R';

    //==== METHODS ====
    public void setNavigabilita(char navigabilita) {
        this.value = navigabilita;
    }

    public char getNavigabilita() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NAVIGABILITA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
