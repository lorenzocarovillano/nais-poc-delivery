package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DUR-1O-PER-GG<br>
 * Variable: B03-DUR-1O-PER-GG from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03Dur1oPerGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03Dur1oPerGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DUR1O_PER_GG;
    }

    public void setB03Dur1oPerGg(int b03Dur1oPerGg) {
        writeIntAsPacked(Pos.B03_DUR1O_PER_GG, b03Dur1oPerGg, Len.Int.B03_DUR1O_PER_GG);
    }

    public void setB03Dur1oPerGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DUR1O_PER_GG, Pos.B03_DUR1O_PER_GG);
    }

    /**Original name: B03-DUR-1O-PER-GG<br>*/
    public int getB03Dur1oPerGg() {
        return readPackedAsInt(Pos.B03_DUR1O_PER_GG, Len.Int.B03_DUR1O_PER_GG);
    }

    public byte[] getB03Dur1oPerGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DUR1O_PER_GG, Pos.B03_DUR1O_PER_GG);
        return buffer;
    }

    public void setB03Dur1oPerGgNull(String b03Dur1oPerGgNull) {
        writeString(Pos.B03_DUR1O_PER_GG_NULL, b03Dur1oPerGgNull, Len.B03_DUR1O_PER_GG_NULL);
    }

    /**Original name: B03-DUR-1O-PER-GG-NULL<br>*/
    public String getB03Dur1oPerGgNull() {
        return readString(Pos.B03_DUR1O_PER_GG_NULL, Len.B03_DUR1O_PER_GG_NULL);
    }

    public String getB03Dur1oPerGgNullFormatted() {
        return Functions.padBlanks(getB03Dur1oPerGgNull(), Len.B03_DUR1O_PER_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DUR1O_PER_GG = 1;
        public static final int B03_DUR1O_PER_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DUR1O_PER_GG = 3;
        public static final int B03_DUR1O_PER_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DUR1O_PER_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
