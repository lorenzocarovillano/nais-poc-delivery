package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.C216ErroreValorizza;
import it.accenture.jnais.ws.occurs.C216TabCalcoloKo;
import it.accenture.jnais.ws.occurs.C216TabVarNotFound;

/**Original name: AREA-WARNING-IVVC0216<br>
 * Variable: AREA-WARNING-IVVC0216 from program IVVS0216<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ivvc0215 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_VAR_NOT_FOUND_MAXOCCURS = 5;
    public static final int TAB_CALCOLO_KO_MAXOCCURS = 5;
    //Original name: C216-ELE-MAX-NOT-FOUND
    private short eleMaxNotFound = DefaultValues.SHORT_VAL;
    //Original name: C216-TAB-VAR-NOT-FOUND
    private C216TabVarNotFound[] tabVarNotFound = new C216TabVarNotFound[TAB_VAR_NOT_FOUND_MAXOCCURS];
    //Original name: C216-ELE-MAX-CALC-KO
    private short eleMaxCalcKo = DefaultValues.SHORT_VAL;
    //Original name: C216-TAB-CALCOLO-KO
    private C216TabCalcoloKo[] tabCalcoloKo = new C216TabCalcoloKo[TAB_CALCOLO_KO_MAXOCCURS];
    //Original name: C216-ERRORE-VALORIZZA
    private C216ErroreValorizza erroreValorizza = new C216ErroreValorizza();

    //==== CONSTRUCTORS ====
    public Ivvc0215() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IVVC0215;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIvvc0215Bytes(buf);
    }

    public void init() {
        for (int tabVarNotFoundIdx = 1; tabVarNotFoundIdx <= TAB_VAR_NOT_FOUND_MAXOCCURS; tabVarNotFoundIdx++) {
            tabVarNotFound[tabVarNotFoundIdx - 1] = new C216TabVarNotFound();
        }
        for (int tabCalcoloKoIdx = 1; tabCalcoloKoIdx <= TAB_CALCOLO_KO_MAXOCCURS; tabCalcoloKoIdx++) {
            tabCalcoloKo[tabCalcoloKoIdx - 1] = new C216TabCalcoloKo();
        }
    }

    public String getIvvc0215Formatted() {
        return MarshalByteExt.bufferToStr(getIvvc0215Bytes());
    }

    public void setIvvc0215Bytes(byte[] buffer) {
        setIvvc0215Bytes(buffer, 1);
    }

    public byte[] getIvvc0215Bytes() {
        byte[] buffer = new byte[Len.IVVC0215];
        return getIvvc0215Bytes(buffer, 1);
    }

    public void setIvvc0215Bytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaVarKoBytes(buffer, position);
        position += Len.AREA_VAR_KO;
        erroreValorizza.setErroreValorizza(MarshalByte.readString(buffer, position, C216ErroreValorizza.Len.ERRORE_VALORIZZA));
    }

    public byte[] getIvvc0215Bytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaVarKoBytes(buffer, position);
        position += Len.AREA_VAR_KO;
        MarshalByte.writeString(buffer, position, erroreValorizza.getErroreValorizza(), C216ErroreValorizza.Len.ERRORE_VALORIZZA);
        return buffer;
    }

    /**Original name: IVVC0216-AREA-VAR-KO<br>
	 * <pre>--> Gestione output variabili variabili</pre>*/
    public byte[] getAreaVarKoBytes() {
        byte[] buffer = new byte[Len.AREA_VAR_KO];
        return getAreaVarKoBytes(buffer, 1);
    }

    public void setAreaVarKoBytes(byte[] buffer, int offset) {
        int position = offset;
        setNotFoundMvvBytes(buffer, position);
        position += Len.NOT_FOUND_MVV;
        setCalcoloKoBytes(buffer, position);
    }

    public byte[] getAreaVarKoBytes(byte[] buffer, int offset) {
        int position = offset;
        getNotFoundMvvBytes(buffer, position);
        position += Len.NOT_FOUND_MVV;
        getCalcoloKoBytes(buffer, position);
        return buffer;
    }

    public void setNotFoundMvvBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxNotFound = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_MAX_NOT_FOUND, 0);
        position += Len.ELE_MAX_NOT_FOUND;
        setAreaVarNotFoundBytes(buffer, position);
    }

    public byte[] getNotFoundMvvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeShortAsPacked(buffer, position, eleMaxNotFound, Len.Int.ELE_MAX_NOT_FOUND, 0);
        position += Len.ELE_MAX_NOT_FOUND;
        getAreaVarNotFoundBytes(buffer, position);
        return buffer;
    }

    public void setEleMaxNotFound(short eleMaxNotFound) {
        this.eleMaxNotFound = eleMaxNotFound;
    }

    public short getEleMaxNotFound() {
        return this.eleMaxNotFound;
    }

    public void setAreaVarNotFoundBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_VAR_NOT_FOUND_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabVarNotFound[idx - 1].setTabVarNotFoundBytes(buffer, position);
                position += C216TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
            }
            else {
                tabVarNotFound[idx - 1].initTabVarNotFoundSpaces();
                position += C216TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
            }
        }
    }

    public byte[] getAreaVarNotFoundBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_VAR_NOT_FOUND_MAXOCCURS; idx++) {
            tabVarNotFound[idx - 1].getTabVarNotFoundBytes(buffer, position);
            position += C216TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
        }
        return buffer;
    }

    public void setCalcoloKoBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxCalcKo = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_MAX_CALC_KO, 0);
        position += Len.ELE_MAX_CALC_KO;
        setAreaCalcKoBytes(buffer, position);
    }

    public byte[] getCalcoloKoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeShortAsPacked(buffer, position, eleMaxCalcKo, Len.Int.ELE_MAX_CALC_KO, 0);
        position += Len.ELE_MAX_CALC_KO;
        getAreaCalcKoBytes(buffer, position);
        return buffer;
    }

    public void setEleMaxCalcKo(short eleMaxCalcKo) {
        this.eleMaxCalcKo = eleMaxCalcKo;
    }

    public short getEleMaxCalcKo() {
        return this.eleMaxCalcKo;
    }

    public void setAreaCalcKoBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_CALCOLO_KO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabCalcoloKo[idx - 1].setTabCalcoloKoBytes(buffer, position);
                position += C216TabCalcoloKo.Len.TAB_CALCOLO_KO;
            }
            else {
                tabCalcoloKo[idx - 1].initTabCalcoloKoSpaces();
                position += C216TabCalcoloKo.Len.TAB_CALCOLO_KO;
            }
        }
    }

    public byte[] getAreaCalcKoBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_CALCOLO_KO_MAXOCCURS; idx++) {
            tabCalcoloKo[idx - 1].getTabCalcoloKoBytes(buffer, position);
            position += C216TabCalcoloKo.Len.TAB_CALCOLO_KO;
        }
        return buffer;
    }

    public C216ErroreValorizza getErroreValorizza() {
        return erroreValorizza;
    }

    public C216TabCalcoloKo getTabCalcoloKo(int idx) {
        return tabCalcoloKo[idx - 1];
    }

    public C216TabVarNotFound getTabVarNotFound(int idx) {
        return tabVarNotFound[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getIvvc0215Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_MAX_NOT_FOUND = 3;
        public static final int AREA_VAR_NOT_FOUND = Ivvc0215.TAB_VAR_NOT_FOUND_MAXOCCURS * C216TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
        public static final int NOT_FOUND_MVV = ELE_MAX_NOT_FOUND + AREA_VAR_NOT_FOUND;
        public static final int ELE_MAX_CALC_KO = 3;
        public static final int AREA_CALC_KO = Ivvc0215.TAB_CALCOLO_KO_MAXOCCURS * C216TabCalcoloKo.Len.TAB_CALCOLO_KO;
        public static final int CALCOLO_KO = ELE_MAX_CALC_KO + AREA_CALC_KO;
        public static final int AREA_VAR_KO = NOT_FOUND_MVV + CALCOLO_KO;
        public static final int IVVC0215 = AREA_VAR_KO + C216ErroreValorizza.Len.ERRORE_VALORIZZA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ELE_MAX_NOT_FOUND = 4;
            public static final int ELE_MAX_CALC_KO = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
