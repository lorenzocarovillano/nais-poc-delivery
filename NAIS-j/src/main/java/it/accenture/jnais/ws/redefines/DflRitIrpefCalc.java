package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RIT-IRPEF-CALC<br>
 * Variable: DFL-RIT-IRPEF-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflRitIrpefCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflRitIrpefCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RIT_IRPEF_CALC;
    }

    public void setDflRitIrpefCalc(AfDecimal dflRitIrpefCalc) {
        writeDecimalAsPacked(Pos.DFL_RIT_IRPEF_CALC, dflRitIrpefCalc.copy());
    }

    public void setDflRitIrpefCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RIT_IRPEF_CALC, Pos.DFL_RIT_IRPEF_CALC);
    }

    /**Original name: DFL-RIT-IRPEF-CALC<br>*/
    public AfDecimal getDflRitIrpefCalc() {
        return readPackedAsDecimal(Pos.DFL_RIT_IRPEF_CALC, Len.Int.DFL_RIT_IRPEF_CALC, Len.Fract.DFL_RIT_IRPEF_CALC);
    }

    public byte[] getDflRitIrpefCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RIT_IRPEF_CALC, Pos.DFL_RIT_IRPEF_CALC);
        return buffer;
    }

    public void setDflRitIrpefCalcNull(String dflRitIrpefCalcNull) {
        writeString(Pos.DFL_RIT_IRPEF_CALC_NULL, dflRitIrpefCalcNull, Len.DFL_RIT_IRPEF_CALC_NULL);
    }

    /**Original name: DFL-RIT-IRPEF-CALC-NULL<br>*/
    public String getDflRitIrpefCalcNull() {
        return readString(Pos.DFL_RIT_IRPEF_CALC_NULL, Len.DFL_RIT_IRPEF_CALC_NULL);
    }

    public String getDflRitIrpefCalcNullFormatted() {
        return Functions.padBlanks(getDflRitIrpefCalcNull(), Len.DFL_RIT_IRPEF_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RIT_IRPEF_CALC = 1;
        public static final int DFL_RIT_IRPEF_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RIT_IRPEF_CALC = 8;
        public static final int DFL_RIT_IRPEF_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RIT_IRPEF_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RIT_IRPEF_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
