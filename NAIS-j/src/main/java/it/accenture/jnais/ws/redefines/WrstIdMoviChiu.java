package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRST-ID-MOVI-CHIU<br>
 * Variable: WRST-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_ID_MOVI_CHIU;
    }

    public void setWrstIdMoviChiu(int wrstIdMoviChiu) {
        writeIntAsPacked(Pos.WRST_ID_MOVI_CHIU, wrstIdMoviChiu, Len.Int.WRST_ID_MOVI_CHIU);
    }

    public void setWrstIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_ID_MOVI_CHIU, Pos.WRST_ID_MOVI_CHIU);
    }

    /**Original name: WRST-ID-MOVI-CHIU<br>*/
    public int getWrstIdMoviChiu() {
        return readPackedAsInt(Pos.WRST_ID_MOVI_CHIU, Len.Int.WRST_ID_MOVI_CHIU);
    }

    public byte[] getWrstIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_ID_MOVI_CHIU, Pos.WRST_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWrstIdMoviChiuSpaces() {
        fill(Pos.WRST_ID_MOVI_CHIU, Len.WRST_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWrstIdMoviChiuNull(String wrstIdMoviChiuNull) {
        writeString(Pos.WRST_ID_MOVI_CHIU_NULL, wrstIdMoviChiuNull, Len.WRST_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WRST-ID-MOVI-CHIU-NULL<br>*/
    public String getWrstIdMoviChiuNull() {
        return readString(Pos.WRST_ID_MOVI_CHIU_NULL, Len.WRST_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_ID_MOVI_CHIU = 1;
        public static final int WRST_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_ID_MOVI_CHIU = 5;
        public static final int WRST_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
