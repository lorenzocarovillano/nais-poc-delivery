package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IMovi;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndGravitaErrore;
import it.accenture.jnais.copy.Movi;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBSE060<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbse060Data implements IMovi {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-MOVI
    private IndGravitaErrore indMovi = new IndGravitaErrore();
    //Original name: MOV-DT-EFF-DB
    private String movDtEffDb = DefaultValues.stringVal(Len.MOV_DT_EFF_DB);
    //Original name: MOVI
    private Movi movi = new Movi();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    @Override
    public void setMovDtEffDb(String movDtEffDb) {
        this.movDtEffDb = Functions.subString(movDtEffDb, Len.MOV_DT_EFF_DB);
    }

    @Override
    public String getMovDtEffDb() {
        return this.movDtEffDb;
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsCptz() {
        return movi.getMovDsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.movi.setMovDsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getIbMovi() {
        throw new FieldNotMappedException("ibMovi");
    }

    @Override
    public void setIbMovi(String ibMovi) {
        throw new FieldNotMappedException("ibMovi");
    }

    @Override
    public String getIbMoviObj() {
        return getIbMovi();
    }

    @Override
    public void setIbMoviObj(String ibMoviObj) {
        setIbMovi(ibMoviObj);
    }

    @Override
    public String getIbOgg() {
        throw new FieldNotMappedException("ibOgg");
    }

    @Override
    public void setIbOgg(String ibOgg) {
        throw new FieldNotMappedException("ibOgg");
    }

    @Override
    public String getIbOggObj() {
        return getIbOgg();
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        setIbOgg(ibOggObj);
    }

    @Override
    public int getIdMoviAnn() {
        throw new FieldNotMappedException("idMoviAnn");
    }

    @Override
    public void setIdMoviAnn(int idMoviAnn) {
        throw new FieldNotMappedException("idMoviAnn");
    }

    @Override
    public Integer getIdMoviAnnObj() {
        return ((Integer)getIdMoviAnn());
    }

    @Override
    public void setIdMoviAnnObj(Integer idMoviAnnObj) {
        setIdMoviAnn(((int)idMoviAnnObj));
    }

    @Override
    public int getIdMoviCollg() {
        throw new FieldNotMappedException("idMoviCollg");
    }

    @Override
    public void setIdMoviCollg(int idMoviCollg) {
        throw new FieldNotMappedException("idMoviCollg");
    }

    @Override
    public Integer getIdMoviCollgObj() {
        return ((Integer)getIdMoviCollg());
    }

    @Override
    public void setIdMoviCollgObj(Integer idMoviCollgObj) {
        setIdMoviCollg(((int)idMoviCollgObj));
    }

    @Override
    public int getIdRich() {
        throw new FieldNotMappedException("idRich");
    }

    @Override
    public void setIdRich(int idRich) {
        throw new FieldNotMappedException("idRich");
    }

    @Override
    public Integer getIdRichObj() {
        return ((Integer)getIdRich());
    }

    @Override
    public void setIdRichObj(Integer idRichObj) {
        setIdRich(((int)idRichObj));
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndGravitaErrore getIndMovi() {
        return indMovi;
    }

    @Override
    public String getLdbv2651DtFineDb() {
        throw new FieldNotMappedException("ldbv2651DtFineDb");
    }

    @Override
    public void setLdbv2651DtFineDb(String ldbv2651DtFineDb) {
        throw new FieldNotMappedException("ldbv2651DtFineDb");
    }

    @Override
    public String getLdbv2651DtInizDb() {
        throw new FieldNotMappedException("ldbv2651DtInizDb");
    }

    @Override
    public void setLdbv2651DtInizDb(String ldbv2651DtInizDb) {
        throw new FieldNotMappedException("ldbv2651DtInizDb");
    }

    @Override
    public int getLdbv2651IdOgg() {
        throw new FieldNotMappedException("ldbv2651IdOgg");
    }

    @Override
    public void setLdbv2651IdOgg(int ldbv2651IdOgg) {
        throw new FieldNotMappedException("ldbv2651IdOgg");
    }

    @Override
    public int getLdbv2651TpMov1() {
        throw new FieldNotMappedException("ldbv2651TpMov1");
    }

    @Override
    public void setLdbv2651TpMov1(int ldbv2651TpMov1) {
        throw new FieldNotMappedException("ldbv2651TpMov1");
    }

    @Override
    public int getLdbv2651TpMov2() {
        throw new FieldNotMappedException("ldbv2651TpMov2");
    }

    @Override
    public void setLdbv2651TpMov2(int ldbv2651TpMov2) {
        throw new FieldNotMappedException("ldbv2651TpMov2");
    }

    @Override
    public int getLdbv2651TpMov3() {
        throw new FieldNotMappedException("ldbv2651TpMov3");
    }

    @Override
    public void setLdbv2651TpMov3(int ldbv2651TpMov3) {
        throw new FieldNotMappedException("ldbv2651TpMov3");
    }

    @Override
    public String getLdbv2651TpOgg() {
        throw new FieldNotMappedException("ldbv2651TpOgg");
    }

    @Override
    public void setLdbv2651TpOgg(String ldbv2651TpOgg) {
        throw new FieldNotMappedException("ldbv2651TpOgg");
    }

    @Override
    public int getLdbv2921IdPol() {
        throw new FieldNotMappedException("ldbv2921IdPol");
    }

    @Override
    public void setLdbv2921IdPol(int ldbv2921IdPol) {
        throw new FieldNotMappedException("ldbv2921IdPol");
    }

    @Override
    public String getLdbv3611DtADb() {
        throw new FieldNotMappedException("ldbv3611DtADb");
    }

    @Override
    public void setLdbv3611DtADb(String ldbv3611DtADb) {
        throw new FieldNotMappedException("ldbv3611DtADb");
    }

    @Override
    public String getLdbv3611DtDaDb() {
        throw new FieldNotMappedException("ldbv3611DtDaDb");
    }

    @Override
    public void setLdbv3611DtDaDb(String ldbv3611DtDaDb) {
        throw new FieldNotMappedException("ldbv3611DtDaDb");
    }

    @Override
    public int getLdbv3611IdOgg() {
        throw new FieldNotMappedException("ldbv3611IdOgg");
    }

    @Override
    public void setLdbv3611IdOgg(int ldbv3611IdOgg) {
        throw new FieldNotMappedException("ldbv3611IdOgg");
    }

    @Override
    public int getLdbv3611TpMovi10() {
        throw new FieldNotMappedException("ldbv3611TpMovi10");
    }

    @Override
    public void setLdbv3611TpMovi10(int ldbv3611TpMovi10) {
        throw new FieldNotMappedException("ldbv3611TpMovi10");
    }

    @Override
    public int getLdbv3611TpMovi11() {
        throw new FieldNotMappedException("ldbv3611TpMovi11");
    }

    @Override
    public void setLdbv3611TpMovi11(int ldbv3611TpMovi11) {
        throw new FieldNotMappedException("ldbv3611TpMovi11");
    }

    @Override
    public int getLdbv3611TpMovi12() {
        throw new FieldNotMappedException("ldbv3611TpMovi12");
    }

    @Override
    public void setLdbv3611TpMovi12(int ldbv3611TpMovi12) {
        throw new FieldNotMappedException("ldbv3611TpMovi12");
    }

    @Override
    public int getLdbv3611TpMovi13() {
        throw new FieldNotMappedException("ldbv3611TpMovi13");
    }

    @Override
    public void setLdbv3611TpMovi13(int ldbv3611TpMovi13) {
        throw new FieldNotMappedException("ldbv3611TpMovi13");
    }

    @Override
    public int getLdbv3611TpMovi14() {
        throw new FieldNotMappedException("ldbv3611TpMovi14");
    }

    @Override
    public void setLdbv3611TpMovi14(int ldbv3611TpMovi14) {
        throw new FieldNotMappedException("ldbv3611TpMovi14");
    }

    @Override
    public int getLdbv3611TpMovi15() {
        throw new FieldNotMappedException("ldbv3611TpMovi15");
    }

    @Override
    public void setLdbv3611TpMovi15(int ldbv3611TpMovi15) {
        throw new FieldNotMappedException("ldbv3611TpMovi15");
    }

    @Override
    public int getLdbv3611TpMovi1() {
        throw new FieldNotMappedException("ldbv3611TpMovi1");
    }

    @Override
    public void setLdbv3611TpMovi1(int ldbv3611TpMovi1) {
        throw new FieldNotMappedException("ldbv3611TpMovi1");
    }

    @Override
    public int getLdbv3611TpMovi2() {
        throw new FieldNotMappedException("ldbv3611TpMovi2");
    }

    @Override
    public void setLdbv3611TpMovi2(int ldbv3611TpMovi2) {
        throw new FieldNotMappedException("ldbv3611TpMovi2");
    }

    @Override
    public int getLdbv3611TpMovi3() {
        throw new FieldNotMappedException("ldbv3611TpMovi3");
    }

    @Override
    public void setLdbv3611TpMovi3(int ldbv3611TpMovi3) {
        throw new FieldNotMappedException("ldbv3611TpMovi3");
    }

    @Override
    public int getLdbv3611TpMovi4() {
        throw new FieldNotMappedException("ldbv3611TpMovi4");
    }

    @Override
    public void setLdbv3611TpMovi4(int ldbv3611TpMovi4) {
        throw new FieldNotMappedException("ldbv3611TpMovi4");
    }

    @Override
    public int getLdbv3611TpMovi5() {
        throw new FieldNotMappedException("ldbv3611TpMovi5");
    }

    @Override
    public void setLdbv3611TpMovi5(int ldbv3611TpMovi5) {
        throw new FieldNotMappedException("ldbv3611TpMovi5");
    }

    @Override
    public int getLdbv3611TpMovi6() {
        throw new FieldNotMappedException("ldbv3611TpMovi6");
    }

    @Override
    public void setLdbv3611TpMovi6(int ldbv3611TpMovi6) {
        throw new FieldNotMappedException("ldbv3611TpMovi6");
    }

    @Override
    public int getLdbv3611TpMovi7() {
        throw new FieldNotMappedException("ldbv3611TpMovi7");
    }

    @Override
    public void setLdbv3611TpMovi7(int ldbv3611TpMovi7) {
        throw new FieldNotMappedException("ldbv3611TpMovi7");
    }

    @Override
    public int getLdbv3611TpMovi8() {
        throw new FieldNotMappedException("ldbv3611TpMovi8");
    }

    @Override
    public void setLdbv3611TpMovi8(int ldbv3611TpMovi8) {
        throw new FieldNotMappedException("ldbv3611TpMovi8");
    }

    @Override
    public int getLdbv3611TpMovi9() {
        throw new FieldNotMappedException("ldbv3611TpMovi9");
    }

    @Override
    public void setLdbv3611TpMovi9(int ldbv3611TpMovi9) {
        throw new FieldNotMappedException("ldbv3611TpMovi9");
    }

    @Override
    public String getLdbv3611TpOgg() {
        throw new FieldNotMappedException("ldbv3611TpOgg");
    }

    @Override
    public void setLdbv3611TpOgg(String ldbv3611TpOgg) {
        throw new FieldNotMappedException("ldbv3611TpOgg");
    }

    @Override
    public int getLdbv5991TpMov10() {
        throw new FieldNotMappedException("ldbv5991TpMov10");
    }

    @Override
    public void setLdbv5991TpMov10(int ldbv5991TpMov10) {
        throw new FieldNotMappedException("ldbv5991TpMov10");
    }

    @Override
    public int getLdbv5991TpMov1() {
        throw new FieldNotMappedException("ldbv5991TpMov1");
    }

    @Override
    public void setLdbv5991TpMov1(int ldbv5991TpMov1) {
        throw new FieldNotMappedException("ldbv5991TpMov1");
    }

    @Override
    public int getLdbv5991TpMov2() {
        throw new FieldNotMappedException("ldbv5991TpMov2");
    }

    @Override
    public void setLdbv5991TpMov2(int ldbv5991TpMov2) {
        throw new FieldNotMappedException("ldbv5991TpMov2");
    }

    @Override
    public int getLdbv5991TpMov3() {
        throw new FieldNotMappedException("ldbv5991TpMov3");
    }

    @Override
    public void setLdbv5991TpMov3(int ldbv5991TpMov3) {
        throw new FieldNotMappedException("ldbv5991TpMov3");
    }

    @Override
    public int getLdbv5991TpMov4() {
        throw new FieldNotMappedException("ldbv5991TpMov4");
    }

    @Override
    public void setLdbv5991TpMov4(int ldbv5991TpMov4) {
        throw new FieldNotMappedException("ldbv5991TpMov4");
    }

    @Override
    public int getLdbv5991TpMov5() {
        throw new FieldNotMappedException("ldbv5991TpMov5");
    }

    @Override
    public void setLdbv5991TpMov5(int ldbv5991TpMov5) {
        throw new FieldNotMappedException("ldbv5991TpMov5");
    }

    @Override
    public int getLdbv5991TpMov6() {
        throw new FieldNotMappedException("ldbv5991TpMov6");
    }

    @Override
    public void setLdbv5991TpMov6(int ldbv5991TpMov6) {
        throw new FieldNotMappedException("ldbv5991TpMov6");
    }

    @Override
    public int getLdbv5991TpMov7() {
        throw new FieldNotMappedException("ldbv5991TpMov7");
    }

    @Override
    public void setLdbv5991TpMov7(int ldbv5991TpMov7) {
        throw new FieldNotMappedException("ldbv5991TpMov7");
    }

    @Override
    public int getLdbv5991TpMov8() {
        throw new FieldNotMappedException("ldbv5991TpMov8");
    }

    @Override
    public void setLdbv5991TpMov8(int ldbv5991TpMov8) {
        throw new FieldNotMappedException("ldbv5991TpMov8");
    }

    @Override
    public int getLdbv5991TpMov9() {
        throw new FieldNotMappedException("ldbv5991TpMov9");
    }

    @Override
    public void setLdbv5991TpMov9(int ldbv5991TpMov9) {
        throw new FieldNotMappedException("ldbv5991TpMov9");
    }

    @Override
    public int getLdbvc591TpMovi1() {
        throw new FieldNotMappedException("ldbvc591TpMovi1");
    }

    @Override
    public void setLdbvc591TpMovi1(int ldbvc591TpMovi1) {
        throw new FieldNotMappedException("ldbvc591TpMovi1");
    }

    @Override
    public int getLdbvc591TpMovi2() {
        throw new FieldNotMappedException("ldbvc591TpMovi2");
    }

    @Override
    public void setLdbvc591TpMovi2(int ldbvc591TpMovi2) {
        throw new FieldNotMappedException("ldbvc591TpMovi2");
    }

    @Override
    public int getLdbve061IdOgg() {
        throw new FieldNotMappedException("ldbve061IdOgg");
    }

    @Override
    public void setLdbve061IdOgg(int ldbve061IdOgg) {
        throw new FieldNotMappedException("ldbve061IdOgg");
    }

    @Override
    public int getLdbve061TpMovi() {
        throw new FieldNotMappedException("ldbve061TpMovi");
    }

    @Override
    public void setLdbve061TpMovi(int ldbve061TpMovi) {
        throw new FieldNotMappedException("ldbve061TpMovi");
    }

    @Override
    public String getLdbve061TpOgg() {
        throw new FieldNotMappedException("ldbve061TpOgg");
    }

    @Override
    public void setLdbve061TpOgg(String ldbve061TpOgg) {
        throw new FieldNotMappedException("ldbve061TpOgg");
    }

    @Override
    public int getMovIdMovi() {
        throw new FieldNotMappedException("movIdMovi");
    }

    @Override
    public void setMovIdMovi(int movIdMovi) {
        throw new FieldNotMappedException("movIdMovi");
    }

    @Override
    public int getMovIdOgg() {
        throw new FieldNotMappedException("movIdOgg");
    }

    @Override
    public void setMovIdOgg(int movIdOgg) {
        throw new FieldNotMappedException("movIdOgg");
    }

    @Override
    public Integer getMovIdOggObj() {
        return ((Integer)getMovIdOgg());
    }

    @Override
    public void setMovIdOggObj(Integer movIdOggObj) {
        setMovIdOgg(((int)movIdOggObj));
    }

    @Override
    public String getMovTpOgg() {
        throw new FieldNotMappedException("movTpOgg");
    }

    @Override
    public void setMovTpOgg(String movTpOgg) {
        throw new FieldNotMappedException("movTpOgg");
    }

    @Override
    public String getMovTpOggObj() {
        return getMovTpOgg();
    }

    @Override
    public void setMovTpOggObj(String movTpOggObj) {
        setMovTpOgg(movTpOggObj);
    }

    public Movi getMovi() {
        return movi;
    }

    @Override
    public String getPolDtEmisDb() {
        throw new FieldNotMappedException("polDtEmisDb");
    }

    @Override
    public void setPolDtEmisDb(String polDtEmisDb) {
        throw new FieldNotMappedException("polDtEmisDb");
    }

    @Override
    public String getPolDtIniEffDb() {
        throw new FieldNotMappedException("polDtIniEffDb");
    }

    @Override
    public void setPolDtIniEffDb(String polDtIniEffDb) {
        throw new FieldNotMappedException("polDtIniEffDb");
    }

    @Override
    public int getTpMovi() {
        throw new FieldNotMappedException("tpMovi");
    }

    @Override
    public void setTpMovi(int tpMovi) {
        throw new FieldNotMappedException("tpMovi");
    }

    @Override
    public Integer getTpMoviObj() {
        return ((Integer)getTpMovi());
    }

    @Override
    public void setTpMoviObj(Integer tpMoviObj) {
        setTpMovi(((int)tpMoviObj));
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MOV_DT_EFF_DB = 10;
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
