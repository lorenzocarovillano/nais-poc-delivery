package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-BOLLO-DETT-L<br>
 * Variable: WDFL-IMPB-BOLLO-DETT-L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbBolloDettL extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbBolloDettL() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_BOLLO_DETT_L;
    }

    public void setWdflImpbBolloDettL(AfDecimal wdflImpbBolloDettL) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_BOLLO_DETT_L, wdflImpbBolloDettL.copy());
    }

    public void setWdflImpbBolloDettLFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_BOLLO_DETT_L, Pos.WDFL_IMPB_BOLLO_DETT_L);
    }

    /**Original name: WDFL-IMPB-BOLLO-DETT-L<br>*/
    public AfDecimal getWdflImpbBolloDettL() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_BOLLO_DETT_L, Len.Int.WDFL_IMPB_BOLLO_DETT_L, Len.Fract.WDFL_IMPB_BOLLO_DETT_L);
    }

    public byte[] getWdflImpbBolloDettLAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_BOLLO_DETT_L, Pos.WDFL_IMPB_BOLLO_DETT_L);
        return buffer;
    }

    public void setWdflImpbBolloDettLNull(String wdflImpbBolloDettLNull) {
        writeString(Pos.WDFL_IMPB_BOLLO_DETT_L_NULL, wdflImpbBolloDettLNull, Len.WDFL_IMPB_BOLLO_DETT_L_NULL);
    }

    /**Original name: WDFL-IMPB-BOLLO-DETT-L-NULL<br>*/
    public String getWdflImpbBolloDettLNull() {
        return readString(Pos.WDFL_IMPB_BOLLO_DETT_L_NULL, Len.WDFL_IMPB_BOLLO_DETT_L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_BOLLO_DETT_L = 1;
        public static final int WDFL_IMPB_BOLLO_DETT_L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_BOLLO_DETT_L = 8;
        public static final int WDFL_IMPB_BOLLO_DETT_L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_BOLLO_DETT_L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_BOLLO_DETT_L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
