package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-DT-INI-VAL-TAR<br>
 * Variable: WGRZ-DT-INI-VAL-TAR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzDtIniValTar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzDtIniValTar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_DT_INI_VAL_TAR;
    }

    public void setWgrzDtIniValTar(int wgrzDtIniValTar) {
        writeIntAsPacked(Pos.WGRZ_DT_INI_VAL_TAR, wgrzDtIniValTar, Len.Int.WGRZ_DT_INI_VAL_TAR);
    }

    public void setWgrzDtIniValTarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_DT_INI_VAL_TAR, Pos.WGRZ_DT_INI_VAL_TAR);
    }

    /**Original name: WGRZ-DT-INI-VAL-TAR<br>*/
    public int getWgrzDtIniValTar() {
        return readPackedAsInt(Pos.WGRZ_DT_INI_VAL_TAR, Len.Int.WGRZ_DT_INI_VAL_TAR);
    }

    public byte[] getWgrzDtIniValTarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_DT_INI_VAL_TAR, Pos.WGRZ_DT_INI_VAL_TAR);
        return buffer;
    }

    public void initWgrzDtIniValTarSpaces() {
        fill(Pos.WGRZ_DT_INI_VAL_TAR, Len.WGRZ_DT_INI_VAL_TAR, Types.SPACE_CHAR);
    }

    public void setWgrzDtIniValTarNull(String wgrzDtIniValTarNull) {
        writeString(Pos.WGRZ_DT_INI_VAL_TAR_NULL, wgrzDtIniValTarNull, Len.WGRZ_DT_INI_VAL_TAR_NULL);
    }

    /**Original name: WGRZ-DT-INI-VAL-TAR-NULL<br>*/
    public String getWgrzDtIniValTarNull() {
        return readString(Pos.WGRZ_DT_INI_VAL_TAR_NULL, Len.WGRZ_DT_INI_VAL_TAR_NULL);
    }

    public String getWgrzDtIniValTarNullFormatted() {
        return Functions.padBlanks(getWgrzDtIniValTarNull(), Len.WGRZ_DT_INI_VAL_TAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_INI_VAL_TAR = 1;
        public static final int WGRZ_DT_INI_VAL_TAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_INI_VAL_TAR = 5;
        public static final int WGRZ_DT_INI_VAL_TAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_DT_INI_VAL_TAR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
