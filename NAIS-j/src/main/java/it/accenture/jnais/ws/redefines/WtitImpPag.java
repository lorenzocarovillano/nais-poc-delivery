package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-IMP-PAG<br>
 * Variable: WTIT-IMP-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitImpPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitImpPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_IMP_PAG;
    }

    public void setWtitImpPag(AfDecimal wtitImpPag) {
        writeDecimalAsPacked(Pos.WTIT_IMP_PAG, wtitImpPag.copy());
    }

    public void setWtitImpPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_IMP_PAG, Pos.WTIT_IMP_PAG);
    }

    /**Original name: WTIT-IMP-PAG<br>*/
    public AfDecimal getWtitImpPag() {
        return readPackedAsDecimal(Pos.WTIT_IMP_PAG, Len.Int.WTIT_IMP_PAG, Len.Fract.WTIT_IMP_PAG);
    }

    public byte[] getWtitImpPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_IMP_PAG, Pos.WTIT_IMP_PAG);
        return buffer;
    }

    public void initWtitImpPagSpaces() {
        fill(Pos.WTIT_IMP_PAG, Len.WTIT_IMP_PAG, Types.SPACE_CHAR);
    }

    public void setWtitImpPagNull(String wtitImpPagNull) {
        writeString(Pos.WTIT_IMP_PAG_NULL, wtitImpPagNull, Len.WTIT_IMP_PAG_NULL);
    }

    /**Original name: WTIT-IMP-PAG-NULL<br>*/
    public String getWtitImpPagNull() {
        return readString(Pos.WTIT_IMP_PAG_NULL, Len.WTIT_IMP_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_PAG = 1;
        public static final int WTIT_IMP_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_PAG = 8;
        public static final int WTIT_IMP_PAG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_PAG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_PAG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
