package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WL30-COD-SUB-AGE<br>
 * Variable: WL30-COD-SUB-AGE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl30CodSubAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl30CodSubAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL30_COD_SUB_AGE;
    }

    public void setWl30CodSubAge(int wl30CodSubAge) {
        writeIntAsPacked(Pos.WL30_COD_SUB_AGE, wl30CodSubAge, Len.Int.WL30_COD_SUB_AGE);
    }

    public void setWl30CodSubAgeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL30_COD_SUB_AGE, Pos.WL30_COD_SUB_AGE);
    }

    /**Original name: WL30-COD-SUB-AGE<br>*/
    public int getWl30CodSubAge() {
        return readPackedAsInt(Pos.WL30_COD_SUB_AGE, Len.Int.WL30_COD_SUB_AGE);
    }

    public byte[] getWl30CodSubAgeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL30_COD_SUB_AGE, Pos.WL30_COD_SUB_AGE);
        return buffer;
    }

    public void initWl30CodSubAgeSpaces() {
        fill(Pos.WL30_COD_SUB_AGE, Len.WL30_COD_SUB_AGE, Types.SPACE_CHAR);
    }

    public void setWl30CodSubAgeNull(String wl30CodSubAgeNull) {
        writeString(Pos.WL30_COD_SUB_AGE_NULL, wl30CodSubAgeNull, Len.WL30_COD_SUB_AGE_NULL);
    }

    /**Original name: WL30-COD-SUB-AGE-NULL<br>*/
    public String getWl30CodSubAgeNull() {
        return readString(Pos.WL30_COD_SUB_AGE_NULL, Len.WL30_COD_SUB_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL30_COD_SUB_AGE = 1;
        public static final int WL30_COD_SUB_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL30_COD_SUB_AGE = 3;
        public static final int WL30_COD_SUB_AGE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL30_COD_SUB_AGE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
