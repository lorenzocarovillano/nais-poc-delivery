package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.notifier.IValueChangeListener;
import com.bphx.ctu.af.core.notifier.StringChangeNotifier;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import com.modernsystems.ctu.data.NumericDisplaySigned;
import it.accenture.jnais.commons.data.to.ICompStrDato;
import it.accenture.jnais.copy.CompStrDato;
import it.accenture.jnais.copy.Idsi0021Area;
import it.accenture.jnais.copy.Idso0021;
import it.accenture.jnais.copy.IndAnagDato;
import it.accenture.jnais.copy.IndCompStrDato;
import it.accenture.jnais.copy.RidefDatoStr;
import it.accenture.jnais.copy.SinonimoStr;
import it.accenture.jnais.ws.occurs.ScrElementsStrDato;
import it.accenture.jnais.ws.occurs.WkElementiPadre;
import it.accenture.jnais.ws.occurs.WkElementiPadreRed;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDSS0020<br>
 * Generated as a class for rule WS.<br>*/
public class Idss0020Data implements ICompStrDato {

    //==== PROPERTIES ====
    public static final int WK_ELEMENTI_PADRE_MAXOCCURS = 10;
    public static final int WK_ELEMENTI_PADRE_RED_MAXOCCURS = 10;
    public static final int SCR_ELEMENTS_STR_DATO_MAXOCCURS = 1000;
    public static final int FLAG_TRATTAMENTO_MAXOCCURS = 1000;
    private int flagTrattamentoListenerSize = FLAG_TRATTAMENTO_MAXOCCURS;
    private IValueChangeListener flagTrattamentoListener = new FlagTrattamentoListener();
    //Original name: INDICE-ANAGR-DATO
    private short indiceAnagrDato = ((short)0);
    //Original name: WK-NUMERO-LIVELLO
    private short wkNumeroLivello = ((short)0);
    //Original name: WK-NUMERO-LIVELLO-RED
    private short wkNumeroLivelloRed = ((short)0);
    //Original name: WK-LIVELLO-PADRE
    private short wkLivelloPadre = ((short)1);
    //Original name: WK-INIZIO-POSIZIONE
    private int wkInizioPosizione = 1;
    //Original name: WK-INIZIO-POSIZIONE-RED
    private int wkInizioPosizioneRed = DefaultValues.INT_VAL;
    //Original name: WK-INIZIO-POSIZIONE-RIC
    private int wkInizioPosizioneRic = DefaultValues.INT_VAL;
    //Original name: WK-INIZIO-POSIZIONE-MOL
    private int wkInizioPosizioneMol = DefaultValues.INT_VAL;
    //Original name: WK-NUMERO-RICORRENZA
    private int wkNumeroRicorrenza = DefaultValues.INT_VAL;
    //Original name: WK-SQLCODE
    private int wkSqlcode = DefaultValues.INT_VAL;
    //Original name: FLAG-REDEFINES
    private boolean flagRedefines = false;
    //Original name: WK-ELEMENTI-PADRE
    private WkElementiPadre[] wkElementiPadre = new WkElementiPadre[WK_ELEMENTI_PADRE_MAXOCCURS];
    //Original name: WK-ELEMENTI-PADRE-RED
    private WkElementiPadreRed[] wkElementiPadreRed = new WkElementiPadreRed[WK_ELEMENTI_PADRE_RED_MAXOCCURS];
    //Original name: WK-LIMITE-LIVELLI
    private short wkLimiteLivelli = ((short)10);
    //Original name: SCR-ELEMENTS-STR-DATO
    private LazyArrayCopy<ScrElementsStrDato> scrElementsStrDato = new LazyArrayCopy<ScrElementsStrDato>(new ScrElementsStrDato(), 1, SCR_ELEMENTS_STR_DATO_MAXOCCURS);
    //Original name: SCR-IND
    private int scrInd = 1;
    /**Original name: PGM-NAME-IDSS0020<br>
	 * <pre>   fine COPY IDSV0042</pre>*/
    private String pgmNameIdss0020 = "IDSS0020";
    //Original name: PGM-NAME-IDSS0080
    private String pgmNameIdss0080 = "IDSS0080";
    //Original name: WK-LIMITE-STR-DATO
    private StringChangeNotifier wkLimiteStrDato = new StringChangeNotifier(Len.WK_LIMITE_STR_DATO);
    //Original name: CONVERSION-VARIABLES
    private ConversionVariables conversionVariables = new ConversionVariables();
    //Original name: WK-CAMPO-ATTIVO
    private char wkCampoAttivo = 'S';
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IND-SINONIM
    private String indSinonim = "0";
    //Original name: IDSV0003
    private Idsv0003 idsv0003 = new Idsv0003();
    //Original name: IDSI0021-AREA
    private Idsi0021Area idsi0021Area = new Idsi0021Area();
    //Original name: IDSO0021
    private Idso0021 idso0021 = new Idso0021();
    //Original name: IDSO0021-IND
    private int idso0021Ind = 1;
    //Original name: ANAG-DATO
    private AnagDato anagDato = new AnagDato();
    //Original name: IND-ANAG-DATO
    private IndAnagDato indAnagDato = new IndAnagDato();
    //Original name: COMP-STR-DATO
    private CompStrDato compStrDato = new CompStrDato();
    //Original name: IND-COMP-STR-DATO
    private IndCompStrDato indCompStrDato = new IndCompStrDato();
    //Original name: SINONIMO-STR
    private SinonimoStr sinonimoStr = new SinonimoStr();
    //Original name: RIDEF-DATO-STR
    private RidefDatoStr ridefDatoStr = new RidefDatoStr();
    /**Original name: WK-COUNTER<br>
	 * <pre>LOCAL-STORAGE SECTION.</pre>*/
    private char wkCounter = 'N';
    //Original name: INDEX-PADRE
    private short indexPadre = DefaultValues.SHORT_VAL;
    //Original name: COMODO-INDICE-PADRE
    private short comodoIndicePadre = DefaultValues.SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Idss0020Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wkElementiPadreIdx = 1; wkElementiPadreIdx <= WK_ELEMENTI_PADRE_MAXOCCURS; wkElementiPadreIdx++) {
            wkElementiPadre[wkElementiPadreIdx - 1] = new WkElementiPadre();
        }
        for (int wkElementiPadreRedIdx = 1; wkElementiPadreRedIdx <= WK_ELEMENTI_PADRE_RED_MAXOCCURS; wkElementiPadreRedIdx++) {
            wkElementiPadreRed[wkElementiPadreRedIdx - 1] = new WkElementiPadreRed();
        }
        wkLimiteStrDato.setValue(1000);
    }

    public void setIndiceAnagrDato(short indiceAnagrDato) {
        this.indiceAnagrDato = indiceAnagrDato;
    }

    public short getIndiceAnagrDato() {
        return this.indiceAnagrDato;
    }

    public void setWkNumeroLivello(short wkNumeroLivello) {
        this.wkNumeroLivello = wkNumeroLivello;
    }

    public short getWkNumeroLivello() {
        return this.wkNumeroLivello;
    }

    public void setWkNumeroLivelloRed(short wkNumeroLivelloRed) {
        this.wkNumeroLivelloRed = wkNumeroLivelloRed;
    }

    public short getWkNumeroLivelloRed() {
        return this.wkNumeroLivelloRed;
    }

    public short getWkLivelloPadre() {
        return this.wkLivelloPadre;
    }

    public void setWkInizioPosizione(int wkInizioPosizione) {
        this.wkInizioPosizione = wkInizioPosizione;
    }

    public int getWkInizioPosizione() {
        return this.wkInizioPosizione;
    }

    public void setWkInizioPosizioneRed(int wkInizioPosizioneRed) {
        this.wkInizioPosizioneRed = wkInizioPosizioneRed;
    }

    public int getWkInizioPosizioneRed() {
        return this.wkInizioPosizioneRed;
    }

    public void setWkInizioPosizioneRic(int wkInizioPosizioneRic) {
        this.wkInizioPosizioneRic = wkInizioPosizioneRic;
    }

    public int getWkInizioPosizioneRic() {
        return this.wkInizioPosizioneRic;
    }

    public void setWkInizioPosizioneMol(int wkInizioPosizioneMol) {
        this.wkInizioPosizioneMol = wkInizioPosizioneMol;
    }

    public int getWkInizioPosizioneMol() {
        return this.wkInizioPosizioneMol;
    }

    public void setWkNumeroRicorrenza(int wkNumeroRicorrenza) {
        this.wkNumeroRicorrenza = wkNumeroRicorrenza;
    }

    public int getWkNumeroRicorrenza() {
        return this.wkNumeroRicorrenza;
    }

    public void setWkSqlcode(int wkSqlcode) {
        this.wkSqlcode = wkSqlcode;
    }

    public int getWkSqlcode() {
        return this.wkSqlcode;
    }

    public String getWkSqlcodeFormatted() {
        return NumericDisplaySigned.asString(getWkSqlcode(), Len.WK_SQLCODE);
    }

    public String getWkSqlcodeAsString() {
        return getWkSqlcodeFormatted();
    }

    public void setFlagRedefines(boolean flagRedefines) {
        this.flagRedefines = flagRedefines;
    }

    public boolean isFlagRedefines() {
        return this.flagRedefines;
    }

    public short getWkLimiteLivelli() {
        return this.wkLimiteLivelli;
    }

    public void setAreaScrematuraBytes(byte[] buffer) {
        setAreaScrematuraBytes(buffer, 1);
    }

    public void setAreaScrematuraBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= SCR_ELEMENTS_STR_DATO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                scrElementsStrDato.get(idx - 1).setScrElementsStrDatoBytes(buffer, position);
                position += ScrElementsStrDato.Len.SCR_ELEMENTS_STR_DATO;
            }
            else {
                ScrElementsStrDato temp_scrElementsStrDato = new ScrElementsStrDato();
                temp_scrElementsStrDato.initScrElementsStrDatoSpaces();
                getScrElementsStrDatoObj().fill(temp_scrElementsStrDato);
                position += ScrElementsStrDato.Len.SCR_ELEMENTS_STR_DATO * (SCR_ELEMENTS_STR_DATO_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public void setScrInd(int scrInd) {
        this.scrInd = scrInd;
    }

    public int getScrInd() {
        return this.scrInd;
    }

    public String getPgmNameIdss0020() {
        return this.pgmNameIdss0020;
    }

    public String getPgmNameIdss0080() {
        return this.pgmNameIdss0080;
    }

    public String getPgmNameIdss0080Formatted() {
        return Functions.padBlanks(getPgmNameIdss0080(), Len.PGM_NAME_IDSS0080);
    }

    public short getWkLimiteStrDato0() {
        return ((short)wkLimiteStrDato.getValue());
    }

    public char getWkCampoAttivo() {
        return this.wkCampoAttivo;
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndSinonim(short indSinonim) {
        this.indSinonim = NumericDisplay.asString(indSinonim, Len.IND_SINONIM);
    }

    public void setIndSinonimFormatted(String indSinonim) {
        this.indSinonim = Trunc.toUnsignedNumeric(indSinonim, Len.IND_SINONIM);
    }

    public short getIndSinonim() {
        return NumericDisplay.asShort(this.indSinonim);
    }

    public void setInputIdss0020Bytes(byte[] buffer) {
        setInputIdss0020Bytes(buffer, 1);
    }

    /**Original name: INPUT-IDSS0020<br>
	 * <pre>   fine COPY IDSV0042
	 *    fine COPY IDSV0045
	 * ************************************************************************
	 *     COPY INPUT
	 * ************************************************************************</pre>*/
    public byte[] getInputIdss0020Bytes() {
        byte[] buffer = new byte[Len.INPUT_IDSS0020];
        return getInputIdss0020Bytes(buffer, 1);
    }

    public void setInputIdss0020Bytes(byte[] buffer, int offset) {
        int position = offset;
        idsi0021Area.setIdsi0021AreaBytes(buffer, position);
    }

    public byte[] getInputIdss0020Bytes(byte[] buffer, int offset) {
        int position = offset;
        idsi0021Area.getIdsi0021AreaBytes(buffer, position);
        return buffer;
    }

    public void setOutputIdss0020Bytes(byte[] buffer) {
        setOutputIdss0020Bytes(buffer, 1);
    }

    /**Original name: OUTPUT-IDSS0020<br>
	 * <pre>************************************************************************
	 *     COPY OUTPUT
	 * ************************************************************************</pre>*/
    public byte[] getOutputIdss0020Bytes() {
        byte[] buffer = new byte[Len.OUTPUT_IDSS0020];
        return getOutputIdss0020Bytes(buffer, 1);
    }

    public void setOutputIdss0020Bytes(byte[] buffer, int offset) {
        int position = offset;
        idso0021.setIdso0021AreaBytes(buffer, position);
    }

    public byte[] getOutputIdss0020Bytes(byte[] buffer, int offset) {
        int position = offset;
        idso0021.getIdso0021AreaBytes(buffer, position);
        return buffer;
    }

    public void initOutputIdss0020HighValues() {
        idso0021.initIdso0021HighValues();
    }

    public void setIdso0021Ind(int idso0021Ind) {
        this.idso0021Ind = idso0021Ind;
    }

    public int getIdso0021Ind() {
        return this.idso0021Ind;
    }

    public void setWkCounter(char wkCounter) {
        this.wkCounter = wkCounter;
    }

    public void setWkCounterFormatted(String wkCounter) {
        setWkCounter(Functions.charAt(wkCounter, Types.CHAR_SIZE));
    }

    public char getWkCounter() {
        return this.wkCounter;
    }

    public void setIndexPadre(short indexPadre) {
        this.indexPadre = indexPadre;
    }

    public short getIndexPadre() {
        return this.indexPadre;
    }

    public void setComodoIndicePadre(short comodoIndicePadre) {
        this.comodoIndicePadre = comodoIndicePadre;
    }

    public short getComodoIndicePadre() {
        return this.comodoIndicePadre;
    }

    public AnagDato getAnagDato() {
        return anagDato;
    }

    @Override
    public char getAreaConvStandard() {
        return compStrDato.getCsdAreaConvStandard();
    }

    @Override
    public void setAreaConvStandard(char areaConvStandard) {
        this.compStrDato.setCsdAreaConvStandard(areaConvStandard);
    }

    @Override
    public Character getAreaConvStandardObj() {
        if (indCompStrDato.getAreaConvStandard() >= 0) {
            return ((Character)getAreaConvStandard());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAreaConvStandardObj(Character areaConvStandardObj) {
        if (areaConvStandardObj != null) {
            setAreaConvStandard(((char)areaConvStandardObj));
            indCompStrDato.setAreaConvStandard(((short)0));
        }
        else {
            indCompStrDato.setAreaConvStandard(((short)-1));
        }
    }

    @Override
    public int getCodCompagniaAnia() {
        return compStrDato.getCsdCodCompagniaAnia();
    }

    @Override
    public void setCodCompagniaAnia(int codCompagniaAnia) {
        this.compStrDato.setCsdCodCompagniaAnia(codCompagniaAnia);
    }

    @Override
    public String getCodDato() {
        return compStrDato.getCsdCodDato();
    }

    @Override
    public void setCodDato(String codDato) {
        this.compStrDato.setCsdCodDato(codDato);
    }

    @Override
    public String getCodDatoObj() {
        if (indCompStrDato.getCodDato() >= 0) {
            return getCodDato();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDatoObj(String codDatoObj) {
        if (codDatoObj != null) {
            setCodDato(codDatoObj);
            indCompStrDato.setCodDato(((short)0));
        }
        else {
            indCompStrDato.setCodDato(((short)-1));
        }
    }

    @Override
    public String getCodDominio() {
        return compStrDato.getCsdCodDominio();
    }

    @Override
    public void setCodDominio(String codDominio) {
        this.compStrDato.setCsdCodDominio(codDominio);
    }

    @Override
    public String getCodDominioObj() {
        if (indCompStrDato.getCodDominio() >= 0) {
            return getCodDominio();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDominioObj(String codDominioObj) {
        if (codDominioObj != null) {
            setCodDominio(codDominioObj);
            indCompStrDato.setCodDominio(((short)0));
        }
        else {
            indCompStrDato.setCodDominio(((short)-1));
        }
    }

    @Override
    public String getCodStrDato2() {
        return compStrDato.getCsdCodStrDato2();
    }

    @Override
    public void setCodStrDato2(String codStrDato2) {
        this.compStrDato.setCsdCodStrDato2(codStrDato2);
    }

    @Override
    public String getCodStrDato2Obj() {
        if (indCompStrDato.getCodStrDato2() >= 0) {
            return getCodStrDato2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodStrDato2Obj(String codStrDato2Obj) {
        if (codStrDato2Obj != null) {
            setCodStrDato2(codStrDato2Obj);
            indCompStrDato.setCodStrDato2(((short)0));
        }
        else {
            indCompStrDato.setCodStrDato2(((short)-1));
        }
    }

    @Override
    public String getCodStrDato() {
        return compStrDato.getCsdCodStrDato();
    }

    @Override
    public void setCodStrDato(String codStrDato) {
        this.compStrDato.setCsdCodStrDato(codStrDato);
    }

    public CompStrDato getCompStrDato() {
        return compStrDato;
    }

    public ConversionVariables getConversionVariables() {
        return conversionVariables;
    }

    @Override
    public String getDsUtente() {
        return compStrDato.getCsdDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.compStrDato.setCsdDsUtente(dsUtente);
    }

    @Override
    public char getFlagCallUsing() {
        return compStrDato.getCsdFlagCallUsing();
    }

    @Override
    public void setFlagCallUsing(char flagCallUsing) {
        this.compStrDato.setCsdFlagCallUsing(flagCallUsing);
    }

    @Override
    public Character getFlagCallUsingObj() {
        if (indCompStrDato.getFlagCallUsing() >= 0) {
            return ((Character)getFlagCallUsing());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlagCallUsingObj(Character flagCallUsingObj) {
        if (flagCallUsingObj != null) {
            setFlagCallUsing(((char)flagCallUsingObj));
            indCompStrDato.setFlagCallUsing(((short)0));
        }
        else {
            indCompStrDato.setFlagCallUsing(((short)-1));
        }
    }

    @Override
    public char getFlagKey() {
        return compStrDato.getCsdFlagKey();
    }

    @Override
    public void setFlagKey(char flagKey) {
        this.compStrDato.setCsdFlagKey(flagKey);
    }

    @Override
    public Character getFlagKeyObj() {
        if (indCompStrDato.getFlagKey() >= 0) {
            return ((Character)getFlagKey());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlagKeyObj(Character flagKeyObj) {
        if (flagKeyObj != null) {
            setFlagKey(((char)flagKeyObj));
            indCompStrDato.setFlagKey(((short)0));
        }
        else {
            indCompStrDato.setFlagKey(((short)-1));
        }
    }

    @Override
    public char getFlagRedefines() {
        return compStrDato.getCsdFlagRedefines();
    }

    @Override
    public void setFlagRedefines(char flagRedefines) {
        this.compStrDato.setCsdFlagRedefines(flagRedefines);
    }

    @Override
    public Character getFlagRedefinesObj() {
        if (indCompStrDato.getFlagRedefines() >= 0) {
            return ((Character)getFlagRedefines());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlagRedefinesObj(Character flagRedefinesObj) {
        if (flagRedefinesObj != null) {
            setFlagRedefines(((char)flagRedefinesObj));
            indCompStrDato.setFlagRedefines(((short)0));
        }
        else {
            indCompStrDato.setFlagRedefines(((short)-1));
        }
    }

    @Override
    public char getFlagReturnCode() {
        return compStrDato.getCsdFlagReturnCode();
    }

    @Override
    public void setFlagReturnCode(char flagReturnCode) {
        this.compStrDato.setCsdFlagReturnCode(flagReturnCode);
    }

    @Override
    public Character getFlagReturnCodeObj() {
        if (indCompStrDato.getFlagReturnCode() >= 0) {
            return ((Character)getFlagReturnCode());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlagReturnCodeObj(Character flagReturnCodeObj) {
        if (flagReturnCodeObj != null) {
            setFlagReturnCode(((char)flagReturnCodeObj));
            indCompStrDato.setFlagReturnCode(((short)0));
        }
        else {
            indCompStrDato.setFlagReturnCode(((short)-1));
        }
    }

    public IValueChangeListener getFlagTrattamentoListener() {
        return flagTrattamentoListener;
    }

    @Override
    public char getFlagWhereCond() {
        return compStrDato.getCsdFlagWhereCond();
    }

    @Override
    public void setFlagWhereCond(char flagWhereCond) {
        this.compStrDato.setCsdFlagWhereCond(flagWhereCond);
    }

    @Override
    public Character getFlagWhereCondObj() {
        if (indCompStrDato.getFlagWhereCond() >= 0) {
            return ((Character)getFlagWhereCond());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlagWhereCondObj(Character flagWhereCondObj) {
        if (flagWhereCondObj != null) {
            setFlagWhereCond(((char)flagWhereCondObj));
            indCompStrDato.setFlagWhereCond(((short)0));
        }
        else {
            indCompStrDato.setFlagWhereCond(((short)-1));
        }
    }

    @Override
    public String getFormattazioneDato() {
        return compStrDato.getCsdFormattazioneDato();
    }

    @Override
    public void setFormattazioneDato(String formattazioneDato) {
        this.compStrDato.setCsdFormattazioneDato(formattazioneDato);
    }

    @Override
    public String getFormattazioneDatoObj() {
        if (indCompStrDato.getFormattazioneDato() >= 0) {
            return getFormattazioneDato();
        }
        else {
            return null;
        }
    }

    @Override
    public void setFormattazioneDatoObj(String formattazioneDatoObj) {
        if (formattazioneDatoObj != null) {
            setFormattazioneDato(formattazioneDatoObj);
            indCompStrDato.setFormattazioneDato(((short)0));
        }
        else {
            indCompStrDato.setFormattazioneDato(((short)-1));
        }
    }

    public Idsi0021Area getIdsi0021Area() {
        return idsi0021Area;
    }

    public Idso0021 getIdso0021() {
        return idso0021;
    }

    public Idsv0003 getIdsv0003() {
        return idsv0003;
    }

    public IndAnagDato getIndAnagDato() {
        return indAnagDato;
    }

    public IndCompStrDato getIndCompStrDato() {
        return indCompStrDato;
    }

    @Override
    public int getLunghezzaDato() {
        return compStrDato.getCsdLunghezzaDato().getCsdLunghezzaDato();
    }

    @Override
    public void setLunghezzaDato(int lunghezzaDato) {
        this.compStrDato.getCsdLunghezzaDato().setCsdLunghezzaDato(lunghezzaDato);
    }

    @Override
    public Integer getLunghezzaDatoObj() {
        if (indCompStrDato.getLunghezzaDato() >= 0) {
            return ((Integer)getLunghezzaDato());
        }
        else {
            return null;
        }
    }

    @Override
    public void setLunghezzaDatoObj(Integer lunghezzaDatoObj) {
        if (lunghezzaDatoObj != null) {
            setLunghezzaDato(((int)lunghezzaDatoObj));
            indCompStrDato.setLunghezzaDato(((short)0));
        }
        else {
            indCompStrDato.setLunghezzaDato(((short)-1));
        }
    }

    @Override
    public char getObbligatorieta() {
        return compStrDato.getCsdObbligatorieta();
    }

    @Override
    public void setObbligatorieta(char obbligatorieta) {
        this.compStrDato.setCsdObbligatorieta(obbligatorieta);
    }

    @Override
    public Character getObbligatorietaObj() {
        if (indCompStrDato.getObbligatorieta() >= 0) {
            return ((Character)getObbligatorieta());
        }
        else {
            return null;
        }
    }

    @Override
    public void setObbligatorietaObj(Character obbligatorietaObj) {
        if (obbligatorietaObj != null) {
            setObbligatorieta(((char)obbligatorietaObj));
            indCompStrDato.setObbligatorieta(((short)0));
        }
        else {
            indCompStrDato.setObbligatorieta(((short)-1));
        }
    }

    @Override
    public int getPosizione() {
        return compStrDato.getCsdPosizione();
    }

    @Override
    public void setPosizione(int posizione) {
        this.compStrDato.setCsdPosizione(posizione);
    }

    @Override
    public short getPrecisioneDato() {
        return compStrDato.getCsdPrecisioneDato().getCsdPrecisioneDato();
    }

    @Override
    public void setPrecisioneDato(short precisioneDato) {
        this.compStrDato.getCsdPrecisioneDato().setCsdPrecisioneDato(precisioneDato);
    }

    @Override
    public Short getPrecisioneDatoObj() {
        if (indCompStrDato.getPrecisioneDato() >= 0) {
            return ((Short)getPrecisioneDato());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrecisioneDatoObj(Short precisioneDatoObj) {
        if (precisioneDatoObj != null) {
            setPrecisioneDato(((short)precisioneDatoObj));
            indCompStrDato.setPrecisioneDato(((short)0));
        }
        else {
            indCompStrDato.setPrecisioneDato(((short)-1));
        }
    }

    @Override
    public int getRicorrenza() {
        return compStrDato.getCsdRicorrenza().getCsdRicorrenza();
    }

    @Override
    public void setRicorrenza(int ricorrenza) {
        this.compStrDato.getCsdRicorrenza().setCsdRicorrenza(ricorrenza);
    }

    @Override
    public Integer getRicorrenzaObj() {
        if (indCompStrDato.getRicorrenza() >= 0) {
            return ((Integer)getRicorrenza());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRicorrenzaObj(Integer ricorrenzaObj) {
        if (ricorrenzaObj != null) {
            setRicorrenza(((int)ricorrenzaObj));
            indCompStrDato.setRicorrenza(((short)0));
        }
        else {
            indCompStrDato.setRicorrenza(((short)-1));
        }
    }

    public RidefDatoStr getRidefDatoStr() {
        return ridefDatoStr;
    }

    public ScrElementsStrDato getScrElementsStrDato(int idx) {
        return scrElementsStrDato.get(idx - 1);
    }

    public LazyArrayCopy<ScrElementsStrDato> getScrElementsStrDatoObj() {
        return scrElementsStrDato;
    }

    @Override
    public String getServizioConvers() {
        return compStrDato.getCsdServizioConvers();
    }

    @Override
    public void setServizioConvers(String servizioConvers) {
        this.compStrDato.setCsdServizioConvers(servizioConvers);
    }

    @Override
    public String getServizioConversObj() {
        if (indCompStrDato.getServizioConvers() >= 0) {
            return getServizioConvers();
        }
        else {
            return null;
        }
    }

    @Override
    public void setServizioConversObj(String servizioConversObj) {
        if (servizioConversObj != null) {
            setServizioConvers(servizioConversObj);
            indCompStrDato.setServizioConvers(((short)0));
        }
        else {
            indCompStrDato.setServizioConvers(((short)-1));
        }
    }

    public SinonimoStr getSinonimoStr() {
        return sinonimoStr;
    }

    @Override
    public String getTipoDato() {
        return compStrDato.getCsdTipoDato();
    }

    @Override
    public void setTipoDato(String tipoDato) {
        this.compStrDato.setCsdTipoDato(tipoDato);
    }

    @Override
    public String getTipoDatoObj() {
        if (indCompStrDato.getTipoDato() >= 0) {
            return getTipoDato();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTipoDatoObj(String tipoDatoObj) {
        if (tipoDatoObj != null) {
            setTipoDato(tipoDatoObj);
            indCompStrDato.setTipoDato(((short)0));
        }
        else {
            indCompStrDato.setTipoDato(((short)-1));
        }
    }

    @Override
    public String getValoreDefault() {
        return compStrDato.getCsdValoreDefault();
    }

    @Override
    public void setValoreDefault(String valoreDefault) {
        this.compStrDato.setCsdValoreDefault(valoreDefault);
    }

    @Override
    public String getValoreDefaultObj() {
        if (indCompStrDato.getValoreDefault() >= 0) {
            return getValoreDefault();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValoreDefaultObj(String valoreDefaultObj) {
        if (valoreDefaultObj != null) {
            setValoreDefault(valoreDefaultObj);
            indCompStrDato.setValoreDefault(((short)0));
        }
        else {
            indCompStrDato.setValoreDefault(((short)-1));
        }
    }

    public WkElementiPadre getWkElementiPadre(int idx) {
        return wkElementiPadre[idx - 1];
    }

    public WkElementiPadreRed getWkElementiPadreRed(int idx) {
        return wkElementiPadreRed[idx - 1];
    }

    public StringChangeNotifier getWkLimiteStrDato() {
        return wkLimiteStrDato;
    }

    //==== INNER CLASSES ====
    /**Original name: FLAG-TRATTAMENTO<br>*/
    public class FlagTrattamentoListener implements IValueChangeListener {

        //==== METHODS ====
        public void change() {
            flagTrattamentoListenerSize = FLAG_TRATTAMENTO_MAXOCCURS;
        }

        public void change(int value) {
            flagTrattamentoListenerSize = value < 1 ? 0 : (value > FLAG_TRATTAMENTO_MAXOCCURS ? FLAG_TRATTAMENTO_MAXOCCURS : value);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_LIMITE_STR_DATO = 4;
        public static final int WS_TIMESTAMP = 18;
        public static final int STR_WH_COD_STR_DATO = 30;
        public static final int IND_USING_AREA = 4;
        public static final int IND_CALL_USING = 4;
        public static final int COMODO_CODICE_STR_DATO = 30;
        public static final int COMODO_SERV_CONVERSIONE = 8;
        public static final int COMODO_TIPO_MOVIMENTO = 5;
        public static final int R999_CODICE_STR_DATO = 30;
        public static final int OUTPUT_IDSS0020 = Idso0021.Len.IDSO0021_AREA;
        public static final int IND_SINONIM = 1;
        public static final int INPUT_IDSS0020 = Idsi0021Area.Len.IDSI0021_AREA;
        public static final int WK_SQLCODE = 9;
        public static final int PGM_NAME_IDSS0080 = 8;
        public static final int WK_NUMERO_LIVELLO_RED = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
