package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRE-PATTUITO-INI<br>
 * Variable: B03-PRE-PATTUITO-INI from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PrePattuitoIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PrePattuitoIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRE_PATTUITO_INI;
    }

    public void setB03PrePattuitoIni(AfDecimal b03PrePattuitoIni) {
        writeDecimalAsPacked(Pos.B03_PRE_PATTUITO_INI, b03PrePattuitoIni.copy());
    }

    public void setB03PrePattuitoIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRE_PATTUITO_INI, Pos.B03_PRE_PATTUITO_INI);
    }

    /**Original name: B03-PRE-PATTUITO-INI<br>*/
    public AfDecimal getB03PrePattuitoIni() {
        return readPackedAsDecimal(Pos.B03_PRE_PATTUITO_INI, Len.Int.B03_PRE_PATTUITO_INI, Len.Fract.B03_PRE_PATTUITO_INI);
    }

    public byte[] getB03PrePattuitoIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRE_PATTUITO_INI, Pos.B03_PRE_PATTUITO_INI);
        return buffer;
    }

    public void setB03PrePattuitoIniNull(String b03PrePattuitoIniNull) {
        writeString(Pos.B03_PRE_PATTUITO_INI_NULL, b03PrePattuitoIniNull, Len.B03_PRE_PATTUITO_INI_NULL);
    }

    /**Original name: B03-PRE-PATTUITO-INI-NULL<br>*/
    public String getB03PrePattuitoIniNull() {
        return readString(Pos.B03_PRE_PATTUITO_INI_NULL, Len.B03_PRE_PATTUITO_INI_NULL);
    }

    public String getB03PrePattuitoIniNullFormatted() {
        return Functions.padBlanks(getB03PrePattuitoIniNull(), Len.B03_PRE_PATTUITO_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRE_PATTUITO_INI = 1;
        public static final int B03_PRE_PATTUITO_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRE_PATTUITO_INI = 8;
        public static final int B03_PRE_PATTUITO_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRE_PATTUITO_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRE_PATTUITO_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
