package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-PAG-REN<br>
 * Variable: WB03-DUR-PAG-REN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DurPagRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DurPagRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR_PAG_REN;
    }

    public void setWb03DurPagRen(int wb03DurPagRen) {
        writeIntAsPacked(Pos.WB03_DUR_PAG_REN, wb03DurPagRen, Len.Int.WB03_DUR_PAG_REN);
    }

    public void setWb03DurPagRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR_PAG_REN, Pos.WB03_DUR_PAG_REN);
    }

    /**Original name: WB03-DUR-PAG-REN<br>*/
    public int getWb03DurPagRen() {
        return readPackedAsInt(Pos.WB03_DUR_PAG_REN, Len.Int.WB03_DUR_PAG_REN);
    }

    public byte[] getWb03DurPagRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR_PAG_REN, Pos.WB03_DUR_PAG_REN);
        return buffer;
    }

    public void setWb03DurPagRenNull(String wb03DurPagRenNull) {
        writeString(Pos.WB03_DUR_PAG_REN_NULL, wb03DurPagRenNull, Len.WB03_DUR_PAG_REN_NULL);
    }

    /**Original name: WB03-DUR-PAG-REN-NULL<br>*/
    public String getWb03DurPagRenNull() {
        return readString(Pos.WB03_DUR_PAG_REN_NULL, Len.WB03_DUR_PAG_REN_NULL);
    }

    public String getWb03DurPagRenNullFormatted() {
        return Functions.padBlanks(getWb03DurPagRenNull(), Len.WB03_DUR_PAG_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR_PAG_REN = 1;
        public static final int WB03_DUR_PAG_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR_PAG_REN = 3;
        public static final int WB03_DUR_PAG_REN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR_PAG_REN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
