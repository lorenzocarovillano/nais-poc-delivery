package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP61-PRE-LRD-DAL2007<br>
 * Variable: WP61-PRE-LRD-DAL2007 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp61PreLrdDal2007 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp61PreLrdDal2007() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP61_PRE_LRD_DAL2007;
    }

    public void setWp61PreLrdDal2007(AfDecimal wp61PreLrdDal2007) {
        writeDecimalAsPacked(Pos.WP61_PRE_LRD_DAL2007, wp61PreLrdDal2007.copy());
    }

    public void setWp61PreLrdDal2007FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP61_PRE_LRD_DAL2007, Pos.WP61_PRE_LRD_DAL2007);
    }

    /**Original name: WP61-PRE-LRD-DAL2007<br>*/
    public AfDecimal getWp61PreLrdDal2007() {
        return readPackedAsDecimal(Pos.WP61_PRE_LRD_DAL2007, Len.Int.WP61_PRE_LRD_DAL2007, Len.Fract.WP61_PRE_LRD_DAL2007);
    }

    public byte[] getWp61PreLrdDal2007AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP61_PRE_LRD_DAL2007, Pos.WP61_PRE_LRD_DAL2007);
        return buffer;
    }

    public void setWp61PreLrdDal2007Null(String wp61PreLrdDal2007Null) {
        writeString(Pos.WP61_PRE_LRD_DAL2007_NULL, wp61PreLrdDal2007Null, Len.WP61_PRE_LRD_DAL2007_NULL);
    }

    /**Original name: WP61-PRE-LRD-DAL2007-NULL<br>*/
    public String getWp61PreLrdDal2007Null() {
        return readString(Pos.WP61_PRE_LRD_DAL2007_NULL, Len.WP61_PRE_LRD_DAL2007_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP61_PRE_LRD_DAL2007 = 1;
        public static final int WP61_PRE_LRD_DAL2007_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_PRE_LRD_DAL2007 = 8;
        public static final int WP61_PRE_LRD_DAL2007_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP61_PRE_LRD_DAL2007 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_PRE_LRD_DAL2007 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
