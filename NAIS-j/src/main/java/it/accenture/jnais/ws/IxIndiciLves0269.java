package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LVES0269<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLves0269 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-GRZ
    private short tabGrz = ((short)0);
    //Original name: IX-TAB-TGA
    private short tabTga = ((short)0);
    //Original name: IX-TAB-SPG
    private short tabSpg = ((short)0);
    //Original name: IX-TAB-PAR
    private short tabPar = ((short)0);
    //Original name: IX-AREA-ISPC0140
    private short areaIspc0140 = ((short)0);
    //Original name: IX-AREA-PAGINA
    private short areaPagina = ((short)0);
    //Original name: IX-TP-PREMIO
    private short tpPremio = ((short)0);
    //Original name: IX-COMPONENTE
    private short componente = ((short)0);
    //Original name: IX-TAB-CALC
    private short tabCalc = ((short)0);
    //Original name: IX-TAB-TIT
    private short tabTit = ((short)0);
    //Original name: IX-TAB-RATA
    private short tabRata = ((short)0);
    //Original name: IX-ALL
    private short all = ((short)0);
    //Original name: IX-MAX
    private short max = ((short)0);
    //Original name: IX-APPO
    private short appo = ((short)0);
    //Original name: IX-DCLGEN
    private short dclgen = ((short)0);
    //Original name: IX-SPG
    private short spg = ((short)0);
    //Original name: IX-APPO-GRZ
    private short appoGrz = ((short)0);
    //Original name: IX-APPO-SPG
    private short appoSpg = ((short)0);
    //Original name: IX-APPO-TGA
    private short appoTga = ((short)0);
    //Original name: IX-RIC-GRZ
    private short ricGrz = ((short)0);
    //Original name: IX-TAB-ASS
    private short tabAss = ((short)0);
    //Original name: IX-CNT
    private short cnt = ((short)0);
    //Original name: IX-RIC-PMO
    private short ricPmo = ((short)0);
    //Original name: IX-RIC-POG
    private short ricPog = ((short)0);
    //Original name: IX-RIC-RAN
    private short ricRan = ((short)0);
    //Original name: IX-TAB-LCCC0490
    private short tabLccc0490 = ((short)0);
    //Original name: IX-RIC-GAR
    private short ricGar = ((short)0);
    //Original name: IX-PAG-IAS
    private short pagIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-IND-GAR
    private short indGar = ((short)0);
    //Original name: IX-AREA-SCHEDA-P
    private short areaSchedaP = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-AREA-SCHEDA-T
    private short areaSchedaT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-VAR-P
    private short tabVarP = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-VAR-T
    private short tabVarT = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setTabGrz(short tabGrz) {
        this.tabGrz = tabGrz;
    }

    public short getTabGrz() {
        return this.tabGrz;
    }

    public void setTabTga(short tabTga) {
        this.tabTga = tabTga;
    }

    public short getTabTga() {
        return this.tabTga;
    }

    public void setTabSpg(short tabSpg) {
        this.tabSpg = tabSpg;
    }

    public short getTabSpg() {
        return this.tabSpg;
    }

    public void setTabPar(short tabPar) {
        this.tabPar = tabPar;
    }

    public short getTabPar() {
        return this.tabPar;
    }

    public void setAreaIspc0140(short areaIspc0140) {
        this.areaIspc0140 = areaIspc0140;
    }

    public short getAreaIspc0140() {
        return this.areaIspc0140;
    }

    public void setAreaPagina(short areaPagina) {
        this.areaPagina = areaPagina;
    }

    public short getAreaPagina() {
        return this.areaPagina;
    }

    public void setTpPremio(short tpPremio) {
        this.tpPremio = tpPremio;
    }

    public short getTpPremio() {
        return this.tpPremio;
    }

    public void setComponente(short componente) {
        this.componente = componente;
    }

    public short getComponente() {
        return this.componente;
    }

    public void setTabCalc(short tabCalc) {
        this.tabCalc = tabCalc;
    }

    public short getTabCalc() {
        return this.tabCalc;
    }

    public void setTabTit(short tabTit) {
        this.tabTit = tabTit;
    }

    public short getTabTit() {
        return this.tabTit;
    }

    public void setTabRata(short tabRata) {
        this.tabRata = tabRata;
    }

    public short getTabRata() {
        return this.tabRata;
    }

    public void setAll(short all) {
        this.all = all;
    }

    public short getAll() {
        return this.all;
    }

    public void setMax(short max) {
        this.max = max;
    }

    public short getMax() {
        return this.max;
    }

    public void setAppo(short appo) {
        this.appo = appo;
    }

    public short getAppo() {
        return this.appo;
    }

    public void setDclgen(short dclgen) {
        this.dclgen = dclgen;
    }

    public short getDclgen() {
        return this.dclgen;
    }

    public void setSpg(short spg) {
        this.spg = spg;
    }

    public short getSpg() {
        return this.spg;
    }

    public void setAppoGrz(short appoGrz) {
        this.appoGrz = appoGrz;
    }

    public short getAppoGrz() {
        return this.appoGrz;
    }

    public void setAppoSpg(short appoSpg) {
        this.appoSpg = appoSpg;
    }

    public short getAppoSpg() {
        return this.appoSpg;
    }

    public void setAppoTga(short appoTga) {
        this.appoTga = appoTga;
    }

    public short getAppoTga() {
        return this.appoTga;
    }

    public void setRicGrz(short ricGrz) {
        this.ricGrz = ricGrz;
    }

    public short getRicGrz() {
        return this.ricGrz;
    }

    public void setTabAss(short tabAss) {
        this.tabAss = tabAss;
    }

    public short getTabAss() {
        return this.tabAss;
    }

    public void setCnt(short cnt) {
        this.cnt = cnt;
    }

    public short getCnt() {
        return this.cnt;
    }

    public void setRicPmo(short ricPmo) {
        this.ricPmo = ricPmo;
    }

    public short getRicPmo() {
        return this.ricPmo;
    }

    public void setRicPog(short ricPog) {
        this.ricPog = ricPog;
    }

    public short getRicPog() {
        return this.ricPog;
    }

    public void setRicRan(short ricRan) {
        this.ricRan = ricRan;
    }

    public short getRicRan() {
        return this.ricRan;
    }

    public void setTabLccc0490(short tabLccc0490) {
        this.tabLccc0490 = tabLccc0490;
    }

    public short getTabLccc0490() {
        return this.tabLccc0490;
    }

    public void setRicGar(short ricGar) {
        this.ricGar = ricGar;
    }

    public short getRicGar() {
        return this.ricGar;
    }

    public void setPagIas(short pagIas) {
        this.pagIas = pagIas;
    }

    public short getPagIas() {
        return this.pagIas;
    }

    public void setIndGar(short indGar) {
        this.indGar = indGar;
    }

    public short getIndGar() {
        return this.indGar;
    }

    public void setAreaSchedaP(short areaSchedaP) {
        this.areaSchedaP = areaSchedaP;
    }

    public short getAreaSchedaP() {
        return this.areaSchedaP;
    }

    public void setAreaSchedaT(short areaSchedaT) {
        this.areaSchedaT = areaSchedaT;
    }

    public short getAreaSchedaT() {
        return this.areaSchedaT;
    }

    public void setTabVarP(short tabVarP) {
        this.tabVarP = tabVarP;
    }

    public short getTabVarP() {
        return this.tabVarP;
    }

    public void setTabVarT(short tabVarT) {
        this.tabVarT = tabVarT;
    }

    public short getTabVarT() {
        return this.tabVarT;
    }
}
