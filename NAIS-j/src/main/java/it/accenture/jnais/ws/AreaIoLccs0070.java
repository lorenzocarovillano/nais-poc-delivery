package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.Lccc0070Funzione;

/**Original name: AREA-IO-LCCS0070<br>
 * Variable: AREA-IO-LCCS0070 from program LCCS0070<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoLccs0070 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LCCC0070-FUNZIONE
    private Lccc0070Funzione funzione = new Lccc0070Funzione();
    //Original name: LCCC0070-ID-OGGETTO
    private int idOggetto = DefaultValues.INT_VAL;
    //Original name: LCCC0070-TP-OGGETTO
    private String tpOggetto = DefaultValues.stringVal(Len.TP_OGGETTO);
    //Original name: LCCC0070-IB-OGGETTO
    private String ibOggetto = DefaultValues.stringVal(Len.IB_OGGETTO);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IO_LCCS0070;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIoLccs0070Bytes(buf);
    }

    public String getAreaIoLccs0070Formatted() {
        return MarshalByteExt.bufferToStr(getAreaIoLccs0070Bytes());
    }

    public void setAreaIoLccs0070Bytes(byte[] buffer) {
        setAreaIoLccs0070Bytes(buffer, 1);
    }

    public byte[] getAreaIoLccs0070Bytes() {
        byte[] buffer = new byte[Len.AREA_IO_LCCS0070];
        return getAreaIoLccs0070Bytes(buffer, 1);
    }

    public void setAreaIoLccs0070Bytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
    }

    public byte[] getAreaIoLccs0070Bytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        funzione.setFunzione(MarshalByte.readString(buffer, position, Lccc0070Funzione.Len.FUNZIONE));
        position += Lccc0070Funzione.Len.FUNZIONE;
        idOggetto = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGGETTO, 0);
        position += Len.ID_OGGETTO;
        tpOggetto = MarshalByte.readString(buffer, position, Len.TP_OGGETTO);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, funzione.getFunzione(), Lccc0070Funzione.Len.FUNZIONE);
        position += Lccc0070Funzione.Len.FUNZIONE;
        MarshalByte.writeIntAsPacked(buffer, position, idOggetto, Len.Int.ID_OGGETTO, 0);
        position += Len.ID_OGGETTO;
        MarshalByte.writeString(buffer, position, tpOggetto, Len.TP_OGGETTO);
        return buffer;
    }

    public void setIdOggetto(int idOggetto) {
        this.idOggetto = idOggetto;
    }

    public int getIdOggetto() {
        return this.idOggetto;
    }

    public void setTpOggetto(String tpOggetto) {
        this.tpOggetto = Functions.subString(tpOggetto, Len.TP_OGGETTO);
    }

    public String getTpOggetto() {
        return this.tpOggetto;
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        ibOggetto = MarshalByte.readString(buffer, position, Len.IB_OGGETTO);
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ibOggetto, Len.IB_OGGETTO);
        return buffer;
    }

    public void setIbOggetto(String ibOggetto) {
        this.ibOggetto = Functions.subString(ibOggetto, Len.IB_OGGETTO);
    }

    public String getIbOggetto() {
        return this.ibOggetto;
    }

    public Lccc0070Funzione getFunzione() {
        return funzione;
    }

    @Override
    public byte[] serialize() {
        return getAreaIoLccs0070Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGGETTO = 5;
        public static final int TP_OGGETTO = 2;
        public static final int DATI_INPUT = Lccc0070Funzione.Len.FUNZIONE + ID_OGGETTO + TP_OGGETTO;
        public static final int IB_OGGETTO = 40;
        public static final int DATI_OUTPUT = IB_OGGETTO;
        public static final int AREA_IO_LCCS0070 = DATI_INPUT + DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGGETTO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
