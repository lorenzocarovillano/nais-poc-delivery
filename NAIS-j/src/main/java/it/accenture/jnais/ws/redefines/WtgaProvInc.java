package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PROV-INC<br>
 * Variable: WTGA-PROV-INC from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PROV_INC;
    }

    public void setWtgaProvInc(AfDecimal wtgaProvInc) {
        writeDecimalAsPacked(Pos.WTGA_PROV_INC, wtgaProvInc.copy());
    }

    public void setWtgaProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PROV_INC, Pos.WTGA_PROV_INC);
    }

    /**Original name: WTGA-PROV-INC<br>*/
    public AfDecimal getWtgaProvInc() {
        return readPackedAsDecimal(Pos.WTGA_PROV_INC, Len.Int.WTGA_PROV_INC, Len.Fract.WTGA_PROV_INC);
    }

    public byte[] getWtgaProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PROV_INC, Pos.WTGA_PROV_INC);
        return buffer;
    }

    public void initWtgaProvIncSpaces() {
        fill(Pos.WTGA_PROV_INC, Len.WTGA_PROV_INC, Types.SPACE_CHAR);
    }

    public void setWtgaProvIncNull(String wtgaProvIncNull) {
        writeString(Pos.WTGA_PROV_INC_NULL, wtgaProvIncNull, Len.WTGA_PROV_INC_NULL);
    }

    /**Original name: WTGA-PROV-INC-NULL<br>*/
    public String getWtgaProvIncNull() {
        return readString(Pos.WTGA_PROV_INC_NULL, Len.WTGA_PROV_INC_NULL);
    }

    public String getWtgaProvIncNullFormatted() {
        return Functions.padBlanks(getWtgaProvIncNull(), Len.WTGA_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PROV_INC = 1;
        public static final int WTGA_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PROV_INC = 8;
        public static final int WTGA_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
