package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP58-IMPST-BOLLO-TOT-V<br>
 * Variable: WP58-IMPST-BOLLO-TOT-V from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp58ImpstBolloTotV extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp58ImpstBolloTotV() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP58_IMPST_BOLLO_TOT_V;
    }

    public void setWp58ImpstBolloTotV(AfDecimal wp58ImpstBolloTotV) {
        writeDecimalAsPacked(Pos.WP58_IMPST_BOLLO_TOT_V, wp58ImpstBolloTotV.copy());
    }

    public void setWp58ImpstBolloTotVFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP58_IMPST_BOLLO_TOT_V, Pos.WP58_IMPST_BOLLO_TOT_V);
    }

    /**Original name: WP58-IMPST-BOLLO-TOT-V<br>*/
    public AfDecimal getWp58ImpstBolloTotV() {
        return readPackedAsDecimal(Pos.WP58_IMPST_BOLLO_TOT_V, Len.Int.WP58_IMPST_BOLLO_TOT_V, Len.Fract.WP58_IMPST_BOLLO_TOT_V);
    }

    public byte[] getWp58ImpstBolloTotVAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP58_IMPST_BOLLO_TOT_V, Pos.WP58_IMPST_BOLLO_TOT_V);
        return buffer;
    }

    public void initWp58ImpstBolloTotVSpaces() {
        fill(Pos.WP58_IMPST_BOLLO_TOT_V, Len.WP58_IMPST_BOLLO_TOT_V, Types.SPACE_CHAR);
    }

    public void setDp58ImpstBolloTotVNull(String dp58ImpstBolloTotVNull) {
        writeString(Pos.WP58_IMPST_BOLLO_TOT_V_NULL, dp58ImpstBolloTotVNull, Len.DP58_IMPST_BOLLO_TOT_V_NULL);
    }

    /**Original name: DP58-IMPST-BOLLO-TOT-V-NULL<br>*/
    public String getDp58ImpstBolloTotVNull() {
        return readString(Pos.WP58_IMPST_BOLLO_TOT_V_NULL, Len.DP58_IMPST_BOLLO_TOT_V_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP58_IMPST_BOLLO_TOT_V = 1;
        public static final int WP58_IMPST_BOLLO_TOT_V_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP58_IMPST_BOLLO_TOT_V = 8;
        public static final int DP58_IMPST_BOLLO_TOT_V_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP58_IMPST_BOLLO_TOT_V = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP58_IMPST_BOLLO_TOT_V = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
