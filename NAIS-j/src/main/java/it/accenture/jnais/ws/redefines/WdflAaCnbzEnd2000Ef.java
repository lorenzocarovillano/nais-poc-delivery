package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFL-AA-CNBZ-END2000-EF<br>
 * Variable: WDFL-AA-CNBZ-END2000-EF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAaCnbzEnd2000Ef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAaCnbzEnd2000Ef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_AA_CNBZ_END2000_EF;
    }

    public void setWdflAaCnbzEnd2000Ef(int wdflAaCnbzEnd2000Ef) {
        writeIntAsPacked(Pos.WDFL_AA_CNBZ_END2000_EF, wdflAaCnbzEnd2000Ef, Len.Int.WDFL_AA_CNBZ_END2000_EF);
    }

    public void setWdflAaCnbzEnd2000EfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_AA_CNBZ_END2000_EF, Pos.WDFL_AA_CNBZ_END2000_EF);
    }

    /**Original name: WDFL-AA-CNBZ-END2000-EF<br>*/
    public int getWdflAaCnbzEnd2000Ef() {
        return readPackedAsInt(Pos.WDFL_AA_CNBZ_END2000_EF, Len.Int.WDFL_AA_CNBZ_END2000_EF);
    }

    public byte[] getWdflAaCnbzEnd2000EfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_AA_CNBZ_END2000_EF, Pos.WDFL_AA_CNBZ_END2000_EF);
        return buffer;
    }

    public void setWdflAaCnbzEnd2000EfNull(String wdflAaCnbzEnd2000EfNull) {
        writeString(Pos.WDFL_AA_CNBZ_END2000_EF_NULL, wdflAaCnbzEnd2000EfNull, Len.WDFL_AA_CNBZ_END2000_EF_NULL);
    }

    /**Original name: WDFL-AA-CNBZ-END2000-EF-NULL<br>*/
    public String getWdflAaCnbzEnd2000EfNull() {
        return readString(Pos.WDFL_AA_CNBZ_END2000_EF_NULL, Len.WDFL_AA_CNBZ_END2000_EF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_AA_CNBZ_END2000_EF = 1;
        public static final int WDFL_AA_CNBZ_END2000_EF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_AA_CNBZ_END2000_EF = 3;
        public static final int WDFL_AA_CNBZ_END2000_EF_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_AA_CNBZ_END2000_EF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
