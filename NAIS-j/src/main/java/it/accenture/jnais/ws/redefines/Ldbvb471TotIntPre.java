package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVB471-TOT-INT-PRE<br>
 * Variable: LDBVB471-TOT-INT-PRE from program LDBSB470<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ldbvb471TotIntPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Ldbvb471TotIntPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBVB471_TOT_INT_PRE;
    }

    public void setLdbvb471TotIntPre(AfDecimal ldbvb471TotIntPre) {
        writeDecimalAsPacked(Pos.LDBVB471_TOT_INT_PRE, ldbvb471TotIntPre.copy());
    }

    public void setLdbvb471TotIntPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LDBVB471_TOT_INT_PRE, Pos.LDBVB471_TOT_INT_PRE);
    }

    /**Original name: LDBVB471-TOT-INT-PRE<br>*/
    public AfDecimal getLdbvb471TotIntPre() {
        return readPackedAsDecimal(Pos.LDBVB471_TOT_INT_PRE, Len.Int.LDBVB471_TOT_INT_PRE, Len.Fract.LDBVB471_TOT_INT_PRE);
    }

    public byte[] getLdbvb471TotIntPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LDBVB471_TOT_INT_PRE, Pos.LDBVB471_TOT_INT_PRE);
        return buffer;
    }

    public void setLdbvb471TotIntPreNull(String ldbvb471TotIntPreNull) {
        writeString(Pos.LDBVB471_TOT_INT_PRE_NULL, ldbvb471TotIntPreNull, Len.LDBVB471_TOT_INT_PRE_NULL);
    }

    /**Original name: LDBVB471-TOT-INT-PRE-NULL<br>*/
    public String getLdbvb471TotIntPreNull() {
        return readString(Pos.LDBVB471_TOT_INT_PRE_NULL, Len.LDBVB471_TOT_INT_PRE_NULL);
    }

    public String getLdbvb471TotIntPreNullFormatted() {
        return Functions.padBlanks(getLdbvb471TotIntPreNull(), Len.LDBVB471_TOT_INT_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LDBVB471_TOT_INT_PRE = 1;
        public static final int LDBVB471_TOT_INT_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBVB471_TOT_INT_PRE = 8;
        public static final int LDBVB471_TOT_INT_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int LDBVB471_TOT_INT_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBVB471_TOT_INT_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
