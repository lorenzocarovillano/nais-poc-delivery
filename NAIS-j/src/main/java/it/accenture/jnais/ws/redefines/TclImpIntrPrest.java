package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-INTR-PREST<br>
 * Variable: TCL-IMP-INTR-PREST from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_INTR_PREST;
    }

    public void setTclImpIntrPrest(AfDecimal tclImpIntrPrest) {
        writeDecimalAsPacked(Pos.TCL_IMP_INTR_PREST, tclImpIntrPrest.copy());
    }

    public void setTclImpIntrPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_INTR_PREST, Pos.TCL_IMP_INTR_PREST);
    }

    /**Original name: TCL-IMP-INTR-PREST<br>*/
    public AfDecimal getTclImpIntrPrest() {
        return readPackedAsDecimal(Pos.TCL_IMP_INTR_PREST, Len.Int.TCL_IMP_INTR_PREST, Len.Fract.TCL_IMP_INTR_PREST);
    }

    public byte[] getTclImpIntrPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_INTR_PREST, Pos.TCL_IMP_INTR_PREST);
        return buffer;
    }

    public void setTclImpIntrPrestNull(String tclImpIntrPrestNull) {
        writeString(Pos.TCL_IMP_INTR_PREST_NULL, tclImpIntrPrestNull, Len.TCL_IMP_INTR_PREST_NULL);
    }

    /**Original name: TCL-IMP-INTR-PREST-NULL<br>*/
    public String getTclImpIntrPrestNull() {
        return readString(Pos.TCL_IMP_INTR_PREST_NULL, Len.TCL_IMP_INTR_PREST_NULL);
    }

    public String getTclImpIntrPrestNullFormatted() {
        return Functions.padBlanks(getTclImpIntrPrestNull(), Len.TCL_IMP_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_INTR_PREST = 1;
        public static final int TCL_IMP_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_INTR_PREST = 8;
        public static final int TCL_IMP_INTR_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_INTR_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
