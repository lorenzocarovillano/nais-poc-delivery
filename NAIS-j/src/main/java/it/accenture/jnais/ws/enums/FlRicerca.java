package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FL-RICERCA<br>
 * Variable: FL-RICERCA from program LCCS0234<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlRicerca {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char NO_TROVATO = '0';
    public static final char SI_TROVATO = '1';

    //==== METHODS ====
    public void setFlRicerca(char flRicerca) {
        this.value = flRicerca;
    }

    public char getFlRicerca() {
        return this.value;
    }

    public boolean isNoTrovato() {
        return value == NO_TROVATO;
    }

    public void setNoTrovato() {
        value = NO_TROVATO;
    }

    public boolean isSiTrovato() {
        return value == SI_TROVATO;
    }

    public void setSiTrovato() {
        value = SI_TROVATO;
    }
}
