package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-VIS-CALC<br>
 * Variable: WDFL-IMPB-VIS-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbVisCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbVisCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_VIS_CALC;
    }

    public void setWdflImpbVisCalc(AfDecimal wdflImpbVisCalc) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_VIS_CALC, wdflImpbVisCalc.copy());
    }

    public void setWdflImpbVisCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_VIS_CALC, Pos.WDFL_IMPB_VIS_CALC);
    }

    /**Original name: WDFL-IMPB-VIS-CALC<br>*/
    public AfDecimal getWdflImpbVisCalc() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_VIS_CALC, Len.Int.WDFL_IMPB_VIS_CALC, Len.Fract.WDFL_IMPB_VIS_CALC);
    }

    public byte[] getWdflImpbVisCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_VIS_CALC, Pos.WDFL_IMPB_VIS_CALC);
        return buffer;
    }

    public void setWdflImpbVisCalcNull(String wdflImpbVisCalcNull) {
        writeString(Pos.WDFL_IMPB_VIS_CALC_NULL, wdflImpbVisCalcNull, Len.WDFL_IMPB_VIS_CALC_NULL);
    }

    /**Original name: WDFL-IMPB-VIS-CALC-NULL<br>*/
    public String getWdflImpbVisCalcNull() {
        return readString(Pos.WDFL_IMPB_VIS_CALC_NULL, Len.WDFL_IMPB_VIS_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_VIS_CALC = 1;
        public static final int WDFL_IMPB_VIS_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_VIS_CALC = 8;
        public static final int WDFL_IMPB_VIS_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_VIS_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_VIS_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
