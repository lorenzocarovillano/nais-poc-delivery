package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WVAS-DT-VALZZ<br>
 * Variable: WVAS-DT-VALZZ from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasDtValzz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasDtValzz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_DT_VALZZ;
    }

    public void setWvasDtValzz(int wvasDtValzz) {
        writeIntAsPacked(Pos.WVAS_DT_VALZZ, wvasDtValzz, Len.Int.WVAS_DT_VALZZ);
    }

    /**Original name: WVAS-DT-VALZZ<br>*/
    public int getWvasDtValzz() {
        return readPackedAsInt(Pos.WVAS_DT_VALZZ, Len.Int.WVAS_DT_VALZZ);
    }

    public void setWvasDtValzzNull(String wvasDtValzzNull) {
        writeString(Pos.WVAS_DT_VALZZ_NULL, wvasDtValzzNull, Len.WVAS_DT_VALZZ_NULL);
    }

    /**Original name: WVAS-DT-VALZZ-NULL<br>*/
    public String getWvasDtValzzNull() {
        return readString(Pos.WVAS_DT_VALZZ_NULL, Len.WVAS_DT_VALZZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_DT_VALZZ = 1;
        public static final int WVAS_DT_VALZZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_DT_VALZZ = 5;
        public static final int WVAS_DT_VALZZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_DT_VALZZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
