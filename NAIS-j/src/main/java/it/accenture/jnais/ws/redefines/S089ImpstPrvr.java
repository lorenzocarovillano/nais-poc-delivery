package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPST-PRVR<br>
 * Variable: S089-IMPST-PRVR from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpstPrvr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpstPrvr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPST_PRVR;
    }

    public void setWlquImpstPrvr(AfDecimal wlquImpstPrvr) {
        writeDecimalAsPacked(Pos.S089_IMPST_PRVR, wlquImpstPrvr.copy());
    }

    public void setWlquImpstPrvrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPST_PRVR, Pos.S089_IMPST_PRVR);
    }

    /**Original name: WLQU-IMPST-PRVR<br>*/
    public AfDecimal getWlquImpstPrvr() {
        return readPackedAsDecimal(Pos.S089_IMPST_PRVR, Len.Int.WLQU_IMPST_PRVR, Len.Fract.WLQU_IMPST_PRVR);
    }

    public byte[] getWlquImpstPrvrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPST_PRVR, Pos.S089_IMPST_PRVR);
        return buffer;
    }

    public void initWlquImpstPrvrSpaces() {
        fill(Pos.S089_IMPST_PRVR, Len.S089_IMPST_PRVR, Types.SPACE_CHAR);
    }

    public void setWlquImpstPrvrNull(String wlquImpstPrvrNull) {
        writeString(Pos.S089_IMPST_PRVR_NULL, wlquImpstPrvrNull, Len.WLQU_IMPST_PRVR_NULL);
    }

    /**Original name: WLQU-IMPST-PRVR-NULL<br>*/
    public String getWlquImpstPrvrNull() {
        return readString(Pos.S089_IMPST_PRVR_NULL, Len.WLQU_IMPST_PRVR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPST_PRVR = 1;
        public static final int S089_IMPST_PRVR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPST_PRVR = 8;
        public static final int WLQU_IMPST_PRVR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_PRVR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_PRVR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
