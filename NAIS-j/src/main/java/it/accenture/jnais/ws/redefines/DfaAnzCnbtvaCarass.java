package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-ANZ-CNBTVA-CARASS<br>
 * Variable: DFA-ANZ-CNBTVA-CARASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaAnzCnbtvaCarass extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaAnzCnbtvaCarass() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_ANZ_CNBTVA_CARASS;
    }

    public void setDfaAnzCnbtvaCarass(short dfaAnzCnbtvaCarass) {
        writeShortAsPacked(Pos.DFA_ANZ_CNBTVA_CARASS, dfaAnzCnbtvaCarass, Len.Int.DFA_ANZ_CNBTVA_CARASS);
    }

    public void setDfaAnzCnbtvaCarassFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_ANZ_CNBTVA_CARASS, Pos.DFA_ANZ_CNBTVA_CARASS);
    }

    /**Original name: DFA-ANZ-CNBTVA-CARASS<br>*/
    public short getDfaAnzCnbtvaCarass() {
        return readPackedAsShort(Pos.DFA_ANZ_CNBTVA_CARASS, Len.Int.DFA_ANZ_CNBTVA_CARASS);
    }

    public byte[] getDfaAnzCnbtvaCarassAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_ANZ_CNBTVA_CARASS, Pos.DFA_ANZ_CNBTVA_CARASS);
        return buffer;
    }

    public void setDfaAnzCnbtvaCarassNull(String dfaAnzCnbtvaCarassNull) {
        writeString(Pos.DFA_ANZ_CNBTVA_CARASS_NULL, dfaAnzCnbtvaCarassNull, Len.DFA_ANZ_CNBTVA_CARASS_NULL);
    }

    /**Original name: DFA-ANZ-CNBTVA-CARASS-NULL<br>*/
    public String getDfaAnzCnbtvaCarassNull() {
        return readString(Pos.DFA_ANZ_CNBTVA_CARASS_NULL, Len.DFA_ANZ_CNBTVA_CARASS_NULL);
    }

    public String getDfaAnzCnbtvaCarassNullFormatted() {
        return Functions.padBlanks(getDfaAnzCnbtvaCarassNull(), Len.DFA_ANZ_CNBTVA_CARASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_ANZ_CNBTVA_CARASS = 1;
        public static final int DFA_ANZ_CNBTVA_CARASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_ANZ_CNBTVA_CARASS = 3;
        public static final int DFA_ANZ_CNBTVA_CARASS_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_ANZ_CNBTVA_CARASS = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
