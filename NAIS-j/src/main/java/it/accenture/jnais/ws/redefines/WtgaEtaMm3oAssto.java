package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ETA-MM-3O-ASSTO<br>
 * Variable: WTGA-ETA-MM-3O-ASSTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaEtaMm3oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaEtaMm3oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ETA_MM3O_ASSTO;
    }

    public void setWtgaEtaMm3oAssto(short wtgaEtaMm3oAssto) {
        writeShortAsPacked(Pos.WTGA_ETA_MM3O_ASSTO, wtgaEtaMm3oAssto, Len.Int.WTGA_ETA_MM3O_ASSTO);
    }

    public void setWtgaEtaMm3oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ETA_MM3O_ASSTO, Pos.WTGA_ETA_MM3O_ASSTO);
    }

    /**Original name: WTGA-ETA-MM-3O-ASSTO<br>*/
    public short getWtgaEtaMm3oAssto() {
        return readPackedAsShort(Pos.WTGA_ETA_MM3O_ASSTO, Len.Int.WTGA_ETA_MM3O_ASSTO);
    }

    public byte[] getWtgaEtaMm3oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ETA_MM3O_ASSTO, Pos.WTGA_ETA_MM3O_ASSTO);
        return buffer;
    }

    public void initWtgaEtaMm3oAsstoSpaces() {
        fill(Pos.WTGA_ETA_MM3O_ASSTO, Len.WTGA_ETA_MM3O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWtgaEtaMm3oAsstoNull(String wtgaEtaMm3oAsstoNull) {
        writeString(Pos.WTGA_ETA_MM3O_ASSTO_NULL, wtgaEtaMm3oAsstoNull, Len.WTGA_ETA_MM3O_ASSTO_NULL);
    }

    /**Original name: WTGA-ETA-MM-3O-ASSTO-NULL<br>*/
    public String getWtgaEtaMm3oAsstoNull() {
        return readString(Pos.WTGA_ETA_MM3O_ASSTO_NULL, Len.WTGA_ETA_MM3O_ASSTO_NULL);
    }

    public String getWtgaEtaMm3oAsstoNullFormatted() {
        return Functions.padBlanks(getWtgaEtaMm3oAsstoNull(), Len.WTGA_ETA_MM3O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ETA_MM3O_ASSTO = 1;
        public static final int WTGA_ETA_MM3O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ETA_MM3O_ASSTO = 2;
        public static final int WTGA_ETA_MM3O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ETA_MM3O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
