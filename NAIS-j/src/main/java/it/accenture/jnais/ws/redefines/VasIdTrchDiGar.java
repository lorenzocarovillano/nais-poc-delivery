package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-ID-TRCH-DI-GAR<br>
 * Variable: VAS-ID-TRCH-DI-GAR from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasIdTrchDiGar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasIdTrchDiGar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_ID_TRCH_DI_GAR;
    }

    public void setVasIdTrchDiGar(int vasIdTrchDiGar) {
        writeIntAsPacked(Pos.VAS_ID_TRCH_DI_GAR, vasIdTrchDiGar, Len.Int.VAS_ID_TRCH_DI_GAR);
    }

    public void setVasIdTrchDiGarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_ID_TRCH_DI_GAR, Pos.VAS_ID_TRCH_DI_GAR);
    }

    /**Original name: VAS-ID-TRCH-DI-GAR<br>*/
    public int getVasIdTrchDiGar() {
        return readPackedAsInt(Pos.VAS_ID_TRCH_DI_GAR, Len.Int.VAS_ID_TRCH_DI_GAR);
    }

    public byte[] getVasIdTrchDiGarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_ID_TRCH_DI_GAR, Pos.VAS_ID_TRCH_DI_GAR);
        return buffer;
    }

    public void setVasIdTrchDiGarNull(String vasIdTrchDiGarNull) {
        writeString(Pos.VAS_ID_TRCH_DI_GAR_NULL, vasIdTrchDiGarNull, Len.VAS_ID_TRCH_DI_GAR_NULL);
    }

    /**Original name: VAS-ID-TRCH-DI-GAR-NULL<br>*/
    public String getVasIdTrchDiGarNull() {
        return readString(Pos.VAS_ID_TRCH_DI_GAR_NULL, Len.VAS_ID_TRCH_DI_GAR_NULL);
    }

    public String getVasIdTrchDiGarNullFormatted() {
        return Functions.padBlanks(getVasIdTrchDiGarNull(), Len.VAS_ID_TRCH_DI_GAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_ID_TRCH_DI_GAR = 1;
        public static final int VAS_ID_TRCH_DI_GAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_ID_TRCH_DI_GAR = 5;
        public static final int VAS_ID_TRCH_DI_GAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_ID_TRCH_DI_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
