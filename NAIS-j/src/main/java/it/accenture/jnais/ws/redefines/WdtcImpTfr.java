package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-IMP-TFR<br>
 * Variable: WDTC-IMP-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_IMP_TFR;
    }

    public void setWdtcImpTfr(AfDecimal wdtcImpTfr) {
        writeDecimalAsPacked(Pos.WDTC_IMP_TFR, wdtcImpTfr.copy());
    }

    public void setWdtcImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_IMP_TFR, Pos.WDTC_IMP_TFR);
    }

    /**Original name: WDTC-IMP-TFR<br>*/
    public AfDecimal getWdtcImpTfr() {
        return readPackedAsDecimal(Pos.WDTC_IMP_TFR, Len.Int.WDTC_IMP_TFR, Len.Fract.WDTC_IMP_TFR);
    }

    public byte[] getWdtcImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_IMP_TFR, Pos.WDTC_IMP_TFR);
        return buffer;
    }

    public void initWdtcImpTfrSpaces() {
        fill(Pos.WDTC_IMP_TFR, Len.WDTC_IMP_TFR, Types.SPACE_CHAR);
    }

    public void setWdtcImpTfrNull(String wdtcImpTfrNull) {
        writeString(Pos.WDTC_IMP_TFR_NULL, wdtcImpTfrNull, Len.WDTC_IMP_TFR_NULL);
    }

    /**Original name: WDTC-IMP-TFR-NULL<br>*/
    public String getWdtcImpTfrNull() {
        return readString(Pos.WDTC_IMP_TFR_NULL, Len.WDTC_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_TFR = 1;
        public static final int WDTC_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_TFR = 8;
        public static final int WDTC_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
