package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-PATTUITO<br>
 * Variable: TGA-PRE-PATTUITO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPrePattuito extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPrePattuito() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_PATTUITO;
    }

    public void setTgaPrePattuito(AfDecimal tgaPrePattuito) {
        writeDecimalAsPacked(Pos.TGA_PRE_PATTUITO, tgaPrePattuito.copy());
    }

    public void setTgaPrePattuitoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_PATTUITO, Pos.TGA_PRE_PATTUITO);
    }

    /**Original name: TGA-PRE-PATTUITO<br>*/
    public AfDecimal getTgaPrePattuito() {
        return readPackedAsDecimal(Pos.TGA_PRE_PATTUITO, Len.Int.TGA_PRE_PATTUITO, Len.Fract.TGA_PRE_PATTUITO);
    }

    public byte[] getTgaPrePattuitoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_PATTUITO, Pos.TGA_PRE_PATTUITO);
        return buffer;
    }

    public void setTgaPrePattuitoNull(String tgaPrePattuitoNull) {
        writeString(Pos.TGA_PRE_PATTUITO_NULL, tgaPrePattuitoNull, Len.TGA_PRE_PATTUITO_NULL);
    }

    /**Original name: TGA-PRE-PATTUITO-NULL<br>*/
    public String getTgaPrePattuitoNull() {
        return readString(Pos.TGA_PRE_PATTUITO_NULL, Len.TGA_PRE_PATTUITO_NULL);
    }

    public String getTgaPrePattuitoNullFormatted() {
        return Functions.padBlanks(getTgaPrePattuitoNull(), Len.TGA_PRE_PATTUITO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_PATTUITO = 1;
        public static final int TGA_PRE_PATTUITO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_PATTUITO = 8;
        public static final int TGA_PRE_PATTUITO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_PATTUITO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_PATTUITO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
