package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-MATU-K2<br>
 * Variable: DFA-MATU-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaMatuK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaMatuK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_MATU_K2;
    }

    public void setDfaMatuK2(AfDecimal dfaMatuK2) {
        writeDecimalAsPacked(Pos.DFA_MATU_K2, dfaMatuK2.copy());
    }

    public void setDfaMatuK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_MATU_K2, Pos.DFA_MATU_K2);
    }

    /**Original name: DFA-MATU-K2<br>*/
    public AfDecimal getDfaMatuK2() {
        return readPackedAsDecimal(Pos.DFA_MATU_K2, Len.Int.DFA_MATU_K2, Len.Fract.DFA_MATU_K2);
    }

    public byte[] getDfaMatuK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_MATU_K2, Pos.DFA_MATU_K2);
        return buffer;
    }

    public void setDfaMatuK2Null(String dfaMatuK2Null) {
        writeString(Pos.DFA_MATU_K2_NULL, dfaMatuK2Null, Len.DFA_MATU_K2_NULL);
    }

    /**Original name: DFA-MATU-K2-NULL<br>*/
    public String getDfaMatuK2Null() {
        return readString(Pos.DFA_MATU_K2_NULL, Len.DFA_MATU_K2_NULL);
    }

    public String getDfaMatuK2NullFormatted() {
        return Functions.padBlanks(getDfaMatuK2Null(), Len.DFA_MATU_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_MATU_K2 = 1;
        public static final int DFA_MATU_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_MATU_K2 = 8;
        public static final int DFA_MATU_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_MATU_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_MATU_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
