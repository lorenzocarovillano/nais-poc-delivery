package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IINT-PREST-EFFLQ<br>
 * Variable: DFL-IINT-PREST-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIintPrestEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIintPrestEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IINT_PREST_EFFLQ;
    }

    public void setDflIintPrestEfflq(AfDecimal dflIintPrestEfflq) {
        writeDecimalAsPacked(Pos.DFL_IINT_PREST_EFFLQ, dflIintPrestEfflq.copy());
    }

    public void setDflIintPrestEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IINT_PREST_EFFLQ, Pos.DFL_IINT_PREST_EFFLQ);
    }

    /**Original name: DFL-IINT-PREST-EFFLQ<br>*/
    public AfDecimal getDflIintPrestEfflq() {
        return readPackedAsDecimal(Pos.DFL_IINT_PREST_EFFLQ, Len.Int.DFL_IINT_PREST_EFFLQ, Len.Fract.DFL_IINT_PREST_EFFLQ);
    }

    public byte[] getDflIintPrestEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IINT_PREST_EFFLQ, Pos.DFL_IINT_PREST_EFFLQ);
        return buffer;
    }

    public void setDflIintPrestEfflqNull(String dflIintPrestEfflqNull) {
        writeString(Pos.DFL_IINT_PREST_EFFLQ_NULL, dflIintPrestEfflqNull, Len.DFL_IINT_PREST_EFFLQ_NULL);
    }

    /**Original name: DFL-IINT-PREST-EFFLQ-NULL<br>*/
    public String getDflIintPrestEfflqNull() {
        return readString(Pos.DFL_IINT_PREST_EFFLQ_NULL, Len.DFL_IINT_PREST_EFFLQ_NULL);
    }

    public String getDflIintPrestEfflqNullFormatted() {
        return Functions.padBlanks(getDflIintPrestEfflqNull(), Len.DFL_IINT_PREST_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IINT_PREST_EFFLQ = 1;
        public static final int DFL_IINT_PREST_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IINT_PREST_EFFLQ = 8;
        public static final int DFL_IINT_PREST_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IINT_PREST_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IINT_PREST_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
