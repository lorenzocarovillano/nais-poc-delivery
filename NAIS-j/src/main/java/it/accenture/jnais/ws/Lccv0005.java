package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.Lccv0005MacroFunzione;
import it.accenture.jnais.ws.enums.Lccv0005Richiesta;

/**Original name: WCOM-LCCS0005<br>
 * Variable: WCOM-LCCS0005 from program LCCS0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Lccv0005 extends SerializableParameter {

    //==== PROPERTIES ====
    /**Original name: LCCV0005-MACRO-FUNZIONE<br>
	 * <pre>--  Macro funzione</pre>*/
    private Lccv0005MacroFunzione macroFunzione = new Lccv0005MacroFunzione();
    /**Original name: LCCV0005-RICHIESTA<br>
	 * <pre>--  Flag gestione scrittura richiesta</pre>*/
    private Lccv0005Richiesta richiesta = new Lccv0005Richiesta();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCV0005;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLccv0005Bytes(buf);
    }

    public String getLccv0005Formatted() {
        return MarshalByteExt.bufferToStr(getLccv0005Bytes());
    }

    public void setLccv0005Bytes(byte[] buffer) {
        setLccv0005Bytes(buffer, 1);
    }

    public byte[] getLccv0005Bytes() {
        byte[] buffer = new byte[Len.LCCV0005];
        return getLccv0005Bytes(buffer, 1);
    }

    public void setLccv0005Bytes(byte[] buffer, int offset) {
        int position = offset;
        macroFunzione.setMacroFunzione(MarshalByte.readString(buffer, position, Lccv0005MacroFunzione.Len.MACRO_FUNZIONE));
        position += Lccv0005MacroFunzione.Len.MACRO_FUNZIONE;
        richiesta.setRichiesta(MarshalByte.readChar(buffer, position));
    }

    public byte[] getLccv0005Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, macroFunzione.getMacroFunzione(), Lccv0005MacroFunzione.Len.MACRO_FUNZIONE);
        position += Lccv0005MacroFunzione.Len.MACRO_FUNZIONE;
        MarshalByte.writeChar(buffer, position, richiesta.getRichiesta());
        return buffer;
    }

    public Lccv0005MacroFunzione getMacroFunzione() {
        return macroFunzione;
    }

    public Lccv0005Richiesta getRichiesta() {
        return richiesta;
    }

    @Override
    public byte[] serialize() {
        return getLccv0005Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCV0005 = Lccv0005MacroFunzione.Len.MACRO_FUNZIONE + Lccv0005Richiesta.Len.RICHIESTA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
