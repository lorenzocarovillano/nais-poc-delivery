package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Ldbvf971;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.occurs.WpmoTabParamMov;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3250<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3250Data {

    //==== PROPERTIES ====
    public static final int DPMO_TAB_PARAM_MOV_MAXOCCURS = 100;
    //Original name: WK-CALL-PGM
    private String wkCallPgm = DefaultValues.stringVal(Len.WK_CALL_PGM);
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*
	 * ---------------------------------------------------------------*
	 *   COPY TIPOLOGICHE
	 * ---------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    //Original name: LDBVF971
    private Ldbvf971 ldbvf971 = new Ldbvf971();
    //Original name: DPMO-ELE-PMO-MAX
    private short dpmoElePmoMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DPMO-TAB-PARAM-MOV
    private WpmoTabParamMov[] dpmoTabParamMov = new WpmoTabParamMov[DPMO_TAB_PARAM_MOV_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: PARAM-MOVI
    private ParamMoviLdbs1470 paramMovi = new ParamMoviLdbs1470();
    //Original name: IX-DCLGEN
    private short ixDclgen = ((short)0);
    //Original name: IX-TAB-PMO
    private short ixTabPmo = ((short)0);

    //==== CONSTRUCTORS ====
    public Lvvs3250Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dpmoTabParamMovIdx = 1; dpmoTabParamMovIdx <= DPMO_TAB_PARAM_MOV_MAXOCCURS; dpmoTabParamMovIdx++) {
            dpmoTabParamMov[dpmoTabParamMovIdx - 1] = new WpmoTabParamMov();
        }
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setDpmoAreaPmoFormatted(String data) {
        byte[] buffer = new byte[Len.DPMO_AREA_PMO];
        MarshalByte.writeString(buffer, 1, data, Len.DPMO_AREA_PMO);
        setDpmoAreaPmoBytes(buffer, 1);
    }

    public void setDpmoAreaPmoBytes(byte[] buffer, int offset) {
        int position = offset;
        dpmoElePmoMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DPMO_TAB_PARAM_MOV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dpmoTabParamMov[idx - 1].setWpmoTabParamMovBytes(buffer, position);
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
            else {
                dpmoTabParamMov[idx - 1].initWpmoTabParamMovSpaces();
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
        }
    }

    public void setDpmoElePmoMax(short dpmoElePmoMax) {
        this.dpmoElePmoMax = dpmoElePmoMax;
    }

    public short getDpmoElePmoMax() {
        return this.dpmoElePmoMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabPmo(short ixTabPmo) {
        this.ixTabPmo = ixTabPmo;
    }

    public short getIxTabPmo() {
        return this.ixTabPmo;
    }

    public WpmoTabParamMov getDpmoTabParamMov(int idx) {
        return dpmoTabParamMov[idx - 1];
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Ldbvf971 getLdbvf971() {
        return ldbvf971;
    }

    public ParamMoviLdbs1470 getParamMovi() {
        return paramMovi;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_CALL_PGM = 8;
        public static final int DPMO_ELE_PMO_MAX = 2;
        public static final int DPMO_AREA_PMO = DPMO_ELE_PMO_MAX + Lvvs3250Data.DPMO_TAB_PARAM_MOV_MAXOCCURS * WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
