package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-PRE-SOLO-RSH<br>
 * Variable: DTR-PRE-SOLO-RSH from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrPreSoloRsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrPreSoloRsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_PRE_SOLO_RSH;
    }

    public void setDtrPreSoloRsh(AfDecimal dtrPreSoloRsh) {
        writeDecimalAsPacked(Pos.DTR_PRE_SOLO_RSH, dtrPreSoloRsh.copy());
    }

    public void setDtrPreSoloRshFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_PRE_SOLO_RSH, Pos.DTR_PRE_SOLO_RSH);
    }

    /**Original name: DTR-PRE-SOLO-RSH<br>*/
    public AfDecimal getDtrPreSoloRsh() {
        return readPackedAsDecimal(Pos.DTR_PRE_SOLO_RSH, Len.Int.DTR_PRE_SOLO_RSH, Len.Fract.DTR_PRE_SOLO_RSH);
    }

    public byte[] getDtrPreSoloRshAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_PRE_SOLO_RSH, Pos.DTR_PRE_SOLO_RSH);
        return buffer;
    }

    public void setDtrPreSoloRshNull(String dtrPreSoloRshNull) {
        writeString(Pos.DTR_PRE_SOLO_RSH_NULL, dtrPreSoloRshNull, Len.DTR_PRE_SOLO_RSH_NULL);
    }

    /**Original name: DTR-PRE-SOLO-RSH-NULL<br>*/
    public String getDtrPreSoloRshNull() {
        return readString(Pos.DTR_PRE_SOLO_RSH_NULL, Len.DTR_PRE_SOLO_RSH_NULL);
    }

    public String getDtrPreSoloRshNullFormatted() {
        return Functions.padBlanks(getDtrPreSoloRshNull(), Len.DTR_PRE_SOLO_RSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_PRE_SOLO_RSH = 1;
        public static final int DTR_PRE_SOLO_RSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_PRE_SOLO_RSH = 8;
        public static final int DTR_PRE_SOLO_RSH_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_PRE_SOLO_RSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_PRE_SOLO_RSH = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
