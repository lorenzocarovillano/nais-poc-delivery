package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPAG-CONT-IMP-ACQ-EXP<br>
 * Variable: WPAG-CONT-IMP-ACQ-EXP from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContImpAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContImpAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_IMP_ACQ_EXP;
    }

    public void setWpagContImpAcqExp(AfDecimal wpagContImpAcqExp) {
        writeDecimalAsPacked(Pos.WPAG_CONT_IMP_ACQ_EXP, wpagContImpAcqExp.copy());
    }

    public void setWpagContImpAcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_IMP_ACQ_EXP, Pos.WPAG_CONT_IMP_ACQ_EXP);
    }

    /**Original name: WPAG-CONT-IMP-ACQ-EXP<br>*/
    public AfDecimal getWpagContImpAcqExp() {
        return readPackedAsDecimal(Pos.WPAG_CONT_IMP_ACQ_EXP, Len.Int.WPAG_CONT_IMP_ACQ_EXP, Len.Fract.WPAG_CONT_IMP_ACQ_EXP);
    }

    public byte[] getWpagContImpAcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_IMP_ACQ_EXP, Pos.WPAG_CONT_IMP_ACQ_EXP);
        return buffer;
    }

    public void initWpagContImpAcqExpSpaces() {
        fill(Pos.WPAG_CONT_IMP_ACQ_EXP, Len.WPAG_CONT_IMP_ACQ_EXP, Types.SPACE_CHAR);
    }

    public void setWpagContImpAcqExpNull(String wpagContImpAcqExpNull) {
        writeString(Pos.WPAG_CONT_IMP_ACQ_EXP_NULL, wpagContImpAcqExpNull, Len.WPAG_CONT_IMP_ACQ_EXP_NULL);
    }

    /**Original name: WPAG-CONT-IMP-ACQ-EXP-NULL<br>*/
    public String getWpagContImpAcqExpNull() {
        return readString(Pos.WPAG_CONT_IMP_ACQ_EXP_NULL, Len.WPAG_CONT_IMP_ACQ_EXP_NULL);
    }

    public String getWpagContImpAcqExpNullFormatted() {
        return Functions.padBlanks(getWpagContImpAcqExpNull(), Len.WPAG_CONT_IMP_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_IMP_ACQ_EXP = 1;
        public static final int WPAG_CONT_IMP_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_IMP_ACQ_EXP = 8;
        public static final int WPAG_CONT_IMP_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_IMP_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_IMP_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
