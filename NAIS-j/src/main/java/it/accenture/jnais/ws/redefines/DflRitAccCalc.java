package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RIT-ACC-CALC<br>
 * Variable: DFL-RIT-ACC-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflRitAccCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflRitAccCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RIT_ACC_CALC;
    }

    public void setDflRitAccCalc(AfDecimal dflRitAccCalc) {
        writeDecimalAsPacked(Pos.DFL_RIT_ACC_CALC, dflRitAccCalc.copy());
    }

    public void setDflRitAccCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RIT_ACC_CALC, Pos.DFL_RIT_ACC_CALC);
    }

    /**Original name: DFL-RIT-ACC-CALC<br>*/
    public AfDecimal getDflRitAccCalc() {
        return readPackedAsDecimal(Pos.DFL_RIT_ACC_CALC, Len.Int.DFL_RIT_ACC_CALC, Len.Fract.DFL_RIT_ACC_CALC);
    }

    public byte[] getDflRitAccCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RIT_ACC_CALC, Pos.DFL_RIT_ACC_CALC);
        return buffer;
    }

    public void setDflRitAccCalcNull(String dflRitAccCalcNull) {
        writeString(Pos.DFL_RIT_ACC_CALC_NULL, dflRitAccCalcNull, Len.DFL_RIT_ACC_CALC_NULL);
    }

    /**Original name: DFL-RIT-ACC-CALC-NULL<br>*/
    public String getDflRitAccCalcNull() {
        return readString(Pos.DFL_RIT_ACC_CALC_NULL, Len.DFL_RIT_ACC_CALC_NULL);
    }

    public String getDflRitAccCalcNullFormatted() {
        return Functions.padBlanks(getDflRitAccCalcNull(), Len.DFL_RIT_ACC_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RIT_ACC_CALC = 1;
        public static final int DFL_RIT_ACC_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RIT_ACC_CALC = 8;
        public static final int DFL_RIT_ACC_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RIT_ACC_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RIT_ACC_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
