package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-ADDIZ-COMUN<br>
 * Variable: LQU-IMPB-ADDIZ-COMUN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbAddizComun extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbAddizComun() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_ADDIZ_COMUN;
    }

    public void setLquImpbAddizComun(AfDecimal lquImpbAddizComun) {
        writeDecimalAsPacked(Pos.LQU_IMPB_ADDIZ_COMUN, lquImpbAddizComun.copy());
    }

    public void setLquImpbAddizComunFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_ADDIZ_COMUN, Pos.LQU_IMPB_ADDIZ_COMUN);
    }

    /**Original name: LQU-IMPB-ADDIZ-COMUN<br>*/
    public AfDecimal getLquImpbAddizComun() {
        return readPackedAsDecimal(Pos.LQU_IMPB_ADDIZ_COMUN, Len.Int.LQU_IMPB_ADDIZ_COMUN, Len.Fract.LQU_IMPB_ADDIZ_COMUN);
    }

    public byte[] getLquImpbAddizComunAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_ADDIZ_COMUN, Pos.LQU_IMPB_ADDIZ_COMUN);
        return buffer;
    }

    public void setLquImpbAddizComunNull(String lquImpbAddizComunNull) {
        writeString(Pos.LQU_IMPB_ADDIZ_COMUN_NULL, lquImpbAddizComunNull, Len.LQU_IMPB_ADDIZ_COMUN_NULL);
    }

    /**Original name: LQU-IMPB-ADDIZ-COMUN-NULL<br>*/
    public String getLquImpbAddizComunNull() {
        return readString(Pos.LQU_IMPB_ADDIZ_COMUN_NULL, Len.LQU_IMPB_ADDIZ_COMUN_NULL);
    }

    public String getLquImpbAddizComunNullFormatted() {
        return Functions.padBlanks(getLquImpbAddizComunNull(), Len.LQU_IMPB_ADDIZ_COMUN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_ADDIZ_COMUN = 1;
        public static final int LQU_IMPB_ADDIZ_COMUN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_ADDIZ_COMUN = 8;
        public static final int LQU_IMPB_ADDIZ_COMUN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_ADDIZ_COMUN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_ADDIZ_COMUN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
