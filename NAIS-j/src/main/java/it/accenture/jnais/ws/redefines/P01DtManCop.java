package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P01-DT-MAN-COP<br>
 * Variable: P01-DT-MAN-COP from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P01DtManCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P01DtManCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P01_DT_MAN_COP;
    }

    public void setP01DtManCop(int p01DtManCop) {
        writeIntAsPacked(Pos.P01_DT_MAN_COP, p01DtManCop, Len.Int.P01_DT_MAN_COP);
    }

    public void setP01DtManCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P01_DT_MAN_COP, Pos.P01_DT_MAN_COP);
    }

    /**Original name: P01-DT-MAN-COP<br>*/
    public int getP01DtManCop() {
        return readPackedAsInt(Pos.P01_DT_MAN_COP, Len.Int.P01_DT_MAN_COP);
    }

    public byte[] getP01DtManCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P01_DT_MAN_COP, Pos.P01_DT_MAN_COP);
        return buffer;
    }

    public void setP01DtManCopNull(String p01DtManCopNull) {
        writeString(Pos.P01_DT_MAN_COP_NULL, p01DtManCopNull, Len.P01_DT_MAN_COP_NULL);
    }

    /**Original name: P01-DT-MAN-COP-NULL<br>*/
    public String getP01DtManCopNull() {
        return readString(Pos.P01_DT_MAN_COP_NULL, Len.P01_DT_MAN_COP_NULL);
    }

    public String getP01DtManCopNullFormatted() {
        return Functions.padBlanks(getP01DtManCopNull(), Len.P01_DT_MAN_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P01_DT_MAN_COP = 1;
        public static final int P01_DT_MAN_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P01_DT_MAN_COP = 5;
        public static final int P01_DT_MAN_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P01_DT_MAN_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
