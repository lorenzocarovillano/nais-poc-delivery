package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-INTR-RETDT<br>
 * Variable: TIT-TOT-INTR-RETDT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotIntrRetdt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotIntrRetdt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_INTR_RETDT;
    }

    public void setTitTotIntrRetdt(AfDecimal titTotIntrRetdt) {
        writeDecimalAsPacked(Pos.TIT_TOT_INTR_RETDT, titTotIntrRetdt.copy());
    }

    public void setTitTotIntrRetdtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_INTR_RETDT, Pos.TIT_TOT_INTR_RETDT);
    }

    /**Original name: TIT-TOT-INTR-RETDT<br>*/
    public AfDecimal getTitTotIntrRetdt() {
        return readPackedAsDecimal(Pos.TIT_TOT_INTR_RETDT, Len.Int.TIT_TOT_INTR_RETDT, Len.Fract.TIT_TOT_INTR_RETDT);
    }

    public byte[] getTitTotIntrRetdtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_INTR_RETDT, Pos.TIT_TOT_INTR_RETDT);
        return buffer;
    }

    public void setTitTotIntrRetdtNull(String titTotIntrRetdtNull) {
        writeString(Pos.TIT_TOT_INTR_RETDT_NULL, titTotIntrRetdtNull, Len.TIT_TOT_INTR_RETDT_NULL);
    }

    /**Original name: TIT-TOT-INTR-RETDT-NULL<br>*/
    public String getTitTotIntrRetdtNull() {
        return readString(Pos.TIT_TOT_INTR_RETDT_NULL, Len.TIT_TOT_INTR_RETDT_NULL);
    }

    public String getTitTotIntrRetdtNullFormatted() {
        return Functions.padBlanks(getTitTotIntrRetdtNull(), Len.TIT_TOT_INTR_RETDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_INTR_RETDT = 1;
        public static final int TIT_TOT_INTR_RETDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_INTR_RETDT = 8;
        public static final int TIT_TOT_INTR_RETDT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_INTR_RETDT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_INTR_RETDT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
