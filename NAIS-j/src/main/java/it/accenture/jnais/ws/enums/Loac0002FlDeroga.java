package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LOAC0002-FL-DEROGA<br>
 * Variable: LOAC0002-FL-DEROGA from copybook LOAC0002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Loac0002FlDeroga {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char NON_TROVATA = 'N';
    public static final char TROVATA = 'S';

    //==== METHODS ====
    public void setLoac0002FlDeroga(char loac0002FlDeroga) {
        this.value = loac0002FlDeroga;
    }

    public char getLoac0002FlDeroga() {
        return this.value;
    }

    public void setNonTrovata() {
        value = NON_TROVATA;
    }

    public boolean isTrovata() {
        return value == TROVATA;
    }

    public void setTrovata() {
        value = TROVATA;
    }
}
