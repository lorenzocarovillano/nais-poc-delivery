package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-DT-VAL-QUOTE<br>
 * Variable: WPAG-DT-VAL-QUOTE from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagDtValQuote extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagDtValQuote() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_DT_VAL_QUOTE;
    }

    public void setWpagDtValQuote(int wpagDtValQuote) {
        writeIntAsPacked(Pos.WPAG_DT_VAL_QUOTE, wpagDtValQuote, Len.Int.WPAG_DT_VAL_QUOTE);
    }

    public void setWpagDtValQuoteFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_DT_VAL_QUOTE, Pos.WPAG_DT_VAL_QUOTE);
    }

    /**Original name: WPAG-DT-VAL-QUOTE<br>*/
    public int getWpagDtValQuote() {
        return readPackedAsInt(Pos.WPAG_DT_VAL_QUOTE, Len.Int.WPAG_DT_VAL_QUOTE);
    }

    public byte[] getWpagDtValQuoteAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_DT_VAL_QUOTE, Pos.WPAG_DT_VAL_QUOTE);
        return buffer;
    }

    public void initWpagDtValQuoteSpaces() {
        fill(Pos.WPAG_DT_VAL_QUOTE, Len.WPAG_DT_VAL_QUOTE, Types.SPACE_CHAR);
    }

    public void setWpagDtValQuoteNull(String wpagDtValQuoteNull) {
        writeString(Pos.WPAG_DT_VAL_QUOTE_NULL, wpagDtValQuoteNull, Len.WPAG_DT_VAL_QUOTE_NULL);
    }

    /**Original name: WPAG-DT-VAL-QUOTE-NULL<br>*/
    public String getWpagDtValQuoteNull() {
        return readString(Pos.WPAG_DT_VAL_QUOTE_NULL, Len.WPAG_DT_VAL_QUOTE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_DT_VAL_QUOTE = 1;
        public static final int WPAG_DT_VAL_QUOTE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_DT_VAL_QUOTE = 5;
        public static final int WPAG_DT_VAL_QUOTE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_DT_VAL_QUOTE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
