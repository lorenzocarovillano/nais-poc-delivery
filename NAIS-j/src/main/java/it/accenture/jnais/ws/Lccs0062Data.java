package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idsv0002;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0062<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0062Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LCCS0062";
    //Original name: WK-ELE-MAX-RATE
    private short wkEleMaxRate = ((short)12);
    //Original name: WK-VARIABILI
    private WkVariabiliLccs0062 wkVariabili = new WkVariabiliLccs0062();
    //Original name: IX-TAB-RATE
    private int ixTabRate = DefaultValues.BIN_INT_VAL;
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    /**Original name: WK-CICLO<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG/SWITCH
	 * ----------------------------------------------------------------*</pre>*/
    private boolean wkCiclo = false;
    //Original name: IO-A2K-LCCC0003
    private IoA2kLccc0003 ioA2kLccc0003 = new IoA2kLccc0003();
    //Original name: IN-RCODE
    private String inRcode = "00";

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public short getWkEleMaxRate() {
        return this.wkEleMaxRate;
    }

    public void setIxTabRate(int ixTabRate) {
        this.ixTabRate = ixTabRate;
    }

    public int getIxTabRate() {
        return this.ixTabRate;
    }

    public void setWkCiclo(boolean wkCiclo) {
        this.wkCiclo = wkCiclo;
    }

    public boolean isWkCiclo() {
        return this.wkCiclo;
    }

    public void setInRcodeFromBuffer(byte[] buffer) {
        inRcode = MarshalByte.readFixedString(buffer, 1, Len.IN_RCODE);
    }

    public String getInRcodeFormatted() {
        return this.inRcode;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public IoA2kLccc0003 getIoA2kLccc0003() {
        return ioA2kLccc0003;
    }

    public WkVariabiliLccs0062 getWkVariabili() {
        return wkVariabili;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IN_RCODE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
