package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-FRQ-COSTI-ATT<br>
 * Variable: WPCO-FRQ-COSTI-ATT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoFrqCostiAtt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoFrqCostiAtt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_FRQ_COSTI_ATT;
    }

    public void setWpcoFrqCostiAtt(int wpcoFrqCostiAtt) {
        writeIntAsPacked(Pos.WPCO_FRQ_COSTI_ATT, wpcoFrqCostiAtt, Len.Int.WPCO_FRQ_COSTI_ATT);
    }

    public void setDpcoFrqCostiAttFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_FRQ_COSTI_ATT, Pos.WPCO_FRQ_COSTI_ATT);
    }

    /**Original name: WPCO-FRQ-COSTI-ATT<br>*/
    public int getWpcoFrqCostiAtt() {
        return readPackedAsInt(Pos.WPCO_FRQ_COSTI_ATT, Len.Int.WPCO_FRQ_COSTI_ATT);
    }

    public byte[] getWpcoFrqCostiAttAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_FRQ_COSTI_ATT, Pos.WPCO_FRQ_COSTI_ATT);
        return buffer;
    }

    public void setWpcoFrqCostiAttNull(String wpcoFrqCostiAttNull) {
        writeString(Pos.WPCO_FRQ_COSTI_ATT_NULL, wpcoFrqCostiAttNull, Len.WPCO_FRQ_COSTI_ATT_NULL);
    }

    /**Original name: WPCO-FRQ-COSTI-ATT-NULL<br>*/
    public String getWpcoFrqCostiAttNull() {
        return readString(Pos.WPCO_FRQ_COSTI_ATT_NULL, Len.WPCO_FRQ_COSTI_ATT_NULL);
    }

    public String getWpcoFrqCostiAttNullFormatted() {
        return Functions.padBlanks(getWpcoFrqCostiAttNull(), Len.WPCO_FRQ_COSTI_ATT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_FRQ_COSTI_ATT = 1;
        public static final int WPCO_FRQ_COSTI_ATT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_FRQ_COSTI_ATT = 3;
        public static final int WPCO_FRQ_COSTI_ATT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_FRQ_COSTI_ATT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
