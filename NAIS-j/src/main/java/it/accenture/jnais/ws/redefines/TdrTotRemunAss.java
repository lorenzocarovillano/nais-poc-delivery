package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-REMUN-ASS<br>
 * Variable: TDR-TOT-REMUN-ASS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_REMUN_ASS;
    }

    public void setTdrTotRemunAss(AfDecimal tdrTotRemunAss) {
        writeDecimalAsPacked(Pos.TDR_TOT_REMUN_ASS, tdrTotRemunAss.copy());
    }

    public void setTdrTotRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_REMUN_ASS, Pos.TDR_TOT_REMUN_ASS);
    }

    /**Original name: TDR-TOT-REMUN-ASS<br>*/
    public AfDecimal getTdrTotRemunAss() {
        return readPackedAsDecimal(Pos.TDR_TOT_REMUN_ASS, Len.Int.TDR_TOT_REMUN_ASS, Len.Fract.TDR_TOT_REMUN_ASS);
    }

    public byte[] getTdrTotRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_REMUN_ASS, Pos.TDR_TOT_REMUN_ASS);
        return buffer;
    }

    public void setTdrTotRemunAssNull(String tdrTotRemunAssNull) {
        writeString(Pos.TDR_TOT_REMUN_ASS_NULL, tdrTotRemunAssNull, Len.TDR_TOT_REMUN_ASS_NULL);
    }

    /**Original name: TDR-TOT-REMUN-ASS-NULL<br>*/
    public String getTdrTotRemunAssNull() {
        return readString(Pos.TDR_TOT_REMUN_ASS_NULL, Len.TDR_TOT_REMUN_ASS_NULL);
    }

    public String getTdrTotRemunAssNullFormatted() {
        return Functions.padBlanks(getTdrTotRemunAssNull(), Len.TDR_TOT_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_REMUN_ASS = 1;
        public static final int TDR_TOT_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_REMUN_ASS = 8;
        public static final int TDR_TOT_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
