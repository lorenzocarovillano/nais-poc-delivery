package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ispc0211DatiInput;
import it.accenture.jnais.ws.occurs.Ispc0211TabErrori;
import it.accenture.jnais.ws.redefines.Ispc0211TabValP;
import it.accenture.jnais.ws.redefines.Ispc0211TabValT;

/**Original name: AREA-IO-ISPS0211<br>
 * Variable: AREA-IO-ISPS0211 from program ISPS0211<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoIsps0211 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int ISPC0211_TAB_ERRORI_MAXOCCURS = 10;
    //Original name: ISPC0211-DATI-INPUT
    private Ispc0211DatiInput ispc0211DatiInput = new Ispc0211DatiInput();
    //Original name: ISPC0211-ESITO
    private String ispc0211Esito = DefaultValues.stringVal(Len.ISPC0211_ESITO);
    //Original name: ISPC0211-ERR-NUM-ELE
    private String ispc0211ErrNumEle = DefaultValues.stringVal(Len.ISPC0211_ERR_NUM_ELE);
    //Original name: ISPC0211-TAB-ERRORI
    private Ispc0211TabErrori[] ispc0211TabErrori = new Ispc0211TabErrori[ISPC0211_TAB_ERRORI_MAXOCCURS];
    /**Original name: ISPC0211-ELE-MAX-SCHEDA-P<br>
	 * <pre>----------------------------------------------------------------*
	 *    SCHEDE PRODOTTO 'P' 'Q' 'O'
	 * ----------------------------------------------------------------*</pre>*/
    private String ispc0211EleMaxSchedaP = DefaultValues.stringVal(Len.ISPC0211_ELE_MAX_SCHEDA_P);
    //Original name: ISPC0211-TAB-VAL-P
    private Ispc0211TabValP ispc0211TabValP = new Ispc0211TabValP();
    /**Original name: ISPC0211-ELE-MAX-SCHEDA-T<br>
	 * <pre>----------------------------------------------------------------*
	 *    SCHEDE TRANCHE 'G' 'H'
	 * ----------------------------------------------------------------*</pre>*/
    private String ispc0211EleMaxSchedaT = DefaultValues.stringVal(Len.ISPC0211_ELE_MAX_SCHEDA_T);
    //Original name: ISPC0211-TAB-VAL-T
    private Ispc0211TabValT ispc0211TabValT = new Ispc0211TabValT();

    //==== CONSTRUCTORS ====
    public AreaIoIsps0211() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IO_ISPS0211;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIoIsps0211Bytes(buf);
    }

    public void init() {
        for (int ispc0211TabErroriIdx = 1; ispc0211TabErroriIdx <= ISPC0211_TAB_ERRORI_MAXOCCURS; ispc0211TabErroriIdx++) {
            ispc0211TabErrori[ispc0211TabErroriIdx - 1] = new Ispc0211TabErrori();
        }
    }

    public String getAreaIoIspc0211Formatted() {
        return MarshalByteExt.bufferToStr(getAreaIoIsps0211Bytes());
    }

    public void setAreaIoIsps0211Bytes(byte[] buffer) {
        setAreaIoIsps0211Bytes(buffer, 1);
    }

    public byte[] getAreaIoIsps0211Bytes() {
        byte[] buffer = new byte[Len.AREA_IO_ISPS0211];
        return getAreaIoIsps0211Bytes(buffer, 1);
    }

    public void setAreaIoIsps0211Bytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0211DatiInput.setIspc0211DatiInputBytes(buffer, position);
        position += Ispc0211DatiInput.Len.ISPC0211_DATI_INPUT;
        setIspc0211AreaErroriBytes(buffer, position);
        position += Len.ISPC0211_AREA_ERRORI;
        setIspc0211DatiIoBytes(buffer, position);
    }

    public byte[] getAreaIoIsps0211Bytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0211DatiInput.getIspc0211DatiInputBytes(buffer, position);
        position += Ispc0211DatiInput.Len.ISPC0211_DATI_INPUT;
        getIspc0211AreaErroriBytes(buffer, position);
        position += Len.ISPC0211_AREA_ERRORI;
        getIspc0211DatiIoBytes(buffer, position);
        return buffer;
    }

    /**Original name: ISPC0211-AREA-ERRORI<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA ERRORI
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getIspc0211AreaErroriBytes() {
        byte[] buffer = new byte[Len.ISPC0211_AREA_ERRORI];
        return getIspc0211AreaErroriBytes(buffer, 1);
    }

    public void setIspc0211AreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0211Esito = MarshalByte.readFixedString(buffer, position, Len.ISPC0211_ESITO);
        position += Len.ISPC0211_ESITO;
        ispc0211ErrNumEle = MarshalByte.readFixedString(buffer, position, Len.ISPC0211_ERR_NUM_ELE);
        position += Len.ISPC0211_ERR_NUM_ELE;
        for (int idx = 1; idx <= ISPC0211_TAB_ERRORI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                ispc0211TabErrori[idx - 1].setIspc0211TabErroriBytes(buffer, position);
                position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
            }
            else {
                ispc0211TabErrori[idx - 1].initIspc0211TabErroriSpaces();
                position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
            }
        }
    }

    public byte[] getIspc0211AreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ispc0211Esito, Len.ISPC0211_ESITO);
        position += Len.ISPC0211_ESITO;
        MarshalByte.writeString(buffer, position, ispc0211ErrNumEle, Len.ISPC0211_ERR_NUM_ELE);
        position += Len.ISPC0211_ERR_NUM_ELE;
        for (int idx = 1; idx <= ISPC0211_TAB_ERRORI_MAXOCCURS; idx++) {
            ispc0211TabErrori[idx - 1].getIspc0211TabErroriBytes(buffer, position);
            position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
        }
        return buffer;
    }

    public void setIspc0211EsitoFormatted(String ispc0211Esito) {
        this.ispc0211Esito = Trunc.toUnsignedNumeric(ispc0211Esito, Len.ISPC0211_ESITO);
    }

    public short getIspc0211Esito() {
        return NumericDisplay.asShort(this.ispc0211Esito);
    }

    public void setIspc0211ErrNumEleFormatted(String ispc0211ErrNumEle) {
        this.ispc0211ErrNumEle = Trunc.toUnsignedNumeric(ispc0211ErrNumEle, Len.ISPC0211_ERR_NUM_ELE);
    }

    public short getIspc0211ErrNumEle() {
        return NumericDisplay.asShort(this.ispc0211ErrNumEle);
    }

    public void setIspc0211DatiIoBytes(byte[] buffer, int offset) {
        int position = offset;
        setIspc0211TabSchedeBytes(buffer, position);
    }

    public byte[] getIspc0211DatiIoBytes(byte[] buffer, int offset) {
        int position = offset;
        getIspc0211TabSchedeBytes(buffer, position);
        return buffer;
    }

    public void setIspc0211TabSchedeBytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0211EleMaxSchedaP = MarshalByte.readFixedString(buffer, position, Len.ISPC0211_ELE_MAX_SCHEDA_P);
        position += Len.ISPC0211_ELE_MAX_SCHEDA_P;
        ispc0211TabValP.setIspc0211TabValPBytes(buffer, position);
        position += Ispc0211TabValP.Len.ISPC0211_TAB_VAL_P;
        ispc0211EleMaxSchedaT = MarshalByte.readFixedString(buffer, position, Len.ISPC0211_ELE_MAX_SCHEDA_T);
        position += Len.ISPC0211_ELE_MAX_SCHEDA_T;
        ispc0211TabValT.setIspc0211TabValTBytes(buffer, position);
    }

    public byte[] getIspc0211TabSchedeBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ispc0211EleMaxSchedaP, Len.ISPC0211_ELE_MAX_SCHEDA_P);
        position += Len.ISPC0211_ELE_MAX_SCHEDA_P;
        ispc0211TabValP.getIspc0211TabValPBytes(buffer, position);
        position += Ispc0211TabValP.Len.ISPC0211_TAB_VAL_P;
        MarshalByte.writeString(buffer, position, ispc0211EleMaxSchedaT, Len.ISPC0211_ELE_MAX_SCHEDA_T);
        position += Len.ISPC0211_ELE_MAX_SCHEDA_T;
        ispc0211TabValT.getIspc0211TabValTBytes(buffer, position);
        return buffer;
    }

    public void setIspc0211EleMaxSchedaP(short ispc0211EleMaxSchedaP) {
        this.ispc0211EleMaxSchedaP = NumericDisplay.asString(ispc0211EleMaxSchedaP, Len.ISPC0211_ELE_MAX_SCHEDA_P);
    }

    public void setIspc0211EleMaxSchedaPFormatted(String ispc0211EleMaxSchedaP) {
        this.ispc0211EleMaxSchedaP = Trunc.toUnsignedNumeric(ispc0211EleMaxSchedaP, Len.ISPC0211_ELE_MAX_SCHEDA_P);
    }

    public short getIspc0211EleMaxSchedaP() {
        return NumericDisplay.asShort(this.ispc0211EleMaxSchedaP);
    }

    public void setIspc0211EleMaxSchedaT(short ispc0211EleMaxSchedaT) {
        this.ispc0211EleMaxSchedaT = NumericDisplay.asString(ispc0211EleMaxSchedaT, Len.ISPC0211_ELE_MAX_SCHEDA_T);
    }

    public void setIspc0211EleMaxSchedaTFormatted(String ispc0211EleMaxSchedaT) {
        this.ispc0211EleMaxSchedaT = Trunc.toUnsignedNumeric(ispc0211EleMaxSchedaT, Len.ISPC0211_ELE_MAX_SCHEDA_T);
    }

    public short getIspc0211EleMaxSchedaT() {
        return NumericDisplay.asShort(this.ispc0211EleMaxSchedaT);
    }

    public Ispc0211DatiInput getIspc0211DatiInput() {
        return ispc0211DatiInput;
    }

    public Ispc0211TabErrori getIspc0211TabErrori(int idx) {
        return ispc0211TabErrori[idx - 1];
    }

    public Ispc0211TabValP getIspc0211TabValP() {
        return ispc0211TabValP;
    }

    public Ispc0211TabValT getIspc0211TabValT() {
        return ispc0211TabValT;
    }

    @Override
    public byte[] serialize() {
        return getAreaIoIsps0211Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ISPC0211_ESITO = 2;
        public static final int ISPC0211_ERR_NUM_ELE = 3;
        public static final int ISPC0211_AREA_ERRORI = ISPC0211_ESITO + ISPC0211_ERR_NUM_ELE + AreaIoIsps0211.ISPC0211_TAB_ERRORI_MAXOCCURS * Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
        public static final int ISPC0211_ELE_MAX_SCHEDA_P = 4;
        public static final int ISPC0211_ELE_MAX_SCHEDA_T = 4;
        public static final int ISPC0211_TAB_SCHEDE = ISPC0211_ELE_MAX_SCHEDA_P + Ispc0211TabValP.Len.ISPC0211_TAB_VAL_P + ISPC0211_ELE_MAX_SCHEDA_T + Ispc0211TabValT.Len.ISPC0211_TAB_VAL_T;
        public static final int ISPC0211_DATI_IO = ISPC0211_TAB_SCHEDE;
        public static final int AREA_IO_ISPS0211 = Ispc0211DatiInput.Len.ISPC0211_DATI_INPUT + ISPC0211_AREA_ERRORI + ISPC0211_DATI_IO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
