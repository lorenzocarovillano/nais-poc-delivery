package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-TAX<br>
 * Variable: DTR-TAX from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_TAX;
    }

    public void setDtrTax(AfDecimal dtrTax) {
        writeDecimalAsPacked(Pos.DTR_TAX, dtrTax.copy());
    }

    public void setDtrTaxFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_TAX, Pos.DTR_TAX);
    }

    /**Original name: DTR-TAX<br>*/
    public AfDecimal getDtrTax() {
        return readPackedAsDecimal(Pos.DTR_TAX, Len.Int.DTR_TAX, Len.Fract.DTR_TAX);
    }

    public byte[] getDtrTaxAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_TAX, Pos.DTR_TAX);
        return buffer;
    }

    public void setDtrTaxNull(String dtrTaxNull) {
        writeString(Pos.DTR_TAX_NULL, dtrTaxNull, Len.DTR_TAX_NULL);
    }

    /**Original name: DTR-TAX-NULL<br>*/
    public String getDtrTaxNull() {
        return readString(Pos.DTR_TAX_NULL, Len.DTR_TAX_NULL);
    }

    public String getDtrTaxNullFormatted() {
        return Functions.padBlanks(getDtrTaxNull(), Len.DTR_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_TAX = 1;
        public static final int DTR_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_TAX = 8;
        public static final int DTR_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
