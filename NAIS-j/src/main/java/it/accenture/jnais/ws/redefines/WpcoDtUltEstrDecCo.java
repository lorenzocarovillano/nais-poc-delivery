package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ESTR-DEC-CO<br>
 * Variable: WPCO-DT-ULT-ESTR-DEC-CO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEstrDecCo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEstrDecCo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ESTR_DEC_CO;
    }

    public void setWpcoDtUltEstrDecCo(int wpcoDtUltEstrDecCo) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ESTR_DEC_CO, wpcoDtUltEstrDecCo, Len.Int.WPCO_DT_ULT_ESTR_DEC_CO);
    }

    public void setDpcoDtUltEstrDecCoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ESTR_DEC_CO, Pos.WPCO_DT_ULT_ESTR_DEC_CO);
    }

    /**Original name: WPCO-DT-ULT-ESTR-DEC-CO<br>*/
    public int getWpcoDtUltEstrDecCo() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ESTR_DEC_CO, Len.Int.WPCO_DT_ULT_ESTR_DEC_CO);
    }

    public byte[] getWpcoDtUltEstrDecCoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ESTR_DEC_CO, Pos.WPCO_DT_ULT_ESTR_DEC_CO);
        return buffer;
    }

    public void setWpcoDtUltEstrDecCoNull(String wpcoDtUltEstrDecCoNull) {
        writeString(Pos.WPCO_DT_ULT_ESTR_DEC_CO_NULL, wpcoDtUltEstrDecCoNull, Len.WPCO_DT_ULT_ESTR_DEC_CO_NULL);
    }

    /**Original name: WPCO-DT-ULT-ESTR-DEC-CO-NULL<br>*/
    public String getWpcoDtUltEstrDecCoNull() {
        return readString(Pos.WPCO_DT_ULT_ESTR_DEC_CO_NULL, Len.WPCO_DT_ULT_ESTR_DEC_CO_NULL);
    }

    public String getWpcoDtUltEstrDecCoNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEstrDecCoNull(), Len.WPCO_DT_ULT_ESTR_DEC_CO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ESTR_DEC_CO = 1;
        public static final int WPCO_DT_ULT_ESTR_DEC_CO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ESTR_DEC_CO = 5;
        public static final int WPCO_DT_ULT_ESTR_DEC_CO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ESTR_DEC_CO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
