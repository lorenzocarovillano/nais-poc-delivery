package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPLI-ID-RAPP-ANA<br>
 * Variable: WPLI-ID-RAPP-ANA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpliIdRappAna extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpliIdRappAna() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPLI_ID_RAPP_ANA;
    }

    public void setWpliIdRappAna(int wpliIdRappAna) {
        writeIntAsPacked(Pos.WPLI_ID_RAPP_ANA, wpliIdRappAna, Len.Int.WPLI_ID_RAPP_ANA);
    }

    public void setWpliIdRappAnaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPLI_ID_RAPP_ANA, Pos.WPLI_ID_RAPP_ANA);
    }

    /**Original name: WPLI-ID-RAPP-ANA<br>*/
    public int getWpliIdRappAna() {
        return readPackedAsInt(Pos.WPLI_ID_RAPP_ANA, Len.Int.WPLI_ID_RAPP_ANA);
    }

    public byte[] getWpliIdRappAnaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPLI_ID_RAPP_ANA, Pos.WPLI_ID_RAPP_ANA);
        return buffer;
    }

    public void initWpliIdRappAnaSpaces() {
        fill(Pos.WPLI_ID_RAPP_ANA, Len.WPLI_ID_RAPP_ANA, Types.SPACE_CHAR);
    }

    public void setWpliIdRappAnaNull(String wpliIdRappAnaNull) {
        writeString(Pos.WPLI_ID_RAPP_ANA_NULL, wpliIdRappAnaNull, Len.WPLI_ID_RAPP_ANA_NULL);
    }

    /**Original name: WPLI-ID-RAPP-ANA-NULL<br>*/
    public String getWpliIdRappAnaNull() {
        return readString(Pos.WPLI_ID_RAPP_ANA_NULL, Len.WPLI_ID_RAPP_ANA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPLI_ID_RAPP_ANA = 1;
        public static final int WPLI_ID_RAPP_ANA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPLI_ID_RAPP_ANA = 5;
        public static final int WPLI_ID_RAPP_ANA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPLI_ID_RAPP_ANA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
