package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-SOPR-TECN<br>
 * Variable: WPAG-RATA-SOPR-TECN from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataSoprTecn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataSoprTecn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_SOPR_TECN;
    }

    public void setWpagRataSoprTecn(AfDecimal wpagRataSoprTecn) {
        writeDecimalAsPacked(Pos.WPAG_RATA_SOPR_TECN, wpagRataSoprTecn.copy());
    }

    public void setWpagRataSoprTecnFormatted(String wpagRataSoprTecn) {
        setWpagRataSoprTecn(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_SOPR_TECN + Len.Fract.WPAG_RATA_SOPR_TECN, Len.Fract.WPAG_RATA_SOPR_TECN, wpagRataSoprTecn));
    }

    public void setWpagRataSoprTecnFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_SOPR_TECN, Pos.WPAG_RATA_SOPR_TECN);
    }

    /**Original name: WPAG-RATA-SOPR-TECN<br>*/
    public AfDecimal getWpagRataSoprTecn() {
        return readPackedAsDecimal(Pos.WPAG_RATA_SOPR_TECN, Len.Int.WPAG_RATA_SOPR_TECN, Len.Fract.WPAG_RATA_SOPR_TECN);
    }

    public byte[] getWpagRataSoprTecnAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_SOPR_TECN, Pos.WPAG_RATA_SOPR_TECN);
        return buffer;
    }

    public void initWpagRataSoprTecnSpaces() {
        fill(Pos.WPAG_RATA_SOPR_TECN, Len.WPAG_RATA_SOPR_TECN, Types.SPACE_CHAR);
    }

    public void setWpagRataSoprTecnNull(String wpagRataSoprTecnNull) {
        writeString(Pos.WPAG_RATA_SOPR_TECN_NULL, wpagRataSoprTecnNull, Len.WPAG_RATA_SOPR_TECN_NULL);
    }

    /**Original name: WPAG-RATA-SOPR-TECN-NULL<br>*/
    public String getWpagRataSoprTecnNull() {
        return readString(Pos.WPAG_RATA_SOPR_TECN_NULL, Len.WPAG_RATA_SOPR_TECN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_SOPR_TECN = 1;
        public static final int WPAG_RATA_SOPR_TECN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_SOPR_TECN = 8;
        public static final int WPAG_RATA_SOPR_TECN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_SOPR_TECN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_SOPR_TECN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
