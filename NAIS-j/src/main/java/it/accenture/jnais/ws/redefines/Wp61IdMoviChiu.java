package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP61-ID-MOVI-CHIU<br>
 * Variable: WP61-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp61IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp61IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP61_ID_MOVI_CHIU;
    }

    public void setWp61IdMoviChiu(int wp61IdMoviChiu) {
        writeIntAsPacked(Pos.WP61_ID_MOVI_CHIU, wp61IdMoviChiu, Len.Int.WP61_ID_MOVI_CHIU);
    }

    public void setWp61IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP61_ID_MOVI_CHIU, Pos.WP61_ID_MOVI_CHIU);
    }

    /**Original name: WP61-ID-MOVI-CHIU<br>*/
    public int getWp61IdMoviChiu() {
        return readPackedAsInt(Pos.WP61_ID_MOVI_CHIU, Len.Int.WP61_ID_MOVI_CHIU);
    }

    public byte[] getWp61IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP61_ID_MOVI_CHIU, Pos.WP61_ID_MOVI_CHIU);
        return buffer;
    }

    public void setWp61IdMoviChiuNull(String wp61IdMoviChiuNull) {
        writeString(Pos.WP61_ID_MOVI_CHIU_NULL, wp61IdMoviChiuNull, Len.WP61_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WP61-ID-MOVI-CHIU-NULL<br>*/
    public String getWp61IdMoviChiuNull() {
        return readString(Pos.WP61_ID_MOVI_CHIU_NULL, Len.WP61_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP61_ID_MOVI_CHIU = 1;
        public static final int WP61_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_ID_MOVI_CHIU = 5;
        public static final int WP61_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
