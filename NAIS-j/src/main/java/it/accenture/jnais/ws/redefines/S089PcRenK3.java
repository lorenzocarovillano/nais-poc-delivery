package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-PC-REN-K3<br>
 * Variable: S089-PC-REN-K3 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089PcRenK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089PcRenK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_PC_REN_K3;
    }

    public void setWlquPcRenK3(AfDecimal wlquPcRenK3) {
        writeDecimalAsPacked(Pos.S089_PC_REN_K3, wlquPcRenK3.copy());
    }

    public void setWlquPcRenK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_PC_REN_K3, Pos.S089_PC_REN_K3);
    }

    /**Original name: WLQU-PC-REN-K3<br>*/
    public AfDecimal getWlquPcRenK3() {
        return readPackedAsDecimal(Pos.S089_PC_REN_K3, Len.Int.WLQU_PC_REN_K3, Len.Fract.WLQU_PC_REN_K3);
    }

    public byte[] getWlquPcRenK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_PC_REN_K3, Pos.S089_PC_REN_K3);
        return buffer;
    }

    public void initWlquPcRenK3Spaces() {
        fill(Pos.S089_PC_REN_K3, Len.S089_PC_REN_K3, Types.SPACE_CHAR);
    }

    public void setWlquPcRenK3Null(String wlquPcRenK3Null) {
        writeString(Pos.S089_PC_REN_K3_NULL, wlquPcRenK3Null, Len.WLQU_PC_REN_K3_NULL);
    }

    /**Original name: WLQU-PC-REN-K3-NULL<br>*/
    public String getWlquPcRenK3Null() {
        return readString(Pos.S089_PC_REN_K3_NULL, Len.WLQU_PC_REN_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_PC_REN_K3 = 1;
        public static final int S089_PC_REN_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_PC_REN_K3 = 4;
        public static final int WLQU_PC_REN_K3_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_PC_REN_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_PC_REN_K3 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
