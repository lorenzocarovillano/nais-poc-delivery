package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-RSH-DFLT<br>
 * Variable: WRST-RIS-RSH-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisRshDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisRshDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_RSH_DFLT;
    }

    public void setWrstRisRshDflt(AfDecimal wrstRisRshDflt) {
        writeDecimalAsPacked(Pos.WRST_RIS_RSH_DFLT, wrstRisRshDflt.copy());
    }

    public void setWrstRisRshDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_RSH_DFLT, Pos.WRST_RIS_RSH_DFLT);
    }

    /**Original name: WRST-RIS-RSH-DFLT<br>*/
    public AfDecimal getWrstRisRshDflt() {
        return readPackedAsDecimal(Pos.WRST_RIS_RSH_DFLT, Len.Int.WRST_RIS_RSH_DFLT, Len.Fract.WRST_RIS_RSH_DFLT);
    }

    public byte[] getWrstRisRshDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_RSH_DFLT, Pos.WRST_RIS_RSH_DFLT);
        return buffer;
    }

    public void initWrstRisRshDfltSpaces() {
        fill(Pos.WRST_RIS_RSH_DFLT, Len.WRST_RIS_RSH_DFLT, Types.SPACE_CHAR);
    }

    public void setWrstRisRshDfltNull(String wrstRisRshDfltNull) {
        writeString(Pos.WRST_RIS_RSH_DFLT_NULL, wrstRisRshDfltNull, Len.WRST_RIS_RSH_DFLT_NULL);
    }

    /**Original name: WRST-RIS-RSH-DFLT-NULL<br>*/
    public String getWrstRisRshDfltNull() {
        return readString(Pos.WRST_RIS_RSH_DFLT_NULL, Len.WRST_RIS_RSH_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_RSH_DFLT = 1;
        public static final int WRST_RIS_RSH_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_RSH_DFLT = 8;
        public static final int WRST_RIS_RSH_DFLT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_RSH_DFLT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_RSH_DFLT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
