package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-TRCH<br>
 * Variable: WS-TP-TRCH from copybook LCCVXTH0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpTrch {

    //==== PROPERTIES ====
    private String value = "";
    public static final String ORDINARIO = "1";
    public static final String AGGIUNTIV = "2";
    public static final String BON_RICOR = "3";
    public static final String BON_FEDEL = "4";
    public static final String DA_TRASFO = "5";
    public static final String CED_REINV = "6";
    public static final String DIFFERIME = "7";
    public static final String CONV_REND = "8";
    public static final String NEG_PRCOS = "9";
    public static final String NEG_RIS_PAR = "10";
    public static final String NEG_RIS_PRO = "11";
    public static final String RIEP_RIS_PRO = "12";
    public static final String NEG_IMPST_SOST = "13";
    public static final String NEG_DA_DIS = "14";
    public static final String NEG_INV_DA_SWIT = "15";
    public static final String INV_DA_SWIT = "16";
    public static final String INV_BONUS_DA_REBATE = "17";
    public static final String BONUS_SCADENZA = "18";
    public static final String BIG_CHANCE_POS = "19";
    public static final String BIG_CHANCE_NEG = "20";
    public static final String CONSOLIDA_POS = "21";
    public static final String CONSOLIDA_NEG = "22";
    public static final String COSTI_SWITCH = "23";
    public static final String COSTI_VAR_RIP_PREMI = "24";
    public static final String COSTI_INSOLUTO = "25";

    //==== METHODS ====
    public void setWsTpTrch(String wsTpTrch) {
        this.value = Functions.subString(wsTpTrch, Len.WS_TP_TRCH);
    }

    public String getWsTpTrch() {
        return this.value;
    }

    public boolean isNegPrcos() {
        return value.equals(NEG_PRCOS);
    }

    public boolean isNegRisPar() {
        return value.equals(NEG_RIS_PAR);
    }

    public boolean isNegRisPro() {
        return value.equals(NEG_RIS_PRO);
    }

    public boolean isNegImpstSost() {
        return value.equals(NEG_IMPST_SOST);
    }

    public boolean isNegDaDis() {
        return value.equals(NEG_DA_DIS);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_TRCH = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
