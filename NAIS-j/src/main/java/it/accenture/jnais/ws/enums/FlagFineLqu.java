package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-FINE-LQU<br>
 * Variable: FLAG-FINE-LQU from program LVVS2790<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagFineLqu {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFineLqu(char fineLqu) {
        this.value = fineLqu;
    }

    public char getFineLqu() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
