package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-OVER-COMM<br>
 * Variable: WB03-OVER-COMM from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03OverComm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03OverComm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_OVER_COMM;
    }

    public void setWb03OverCommFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_OVER_COMM, Pos.WB03_OVER_COMM);
    }

    /**Original name: WB03-OVER-COMM<br>*/
    public AfDecimal getWb03OverComm() {
        return readPackedAsDecimal(Pos.WB03_OVER_COMM, Len.Int.WB03_OVER_COMM, Len.Fract.WB03_OVER_COMM);
    }

    public byte[] getWb03OverCommAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_OVER_COMM, Pos.WB03_OVER_COMM);
        return buffer;
    }

    public void setWb03OverCommNull(String wb03OverCommNull) {
        writeString(Pos.WB03_OVER_COMM_NULL, wb03OverCommNull, Len.WB03_OVER_COMM_NULL);
    }

    /**Original name: WB03-OVER-COMM-NULL<br>*/
    public String getWb03OverCommNull() {
        return readString(Pos.WB03_OVER_COMM_NULL, Len.WB03_OVER_COMM_NULL);
    }

    public String getWb03OverCommNullFormatted() {
        return Functions.padBlanks(getWb03OverCommNull(), Len.WB03_OVER_COMM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_OVER_COMM = 1;
        public static final int WB03_OVER_COMM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_OVER_COMM = 8;
        public static final int WB03_OVER_COMM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_OVER_COMM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_OVER_COMM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
