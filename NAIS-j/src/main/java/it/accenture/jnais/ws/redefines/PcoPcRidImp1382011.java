package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-PC-RID-IMP-1382011<br>
 * Variable: PCO-PC-RID-IMP-1382011 from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoPcRidImp1382011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoPcRidImp1382011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_PC_RID_IMP1382011;
    }

    public void setPcoPcRidImp1382011(AfDecimal pcoPcRidImp1382011) {
        writeDecimalAsPacked(Pos.PCO_PC_RID_IMP1382011, pcoPcRidImp1382011.copy());
    }

    public void setPcoPcRidImp1382011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_PC_RID_IMP1382011, Pos.PCO_PC_RID_IMP1382011);
    }

    /**Original name: PCO-PC-RID-IMP-1382011<br>*/
    public AfDecimal getPcoPcRidImp1382011() {
        return readPackedAsDecimal(Pos.PCO_PC_RID_IMP1382011, Len.Int.PCO_PC_RID_IMP1382011, Len.Fract.PCO_PC_RID_IMP1382011);
    }

    public byte[] getPcoPcRidImp1382011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_PC_RID_IMP1382011, Pos.PCO_PC_RID_IMP1382011);
        return buffer;
    }

    public void initPcoPcRidImp1382011HighValues() {
        fill(Pos.PCO_PC_RID_IMP1382011, Len.PCO_PC_RID_IMP1382011, Types.HIGH_CHAR_VAL);
    }

    public void setPcoPcRidImp1382011Null(String pcoPcRidImp1382011Null) {
        writeString(Pos.PCO_PC_RID_IMP1382011_NULL, pcoPcRidImp1382011Null, Len.PCO_PC_RID_IMP1382011_NULL);
    }

    /**Original name: PCO-PC-RID-IMP-1382011-NULL<br>*/
    public String getPcoPcRidImp1382011Null() {
        return readString(Pos.PCO_PC_RID_IMP1382011_NULL, Len.PCO_PC_RID_IMP1382011_NULL);
    }

    public String getPcoPcRidImp1382011NullFormatted() {
        return Functions.padBlanks(getPcoPcRidImp1382011Null(), Len.PCO_PC_RID_IMP1382011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_PC_RID_IMP1382011 = 1;
        public static final int PCO_PC_RID_IMP1382011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_PC_RID_IMP1382011 = 4;
        public static final int PCO_PC_RID_IMP1382011_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_PC_RID_IMP1382011 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_PC_RID_IMP1382011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
