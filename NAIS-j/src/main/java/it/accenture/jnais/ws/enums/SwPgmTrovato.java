package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-PGM-TROVATO<br>
 * Variable: SW-PGM-TROVATO from program IDSS0010<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwPgmTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO = 'S';
    public static final char NO_TROVATO = 'N';

    //==== METHODS ====
    public void setSwPgmTrovato(char swPgmTrovato) {
        this.value = swPgmTrovato;
    }

    public char getSwPgmTrovato() {
        return this.value;
    }

    public boolean isTrovato() {
        return value == TROVATO;
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public void setNoTrovato() {
        value = NO_TROVATO;
    }
}
