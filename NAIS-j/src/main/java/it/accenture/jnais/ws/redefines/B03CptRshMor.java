package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CPT-RSH-MOR<br>
 * Variable: B03-CPT-RSH-MOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CptRshMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CptRshMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CPT_RSH_MOR;
    }

    public void setB03CptRshMor(AfDecimal b03CptRshMor) {
        writeDecimalAsPacked(Pos.B03_CPT_RSH_MOR, b03CptRshMor.copy());
    }

    public void setB03CptRshMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CPT_RSH_MOR, Pos.B03_CPT_RSH_MOR);
    }

    /**Original name: B03-CPT-RSH-MOR<br>*/
    public AfDecimal getB03CptRshMor() {
        return readPackedAsDecimal(Pos.B03_CPT_RSH_MOR, Len.Int.B03_CPT_RSH_MOR, Len.Fract.B03_CPT_RSH_MOR);
    }

    public byte[] getB03CptRshMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CPT_RSH_MOR, Pos.B03_CPT_RSH_MOR);
        return buffer;
    }

    public void setB03CptRshMorNull(String b03CptRshMorNull) {
        writeString(Pos.B03_CPT_RSH_MOR_NULL, b03CptRshMorNull, Len.B03_CPT_RSH_MOR_NULL);
    }

    /**Original name: B03-CPT-RSH-MOR-NULL<br>*/
    public String getB03CptRshMorNull() {
        return readString(Pos.B03_CPT_RSH_MOR_NULL, Len.B03_CPT_RSH_MOR_NULL);
    }

    public String getB03CptRshMorNullFormatted() {
        return Functions.padBlanks(getB03CptRshMorNull(), Len.B03_CPT_RSH_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CPT_RSH_MOR = 1;
        public static final int B03_CPT_RSH_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CPT_RSH_MOR = 8;
        public static final int B03_CPT_RSH_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CPT_RSH_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CPT_RSH_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
