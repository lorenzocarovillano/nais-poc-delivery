package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPB-VIS-662014<br>
 * Variable: S089-IMPB-VIS-662014 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpbVis662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpbVis662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPB_VIS662014;
    }

    public void setWlquImpbVis662014(AfDecimal wlquImpbVis662014) {
        writeDecimalAsPacked(Pos.S089_IMPB_VIS662014, wlquImpbVis662014.copy());
    }

    public void setWlquImpbVis662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPB_VIS662014, Pos.S089_IMPB_VIS662014);
    }

    /**Original name: WLQU-IMPB-VIS-662014<br>*/
    public AfDecimal getWlquImpbVis662014() {
        return readPackedAsDecimal(Pos.S089_IMPB_VIS662014, Len.Int.WLQU_IMPB_VIS662014, Len.Fract.WLQU_IMPB_VIS662014);
    }

    public byte[] getWlquImpbVis662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPB_VIS662014, Pos.S089_IMPB_VIS662014);
        return buffer;
    }

    public void initWlquImpbVis662014Spaces() {
        fill(Pos.S089_IMPB_VIS662014, Len.S089_IMPB_VIS662014, Types.SPACE_CHAR);
    }

    public void setWlquImpbVis662014Null(String wlquImpbVis662014Null) {
        writeString(Pos.S089_IMPB_VIS662014_NULL, wlquImpbVis662014Null, Len.WLQU_IMPB_VIS662014_NULL);
    }

    /**Original name: WLQU-IMPB-VIS-662014-NULL<br>*/
    public String getWlquImpbVis662014Null() {
        return readString(Pos.S089_IMPB_VIS662014_NULL, Len.WLQU_IMPB_VIS662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPB_VIS662014 = 1;
        public static final int S089_IMPB_VIS662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPB_VIS662014 = 8;
        public static final int WLQU_IMPB_VIS662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_VIS662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_VIS662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
