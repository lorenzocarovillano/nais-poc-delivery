package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: D09-MQ-TEMPO-ATTESA-2<br>
 * Variable: D09-MQ-TEMPO-ATTESA-2 from program LDBS6730<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class D09MqTempoAttesa2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public D09MqTempoAttesa2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.D09_MQ_TEMPO_ATTESA2;
    }

    public void setD09MqTempoAttesa2(long d09MqTempoAttesa2) {
        writeLongAsPacked(Pos.D09_MQ_TEMPO_ATTESA2, d09MqTempoAttesa2, Len.Int.D09_MQ_TEMPO_ATTESA2);
    }

    public void setD09MqTempoAttesa2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.D09_MQ_TEMPO_ATTESA2, Pos.D09_MQ_TEMPO_ATTESA2);
    }

    /**Original name: D09-MQ-TEMPO-ATTESA-2<br>*/
    public long getD09MqTempoAttesa2() {
        return readPackedAsLong(Pos.D09_MQ_TEMPO_ATTESA2, Len.Int.D09_MQ_TEMPO_ATTESA2);
    }

    public byte[] getD09MqTempoAttesa2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.D09_MQ_TEMPO_ATTESA2, Pos.D09_MQ_TEMPO_ATTESA2);
        return buffer;
    }

    public void setD09MqTempoAttesa2Null(String d09MqTempoAttesa2Null) {
        writeString(Pos.D09_MQ_TEMPO_ATTESA2_NULL, d09MqTempoAttesa2Null, Len.D09_MQ_TEMPO_ATTESA2_NULL);
    }

    /**Original name: D09-MQ-TEMPO-ATTESA-2-NULL<br>*/
    public String getD09MqTempoAttesa2Null() {
        return readString(Pos.D09_MQ_TEMPO_ATTESA2_NULL, Len.D09_MQ_TEMPO_ATTESA2_NULL);
    }

    public String getD09MqTempoAttesa2NullFormatted() {
        return Functions.padBlanks(getD09MqTempoAttesa2Null(), Len.D09_MQ_TEMPO_ATTESA2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int D09_MQ_TEMPO_ATTESA2 = 1;
        public static final int D09_MQ_TEMPO_ATTESA2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int D09_MQ_TEMPO_ATTESA2 = 6;
        public static final int D09_MQ_TEMPO_ATTESA2_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int D09_MQ_TEMPO_ATTESA2 = 10;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
