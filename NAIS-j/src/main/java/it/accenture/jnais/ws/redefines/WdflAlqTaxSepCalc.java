package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ALQ-TAX-SEP-CALC<br>
 * Variable: WDFL-ALQ-TAX-SEP-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAlqTaxSepCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAlqTaxSepCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ALQ_TAX_SEP_CALC;
    }

    public void setWdflAlqTaxSepCalc(AfDecimal wdflAlqTaxSepCalc) {
        writeDecimalAsPacked(Pos.WDFL_ALQ_TAX_SEP_CALC, wdflAlqTaxSepCalc.copy());
    }

    public void setWdflAlqTaxSepCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ALQ_TAX_SEP_CALC, Pos.WDFL_ALQ_TAX_SEP_CALC);
    }

    /**Original name: WDFL-ALQ-TAX-SEP-CALC<br>*/
    public AfDecimal getWdflAlqTaxSepCalc() {
        return readPackedAsDecimal(Pos.WDFL_ALQ_TAX_SEP_CALC, Len.Int.WDFL_ALQ_TAX_SEP_CALC, Len.Fract.WDFL_ALQ_TAX_SEP_CALC);
    }

    public byte[] getWdflAlqTaxSepCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ALQ_TAX_SEP_CALC, Pos.WDFL_ALQ_TAX_SEP_CALC);
        return buffer;
    }

    public void setWdflAlqTaxSepCalcNull(String wdflAlqTaxSepCalcNull) {
        writeString(Pos.WDFL_ALQ_TAX_SEP_CALC_NULL, wdflAlqTaxSepCalcNull, Len.WDFL_ALQ_TAX_SEP_CALC_NULL);
    }

    /**Original name: WDFL-ALQ-TAX-SEP-CALC-NULL<br>*/
    public String getWdflAlqTaxSepCalcNull() {
        return readString(Pos.WDFL_ALQ_TAX_SEP_CALC_NULL, Len.WDFL_ALQ_TAX_SEP_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_TAX_SEP_CALC = 1;
        public static final int WDFL_ALQ_TAX_SEP_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_TAX_SEP_CALC = 4;
        public static final int WDFL_ALQ_TAX_SEP_CALC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_TAX_SEP_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_TAX_SEP_CALC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
