package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-ERRORE<br>
 * Variable: WK-ERRORE from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkErrore {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_ERRORE);
    public static final String YES = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setWkErrore(String wkErrore) {
        this.value = Functions.subString(wkErrore, Len.WK_ERRORE);
    }

    public String getWkErrore() {
        return this.value;
    }

    public boolean isYes() {
        return value.equals(YES);
    }

    public void setYes() {
        value = YES;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_ERRORE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
