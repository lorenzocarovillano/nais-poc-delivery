package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: S0024-TAB-MOV-ANNULL<br>
 * Variables: S0024-TAB-MOV-ANNULL from copybook LCCC0001<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class S0024TabMovAnnull {

    //==== PROPERTIES ====
    //Original name: S0024-ID-MOVI-ANN
    private int s0024IdMoviAnn = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setTabMovAnnullBytes(byte[] buffer, int offset) {
        int position = offset;
        s0024IdMoviAnn = MarshalByte.readPackedAsInt(buffer, position, Len.Int.S0024_ID_MOVI_ANN, 0);
    }

    public byte[] getTabMovAnnullBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, s0024IdMoviAnn, Len.Int.S0024_ID_MOVI_ANN, 0);
        return buffer;
    }

    public void initTabMovAnnullSpaces() {
        s0024IdMoviAnn = Types.INVALID_INT_VAL;
    }

    public void setS0024IdMoviAnn(int s0024IdMoviAnn) {
        this.s0024IdMoviAnn = s0024IdMoviAnn;
    }

    public int getS0024IdMoviAnn() {
        return this.s0024IdMoviAnn;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int S0024_ID_MOVI_ANN = 5;
        public static final int TAB_MOV_ANNULL = S0024_ID_MOVI_ANN;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int S0024_ID_MOVI_ANN = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
