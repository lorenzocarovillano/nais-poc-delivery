package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-ANZ-CNBTVA-CARAZI<br>
 * Variable: DFA-ANZ-CNBTVA-CARAZI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaAnzCnbtvaCarazi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaAnzCnbtvaCarazi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_ANZ_CNBTVA_CARAZI;
    }

    public void setDfaAnzCnbtvaCarazi(short dfaAnzCnbtvaCarazi) {
        writeShortAsPacked(Pos.DFA_ANZ_CNBTVA_CARAZI, dfaAnzCnbtvaCarazi, Len.Int.DFA_ANZ_CNBTVA_CARAZI);
    }

    public void setDfaAnzCnbtvaCaraziFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_ANZ_CNBTVA_CARAZI, Pos.DFA_ANZ_CNBTVA_CARAZI);
    }

    /**Original name: DFA-ANZ-CNBTVA-CARAZI<br>*/
    public short getDfaAnzCnbtvaCarazi() {
        return readPackedAsShort(Pos.DFA_ANZ_CNBTVA_CARAZI, Len.Int.DFA_ANZ_CNBTVA_CARAZI);
    }

    public byte[] getDfaAnzCnbtvaCaraziAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_ANZ_CNBTVA_CARAZI, Pos.DFA_ANZ_CNBTVA_CARAZI);
        return buffer;
    }

    public void setDfaAnzCnbtvaCaraziNull(String dfaAnzCnbtvaCaraziNull) {
        writeString(Pos.DFA_ANZ_CNBTVA_CARAZI_NULL, dfaAnzCnbtvaCaraziNull, Len.DFA_ANZ_CNBTVA_CARAZI_NULL);
    }

    /**Original name: DFA-ANZ-CNBTVA-CARAZI-NULL<br>*/
    public String getDfaAnzCnbtvaCaraziNull() {
        return readString(Pos.DFA_ANZ_CNBTVA_CARAZI_NULL, Len.DFA_ANZ_CNBTVA_CARAZI_NULL);
    }

    public String getDfaAnzCnbtvaCaraziNullFormatted() {
        return Functions.padBlanks(getDfaAnzCnbtvaCaraziNull(), Len.DFA_ANZ_CNBTVA_CARAZI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_ANZ_CNBTVA_CARAZI = 1;
        public static final int DFA_ANZ_CNBTVA_CARAZI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_ANZ_CNBTVA_CARAZI = 3;
        public static final int DFA_ANZ_CNBTVA_CARAZI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_ANZ_CNBTVA_CARAZI = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
