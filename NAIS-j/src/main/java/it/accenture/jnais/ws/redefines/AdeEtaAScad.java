package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-ETA-A-SCAD<br>
 * Variable: ADE-ETA-A-SCAD from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeEtaAScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeEtaAScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_ETA_A_SCAD;
    }

    public void setAdeEtaAScad(int adeEtaAScad) {
        writeIntAsPacked(Pos.ADE_ETA_A_SCAD, adeEtaAScad, Len.Int.ADE_ETA_A_SCAD);
    }

    public void setAdeEtaAScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_ETA_A_SCAD, Pos.ADE_ETA_A_SCAD);
    }

    /**Original name: ADE-ETA-A-SCAD<br>*/
    public int getAdeEtaAScad() {
        return readPackedAsInt(Pos.ADE_ETA_A_SCAD, Len.Int.ADE_ETA_A_SCAD);
    }

    public byte[] getAdeEtaAScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_ETA_A_SCAD, Pos.ADE_ETA_A_SCAD);
        return buffer;
    }

    public void setAdeEtaAScadNull(String adeEtaAScadNull) {
        writeString(Pos.ADE_ETA_A_SCAD_NULL, adeEtaAScadNull, Len.ADE_ETA_A_SCAD_NULL);
    }

    /**Original name: ADE-ETA-A-SCAD-NULL<br>*/
    public String getAdeEtaAScadNull() {
        return readString(Pos.ADE_ETA_A_SCAD_NULL, Len.ADE_ETA_A_SCAD_NULL);
    }

    public String getAdeEtaAScadNullFormatted() {
        return Functions.padBlanks(getAdeEtaAScadNull(), Len.ADE_ETA_A_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_ETA_A_SCAD = 1;
        public static final int ADE_ETA_A_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_ETA_A_SCAD = 3;
        public static final int ADE_ETA_A_SCAD_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_ETA_A_SCAD = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
