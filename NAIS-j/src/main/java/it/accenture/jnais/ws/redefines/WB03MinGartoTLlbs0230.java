package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-MIN-GARTO-T<br>
 * Variable: W-B03-MIN-GARTO-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03MinGartoTLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03MinGartoTLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_MIN_GARTO_T;
    }

    public void setwB03MinGartoT(AfDecimal wB03MinGartoT) {
        writeDecimalAsPacked(Pos.W_B03_MIN_GARTO_T, wB03MinGartoT.copy());
    }

    /**Original name: W-B03-MIN-GARTO-T<br>*/
    public AfDecimal getwB03MinGartoT() {
        return readPackedAsDecimal(Pos.W_B03_MIN_GARTO_T, Len.Int.W_B03_MIN_GARTO_T, Len.Fract.W_B03_MIN_GARTO_T);
    }

    public byte[] getwB03MinGartoTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_MIN_GARTO_T, Pos.W_B03_MIN_GARTO_T);
        return buffer;
    }

    public void setwB03MinGartoTNull(String wB03MinGartoTNull) {
        writeString(Pos.W_B03_MIN_GARTO_T_NULL, wB03MinGartoTNull, Len.W_B03_MIN_GARTO_T_NULL);
    }

    /**Original name: W-B03-MIN-GARTO-T-NULL<br>*/
    public String getwB03MinGartoTNull() {
        return readString(Pos.W_B03_MIN_GARTO_T_NULL, Len.W_B03_MIN_GARTO_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_MIN_GARTO_T = 1;
        public static final int W_B03_MIN_GARTO_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_MIN_GARTO_T = 8;
        public static final int W_B03_MIN_GARTO_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_MIN_GARTO_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_MIN_GARTO_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
