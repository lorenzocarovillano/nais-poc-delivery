package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-DUR-GG<br>
 * Variable: PMO-DUR-GG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoDurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoDurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_DUR_GG;
    }

    public void setPmoDurGg(int pmoDurGg) {
        writeIntAsPacked(Pos.PMO_DUR_GG, pmoDurGg, Len.Int.PMO_DUR_GG);
    }

    public void setPmoDurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_DUR_GG, Pos.PMO_DUR_GG);
    }

    /**Original name: PMO-DUR-GG<br>*/
    public int getPmoDurGg() {
        return readPackedAsInt(Pos.PMO_DUR_GG, Len.Int.PMO_DUR_GG);
    }

    public byte[] getPmoDurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_DUR_GG, Pos.PMO_DUR_GG);
        return buffer;
    }

    public void initPmoDurGgHighValues() {
        fill(Pos.PMO_DUR_GG, Len.PMO_DUR_GG, Types.HIGH_CHAR_VAL);
    }

    public void setPmoDurGgNull(String pmoDurGgNull) {
        writeString(Pos.PMO_DUR_GG_NULL, pmoDurGgNull, Len.PMO_DUR_GG_NULL);
    }

    /**Original name: PMO-DUR-GG-NULL<br>*/
    public String getPmoDurGgNull() {
        return readString(Pos.PMO_DUR_GG_NULL, Len.PMO_DUR_GG_NULL);
    }

    public String getPmoDurGgNullFormatted() {
        return Functions.padBlanks(getPmoDurGgNull(), Len.PMO_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_DUR_GG = 1;
        public static final int PMO_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_DUR_GG = 3;
        public static final int PMO_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
