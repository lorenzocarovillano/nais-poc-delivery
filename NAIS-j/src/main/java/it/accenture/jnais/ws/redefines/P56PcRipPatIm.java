package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-PC-RIP-PAT-IM<br>
 * Variable: P56-PC-RIP-PAT-IM from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56PcRipPatIm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56PcRipPatIm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_PC_RIP_PAT_IM;
    }

    public void setP56PcRipPatIm(AfDecimal p56PcRipPatIm) {
        writeDecimalAsPacked(Pos.P56_PC_RIP_PAT_IM, p56PcRipPatIm.copy());
    }

    public void setP56PcRipPatImFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_PC_RIP_PAT_IM, Pos.P56_PC_RIP_PAT_IM);
    }

    /**Original name: P56-PC-RIP-PAT-IM<br>*/
    public AfDecimal getP56PcRipPatIm() {
        return readPackedAsDecimal(Pos.P56_PC_RIP_PAT_IM, Len.Int.P56_PC_RIP_PAT_IM, Len.Fract.P56_PC_RIP_PAT_IM);
    }

    public byte[] getP56PcRipPatImAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_PC_RIP_PAT_IM, Pos.P56_PC_RIP_PAT_IM);
        return buffer;
    }

    public void setP56PcRipPatImNull(String p56PcRipPatImNull) {
        writeString(Pos.P56_PC_RIP_PAT_IM_NULL, p56PcRipPatImNull, Len.P56_PC_RIP_PAT_IM_NULL);
    }

    /**Original name: P56-PC-RIP-PAT-IM-NULL<br>*/
    public String getP56PcRipPatImNull() {
        return readString(Pos.P56_PC_RIP_PAT_IM_NULL, Len.P56_PC_RIP_PAT_IM_NULL);
    }

    public String getP56PcRipPatImNullFormatted() {
        return Functions.padBlanks(getP56PcRipPatImNull(), Len.P56_PC_RIP_PAT_IM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_PC_RIP_PAT_IM = 1;
        public static final int P56_PC_RIP_PAT_IM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_PC_RIP_PAT_IM = 4;
        public static final int P56_PC_RIP_PAT_IM_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_PC_RIP_PAT_IM = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_PC_RIP_PAT_IM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
