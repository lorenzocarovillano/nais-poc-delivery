package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-RIT-ACC-DFZ<br>
 * Variable: WDFL-IMPB-RIT-ACC-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbRitAccDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbRitAccDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_RIT_ACC_DFZ;
    }

    public void setWdflImpbRitAccDfz(AfDecimal wdflImpbRitAccDfz) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_RIT_ACC_DFZ, wdflImpbRitAccDfz.copy());
    }

    public void setWdflImpbRitAccDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_RIT_ACC_DFZ, Pos.WDFL_IMPB_RIT_ACC_DFZ);
    }

    /**Original name: WDFL-IMPB-RIT-ACC-DFZ<br>*/
    public AfDecimal getWdflImpbRitAccDfz() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_RIT_ACC_DFZ, Len.Int.WDFL_IMPB_RIT_ACC_DFZ, Len.Fract.WDFL_IMPB_RIT_ACC_DFZ);
    }

    public byte[] getWdflImpbRitAccDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_RIT_ACC_DFZ, Pos.WDFL_IMPB_RIT_ACC_DFZ);
        return buffer;
    }

    public void setWdflImpbRitAccDfzNull(String wdflImpbRitAccDfzNull) {
        writeString(Pos.WDFL_IMPB_RIT_ACC_DFZ_NULL, wdflImpbRitAccDfzNull, Len.WDFL_IMPB_RIT_ACC_DFZ_NULL);
    }

    /**Original name: WDFL-IMPB-RIT-ACC-DFZ-NULL<br>*/
    public String getWdflImpbRitAccDfzNull() {
        return readString(Pos.WDFL_IMPB_RIT_ACC_DFZ_NULL, Len.WDFL_IMPB_RIT_ACC_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_RIT_ACC_DFZ = 1;
        public static final int WDFL_IMPB_RIT_ACC_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_RIT_ACC_DFZ = 8;
        public static final int WDFL_IMPB_RIT_ACC_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_RIT_ACC_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_RIT_ACC_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
