package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-QTZ-EMIS<br>
 * Variable: WB03-DT-QTZ-EMIS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtQtzEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtQtzEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_QTZ_EMIS;
    }

    public void setWb03DtQtzEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_QTZ_EMIS, Pos.WB03_DT_QTZ_EMIS);
    }

    /**Original name: WB03-DT-QTZ-EMIS<br>*/
    public int getWb03DtQtzEmis() {
        return readPackedAsInt(Pos.WB03_DT_QTZ_EMIS, Len.Int.WB03_DT_QTZ_EMIS);
    }

    public byte[] getWb03DtQtzEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_QTZ_EMIS, Pos.WB03_DT_QTZ_EMIS);
        return buffer;
    }

    public void setWb03DtQtzEmisNull(String wb03DtQtzEmisNull) {
        writeString(Pos.WB03_DT_QTZ_EMIS_NULL, wb03DtQtzEmisNull, Len.WB03_DT_QTZ_EMIS_NULL);
    }

    /**Original name: WB03-DT-QTZ-EMIS-NULL<br>*/
    public String getWb03DtQtzEmisNull() {
        return readString(Pos.WB03_DT_QTZ_EMIS_NULL, Len.WB03_DT_QTZ_EMIS_NULL);
    }

    public String getWb03DtQtzEmisNullFormatted() {
        return Functions.padBlanks(getWb03DtQtzEmisNull(), Len.WB03_DT_QTZ_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_QTZ_EMIS = 1;
        public static final int WB03_DT_QTZ_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_QTZ_EMIS = 5;
        public static final int WB03_DT_QTZ_EMIS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_QTZ_EMIS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
