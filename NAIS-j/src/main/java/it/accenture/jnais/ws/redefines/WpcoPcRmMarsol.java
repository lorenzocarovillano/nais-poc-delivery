package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-PC-RM-MARSOL<br>
 * Variable: WPCO-PC-RM-MARSOL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoPcRmMarsol extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoPcRmMarsol() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_PC_RM_MARSOL;
    }

    public void setWpcoPcRmMarsol(AfDecimal wpcoPcRmMarsol) {
        writeDecimalAsPacked(Pos.WPCO_PC_RM_MARSOL, wpcoPcRmMarsol.copy());
    }

    public void setDpcoPcRmMarsolFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_PC_RM_MARSOL, Pos.WPCO_PC_RM_MARSOL);
    }

    /**Original name: WPCO-PC-RM-MARSOL<br>*/
    public AfDecimal getWpcoPcRmMarsol() {
        return readPackedAsDecimal(Pos.WPCO_PC_RM_MARSOL, Len.Int.WPCO_PC_RM_MARSOL, Len.Fract.WPCO_PC_RM_MARSOL);
    }

    public byte[] getWpcoPcRmMarsolAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_PC_RM_MARSOL, Pos.WPCO_PC_RM_MARSOL);
        return buffer;
    }

    public void setWpcoPcRmMarsolNull(String wpcoPcRmMarsolNull) {
        writeString(Pos.WPCO_PC_RM_MARSOL_NULL, wpcoPcRmMarsolNull, Len.WPCO_PC_RM_MARSOL_NULL);
    }

    /**Original name: WPCO-PC-RM-MARSOL-NULL<br>*/
    public String getWpcoPcRmMarsolNull() {
        return readString(Pos.WPCO_PC_RM_MARSOL_NULL, Len.WPCO_PC_RM_MARSOL_NULL);
    }

    public String getWpcoPcRmMarsolNullFormatted() {
        return Functions.padBlanks(getWpcoPcRmMarsolNull(), Len.WPCO_PC_RM_MARSOL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_PC_RM_MARSOL = 1;
        public static final int WPCO_PC_RM_MARSOL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_PC_RM_MARSOL = 4;
        public static final int WPCO_PC_RM_MARSOL_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_PC_RM_MARSOL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_PC_RM_MARSOL = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
