package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPST-252<br>
 * Variable: TCL-IMPST-252 from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpst252 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpst252() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPST252;
    }

    public void setTclImpst252(AfDecimal tclImpst252) {
        writeDecimalAsPacked(Pos.TCL_IMPST252, tclImpst252.copy());
    }

    public void setTclImpst252FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPST252, Pos.TCL_IMPST252);
    }

    /**Original name: TCL-IMPST-252<br>*/
    public AfDecimal getTclImpst252() {
        return readPackedAsDecimal(Pos.TCL_IMPST252, Len.Int.TCL_IMPST252, Len.Fract.TCL_IMPST252);
    }

    public byte[] getTclImpst252AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPST252, Pos.TCL_IMPST252);
        return buffer;
    }

    public void setTclImpst252Null(String tclImpst252Null) {
        writeString(Pos.TCL_IMPST252_NULL, tclImpst252Null, Len.TCL_IMPST252_NULL);
    }

    /**Original name: TCL-IMPST-252-NULL<br>*/
    public String getTclImpst252Null() {
        return readString(Pos.TCL_IMPST252_NULL, Len.TCL_IMPST252_NULL);
    }

    public String getTclImpst252NullFormatted() {
        return Functions.padBlanks(getTclImpst252Null(), Len.TCL_IMPST252_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPST252 = 1;
        public static final int TCL_IMPST252_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPST252 = 8;
        public static final int TCL_IMPST252_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPST252 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPST252 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
