package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-MONT-END2000<br>
 * Variable: LQU-MONT-END2000 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquMontEnd2000 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquMontEnd2000() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_MONT_END2000;
    }

    public void setLquMontEnd2000(AfDecimal lquMontEnd2000) {
        writeDecimalAsPacked(Pos.LQU_MONT_END2000, lquMontEnd2000.copy());
    }

    public void setLquMontEnd2000FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_MONT_END2000, Pos.LQU_MONT_END2000);
    }

    /**Original name: LQU-MONT-END2000<br>*/
    public AfDecimal getLquMontEnd2000() {
        return readPackedAsDecimal(Pos.LQU_MONT_END2000, Len.Int.LQU_MONT_END2000, Len.Fract.LQU_MONT_END2000);
    }

    public byte[] getLquMontEnd2000AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_MONT_END2000, Pos.LQU_MONT_END2000);
        return buffer;
    }

    public void setLquMontEnd2000Null(String lquMontEnd2000Null) {
        writeString(Pos.LQU_MONT_END2000_NULL, lquMontEnd2000Null, Len.LQU_MONT_END2000_NULL);
    }

    /**Original name: LQU-MONT-END2000-NULL<br>*/
    public String getLquMontEnd2000Null() {
        return readString(Pos.LQU_MONT_END2000_NULL, Len.LQU_MONT_END2000_NULL);
    }

    public String getLquMontEnd2000NullFormatted() {
        return Functions.padBlanks(getLquMontEnd2000Null(), Len.LQU_MONT_END2000_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_MONT_END2000 = 1;
        public static final int LQU_MONT_END2000_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_MONT_END2000 = 8;
        public static final int LQU_MONT_END2000_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_MONT_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_MONT_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
