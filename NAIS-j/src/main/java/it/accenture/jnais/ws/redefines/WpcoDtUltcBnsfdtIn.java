package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-BNSFDT-IN<br>
 * Variable: WPCO-DT-ULTC-BNSFDT-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcBnsfdtIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcBnsfdtIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_BNSFDT_IN;
    }

    public void setWpcoDtUltcBnsfdtIn(int wpcoDtUltcBnsfdtIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_BNSFDT_IN, wpcoDtUltcBnsfdtIn, Len.Int.WPCO_DT_ULTC_BNSFDT_IN);
    }

    public void setDpcoDtUltcBnsfdtInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_BNSFDT_IN, Pos.WPCO_DT_ULTC_BNSFDT_IN);
    }

    /**Original name: WPCO-DT-ULTC-BNSFDT-IN<br>*/
    public int getWpcoDtUltcBnsfdtIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_BNSFDT_IN, Len.Int.WPCO_DT_ULTC_BNSFDT_IN);
    }

    public byte[] getWpcoDtUltcBnsfdtInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_BNSFDT_IN, Pos.WPCO_DT_ULTC_BNSFDT_IN);
        return buffer;
    }

    public void setWpcoDtUltcBnsfdtInNull(String wpcoDtUltcBnsfdtInNull) {
        writeString(Pos.WPCO_DT_ULTC_BNSFDT_IN_NULL, wpcoDtUltcBnsfdtInNull, Len.WPCO_DT_ULTC_BNSFDT_IN_NULL);
    }

    /**Original name: WPCO-DT-ULTC-BNSFDT-IN-NULL<br>*/
    public String getWpcoDtUltcBnsfdtInNull() {
        return readString(Pos.WPCO_DT_ULTC_BNSFDT_IN_NULL, Len.WPCO_DT_ULTC_BNSFDT_IN_NULL);
    }

    public String getWpcoDtUltcBnsfdtInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcBnsfdtInNull(), Len.WPCO_DT_ULTC_BNSFDT_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_BNSFDT_IN = 1;
        public static final int WPCO_DT_ULTC_BNSFDT_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_BNSFDT_IN = 5;
        public static final int WPCO_DT_ULTC_BNSFDT_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_BNSFDT_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
