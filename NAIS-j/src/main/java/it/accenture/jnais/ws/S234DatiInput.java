package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: S234-DATI-INPUT<br>
 * Variable: S234-DATI-INPUT from program LCCS0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class S234DatiInput extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: S234-ID-OGG-EOC
    private int idOggEoc = DefaultValues.INT_VAL;
    //Original name: S234-TIPO-OGG-EOC
    private String tipoOggEoc = DefaultValues.stringVal(Len.TIPO_OGG_EOC);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S234_DATI_INPUT;
    }

    @Override
    public void deserialize(byte[] buf) {
        setS234DatiInputBytes(buf);
    }

    public String getS234DatiInputFormatted() {
        return MarshalByteExt.bufferToStr(getS234DatiInputBytes());
    }

    public void setS234DatiInputBytes(byte[] buffer) {
        setS234DatiInputBytes(buffer, 1);
    }

    public byte[] getS234DatiInputBytes() {
        byte[] buffer = new byte[Len.S234_DATI_INPUT];
        return getS234DatiInputBytes(buffer, 1);
    }

    public void setS234DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        idOggEoc = MarshalByte.readInt(buffer, position, Len.ID_OGG_EOC);
        position += Len.ID_OGG_EOC;
        tipoOggEoc = MarshalByte.readString(buffer, position, Len.TIPO_OGG_EOC);
    }

    public byte[] getS234DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeInt(buffer, position, idOggEoc, Len.ID_OGG_EOC);
        position += Len.ID_OGG_EOC;
        MarshalByte.writeString(buffer, position, tipoOggEoc, Len.TIPO_OGG_EOC);
        return buffer;
    }

    public void setIdOggEoc(int idOggEoc) {
        this.idOggEoc = idOggEoc;
    }

    public int getIdOggEoc() {
        return this.idOggEoc;
    }

    public void setTipoOggEoc(String tipoOggEoc) {
        this.tipoOggEoc = Functions.subString(tipoOggEoc, Len.TIPO_OGG_EOC);
    }

    public String getTipoOggEoc() {
        return this.tipoOggEoc;
    }

    @Override
    public byte[] serialize() {
        return getS234DatiInputBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG_EOC = 9;
        public static final int TIPO_OGG_EOC = 2;
        public static final int S234_DATI_INPUT = ID_OGG_EOC + TIPO_OGG_EOC;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
