package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-DT-INI-VAL-TAR<br>
 * Variable: TGA-DT-INI-VAL-TAR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaDtIniValTar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaDtIniValTar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_DT_INI_VAL_TAR;
    }

    public void setTgaDtIniValTar(int tgaDtIniValTar) {
        writeIntAsPacked(Pos.TGA_DT_INI_VAL_TAR, tgaDtIniValTar, Len.Int.TGA_DT_INI_VAL_TAR);
    }

    public void setTgaDtIniValTarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_DT_INI_VAL_TAR, Pos.TGA_DT_INI_VAL_TAR);
    }

    /**Original name: TGA-DT-INI-VAL-TAR<br>*/
    public int getTgaDtIniValTar() {
        return readPackedAsInt(Pos.TGA_DT_INI_VAL_TAR, Len.Int.TGA_DT_INI_VAL_TAR);
    }

    public String getTgaDtIniValTarFormatted() {
        return PicFormatter.display(new PicParams("S9(8)V").setUsage(PicUsage.PACKED)).format(getTgaDtIniValTar()).toString();
    }

    public byte[] getTgaDtIniValTarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_DT_INI_VAL_TAR, Pos.TGA_DT_INI_VAL_TAR);
        return buffer;
    }

    public void setTgaDtIniValTarNull(String tgaDtIniValTarNull) {
        writeString(Pos.TGA_DT_INI_VAL_TAR_NULL, tgaDtIniValTarNull, Len.TGA_DT_INI_VAL_TAR_NULL);
    }

    /**Original name: TGA-DT-INI-VAL-TAR-NULL<br>*/
    public String getTgaDtIniValTarNull() {
        return readString(Pos.TGA_DT_INI_VAL_TAR_NULL, Len.TGA_DT_INI_VAL_TAR_NULL);
    }

    public String getTgaDtIniValTarNullFormatted() {
        return Functions.padBlanks(getTgaDtIniValTarNull(), Len.TGA_DT_INI_VAL_TAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_DT_INI_VAL_TAR = 1;
        public static final int TGA_DT_INI_VAL_TAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_DT_INI_VAL_TAR = 5;
        public static final int TGA_DT_INI_VAL_TAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_DT_INI_VAL_TAR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
