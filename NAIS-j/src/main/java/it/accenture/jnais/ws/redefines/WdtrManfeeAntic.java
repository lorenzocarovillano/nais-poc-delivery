package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-MANFEE-ANTIC<br>
 * Variable: WDTR-MANFEE-ANTIC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_MANFEE_ANTIC;
    }

    public void setWdtrManfeeAntic(AfDecimal wdtrManfeeAntic) {
        writeDecimalAsPacked(Pos.WDTR_MANFEE_ANTIC, wdtrManfeeAntic.copy());
    }

    /**Original name: WDTR-MANFEE-ANTIC<br>*/
    public AfDecimal getWdtrManfeeAntic() {
        return readPackedAsDecimal(Pos.WDTR_MANFEE_ANTIC, Len.Int.WDTR_MANFEE_ANTIC, Len.Fract.WDTR_MANFEE_ANTIC);
    }

    public void setWdtrManfeeAnticNull(String wdtrManfeeAnticNull) {
        writeString(Pos.WDTR_MANFEE_ANTIC_NULL, wdtrManfeeAnticNull, Len.WDTR_MANFEE_ANTIC_NULL);
    }

    /**Original name: WDTR-MANFEE-ANTIC-NULL<br>*/
    public String getWdtrManfeeAnticNull() {
        return readString(Pos.WDTR_MANFEE_ANTIC_NULL, Len.WDTR_MANFEE_ANTIC_NULL);
    }

    public String getWdtrManfeeAnticNullFormatted() {
        return Functions.padBlanks(getWdtrManfeeAnticNull(), Len.WDTR_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_MANFEE_ANTIC = 1;
        public static final int WDTR_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_MANFEE_ANTIC = 8;
        public static final int WDTR_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
