package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-PC-GAR-NORISK-MARS<br>
 * Variable: PCO-PC-GAR-NORISK-MARS from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoPcGarNoriskMars extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoPcGarNoriskMars() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_PC_GAR_NORISK_MARS;
    }

    public void setPcoPcGarNoriskMars(AfDecimal pcoPcGarNoriskMars) {
        writeDecimalAsPacked(Pos.PCO_PC_GAR_NORISK_MARS, pcoPcGarNoriskMars.copy());
    }

    public void setPcoPcGarNoriskMarsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_PC_GAR_NORISK_MARS, Pos.PCO_PC_GAR_NORISK_MARS);
    }

    /**Original name: PCO-PC-GAR-NORISK-MARS<br>*/
    public AfDecimal getPcoPcGarNoriskMars() {
        return readPackedAsDecimal(Pos.PCO_PC_GAR_NORISK_MARS, Len.Int.PCO_PC_GAR_NORISK_MARS, Len.Fract.PCO_PC_GAR_NORISK_MARS);
    }

    public byte[] getPcoPcGarNoriskMarsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_PC_GAR_NORISK_MARS, Pos.PCO_PC_GAR_NORISK_MARS);
        return buffer;
    }

    public void initPcoPcGarNoriskMarsHighValues() {
        fill(Pos.PCO_PC_GAR_NORISK_MARS, Len.PCO_PC_GAR_NORISK_MARS, Types.HIGH_CHAR_VAL);
    }

    public void setPcoPcGarNoriskMarsNull(String pcoPcGarNoriskMarsNull) {
        writeString(Pos.PCO_PC_GAR_NORISK_MARS_NULL, pcoPcGarNoriskMarsNull, Len.PCO_PC_GAR_NORISK_MARS_NULL);
    }

    /**Original name: PCO-PC-GAR-NORISK-MARS-NULL<br>*/
    public String getPcoPcGarNoriskMarsNull() {
        return readString(Pos.PCO_PC_GAR_NORISK_MARS_NULL, Len.PCO_PC_GAR_NORISK_MARS_NULL);
    }

    public String getPcoPcGarNoriskMarsNullFormatted() {
        return Functions.padBlanks(getPcoPcGarNoriskMarsNull(), Len.PCO_PC_GAR_NORISK_MARS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_PC_GAR_NORISK_MARS = 1;
        public static final int PCO_PC_GAR_NORISK_MARS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_PC_GAR_NORISK_MARS = 4;
        public static final int PCO_PC_GAR_NORISK_MARS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_PC_GAR_NORISK_MARS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_PC_GAR_NORISK_MARS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
