package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPRE-PREST-RES-EFF<br>
 * Variable: WPRE-PREST-RES-EFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WprePrestResEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WprePrestResEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_PREST_RES_EFF;
    }

    public void setWprePrestResEff(AfDecimal wprePrestResEff) {
        writeDecimalAsPacked(Pos.WPRE_PREST_RES_EFF, wprePrestResEff.copy());
    }

    public void setWprePrestResEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_PREST_RES_EFF, Pos.WPRE_PREST_RES_EFF);
    }

    /**Original name: WPRE-PREST-RES-EFF<br>*/
    public AfDecimal getWprePrestResEff() {
        return readPackedAsDecimal(Pos.WPRE_PREST_RES_EFF, Len.Int.WPRE_PREST_RES_EFF, Len.Fract.WPRE_PREST_RES_EFF);
    }

    public byte[] getWprePrestResEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_PREST_RES_EFF, Pos.WPRE_PREST_RES_EFF);
        return buffer;
    }

    public void initWprePrestResEffSpaces() {
        fill(Pos.WPRE_PREST_RES_EFF, Len.WPRE_PREST_RES_EFF, Types.SPACE_CHAR);
    }

    public void setWprePrestResEffNull(String wprePrestResEffNull) {
        writeString(Pos.WPRE_PREST_RES_EFF_NULL, wprePrestResEffNull, Len.WPRE_PREST_RES_EFF_NULL);
    }

    /**Original name: WPRE-PREST-RES-EFF-NULL<br>*/
    public String getWprePrestResEffNull() {
        return readString(Pos.WPRE_PREST_RES_EFF_NULL, Len.WPRE_PREST_RES_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_PREST_RES_EFF = 1;
        public static final int WPRE_PREST_RES_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_PREST_RES_EFF = 8;
        public static final int WPRE_PREST_RES_EFF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPRE_PREST_RES_EFF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_PREST_RES_EFF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
