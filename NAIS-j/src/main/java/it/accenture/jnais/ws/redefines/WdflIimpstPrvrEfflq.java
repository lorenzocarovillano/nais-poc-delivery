package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IIMPST-PRVR-EFFLQ<br>
 * Variable: WDFL-IIMPST-PRVR-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIimpstPrvrEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIimpstPrvrEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IIMPST_PRVR_EFFLQ;
    }

    public void setWdflIimpstPrvrEfflq(AfDecimal wdflIimpstPrvrEfflq) {
        writeDecimalAsPacked(Pos.WDFL_IIMPST_PRVR_EFFLQ, wdflIimpstPrvrEfflq.copy());
    }

    public void setWdflIimpstPrvrEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IIMPST_PRVR_EFFLQ, Pos.WDFL_IIMPST_PRVR_EFFLQ);
    }

    /**Original name: WDFL-IIMPST-PRVR-EFFLQ<br>*/
    public AfDecimal getWdflIimpstPrvrEfflq() {
        return readPackedAsDecimal(Pos.WDFL_IIMPST_PRVR_EFFLQ, Len.Int.WDFL_IIMPST_PRVR_EFFLQ, Len.Fract.WDFL_IIMPST_PRVR_EFFLQ);
    }

    public byte[] getWdflIimpstPrvrEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IIMPST_PRVR_EFFLQ, Pos.WDFL_IIMPST_PRVR_EFFLQ);
        return buffer;
    }

    public void setWdflIimpstPrvrEfflqNull(String wdflIimpstPrvrEfflqNull) {
        writeString(Pos.WDFL_IIMPST_PRVR_EFFLQ_NULL, wdflIimpstPrvrEfflqNull, Len.WDFL_IIMPST_PRVR_EFFLQ_NULL);
    }

    /**Original name: WDFL-IIMPST-PRVR-EFFLQ-NULL<br>*/
    public String getWdflIimpstPrvrEfflqNull() {
        return readString(Pos.WDFL_IIMPST_PRVR_EFFLQ_NULL, Len.WDFL_IIMPST_PRVR_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IIMPST_PRVR_EFFLQ = 1;
        public static final int WDFL_IIMPST_PRVR_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IIMPST_PRVR_EFFLQ = 8;
        public static final int WDFL_IIMPST_PRVR_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IIMPST_PRVR_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IIMPST_PRVR_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
