package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: GG-DIFF<br>
 * Variable: GG-DIFF from program LCCS0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class GgDiff extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: GG-DIFF
    private String ggDiff = "";

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GG_DIFF;
    }

    @Override
    public void deserialize(byte[] buf) {
        setGgDiffFromBuffer(buf);
    }

    public void setGgDiff(int ggDiff) {
        this.ggDiff = NumericDisplay.asString(ggDiff, Len.GG_DIFF);
    }

    public void setGgDiffFormatted(String ggDiff) {
        this.ggDiff = Trunc.toUnsignedNumeric(ggDiff, Len.GG_DIFF);
    }

    public void setGgDiffFromBuffer(byte[] buffer, int offset) {
        setGgDiffFormatted(MarshalByte.readString(buffer, offset, Len.GG_DIFF));
    }

    public void setGgDiffFromBuffer(byte[] buffer) {
        setGgDiffFromBuffer(buffer, 1);
    }

    public int getGgDiff() {
        return NumericDisplay.asInt(this.ggDiff);
    }

    @Override
    public byte[] serialize() {
        return MarshalByteExt.intToBuffer(getGgDiff(), Len.Int.GG_DIFF, SignType.NO_SIGN);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GG_DIFF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GG_DIFF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int GG_DIFF = 0;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
