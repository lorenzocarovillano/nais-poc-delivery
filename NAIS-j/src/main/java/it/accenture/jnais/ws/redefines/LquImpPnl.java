package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-PNL<br>
 * Variable: LQU-IMP-PNL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpPnl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpPnl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_PNL;
    }

    public void setLquImpPnl(AfDecimal lquImpPnl) {
        writeDecimalAsPacked(Pos.LQU_IMP_PNL, lquImpPnl.copy());
    }

    public void setLquImpPnlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_PNL, Pos.LQU_IMP_PNL);
    }

    /**Original name: LQU-IMP-PNL<br>*/
    public AfDecimal getLquImpPnl() {
        return readPackedAsDecimal(Pos.LQU_IMP_PNL, Len.Int.LQU_IMP_PNL, Len.Fract.LQU_IMP_PNL);
    }

    public byte[] getLquImpPnlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_PNL, Pos.LQU_IMP_PNL);
        return buffer;
    }

    public void setLquImpPnlNull(String lquImpPnlNull) {
        writeString(Pos.LQU_IMP_PNL_NULL, lquImpPnlNull, Len.LQU_IMP_PNL_NULL);
    }

    /**Original name: LQU-IMP-PNL-NULL<br>*/
    public String getLquImpPnlNull() {
        return readString(Pos.LQU_IMP_PNL_NULL, Len.LQU_IMP_PNL_NULL);
    }

    public String getLquImpPnlNullFormatted() {
        return Functions.padBlanks(getLquImpPnlNull(), Len.LQU_IMP_PNL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_PNL = 1;
        public static final int LQU_IMP_PNL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_PNL = 8;
        public static final int LQU_IMP_PNL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_PNL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_PNL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
