package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0211-TAB-VAR-NOT-FOUND<br>
 * Variables: IVVC0211-TAB-VAR-NOT-FOUND from copybook IVVC0211<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0211TabVarNotFound {

    //==== PROPERTIES ====
    //Original name: IVVC0211-VAR-NOT-FOUND
    private String found = DefaultValues.stringVal(Len.FOUND);
    //Original name: IVVC0211-VAR-NOT-FOUND-SP
    private char foundSp = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setTabVarNotFoundBytes(byte[] buffer, int offset) {
        int position = offset;
        found = MarshalByte.readString(buffer, position, Len.FOUND);
        position += Len.FOUND;
        foundSp = MarshalByte.readChar(buffer, position);
    }

    public byte[] getTabVarNotFoundBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, found, Len.FOUND);
        position += Len.FOUND;
        MarshalByte.writeChar(buffer, position, foundSp);
        return buffer;
    }

    public void initTabVarNotFoundSpaces() {
        found = "";
        foundSp = Types.SPACE_CHAR;
    }

    public void setFound(String found) {
        this.found = Functions.subString(found, Len.FOUND);
    }

    public String getFound() {
        return this.found;
    }

    public void setFoundSp(char foundSp) {
        this.foundSp = foundSp;
    }

    public char getFoundSp() {
        return this.foundSp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FOUND = 19;
        public static final int FOUND_SP = 1;
        public static final int TAB_VAR_NOT_FOUND = FOUND + FOUND_SP;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
