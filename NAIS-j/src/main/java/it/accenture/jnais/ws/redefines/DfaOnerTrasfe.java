package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-ONER-TRASFE<br>
 * Variable: DFA-ONER-TRASFE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaOnerTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaOnerTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_ONER_TRASFE;
    }

    public void setDfaOnerTrasfe(AfDecimal dfaOnerTrasfe) {
        writeDecimalAsPacked(Pos.DFA_ONER_TRASFE, dfaOnerTrasfe.copy());
    }

    public void setDfaOnerTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_ONER_TRASFE, Pos.DFA_ONER_TRASFE);
    }

    /**Original name: DFA-ONER-TRASFE<br>*/
    public AfDecimal getDfaOnerTrasfe() {
        return readPackedAsDecimal(Pos.DFA_ONER_TRASFE, Len.Int.DFA_ONER_TRASFE, Len.Fract.DFA_ONER_TRASFE);
    }

    public byte[] getDfaOnerTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_ONER_TRASFE, Pos.DFA_ONER_TRASFE);
        return buffer;
    }

    public void setDfaOnerTrasfeNull(String dfaOnerTrasfeNull) {
        writeString(Pos.DFA_ONER_TRASFE_NULL, dfaOnerTrasfeNull, Len.DFA_ONER_TRASFE_NULL);
    }

    /**Original name: DFA-ONER-TRASFE-NULL<br>*/
    public String getDfaOnerTrasfeNull() {
        return readString(Pos.DFA_ONER_TRASFE_NULL, Len.DFA_ONER_TRASFE_NULL);
    }

    public String getDfaOnerTrasfeNullFormatted() {
        return Functions.padBlanks(getDfaOnerTrasfeNull(), Len.DFA_ONER_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_ONER_TRASFE = 1;
        public static final int DFA_ONER_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_ONER_TRASFE = 8;
        public static final int DFA_ONER_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_ONER_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_ONER_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
