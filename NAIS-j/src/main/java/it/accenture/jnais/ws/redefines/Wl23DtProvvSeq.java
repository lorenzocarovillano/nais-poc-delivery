package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WL23-DT-PROVV-SEQ<br>
 * Variable: WL23-DT-PROVV-SEQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl23DtProvvSeq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl23DtProvvSeq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL23_DT_PROVV_SEQ;
    }

    public void setWl23DtProvvSeq(int wl23DtProvvSeq) {
        writeIntAsPacked(Pos.WL23_DT_PROVV_SEQ, wl23DtProvvSeq, Len.Int.WL23_DT_PROVV_SEQ);
    }

    public void setWl23DtProvvSeqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL23_DT_PROVV_SEQ, Pos.WL23_DT_PROVV_SEQ);
    }

    /**Original name: WL23-DT-PROVV-SEQ<br>*/
    public int getWl23DtProvvSeq() {
        return readPackedAsInt(Pos.WL23_DT_PROVV_SEQ, Len.Int.WL23_DT_PROVV_SEQ);
    }

    public byte[] getWl23DtProvvSeqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL23_DT_PROVV_SEQ, Pos.WL23_DT_PROVV_SEQ);
        return buffer;
    }

    public void initWl23DtProvvSeqSpaces() {
        fill(Pos.WL23_DT_PROVV_SEQ, Len.WL23_DT_PROVV_SEQ, Types.SPACE_CHAR);
    }

    public void setWl23DtProvvSeqNull(String wl23DtProvvSeqNull) {
        writeString(Pos.WL23_DT_PROVV_SEQ_NULL, wl23DtProvvSeqNull, Len.WL23_DT_PROVV_SEQ_NULL);
    }

    /**Original name: WL23-DT-PROVV-SEQ-NULL<br>*/
    public String getWl23DtProvvSeqNull() {
        return readString(Pos.WL23_DT_PROVV_SEQ_NULL, Len.WL23_DT_PROVV_SEQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL23_DT_PROVV_SEQ = 1;
        public static final int WL23_DT_PROVV_SEQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL23_DT_PROVV_SEQ = 5;
        public static final int WL23_DT_PROVV_SEQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL23_DT_PROVV_SEQ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
