package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-PROV-2AA-ACQ<br>
 * Variable: PVT-PROV-2AA-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtProv2aaAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtProv2aaAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_PROV2AA_ACQ;
    }

    public void setPvtProv2aaAcq(AfDecimal pvtProv2aaAcq) {
        writeDecimalAsPacked(Pos.PVT_PROV2AA_ACQ, pvtProv2aaAcq.copy());
    }

    public void setPvtProv2aaAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_PROV2AA_ACQ, Pos.PVT_PROV2AA_ACQ);
    }

    /**Original name: PVT-PROV-2AA-ACQ<br>*/
    public AfDecimal getPvtProv2aaAcq() {
        return readPackedAsDecimal(Pos.PVT_PROV2AA_ACQ, Len.Int.PVT_PROV2AA_ACQ, Len.Fract.PVT_PROV2AA_ACQ);
    }

    public byte[] getPvtProv2aaAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_PROV2AA_ACQ, Pos.PVT_PROV2AA_ACQ);
        return buffer;
    }

    public void setPvtProv2aaAcqNull(String pvtProv2aaAcqNull) {
        writeString(Pos.PVT_PROV2AA_ACQ_NULL, pvtProv2aaAcqNull, Len.PVT_PROV2AA_ACQ_NULL);
    }

    /**Original name: PVT-PROV-2AA-ACQ-NULL<br>*/
    public String getPvtProv2aaAcqNull() {
        return readString(Pos.PVT_PROV2AA_ACQ_NULL, Len.PVT_PROV2AA_ACQ_NULL);
    }

    public String getPvtProv2aaAcqNullFormatted() {
        return Functions.padBlanks(getPvtProv2aaAcqNull(), Len.PVT_PROV2AA_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_PROV2AA_ACQ = 1;
        public static final int PVT_PROV2AA_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_PROV2AA_ACQ = 8;
        public static final int PVT_PROV2AA_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_PROV2AA_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PVT_PROV2AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
