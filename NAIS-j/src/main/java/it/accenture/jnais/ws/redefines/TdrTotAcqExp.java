package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-ACQ-EXP<br>
 * Variable: TDR-TOT-ACQ-EXP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_ACQ_EXP;
    }

    public void setTdrTotAcqExp(AfDecimal tdrTotAcqExp) {
        writeDecimalAsPacked(Pos.TDR_TOT_ACQ_EXP, tdrTotAcqExp.copy());
    }

    public void setTdrTotAcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_ACQ_EXP, Pos.TDR_TOT_ACQ_EXP);
    }

    /**Original name: TDR-TOT-ACQ-EXP<br>*/
    public AfDecimal getTdrTotAcqExp() {
        return readPackedAsDecimal(Pos.TDR_TOT_ACQ_EXP, Len.Int.TDR_TOT_ACQ_EXP, Len.Fract.TDR_TOT_ACQ_EXP);
    }

    public byte[] getTdrTotAcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_ACQ_EXP, Pos.TDR_TOT_ACQ_EXP);
        return buffer;
    }

    public void setTdrTotAcqExpNull(String tdrTotAcqExpNull) {
        writeString(Pos.TDR_TOT_ACQ_EXP_NULL, tdrTotAcqExpNull, Len.TDR_TOT_ACQ_EXP_NULL);
    }

    /**Original name: TDR-TOT-ACQ-EXP-NULL<br>*/
    public String getTdrTotAcqExpNull() {
        return readString(Pos.TDR_TOT_ACQ_EXP_NULL, Len.TDR_TOT_ACQ_EXP_NULL);
    }

    public String getTdrTotAcqExpNullFormatted() {
        return Functions.padBlanks(getTdrTotAcqExpNull(), Len.TDR_TOT_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_ACQ_EXP = 1;
        public static final int TDR_TOT_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_ACQ_EXP = 8;
        public static final int TDR_TOT_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
