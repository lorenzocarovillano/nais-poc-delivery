package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-IMP-AZ<br>
 * Variable: ADE-IMP-AZ from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_IMP_AZ;
    }

    public void setAdeImpAz(AfDecimal adeImpAz) {
        writeDecimalAsPacked(Pos.ADE_IMP_AZ, adeImpAz.copy());
    }

    public void setAdeImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_IMP_AZ, Pos.ADE_IMP_AZ);
    }

    /**Original name: ADE-IMP-AZ<br>*/
    public AfDecimal getAdeImpAz() {
        return readPackedAsDecimal(Pos.ADE_IMP_AZ, Len.Int.ADE_IMP_AZ, Len.Fract.ADE_IMP_AZ);
    }

    public byte[] getAdeImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_IMP_AZ, Pos.ADE_IMP_AZ);
        return buffer;
    }

    public void setAdeImpAzNull(String adeImpAzNull) {
        writeString(Pos.ADE_IMP_AZ_NULL, adeImpAzNull, Len.ADE_IMP_AZ_NULL);
    }

    /**Original name: ADE-IMP-AZ-NULL<br>*/
    public String getAdeImpAzNull() {
        return readString(Pos.ADE_IMP_AZ_NULL, Len.ADE_IMP_AZ_NULL);
    }

    public String getAdeImpAzNullFormatted() {
        return Functions.padBlanks(getAdeImpAzNull(), Len.ADE_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_IMP_AZ = 1;
        public static final int ADE_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_IMP_AZ = 8;
        public static final int ADE_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
