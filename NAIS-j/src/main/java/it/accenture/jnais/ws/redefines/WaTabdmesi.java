package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WA-TABDMESI<br>
 * Variable: WA-TABDMESI from program LCCS0003<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WaTabdmesi extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int DMESE_MAXOCCURS = 12;

    //==== CONSTRUCTORS ====
    public WaTabdmesi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WA_TABDMESI;
    }

    @Override
    public void init() {
        int position = 1;
        writeString(position, "GENNAIO", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "FEBBRAIO", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "MARZO", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "APRILE", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "MAGGIO", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "GIUGNO", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "LUGLIO", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "AGOSTO", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "SETTEMBRE", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "OTTOBRE", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "NOVEMBRE", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "DICEMBRE", Len.FLR1);
    }

    /**Original name: WA-DMESE<br>*/
    public String getDmese(int dmeseIdx) {
        int position = Pos.waDmese(dmeseIdx - 1);
        return readString(position, Len.DMESE);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WA_TABDMESI = 1;
        public static final int FLR1 = WA_TABDMESI;
        public static final int FLR2 = FLR1 + Len.FLR1;
        public static final int FLR3 = FLR2 + Len.FLR1;
        public static final int FLR4 = FLR3 + Len.FLR1;
        public static final int FLR5 = FLR4 + Len.FLR1;
        public static final int FLR6 = FLR5 + Len.FLR1;
        public static final int FLR7 = FLR6 + Len.FLR1;
        public static final int FLR8 = FLR7 + Len.FLR1;
        public static final int FLR9 = FLR8 + Len.FLR1;
        public static final int FLR10 = FLR9 + Len.FLR1;
        public static final int FLR11 = FLR10 + Len.FLR1;
        public static final int FLR12 = FLR11 + Len.FLR1;
        public static final int FILLER_WS = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int waDmese(int idx) {
            return FILLER_WS + idx * Len.DMESE;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 9;
        public static final int DMESE = 9;
        public static final int WA_TABDMESI = 12 * FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
