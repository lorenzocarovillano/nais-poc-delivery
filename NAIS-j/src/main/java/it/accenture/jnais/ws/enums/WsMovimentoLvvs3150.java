package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-MOVIMENTO<br>
 * Variable: WS-MOVIMENTO from copybook LCCVXMVZ<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsMovimentoLvvs3150 {

    //==== PROPERTIES ====
    private String value = "00000";
    public static final String SCHEDUL_OPERAZ_AUTOM = "06000";
    public static final String RITAR_ACCES = "06001";
    public static final String GENER_TRANCH = "06002";
    public static final String GENER_TRAINC = "06003";
    public static final String GENER_TRAPRE = "06004";
    public static final String GENER_RISPAR = "06005";
    public static final String ADPRE_PRESTA = "06006";
    public static final String GENER_CEDOLA = "06007";
    public static final String RICON_COLLET = "06008";
    public static final String VARIA_OPZION = "06009";
    public static final String CALC_IMPOSTA_SOSTIT = "06010";
    public static final String CALC_IMPST_BOLLO = "06012";
    public static final String BONUS_FEDE = "06013";
    public static final String SIGNI_RISCH = "06014";
    public static final String ATTUA_STINV = "06015";
    public static final String BONUS_RICOR = "06016";
    public static final String DETERM_BONUS = "06017";
    public static final String LIQUI_CEDOLE = "06018";
    public static final String APPL_CALC_IMPST_SOST = "06020";
    public static final String STORNO_AUTOMATICO_PUR = "06021";
    public static final String LIQUI_MANFEE = "06023";
    public static final String CALC_MANFEE_RICH = "06024";
    public static final String LIQ_MANFEE_ANTIC = "06027";
    public static final String LIQ_MANFEE_RICH = "06028";
    public static final String LIQ_MANFEE_RISC_SECON_AA = "06029";
    public static final String LIQ_MANFEE_RISC_ALTRE_PO = "06030";
    public static final String COMMISSIONI_UNIT = "06031";
    public static final String CALC_BONUS_REBATE = "06032";
    public static final String DIFFERIMENTO_PROROGA = "06033";
    public static final String LIQ_MAS_IS_X_ANTIC = "06034";
    public static final String CONS_CNBT_CPTZ = "06035";
    public static final String RISPAR_PROGRAM_REINV = "06036";
    public static final String MOVIM_STORNO_CEDOLA = "06037";
    public static final String GENER_PREVIRT = "06038";
    public static final String ADEG_PRESGIORN = "06039";
    public static final String PRESCRZ_AUTOM = "06040";
    public static final String COMUN_DORM_MEF = "06041";
    public static final String LIQUI_DORM_MEF = "06042";
    public static final String ATT_SERV_CONS_RENDIM = "06043";
    public static final String ATT_SERV_REINVST_CALI = "06044";
    public static final String ATT_SERV_BIG_CHANCE_DE = "06045";
    public static final String CALCOLI_PREVISIONALI = "06049";
    public static final String RINN_TAC_KM2009 = "06050";
    public static final String VERIFICA_RINNOVO_TACITO = "06051";
    public static final String STATIS_RICH_EST = "06052";
    public static final String ATT_SERV_BIG_CHANCE = "06053";
    public static final String ATTRIB_COSTI_AMM = "06054";
    public static final String CALC_BONUS_GG_KM2009 = "06056";
    public static final String APPLICA_BONUS_KM2009 = "06057";
    public static final String CRIST_AL20140630 = "06058";
    public static final String REPORT_COMMIS_GEST = "06059";
    public static final String ESTRAZIONE_DECESSI_COMUNICATI = "06063";
    public static final String ESTRAZIONE_CALCOLO_COSTI = "06064";
    public static final String GESTIONE_TRANCHE_CT0 = "06065";
    public static final String INVEST_OPER_STRAORD = "06070";
    public static final String DISINV_OPER_STRAORD = "06071";
    public static final String MOVI_ANAG = "06080";
    public static final String MOVI_ANAG_EVE = "06081";
    public static final String MOVIM_QUIETA = "06101";
    public static final String COMMISSIONI_UNIT_SWITCH = "06131";
    public static final String MOVIM_QUIETA_INT_PREST = "06102";
    public static final String INVESTIMENTO = "06201";
    public static final String DISINVESTIMENTO = "06202";
    public static final String REINVESTIMENTO = "06203";
    public static final String CALCOLO_RISERVE = "06204";
    public static final String COMUNIC_INVESTIMENTO = "06205";
    public static final String COMUNIC_DISINVESTIMENTO = "06206";
    public static final String PORTAFOGLIO_DINAMICO = "06207";
    public static final String ESTRATTO_CONTO = "06208";
    public static final String ATT_STRAT_INVST = "06210";
    public static final String AGG_SALDO_QUOTE = "06212";
    public static final String AGG_SALDO_PREMI = "06213";
    public static final String CERTIF_FISCALI = "06214";
    public static final String ELAB_FLS_BATCH_FND = "06301";
    public static final String CONS_ELAB_FLS_BATCH_FND = "06302";
    public static final String CAMBIO_STATO = "06305";
    public static final String QUADR_CPI_PROT_BUND = "06306";
    public static final String INQUIRY_COLL = "06500";
    public static final String INQUIRY_ADES = "06501";
    public static final String INQUIRY_INDIV = "06502";
    public static final String INQUIRY_DAT_DEF = "06503";
    public static final String ANALISI_PERDITE = "06601";
    public static final String ESPLO_TRATTATI = "06700";
    public static final String RICALC_RINNOVO = "06701";
    public static final String DIST_PRE_RISC_CESSIONI = "06710";
    public static final String DIST_PRE_RISC_RINNOVI = "06711";
    public static final String DIST_PRE_RISC_STORNI = "06712";
    public static final String DIST_PRE_RISC_LIQUIDAZ = "06713";
    public static final String DIST_PRE_RISC_RIATTIVAZ = "06714";
    public static final String DIST_PRE_COMMERC_CESSIONI = "06715";
    public static final String DIST_PRE_COMMERC_RINNOVI = "06716";
    public static final String DIST_PRE_COMMERC_STORNI = "06717";
    public static final String DIST_PRE_COMMERC_LIQUIDAZ = "06718";
    public static final String DIST_PRE_COMMERC_RIATTIVAZ = "06719";
    public static final String RICAGG_GEST_RENDITE = "06800";
    public static final String GEST_DATI_PAGAM_RID = "06801";
    public static final String CALC_BONUS_SCADENZA = "06802";
    public static final String SOSPENSIONE_RENDITA = "06901";
    public static final String RIATTIVAZIONE_RENDITA = "06902";
    public static final String QUIETANZAM_RATA_DI_RENDITA = "06903";
    public static final String STORNO_CONCLUSIONE_RENDITA = "06904";
    public static final String PRENOTA_RECUP_CONFERME = "06905";
    public static final String PRENOTA_CARICA_CONF_GIORNO = "06906";
    public static final String PRENOTA_RECUP_GAP_IM = "06907";
    public static final String PRENOTA_ESTRAT_PDL = "06908";
    public static final String PRENOTA_ESTRAT_STOR_SCAD = "06909";
    public static final String PRENOTA_RECUP_FILE_PREC = "06910";
    public static final String PRENOTA_RECUP_CALCOLO_PDL = "06911";
    public static final String PRENOTA_BONFICA_FLUSSO = "06912";
    public static final String PRENOTA_RECUP_BACKUP = "06913";

    //==== METHODS ====
    public void setWsMovimento(int wsMovimento) {
        this.value = NumericDisplay.asString(wsMovimento, Len.WS_MOVIMENTO);
    }

    public void setWsMovimentoFormatted(String wsMovimento) {
        this.value = Trunc.toUnsignedNumeric(wsMovimento, Len.WS_MOVIMENTO);
    }

    public int getWsMovimento() {
        return NumericDisplay.asInt(this.value);
    }

    public String getWsMovimentoFormatted() {
        return this.value;
    }

    public boolean isGenerCedola() {
        return getWsMovimentoFormatted().equals(GENER_CEDOLA);
    }

    public void setGenerCedola() {
        setWsMovimentoFormatted(GENER_CEDOLA);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_MOVIMENTO = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
