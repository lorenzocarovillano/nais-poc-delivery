package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: A25-COD-PERS-SECOND<br>
 * Variable: A25-COD-PERS-SECOND from program LDBS1300<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A25CodPersSecond extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A25CodPersSecond() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A25_COD_PERS_SECOND;
    }

    public void setA25CodPersSecond(long a25CodPersSecond) {
        writeLongAsPacked(Pos.A25_COD_PERS_SECOND, a25CodPersSecond, Len.Int.A25_COD_PERS_SECOND);
    }

    public void setA25CodPersSecondFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A25_COD_PERS_SECOND, Pos.A25_COD_PERS_SECOND);
    }

    /**Original name: A25-COD-PERS-SECOND<br>*/
    public long getA25CodPersSecond() {
        return readPackedAsLong(Pos.A25_COD_PERS_SECOND, Len.Int.A25_COD_PERS_SECOND);
    }

    public byte[] getA25CodPersSecondAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A25_COD_PERS_SECOND, Pos.A25_COD_PERS_SECOND);
        return buffer;
    }

    public void setA25CodPersSecondNull(String a25CodPersSecondNull) {
        writeString(Pos.A25_COD_PERS_SECOND_NULL, a25CodPersSecondNull, Len.A25_COD_PERS_SECOND_NULL);
    }

    /**Original name: A25-COD-PERS-SECOND-NULL<br>*/
    public String getA25CodPersSecondNull() {
        return readString(Pos.A25_COD_PERS_SECOND_NULL, Len.A25_COD_PERS_SECOND_NULL);
    }

    public String getA25CodPersSecondNullFormatted() {
        return Functions.padBlanks(getA25CodPersSecondNull(), Len.A25_COD_PERS_SECOND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A25_COD_PERS_SECOND = 1;
        public static final int A25_COD_PERS_SECOND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_COD_PERS_SECOND = 6;
        public static final int A25_COD_PERS_SECOND_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_COD_PERS_SECOND = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
