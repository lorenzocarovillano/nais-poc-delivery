package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-MANFEE-REC<br>
 * Variable: TDR-TOT-MANFEE-REC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotManfeeRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotManfeeRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_MANFEE_REC;
    }

    public void setTdrTotManfeeRec(AfDecimal tdrTotManfeeRec) {
        writeDecimalAsPacked(Pos.TDR_TOT_MANFEE_REC, tdrTotManfeeRec.copy());
    }

    public void setTdrTotManfeeRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_MANFEE_REC, Pos.TDR_TOT_MANFEE_REC);
    }

    /**Original name: TDR-TOT-MANFEE-REC<br>*/
    public AfDecimal getTdrTotManfeeRec() {
        return readPackedAsDecimal(Pos.TDR_TOT_MANFEE_REC, Len.Int.TDR_TOT_MANFEE_REC, Len.Fract.TDR_TOT_MANFEE_REC);
    }

    public byte[] getTdrTotManfeeRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_MANFEE_REC, Pos.TDR_TOT_MANFEE_REC);
        return buffer;
    }

    public void setTdrTotManfeeRecNull(String tdrTotManfeeRecNull) {
        writeString(Pos.TDR_TOT_MANFEE_REC_NULL, tdrTotManfeeRecNull, Len.TDR_TOT_MANFEE_REC_NULL);
    }

    /**Original name: TDR-TOT-MANFEE-REC-NULL<br>*/
    public String getTdrTotManfeeRecNull() {
        return readString(Pos.TDR_TOT_MANFEE_REC_NULL, Len.TDR_TOT_MANFEE_REC_NULL);
    }

    public String getTdrTotManfeeRecNullFormatted() {
        return Functions.padBlanks(getTdrTotManfeeRecNull(), Len.TDR_TOT_MANFEE_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_MANFEE_REC = 1;
        public static final int TDR_TOT_MANFEE_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_MANFEE_REC = 8;
        public static final int TDR_TOT_MANFEE_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_MANFEE_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_MANFEE_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
