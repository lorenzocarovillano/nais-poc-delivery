package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-DT-CONCS-PREST<br>
 * Variable: PRE-DT-CONCS-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreDtConcsPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreDtConcsPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_DT_CONCS_PREST;
    }

    public void setPreDtConcsPrest(int preDtConcsPrest) {
        writeIntAsPacked(Pos.PRE_DT_CONCS_PREST, preDtConcsPrest, Len.Int.PRE_DT_CONCS_PREST);
    }

    public void setPreDtConcsPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_DT_CONCS_PREST, Pos.PRE_DT_CONCS_PREST);
    }

    /**Original name: PRE-DT-CONCS-PREST<br>*/
    public int getPreDtConcsPrest() {
        return readPackedAsInt(Pos.PRE_DT_CONCS_PREST, Len.Int.PRE_DT_CONCS_PREST);
    }

    public byte[] getPreDtConcsPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_DT_CONCS_PREST, Pos.PRE_DT_CONCS_PREST);
        return buffer;
    }

    public void setPreDtConcsPrestNull(String preDtConcsPrestNull) {
        writeString(Pos.PRE_DT_CONCS_PREST_NULL, preDtConcsPrestNull, Len.PRE_DT_CONCS_PREST_NULL);
    }

    /**Original name: PRE-DT-CONCS-PREST-NULL<br>*/
    public String getPreDtConcsPrestNull() {
        return readString(Pos.PRE_DT_CONCS_PREST_NULL, Len.PRE_DT_CONCS_PREST_NULL);
    }

    public String getPreDtConcsPrestNullFormatted() {
        return Functions.padBlanks(getPreDtConcsPrestNull(), Len.PRE_DT_CONCS_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_DT_CONCS_PREST = 1;
        public static final int PRE_DT_CONCS_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_DT_CONCS_PREST = 5;
        public static final int PRE_DT_CONCS_PREST_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_DT_CONCS_PREST = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
