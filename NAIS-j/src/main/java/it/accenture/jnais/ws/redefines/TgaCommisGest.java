package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-COMMIS-GEST<br>
 * Variable: TGA-COMMIS-GEST from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaCommisGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaCommisGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_COMMIS_GEST;
    }

    public void setTgaCommisGest(AfDecimal tgaCommisGest) {
        writeDecimalAsPacked(Pos.TGA_COMMIS_GEST, tgaCommisGest.copy());
    }

    public void setTgaCommisGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_COMMIS_GEST, Pos.TGA_COMMIS_GEST);
    }

    /**Original name: TGA-COMMIS-GEST<br>*/
    public AfDecimal getTgaCommisGest() {
        return readPackedAsDecimal(Pos.TGA_COMMIS_GEST, Len.Int.TGA_COMMIS_GEST, Len.Fract.TGA_COMMIS_GEST);
    }

    public byte[] getTgaCommisGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_COMMIS_GEST, Pos.TGA_COMMIS_GEST);
        return buffer;
    }

    public void setTgaCommisGestNull(String tgaCommisGestNull) {
        writeString(Pos.TGA_COMMIS_GEST_NULL, tgaCommisGestNull, Len.TGA_COMMIS_GEST_NULL);
    }

    /**Original name: TGA-COMMIS-GEST-NULL<br>*/
    public String getTgaCommisGestNull() {
        return readString(Pos.TGA_COMMIS_GEST_NULL, Len.TGA_COMMIS_GEST_NULL);
    }

    public String getTgaCommisGestNullFormatted() {
        return Functions.padBlanks(getTgaCommisGestNull(), Len.TGA_COMMIS_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_COMMIS_GEST = 1;
        public static final int TGA_COMMIS_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_COMMIS_GEST = 8;
        public static final int TGA_COMMIS_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_COMMIS_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
