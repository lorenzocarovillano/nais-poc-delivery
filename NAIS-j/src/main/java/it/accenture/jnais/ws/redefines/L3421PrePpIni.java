package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PRE-PP-INI<br>
 * Variable: L3421-PRE-PP-INI from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PrePpIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PrePpIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PRE_PP_INI;
    }

    public void setL3421PrePpIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PRE_PP_INI, Pos.L3421_PRE_PP_INI);
    }

    /**Original name: L3421-PRE-PP-INI<br>*/
    public AfDecimal getL3421PrePpIni() {
        return readPackedAsDecimal(Pos.L3421_PRE_PP_INI, Len.Int.L3421_PRE_PP_INI, Len.Fract.L3421_PRE_PP_INI);
    }

    public byte[] getL3421PrePpIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PRE_PP_INI, Pos.L3421_PRE_PP_INI);
        return buffer;
    }

    /**Original name: L3421-PRE-PP-INI-NULL<br>*/
    public String getL3421PrePpIniNull() {
        return readString(Pos.L3421_PRE_PP_INI_NULL, Len.L3421_PRE_PP_INI_NULL);
    }

    public String getL3421PrePpIniNullFormatted() {
        return Functions.padBlanks(getL3421PrePpIniNull(), Len.L3421_PRE_PP_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PRE_PP_INI = 1;
        public static final int L3421_PRE_PP_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PRE_PP_INI = 8;
        public static final int L3421_PRE_PP_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PRE_PP_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PRE_PP_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
