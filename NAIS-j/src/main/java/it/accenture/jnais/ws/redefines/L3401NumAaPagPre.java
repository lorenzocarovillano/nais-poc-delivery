package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-NUM-AA-PAG-PRE<br>
 * Variable: L3401-NUM-AA-PAG-PRE from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401NumAaPagPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401NumAaPagPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_NUM_AA_PAG_PRE;
    }

    public void setL3401NumAaPagPre(int l3401NumAaPagPre) {
        writeIntAsPacked(Pos.L3401_NUM_AA_PAG_PRE, l3401NumAaPagPre, Len.Int.L3401_NUM_AA_PAG_PRE);
    }

    public void setL3401NumAaPagPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_NUM_AA_PAG_PRE, Pos.L3401_NUM_AA_PAG_PRE);
    }

    /**Original name: L3401-NUM-AA-PAG-PRE<br>*/
    public int getL3401NumAaPagPre() {
        return readPackedAsInt(Pos.L3401_NUM_AA_PAG_PRE, Len.Int.L3401_NUM_AA_PAG_PRE);
    }

    public byte[] getL3401NumAaPagPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_NUM_AA_PAG_PRE, Pos.L3401_NUM_AA_PAG_PRE);
        return buffer;
    }

    /**Original name: L3401-NUM-AA-PAG-PRE-NULL<br>*/
    public String getL3401NumAaPagPreNull() {
        return readString(Pos.L3401_NUM_AA_PAG_PRE_NULL, Len.L3401_NUM_AA_PAG_PRE_NULL);
    }

    public String getL3401NumAaPagPreNullFormatted() {
        return Functions.padBlanks(getL3401NumAaPagPreNull(), Len.L3401_NUM_AA_PAG_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_NUM_AA_PAG_PRE = 1;
        public static final int L3401_NUM_AA_PAG_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_NUM_AA_PAG_PRE = 3;
        public static final int L3401_NUM_AA_PAG_PRE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_NUM_AA_PAG_PRE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
