package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvmov1;
import it.accenture.jnais.copy.WmovDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WMOV-AREA-MOVIMENTO<br>
 * Variable: WMOV-AREA-MOVIMENTO from program LCCS0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WmovAreaMovimento extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WMOV-ELE-MOV-MAX
    private short wmovEleMovMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVMOV1
    private Lccvmov1 lccvmov1 = new Lccvmov1();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMOV_AREA_MOVIMENTO;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWmovAreaMovimentoBytes(buf);
    }

    public String getWmovAreaMovimentoFormatted() {
        return MarshalByteExt.bufferToStr(getWmovAreaMovimentoBytes());
    }

    public void setWmovAreaMovimentoBytes(byte[] buffer) {
        setWmovAreaMovimentoBytes(buffer, 1);
    }

    public byte[] getWmovAreaMovimentoBytes() {
        byte[] buffer = new byte[Len.WMOV_AREA_MOVIMENTO];
        return getWmovAreaMovimentoBytes(buffer, 1);
    }

    public void setWmovAreaMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        wmovEleMovMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWmovTabMovBytes(buffer, position);
    }

    public byte[] getWmovAreaMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wmovEleMovMax);
        position += Types.SHORT_SIZE;
        getWmovTabMovBytes(buffer, position);
        return buffer;
    }

    public void setWmovEleMovMax(short wmovEleMovMax) {
        this.wmovEleMovMax = wmovEleMovMax;
    }

    public short getWmovEleMovMax() {
        return this.wmovEleMovMax;
    }

    public void setWmovTabMovBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvmov1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvmov1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvmov1.Len.Int.ID_PTF, 0));
        position += Lccvmov1.Len.ID_PTF;
        lccvmov1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWmovTabMovBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvmov1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvmov1.getIdPtf(), Lccvmov1.Len.Int.ID_PTF, 0);
        position += Lccvmov1.Len.ID_PTF;
        lccvmov1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public Lccvmov1 getLccvmov1() {
        return lccvmov1;
    }

    @Override
    public byte[] serialize() {
        return getWmovAreaMovimentoBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WMOV_ELE_MOV_MAX = 2;
        public static final int WMOV_TAB_MOV = WpolStatus.Len.STATUS + Lccvmov1.Len.ID_PTF + WmovDati.Len.DATI;
        public static final int WMOV_AREA_MOVIMENTO = WMOV_ELE_MOV_MAX + WMOV_TAB_MOV;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
