package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0211-TAB-INFO1<br>
 * Variable: IVVC0211-TAB-INFO1 from program IVVS0211<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ivvc0211TabInfo1 extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_INFO_MAXOCCURS = 100;

    //==== CONSTRUCTORS ====
    public Ivvc0211TabInfo1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IVVC0211_TAB_INFO1;
    }

    public String getS211TabInfo1Formatted() {
        return readFixedString(Pos.IVVC0211_TAB_INFO1, Len.IVVC0211_TAB_INFO1);
    }

    public void setIvvc0211TabInfo1Bytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.IVVC0211_TAB_INFO1, Pos.IVVC0211_TAB_INFO1);
    }

    public byte[] getIvvc0211TabInfo1Bytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.IVVC0211_TAB_INFO1, Pos.IVVC0211_TAB_INFO1);
        return buffer;
    }

    public void setS211TabAlias(int s211TabAliasIdx, String s211TabAlias) {
        int position = Pos.ivvc0211TabAlias(s211TabAliasIdx - 1);
        writeString(position, s211TabAlias, Len.TAB_ALIAS);
    }

    /**Original name: IVVC0211-TAB-ALIAS<br>*/
    public String getTabAlias(int tabAliasIdx) {
        int position = Pos.ivvc0211TabAlias(tabAliasIdx - 1);
        return readString(position, Len.TAB_ALIAS);
    }

    public String getTabAliasFormatted(int tabAliasIdx) {
        return Functions.padBlanks(getTabAlias(tabAliasIdx), Len.TAB_ALIAS);
    }

    public void setS211NumEleTabAlias(int s211NumEleTabAliasIdx, int s211NumEleTabAlias) {
        int position = Pos.ivvc0211NumEleTabAlias(s211NumEleTabAliasIdx - 1);
        writeIntAsPacked(position, s211NumEleTabAlias, Len.Int.S211_NUM_ELE_TAB_ALIAS);
    }

    /**Original name: S211-NUM-ELE-TAB-ALIAS<br>*/
    public int getS211NumEleTabAlias(int s211NumEleTabAliasIdx) {
        int position = Pos.ivvc0211NumEleTabAlias(s211NumEleTabAliasIdx - 1);
        return readPackedAsInt(position, Len.Int.S211_NUM_ELE_TAB_ALIAS);
    }

    public void setS211PosizIni(int s211PosizIniIdx, int s211PosizIni) {
        int position = Pos.ivvc0211PosizIni(s211PosizIniIdx - 1);
        writeIntAsPacked(position, s211PosizIni, Len.Int.POSIZ_INI);
    }

    /**Original name: IVVC0211-POSIZ-INI<br>*/
    public int getPosizIni(int posizIniIdx) {
        int position = Pos.ivvc0211PosizIni(posizIniIdx - 1);
        return readPackedAsInt(position, Len.Int.POSIZ_INI);
    }

    public void setS211Lunghezza(int s211LunghezzaIdx, int s211Lunghezza) {
        int position = Pos.ivvc0211Lunghezza(s211LunghezzaIdx - 1);
        writeIntAsPacked(position, s211Lunghezza, Len.Int.LUNGHEZZA);
    }

    /**Original name: IVVC0211-LUNGHEZZA<br>*/
    public int getLunghezza(int lunghezzaIdx) {
        int position = Pos.ivvc0211Lunghezza(lunghezzaIdx - 1);
        return readPackedAsInt(position, Len.Int.LUNGHEZZA);
    }

    public void setS211RestoTabInfo1(String s211RestoTabInfo1) {
        writeString(Pos.RESTO_TAB_INFO1, s211RestoTabInfo1, Len.S211_RESTO_TAB_INFO1);
    }

    /**Original name: S211-RESTO-TAB-INFO1<br>*/
    public String getS211RestoTabInfo1() {
        return readString(Pos.RESTO_TAB_INFO1, Len.S211_RESTO_TAB_INFO1);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int IVVC0211_TAB_INFO1 = 1;
        public static final int IVVC0211_TAB_INFO1_R = 1;
        public static final int FLR1 = IVVC0211_TAB_INFO1_R;
        public static final int RESTO_TAB_INFO1 = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int ivvc0211TabInfo(int idx) {
            return IVVC0211_TAB_INFO1 + idx * Len.TAB_INFO;
        }

        public static int ivvc0211TabAlias(int idx) {
            return ivvc0211TabInfo(idx);
        }

        public static int ivvc0211NumEleTabAlias(int idx) {
            return ivvc0211TabAlias(idx) + Len.TAB_ALIAS;
        }

        public static int ivvc0211PosizIni(int idx) {
            return ivvc0211NumEleTabAlias(idx) + Len.NUM_ELE_TAB_ALIAS;
        }

        public static int ivvc0211Lunghezza(int idx) {
            return ivvc0211PosizIni(idx) + Len.POSIZ_INI;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TAB_ALIAS = 3;
        public static final int NUM_ELE_TAB_ALIAS = 3;
        public static final int POSIZ_INI = 5;
        public static final int LUNGHEZZA = 5;
        public static final int TAB_INFO = TAB_ALIAS + NUM_ELE_TAB_ALIAS + POSIZ_INI + LUNGHEZZA;
        public static final int FLR1 = 16;
        public static final int IVVC0211_TAB_INFO1 = Ivvc0211TabInfo1.TAB_INFO_MAXOCCURS * TAB_INFO;
        public static final int S211_RESTO_TAB_INFO1 = 1584;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int S211_NUM_ELE_TAB_ALIAS = 5;
            public static final int POSIZ_INI = 9;
            public static final int LUNGHEZZA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
