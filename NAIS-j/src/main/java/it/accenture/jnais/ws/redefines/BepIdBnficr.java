package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEP-ID-BNFICR<br>
 * Variable: BEP-ID-BNFICR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BepIdBnficr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BepIdBnficr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEP_ID_BNFICR;
    }

    public void setBepIdBnficr(int bepIdBnficr) {
        writeIntAsPacked(Pos.BEP_ID_BNFICR, bepIdBnficr, Len.Int.BEP_ID_BNFICR);
    }

    public void setBepIdBnficrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEP_ID_BNFICR, Pos.BEP_ID_BNFICR);
    }

    /**Original name: BEP-ID-BNFICR<br>*/
    public int getBepIdBnficr() {
        return readPackedAsInt(Pos.BEP_ID_BNFICR, Len.Int.BEP_ID_BNFICR);
    }

    public byte[] getBepIdBnficrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEP_ID_BNFICR, Pos.BEP_ID_BNFICR);
        return buffer;
    }

    public void setBepIdBnficrNull(String bepIdBnficrNull) {
        writeString(Pos.BEP_ID_BNFICR_NULL, bepIdBnficrNull, Len.BEP_ID_BNFICR_NULL);
    }

    /**Original name: BEP-ID-BNFICR-NULL<br>*/
    public String getBepIdBnficrNull() {
        return readString(Pos.BEP_ID_BNFICR_NULL, Len.BEP_ID_BNFICR_NULL);
    }

    public String getBepIdBnficrNullFormatted() {
        return Functions.padBlanks(getBepIdBnficrNull(), Len.BEP_ID_BNFICR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEP_ID_BNFICR = 1;
        public static final int BEP_ID_BNFICR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEP_ID_BNFICR = 5;
        public static final int BEP_ID_BNFICR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEP_ID_BNFICR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
