package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-LIVELLO-GEN-TIT<br>
 * Variable: WCOM-LIVELLO-GEN-TIT from copybook LCCC0261<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomLivelloGenTit {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.LIVELLO_GEN_TIT);
    public static final String UNICO = "PO";
    public static final String SEPARATO = "AD";

    //==== METHODS ====
    public void setLivelloGenTit(String livelloGenTit) {
        this.value = Functions.subString(livelloGenTit, Len.LIVELLO_GEN_TIT);
    }

    public String getLivelloGenTit() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LIVELLO_GEN_TIT = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
