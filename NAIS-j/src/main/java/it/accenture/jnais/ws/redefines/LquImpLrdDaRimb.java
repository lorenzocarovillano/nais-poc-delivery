package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-LRD-DA-RIMB<br>
 * Variable: LQU-IMP-LRD-DA-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpLrdDaRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpLrdDaRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_LRD_DA_RIMB;
    }

    public void setLquImpLrdDaRimb(AfDecimal lquImpLrdDaRimb) {
        writeDecimalAsPacked(Pos.LQU_IMP_LRD_DA_RIMB, lquImpLrdDaRimb.copy());
    }

    public void setLquImpLrdDaRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_LRD_DA_RIMB, Pos.LQU_IMP_LRD_DA_RIMB);
    }

    /**Original name: LQU-IMP-LRD-DA-RIMB<br>*/
    public AfDecimal getLquImpLrdDaRimb() {
        return readPackedAsDecimal(Pos.LQU_IMP_LRD_DA_RIMB, Len.Int.LQU_IMP_LRD_DA_RIMB, Len.Fract.LQU_IMP_LRD_DA_RIMB);
    }

    public byte[] getLquImpLrdDaRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_LRD_DA_RIMB, Pos.LQU_IMP_LRD_DA_RIMB);
        return buffer;
    }

    public void setLquImpLrdDaRimbNull(String lquImpLrdDaRimbNull) {
        writeString(Pos.LQU_IMP_LRD_DA_RIMB_NULL, lquImpLrdDaRimbNull, Len.LQU_IMP_LRD_DA_RIMB_NULL);
    }

    /**Original name: LQU-IMP-LRD-DA-RIMB-NULL<br>*/
    public String getLquImpLrdDaRimbNull() {
        return readString(Pos.LQU_IMP_LRD_DA_RIMB_NULL, Len.LQU_IMP_LRD_DA_RIMB_NULL);
    }

    public String getLquImpLrdDaRimbNullFormatted() {
        return Functions.padBlanks(getLquImpLrdDaRimbNull(), Len.LQU_IMP_LRD_DA_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_LRD_DA_RIMB = 1;
        public static final int LQU_IMP_LRD_DA_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_LRD_DA_RIMB = 8;
        public static final int LQU_IMP_LRD_DA_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_LRD_DA_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_LRD_DA_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
