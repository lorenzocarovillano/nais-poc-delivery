package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-TAX<br>
 * Variable: WDTC-TAX from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_TAX;
    }

    public void setWdtcTax(AfDecimal wdtcTax) {
        writeDecimalAsPacked(Pos.WDTC_TAX, wdtcTax.copy());
    }

    public void setWdtcTaxFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_TAX, Pos.WDTC_TAX);
    }

    /**Original name: WDTC-TAX<br>*/
    public AfDecimal getWdtcTax() {
        return readPackedAsDecimal(Pos.WDTC_TAX, Len.Int.WDTC_TAX, Len.Fract.WDTC_TAX);
    }

    public byte[] getWdtcTaxAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_TAX, Pos.WDTC_TAX);
        return buffer;
    }

    public void initWdtcTaxSpaces() {
        fill(Pos.WDTC_TAX, Len.WDTC_TAX, Types.SPACE_CHAR);
    }

    public void setWdtcTaxNull(String wdtcTaxNull) {
        writeString(Pos.WDTC_TAX_NULL, wdtcTaxNull, Len.WDTC_TAX_NULL);
    }

    /**Original name: WDTC-TAX-NULL<br>*/
    public String getWdtcTaxNull() {
        return readString(Pos.WDTC_TAX_NULL, Len.WDTC_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_TAX = 1;
        public static final int WDTC_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_TAX = 8;
        public static final int WDTC_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
