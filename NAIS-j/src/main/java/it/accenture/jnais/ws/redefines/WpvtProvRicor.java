package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPVT-PROV-RICOR<br>
 * Variable: WPVT-PROV-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_PROV_RICOR;
    }

    public void setWpvtProvRicor(AfDecimal wpvtProvRicor) {
        writeDecimalAsPacked(Pos.WPVT_PROV_RICOR, wpvtProvRicor.copy());
    }

    public void setWpvtProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_PROV_RICOR, Pos.WPVT_PROV_RICOR);
    }

    /**Original name: WPVT-PROV-RICOR<br>*/
    public AfDecimal getWpvtProvRicor() {
        return readPackedAsDecimal(Pos.WPVT_PROV_RICOR, Len.Int.WPVT_PROV_RICOR, Len.Fract.WPVT_PROV_RICOR);
    }

    public byte[] getWpvtProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_PROV_RICOR, Pos.WPVT_PROV_RICOR);
        return buffer;
    }

    public void initWpvtProvRicorSpaces() {
        fill(Pos.WPVT_PROV_RICOR, Len.WPVT_PROV_RICOR, Types.SPACE_CHAR);
    }

    public void setWpvtProvRicorNull(String wpvtProvRicorNull) {
        writeString(Pos.WPVT_PROV_RICOR_NULL, wpvtProvRicorNull, Len.WPVT_PROV_RICOR_NULL);
    }

    /**Original name: WPVT-PROV-RICOR-NULL<br>*/
    public String getWpvtProvRicorNull() {
        return readString(Pos.WPVT_PROV_RICOR_NULL, Len.WPVT_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_PROV_RICOR = 1;
        public static final int WPVT_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_PROV_RICOR = 8;
        public static final int WPVT_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPVT_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
