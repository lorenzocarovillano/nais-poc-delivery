package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P58-ID-MOVI-CHIU<br>
 * Variable: P58-ID-MOVI-CHIU from program IDBSP580<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P58IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P58IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P58_ID_MOVI_CHIU;
    }

    public void setP58IdMoviChiu(int p58IdMoviChiu) {
        writeIntAsPacked(Pos.P58_ID_MOVI_CHIU, p58IdMoviChiu, Len.Int.P58_ID_MOVI_CHIU);
    }

    public void setP58IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P58_ID_MOVI_CHIU, Pos.P58_ID_MOVI_CHIU);
    }

    /**Original name: P58-ID-MOVI-CHIU<br>*/
    public int getP58IdMoviChiu() {
        return readPackedAsInt(Pos.P58_ID_MOVI_CHIU, Len.Int.P58_ID_MOVI_CHIU);
    }

    public byte[] getP58IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P58_ID_MOVI_CHIU, Pos.P58_ID_MOVI_CHIU);
        return buffer;
    }

    public void setP58IdMoviChiuNull(String p58IdMoviChiuNull) {
        writeString(Pos.P58_ID_MOVI_CHIU_NULL, p58IdMoviChiuNull, Len.P58_ID_MOVI_CHIU_NULL);
    }

    /**Original name: P58-ID-MOVI-CHIU-NULL<br>*/
    public String getP58IdMoviChiuNull() {
        return readString(Pos.P58_ID_MOVI_CHIU_NULL, Len.P58_ID_MOVI_CHIU_NULL);
    }

    public String getP58IdMoviChiuNullFormatted() {
        return Functions.padBlanks(getP58IdMoviChiuNull(), Len.P58_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P58_ID_MOVI_CHIU = 1;
        public static final int P58_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P58_ID_MOVI_CHIU = 5;
        public static final int P58_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P58_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
