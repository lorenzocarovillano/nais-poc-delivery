package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.commons.data.to.ITemporaryData;
import it.accenture.jnais.copy.IndAnagDato;
import it.accenture.jnais.copy.TemporaryData;
import it.accenture.jnais.ws.enums.TemporaryDataTrovati;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDSS0300<br>
 * Generated as a class for rule WS.<br>*/
public class Idss0300Data implements ITemporaryData {

    //==== PROPERTIES ====
    //Original name: WK-DATA-PACKAGE-MAX
    private long wkDataPackageMax = DefaultValues.BIN_LONG_VAL;
    //Original name: WK-LEN-PACKAGES-NO-STD
    private long wkLenPackagesNoStd = DefaultValues.BIN_LONG_VAL;
    //Original name: WK-LEN-TRANS-DATA
    private int wkLenTransData = DefaultValues.BIN_INT_VAL;
    //Original name: WK-TOT-NUM-FRAMES
    private short wkTotNumFrames = DefaultValues.BIN_SHORT_VAL;
    //Original name: WK-REMAINDER-PACKAGE
    private int wkRemainderPackage = DefaultValues.BIN_INT_VAL;
    //Original name: WK-START-POSITION
    private int wkStartPosition = DefaultValues.INT_VAL;
    //Original name: WK-IND-FRAME
    private short wkIndFrame = DefaultValues.BIN_SHORT_VAL;
    //Original name: NUM-FRAME-APPEND
    private short numFrameAppend = DefaultValues.BIN_SHORT_VAL;
    //Original name: TEMPORARY-DATA-TROVATI
    private TemporaryDataTrovati temporaryDataTrovati = new TemporaryDataTrovati();
    //Original name: TEMPORARY-DATA
    private TemporaryData temporaryData = new TemporaryData();
    //Original name: IND-TEMPORARY-DATA
    private IndAnagDato indTemporaryData = new IndAnagDato();

    //==== METHODS ====
    public void setWkDataPackageMax(long wkDataPackageMax) {
        this.wkDataPackageMax = wkDataPackageMax;
    }

    public long getWkDataPackageMax() {
        return this.wkDataPackageMax;
    }

    public void setWkLenPackagesNoStd(long wkLenPackagesNoStd) {
        this.wkLenPackagesNoStd = wkLenPackagesNoStd;
    }

    public long getWkLenPackagesNoStd() {
        return this.wkLenPackagesNoStd;
    }

    public void setWkLenTransData(int wkLenTransData) {
        this.wkLenTransData = wkLenTransData;
    }

    public int getWkLenTransData() {
        return this.wkLenTransData;
    }

    public void setWkTotNumFrames(short wkTotNumFrames) {
        this.wkTotNumFrames = wkTotNumFrames;
    }

    public short getWkTotNumFrames() {
        return this.wkTotNumFrames;
    }

    public void setWkRemainderPackage(int wkRemainderPackage) {
        this.wkRemainderPackage = wkRemainderPackage;
    }

    public int getWkRemainderPackage() {
        return this.wkRemainderPackage;
    }

    public void setWkStartPosition(int wkStartPosition) {
        this.wkStartPosition = wkStartPosition;
    }

    public int getWkStartPosition() {
        return this.wkStartPosition;
    }

    public void setWkIndFrame(short wkIndFrame) {
        this.wkIndFrame = wkIndFrame;
    }

    public short getWkIndFrame() {
        return this.wkIndFrame;
    }

    public void setNumFrameAppend(short numFrameAppend) {
        this.numFrameAppend = numFrameAppend;
    }

    public short getNumFrameAppend() {
        return this.numFrameAppend;
    }

    @Override
    public int getActualRecurrence() {
        return temporaryData.getActualRecurrence();
    }

    @Override
    public void setActualRecurrence(int actualRecurrence) {
        this.temporaryData.setActualRecurrence(actualRecurrence);
    }

    @Override
    public Integer getActualRecurrenceObj() {
        if (indTemporaryData.getPrecisioneDato() >= 0) {
            return ((Integer)getActualRecurrence());
        }
        else {
            return null;
        }
    }

    @Override
    public void setActualRecurrenceObj(Integer actualRecurrenceObj) {
        if (actualRecurrenceObj != null) {
            setActualRecurrence(((int)actualRecurrenceObj));
            indTemporaryData.setPrecisioneDato(((short)0));
        }
        else {
            indTemporaryData.setPrecisioneDato(((short)-1));
        }
    }

    @Override
    public String getAliasStrDato() {
        return temporaryData.getAliasStrDato();
    }

    @Override
    public void setAliasStrDato(String aliasStrDato) {
        this.temporaryData.setAliasStrDato(aliasStrDato);
    }

    @Override
    public String getBufferDataVchar() {
        return temporaryData.getBufferDataVcharFormatted();
    }

    @Override
    public void setBufferDataVchar(String bufferDataVchar) {
        this.temporaryData.setBufferDataVcharFormatted(bufferDataVchar);
    }

    @Override
    public String getBufferDataVcharObj() {
        if (indTemporaryData.getFormattazioneDato() >= 0) {
            return getBufferDataVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setBufferDataVcharObj(String bufferDataVcharObj) {
        if (bufferDataVcharObj != null) {
            setBufferDataVchar(bufferDataVcharObj);
            indTemporaryData.setFormattazioneDato(((short)0));
        }
        else {
            indTemporaryData.setFormattazioneDato(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return temporaryData.getCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.temporaryData.setCodCompAnia(codCompAnia);
    }

    @Override
    public char getDsOperSql() {
        return temporaryData.getDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.temporaryData.setDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return temporaryData.getDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.temporaryData.setDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return temporaryData.getDsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.temporaryData.setDsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return temporaryData.getDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.temporaryData.setDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return temporaryData.getDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.temporaryData.setDsVer(dsVer);
    }

    @Override
    public char getFlContiguousData() {
        return temporaryData.getFlContiguousData();
    }

    @Override
    public void setFlContiguousData(char flContiguousData) {
        this.temporaryData.setFlContiguousData(flContiguousData);
    }

    @Override
    public Character getFlContiguousDataObj() {
        if (indTemporaryData.getDescDato() >= 0) {
            return ((Character)getFlContiguousData());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlContiguousDataObj(Character flContiguousDataObj) {
        if (flContiguousDataObj != null) {
            setFlContiguousData(((char)flContiguousDataObj));
            indTemporaryData.setFlContiguousData(((short)0));
        }
        else {
            indTemporaryData.setFlContiguousData(((short)-1));
        }
    }

    @Override
    public String getIdSession() {
        return temporaryData.getIdSession();
    }

    @Override
    public void setIdSession(String idSession) {
        this.temporaryData.setIdSession(idSession);
    }

    @Override
    public long getIdTemporaryData() {
        return temporaryData.getIdTemporaryData();
    }

    @Override
    public void setIdTemporaryData(long idTemporaryData) {
        this.temporaryData.setIdTemporaryData(idTemporaryData);
    }

    @Override
    public int getNumFrame() {
        return temporaryData.getNumFrame();
    }

    @Override
    public void setNumFrame(int numFrame) {
        this.temporaryData.setNumFrame(numFrame);
    }

    @Override
    public int getPartialRecurrence() {
        return temporaryData.getPartialRecurrence();
    }

    @Override
    public void setPartialRecurrence(int partialRecurrence) {
        this.temporaryData.setPartialRecurrence(partialRecurrence);
    }

    @Override
    public Integer getPartialRecurrenceObj() {
        if (indTemporaryData.getLunghezzaDato() >= 0) {
            return ((Integer)getPartialRecurrence());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPartialRecurrenceObj(Integer partialRecurrenceObj) {
        if (partialRecurrenceObj != null) {
            setPartialRecurrence(((int)partialRecurrenceObj));
            indTemporaryData.setLunghezzaDato(((short)0));
        }
        else {
            indTemporaryData.setLunghezzaDato(((short)-1));
        }
    }

    public TemporaryData getTemporaryData() {
        return temporaryData;
    }

    public TemporaryDataTrovati getTemporaryDataTrovati() {
        return temporaryDataTrovati;
    }

    @Override
    public int getTotalRecurrence() {
        return temporaryData.getTotalRecurrence();
    }

    @Override
    public void setTotalRecurrence(int totalRecurrence) {
        this.temporaryData.setTotalRecurrence(totalRecurrence);
    }

    @Override
    public Integer getTotalRecurrenceObj() {
        if (indTemporaryData.getTipoDato() >= 0) {
            return ((Integer)getTotalRecurrence());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotalRecurrenceObj(Integer totalRecurrenceObj) {
        if (totalRecurrenceObj != null) {
            setTotalRecurrence(((int)totalRecurrenceObj));
            indTemporaryData.setTipoDato(((short)0));
        }
        else {
            indTemporaryData.setTipoDato(((short)-1));
        }
    }

    @Override
    public String getTypeRecord() {
        return temporaryData.getTypeRecord();
    }

    @Override
    public void setTypeRecord(String typeRecord) {
        this.temporaryData.setTypeRecord(typeRecord);
    }

    @Override
    public String getTypeRecordObj() {
        if (indTemporaryData.getCodDominio() >= 0) {
            return getTypeRecord();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTypeRecordObj(String typeRecordObj) {
        if (typeRecordObj != null) {
            setTypeRecord(typeRecordObj);
            indTemporaryData.setTypeRecord(((short)0));
        }
        else {
            indTemporaryData.setTypeRecord(((short)-1));
        }
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_START_POSITION = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
