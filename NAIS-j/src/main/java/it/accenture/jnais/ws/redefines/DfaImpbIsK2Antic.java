package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPB-IS-K2-ANTIC<br>
 * Variable: DFA-IMPB-IS-K2-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpbIsK2Antic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpbIsK2Antic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPB_IS_K2_ANTIC;
    }

    public void setDfaImpbIsK2Antic(AfDecimal dfaImpbIsK2Antic) {
        writeDecimalAsPacked(Pos.DFA_IMPB_IS_K2_ANTIC, dfaImpbIsK2Antic.copy());
    }

    public void setDfaImpbIsK2AnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPB_IS_K2_ANTIC, Pos.DFA_IMPB_IS_K2_ANTIC);
    }

    /**Original name: DFA-IMPB-IS-K2-ANTIC<br>*/
    public AfDecimal getDfaImpbIsK2Antic() {
        return readPackedAsDecimal(Pos.DFA_IMPB_IS_K2_ANTIC, Len.Int.DFA_IMPB_IS_K2_ANTIC, Len.Fract.DFA_IMPB_IS_K2_ANTIC);
    }

    public byte[] getDfaImpbIsK2AnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPB_IS_K2_ANTIC, Pos.DFA_IMPB_IS_K2_ANTIC);
        return buffer;
    }

    public void setDfaImpbIsK2AnticNull(String dfaImpbIsK2AnticNull) {
        writeString(Pos.DFA_IMPB_IS_K2_ANTIC_NULL, dfaImpbIsK2AnticNull, Len.DFA_IMPB_IS_K2_ANTIC_NULL);
    }

    /**Original name: DFA-IMPB-IS-K2-ANTIC-NULL<br>*/
    public String getDfaImpbIsK2AnticNull() {
        return readString(Pos.DFA_IMPB_IS_K2_ANTIC_NULL, Len.DFA_IMPB_IS_K2_ANTIC_NULL);
    }

    public String getDfaImpbIsK2AnticNullFormatted() {
        return Functions.padBlanks(getDfaImpbIsK2AnticNull(), Len.DFA_IMPB_IS_K2_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IS_K2_ANTIC = 1;
        public static final int DFA_IMPB_IS_K2_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IS_K2_ANTIC = 8;
        public static final int DFA_IMPB_IS_K2_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IS_K2_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IS_K2_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
