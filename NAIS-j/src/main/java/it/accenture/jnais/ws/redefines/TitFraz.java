package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-FRAZ<br>
 * Variable: TIT-FRAZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_FRAZ;
    }

    public void setTitFraz(int titFraz) {
        writeIntAsPacked(Pos.TIT_FRAZ, titFraz, Len.Int.TIT_FRAZ);
    }

    public void setTitFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_FRAZ, Pos.TIT_FRAZ);
    }

    /**Original name: TIT-FRAZ<br>*/
    public int getTitFraz() {
        return readPackedAsInt(Pos.TIT_FRAZ, Len.Int.TIT_FRAZ);
    }

    public byte[] getTitFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_FRAZ, Pos.TIT_FRAZ);
        return buffer;
    }

    public void setTitFrazNull(String titFrazNull) {
        writeString(Pos.TIT_FRAZ_NULL, titFrazNull, Len.TIT_FRAZ_NULL);
    }

    /**Original name: TIT-FRAZ-NULL<br>*/
    public String getTitFrazNull() {
        return readString(Pos.TIT_FRAZ_NULL, Len.TIT_FRAZ_NULL);
    }

    public String getTitFrazNullFormatted() {
        return Functions.padBlanks(getTitFrazNull(), Len.TIT_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_FRAZ = 1;
        public static final int TIT_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_FRAZ = 3;
        public static final int TIT_FRAZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_FRAZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
