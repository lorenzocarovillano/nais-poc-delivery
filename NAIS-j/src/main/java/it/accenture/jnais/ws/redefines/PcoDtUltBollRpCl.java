package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-RP-CL<br>
 * Variable: PCO-DT-ULT-BOLL-RP-CL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollRpCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollRpCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_RP_CL;
    }

    public void setPcoDtUltBollRpCl(int pcoDtUltBollRpCl) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_RP_CL, pcoDtUltBollRpCl, Len.Int.PCO_DT_ULT_BOLL_RP_CL);
    }

    public void setPcoDtUltBollRpClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RP_CL, Pos.PCO_DT_ULT_BOLL_RP_CL);
    }

    /**Original name: PCO-DT-ULT-BOLL-RP-CL<br>*/
    public int getPcoDtUltBollRpCl() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_RP_CL, Len.Int.PCO_DT_ULT_BOLL_RP_CL);
    }

    public byte[] getPcoDtUltBollRpClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RP_CL, Pos.PCO_DT_ULT_BOLL_RP_CL);
        return buffer;
    }

    public void initPcoDtUltBollRpClHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_RP_CL, Len.PCO_DT_ULT_BOLL_RP_CL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollRpClNull(String pcoDtUltBollRpClNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_RP_CL_NULL, pcoDtUltBollRpClNull, Len.PCO_DT_ULT_BOLL_RP_CL_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-RP-CL-NULL<br>*/
    public String getPcoDtUltBollRpClNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_RP_CL_NULL, Len.PCO_DT_ULT_BOLL_RP_CL_NULL);
    }

    public String getPcoDtUltBollRpClNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollRpClNull(), Len.PCO_DT_ULT_BOLL_RP_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RP_CL = 1;
        public static final int PCO_DT_ULT_BOLL_RP_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RP_CL = 5;
        public static final int PCO_DT_ULT_BOLL_RP_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_RP_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
