package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-IMP-REC-RIT-VIS<br>
 * Variable: WADE-IMP-REC-RIT-VIS from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeImpRecRitVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeImpRecRitVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_IMP_REC_RIT_VIS;
    }

    public void setWadeImpRecRitVis(AfDecimal wadeImpRecRitVis) {
        writeDecimalAsPacked(Pos.WADE_IMP_REC_RIT_VIS, wadeImpRecRitVis.copy());
    }

    public void setWadeImpRecRitVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_IMP_REC_RIT_VIS, Pos.WADE_IMP_REC_RIT_VIS);
    }

    /**Original name: WADE-IMP-REC-RIT-VIS<br>*/
    public AfDecimal getWadeImpRecRitVis() {
        return readPackedAsDecimal(Pos.WADE_IMP_REC_RIT_VIS, Len.Int.WADE_IMP_REC_RIT_VIS, Len.Fract.WADE_IMP_REC_RIT_VIS);
    }

    public byte[] getWadeImpRecRitVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_IMP_REC_RIT_VIS, Pos.WADE_IMP_REC_RIT_VIS);
        return buffer;
    }

    public void initWadeImpRecRitVisSpaces() {
        fill(Pos.WADE_IMP_REC_RIT_VIS, Len.WADE_IMP_REC_RIT_VIS, Types.SPACE_CHAR);
    }

    public void setWadeImpRecRitVisNull(String wadeImpRecRitVisNull) {
        writeString(Pos.WADE_IMP_REC_RIT_VIS_NULL, wadeImpRecRitVisNull, Len.WADE_IMP_REC_RIT_VIS_NULL);
    }

    /**Original name: WADE-IMP-REC-RIT-VIS-NULL<br>*/
    public String getWadeImpRecRitVisNull() {
        return readString(Pos.WADE_IMP_REC_RIT_VIS_NULL, Len.WADE_IMP_REC_RIT_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_IMP_REC_RIT_VIS = 1;
        public static final int WADE_IMP_REC_RIT_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_IMP_REC_RIT_VIS = 8;
        public static final int WADE_IMP_REC_RIT_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_IMP_REC_RIT_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_IMP_REC_RIT_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
