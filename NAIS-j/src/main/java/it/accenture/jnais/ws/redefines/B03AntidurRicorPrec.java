package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ANTIDUR-RICOR-PREC<br>
 * Variable: B03-ANTIDUR-RICOR-PREC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03AntidurRicorPrec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03AntidurRicorPrec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ANTIDUR_RICOR_PREC;
    }

    public void setB03AntidurRicorPrec(int b03AntidurRicorPrec) {
        writeIntAsPacked(Pos.B03_ANTIDUR_RICOR_PREC, b03AntidurRicorPrec, Len.Int.B03_ANTIDUR_RICOR_PREC);
    }

    public void setB03AntidurRicorPrecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ANTIDUR_RICOR_PREC, Pos.B03_ANTIDUR_RICOR_PREC);
    }

    /**Original name: B03-ANTIDUR-RICOR-PREC<br>*/
    public int getB03AntidurRicorPrec() {
        return readPackedAsInt(Pos.B03_ANTIDUR_RICOR_PREC, Len.Int.B03_ANTIDUR_RICOR_PREC);
    }

    public byte[] getB03AntidurRicorPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ANTIDUR_RICOR_PREC, Pos.B03_ANTIDUR_RICOR_PREC);
        return buffer;
    }

    public void setB03AntidurRicorPrecNull(String b03AntidurRicorPrecNull) {
        writeString(Pos.B03_ANTIDUR_RICOR_PREC_NULL, b03AntidurRicorPrecNull, Len.B03_ANTIDUR_RICOR_PREC_NULL);
    }

    /**Original name: B03-ANTIDUR-RICOR-PREC-NULL<br>*/
    public String getB03AntidurRicorPrecNull() {
        return readString(Pos.B03_ANTIDUR_RICOR_PREC_NULL, Len.B03_ANTIDUR_RICOR_PREC_NULL);
    }

    public String getB03AntidurRicorPrecNullFormatted() {
        return Functions.padBlanks(getB03AntidurRicorPrecNull(), Len.B03_ANTIDUR_RICOR_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ANTIDUR_RICOR_PREC = 1;
        public static final int B03_ANTIDUR_RICOR_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ANTIDUR_RICOR_PREC = 3;
        public static final int B03_ANTIDUR_RICOR_PREC_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ANTIDUR_RICOR_PREC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
