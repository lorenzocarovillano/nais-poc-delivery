package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.P58IdMoviChiu;
import it.accenture.jnais.ws.redefines.P58ImpstBolloDettV;
import it.accenture.jnais.ws.redefines.P58ImpstBolloTotV;

/**Original name: IMPST-BOLLO<br>
 * Variable: IMPST-BOLLO from copybook IDBVP581<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ImpstBollo extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: P58-ID-IMPST-BOLLO
    private int p58IdImpstBollo = DefaultValues.INT_VAL;
    //Original name: P58-COD-COMP-ANIA
    private int p58CodCompAnia = DefaultValues.INT_VAL;
    //Original name: P58-ID-POLI
    private int p58IdPoli = DefaultValues.INT_VAL;
    //Original name: P58-IB-POLI
    private String p58IbPoli = DefaultValues.stringVal(Len.P58_IB_POLI);
    //Original name: P58-COD-FISC
    private String p58CodFisc = DefaultValues.stringVal(Len.P58_COD_FISC);
    //Original name: P58-COD-PART-IVA
    private String p58CodPartIva = DefaultValues.stringVal(Len.P58_COD_PART_IVA);
    //Original name: P58-ID-RAPP-ANA
    private int p58IdRappAna = DefaultValues.INT_VAL;
    //Original name: P58-ID-MOVI-CRZ
    private int p58IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: P58-ID-MOVI-CHIU
    private P58IdMoviChiu p58IdMoviChiu = new P58IdMoviChiu();
    //Original name: P58-DT-INI-EFF
    private int p58DtIniEff = DefaultValues.INT_VAL;
    //Original name: P58-DT-END-EFF
    private int p58DtEndEff = DefaultValues.INT_VAL;
    //Original name: P58-DT-INI-CALC
    private int p58DtIniCalc = DefaultValues.INT_VAL;
    //Original name: P58-DT-END-CALC
    private int p58DtEndCalc = DefaultValues.INT_VAL;
    //Original name: P58-TP-CAUS-BOLLO
    private String p58TpCausBollo = DefaultValues.stringVal(Len.P58_TP_CAUS_BOLLO);
    //Original name: P58-IMPST-BOLLO-DETT-C
    private AfDecimal p58ImpstBolloDettC = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: P58-IMPST-BOLLO-DETT-V
    private P58ImpstBolloDettV p58ImpstBolloDettV = new P58ImpstBolloDettV();
    //Original name: P58-IMPST-BOLLO-TOT-V
    private P58ImpstBolloTotV p58ImpstBolloTotV = new P58ImpstBolloTotV();
    //Original name: P58-IMPST-BOLLO-TOT-R
    private AfDecimal p58ImpstBolloTotR = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: P58-DS-RIGA
    private long p58DsRiga = DefaultValues.LONG_VAL;
    //Original name: P58-DS-OPER-SQL
    private char p58DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P58-DS-VER
    private int p58DsVer = DefaultValues.INT_VAL;
    //Original name: P58-DS-TS-INI-CPTZ
    private long p58DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: P58-DS-TS-END-CPTZ
    private long p58DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: P58-DS-UTENTE
    private String p58DsUtente = DefaultValues.stringVal(Len.P58_DS_UTENTE);
    //Original name: P58-DS-STATO-ELAB
    private char p58DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IMPST_BOLLO;
    }

    @Override
    public void deserialize(byte[] buf) {
        setImpstBolloBytes(buf);
    }

    public void setImpstBolloFormatted(String data) {
        byte[] buffer = new byte[Len.IMPST_BOLLO];
        MarshalByte.writeString(buffer, 1, data, Len.IMPST_BOLLO);
        setImpstBolloBytes(buffer, 1);
    }

    public String getImpstBolloFormatted() {
        return MarshalByteExt.bufferToStr(getImpstBolloBytes());
    }

    public void setImpstBolloBytes(byte[] buffer) {
        setImpstBolloBytes(buffer, 1);
    }

    public byte[] getImpstBolloBytes() {
        byte[] buffer = new byte[Len.IMPST_BOLLO];
        return getImpstBolloBytes(buffer, 1);
    }

    public void setImpstBolloBytes(byte[] buffer, int offset) {
        int position = offset;
        p58IdImpstBollo = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P58_ID_IMPST_BOLLO, 0);
        position += Len.P58_ID_IMPST_BOLLO;
        p58CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P58_COD_COMP_ANIA, 0);
        position += Len.P58_COD_COMP_ANIA;
        p58IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P58_ID_POLI, 0);
        position += Len.P58_ID_POLI;
        p58IbPoli = MarshalByte.readString(buffer, position, Len.P58_IB_POLI);
        position += Len.P58_IB_POLI;
        p58CodFisc = MarshalByte.readString(buffer, position, Len.P58_COD_FISC);
        position += Len.P58_COD_FISC;
        p58CodPartIva = MarshalByte.readString(buffer, position, Len.P58_COD_PART_IVA);
        position += Len.P58_COD_PART_IVA;
        p58IdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P58_ID_RAPP_ANA, 0);
        position += Len.P58_ID_RAPP_ANA;
        p58IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P58_ID_MOVI_CRZ, 0);
        position += Len.P58_ID_MOVI_CRZ;
        p58IdMoviChiu.setP58IdMoviChiuFromBuffer(buffer, position);
        position += P58IdMoviChiu.Len.P58_ID_MOVI_CHIU;
        p58DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P58_DT_INI_EFF, 0);
        position += Len.P58_DT_INI_EFF;
        p58DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P58_DT_END_EFF, 0);
        position += Len.P58_DT_END_EFF;
        p58DtIniCalc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P58_DT_INI_CALC, 0);
        position += Len.P58_DT_INI_CALC;
        p58DtEndCalc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P58_DT_END_CALC, 0);
        position += Len.P58_DT_END_CALC;
        p58TpCausBollo = MarshalByte.readString(buffer, position, Len.P58_TP_CAUS_BOLLO);
        position += Len.P58_TP_CAUS_BOLLO;
        p58ImpstBolloDettC.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.P58_IMPST_BOLLO_DETT_C, Len.Fract.P58_IMPST_BOLLO_DETT_C));
        position += Len.P58_IMPST_BOLLO_DETT_C;
        p58ImpstBolloDettV.setP58ImpstBolloDettVFromBuffer(buffer, position);
        position += P58ImpstBolloDettV.Len.P58_IMPST_BOLLO_DETT_V;
        p58ImpstBolloTotV.setP58ImpstBolloTotVFromBuffer(buffer, position);
        position += P58ImpstBolloTotV.Len.P58_IMPST_BOLLO_TOT_V;
        p58ImpstBolloTotR.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.P58_IMPST_BOLLO_TOT_R, Len.Fract.P58_IMPST_BOLLO_TOT_R));
        position += Len.P58_IMPST_BOLLO_TOT_R;
        p58DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P58_DS_RIGA, 0);
        position += Len.P58_DS_RIGA;
        p58DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p58DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P58_DS_VER, 0);
        position += Len.P58_DS_VER;
        p58DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P58_DS_TS_INI_CPTZ, 0);
        position += Len.P58_DS_TS_INI_CPTZ;
        p58DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P58_DS_TS_END_CPTZ, 0);
        position += Len.P58_DS_TS_END_CPTZ;
        p58DsUtente = MarshalByte.readString(buffer, position, Len.P58_DS_UTENTE);
        position += Len.P58_DS_UTENTE;
        p58DsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getImpstBolloBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, p58IdImpstBollo, Len.Int.P58_ID_IMPST_BOLLO, 0);
        position += Len.P58_ID_IMPST_BOLLO;
        MarshalByte.writeIntAsPacked(buffer, position, p58CodCompAnia, Len.Int.P58_COD_COMP_ANIA, 0);
        position += Len.P58_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, p58IdPoli, Len.Int.P58_ID_POLI, 0);
        position += Len.P58_ID_POLI;
        MarshalByte.writeString(buffer, position, p58IbPoli, Len.P58_IB_POLI);
        position += Len.P58_IB_POLI;
        MarshalByte.writeString(buffer, position, p58CodFisc, Len.P58_COD_FISC);
        position += Len.P58_COD_FISC;
        MarshalByte.writeString(buffer, position, p58CodPartIva, Len.P58_COD_PART_IVA);
        position += Len.P58_COD_PART_IVA;
        MarshalByte.writeIntAsPacked(buffer, position, p58IdRappAna, Len.Int.P58_ID_RAPP_ANA, 0);
        position += Len.P58_ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, p58IdMoviCrz, Len.Int.P58_ID_MOVI_CRZ, 0);
        position += Len.P58_ID_MOVI_CRZ;
        p58IdMoviChiu.getP58IdMoviChiuAsBuffer(buffer, position);
        position += P58IdMoviChiu.Len.P58_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, p58DtIniEff, Len.Int.P58_DT_INI_EFF, 0);
        position += Len.P58_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, p58DtEndEff, Len.Int.P58_DT_END_EFF, 0);
        position += Len.P58_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, p58DtIniCalc, Len.Int.P58_DT_INI_CALC, 0);
        position += Len.P58_DT_INI_CALC;
        MarshalByte.writeIntAsPacked(buffer, position, p58DtEndCalc, Len.Int.P58_DT_END_CALC, 0);
        position += Len.P58_DT_END_CALC;
        MarshalByte.writeString(buffer, position, p58TpCausBollo, Len.P58_TP_CAUS_BOLLO);
        position += Len.P58_TP_CAUS_BOLLO;
        MarshalByte.writeDecimalAsPacked(buffer, position, p58ImpstBolloDettC.copy());
        position += Len.P58_IMPST_BOLLO_DETT_C;
        p58ImpstBolloDettV.getP58ImpstBolloDettVAsBuffer(buffer, position);
        position += P58ImpstBolloDettV.Len.P58_IMPST_BOLLO_DETT_V;
        p58ImpstBolloTotV.getP58ImpstBolloTotVAsBuffer(buffer, position);
        position += P58ImpstBolloTotV.Len.P58_IMPST_BOLLO_TOT_V;
        MarshalByte.writeDecimalAsPacked(buffer, position, p58ImpstBolloTotR.copy());
        position += Len.P58_IMPST_BOLLO_TOT_R;
        MarshalByte.writeLongAsPacked(buffer, position, p58DsRiga, Len.Int.P58_DS_RIGA, 0);
        position += Len.P58_DS_RIGA;
        MarshalByte.writeChar(buffer, position, p58DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, p58DsVer, Len.Int.P58_DS_VER, 0);
        position += Len.P58_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, p58DsTsIniCptz, Len.Int.P58_DS_TS_INI_CPTZ, 0);
        position += Len.P58_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, p58DsTsEndCptz, Len.Int.P58_DS_TS_END_CPTZ, 0);
        position += Len.P58_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, p58DsUtente, Len.P58_DS_UTENTE);
        position += Len.P58_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, p58DsStatoElab);
        return buffer;
    }

    public void setP58IdImpstBollo(int p58IdImpstBollo) {
        this.p58IdImpstBollo = p58IdImpstBollo;
    }

    public int getP58IdImpstBollo() {
        return this.p58IdImpstBollo;
    }

    public void setP58CodCompAnia(int p58CodCompAnia) {
        this.p58CodCompAnia = p58CodCompAnia;
    }

    public int getP58CodCompAnia() {
        return this.p58CodCompAnia;
    }

    public void setP58IdPoli(int p58IdPoli) {
        this.p58IdPoli = p58IdPoli;
    }

    public int getP58IdPoli() {
        return this.p58IdPoli;
    }

    public void setP58IbPoli(String p58IbPoli) {
        this.p58IbPoli = Functions.subString(p58IbPoli, Len.P58_IB_POLI);
    }

    public String getP58IbPoli() {
        return this.p58IbPoli;
    }

    public void setP58CodFisc(String p58CodFisc) {
        this.p58CodFisc = Functions.subString(p58CodFisc, Len.P58_COD_FISC);
    }

    public String getP58CodFisc() {
        return this.p58CodFisc;
    }

    public String getP58CodFiscFormatted() {
        return Functions.padBlanks(getP58CodFisc(), Len.P58_COD_FISC);
    }

    public void setP58CodPartIva(String p58CodPartIva) {
        this.p58CodPartIva = Functions.subString(p58CodPartIva, Len.P58_COD_PART_IVA);
    }

    public String getP58CodPartIva() {
        return this.p58CodPartIva;
    }

    public String getP58CodPartIvaFormatted() {
        return Functions.padBlanks(getP58CodPartIva(), Len.P58_COD_PART_IVA);
    }

    public void setP58IdRappAna(int p58IdRappAna) {
        this.p58IdRappAna = p58IdRappAna;
    }

    public int getP58IdRappAna() {
        return this.p58IdRappAna;
    }

    public void setP58IdMoviCrz(int p58IdMoviCrz) {
        this.p58IdMoviCrz = p58IdMoviCrz;
    }

    public int getP58IdMoviCrz() {
        return this.p58IdMoviCrz;
    }

    public void setP58DtIniEff(int p58DtIniEff) {
        this.p58DtIniEff = p58DtIniEff;
    }

    public int getP58DtIniEff() {
        return this.p58DtIniEff;
    }

    public void setP58DtEndEff(int p58DtEndEff) {
        this.p58DtEndEff = p58DtEndEff;
    }

    public int getP58DtEndEff() {
        return this.p58DtEndEff;
    }

    public void setP58DtIniCalc(int p58DtIniCalc) {
        this.p58DtIniCalc = p58DtIniCalc;
    }

    public int getP58DtIniCalc() {
        return this.p58DtIniCalc;
    }

    public void setP58DtEndCalc(int p58DtEndCalc) {
        this.p58DtEndCalc = p58DtEndCalc;
    }

    public int getP58DtEndCalc() {
        return this.p58DtEndCalc;
    }

    public void setP58TpCausBollo(String p58TpCausBollo) {
        this.p58TpCausBollo = Functions.subString(p58TpCausBollo, Len.P58_TP_CAUS_BOLLO);
    }

    public String getP58TpCausBollo() {
        return this.p58TpCausBollo;
    }

    public void setP58ImpstBolloDettC(AfDecimal p58ImpstBolloDettC) {
        this.p58ImpstBolloDettC.assign(p58ImpstBolloDettC);
    }

    public AfDecimal getP58ImpstBolloDettC() {
        return this.p58ImpstBolloDettC.copy();
    }

    public void setP58ImpstBolloTotR(AfDecimal p58ImpstBolloTotR) {
        this.p58ImpstBolloTotR.assign(p58ImpstBolloTotR);
    }

    public AfDecimal getP58ImpstBolloTotR() {
        return this.p58ImpstBolloTotR.copy();
    }

    public void setP58DsRiga(long p58DsRiga) {
        this.p58DsRiga = p58DsRiga;
    }

    public long getP58DsRiga() {
        return this.p58DsRiga;
    }

    public void setP58DsOperSql(char p58DsOperSql) {
        this.p58DsOperSql = p58DsOperSql;
    }

    public void setP58DsOperSqlFormatted(String p58DsOperSql) {
        setP58DsOperSql(Functions.charAt(p58DsOperSql, Types.CHAR_SIZE));
    }

    public char getP58DsOperSql() {
        return this.p58DsOperSql;
    }

    public void setP58DsVer(int p58DsVer) {
        this.p58DsVer = p58DsVer;
    }

    public int getP58DsVer() {
        return this.p58DsVer;
    }

    public void setP58DsTsIniCptz(long p58DsTsIniCptz) {
        this.p58DsTsIniCptz = p58DsTsIniCptz;
    }

    public long getP58DsTsIniCptz() {
        return this.p58DsTsIniCptz;
    }

    public void setP58DsTsEndCptz(long p58DsTsEndCptz) {
        this.p58DsTsEndCptz = p58DsTsEndCptz;
    }

    public long getP58DsTsEndCptz() {
        return this.p58DsTsEndCptz;
    }

    public void setP58DsUtente(String p58DsUtente) {
        this.p58DsUtente = Functions.subString(p58DsUtente, Len.P58_DS_UTENTE);
    }

    public String getP58DsUtente() {
        return this.p58DsUtente;
    }

    public void setP58DsStatoElab(char p58DsStatoElab) {
        this.p58DsStatoElab = p58DsStatoElab;
    }

    public void setP58DsStatoElabFormatted(String p58DsStatoElab) {
        setP58DsStatoElab(Functions.charAt(p58DsStatoElab, Types.CHAR_SIZE));
    }

    public char getP58DsStatoElab() {
        return this.p58DsStatoElab;
    }

    public P58IdMoviChiu getP58IdMoviChiu() {
        return p58IdMoviChiu;
    }

    public P58ImpstBolloDettV getP58ImpstBolloDettV() {
        return p58ImpstBolloDettV;
    }

    public P58ImpstBolloTotV getP58ImpstBolloTotV() {
        return p58ImpstBolloTotV;
    }

    @Override
    public byte[] serialize() {
        return getImpstBolloBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P58_ID_IMPST_BOLLO = 5;
        public static final int P58_COD_COMP_ANIA = 3;
        public static final int P58_ID_POLI = 5;
        public static final int P58_IB_POLI = 40;
        public static final int P58_COD_FISC = 16;
        public static final int P58_COD_PART_IVA = 11;
        public static final int P58_ID_RAPP_ANA = 5;
        public static final int P58_ID_MOVI_CRZ = 5;
        public static final int P58_DT_INI_EFF = 5;
        public static final int P58_DT_END_EFF = 5;
        public static final int P58_DT_INI_CALC = 5;
        public static final int P58_DT_END_CALC = 5;
        public static final int P58_TP_CAUS_BOLLO = 2;
        public static final int P58_IMPST_BOLLO_DETT_C = 8;
        public static final int P58_IMPST_BOLLO_TOT_R = 8;
        public static final int P58_DS_RIGA = 6;
        public static final int P58_DS_OPER_SQL = 1;
        public static final int P58_DS_VER = 5;
        public static final int P58_DS_TS_INI_CPTZ = 10;
        public static final int P58_DS_TS_END_CPTZ = 10;
        public static final int P58_DS_UTENTE = 20;
        public static final int P58_DS_STATO_ELAB = 1;
        public static final int IMPST_BOLLO = P58_ID_IMPST_BOLLO + P58_COD_COMP_ANIA + P58_ID_POLI + P58_IB_POLI + P58_COD_FISC + P58_COD_PART_IVA + P58_ID_RAPP_ANA + P58_ID_MOVI_CRZ + P58IdMoviChiu.Len.P58_ID_MOVI_CHIU + P58_DT_INI_EFF + P58_DT_END_EFF + P58_DT_INI_CALC + P58_DT_END_CALC + P58_TP_CAUS_BOLLO + P58_IMPST_BOLLO_DETT_C + P58ImpstBolloDettV.Len.P58_IMPST_BOLLO_DETT_V + P58ImpstBolloTotV.Len.P58_IMPST_BOLLO_TOT_V + P58_IMPST_BOLLO_TOT_R + P58_DS_RIGA + P58_DS_OPER_SQL + P58_DS_VER + P58_DS_TS_INI_CPTZ + P58_DS_TS_END_CPTZ + P58_DS_UTENTE + P58_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P58_ID_IMPST_BOLLO = 9;
            public static final int P58_COD_COMP_ANIA = 5;
            public static final int P58_ID_POLI = 9;
            public static final int P58_ID_RAPP_ANA = 9;
            public static final int P58_ID_MOVI_CRZ = 9;
            public static final int P58_DT_INI_EFF = 8;
            public static final int P58_DT_END_EFF = 8;
            public static final int P58_DT_INI_CALC = 8;
            public static final int P58_DT_END_CALC = 8;
            public static final int P58_IMPST_BOLLO_DETT_C = 12;
            public static final int P58_IMPST_BOLLO_TOT_R = 12;
            public static final int P58_DS_RIGA = 10;
            public static final int P58_DS_VER = 9;
            public static final int P58_DS_TS_INI_CPTZ = 18;
            public static final int P58_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P58_IMPST_BOLLO_DETT_C = 3;
            public static final int P58_IMPST_BOLLO_TOT_R = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
