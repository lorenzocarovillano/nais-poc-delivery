package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WL23-VAL-RISC-INI-VINPG<br>
 * Variable: WL23-VAL-RISC-INI-VINPG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl23ValRiscIniVinpg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl23ValRiscIniVinpg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL23_VAL_RISC_INI_VINPG;
    }

    public void setWl23ValRiscIniVinpg(AfDecimal wl23ValRiscIniVinpg) {
        writeDecimalAsPacked(Pos.WL23_VAL_RISC_INI_VINPG, wl23ValRiscIniVinpg.copy());
    }

    public void setWl23ValRiscIniVinpgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL23_VAL_RISC_INI_VINPG, Pos.WL23_VAL_RISC_INI_VINPG);
    }

    /**Original name: WL23-VAL-RISC-INI-VINPG<br>*/
    public AfDecimal getWl23ValRiscIniVinpg() {
        return readPackedAsDecimal(Pos.WL23_VAL_RISC_INI_VINPG, Len.Int.WL23_VAL_RISC_INI_VINPG, Len.Fract.WL23_VAL_RISC_INI_VINPG);
    }

    public byte[] getWl23ValRiscIniVinpgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL23_VAL_RISC_INI_VINPG, Pos.WL23_VAL_RISC_INI_VINPG);
        return buffer;
    }

    public void initWl23ValRiscIniVinpgSpaces() {
        fill(Pos.WL23_VAL_RISC_INI_VINPG, Len.WL23_VAL_RISC_INI_VINPG, Types.SPACE_CHAR);
    }

    public void setWl23ValRiscIniVinpgNull(String wl23ValRiscIniVinpgNull) {
        writeString(Pos.WL23_VAL_RISC_INI_VINPG_NULL, wl23ValRiscIniVinpgNull, Len.WL23_VAL_RISC_INI_VINPG_NULL);
    }

    /**Original name: WL23-VAL-RISC-INI-VINPG-NULL<br>*/
    public String getWl23ValRiscIniVinpgNull() {
        return readString(Pos.WL23_VAL_RISC_INI_VINPG_NULL, Len.WL23_VAL_RISC_INI_VINPG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL23_VAL_RISC_INI_VINPG = 1;
        public static final int WL23_VAL_RISC_INI_VINPG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL23_VAL_RISC_INI_VINPG = 8;
        public static final int WL23_VAL_RISC_INI_VINPG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WL23_VAL_RISC_INI_VINPG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WL23_VAL_RISC_INI_VINPG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
