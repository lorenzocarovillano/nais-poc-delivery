package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-DT-VLDT-PROD<br>
 * Variable: L3421-DT-VLDT-PROD from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421DtVldtProd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421DtVldtProd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_DT_VLDT_PROD;
    }

    public void setL3421DtVldtProdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_DT_VLDT_PROD, Pos.L3421_DT_VLDT_PROD);
    }

    /**Original name: L3421-DT-VLDT-PROD<br>*/
    public int getL3421DtVldtProd() {
        return readPackedAsInt(Pos.L3421_DT_VLDT_PROD, Len.Int.L3421_DT_VLDT_PROD);
    }

    public byte[] getL3421DtVldtProdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_DT_VLDT_PROD, Pos.L3421_DT_VLDT_PROD);
        return buffer;
    }

    /**Original name: L3421-DT-VLDT-PROD-NULL<br>*/
    public String getL3421DtVldtProdNull() {
        return readString(Pos.L3421_DT_VLDT_PROD_NULL, Len.L3421_DT_VLDT_PROD_NULL);
    }

    public String getL3421DtVldtProdNullFormatted() {
        return Functions.padBlanks(getL3421DtVldtProdNull(), Len.L3421_DT_VLDT_PROD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_DT_VLDT_PROD = 1;
        public static final int L3421_DT_VLDT_PROD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_DT_VLDT_PROD = 5;
        public static final int L3421_DT_VLDT_PROD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_DT_VLDT_PROD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
