package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: OPERAZIONE-FILESQS<br>
 * Variable: OPERAZIONE-FILESQS from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class OperazioneFilesqs {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.OPERAZIONE_FILESQS);
    public static final String OPEN_FILESQS_OPER = "OP";
    public static final String CLOSE_FILESQS_OPER = "CL";
    public static final String READ_FILESQS_OPER = "RE";
    public static final String WRITE_FILESQS_OPER = "WR";
    public static final String REWRITE_FILESQS_OPER = "RW";

    //==== METHODS ====
    public void setOperazioneFilesqs(String operazioneFilesqs) {
        this.value = Functions.subString(operazioneFilesqs, Len.OPERAZIONE_FILESQS);
    }

    public String getOperazioneFilesqs() {
        return this.value;
    }

    public void setOpenFilesqsOper() {
        value = OPEN_FILESQS_OPER;
    }

    public void setCloseFilesqsOper() {
        value = CLOSE_FILESQS_OPER;
    }

    public boolean isReadFilesqsOper() {
        return value.equals(READ_FILESQS_OPER);
    }

    public void setReadFilesqsOper() {
        value = READ_FILESQS_OPER;
    }

    public void setWriteFilesqsOper() {
        value = WRITE_FILESQS_OPER;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OPERAZIONE_FILESQS = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
