package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-ID-MOVI-CHIU<br>
 * Variable: PRE-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_ID_MOVI_CHIU;
    }

    public void setPreIdMoviChiu(int preIdMoviChiu) {
        writeIntAsPacked(Pos.PRE_ID_MOVI_CHIU, preIdMoviChiu, Len.Int.PRE_ID_MOVI_CHIU);
    }

    public void setPreIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_ID_MOVI_CHIU, Pos.PRE_ID_MOVI_CHIU);
    }

    /**Original name: PRE-ID-MOVI-CHIU<br>*/
    public int getPreIdMoviChiu() {
        return readPackedAsInt(Pos.PRE_ID_MOVI_CHIU, Len.Int.PRE_ID_MOVI_CHIU);
    }

    public byte[] getPreIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_ID_MOVI_CHIU, Pos.PRE_ID_MOVI_CHIU);
        return buffer;
    }

    public void setPreIdMoviChiuNull(String preIdMoviChiuNull) {
        writeString(Pos.PRE_ID_MOVI_CHIU_NULL, preIdMoviChiuNull, Len.PRE_ID_MOVI_CHIU_NULL);
    }

    /**Original name: PRE-ID-MOVI-CHIU-NULL<br>*/
    public String getPreIdMoviChiuNull() {
        return readString(Pos.PRE_ID_MOVI_CHIU_NULL, Len.PRE_ID_MOVI_CHIU_NULL);
    }

    public String getPreIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getPreIdMoviChiuNull(), Len.PRE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_ID_MOVI_CHIU = 1;
        public static final int PRE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_ID_MOVI_CHIU = 5;
        public static final int PRE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
