package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-PC-RIP-PRE<br>
 * Variable: WGRZ-PC-RIP-PRE from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzPcRipPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzPcRipPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_PC_RIP_PRE;
    }

    public void setWgrzPcRipPre(AfDecimal wgrzPcRipPre) {
        writeDecimalAsPacked(Pos.WGRZ_PC_RIP_PRE, wgrzPcRipPre.copy());
    }

    public void setWgrzPcRipPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_PC_RIP_PRE, Pos.WGRZ_PC_RIP_PRE);
    }

    /**Original name: WGRZ-PC-RIP-PRE<br>*/
    public AfDecimal getWgrzPcRipPre() {
        return readPackedAsDecimal(Pos.WGRZ_PC_RIP_PRE, Len.Int.WGRZ_PC_RIP_PRE, Len.Fract.WGRZ_PC_RIP_PRE);
    }

    public byte[] getWgrzPcRipPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_PC_RIP_PRE, Pos.WGRZ_PC_RIP_PRE);
        return buffer;
    }

    public void initWgrzPcRipPreSpaces() {
        fill(Pos.WGRZ_PC_RIP_PRE, Len.WGRZ_PC_RIP_PRE, Types.SPACE_CHAR);
    }

    public void setWgrzPcRipPreNull(String wgrzPcRipPreNull) {
        writeString(Pos.WGRZ_PC_RIP_PRE_NULL, wgrzPcRipPreNull, Len.WGRZ_PC_RIP_PRE_NULL);
    }

    /**Original name: WGRZ-PC-RIP-PRE-NULL<br>*/
    public String getWgrzPcRipPreNull() {
        return readString(Pos.WGRZ_PC_RIP_PRE_NULL, Len.WGRZ_PC_RIP_PRE_NULL);
    }

    public String getVgrzPcRipPreNullFormatted() {
        return Functions.padBlanks(getWgrzPcRipPreNull(), Len.WGRZ_PC_RIP_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_PC_RIP_PRE = 1;
        public static final int WGRZ_PC_RIP_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_PC_RIP_PRE = 4;
        public static final int WGRZ_PC_RIP_PRE_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WGRZ_PC_RIP_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_PC_RIP_PRE = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
