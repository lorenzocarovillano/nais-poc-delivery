package it.accenture.jnais.ws;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LVVS0560<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLvvs0560 {

    //==== PROPERTIES ====
    //Original name: IX-DCLGEN
    private short dclgen = ((short)0);
    //Original name: IX-TAB-GRZ
    private short tabGrz = ((short)0);
    //Original name: IX-TAB-L19
    private short tabL19 = ((short)0);
    //Original name: IX-TAB-TGA
    private short tabTga = ((short)0);
    //Original name: IX-TAB-VAS
    private short tabVas = ((short)0);
    //Original name: IX-GUIDA-GRZ
    private short guidaGrz = ((short)0);
    //Original name: IX-TAB-0501
    private short tab0501 = ((short)0);
    //Original name: IX-TAB-TEMP
    private short tabTemp = ((short)0);
    //Original name: IX-TAB-POS
    private short tabPos = ((short)0);

    //==== METHODS ====
    public void setDclgen(short dclgen) {
        this.dclgen = dclgen;
    }

    public short getDclgen() {
        return this.dclgen;
    }

    public void setTabGrz(short tabGrz) {
        this.tabGrz = tabGrz;
    }

    public short getTabGrz() {
        return this.tabGrz;
    }

    public void setTabL19(short tabL19) {
        this.tabL19 = tabL19;
    }

    public short getTabL19() {
        return this.tabL19;
    }

    public void setTabTga(short tabTga) {
        this.tabTga = tabTga;
    }

    public short getTabTga() {
        return this.tabTga;
    }

    public void setTabVas(short tabVas) {
        this.tabVas = tabVas;
    }

    public short getTabVas() {
        return this.tabVas;
    }

    public void setGuidaGrz(short guidaGrz) {
        this.guidaGrz = guidaGrz;
    }

    public short getGuidaGrz() {
        return this.guidaGrz;
    }

    public void setTab0501(short tab0501) {
        this.tab0501 = tab0501;
    }

    public short getTab0501() {
        return this.tab0501;
    }

    public void setTabTemp(short tabTemp) {
        this.tabTemp = tabTemp;
    }

    public short getTabTemp() {
        return this.tabTemp;
    }

    public void setTabPos(short tabPos) {
        this.tabPos = tabPos;
    }

    public short getTabPos() {
        return this.tabPos;
    }
}
