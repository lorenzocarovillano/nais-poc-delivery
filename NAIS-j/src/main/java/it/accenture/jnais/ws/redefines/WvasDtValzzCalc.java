package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WVAS-DT-VALZZ-CALC<br>
 * Variable: WVAS-DT-VALZZ-CALC from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasDtValzzCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasDtValzzCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_DT_VALZZ_CALC;
    }

    public void setWvasDtValzzCalc(int wvasDtValzzCalc) {
        writeIntAsPacked(Pos.WVAS_DT_VALZZ_CALC, wvasDtValzzCalc, Len.Int.WVAS_DT_VALZZ_CALC);
    }

    /**Original name: WVAS-DT-VALZZ-CALC<br>*/
    public int getWvasDtValzzCalc() {
        return readPackedAsInt(Pos.WVAS_DT_VALZZ_CALC, Len.Int.WVAS_DT_VALZZ_CALC);
    }

    public void setWvasDtValzzCalcNull(String wvasDtValzzCalcNull) {
        writeString(Pos.WVAS_DT_VALZZ_CALC_NULL, wvasDtValzzCalcNull, Len.WVAS_DT_VALZZ_CALC_NULL);
    }

    /**Original name: WVAS-DT-VALZZ-CALC-NULL<br>*/
    public String getWvasDtValzzCalcNull() {
        return readString(Pos.WVAS_DT_VALZZ_CALC_NULL, Len.WVAS_DT_VALZZ_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_DT_VALZZ_CALC = 1;
        public static final int WVAS_DT_VALZZ_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_DT_VALZZ_CALC = 5;
        public static final int WVAS_DT_VALZZ_CALC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_DT_VALZZ_CALC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
