package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ALQ-MARG-C-SUBRSH<br>
 * Variable: B03-ALQ-MARG-C-SUBRSH from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03AlqMargCSubrsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03AlqMargCSubrsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ALQ_MARG_C_SUBRSH;
    }

    public void setB03AlqMargCSubrsh(AfDecimal b03AlqMargCSubrsh) {
        writeDecimalAsPacked(Pos.B03_ALQ_MARG_C_SUBRSH, b03AlqMargCSubrsh.copy());
    }

    public void setB03AlqMargCSubrshFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ALQ_MARG_C_SUBRSH, Pos.B03_ALQ_MARG_C_SUBRSH);
    }

    /**Original name: B03-ALQ-MARG-C-SUBRSH<br>*/
    public AfDecimal getB03AlqMargCSubrsh() {
        return readPackedAsDecimal(Pos.B03_ALQ_MARG_C_SUBRSH, Len.Int.B03_ALQ_MARG_C_SUBRSH, Len.Fract.B03_ALQ_MARG_C_SUBRSH);
    }

    public byte[] getB03AlqMargCSubrshAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ALQ_MARG_C_SUBRSH, Pos.B03_ALQ_MARG_C_SUBRSH);
        return buffer;
    }

    public void setB03AlqMargCSubrshNull(String b03AlqMargCSubrshNull) {
        writeString(Pos.B03_ALQ_MARG_C_SUBRSH_NULL, b03AlqMargCSubrshNull, Len.B03_ALQ_MARG_C_SUBRSH_NULL);
    }

    /**Original name: B03-ALQ-MARG-C-SUBRSH-NULL<br>*/
    public String getB03AlqMargCSubrshNull() {
        return readString(Pos.B03_ALQ_MARG_C_SUBRSH_NULL, Len.B03_ALQ_MARG_C_SUBRSH_NULL);
    }

    public String getB03AlqMargCSubrshNullFormatted() {
        return Functions.padBlanks(getB03AlqMargCSubrshNull(), Len.B03_ALQ_MARG_C_SUBRSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ALQ_MARG_C_SUBRSH = 1;
        public static final int B03_ALQ_MARG_C_SUBRSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ALQ_MARG_C_SUBRSH = 4;
        public static final int B03_ALQ_MARG_C_SUBRSH_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_ALQ_MARG_C_SUBRSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ALQ_MARG_C_SUBRSH = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
