package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-TS-TARI-DOV<br>
 * Variable: W-B03-TS-TARI-DOV from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03TsTariDovLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03TsTariDovLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_TS_TARI_DOV;
    }

    public void setwB03TsTariDov(AfDecimal wB03TsTariDov) {
        writeDecimalAsPacked(Pos.W_B03_TS_TARI_DOV, wB03TsTariDov.copy());
    }

    /**Original name: W-B03-TS-TARI-DOV<br>*/
    public AfDecimal getwB03TsTariDov() {
        return readPackedAsDecimal(Pos.W_B03_TS_TARI_DOV, Len.Int.W_B03_TS_TARI_DOV, Len.Fract.W_B03_TS_TARI_DOV);
    }

    public byte[] getwB03TsTariDovAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_TS_TARI_DOV, Pos.W_B03_TS_TARI_DOV);
        return buffer;
    }

    public void setwB03TsTariDovNull(String wB03TsTariDovNull) {
        writeString(Pos.W_B03_TS_TARI_DOV_NULL, wB03TsTariDovNull, Len.W_B03_TS_TARI_DOV_NULL);
    }

    /**Original name: W-B03-TS-TARI-DOV-NULL<br>*/
    public String getwB03TsTariDovNull() {
        return readString(Pos.W_B03_TS_TARI_DOV_NULL, Len.W_B03_TS_TARI_DOV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_TS_TARI_DOV = 1;
        public static final int W_B03_TS_TARI_DOV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_TS_TARI_DOV = 8;
        public static final int W_B03_TS_TARI_DOV_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_TS_TARI_DOV = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_TS_TARI_DOV = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
