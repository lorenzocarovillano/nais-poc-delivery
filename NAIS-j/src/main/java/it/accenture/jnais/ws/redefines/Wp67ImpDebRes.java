package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-IMP-DEB-RES<br>
 * Variable: WP67-IMP-DEB-RES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67ImpDebRes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67ImpDebRes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_IMP_DEB_RES;
    }

    public void setWp67ImpDebRes(AfDecimal wp67ImpDebRes) {
        writeDecimalAsPacked(Pos.WP67_IMP_DEB_RES, wp67ImpDebRes.copy());
    }

    public void setWp67ImpDebResFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_IMP_DEB_RES, Pos.WP67_IMP_DEB_RES);
    }

    /**Original name: WP67-IMP-DEB-RES<br>*/
    public AfDecimal getWp67ImpDebRes() {
        return readPackedAsDecimal(Pos.WP67_IMP_DEB_RES, Len.Int.WP67_IMP_DEB_RES, Len.Fract.WP67_IMP_DEB_RES);
    }

    public byte[] getWp67ImpDebResAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_IMP_DEB_RES, Pos.WP67_IMP_DEB_RES);
        return buffer;
    }

    public void setWp67ImpDebResNull(String wp67ImpDebResNull) {
        writeString(Pos.WP67_IMP_DEB_RES_NULL, wp67ImpDebResNull, Len.WP67_IMP_DEB_RES_NULL);
    }

    /**Original name: WP67-IMP-DEB-RES-NULL<br>*/
    public String getWp67ImpDebResNull() {
        return readString(Pos.WP67_IMP_DEB_RES_NULL, Len.WP67_IMP_DEB_RES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_IMP_DEB_RES = 1;
        public static final int WP67_IMP_DEB_RES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_IMP_DEB_RES = 8;
        public static final int WP67_IMP_DEB_RES_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_IMP_DEB_RES = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_IMP_DEB_RES = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
