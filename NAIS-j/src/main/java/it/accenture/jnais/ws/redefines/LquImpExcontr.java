package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-EXCONTR<br>
 * Variable: LQU-IMP-EXCONTR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpExcontr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpExcontr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_EXCONTR;
    }

    public void setLquImpExcontr(AfDecimal lquImpExcontr) {
        writeDecimalAsPacked(Pos.LQU_IMP_EXCONTR, lquImpExcontr.copy());
    }

    public void setLquImpExcontrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_EXCONTR, Pos.LQU_IMP_EXCONTR);
    }

    /**Original name: LQU-IMP-EXCONTR<br>*/
    public AfDecimal getLquImpExcontr() {
        return readPackedAsDecimal(Pos.LQU_IMP_EXCONTR, Len.Int.LQU_IMP_EXCONTR, Len.Fract.LQU_IMP_EXCONTR);
    }

    public byte[] getLquImpExcontrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_EXCONTR, Pos.LQU_IMP_EXCONTR);
        return buffer;
    }

    public void setLquImpExcontrNull(String lquImpExcontrNull) {
        writeString(Pos.LQU_IMP_EXCONTR_NULL, lquImpExcontrNull, Len.LQU_IMP_EXCONTR_NULL);
    }

    /**Original name: LQU-IMP-EXCONTR-NULL<br>*/
    public String getLquImpExcontrNull() {
        return readString(Pos.LQU_IMP_EXCONTR_NULL, Len.LQU_IMP_EXCONTR_NULL);
    }

    public String getLquImpExcontrNullFormatted() {
        return Functions.padBlanks(getLquImpExcontrNull(), Len.LQU_IMP_EXCONTR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_EXCONTR = 1;
        public static final int LQU_IMP_EXCONTR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_EXCONTR = 8;
        public static final int LQU_IMP_EXCONTR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_EXCONTR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_EXCONTR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
