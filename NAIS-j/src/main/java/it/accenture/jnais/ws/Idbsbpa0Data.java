package it.accenture.jnais.ws;

import it.accenture.jnais.copy.BtcParallelismDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndGravitaErrore;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSBPA0<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsbpa0Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-BTC-PARALLELISM
    private IndGravitaErrore indBtcParallelism = new IndGravitaErrore();
    //Original name: BTC-PARALLELISM-DB
    private BtcParallelismDb btcParallelismDb = new BtcParallelismDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public BtcParallelismDb getBtcParallelismDb() {
        return btcParallelismDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndGravitaErrore getIndBtcParallelism() {
        return indBtcParallelism;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
