package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-DUR-GG<br>
 * Variable: L3401-DUR-GG from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401DurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401DurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_DUR_GG;
    }

    public void setL3401DurGg(int l3401DurGg) {
        writeIntAsPacked(Pos.L3401_DUR_GG, l3401DurGg, Len.Int.L3401_DUR_GG);
    }

    public void setL3401DurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_DUR_GG, Pos.L3401_DUR_GG);
    }

    /**Original name: L3401-DUR-GG<br>*/
    public int getL3401DurGg() {
        return readPackedAsInt(Pos.L3401_DUR_GG, Len.Int.L3401_DUR_GG);
    }

    public byte[] getL3401DurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_DUR_GG, Pos.L3401_DUR_GG);
        return buffer;
    }

    /**Original name: L3401-DUR-GG-NULL<br>*/
    public String getL3401DurGgNull() {
        return readString(Pos.L3401_DUR_GG_NULL, Len.L3401_DUR_GG_NULL);
    }

    public String getL3401DurGgNullFormatted() {
        return Functions.padBlanks(getL3401DurGgNull(), Len.L3401_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_DUR_GG = 1;
        public static final int L3401_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_DUR_GG = 3;
        public static final int L3401_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
