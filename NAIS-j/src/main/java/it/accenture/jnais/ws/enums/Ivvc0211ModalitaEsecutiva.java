package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IVVC0211-MODALITA-ESECUTIVA<br>
 * Variable: IVVC0211-MODALITA-ESECUTIVA from copybook IVVC0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ivvc0211ModalitaEsecutiva {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char ON_LINE = 'O';
    public static final char BATCH = 'B';
    public static final char BATCH_INFR = 'I';

    //==== METHODS ====
    public void setModalitaEsecutiva(char modalitaEsecutiva) {
        this.value = modalitaEsecutiva;
    }

    public char getModalitaEsecutiva() {
        return this.value;
    }

    public void setS211Batch() {
        value = BATCH;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MODALITA_ESECUTIVA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
