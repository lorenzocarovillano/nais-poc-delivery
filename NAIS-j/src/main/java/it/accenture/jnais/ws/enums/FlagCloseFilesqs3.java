package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-CLOSE-FILESQS3<br>
 * Variable: FLAG-CLOSE-FILESQS3 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCloseFilesqs3 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCloseFilesqs3(char flagCloseFilesqs3) {
        this.value = flagCloseFilesqs3;
    }

    public char getFlagCloseFilesqs3() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setCloseFilesqs3No() {
        value = NO;
    }
}
