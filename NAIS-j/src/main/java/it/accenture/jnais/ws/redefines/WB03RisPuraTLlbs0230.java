package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-RIS-PURA-T<br>
 * Variable: W-B03-RIS-PURA-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03RisPuraTLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03RisPuraTLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_RIS_PURA_T;
    }

    public void setwB03RisPuraT(AfDecimal wB03RisPuraT) {
        writeDecimalAsPacked(Pos.W_B03_RIS_PURA_T, wB03RisPuraT.copy());
    }

    /**Original name: W-B03-RIS-PURA-T<br>*/
    public AfDecimal getwB03RisPuraT() {
        return readPackedAsDecimal(Pos.W_B03_RIS_PURA_T, Len.Int.W_B03_RIS_PURA_T, Len.Fract.W_B03_RIS_PURA_T);
    }

    public byte[] getwB03RisPuraTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_RIS_PURA_T, Pos.W_B03_RIS_PURA_T);
        return buffer;
    }

    public void setwB03RisPuraTNull(String wB03RisPuraTNull) {
        writeString(Pos.W_B03_RIS_PURA_T_NULL, wB03RisPuraTNull, Len.W_B03_RIS_PURA_T_NULL);
    }

    /**Original name: W-B03-RIS-PURA-T-NULL<br>*/
    public String getwB03RisPuraTNull() {
        return readString(Pos.W_B03_RIS_PURA_T_NULL, Len.W_B03_RIS_PURA_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_PURA_T = 1;
        public static final int W_B03_RIS_PURA_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_PURA_T = 8;
        public static final int W_B03_RIS_PURA_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_PURA_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_PURA_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
