package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-PC-ABB-TIT-STAT<br>
 * Variable: S089-PC-ABB-TIT-STAT from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089PcAbbTitStat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089PcAbbTitStat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_PC_ABB_TIT_STAT;
    }

    public void setWlquPcAbbTitStat(AfDecimal wlquPcAbbTitStat) {
        writeDecimalAsPacked(Pos.S089_PC_ABB_TIT_STAT, wlquPcAbbTitStat.copy());
    }

    public void setWlquPcAbbTitStatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_PC_ABB_TIT_STAT, Pos.S089_PC_ABB_TIT_STAT);
    }

    /**Original name: WLQU-PC-ABB-TIT-STAT<br>*/
    public AfDecimal getWlquPcAbbTitStat() {
        return readPackedAsDecimal(Pos.S089_PC_ABB_TIT_STAT, Len.Int.WLQU_PC_ABB_TIT_STAT, Len.Fract.WLQU_PC_ABB_TIT_STAT);
    }

    public byte[] getWlquPcAbbTitStatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_PC_ABB_TIT_STAT, Pos.S089_PC_ABB_TIT_STAT);
        return buffer;
    }

    public void initWlquPcAbbTitStatSpaces() {
        fill(Pos.S089_PC_ABB_TIT_STAT, Len.S089_PC_ABB_TIT_STAT, Types.SPACE_CHAR);
    }

    public void setWlquPcAbbTitStatNull(String wlquPcAbbTitStatNull) {
        writeString(Pos.S089_PC_ABB_TIT_STAT_NULL, wlquPcAbbTitStatNull, Len.WLQU_PC_ABB_TIT_STAT_NULL);
    }

    /**Original name: WLQU-PC-ABB-TIT-STAT-NULL<br>*/
    public String getWlquPcAbbTitStatNull() {
        return readString(Pos.S089_PC_ABB_TIT_STAT_NULL, Len.WLQU_PC_ABB_TIT_STAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_PC_ABB_TIT_STAT = 1;
        public static final int S089_PC_ABB_TIT_STAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_PC_ABB_TIT_STAT = 4;
        public static final int WLQU_PC_ABB_TIT_STAT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_PC_ABB_TIT_STAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_PC_ABB_TIT_STAT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
