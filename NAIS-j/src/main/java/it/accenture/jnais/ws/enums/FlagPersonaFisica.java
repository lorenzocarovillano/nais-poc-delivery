package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-PERSONA-FISICA<br>
 * Variable: FLAG-PERSONA-FISICA from program LVVS2720<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagPersonaFisica {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagPersonaFisica(char flagPersonaFisica) {
        this.value = flagPersonaFisica;
    }

    public char getFlagPersonaFisica() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
