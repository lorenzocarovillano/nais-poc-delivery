package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-IMP-LRD-DFZ<br>
 * Variable: TLI-IMP-LRD-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliImpLrdDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliImpLrdDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_IMP_LRD_DFZ;
    }

    public void setTliImpLrdDfz(AfDecimal tliImpLrdDfz) {
        writeDecimalAsPacked(Pos.TLI_IMP_LRD_DFZ, tliImpLrdDfz.copy());
    }

    public void setTliImpLrdDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_IMP_LRD_DFZ, Pos.TLI_IMP_LRD_DFZ);
    }

    /**Original name: TLI-IMP-LRD-DFZ<br>*/
    public AfDecimal getTliImpLrdDfz() {
        return readPackedAsDecimal(Pos.TLI_IMP_LRD_DFZ, Len.Int.TLI_IMP_LRD_DFZ, Len.Fract.TLI_IMP_LRD_DFZ);
    }

    public byte[] getTliImpLrdDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_IMP_LRD_DFZ, Pos.TLI_IMP_LRD_DFZ);
        return buffer;
    }

    public void setTliImpLrdDfzNull(String tliImpLrdDfzNull) {
        writeString(Pos.TLI_IMP_LRD_DFZ_NULL, tliImpLrdDfzNull, Len.TLI_IMP_LRD_DFZ_NULL);
    }

    /**Original name: TLI-IMP-LRD-DFZ-NULL<br>*/
    public String getTliImpLrdDfzNull() {
        return readString(Pos.TLI_IMP_LRD_DFZ_NULL, Len.TLI_IMP_LRD_DFZ_NULL);
    }

    public String getTliImpLrdDfzNullFormatted() {
        return Functions.padBlanks(getTliImpLrdDfzNull(), Len.TLI_IMP_LRD_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_IMP_LRD_DFZ = 1;
        public static final int TLI_IMP_LRD_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_IMP_LRD_DFZ = 8;
        public static final int TLI_IMP_LRD_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_IMP_LRD_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_IMP_LRD_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
