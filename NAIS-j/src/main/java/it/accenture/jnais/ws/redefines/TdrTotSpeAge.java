package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-SPE-AGE<br>
 * Variable: TDR-TOT-SPE-AGE from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotSpeAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotSpeAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_SPE_AGE;
    }

    public void setTdrTotSpeAge(AfDecimal tdrTotSpeAge) {
        writeDecimalAsPacked(Pos.TDR_TOT_SPE_AGE, tdrTotSpeAge.copy());
    }

    public void setTdrTotSpeAgeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_SPE_AGE, Pos.TDR_TOT_SPE_AGE);
    }

    /**Original name: TDR-TOT-SPE-AGE<br>*/
    public AfDecimal getTdrTotSpeAge() {
        return readPackedAsDecimal(Pos.TDR_TOT_SPE_AGE, Len.Int.TDR_TOT_SPE_AGE, Len.Fract.TDR_TOT_SPE_AGE);
    }

    public byte[] getTdrTotSpeAgeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_SPE_AGE, Pos.TDR_TOT_SPE_AGE);
        return buffer;
    }

    public void setTdrTotSpeAgeNull(String tdrTotSpeAgeNull) {
        writeString(Pos.TDR_TOT_SPE_AGE_NULL, tdrTotSpeAgeNull, Len.TDR_TOT_SPE_AGE_NULL);
    }

    /**Original name: TDR-TOT-SPE-AGE-NULL<br>*/
    public String getTdrTotSpeAgeNull() {
        return readString(Pos.TDR_TOT_SPE_AGE_NULL, Len.TDR_TOT_SPE_AGE_NULL);
    }

    public String getTdrTotSpeAgeNullFormatted() {
        return Functions.padBlanks(getTdrTotSpeAgeNull(), Len.TDR_TOT_SPE_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SPE_AGE = 1;
        public static final int TDR_TOT_SPE_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SPE_AGE = 8;
        public static final int TDR_TOT_SPE_AGE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SPE_AGE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SPE_AGE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
