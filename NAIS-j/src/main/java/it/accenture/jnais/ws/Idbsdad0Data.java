package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idbvdad3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndDfltAdes;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSDAD0<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsdad0Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-DFLT-ADES
    private IndDfltAdes indDfltAdes = new IndDfltAdes();
    //Original name: IDBVDAD3
    private Idbvdad3 idbvdad3 = new Idbvdad3();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idbvdad3 getIdbvdad3() {
        return idbvdad3;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndDfltAdes getIndDfltAdes() {
        return indDfltAdes;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
