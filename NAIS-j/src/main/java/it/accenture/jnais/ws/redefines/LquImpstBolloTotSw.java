package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-BOLLO-TOT-SW<br>
 * Variable: LQU-IMPST-BOLLO-TOT-SW from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpstBolloTotSw extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpstBolloTotSw() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST_BOLLO_TOT_SW;
    }

    public void setLquImpstBolloTotSw(AfDecimal lquImpstBolloTotSw) {
        writeDecimalAsPacked(Pos.LQU_IMPST_BOLLO_TOT_SW, lquImpstBolloTotSw.copy());
    }

    public void setLquImpstBolloTotSwFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST_BOLLO_TOT_SW, Pos.LQU_IMPST_BOLLO_TOT_SW);
    }

    /**Original name: LQU-IMPST-BOLLO-TOT-SW<br>*/
    public AfDecimal getLquImpstBolloTotSw() {
        return readPackedAsDecimal(Pos.LQU_IMPST_BOLLO_TOT_SW, Len.Int.LQU_IMPST_BOLLO_TOT_SW, Len.Fract.LQU_IMPST_BOLLO_TOT_SW);
    }

    public byte[] getLquImpstBolloTotSwAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST_BOLLO_TOT_SW, Pos.LQU_IMPST_BOLLO_TOT_SW);
        return buffer;
    }

    public void setLquImpstBolloTotSwNull(String lquImpstBolloTotSwNull) {
        writeString(Pos.LQU_IMPST_BOLLO_TOT_SW_NULL, lquImpstBolloTotSwNull, Len.LQU_IMPST_BOLLO_TOT_SW_NULL);
    }

    /**Original name: LQU-IMPST-BOLLO-TOT-SW-NULL<br>*/
    public String getLquImpstBolloTotSwNull() {
        return readString(Pos.LQU_IMPST_BOLLO_TOT_SW_NULL, Len.LQU_IMPST_BOLLO_TOT_SW_NULL);
    }

    public String getLquImpstBolloTotSwNullFormatted() {
        return Functions.padBlanks(getLquImpstBolloTotSwNull(), Len.LQU_IMPST_BOLLO_TOT_SW_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_BOLLO_TOT_SW = 1;
        public static final int LQU_IMPST_BOLLO_TOT_SW_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_BOLLO_TOT_SW = 8;
        public static final int LQU_IMPST_BOLLO_TOT_SW_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_BOLLO_TOT_SW = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_BOLLO_TOT_SW = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
