package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-VAL-RISC-END-LEAS<br>
 * Variable: WP67-VAL-RISC-END-LEAS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67ValRiscEndLeas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67ValRiscEndLeas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_VAL_RISC_END_LEAS;
    }

    public void setWp67ValRiscEndLeas(AfDecimal wp67ValRiscEndLeas) {
        writeDecimalAsPacked(Pos.WP67_VAL_RISC_END_LEAS, wp67ValRiscEndLeas.copy());
    }

    public void setWp67ValRiscEndLeasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_VAL_RISC_END_LEAS, Pos.WP67_VAL_RISC_END_LEAS);
    }

    /**Original name: WP67-VAL-RISC-END-LEAS<br>*/
    public AfDecimal getWp67ValRiscEndLeas() {
        return readPackedAsDecimal(Pos.WP67_VAL_RISC_END_LEAS, Len.Int.WP67_VAL_RISC_END_LEAS, Len.Fract.WP67_VAL_RISC_END_LEAS);
    }

    public byte[] getWp67ValRiscEndLeasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_VAL_RISC_END_LEAS, Pos.WP67_VAL_RISC_END_LEAS);
        return buffer;
    }

    public void setWp67ValRiscEndLeasNull(String wp67ValRiscEndLeasNull) {
        writeString(Pos.WP67_VAL_RISC_END_LEAS_NULL, wp67ValRiscEndLeasNull, Len.WP67_VAL_RISC_END_LEAS_NULL);
    }

    /**Original name: WP67-VAL-RISC-END-LEAS-NULL<br>*/
    public String getWp67ValRiscEndLeasNull() {
        return readString(Pos.WP67_VAL_RISC_END_LEAS_NULL, Len.WP67_VAL_RISC_END_LEAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_VAL_RISC_END_LEAS = 1;
        public static final int WP67_VAL_RISC_END_LEAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_VAL_RISC_END_LEAS = 8;
        public static final int WP67_VAL_RISC_END_LEAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_VAL_RISC_END_LEAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_VAL_RISC_END_LEAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
