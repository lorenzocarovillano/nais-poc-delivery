package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.occurs.WranTabRappAnag;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0109<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0109Data {

    //==== PROPERTIES ====
    public static final int DRAN_TAB_RAPP_ANAG_MAXOCCURS = 100;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0109";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: DRAN-ELE-RAN-MAX
    private short dranEleRanMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DRAN-TAB-RAPP-ANAG
    private WranTabRappAnag[] dranTabRappAnag = new WranTabRappAnag[DRAN_TAB_RAPP_ANAG_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-RAN
    private short ixTabRan = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCC1751
    private Lccc1751 lccc1751 = new Lccc1751();

    //==== CONSTRUCTORS ====
    public Lvvs0109Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dranTabRappAnagIdx = 1; dranTabRappAnagIdx <= DRAN_TAB_RAPP_ANAG_MAXOCCURS; dranTabRappAnagIdx++) {
            dranTabRappAnag[dranTabRappAnagIdx - 1] = new WranTabRappAnag();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setDranAreaRanFormatted(String data) {
        byte[] buffer = new byte[Len.DRAN_AREA_RAN];
        MarshalByte.writeString(buffer, 1, data, Len.DRAN_AREA_RAN);
        setDranAreaRanBytes(buffer, 1);
    }

    public void setDranAreaRanBytes(byte[] buffer, int offset) {
        int position = offset;
        dranEleRanMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DRAN_TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dranTabRappAnag[idx - 1].setWranTabRappAnagBytes(buffer, position);
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
            else {
                dranTabRappAnag[idx - 1].initWranTabRappAnagSpaces();
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
        }
    }

    public void setDranEleRanMax(short dranEleRanMax) {
        this.dranEleRanMax = dranEleRanMax;
    }

    public short getDranEleRanMax() {
        return this.dranEleRanMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabRan(short ixTabRan) {
        this.ixTabRan = ixTabRan;
    }

    public short getIxTabRan() {
        return this.ixTabRan;
    }

    public WranTabRappAnag getDranTabRappAnag(int idx) {
        return dranTabRappAnag[idx - 1];
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccc1751 getLccc1751() {
        return lccc1751;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_DT_APPOGGIO = 8;
        public static final int WK_RESTO = 2;
        public static final int WK_INTERO = 5;
        public static final int GG_DIFF = 5;
        public static final int GG_DIFF1 = 5;
        public static final int DRAN_ELE_RAN_MAX = 2;
        public static final int DRAN_AREA_RAN = DRAN_ELE_RAN_MAX + Lvvs0109Data.DRAN_TAB_RAPP_ANAG_MAXOCCURS * WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
