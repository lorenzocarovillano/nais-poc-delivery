package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-PC-PRE-TRASFERITO<br>
 * Variable: OCO-PC-PRE-TRASFERITO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoPcPreTrasferito extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoPcPreTrasferito() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_PC_PRE_TRASFERITO;
    }

    public void setOcoPcPreTrasferito(AfDecimal ocoPcPreTrasferito) {
        writeDecimalAsPacked(Pos.OCO_PC_PRE_TRASFERITO, ocoPcPreTrasferito.copy());
    }

    public void setOcoPcPreTrasferitoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_PC_PRE_TRASFERITO, Pos.OCO_PC_PRE_TRASFERITO);
    }

    /**Original name: OCO-PC-PRE-TRASFERITO<br>*/
    public AfDecimal getOcoPcPreTrasferito() {
        return readPackedAsDecimal(Pos.OCO_PC_PRE_TRASFERITO, Len.Int.OCO_PC_PRE_TRASFERITO, Len.Fract.OCO_PC_PRE_TRASFERITO);
    }

    public byte[] getOcoPcPreTrasferitoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_PC_PRE_TRASFERITO, Pos.OCO_PC_PRE_TRASFERITO);
        return buffer;
    }

    public void setOcoPcPreTrasferitoNull(String ocoPcPreTrasferitoNull) {
        writeString(Pos.OCO_PC_PRE_TRASFERITO_NULL, ocoPcPreTrasferitoNull, Len.OCO_PC_PRE_TRASFERITO_NULL);
    }

    /**Original name: OCO-PC-PRE-TRASFERITO-NULL<br>*/
    public String getOcoPcPreTrasferitoNull() {
        return readString(Pos.OCO_PC_PRE_TRASFERITO_NULL, Len.OCO_PC_PRE_TRASFERITO_NULL);
    }

    public String getOcoPcPreTrasferitoNullFormatted() {
        return Functions.padBlanks(getOcoPcPreTrasferitoNull(), Len.OCO_PC_PRE_TRASFERITO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_PC_PRE_TRASFERITO = 1;
        public static final int OCO_PC_PRE_TRASFERITO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_PC_PRE_TRASFERITO = 4;
        public static final int OCO_PC_PRE_TRASFERITO_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_PC_PRE_TRASFERITO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_PC_PRE_TRASFERITO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
