package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;

/**Original name: STRUCT-ALFA<br>
 * Variable: STRUCT-ALFA from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class StructAlfa extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELE_ALFA_MAXOCCURS = 60;

    //==== CONSTRUCTORS ====
    public StructAlfa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.STRUCT_ALFA;
    }

    public void setStructAlfa(String structAlfa) {
        writeString(Pos.STRUCT_ALFA, structAlfa, Len.STRUCT_ALFA);
    }

    /**Original name: STRUCT-ALFA<br>*/
    public String getStructAlfa() {
        return readString(Pos.STRUCT_ALFA, Len.STRUCT_ALFA);
    }

    /**Original name: ELE-ALFA<br>*/
    public char getEleAlfa(int eleAlfaIdx) {
        int position = Pos.eleAlfa(eleAlfaIdx - 1);
        return readChar(position);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int STRUCT_ALFA = 1;
        public static final int ELEMENT_ALFA = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int eleAlfa(int idx) {
            return ELEMENT_ALFA + idx * Len.ELE_ALFA;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_ALFA = 1;
        public static final int STRUCT_ALFA = 60;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
