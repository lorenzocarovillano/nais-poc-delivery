package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-PREST-FAIVL<br>
 * Variable: RST-RIS-PREST-FAIVL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisPrestFaivl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisPrestFaivl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_PREST_FAIVL;
    }

    public void setRstRisPrestFaivl(AfDecimal rstRisPrestFaivl) {
        writeDecimalAsPacked(Pos.RST_RIS_PREST_FAIVL, rstRisPrestFaivl.copy());
    }

    public void setRstRisPrestFaivlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_PREST_FAIVL, Pos.RST_RIS_PREST_FAIVL);
    }

    /**Original name: RST-RIS-PREST-FAIVL<br>*/
    public AfDecimal getRstRisPrestFaivl() {
        return readPackedAsDecimal(Pos.RST_RIS_PREST_FAIVL, Len.Int.RST_RIS_PREST_FAIVL, Len.Fract.RST_RIS_PREST_FAIVL);
    }

    public byte[] getRstRisPrestFaivlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_PREST_FAIVL, Pos.RST_RIS_PREST_FAIVL);
        return buffer;
    }

    public void setRstRisPrestFaivlNull(String rstRisPrestFaivlNull) {
        writeString(Pos.RST_RIS_PREST_FAIVL_NULL, rstRisPrestFaivlNull, Len.RST_RIS_PREST_FAIVL_NULL);
    }

    /**Original name: RST-RIS-PREST-FAIVL-NULL<br>*/
    public String getRstRisPrestFaivlNull() {
        return readString(Pos.RST_RIS_PREST_FAIVL_NULL, Len.RST_RIS_PREST_FAIVL_NULL);
    }

    public String getRstRisPrestFaivlNullFormatted() {
        return Functions.padBlanks(getRstRisPrestFaivlNull(), Len.RST_RIS_PREST_FAIVL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_PREST_FAIVL = 1;
        public static final int RST_RIS_PREST_FAIVL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_PREST_FAIVL = 8;
        public static final int RST_RIS_PREST_FAIVL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_PREST_FAIVL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_PREST_FAIVL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
