package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-ID-GAR<br>
 * Variable: PVT-ID-GAR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtIdGar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtIdGar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_ID_GAR;
    }

    public void setPvtIdGar(int pvtIdGar) {
        writeIntAsPacked(Pos.PVT_ID_GAR, pvtIdGar, Len.Int.PVT_ID_GAR);
    }

    public void setPvtIdGarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_ID_GAR, Pos.PVT_ID_GAR);
    }

    /**Original name: PVT-ID-GAR<br>*/
    public int getPvtIdGar() {
        return readPackedAsInt(Pos.PVT_ID_GAR, Len.Int.PVT_ID_GAR);
    }

    public byte[] getPvtIdGarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_ID_GAR, Pos.PVT_ID_GAR);
        return buffer;
    }

    public void setPvtIdGarNull(String pvtIdGarNull) {
        writeString(Pos.PVT_ID_GAR_NULL, pvtIdGarNull, Len.PVT_ID_GAR_NULL);
    }

    /**Original name: PVT-ID-GAR-NULL<br>*/
    public String getPvtIdGarNull() {
        return readString(Pos.PVT_ID_GAR_NULL, Len.PVT_ID_GAR_NULL);
    }

    public String getPvtIdGarNullFormatted() {
        return Functions.padBlanks(getPvtIdGarNull(), Len.PVT_ID_GAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_ID_GAR = 1;
        public static final int PVT_ID_GAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_ID_GAR = 5;
        public static final int PVT_ID_GAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_ID_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
