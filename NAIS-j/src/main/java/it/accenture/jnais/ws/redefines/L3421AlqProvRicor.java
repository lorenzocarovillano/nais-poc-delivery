package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-ALQ-PROV-RICOR<br>
 * Variable: L3421-ALQ-PROV-RICOR from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421AlqProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421AlqProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_ALQ_PROV_RICOR;
    }

    public void setL3421AlqProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_ALQ_PROV_RICOR, Pos.L3421_ALQ_PROV_RICOR);
    }

    /**Original name: L3421-ALQ-PROV-RICOR<br>*/
    public AfDecimal getL3421AlqProvRicor() {
        return readPackedAsDecimal(Pos.L3421_ALQ_PROV_RICOR, Len.Int.L3421_ALQ_PROV_RICOR, Len.Fract.L3421_ALQ_PROV_RICOR);
    }

    public byte[] getL3421AlqProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_ALQ_PROV_RICOR, Pos.L3421_ALQ_PROV_RICOR);
        return buffer;
    }

    /**Original name: L3421-ALQ-PROV-RICOR-NULL<br>*/
    public String getL3421AlqProvRicorNull() {
        return readString(Pos.L3421_ALQ_PROV_RICOR_NULL, Len.L3421_ALQ_PROV_RICOR_NULL);
    }

    public String getL3421AlqProvRicorNullFormatted() {
        return Functions.padBlanks(getL3421AlqProvRicorNull(), Len.L3421_ALQ_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_ALQ_PROV_RICOR = 1;
        public static final int L3421_ALQ_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_ALQ_PROV_RICOR = 4;
        public static final int L3421_ALQ_PROV_RICOR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_ALQ_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_ALQ_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
