package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTLI-ID-GAR-LIQ<br>
 * Variable: WTLI-ID-GAR-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliIdGarLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliIdGarLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_ID_GAR_LIQ;
    }

    public void setWtliIdGarLiq(int wtliIdGarLiq) {
        writeIntAsPacked(Pos.WTLI_ID_GAR_LIQ, wtliIdGarLiq, Len.Int.WTLI_ID_GAR_LIQ);
    }

    public void setWtliIdGarLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_ID_GAR_LIQ, Pos.WTLI_ID_GAR_LIQ);
    }

    /**Original name: WTLI-ID-GAR-LIQ<br>*/
    public int getWtliIdGarLiq() {
        return readPackedAsInt(Pos.WTLI_ID_GAR_LIQ, Len.Int.WTLI_ID_GAR_LIQ);
    }

    public byte[] getWtliIdGarLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_ID_GAR_LIQ, Pos.WTLI_ID_GAR_LIQ);
        return buffer;
    }

    public void initWtliIdGarLiqSpaces() {
        fill(Pos.WTLI_ID_GAR_LIQ, Len.WTLI_ID_GAR_LIQ, Types.SPACE_CHAR);
    }

    public void setWtliIdGarLiqNull(String wtliIdGarLiqNull) {
        writeString(Pos.WTLI_ID_GAR_LIQ_NULL, wtliIdGarLiqNull, Len.WTLI_ID_GAR_LIQ_NULL);
    }

    /**Original name: WTLI-ID-GAR-LIQ-NULL<br>*/
    public String getWtliIdGarLiqNull() {
        return readString(Pos.WTLI_ID_GAR_LIQ_NULL, Len.WTLI_ID_GAR_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_ID_GAR_LIQ = 1;
        public static final int WTLI_ID_GAR_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_ID_GAR_LIQ = 5;
        public static final int WTLI_ID_GAR_LIQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_ID_GAR_LIQ = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
