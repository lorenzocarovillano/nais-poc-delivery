package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRL-ID-MOVI-CHIU<br>
 * Variable: GRL-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrlIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrlIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRL_ID_MOVI_CHIU;
    }

    public void setGrlIdMoviChiu(int grlIdMoviChiu) {
        writeIntAsPacked(Pos.GRL_ID_MOVI_CHIU, grlIdMoviChiu, Len.Int.GRL_ID_MOVI_CHIU);
    }

    public void setGrlIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRL_ID_MOVI_CHIU, Pos.GRL_ID_MOVI_CHIU);
    }

    /**Original name: GRL-ID-MOVI-CHIU<br>*/
    public int getGrlIdMoviChiu() {
        return readPackedAsInt(Pos.GRL_ID_MOVI_CHIU, Len.Int.GRL_ID_MOVI_CHIU);
    }

    public byte[] getGrlIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRL_ID_MOVI_CHIU, Pos.GRL_ID_MOVI_CHIU);
        return buffer;
    }

    public void setGrlIdMoviChiuNull(String grlIdMoviChiuNull) {
        writeString(Pos.GRL_ID_MOVI_CHIU_NULL, grlIdMoviChiuNull, Len.GRL_ID_MOVI_CHIU_NULL);
    }

    /**Original name: GRL-ID-MOVI-CHIU-NULL<br>*/
    public String getGrlIdMoviChiuNull() {
        return readString(Pos.GRL_ID_MOVI_CHIU_NULL, Len.GRL_ID_MOVI_CHIU_NULL);
    }

    public String getGrlIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getGrlIdMoviChiuNull(), Len.GRL_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRL_ID_MOVI_CHIU = 1;
        public static final int GRL_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRL_ID_MOVI_CHIU = 5;
        public static final int GRL_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRL_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
