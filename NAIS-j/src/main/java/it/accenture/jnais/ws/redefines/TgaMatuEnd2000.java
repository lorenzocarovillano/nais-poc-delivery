package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-MATU-END2000<br>
 * Variable: TGA-MATU-END2000 from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaMatuEnd2000 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaMatuEnd2000() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_MATU_END2000;
    }

    public void setTgaMatuEnd2000(AfDecimal tgaMatuEnd2000) {
        writeDecimalAsPacked(Pos.TGA_MATU_END2000, tgaMatuEnd2000.copy());
    }

    public void setTgaMatuEnd2000FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_MATU_END2000, Pos.TGA_MATU_END2000);
    }

    /**Original name: TGA-MATU-END2000<br>*/
    public AfDecimal getTgaMatuEnd2000() {
        return readPackedAsDecimal(Pos.TGA_MATU_END2000, Len.Int.TGA_MATU_END2000, Len.Fract.TGA_MATU_END2000);
    }

    public byte[] getTgaMatuEnd2000AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_MATU_END2000, Pos.TGA_MATU_END2000);
        return buffer;
    }

    public void setTgaMatuEnd2000Null(String tgaMatuEnd2000Null) {
        writeString(Pos.TGA_MATU_END2000_NULL, tgaMatuEnd2000Null, Len.TGA_MATU_END2000_NULL);
    }

    /**Original name: TGA-MATU-END2000-NULL<br>*/
    public String getTgaMatuEnd2000Null() {
        return readString(Pos.TGA_MATU_END2000_NULL, Len.TGA_MATU_END2000_NULL);
    }

    public String getTgaMatuEnd2000NullFormatted() {
        return Functions.padBlanks(getTgaMatuEnd2000Null(), Len.TGA_MATU_END2000_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_MATU_END2000 = 1;
        public static final int TGA_MATU_END2000_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_MATU_END2000 = 8;
        public static final int TGA_MATU_END2000_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_MATU_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_MATU_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
