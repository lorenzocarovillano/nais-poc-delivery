package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-DT-RICH-ADD-RID<br>
 * Variable: TIT-DT-RICH-ADD-RID from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitDtRichAddRid extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitDtRichAddRid() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_DT_RICH_ADD_RID;
    }

    public void setTitDtRichAddRid(int titDtRichAddRid) {
        writeIntAsPacked(Pos.TIT_DT_RICH_ADD_RID, titDtRichAddRid, Len.Int.TIT_DT_RICH_ADD_RID);
    }

    public void setTitDtRichAddRidFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_DT_RICH_ADD_RID, Pos.TIT_DT_RICH_ADD_RID);
    }

    /**Original name: TIT-DT-RICH-ADD-RID<br>*/
    public int getTitDtRichAddRid() {
        return readPackedAsInt(Pos.TIT_DT_RICH_ADD_RID, Len.Int.TIT_DT_RICH_ADD_RID);
    }

    public byte[] getTitDtRichAddRidAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_DT_RICH_ADD_RID, Pos.TIT_DT_RICH_ADD_RID);
        return buffer;
    }

    public void setTitDtRichAddRidNull(String titDtRichAddRidNull) {
        writeString(Pos.TIT_DT_RICH_ADD_RID_NULL, titDtRichAddRidNull, Len.TIT_DT_RICH_ADD_RID_NULL);
    }

    /**Original name: TIT-DT-RICH-ADD-RID-NULL<br>*/
    public String getTitDtRichAddRidNull() {
        return readString(Pos.TIT_DT_RICH_ADD_RID_NULL, Len.TIT_DT_RICH_ADD_RID_NULL);
    }

    public String getTitDtRichAddRidNullFormatted() {
        return Functions.padBlanks(getTitDtRichAddRidNull(), Len.TIT_DT_RICH_ADD_RID_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_DT_RICH_ADD_RID = 1;
        public static final int TIT_DT_RICH_ADD_RID_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_DT_RICH_ADD_RID = 5;
        public static final int TIT_DT_RICH_ADD_RID_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_DT_RICH_ADD_RID = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
