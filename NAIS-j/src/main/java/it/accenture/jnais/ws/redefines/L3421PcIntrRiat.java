package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PC-INTR-RIAT<br>
 * Variable: L3421-PC-INTR-RIAT from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PcIntrRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PcIntrRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PC_INTR_RIAT;
    }

    public void setL3421PcIntrRiatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PC_INTR_RIAT, Pos.L3421_PC_INTR_RIAT);
    }

    /**Original name: L3421-PC-INTR-RIAT<br>*/
    public AfDecimal getL3421PcIntrRiat() {
        return readPackedAsDecimal(Pos.L3421_PC_INTR_RIAT, Len.Int.L3421_PC_INTR_RIAT, Len.Fract.L3421_PC_INTR_RIAT);
    }

    public byte[] getL3421PcIntrRiatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PC_INTR_RIAT, Pos.L3421_PC_INTR_RIAT);
        return buffer;
    }

    /**Original name: L3421-PC-INTR-RIAT-NULL<br>*/
    public String getL3421PcIntrRiatNull() {
        return readString(Pos.L3421_PC_INTR_RIAT_NULL, Len.L3421_PC_INTR_RIAT_NULL);
    }

    public String getL3421PcIntrRiatNullFormatted() {
        return Functions.padBlanks(getL3421PcIntrRiatNull(), Len.L3421_PC_INTR_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PC_INTR_RIAT = 1;
        public static final int L3421_PC_INTR_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PC_INTR_RIAT = 4;
        public static final int L3421_PC_INTR_RIAT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PC_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PC_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
