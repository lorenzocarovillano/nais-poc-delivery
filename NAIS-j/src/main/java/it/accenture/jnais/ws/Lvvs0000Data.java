package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.TabGiorniLvvs0000;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0000<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0000Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0000";
    //Original name: WK-DATA-OUTPUT-V
    private WkDataOutputV wkDataOutputV = new WkDataOutputV();
    //Original name: WK-APPO-DEC-V
    private WkAppoDecV wkAppoDecV = new WkAppoDecV();
    //Original name: WK-DATA-INPUT
    private WkDataInput wkDataInput = new WkDataInput();
    //Original name: WK-APPO-MESI
    private int wkAppoMesi = 0;
    //Original name: WK-APPO-ANNI
    private int wkAppoAnni = 0;
    //Original name: TAB-GIORNI
    private TabGiorniLvvs0000 tabGiorni = new TabGiorniLvvs0000();
    //Original name: IND-MESE
    private short indMese = ((short)0);
    //Original name: WK-ANNO-DIVISIONE
    private short wkAnnoDivisione = DefaultValues.SHORT_VAL;
    //Original name: RESTO
    private short resto = ((short)0);
    //Original name: RISULT
    private short risult = ((short)0);
    //Original name: WK-GG-APPOGGIO
    private short wkGgAppoggio = ((short)0);
    //Original name: WK-GG-DA-SOMMARE
    private String wkGgDaSommare = "00";
    //Original name: WK-MESI
    private short wkMesi = ((short)0);

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkAppoMesi(int wkAppoMesi) {
        this.wkAppoMesi = wkAppoMesi;
    }

    public int getWkAppoMesi() {
        return this.wkAppoMesi;
    }

    public void setWkAppoAnni(int wkAppoAnni) {
        this.wkAppoAnni = wkAppoAnni;
    }

    public int getWkAppoAnni() {
        return this.wkAppoAnni;
    }

    public void setIndMese(short indMese) {
        this.indMese = indMese;
    }

    public short getIndMese() {
        return this.indMese;
    }

    public void setWkAnnoDivisione(short wkAnnoDivisione) {
        this.wkAnnoDivisione = wkAnnoDivisione;
    }

    public short getWkAnnoDivisione() {
        return this.wkAnnoDivisione;
    }

    public void setResto(short resto) {
        this.resto = resto;
    }

    public short getResto() {
        return this.resto;
    }

    public void setRisult(short risult) {
        this.risult = risult;
    }

    public short getRisult() {
        return this.risult;
    }

    public void setWkGgAppoggio(short wkGgAppoggio) {
        this.wkGgAppoggio = wkGgAppoggio;
    }

    public short getWkGgAppoggio() {
        return this.wkGgAppoggio;
    }

    public void setWkGgDaSommare(short wkGgDaSommare) {
        this.wkGgDaSommare = NumericDisplay.asString(wkGgDaSommare, Len.WK_GG_DA_SOMMARE);
    }

    public void setWkGgDaSommareFormatted(String wkGgDaSommare) {
        this.wkGgDaSommare = Trunc.toUnsignedNumeric(wkGgDaSommare, Len.WK_GG_DA_SOMMARE);
    }

    public short getWkGgDaSommare() {
        return NumericDisplay.asShort(this.wkGgDaSommare);
    }

    public String getWkGgDaSommareFormatted() {
        return this.wkGgDaSommare;
    }

    public void setWkMesi(short wkMesi) {
        this.wkMesi = wkMesi;
    }

    public short getWkMesi() {
        return this.wkMesi;
    }

    public TabGiorniLvvs0000 getTabGiorni() {
        return tabGiorni;
    }

    public WkAppoDecV getWkAppoDecV() {
        return wkAppoDecV;
    }

    public WkDataInput getWkDataInput() {
        return wkDataInput;
    }

    public WkDataOutputV getWkDataOutputV() {
        return wkDataOutputV;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_APPO_MESI = 9;
        public static final int WK_APPO_ANNI = 9;
        public static final int WK_ANNO_DIVISIONE = 4;
        public static final int RESTO = 1;
        public static final int RISULT = 3;
        public static final int WK_GG_APPOGGIO = 4;
        public static final int WK_GG_DA_SOMMARE = 2;
        public static final int WK_MESI = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
