package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IABV0006-DESC-TYPE<br>
 * Variable: IABV0006-DESC-TYPE from copybook IABV0006<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0006DescType {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char ONLY_DESC = 'D';
    public static final char COUNTER_DESC = 'C';

    //==== METHODS ====
    public void setDescType(char descType) {
        this.value = descType;
    }

    public char getDescType() {
        return this.value;
    }

    public boolean isIabv0006OnlyDesc() {
        return value == ONLY_DESC;
    }

    public void setIabv0006CounterDesc() {
        value = COUNTER_DESC;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESC_TYPE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
