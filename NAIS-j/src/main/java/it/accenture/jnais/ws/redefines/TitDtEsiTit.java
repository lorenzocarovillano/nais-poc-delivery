package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-DT-ESI-TIT<br>
 * Variable: TIT-DT-ESI-TIT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitDtEsiTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitDtEsiTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_DT_ESI_TIT;
    }

    public void setTitDtEsiTit(int titDtEsiTit) {
        writeIntAsPacked(Pos.TIT_DT_ESI_TIT, titDtEsiTit, Len.Int.TIT_DT_ESI_TIT);
    }

    public void setTitDtEsiTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_DT_ESI_TIT, Pos.TIT_DT_ESI_TIT);
    }

    /**Original name: TIT-DT-ESI-TIT<br>*/
    public int getTitDtEsiTit() {
        return readPackedAsInt(Pos.TIT_DT_ESI_TIT, Len.Int.TIT_DT_ESI_TIT);
    }

    public byte[] getTitDtEsiTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_DT_ESI_TIT, Pos.TIT_DT_ESI_TIT);
        return buffer;
    }

    public void setTitDtEsiTitNull(String titDtEsiTitNull) {
        writeString(Pos.TIT_DT_ESI_TIT_NULL, titDtEsiTitNull, Len.TIT_DT_ESI_TIT_NULL);
    }

    /**Original name: TIT-DT-ESI-TIT-NULL<br>*/
    public String getTitDtEsiTitNull() {
        return readString(Pos.TIT_DT_ESI_TIT_NULL, Len.TIT_DT_ESI_TIT_NULL);
    }

    public String getTitDtEsiTitNullFormatted() {
        return Functions.padBlanks(getTitDtEsiTitNull(), Len.TIT_DT_ESI_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_DT_ESI_TIT = 1;
        public static final int TIT_DT_ESI_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_DT_ESI_TIT = 5;
        public static final int TIT_DT_ESI_TIT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_DT_ESI_TIT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
