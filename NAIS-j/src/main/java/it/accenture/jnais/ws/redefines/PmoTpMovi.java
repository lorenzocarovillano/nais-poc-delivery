package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-TP-MOVI<br>
 * Variable: PMO-TP-MOVI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoTpMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoTpMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_TP_MOVI;
    }

    public void setPmoTpMovi(int pmoTpMovi) {
        writeIntAsPacked(Pos.PMO_TP_MOVI, pmoTpMovi, Len.Int.PMO_TP_MOVI);
    }

    public void setPmoTpMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_TP_MOVI, Pos.PMO_TP_MOVI);
    }

    /**Original name: PMO-TP-MOVI<br>*/
    public int getPmoTpMovi() {
        return readPackedAsInt(Pos.PMO_TP_MOVI, Len.Int.PMO_TP_MOVI);
    }

    public byte[] getPmoTpMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_TP_MOVI, Pos.PMO_TP_MOVI);
        return buffer;
    }

    public void initPmoTpMoviHighValues() {
        fill(Pos.PMO_TP_MOVI, Len.PMO_TP_MOVI, Types.HIGH_CHAR_VAL);
    }

    public void setPmoTpMoviNull(String pmoTpMoviNull) {
        writeString(Pos.PMO_TP_MOVI_NULL, pmoTpMoviNull, Len.PMO_TP_MOVI_NULL);
    }

    /**Original name: PMO-TP-MOVI-NULL<br>*/
    public String getPmoTpMoviNull() {
        return readString(Pos.PMO_TP_MOVI_NULL, Len.PMO_TP_MOVI_NULL);
    }

    public String getPmoTpMoviNullFormatted() {
        return Functions.padBlanks(getPmoTpMoviNull(), Len.PMO_TP_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_TP_MOVI = 1;
        public static final int PMO_TP_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_TP_MOVI = 3;
        public static final int PMO_TP_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_TP_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
