package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccc00011;
import it.accenture.jnais.copy.Lccc0261;
import it.accenture.jnais.copy.WcomDatiBlocco;
import it.accenture.jnais.copy.WcomDatiDeroghe;
import it.accenture.jnais.copy.WcomDatiMbs;
import it.accenture.jnais.copy.WcomDatiPolizza;
import it.accenture.jnais.copy.WcomStati;
import it.accenture.jnais.copy.WcomTastiDaAbilitare;
import it.accenture.jnais.ws.enums.WcomBsCallType;
import it.accenture.jnais.ws.enums.WcomModificaDtdecor;
import it.accenture.jnais.ws.enums.WcomNavigabilita;
import it.accenture.jnais.ws.enums.WcomPrimaVolta;
import it.accenture.jnais.ws.enums.WcomStepSucc;
import it.accenture.jnais.ws.enums.WcomTipoOperazione;
import it.accenture.jnais.ws.enums.WcomTpVisualizPag;
import it.accenture.jnais.ws.enums.WcomWrite;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: AREA-PASSAGGIO<br>
 * Variable: AREA-PASSAGGIO from program LOAS0110<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaPassaggio extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LCCC0001
    private Lccc00011 lccc0001 = new Lccc00011();
    //Original name: LCCC0261
    private Lccc0261 lccc0261 = new Lccc0261();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_PASSAGGIO;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaPassaggioBytes(buf);
    }

    public String getAreaPassaggioFormatted() {
        return MarshalByteExt.bufferToStr(getAreaPassaggioBytes());
    }

    public void setAreaPassaggioBytes(byte[] buffer) {
        setAreaPassaggioBytes(buffer, 1);
    }

    public byte[] getAreaPassaggioBytes() {
        byte[] buffer = new byte[Len.AREA_PASSAGGIO];
        return getAreaPassaggioBytes(buffer, 1);
    }

    public void setAreaPassaggioBytes(byte[] buffer, int offset) {
        int position = offset;
        setWcomAreaStatiBytes(buffer, position);
        position += Len.WCOM_AREA_STATI;
        setWkAreaLccc0261Bytes(buffer, position);
    }

    public byte[] getAreaPassaggioBytes(byte[] buffer, int offset) {
        int position = offset;
        getWcomAreaStatiBytes(buffer, position);
        position += Len.WCOM_AREA_STATI;
        getWkAreaLccc0261Bytes(buffer, position);
        return buffer;
    }

    /**Original name: WCOM-AREA-STATI<br>*/
    public byte[] getWcomAreaStatiBytes() {
        byte[] buffer = new byte[Len.WCOM_AREA_STATI];
        return getWcomAreaStatiBytes(buffer, 1);
    }

    public void setWcomAreaStatiBytes(byte[] buffer, int offset) {
        int position = offset;
        lccc0001.getTipoOperazione().setTipoOperazione(MarshalByte.readString(buffer, position, WcomTipoOperazione.Len.TIPO_OPERAZIONE));
        position += WcomTipoOperazione.Len.TIPO_OPERAZIONE;
        lccc0001.getNavigabilita().setNavigabilita(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.getTastiDaAbilitare().setTastiDaAbilitareBytes(buffer, position);
        position += WcomTastiDaAbilitare.Len.TASTI_DA_ABILITARE;
        lccc0001.setDatiActuatorBytes(buffer, position);
        position += Lccc00011.Len.DATI_ACTUATOR;
        lccc0001.getStati().setStatiBytes(buffer, position);
        position += WcomStati.Len.STATI;
        lccc0001.getDatiDeroghe().setDatiDerogheBytes(buffer, position);
        position += WcomDatiDeroghe.Len.DATI_DEROGHE;
        lccc0001.setMovimentiAnnullBytes(buffer, position);
        position += Lccc00011.Len.MOVIMENTI_ANNULL;
        lccc0001.setIdMoviCrz(MarshalByte.readPackedAsInt(buffer, position, Lccc00011.Len.Int.ID_MOVI_CRZ, 0));
        position += Lccc00011.Len.ID_MOVI_CRZ;
        lccc0001.setFlagTariffaRischio(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.setAreaPlatfondBytes(buffer, position);
        position += Lccc00011.Len.AREA_PLATFOND;
        lccc0001.getModificaDtdecor().setModificaDtdecor(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.getStatusDer().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.setIdRichEst(MarshalByte.readPackedAsInt(buffer, position, Lccc00011.Len.Int.ID_RICH_EST, 0));
        position += Lccc00011.Len.ID_RICH_EST;
        lccc0001.setCodiceIniziativa(MarshalByte.readString(buffer, position, Lccc00011.Len.CODICE_INIZIATIVA));
        position += Lccc00011.Len.CODICE_INIZIATIVA;
        lccc0001.getTpVisualizPag().setTpVisualizPag(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.setFlr1(MarshalByte.readString(buffer, position, Lccc00011.Len.FLR1));
    }

    public byte[] getWcomAreaStatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, lccc0001.getTipoOperazione().getTipoOperazione(), WcomTipoOperazione.Len.TIPO_OPERAZIONE);
        position += WcomTipoOperazione.Len.TIPO_OPERAZIONE;
        MarshalByte.writeChar(buffer, position, lccc0001.getNavigabilita().getNavigabilita());
        position += Types.CHAR_SIZE;
        lccc0001.getTastiDaAbilitare().getTastiDaAbilitareBytes(buffer, position);
        position += WcomTastiDaAbilitare.Len.TASTI_DA_ABILITARE;
        lccc0001.getDatiActuatorBytes(buffer, position);
        position += Lccc00011.Len.DATI_ACTUATOR;
        lccc0001.getStati().getStatiBytes(buffer, position);
        position += WcomStati.Len.STATI;
        lccc0001.getDatiDeroghe().getDatiDerogheBytes(buffer, position);
        position += WcomDatiDeroghe.Len.DATI_DEROGHE;
        lccc0001.getMovimentiAnnullBytes(buffer, position);
        position += Lccc00011.Len.MOVIMENTI_ANNULL;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0001.getIdMoviCrz(), Lccc00011.Len.Int.ID_MOVI_CRZ, 0);
        position += Lccc00011.Len.ID_MOVI_CRZ;
        MarshalByte.writeChar(buffer, position, lccc0001.getFlagTariffaRischio());
        position += Types.CHAR_SIZE;
        lccc0001.getAreaPlatfondBytes(buffer, position);
        position += Lccc00011.Len.AREA_PLATFOND;
        MarshalByte.writeChar(buffer, position, lccc0001.getModificaDtdecor().getModificaDtdecor());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, lccc0001.getStatusDer().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0001.getIdRichEst(), Lccc00011.Len.Int.ID_RICH_EST, 0);
        position += Lccc00011.Len.ID_RICH_EST;
        MarshalByte.writeString(buffer, position, lccc0001.getCodiceIniziativa(), Lccc00011.Len.CODICE_INIZIATIVA);
        position += Lccc00011.Len.CODICE_INIZIATIVA;
        MarshalByte.writeChar(buffer, position, lccc0001.getTpVisualizPag().getTpVisualizPag());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, lccc0001.getFlr1(), Lccc00011.Len.FLR1);
        return buffer;
    }

    public void setWkAreaLccc0261Bytes(byte[] buffer, int offset) {
        int position = offset;
        lccc0261.getBsCallType().setBsCallType(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0261.getDatiPolizza().setDatiPolizzaBytes(buffer, position);
        position += WcomDatiPolizza.Len.DATI_POLIZZA;
        lccc0261.setDatiAdesioneBytes(buffer, position);
        position += Lccc0261.Len.DATI_ADESIONE;
        lccc0261.setIdMoviCreaz(MarshalByte.readPackedAsInt(buffer, position, Lccc0261.Len.Int.ID_MOVI_CREAZ, 0));
        position += Lccc0261.Len.ID_MOVI_CREAZ;
        lccc0261.getPrimaVolta().setPrimaVolta(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0261.getStepSucc().setStepSucc(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0261.getWrite().setWrite(MarshalByte.readString(buffer, position, WcomWrite.Len.WRITE));
        position += WcomWrite.Len.WRITE;
        lccc0261.getDatiMbs().setDatiMbsBytes(buffer, position);
        position += WcomDatiMbs.Len.DATI_MBS;
        lccc0261.getDatiBlocco().setDatiBloccoBytes(buffer, position);
    }

    public byte[] getWkAreaLccc0261Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccc0261.getBsCallType().getBsCallType());
        position += Types.CHAR_SIZE;
        lccc0261.getDatiPolizza().getDatiPolizzaBytes(buffer, position);
        position += WcomDatiPolizza.Len.DATI_POLIZZA;
        lccc0261.getDatiAdesioneBytes(buffer, position);
        position += Lccc0261.Len.DATI_ADESIONE;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0261.getIdMoviCreaz(), Lccc0261.Len.Int.ID_MOVI_CREAZ, 0);
        position += Lccc0261.Len.ID_MOVI_CREAZ;
        MarshalByte.writeChar(buffer, position, lccc0261.getPrimaVolta().getPrimaVolta());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, lccc0261.getStepSucc().getStepSucc());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, lccc0261.getWrite().getWrite(), WcomWrite.Len.WRITE);
        position += WcomWrite.Len.WRITE;
        lccc0261.getDatiMbs().getDatiMbsBytes(buffer, position);
        position += WcomDatiMbs.Len.DATI_MBS;
        lccc0261.getDatiBlocco().getDatiBloccoBytes(buffer, position);
        return buffer;
    }

    public Lccc00011 getLccc0001() {
        return lccc0001;
    }

    public Lccc0261 getLccc0261() {
        return lccc0261;
    }

    @Override
    public byte[] serialize() {
        return getAreaPassaggioBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WCOM_AREA_STATI = WcomTipoOperazione.Len.TIPO_OPERAZIONE + WcomNavigabilita.Len.NAVIGABILITA + WcomTastiDaAbilitare.Len.TASTI_DA_ABILITARE + Lccc00011.Len.DATI_ACTUATOR + WcomStati.Len.STATI + WcomDatiDeroghe.Len.DATI_DEROGHE + Lccc00011.Len.MOVIMENTI_ANNULL + Lccc00011.Len.ID_MOVI_CRZ + Lccc00011.Len.FLAG_TARIFFA_RISCHIO + Lccc00011.Len.AREA_PLATFOND + WcomModificaDtdecor.Len.MODIFICA_DTDECOR + WpolStatus.Len.STATUS + Lccc00011.Len.ID_RICH_EST + Lccc00011.Len.CODICE_INIZIATIVA + WcomTpVisualizPag.Len.TP_VISUALIZ_PAG + Lccc00011.Len.FLR1;
        public static final int WK_AREA_LCCC0261 = WcomBsCallType.Len.BS_CALL_TYPE + WcomDatiPolizza.Len.DATI_POLIZZA + Lccc0261.Len.DATI_ADESIONE + Lccc0261.Len.ID_MOVI_CREAZ + WcomPrimaVolta.Len.PRIMA_VOLTA + WcomStepSucc.Len.STEP_SUCC + WcomWrite.Len.WRITE + WcomDatiMbs.Len.DATI_MBS + WcomDatiBlocco.Len.DATI_BLOCCO;
        public static final int AREA_PASSAGGIO = WCOM_AREA_STATI + WK_AREA_LCCC0261;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
