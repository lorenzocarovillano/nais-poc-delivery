package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-PILDI-TR-I<br>
 * Variable: WPCO-DT-ULTC-PILDI-TR-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcPildiTrI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcPildiTrI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_PILDI_TR_I;
    }

    public void setWpcoDtUltcPildiTrI(int wpcoDtUltcPildiTrI) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_PILDI_TR_I, wpcoDtUltcPildiTrI, Len.Int.WPCO_DT_ULTC_PILDI_TR_I);
    }

    public void setDpcoDtUltcPildiTrIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_PILDI_TR_I, Pos.WPCO_DT_ULTC_PILDI_TR_I);
    }

    /**Original name: WPCO-DT-ULTC-PILDI-TR-I<br>*/
    public int getWpcoDtUltcPildiTrI() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_PILDI_TR_I, Len.Int.WPCO_DT_ULTC_PILDI_TR_I);
    }

    public byte[] getWpcoDtUltcPildiTrIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_PILDI_TR_I, Pos.WPCO_DT_ULTC_PILDI_TR_I);
        return buffer;
    }

    public void setWpcoDtUltcPildiTrINull(String wpcoDtUltcPildiTrINull) {
        writeString(Pos.WPCO_DT_ULTC_PILDI_TR_I_NULL, wpcoDtUltcPildiTrINull, Len.WPCO_DT_ULTC_PILDI_TR_I_NULL);
    }

    /**Original name: WPCO-DT-ULTC-PILDI-TR-I-NULL<br>*/
    public String getWpcoDtUltcPildiTrINull() {
        return readString(Pos.WPCO_DT_ULTC_PILDI_TR_I_NULL, Len.WPCO_DT_ULTC_PILDI_TR_I_NULL);
    }

    public String getWpcoDtUltcPildiTrINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcPildiTrINull(), Len.WPCO_DT_ULTC_PILDI_TR_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_PILDI_TR_I = 1;
        public static final int WPCO_DT_ULTC_PILDI_TR_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_PILDI_TR_I = 5;
        public static final int WPCO_DT_ULTC_PILDI_TR_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_PILDI_TR_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
