package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-ETA-AA-1O-ASSTO<br>
 * Variable: GRZ-ETA-AA-1O-ASSTO from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzEtaAa1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzEtaAa1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_ETA_AA1O_ASSTO;
    }

    public void setGrzEtaAa1oAssto(short grzEtaAa1oAssto) {
        writeShortAsPacked(Pos.GRZ_ETA_AA1O_ASSTO, grzEtaAa1oAssto, Len.Int.GRZ_ETA_AA1O_ASSTO);
    }

    public void setGrzEtaAa1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_ETA_AA1O_ASSTO, Pos.GRZ_ETA_AA1O_ASSTO);
    }

    /**Original name: GRZ-ETA-AA-1O-ASSTO<br>*/
    public short getGrzEtaAa1oAssto() {
        return readPackedAsShort(Pos.GRZ_ETA_AA1O_ASSTO, Len.Int.GRZ_ETA_AA1O_ASSTO);
    }

    public byte[] getGrzEtaAa1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_ETA_AA1O_ASSTO, Pos.GRZ_ETA_AA1O_ASSTO);
        return buffer;
    }

    public void setGrzEtaAa1oAsstoNull(String grzEtaAa1oAsstoNull) {
        writeString(Pos.GRZ_ETA_AA1O_ASSTO_NULL, grzEtaAa1oAsstoNull, Len.GRZ_ETA_AA1O_ASSTO_NULL);
    }

    /**Original name: GRZ-ETA-AA-1O-ASSTO-NULL<br>*/
    public String getGrzEtaAa1oAsstoNull() {
        return readString(Pos.GRZ_ETA_AA1O_ASSTO_NULL, Len.GRZ_ETA_AA1O_ASSTO_NULL);
    }

    public String getGrzEtaAa1oAsstoNullFormatted() {
        return Functions.padBlanks(getGrzEtaAa1oAsstoNull(), Len.GRZ_ETA_AA1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_ETA_AA1O_ASSTO = 1;
        public static final int GRZ_ETA_AA1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_ETA_AA1O_ASSTO = 2;
        public static final int GRZ_ETA_AA1O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_ETA_AA1O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
