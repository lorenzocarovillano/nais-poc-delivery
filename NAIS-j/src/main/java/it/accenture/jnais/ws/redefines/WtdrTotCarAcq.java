package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-CAR-ACQ<br>
 * Variable: WTDR-TOT-CAR-ACQ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_CAR_ACQ;
    }

    public void setWtdrTotCarAcq(AfDecimal wtdrTotCarAcq) {
        writeDecimalAsPacked(Pos.WTDR_TOT_CAR_ACQ, wtdrTotCarAcq.copy());
    }

    /**Original name: WTDR-TOT-CAR-ACQ<br>*/
    public AfDecimal getWtdrTotCarAcq() {
        return readPackedAsDecimal(Pos.WTDR_TOT_CAR_ACQ, Len.Int.WTDR_TOT_CAR_ACQ, Len.Fract.WTDR_TOT_CAR_ACQ);
    }

    public void setWtdrTotCarAcqNull(String wtdrTotCarAcqNull) {
        writeString(Pos.WTDR_TOT_CAR_ACQ_NULL, wtdrTotCarAcqNull, Len.WTDR_TOT_CAR_ACQ_NULL);
    }

    /**Original name: WTDR-TOT-CAR-ACQ-NULL<br>*/
    public String getWtdrTotCarAcqNull() {
        return readString(Pos.WTDR_TOT_CAR_ACQ_NULL, Len.WTDR_TOT_CAR_ACQ_NULL);
    }

    public String getWtdrTotCarAcqNullFormatted() {
        return Functions.padBlanks(getWtdrTotCarAcqNull(), Len.WTDR_TOT_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_CAR_ACQ = 1;
        public static final int WTDR_TOT_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_CAR_ACQ = 8;
        public static final int WTDR_TOT_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
