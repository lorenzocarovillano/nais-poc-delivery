package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-MOV<br>
 * Variable: WK-MOV from program LOAS0820<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkMov {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_MOV);
    public static final String TROVATO = "S";
    public static final String NON_TROVATO = "N";

    //==== METHODS ====
    public void setWkMov(String wkMov) {
        this.value = Functions.subString(wkMov, Len.WK_MOV);
    }

    public String getWkMov() {
        return this.value;
    }

    public boolean isTrovato() {
        return value.equals(TROVATO);
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public boolean isNonTrovato() {
        return value.equals(NON_TROVATO);
    }

    public void setNonTrovato() {
        value = NON_TROVATO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_MOV = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
