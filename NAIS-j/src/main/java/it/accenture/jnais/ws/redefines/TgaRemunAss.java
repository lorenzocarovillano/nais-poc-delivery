package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-REMUN-ASS<br>
 * Variable: TGA-REMUN-ASS from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_REMUN_ASS;
    }

    public void setTgaRemunAss(AfDecimal tgaRemunAss) {
        writeDecimalAsPacked(Pos.TGA_REMUN_ASS, tgaRemunAss.copy());
    }

    public void setTgaRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_REMUN_ASS, Pos.TGA_REMUN_ASS);
    }

    /**Original name: TGA-REMUN-ASS<br>*/
    public AfDecimal getTgaRemunAss() {
        return readPackedAsDecimal(Pos.TGA_REMUN_ASS, Len.Int.TGA_REMUN_ASS, Len.Fract.TGA_REMUN_ASS);
    }

    public byte[] getTgaRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_REMUN_ASS, Pos.TGA_REMUN_ASS);
        return buffer;
    }

    public void setTgaRemunAssNull(String tgaRemunAssNull) {
        writeString(Pos.TGA_REMUN_ASS_NULL, tgaRemunAssNull, Len.TGA_REMUN_ASS_NULL);
    }

    /**Original name: TGA-REMUN-ASS-NULL<br>*/
    public String getTgaRemunAssNull() {
        return readString(Pos.TGA_REMUN_ASS_NULL, Len.TGA_REMUN_ASS_NULL);
    }

    public String getTgaRemunAssNullFormatted() {
        return Functions.padBlanks(getTgaRemunAssNull(), Len.TGA_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_REMUN_ASS = 1;
        public static final int TGA_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_REMUN_ASS = 8;
        public static final int TGA_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
