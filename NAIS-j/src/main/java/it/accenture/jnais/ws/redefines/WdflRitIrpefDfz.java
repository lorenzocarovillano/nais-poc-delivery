package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RIT-IRPEF-DFZ<br>
 * Variable: WDFL-RIT-IRPEF-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflRitIrpefDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflRitIrpefDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RIT_IRPEF_DFZ;
    }

    public void setWdflRitIrpefDfz(AfDecimal wdflRitIrpefDfz) {
        writeDecimalAsPacked(Pos.WDFL_RIT_IRPEF_DFZ, wdflRitIrpefDfz.copy());
    }

    public void setWdflRitIrpefDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RIT_IRPEF_DFZ, Pos.WDFL_RIT_IRPEF_DFZ);
    }

    /**Original name: WDFL-RIT-IRPEF-DFZ<br>*/
    public AfDecimal getWdflRitIrpefDfz() {
        return readPackedAsDecimal(Pos.WDFL_RIT_IRPEF_DFZ, Len.Int.WDFL_RIT_IRPEF_DFZ, Len.Fract.WDFL_RIT_IRPEF_DFZ);
    }

    public byte[] getWdflRitIrpefDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RIT_IRPEF_DFZ, Pos.WDFL_RIT_IRPEF_DFZ);
        return buffer;
    }

    public void setWdflRitIrpefDfzNull(String wdflRitIrpefDfzNull) {
        writeString(Pos.WDFL_RIT_IRPEF_DFZ_NULL, wdflRitIrpefDfzNull, Len.WDFL_RIT_IRPEF_DFZ_NULL);
    }

    /**Original name: WDFL-RIT-IRPEF-DFZ-NULL<br>*/
    public String getWdflRitIrpefDfzNull() {
        return readString(Pos.WDFL_RIT_IRPEF_DFZ_NULL, Len.WDFL_RIT_IRPEF_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_IRPEF_DFZ = 1;
        public static final int WDFL_RIT_IRPEF_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_IRPEF_DFZ = 8;
        public static final int WDFL_RIT_IRPEF_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_IRPEF_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_IRPEF_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
