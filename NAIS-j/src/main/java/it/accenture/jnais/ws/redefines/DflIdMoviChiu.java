package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ID-MOVI-CHIU<br>
 * Variable: DFL-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ID_MOVI_CHIU;
    }

    public void setDflIdMoviChiu(int dflIdMoviChiu) {
        writeIntAsPacked(Pos.DFL_ID_MOVI_CHIU, dflIdMoviChiu, Len.Int.DFL_ID_MOVI_CHIU);
    }

    public void setDflIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ID_MOVI_CHIU, Pos.DFL_ID_MOVI_CHIU);
    }

    /**Original name: DFL-ID-MOVI-CHIU<br>*/
    public int getDflIdMoviChiu() {
        return readPackedAsInt(Pos.DFL_ID_MOVI_CHIU, Len.Int.DFL_ID_MOVI_CHIU);
    }

    public byte[] getDflIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ID_MOVI_CHIU, Pos.DFL_ID_MOVI_CHIU);
        return buffer;
    }

    public void setDflIdMoviChiuNull(String dflIdMoviChiuNull) {
        writeString(Pos.DFL_ID_MOVI_CHIU_NULL, dflIdMoviChiuNull, Len.DFL_ID_MOVI_CHIU_NULL);
    }

    /**Original name: DFL-ID-MOVI-CHIU-NULL<br>*/
    public String getDflIdMoviChiuNull() {
        return readString(Pos.DFL_ID_MOVI_CHIU_NULL, Len.DFL_ID_MOVI_CHIU_NULL);
    }

    public String getDflIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getDflIdMoviChiuNull(), Len.DFL_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ID_MOVI_CHIU = 1;
        public static final int DFL_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ID_MOVI_CHIU = 5;
        public static final int DFL_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
