package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-REN-K1<br>
 * Variable: LQU-IMP-REN-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpRenK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpRenK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_REN_K1;
    }

    public void setLquImpRenK1(AfDecimal lquImpRenK1) {
        writeDecimalAsPacked(Pos.LQU_IMP_REN_K1, lquImpRenK1.copy());
    }

    public void setLquImpRenK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_REN_K1, Pos.LQU_IMP_REN_K1);
    }

    /**Original name: LQU-IMP-REN-K1<br>*/
    public AfDecimal getLquImpRenK1() {
        return readPackedAsDecimal(Pos.LQU_IMP_REN_K1, Len.Int.LQU_IMP_REN_K1, Len.Fract.LQU_IMP_REN_K1);
    }

    public byte[] getLquImpRenK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_REN_K1, Pos.LQU_IMP_REN_K1);
        return buffer;
    }

    public void setLquImpRenK1Null(String lquImpRenK1Null) {
        writeString(Pos.LQU_IMP_REN_K1_NULL, lquImpRenK1Null, Len.LQU_IMP_REN_K1_NULL);
    }

    /**Original name: LQU-IMP-REN-K1-NULL<br>*/
    public String getLquImpRenK1Null() {
        return readString(Pos.LQU_IMP_REN_K1_NULL, Len.LQU_IMP_REN_K1_NULL);
    }

    public String getLquImpRenK1NullFormatted() {
        return Functions.padBlanks(getLquImpRenK1Null(), Len.LQU_IMP_REN_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_REN_K1 = 1;
        public static final int LQU_IMP_REN_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_REN_K1 = 8;
        public static final int LQU_IMP_REN_K1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_REN_K1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_REN_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
