package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WTGS-TAB-TGS1<br>
 * Variable: WTGS-TAB-TGS1 from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgsTabTgs1 extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_TRAN_MAXOCCURS = 1250;

    //==== CONSTRUCTORS ====
    public WtgsTabTgs1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGS_TAB_TGS1;
    }

    public String getWtgsTabTgs1Formatted() {
        return readFixedString(Pos.WTGS_TAB_TGS1, Len.WTGS_TAB_TGS1);
    }

    public void setIdTrchDiGar(int idTrchDiGarIdx, int idTrchDiGar) {
        int position = Pos.wtgsIdTrchDiGar(idTrchDiGarIdx - 1);
        writeIntAsPacked(position, idTrchDiGar, Len.Int.ID_TRCH_DI_GAR);
    }

    /**Original name: WTGS-ID-TRCH-DI-GAR<br>*/
    public int getIdTrchDiGar(int idTrchDiGarIdx) {
        int position = Pos.wtgsIdTrchDiGar(idTrchDiGarIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_TRCH_DI_GAR);
    }

    public void setIdGar(int idGarIdx, int idGar) {
        int position = Pos.wtgsIdGar(idGarIdx - 1);
        writeIntAsPacked(position, idGar, Len.Int.ID_GAR);
    }

    /**Original name: WTGS-ID-GAR<br>*/
    public int getIdGar(int idGarIdx) {
        int position = Pos.wtgsIdGar(idGarIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_GAR);
    }

    public void setRestoTabTgs1(String restoTabTgs1) {
        writeString(Pos.RESTO_TAB_TGS1, restoTabTgs1, Len.RESTO_TAB_TGS1);
    }

    /**Original name: WTGS-RESTO-TAB-TGS1<br>*/
    public String getRestoTabTgs1() {
        return readString(Pos.RESTO_TAB_TGS1, Len.RESTO_TAB_TGS1);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGS_TAB_TGS1 = 1;
        public static final int WTGS_TAB_TGS1_R = 1;
        public static final int FLR1 = WTGS_TAB_TGS1_R;
        public static final int RESTO_TAB_TGS1 = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wtgsTabTran(int idx) {
            return WTGS_TAB_TGS1 + idx * Len.TAB_TRAN;
        }

        public static int wtgsIdTrchDiGar(int idx) {
            return wtgsTabTran(idx);
        }

        public static int wtgsIdGar(int idx) {
            return wtgsIdTrchDiGar(idx) + Len.ID_TRCH_DI_GAR;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_TRCH_DI_GAR = 5;
        public static final int ID_GAR = 5;
        public static final int TAB_TRAN = ID_TRCH_DI_GAR + ID_GAR;
        public static final int FLR1 = 10;
        public static final int WTGS_TAB_TGS1 = WtgsTabTgs1.TAB_TRAN_MAXOCCURS * TAB_TRAN;
        public static final int RESTO_TAB_TGS1 = 12490;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_TRCH_DI_GAR = 9;
            public static final int ID_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
