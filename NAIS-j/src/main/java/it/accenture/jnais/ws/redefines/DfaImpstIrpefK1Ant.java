package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPST-IRPEF-K1-ANT<br>
 * Variable: DFA-IMPST-IRPEF-K1-ANT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpstIrpefK1Ant extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpstIrpefK1Ant() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPST_IRPEF_K1_ANT;
    }

    public void setDfaImpstIrpefK1Ant(AfDecimal dfaImpstIrpefK1Ant) {
        writeDecimalAsPacked(Pos.DFA_IMPST_IRPEF_K1_ANT, dfaImpstIrpefK1Ant.copy());
    }

    public void setDfaImpstIrpefK1AntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPST_IRPEF_K1_ANT, Pos.DFA_IMPST_IRPEF_K1_ANT);
    }

    /**Original name: DFA-IMPST-IRPEF-K1-ANT<br>*/
    public AfDecimal getDfaImpstIrpefK1Ant() {
        return readPackedAsDecimal(Pos.DFA_IMPST_IRPEF_K1_ANT, Len.Int.DFA_IMPST_IRPEF_K1_ANT, Len.Fract.DFA_IMPST_IRPEF_K1_ANT);
    }

    public byte[] getDfaImpstIrpefK1AntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPST_IRPEF_K1_ANT, Pos.DFA_IMPST_IRPEF_K1_ANT);
        return buffer;
    }

    public void setDfaImpstIrpefK1AntNull(String dfaImpstIrpefK1AntNull) {
        writeString(Pos.DFA_IMPST_IRPEF_K1_ANT_NULL, dfaImpstIrpefK1AntNull, Len.DFA_IMPST_IRPEF_K1_ANT_NULL);
    }

    /**Original name: DFA-IMPST-IRPEF-K1-ANT-NULL<br>*/
    public String getDfaImpstIrpefK1AntNull() {
        return readString(Pos.DFA_IMPST_IRPEF_K1_ANT_NULL, Len.DFA_IMPST_IRPEF_K1_ANT_NULL);
    }

    public String getDfaImpstIrpefK1AntNullFormatted() {
        return Functions.padBlanks(getDfaImpstIrpefK1AntNull(), Len.DFA_IMPST_IRPEF_K1_ANT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_IRPEF_K1_ANT = 1;
        public static final int DFA_IMPST_IRPEF_K1_ANT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_IRPEF_K1_ANT = 8;
        public static final int DFA_IMPST_IRPEF_K1_ANT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_IRPEF_K1_ANT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_IRPEF_K1_ANT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
