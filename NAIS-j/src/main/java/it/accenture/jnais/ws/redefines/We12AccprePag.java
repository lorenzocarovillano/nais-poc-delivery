package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WE12-ACCPRE-PAG<br>
 * Variable: WE12-ACCPRE-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class We12AccprePag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public We12AccprePag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WE12_ACCPRE_PAG;
    }

    public void setWe12AccprePag(AfDecimal we12AccprePag) {
        writeDecimalAsPacked(Pos.WE12_ACCPRE_PAG, we12AccprePag.copy());
    }

    /**Original name: WE12-ACCPRE-PAG<br>*/
    public AfDecimal getWe12AccprePag() {
        return readPackedAsDecimal(Pos.WE12_ACCPRE_PAG, Len.Int.WE12_ACCPRE_PAG, Len.Fract.WE12_ACCPRE_PAG);
    }

    public byte[] getWe12AccprePagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WE12_ACCPRE_PAG, Pos.WE12_ACCPRE_PAG);
        return buffer;
    }

    public void setWe12AccprePagNull(String we12AccprePagNull) {
        writeString(Pos.WE12_ACCPRE_PAG_NULL, we12AccprePagNull, Len.WE12_ACCPRE_PAG_NULL);
    }

    /**Original name: WE12-ACCPRE-PAG-NULL<br>*/
    public String getWe12AccprePagNull() {
        return readString(Pos.WE12_ACCPRE_PAG_NULL, Len.WE12_ACCPRE_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WE12_ACCPRE_PAG = 1;
        public static final int WE12_ACCPRE_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WE12_ACCPRE_PAG = 8;
        public static final int WE12_ACCPRE_PAG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WE12_ACCPRE_PAG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WE12_ACCPRE_PAG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
