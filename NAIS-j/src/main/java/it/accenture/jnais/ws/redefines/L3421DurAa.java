package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-DUR-AA<br>
 * Variable: L3421-DUR-AA from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421DurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421DurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_DUR_AA;
    }

    public void setL3421DurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_DUR_AA, Pos.L3421_DUR_AA);
    }

    /**Original name: L3421-DUR-AA<br>*/
    public int getL3421DurAa() {
        return readPackedAsInt(Pos.L3421_DUR_AA, Len.Int.L3421_DUR_AA);
    }

    public byte[] getL3421DurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_DUR_AA, Pos.L3421_DUR_AA);
        return buffer;
    }

    /**Original name: L3421-DUR-AA-NULL<br>*/
    public String getL3421DurAaNull() {
        return readString(Pos.L3421_DUR_AA_NULL, Len.L3421_DUR_AA_NULL);
    }

    public String getL3421DurAaNullFormatted() {
        return Functions.padBlanks(getL3421DurAaNull(), Len.L3421_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_DUR_AA = 1;
        public static final int L3421_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_DUR_AA = 3;
        public static final int L3421_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
