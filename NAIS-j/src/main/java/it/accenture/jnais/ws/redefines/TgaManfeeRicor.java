package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-MANFEE-RICOR<br>
 * Variable: TGA-MANFEE-RICOR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaManfeeRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaManfeeRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_MANFEE_RICOR;
    }

    public void setTgaManfeeRicor(AfDecimal tgaManfeeRicor) {
        writeDecimalAsPacked(Pos.TGA_MANFEE_RICOR, tgaManfeeRicor.copy());
    }

    public void setTgaManfeeRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_MANFEE_RICOR, Pos.TGA_MANFEE_RICOR);
    }

    /**Original name: TGA-MANFEE-RICOR<br>*/
    public AfDecimal getTgaManfeeRicor() {
        return readPackedAsDecimal(Pos.TGA_MANFEE_RICOR, Len.Int.TGA_MANFEE_RICOR, Len.Fract.TGA_MANFEE_RICOR);
    }

    public byte[] getTgaManfeeRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_MANFEE_RICOR, Pos.TGA_MANFEE_RICOR);
        return buffer;
    }

    public void setTgaManfeeRicorNull(String tgaManfeeRicorNull) {
        writeString(Pos.TGA_MANFEE_RICOR_NULL, tgaManfeeRicorNull, Len.TGA_MANFEE_RICOR_NULL);
    }

    /**Original name: TGA-MANFEE-RICOR-NULL<br>*/
    public String getTgaManfeeRicorNull() {
        return readString(Pos.TGA_MANFEE_RICOR_NULL, Len.TGA_MANFEE_RICOR_NULL);
    }

    public String getTgaManfeeRicorNullFormatted() {
        return Functions.padBlanks(getTgaManfeeRicorNull(), Len.TGA_MANFEE_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_MANFEE_RICOR = 1;
        public static final int TGA_MANFEE_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_MANFEE_RICOR = 8;
        public static final int TGA_MANFEE_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_MANFEE_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_MANFEE_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
