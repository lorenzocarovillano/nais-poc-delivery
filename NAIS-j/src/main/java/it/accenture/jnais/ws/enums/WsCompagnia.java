package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-COMPAGNIA<br>
 * Variable: WS-COMPAGNIA from program LVES0245<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsCompagnia {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WS_COMPAGNIA);
    public static final String UNIPOL = "00001";
    public static final String AURORA = "00006";

    //==== METHODS ====
    public void setWsCompagniaFormatted(String wsCompagnia) {
        this.value = Trunc.toUnsignedNumeric(wsCompagnia, Len.WS_COMPAGNIA);
    }

    public int getWsCompagnia() {
        return NumericDisplay.asInt(this.value);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_COMPAGNIA = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
