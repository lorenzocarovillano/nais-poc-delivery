package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-EC-TCM-IND<br>
 * Variable: PCO-DT-ULT-EC-TCM-IND from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEcTcmInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEcTcmInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_EC_TCM_IND;
    }

    public void setPcoDtUltEcTcmInd(int pcoDtUltEcTcmInd) {
        writeIntAsPacked(Pos.PCO_DT_ULT_EC_TCM_IND, pcoDtUltEcTcmInd, Len.Int.PCO_DT_ULT_EC_TCM_IND);
    }

    public void setPcoDtUltEcTcmIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_EC_TCM_IND, Pos.PCO_DT_ULT_EC_TCM_IND);
    }

    /**Original name: PCO-DT-ULT-EC-TCM-IND<br>*/
    public int getPcoDtUltEcTcmInd() {
        return readPackedAsInt(Pos.PCO_DT_ULT_EC_TCM_IND, Len.Int.PCO_DT_ULT_EC_TCM_IND);
    }

    public byte[] getPcoDtUltEcTcmIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_EC_TCM_IND, Pos.PCO_DT_ULT_EC_TCM_IND);
        return buffer;
    }

    public void initPcoDtUltEcTcmIndHighValues() {
        fill(Pos.PCO_DT_ULT_EC_TCM_IND, Len.PCO_DT_ULT_EC_TCM_IND, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEcTcmIndNull(String pcoDtUltEcTcmIndNull) {
        writeString(Pos.PCO_DT_ULT_EC_TCM_IND_NULL, pcoDtUltEcTcmIndNull, Len.PCO_DT_ULT_EC_TCM_IND_NULL);
    }

    /**Original name: PCO-DT-ULT-EC-TCM-IND-NULL<br>*/
    public String getPcoDtUltEcTcmIndNull() {
        return readString(Pos.PCO_DT_ULT_EC_TCM_IND_NULL, Len.PCO_DT_ULT_EC_TCM_IND_NULL);
    }

    public String getPcoDtUltEcTcmIndNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEcTcmIndNull(), Len.PCO_DT_ULT_EC_TCM_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_TCM_IND = 1;
        public static final int PCO_DT_ULT_EC_TCM_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_TCM_IND = 5;
        public static final int PCO_DT_ULT_EC_TCM_IND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_EC_TCM_IND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
