package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-ID-MOVI-CHIU<br>
 * Variable: PVT-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_ID_MOVI_CHIU;
    }

    public void setPvtIdMoviChiu(int pvtIdMoviChiu) {
        writeIntAsPacked(Pos.PVT_ID_MOVI_CHIU, pvtIdMoviChiu, Len.Int.PVT_ID_MOVI_CHIU);
    }

    public void setPvtIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_ID_MOVI_CHIU, Pos.PVT_ID_MOVI_CHIU);
    }

    /**Original name: PVT-ID-MOVI-CHIU<br>*/
    public int getPvtIdMoviChiu() {
        return readPackedAsInt(Pos.PVT_ID_MOVI_CHIU, Len.Int.PVT_ID_MOVI_CHIU);
    }

    public byte[] getPvtIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_ID_MOVI_CHIU, Pos.PVT_ID_MOVI_CHIU);
        return buffer;
    }

    public void setPvtIdMoviChiuNull(String pvtIdMoviChiuNull) {
        writeString(Pos.PVT_ID_MOVI_CHIU_NULL, pvtIdMoviChiuNull, Len.PVT_ID_MOVI_CHIU_NULL);
    }

    /**Original name: PVT-ID-MOVI-CHIU-NULL<br>*/
    public String getPvtIdMoviChiuNull() {
        return readString(Pos.PVT_ID_MOVI_CHIU_NULL, Len.PVT_ID_MOVI_CHIU_NULL);
    }

    public String getPvtIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getPvtIdMoviChiuNull(), Len.PVT_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_ID_MOVI_CHIU = 1;
        public static final int PVT_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_ID_MOVI_CHIU = 5;
        public static final int PVT_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
