package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: E06-CPT-PROTETTO<br>
 * Variable: E06-CPT-PROTETTO from program IDBSE060<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E06CptProtetto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E06CptProtetto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E06_CPT_PROTETTO;
    }

    public void setE06CptProtetto(AfDecimal e06CptProtetto) {
        writeDecimalAsPacked(Pos.E06_CPT_PROTETTO, e06CptProtetto.copy());
    }

    public void setE06CptProtettoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E06_CPT_PROTETTO, Pos.E06_CPT_PROTETTO);
    }

    /**Original name: E06-CPT-PROTETTO<br>*/
    public AfDecimal getE06CptProtetto() {
        return readPackedAsDecimal(Pos.E06_CPT_PROTETTO, Len.Int.E06_CPT_PROTETTO, Len.Fract.E06_CPT_PROTETTO);
    }

    public byte[] getE06CptProtettoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E06_CPT_PROTETTO, Pos.E06_CPT_PROTETTO);
        return buffer;
    }

    public void setE06CptProtettoNull(String e06CptProtettoNull) {
        writeString(Pos.E06_CPT_PROTETTO_NULL, e06CptProtettoNull, Len.E06_CPT_PROTETTO_NULL);
    }

    /**Original name: E06-CPT-PROTETTO-NULL<br>*/
    public String getE06CptProtettoNull() {
        return readString(Pos.E06_CPT_PROTETTO_NULL, Len.E06_CPT_PROTETTO_NULL);
    }

    public String getE06CptProtettoNullFormatted() {
        return Functions.padBlanks(getE06CptProtettoNull(), Len.E06_CPT_PROTETTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E06_CPT_PROTETTO = 1;
        public static final int E06_CPT_PROTETTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E06_CPT_PROTETTO = 8;
        public static final int E06_CPT_PROTETTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E06_CPT_PROTETTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int E06_CPT_PROTETTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
