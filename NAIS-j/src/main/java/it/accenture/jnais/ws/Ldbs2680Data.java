package it.accenture.jnais.ws;

import it.accenture.jnais.copy.DettTitContDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndParamMovi;
import it.accenture.jnais.copy.Ldbv2681;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS2680<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs2680Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-PARAM-MOVI
    private IndParamMovi indParamMovi = new IndParamMovi();
    //Original name: PARAM-MOVI-DB
    private DettTitContDb paramMoviDb = new DettTitContDb();
    //Original name: LDBV2681
    private Ldbv2681 ldbv2681 = new Ldbv2681();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndParamMovi getIndParamMovi() {
        return indParamMovi;
    }

    public Ldbv2681 getLdbv2681() {
        return ldbv2681;
    }

    public DettTitContDb getParamMoviDb() {
        return paramMoviDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
