package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-PRLCOS<br>
 * Variable: WPCO-DT-ULT-ELAB-PRLCOS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabPrlcos extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabPrlcos() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_PRLCOS;
    }

    public void setWpcoDtUltElabPrlcos(int wpcoDtUltElabPrlcos) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_PRLCOS, wpcoDtUltElabPrlcos, Len.Int.WPCO_DT_ULT_ELAB_PRLCOS);
    }

    public void setDpcoDtUltElabPrlcosFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_PRLCOS, Pos.WPCO_DT_ULT_ELAB_PRLCOS);
    }

    /**Original name: WPCO-DT-ULT-ELAB-PRLCOS<br>*/
    public int getWpcoDtUltElabPrlcos() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_PRLCOS, Len.Int.WPCO_DT_ULT_ELAB_PRLCOS);
    }

    public byte[] getWpcoDtUltElabPrlcosAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_PRLCOS, Pos.WPCO_DT_ULT_ELAB_PRLCOS);
        return buffer;
    }

    public void setWpcoDtUltElabPrlcosNull(String wpcoDtUltElabPrlcosNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_PRLCOS_NULL, wpcoDtUltElabPrlcosNull, Len.WPCO_DT_ULT_ELAB_PRLCOS_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-PRLCOS-NULL<br>*/
    public String getWpcoDtUltElabPrlcosNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_PRLCOS_NULL, Len.WPCO_DT_ULT_ELAB_PRLCOS_NULL);
    }

    public String getWpcoDtUltElabPrlcosNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabPrlcosNull(), Len.WPCO_DT_ULT_ELAB_PRLCOS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_PRLCOS = 1;
        public static final int WPCO_DT_ULT_ELAB_PRLCOS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_PRLCOS = 5;
        public static final int WPCO_DT_ULT_ELAB_PRLCOS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_PRLCOS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
