package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-ID-ADES<br>
 * Variable: L3401-ID-ADES from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401IdAdes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401IdAdes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_ID_ADES;
    }

    public void setL3401IdAdes(int l3401IdAdes) {
        writeIntAsPacked(Pos.L3401_ID_ADES, l3401IdAdes, Len.Int.L3401_ID_ADES);
    }

    public void setL3401IdAdesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_ID_ADES, Pos.L3401_ID_ADES);
    }

    /**Original name: L3401-ID-ADES<br>*/
    public int getL3401IdAdes() {
        return readPackedAsInt(Pos.L3401_ID_ADES, Len.Int.L3401_ID_ADES);
    }

    public byte[] getL3401IdAdesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_ID_ADES, Pos.L3401_ID_ADES);
        return buffer;
    }

    /**Original name: L3401-ID-ADES-NULL<br>*/
    public String getL3401IdAdesNull() {
        return readString(Pos.L3401_ID_ADES_NULL, Len.L3401_ID_ADES_NULL);
    }

    public String getL3401IdAdesNullFormatted() {
        return Functions.padBlanks(getL3401IdAdesNull(), Len.L3401_ID_ADES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_ID_ADES = 1;
        public static final int L3401_ID_ADES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_ID_ADES = 5;
        public static final int L3401_ID_ADES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
