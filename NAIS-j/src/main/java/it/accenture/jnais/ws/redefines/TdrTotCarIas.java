package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-CAR-IAS<br>
 * Variable: TDR-TOT-CAR-IAS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotCarIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotCarIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_CAR_IAS;
    }

    public void setTdrTotCarIas(AfDecimal tdrTotCarIas) {
        writeDecimalAsPacked(Pos.TDR_TOT_CAR_IAS, tdrTotCarIas.copy());
    }

    public void setTdrTotCarIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_CAR_IAS, Pos.TDR_TOT_CAR_IAS);
    }

    /**Original name: TDR-TOT-CAR-IAS<br>*/
    public AfDecimal getTdrTotCarIas() {
        return readPackedAsDecimal(Pos.TDR_TOT_CAR_IAS, Len.Int.TDR_TOT_CAR_IAS, Len.Fract.TDR_TOT_CAR_IAS);
    }

    public byte[] getTdrTotCarIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_CAR_IAS, Pos.TDR_TOT_CAR_IAS);
        return buffer;
    }

    public void setTdrTotCarIasNull(String tdrTotCarIasNull) {
        writeString(Pos.TDR_TOT_CAR_IAS_NULL, tdrTotCarIasNull, Len.TDR_TOT_CAR_IAS_NULL);
    }

    /**Original name: TDR-TOT-CAR-IAS-NULL<br>*/
    public String getTdrTotCarIasNull() {
        return readString(Pos.TDR_TOT_CAR_IAS_NULL, Len.TDR_TOT_CAR_IAS_NULL);
    }

    public String getTdrTotCarIasNullFormatted() {
        return Functions.padBlanks(getTdrTotCarIasNull(), Len.TDR_TOT_CAR_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_CAR_IAS = 1;
        public static final int TDR_TOT_CAR_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_CAR_IAS = 8;
        public static final int TDR_TOT_CAR_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_CAR_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_CAR_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
