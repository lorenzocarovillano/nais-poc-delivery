package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvric1;
import it.accenture.jnais.copy.WricDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WRIC-AREA-RICHIESTA<br>
 * Variable: WRIC-AREA-RICHIESTA from program LCCS0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WricAreaRichiesta extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WRIC-ELE-RICH-MAX
    private short wricEleRichMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVRIC1
    private Lccvric1 lccvric1 = new Lccvric1();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIC_AREA_RICHIESTA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWricAreaRichiestaBytes(buf);
    }

    public String getWricAreaRichiestaFormatted() {
        return MarshalByteExt.bufferToStr(getWricAreaRichiestaBytes());
    }

    public void setWricAreaRichiestaBytes(byte[] buffer) {
        setWricAreaRichiestaBytes(buffer, 1);
    }

    public byte[] getWricAreaRichiestaBytes() {
        byte[] buffer = new byte[Len.WRIC_AREA_RICHIESTA];
        return getWricAreaRichiestaBytes(buffer, 1);
    }

    public void setWricAreaRichiestaBytes(byte[] buffer, int offset) {
        int position = offset;
        wricEleRichMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWricTabRichBytes(buffer, position);
    }

    public byte[] getWricAreaRichiestaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wricEleRichMax);
        position += Types.SHORT_SIZE;
        getWricTabRichBytes(buffer, position);
        return buffer;
    }

    public void setWricEleRichMax(short wricEleRichMax) {
        this.wricEleRichMax = wricEleRichMax;
    }

    public short getWricEleRichMax() {
        return this.wricEleRichMax;
    }

    public void setWricTabRichBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvric1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvric1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvric1.Len.Int.ID_PTF, 0));
        position += Lccvric1.Len.ID_PTF;
        lccvric1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWricTabRichBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvric1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvric1.getIdPtf(), Lccvric1.Len.Int.ID_PTF, 0);
        position += Lccvric1.Len.ID_PTF;
        lccvric1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public Lccvric1 getLccvric1() {
        return lccvric1;
    }

    @Override
    public byte[] serialize() {
        return getWricAreaRichiestaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIC_ELE_RICH_MAX = 2;
        public static final int WRIC_TAB_RICH = WpolStatus.Len.STATUS + Lccvric1.Len.ID_PTF + WricDati.Len.DATI;
        public static final int WRIC_AREA_RICHIESTA = WRIC_ELE_RICH_MAX + WRIC_TAB_RICH;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
