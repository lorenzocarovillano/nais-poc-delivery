package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-CAR-ACQ<br>
 * Variable: DTR-CAR-ACQ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_CAR_ACQ;
    }

    public void setDtrCarAcq(AfDecimal dtrCarAcq) {
        writeDecimalAsPacked(Pos.DTR_CAR_ACQ, dtrCarAcq.copy());
    }

    public void setDtrCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_CAR_ACQ, Pos.DTR_CAR_ACQ);
    }

    /**Original name: DTR-CAR-ACQ<br>*/
    public AfDecimal getDtrCarAcq() {
        return readPackedAsDecimal(Pos.DTR_CAR_ACQ, Len.Int.DTR_CAR_ACQ, Len.Fract.DTR_CAR_ACQ);
    }

    public byte[] getDtrCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_CAR_ACQ, Pos.DTR_CAR_ACQ);
        return buffer;
    }

    public void setDtrCarAcqNull(String dtrCarAcqNull) {
        writeString(Pos.DTR_CAR_ACQ_NULL, dtrCarAcqNull, Len.DTR_CAR_ACQ_NULL);
    }

    /**Original name: DTR-CAR-ACQ-NULL<br>*/
    public String getDtrCarAcqNull() {
        return readString(Pos.DTR_CAR_ACQ_NULL, Len.DTR_CAR_ACQ_NULL);
    }

    public String getDtrCarAcqNullFormatted() {
        return Functions.padBlanks(getDtrCarAcqNull(), Len.DTR_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_CAR_ACQ = 1;
        public static final int DTR_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_CAR_ACQ = 8;
        public static final int DTR_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
