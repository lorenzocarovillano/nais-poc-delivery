package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-DUR-GG<br>
 * Variable: POL-DUR-GG from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolDurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_DUR_GG;
    }

    public void setPolDurGg(int polDurGg) {
        writeIntAsPacked(Pos.POL_DUR_GG, polDurGg, Len.Int.POL_DUR_GG);
    }

    public void setPolDurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_DUR_GG, Pos.POL_DUR_GG);
    }

    /**Original name: POL-DUR-GG<br>*/
    public int getPolDurGg() {
        return readPackedAsInt(Pos.POL_DUR_GG, Len.Int.POL_DUR_GG);
    }

    public byte[] getPolDurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_DUR_GG, Pos.POL_DUR_GG);
        return buffer;
    }

    public void setPolDurGgNull(String polDurGgNull) {
        writeString(Pos.POL_DUR_GG_NULL, polDurGgNull, Len.POL_DUR_GG_NULL);
    }

    /**Original name: POL-DUR-GG-NULL<br>*/
    public String getPolDurGgNull() {
        return readString(Pos.POL_DUR_GG_NULL, Len.POL_DUR_GG_NULL);
    }

    public String getPolDurGgNullFormatted() {
        return Functions.padBlanks(getPolDurGgNull(), Len.POL_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_DUR_GG = 1;
        public static final int POL_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_DUR_GG = 3;
        public static final int POL_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
