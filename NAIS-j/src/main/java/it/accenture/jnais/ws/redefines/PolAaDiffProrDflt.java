package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-AA-DIFF-PROR-DFLT<br>
 * Variable: POL-AA-DIFF-PROR-DFLT from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolAaDiffProrDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolAaDiffProrDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_AA_DIFF_PROR_DFLT;
    }

    public void setPolAaDiffProrDflt(int polAaDiffProrDflt) {
        writeIntAsPacked(Pos.POL_AA_DIFF_PROR_DFLT, polAaDiffProrDflt, Len.Int.POL_AA_DIFF_PROR_DFLT);
    }

    public void setPolAaDiffProrDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_AA_DIFF_PROR_DFLT, Pos.POL_AA_DIFF_PROR_DFLT);
    }

    /**Original name: POL-AA-DIFF-PROR-DFLT<br>*/
    public int getPolAaDiffProrDflt() {
        return readPackedAsInt(Pos.POL_AA_DIFF_PROR_DFLT, Len.Int.POL_AA_DIFF_PROR_DFLT);
    }

    public byte[] getPolAaDiffProrDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_AA_DIFF_PROR_DFLT, Pos.POL_AA_DIFF_PROR_DFLT);
        return buffer;
    }

    public void setPolAaDiffProrDfltNull(String polAaDiffProrDfltNull) {
        writeString(Pos.POL_AA_DIFF_PROR_DFLT_NULL, polAaDiffProrDfltNull, Len.POL_AA_DIFF_PROR_DFLT_NULL);
    }

    /**Original name: POL-AA-DIFF-PROR-DFLT-NULL<br>*/
    public String getPolAaDiffProrDfltNull() {
        return readString(Pos.POL_AA_DIFF_PROR_DFLT_NULL, Len.POL_AA_DIFF_PROR_DFLT_NULL);
    }

    public String getPolAaDiffProrDfltNullFormatted() {
        return Functions.padBlanks(getPolAaDiffProrDfltNull(), Len.POL_AA_DIFF_PROR_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_AA_DIFF_PROR_DFLT = 1;
        public static final int POL_AA_DIFF_PROR_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_AA_DIFF_PROR_DFLT = 3;
        public static final int POL_AA_DIFF_PROR_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_AA_DIFF_PROR_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
