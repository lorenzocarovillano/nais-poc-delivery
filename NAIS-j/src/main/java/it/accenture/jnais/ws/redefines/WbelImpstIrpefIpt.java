package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMPST-IRPEF-IPT<br>
 * Variable: WBEL-IMPST-IRPEF-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpstIrpefIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpstIrpefIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMPST_IRPEF_IPT;
    }

    public void setWbelImpstIrpefIpt(AfDecimal wbelImpstIrpefIpt) {
        writeDecimalAsPacked(Pos.WBEL_IMPST_IRPEF_IPT, wbelImpstIrpefIpt.copy());
    }

    public void setWbelImpstIrpefIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMPST_IRPEF_IPT, Pos.WBEL_IMPST_IRPEF_IPT);
    }

    /**Original name: WBEL-IMPST-IRPEF-IPT<br>*/
    public AfDecimal getWbelImpstIrpefIpt() {
        return readPackedAsDecimal(Pos.WBEL_IMPST_IRPEF_IPT, Len.Int.WBEL_IMPST_IRPEF_IPT, Len.Fract.WBEL_IMPST_IRPEF_IPT);
    }

    public byte[] getWbelImpstIrpefIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMPST_IRPEF_IPT, Pos.WBEL_IMPST_IRPEF_IPT);
        return buffer;
    }

    public void initWbelImpstIrpefIptSpaces() {
        fill(Pos.WBEL_IMPST_IRPEF_IPT, Len.WBEL_IMPST_IRPEF_IPT, Types.SPACE_CHAR);
    }

    public void setWbelImpstIrpefIptNull(String wbelImpstIrpefIptNull) {
        writeString(Pos.WBEL_IMPST_IRPEF_IPT_NULL, wbelImpstIrpefIptNull, Len.WBEL_IMPST_IRPEF_IPT_NULL);
    }

    /**Original name: WBEL-IMPST-IRPEF-IPT-NULL<br>*/
    public String getWbelImpstIrpefIptNull() {
        return readString(Pos.WBEL_IMPST_IRPEF_IPT_NULL, Len.WBEL_IMPST_IRPEF_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_IRPEF_IPT = 1;
        public static final int WBEL_IMPST_IRPEF_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_IRPEF_IPT = 8;
        public static final int WBEL_IMPST_IRPEF_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_IRPEF_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_IRPEF_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
