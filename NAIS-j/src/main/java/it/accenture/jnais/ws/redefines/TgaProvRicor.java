package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PROV-RICOR<br>
 * Variable: TGA-PROV-RICOR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PROV_RICOR;
    }

    public void setTgaProvRicor(AfDecimal tgaProvRicor) {
        writeDecimalAsPacked(Pos.TGA_PROV_RICOR, tgaProvRicor.copy());
    }

    public void setTgaProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PROV_RICOR, Pos.TGA_PROV_RICOR);
    }

    /**Original name: TGA-PROV-RICOR<br>*/
    public AfDecimal getTgaProvRicor() {
        return readPackedAsDecimal(Pos.TGA_PROV_RICOR, Len.Int.TGA_PROV_RICOR, Len.Fract.TGA_PROV_RICOR);
    }

    public byte[] getTgaProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PROV_RICOR, Pos.TGA_PROV_RICOR);
        return buffer;
    }

    public void setTgaProvRicorNull(String tgaProvRicorNull) {
        writeString(Pos.TGA_PROV_RICOR_NULL, tgaProvRicorNull, Len.TGA_PROV_RICOR_NULL);
    }

    /**Original name: TGA-PROV-RICOR-NULL<br>*/
    public String getTgaProvRicorNull() {
        return readString(Pos.TGA_PROV_RICOR_NULL, Len.TGA_PROV_RICOR_NULL);
    }

    public String getTgaProvRicorNullFormatted() {
        return Functions.padBlanks(getTgaProvRicorNull(), Len.TGA_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PROV_RICOR = 1;
        public static final int TGA_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PROV_RICOR = 8;
        public static final int TGA_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
