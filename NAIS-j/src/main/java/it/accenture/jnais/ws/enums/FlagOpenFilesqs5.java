package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-OPEN-FILESQS5<br>
 * Variable: FLAG-OPEN-FILESQS5 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagOpenFilesqs5 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagOpenFilesqs5(char flagOpenFilesqs5) {
        this.value = flagOpenFilesqs5;
    }

    public char getFlagOpenFilesqs5() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setOpenFilesqs5Si() {
        value = SI;
    }
}
