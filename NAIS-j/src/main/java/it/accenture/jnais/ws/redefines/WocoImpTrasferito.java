package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-IMP-TRASFERITO<br>
 * Variable: WOCO-IMP-TRASFERITO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoImpTrasferito extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoImpTrasferito() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_IMP_TRASFERITO;
    }

    public void setWocoImpTrasferito(AfDecimal wocoImpTrasferito) {
        writeDecimalAsPacked(Pos.WOCO_IMP_TRASFERITO, wocoImpTrasferito.copy());
    }

    public void setWocoImpTrasferitoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_IMP_TRASFERITO, Pos.WOCO_IMP_TRASFERITO);
    }

    /**Original name: WOCO-IMP-TRASFERITO<br>*/
    public AfDecimal getWocoImpTrasferito() {
        return readPackedAsDecimal(Pos.WOCO_IMP_TRASFERITO, Len.Int.WOCO_IMP_TRASFERITO, Len.Fract.WOCO_IMP_TRASFERITO);
    }

    public byte[] getWocoImpTrasferitoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_IMP_TRASFERITO, Pos.WOCO_IMP_TRASFERITO);
        return buffer;
    }

    public void initWocoImpTrasferitoSpaces() {
        fill(Pos.WOCO_IMP_TRASFERITO, Len.WOCO_IMP_TRASFERITO, Types.SPACE_CHAR);
    }

    public void setWocoImpTrasferitoNull(String wocoImpTrasferitoNull) {
        writeString(Pos.WOCO_IMP_TRASFERITO_NULL, wocoImpTrasferitoNull, Len.WOCO_IMP_TRASFERITO_NULL);
    }

    /**Original name: WOCO-IMP-TRASFERITO-NULL<br>*/
    public String getWocoImpTrasferitoNull() {
        return readString(Pos.WOCO_IMP_TRASFERITO_NULL, Len.WOCO_IMP_TRASFERITO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_IMP_TRASFERITO = 1;
        public static final int WOCO_IMP_TRASFERITO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_IMP_TRASFERITO = 8;
        public static final int WOCO_IMP_TRASFERITO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_IMP_TRASFERITO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_IMP_TRASFERITO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
