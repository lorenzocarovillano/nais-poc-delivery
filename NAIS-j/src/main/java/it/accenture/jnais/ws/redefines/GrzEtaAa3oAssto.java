package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-ETA-AA-3O-ASSTO<br>
 * Variable: GRZ-ETA-AA-3O-ASSTO from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzEtaAa3oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzEtaAa3oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_ETA_AA3O_ASSTO;
    }

    public void setGrzEtaAa3oAssto(short grzEtaAa3oAssto) {
        writeShortAsPacked(Pos.GRZ_ETA_AA3O_ASSTO, grzEtaAa3oAssto, Len.Int.GRZ_ETA_AA3O_ASSTO);
    }

    public void setGrzEtaAa3oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_ETA_AA3O_ASSTO, Pos.GRZ_ETA_AA3O_ASSTO);
    }

    /**Original name: GRZ-ETA-AA-3O-ASSTO<br>*/
    public short getGrzEtaAa3oAssto() {
        return readPackedAsShort(Pos.GRZ_ETA_AA3O_ASSTO, Len.Int.GRZ_ETA_AA3O_ASSTO);
    }

    public byte[] getGrzEtaAa3oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_ETA_AA3O_ASSTO, Pos.GRZ_ETA_AA3O_ASSTO);
        return buffer;
    }

    public void setGrzEtaAa3oAsstoNull(String grzEtaAa3oAsstoNull) {
        writeString(Pos.GRZ_ETA_AA3O_ASSTO_NULL, grzEtaAa3oAsstoNull, Len.GRZ_ETA_AA3O_ASSTO_NULL);
    }

    /**Original name: GRZ-ETA-AA-3O-ASSTO-NULL<br>*/
    public String getGrzEtaAa3oAsstoNull() {
        return readString(Pos.GRZ_ETA_AA3O_ASSTO_NULL, Len.GRZ_ETA_AA3O_ASSTO_NULL);
    }

    public String getGrzEtaAa3oAsstoNullFormatted() {
        return Functions.padBlanks(getGrzEtaAa3oAsstoNull(), Len.GRZ_ETA_AA3O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_ETA_AA3O_ASSTO = 1;
        public static final int GRZ_ETA_AA3O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_ETA_AA3O_ASSTO = 2;
        public static final int GRZ_ETA_AA3O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_ETA_AA3O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
