package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Ivvc0212;
import it.accenture.jnais.copy.Ivvc0217;
import it.accenture.jnais.copy.Ivvc0224;
import it.accenture.jnais.copy.Lccvdad1;
import it.accenture.jnais.copy.Lccvdco1;
import it.accenture.jnais.copy.Lccvdfa1;
import it.accenture.jnais.copy.Lccvdfl1;
import it.accenture.jnais.copy.Lccvmov1;
import it.accenture.jnais.copy.Lccvp011;
import it.accenture.jnais.copy.Lccvp611;
import it.accenture.jnais.copy.Lccvp671;
import it.accenture.jnais.copy.Lccvpol1;
import it.accenture.jnais.copy.Lccvric1;
import it.accenture.jnais.copy.Lccvtcl1;
import it.accenture.jnais.copy.WdadDati;
import it.accenture.jnais.copy.WdcoDati;
import it.accenture.jnais.copy.WdfaDati;
import it.accenture.jnais.copy.WdflDati;
import it.accenture.jnais.copy.WmovDati;
import it.accenture.jnais.copy.Wp01Dati;
import it.accenture.jnais.copy.Wp61Dati;
import it.accenture.jnais.copy.Wp67Dati;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.copy.WricDati;
import it.accenture.jnais.copy.WtclDati;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.occurs.S089TabLiq;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;
import it.accenture.jnais.ws.occurs.WadeTabAdes;
import it.accenture.jnais.ws.occurs.WbelTabBeneLiq;
import it.accenture.jnais.ws.occurs.WbepTabBeneficiari;
import it.accenture.jnais.ws.occurs.WcltTabClauTxt;
import it.accenture.jnais.ws.occurs.WdeqTabDettQuest;
import it.accenture.jnais.ws.occurs.WdtcTabDtc;
import it.accenture.jnais.ws.occurs.We15TabE15;
import it.accenture.jnais.ws.occurs.WgrlTabGarLiq;
import it.accenture.jnais.ws.occurs.WgrzTabGar;
import it.accenture.jnais.ws.occurs.WisoTabImpSost;
import it.accenture.jnais.ws.occurs.Wl19TabFnd;
import it.accenture.jnais.ws.occurs.Wl23TabVincPeg;
import it.accenture.jnais.ws.occurs.Wl30TabReinvstPoli;
import it.accenture.jnais.ws.occurs.WmfzTabMoviFinrio;
import it.accenture.jnais.ws.occurs.WocoTabOggColl;
import it.accenture.jnais.ws.occurs.WopzOpzioni;
import it.accenture.jnais.ws.occurs.Wp56TabQuestAdegVer;
import it.accenture.jnais.ws.occurs.Wp58TabImpstBol;
import it.accenture.jnais.ws.occurs.Wp86TabMotLiq;
import it.accenture.jnais.ws.occurs.Wp88TabServVal;
import it.accenture.jnais.ws.occurs.Wp89TabDservVal;
import it.accenture.jnais.ws.occurs.WpliTabPercLiq;
import it.accenture.jnais.ws.occurs.WpmoTabParamMov;
import it.accenture.jnais.ws.occurs.WpogTabParamOgg;
import it.accenture.jnais.ws.occurs.WpreTabPrestiti;
import it.accenture.jnais.ws.occurs.WpvtTabProvTr;
import it.accenture.jnais.ws.occurs.WqueTabQuest;
import it.accenture.jnais.ws.occurs.WranTabRappAnag;
import it.accenture.jnais.ws.occurs.WrdfTabRicDisinv;
import it.accenture.jnais.ws.occurs.WrifTabRicInv;
import it.accenture.jnais.ws.occurs.WrreTabRappRete;
import it.accenture.jnais.ws.occurs.WrstTabRst;
import it.accenture.jnais.ws.occurs.WsdiTabStraInv;
import it.accenture.jnais.ws.occurs.WspgTabSpg;
import it.accenture.jnais.ws.occurs.WtgaTabTran;
import it.accenture.jnais.ws.occurs.WtitTabTitCont;
import it.accenture.jnais.ws.occurs.WtliTabTrchLiq;
import it.accenture.jnais.ws.ptr.AreaIvvc0223;
import it.accenture.jnais.ws.ptr.Ivvc0222AreaFndXTranche;

/**Original name: AREA-BUSINESS<br>
 * Variable: AREA-BUSINESS from program IVVS0216<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaBusiness extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int WADE_TAB_ADES_MAXOCCURS = 1;
    public static final int WBEP_TAB_BENEFICIARI_MAXOCCURS = 20;
    public static final int WBEL_TAB_BENE_LIQ_MAXOCCURS = 15;
    public static final int WDEQ_TAB_DETT_QUEST_MAXOCCURS = 180;
    public static final int WDTC_TAB_DTC_MAXOCCURS = 100;
    public static final int WGRL_TAB_GAR_LIQ_MAXOCCURS = 20;
    public static final int WISO_TAB_IMP_SOST_MAXOCCURS = 1;
    public static final int WMFZ_TAB_MOVI_FINRIO_MAXOCCURS = 10;
    public static final int WPOG_TAB_PARAM_OGG_MAXOCCURS = 200;
    public static final int WPMO_TAB_PARAM_MOV_MAXOCCURS = 100;
    public static final int WPLI_TAB_PERC_LIQ_MAXOCCURS = 30;
    public static final int WPRE_TAB_PRESTITI_MAXOCCURS = 10;
    public static final int WPVT_TAB_PROV_TR_MAXOCCURS = 10;
    public static final int WQUE_TAB_QUEST_MAXOCCURS = 12;
    public static final int WRDF_TAB_RIC_DISINV_MAXOCCURS = 1250;
    public static final int WRIF_TAB_RIC_INV_MAXOCCURS = 1250;
    public static final int WRAN_TAB_RAPP_ANAG_MAXOCCURS = 100;
    public static final int WE15_TAB_E15_MAXOCCURS = 100;
    public static final int WSPG_TAB_SPG_MAXOCCURS = 20;
    public static final int WSDI_TAB_STRA_INV_MAXOCCURS = 20;
    public static final int WTIT_TAB_TIT_CONT_MAXOCCURS = 200;
    public static final int W1TGA_TAB_TRAN_MAXOCCURS = 1250;
    public static final int WTLI_TAB_TRCH_LIQ_MAXOCCURS = 1250;
    public static final int WOCO_TAB_OGG_COLL_MAXOCCURS = 60;
    public static final int WRST_TAB_RST_MAXOCCURS = 10;
    public static final int WL19_TAB_FND_MAXOCCURS = 250;
    public static final int WL30_TAB_REINVST_POLI_MAXOCCURS = 40;
    public static final int WL23_TAB_VINC_PEG_MAXOCCURS = 10;
    public static final int WCLT_TAB_CLAU_TXT_MAXOCCURS = 54;
    public static final int WP58_TAB_IMPST_BOL_MAXOCCURS = 75;
    public static final int WP86_TAB_MOT_LIQ_MAXOCCURS = 1;
    public static final int WP88_TAB_SERV_VAL_MAXOCCURS = 10;
    public static final int WP89_TAB_DSERV_VAL_MAXOCCURS = 350;
    public static final int WP56_TAB_QUEST_ADEG_VER_MAXOCCURS = 100;
    public static final int WGRZ_TAB_GAR_MAXOCCURS = 20;
    public static final int WLQU_TAB_LIQ_MAXOCCURS = 30;
    public static final int WRRE_TAB_RAPP_RETE_MAXOCCURS = 10;
    public static final int WGOP_TAB_GAR_MAXOCCURS = 20;
    public static final int WTOP_TAB_TRAN_MAXOCCURS = 20;
    //Original name: WADE-ELE-ADES-MAX
    private short wadeEleAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WADE-TAB-ADES
    private WadeTabAdes[] wadeTabAdes = new WadeTabAdes[WADE_TAB_ADES_MAXOCCURS];
    //Original name: WBEP-ELE-BENEF-MAX
    private short wbepEleBenefMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WBEP-TAB-BENEFICIARI
    private WbepTabBeneficiari[] wbepTabBeneficiari = new WbepTabBeneficiari[WBEP_TAB_BENEFICIARI_MAXOCCURS];
    //Original name: WBEL-ELE-BENEF-LIQ-MAX
    private short wbelEleBenefLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WBEL-TAB-BENE-LIQ
    private WbelTabBeneLiq[] wbelTabBeneLiq = new WbelTabBeneLiq[WBEL_TAB_BENE_LIQ_MAXOCCURS];
    //Original name: WDCO-ELE-COLL-MAX
    private short wdcoEleCollMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDCO1
    private Lccvdco1 lccvdco1 = new Lccvdco1();
    //Original name: WDFA-ELE-FISC-ADES-MAX
    private short wdfaEleFiscAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDFA1
    private Lccvdfa1 lccvdfa1 = new Lccvdfa1();
    //Original name: WDEQ-ELE-DETT-QUEST-MAX
    private short wdeqEleDettQuestMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WDEQ-TAB-DETT-QUEST
    private WdeqTabDettQuest[] wdeqTabDettQuest = new WdeqTabDettQuest[WDEQ_TAB_DETT_QUEST_MAXOCCURS];
    //Original name: WDTC-ELE-DETT-TIT-MAX
    private short wdtcEleDettTitMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WDTC-TAB-DTC
    private WdtcTabDtc[] wdtcTabDtc = new WdtcTabDtc[WDTC_TAB_DTC_MAXOCCURS];
    //Original name: WGRZ-ELE-GARANZIA-MAX
    private short wgrzEleGaranziaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WGRZ-TAB-GAR
    private WgrzTabGar[] wgrzTabGar = new WgrzTabGar[WGRZ_TAB_GAR_MAXOCCURS];
    //Original name: WGRL-ELE-GAR-LIQ-MAX
    private short wgrlEleGarLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WGRL-TAB-GAR-LIQ
    private WgrlTabGarLiq[] wgrlTabGarLiq = new WgrlTabGarLiq[WGRL_TAB_GAR_LIQ_MAXOCCURS];
    //Original name: WISO-ELE-IMP-SOST-MAX
    private short wisoEleImpSostMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WISO-TAB-IMP-SOST
    private WisoTabImpSost[] wisoTabImpSost = new WisoTabImpSost[WISO_TAB_IMP_SOST_MAXOCCURS];
    //Original name: WLQU-ELE-LIQ-MAX
    private short wlquEleLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WLQU-TAB-LIQ
    private S089TabLiq[] wlquTabLiq = new S089TabLiq[WLQU_TAB_LIQ_MAXOCCURS];
    //Original name: WMOV-ELE-MOVI-MAX
    private short wmovEleMoviMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVMOV1
    private Lccvmov1 lccvmov1 = new Lccvmov1();
    //Original name: WMFZ-ELE-MOVI-FINRIO-MAX
    private short wmfzEleMoviFinrioMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WMFZ-TAB-MOVI-FINRIO
    private WmfzTabMoviFinrio[] wmfzTabMoviFinrio = new WmfzTabMoviFinrio[WMFZ_TAB_MOVI_FINRIO_MAXOCCURS];
    //Original name: WPOG-ELE-PARAM-OGG-MAX
    private short wpogEleParamOggMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPOG-TAB-PARAM-OGG
    private WpogTabParamOgg[] wpogTabParamOgg = new WpogTabParamOgg[WPOG_TAB_PARAM_OGG_MAXOCCURS];
    //Original name: WPMO-ELE-PARAM-MOV-MAX
    private short wpmoEleParamMovMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPMO-TAB-PARAM-MOV
    private WpmoTabParamMov[] wpmoTabParamMov = new WpmoTabParamMov[WPMO_TAB_PARAM_MOV_MAXOCCURS];
    //Original name: WPLI-ELE-PERC-LIQ-MAX
    private short wpliElePercLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPLI-TAB-PERC-LIQ
    private WpliTabPercLiq[] wpliTabPercLiq = new WpliTabPercLiq[WPLI_TAB_PERC_LIQ_MAXOCCURS];
    //Original name: WPOL-ELE-POLI-MAX
    private short wpolElePoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1 lccvpol1 = new Lccvpol1();
    //Original name: WPRE-ELE-PRESTITI-MAX
    private short wpreElePrestitiMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPRE-TAB-PRESTITI
    private WpreTabPrestiti[] wpreTabPrestiti = new WpreTabPrestiti[WPRE_TAB_PRESTITI_MAXOCCURS];
    //Original name: WPVT-ELE-PROV-MAX
    private short wpvtEleProvMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPVT-TAB-PROV-TR
    private WpvtTabProvTr[] wpvtTabProvTr = new WpvtTabProvTr[WPVT_TAB_PROV_TR_MAXOCCURS];
    //Original name: WQUE-ELE-QUEST-MAX
    private short wqueEleQuestMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WQUE-TAB-QUEST
    private WqueTabQuest[] wqueTabQuest = new WqueTabQuest[WQUE_TAB_QUEST_MAXOCCURS];
    //Original name: WRIC-ELE-RICH-MAX
    private short wricEleRichMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVRIC1
    private Lccvric1 lccvric1 = new Lccvric1();
    //Original name: WRDF-ELE-RIC-INV-MAX
    private short wrdfEleRicInvMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRDF-TAB-RIC-DISINV
    private LazyArrayCopy<WrdfTabRicDisinv> wrdfTabRicDisinv = new LazyArrayCopy<WrdfTabRicDisinv>(new WrdfTabRicDisinv(), 1, WRDF_TAB_RIC_DISINV_MAXOCCURS);
    //Original name: WRIF-ELE-RIC-INV-MAX
    private short wrifEleRicInvMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRIF-TAB-RIC-INV
    private LazyArrayCopy<WrifTabRicInv> wrifTabRicInv = new LazyArrayCopy<WrifTabRicInv>(new WrifTabRicInv(), 1, WRIF_TAB_RIC_INV_MAXOCCURS);
    //Original name: WRAN-ELE-RAPP-ANAG-MAX
    private short wranEleRappAnagMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRAN-TAB-RAPP-ANAG
    private WranTabRappAnag[] wranTabRappAnag = new WranTabRappAnag[WRAN_TAB_RAPP_ANAG_MAXOCCURS];
    //Original name: WE15-ELE-EST-RAPP-ANAG-MAX
    private short we15EleEstRappAnagMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WE15-TAB-E15
    private We15TabE15[] we15TabE15 = new We15TabE15[WE15_TAB_E15_MAXOCCURS];
    //Original name: WRRE-ELE-RAPP-RETE-MAX
    private short wrreEleRappReteMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRRE-TAB-RAPP-RETE
    private WrreTabRappRete[] wrreTabRappRete = new WrreTabRappRete[WRRE_TAB_RAPP_RETE_MAXOCCURS];
    //Original name: WSPG-ELE-SOPRAP-GAR-MAX
    private short wspgEleSoprapGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WSPG-TAB-SPG
    private WspgTabSpg[] wspgTabSpg = new WspgTabSpg[WSPG_TAB_SPG_MAXOCCURS];
    //Original name: WSDI-ELE-STRA-INV-MAX
    private short wsdiEleStraInvMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WSDI-TAB-STRA-INV
    private WsdiTabStraInv[] wsdiTabStraInv = new WsdiTabStraInv[WSDI_TAB_STRA_INV_MAXOCCURS];
    //Original name: WTIT-ELE-TIT-CONT-MAX
    private short wtitEleTitContMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTIT-TAB-TIT-CONT
    private WtitTabTitCont[] wtitTabTitCont = new WtitTabTitCont[WTIT_TAB_TIT_CONT_MAXOCCURS];
    //Original name: WTCL-ELE-TIT-LIQ-MAX
    private short wtclEleTitLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVTCL1
    private Lccvtcl1 lccvtcl1 = new Lccvtcl1();
    //Original name: W1TGA-ELE-TRAN-MAX
    private short w1tgaEleTranMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: W1TGA-TAB-TRAN
    private LazyArrayCopy<W1tgaTabTran> w1tgaTabTran = new LazyArrayCopy<W1tgaTabTran>(new W1tgaTabTran(), 1, W1TGA_TAB_TRAN_MAXOCCURS);
    //Original name: WTLI-ELE-TRCH-LIQ-MAX
    private short wtliEleTrchLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTLI-TAB-TRCH-LIQ
    private LazyArrayCopy<WtliTabTrchLiq> wtliTabTrchLiq = new LazyArrayCopy<WtliTabTrchLiq>(new WtliTabTrchLiq(), 1, WTLI_TAB_TRCH_LIQ_MAXOCCURS);
    //Original name: WDAD-ELE-DFLT-ADES-MAX
    private short wdadEleDfltAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDAD1
    private Lccvdad1 lccvdad1 = new Lccvdad1();
    //Original name: WOCO-ELE-OGG-COLL-MAX
    private short wocoEleOggCollMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WOCO-TAB-OGG-COLL
    private WocoTabOggColl[] wocoTabOggColl = new WocoTabOggColl[WOCO_TAB_OGG_COLL_MAXOCCURS];
    //Original name: WDFL-ELE-DFL-MAX
    private short wdflEleDflMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDFL1
    private Lccvdfl1 lccvdfl1 = new Lccvdfl1();
    //Original name: WRST-ELE-RST-MAX
    private short wrstEleRstMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRST-TAB-RST
    private WrstTabRst[] wrstTabRst = new WrstTabRst[WRST_TAB_RST_MAXOCCURS];
    /**Original name: WL19-ELE-FND-MAX<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY 7 PER LA GESTIONE DELLE OCCURS
	 * ----------------------------------------------------------------*</pre>*/
    private short wl19EleFndMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WL19-TAB-FND
    private Wl19TabFnd[] wl19TabFnd = new Wl19TabFnd[WL19_TAB_FND_MAXOCCURS];
    //Original name: WL30-ELE-REINVST-POLI-MAX
    private short wl30EleReinvstPoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WL30-TAB-REINVST-POLI
    private Wl30TabReinvstPoli[] wl30TabReinvstPoli = new Wl30TabReinvstPoli[WL30_TAB_REINVST_POLI_MAXOCCURS];
    //Original name: WL23-ELE-VINC-PEG-MAX
    private short wl23EleVincPegMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WL23-TAB-VINC-PEG
    private Wl23TabVincPeg[] wl23TabVincPeg = new Wl23TabVincPeg[WL23_TAB_VINC_PEG_MAXOCCURS];
    //Original name: IVVC0217
    private Ivvc0217 ivvc0217 = new Ivvc0217();
    //Original name: WGOP-ELE-GARANZIA-OPZ-MAX
    private short wgopEleGaranziaOpzMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WGOP-TAB-GAR
    private WgrzTabGar[] wgopTabGar = new WgrzTabGar[WGOP_TAB_GAR_MAXOCCURS];
    //Original name: WTOP-ELE-TRAN-OPZ-MAX
    private short wtopEleTranOpzMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTOP-TAB-TRAN
    private WtgaTabTran[] wtopTabTran = new WtgaTabTran[WTOP_TAB_TRAN_MAXOCCURS];
    //Original name: IVVC0212
    private Ivvc0212 ivvc0212 = new Ivvc0212();
    //Original name: WCLT-ELE-CLAU-TXT-MAX
    private short wcltEleClauTxtMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WCLT-TAB-CLAU-TXT
    private WcltTabClauTxt[] wcltTabClauTxt = new WcltTabClauTxt[WCLT_TAB_CLAU_TXT_MAXOCCURS];
    //Original name: WP58-ELE-MAX-IMPST-BOL
    private short wp58EleMaxImpstBol = DefaultValues.BIN_SHORT_VAL;
    //Original name: WP58-TAB-IMPST-BOL
    private Wp58TabImpstBol[] wp58TabImpstBol = new Wp58TabImpstBol[WP58_TAB_IMPST_BOL_MAXOCCURS];
    //Original name: WP61-ELE-MAX-D-CRIST
    private short wp61EleMaxDCrist = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVP611
    private Lccvp611 lccvp611 = new Lccvp611();
    //Original name: WP67-ELE-MAX-EST-POLI-CPI-PR
    private short wp67EleMaxEstPoliCpiPr = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVP671
    private Lccvp671 lccvp671 = new Lccvp671();
    //Original name: WP01-ELE-MAX-RICH-EST
    private short wp01EleMaxRichEst = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVP011
    private Lccvp011 lccvp011 = new Lccvp011();
    /**Original name: IVVC0222-AREA-FND-X-TRANCHE<br>
	 * <pre>--  AREA CONTROVALORI FONDI X TRANCE</pre>*/
    private Ivvc0222AreaFndXTranche ivvc0222AreaFndXTranche = new Ivvc0222AreaFndXTranche();
    //Original name: WP86-ELE-MOT-LIQ-MAX
    private short wp86EleMotLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WP86-TAB-MOT-LIQ
    private Wp86TabMotLiq[] wp86TabMotLiq = new Wp86TabMotLiq[WP86_TAB_MOT_LIQ_MAXOCCURS];
    /**Original name: AREA-IVVC0223<br>
	 * <pre>--  AREA VARIABILI PER GARANZIA</pre>*/
    private AreaIvvc0223 areaIvvc0223 = new AreaIvvc0223();
    /**Original name: WK-MOVI-ORIG<br>
	 * <pre>--  VARIABILE PER APPOGGIO MOVIMENTO DI ORIGINE (X LIQUIDAZIONE)</pre>*/
    private String wkMoviOrig = DefaultValues.stringVal(Len.WK_MOVI_ORIG);
    //Original name: WP88-ELE-SER-VAL-MAX
    private short wp88EleSerValMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WP88-TAB-SERV-VAL
    private Wp88TabServVal[] wp88TabServVal = new Wp88TabServVal[WP88_TAB_SERV_VAL_MAXOCCURS];
    //Original name: WP89-ELE-DSERV-VAL-MAX
    private short wp89EleDservValMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WP89-TAB-DSERV-VAL
    private Wp89TabDservVal[] wp89TabDservVal = new Wp89TabDservVal[WP89_TAB_DSERV_VAL_MAXOCCURS];
    //Original name: WP56-QUEST-ADEG-VER-MAX
    private short wp56QuestAdegVerMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WP56-TAB-QUEST-ADEG-VER
    private Wp56TabQuestAdegVer[] wp56TabQuestAdegVer = new Wp56TabQuestAdegVer[WP56_TAB_QUEST_ADEG_VER_MAXOCCURS];
    //Original name: IVVC0224
    private Ivvc0224 ivvc0224 = new Ivvc0224();

    //==== CONSTRUCTORS ====
    public AreaBusiness() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_BUSINESS;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaBusinessBytes(buf);
    }

    public void init() {
        for (int wadeTabAdesIdx = 1; wadeTabAdesIdx <= WADE_TAB_ADES_MAXOCCURS; wadeTabAdesIdx++) {
            wadeTabAdes[wadeTabAdesIdx - 1] = new WadeTabAdes();
        }
        for (int wbepTabBeneficiariIdx = 1; wbepTabBeneficiariIdx <= WBEP_TAB_BENEFICIARI_MAXOCCURS; wbepTabBeneficiariIdx++) {
            wbepTabBeneficiari[wbepTabBeneficiariIdx - 1] = new WbepTabBeneficiari();
        }
        for (int wbelTabBeneLiqIdx = 1; wbelTabBeneLiqIdx <= WBEL_TAB_BENE_LIQ_MAXOCCURS; wbelTabBeneLiqIdx++) {
            wbelTabBeneLiq[wbelTabBeneLiqIdx - 1] = new WbelTabBeneLiq();
        }
        for (int wdeqTabDettQuestIdx = 1; wdeqTabDettQuestIdx <= WDEQ_TAB_DETT_QUEST_MAXOCCURS; wdeqTabDettQuestIdx++) {
            wdeqTabDettQuest[wdeqTabDettQuestIdx - 1] = new WdeqTabDettQuest();
        }
        for (int wdtcTabDtcIdx = 1; wdtcTabDtcIdx <= WDTC_TAB_DTC_MAXOCCURS; wdtcTabDtcIdx++) {
            wdtcTabDtc[wdtcTabDtcIdx - 1] = new WdtcTabDtc();
        }
        for (int wgrzTabGarIdx = 1; wgrzTabGarIdx <= WGRZ_TAB_GAR_MAXOCCURS; wgrzTabGarIdx++) {
            wgrzTabGar[wgrzTabGarIdx - 1] = new WgrzTabGar();
        }
        for (int wgrlTabGarLiqIdx = 1; wgrlTabGarLiqIdx <= WGRL_TAB_GAR_LIQ_MAXOCCURS; wgrlTabGarLiqIdx++) {
            wgrlTabGarLiq[wgrlTabGarLiqIdx - 1] = new WgrlTabGarLiq();
        }
        for (int wisoTabImpSostIdx = 1; wisoTabImpSostIdx <= WISO_TAB_IMP_SOST_MAXOCCURS; wisoTabImpSostIdx++) {
            wisoTabImpSost[wisoTabImpSostIdx - 1] = new WisoTabImpSost();
        }
        for (int wlquTabLiqIdx = 1; wlquTabLiqIdx <= WLQU_TAB_LIQ_MAXOCCURS; wlquTabLiqIdx++) {
            wlquTabLiq[wlquTabLiqIdx - 1] = new S089TabLiq();
        }
        for (int wmfzTabMoviFinrioIdx = 1; wmfzTabMoviFinrioIdx <= WMFZ_TAB_MOVI_FINRIO_MAXOCCURS; wmfzTabMoviFinrioIdx++) {
            wmfzTabMoviFinrio[wmfzTabMoviFinrioIdx - 1] = new WmfzTabMoviFinrio();
        }
        for (int wpogTabParamOggIdx = 1; wpogTabParamOggIdx <= WPOG_TAB_PARAM_OGG_MAXOCCURS; wpogTabParamOggIdx++) {
            wpogTabParamOgg[wpogTabParamOggIdx - 1] = new WpogTabParamOgg();
        }
        for (int wpmoTabParamMovIdx = 1; wpmoTabParamMovIdx <= WPMO_TAB_PARAM_MOV_MAXOCCURS; wpmoTabParamMovIdx++) {
            wpmoTabParamMov[wpmoTabParamMovIdx - 1] = new WpmoTabParamMov();
        }
        for (int wpliTabPercLiqIdx = 1; wpliTabPercLiqIdx <= WPLI_TAB_PERC_LIQ_MAXOCCURS; wpliTabPercLiqIdx++) {
            wpliTabPercLiq[wpliTabPercLiqIdx - 1] = new WpliTabPercLiq();
        }
        for (int wpreTabPrestitiIdx = 1; wpreTabPrestitiIdx <= WPRE_TAB_PRESTITI_MAXOCCURS; wpreTabPrestitiIdx++) {
            wpreTabPrestiti[wpreTabPrestitiIdx - 1] = new WpreTabPrestiti();
        }
        for (int wpvtTabProvTrIdx = 1; wpvtTabProvTrIdx <= WPVT_TAB_PROV_TR_MAXOCCURS; wpvtTabProvTrIdx++) {
            wpvtTabProvTr[wpvtTabProvTrIdx - 1] = new WpvtTabProvTr();
        }
        for (int wqueTabQuestIdx = 1; wqueTabQuestIdx <= WQUE_TAB_QUEST_MAXOCCURS; wqueTabQuestIdx++) {
            wqueTabQuest[wqueTabQuestIdx - 1] = new WqueTabQuest();
        }
        for (int wranTabRappAnagIdx = 1; wranTabRappAnagIdx <= WRAN_TAB_RAPP_ANAG_MAXOCCURS; wranTabRappAnagIdx++) {
            wranTabRappAnag[wranTabRappAnagIdx - 1] = new WranTabRappAnag();
        }
        for (int we15TabE15Idx = 1; we15TabE15Idx <= WE15_TAB_E15_MAXOCCURS; we15TabE15Idx++) {
            we15TabE15[we15TabE15Idx - 1] = new We15TabE15();
        }
        for (int wrreTabRappReteIdx = 1; wrreTabRappReteIdx <= WRRE_TAB_RAPP_RETE_MAXOCCURS; wrreTabRappReteIdx++) {
            wrreTabRappRete[wrreTabRappReteIdx - 1] = new WrreTabRappRete();
        }
        for (int wspgTabSpgIdx = 1; wspgTabSpgIdx <= WSPG_TAB_SPG_MAXOCCURS; wspgTabSpgIdx++) {
            wspgTabSpg[wspgTabSpgIdx - 1] = new WspgTabSpg();
        }
        for (int wsdiTabStraInvIdx = 1; wsdiTabStraInvIdx <= WSDI_TAB_STRA_INV_MAXOCCURS; wsdiTabStraInvIdx++) {
            wsdiTabStraInv[wsdiTabStraInvIdx - 1] = new WsdiTabStraInv();
        }
        for (int wtitTabTitContIdx = 1; wtitTabTitContIdx <= WTIT_TAB_TIT_CONT_MAXOCCURS; wtitTabTitContIdx++) {
            wtitTabTitCont[wtitTabTitContIdx - 1] = new WtitTabTitCont();
        }
        for (int wocoTabOggCollIdx = 1; wocoTabOggCollIdx <= WOCO_TAB_OGG_COLL_MAXOCCURS; wocoTabOggCollIdx++) {
            wocoTabOggColl[wocoTabOggCollIdx - 1] = new WocoTabOggColl();
        }
        for (int wrstTabRstIdx = 1; wrstTabRstIdx <= WRST_TAB_RST_MAXOCCURS; wrstTabRstIdx++) {
            wrstTabRst[wrstTabRstIdx - 1] = new WrstTabRst();
        }
        for (int wl19TabFndIdx = 1; wl19TabFndIdx <= WL19_TAB_FND_MAXOCCURS; wl19TabFndIdx++) {
            wl19TabFnd[wl19TabFndIdx - 1] = new Wl19TabFnd();
        }
        for (int wl30TabReinvstPoliIdx = 1; wl30TabReinvstPoliIdx <= WL30_TAB_REINVST_POLI_MAXOCCURS; wl30TabReinvstPoliIdx++) {
            wl30TabReinvstPoli[wl30TabReinvstPoliIdx - 1] = new Wl30TabReinvstPoli();
        }
        for (int wl23TabVincPegIdx = 1; wl23TabVincPegIdx <= WL23_TAB_VINC_PEG_MAXOCCURS; wl23TabVincPegIdx++) {
            wl23TabVincPeg[wl23TabVincPegIdx - 1] = new Wl23TabVincPeg();
        }
        for (int wgopTabGarIdx = 1; wgopTabGarIdx <= WGOP_TAB_GAR_MAXOCCURS; wgopTabGarIdx++) {
            wgopTabGar[wgopTabGarIdx - 1] = new WgrzTabGar();
        }
        for (int wtopTabTranIdx = 1; wtopTabTranIdx <= WTOP_TAB_TRAN_MAXOCCURS; wtopTabTranIdx++) {
            wtopTabTran[wtopTabTranIdx - 1] = new WtgaTabTran();
        }
        for (int wcltTabClauTxtIdx = 1; wcltTabClauTxtIdx <= WCLT_TAB_CLAU_TXT_MAXOCCURS; wcltTabClauTxtIdx++) {
            wcltTabClauTxt[wcltTabClauTxtIdx - 1] = new WcltTabClauTxt();
        }
        for (int wp58TabImpstBolIdx = 1; wp58TabImpstBolIdx <= WP58_TAB_IMPST_BOL_MAXOCCURS; wp58TabImpstBolIdx++) {
            wp58TabImpstBol[wp58TabImpstBolIdx - 1] = new Wp58TabImpstBol();
        }
        for (int wp86TabMotLiqIdx = 1; wp86TabMotLiqIdx <= WP86_TAB_MOT_LIQ_MAXOCCURS; wp86TabMotLiqIdx++) {
            wp86TabMotLiq[wp86TabMotLiqIdx - 1] = new Wp86TabMotLiq();
        }
        for (int wp88TabServValIdx = 1; wp88TabServValIdx <= WP88_TAB_SERV_VAL_MAXOCCURS; wp88TabServValIdx++) {
            wp88TabServVal[wp88TabServValIdx - 1] = new Wp88TabServVal();
        }
        for (int wp89TabDservValIdx = 1; wp89TabDservValIdx <= WP89_TAB_DSERV_VAL_MAXOCCURS; wp89TabDservValIdx++) {
            wp89TabDservVal[wp89TabDservValIdx - 1] = new Wp89TabDservVal();
        }
        for (int wp56TabQuestAdegVerIdx = 1; wp56TabQuestAdegVerIdx <= WP56_TAB_QUEST_ADEG_VER_MAXOCCURS; wp56TabQuestAdegVerIdx++) {
            wp56TabQuestAdegVer[wp56TabQuestAdegVerIdx - 1] = new Wp56TabQuestAdegVer();
        }
    }

    public void setAreaBusinessBytes(byte[] buffer) {
        setAreaBusinessBytes(buffer, 1);
    }

    public byte[] getAreaBusinessBytes() {
        byte[] buffer = new byte[Len.AREA_BUSINESS];
        return getAreaBusinessBytes(buffer, 1);
    }

    public void setAreaBusinessBytes(byte[] buffer, int offset) {
        int position = offset;
        setWadeAreaAdesioneBytes(buffer, position);
        position += Len.WADE_AREA_ADESIONE;
        setWbepAreaBenefBytes(buffer, position);
        position += Len.WBEP_AREA_BENEF;
        setWbelAreaBenefLiqBytes(buffer, position);
        position += Len.WBEL_AREA_BENEF_LIQ;
        setWdcoAreaDtCollettivaBytes(buffer, position);
        position += Len.WDCO_AREA_DT_COLLETTIVA;
        setWdfaAreaDtFiscAdesBytes(buffer, position);
        position += Len.WDFA_AREA_DT_FISC_ADES;
        setWdeqAreaDettQuestBytes(buffer, position);
        position += Len.WDEQ_AREA_DETT_QUEST;
        setWdtcAreaDettTitContBytes(buffer, position);
        position += Len.WDTC_AREA_DETT_TIT_CONT;
        setWgrzAreaGaranziaBytes(buffer, position);
        position += Len.WGRZ_AREA_GARANZIA;
        setWgrlAreaGaranziaLiqBytes(buffer, position);
        position += Len.WGRL_AREA_GARANZIA_LIQ;
        setWisoAreaImpostaSostBytes(buffer, position);
        position += Len.WISO_AREA_IMPOSTA_SOST;
        setWlquAreaLiquidazioneBytes(buffer, position);
        position += Len.WLQU_AREA_LIQUIDAZIONE;
        setWmovAreaMovimentoBytes(buffer, position);
        position += Len.WMOV_AREA_MOVIMENTO;
        setWmfzAreaMoviFinrioBytes(buffer, position);
        position += Len.WMFZ_AREA_MOVI_FINRIO;
        setWpogAreaParamOggBytes(buffer, position);
        position += Len.WPOG_AREA_PARAM_OGG;
        setWpmoAreaParamMovBytes(buffer, position);
        position += Len.WPMO_AREA_PARAM_MOV;
        setWpliAreaPercLiqBytes(buffer, position);
        position += Len.WPLI_AREA_PERC_LIQ;
        setWpolAreaPolizzaBytes(buffer, position);
        position += Len.WPOL_AREA_POLIZZA;
        setWpreAreaPrestitiBytes(buffer, position);
        position += Len.WPRE_AREA_PRESTITI;
        setWpvtAreaProvBytes(buffer, position);
        position += Len.WPVT_AREA_PROV;
        setWqueAreaQuestBytes(buffer, position);
        position += Len.WQUE_AREA_QUEST;
        setWricAreaRichBytes(buffer, position);
        position += Len.WRIC_AREA_RICH;
        setWrdfAreaRichDisinvFndBytes(buffer, position);
        position += Len.WRDF_AREA_RICH_DISINV_FND;
        setWrifAreaRichInvFndBytes(buffer, position);
        position += Len.WRIF_AREA_RICH_INV_FND;
        setWranAreaRappAnagBytes(buffer, position);
        position += Len.WRAN_AREA_RAPP_ANAG;
        setWe15AreaEstRappAnagBytes(buffer, position);
        position += Len.WE15_AREA_EST_RAPP_ANAG;
        setWrreAreaRappReteBytes(buffer, position);
        position += Len.WRRE_AREA_RAPP_RETE;
        setWspgAreaSoprapGarBytes(buffer, position);
        position += Len.WSPG_AREA_SOPRAP_GAR;
        setWsdiAreaStraInvBytes(buffer, position);
        position += Len.WSDI_AREA_STRA_INV;
        setWtitAreaTitContBytes(buffer, position);
        position += Len.WTIT_AREA_TIT_CONT;
        setWtclAreaTitLiqBytes(buffer, position);
        position += Len.WTCL_AREA_TIT_LIQ;
        setW1tgaAreaTrancheBytes(buffer, position);
        position += Len.W1TGA_AREA_TRANCHE;
        setWtliAreaTrchLiqBytes(buffer, position);
        position += Len.WTLI_AREA_TRCH_LIQ;
        setWdadAreaDefaultAdesBytes(buffer, position);
        position += Len.WDAD_AREA_DEFAULT_ADES;
        setWocoAreaOggCollBytes(buffer, position);
        position += Len.WOCO_AREA_OGG_COLL;
        setWdflAreaDflBytes(buffer, position);
        position += Len.WDFL_AREA_DFL;
        setWrstAreaRstBytes(buffer, position);
        position += Len.WRST_AREA_RST;
        setWl19AreaQuoteBytes(buffer, position);
        position += Len.WL19_AREA_QUOTE;
        setWl30AreaReinvstPoliBytes(buffer, position);
        position += Len.WL30_AREA_REINVST_POLI;
        setWl23AreaVincPegBytes(buffer, position);
        position += Len.WL23_AREA_VINC_PEG;
        setWopzAreaOpzioniBytes(buffer, position);
        position += Len.WOPZ_AREA_OPZIONI;
        setWgopAreaGaranziaOpzBytes(buffer, position);
        position += Len.WGOP_AREA_GARANZIA_OPZ;
        setWtopAreaTrancheOpzBytes(buffer, position);
        position += Len.WTOP_AREA_TRANCHE_OPZ;
        setWcntAreaDatiContestBytes(buffer, position);
        position += Len.WCNT_AREA_DATI_CONTEST;
        setWcltAreaClauTxtBytes(buffer, position);
        position += Len.WCLT_AREA_CLAU_TXT;
        setWp58AreaImpostaBolloBytes(buffer, position);
        position += Len.WP58_AREA_IMPOSTA_BOLLO;
        setWp61AreaDCristBytes(buffer, position);
        position += Len.WP61_AREA_D_CRIST;
        setWp67AreaEstPoliCpiPrBytes(buffer, position);
        position += Len.WP67_AREA_EST_POLI_CPI_PR;
        setWp01AreaRichEstBytes(buffer, position);
        position += Len.WP01_AREA_RICH_EST;
        ivvc0222AreaFndXTranche.setIvvc0222AreaFndXTrancheBytes(buffer, position);
        position += Ivvc0222AreaFndXTranche.Len.IVVC0222_AREA_FND_X_TRANCHE;
        setWp86AreaMotLiqBytes(buffer, position);
        position += Len.WP86_AREA_MOT_LIQ;
        areaIvvc0223.setAreaIvvc0223Bytes(buffer, position);
        position += AreaIvvc0223.Len.AREA_IVVC0223;
        wkMoviOrig = MarshalByte.readFixedString(buffer, position, Len.WK_MOVI_ORIG);
        position += Len.WK_MOVI_ORIG;
        setWp88AreaServValBytes(buffer, position);
        position += Len.WP88_AREA_SERV_VAL;
        setWp89AreaDservValBytes(buffer, position);
        position += Len.WP89_AREA_DSERV_VAL;
        setWp56AreaQuestAdegVerBytes(buffer, position);
        position += Len.WP56_AREA_QUEST_ADEG_VER;
        setWcdgAreaCommisGestBytes(buffer, position);
    }

    public byte[] getAreaBusinessBytes(byte[] buffer, int offset) {
        int position = offset;
        getWadeAreaAdesioneBytes(buffer, position);
        position += Len.WADE_AREA_ADESIONE;
        getWbepAreaBenefBytes(buffer, position);
        position += Len.WBEP_AREA_BENEF;
        getWbelAreaBenefLiqBytes(buffer, position);
        position += Len.WBEL_AREA_BENEF_LIQ;
        getWdcoAreaDtCollettivaBytes(buffer, position);
        position += Len.WDCO_AREA_DT_COLLETTIVA;
        getWdfaAreaDtFiscAdesBytes(buffer, position);
        position += Len.WDFA_AREA_DT_FISC_ADES;
        getWdeqAreaDettQuestBytes(buffer, position);
        position += Len.WDEQ_AREA_DETT_QUEST;
        getWdtcAreaDettTitContBytes(buffer, position);
        position += Len.WDTC_AREA_DETT_TIT_CONT;
        getWgrzAreaGaranziaBytes(buffer, position);
        position += Len.WGRZ_AREA_GARANZIA;
        getWgrlAreaGaranziaLiqBytes(buffer, position);
        position += Len.WGRL_AREA_GARANZIA_LIQ;
        getWisoAreaImpostaSostBytes(buffer, position);
        position += Len.WISO_AREA_IMPOSTA_SOST;
        getWlquAreaLiquidazioneBytes(buffer, position);
        position += Len.WLQU_AREA_LIQUIDAZIONE;
        getWmovAreaMovimentoBytes(buffer, position);
        position += Len.WMOV_AREA_MOVIMENTO;
        getWmfzAreaMoviFinrioBytes(buffer, position);
        position += Len.WMFZ_AREA_MOVI_FINRIO;
        getWpogAreaParamOggBytes(buffer, position);
        position += Len.WPOG_AREA_PARAM_OGG;
        getWpmoAreaParamMovBytes(buffer, position);
        position += Len.WPMO_AREA_PARAM_MOV;
        getWpliAreaPercLiqBytes(buffer, position);
        position += Len.WPLI_AREA_PERC_LIQ;
        getWpolAreaPolizzaBytes(buffer, position);
        position += Len.WPOL_AREA_POLIZZA;
        getWpreAreaPrestitiBytes(buffer, position);
        position += Len.WPRE_AREA_PRESTITI;
        getWpvtAreaProvBytes(buffer, position);
        position += Len.WPVT_AREA_PROV;
        getWqueAreaQuestBytes(buffer, position);
        position += Len.WQUE_AREA_QUEST;
        getWricAreaRichBytes(buffer, position);
        position += Len.WRIC_AREA_RICH;
        getWrdfAreaRichDisinvFndBytes(buffer, position);
        position += Len.WRDF_AREA_RICH_DISINV_FND;
        getWrifAreaRichInvFndBytes(buffer, position);
        position += Len.WRIF_AREA_RICH_INV_FND;
        getWranAreaRappAnagBytes(buffer, position);
        position += Len.WRAN_AREA_RAPP_ANAG;
        getWe15AreaEstRappAnagBytes(buffer, position);
        position += Len.WE15_AREA_EST_RAPP_ANAG;
        getWrreAreaRappReteBytes(buffer, position);
        position += Len.WRRE_AREA_RAPP_RETE;
        getWspgAreaSoprapGarBytes(buffer, position);
        position += Len.WSPG_AREA_SOPRAP_GAR;
        getWsdiAreaStraInvBytes(buffer, position);
        position += Len.WSDI_AREA_STRA_INV;
        getWtitAreaTitContBytes(buffer, position);
        position += Len.WTIT_AREA_TIT_CONT;
        getWtclAreaTitLiqBytes(buffer, position);
        position += Len.WTCL_AREA_TIT_LIQ;
        getW1tgaAreaTrancheBytes(buffer, position);
        position += Len.W1TGA_AREA_TRANCHE;
        getWtliAreaTrchLiqBytes(buffer, position);
        position += Len.WTLI_AREA_TRCH_LIQ;
        getWdadAreaDefaultAdesBytes(buffer, position);
        position += Len.WDAD_AREA_DEFAULT_ADES;
        getWocoAreaOggCollBytes(buffer, position);
        position += Len.WOCO_AREA_OGG_COLL;
        getWdflAreaDflBytes(buffer, position);
        position += Len.WDFL_AREA_DFL;
        getWrstAreaRstBytes(buffer, position);
        position += Len.WRST_AREA_RST;
        getWl19AreaQuoteBytes(buffer, position);
        position += Len.WL19_AREA_QUOTE;
        getWl30AreaReinvstPoliBytes(buffer, position);
        position += Len.WL30_AREA_REINVST_POLI;
        getWl23AreaVincPegBytes(buffer, position);
        position += Len.WL23_AREA_VINC_PEG;
        getWopzAreaOpzioniBytes(buffer, position);
        position += Len.WOPZ_AREA_OPZIONI;
        getWgopAreaGaranziaOpzBytes(buffer, position);
        position += Len.WGOP_AREA_GARANZIA_OPZ;
        getWtopAreaTrancheOpzBytes(buffer, position);
        position += Len.WTOP_AREA_TRANCHE_OPZ;
        getWcntAreaDatiContestBytes(buffer, position);
        position += Len.WCNT_AREA_DATI_CONTEST;
        getWcltAreaClauTxtBytes(buffer, position);
        position += Len.WCLT_AREA_CLAU_TXT;
        getWp58AreaImpostaBolloBytes(buffer, position);
        position += Len.WP58_AREA_IMPOSTA_BOLLO;
        getWp61AreaDCristBytes(buffer, position);
        position += Len.WP61_AREA_D_CRIST;
        getWp67AreaEstPoliCpiPrBytes(buffer, position);
        position += Len.WP67_AREA_EST_POLI_CPI_PR;
        getWp01AreaRichEstBytes(buffer, position);
        position += Len.WP01_AREA_RICH_EST;
        ivvc0222AreaFndXTranche.getIvvc0222AreaFndXTrancheBytes(buffer, position);
        position += Ivvc0222AreaFndXTranche.Len.IVVC0222_AREA_FND_X_TRANCHE;
        getWp86AreaMotLiqBytes(buffer, position);
        position += Len.WP86_AREA_MOT_LIQ;
        areaIvvc0223.getAreaIvvc0223Bytes(buffer, position);
        position += AreaIvvc0223.Len.AREA_IVVC0223;
        MarshalByte.writeString(buffer, position, wkMoviOrig, Len.WK_MOVI_ORIG);
        position += Len.WK_MOVI_ORIG;
        getWp88AreaServValBytes(buffer, position);
        position += Len.WP88_AREA_SERV_VAL;
        getWp89AreaDservValBytes(buffer, position);
        position += Len.WP89_AREA_DSERV_VAL;
        getWp56AreaQuestAdegVerBytes(buffer, position);
        position += Len.WP56_AREA_QUEST_ADEG_VER;
        getWcdgAreaCommisGestBytes(buffer, position);
        return buffer;
    }

    public String getWadeAreaAdesioneFormatted() {
        return MarshalByteExt.bufferToStr(getWadeAreaAdesioneBytes());
    }

    /**Original name: WADE-AREA-ADESIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA INPUT VARIABILI
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA
	 *    LUNG.
	 * ----------------------------------------------------------------*
	 * --  AREA ADESIONE</pre>*/
    public byte[] getWadeAreaAdesioneBytes() {
        byte[] buffer = new byte[Len.WADE_AREA_ADESIONE];
        return getWadeAreaAdesioneBytes(buffer, 1);
    }

    public void setWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        wadeEleAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WADE_TAB_ADES_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wadeTabAdes[idx - 1].setWadeTabAdesBytes(buffer, position);
                position += WadeTabAdes.Len.WADE_TAB_ADES;
            }
            else {
                wadeTabAdes[idx - 1].initWadeTabAdesSpaces();
                position += WadeTabAdes.Len.WADE_TAB_ADES;
            }
        }
    }

    public byte[] getWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wadeEleAdesMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WADE_TAB_ADES_MAXOCCURS; idx++) {
            wadeTabAdes[idx - 1].getWadeTabAdesBytes(buffer, position);
            position += WadeTabAdes.Len.WADE_TAB_ADES;
        }
        return buffer;
    }

    public void setWadeEleAdesMax(short wadeEleAdesMax) {
        this.wadeEleAdesMax = wadeEleAdesMax;
    }

    public short getWadeEleAdesMax() {
        return this.wadeEleAdesMax;
    }

    public String getWbepAreaBenefFormatted() {
        return MarshalByteExt.bufferToStr(getWbepAreaBenefBytes());
    }

    /**Original name: WBEP-AREA-BENEF<br>
	 * <pre>--  AREA BENEFICIARI</pre>*/
    public byte[] getWbepAreaBenefBytes() {
        byte[] buffer = new byte[Len.WBEP_AREA_BENEF];
        return getWbepAreaBenefBytes(buffer, 1);
    }

    public void setWbepAreaBenefBytes(byte[] buffer, int offset) {
        int position = offset;
        wbepEleBenefMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WBEP_TAB_BENEFICIARI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wbepTabBeneficiari[idx - 1].setWbepTabBeneficiariBytes(buffer, position);
                position += WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
            }
            else {
                wbepTabBeneficiari[idx - 1].initWbepTabBeneficiariSpaces();
                position += WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
            }
        }
    }

    public byte[] getWbepAreaBenefBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wbepEleBenefMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WBEP_TAB_BENEFICIARI_MAXOCCURS; idx++) {
            wbepTabBeneficiari[idx - 1].getWbepTabBeneficiariBytes(buffer, position);
            position += WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
        }
        return buffer;
    }

    public void setWbepEleBenefMax(short wbepEleBenefMax) {
        this.wbepEleBenefMax = wbepEleBenefMax;
    }

    public short getWbepEleBenefMax() {
        return this.wbepEleBenefMax;
    }

    public String getWbelAreaBenefLiqFormatted() {
        return MarshalByteExt.bufferToStr(getWbelAreaBenefLiqBytes());
    }

    /**Original name: WBEL-AREA-BENEF-LIQ<br>
	 * <pre>--  AREA BENEFICIARIO DI LIQUIDAZIONE</pre>*/
    public byte[] getWbelAreaBenefLiqBytes() {
        byte[] buffer = new byte[Len.WBEL_AREA_BENEF_LIQ];
        return getWbelAreaBenefLiqBytes(buffer, 1);
    }

    public void setWbelAreaBenefLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wbelEleBenefLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WBEL_TAB_BENE_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wbelTabBeneLiq[idx - 1].setWbelTabBeneLiqBytes(buffer, position);
                position += WbelTabBeneLiq.Len.WBEL_TAB_BENE_LIQ;
            }
            else {
                wbelTabBeneLiq[idx - 1].initWbelTabBeneLiqSpaces();
                position += WbelTabBeneLiq.Len.WBEL_TAB_BENE_LIQ;
            }
        }
    }

    public byte[] getWbelAreaBenefLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wbelEleBenefLiqMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WBEL_TAB_BENE_LIQ_MAXOCCURS; idx++) {
            wbelTabBeneLiq[idx - 1].getWbelTabBeneLiqBytes(buffer, position);
            position += WbelTabBeneLiq.Len.WBEL_TAB_BENE_LIQ;
        }
        return buffer;
    }

    public void setWbelEleBenefLiqMax(short wbelEleBenefLiqMax) {
        this.wbelEleBenefLiqMax = wbelEleBenefLiqMax;
    }

    public short getWbelEleBenefLiqMax() {
        return this.wbelEleBenefLiqMax;
    }

    public String getWdcoAreaDtCollettivaFormatted() {
        return MarshalByteExt.bufferToStr(getWdcoAreaDtCollettivaBytes());
    }

    /**Original name: WDCO-AREA-DT-COLLETTIVA<br>
	 * <pre>--  AREA DATI COLLETTIVA</pre>*/
    public byte[] getWdcoAreaDtCollettivaBytes() {
        byte[] buffer = new byte[Len.WDCO_AREA_DT_COLLETTIVA];
        return getWdcoAreaDtCollettivaBytes(buffer, 1);
    }

    public void setWdcoAreaDtCollettivaBytes(byte[] buffer, int offset) {
        int position = offset;
        wdcoEleCollMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWdcoTabCollBytes(buffer, position);
    }

    public byte[] getWdcoAreaDtCollettivaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdcoEleCollMax);
        position += Types.SHORT_SIZE;
        getWdcoTabCollBytes(buffer, position);
        return buffer;
    }

    public void setWdcoEleCollMax(short wdcoEleCollMax) {
        this.wdcoEleCollMax = wdcoEleCollMax;
    }

    public short getWdcoEleCollMax() {
        return this.wdcoEleCollMax;
    }

    public void setWdcoTabCollBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdco1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdco1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdco1.Len.Int.ID_PTF, 0));
        position += Lccvdco1.Len.ID_PTF;
        lccvdco1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdcoTabCollBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdco1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdco1.getIdPtf(), Lccvdco1.Len.Int.ID_PTF, 0);
        position += Lccvdco1.Len.ID_PTF;
        lccvdco1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public String getWdfaAreaDtFiscAdesFormatted() {
        return MarshalByteExt.bufferToStr(getWdfaAreaDtFiscAdesBytes());
    }

    /**Original name: WDFA-AREA-DT-FISC-ADES<br>
	 * <pre>--  AREA DATI FISCALE ADESIONE</pre>*/
    public byte[] getWdfaAreaDtFiscAdesBytes() {
        byte[] buffer = new byte[Len.WDFA_AREA_DT_FISC_ADES];
        return getWdfaAreaDtFiscAdesBytes(buffer, 1);
    }

    public void setWdfaAreaDtFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        wdfaEleFiscAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWdfaTabFiscAdesBytes(buffer, position);
    }

    public byte[] getWdfaAreaDtFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdfaEleFiscAdesMax);
        position += Types.SHORT_SIZE;
        getWdfaTabFiscAdesBytes(buffer, position);
        return buffer;
    }

    public void setWdfaEleFiscAdesMax(short wdfaEleFiscAdesMax) {
        this.wdfaEleFiscAdesMax = wdfaEleFiscAdesMax;
    }

    public short getWdfaEleFiscAdesMax() {
        return this.wdfaEleFiscAdesMax;
    }

    public void setWdfaTabFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdfa1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdfa1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdfa1.Len.Int.ID_PTF, 0));
        position += Lccvdfa1.Len.ID_PTF;
        lccvdfa1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdfaTabFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdfa1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdfa1.getIdPtf(), Lccvdfa1.Len.Int.ID_PTF, 0);
        position += Lccvdfa1.Len.ID_PTF;
        lccvdfa1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public String getWdeqAreaDettQuestFormatted() {
        return MarshalByteExt.bufferToStr(getWdeqAreaDettQuestBytes());
    }

    /**Original name: WDEQ-AREA-DETT-QUEST<br>
	 * <pre>--  AREA DETTAGLIO QUESTIONARIO</pre>*/
    public byte[] getWdeqAreaDettQuestBytes() {
        byte[] buffer = new byte[Len.WDEQ_AREA_DETT_QUEST];
        return getWdeqAreaDettQuestBytes(buffer, 1);
    }

    public void setWdeqAreaDettQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        wdeqEleDettQuestMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WDEQ_TAB_DETT_QUEST_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wdeqTabDettQuest[idx - 1].setWdeqTabDettQuestBytes(buffer, position);
                position += WdeqTabDettQuest.Len.WDEQ_TAB_DETT_QUEST;
            }
            else {
                wdeqTabDettQuest[idx - 1].initWdeqTabDettQuestSpaces();
                position += WdeqTabDettQuest.Len.WDEQ_TAB_DETT_QUEST;
            }
        }
    }

    public byte[] getWdeqAreaDettQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdeqEleDettQuestMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WDEQ_TAB_DETT_QUEST_MAXOCCURS; idx++) {
            wdeqTabDettQuest[idx - 1].getWdeqTabDettQuestBytes(buffer, position);
            position += WdeqTabDettQuest.Len.WDEQ_TAB_DETT_QUEST;
        }
        return buffer;
    }

    public void setWdeqEleDettQuestMax(short wdeqEleDettQuestMax) {
        this.wdeqEleDettQuestMax = wdeqEleDettQuestMax;
    }

    public short getWdeqEleDettQuestMax() {
        return this.wdeqEleDettQuestMax;
    }

    public String getWdtcAreaDettTitContFormatted() {
        return MarshalByteExt.bufferToStr(getWdtcAreaDettTitContBytes());
    }

    /**Original name: WDTC-AREA-DETT-TIT-CONT<br>
	 * <pre>--  AREA DETTAGLIO TITOLO CONTABILE</pre>*/
    public byte[] getWdtcAreaDettTitContBytes() {
        byte[] buffer = new byte[Len.WDTC_AREA_DETT_TIT_CONT];
        return getWdtcAreaDettTitContBytes(buffer, 1);
    }

    public void setWdtcAreaDettTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        wdtcEleDettTitMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WDTC_TAB_DTC_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wdtcTabDtc[idx - 1].setWdtcTabDtcBytes(buffer, position);
                position += WdtcTabDtc.Len.WDTC_TAB_DTC;
            }
            else {
                wdtcTabDtc[idx - 1].initWdtcTabDtcSpaces();
                position += WdtcTabDtc.Len.WDTC_TAB_DTC;
            }
        }
    }

    public byte[] getWdtcAreaDettTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdtcEleDettTitMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WDTC_TAB_DTC_MAXOCCURS; idx++) {
            wdtcTabDtc[idx - 1].getWdtcTabDtcBytes(buffer, position);
            position += WdtcTabDtc.Len.WDTC_TAB_DTC;
        }
        return buffer;
    }

    public void setWdtcEleDettTitMax(short wdtcEleDettTitMax) {
        this.wdtcEleDettTitMax = wdtcEleDettTitMax;
    }

    public short getWdtcEleDettTitMax() {
        return this.wdtcEleDettTitMax;
    }

    public String getWgrzAreaGaranziaFormatted() {
        return MarshalByteExt.bufferToStr(getWgrzAreaGaranziaBytes());
    }

    /**Original name: WGRZ-AREA-GARANZIA<br>
	 * <pre>--  AREA GARANZIA</pre>*/
    public byte[] getWgrzAreaGaranziaBytes() {
        byte[] buffer = new byte[Len.WGRZ_AREA_GARANZIA];
        return getWgrzAreaGaranziaBytes(buffer, 1);
    }

    public void setWgrzAreaGaranziaBytes(byte[] buffer, int offset) {
        int position = offset;
        wgrzEleGaranziaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGRZ_TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wgrzTabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                wgrzTabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public byte[] getWgrzAreaGaranziaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wgrzEleGaranziaMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGRZ_TAB_GAR_MAXOCCURS; idx++) {
            wgrzTabGar[idx - 1].getTabGarBytes(buffer, position);
            position += WgrzTabGar.Len.TAB_GAR;
        }
        return buffer;
    }

    public void setWgrzEleGaranziaMax(short wgrzEleGaranziaMax) {
        this.wgrzEleGaranziaMax = wgrzEleGaranziaMax;
    }

    public short getWgrzEleGaranziaMax() {
        return this.wgrzEleGaranziaMax;
    }

    public String getWgrlAreaGaranziaLiqFormatted() {
        return MarshalByteExt.bufferToStr(getWgrlAreaGaranziaLiqBytes());
    }

    /**Original name: WGRL-AREA-GARANZIA-LIQ<br>
	 * <pre>--  AREA GARANZIA DI LIQUIDAZIONE</pre>*/
    public byte[] getWgrlAreaGaranziaLiqBytes() {
        byte[] buffer = new byte[Len.WGRL_AREA_GARANZIA_LIQ];
        return getWgrlAreaGaranziaLiqBytes(buffer, 1);
    }

    public void setWgrlAreaGaranziaLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wgrlEleGarLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGRL_TAB_GAR_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wgrlTabGarLiq[idx - 1].setWgrlTabGarLiqBytes(buffer, position);
                position += WgrlTabGarLiq.Len.WGRL_TAB_GAR_LIQ;
            }
            else {
                wgrlTabGarLiq[idx - 1].initWgrlTabGarLiqSpaces();
                position += WgrlTabGarLiq.Len.WGRL_TAB_GAR_LIQ;
            }
        }
    }

    public byte[] getWgrlAreaGaranziaLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wgrlEleGarLiqMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGRL_TAB_GAR_LIQ_MAXOCCURS; idx++) {
            wgrlTabGarLiq[idx - 1].getWgrlTabGarLiqBytes(buffer, position);
            position += WgrlTabGarLiq.Len.WGRL_TAB_GAR_LIQ;
        }
        return buffer;
    }

    public void setWgrlEleGarLiqMax(short wgrlEleGarLiqMax) {
        this.wgrlEleGarLiqMax = wgrlEleGarLiqMax;
    }

    public short getWgrlEleGarLiqMax() {
        return this.wgrlEleGarLiqMax;
    }

    public String getWisoAreaImpostaSostFormatted() {
        return MarshalByteExt.bufferToStr(getWisoAreaImpostaSostBytes());
    }

    /**Original name: WISO-AREA-IMPOSTA-SOST<br>
	 * <pre>--  AREA IMPOSTA SOSTITUTIVA</pre>*/
    public byte[] getWisoAreaImpostaSostBytes() {
        byte[] buffer = new byte[Len.WISO_AREA_IMPOSTA_SOST];
        return getWisoAreaImpostaSostBytes(buffer, 1);
    }

    public void setWisoAreaImpostaSostBytes(byte[] buffer, int offset) {
        int position = offset;
        wisoEleImpSostMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WISO_TAB_IMP_SOST_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wisoTabImpSost[idx - 1].setWisoTabImpSostBytes(buffer, position);
                position += WisoTabImpSost.Len.WISO_TAB_IMP_SOST;
            }
            else {
                wisoTabImpSost[idx - 1].initWisoTabImpSostSpaces();
                position += WisoTabImpSost.Len.WISO_TAB_IMP_SOST;
            }
        }
    }

    public byte[] getWisoAreaImpostaSostBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wisoEleImpSostMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WISO_TAB_IMP_SOST_MAXOCCURS; idx++) {
            wisoTabImpSost[idx - 1].getWisoTabImpSostBytes(buffer, position);
            position += WisoTabImpSost.Len.WISO_TAB_IMP_SOST;
        }
        return buffer;
    }

    public void setWisoEleImpSostMax(short wisoEleImpSostMax) {
        this.wisoEleImpSostMax = wisoEleImpSostMax;
    }

    public short getWisoEleImpSostMax() {
        return this.wisoEleImpSostMax;
    }

    public String getWlquAreaLiquidazioneFormatted() {
        return MarshalByteExt.bufferToStr(getWlquAreaLiquidazioneBytes());
    }

    /**Original name: WLQU-AREA-LIQUIDAZIONE<br>
	 * <pre>--  AREA LIQUIDAZIONE</pre>*/
    public byte[] getWlquAreaLiquidazioneBytes() {
        byte[] buffer = new byte[Len.WLQU_AREA_LIQUIDAZIONE];
        return getWlquAreaLiquidazioneBytes(buffer, 1);
    }

    public void setWlquAreaLiquidazioneBytes(byte[] buffer, int offset) {
        int position = offset;
        wlquEleLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WLQU_TAB_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wlquTabLiq[idx - 1].setWlquTabLiqBytes(buffer, position);
                position += S089TabLiq.Len.WLQU_TAB_LIQ;
            }
            else {
                wlquTabLiq[idx - 1].initWlquTabLiqSpaces();
                position += S089TabLiq.Len.WLQU_TAB_LIQ;
            }
        }
    }

    public byte[] getWlquAreaLiquidazioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wlquEleLiqMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WLQU_TAB_LIQ_MAXOCCURS; idx++) {
            wlquTabLiq[idx - 1].getWlquTabLiqBytes(buffer, position);
            position += S089TabLiq.Len.WLQU_TAB_LIQ;
        }
        return buffer;
    }

    public void setWlquEleLiqMax(short wlquEleLiqMax) {
        this.wlquEleLiqMax = wlquEleLiqMax;
    }

    public short getWlquEleLiqMax() {
        return this.wlquEleLiqMax;
    }

    public String getWmovAreaMovimentoFormatted() {
        return MarshalByteExt.bufferToStr(getWmovAreaMovimentoBytes());
    }

    /**Original name: WMOV-AREA-MOVIMENTO<br>
	 * <pre>--  AREA MOVIMENTO</pre>*/
    public byte[] getWmovAreaMovimentoBytes() {
        byte[] buffer = new byte[Len.WMOV_AREA_MOVIMENTO];
        return getWmovAreaMovimentoBytes(buffer, 1);
    }

    public void setWmovAreaMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        wmovEleMoviMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWmovTabMoviBytes(buffer, position);
    }

    public byte[] getWmovAreaMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wmovEleMoviMax);
        position += Types.SHORT_SIZE;
        getWmovTabMoviBytes(buffer, position);
        return buffer;
    }

    public void setWmovEleMoviMax(short wmovEleMoviMax) {
        this.wmovEleMoviMax = wmovEleMoviMax;
    }

    public short getWmovEleMoviMax() {
        return this.wmovEleMoviMax;
    }

    public void setWmovTabMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvmov1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvmov1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvmov1.Len.Int.ID_PTF, 0));
        position += Lccvmov1.Len.ID_PTF;
        lccvmov1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWmovTabMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvmov1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvmov1.getIdPtf(), Lccvmov1.Len.Int.ID_PTF, 0);
        position += Lccvmov1.Len.ID_PTF;
        lccvmov1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWmfzAreaMoviFinrioBytes(byte[] buffer, int offset) {
        int position = offset;
        wmfzEleMoviFinrioMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WMFZ_TAB_MOVI_FINRIO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wmfzTabMoviFinrio[idx - 1].setWmfzTabMoviFinrioBytes(buffer, position);
                position += WmfzTabMoviFinrio.Len.WMFZ_TAB_MOVI_FINRIO;
            }
            else {
                wmfzTabMoviFinrio[idx - 1].initWmfzTabMoviFinrioSpaces();
                position += WmfzTabMoviFinrio.Len.WMFZ_TAB_MOVI_FINRIO;
            }
        }
    }

    public byte[] getWmfzAreaMoviFinrioBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wmfzEleMoviFinrioMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WMFZ_TAB_MOVI_FINRIO_MAXOCCURS; idx++) {
            wmfzTabMoviFinrio[idx - 1].getWmfzTabMoviFinrioBytes(buffer, position);
            position += WmfzTabMoviFinrio.Len.WMFZ_TAB_MOVI_FINRIO;
        }
        return buffer;
    }

    public void setWmfzEleMoviFinrioMax(short wmfzEleMoviFinrioMax) {
        this.wmfzEleMoviFinrioMax = wmfzEleMoviFinrioMax;
    }

    public short getWmfzEleMoviFinrioMax() {
        return this.wmfzEleMoviFinrioMax;
    }

    public void setWpogAreaParamOggFormatted(String data) {
        byte[] buffer = new byte[Len.WPOG_AREA_PARAM_OGG];
        MarshalByte.writeString(buffer, 1, data, Len.WPOG_AREA_PARAM_OGG);
        setWpogAreaParamOggBytes(buffer, 1);
    }

    public String getWpogAreaParamOggFormatted() {
        return MarshalByteExt.bufferToStr(getWpogAreaParamOggBytes());
    }

    /**Original name: WPOG-AREA-PARAM-OGG<br>
	 * <pre>--  AREA PARAMETRO OGGETTO</pre>*/
    public byte[] getWpogAreaParamOggBytes() {
        byte[] buffer = new byte[Len.WPOG_AREA_PARAM_OGG];
        return getWpogAreaParamOggBytes(buffer, 1);
    }

    public void setWpogAreaParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        wpogEleParamOggMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPOG_TAB_PARAM_OGG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpogTabParamOgg[idx - 1].setWpogTabParamOggBytes(buffer, position);
                position += WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
            }
            else {
                wpogTabParamOgg[idx - 1].initWpogTabParamOggSpaces();
                position += WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
            }
        }
    }

    public byte[] getWpogAreaParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpogEleParamOggMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPOG_TAB_PARAM_OGG_MAXOCCURS; idx++) {
            wpogTabParamOgg[idx - 1].getWpogTabParamOggBytes(buffer, position);
            position += WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
        }
        return buffer;
    }

    public void setWpogEleParamOggMax(short wpogEleParamOggMax) {
        this.wpogEleParamOggMax = wpogEleParamOggMax;
    }

    public short getWpogEleParamOggMax() {
        return this.wpogEleParamOggMax;
    }

    public String getWpmoAreaParamMovFormatted() {
        return MarshalByteExt.bufferToStr(getWpmoAreaParamMovBytes());
    }

    /**Original name: WPMO-AREA-PARAM-MOV<br>
	 * <pre>--  AREA PARAMETRO MOVIMENTO</pre>*/
    public byte[] getWpmoAreaParamMovBytes() {
        byte[] buffer = new byte[Len.WPMO_AREA_PARAM_MOV];
        return getWpmoAreaParamMovBytes(buffer, 1);
    }

    public void setWpmoAreaParamMovBytes(byte[] buffer, int offset) {
        int position = offset;
        wpmoEleParamMovMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPMO_TAB_PARAM_MOV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpmoTabParamMov[idx - 1].setWpmoTabParamMovBytes(buffer, position);
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
            else {
                wpmoTabParamMov[idx - 1].initWpmoTabParamMovSpaces();
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
        }
    }

    public byte[] getWpmoAreaParamMovBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpmoEleParamMovMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPMO_TAB_PARAM_MOV_MAXOCCURS; idx++) {
            wpmoTabParamMov[idx - 1].getWpmoTabParamMovBytes(buffer, position);
            position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
        }
        return buffer;
    }

    public void setWpmoEleParamMovMax(short wpmoEleParamMovMax) {
        this.wpmoEleParamMovMax = wpmoEleParamMovMax;
    }

    public short getWpmoEleParamMovMax() {
        return this.wpmoEleParamMovMax;
    }

    public String getWpliAreaPercLiqFormatted() {
        return MarshalByteExt.bufferToStr(getWpliAreaPercLiqBytes());
    }

    /**Original name: WPLI-AREA-PERC-LIQ<br>
	 * <pre>--  AREA PERCIPIENTE LIQUIDAZIONE</pre>*/
    public byte[] getWpliAreaPercLiqBytes() {
        byte[] buffer = new byte[Len.WPLI_AREA_PERC_LIQ];
        return getWpliAreaPercLiqBytes(buffer, 1);
    }

    public void setWpliAreaPercLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wpliElePercLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPLI_TAB_PERC_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpliTabPercLiq[idx - 1].setWpliTabPercLiqBytes(buffer, position);
                position += WpliTabPercLiq.Len.WPLI_TAB_PERC_LIQ;
            }
            else {
                wpliTabPercLiq[idx - 1].initWpliTabPercLiqSpaces();
                position += WpliTabPercLiq.Len.WPLI_TAB_PERC_LIQ;
            }
        }
    }

    public byte[] getWpliAreaPercLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpliElePercLiqMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPLI_TAB_PERC_LIQ_MAXOCCURS; idx++) {
            wpliTabPercLiq[idx - 1].getWpliTabPercLiqBytes(buffer, position);
            position += WpliTabPercLiq.Len.WPLI_TAB_PERC_LIQ;
        }
        return buffer;
    }

    public void setWpliElePercLiqMax(short wpliElePercLiqMax) {
        this.wpliElePercLiqMax = wpliElePercLiqMax;
    }

    public short getWpliElePercLiqMax() {
        return this.wpliElePercLiqMax;
    }

    public String getWpolAreaPolizzaFormatted() {
        return MarshalByteExt.bufferToStr(getWpolAreaPolizzaBytes());
    }

    /**Original name: WPOL-AREA-POLIZZA<br>
	 * <pre>--  AREA POLIZZA</pre>*/
    public byte[] getWpolAreaPolizzaBytes() {
        byte[] buffer = new byte[Len.WPOL_AREA_POLIZZA];
        return getWpolAreaPolizzaBytes(buffer, 1);
    }

    public void setWpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        wpolElePoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWpolTabPoliBytes(buffer, position);
    }

    public byte[] getWpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpolElePoliMax);
        position += Types.SHORT_SIZE;
        getWpolTabPoliBytes(buffer, position);
        return buffer;
    }

    public void setWpolElePoliMax(short wpolElePoliMax) {
        this.wpolElePoliMax = wpolElePoliMax;
    }

    public short getWpolElePoliMax() {
        return this.wpolElePoliMax;
    }

    public void setWpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1.Len.Int.ID_PTF, 0));
        position += Lccvpol1.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvpol1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvpol1.getIdPtf(), Lccvpol1.Len.Int.ID_PTF, 0);
        position += Lccvpol1.Len.ID_PTF;
        lccvpol1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWpreAreaPrestitiBytes(byte[] buffer, int offset) {
        int position = offset;
        wpreElePrestitiMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPRE_TAB_PRESTITI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpreTabPrestiti[idx - 1].setWpreTabPrestitiBytes(buffer, position);
                position += WpreTabPrestiti.Len.WPRE_TAB_PRESTITI;
            }
            else {
                wpreTabPrestiti[idx - 1].initWpreTabPrestitiSpaces();
                position += WpreTabPrestiti.Len.WPRE_TAB_PRESTITI;
            }
        }
    }

    public byte[] getWpreAreaPrestitiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpreElePrestitiMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPRE_TAB_PRESTITI_MAXOCCURS; idx++) {
            wpreTabPrestiti[idx - 1].getWpreTabPrestitiBytes(buffer, position);
            position += WpreTabPrestiti.Len.WPRE_TAB_PRESTITI;
        }
        return buffer;
    }

    public void setWpreElePrestitiMax(short wpreElePrestitiMax) {
        this.wpreElePrestitiMax = wpreElePrestitiMax;
    }

    public short getWpreElePrestitiMax() {
        return this.wpreElePrestitiMax;
    }

    public String getWpvtAreaProvFormatted() {
        return MarshalByteExt.bufferToStr(getWpvtAreaProvBytes());
    }

    /**Original name: WPVT-AREA-PROV<br>
	 * <pre>*--  AREA PROVVIGIONI DI TRANCHE</pre>*/
    public byte[] getWpvtAreaProvBytes() {
        byte[] buffer = new byte[Len.WPVT_AREA_PROV];
        return getWpvtAreaProvBytes(buffer, 1);
    }

    public void setWpvtAreaProvBytes(byte[] buffer, int offset) {
        int position = offset;
        wpvtEleProvMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPVT_TAB_PROV_TR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpvtTabProvTr[idx - 1].setWpvtTabProvTrBytes(buffer, position);
                position += WpvtTabProvTr.Len.WPVT_TAB_PROV_TR;
            }
            else {
                wpvtTabProvTr[idx - 1].initWpvtTabProvTrSpaces();
                position += WpvtTabProvTr.Len.WPVT_TAB_PROV_TR;
            }
        }
    }

    public byte[] getWpvtAreaProvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpvtEleProvMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPVT_TAB_PROV_TR_MAXOCCURS; idx++) {
            wpvtTabProvTr[idx - 1].getWpvtTabProvTrBytes(buffer, position);
            position += WpvtTabProvTr.Len.WPVT_TAB_PROV_TR;
        }
        return buffer;
    }

    public void setWpvtEleProvMax(short wpvtEleProvMax) {
        this.wpvtEleProvMax = wpvtEleProvMax;
    }

    public short getWpvtEleProvMax() {
        return this.wpvtEleProvMax;
    }

    public String getWqueAreaQuestFormatted() {
        return MarshalByteExt.bufferToStr(getWqueAreaQuestBytes());
    }

    /**Original name: WQUE-AREA-QUEST<br>
	 * <pre>--  AREA QUESTIONARIO</pre>*/
    public byte[] getWqueAreaQuestBytes() {
        byte[] buffer = new byte[Len.WQUE_AREA_QUEST];
        return getWqueAreaQuestBytes(buffer, 1);
    }

    public void setWqueAreaQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        wqueEleQuestMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WQUE_TAB_QUEST_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wqueTabQuest[idx - 1].setWqueTabQuestBytes(buffer, position);
                position += WqueTabQuest.Len.WQUE_TAB_QUEST;
            }
            else {
                wqueTabQuest[idx - 1].initWqueTabQuestSpaces();
                position += WqueTabQuest.Len.WQUE_TAB_QUEST;
            }
        }
    }

    public byte[] getWqueAreaQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wqueEleQuestMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WQUE_TAB_QUEST_MAXOCCURS; idx++) {
            wqueTabQuest[idx - 1].getWqueTabQuestBytes(buffer, position);
            position += WqueTabQuest.Len.WQUE_TAB_QUEST;
        }
        return buffer;
    }

    public void setWqueEleQuestMax(short wqueEleQuestMax) {
        this.wqueEleQuestMax = wqueEleQuestMax;
    }

    public short getWqueEleQuestMax() {
        return this.wqueEleQuestMax;
    }

    public void setWricAreaRichBytes(byte[] buffer, int offset) {
        int position = offset;
        wricEleRichMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWricTabRichBytes(buffer, position);
    }

    public byte[] getWricAreaRichBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wricEleRichMax);
        position += Types.SHORT_SIZE;
        getWricTabRichBytes(buffer, position);
        return buffer;
    }

    public void setWricEleRichMax(short wricEleRichMax) {
        this.wricEleRichMax = wricEleRichMax;
    }

    public short getWricEleRichMax() {
        return this.wricEleRichMax;
    }

    public void setWricTabRichBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvric1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvric1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvric1.Len.Int.ID_PTF, 0));
        position += Lccvric1.Len.ID_PTF;
        lccvric1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWricTabRichBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvric1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvric1.getIdPtf(), Lccvric1.Len.Int.ID_PTF, 0);
        position += Lccvric1.Len.ID_PTF;
        lccvric1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public String getWrdfAreaRichDisinvFndFormatted() {
        return MarshalByteExt.bufferToStr(getWrdfAreaRichDisinvFndBytes());
    }

    /**Original name: WRDF-AREA-RICH-DISINV-FND<br>
	 * <pre>--  AREA RICHIESTA DISINVESTIMENTO FONDO</pre>*/
    public byte[] getWrdfAreaRichDisinvFndBytes() {
        byte[] buffer = new byte[Len.WRDF_AREA_RICH_DISINV_FND];
        return getWrdfAreaRichDisinvFndBytes(buffer, 1);
    }

    public void setWrdfAreaRichDisinvFndBytes(byte[] buffer, int offset) {
        int position = offset;
        wrdfEleRicInvMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRDF_TAB_RIC_DISINV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wrdfTabRicDisinv.get(idx - 1).setWrdfTabRicDisinvBytes(buffer, position);
                position += WrdfTabRicDisinv.Len.WRDF_TAB_RIC_DISINV;
            }
            else {
                WrdfTabRicDisinv temp_wrdfTabRicDisinv = new WrdfTabRicDisinv();
                temp_wrdfTabRicDisinv.initWrdfTabRicDisinvSpaces();
                getWrdfTabRicDisinvObj().fill(temp_wrdfTabRicDisinv);
                position += WrdfTabRicDisinv.Len.WRDF_TAB_RIC_DISINV * (WRDF_TAB_RIC_DISINV_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public byte[] getWrdfAreaRichDisinvFndBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wrdfEleRicInvMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRDF_TAB_RIC_DISINV_MAXOCCURS; idx++) {
            wrdfTabRicDisinv.get(idx - 1).getWrdfTabRicDisinvBytes(buffer, position);
            position += WrdfTabRicDisinv.Len.WRDF_TAB_RIC_DISINV;
        }
        return buffer;
    }

    public void setWrdfEleRicInvMax(short wrdfEleRicInvMax) {
        this.wrdfEleRicInvMax = wrdfEleRicInvMax;
    }

    public short getWrdfEleRicInvMax() {
        return this.wrdfEleRicInvMax;
    }

    public String getWrifAreaRichInvFndFormatted() {
        return MarshalByteExt.bufferToStr(getWrifAreaRichInvFndBytes());
    }

    /**Original name: WRIF-AREA-RICH-INV-FND<br>
	 * <pre>--  AREA RICHIESTA INVESTIMENTO FONDO</pre>*/
    public byte[] getWrifAreaRichInvFndBytes() {
        byte[] buffer = new byte[Len.WRIF_AREA_RICH_INV_FND];
        return getWrifAreaRichInvFndBytes(buffer, 1);
    }

    public void setWrifAreaRichInvFndBytes(byte[] buffer, int offset) {
        int position = offset;
        wrifEleRicInvMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRIF_TAB_RIC_INV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wrifTabRicInv.get(idx - 1).setWrifTabRicInvBytes(buffer, position);
                position += WrifTabRicInv.Len.WRIF_TAB_RIC_INV;
            }
            else {
                WrifTabRicInv temp_wrifTabRicInv = new WrifTabRicInv();
                temp_wrifTabRicInv.initWrifTabRicInvSpaces();
                getWrifTabRicInvObj().fill(temp_wrifTabRicInv);
                position += WrifTabRicInv.Len.WRIF_TAB_RIC_INV * (WRIF_TAB_RIC_INV_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public byte[] getWrifAreaRichInvFndBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wrifEleRicInvMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRIF_TAB_RIC_INV_MAXOCCURS; idx++) {
            wrifTabRicInv.get(idx - 1).getWrifTabRicInvBytes(buffer, position);
            position += WrifTabRicInv.Len.WRIF_TAB_RIC_INV;
        }
        return buffer;
    }

    public void setWrifEleRicInvMax(short wrifEleRicInvMax) {
        this.wrifEleRicInvMax = wrifEleRicInvMax;
    }

    public short getWrifEleRicInvMax() {
        return this.wrifEleRicInvMax;
    }

    public String getWranAreaRappAnagFormatted() {
        return MarshalByteExt.bufferToStr(getWranAreaRappAnagBytes());
    }

    /**Original name: WRAN-AREA-RAPP-ANAG<br>
	 * <pre>--  AREA RAPPORTO ANAGRAFICO</pre>*/
    public byte[] getWranAreaRappAnagBytes() {
        byte[] buffer = new byte[Len.WRAN_AREA_RAPP_ANAG];
        return getWranAreaRappAnagBytes(buffer, 1);
    }

    public void setWranAreaRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        wranEleRappAnagMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRAN_TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wranTabRappAnag[idx - 1].setWranTabRappAnagBytes(buffer, position);
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
            else {
                wranTabRappAnag[idx - 1].initWranTabRappAnagSpaces();
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
        }
    }

    public byte[] getWranAreaRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wranEleRappAnagMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRAN_TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            wranTabRappAnag[idx - 1].getWranTabRappAnagBytes(buffer, position);
            position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
        }
        return buffer;
    }

    public void setWranEleRappAnagMax(short wranEleRappAnagMax) {
        this.wranEleRappAnagMax = wranEleRappAnagMax;
    }

    public short getWranEleRappAnagMax() {
        return this.wranEleRappAnagMax;
    }

    public void setWe15AreaEstRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        we15EleEstRappAnagMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WE15_TAB_E15_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                we15TabE15[idx - 1].setWe15TabE15Bytes(buffer, position);
                position += We15TabE15.Len.WE15_TAB_E15;
            }
            else {
                we15TabE15[idx - 1].initWe15TabE15Spaces();
                position += We15TabE15.Len.WE15_TAB_E15;
            }
        }
    }

    public byte[] getWe15AreaEstRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, we15EleEstRappAnagMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WE15_TAB_E15_MAXOCCURS; idx++) {
            we15TabE15[idx - 1].getWe15TabE15Bytes(buffer, position);
            position += We15TabE15.Len.WE15_TAB_E15;
        }
        return buffer;
    }

    public void setWe15EleEstRappAnagMax(short we15EleEstRappAnagMax) {
        this.we15EleEstRappAnagMax = we15EleEstRappAnagMax;
    }

    public short getWe15EleEstRappAnagMax() {
        return this.we15EleEstRappAnagMax;
    }

    public String getWrreAreaRappReteFormatted() {
        return MarshalByteExt.bufferToStr(getWrreAreaRappReteBytes());
    }

    /**Original name: WRRE-AREA-RAPP-RETE<br>
	 * <pre>--  AREA RAPPORTO RETE</pre>*/
    public byte[] getWrreAreaRappReteBytes() {
        byte[] buffer = new byte[Len.WRRE_AREA_RAPP_RETE];
        return getWrreAreaRappReteBytes(buffer, 1);
    }

    public void setWrreAreaRappReteBytes(byte[] buffer, int offset) {
        int position = offset;
        wrreEleRappReteMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRRE_TAB_RAPP_RETE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wrreTabRappRete[idx - 1].setWrreTabRappReteBytes(buffer, position);
                position += WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
            }
            else {
                wrreTabRappRete[idx - 1].initWrreTabRappReteSpaces();
                position += WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
            }
        }
    }

    public byte[] getWrreAreaRappReteBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wrreEleRappReteMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRRE_TAB_RAPP_RETE_MAXOCCURS; idx++) {
            wrreTabRappRete[idx - 1].getTabRreBytes(buffer, position);
            position += WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
        }
        return buffer;
    }

    public void setWrreEleRappReteMax(short wrreEleRappReteMax) {
        this.wrreEleRappReteMax = wrreEleRappReteMax;
    }

    public short getWrreEleRappReteMax() {
        return this.wrreEleRappReteMax;
    }

    public String getWspgAreaSoprapGarFormatted() {
        return MarshalByteExt.bufferToStr(getWspgAreaSoprapGarBytes());
    }

    /**Original name: WSPG-AREA-SOPRAP-GAR<br>
	 * <pre>--  AREA SOPRAPREMIO DI GARANZIA</pre>*/
    public byte[] getWspgAreaSoprapGarBytes() {
        byte[] buffer = new byte[Len.WSPG_AREA_SOPRAP_GAR];
        return getWspgAreaSoprapGarBytes(buffer, 1);
    }

    public void setWspgAreaSoprapGarBytes(byte[] buffer, int offset) {
        int position = offset;
        wspgEleSoprapGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WSPG_TAB_SPG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wspgTabSpg[idx - 1].setWspgTabSpgBytes(buffer, position);
                position += WspgTabSpg.Len.WSPG_TAB_SPG;
            }
            else {
                wspgTabSpg[idx - 1].initWspgTabSpgSpaces();
                position += WspgTabSpg.Len.WSPG_TAB_SPG;
            }
        }
    }

    public byte[] getWspgAreaSoprapGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wspgEleSoprapGarMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WSPG_TAB_SPG_MAXOCCURS; idx++) {
            wspgTabSpg[idx - 1].getWspgTabSpgBytes(buffer, position);
            position += WspgTabSpg.Len.WSPG_TAB_SPG;
        }
        return buffer;
    }

    public void setWspgEleSoprapGarMax(short wspgEleSoprapGarMax) {
        this.wspgEleSoprapGarMax = wspgEleSoprapGarMax;
    }

    public short getWspgEleSoprapGarMax() {
        return this.wspgEleSoprapGarMax;
    }

    public String getWsdiAreaStraInvFormatted() {
        return MarshalByteExt.bufferToStr(getWsdiAreaStraInvBytes());
    }

    /**Original name: WSDI-AREA-STRA-INV<br>
	 * <pre>--  AREA STRATEGIA DI INVESTIMENTO</pre>*/
    public byte[] getWsdiAreaStraInvBytes() {
        byte[] buffer = new byte[Len.WSDI_AREA_STRA_INV];
        return getWsdiAreaStraInvBytes(buffer, 1);
    }

    public void setWsdiAreaStraInvBytes(byte[] buffer, int offset) {
        int position = offset;
        wsdiEleStraInvMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WSDI_TAB_STRA_INV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wsdiTabStraInv[idx - 1].setWsdiTabStraInvBytes(buffer, position);
                position += WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
            }
            else {
                wsdiTabStraInv[idx - 1].initWsdiTabStraInvSpaces();
                position += WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
            }
        }
    }

    public byte[] getWsdiAreaStraInvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wsdiEleStraInvMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WSDI_TAB_STRA_INV_MAXOCCURS; idx++) {
            wsdiTabStraInv[idx - 1].getWsdiTabStraInvBytes(buffer, position);
            position += WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
        }
        return buffer;
    }

    public void setWsdiEleStraInvMax(short wsdiEleStraInvMax) {
        this.wsdiEleStraInvMax = wsdiEleStraInvMax;
    }

    public short getWsdiEleStraInvMax() {
        return this.wsdiEleStraInvMax;
    }

    public String getWtitAreaTitContFormatted() {
        return MarshalByteExt.bufferToStr(getWtitAreaTitContBytes());
    }

    /**Original name: WTIT-AREA-TIT-CONT<br>
	 * <pre>--  AREA TITOLO CONTABILE</pre>*/
    public byte[] getWtitAreaTitContBytes() {
        byte[] buffer = new byte[Len.WTIT_AREA_TIT_CONT];
        return getWtitAreaTitContBytes(buffer, 1);
    }

    public void setWtitAreaTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        wtitEleTitContMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTIT_TAB_TIT_CONT_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wtitTabTitCont[idx - 1].setWtitTabTitContBytes(buffer, position);
                position += WtitTabTitCont.Len.WTIT_TAB_TIT_CONT;
            }
            else {
                wtitTabTitCont[idx - 1].initWtitTabTitContSpaces();
                position += WtitTabTitCont.Len.WTIT_TAB_TIT_CONT;
            }
        }
    }

    public byte[] getWtitAreaTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtitEleTitContMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTIT_TAB_TIT_CONT_MAXOCCURS; idx++) {
            wtitTabTitCont[idx - 1].getWtitTabTitContBytes(buffer, position);
            position += WtitTabTitCont.Len.WTIT_TAB_TIT_CONT;
        }
        return buffer;
    }

    public void setWtitEleTitContMax(short wtitEleTitContMax) {
        this.wtitEleTitContMax = wtitEleTitContMax;
    }

    public short getWtitEleTitContMax() {
        return this.wtitEleTitContMax;
    }

    public void setWtclAreaTitLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wtclEleTitLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWtclTabTitLiqBytes(buffer, position);
    }

    public byte[] getWtclAreaTitLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtclEleTitLiqMax);
        position += Types.SHORT_SIZE;
        getWtclTabTitLiqBytes(buffer, position);
        return buffer;
    }

    public void setWtclEleTitLiqMax(short wtclEleTitLiqMax) {
        this.wtclEleTitLiqMax = wtclEleTitLiqMax;
    }

    public short getWtclEleTitLiqMax() {
        return this.wtclEleTitLiqMax;
    }

    public void setWtclTabTitLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvtcl1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvtcl1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvtcl1.Len.Int.ID_PTF, 0));
        position += Lccvtcl1.Len.ID_PTF;
        lccvtcl1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWtclTabTitLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvtcl1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvtcl1.getIdPtf(), Lccvtcl1.Len.Int.ID_PTF, 0);
        position += Lccvtcl1.Len.ID_PTF;
        lccvtcl1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setW1tgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        w1tgaEleTranMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= W1TGA_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                w1tgaTabTran.get(idx - 1).setW1tgaTabTranBytes(buffer, position);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN;
            }
            else {
                W1tgaTabTran temp_w1tgaTabTran = new W1tgaTabTran();
                temp_w1tgaTabTran.initW1tgaTabTranSpaces();
                getW1tgaTabTranObj().fill(temp_w1tgaTabTran);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN * (W1TGA_TAB_TRAN_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public byte[] getW1tgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, w1tgaEleTranMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= W1TGA_TAB_TRAN_MAXOCCURS; idx++) {
            w1tgaTabTran.get(idx - 1).getW1tgaTabTranBytes(buffer, position);
            position += W1tgaTabTran.Len.W1TGA_TAB_TRAN;
        }
        return buffer;
    }

    public void setW1tgaEleTranMax(short w1tgaEleTranMax) {
        this.w1tgaEleTranMax = w1tgaEleTranMax;
    }

    public short getW1tgaEleTranMax() {
        return this.w1tgaEleTranMax;
    }

    public String getWtliAreaTrchLiqFormatted() {
        return MarshalByteExt.bufferToStr(getWtliAreaTrchLiqBytes());
    }

    /**Original name: WTLI-AREA-TRCH-LIQ<br>
	 * <pre>-- AREA TRANCHE GARANZIA DI LIQUIDAZIONE</pre>*/
    public byte[] getWtliAreaTrchLiqBytes() {
        byte[] buffer = new byte[Len.WTLI_AREA_TRCH_LIQ];
        return getWtliAreaTrchLiqBytes(buffer, 1);
    }

    public void setWtliAreaTrchLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wtliEleTrchLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTLI_TAB_TRCH_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wtliTabTrchLiq.get(idx - 1).setWtliTabTrchLiqBytes(buffer, position);
                position += WtliTabTrchLiq.Len.WTLI_TAB_TRCH_LIQ;
            }
            else {
                WtliTabTrchLiq temp_wtliTabTrchLiq = new WtliTabTrchLiq();
                temp_wtliTabTrchLiq.initWtliTabTrchLiqSpaces();
                getWtliTabTrchLiqObj().fill(temp_wtliTabTrchLiq);
                position += WtliTabTrchLiq.Len.WTLI_TAB_TRCH_LIQ * (WTLI_TAB_TRCH_LIQ_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public byte[] getWtliAreaTrchLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtliEleTrchLiqMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTLI_TAB_TRCH_LIQ_MAXOCCURS; idx++) {
            wtliTabTrchLiq.get(idx - 1).getWtliTabTrchLiqBytes(buffer, position);
            position += WtliTabTrchLiq.Len.WTLI_TAB_TRCH_LIQ;
        }
        return buffer;
    }

    public void setWtliEleTrchLiqMax(short wtliEleTrchLiqMax) {
        this.wtliEleTrchLiqMax = wtliEleTrchLiqMax;
    }

    public short getWtliEleTrchLiqMax() {
        return this.wtliEleTrchLiqMax;
    }

    public String getWdadAreaDefaultAdesFormatted() {
        return MarshalByteExt.bufferToStr(getWdadAreaDefaultAdesBytes());
    }

    /**Original name: WDAD-AREA-DEFAULT-ADES<br>
	 * <pre>-- AREA DEFAULT ADESIONE</pre>*/
    public byte[] getWdadAreaDefaultAdesBytes() {
        byte[] buffer = new byte[Len.WDAD_AREA_DEFAULT_ADES];
        return getWdadAreaDefaultAdesBytes(buffer, 1);
    }

    public void setWdadAreaDefaultAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        wdadEleDfltAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWdadTabDfltAdesBytes(buffer, position);
    }

    public byte[] getWdadAreaDefaultAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdadEleDfltAdesMax);
        position += Types.SHORT_SIZE;
        getWdadTabDfltAdesBytes(buffer, position);
        return buffer;
    }

    public void setWdadEleDfltAdesMax(short wdadEleDfltAdesMax) {
        this.wdadEleDfltAdesMax = wdadEleDfltAdesMax;
    }

    public short getWdadEleDfltAdesMax() {
        return this.wdadEleDfltAdesMax;
    }

    public void setWdadTabDfltAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdad1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdad1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdad1.Len.Int.ID_PTF, 0));
        position += Lccvdad1.Len.ID_PTF;
        lccvdad1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdadTabDfltAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdad1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdad1.getIdPtf(), Lccvdad1.Len.Int.ID_PTF, 0);
        position += Lccvdad1.Len.ID_PTF;
        lccvdad1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public String getWocoAreaOggCollFormatted() {
        return MarshalByteExt.bufferToStr(getWocoAreaOggCollBytes());
    }

    /**Original name: WOCO-AREA-OGG-COLL<br>
	 * <pre>-- AREA OGGETTO COLLEGATO</pre>*/
    public byte[] getWocoAreaOggCollBytes() {
        byte[] buffer = new byte[Len.WOCO_AREA_OGG_COLL];
        return getWocoAreaOggCollBytes(buffer, 1);
    }

    public void setWocoAreaOggCollBytes(byte[] buffer, int offset) {
        int position = offset;
        wocoEleOggCollMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WOCO_TAB_OGG_COLL_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wocoTabOggColl[idx - 1].setWocoTabOggCollBytes(buffer, position);
                position += WocoTabOggColl.Len.WOCO_TAB_OGG_COLL;
            }
            else {
                wocoTabOggColl[idx - 1].initWocoTabOggCollSpaces();
                position += WocoTabOggColl.Len.WOCO_TAB_OGG_COLL;
            }
        }
    }

    public byte[] getWocoAreaOggCollBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wocoEleOggCollMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WOCO_TAB_OGG_COLL_MAXOCCURS; idx++) {
            wocoTabOggColl[idx - 1].getWocoTabOggCollBytes(buffer, position);
            position += WocoTabOggColl.Len.WOCO_TAB_OGG_COLL;
        }
        return buffer;
    }

    public void setWocoEleOggCollMax(short wocoEleOggCollMax) {
        this.wocoEleOggCollMax = wocoEleOggCollMax;
    }

    public short getWocoEleOggCollMax() {
        return this.wocoEleOggCollMax;
    }

    public String getWdflAreaDflFormatted() {
        return MarshalByteExt.bufferToStr(getWdflAreaDflBytes());
    }

    /**Original name: WDFL-AREA-DFL<br>
	 * <pre>-- AREA DATI FORZATI DI LIQUIDAZIONE</pre>*/
    public byte[] getWdflAreaDflBytes() {
        byte[] buffer = new byte[Len.WDFL_AREA_DFL];
        return getWdflAreaDflBytes(buffer, 1);
    }

    public void setWdflAreaDflBytes(byte[] buffer, int offset) {
        int position = offset;
        wdflEleDflMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWdflTabDflBytes(buffer, position);
    }

    public byte[] getWdflAreaDflBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdflEleDflMax);
        position += Types.SHORT_SIZE;
        getWdflTabDflBytes(buffer, position);
        return buffer;
    }

    public void setWdflEleDflMax(short wdflEleDflMax) {
        this.wdflEleDflMax = wdflEleDflMax;
    }

    public short getWdflEleDflMax() {
        return this.wdflEleDflMax;
    }

    public void setWdflTabDflBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdfl1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdfl1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdfl1.Len.Int.ID_PTF, 0));
        position += Lccvdfl1.Len.ID_PTF;
        lccvdfl1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdflTabDflBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdfl1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdfl1.getIdPtf(), Lccvdfl1.Len.Int.ID_PTF, 0);
        position += Lccvdfl1.Len.ID_PTF;
        lccvdfl1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public String getWrstAreaRstFormatted() {
        return MarshalByteExt.bufferToStr(getWrstAreaRstBytes());
    }

    /**Original name: WRST-AREA-RST<br>
	 * <pre>-- AREA RISERVA DI TRANCHE</pre>*/
    public byte[] getWrstAreaRstBytes() {
        byte[] buffer = new byte[Len.WRST_AREA_RST];
        return getWrstAreaRstBytes(buffer, 1);
    }

    public void setWrstAreaRstBytes(byte[] buffer, int offset) {
        int position = offset;
        wrstEleRstMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRST_TAB_RST_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wrstTabRst[idx - 1].setWrstTabRstBytes(buffer, position);
                position += WrstTabRst.Len.WRST_TAB_RST;
            }
            else {
                wrstTabRst[idx - 1].initWrstTabRstSpaces();
                position += WrstTabRst.Len.WRST_TAB_RST;
            }
        }
    }

    public byte[] getWrstAreaRstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wrstEleRstMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRST_TAB_RST_MAXOCCURS; idx++) {
            wrstTabRst[idx - 1].getWrstTabRstBytes(buffer, position);
            position += WrstTabRst.Len.WRST_TAB_RST;
        }
        return buffer;
    }

    public void setWrstEleRstMax(short wrstEleRstMax) {
        this.wrstEleRstMax = wrstEleRstMax;
    }

    public short getWrstEleRstMax() {
        return this.wrstEleRstMax;
    }

    public String getWl19AreaQuoteFormatted() {
        return MarshalByteExt.bufferToStr(getWl19AreaQuoteBytes());
    }

    /**Original name: WL19-AREA-QUOTE<br>
	 * <pre>-- AREA QUOTAZ FONDI UNIT</pre>*/
    public byte[] getWl19AreaQuoteBytes() {
        byte[] buffer = new byte[Len.WL19_AREA_QUOTE];
        return getWl19AreaQuoteBytes(buffer, 1);
    }

    public void setWl19AreaQuoteBytes(byte[] buffer, int offset) {
        int position = offset;
        wl19EleFndMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWl19TabellaBytes(buffer, position);
    }

    public byte[] getWl19AreaQuoteBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wl19EleFndMax);
        position += Types.SHORT_SIZE;
        getWl19TabellaBytes(buffer, position);
        return buffer;
    }

    public void setWl19EleFndMax(short wl19EleFndMax) {
        this.wl19EleFndMax = wl19EleFndMax;
    }

    public short getWl19EleFndMax() {
        return this.wl19EleFndMax;
    }

    public void setWl19TabellaBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= WL19_TAB_FND_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wl19TabFnd[idx - 1].setWl19TabFndBytes(buffer, position);
                position += Wl19TabFnd.Len.WL19_TAB_FND;
            }
            else {
                wl19TabFnd[idx - 1].initWl19TabFndSpaces();
                position += Wl19TabFnd.Len.WL19_TAB_FND;
            }
        }
    }

    public byte[] getWl19TabellaBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= WL19_TAB_FND_MAXOCCURS; idx++) {
            wl19TabFnd[idx - 1].getWl19TabFndBytes(buffer, position);
            position += Wl19TabFnd.Len.WL19_TAB_FND;
        }
        return buffer;
    }

    public String getWl30AreaReinvstPoliFormatted() {
        return MarshalByteExt.bufferToStr(getWl30AreaReinvstPoliBytes());
    }

    /**Original name: WL30-AREA-REINVST-POLI<br>
	 * <pre>-- AREA REINVEST</pre>*/
    public byte[] getWl30AreaReinvstPoliBytes() {
        byte[] buffer = new byte[Len.WL30_AREA_REINVST_POLI];
        return getWl30AreaReinvstPoliBytes(buffer, 1);
    }

    public void setWl30AreaReinvstPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        wl30EleReinvstPoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WL30_TAB_REINVST_POLI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wl30TabReinvstPoli[idx - 1].setWl30TabReinvstPoliBytes(buffer, position);
                position += Wl30TabReinvstPoli.Len.WL30_TAB_REINVST_POLI;
            }
            else {
                wl30TabReinvstPoli[idx - 1].initWl30TabReinvstPoliSpaces();
                position += Wl30TabReinvstPoli.Len.WL30_TAB_REINVST_POLI;
            }
        }
    }

    public byte[] getWl30AreaReinvstPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wl30EleReinvstPoliMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WL30_TAB_REINVST_POLI_MAXOCCURS; idx++) {
            wl30TabReinvstPoli[idx - 1].getWl30TabReinvstPoliBytes(buffer, position);
            position += Wl30TabReinvstPoli.Len.WL30_TAB_REINVST_POLI;
        }
        return buffer;
    }

    public void setWl30EleReinvstPoliMax(short wl30EleReinvstPoliMax) {
        this.wl30EleReinvstPoliMax = wl30EleReinvstPoliMax;
    }

    public short getWl30EleReinvstPoliMax() {
        return this.wl30EleReinvstPoliMax;
    }

    public String getWl23AreaVincPegFormatted() {
        return MarshalByteExt.bufferToStr(getWl23AreaVincPegBytes());
    }

    /**Original name: WL23-AREA-VINC-PEG<br>
	 * <pre>--  VINCOLO PEGNO</pre>*/
    public byte[] getWl23AreaVincPegBytes() {
        byte[] buffer = new byte[Len.WL23_AREA_VINC_PEG];
        return getWl23AreaVincPegBytes(buffer, 1);
    }

    public void setWl23AreaVincPegBytes(byte[] buffer, int offset) {
        int position = offset;
        wl23EleVincPegMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WL23_TAB_VINC_PEG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wl23TabVincPeg[idx - 1].setWl23TabVincPegBytes(buffer, position);
                position += Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;
            }
            else {
                wl23TabVincPeg[idx - 1].initWl23TabVincPegSpaces();
                position += Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;
            }
        }
    }

    public byte[] getWl23AreaVincPegBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wl23EleVincPegMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WL23_TAB_VINC_PEG_MAXOCCURS; idx++) {
            wl23TabVincPeg[idx - 1].getWl23TabVincPegBytes(buffer, position);
            position += Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;
        }
        return buffer;
    }

    public void setWl23EleVincPegMax(short wl23EleVincPegMax) {
        this.wl23EleVincPegMax = wl23EleVincPegMax;
    }

    public short getWl23EleVincPegMax() {
        return this.wl23EleVincPegMax;
    }

    public void setWopzAreaOpzioniBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0217.eleMaxOpzioni = MarshalByte.readFixedString(buffer, position, Ivvc0217.Len.ELE_MAX_OPZIONI);
        position += Ivvc0217.Len.ELE_MAX_OPZIONI;
        for (int idx = 1; idx <= Ivvc0217.OPZIONI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                ivvc0217.getOpzioni(idx).setOpzioniBytes(buffer, position);
                position += WopzOpzioni.Len.OPZIONI;
            }
            else {
                ivvc0217.getOpzioni(idx).initOpzioniSpaces();
                position += WopzOpzioni.Len.OPZIONI;
            }
        }
    }

    public byte[] getWopzAreaOpzioniBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ivvc0217.eleMaxOpzioni, Ivvc0217.Len.ELE_MAX_OPZIONI);
        position += Ivvc0217.Len.ELE_MAX_OPZIONI;
        for (int idx = 1; idx <= Ivvc0217.OPZIONI_MAXOCCURS; idx++) {
            ivvc0217.getOpzioni(idx).getOpzioniBytes(buffer, position);
            position += WopzOpzioni.Len.OPZIONI;
        }
        return buffer;
    }

    public void setWgopAreaGaranziaOpzBytes(byte[] buffer, int offset) {
        int position = offset;
        wgopEleGaranziaOpzMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGOP_TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wgopTabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                wgopTabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public byte[] getWgopAreaGaranziaOpzBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wgopEleGaranziaOpzMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WGOP_TAB_GAR_MAXOCCURS; idx++) {
            wgopTabGar[idx - 1].getTabGarBytes(buffer, position);
            position += WgrzTabGar.Len.TAB_GAR;
        }
        return buffer;
    }

    public void setWgopEleGaranziaOpzMax(short wgopEleGaranziaOpzMax) {
        this.wgopEleGaranziaOpzMax = wgopEleGaranziaOpzMax;
    }

    public short getWgopEleGaranziaOpzMax() {
        return this.wgopEleGaranziaOpzMax;
    }

    public void setWtopAreaTrancheOpzBytes(byte[] buffer, int offset) {
        int position = offset;
        wtopEleTranOpzMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTOP_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wtopTabTran[idx - 1].setTabTranBytes(buffer, position);
                position += WtgaTabTran.Len.TAB_TRAN;
            }
            else {
                wtopTabTran[idx - 1].initTabTranSpaces();
                position += WtgaTabTran.Len.TAB_TRAN;
            }
        }
    }

    public byte[] getWtopAreaTrancheOpzBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtopEleTranOpzMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTOP_TAB_TRAN_MAXOCCURS; idx++) {
            wtopTabTran[idx - 1].getTabTranBytes(buffer, position);
            position += WtgaTabTran.Len.TAB_TRAN;
        }
        return buffer;
    }

    public void setWtopEleTranOpzMax(short wtopEleTranOpzMax) {
        this.wtopEleTranOpzMax = wtopEleTranOpzMax;
    }

    public short getWtopEleTranOpzMax() {
        return this.wtopEleTranOpzMax;
    }

    public void setWcntAreaDatiContestBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0212.setWcntAreaVariabiliContBytes(buffer, position);
    }

    public byte[] getWcntAreaDatiContestBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0212.getWcntAreaVariabiliContBytes(buffer, position);
        return buffer;
    }

    public void setWcltAreaClauTxtBytes(byte[] buffer, int offset) {
        int position = offset;
        wcltEleClauTxtMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WCLT_TAB_CLAU_TXT_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wcltTabClauTxt[idx - 1].setWcltTabClauTxtBytes(buffer, position);
                position += WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;
            }
            else {
                wcltTabClauTxt[idx - 1].initWcltTabClauTxtSpaces();
                position += WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;
            }
        }
    }

    public byte[] getWcltAreaClauTxtBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wcltEleClauTxtMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WCLT_TAB_CLAU_TXT_MAXOCCURS; idx++) {
            wcltTabClauTxt[idx - 1].getWcltTabClauTxtBytes(buffer, position);
            position += WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;
        }
        return buffer;
    }

    public void setWcltEleClauTxtMax(short wcltEleClauTxtMax) {
        this.wcltEleClauTxtMax = wcltEleClauTxtMax;
    }

    public short getWcltEleClauTxtMax() {
        return this.wcltEleClauTxtMax;
    }

    public String getWp58AreaImpostaBolloFormatted() {
        return MarshalByteExt.bufferToStr(getWp58AreaImpostaBolloBytes());
    }

    /**Original name: WP58-AREA-IMPOSTA-BOLLO<br>
	 * <pre>--  IMPOSTA DI BOLLO</pre>*/
    public byte[] getWp58AreaImpostaBolloBytes() {
        byte[] buffer = new byte[Len.WP58_AREA_IMPOSTA_BOLLO];
        return getWp58AreaImpostaBolloBytes(buffer, 1);
    }

    public void setWp58AreaImpostaBolloBytes(byte[] buffer, int offset) {
        int position = offset;
        wp58EleMaxImpstBol = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP58_TAB_IMPST_BOL_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wp58TabImpstBol[idx - 1].setWp58TabImpstBolBytes(buffer, position);
                position += Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
            }
            else {
                wp58TabImpstBol[idx - 1].initWp58TabImpstBolSpaces();
                position += Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
            }
        }
    }

    public byte[] getWp58AreaImpostaBolloBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp58EleMaxImpstBol);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP58_TAB_IMPST_BOL_MAXOCCURS; idx++) {
            wp58TabImpstBol[idx - 1].getWp58TabImpstBolBytes(buffer, position);
            position += Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
        }
        return buffer;
    }

    public void setWp58EleMaxImpstBol(short wp58EleMaxImpstBol) {
        this.wp58EleMaxImpstBol = wp58EleMaxImpstBol;
    }

    public short getWp58EleMaxImpstBol() {
        return this.wp58EleMaxImpstBol;
    }

    public String getWp61AreaDCristFormatted() {
        return MarshalByteExt.bufferToStr(getWp61AreaDCristBytes());
    }

    /**Original name: WP61-AREA-D-CRIST<br>
	 * <pre>--  DATI CRISTALLIZZATI</pre>*/
    public byte[] getWp61AreaDCristBytes() {
        byte[] buffer = new byte[Len.WP61_AREA_D_CRIST];
        return getWp61AreaDCristBytes(buffer, 1);
    }

    public void setWp61AreaDCristBytes(byte[] buffer, int offset) {
        int position = offset;
        wp61EleMaxDCrist = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWp61TabDCristBytes(buffer, position);
    }

    public byte[] getWp61AreaDCristBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp61EleMaxDCrist);
        position += Types.SHORT_SIZE;
        getWp61TabDCristBytes(buffer, position);
        return buffer;
    }

    public void setWp61EleMaxDCrist(short wp61EleMaxDCrist) {
        this.wp61EleMaxDCrist = wp61EleMaxDCrist;
    }

    public short getWp61EleMaxDCrist() {
        return this.wp61EleMaxDCrist;
    }

    public void setWp61TabDCristBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp611.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp611.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp611.Len.Int.ID_PTF, 0));
        position += Lccvp611.Len.ID_PTF;
        lccvp611.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp61TabDCristBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp611.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp611.getIdPtf(), Lccvp611.Len.Int.ID_PTF, 0);
        position += Lccvp611.Len.ID_PTF;
        lccvp611.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public String getWp67AreaEstPoliCpiPrFormatted() {
        return MarshalByteExt.bufferToStr(getWp67AreaEstPoliCpiPrBytes());
    }

    /**Original name: WP67-AREA-EST-POLI-CPI-PR<br>
	 * <pre>--  DATI AGGIUNTIVI PER POLIZZE CPI</pre>*/
    public byte[] getWp67AreaEstPoliCpiPrBytes() {
        byte[] buffer = new byte[Len.WP67_AREA_EST_POLI_CPI_PR];
        return getWp67AreaEstPoliCpiPrBytes(buffer, 1);
    }

    public void setWp67AreaEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        wp67EleMaxEstPoliCpiPr = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWp67TabEstPoliCpiPrBytes(buffer, position);
    }

    public byte[] getWp67AreaEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp67EleMaxEstPoliCpiPr);
        position += Types.SHORT_SIZE;
        getWp67TabEstPoliCpiPrBytes(buffer, position);
        return buffer;
    }

    public void setWp67EleMaxEstPoliCpiPr(short wp67EleMaxEstPoliCpiPr) {
        this.wp67EleMaxEstPoliCpiPr = wp67EleMaxEstPoliCpiPr;
    }

    public short getWp67EleMaxEstPoliCpiPr() {
        return this.wp67EleMaxEstPoliCpiPr;
    }

    public void setWp67TabEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp671.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp671.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp671.Len.Int.ID_PTF, 0));
        position += Lccvp671.Len.ID_PTF;
        lccvp671.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp67TabEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp671.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp671.getIdPtf(), Lccvp671.Len.Int.ID_PTF, 0);
        position += Lccvp671.Len.ID_PTF;
        lccvp671.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public String getWp01AreaRichEstFormatted() {
        return MarshalByteExt.bufferToStr(getWp01AreaRichEstBytes());
    }

    /**Original name: WP01-AREA-RICH-EST<br>
	 * <pre>--  RICHIESTA ESTERNA</pre>*/
    public byte[] getWp01AreaRichEstBytes() {
        byte[] buffer = new byte[Len.WP01_AREA_RICH_EST];
        return getWp01AreaRichEstBytes(buffer, 1);
    }

    public void setWp01AreaRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        wp01EleMaxRichEst = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWp01TabRichEstBytes(buffer, position);
    }

    public byte[] getWp01AreaRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp01EleMaxRichEst);
        position += Types.SHORT_SIZE;
        getWp01TabRichEstBytes(buffer, position);
        return buffer;
    }

    public void setWp01EleMaxRichEst(short wp01EleMaxRichEst) {
        this.wp01EleMaxRichEst = wp01EleMaxRichEst;
    }

    public short getWp01EleMaxRichEst() {
        return this.wp01EleMaxRichEst;
    }

    public String getWp01TabRichEstFormatted() {
        return MarshalByteExt.bufferToStr(getWp01TabRichEstBytes());
    }

    /**Original name: WP01-TAB-RICH-EST<br>*/
    public byte[] getWp01TabRichEstBytes() {
        byte[] buffer = new byte[Len.WP01_TAB_RICH_EST];
        return getWp01TabRichEstBytes(buffer, 1);
    }

    public void setWp01TabRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp011.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp011.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp011.Len.Int.ID_PTF, 0));
        position += Lccvp011.Len.ID_PTF;
        lccvp011.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp01TabRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp011.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp011.getIdPtf(), Lccvp011.Len.Int.ID_PTF, 0);
        position += Lccvp011.Len.ID_PTF;
        lccvp011.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public String getWp86AreaMotLiqFormatted() {
        return MarshalByteExt.bufferToStr(getWp86AreaMotLiqBytes());
    }

    /**Original name: WP86-AREA-MOT-LIQ<br>
	 * <pre>-- MOTIVO LIQUIDAZIONE</pre>*/
    public byte[] getWp86AreaMotLiqBytes() {
        byte[] buffer = new byte[Len.WP86_AREA_MOT_LIQ];
        return getWp86AreaMotLiqBytes(buffer, 1);
    }

    public void setWp86AreaMotLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wp86EleMotLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP86_TAB_MOT_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wp86TabMotLiq[idx - 1].setWp86TabMotLiqBytes(buffer, position);
                position += Wp86TabMotLiq.Len.WP86_TAB_MOT_LIQ;
            }
            else {
                wp86TabMotLiq[idx - 1].initWp86TabMotLiqSpaces();
                position += Wp86TabMotLiq.Len.WP86_TAB_MOT_LIQ;
            }
        }
    }

    public byte[] getWp86AreaMotLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp86EleMotLiqMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP86_TAB_MOT_LIQ_MAXOCCURS; idx++) {
            wp86TabMotLiq[idx - 1].getWp86TabMotLiqBytes(buffer, position);
            position += Wp86TabMotLiq.Len.WP86_TAB_MOT_LIQ;
        }
        return buffer;
    }

    public void setWp86EleMotLiqMax(short wp86EleMotLiqMax) {
        this.wp86EleMotLiqMax = wp86EleMotLiqMax;
    }

    public short getWp86EleMotLiqMax() {
        return this.wp86EleMotLiqMax;
    }

    public String getWkMoviOrigFormatted() {
        return this.wkMoviOrig;
    }

    public String getWp88AreaServValFormatted() {
        return MarshalByteExt.bufferToStr(getWp88AreaServValBytes());
    }

    /**Original name: WP88-AREA-SERV-VAL<br>
	 * <pre>--  ATTIVAZIONE SERVIZI VALORE</pre>*/
    public byte[] getWp88AreaServValBytes() {
        byte[] buffer = new byte[Len.WP88_AREA_SERV_VAL];
        return getWp88AreaServValBytes(buffer, 1);
    }

    public void setWp88AreaServValBytes(byte[] buffer, int offset) {
        int position = offset;
        wp88EleSerValMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP88_TAB_SERV_VAL_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wp88TabServVal[idx - 1].setWp88TabServValBytes(buffer, position);
                position += Wp88TabServVal.Len.WP88_TAB_SERV_VAL;
            }
            else {
                wp88TabServVal[idx - 1].initWp88TabServValSpaces();
                position += Wp88TabServVal.Len.WP88_TAB_SERV_VAL;
            }
        }
    }

    public byte[] getWp88AreaServValBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp88EleSerValMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP88_TAB_SERV_VAL_MAXOCCURS; idx++) {
            wp88TabServVal[idx - 1].getWp88TabServValBytes(buffer, position);
            position += Wp88TabServVal.Len.WP88_TAB_SERV_VAL;
        }
        return buffer;
    }

    public void setWp88EleSerValMax(short wp88EleSerValMax) {
        this.wp88EleSerValMax = wp88EleSerValMax;
    }

    public short getWp88EleSerValMax() {
        return this.wp88EleSerValMax;
    }

    public String getWp89AreaDservValFormatted() {
        return MarshalByteExt.bufferToStr(getWp89AreaDservValBytes());
    }

    /**Original name: WP89-AREA-DSERV-VAL<br>
	 * <pre>--  DETTAGLIO ATTIVAZIONE SERVIZI VALORE</pre>*/
    public byte[] getWp89AreaDservValBytes() {
        byte[] buffer = new byte[Len.WP89_AREA_DSERV_VAL];
        return getWp89AreaDservValBytes(buffer, 1);
    }

    public void setWp89AreaDservValBytes(byte[] buffer, int offset) {
        int position = offset;
        wp89EleDservValMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP89_TAB_DSERV_VAL_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wp89TabDservVal[idx - 1].setWp89TabDservValBytes(buffer, position);
                position += Wp89TabDservVal.Len.WP89_TAB_DSERV_VAL;
            }
            else {
                wp89TabDservVal[idx - 1].initWp89TabDservValSpaces();
                position += Wp89TabDservVal.Len.WP89_TAB_DSERV_VAL;
            }
        }
    }

    public byte[] getWp89AreaDservValBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp89EleDservValMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP89_TAB_DSERV_VAL_MAXOCCURS; idx++) {
            wp89TabDservVal[idx - 1].getWp89TabDservValBytes(buffer, position);
            position += Wp89TabDservVal.Len.WP89_TAB_DSERV_VAL;
        }
        return buffer;
    }

    public void setWp89EleDservValMax(short wp89EleDservValMax) {
        this.wp89EleDservValMax = wp89EleDservValMax;
    }

    public short getWp89EleDservValMax() {
        return this.wp89EleDservValMax;
    }

    public String getWp56AreaQuestAdegVerFormatted() {
        return MarshalByteExt.bufferToStr(getWp56AreaQuestAdegVerBytes());
    }

    /**Original name: WP56-AREA-QUEST-ADEG-VER<br>
	 * <pre>--  QUESTIONARIO ADEGUATA VERIFICA</pre>*/
    public byte[] getWp56AreaQuestAdegVerBytes() {
        byte[] buffer = new byte[Len.WP56_AREA_QUEST_ADEG_VER];
        return getWp56AreaQuestAdegVerBytes(buffer, 1);
    }

    public void setWp56AreaQuestAdegVerBytes(byte[] buffer, int offset) {
        int position = offset;
        wp56QuestAdegVerMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP56_TAB_QUEST_ADEG_VER_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wp56TabQuestAdegVer[idx - 1].setWp56TabQuestAdegVerBytes(buffer, position);
                position += Wp56TabQuestAdegVer.Len.WP56_TAB_QUEST_ADEG_VER;
            }
            else {
                wp56TabQuestAdegVer[idx - 1].initWp56TabQuestAdegVerSpaces();
                position += Wp56TabQuestAdegVer.Len.WP56_TAB_QUEST_ADEG_VER;
            }
        }
    }

    public byte[] getWp56AreaQuestAdegVerBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp56QuestAdegVerMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WP56_TAB_QUEST_ADEG_VER_MAXOCCURS; idx++) {
            wp56TabQuestAdegVer[idx - 1].getWp56TabQuestAdegVerBytes(buffer, position);
            position += Wp56TabQuestAdegVer.Len.WP56_TAB_QUEST_ADEG_VER;
        }
        return buffer;
    }

    public void setWp56QuestAdegVerMax(short wp56QuestAdegVerMax) {
        this.wp56QuestAdegVerMax = wp56QuestAdegVerMax;
    }

    public short getWp56QuestAdegVerMax() {
        return this.wp56QuestAdegVerMax;
    }

    public void setWcdgAreaCommisGestBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0224.setAreaCommisGestCalcBytes(buffer, position);
        position += Ivvc0224.Len.AREA_COMMIS_GEST_CALC;
        ivvc0224.setAreaCommisGestVvBytes(buffer, position);
        position += Ivvc0224.Len.AREA_COMMIS_GEST_VV;
        ivvc0224.setAreaCommisGestOutBytes(buffer, position);
    }

    public byte[] getWcdgAreaCommisGestBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0224.getAreaCommisGestCalcBytes(buffer, position);
        position += Ivvc0224.Len.AREA_COMMIS_GEST_CALC;
        ivvc0224.getAreaCommisGestVvBytes(buffer, position);
        position += Ivvc0224.Len.AREA_COMMIS_GEST_VV;
        ivvc0224.getAreaCommisGestOutBytes(buffer, position);
        return buffer;
    }

    public AreaIvvc0223 getAreaIvvc0223() {
        return areaIvvc0223;
    }

    public Ivvc0212 getIvvc0212() {
        return ivvc0212;
    }

    public Ivvc0222AreaFndXTranche getIvvc0222AreaFndXTranche() {
        return ivvc0222AreaFndXTranche;
    }

    public Ivvc0224 getIvvc0224() {
        return ivvc0224;
    }

    public Lccvdad1 getLccvdad1() {
        return lccvdad1;
    }

    public Lccvdco1 getLccvdco1() {
        return lccvdco1;
    }

    public Lccvdfa1 getLccvdfa1() {
        return lccvdfa1;
    }

    public Lccvdfl1 getLccvdfl1() {
        return lccvdfl1;
    }

    public Lccvmov1 getLccvmov1() {
        return lccvmov1;
    }

    public Lccvp011 getLccvp011() {
        return lccvp011;
    }

    public Lccvp611 getLccvp611() {
        return lccvp611;
    }

    public Lccvp671 getLccvp671() {
        return lccvp671;
    }

    public Lccvpol1 getLccvpol1() {
        return lccvpol1;
    }

    public Lccvric1 getLccvric1() {
        return lccvric1;
    }

    public W1tgaTabTran getW1tgaTabTran(int idx) {
        return w1tgaTabTran.get(idx - 1);
    }

    public LazyArrayCopy<W1tgaTabTran> getW1tgaTabTranObj() {
        return w1tgaTabTran;
    }

    public WadeTabAdes getWadeTabAdes(int idx) {
        return wadeTabAdes[idx - 1];
    }

    public WbelTabBeneLiq getWbelTabBeneLiq(int idx) {
        return wbelTabBeneLiq[idx - 1];
    }

    public WbepTabBeneficiari getWbepTabBeneficiari(int idx) {
        return wbepTabBeneficiari[idx - 1];
    }

    public WdeqTabDettQuest getWdeqTabDettQuest(int idx) {
        return wdeqTabDettQuest[idx - 1];
    }

    public WdtcTabDtc getWdtcTabDtc(int idx) {
        return wdtcTabDtc[idx - 1];
    }

    public We15TabE15 getWe15TabE15(int idx) {
        return we15TabE15[idx - 1];
    }

    public WgrzTabGar getWgopTabGar(int idx) {
        return wgopTabGar[idx - 1];
    }

    public WgrlTabGarLiq getWgrlTabGarLiq(int idx) {
        return wgrlTabGarLiq[idx - 1];
    }

    public WgrzTabGar getWgrzTabGar(int idx) {
        return wgrzTabGar[idx - 1];
    }

    public WisoTabImpSost getWisoTabImpSost(int idx) {
        return wisoTabImpSost[idx - 1];
    }

    public Wl23TabVincPeg getWl23TabVincPeg(int idx) {
        return wl23TabVincPeg[idx - 1];
    }

    public Wl30TabReinvstPoli getWl30TabReinvstPoli(int idx) {
        return wl30TabReinvstPoli[idx - 1];
    }

    public S089TabLiq getWlquTabLiq(int idx) {
        return wlquTabLiq[idx - 1];
    }

    public WmfzTabMoviFinrio getWmfzTabMoviFinrio(int idx) {
        return wmfzTabMoviFinrio[idx - 1];
    }

    public WocoTabOggColl getWocoTabOggColl(int idx) {
        return wocoTabOggColl[idx - 1];
    }

    public Wp58TabImpstBol getWp58TabImpstBol(int idx) {
        return wp58TabImpstBol[idx - 1];
    }

    public WpliTabPercLiq getWpliTabPercLiq(int idx) {
        return wpliTabPercLiq[idx - 1];
    }

    public WpmoTabParamMov getWpmoTabParamMov(int idx) {
        return wpmoTabParamMov[idx - 1];
    }

    public WpogTabParamOgg getWpogTabParamOgg(int idx) {
        return wpogTabParamOgg[idx - 1];
    }

    public WpreTabPrestiti getWpreTabPrestiti(int idx) {
        return wpreTabPrestiti[idx - 1];
    }

    public WpvtTabProvTr getWpvtTabProvTr(int idx) {
        return wpvtTabProvTr[idx - 1];
    }

    public WqueTabQuest getWqueTabQuest(int idx) {
        return wqueTabQuest[idx - 1];
    }

    public WranTabRappAnag getWranTabRappAnag(int idx) {
        return wranTabRappAnag[idx - 1];
    }

    public LazyArrayCopy<WrdfTabRicDisinv> getWrdfTabRicDisinvObj() {
        return wrdfTabRicDisinv;
    }

    public WrifTabRicInv getWrifTabRicInv(int idx) {
        return wrifTabRicInv.get(idx - 1);
    }

    public LazyArrayCopy<WrifTabRicInv> getWrifTabRicInvObj() {
        return wrifTabRicInv;
    }

    public WrreTabRappRete getWrreTabRappRete(int idx) {
        return wrreTabRappRete[idx - 1];
    }

    public WrstTabRst getWrstTabRst(int idx) {
        return wrstTabRst[idx - 1];
    }

    public WsdiTabStraInv getWsdiTabStraInv(int idx) {
        return wsdiTabStraInv[idx - 1];
    }

    public WspgTabSpg getWspgTabSpg(int idx) {
        return wspgTabSpg[idx - 1];
    }

    public WtitTabTitCont getWtitTabTitCont(int idx) {
        return wtitTabTitCont[idx - 1];
    }

    public WtliTabTrchLiq getWtliTabTrchLiq(int idx) {
        return wtliTabTrchLiq.get(idx - 1);
    }

    public LazyArrayCopy<WtliTabTrchLiq> getWtliTabTrchLiqObj() {
        return wtliTabTrchLiq;
    }

    public WtgaTabTran getWtopTabTran(int idx) {
        return wtopTabTran[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getAreaBusinessBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_ELE_ADES_MAX = 2;
        public static final int WADE_AREA_ADESIONE = WADE_ELE_ADES_MAX + AreaBusiness.WADE_TAB_ADES_MAXOCCURS * WadeTabAdes.Len.WADE_TAB_ADES;
        public static final int WBEP_ELE_BENEF_MAX = 2;
        public static final int WBEP_AREA_BENEF = WBEP_ELE_BENEF_MAX + AreaBusiness.WBEP_TAB_BENEFICIARI_MAXOCCURS * WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
        public static final int WBEL_ELE_BENEF_LIQ_MAX = 2;
        public static final int WBEL_AREA_BENEF_LIQ = WBEL_ELE_BENEF_LIQ_MAX + AreaBusiness.WBEL_TAB_BENE_LIQ_MAXOCCURS * WbelTabBeneLiq.Len.WBEL_TAB_BENE_LIQ;
        public static final int WDCO_ELE_COLL_MAX = 2;
        public static final int WDCO_TAB_COLL = WpolStatus.Len.STATUS + Lccvdco1.Len.ID_PTF + WdcoDati.Len.DATI;
        public static final int WDCO_AREA_DT_COLLETTIVA = WDCO_ELE_COLL_MAX + WDCO_TAB_COLL;
        public static final int WDFA_ELE_FISC_ADES_MAX = 2;
        public static final int WDFA_TAB_FISC_ADES = WpolStatus.Len.STATUS + Lccvdfa1.Len.ID_PTF + WdfaDati.Len.DATI;
        public static final int WDFA_AREA_DT_FISC_ADES = WDFA_ELE_FISC_ADES_MAX + WDFA_TAB_FISC_ADES;
        public static final int WDEQ_ELE_DETT_QUEST_MAX = 2;
        public static final int WDEQ_AREA_DETT_QUEST = WDEQ_ELE_DETT_QUEST_MAX + AreaBusiness.WDEQ_TAB_DETT_QUEST_MAXOCCURS * WdeqTabDettQuest.Len.WDEQ_TAB_DETT_QUEST;
        public static final int WDTC_ELE_DETT_TIT_MAX = 2;
        public static final int WDTC_AREA_DETT_TIT_CONT = WDTC_ELE_DETT_TIT_MAX + AreaBusiness.WDTC_TAB_DTC_MAXOCCURS * WdtcTabDtc.Len.WDTC_TAB_DTC;
        public static final int WGRZ_ELE_GARANZIA_MAX = 2;
        public static final int WGRZ_AREA_GARANZIA = WGRZ_ELE_GARANZIA_MAX + AreaBusiness.WGRZ_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int WGRL_ELE_GAR_LIQ_MAX = 2;
        public static final int WGRL_AREA_GARANZIA_LIQ = WGRL_ELE_GAR_LIQ_MAX + AreaBusiness.WGRL_TAB_GAR_LIQ_MAXOCCURS * WgrlTabGarLiq.Len.WGRL_TAB_GAR_LIQ;
        public static final int WISO_ELE_IMP_SOST_MAX = 2;
        public static final int WISO_AREA_IMPOSTA_SOST = WISO_ELE_IMP_SOST_MAX + AreaBusiness.WISO_TAB_IMP_SOST_MAXOCCURS * WisoTabImpSost.Len.WISO_TAB_IMP_SOST;
        public static final int WLQU_ELE_LIQ_MAX = 2;
        public static final int WLQU_AREA_LIQUIDAZIONE = WLQU_ELE_LIQ_MAX + AreaBusiness.WLQU_TAB_LIQ_MAXOCCURS * S089TabLiq.Len.WLQU_TAB_LIQ;
        public static final int WMOV_ELE_MOVI_MAX = 2;
        public static final int WMOV_TAB_MOVI = WpolStatus.Len.STATUS + Lccvmov1.Len.ID_PTF + WmovDati.Len.DATI;
        public static final int WMOV_AREA_MOVIMENTO = WMOV_ELE_MOVI_MAX + WMOV_TAB_MOVI;
        public static final int WMFZ_ELE_MOVI_FINRIO_MAX = 2;
        public static final int WMFZ_AREA_MOVI_FINRIO = WMFZ_ELE_MOVI_FINRIO_MAX + AreaBusiness.WMFZ_TAB_MOVI_FINRIO_MAXOCCURS * WmfzTabMoviFinrio.Len.WMFZ_TAB_MOVI_FINRIO;
        public static final int WPOG_ELE_PARAM_OGG_MAX = 2;
        public static final int WPOG_AREA_PARAM_OGG = WPOG_ELE_PARAM_OGG_MAX + AreaBusiness.WPOG_TAB_PARAM_OGG_MAXOCCURS * WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
        public static final int WPMO_ELE_PARAM_MOV_MAX = 2;
        public static final int WPMO_AREA_PARAM_MOV = WPMO_ELE_PARAM_MOV_MAX + AreaBusiness.WPMO_TAB_PARAM_MOV_MAXOCCURS * WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
        public static final int WPLI_ELE_PERC_LIQ_MAX = 2;
        public static final int WPLI_AREA_PERC_LIQ = WPLI_ELE_PERC_LIQ_MAX + AreaBusiness.WPLI_TAB_PERC_LIQ_MAXOCCURS * WpliTabPercLiq.Len.WPLI_TAB_PERC_LIQ;
        public static final int WPOL_ELE_POLI_MAX = 2;
        public static final int WPOL_TAB_POLI = WpolStatus.Len.STATUS + Lccvpol1.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int WPOL_AREA_POLIZZA = WPOL_ELE_POLI_MAX + WPOL_TAB_POLI;
        public static final int WPRE_ELE_PRESTITI_MAX = 2;
        public static final int WPRE_AREA_PRESTITI = WPRE_ELE_PRESTITI_MAX + AreaBusiness.WPRE_TAB_PRESTITI_MAXOCCURS * WpreTabPrestiti.Len.WPRE_TAB_PRESTITI;
        public static final int WPVT_ELE_PROV_MAX = 2;
        public static final int WPVT_AREA_PROV = WPVT_ELE_PROV_MAX + AreaBusiness.WPVT_TAB_PROV_TR_MAXOCCURS * WpvtTabProvTr.Len.WPVT_TAB_PROV_TR;
        public static final int WQUE_ELE_QUEST_MAX = 2;
        public static final int WQUE_AREA_QUEST = WQUE_ELE_QUEST_MAX + AreaBusiness.WQUE_TAB_QUEST_MAXOCCURS * WqueTabQuest.Len.WQUE_TAB_QUEST;
        public static final int WRIC_ELE_RICH_MAX = 2;
        public static final int WRIC_TAB_RICH = WpolStatus.Len.STATUS + Lccvric1.Len.ID_PTF + WricDati.Len.DATI;
        public static final int WRIC_AREA_RICH = WRIC_ELE_RICH_MAX + WRIC_TAB_RICH;
        public static final int WRDF_ELE_RIC_INV_MAX = 2;
        public static final int WRDF_AREA_RICH_DISINV_FND = WRDF_ELE_RIC_INV_MAX + AreaBusiness.WRDF_TAB_RIC_DISINV_MAXOCCURS * WrdfTabRicDisinv.Len.WRDF_TAB_RIC_DISINV;
        public static final int WRIF_ELE_RIC_INV_MAX = 2;
        public static final int WRIF_AREA_RICH_INV_FND = WRIF_ELE_RIC_INV_MAX + AreaBusiness.WRIF_TAB_RIC_INV_MAXOCCURS * WrifTabRicInv.Len.WRIF_TAB_RIC_INV;
        public static final int WRAN_ELE_RAPP_ANAG_MAX = 2;
        public static final int WRAN_AREA_RAPP_ANAG = WRAN_ELE_RAPP_ANAG_MAX + AreaBusiness.WRAN_TAB_RAPP_ANAG_MAXOCCURS * WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
        public static final int WE15_ELE_EST_RAPP_ANAG_MAX = 2;
        public static final int WE15_AREA_EST_RAPP_ANAG = WE15_ELE_EST_RAPP_ANAG_MAX + AreaBusiness.WE15_TAB_E15_MAXOCCURS * We15TabE15.Len.WE15_TAB_E15;
        public static final int WRRE_ELE_RAPP_RETE_MAX = 2;
        public static final int WRRE_AREA_RAPP_RETE = WRRE_ELE_RAPP_RETE_MAX + AreaBusiness.WRRE_TAB_RAPP_RETE_MAXOCCURS * WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
        public static final int WSPG_ELE_SOPRAP_GAR_MAX = 2;
        public static final int WSPG_AREA_SOPRAP_GAR = WSPG_ELE_SOPRAP_GAR_MAX + AreaBusiness.WSPG_TAB_SPG_MAXOCCURS * WspgTabSpg.Len.WSPG_TAB_SPG;
        public static final int WSDI_ELE_STRA_INV_MAX = 2;
        public static final int WSDI_AREA_STRA_INV = WSDI_ELE_STRA_INV_MAX + AreaBusiness.WSDI_TAB_STRA_INV_MAXOCCURS * WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
        public static final int WTIT_ELE_TIT_CONT_MAX = 2;
        public static final int WTIT_AREA_TIT_CONT = WTIT_ELE_TIT_CONT_MAX + AreaBusiness.WTIT_TAB_TIT_CONT_MAXOCCURS * WtitTabTitCont.Len.WTIT_TAB_TIT_CONT;
        public static final int WTCL_ELE_TIT_LIQ_MAX = 2;
        public static final int WTCL_TAB_TIT_LIQ = WpolStatus.Len.STATUS + Lccvtcl1.Len.ID_PTF + WtclDati.Len.DATI;
        public static final int WTCL_AREA_TIT_LIQ = WTCL_ELE_TIT_LIQ_MAX + WTCL_TAB_TIT_LIQ;
        public static final int W1TGA_ELE_TRAN_MAX = 2;
        public static final int W1TGA_AREA_TRANCHE = W1TGA_ELE_TRAN_MAX + AreaBusiness.W1TGA_TAB_TRAN_MAXOCCURS * W1tgaTabTran.Len.W1TGA_TAB_TRAN;
        public static final int WTLI_ELE_TRCH_LIQ_MAX = 2;
        public static final int WTLI_AREA_TRCH_LIQ = WTLI_ELE_TRCH_LIQ_MAX + AreaBusiness.WTLI_TAB_TRCH_LIQ_MAXOCCURS * WtliTabTrchLiq.Len.WTLI_TAB_TRCH_LIQ;
        public static final int WDAD_ELE_DFLT_ADES_MAX = 2;
        public static final int WDAD_TAB_DFLT_ADES = WpolStatus.Len.STATUS + Lccvdad1.Len.ID_PTF + WdadDati.Len.DATI;
        public static final int WDAD_AREA_DEFAULT_ADES = WDAD_ELE_DFLT_ADES_MAX + WDAD_TAB_DFLT_ADES;
        public static final int WOCO_ELE_OGG_COLL_MAX = 2;
        public static final int WOCO_AREA_OGG_COLL = WOCO_ELE_OGG_COLL_MAX + AreaBusiness.WOCO_TAB_OGG_COLL_MAXOCCURS * WocoTabOggColl.Len.WOCO_TAB_OGG_COLL;
        public static final int WDFL_ELE_DFL_MAX = 2;
        public static final int WDFL_TAB_DFL = WpolStatus.Len.STATUS + Lccvdfl1.Len.ID_PTF + WdflDati.Len.DATI;
        public static final int WDFL_AREA_DFL = WDFL_ELE_DFL_MAX + WDFL_TAB_DFL;
        public static final int WRST_ELE_RST_MAX = 2;
        public static final int WRST_AREA_RST = WRST_ELE_RST_MAX + AreaBusiness.WRST_TAB_RST_MAXOCCURS * WrstTabRst.Len.WRST_TAB_RST;
        public static final int WL19_ELE_FND_MAX = 2;
        public static final int WL19_TABELLA = AreaBusiness.WL19_TAB_FND_MAXOCCURS * Wl19TabFnd.Len.WL19_TAB_FND;
        public static final int WL19_AREA_QUOTE = WL19_ELE_FND_MAX + WL19_TABELLA;
        public static final int WL30_ELE_REINVST_POLI_MAX = 2;
        public static final int WL30_AREA_REINVST_POLI = WL30_ELE_REINVST_POLI_MAX + AreaBusiness.WL30_TAB_REINVST_POLI_MAXOCCURS * Wl30TabReinvstPoli.Len.WL30_TAB_REINVST_POLI;
        public static final int WL23_ELE_VINC_PEG_MAX = 2;
        public static final int WL23_AREA_VINC_PEG = WL23_ELE_VINC_PEG_MAX + AreaBusiness.WL23_TAB_VINC_PEG_MAXOCCURS * Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;
        public static final int WOPZ_AREA_OPZIONI = Ivvc0217.Len.ELE_MAX_OPZIONI + Ivvc0217.OPZIONI_MAXOCCURS * WopzOpzioni.Len.OPZIONI;
        public static final int WGOP_ELE_GARANZIA_OPZ_MAX = 2;
        public static final int WGOP_AREA_GARANZIA_OPZ = WGOP_ELE_GARANZIA_OPZ_MAX + AreaBusiness.WGOP_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int WTOP_ELE_TRAN_OPZ_MAX = 2;
        public static final int WTOP_AREA_TRANCHE_OPZ = WTOP_ELE_TRAN_OPZ_MAX + AreaBusiness.WTOP_TAB_TRAN_MAXOCCURS * WtgaTabTran.Len.TAB_TRAN;
        public static final int WCNT_AREA_DATI_CONTEST = Ivvc0212.Len.WCNT_AREA_VARIABILI_CONT;
        public static final int WCLT_ELE_CLAU_TXT_MAX = 2;
        public static final int WCLT_AREA_CLAU_TXT = WCLT_ELE_CLAU_TXT_MAX + AreaBusiness.WCLT_TAB_CLAU_TXT_MAXOCCURS * WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;
        public static final int WP58_ELE_MAX_IMPST_BOL = 2;
        public static final int WP58_AREA_IMPOSTA_BOLLO = WP58_ELE_MAX_IMPST_BOL + AreaBusiness.WP58_TAB_IMPST_BOL_MAXOCCURS * Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
        public static final int WP61_ELE_MAX_D_CRIST = 2;
        public static final int WP61_TAB_D_CRIST = WpolStatus.Len.STATUS + Lccvp611.Len.ID_PTF + Wp61Dati.Len.DATI;
        public static final int WP61_AREA_D_CRIST = WP61_ELE_MAX_D_CRIST + WP61_TAB_D_CRIST;
        public static final int WP67_ELE_MAX_EST_POLI_CPI_PR = 2;
        public static final int WP67_TAB_EST_POLI_CPI_PR = WpolStatus.Len.STATUS + Lccvp671.Len.ID_PTF + Wp67Dati.Len.DATI;
        public static final int WP67_AREA_EST_POLI_CPI_PR = WP67_ELE_MAX_EST_POLI_CPI_PR + WP67_TAB_EST_POLI_CPI_PR;
        public static final int WP01_ELE_MAX_RICH_EST = 2;
        public static final int WP01_TAB_RICH_EST = WpolStatus.Len.STATUS + Lccvp011.Len.ID_PTF + Wp01Dati.Len.DATI;
        public static final int WP01_AREA_RICH_EST = WP01_ELE_MAX_RICH_EST + WP01_TAB_RICH_EST;
        public static final int WP86_ELE_MOT_LIQ_MAX = 2;
        public static final int WP86_AREA_MOT_LIQ = WP86_ELE_MOT_LIQ_MAX + AreaBusiness.WP86_TAB_MOT_LIQ_MAXOCCURS * Wp86TabMotLiq.Len.WP86_TAB_MOT_LIQ;
        public static final int WK_MOVI_ORIG = 5;
        public static final int WP88_ELE_SER_VAL_MAX = 2;
        public static final int WP88_AREA_SERV_VAL = WP88_ELE_SER_VAL_MAX + AreaBusiness.WP88_TAB_SERV_VAL_MAXOCCURS * Wp88TabServVal.Len.WP88_TAB_SERV_VAL;
        public static final int WP89_ELE_DSERV_VAL_MAX = 2;
        public static final int WP89_AREA_DSERV_VAL = WP89_ELE_DSERV_VAL_MAX + AreaBusiness.WP89_TAB_DSERV_VAL_MAXOCCURS * Wp89TabDservVal.Len.WP89_TAB_DSERV_VAL;
        public static final int WP56_QUEST_ADEG_VER_MAX = 2;
        public static final int WP56_AREA_QUEST_ADEG_VER = WP56_QUEST_ADEG_VER_MAX + AreaBusiness.WP56_TAB_QUEST_ADEG_VER_MAXOCCURS * Wp56TabQuestAdegVer.Len.WP56_TAB_QUEST_ADEG_VER;
        public static final int WCDG_AREA_COMMIS_GEST = Ivvc0224.Len.AREA_COMMIS_GEST_CALC + Ivvc0224.Len.AREA_COMMIS_GEST_VV + Ivvc0224.Len.AREA_COMMIS_GEST_OUT;
        public static final int AREA_BUSINESS = WADE_AREA_ADESIONE + WBEP_AREA_BENEF + WBEL_AREA_BENEF_LIQ + WDCO_AREA_DT_COLLETTIVA + WDFA_AREA_DT_FISC_ADES + WDEQ_AREA_DETT_QUEST + WDTC_AREA_DETT_TIT_CONT + WGRZ_AREA_GARANZIA + WGRL_AREA_GARANZIA_LIQ + WISO_AREA_IMPOSTA_SOST + WLQU_AREA_LIQUIDAZIONE + WMOV_AREA_MOVIMENTO + WMFZ_AREA_MOVI_FINRIO + WPOG_AREA_PARAM_OGG + WPMO_AREA_PARAM_MOV + WPLI_AREA_PERC_LIQ + WPOL_AREA_POLIZZA + WPRE_AREA_PRESTITI + WPVT_AREA_PROV + WQUE_AREA_QUEST + WRIC_AREA_RICH + WRDF_AREA_RICH_DISINV_FND + WRIF_AREA_RICH_INV_FND + WRAN_AREA_RAPP_ANAG + WE15_AREA_EST_RAPP_ANAG + WRRE_AREA_RAPP_RETE + WSPG_AREA_SOPRAP_GAR + WSDI_AREA_STRA_INV + WTIT_AREA_TIT_CONT + WTCL_AREA_TIT_LIQ + W1TGA_AREA_TRANCHE + WTLI_AREA_TRCH_LIQ + WDAD_AREA_DEFAULT_ADES + WOCO_AREA_OGG_COLL + WDFL_AREA_DFL + WRST_AREA_RST + WL19_AREA_QUOTE + WL30_AREA_REINVST_POLI + WL23_AREA_VINC_PEG + WOPZ_AREA_OPZIONI + WGOP_AREA_GARANZIA_OPZ + WTOP_AREA_TRANCHE_OPZ + WCNT_AREA_DATI_CONTEST + WCLT_AREA_CLAU_TXT + WP58_AREA_IMPOSTA_BOLLO + WP61_AREA_D_CRIST + WP67_AREA_EST_POLI_CPI_PR + WP01_AREA_RICH_EST + Ivvc0222AreaFndXTranche.Len.IVVC0222_AREA_FND_X_TRANCHE + WP86_AREA_MOT_LIQ + AreaIvvc0223.Len.AREA_IVVC0223 + WK_MOVI_ORIG + WP88_AREA_SERV_VAL + WP89_AREA_DSERV_VAL + WP56_AREA_QUEST_ADEG_VER + WCDG_AREA_COMMIS_GEST;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
