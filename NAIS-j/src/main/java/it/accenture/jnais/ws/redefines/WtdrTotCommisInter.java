package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-COMMIS-INTER<br>
 * Variable: WTDR-TOT-COMMIS-INTER from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_COMMIS_INTER;
    }

    public void setWtdrTotCommisInter(AfDecimal wtdrTotCommisInter) {
        writeDecimalAsPacked(Pos.WTDR_TOT_COMMIS_INTER, wtdrTotCommisInter.copy());
    }

    /**Original name: WTDR-TOT-COMMIS-INTER<br>*/
    public AfDecimal getWtdrTotCommisInter() {
        return readPackedAsDecimal(Pos.WTDR_TOT_COMMIS_INTER, Len.Int.WTDR_TOT_COMMIS_INTER, Len.Fract.WTDR_TOT_COMMIS_INTER);
    }

    public void setWtdrTotCommisInterNull(String wtdrTotCommisInterNull) {
        writeString(Pos.WTDR_TOT_COMMIS_INTER_NULL, wtdrTotCommisInterNull, Len.WTDR_TOT_COMMIS_INTER_NULL);
    }

    /**Original name: WTDR-TOT-COMMIS-INTER-NULL<br>*/
    public String getWtdrTotCommisInterNull() {
        return readString(Pos.WTDR_TOT_COMMIS_INTER_NULL, Len.WTDR_TOT_COMMIS_INTER_NULL);
    }

    public String getWtdrTotCommisInterNullFormatted() {
        return Functions.padBlanks(getWtdrTotCommisInterNull(), Len.WTDR_TOT_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_COMMIS_INTER = 1;
        public static final int WTDR_TOT_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_COMMIS_INTER = 8;
        public static final int WTDR_TOT_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
