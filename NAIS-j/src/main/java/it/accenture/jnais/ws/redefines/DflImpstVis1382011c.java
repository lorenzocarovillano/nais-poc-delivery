package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-VIS-1382011C<br>
 * Variable: DFL-IMPST-VIS-1382011C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstVis1382011c extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstVis1382011c() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_VIS1382011C;
    }

    public void setDflImpstVis1382011c(AfDecimal dflImpstVis1382011c) {
        writeDecimalAsPacked(Pos.DFL_IMPST_VIS1382011C, dflImpstVis1382011c.copy());
    }

    public void setDflImpstVis1382011cFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_VIS1382011C, Pos.DFL_IMPST_VIS1382011C);
    }

    /**Original name: DFL-IMPST-VIS-1382011C<br>*/
    public AfDecimal getDflImpstVis1382011c() {
        return readPackedAsDecimal(Pos.DFL_IMPST_VIS1382011C, Len.Int.DFL_IMPST_VIS1382011C, Len.Fract.DFL_IMPST_VIS1382011C);
    }

    public byte[] getDflImpstVis1382011cAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_VIS1382011C, Pos.DFL_IMPST_VIS1382011C);
        return buffer;
    }

    public void setDflImpstVis1382011cNull(String dflImpstVis1382011cNull) {
        writeString(Pos.DFL_IMPST_VIS1382011C_NULL, dflImpstVis1382011cNull, Len.DFL_IMPST_VIS1382011C_NULL);
    }

    /**Original name: DFL-IMPST-VIS-1382011C-NULL<br>*/
    public String getDflImpstVis1382011cNull() {
        return readString(Pos.DFL_IMPST_VIS1382011C_NULL, Len.DFL_IMPST_VIS1382011C_NULL);
    }

    public String getDflImpstVis1382011cNullFormatted() {
        return Functions.padBlanks(getDflImpstVis1382011cNull(), Len.DFL_IMPST_VIS1382011C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS1382011C = 1;
        public static final int DFL_IMPST_VIS1382011C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS1382011C = 8;
        public static final int DFL_IMPST_VIS1382011C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS1382011C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS1382011C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
