package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-OLD-TS-TEC<br>
 * Variable: WTGA-OLD-TS-TEC from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaOldTsTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaOldTsTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_OLD_TS_TEC;
    }

    public void setWtgaOldTsTec(AfDecimal wtgaOldTsTec) {
        writeDecimalAsPacked(Pos.WTGA_OLD_TS_TEC, wtgaOldTsTec.copy());
    }

    public void setWtgaOldTsTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_OLD_TS_TEC, Pos.WTGA_OLD_TS_TEC);
    }

    /**Original name: WTGA-OLD-TS-TEC<br>*/
    public AfDecimal getWtgaOldTsTec() {
        return readPackedAsDecimal(Pos.WTGA_OLD_TS_TEC, Len.Int.WTGA_OLD_TS_TEC, Len.Fract.WTGA_OLD_TS_TEC);
    }

    public byte[] getWtgaOldTsTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_OLD_TS_TEC, Pos.WTGA_OLD_TS_TEC);
        return buffer;
    }

    public void initWtgaOldTsTecSpaces() {
        fill(Pos.WTGA_OLD_TS_TEC, Len.WTGA_OLD_TS_TEC, Types.SPACE_CHAR);
    }

    public void setWtgaOldTsTecNull(String wtgaOldTsTecNull) {
        writeString(Pos.WTGA_OLD_TS_TEC_NULL, wtgaOldTsTecNull, Len.WTGA_OLD_TS_TEC_NULL);
    }

    /**Original name: WTGA-OLD-TS-TEC-NULL<br>*/
    public String getWtgaOldTsTecNull() {
        return readString(Pos.WTGA_OLD_TS_TEC_NULL, Len.WTGA_OLD_TS_TEC_NULL);
    }

    public String getWtgaOldTsTecNullFormatted() {
        return Functions.padBlanks(getWtgaOldTsTecNull(), Len.WTGA_OLD_TS_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_OLD_TS_TEC = 1;
        public static final int WTGA_OLD_TS_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_OLD_TS_TEC = 8;
        public static final int WTGA_OLD_TS_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_OLD_TS_TEC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_OLD_TS_TEC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
