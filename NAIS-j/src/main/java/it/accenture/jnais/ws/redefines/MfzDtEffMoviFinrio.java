package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MFZ-DT-EFF-MOVI-FINRIO<br>
 * Variable: MFZ-DT-EFF-MOVI-FINRIO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MfzDtEffMoviFinrio extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MfzDtEffMoviFinrio() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MFZ_DT_EFF_MOVI_FINRIO;
    }

    public void setMfzDtEffMoviFinrio(int mfzDtEffMoviFinrio) {
        writeIntAsPacked(Pos.MFZ_DT_EFF_MOVI_FINRIO, mfzDtEffMoviFinrio, Len.Int.MFZ_DT_EFF_MOVI_FINRIO);
    }

    public void setMfzDtEffMoviFinrioFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MFZ_DT_EFF_MOVI_FINRIO, Pos.MFZ_DT_EFF_MOVI_FINRIO);
    }

    /**Original name: MFZ-DT-EFF-MOVI-FINRIO<br>*/
    public int getMfzDtEffMoviFinrio() {
        return readPackedAsInt(Pos.MFZ_DT_EFF_MOVI_FINRIO, Len.Int.MFZ_DT_EFF_MOVI_FINRIO);
    }

    public byte[] getMfzDtEffMoviFinrioAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MFZ_DT_EFF_MOVI_FINRIO, Pos.MFZ_DT_EFF_MOVI_FINRIO);
        return buffer;
    }

    public void setMfzDtEffMoviFinrioNull(String mfzDtEffMoviFinrioNull) {
        writeString(Pos.MFZ_DT_EFF_MOVI_FINRIO_NULL, mfzDtEffMoviFinrioNull, Len.MFZ_DT_EFF_MOVI_FINRIO_NULL);
    }

    /**Original name: MFZ-DT-EFF-MOVI-FINRIO-NULL<br>*/
    public String getMfzDtEffMoviFinrioNull() {
        return readString(Pos.MFZ_DT_EFF_MOVI_FINRIO_NULL, Len.MFZ_DT_EFF_MOVI_FINRIO_NULL);
    }

    public String getMfzDtEffMoviFinrioNullFormatted() {
        return Functions.padBlanks(getMfzDtEffMoviFinrioNull(), Len.MFZ_DT_EFF_MOVI_FINRIO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MFZ_DT_EFF_MOVI_FINRIO = 1;
        public static final int MFZ_DT_EFF_MOVI_FINRIO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MFZ_DT_EFF_MOVI_FINRIO = 5;
        public static final int MFZ_DT_EFF_MOVI_FINRIO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MFZ_DT_EFF_MOVI_FINRIO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
