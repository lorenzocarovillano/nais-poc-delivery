package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.Idsv0502;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Ivvc0224;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3480<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3480Data {

    //==== PROPERTIES ====
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IVVC0224
    private Ivvc0224 ivvc0224 = new Ivvc0224();
    //Original name: IX-INDICI
    private IxIndiciLvvs3480 ixIndici = new IxIndiciLvvs3480();
    //Original name: IDSV0502
    private Idsv0502 idsv0502 = new Idsv0502();
    //Original name: INT-REGISTER1
    private short intRegister1 = DefaultValues.SHORT_VAL;

    //==== METHODS ====
    public void setIntRegister1(short intRegister1) {
        this.intRegister1 = intRegister1;
    }

    public short getIntRegister1() {
        return this.intRegister1;
    }

    public Idsv0502 getIdsv0502() {
        return idsv0502;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Ivvc0224 getIvvc0224() {
        return ivvc0224;
    }

    public IxIndiciLvvs3480 getIxIndici() {
        return ixIndici;
    }
}
