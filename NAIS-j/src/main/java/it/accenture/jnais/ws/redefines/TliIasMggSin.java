package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-IAS-MGG-SIN<br>
 * Variable: TLI-IAS-MGG-SIN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliIasMggSin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliIasMggSin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_IAS_MGG_SIN;
    }

    public void setTliIasMggSin(AfDecimal tliIasMggSin) {
        writeDecimalAsPacked(Pos.TLI_IAS_MGG_SIN, tliIasMggSin.copy());
    }

    public void setTliIasMggSinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_IAS_MGG_SIN, Pos.TLI_IAS_MGG_SIN);
    }

    /**Original name: TLI-IAS-MGG-SIN<br>*/
    public AfDecimal getTliIasMggSin() {
        return readPackedAsDecimal(Pos.TLI_IAS_MGG_SIN, Len.Int.TLI_IAS_MGG_SIN, Len.Fract.TLI_IAS_MGG_SIN);
    }

    public byte[] getTliIasMggSinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_IAS_MGG_SIN, Pos.TLI_IAS_MGG_SIN);
        return buffer;
    }

    public void setTliIasMggSinNull(String tliIasMggSinNull) {
        writeString(Pos.TLI_IAS_MGG_SIN_NULL, tliIasMggSinNull, Len.TLI_IAS_MGG_SIN_NULL);
    }

    /**Original name: TLI-IAS-MGG-SIN-NULL<br>*/
    public String getTliIasMggSinNull() {
        return readString(Pos.TLI_IAS_MGG_SIN_NULL, Len.TLI_IAS_MGG_SIN_NULL);
    }

    public String getTliIasMggSinNullFormatted() {
        return Functions.padBlanks(getTliIasMggSinNull(), Len.TLI_IAS_MGG_SIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_IAS_MGG_SIN = 1;
        public static final int TLI_IAS_MGG_SIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_IAS_MGG_SIN = 8;
        public static final int TLI_IAS_MGG_SIN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_IAS_MGG_SIN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_IAS_MGG_SIN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
