package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-IMP-TFR-STRC<br>
 * Variable: WTDR-IMP-TFR-STRC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrImpTfrStrc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrImpTfrStrc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_IMP_TFR_STRC;
    }

    public void setWtdrImpTfrStrc(AfDecimal wtdrImpTfrStrc) {
        writeDecimalAsPacked(Pos.WTDR_IMP_TFR_STRC, wtdrImpTfrStrc.copy());
    }

    /**Original name: WTDR-IMP-TFR-STRC<br>*/
    public AfDecimal getWtdrImpTfrStrc() {
        return readPackedAsDecimal(Pos.WTDR_IMP_TFR_STRC, Len.Int.WTDR_IMP_TFR_STRC, Len.Fract.WTDR_IMP_TFR_STRC);
    }

    public void setWtdrImpTfrStrcNull(String wtdrImpTfrStrcNull) {
        writeString(Pos.WTDR_IMP_TFR_STRC_NULL, wtdrImpTfrStrcNull, Len.WTDR_IMP_TFR_STRC_NULL);
    }

    /**Original name: WTDR-IMP-TFR-STRC-NULL<br>*/
    public String getWtdrImpTfrStrcNull() {
        return readString(Pos.WTDR_IMP_TFR_STRC_NULL, Len.WTDR_IMP_TFR_STRC_NULL);
    }

    public String getWtdrImpTfrStrcNullFormatted() {
        return Functions.padBlanks(getWtdrImpTfrStrcNull(), Len.WTDR_IMP_TFR_STRC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_TFR_STRC = 1;
        public static final int WTDR_IMP_TFR_STRC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_TFR_STRC = 8;
        public static final int WTDR_IMP_TFR_STRC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_TFR_STRC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_TFR_STRC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
