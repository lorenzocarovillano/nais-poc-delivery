package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-IAS-MGG-SIN<br>
 * Variable: WTLI-IAS-MGG-SIN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliIasMggSin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliIasMggSin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_IAS_MGG_SIN;
    }

    public void setWtliIasMggSin(AfDecimal wtliIasMggSin) {
        writeDecimalAsPacked(Pos.WTLI_IAS_MGG_SIN, wtliIasMggSin.copy());
    }

    public void setWtliIasMggSinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_IAS_MGG_SIN, Pos.WTLI_IAS_MGG_SIN);
    }

    /**Original name: WTLI-IAS-MGG-SIN<br>*/
    public AfDecimal getWtliIasMggSin() {
        return readPackedAsDecimal(Pos.WTLI_IAS_MGG_SIN, Len.Int.WTLI_IAS_MGG_SIN, Len.Fract.WTLI_IAS_MGG_SIN);
    }

    public byte[] getWtliIasMggSinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_IAS_MGG_SIN, Pos.WTLI_IAS_MGG_SIN);
        return buffer;
    }

    public void initWtliIasMggSinSpaces() {
        fill(Pos.WTLI_IAS_MGG_SIN, Len.WTLI_IAS_MGG_SIN, Types.SPACE_CHAR);
    }

    public void setWtliIasMggSinNull(String wtliIasMggSinNull) {
        writeString(Pos.WTLI_IAS_MGG_SIN_NULL, wtliIasMggSinNull, Len.WTLI_IAS_MGG_SIN_NULL);
    }

    /**Original name: WTLI-IAS-MGG-SIN-NULL<br>*/
    public String getWtliIasMggSinNull() {
        return readString(Pos.WTLI_IAS_MGG_SIN_NULL, Len.WTLI_IAS_MGG_SIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_IAS_MGG_SIN = 1;
        public static final int WTLI_IAS_MGG_SIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_IAS_MGG_SIN = 8;
        public static final int WTLI_IAS_MGG_SIN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_IAS_MGG_SIN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_IAS_MGG_SIN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
