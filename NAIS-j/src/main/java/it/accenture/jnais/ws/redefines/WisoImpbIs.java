package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WISO-IMPB-IS<br>
 * Variable: WISO-IMPB-IS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoImpbIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoImpbIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_IMPB_IS;
    }

    public void setWisoImpbIs(AfDecimal wisoImpbIs) {
        writeDecimalAsPacked(Pos.WISO_IMPB_IS, wisoImpbIs.copy());
    }

    public void setWisoImpbIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_IMPB_IS, Pos.WISO_IMPB_IS);
    }

    /**Original name: WISO-IMPB-IS<br>*/
    public AfDecimal getWisoImpbIs() {
        return readPackedAsDecimal(Pos.WISO_IMPB_IS, Len.Int.WISO_IMPB_IS, Len.Fract.WISO_IMPB_IS);
    }

    public byte[] getWisoImpbIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_IMPB_IS, Pos.WISO_IMPB_IS);
        return buffer;
    }

    public void initWisoImpbIsSpaces() {
        fill(Pos.WISO_IMPB_IS, Len.WISO_IMPB_IS, Types.SPACE_CHAR);
    }

    public void setWisoImpbIsNull(String wisoImpbIsNull) {
        writeString(Pos.WISO_IMPB_IS_NULL, wisoImpbIsNull, Len.WISO_IMPB_IS_NULL);
    }

    /**Original name: WISO-IMPB-IS-NULL<br>*/
    public String getWisoImpbIsNull() {
        return readString(Pos.WISO_IMPB_IS_NULL, Len.WISO_IMPB_IS_NULL);
    }

    public String getDisoImpbIsNullFormatted() {
        return Functions.padBlanks(getWisoImpbIsNull(), Len.WISO_IMPB_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_IMPB_IS = 1;
        public static final int WISO_IMPB_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_IMPB_IS = 8;
        public static final int WISO_IMPB_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WISO_IMPB_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_IMPB_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
