package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-TOT-IMPST<br>
 * Variable: WDFA-TOT-IMPST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaTotImpst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaTotImpst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_TOT_IMPST;
    }

    public void setWdfaTotImpst(AfDecimal wdfaTotImpst) {
        writeDecimalAsPacked(Pos.WDFA_TOT_IMPST, wdfaTotImpst.copy());
    }

    public void setWdfaTotImpstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_TOT_IMPST, Pos.WDFA_TOT_IMPST);
    }

    /**Original name: WDFA-TOT-IMPST<br>*/
    public AfDecimal getWdfaTotImpst() {
        return readPackedAsDecimal(Pos.WDFA_TOT_IMPST, Len.Int.WDFA_TOT_IMPST, Len.Fract.WDFA_TOT_IMPST);
    }

    public byte[] getWdfaTotImpstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_TOT_IMPST, Pos.WDFA_TOT_IMPST);
        return buffer;
    }

    public void setWdfaTotImpstNull(String wdfaTotImpstNull) {
        writeString(Pos.WDFA_TOT_IMPST_NULL, wdfaTotImpstNull, Len.WDFA_TOT_IMPST_NULL);
    }

    /**Original name: WDFA-TOT-IMPST-NULL<br>*/
    public String getWdfaTotImpstNull() {
        return readString(Pos.WDFA_TOT_IMPST_NULL, Len.WDFA_TOT_IMPST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_TOT_IMPST = 1;
        public static final int WDFA_TOT_IMPST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_TOT_IMPST = 8;
        public static final int WDFA_TOT_IMPST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_TOT_IMPST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_TOT_IMPST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
