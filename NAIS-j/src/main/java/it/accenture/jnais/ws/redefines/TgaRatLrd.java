package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-RAT-LRD<br>
 * Variable: TGA-RAT-LRD from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaRatLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaRatLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_RAT_LRD;
    }

    public void setTgaRatLrd(AfDecimal tgaRatLrd) {
        writeDecimalAsPacked(Pos.TGA_RAT_LRD, tgaRatLrd.copy());
    }

    public void setTgaRatLrdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_RAT_LRD, Pos.TGA_RAT_LRD);
    }

    /**Original name: TGA-RAT-LRD<br>*/
    public AfDecimal getTgaRatLrd() {
        return readPackedAsDecimal(Pos.TGA_RAT_LRD, Len.Int.TGA_RAT_LRD, Len.Fract.TGA_RAT_LRD);
    }

    public byte[] getTgaRatLrdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_RAT_LRD, Pos.TGA_RAT_LRD);
        return buffer;
    }

    public void setTgaRatLrdNull(String tgaRatLrdNull) {
        writeString(Pos.TGA_RAT_LRD_NULL, tgaRatLrdNull, Len.TGA_RAT_LRD_NULL);
    }

    /**Original name: TGA-RAT-LRD-NULL<br>*/
    public String getTgaRatLrdNull() {
        return readString(Pos.TGA_RAT_LRD_NULL, Len.TGA_RAT_LRD_NULL);
    }

    public String getTgaRatLrdNullFormatted() {
        return Functions.padBlanks(getTgaRatLrdNull(), Len.TGA_RAT_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_RAT_LRD = 1;
        public static final int TGA_RAT_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_RAT_LRD = 8;
        public static final int TGA_RAT_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_RAT_LRD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_RAT_LRD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
