package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-BOLLO-DETT-C<br>
 * Variable: LQU-IMPST-BOLLO-DETT-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpstBolloDettC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpstBolloDettC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST_BOLLO_DETT_C;
    }

    public void setLquImpstBolloDettC(AfDecimal lquImpstBolloDettC) {
        writeDecimalAsPacked(Pos.LQU_IMPST_BOLLO_DETT_C, lquImpstBolloDettC.copy());
    }

    public void setLquImpstBolloDettCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST_BOLLO_DETT_C, Pos.LQU_IMPST_BOLLO_DETT_C);
    }

    /**Original name: LQU-IMPST-BOLLO-DETT-C<br>*/
    public AfDecimal getLquImpstBolloDettC() {
        return readPackedAsDecimal(Pos.LQU_IMPST_BOLLO_DETT_C, Len.Int.LQU_IMPST_BOLLO_DETT_C, Len.Fract.LQU_IMPST_BOLLO_DETT_C);
    }

    public byte[] getLquImpstBolloDettCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST_BOLLO_DETT_C, Pos.LQU_IMPST_BOLLO_DETT_C);
        return buffer;
    }

    public void setLquImpstBolloDettCNull(String lquImpstBolloDettCNull) {
        writeString(Pos.LQU_IMPST_BOLLO_DETT_C_NULL, lquImpstBolloDettCNull, Len.LQU_IMPST_BOLLO_DETT_C_NULL);
    }

    /**Original name: LQU-IMPST-BOLLO-DETT-C-NULL<br>*/
    public String getLquImpstBolloDettCNull() {
        return readString(Pos.LQU_IMPST_BOLLO_DETT_C_NULL, Len.LQU_IMPST_BOLLO_DETT_C_NULL);
    }

    public String getLquImpstBolloDettCNullFormatted() {
        return Functions.padBlanks(getLquImpstBolloDettCNull(), Len.LQU_IMPST_BOLLO_DETT_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_BOLLO_DETT_C = 1;
        public static final int LQU_IMPST_BOLLO_DETT_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_BOLLO_DETT_C = 8;
        public static final int LQU_IMPST_BOLLO_DETT_C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_BOLLO_DETT_C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_BOLLO_DETT_C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
