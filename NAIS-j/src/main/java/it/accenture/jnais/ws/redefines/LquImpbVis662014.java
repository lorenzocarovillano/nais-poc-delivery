package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-VIS-662014<br>
 * Variable: LQU-IMPB-VIS-662014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbVis662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbVis662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_VIS662014;
    }

    public void setLquImpbVis662014(AfDecimal lquImpbVis662014) {
        writeDecimalAsPacked(Pos.LQU_IMPB_VIS662014, lquImpbVis662014.copy());
    }

    public void setLquImpbVis662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_VIS662014, Pos.LQU_IMPB_VIS662014);
    }

    /**Original name: LQU-IMPB-VIS-662014<br>*/
    public AfDecimal getLquImpbVis662014() {
        return readPackedAsDecimal(Pos.LQU_IMPB_VIS662014, Len.Int.LQU_IMPB_VIS662014, Len.Fract.LQU_IMPB_VIS662014);
    }

    public byte[] getLquImpbVis662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_VIS662014, Pos.LQU_IMPB_VIS662014);
        return buffer;
    }

    public void setLquImpbVis662014Null(String lquImpbVis662014Null) {
        writeString(Pos.LQU_IMPB_VIS662014_NULL, lquImpbVis662014Null, Len.LQU_IMPB_VIS662014_NULL);
    }

    /**Original name: LQU-IMPB-VIS-662014-NULL<br>*/
    public String getLquImpbVis662014Null() {
        return readString(Pos.LQU_IMPB_VIS662014_NULL, Len.LQU_IMPB_VIS662014_NULL);
    }

    public String getLquImpbVis662014NullFormatted() {
        return Functions.padBlanks(getLquImpbVis662014Null(), Len.LQU_IMPB_VIS662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_VIS662014 = 1;
        public static final int LQU_IMPB_VIS662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_VIS662014 = 8;
        public static final int LQU_IMPB_VIS662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_VIS662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_VIS662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
