package it.accenture.jnais.ws;

import it.accenture.jnais.copy.BilaFndEstrDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndMoviFinrio;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSB010<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsb010Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-BILA-FND-ESTR
    private IndMoviFinrio indBilaFndEstr = new IndMoviFinrio();
    //Original name: BILA-FND-ESTR-DB
    private BilaFndEstrDb bilaFndEstrDb = new BilaFndEstrDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public BilaFndEstrDb getBilaFndEstrDb() {
        return bilaFndEstrDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndMoviFinrio getIndBilaFndEstr() {
        return indBilaFndEstr;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
