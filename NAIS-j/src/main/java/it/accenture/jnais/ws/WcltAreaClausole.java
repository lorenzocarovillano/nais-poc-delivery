package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WcltTabClauTxt;

/**Original name: WCLT-AREA-CLAUSOLE<br>
 * Variable: WCLT-AREA-CLAUSOLE from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WcltAreaClausole extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_CLAU_TEST_MAXOCCURS = 54;
    //Original name: WCLT-ELE-CLAU-TEST-MAX
    private short eleClauTestMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WCLT-TAB-CLAU-TEST
    private WcltTabClauTxt[] tabClauTest = new WcltTabClauTxt[TAB_CLAU_TEST_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WcltAreaClausole() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WCLT_AREA_CLAUSOLE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWcltAreaClausoleBytes(buf);
    }

    public void init() {
        for (int tabClauTestIdx = 1; tabClauTestIdx <= TAB_CLAU_TEST_MAXOCCURS; tabClauTestIdx++) {
            tabClauTest[tabClauTestIdx - 1] = new WcltTabClauTxt();
        }
    }

    public String getWcltAreaClausoleFormatted() {
        return MarshalByteExt.bufferToStr(getWcltAreaClausoleBytes());
    }

    public void setWcltAreaClausoleBytes(byte[] buffer) {
        setWcltAreaClausoleBytes(buffer, 1);
    }

    public byte[] getWcltAreaClausoleBytes() {
        byte[] buffer = new byte[Len.WCLT_AREA_CLAUSOLE];
        return getWcltAreaClausoleBytes(buffer, 1);
    }

    public void setWcltAreaClausoleBytes(byte[] buffer, int offset) {
        int position = offset;
        eleClauTestMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_CLAU_TEST_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabClauTest[idx - 1].setWcltTabClauTxtBytes(buffer, position);
                position += WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;
            }
            else {
                tabClauTest[idx - 1].initWcltTabClauTxtSpaces();
                position += WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;
            }
        }
    }

    public byte[] getWcltAreaClausoleBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleClauTestMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_CLAU_TEST_MAXOCCURS; idx++) {
            tabClauTest[idx - 1].getWcltTabClauTxtBytes(buffer, position);
            position += WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;
        }
        return buffer;
    }

    public void setEleClauTestMax(short eleClauTestMax) {
        this.eleClauTestMax = eleClauTestMax;
    }

    public short getEleClauTestMax() {
        return this.eleClauTestMax;
    }

    @Override
    public byte[] serialize() {
        return getWcltAreaClausoleBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_CLAU_TEST_MAX = 2;
        public static final int WCLT_AREA_CLAUSOLE = ELE_CLAU_TEST_MAX + WcltAreaClausole.TAB_CLAU_TEST_MAXOCCURS * WcltTabClauTxt.Len.WCLT_TAB_CLAU_TXT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
