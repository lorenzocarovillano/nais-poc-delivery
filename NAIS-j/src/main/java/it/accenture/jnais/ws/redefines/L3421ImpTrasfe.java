package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-TRASFE<br>
 * Variable: L3421-IMP-TRASFE from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_TRASFE;
    }

    public void setL3421ImpTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_TRASFE, Pos.L3421_IMP_TRASFE);
    }

    /**Original name: L3421-IMP-TRASFE<br>*/
    public AfDecimal getL3421ImpTrasfe() {
        return readPackedAsDecimal(Pos.L3421_IMP_TRASFE, Len.Int.L3421_IMP_TRASFE, Len.Fract.L3421_IMP_TRASFE);
    }

    public byte[] getL3421ImpTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_TRASFE, Pos.L3421_IMP_TRASFE);
        return buffer;
    }

    /**Original name: L3421-IMP-TRASFE-NULL<br>*/
    public String getL3421ImpTrasfeNull() {
        return readString(Pos.L3421_IMP_TRASFE_NULL, Len.L3421_IMP_TRASFE_NULL);
    }

    public String getL3421ImpTrasfeNullFormatted() {
        return Functions.padBlanks(getL3421ImpTrasfeNull(), Len.L3421_IMP_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_TRASFE = 1;
        public static final int L3421_IMP_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_TRASFE = 8;
        public static final int L3421_IMP_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
