package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP61-ID-ADES<br>
 * Variable: WP61-ID-ADES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp61IdAdes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp61IdAdes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP61_ID_ADES;
    }

    public void setWp61IdAdes(int wp61IdAdes) {
        writeIntAsPacked(Pos.WP61_ID_ADES, wp61IdAdes, Len.Int.WP61_ID_ADES);
    }

    public void setWp61IdAdesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP61_ID_ADES, Pos.WP61_ID_ADES);
    }

    /**Original name: WP61-ID-ADES<br>*/
    public int getWp61IdAdes() {
        return readPackedAsInt(Pos.WP61_ID_ADES, Len.Int.WP61_ID_ADES);
    }

    public byte[] getWp61IdAdesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP61_ID_ADES, Pos.WP61_ID_ADES);
        return buffer;
    }

    public void setWp61IdAdesNull(String wp61IdAdesNull) {
        writeString(Pos.WP61_ID_ADES_NULL, wp61IdAdesNull, Len.WP61_ID_ADES_NULL);
    }

    /**Original name: WP61-ID-ADES-NULL<br>*/
    public String getWp61IdAdesNull() {
        return readString(Pos.WP61_ID_ADES_NULL, Len.WP61_ID_ADES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP61_ID_ADES = 1;
        public static final int WP61_ID_ADES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_ID_ADES = 5;
        public static final int WP61_ID_ADES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
