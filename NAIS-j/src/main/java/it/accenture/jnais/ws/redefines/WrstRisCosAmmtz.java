package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-COS-AMMTZ<br>
 * Variable: WRST-RIS-COS-AMMTZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisCosAmmtz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisCosAmmtz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_COS_AMMTZ;
    }

    public void setWrstRisCosAmmtz(AfDecimal wrstRisCosAmmtz) {
        writeDecimalAsPacked(Pos.WRST_RIS_COS_AMMTZ, wrstRisCosAmmtz.copy());
    }

    public void setWrstRisCosAmmtzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_COS_AMMTZ, Pos.WRST_RIS_COS_AMMTZ);
    }

    /**Original name: WRST-RIS-COS-AMMTZ<br>*/
    public AfDecimal getWrstRisCosAmmtz() {
        return readPackedAsDecimal(Pos.WRST_RIS_COS_AMMTZ, Len.Int.WRST_RIS_COS_AMMTZ, Len.Fract.WRST_RIS_COS_AMMTZ);
    }

    public byte[] getWrstRisCosAmmtzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_COS_AMMTZ, Pos.WRST_RIS_COS_AMMTZ);
        return buffer;
    }

    public void initWrstRisCosAmmtzSpaces() {
        fill(Pos.WRST_RIS_COS_AMMTZ, Len.WRST_RIS_COS_AMMTZ, Types.SPACE_CHAR);
    }

    public void setWrstRisCosAmmtzNull(String wrstRisCosAmmtzNull) {
        writeString(Pos.WRST_RIS_COS_AMMTZ_NULL, wrstRisCosAmmtzNull, Len.WRST_RIS_COS_AMMTZ_NULL);
    }

    /**Original name: WRST-RIS-COS-AMMTZ-NULL<br>*/
    public String getWrstRisCosAmmtzNull() {
        return readString(Pos.WRST_RIS_COS_AMMTZ_NULL, Len.WRST_RIS_COS_AMMTZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_COS_AMMTZ = 1;
        public static final int WRST_RIS_COS_AMMTZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_COS_AMMTZ = 8;
        public static final int WRST_RIS_COS_AMMTZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_COS_AMMTZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_COS_AMMTZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
