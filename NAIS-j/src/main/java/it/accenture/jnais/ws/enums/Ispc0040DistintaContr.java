package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ISPC0040-DISTINTA-CONTR<br>
 * Variable: ISPC0040-DISTINTA-CONTR from copybook ISPC0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0040DistintaContr {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char NO = '0';
    public static final char SI = '1';

    //==== METHODS ====
    public void setDistintaContr(char distintaContr) {
        this.value = distintaContr;
    }

    public char getDistintaContr() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DISTINTA_CONTR = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
