package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-MAT<br>
 * Variable: RST-RIS-MAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisMat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisMat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_MAT;
    }

    public void setRstRisMat(AfDecimal rstRisMat) {
        writeDecimalAsPacked(Pos.RST_RIS_MAT, rstRisMat.copy());
    }

    public void setRstRisMatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_MAT, Pos.RST_RIS_MAT);
    }

    /**Original name: RST-RIS-MAT<br>*/
    public AfDecimal getRstRisMat() {
        return readPackedAsDecimal(Pos.RST_RIS_MAT, Len.Int.RST_RIS_MAT, Len.Fract.RST_RIS_MAT);
    }

    public byte[] getRstRisMatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_MAT, Pos.RST_RIS_MAT);
        return buffer;
    }

    public void setRstRisMatNull(String rstRisMatNull) {
        writeString(Pos.RST_RIS_MAT_NULL, rstRisMatNull, Len.RST_RIS_MAT_NULL);
    }

    /**Original name: RST-RIS-MAT-NULL<br>*/
    public String getRstRisMatNull() {
        return readString(Pos.RST_RIS_MAT_NULL, Len.RST_RIS_MAT_NULL);
    }

    public String getRstRisMatNullFormatted() {
        return Functions.padBlanks(getRstRisMatNull(), Len.RST_RIS_MAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_MAT = 1;
        public static final int RST_RIS_MAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_MAT = 8;
        public static final int RST_RIS_MAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_MAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_MAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
