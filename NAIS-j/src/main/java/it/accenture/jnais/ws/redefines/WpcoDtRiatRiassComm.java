package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-RIAT-RIASS-COMM<br>
 * Variable: WPCO-DT-RIAT-RIASS-COMM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtRiatRiassComm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtRiatRiassComm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_RIAT_RIASS_COMM;
    }

    public void setWpcoDtRiatRiassComm(int wpcoDtRiatRiassComm) {
        writeIntAsPacked(Pos.WPCO_DT_RIAT_RIASS_COMM, wpcoDtRiatRiassComm, Len.Int.WPCO_DT_RIAT_RIASS_COMM);
    }

    public void setDpcoDtRiatRiassCommFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_RIAT_RIASS_COMM, Pos.WPCO_DT_RIAT_RIASS_COMM);
    }

    /**Original name: WPCO-DT-RIAT-RIASS-COMM<br>*/
    public int getWpcoDtRiatRiassComm() {
        return readPackedAsInt(Pos.WPCO_DT_RIAT_RIASS_COMM, Len.Int.WPCO_DT_RIAT_RIASS_COMM);
    }

    public byte[] getWpcoDtRiatRiassCommAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_RIAT_RIASS_COMM, Pos.WPCO_DT_RIAT_RIASS_COMM);
        return buffer;
    }

    public void setWpcoDtRiatRiassCommNull(String wpcoDtRiatRiassCommNull) {
        writeString(Pos.WPCO_DT_RIAT_RIASS_COMM_NULL, wpcoDtRiatRiassCommNull, Len.WPCO_DT_RIAT_RIASS_COMM_NULL);
    }

    /**Original name: WPCO-DT-RIAT-RIASS-COMM-NULL<br>*/
    public String getWpcoDtRiatRiassCommNull() {
        return readString(Pos.WPCO_DT_RIAT_RIASS_COMM_NULL, Len.WPCO_DT_RIAT_RIASS_COMM_NULL);
    }

    public String getWpcoDtRiatRiassCommNullFormatted() {
        return Functions.padBlanks(getWpcoDtRiatRiassCommNull(), Len.WPCO_DT_RIAT_RIASS_COMM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_RIAT_RIASS_COMM = 1;
        public static final int WPCO_DT_RIAT_RIASS_COMM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_RIAT_RIASS_COMM = 5;
        public static final int WPCO_DT_RIAT_RIASS_COMM_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_RIAT_RIASS_COMM = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
