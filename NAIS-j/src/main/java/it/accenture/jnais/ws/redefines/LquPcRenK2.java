package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-PC-REN-K2<br>
 * Variable: LQU-PC-REN-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquPcRenK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquPcRenK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_PC_REN_K2;
    }

    public void setLquPcRenK2(AfDecimal lquPcRenK2) {
        writeDecimalAsPacked(Pos.LQU_PC_REN_K2, lquPcRenK2.copy());
    }

    public void setLquPcRenK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_PC_REN_K2, Pos.LQU_PC_REN_K2);
    }

    /**Original name: LQU-PC-REN-K2<br>*/
    public AfDecimal getLquPcRenK2() {
        return readPackedAsDecimal(Pos.LQU_PC_REN_K2, Len.Int.LQU_PC_REN_K2, Len.Fract.LQU_PC_REN_K2);
    }

    public byte[] getLquPcRenK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_PC_REN_K2, Pos.LQU_PC_REN_K2);
        return buffer;
    }

    public void setLquPcRenK2Null(String lquPcRenK2Null) {
        writeString(Pos.LQU_PC_REN_K2_NULL, lquPcRenK2Null, Len.LQU_PC_REN_K2_NULL);
    }

    /**Original name: LQU-PC-REN-K2-NULL<br>*/
    public String getLquPcRenK2Null() {
        return readString(Pos.LQU_PC_REN_K2_NULL, Len.LQU_PC_REN_K2_NULL);
    }

    public String getLquPcRenK2NullFormatted() {
        return Functions.padBlanks(getLquPcRenK2Null(), Len.LQU_PC_REN_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_PC_REN_K2 = 1;
        public static final int LQU_PC_REN_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_PC_REN_K2 = 4;
        public static final int LQU_PC_REN_K2_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_PC_REN_K2 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_PC_REN_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
