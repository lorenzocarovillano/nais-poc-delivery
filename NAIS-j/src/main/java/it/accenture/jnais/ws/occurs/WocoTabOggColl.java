package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvoco1;
import it.accenture.jnais.copy.WocoDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WOCO-TAB-OGG-COLL<br>
 * Variables: WOCO-TAB-OGG-COLL from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WocoTabOggColl {

    //==== PROPERTIES ====
    //Original name: LCCVOCO1
    private Lccvoco1 lccvoco1 = new Lccvoco1();

    //==== METHODS ====
    public void setVocoTabOggCollgBytes(byte[] buffer) {
        setWocoTabOggCollBytes(buffer, 1);
    }

    public byte[] getTabOggCollgBytes() {
        byte[] buffer = new byte[Len.WOCO_TAB_OGG_COLL];
        return getWocoTabOggCollBytes(buffer, 1);
    }

    public void setWocoTabOggCollBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvoco1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvoco1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvoco1.Len.Int.ID_PTF, 0));
        position += Lccvoco1.Len.ID_PTF;
        lccvoco1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWocoTabOggCollBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvoco1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvoco1.getIdPtf(), Lccvoco1.Len.Int.ID_PTF, 0);
        position += Lccvoco1.Len.ID_PTF;
        lccvoco1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWocoTabOggCollSpaces() {
        lccvoco1.initLccvoco1Spaces();
    }

    public Lccvoco1 getLccvoco1() {
        return lccvoco1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_TAB_OGG_COLL = WpolStatus.Len.STATUS + Lccvoco1.Len.ID_PTF + WocoDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
