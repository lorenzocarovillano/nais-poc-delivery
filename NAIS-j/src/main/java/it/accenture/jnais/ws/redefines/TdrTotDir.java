package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-DIR<br>
 * Variable: TDR-TOT-DIR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotDir extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotDir() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_DIR;
    }

    public void setTdrTotDir(AfDecimal tdrTotDir) {
        writeDecimalAsPacked(Pos.TDR_TOT_DIR, tdrTotDir.copy());
    }

    public void setTdrTotDirFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_DIR, Pos.TDR_TOT_DIR);
    }

    /**Original name: TDR-TOT-DIR<br>*/
    public AfDecimal getTdrTotDir() {
        return readPackedAsDecimal(Pos.TDR_TOT_DIR, Len.Int.TDR_TOT_DIR, Len.Fract.TDR_TOT_DIR);
    }

    public byte[] getTdrTotDirAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_DIR, Pos.TDR_TOT_DIR);
        return buffer;
    }

    public void setTdrTotDirNull(String tdrTotDirNull) {
        writeString(Pos.TDR_TOT_DIR_NULL, tdrTotDirNull, Len.TDR_TOT_DIR_NULL);
    }

    /**Original name: TDR-TOT-DIR-NULL<br>*/
    public String getTdrTotDirNull() {
        return readString(Pos.TDR_TOT_DIR_NULL, Len.TDR_TOT_DIR_NULL);
    }

    public String getTdrTotDirNullFormatted() {
        return Functions.padBlanks(getTdrTotDirNull(), Len.TDR_TOT_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_DIR = 1;
        public static final int TDR_TOT_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_DIR = 8;
        public static final int TDR_TOT_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
