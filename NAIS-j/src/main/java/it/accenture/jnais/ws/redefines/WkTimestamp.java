package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WK-TIMESTAMP<br>
 * Variable: WK-TIMESTAMP from program LVVS2720<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkTimestamp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkTimestamp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_TIMESTAMP;
    }

    @Override
    public void init() {
        int position = 1;
        writeString(position, "", Len.DATA_EFFETTO);
        position += Len.DATA_EFFETTO;
        writeString(position, "2359999999", Len.ORA);
    }

    public void setWkDataEffetto(String wkDataEffetto) {
        writeString(Pos.DATA_EFFETTO, wkDataEffetto, Len.DATA_EFFETTO);
    }

    /**Original name: WK-DATA-EFFETTO<br>*/
    public String getWkDataEffetto() {
        return readString(Pos.DATA_EFFETTO, Len.DATA_EFFETTO);
    }

    /**Original name: WK-TIMESTAMP-N<br>*/
    public long getWkTimestampN() {
        return readNumDispUnsignedLong(Pos.WK_TIMESTAMP_N, Len.WK_TIMESTAMP_N);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_TIMESTAMP = 1;
        public static final int DATA_EFFETTO = WK_TIMESTAMP;
        public static final int ORA = DATA_EFFETTO + Len.DATA_EFFETTO;
        public static final int WK_TIMESTAMP_N = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DATA_EFFETTO = 8;
        public static final int ORA = 10;
        public static final int WK_TIMESTAMP = DATA_EFFETTO + ORA;
        public static final int WK_TIMESTAMP_N = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
