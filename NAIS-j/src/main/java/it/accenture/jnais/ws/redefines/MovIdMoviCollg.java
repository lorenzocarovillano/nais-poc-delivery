package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MOV-ID-MOVI-COLLG<br>
 * Variable: MOV-ID-MOVI-COLLG from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MovIdMoviCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MovIdMoviCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MOV_ID_MOVI_COLLG;
    }

    public void setMovIdMoviCollg(int movIdMoviCollg) {
        writeIntAsPacked(Pos.MOV_ID_MOVI_COLLG, movIdMoviCollg, Len.Int.MOV_ID_MOVI_COLLG);
    }

    public void setMovIdMoviCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MOV_ID_MOVI_COLLG, Pos.MOV_ID_MOVI_COLLG);
    }

    /**Original name: MOV-ID-MOVI-COLLG<br>*/
    public int getMovIdMoviCollg() {
        return readPackedAsInt(Pos.MOV_ID_MOVI_COLLG, Len.Int.MOV_ID_MOVI_COLLG);
    }

    public byte[] getMovIdMoviCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MOV_ID_MOVI_COLLG, Pos.MOV_ID_MOVI_COLLG);
        return buffer;
    }

    public void setMovIdMoviCollgNull(String movIdMoviCollgNull) {
        writeString(Pos.MOV_ID_MOVI_COLLG_NULL, movIdMoviCollgNull, Len.MOV_ID_MOVI_COLLG_NULL);
    }

    /**Original name: MOV-ID-MOVI-COLLG-NULL<br>*/
    public String getMovIdMoviCollgNull() {
        return readString(Pos.MOV_ID_MOVI_COLLG_NULL, Len.MOV_ID_MOVI_COLLG_NULL);
    }

    public String getMovIdMoviCollgNullFormatted() {
        return Functions.padBlanks(getMovIdMoviCollgNull(), Len.MOV_ID_MOVI_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MOV_ID_MOVI_COLLG = 1;
        public static final int MOV_ID_MOVI_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MOV_ID_MOVI_COLLG = 5;
        public static final int MOV_ID_MOVI_COLLG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MOV_ID_MOVI_COLLG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
