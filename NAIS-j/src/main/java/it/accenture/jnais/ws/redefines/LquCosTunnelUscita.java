package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-COS-TUNNEL-USCITA<br>
 * Variable: LQU-COS-TUNNEL-USCITA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquCosTunnelUscita extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquCosTunnelUscita() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_COS_TUNNEL_USCITA;
    }

    public void setLquCosTunnelUscita(AfDecimal lquCosTunnelUscita) {
        writeDecimalAsPacked(Pos.LQU_COS_TUNNEL_USCITA, lquCosTunnelUscita.copy());
    }

    public void setLquCosTunnelUscitaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_COS_TUNNEL_USCITA, Pos.LQU_COS_TUNNEL_USCITA);
    }

    /**Original name: LQU-COS-TUNNEL-USCITA<br>*/
    public AfDecimal getLquCosTunnelUscita() {
        return readPackedAsDecimal(Pos.LQU_COS_TUNNEL_USCITA, Len.Int.LQU_COS_TUNNEL_USCITA, Len.Fract.LQU_COS_TUNNEL_USCITA);
    }

    public byte[] getLquCosTunnelUscitaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_COS_TUNNEL_USCITA, Pos.LQU_COS_TUNNEL_USCITA);
        return buffer;
    }

    public void setLquCosTunnelUscitaNull(String lquCosTunnelUscitaNull) {
        writeString(Pos.LQU_COS_TUNNEL_USCITA_NULL, lquCosTunnelUscitaNull, Len.LQU_COS_TUNNEL_USCITA_NULL);
    }

    /**Original name: LQU-COS-TUNNEL-USCITA-NULL<br>*/
    public String getLquCosTunnelUscitaNull() {
        return readString(Pos.LQU_COS_TUNNEL_USCITA_NULL, Len.LQU_COS_TUNNEL_USCITA_NULL);
    }

    public String getLquCosTunnelUscitaNullFormatted() {
        return Functions.padBlanks(getLquCosTunnelUscitaNull(), Len.LQU_COS_TUNNEL_USCITA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_COS_TUNNEL_USCITA = 1;
        public static final int LQU_COS_TUNNEL_USCITA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_COS_TUNNEL_USCITA = 8;
        public static final int LQU_COS_TUNNEL_USCITA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_COS_TUNNEL_USCITA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_COS_TUNNEL_USCITA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
