package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCDG-FND-X-CDG-CALC<br>
 * Variables: WCDG-FND-X-CDG-CALC from copybook IVVC0224<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WcdgFndXCdgCalc {

    //==== PROPERTIES ====
    //Original name: WCDG-COD-FND-CALC
    private String codFndCalc = DefaultValues.stringVal(Len.COD_FND_CALC);
    //Original name: WCDG-PERC-FND-CALC
    private AfDecimal percFndCalc = new AfDecimal(DefaultValues.DEC_VAL, 10, 7);
    //Original name: WCDG-CDG-REST-CALC
    private AfDecimal cdgRestCalc = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);

    //==== METHODS ====
    public void setFndXCdgCalcBytes(byte[] buffer, int offset) {
        int position = offset;
        codFndCalc = MarshalByte.readString(buffer, position, Len.COD_FND_CALC);
        position += Len.COD_FND_CALC;
        percFndCalc.assign(MarshalByte.readDecimal(buffer, position, Len.Int.PERC_FND_CALC, Len.Fract.PERC_FND_CALC));
        position += Len.PERC_FND_CALC;
        cdgRestCalc.assign(MarshalByte.readDecimal(buffer, position, Len.Int.CDG_REST_CALC, Len.Fract.CDG_REST_CALC));
    }

    public byte[] getFndXCdgCalcBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codFndCalc, Len.COD_FND_CALC);
        position += Len.COD_FND_CALC;
        MarshalByte.writeDecimal(buffer, position, percFndCalc.copy());
        position += Len.PERC_FND_CALC;
        MarshalByte.writeDecimal(buffer, position, cdgRestCalc.copy());
        return buffer;
    }

    public void initFndXCdgCalcSpaces() {
        codFndCalc = "";
        percFndCalc.setNaN();
        cdgRestCalc.setNaN();
    }

    public void setCodFndCalc(String codFndCalc) {
        this.codFndCalc = Functions.subString(codFndCalc, Len.COD_FND_CALC);
    }

    public String getCodFndCalc() {
        return this.codFndCalc;
    }

    public void setPercFndCalc(AfDecimal percFndCalc) {
        this.percFndCalc.assign(percFndCalc);
    }

    public AfDecimal getPercFndCalc() {
        return this.percFndCalc.copy();
    }

    public void setCdgRestCalc(AfDecimal cdgRestCalc) {
        this.cdgRestCalc.assign(cdgRestCalc);
    }

    public AfDecimal getCdgRestCalc() {
        return this.cdgRestCalc.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_FND_CALC = 20;
        public static final int PERC_FND_CALC = 10;
        public static final int CDG_REST_CALC = 18;
        public static final int FND_X_CDG_CALC = COD_FND_CALC + PERC_FND_CALC + CDG_REST_CALC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PERC_FND_CALC = 3;
            public static final int CDG_REST_CALC = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PERC_FND_CALC = 7;
            public static final int CDG_REST_CALC = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
