package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: C214-MODALITA-ID-TIT<br>
 * Variable: C214-MODALITA-ID-TIT from copybook IVVC0213<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class C214ModalitaIdTit {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char FITTZIO = 'F';
    public static final char AUTENTICO = 'A';

    //==== METHODS ====
    public void setModalitaIdTit(char modalitaIdTit) {
        this.value = modalitaIdTit;
    }

    public char getModalitaIdTit() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MODALITA_ID_TIT = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
