package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-DATA-OUTPUT-V<br>
 * Variable: WK-DATA-OUTPUT-V from program LVVS0000<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WkDataOutputV {

    //==== PROPERTIES ====
    //Original name: WK-NUMERICI
    private String numerici = DefaultValues.stringVal(Len.NUMERICI);
    //Original name: WK-DECIMALI
    private String decimali = DefaultValues.stringVal(Len.DECIMALI);

    //==== METHODS ====
    public void setWkDataOutput(AfDecimal wkDataOutput) {
        int position = 1;
        byte[] buffer = getWkDataOutputVBytes();
        MarshalByte.writeDecimal(buffer, position, wkDataOutput.copy(), SignType.NO_SIGN);
        setWkDataOutputVBytes(buffer);
    }

    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    public AfDecimal getWkDataOutput() {
        int position = 1;
        return MarshalByte.readDecimal(getWkDataOutputVBytes(), position, Len.Int.WK_DATA_OUTPUT, Len.Fract.WK_DATA_OUTPUT, SignType.NO_SIGN);
    }

    public void setWkDataOutputVBytes(byte[] buffer) {
        setWkDataOutputVBytes(buffer, 1);
    }

    /**Original name: WK-DATA-OUTPUT-V<br>*/
    public byte[] getWkDataOutputVBytes() {
        byte[] buffer = new byte[Len.WK_DATA_OUTPUT_V];
        return getWkDataOutputVBytes(buffer, 1);
    }

    public void setWkDataOutputVBytes(byte[] buffer, int offset) {
        int position = offset;
        numerici = MarshalByte.readFixedString(buffer, position, Len.NUMERICI);
        position += Len.NUMERICI;
        decimali = MarshalByte.readFixedString(buffer, position, Len.DECIMALI);
    }

    public byte[] getWkDataOutputVBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, numerici, Len.NUMERICI);
        position += Len.NUMERICI;
        MarshalByte.writeString(buffer, position, decimali, Len.DECIMALI);
        return buffer;
    }

    public void setNumericiFormatted(String numerici) {
        this.numerici = Trunc.toUnsignedNumeric(numerici, Len.NUMERICI);
    }

    public short getNumerici() {
        return NumericDisplay.asShort(this.numerici);
    }

    public void setDecimali(int decimali) {
        this.decimali = NumericDisplay.asString(decimali, Len.DECIMALI);
    }

    public void setDecimaliFormatted(String decimali) {
        this.decimali = Trunc.toUnsignedNumeric(decimali, Len.DECIMALI);
    }

    public int getDecimali() {
        return NumericDisplay.asInt(this.decimali);
    }

    public String getDecimaliFormatted() {
        return this.decimali;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NUMERICI = 4;
        public static final int DECIMALI = 7;
        public static final int WK_DATA_OUTPUT_V = NUMERICI + DECIMALI;
        public static final int WK_DATA_OUTPUT = 11;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_DATA_OUTPUT = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WK_DATA_OUTPUT = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
