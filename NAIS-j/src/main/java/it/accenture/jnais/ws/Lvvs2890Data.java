package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Ldbv1301;
import it.accenture.jnais.ws.occurs.WranTabRappAnag;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS2890<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs2890Data {

    //==== PROPERTIES ====
    public static final int DRAN_TAB_RAPP_ANAG_MAXOCCURS = 100;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS2890";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: AREA-RECUPERO-ANAGRAFICA
    private AreaRecuperoAnagrafica areaRecuperoAnagrafica = new AreaRecuperoAnagrafica();
    //Original name: AREA-CALL-IWFS0050
    private AreaCallIwfs0050 areaCallIwfs0050 = new AreaCallIwfs0050();
    //Original name: DRAN-ELE-RAN-MAX
    private short dranEleRanMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DRAN-TAB-RAPP-ANAG
    private WranTabRappAnag[] dranTabRappAnag = new WranTabRappAnag[DRAN_TAB_RAPP_ANAG_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: LDBV1301
    private Ldbv1301 ldbv1301 = new Ldbv1301();
    //Original name: PERS
    private Pers pers = new Pers();

    //==== CONSTRUCTORS ====
    public Lvvs2890Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dranTabRappAnagIdx = 1; dranTabRappAnagIdx <= DRAN_TAB_RAPP_ANAG_MAXOCCURS; dranTabRappAnagIdx++) {
            dranTabRappAnag[dranTabRappAnagIdx - 1] = new WranTabRappAnag();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getWkPgmFormatted() {
        return Functions.padBlanks(getWkPgm(), Len.WK_PGM);
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setDranAreaRanFormatted(String data) {
        byte[] buffer = new byte[Len.DRAN_AREA_RAN];
        MarshalByte.writeString(buffer, 1, data, Len.DRAN_AREA_RAN);
        setDranAreaRanBytes(buffer, 1);
    }

    public void setDranAreaRanBytes(byte[] buffer, int offset) {
        int position = offset;
        dranEleRanMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DRAN_TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dranTabRappAnag[idx - 1].setWranTabRappAnagBytes(buffer, position);
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
            else {
                dranTabRappAnag[idx - 1].initWranTabRappAnagSpaces();
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
        }
    }

    public void setDranEleRanMax(short dranEleRanMax) {
        this.dranEleRanMax = dranEleRanMax;
    }

    public short getDranEleRanMax() {
        return this.dranEleRanMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public AreaCallIwfs0050 getAreaCallIwfs0050() {
        return areaCallIwfs0050;
    }

    public AreaRecuperoAnagrafica getAreaRecuperoAnagrafica() {
        return areaRecuperoAnagrafica;
    }

    public WranTabRappAnag getDranTabRappAnag(int idx) {
        return dranTabRappAnag[idx - 1];
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Ldbv1301 getLdbv1301() {
        return ldbv1301;
    }

    public Pers getPers() {
        return pers;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DRAN_ELE_RAN_MAX = 2;
        public static final int DRAN_AREA_RAN = DRAN_ELE_RAN_MAX + Lvvs2890Data.DRAN_TAB_RAPP_ANAG_MAXOCCURS * WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
        public static final int WK_CALL_PGM = 8;
        public static final int WK_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
