package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PRE-RIASTO-ECC<br>
 * Variable: W-B03-PRE-RIASTO-ECC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03PreRiastoEccLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03PreRiastoEccLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PRE_RIASTO_ECC;
    }

    public void setwB03PreRiastoEcc(AfDecimal wB03PreRiastoEcc) {
        writeDecimalAsPacked(Pos.W_B03_PRE_RIASTO_ECC, wB03PreRiastoEcc.copy());
    }

    /**Original name: W-B03-PRE-RIASTO-ECC<br>*/
    public AfDecimal getwB03PreRiastoEcc() {
        return readPackedAsDecimal(Pos.W_B03_PRE_RIASTO_ECC, Len.Int.W_B03_PRE_RIASTO_ECC, Len.Fract.W_B03_PRE_RIASTO_ECC);
    }

    public byte[] getwB03PreRiastoEccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PRE_RIASTO_ECC, Pos.W_B03_PRE_RIASTO_ECC);
        return buffer;
    }

    public void setwB03PreRiastoEccNull(String wB03PreRiastoEccNull) {
        writeString(Pos.W_B03_PRE_RIASTO_ECC_NULL, wB03PreRiastoEccNull, Len.W_B03_PRE_RIASTO_ECC_NULL);
    }

    /**Original name: W-B03-PRE-RIASTO-ECC-NULL<br>*/
    public String getwB03PreRiastoEccNull() {
        return readString(Pos.W_B03_PRE_RIASTO_ECC_NULL, Len.W_B03_PRE_RIASTO_ECC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_RIASTO_ECC = 1;
        public static final int W_B03_PRE_RIASTO_ECC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_RIASTO_ECC = 8;
        public static final int W_B03_PRE_RIASTO_ECC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_RIASTO_ECC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_RIASTO_ECC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
