package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: BPA-NUM-ROW-SCHEDULE<br>
 * Variable: BPA-NUM-ROW-SCHEDULE from program IABS0130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BpaNumRowSchedule extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BpaNumRowSchedule() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BPA_NUM_ROW_SCHEDULE;
    }

    public void setBpaNumRowSchedule(int bpaNumRowSchedule) {
        writeIntAsPacked(Pos.BPA_NUM_ROW_SCHEDULE, bpaNumRowSchedule, Len.Int.BPA_NUM_ROW_SCHEDULE);
    }

    public void setBpaNumRowScheduleFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BPA_NUM_ROW_SCHEDULE, Pos.BPA_NUM_ROW_SCHEDULE);
    }

    /**Original name: BPA-NUM-ROW-SCHEDULE<br>*/
    public int getBpaNumRowSchedule() {
        return readPackedAsInt(Pos.BPA_NUM_ROW_SCHEDULE, Len.Int.BPA_NUM_ROW_SCHEDULE);
    }

    public byte[] getBpaNumRowScheduleAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BPA_NUM_ROW_SCHEDULE, Pos.BPA_NUM_ROW_SCHEDULE);
        return buffer;
    }

    public void initBpaNumRowScheduleHighValues() {
        fill(Pos.BPA_NUM_ROW_SCHEDULE, Len.BPA_NUM_ROW_SCHEDULE, Types.HIGH_CHAR_VAL);
    }

    public void setBpaNumRowScheduleNull(String bpaNumRowScheduleNull) {
        writeString(Pos.BPA_NUM_ROW_SCHEDULE_NULL, bpaNumRowScheduleNull, Len.BPA_NUM_ROW_SCHEDULE_NULL);
    }

    /**Original name: BPA-NUM-ROW-SCHEDULE-NULL<br>*/
    public String getBpaNumRowScheduleNull() {
        return readString(Pos.BPA_NUM_ROW_SCHEDULE_NULL, Len.BPA_NUM_ROW_SCHEDULE_NULL);
    }

    public String getBpaNumRowScheduleNullFormatted() {
        return Functions.padBlanks(getBpaNumRowScheduleNull(), Len.BPA_NUM_ROW_SCHEDULE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BPA_NUM_ROW_SCHEDULE = 1;
        public static final int BPA_NUM_ROW_SCHEDULE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BPA_NUM_ROW_SCHEDULE = 5;
        public static final int BPA_NUM_ROW_SCHEDULE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BPA_NUM_ROW_SCHEDULE = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
