package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Lccc0490PresenzaRateo;
import it.accenture.jnais.ws.enums.Lccc0490TitoliEmessi;
import it.accenture.jnais.ws.occurs.Lccc0490TabGrz;

/**Original name: LCCC0490<br>
 * Variable: LCCC0490 from copybook LCCC0490<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Lccc0490 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_GRZ_MAXOCCURS = 20;
    /**Original name: LCCC0490-ID-POLI<br>
	 * <pre>      Identificativo polizza</pre>*/
    private String idPoli = DefaultValues.stringVal(Len.ID_POLI);
    /**Original name: LCCC0490-DT-DECOR-POLI<br>
	 * <pre>      Data decorrenza della polizza</pre>*/
    private String dtDecorPoli = DefaultValues.stringVal(Len.DT_DECOR_POLI);
    //Original name: LCCC0490-TAB-GRZ
    private Lccc0490TabGrz[] tabGrz = new Lccc0490TabGrz[TAB_GRZ_MAXOCCURS];
    /**Original name: LCCC0490-RATEO<br>
	 * <pre>      Rateo</pre>*/
    private String rateo = DefaultValues.stringVal(Len.RATEO);
    /**Original name: LCCC0490-DT-FINE-COP<br>
	 * <pre>      Data fine copertura</pre>*/
    private String dtFineCop = DefaultValues.stringVal(Len.DT_FINE_COP);
    /**Original name: LCCC0490-PRESENZA-RATEO<br>
	 * <pre>      Flag presenza rateo</pre>*/
    private Lccc0490PresenzaRateo presenzaRateo = new Lccc0490PresenzaRateo();
    /**Original name: LCCC0490-TITOLI-EMESSI<br>
	 * <pre>      Flag presenza titoli emessi da stornare</pre>*/
    private Lccc0490TitoliEmessi titoliEmessi = new Lccc0490TitoliEmessi();

    //==== CONSTRUCTORS ====
    public Lccc0490() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0490;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLccc0490Bytes(buf);
    }

    public void init() {
        for (int tabGrzIdx = 1; tabGrzIdx <= TAB_GRZ_MAXOCCURS; tabGrzIdx++) {
            tabGrz[tabGrzIdx - 1] = new Lccc0490TabGrz();
        }
    }

    public String getLccc0490Formatted() {
        return MarshalByteExt.bufferToStr(getLccc0490Bytes());
    }

    public void setLccc0490Bytes(byte[] buffer) {
        setLccc0490Bytes(buffer, 1);
    }

    public byte[] getLccc0490Bytes() {
        byte[] buffer = new byte[Len.LCCC0490];
        return getLccc0490Bytes(buffer, 1);
    }

    public void setLccc0490Bytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaInputBytes(buffer, position);
        position += Len.AREA_INPUT;
        setAreaOutputBytes(buffer, position);
    }

    public byte[] getLccc0490Bytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaInputBytes(buffer, position);
        position += Len.AREA_INPUT;
        getAreaOutputBytes(buffer, position);
        return buffer;
    }

    public void setAreaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readFixedString(buffer, position, Len.ID_POLI);
        position += Len.ID_POLI;
        dtDecorPoli = MarshalByte.readFixedString(buffer, position, Len.DT_DECOR_POLI);
        position += Len.DT_DECOR_POLI;
        for (int idx = 1; idx <= TAB_GRZ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabGrz[idx - 1].setTabGrzBytes(buffer, position);
                position += Lccc0490TabGrz.Len.TAB_GRZ;
            }
            else {
                tabGrz[idx - 1].initTabGrzSpaces();
                position += Lccc0490TabGrz.Len.TAB_GRZ;
            }
        }
    }

    public byte[] getAreaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, idPoli, Len.ID_POLI);
        position += Len.ID_POLI;
        MarshalByte.writeString(buffer, position, dtDecorPoli, Len.DT_DECOR_POLI);
        position += Len.DT_DECOR_POLI;
        for (int idx = 1; idx <= TAB_GRZ_MAXOCCURS; idx++) {
            tabGrz[idx - 1].getTabGrzBytes(buffer, position);
            position += Lccc0490TabGrz.Len.TAB_GRZ;
        }
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = NumericDisplay.asString(idPoli, Len.ID_POLI);
    }

    public void setIdPoliFormatted(String idPoli) {
        this.idPoli = Trunc.toUnsignedNumeric(idPoli, Len.ID_POLI);
    }

    public int getIdPoli() {
        return NumericDisplay.asInt(this.idPoli);
    }

    public String getIdPoliFormatted() {
        return this.idPoli;
    }

    public String getIdPoliAsString() {
        return getIdPoliFormatted();
    }

    public void setDtDecorPoli(int dtDecorPoli) {
        this.dtDecorPoli = NumericDisplay.asString(dtDecorPoli, Len.DT_DECOR_POLI);
    }

    public void setDtDecorPoliFormatted(String dtDecorPoli) {
        this.dtDecorPoli = Trunc.toUnsignedNumeric(dtDecorPoli, Len.DT_DECOR_POLI);
    }

    public int getDtDecorPoli() {
        return NumericDisplay.asInt(this.dtDecorPoli);
    }

    public String getDtDecorPoliFormatted() {
        return this.dtDecorPoli;
    }

    public String getDtDecorPoliAsString() {
        return getDtDecorPoliFormatted();
    }

    public void setAreaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        rateo = MarshalByte.readFixedString(buffer, position, Len.RATEO);
        position += Len.RATEO;
        dtFineCop = MarshalByte.readFixedString(buffer, position, Len.DT_FINE_COP);
        position += Len.DT_FINE_COP;
        presenzaRateo.setPresenzaRateo(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        titoliEmessi.setTitoliEmessi(MarshalByte.readChar(buffer, position));
    }

    public byte[] getAreaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, rateo, Len.RATEO);
        position += Len.RATEO;
        MarshalByte.writeString(buffer, position, dtFineCop, Len.DT_FINE_COP);
        position += Len.DT_FINE_COP;
        MarshalByte.writeChar(buffer, position, presenzaRateo.getPresenzaRateo());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, titoliEmessi.getTitoliEmessi());
        return buffer;
    }

    public void setRateo(int rateo) {
        this.rateo = NumericDisplay.asString(rateo, Len.RATEO);
    }

    public void setRateoFormatted(String rateo) {
        this.rateo = Trunc.toUnsignedNumeric(rateo, Len.RATEO);
    }

    public int getRateo() {
        return NumericDisplay.asInt(this.rateo);
    }

    public void setDtFineCop(int dtFineCop) {
        this.dtFineCop = NumericDisplay.asString(dtFineCop, Len.DT_FINE_COP);
    }

    public void setDtFineCopFormatted(String dtFineCop) {
        this.dtFineCop = Trunc.toUnsignedNumeric(dtFineCop, Len.DT_FINE_COP);
    }

    public int getDtFineCop() {
        return NumericDisplay.asInt(this.dtFineCop);
    }

    public String getDtFineCopFormatted() {
        return this.dtFineCop;
    }

    public Lccc0490PresenzaRateo getPresenzaRateo() {
        return presenzaRateo;
    }

    public Lccc0490TabGrz getTabGrz(int idx) {
        return tabGrz[idx - 1];
    }

    public Lccc0490TitoliEmessi getTitoliEmessi() {
        return titoliEmessi;
    }

    @Override
    public byte[] serialize() {
        return getLccc0490Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POLI = 9;
        public static final int DT_DECOR_POLI = 8;
        public static final int AREA_INPUT = ID_POLI + DT_DECOR_POLI + Lccc0490.TAB_GRZ_MAXOCCURS * Lccc0490TabGrz.Len.TAB_GRZ;
        public static final int RATEO = 9;
        public static final int DT_FINE_COP = 8;
        public static final int AREA_OUTPUT = RATEO + DT_FINE_COP + Lccc0490PresenzaRateo.Len.PRESENZA_RATEO + Lccc0490TitoliEmessi.Len.TITOLI_EMESSI;
        public static final int LCCC0490 = AREA_INPUT + AREA_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
