package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDAD-IMP-AZ<br>
 * Variable: WDAD-IMP-AZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_IMP_AZ;
    }

    public void setWdadImpAz(AfDecimal wdadImpAz) {
        writeDecimalAsPacked(Pos.WDAD_IMP_AZ, wdadImpAz.copy());
    }

    public void setWdadImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_IMP_AZ, Pos.WDAD_IMP_AZ);
    }

    /**Original name: WDAD-IMP-AZ<br>*/
    public AfDecimal getWdadImpAz() {
        return readPackedAsDecimal(Pos.WDAD_IMP_AZ, Len.Int.WDAD_IMP_AZ, Len.Fract.WDAD_IMP_AZ);
    }

    public byte[] getWdadImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_IMP_AZ, Pos.WDAD_IMP_AZ);
        return buffer;
    }

    public void setWdadImpAzNull(String wdadImpAzNull) {
        writeString(Pos.WDAD_IMP_AZ_NULL, wdadImpAzNull, Len.WDAD_IMP_AZ_NULL);
    }

    /**Original name: WDAD-IMP-AZ-NULL<br>*/
    public String getWdadImpAzNull() {
        return readString(Pos.WDAD_IMP_AZ_NULL, Len.WDAD_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_AZ = 1;
        public static final int WDAD_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_AZ = 8;
        public static final int WDAD_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
