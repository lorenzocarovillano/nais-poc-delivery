package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMPB-VIS<br>
 * Variable: LQU-TOT-IMPB-VIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpbVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpbVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMPB_VIS;
    }

    public void setLquTotImpbVis(AfDecimal lquTotImpbVis) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMPB_VIS, lquTotImpbVis.copy());
    }

    public void setLquTotImpbVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMPB_VIS, Pos.LQU_TOT_IMPB_VIS);
    }

    /**Original name: LQU-TOT-IMPB-VIS<br>*/
    public AfDecimal getLquTotImpbVis() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMPB_VIS, Len.Int.LQU_TOT_IMPB_VIS, Len.Fract.LQU_TOT_IMPB_VIS);
    }

    public byte[] getLquTotImpbVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMPB_VIS, Pos.LQU_TOT_IMPB_VIS);
        return buffer;
    }

    public void setLquTotImpbVisNull(String lquTotImpbVisNull) {
        writeString(Pos.LQU_TOT_IMPB_VIS_NULL, lquTotImpbVisNull, Len.LQU_TOT_IMPB_VIS_NULL);
    }

    /**Original name: LQU-TOT-IMPB-VIS-NULL<br>*/
    public String getLquTotImpbVisNull() {
        return readString(Pos.LQU_TOT_IMPB_VIS_NULL, Len.LQU_TOT_IMPB_VIS_NULL);
    }

    public String getLquTotImpbVisNullFormatted() {
        return Functions.padBlanks(getLquTotImpbVisNull(), Len.LQU_TOT_IMPB_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMPB_VIS = 1;
        public static final int LQU_TOT_IMPB_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMPB_VIS = 8;
        public static final int LQU_TOT_IMPB_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMPB_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMPB_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
