package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Idsv0303;
import it.accenture.jnais.copy.Ivvc0212;
import it.accenture.jnais.copy.Ivvc0216Lves0269;
import it.accenture.jnais.copy.Ivvc0218Loas0310;
import it.accenture.jnais.copy.Lccc0006;
import it.accenture.jnais.copy.Lccvdfa1Lves0269;
import it.accenture.jnais.copy.Lccvmov1Lves0269;
import it.accenture.jnais.copy.Ldbi0731;
import it.accenture.jnais.copy.Ldbo07312;
import it.accenture.jnais.copy.ParamComp;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.copy.WdfaDati;
import it.accenture.jnais.copy.WkIspcMax;
import it.accenture.jnais.copy.WmovDati;
import it.accenture.jnais.ws.enums.Ispv0000TipologiaFondo;
import it.accenture.jnais.ws.enums.WkCodSovrap;
import it.accenture.jnais.ws.enums.WkFlagGar;
import it.accenture.jnais.ws.enums.WkFlagRatalo;
import it.accenture.jnais.ws.enums.WkFlg;
import it.accenture.jnais.ws.enums.WkFlgTrch;
import it.accenture.jnais.ws.enums.WkQuestsan;
import it.accenture.jnais.ws.enums.WkRicGrz;
import it.accenture.jnais.ws.enums.WkRicSpg;
import it.accenture.jnais.ws.enums.WkRicTga;
import it.accenture.jnais.ws.enums.WkVisitaMedica;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsCompagnia;
import it.accenture.jnais.ws.enums.WsFlagFineEle;
import it.accenture.jnais.ws.enums.WsFlagRicerca;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTpRappAna;
import it.accenture.jnais.ws.enums.WsTpStatBus;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;
import it.accenture.jnais.ws.occurs.WbepTabBeneficiari;
import it.accenture.jnais.ws.occurs.WdeqTabDettQuest;
import it.accenture.jnais.ws.occurs.WgrzTabGar;
import it.accenture.jnais.ws.occurs.WocoTabOggColl;
import it.accenture.jnais.ws.occurs.WpmoTabParamMov;
import it.accenture.jnais.ws.occurs.WpogTabParamOgg;
import it.accenture.jnais.ws.occurs.WqueTabQuest;
import it.accenture.jnais.ws.occurs.WranTabRappAnag;
import it.accenture.jnais.ws.occurs.WsdiTabStraInv;
import it.accenture.jnais.ws.occurs.WspgTabSpg;
import it.accenture.jnais.ws.occurs.WtgaTabTran;
import it.accenture.jnais.ws.redefines.WskdTabValP;
import it.accenture.jnais.ws.redefines.WskdTabValT;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVES0269<br>
 * Generated as a class for rule WS.<br>*/
public class Lves0269Data {

    //==== PROPERTIES ====
    public static final int VRAN_TAB_RAPP_ANAG_MAXOCCURS = 100;
    public static final int VGRZ_TAB_GAR_MAXOCCURS = 20;
    public static final int VTGA_TAB_TRAN_MAXOCCURS = 20;
    public static final int VSPG_TAB_SPG_MAXOCCURS = 20;
    public static final int VSDI_TAB_STRA_INV_MAXOCCURS = 20;
    public static final int VPMO_TAB_PARAM_MOV_MAXOCCURS = 100;
    public static final int VPOG_TAB_PARAM_OGG_MAXOCCURS = 200;
    public static final int VBEP_TAB_BENEFICIARI_MAXOCCURS = 20;
    public static final int VQUE_TAB_QUEST_MAXOCCURS = 12;
    public static final int VDEQ_TAB_DETT_QUEST_MAXOCCURS = 180;
    public static final int VOCO_TAB_OGG_COLLG_MAXOCCURS = 60;
    public static final int VS_GRZ_TAB_GAR_MAXOCCURS = 20;
    public static final int VS_SPG_TAB_SPG_MAXOCCURS = 20;
    public static final int VS_TGA_TAB_TRAN_MAXOCCURS = 1250;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVES0269";
    //Original name: WK-TABELLA
    private String wkTabella = "";
    //Original name: IDSS0300
    private String idss0300 = "IDSS0300";
    //Original name: AREA-IO-LCCS0033
    private AreaIoLccs0033 areaIoLccs0033 = new AreaIoLccs0033();
    //Original name: LCCC0490
    private Lccc0490 lccc0490 = new Lccc0490();
    //Original name: WS-TP-DATO
    private WsTpDatoLves0269 wsTpDato = new WsTpDatoLves0269();
    //Original name: WS-COMPAGNIA
    private WsCompagnia wsCompagnia = new WsCompagnia();
    //Original name: WS-LIV-PROD
    private char wsLivProd = 'P';
    //Original name: WS-TP-GAR
    private short wsTpGar = DefaultValues.SHORT_VAL;
    //Original name: WS-TP-TRCH
    private String wsTpTrch = DefaultValues.stringVal(Len.WS_TP_TRCH);
    //Original name: WK-SOPR-GAR-ELE-MAX
    private short wkSoprGarEleMax = ((short)20);
    //Original name: WK-ASSET-ELE-MAX
    private short wkAssetEleMax = ((short)250);
    //Original name: WK-ELE-MAX-TIT-CONT
    private short wkEleMaxTitCont = ((short)12);
    //Original name: WK-ELE-MAX-TRCH-DI-GAR
    private short wkEleMaxTrchDiGar = ((short)2);
    //Original name: WK-ELE-MAX-PARAM-D-CALC
    private short wkEleMaxParamDCalc = ((short)20);
    //Original name: WK-ELE-MAX-TIT-RAT
    private short wkEleMaxTitRat = ((short)12);
    //Original name: WK-VARIABILI
    private WkVariabiliLves0269 wkVariabili = new WkVariabiliLves0269();
    /**Original name: WS-FLAG-RICERCA<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG/SWITCH
	 * ----------------------------------------------------------------*</pre>*/
    private WsFlagRicerca wsFlagRicerca = new WsFlagRicerca();
    //Original name: WS-FLAG-FINE-ELE
    private WsFlagFineEle wsFlagFineEle = new WsFlagFineEle();
    //Original name: WK-RIC-GRZ
    private WkRicGrz wkRicGrz = new WkRicGrz();
    //Original name: WK-RIC-TGA
    private WkRicTga wkRicTga = new WkRicTga();
    //Original name: WK-RIC-SPG
    private WkRicSpg wkRicSpg = new WkRicSpg();
    //Original name: WK-FLG
    private WkFlg wkFlg = new WkFlg();
    //Original name: WK-FLG-TRCH
    private WkFlgTrch wkFlgTrch = new WkFlgTrch();
    //Original name: WK-QUESTSAN
    private WkQuestsan wkQuestsan = new WkQuestsan();
    //Original name: WK-VISITA-MEDICA
    private WkVisitaMedica wkVisitaMedica = new WkVisitaMedica();
    //Original name: WK-QUIETANZAMENTO
    private boolean wkQuietanzamento = false;
    //Original name: WK-GETRA
    private boolean wkGetra = false;
    //Original name: WK-COD-SOVRAP
    private WkCodSovrap wkCodSovrap = new WkCodSovrap();
    //Original name: WK-TP-FONDO
    private Ispv0000TipologiaFondo wkTpFondo = new Ispv0000TipologiaFondo();
    //Original name: WK-FLAG-RATALO
    private WkFlagRatalo wkFlagRatalo = new WkFlagRatalo();
    //Original name: WK-FLAG-GAR
    private WkFlagGar wkFlagGar = new WkFlagGar();
    //Original name: IX-INDICI
    private IxIndiciLves0269 ixIndici = new IxIndiciLves0269();
    //Original name: VRAN-ELE-RAPP-ANAG-MAX
    private short vranEleRappAnagMax = ((short)0);
    //Original name: VRAN-TAB-RAPP-ANAG
    private WranTabRappAnag[] vranTabRappAnag = new WranTabRappAnag[VRAN_TAB_RAPP_ANAG_MAXOCCURS];
    //Original name: LCCVDFA1
    private Lccvdfa1Lves0269 lccvdfa1 = new Lccvdfa1Lves0269();
    //Original name: VGRZ-ELE-GARANZIA-MAX
    private short vgrzEleGaranziaMax = ((short)0);
    //Original name: VGRZ-TAB-GAR
    private WgrzTabGar[] vgrzTabGar = new WgrzTabGar[VGRZ_TAB_GAR_MAXOCCURS];
    //Original name: VTGA-ELE-TRAN-MAX
    private short vtgaEleTranMax = ((short)0);
    //Original name: VTGA-TAB-TRAN
    private WtgaTabTran[] vtgaTabTran = new WtgaTabTran[VTGA_TAB_TRAN_MAXOCCURS];
    //Original name: VSPG-ELE-SOPR-GAR-MAX
    private short vspgEleSoprGarMax = ((short)0);
    //Original name: VSPG-TAB-SPG
    private WspgTabSpg[] vspgTabSpg = new WspgTabSpg[VSPG_TAB_SPG_MAXOCCURS];
    //Original name: VSDI-ELE-STRA-INV-MAX
    private short vsdiEleStraInvMax = ((short)0);
    //Original name: VSDI-TAB-STRA-INV
    private WsdiTabStraInv[] vsdiTabStraInv = new WsdiTabStraInv[VSDI_TAB_STRA_INV_MAXOCCURS];
    //Original name: VPMO-ELE-PARAM-MOV-MAX
    private short vpmoEleParamMovMax = ((short)0);
    //Original name: VPMO-TAB-PARAM-MOV
    private WpmoTabParamMov[] vpmoTabParamMov = new WpmoTabParamMov[VPMO_TAB_PARAM_MOV_MAXOCCURS];
    //Original name: VPOG-ELE-PARAM-OGG-MAX
    private short vpogEleParamOggMax = ((short)0);
    //Original name: VPOG-TAB-PARAM-OGG
    private WpogTabParamOgg[] vpogTabParamOgg = new WpogTabParamOgg[VPOG_TAB_PARAM_OGG_MAXOCCURS];
    //Original name: VBEP-ELE-BENEFICIARI
    private short vbepEleBeneficiari = ((short)0);
    //Original name: VBEP-TAB-BENEFICIARI
    private WbepTabBeneficiari[] vbepTabBeneficiari = new WbepTabBeneficiari[VBEP_TAB_BENEFICIARI_MAXOCCURS];
    //Original name: VQUE-ELE-QUEST-MAX
    private short vqueEleQuestMax = ((short)0);
    //Original name: VQUE-TAB-QUEST
    private WqueTabQuest[] vqueTabQuest = new WqueTabQuest[VQUE_TAB_QUEST_MAXOCCURS];
    //Original name: VDEQ-ELE-DETT-QUEST-MAX
    private short vdeqEleDettQuestMax = ((short)0);
    //Original name: VDEQ-TAB-DETT-QUEST
    private WdeqTabDettQuest[] vdeqTabDettQuest = new WdeqTabDettQuest[VDEQ_TAB_DETT_QUEST_MAXOCCURS];
    //Original name: VOCO-ELE-OGG-COLLG-MAX
    private short vocoEleOggCollgMax = ((short)0);
    //Original name: VOCO-TAB-OGG-COLLG
    private WocoTabOggColl[] vocoTabOggCollg = new WocoTabOggColl[VOCO_TAB_OGG_COLLG_MAXOCCURS];
    //Original name: VMOV-ELE-MOV-MAX
    private short vmovEleMovMax = ((short)0);
    //Original name: LCCVMOV1
    private Lccvmov1Lves0269 lccvmov1 = new Lccvmov1Lves0269();
    //Original name: VS-GRZ-ELE-GARANZIA-MAX
    private short vsGrzEleGaranziaMax = ((short)0);
    //Original name: VS-GRZ-TAB-GAR
    private WgrzTabGar[] vsGrzTabGar = new WgrzTabGar[VS_GRZ_TAB_GAR_MAXOCCURS];
    //Original name: VS-SPG-ELE-SOPR-GAR-MAX
    private short vsSpgEleSoprGarMax = ((short)0);
    //Original name: VS-SPG-TAB-SPG
    private WspgTabSpg[] vsSpgTabSpg = new WspgTabSpg[VS_SPG_TAB_SPG_MAXOCCURS];
    //Original name: VS-TGA-ELE-TRAN-MAX
    private short vsTgaEleTranMax = ((short)0);
    //Original name: VS-TGA-TAB-TRAN
    private LazyArrayCopy<W1tgaTabTran> vsTgaTabTran = new LazyArrayCopy<W1tgaTabTran>(new W1tgaTabTran(), 1, VS_TGA_TAB_TRAN_MAXOCCURS);
    //Original name: LCCC0006
    private Lccc0006 lccc0006 = new Lccc0006();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    /**Original name: WS-TP-STAT-BUS<br>
	 * <pre>*****************************************************************
	 *     TP_STAT_BUS (Stato Oggetto di Business)
	 * *****************************************************************</pre>*/
    private WsTpStatBus wsTpStatBus = new WsTpStatBus();
    /**Original name: WS-TP-RAPP-ANA<br>
	 * <pre>*****************************************************************
	 *     TP_RAPP_ANA (Tipo Rapporto Anagrafico)
	 * *****************************************************************</pre>*/
    private WsTpRappAna wsTpRappAna = new WsTpRappAna();
    //Original name: WK-SPG-MAX-A
    private short wkSpgMaxA = ((short)20);
    //Original name: AREA-LCCC0062
    private WcomAreaPagina areaLccc0062 = new WcomAreaPagina();
    //Original name: LDBI0731
    private Ldbi0731 ldbi0731 = new Ldbi0731();
    //Original name: LDBO0731
    private Ldbo07312 ldbo07312 = new Ldbo07312();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: LCCC1901-AREA
    private Lccc1901 lccc1901 = new Lccc1901();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IDSV0301
    private Idsv0301Lves0269 idsv0301 = new Idsv0301Lves0269();
    //Original name: IDSV0303
    private Idsv0303 idsv0303 = new Idsv0303();
    //Original name: TRCH-DI-GAR
    private TrchDiGarIvvs0216 trchDiGar = new TrchDiGarIvvs0216();
    //Original name: PARAM-COMP
    private ParamComp paramComp = new ParamComp();
    //Original name: LCCV0021
    private Lccv0021 lccv0021 = new Lccv0021();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: AREA-IO-CALC-CONTR
    private AreaIoIsps0140 areaIoCalcContr = new AreaIoIsps0140();
    //Original name: WK-ISPC-MAX
    private WkIspcMax wkIspcMax = new WkIspcMax();
    //Original name: IVVC0216
    private Ivvc0216Lves0269 ivvc0216 = new Ivvc0216Lves0269();
    //Original name: IVVC0212
    private Ivvc0212 ivvc0212 = new Ivvc0212();
    //Original name: AREA-IO-IVVS0211
    private InputIvvs0211 areaIoIvvs0211 = new InputIvvs0211();
    //Original name: IVVC0218
    private Ivvc0218Loas0310 ivvc0218 = new Ivvc0218Loas0310();
    //Original name: INT-REGISTER1
    private short intRegister1 = DefaultValues.SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lves0269Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int vranTabRappAnagIdx = 1; vranTabRappAnagIdx <= VRAN_TAB_RAPP_ANAG_MAXOCCURS; vranTabRappAnagIdx++) {
            vranTabRappAnag[vranTabRappAnagIdx - 1] = new WranTabRappAnag();
        }
        for (int vgrzTabGarIdx = 1; vgrzTabGarIdx <= VGRZ_TAB_GAR_MAXOCCURS; vgrzTabGarIdx++) {
            vgrzTabGar[vgrzTabGarIdx - 1] = new WgrzTabGar();
        }
        for (int vtgaTabTranIdx = 1; vtgaTabTranIdx <= VTGA_TAB_TRAN_MAXOCCURS; vtgaTabTranIdx++) {
            vtgaTabTran[vtgaTabTranIdx - 1] = new WtgaTabTran();
        }
        for (int vspgTabSpgIdx = 1; vspgTabSpgIdx <= VSPG_TAB_SPG_MAXOCCURS; vspgTabSpgIdx++) {
            vspgTabSpg[vspgTabSpgIdx - 1] = new WspgTabSpg();
        }
        for (int vsdiTabStraInvIdx = 1; vsdiTabStraInvIdx <= VSDI_TAB_STRA_INV_MAXOCCURS; vsdiTabStraInvIdx++) {
            vsdiTabStraInv[vsdiTabStraInvIdx - 1] = new WsdiTabStraInv();
        }
        for (int vpmoTabParamMovIdx = 1; vpmoTabParamMovIdx <= VPMO_TAB_PARAM_MOV_MAXOCCURS; vpmoTabParamMovIdx++) {
            vpmoTabParamMov[vpmoTabParamMovIdx - 1] = new WpmoTabParamMov();
        }
        for (int vpogTabParamOggIdx = 1; vpogTabParamOggIdx <= VPOG_TAB_PARAM_OGG_MAXOCCURS; vpogTabParamOggIdx++) {
            vpogTabParamOgg[vpogTabParamOggIdx - 1] = new WpogTabParamOgg();
        }
        for (int vbepTabBeneficiariIdx = 1; vbepTabBeneficiariIdx <= VBEP_TAB_BENEFICIARI_MAXOCCURS; vbepTabBeneficiariIdx++) {
            vbepTabBeneficiari[vbepTabBeneficiariIdx - 1] = new WbepTabBeneficiari();
        }
        for (int vqueTabQuestIdx = 1; vqueTabQuestIdx <= VQUE_TAB_QUEST_MAXOCCURS; vqueTabQuestIdx++) {
            vqueTabQuest[vqueTabQuestIdx - 1] = new WqueTabQuest();
        }
        for (int vdeqTabDettQuestIdx = 1; vdeqTabDettQuestIdx <= VDEQ_TAB_DETT_QUEST_MAXOCCURS; vdeqTabDettQuestIdx++) {
            vdeqTabDettQuest[vdeqTabDettQuestIdx - 1] = new WdeqTabDettQuest();
        }
        for (int vocoTabOggCollgIdx = 1; vocoTabOggCollgIdx <= VOCO_TAB_OGG_COLLG_MAXOCCURS; vocoTabOggCollgIdx++) {
            vocoTabOggCollg[vocoTabOggCollgIdx - 1] = new WocoTabOggColl();
        }
        for (int vsGrzTabGarIdx = 1; vsGrzTabGarIdx <= VS_GRZ_TAB_GAR_MAXOCCURS; vsGrzTabGarIdx++) {
            vsGrzTabGar[vsGrzTabGarIdx - 1] = new WgrzTabGar();
        }
        for (int vsSpgTabSpgIdx = 1; vsSpgTabSpgIdx <= VS_SPG_TAB_SPG_MAXOCCURS; vsSpgTabSpgIdx++) {
            vsSpgTabSpg[vsSpgTabSpgIdx - 1] = new WspgTabSpg();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public String getIdss0300() {
        return this.idss0300;
    }

    public char getWsLivProd() {
        return this.wsLivProd;
    }

    public void setWsTpGar(short wsTpGar) {
        this.wsTpGar = wsTpGar;
    }

    public short getWsTpGar() {
        return this.wsTpGar;
    }

    public void setWsTpTrch(String wsTpTrch) {
        this.wsTpTrch = Functions.subString(wsTpTrch, Len.WS_TP_TRCH);
    }

    public String getWsTpTrch() {
        return this.wsTpTrch;
    }

    public short getWkSoprGarEleMax() {
        return this.wkSoprGarEleMax;
    }

    public short getWkAssetEleMax() {
        return this.wkAssetEleMax;
    }

    public short getWkEleMaxTitCont() {
        return this.wkEleMaxTitCont;
    }

    public short getWkEleMaxTrchDiGar() {
        return this.wkEleMaxTrchDiGar;
    }

    public short getWkEleMaxParamDCalc() {
        return this.wkEleMaxParamDCalc;
    }

    public short getWkEleMaxTitRat() {
        return this.wkEleMaxTitRat;
    }

    public void setWkQuietanzamento(boolean wkQuietanzamento) {
        this.wkQuietanzamento = wkQuietanzamento;
    }

    public boolean isWkQuietanzamento() {
        return this.wkQuietanzamento;
    }

    public void setWkGetra(boolean wkGetra) {
        this.wkGetra = wkGetra;
    }

    public boolean isWkGetra() {
        return this.wkGetra;
    }

    public String getVranAreaRappAnagFormatted() {
        return MarshalByteExt.bufferToStr(getVranAreaRappAnagBytes());
    }

    /**Original name: VRAN-AREA-RAPP-ANAG<br>
	 * <pre>----------------------------------------------------------------*
	 *     AREE DI APPOGGIO PER IL VALORIZZATORE VARIABILI
	 * ----------------------------------------------------------------*
	 * --> AREA RAPPORTO ANAGRAFICO</pre>*/
    public byte[] getVranAreaRappAnagBytes() {
        byte[] buffer = new byte[Len.VRAN_AREA_RAPP_ANAG];
        return getVranAreaRappAnagBytes(buffer, 1);
    }

    public byte[] getVranAreaRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vranEleRappAnagMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VRAN_TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            vranTabRappAnag[idx - 1].getWranTabRappAnagBytes(buffer, position);
            position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
        }
        return buffer;
    }

    public void setVranEleRappAnagMax(short vranEleRappAnagMax) {
        this.vranEleRappAnagMax = vranEleRappAnagMax;
    }

    public short getVranEleRappAnagMax() {
        return this.vranEleRappAnagMax;
    }

    public String getVdfaTabFiscAdesFormatted() {
        return MarshalByteExt.bufferToStr(getVdfaTabFiscAdesBytes());
    }

    /**Original name: VDFA-TAB-FISC-ADES<br>*/
    public byte[] getVdfaTabFiscAdesBytes() {
        byte[] buffer = new byte[Len.VDFA_TAB_FISC_ADES];
        return getVdfaTabFiscAdesBytes(buffer, 1);
    }

    public byte[] getVdfaTabFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdfa1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdfa1.getIdPtf(), Lccvdfa1Lves0269.Len.Int.ID_PTF, 0);
        position += Lccvdfa1Lves0269.Len.ID_PTF;
        lccvdfa1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public String getVgrzAreaGaranziaFormatted() {
        return MarshalByteExt.bufferToStr(getVgrzAreaGaranziaBytes());
    }

    public void setVgrzAreaGaranziaBytes(byte[] buffer) {
        setVgrzAreaGaranziaBytes(buffer, 1);
    }

    /**Original name: VGRZ-AREA-GARANZIA<br>
	 * <pre>--> AREA GARANZIA</pre>*/
    public byte[] getVgrzAreaGaranziaBytes() {
        byte[] buffer = new byte[Len.VGRZ_AREA_GARANZIA];
        return getVgrzAreaGaranziaBytes(buffer, 1);
    }

    public void setVgrzAreaGaranziaBytes(byte[] buffer, int offset) {
        int position = offset;
        vgrzEleGaranziaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VGRZ_TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                vgrzTabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                vgrzTabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public byte[] getVgrzAreaGaranziaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vgrzEleGaranziaMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VGRZ_TAB_GAR_MAXOCCURS; idx++) {
            vgrzTabGar[idx - 1].getTabGarBytes(buffer, position);
            position += WgrzTabGar.Len.TAB_GAR;
        }
        return buffer;
    }

    public void setVgrzEleGaranziaMax(short vgrzEleGaranziaMax) {
        this.vgrzEleGaranziaMax = vgrzEleGaranziaMax;
    }

    public short getVgrzEleGaranziaMax() {
        return this.vgrzEleGaranziaMax;
    }

    public String getVtgaAreaTrancheFormatted() {
        return MarshalByteExt.bufferToStr(getVtgaAreaTrancheBytes());
    }

    /**Original name: VTGA-AREA-TRANCHE<br>
	 * <pre>--> AREA TRANCHE DI GARANZIA</pre>*/
    public byte[] getVtgaAreaTrancheBytes() {
        byte[] buffer = new byte[Len.VTGA_AREA_TRANCHE];
        return getVtgaAreaTrancheBytes(buffer, 1);
    }

    public byte[] getVtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vtgaEleTranMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VTGA_TAB_TRAN_MAXOCCURS; idx++) {
            vtgaTabTran[idx - 1].getTabTranBytes(buffer, position);
            position += WtgaTabTran.Len.TAB_TRAN;
        }
        return buffer;
    }

    public void setVtgaEleTranMax(short vtgaEleTranMax) {
        this.vtgaEleTranMax = vtgaEleTranMax;
    }

    public short getVtgaEleTranMax() {
        return this.vtgaEleTranMax;
    }

    public String getVspgAreaSoprGarFormatted() {
        return MarshalByteExt.bufferToStr(getVspgAreaSoprGarBytes());
    }

    public void setVspgAreaSoprGarBytes(byte[] buffer) {
        setVspgAreaSoprGarBytes(buffer, 1);
    }

    /**Original name: VSPG-AREA-SOPR-GAR<br>
	 * <pre>--> AREA SOPRAPREMIO DI GARANZIA</pre>*/
    public byte[] getVspgAreaSoprGarBytes() {
        byte[] buffer = new byte[Len.VSPG_AREA_SOPR_GAR];
        return getVspgAreaSoprGarBytes(buffer, 1);
    }

    public void setVspgAreaSoprGarBytes(byte[] buffer, int offset) {
        int position = offset;
        vspgEleSoprGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VSPG_TAB_SPG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                vspgTabSpg[idx - 1].setWspgTabSpgBytes(buffer, position);
                position += WspgTabSpg.Len.WSPG_TAB_SPG;
            }
            else {
                vspgTabSpg[idx - 1].initWspgTabSpgSpaces();
                position += WspgTabSpg.Len.WSPG_TAB_SPG;
            }
        }
    }

    public byte[] getVspgAreaSoprGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vspgEleSoprGarMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VSPG_TAB_SPG_MAXOCCURS; idx++) {
            vspgTabSpg[idx - 1].getWspgTabSpgBytes(buffer, position);
            position += WspgTabSpg.Len.WSPG_TAB_SPG;
        }
        return buffer;
    }

    public void setVspgEleSoprGarMax(short vspgEleSoprGarMax) {
        this.vspgEleSoprGarMax = vspgEleSoprGarMax;
    }

    public short getVspgEleSoprGarMax() {
        return this.vspgEleSoprGarMax;
    }

    public String getVsdiAreaStraInvFormatted() {
        return MarshalByteExt.bufferToStr(getVsdiAreaStraInvBytes());
    }

    /**Original name: VSDI-AREA-STRA-INV<br>
	 * <pre>--> AREA STRATEGIA DI INVESTIMENTO</pre>*/
    public byte[] getVsdiAreaStraInvBytes() {
        byte[] buffer = new byte[Len.VSDI_AREA_STRA_INV];
        return getVsdiAreaStraInvBytes(buffer, 1);
    }

    public byte[] getVsdiAreaStraInvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vsdiEleStraInvMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VSDI_TAB_STRA_INV_MAXOCCURS; idx++) {
            vsdiTabStraInv[idx - 1].getWsdiTabStraInvBytes(buffer, position);
            position += WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
        }
        return buffer;
    }

    public void setVsdiEleStraInvMax(short vsdiEleStraInvMax) {
        this.vsdiEleStraInvMax = vsdiEleStraInvMax;
    }

    public short getVsdiEleStraInvMax() {
        return this.vsdiEleStraInvMax;
    }

    public String getVpmoAreaParamMovFormatted() {
        return MarshalByteExt.bufferToStr(getVpmoAreaParamMovBytes());
    }

    /**Original name: VPMO-AREA-PARAM-MOV<br>
	 * <pre>--  PARAMETRO MOVIMENTO</pre>*/
    public byte[] getVpmoAreaParamMovBytes() {
        byte[] buffer = new byte[Len.VPMO_AREA_PARAM_MOV];
        return getVpmoAreaParamMovBytes(buffer, 1);
    }

    public byte[] getVpmoAreaParamMovBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vpmoEleParamMovMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VPMO_TAB_PARAM_MOV_MAXOCCURS; idx++) {
            vpmoTabParamMov[idx - 1].getWpmoTabParamMovBytes(buffer, position);
            position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
        }
        return buffer;
    }

    public void setVpmoEleParamMovMax(short vpmoEleParamMovMax) {
        this.vpmoEleParamMovMax = vpmoEleParamMovMax;
    }

    public short getVpmoEleParamMovMax() {
        return this.vpmoEleParamMovMax;
    }

    public String getVpogAreaParamOggFormatted() {
        return MarshalByteExt.bufferToStr(getVpogAreaParamOggBytes());
    }

    /**Original name: VPOG-AREA-PARAM-OGG<br>
	 * <pre>--  PARAMETRO OGGETTO</pre>*/
    public byte[] getVpogAreaParamOggBytes() {
        byte[] buffer = new byte[Len.VPOG_AREA_PARAM_OGG];
        return getVpogAreaParamOggBytes(buffer, 1);
    }

    public byte[] getVpogAreaParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vpogEleParamOggMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VPOG_TAB_PARAM_OGG_MAXOCCURS; idx++) {
            vpogTabParamOgg[idx - 1].getWpogTabParamOggBytes(buffer, position);
            position += WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
        }
        return buffer;
    }

    public void setVpogEleParamOggMax(short vpogEleParamOggMax) {
        this.vpogEleParamOggMax = vpogEleParamOggMax;
    }

    public short getVpogEleParamOggMax() {
        return this.vpogEleParamOggMax;
    }

    public String getVbepAreaBeneficiariFormatted() {
        return MarshalByteExt.bufferToStr(getVbepAreaBeneficiariBytes());
    }

    /**Original name: VBEP-AREA-BENEFICIARI<br>
	 * <pre>--  BENEFICIARI</pre>*/
    public byte[] getVbepAreaBeneficiariBytes() {
        byte[] buffer = new byte[Len.VBEP_AREA_BENEFICIARI];
        return getVbepAreaBeneficiariBytes(buffer, 1);
    }

    public byte[] getVbepAreaBeneficiariBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vbepEleBeneficiari);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VBEP_TAB_BENEFICIARI_MAXOCCURS; idx++) {
            vbepTabBeneficiari[idx - 1].getWbepTabBeneficiariBytes(buffer, position);
            position += WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
        }
        return buffer;
    }

    public void setVbepEleBeneficiari(short vbepEleBeneficiari) {
        this.vbepEleBeneficiari = vbepEleBeneficiari;
    }

    public short getVbepEleBeneficiari() {
        return this.vbepEleBeneficiari;
    }

    public String getVqueAreaQuestFormatted() {
        return MarshalByteExt.bufferToStr(getVqueAreaQuestBytes());
    }

    /**Original name: VQUE-AREA-QUEST<br>
	 * <pre>---- QUESTIONARIO</pre>*/
    public byte[] getVqueAreaQuestBytes() {
        byte[] buffer = new byte[Len.VQUE_AREA_QUEST];
        return getVqueAreaQuestBytes(buffer, 1);
    }

    public byte[] getVqueAreaQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vqueEleQuestMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VQUE_TAB_QUEST_MAXOCCURS; idx++) {
            vqueTabQuest[idx - 1].getWqueTabQuestBytes(buffer, position);
            position += WqueTabQuest.Len.WQUE_TAB_QUEST;
        }
        return buffer;
    }

    public void setVqueEleQuestMax(short vqueEleQuestMax) {
        this.vqueEleQuestMax = vqueEleQuestMax;
    }

    public short getVqueEleQuestMax() {
        return this.vqueEleQuestMax;
    }

    public String getVdeqAreaDettQuestFormatted() {
        return MarshalByteExt.bufferToStr(getVdeqAreaDettQuestBytes());
    }

    /**Original name: VDEQ-AREA-DETT-QUEST<br>
	 * <pre>---- DETTAGLIO QUESTIONARIO</pre>*/
    public byte[] getVdeqAreaDettQuestBytes() {
        byte[] buffer = new byte[Len.VDEQ_AREA_DETT_QUEST];
        return getVdeqAreaDettQuestBytes(buffer, 1);
    }

    public byte[] getVdeqAreaDettQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vdeqEleDettQuestMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VDEQ_TAB_DETT_QUEST_MAXOCCURS; idx++) {
            vdeqTabDettQuest[idx - 1].getWdeqTabDettQuestBytes(buffer, position);
            position += WdeqTabDettQuest.Len.WDEQ_TAB_DETT_QUEST;
        }
        return buffer;
    }

    public void setVdeqEleDettQuestMax(short vdeqEleDettQuestMax) {
        this.vdeqEleDettQuestMax = vdeqEleDettQuestMax;
    }

    public short getVdeqEleDettQuestMax() {
        return this.vdeqEleDettQuestMax;
    }

    public String getVocoAreaOggCollgFormatted() {
        return MarshalByteExt.bufferToStr(getVocoAreaOggCollgBytes());
    }

    /**Original name: VOCO-AREA-OGG-COLLG<br>
	 * <pre>--  OGGETTO COLLEGATO</pre>*/
    public byte[] getVocoAreaOggCollgBytes() {
        byte[] buffer = new byte[Len.VOCO_AREA_OGG_COLLG];
        return getVocoAreaOggCollgBytes(buffer, 1);
    }

    public byte[] getVocoAreaOggCollgBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vocoEleOggCollgMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VOCO_TAB_OGG_COLLG_MAXOCCURS; idx++) {
            vocoTabOggCollg[idx - 1].getWocoTabOggCollBytes(buffer, position);
            position += WocoTabOggColl.Len.WOCO_TAB_OGG_COLL;
        }
        return buffer;
    }

    public void setVocoEleOggCollgMax(short vocoEleOggCollgMax) {
        this.vocoEleOggCollgMax = vocoEleOggCollgMax;
    }

    public short getVocoEleOggCollgMax() {
        return this.vocoEleOggCollgMax;
    }

    public String getVmovAreaMovimentoFormatted() {
        return MarshalByteExt.bufferToStr(getVmovAreaMovimentoBytes());
    }

    /**Original name: VMOV-AREA-MOVIMENTO<br>
	 * <pre>-- MOVIMENTO</pre>*/
    public byte[] getVmovAreaMovimentoBytes() {
        byte[] buffer = new byte[Len.VMOV_AREA_MOVIMENTO];
        return getVmovAreaMovimentoBytes(buffer, 1);
    }

    public byte[] getVmovAreaMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vmovEleMovMax);
        position += Types.SHORT_SIZE;
        getVmovTabMovBytes(buffer, position);
        return buffer;
    }

    public short getVmovEleMovMax() {
        return this.vmovEleMovMax;
    }

    public byte[] getVmovTabMovBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvmov1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvmov1.getIdPtf(), Lccvmov1Lves0269.Len.Int.ID_PTF, 0);
        position += Lccvmov1Lves0269.Len.ID_PTF;
        lccvmov1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    /**Original name: VS-GRZ-AREA-GARANZIA<br>
	 * <pre>--> AREA GARANZIA</pre>*/
    public byte[] getVsGrzAreaGaranziaBytes() {
        byte[] buffer = new byte[Len.VS_GRZ_AREA_GARANZIA];
        return getVsGrzAreaGaranziaBytes(buffer, 1);
    }

    public byte[] getVsGrzAreaGaranziaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vsGrzEleGaranziaMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VS_GRZ_TAB_GAR_MAXOCCURS; idx++) {
            vsGrzTabGar[idx - 1].getTabGarBytes(buffer, position);
            position += WgrzTabGar.Len.TAB_GAR;
        }
        return buffer;
    }

    public void setVsGrzEleGaranziaMax(short vsGrzEleGaranziaMax) {
        this.vsGrzEleGaranziaMax = vsGrzEleGaranziaMax;
    }

    public short getVsGrzEleGaranziaMax() {
        return this.vsGrzEleGaranziaMax;
    }

    /**Original name: VS-SPG-AREA-SOPR-GAR<br>
	 * <pre>--> AREA SOPRAPREMIO DI GARANZIA</pre>*/
    public byte[] getVsSpgAreaSoprGarBytes() {
        byte[] buffer = new byte[Len.VS_SPG_AREA_SOPR_GAR];
        return getVsSpgAreaSoprGarBytes(buffer, 1);
    }

    public byte[] getVsSpgAreaSoprGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vsSpgEleSoprGarMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VS_SPG_TAB_SPG_MAXOCCURS; idx++) {
            vsSpgTabSpg[idx - 1].getWspgTabSpgBytes(buffer, position);
            position += WspgTabSpg.Len.WSPG_TAB_SPG;
        }
        return buffer;
    }

    public void setVsSpgEleSoprGarMax(short vsSpgEleSoprGarMax) {
        this.vsSpgEleSoprGarMax = vsSpgEleSoprGarMax;
    }

    public short getVsSpgEleSoprGarMax() {
        return this.vsSpgEleSoprGarMax;
    }

    public void setVsTgaEleTranMax(short vsTgaEleTranMax) {
        this.vsTgaEleTranMax = vsTgaEleTranMax;
    }

    public short getVsTgaEleTranMax() {
        return this.vsTgaEleTranMax;
    }

    public short getWkSpgMaxA() {
        return this.wkSpgMaxA;
    }

    public void setLdbo07311Formatted(String data) {
        byte[] buffer = new byte[Len.LDBO07311];
        MarshalByte.writeString(buffer, 1, data, Len.LDBO07311);
        setLdbo07311Bytes(buffer, 1);
    }

    public void setLdbo07311Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbo07312.setAreaLdbo0731Bytes(buffer, position);
    }

    public void setWskdAreaSchedaFormatted(String data) {
        byte[] buffer = new byte[Len.WSKD_AREA_SCHEDA];
        MarshalByte.writeString(buffer, 1, data, Len.WSKD_AREA_SCHEDA);
        setWskdAreaSchedaBytes(buffer, 1);
    }

    public void setWskdAreaSchedaBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0216.setWskdDee(MarshalByte.readString(buffer, position, Ivvc0216Lves0269.Len.WSKD_DEE));
        position += Ivvc0216Lves0269.Len.WSKD_DEE;
        ivvc0216.setWskdEleLivelloMaxP(MarshalByte.readPackedAsShort(buffer, position, Ivvc0216Lves0269.Len.Int.WSKD_ELE_LIVELLO_MAX_P, 0));
        position += Ivvc0216Lves0269.Len.WSKD_ELE_LIVELLO_MAX_P;
        ivvc0216.getWskdTabValP().setWskdTabValPBytes(buffer, position);
        position += WskdTabValP.Len.WSKD_TAB_VAL_P;
        ivvc0216.setWskdEleLivelloMaxT(MarshalByte.readPackedAsShort(buffer, position, Ivvc0216Lves0269.Len.Int.WSKD_ELE_LIVELLO_MAX_T, 0));
        position += Ivvc0216Lves0269.Len.WSKD_ELE_LIVELLO_MAX_T;
        ivvc0216.getWskdTabValT().setWskdTabValTBytes(buffer, position);
        position += WskdTabValT.Len.WSKD_TAB_VAL_T;
        ivvc0216.setWskdVarAutOperBytes(buffer, position);
    }

    public void setIntRegister1(short intRegister1) {
        this.intRegister1 = intRegister1;
    }

    public short getIntRegister1() {
        return this.intRegister1;
    }

    public AreaIoIsps0140 getAreaIoCalcContr() {
        return areaIoCalcContr;
    }

    public InputIvvs0211 getAreaIoIvvs0211() {
        return areaIoIvvs0211;
    }

    public AreaIoLccs0033 getAreaIoLccs0033() {
        return areaIoLccs0033;
    }

    public WcomAreaPagina getAreaLccc0062() {
        return areaLccc0062;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Idsv0301Lves0269 getIdsv0301() {
        return idsv0301;
    }

    public Idsv0303 getIdsv0303() {
        return idsv0303;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Ivvc0212 getIvvc0212() {
        return ivvc0212;
    }

    public Ivvc0216Lves0269 getIvvc0216() {
        return ivvc0216;
    }

    public Ivvc0218Loas0310 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndiciLves0269 getIxIndici() {
        return ixIndici;
    }

    public Lccc0006 getLccc0006() {
        return lccc0006;
    }

    public Lccc0490 getLccc0490() {
        return lccc0490;
    }

    public Lccc1901 getLccc1901() {
        return lccc1901;
    }

    public Lccv0021 getLccv0021() {
        return lccv0021;
    }

    public Lccvmov1Lves0269 getLccvmov1() {
        return lccvmov1;
    }

    public Ldbi0731 getLdbi0731() {
        return ldbi0731;
    }

    public Ldbo07312 getLdbo07312() {
        return ldbo07312;
    }

    public ParamComp getParamComp() {
        return paramComp;
    }

    public TrchDiGarIvvs0216 getTrchDiGar() {
        return trchDiGar;
    }

    public WbepTabBeneficiari getVbepTabBeneficiari(int idx) {
        return vbepTabBeneficiari[idx - 1];
    }

    public WdeqTabDettQuest getVdeqTabDettQuest(int idx) {
        return vdeqTabDettQuest[idx - 1];
    }

    public WgrzTabGar getVgrzTabGar(int idx) {
        return vgrzTabGar[idx - 1];
    }

    public WocoTabOggColl getVocoTabOggCollg(int idx) {
        return vocoTabOggCollg[idx - 1];
    }

    public WpmoTabParamMov getVpmoTabParamMov(int idx) {
        return vpmoTabParamMov[idx - 1];
    }

    public WpogTabParamOgg getVpogTabParamOgg(int idx) {
        return vpogTabParamOgg[idx - 1];
    }

    public WqueTabQuest getVqueTabQuest(int idx) {
        return vqueTabQuest[idx - 1];
    }

    public WranTabRappAnag getVranTabRappAnag(int idx) {
        return vranTabRappAnag[idx - 1];
    }

    public WgrzTabGar getVsGrzTabGar(int idx) {
        return vsGrzTabGar[idx - 1];
    }

    public WspgTabSpg getVsSpgTabSpg(int idx) {
        return vsSpgTabSpg[idx - 1];
    }

    public W1tgaTabTran getVsTgaTabTran(int idx) {
        return vsTgaTabTran.get(idx - 1);
    }

    public WsdiTabStraInv getVsdiTabStraInv(int idx) {
        return vsdiTabStraInv[idx - 1];
    }

    public WspgTabSpg getVspgTabSpg(int idx) {
        return vspgTabSpg[idx - 1];
    }

    public WtgaTabTran getVtgaTabTran(int idx) {
        return vtgaTabTran[idx - 1];
    }

    public WkCodSovrap getWkCodSovrap() {
        return wkCodSovrap;
    }

    public WkFlagGar getWkFlagGar() {
        return wkFlagGar;
    }

    public WkFlagRatalo getWkFlagRatalo() {
        return wkFlagRatalo;
    }

    public WkFlg getWkFlg() {
        return wkFlg;
    }

    public WkFlgTrch getWkFlgTrch() {
        return wkFlgTrch;
    }

    public WkIspcMax getWkIspcMax() {
        return wkIspcMax;
    }

    public WkQuestsan getWkQuestsan() {
        return wkQuestsan;
    }

    public WkRicGrz getWkRicGrz() {
        return wkRicGrz;
    }

    public WkRicSpg getWkRicSpg() {
        return wkRicSpg;
    }

    public WkRicTga getWkRicTga() {
        return wkRicTga;
    }

    public Ispv0000TipologiaFondo getWkTpFondo() {
        return wkTpFondo;
    }

    public WkVariabiliLves0269 getWkVariabili() {
        return wkVariabili;
    }

    public WkVisitaMedica getWkVisitaMedica() {
        return wkVisitaMedica;
    }

    public WsCompagnia getWsCompagnia() {
        return wsCompagnia;
    }

    public WsFlagFineEle getWsFlagFineEle() {
        return wsFlagFineEle;
    }

    public WsFlagRicerca getWsFlagRicerca() {
        return wsFlagRicerca;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTpDatoLves0269 getWsTpDato() {
        return wsTpDato;
    }

    public WsTpRappAna getWsTpRappAna() {
        return wsTpRappAna;
    }

    public WsTpStatBus getWsTpStatBus() {
        return wsTpStatBus;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_GAR = 2;
        public static final int WS_TP_TRCH = 2;
        public static final int XX_DIFF = 5;
        public static final int IN_RCODE = 2;
        public static final int WSKD_AREA_SCHEDA = Ivvc0216Lves0269.Len.WSKD_DEE + Ivvc0216Lves0269.Len.WSKD_ELE_LIVELLO_MAX_P + WskdTabValP.Len.WSKD_TAB_VAL_P + Ivvc0216Lves0269.Len.WSKD_ELE_LIVELLO_MAX_T + WskdTabValT.Len.WSKD_TAB_VAL_T + Ivvc0216Lves0269.Len.WSKD_VAR_AUT_OPER;
        public static final int WK_TABELLA = 8;
        public static final int VRAN_ELE_RAPP_ANAG_MAX = 2;
        public static final int VRAN_AREA_RAPP_ANAG = VRAN_ELE_RAPP_ANAG_MAX + Lves0269Data.VRAN_TAB_RAPP_ANAG_MAXOCCURS * WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
        public static final int VDFA_TAB_FISC_ADES = WpolStatus.Len.STATUS + Lccvdfa1Lves0269.Len.ID_PTF + WdfaDati.Len.DATI;
        public static final int VGRZ_ELE_GARANZIA_MAX = 2;
        public static final int VGRZ_AREA_GARANZIA = VGRZ_ELE_GARANZIA_MAX + Lves0269Data.VGRZ_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int VTGA_ELE_TRAN_MAX = 2;
        public static final int VTGA_AREA_TRANCHE = VTGA_ELE_TRAN_MAX + Lves0269Data.VTGA_TAB_TRAN_MAXOCCURS * WtgaTabTran.Len.TAB_TRAN;
        public static final int VSPG_ELE_SOPR_GAR_MAX = 2;
        public static final int VSPG_AREA_SOPR_GAR = VSPG_ELE_SOPR_GAR_MAX + Lves0269Data.VSPG_TAB_SPG_MAXOCCURS * WspgTabSpg.Len.WSPG_TAB_SPG;
        public static final int VSDI_ELE_STRA_INV_MAX = 2;
        public static final int VSDI_AREA_STRA_INV = VSDI_ELE_STRA_INV_MAX + Lves0269Data.VSDI_TAB_STRA_INV_MAXOCCURS * WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
        public static final int VPMO_ELE_PARAM_MOV_MAX = 2;
        public static final int VPMO_AREA_PARAM_MOV = VPMO_ELE_PARAM_MOV_MAX + Lves0269Data.VPMO_TAB_PARAM_MOV_MAXOCCURS * WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
        public static final int VPOG_ELE_PARAM_OGG_MAX = 2;
        public static final int VPOG_AREA_PARAM_OGG = VPOG_ELE_PARAM_OGG_MAX + Lves0269Data.VPOG_TAB_PARAM_OGG_MAXOCCURS * WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
        public static final int VBEP_ELE_BENEFICIARI = 2;
        public static final int VBEP_AREA_BENEFICIARI = VBEP_ELE_BENEFICIARI + Lves0269Data.VBEP_TAB_BENEFICIARI_MAXOCCURS * WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
        public static final int VQUE_ELE_QUEST_MAX = 2;
        public static final int VQUE_AREA_QUEST = VQUE_ELE_QUEST_MAX + Lves0269Data.VQUE_TAB_QUEST_MAXOCCURS * WqueTabQuest.Len.WQUE_TAB_QUEST;
        public static final int VDEQ_ELE_DETT_QUEST_MAX = 2;
        public static final int VDEQ_AREA_DETT_QUEST = VDEQ_ELE_DETT_QUEST_MAX + Lves0269Data.VDEQ_TAB_DETT_QUEST_MAXOCCURS * WdeqTabDettQuest.Len.WDEQ_TAB_DETT_QUEST;
        public static final int VOCO_ELE_OGG_COLLG_MAX = 2;
        public static final int VOCO_AREA_OGG_COLLG = VOCO_ELE_OGG_COLLG_MAX + Lves0269Data.VOCO_TAB_OGG_COLLG_MAXOCCURS * WocoTabOggColl.Len.WOCO_TAB_OGG_COLL;
        public static final int VMOV_ELE_MOV_MAX = 2;
        public static final int VMOV_TAB_MOV = WpolStatus.Len.STATUS + Lccvmov1Lves0269.Len.ID_PTF + WmovDati.Len.DATI;
        public static final int VMOV_AREA_MOVIMENTO = VMOV_ELE_MOV_MAX + VMOV_TAB_MOV;
        public static final int VS_GRZ_ELE_GARANZIA_MAX = 2;
        public static final int VS_GRZ_AREA_GARANZIA = VS_GRZ_ELE_GARANZIA_MAX + Lves0269Data.VS_GRZ_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int VS_SPG_ELE_SOPR_GAR_MAX = 2;
        public static final int VS_SPG_AREA_SOPR_GAR = VS_SPG_ELE_SOPR_GAR_MAX + Lves0269Data.VS_SPG_TAB_SPG_MAXOCCURS * WspgTabSpg.Len.WSPG_TAB_SPG;
        public static final int LDBO07311 = Ldbo07312.Len.AREA_LDBO0731;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
