package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.NumAdeGarTra;
import it.accenture.jnais.ws.enums.WsOggettoInPtf;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0070<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0070Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LCCS0070";
    //Original name: WK-TABELLA
    private String wkTabella = "";
    //Original name: WS-NUM-APPO
    private String wsNumAppo = "000000000";
    //Original name: WS-NUMER-FILLER
    private String wsNumerFiller = "";
    /**Original name: WS-OGGETTO-IN-PTF<br>
	 * <pre>----------------------------------------------------------------*
	 *     INDICI
	 * ----------------------------------------------------------------*
	 * 01  IX-INDICI.
	 * ----------------------------------------------------------------*
	 *     FLAG
	 * ----------------------------------------------------------------*</pre>*/
    private WsOggettoInPtf wsOggettoInPtf = new WsOggettoInPtf();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: NUM-ADE-GAR-TRA
    private NumAdeGarTra numAdeGarTra = new NumAdeGarTra();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public String getWsIbOggettoFormatted() {
        return MarshalByteExt.bufferToStr(getWsIbOggettoBytes());
    }

    /**Original name: WS-IB-OGGETTO<br>
	 * <pre>-- NUMERAZIONE ADESIONE GARANZIA E TRANCHE</pre>*/
    public byte[] getWsIbOggettoBytes() {
        byte[] buffer = new byte[Len.WS_IB_OGGETTO];
        return getWsIbOggettoBytes(buffer, 1);
    }

    public byte[] getWsIbOggettoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wsNumAppo, Len.WS_NUM_APPO);
        position += Len.WS_NUM_APPO;
        MarshalByte.writeString(buffer, position, wsNumerFiller, Len.WS_NUMER_FILLER);
        return buffer;
    }

    public void setWsNumAppo(int wsNumAppo) {
        this.wsNumAppo = NumericDisplay.asString(wsNumAppo, Len.WS_NUM_APPO);
    }

    public void setWsNumAppoFormatted(String wsNumAppo) {
        this.wsNumAppo = Trunc.toUnsignedNumeric(wsNumAppo, Len.WS_NUM_APPO);
    }

    public int getWsNumAppo() {
        return NumericDisplay.asInt(this.wsNumAppo);
    }

    public void setWsNumerFiller(String wsNumerFiller) {
        this.wsNumerFiller = Functions.subString(wsNumerFiller, Len.WS_NUMER_FILLER);
    }

    public String getWsNumerFiller() {
        return this.wsNumerFiller;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public NumAdeGarTra getNumAdeGarTra() {
        return numAdeGarTra;
    }

    public WsOggettoInPtf getWsOggettoInPtf() {
        return wsOggettoInPtf;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_NUM_APPO = 9;
        public static final int WS_NUMER_FILLER = 31;
        public static final int WS_IB_OGGETTO = WS_NUM_APPO + WS_NUMER_FILLER;
        public static final int WK_TABELLA = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
