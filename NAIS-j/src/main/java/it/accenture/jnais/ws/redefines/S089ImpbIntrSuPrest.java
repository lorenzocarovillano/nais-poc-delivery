package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPB-INTR-SU-PREST<br>
 * Variable: S089-IMPB-INTR-SU-PREST from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpbIntrSuPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpbIntrSuPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPB_INTR_SU_PREST;
    }

    public void setWlquImpbIntrSuPrest(AfDecimal wlquImpbIntrSuPrest) {
        writeDecimalAsPacked(Pos.S089_IMPB_INTR_SU_PREST, wlquImpbIntrSuPrest.copy());
    }

    public void setWlquImpbIntrSuPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPB_INTR_SU_PREST, Pos.S089_IMPB_INTR_SU_PREST);
    }

    /**Original name: WLQU-IMPB-INTR-SU-PREST<br>*/
    public AfDecimal getWlquImpbIntrSuPrest() {
        return readPackedAsDecimal(Pos.S089_IMPB_INTR_SU_PREST, Len.Int.WLQU_IMPB_INTR_SU_PREST, Len.Fract.WLQU_IMPB_INTR_SU_PREST);
    }

    public byte[] getWlquImpbIntrSuPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPB_INTR_SU_PREST, Pos.S089_IMPB_INTR_SU_PREST);
        return buffer;
    }

    public void initWlquImpbIntrSuPrestSpaces() {
        fill(Pos.S089_IMPB_INTR_SU_PREST, Len.S089_IMPB_INTR_SU_PREST, Types.SPACE_CHAR);
    }

    public void setWlquImpbIntrSuPrestNull(String wlquImpbIntrSuPrestNull) {
        writeString(Pos.S089_IMPB_INTR_SU_PREST_NULL, wlquImpbIntrSuPrestNull, Len.WLQU_IMPB_INTR_SU_PREST_NULL);
    }

    /**Original name: WLQU-IMPB-INTR-SU-PREST-NULL<br>*/
    public String getWlquImpbIntrSuPrestNull() {
        return readString(Pos.S089_IMPB_INTR_SU_PREST_NULL, Len.WLQU_IMPB_INTR_SU_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPB_INTR_SU_PREST = 1;
        public static final int S089_IMPB_INTR_SU_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPB_INTR_SU_PREST = 8;
        public static final int WLQU_IMPB_INTR_SU_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_INTR_SU_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_INTR_SU_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
