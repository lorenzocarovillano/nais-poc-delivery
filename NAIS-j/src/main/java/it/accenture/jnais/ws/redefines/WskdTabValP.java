package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WSKD-TAB-VAL-P<br>
 * Variable: WSKD-TAB-VAL-P from program LOAS0800<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WskdTabValP extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_VARIABILI_P_MAXOCCURS = 100;
    public static final int TAB_LIVELLO_P_MAXOCCURS = 3;

    //==== CONSTRUCTORS ====
    public WskdTabValP() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSKD_TAB_VAL_P;
    }

    public String getWskdTabValPFormatted() {
        return readFixedString(Pos.WSKD_TAB_VAL_P, Len.WSKD_TAB_VAL_P);
    }

    public void setWskdTabValPBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSKD_TAB_VAL_P, Pos.WSKD_TAB_VAL_P);
    }

    public byte[] getWskdTabValPBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSKD_TAB_VAL_P, Pos.WSKD_TAB_VAL_P);
        return buffer;
    }

    public void setWskdIdPolPFormatted(int wskdIdPolPIdx, String wskdIdPolP) {
        int position = Pos.wskdIdPolP(wskdIdPolPIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(wskdIdPolP, Len.ID_POL_P), Len.ID_POL_P);
    }

    /**Original name: WSKD-ID-POL-P<br>*/
    public int getWskdIdPolP(int wskdIdPolPIdx) {
        int position = Pos.wskdIdPolP(wskdIdPolPIdx - 1);
        return readNumDispUnsignedInt(position, Len.ID_POL_P);
    }

    public void setWskdCodTipoOpzioneP(int wskdCodTipoOpzionePIdx, String wskdCodTipoOpzioneP) {
        int position = Pos.wskdCodTipoOpzioneP(wskdCodTipoOpzionePIdx - 1);
        writeString(position, wskdCodTipoOpzioneP, Len.COD_TIPO_OPZIONE_P);
    }

    /**Original name: WSKD-COD-TIPO-OPZIONE-P<br>*/
    public String getCodTipoOpzioneP(int codTipoOpzionePIdx) {
        int position = Pos.wskdCodTipoOpzioneP(codTipoOpzionePIdx - 1);
        return readString(position, Len.COD_TIPO_OPZIONE_P);
    }

    public void setWskdTpLivelloP(int wskdTpLivelloPIdx, char wskdTpLivelloP) {
        int position = Pos.wskdTpLivelloP(wskdTpLivelloPIdx - 1);
        writeChar(position, wskdTpLivelloP);
    }

    /**Original name: WSKD-TP-LIVELLO-P<br>*/
    public char getTpLivelloP(int tpLivelloPIdx) {
        int position = Pos.wskdTpLivelloP(tpLivelloPIdx - 1);
        return readChar(position);
    }

    public void setWskdCodLivelloP(int wskdCodLivelloPIdx, String wskdCodLivelloP) {
        int position = Pos.wskdCodLivelloP(wskdCodLivelloPIdx - 1);
        writeString(position, wskdCodLivelloP, Len.COD_LIVELLO_P);
    }

    /**Original name: WSKD-COD-LIVELLO-P<br>*/
    public String getCodLivelloP(int codLivelloPIdx) {
        int position = Pos.wskdCodLivelloP(codLivelloPIdx - 1);
        return readString(position, Len.COD_LIVELLO_P);
    }

    public void setWskdIdLivelloPFormatted(int wskdIdLivelloPIdx, String wskdIdLivelloP) {
        int position = Pos.wskdIdLivelloP(wskdIdLivelloPIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(wskdIdLivelloP, Len.ID_LIVELLO_P), Len.ID_LIVELLO_P);
    }

    /**Original name: WSKD-ID-LIVELLO-P<br>*/
    public int getWskdIdLivelloP(int wskdIdLivelloPIdx) {
        int position = Pos.wskdIdLivelloP(wskdIdLivelloPIdx - 1);
        return readNumDispUnsignedInt(position, Len.ID_LIVELLO_P);
    }

    public String getIdLivelloPFormatted(int idLivelloPIdx) {
        int position = Pos.wskdIdLivelloP(idLivelloPIdx - 1);
        return readFixedString(position, Len.ID_LIVELLO_P);
    }

    public void setWskdDtInizProdP(int wskdDtInizProdPIdx, String wskdDtInizProdP) {
        int position = Pos.wskdDtInizProdP(wskdDtInizProdPIdx - 1);
        writeString(position, wskdDtInizProdP, Len.DT_INIZ_PROD_P);
    }

    /**Original name: WSKD-DT-INIZ-PROD-P<br>*/
    public String getDtInizProdP(int dtInizProdPIdx) {
        int position = Pos.wskdDtInizProdP(dtInizProdPIdx - 1);
        return readString(position, Len.DT_INIZ_PROD_P);
    }

    public String getWskdDtInizProdPFormatted(int wskdDtInizProdPIdx) {
        return Functions.padBlanks(getDtInizProdP(wskdDtInizProdPIdx), Len.DT_INIZ_PROD_P);
    }

    public void setWskdCodRgmFiscP(int wskdCodRgmFiscPIdx, String wskdCodRgmFiscP) {
        int position = Pos.wskdCodRgmFiscP(wskdCodRgmFiscPIdx - 1);
        writeString(position, wskdCodRgmFiscP, Len.COD_RGM_FISC_P);
    }

    /**Original name: WSKD-COD-RGM-FISC-P<br>*/
    public String getCodRgmFiscP(int codRgmFiscPIdx) {
        int position = Pos.wskdCodRgmFiscP(codRgmFiscPIdx - 1);
        return readString(position, Len.COD_RGM_FISC_P);
    }

    public String getCodRgmFiscPFormatted(int codRgmFiscPIdx) {
        return Functions.padBlanks(getCodRgmFiscP(codRgmFiscPIdx), Len.COD_RGM_FISC_P);
    }

    public void setWskdNomeServizioP(int wskdNomeServizioPIdx, String wskdNomeServizioP) {
        int position = Pos.wskdNomeServizioP(wskdNomeServizioPIdx - 1);
        writeString(position, wskdNomeServizioP, Len.NOME_SERVIZIO_P);
    }

    /**Original name: WSKD-NOME-SERVIZIO-P<br>*/
    public String getWskdNomeServizioP(int wskdNomeServizioPIdx) {
        int position = Pos.wskdNomeServizioP(wskdNomeServizioPIdx - 1);
        return readString(position, Len.NOME_SERVIZIO_P);
    }

    /**Original name: WSKD-AREA-VARIABILI-P<br>*/
    public byte[] getWskdAreaVariabiliPBytes(int wskdAreaVariabiliPIdx) {
        byte[] buffer = new byte[Len.AREA_VARIABILI_P];
        return getWskdAreaVariabiliPBytes(wskdAreaVariabiliPIdx, buffer, 1);
    }

    public byte[] getWskdAreaVariabiliPBytes(int wskdAreaVariabiliPIdx, byte[] buffer, int offset) {
        int position = Pos.wskdAreaVariabiliP(wskdAreaVariabiliPIdx - 1);
        getBytes(buffer, offset, Len.AREA_VARIABILI_P, position);
        return buffer;
    }

    public void setWskdEleVariabiliMaxP(int wskdEleVariabiliMaxPIdx, short wskdEleVariabiliMaxP) {
        int position = Pos.wskdEleVariabiliMaxP(wskdEleVariabiliMaxPIdx - 1);
        writeShortAsPacked(position, wskdEleVariabiliMaxP, Len.Int.ELE_VARIABILI_MAX_P);
    }

    /**Original name: WSKD-ELE-VARIABILI-MAX-P<br>*/
    public short getEleVariabiliMaxP(int eleVariabiliMaxPIdx) {
        int position = Pos.wskdEleVariabiliMaxP(eleVariabiliMaxPIdx - 1);
        return readPackedAsShort(position, Len.Int.ELE_VARIABILI_MAX_P);
    }

    public void setWskdCodVariabileP(int wskdCodVariabilePIdx1, int wskdCodVariabilePIdx2, String wskdCodVariabileP) {
        int position = Pos.wskdCodVariabileP(wskdCodVariabilePIdx1 - 1, wskdCodVariabilePIdx2 - 1);
        writeString(position, wskdCodVariabileP, Len.COD_VARIABILE_P);
    }

    /**Original name: WSKD-COD-VARIABILE-P<br>*/
    public String getCodVariabileP(int codVariabilePIdx1, int codVariabilePIdx2) {
        int position = Pos.wskdCodVariabileP(codVariabilePIdx1 - 1, codVariabilePIdx2 - 1);
        return readString(position, Len.COD_VARIABILE_P);
    }

    public void setWskdTpDatoP(int wskdTpDatoPIdx1, int wskdTpDatoPIdx2, char wskdTpDatoP) {
        int position = Pos.wskdTpDatoP(wskdTpDatoPIdx1 - 1, wskdTpDatoPIdx2 - 1);
        writeChar(position, wskdTpDatoP);
    }

    public void setWskdTpDatoPFormatted(int wskdTpDatoPIdx1, int wskdTpDatoPIdx2, String wskdTpDatoP) {
        setWskdTpDatoP(wskdTpDatoPIdx1, wskdTpDatoPIdx2, Functions.charAt(wskdTpDatoP, Types.CHAR_SIZE));
    }

    /**Original name: WSKD-TP-DATO-P<br>*/
    public char getTpDatoP(int tpDatoPIdx1, int tpDatoPIdx2) {
        int position = Pos.wskdTpDatoP(tpDatoPIdx1 - 1, tpDatoPIdx2 - 1);
        return readChar(position);
    }

    public void setWskdValGenericoP(int wskdValGenericoPIdx1, int wskdValGenericoPIdx2, String wskdValGenericoP) {
        int position = Pos.wskdValGenericoP(wskdValGenericoPIdx1 - 1, wskdValGenericoPIdx2 - 1);
        writeString(position, wskdValGenericoP, Len.VAL_GENERICO_P);
    }

    /**Original name: WSKD-VAL-GENERICO-P<br>*/
    public String getValGenericoP(int valGenericoPIdx1, int valGenericoPIdx2) {
        int position = Pos.wskdValGenericoP(valGenericoPIdx1 - 1, valGenericoPIdx2 - 1);
        return readString(position, Len.VAL_GENERICO_P);
    }

    /**Original name: WSKD-VAL-IMP-P<br>*/
    public AfDecimal getWskdValImpP(int wskdValImpPIdx1, int wskdValImpPIdx2) {
        int position = Pos.wskdValImpP(wskdValImpPIdx1 - 1, wskdValImpPIdx2 - 1);
        return readDecimal(position, Len.Int.WSKD_VAL_IMP_P, Len.Fract.WSKD_VAL_IMP_P);
    }

    /**Original name: WSKD-VAL-PERC-P<br>*/
    public AfDecimal getWskdValPercP(int wskdValPercPIdx1, int wskdValPercPIdx2) {
        int position = Pos.wskdValPercP(wskdValPercPIdx1 - 1, wskdValPercPIdx2 - 1);
        return readDecimal(position, Len.Int.WSKD_VAL_PERC_P, Len.Fract.WSKD_VAL_PERC_P, SignType.NO_SIGN);
    }

    public void setWskdValStrP(int wskdValStrPIdx1, int wskdValStrPIdx2, String wskdValStrP) {
        int position = Pos.wskdValStrP(wskdValStrPIdx1 - 1, wskdValStrPIdx2 - 1);
        writeString(position, wskdValStrP, Len.WSKD_VAL_STR_P);
    }

    /**Original name: WSKD-VAL-STR-P<br>*/
    public String getWskdValStrP(int wskdValStrPIdx1, int wskdValStrPIdx2) {
        int position = Pos.wskdValStrP(wskdValStrPIdx1 - 1, wskdValStrPIdx2 - 1);
        return readString(position, Len.WSKD_VAL_STR_P);
    }

    public void setWskdRestoTabValP(String wskdRestoTabValP) {
        writeString(Pos.RESTO_TAB_VAL_P, wskdRestoTabValP, Len.WSKD_RESTO_TAB_VAL_P);
    }

    /**Original name: WSKD-RESTO-TAB-VAL-P<br>*/
    public String getWskdRestoTabValP() {
        return readString(Pos.RESTO_TAB_VAL_P, Len.WSKD_RESTO_TAB_VAL_P);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSKD_TAB_VAL_P = 1;
        public static final int WSKD_TAB_VAL_R_P = 1;
        public static final int FLR1 = WSKD_TAB_VAL_R_P;
        public static final int RESTO_TAB_VAL_P = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wskdTabLivelloP(int idx) {
            return WSKD_TAB_VAL_P + idx * Len.TAB_LIVELLO_P;
        }

        public static int wskdIdPolP(int idx) {
            return wskdTabLivelloP(idx);
        }

        public static int wskdDatiLivelloP(int idx) {
            return wskdIdPolP(idx) + Len.ID_POL_P;
        }

        public static int wskdCodTipoOpzioneP(int idx) {
            return wskdDatiLivelloP(idx);
        }

        public static int wskdTpLivelloP(int idx) {
            return wskdCodTipoOpzioneP(idx) + Len.COD_TIPO_OPZIONE_P;
        }

        public static int wskdCodLivelloP(int idx) {
            return wskdTpLivelloP(idx) + Len.TP_LIVELLO_P;
        }

        public static int wskdIdLivelloP(int idx) {
            return wskdCodLivelloP(idx) + Len.COD_LIVELLO_P;
        }

        public static int wskdDtInizProdP(int idx) {
            return wskdIdLivelloP(idx) + Len.ID_LIVELLO_P;
        }

        public static int wskdCodRgmFiscP(int idx) {
            return wskdDtInizProdP(idx) + Len.DT_INIZ_PROD_P;
        }

        public static int wskdNomeServizioP(int idx) {
            return wskdCodRgmFiscP(idx) + Len.COD_RGM_FISC_P;
        }

        public static int wskdAreaVariabiliP(int idx) {
            return wskdNomeServizioP(idx) + Len.NOME_SERVIZIO_P;
        }

        public static int wskdEleVariabiliMaxP(int idx) {
            return wskdAreaVariabiliP(idx);
        }

        public static int wskdTabVariabiliP(int idx1, int idx2) {
            return wskdEleVariabiliMaxP(idx1) + Len.ELE_VARIABILI_MAX_P + idx2 * Len.TAB_VARIABILI_P;
        }

        public static int wskdAreaVariabileP(int idx1, int idx2) {
            return wskdTabVariabiliP(idx1, idx2);
        }

        public static int wskdCodVariabileP(int idx1, int idx2) {
            return wskdAreaVariabileP(idx1, idx2);
        }

        public static int wskdTpDatoP(int idx1, int idx2) {
            return wskdCodVariabileP(idx1, idx2) + Len.COD_VARIABILE_P;
        }

        public static int wskdValGenericoP(int idx1, int idx2) {
            return wskdTpDatoP(idx1, idx2) + Len.TP_DATO_P;
        }

        public static int wskdValImpGenP(int idx1, int idx2) {
            return wskdValGenericoP(idx1, idx2);
        }

        public static int wskdValImpP(int idx1, int idx2) {
            return wskdValImpGenP(idx1, idx2);
        }

        public static int wskdValImpFillerP(int idx1, int idx2) {
            return wskdValImpP(idx1, idx2) + Len.VAL_IMP_P;
        }

        public static int wskdValPercGenP(int idx1, int idx2) {
            return wskdValGenericoP(idx1, idx2);
        }

        public static int wskdValPercP(int idx1, int idx2) {
            return wskdValPercGenP(idx1, idx2);
        }

        public static int wskdValPercFillerP(int idx1, int idx2) {
            return wskdValPercP(idx1, idx2) + Len.VAL_PERC_P;
        }

        public static int wskdValStrGenP(int idx1, int idx2) {
            return wskdValGenericoP(idx1, idx2);
        }

        public static int wskdValStrP(int idx1, int idx2) {
            return wskdValStrGenP(idx1, idx2);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POL_P = 9;
        public static final int COD_TIPO_OPZIONE_P = 2;
        public static final int TP_LIVELLO_P = 1;
        public static final int COD_LIVELLO_P = 12;
        public static final int ID_LIVELLO_P = 9;
        public static final int DT_INIZ_PROD_P = 8;
        public static final int COD_RGM_FISC_P = 2;
        public static final int NOME_SERVIZIO_P = 8;
        public static final int ELE_VARIABILI_MAX_P = 3;
        public static final int COD_VARIABILE_P = 12;
        public static final int TP_DATO_P = 1;
        public static final int VAL_GENERICO_P = 60;
        public static final int AREA_VARIABILE_P = COD_VARIABILE_P + TP_DATO_P + VAL_GENERICO_P;
        public static final int TAB_VARIABILI_P = AREA_VARIABILE_P;
        public static final int AREA_VARIABILI_P = ELE_VARIABILI_MAX_P + WskdTabValP.TAB_VARIABILI_P_MAXOCCURS * TAB_VARIABILI_P;
        public static final int DATI_LIVELLO_P = COD_TIPO_OPZIONE_P + TP_LIVELLO_P + COD_LIVELLO_P + ID_LIVELLO_P + DT_INIZ_PROD_P + COD_RGM_FISC_P + NOME_SERVIZIO_P + AREA_VARIABILI_P;
        public static final int TAB_LIVELLO_P = ID_POL_P + DATI_LIVELLO_P;
        public static final int VAL_IMP_P = 18;
        public static final int VAL_PERC_P = 14;
        public static final int FLR1 = 7354;
        public static final int WSKD_TAB_VAL_P = WskdTabValP.TAB_LIVELLO_P_MAXOCCURS * TAB_LIVELLO_P;
        public static final int WSKD_RESTO_TAB_VAL_P = 14708;
        public static final int WSKD_VAL_STR_P = 60;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ELE_VARIABILI_MAX_P = 4;
            public static final int WSKD_VAL_IMP_P = 11;
            public static final int WSKD_VAL_PERC_P = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WSKD_VAL_IMP_P = 7;
            public static final int WSKD_VAL_PERC_P = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
