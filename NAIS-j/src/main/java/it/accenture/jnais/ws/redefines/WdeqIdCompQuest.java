package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WDEQ-ID-COMP-QUEST<br>
 * Variable: WDEQ-ID-COMP-QUEST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdeqIdCompQuest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdeqIdCompQuest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDEQ_ID_COMP_QUEST;
    }

    public void setWdeqIdCompQuest(int wdeqIdCompQuest) {
        writeIntAsPacked(Pos.WDEQ_ID_COMP_QUEST, wdeqIdCompQuest, Len.Int.WDEQ_ID_COMP_QUEST);
    }

    public void setWdeqIdCompQuestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDEQ_ID_COMP_QUEST, Pos.WDEQ_ID_COMP_QUEST);
    }

    /**Original name: WDEQ-ID-COMP-QUEST<br>*/
    public int getWdeqIdCompQuest() {
        return readPackedAsInt(Pos.WDEQ_ID_COMP_QUEST, Len.Int.WDEQ_ID_COMP_QUEST);
    }

    public byte[] getWdeqIdCompQuestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDEQ_ID_COMP_QUEST, Pos.WDEQ_ID_COMP_QUEST);
        return buffer;
    }

    public void initWdeqIdCompQuestSpaces() {
        fill(Pos.WDEQ_ID_COMP_QUEST, Len.WDEQ_ID_COMP_QUEST, Types.SPACE_CHAR);
    }

    public void setWdeqIdCompQuestNull(String wdeqIdCompQuestNull) {
        writeString(Pos.WDEQ_ID_COMP_QUEST_NULL, wdeqIdCompQuestNull, Len.WDEQ_ID_COMP_QUEST_NULL);
    }

    /**Original name: WDEQ-ID-COMP-QUEST-NULL<br>*/
    public String getWdeqIdCompQuestNull() {
        return readString(Pos.WDEQ_ID_COMP_QUEST_NULL, Len.WDEQ_ID_COMP_QUEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDEQ_ID_COMP_QUEST = 1;
        public static final int WDEQ_ID_COMP_QUEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDEQ_ID_COMP_QUEST = 5;
        public static final int WDEQ_ID_COMP_QUEST_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDEQ_ID_COMP_QUEST = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
