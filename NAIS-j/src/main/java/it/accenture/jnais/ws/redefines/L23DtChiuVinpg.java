package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L23-DT-CHIU-VINPG<br>
 * Variable: L23-DT-CHIU-VINPG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L23DtChiuVinpg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L23DtChiuVinpg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L23_DT_CHIU_VINPG;
    }

    public void setL23DtChiuVinpg(int l23DtChiuVinpg) {
        writeIntAsPacked(Pos.L23_DT_CHIU_VINPG, l23DtChiuVinpg, Len.Int.L23_DT_CHIU_VINPG);
    }

    public void setL23DtChiuVinpgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L23_DT_CHIU_VINPG, Pos.L23_DT_CHIU_VINPG);
    }

    /**Original name: L23-DT-CHIU-VINPG<br>*/
    public int getL23DtChiuVinpg() {
        return readPackedAsInt(Pos.L23_DT_CHIU_VINPG, Len.Int.L23_DT_CHIU_VINPG);
    }

    public byte[] getL23DtChiuVinpgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L23_DT_CHIU_VINPG, Pos.L23_DT_CHIU_VINPG);
        return buffer;
    }

    public void setL23DtChiuVinpgNull(String l23DtChiuVinpgNull) {
        writeString(Pos.L23_DT_CHIU_VINPG_NULL, l23DtChiuVinpgNull, Len.L23_DT_CHIU_VINPG_NULL);
    }

    /**Original name: L23-DT-CHIU-VINPG-NULL<br>*/
    public String getL23DtChiuVinpgNull() {
        return readString(Pos.L23_DT_CHIU_VINPG_NULL, Len.L23_DT_CHIU_VINPG_NULL);
    }

    public String getL23DtChiuVinpgNullFormatted() {
        return Functions.padBlanks(getL23DtChiuVinpgNull(), Len.L23_DT_CHIU_VINPG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L23_DT_CHIU_VINPG = 1;
        public static final int L23_DT_CHIU_VINPG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L23_DT_CHIU_VINPG = 5;
        public static final int L23_DT_CHIU_VINPG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L23_DT_CHIU_VINPG = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
