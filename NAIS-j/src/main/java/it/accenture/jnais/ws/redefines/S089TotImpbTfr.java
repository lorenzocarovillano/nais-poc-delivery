package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMPB-TFR<br>
 * Variable: S089-TOT-IMPB-TFR from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpbTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpbTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMPB_TFR;
    }

    public void setWlquTotImpbTfr(AfDecimal wlquTotImpbTfr) {
        writeDecimalAsPacked(Pos.S089_TOT_IMPB_TFR, wlquTotImpbTfr.copy());
    }

    public void setWlquTotImpbTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMPB_TFR, Pos.S089_TOT_IMPB_TFR);
    }

    /**Original name: WLQU-TOT-IMPB-TFR<br>*/
    public AfDecimal getWlquTotImpbTfr() {
        return readPackedAsDecimal(Pos.S089_TOT_IMPB_TFR, Len.Int.WLQU_TOT_IMPB_TFR, Len.Fract.WLQU_TOT_IMPB_TFR);
    }

    public byte[] getWlquTotImpbTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMPB_TFR, Pos.S089_TOT_IMPB_TFR);
        return buffer;
    }

    public void initWlquTotImpbTfrSpaces() {
        fill(Pos.S089_TOT_IMPB_TFR, Len.S089_TOT_IMPB_TFR, Types.SPACE_CHAR);
    }

    public void setWlquTotImpbTfrNull(String wlquTotImpbTfrNull) {
        writeString(Pos.S089_TOT_IMPB_TFR_NULL, wlquTotImpbTfrNull, Len.WLQU_TOT_IMPB_TFR_NULL);
    }

    /**Original name: WLQU-TOT-IMPB-TFR-NULL<br>*/
    public String getWlquTotImpbTfrNull() {
        return readString(Pos.S089_TOT_IMPB_TFR_NULL, Len.WLQU_TOT_IMPB_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMPB_TFR = 1;
        public static final int S089_TOT_IMPB_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMPB_TFR = 8;
        public static final int WLQU_TOT_IMPB_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMPB_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMPB_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
