package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-NUM-MM-CALC-MORA<br>
 * Variable: WPCO-NUM-MM-CALC-MORA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoNumMmCalcMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoNumMmCalcMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_NUM_MM_CALC_MORA;
    }

    public void setWpcoNumMmCalcMora(int wpcoNumMmCalcMora) {
        writeIntAsPacked(Pos.WPCO_NUM_MM_CALC_MORA, wpcoNumMmCalcMora, Len.Int.WPCO_NUM_MM_CALC_MORA);
    }

    public void setDpcoNumMmCalcMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_NUM_MM_CALC_MORA, Pos.WPCO_NUM_MM_CALC_MORA);
    }

    /**Original name: WPCO-NUM-MM-CALC-MORA<br>*/
    public int getWpcoNumMmCalcMora() {
        return readPackedAsInt(Pos.WPCO_NUM_MM_CALC_MORA, Len.Int.WPCO_NUM_MM_CALC_MORA);
    }

    public byte[] getWpcoNumMmCalcMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_NUM_MM_CALC_MORA, Pos.WPCO_NUM_MM_CALC_MORA);
        return buffer;
    }

    public void setWpcoNumMmCalcMoraNull(String wpcoNumMmCalcMoraNull) {
        writeString(Pos.WPCO_NUM_MM_CALC_MORA_NULL, wpcoNumMmCalcMoraNull, Len.WPCO_NUM_MM_CALC_MORA_NULL);
    }

    /**Original name: WPCO-NUM-MM-CALC-MORA-NULL<br>*/
    public String getWpcoNumMmCalcMoraNull() {
        return readString(Pos.WPCO_NUM_MM_CALC_MORA_NULL, Len.WPCO_NUM_MM_CALC_MORA_NULL);
    }

    public String getWpcoNumMmCalcMoraNullFormatted() {
        return Functions.padBlanks(getWpcoNumMmCalcMoraNull(), Len.WPCO_NUM_MM_CALC_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_NUM_MM_CALC_MORA = 1;
        public static final int WPCO_NUM_MM_CALC_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_NUM_MM_CALC_MORA = 3;
        public static final int WPCO_NUM_MM_CALC_MORA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_NUM_MM_CALC_MORA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
