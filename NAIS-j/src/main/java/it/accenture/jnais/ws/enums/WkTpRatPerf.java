package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-TP-RAT-PERF<br>
 * Variable: WK-TP-RAT-PERF from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkTpRatPerf {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char BATCH = 'B';
    public static final char SEPARATE = 'S';
    public static final char UNICA = 'U';

    //==== METHODS ====
    public void setWkTpRatPerf(char wkTpRatPerf) {
        this.value = wkTpRatPerf;
    }

    public char getWkTpRatPerf() {
        return this.value;
    }

    public boolean isUnica() {
        return value == UNICA;
    }
}
