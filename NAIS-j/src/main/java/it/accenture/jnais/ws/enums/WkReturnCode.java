package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-RETURN-CODE<br>
 * Variable: WK-RETURN-CODE from program LCCS1750<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkReturnCode {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_RETURN_CODE);
    public static final String INVALID_OPER = "D2";
    public static final String FIELD_NOT_VALUED = "C1";

    //==== METHODS ====
    public void setWkReturnCode(String wkReturnCode) {
        this.value = Functions.subString(wkReturnCode, Len.WK_RETURN_CODE);
    }

    public String getWkReturnCode() {
        return this.value;
    }

    public void setInvalidOper() {
        value = INVALID_OPER;
    }

    public void setFieldNotValued() {
        value = FIELD_NOT_VALUED;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_RETURN_CODE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
