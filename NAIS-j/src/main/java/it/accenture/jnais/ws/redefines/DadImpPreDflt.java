package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-IMP-PRE-DFLT<br>
 * Variable: DAD-IMP-PRE-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadImpPreDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadImpPreDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_IMP_PRE_DFLT;
    }

    public void setDadImpPreDflt(AfDecimal dadImpPreDflt) {
        writeDecimalAsPacked(Pos.DAD_IMP_PRE_DFLT, dadImpPreDflt.copy());
    }

    public void setDadImpPreDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_IMP_PRE_DFLT, Pos.DAD_IMP_PRE_DFLT);
    }

    /**Original name: DAD-IMP-PRE-DFLT<br>*/
    public AfDecimal getDadImpPreDflt() {
        return readPackedAsDecimal(Pos.DAD_IMP_PRE_DFLT, Len.Int.DAD_IMP_PRE_DFLT, Len.Fract.DAD_IMP_PRE_DFLT);
    }

    public byte[] getDadImpPreDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_IMP_PRE_DFLT, Pos.DAD_IMP_PRE_DFLT);
        return buffer;
    }

    public void setDadImpPreDfltNull(String dadImpPreDfltNull) {
        writeString(Pos.DAD_IMP_PRE_DFLT_NULL, dadImpPreDfltNull, Len.DAD_IMP_PRE_DFLT_NULL);
    }

    /**Original name: DAD-IMP-PRE-DFLT-NULL<br>*/
    public String getDadImpPreDfltNull() {
        return readString(Pos.DAD_IMP_PRE_DFLT_NULL, Len.DAD_IMP_PRE_DFLT_NULL);
    }

    public String getDadImpPreDfltNullFormatted() {
        return Functions.padBlanks(getDadImpPreDfltNull(), Len.DAD_IMP_PRE_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_IMP_PRE_DFLT = 1;
        public static final int DAD_IMP_PRE_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_IMP_PRE_DFLT = 8;
        public static final int DAD_IMP_PRE_DFLT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_IMP_PRE_DFLT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DAD_IMP_PRE_DFLT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
