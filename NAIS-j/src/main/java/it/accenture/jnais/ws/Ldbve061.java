package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LDBVE061<br>
 * Variable: LDBVE061 from copybook LDBVE061<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbve061 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBVE061-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBVE061-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBVE061-TP-MOVI
    private int tpMovi = DefaultValues.INT_VAL;
    //Original name: LDBVE061-DT-EFF-I
    private int dtEffI = DefaultValues.INT_VAL;
    //Original name: LDBVE061-DT-EFF-O
    private int dtEffO = DefaultValues.INT_VAL;
    //Original name: LDBVE061-DS-CPTZ-O
    private long dsCptzO = DefaultValues.LONG_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBVE061;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbve061Bytes(buf);
    }

    public String getLdbve061Formatted() {
        return MarshalByteExt.bufferToStr(getLdbve061Bytes());
    }

    public void setLdbve061Bytes(byte[] buffer) {
        setLdbve061Bytes(buffer, 1);
    }

    public byte[] getLdbve061Bytes() {
        byte[] buffer = new byte[Len.LDBVE061];
        return getLdbve061Bytes(buffer, 1);
    }

    public void setLdbve061Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI, 0);
        position += Len.TP_MOVI;
        dtEffI = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_EFF_I, 0);
        position += Len.DT_EFF_I;
        dtEffO = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_EFF_O, 0);
        position += Len.DT_EFF_O;
        dsCptzO = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_CPTZ_O, 0);
    }

    public byte[] getLdbve061Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi, Len.Int.TP_MOVI, 0);
        position += Len.TP_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, dtEffI, Len.Int.DT_EFF_I, 0);
        position += Len.DT_EFF_I;
        MarshalByte.writeIntAsPacked(buffer, position, dtEffO, Len.Int.DT_EFF_O, 0);
        position += Len.DT_EFF_O;
        MarshalByte.writeLongAsPacked(buffer, position, dsCptzO, Len.Int.DS_CPTZ_O, 0);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpMovi(int tpMovi) {
        this.tpMovi = tpMovi;
    }

    public int getTpMovi() {
        return this.tpMovi;
    }

    public void setDtEffI(int dtEffI) {
        this.dtEffI = dtEffI;
    }

    public int getDtEffI() {
        return this.dtEffI;
    }

    public void setDtEffO(int dtEffO) {
        this.dtEffO = dtEffO;
    }

    public int getDtEffO() {
        return this.dtEffO;
    }

    public void setDsCptzO(long dsCptzO) {
        this.dsCptzO = dsCptzO;
    }

    public long getDsCptzO() {
        return this.dsCptzO;
    }

    @Override
    public byte[] serialize() {
        return getLdbve061Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int TP_MOVI = 3;
        public static final int DT_EFF_I = 5;
        public static final int DT_EFF_O = 5;
        public static final int DS_CPTZ_O = 10;
        public static final int LDBVE061 = ID_OGG + TP_OGG + TP_MOVI + DT_EFF_I + DT_EFF_O + DS_CPTZ_O;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int TP_MOVI = 5;
            public static final int DT_EFF_I = 8;
            public static final int DT_EFF_O = 8;
            public static final int DS_CPTZ_O = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
