package it.accenture.jnais.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: WK-GESTIONE-MSG-ERR<br>
 * Variable: WK-GESTIONE-MSG-ERR from program LOAS0800<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkGestioneMsgErr {

    //==== PROPERTIES ====
    //Original name: WK-COD-ERR
    private String codErr = "000000";
    //Original name: WK-LABEL-ERR
    private String labelErr = "";
    //Original name: WK-STRING
    private String stringFld = "";

    //==== METHODS ====
    public void setCodErr(String codErr) {
        this.codErr = Functions.subString(codErr, Len.COD_ERR);
    }

    public String getCodErr() {
        return this.codErr;
    }

    public String getCodErrFormatted() {
        return Functions.padBlanks(getCodErr(), Len.COD_ERR);
    }

    public void setLabelErr(String labelErr) {
        this.labelErr = Functions.subString(labelErr, Len.LABEL_ERR);
    }

    public String getLabelErr() {
        return this.labelErr;
    }

    public void setStringFld(String stringFld) {
        this.stringFld = Functions.subString(stringFld, Len.STRING_FLD);
    }

    public String getStringFld() {
        return this.stringFld;
    }

    public String getStringFldFormatted() {
        return Functions.padBlanks(getStringFld(), Len.STRING_FLD);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_ERR = 6;
        public static final int LABEL_ERR = 30;
        public static final int STRING_FLD = 85;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
