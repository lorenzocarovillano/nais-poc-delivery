package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-EC-TCM-COLL<br>
 * Variable: WPCO-DT-ULT-EC-TCM-COLL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEcTcmColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEcTcmColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_EC_TCM_COLL;
    }

    public void setWpcoDtUltEcTcmColl(int wpcoDtUltEcTcmColl) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_EC_TCM_COLL, wpcoDtUltEcTcmColl, Len.Int.WPCO_DT_ULT_EC_TCM_COLL);
    }

    public void setDpcoDtUltEcTcmCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_EC_TCM_COLL, Pos.WPCO_DT_ULT_EC_TCM_COLL);
    }

    /**Original name: WPCO-DT-ULT-EC-TCM-COLL<br>*/
    public int getWpcoDtUltEcTcmColl() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_EC_TCM_COLL, Len.Int.WPCO_DT_ULT_EC_TCM_COLL);
    }

    public byte[] getWpcoDtUltEcTcmCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_EC_TCM_COLL, Pos.WPCO_DT_ULT_EC_TCM_COLL);
        return buffer;
    }

    public void setWpcoDtUltEcTcmCollNull(String wpcoDtUltEcTcmCollNull) {
        writeString(Pos.WPCO_DT_ULT_EC_TCM_COLL_NULL, wpcoDtUltEcTcmCollNull, Len.WPCO_DT_ULT_EC_TCM_COLL_NULL);
    }

    /**Original name: WPCO-DT-ULT-EC-TCM-COLL-NULL<br>*/
    public String getWpcoDtUltEcTcmCollNull() {
        return readString(Pos.WPCO_DT_ULT_EC_TCM_COLL_NULL, Len.WPCO_DT_ULT_EC_TCM_COLL_NULL);
    }

    public String getWpcoDtUltEcTcmCollNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEcTcmCollNull(), Len.WPCO_DT_ULT_EC_TCM_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_TCM_COLL = 1;
        public static final int WPCO_DT_ULT_EC_TCM_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_TCM_COLL = 5;
        public static final int WPCO_DT_ULT_EC_TCM_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_EC_TCM_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
