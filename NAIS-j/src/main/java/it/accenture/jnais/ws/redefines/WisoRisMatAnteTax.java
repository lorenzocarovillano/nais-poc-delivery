package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WISO-RIS-MAT-ANTE-TAX<br>
 * Variable: WISO-RIS-MAT-ANTE-TAX from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoRisMatAnteTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoRisMatAnteTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_RIS_MAT_ANTE_TAX;
    }

    public void setWisoRisMatAnteTax(AfDecimal wisoRisMatAnteTax) {
        writeDecimalAsPacked(Pos.WISO_RIS_MAT_ANTE_TAX, wisoRisMatAnteTax.copy());
    }

    public void setWisoRisMatAnteTaxFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_RIS_MAT_ANTE_TAX, Pos.WISO_RIS_MAT_ANTE_TAX);
    }

    /**Original name: WISO-RIS-MAT-ANTE-TAX<br>*/
    public AfDecimal getWisoRisMatAnteTax() {
        return readPackedAsDecimal(Pos.WISO_RIS_MAT_ANTE_TAX, Len.Int.WISO_RIS_MAT_ANTE_TAX, Len.Fract.WISO_RIS_MAT_ANTE_TAX);
    }

    public byte[] getWisoRisMatAnteTaxAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_RIS_MAT_ANTE_TAX, Pos.WISO_RIS_MAT_ANTE_TAX);
        return buffer;
    }

    public void initWisoRisMatAnteTaxSpaces() {
        fill(Pos.WISO_RIS_MAT_ANTE_TAX, Len.WISO_RIS_MAT_ANTE_TAX, Types.SPACE_CHAR);
    }

    public void setWisoRisMatAnteTaxNull(String wisoRisMatAnteTaxNull) {
        writeString(Pos.WISO_RIS_MAT_ANTE_TAX_NULL, wisoRisMatAnteTaxNull, Len.WISO_RIS_MAT_ANTE_TAX_NULL);
    }

    /**Original name: WISO-RIS-MAT-ANTE-TAX-NULL<br>*/
    public String getWisoRisMatAnteTaxNull() {
        return readString(Pos.WISO_RIS_MAT_ANTE_TAX_NULL, Len.WISO_RIS_MAT_ANTE_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_RIS_MAT_ANTE_TAX = 1;
        public static final int WISO_RIS_MAT_ANTE_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_RIS_MAT_ANTE_TAX = 8;
        public static final int WISO_RIS_MAT_ANTE_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WISO_RIS_MAT_ANTE_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_RIS_MAT_ANTE_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
