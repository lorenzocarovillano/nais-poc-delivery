package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ACCPRE-VIS-EFFLQ<br>
 * Variable: DFL-ACCPRE-VIS-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAccpreVisEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAccpreVisEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ACCPRE_VIS_EFFLQ;
    }

    public void setDflAccpreVisEfflq(AfDecimal dflAccpreVisEfflq) {
        writeDecimalAsPacked(Pos.DFL_ACCPRE_VIS_EFFLQ, dflAccpreVisEfflq.copy());
    }

    public void setDflAccpreVisEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ACCPRE_VIS_EFFLQ, Pos.DFL_ACCPRE_VIS_EFFLQ);
    }

    /**Original name: DFL-ACCPRE-VIS-EFFLQ<br>*/
    public AfDecimal getDflAccpreVisEfflq() {
        return readPackedAsDecimal(Pos.DFL_ACCPRE_VIS_EFFLQ, Len.Int.DFL_ACCPRE_VIS_EFFLQ, Len.Fract.DFL_ACCPRE_VIS_EFFLQ);
    }

    public byte[] getDflAccpreVisEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ACCPRE_VIS_EFFLQ, Pos.DFL_ACCPRE_VIS_EFFLQ);
        return buffer;
    }

    public void setDflAccpreVisEfflqNull(String dflAccpreVisEfflqNull) {
        writeString(Pos.DFL_ACCPRE_VIS_EFFLQ_NULL, dflAccpreVisEfflqNull, Len.DFL_ACCPRE_VIS_EFFLQ_NULL);
    }

    /**Original name: DFL-ACCPRE-VIS-EFFLQ-NULL<br>*/
    public String getDflAccpreVisEfflqNull() {
        return readString(Pos.DFL_ACCPRE_VIS_EFFLQ_NULL, Len.DFL_ACCPRE_VIS_EFFLQ_NULL);
    }

    public String getDflAccpreVisEfflqNullFormatted() {
        return Functions.padBlanks(getDflAccpreVisEfflqNull(), Len.DFL_ACCPRE_VIS_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_VIS_EFFLQ = 1;
        public static final int DFL_ACCPRE_VIS_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_VIS_EFFLQ = 8;
        public static final int DFL_ACCPRE_VIS_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_VIS_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_VIS_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
