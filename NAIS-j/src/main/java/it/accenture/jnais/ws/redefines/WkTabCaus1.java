package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WK-TAB-CAUS1<br>
 * Variable: WK-TAB-CAUS1 from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkTabCaus1 extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_TRAN_MAXOCCURS = 1250;

    //==== CONSTRUCTORS ====
    public WkTabCaus1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_TAB_CAUS1;
    }

    public String getWkTabCaus1Formatted() {
        return readFixedString(Pos.WK_TAB_CAUS1, Len.WK_TAB_CAUS1);
    }

    public void setTcIdTrch(int tcIdTrchIdx, int tcIdTrch) {
        int position = Pos.wkTcIdTrch(tcIdTrchIdx - 1);
        writeIntAsPacked(position, tcIdTrch, Len.Int.TC_ID_TRCH);
    }

    /**Original name: WK-TC-ID-TRCH<br>*/
    public int getTcIdTrch(int tcIdTrchIdx) {
        int position = Pos.wkTcIdTrch(tcIdTrchIdx - 1);
        return readPackedAsInt(position, Len.Int.TC_ID_TRCH);
    }

    public void setTcTpStatDisinv(int tcTpStatDisinvIdx, String tcTpStatDisinv) {
        int position = Pos.wkTcTpStatDisinv(tcTpStatDisinvIdx - 1);
        writeString(position, tcTpStatDisinv, Len.TC_TP_STAT_DISINV);
    }

    /**Original name: WK-TC-TP-STAT-DISINV<br>*/
    public String getTcTpStatDisinv(int tcTpStatDisinvIdx) {
        int position = Pos.wkTcTpStatDisinv(tcTpStatDisinvIdx - 1);
        return readString(position, Len.TC_TP_STAT_DISINV);
    }

    public void setRestoTabCaus1(String restoTabCaus1) {
        writeString(Pos.RESTO_TAB_CAUS1, restoTabCaus1, Len.RESTO_TAB_CAUS1);
    }

    /**Original name: WK-RESTO-TAB-CAUS1<br>*/
    public String getRestoTabCaus1() {
        return readString(Pos.RESTO_TAB_CAUS1, Len.RESTO_TAB_CAUS1);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_TAB_CAUS1 = 1;
        public static final int WK_TAB_CAUS1_R = 1;
        public static final int FLR1 = WK_TAB_CAUS1_R;
        public static final int RESTO_TAB_CAUS1 = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wkTabTran(int idx) {
            return WK_TAB_CAUS1 + idx * Len.TAB_TRAN;
        }

        public static int wkTcIdTrch(int idx) {
            return wkTabTran(idx);
        }

        public static int wkTcTpStatDisinv(int idx) {
            return wkTcIdTrch(idx) + Len.TC_ID_TRCH;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TC_ID_TRCH = 5;
        public static final int TC_TP_STAT_DISINV = 2;
        public static final int TAB_TRAN = TC_ID_TRCH + TC_TP_STAT_DISINV;
        public static final int FLR1 = 7;
        public static final int WK_TAB_CAUS1 = WkTabCaus1.TAB_TRAN_MAXOCCURS * TAB_TRAN;
        public static final int RESTO_TAB_CAUS1 = 8743;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TC_ID_TRCH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
