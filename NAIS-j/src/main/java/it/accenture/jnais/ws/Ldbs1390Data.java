package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.commons.data.to.IAnagDatoMatrValVar;
import it.accenture.jnais.copy.AnagDatoLdbs1390;
import it.accenture.jnais.copy.IndAnagDato;
import it.accenture.jnais.copy.IndMatrValVar;
import it.accenture.jnais.copy.MatrValVar;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS1390<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs1390Data implements IAnagDatoMatrValVar {

    //==== PROPERTIES ====
    //Original name: WK-IND-SERV
    private short wkIndServ = ((short)0);
    //Original name: WS-SQLCODE
    private int wsSqlcode = 0;
    //Original name: MATR-VAL-VAR
    private MatrValVar matrValVar = new MatrValVar();
    //Original name: IND-MATR-VAL-VAR
    private IndMatrValVar indMatrValVar = new IndMatrValVar();
    //Original name: ANAG-DATO
    private AnagDatoLdbs1390 anagDato = new AnagDatoLdbs1390();
    //Original name: IND-ANAG-DATO
    private IndAnagDato indAnagDato = new IndAnagDato();

    //==== METHODS ====
    public void setWkIndServ(short wkIndServ) {
        this.wkIndServ = wkIndServ;
    }

    public short getWkIndServ() {
        return this.wkIndServ;
    }

    public void setWsSqlcode(int wsSqlcode) {
        this.wsSqlcode = wsSqlcode;
    }

    public int getWsSqlcode() {
        return this.wsSqlcode;
    }

    @Override
    public String getAdaFormattazioneDato() {
        return anagDato.getAdaFormattazioneDato();
    }

    @Override
    public void setAdaFormattazioneDato(String adaFormattazioneDato) {
        this.anagDato.setAdaFormattazioneDato(adaFormattazioneDato);
    }

    @Override
    public String getAdaFormattazioneDatoObj() {
        if (indAnagDato.getFormattazioneDato() >= 0) {
            return getAdaFormattazioneDato();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdaFormattazioneDatoObj(String adaFormattazioneDatoObj) {
        if (adaFormattazioneDatoObj != null) {
            setAdaFormattazioneDato(adaFormattazioneDatoObj);
            indAnagDato.setFormattazioneDato(((short)0));
        }
        else {
            indAnagDato.setFormattazioneDato(((short)-1));
        }
    }

    @Override
    public int getAdaLunghezzaDato() {
        return anagDato.getAdaLunghezzaDato().getAdaLunghezzaDato();
    }

    @Override
    public void setAdaLunghezzaDato(int adaLunghezzaDato) {
        this.anagDato.getAdaLunghezzaDato().setAdaLunghezzaDato(adaLunghezzaDato);
    }

    @Override
    public Integer getAdaLunghezzaDatoObj() {
        if (indAnagDato.getLunghezzaDato() >= 0) {
            return ((Integer)getAdaLunghezzaDato());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdaLunghezzaDatoObj(Integer adaLunghezzaDatoObj) {
        if (adaLunghezzaDatoObj != null) {
            setAdaLunghezzaDato(((int)adaLunghezzaDatoObj));
            indAnagDato.setLunghezzaDato(((short)0));
        }
        else {
            indAnagDato.setLunghezzaDato(((short)-1));
        }
    }

    @Override
    public short getAdaPrecisioneDato() {
        return anagDato.getAdaPrecisioneDato().getAdaPrecisioneDato();
    }

    @Override
    public void setAdaPrecisioneDato(short adaPrecisioneDato) {
        this.anagDato.getAdaPrecisioneDato().setAdaPrecisioneDato(adaPrecisioneDato);
    }

    @Override
    public Short getAdaPrecisioneDatoObj() {
        if (indAnagDato.getPrecisioneDato() >= 0) {
            return ((Short)getAdaPrecisioneDato());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdaPrecisioneDatoObj(Short adaPrecisioneDatoObj) {
        if (adaPrecisioneDatoObj != null) {
            setAdaPrecisioneDato(((short)adaPrecisioneDatoObj));
            indAnagDato.setPrecisioneDato(((short)0));
        }
        else {
            indAnagDato.setPrecisioneDato(((short)-1));
        }
    }

    @Override
    public String getAdaTipoDato() {
        return anagDato.getAdaTipoDato();
    }

    @Override
    public void setAdaTipoDato(String adaTipoDato) {
        this.anagDato.setAdaTipoDato(adaTipoDato);
    }

    @Override
    public String getAdaTipoDatoObj() {
        if (indAnagDato.getTipoDato() >= 0) {
            return getAdaTipoDato();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdaTipoDatoObj(String adaTipoDatoObj) {
        if (adaTipoDatoObj != null) {
            setAdaTipoDato(adaTipoDatoObj);
            indAnagDato.setTipoDato(((short)0));
        }
        else {
            indAnagDato.setTipoDato(((short)-1));
        }
    }

    public AnagDatoLdbs1390 getAnagDato() {
        return anagDato;
    }

    public IndAnagDato getIndAnagDato() {
        return indAnagDato;
    }

    public IndMatrValVar getIndMatrValVar() {
        return indMatrValVar;
    }

    public MatrValVar getMatrValVar() {
        return matrValVar;
    }

    @Override
    public String getMvvCodDatoExt() {
        return matrValVar.getMvvCodDatoExt();
    }

    @Override
    public void setMvvCodDatoExt(String mvvCodDatoExt) {
        this.matrValVar.setMvvCodDatoExt(mvvCodDatoExt);
    }

    @Override
    public String getMvvCodDatoExtObj() {
        if (indMatrValVar.getCodDatoExt() >= 0) {
            return getMvvCodDatoExt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvCodDatoExtObj(String mvvCodDatoExtObj) {
        if (mvvCodDatoExtObj != null) {
            setMvvCodDatoExt(mvvCodDatoExtObj);
            indMatrValVar.setCodDatoExt(((short)0));
        }
        else {
            indMatrValVar.setCodDatoExt(((short)-1));
        }
    }

    @Override
    public String getMvvCodDatoInterno() {
        return matrValVar.getMvvCodDatoInterno();
    }

    @Override
    public void setMvvCodDatoInterno(String mvvCodDatoInterno) {
        this.matrValVar.setMvvCodDatoInterno(mvvCodDatoInterno);
    }

    @Override
    public String getMvvCodDatoInternoObj() {
        if (indMatrValVar.getCodDatoInterno() >= 0) {
            return getMvvCodDatoInterno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvCodDatoInternoObj(String mvvCodDatoInternoObj) {
        if (mvvCodDatoInternoObj != null) {
            setMvvCodDatoInterno(mvvCodDatoInternoObj);
            indMatrValVar.setCodDatoInterno(((short)0));
        }
        else {
            indMatrValVar.setCodDatoInterno(((short)-1));
        }
    }

    @Override
    public String getMvvCodDatoPtf() {
        return matrValVar.getMvvCodDatoPtf();
    }

    @Override
    public void setMvvCodDatoPtf(String mvvCodDatoPtf) {
        this.matrValVar.setMvvCodDatoPtf(mvvCodDatoPtf);
    }

    @Override
    public String getMvvCodDatoPtfObj() {
        if (indMatrValVar.getCodDatoPtf() >= 0) {
            return getMvvCodDatoPtf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvCodDatoPtfObj(String mvvCodDatoPtfObj) {
        if (mvvCodDatoPtfObj != null) {
            setMvvCodDatoPtf(mvvCodDatoPtfObj);
            indMatrValVar.setCodDatoPtf(((short)0));
        }
        else {
            indMatrValVar.setCodDatoPtf(((short)-1));
        }
    }

    @Override
    public String getMvvCodParametro() {
        return matrValVar.getMvvCodParametro();
    }

    @Override
    public void setMvvCodParametro(String mvvCodParametro) {
        this.matrValVar.setMvvCodParametro(mvvCodParametro);
    }

    @Override
    public String getMvvCodParametroObj() {
        if (indMatrValVar.getCodParametro() >= 0) {
            return getMvvCodParametro();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvCodParametroObj(String mvvCodParametroObj) {
        if (mvvCodParametroObj != null) {
            setMvvCodParametro(mvvCodParametroObj);
            indMatrValVar.setCodParametro(((short)0));
        }
        else {
            indMatrValVar.setCodParametro(((short)-1));
        }
    }

    @Override
    public String getMvvCodStrDatoPtf() {
        return matrValVar.getMvvCodStrDatoPtf();
    }

    @Override
    public void setMvvCodStrDatoPtf(String mvvCodStrDatoPtf) {
        this.matrValVar.setMvvCodStrDatoPtf(mvvCodStrDatoPtf);
    }

    @Override
    public String getMvvCodStrDatoPtfObj() {
        if (indMatrValVar.getCodStrDatoPtf() >= 0) {
            return getMvvCodStrDatoPtf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvCodStrDatoPtfObj(String mvvCodStrDatoPtfObj) {
        if (mvvCodStrDatoPtfObj != null) {
            setMvvCodStrDatoPtf(mvvCodStrDatoPtfObj);
            indMatrValVar.setCodStrDatoPtf(((short)0));
        }
        else {
            indMatrValVar.setCodStrDatoPtf(((short)-1));
        }
    }

    @Override
    public int getMvvIdMatrValVar() {
        return matrValVar.getMvvIdMatrValVar();
    }

    @Override
    public void setMvvIdMatrValVar(int mvvIdMatrValVar) {
        this.matrValVar.setMvvIdMatrValVar(mvvIdMatrValVar);
    }

    @Override
    public int getMvvIdpMatrValVar() {
        return matrValVar.getMvvIdpMatrValVar().getMvvIdpMatrValVar();
    }

    @Override
    public void setMvvIdpMatrValVar(int mvvIdpMatrValVar) {
        this.matrValVar.getMvvIdpMatrValVar().setMvvIdpMatrValVar(mvvIdpMatrValVar);
    }

    @Override
    public Integer getMvvIdpMatrValVarObj() {
        if (indMatrValVar.getIdpMatrValVar() >= 0) {
            return ((Integer)getMvvIdpMatrValVar());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvIdpMatrValVarObj(Integer mvvIdpMatrValVarObj) {
        if (mvvIdpMatrValVarObj != null) {
            setMvvIdpMatrValVar(((int)mvvIdpMatrValVarObj));
            indMatrValVar.setIdpMatrValVar(((short)0));
        }
        else {
            indMatrValVar.setIdpMatrValVar(((short)-1));
        }
    }

    @Override
    public String getMvvLivelloOperazione() {
        return matrValVar.getMvvLivelloOperazione();
    }

    @Override
    public void setMvvLivelloOperazione(String mvvLivelloOperazione) {
        this.matrValVar.setMvvLivelloOperazione(mvvLivelloOperazione);
    }

    @Override
    public String getMvvLivelloOperazioneObj() {
        if (indMatrValVar.getLivelloOperazione() >= 0) {
            return getMvvLivelloOperazione();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvLivelloOperazioneObj(String mvvLivelloOperazioneObj) {
        if (mvvLivelloOperazioneObj != null) {
            setMvvLivelloOperazione(mvvLivelloOperazioneObj);
            indMatrValVar.setLivelloOperazione(((short)0));
        }
        else {
            indMatrValVar.setLivelloOperazione(((short)-1));
        }
    }

    @Override
    public String getMvvModuloCalcolo() {
        return matrValVar.getMvvModuloCalcolo();
    }

    @Override
    public void setMvvModuloCalcolo(String mvvModuloCalcolo) {
        this.matrValVar.setMvvModuloCalcolo(mvvModuloCalcolo);
    }

    @Override
    public String getMvvModuloCalcoloObj() {
        if (indMatrValVar.getModuloCalcolo() >= 0) {
            return getMvvModuloCalcolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvModuloCalcoloObj(String mvvModuloCalcoloObj) {
        if (mvvModuloCalcoloObj != null) {
            setMvvModuloCalcolo(mvvModuloCalcoloObj);
            indMatrValVar.setModuloCalcolo(((short)0));
        }
        else {
            indMatrValVar.setModuloCalcolo(((short)-1));
        }
    }

    @Override
    public char getMvvObbligatorieta() {
        return matrValVar.getMvvObbligatorieta();
    }

    @Override
    public void setMvvObbligatorieta(char mvvObbligatorieta) {
        this.matrValVar.setMvvObbligatorieta(mvvObbligatorieta);
    }

    @Override
    public Character getMvvObbligatorietaObj() {
        if (indMatrValVar.getObbligatorieta() >= 0) {
            return ((Character)getMvvObbligatorieta());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvObbligatorietaObj(Character mvvObbligatorietaObj) {
        if (mvvObbligatorietaObj != null) {
            setMvvObbligatorieta(((char)mvvObbligatorietaObj));
            indMatrValVar.setObbligatorieta(((short)0));
        }
        else {
            indMatrValVar.setObbligatorieta(((short)-1));
        }
    }

    @Override
    public String getMvvOperazione() {
        return matrValVar.getMvvOperazione();
    }

    @Override
    public void setMvvOperazione(String mvvOperazione) {
        this.matrValVar.setMvvOperazione(mvvOperazione);
    }

    @Override
    public String getMvvOperazioneObj() {
        if (indMatrValVar.getOperazione() >= 0) {
            return getMvvOperazione();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvOperazioneObj(String mvvOperazioneObj) {
        if (mvvOperazioneObj != null) {
            setMvvOperazione(mvvOperazioneObj);
            indMatrValVar.setOperazione(((short)0));
        }
        else {
            indMatrValVar.setOperazione(((short)-1));
        }
    }

    @Override
    public String getMvvServizioLettura() {
        return matrValVar.getMvvServizioLettura();
    }

    @Override
    public void setMvvServizioLettura(String mvvServizioLettura) {
        this.matrValVar.setMvvServizioLettura(mvvServizioLettura);
    }

    @Override
    public String getMvvServizioLetturaObj() {
        if (indMatrValVar.getServizioLettura() >= 0) {
            return getMvvServizioLettura();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvServizioLetturaObj(String mvvServizioLetturaObj) {
        if (mvvServizioLetturaObj != null) {
            setMvvServizioLettura(mvvServizioLetturaObj);
            indMatrValVar.setServizioLettura(((short)0));
        }
        else {
            indMatrValVar.setServizioLettura(((short)-1));
        }
    }

    @Override
    public int getMvvTipoMovimento() {
        return matrValVar.getMvvTipoMovimento().getMvvTipoMovimento();
    }

    @Override
    public void setMvvTipoMovimento(int mvvTipoMovimento) {
        this.matrValVar.getMvvTipoMovimento().setMvvTipoMovimento(mvvTipoMovimento);
    }

    @Override
    public Integer getMvvTipoMovimentoObj() {
        if (indMatrValVar.getTipoMovimento() >= 0) {
            return ((Integer)getMvvTipoMovimento());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvTipoMovimentoObj(Integer mvvTipoMovimentoObj) {
        if (mvvTipoMovimentoObj != null) {
            setMvvTipoMovimento(((int)mvvTipoMovimentoObj));
            indMatrValVar.setTipoMovimento(((short)0));
        }
        else {
            indMatrValVar.setTipoMovimento(((short)-1));
        }
    }

    @Override
    public String getMvvTipoOggetto() {
        return matrValVar.getMvvTipoOggetto();
    }

    @Override
    public void setMvvTipoOggetto(String mvvTipoOggetto) {
        this.matrValVar.setMvvTipoOggetto(mvvTipoOggetto);
    }

    @Override
    public String getMvvTipoOggettoObj() {
        if (indMatrValVar.getTipoOggetto() >= 0) {
            return getMvvTipoOggetto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvTipoOggettoObj(String mvvTipoOggettoObj) {
        if (mvvTipoOggettoObj != null) {
            setMvvTipoOggetto(mvvTipoOggettoObj);
            indMatrValVar.setTipoOggetto(((short)0));
        }
        else {
            indMatrValVar.setTipoOggetto(((short)-1));
        }
    }

    @Override
    public String getMvvValoreDefault() {
        return matrValVar.getMvvValoreDefault();
    }

    @Override
    public void setMvvValoreDefault(String mvvValoreDefault) {
        this.matrValVar.setMvvValoreDefault(mvvValoreDefault);
    }

    @Override
    public String getMvvValoreDefaultObj() {
        if (indMatrValVar.getValoreDefault() >= 0) {
            return getMvvValoreDefault();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvValoreDefaultObj(String mvvValoreDefaultObj) {
        if (mvvValoreDefaultObj != null) {
            setMvvValoreDefault(mvvValoreDefaultObj);
            indMatrValVar.setValoreDefault(((short)0));
        }
        else {
            indMatrValVar.setValoreDefault(((short)-1));
        }
    }

    @Override
    public String getMvvWhereConditionVchar() {
        return matrValVar.getMvvWhereConditionVcharFormatted();
    }

    @Override
    public void setMvvWhereConditionVchar(String mvvWhereConditionVchar) {
        this.matrValVar.setMvvWhereConditionVcharFormatted(mvvWhereConditionVchar);
    }

    @Override
    public String getMvvWhereConditionVcharObj() {
        if (indMatrValVar.getWhereCondition() >= 0) {
            return getMvvWhereConditionVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMvvWhereConditionVcharObj(String mvvWhereConditionVcharObj) {
        if (mvvWhereConditionVcharObj != null) {
            setMvvWhereConditionVchar(mvvWhereConditionVcharObj);
            indMatrValVar.setWhereCondition(((short)0));
        }
        else {
            indMatrValVar.setWhereCondition(((short)-1));
        }
    }
}
