package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WS-TAB-LIQUI-COMUN<br>
 * Variable: WS-TAB-LIQUI-COMUN from program LVVS0089<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsTabLiquiComun extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int EL_TAB_LIQUI_COMUN_MAXOCCURS = 50;

    //==== CONSTRUCTORS ====
    public WsTabLiquiComun() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_TAB_LIQUI_COMUN;
    }

    @Override
    public void init() {
        int position = 1;
        writeString(position, "05010", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "03012", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "05006", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "05005", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "05002", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "03009", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "05018", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "03019", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "05026", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "06009", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "05015", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "03017", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "02319", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "02318", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "02325", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "02324", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "02317", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "02316", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "02323", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "02322", Len.FLR1);
    }

    /**Original name: WS-MOVI-LIQUID<br>*/
    public int getMoviLiquid(int moviLiquidIdx) {
        int position = Pos.wsMoviLiquid(moviLiquidIdx - 1);
        return readNumDispUnsignedInt(position, Len.MOVI_LIQUID);
    }

    public String getMoviLiquidFormatted(int moviLiquidIdx) {
        int position = Pos.wsMoviLiquid(moviLiquidIdx - 1);
        return readFixedString(position, Len.MOVI_LIQUID);
    }

    /**Original name: WS-MOVI-COMUN<br>*/
    public int getMoviComun(int moviComunIdx) {
        int position = Pos.wsMoviComun(moviComunIdx - 1);
        return readNumDispUnsignedInt(position, Len.MOVI_COMUN);
    }

    public String getMoviComunFormatted(int moviComunIdx) {
        int position = Pos.wsMoviComun(moviComunIdx - 1);
        return readFixedString(position, Len.MOVI_COMUN);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_TAB_LIQUI_COMUN = 1;
        public static final int FLR1 = WS_TAB_LIQUI_COMUN;
        public static final int FLR2 = FLR1 + Len.FLR1;
        public static final int FLR3 = FLR2 + Len.FLR1;
        public static final int FLR4 = FLR3 + Len.FLR1;
        public static final int FLR5 = FLR4 + Len.FLR1;
        public static final int FLR6 = FLR5 + Len.FLR1;
        public static final int FLR7 = FLR6 + Len.FLR1;
        public static final int FLR8 = FLR7 + Len.FLR1;
        public static final int FLR9 = FLR8 + Len.FLR1;
        public static final int FLR10 = FLR9 + Len.FLR1;
        public static final int FLR11 = FLR10 + Len.FLR1;
        public static final int FLR12 = FLR11 + Len.FLR1;
        public static final int FLR13 = FLR12 + Len.FLR1;
        public static final int FLR14 = FLR13 + Len.FLR1;
        public static final int FLR15 = FLR14 + Len.FLR1;
        public static final int FLR16 = FLR15 + Len.FLR1;
        public static final int FLR17 = FLR16 + Len.FLR1;
        public static final int FLR18 = FLR17 + Len.FLR1;
        public static final int FLR19 = FLR18 + Len.FLR1;
        public static final int FLR20 = FLR19 + Len.FLR1;
        public static final int FLR21 = FLR20 + Len.FLR1;
        public static final int WS_TAB_LIQUI_COMUN_R = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wsElTabLiquiComun(int idx) {
            return WS_TAB_LIQUI_COMUN_R + idx * Len.EL_TAB_LIQUI_COMUN;
        }

        public static int wsMoviLiquid(int idx) {
            return wsElTabLiquiComun(idx);
        }

        public static int wsMoviComun(int idx) {
            return wsMoviLiquid(idx) + Len.MOVI_LIQUID;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 5;
        public static final int MOVI_LIQUID = 5;
        public static final int MOVI_COMUN = 5;
        public static final int EL_TAB_LIQUI_COMUN = MOVI_LIQUID + MOVI_COMUN;
        public static final int FLR21 = 400;
        public static final int WS_TAB_LIQUI_COMUN = FLR21 + 20 * FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
