package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-RICOR<br>
 * Variable: WPAG-IMP-RICOR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_RICOR;
    }

    public void setWpagImpRicor(AfDecimal wpagImpRicor) {
        writeDecimalAsPacked(Pos.WPAG_IMP_RICOR, wpagImpRicor.copy());
    }

    public void setWpagImpRicorFormatted(String wpagImpRicor) {
        setWpagImpRicor(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_RICOR + Len.Fract.WPAG_IMP_RICOR, Len.Fract.WPAG_IMP_RICOR, wpagImpRicor));
    }

    public void setWpagImpRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_RICOR, Pos.WPAG_IMP_RICOR);
    }

    /**Original name: WPAG-IMP-RICOR<br>*/
    public AfDecimal getWpagImpRicor() {
        return readPackedAsDecimal(Pos.WPAG_IMP_RICOR, Len.Int.WPAG_IMP_RICOR, Len.Fract.WPAG_IMP_RICOR);
    }

    public byte[] getWpagImpRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_RICOR, Pos.WPAG_IMP_RICOR);
        return buffer;
    }

    public void initWpagImpRicorSpaces() {
        fill(Pos.WPAG_IMP_RICOR, Len.WPAG_IMP_RICOR, Types.SPACE_CHAR);
    }

    public void setWpagImpRicorNull(String wpagImpRicorNull) {
        writeString(Pos.WPAG_IMP_RICOR_NULL, wpagImpRicorNull, Len.WPAG_IMP_RICOR_NULL);
    }

    /**Original name: WPAG-IMP-RICOR-NULL<br>*/
    public String getWpagImpRicorNull() {
        return readString(Pos.WPAG_IMP_RICOR_NULL, Len.WPAG_IMP_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_RICOR = 1;
        public static final int WPAG_IMP_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_RICOR = 8;
        public static final int WPAG_IMP_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
