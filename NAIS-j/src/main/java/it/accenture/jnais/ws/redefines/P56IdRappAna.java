package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-ID-RAPP-ANA<br>
 * Variable: P56-ID-RAPP-ANA from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56IdRappAna extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56IdRappAna() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_ID_RAPP_ANA;
    }

    public void setP56IdRappAna(int p56IdRappAna) {
        writeIntAsPacked(Pos.P56_ID_RAPP_ANA, p56IdRappAna, Len.Int.P56_ID_RAPP_ANA);
    }

    public void setP56IdRappAnaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_ID_RAPP_ANA, Pos.P56_ID_RAPP_ANA);
    }

    /**Original name: P56-ID-RAPP-ANA<br>*/
    public int getP56IdRappAna() {
        return readPackedAsInt(Pos.P56_ID_RAPP_ANA, Len.Int.P56_ID_RAPP_ANA);
    }

    public byte[] getP56IdRappAnaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_ID_RAPP_ANA, Pos.P56_ID_RAPP_ANA);
        return buffer;
    }

    public void setP56IdRappAnaNull(String p56IdRappAnaNull) {
        writeString(Pos.P56_ID_RAPP_ANA_NULL, p56IdRappAnaNull, Len.P56_ID_RAPP_ANA_NULL);
    }

    /**Original name: P56-ID-RAPP-ANA-NULL<br>*/
    public String getP56IdRappAnaNull() {
        return readString(Pos.P56_ID_RAPP_ANA_NULL, Len.P56_ID_RAPP_ANA_NULL);
    }

    public String getP56IdRappAnaNullFormatted() {
        return Functions.padBlanks(getP56IdRappAnaNull(), Len.P56_ID_RAPP_ANA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_ID_RAPP_ANA = 1;
        public static final int P56_ID_RAPP_ANA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_ID_RAPP_ANA = 5;
        public static final int P56_ID_RAPP_ANA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_ID_RAPP_ANA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
