package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-IMP-AZ<br>
 * Variable: TDR-IMP-AZ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_IMP_AZ;
    }

    public void setTdrImpAz(AfDecimal tdrImpAz) {
        writeDecimalAsPacked(Pos.TDR_IMP_AZ, tdrImpAz.copy());
    }

    public void setTdrImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_IMP_AZ, Pos.TDR_IMP_AZ);
    }

    /**Original name: TDR-IMP-AZ<br>*/
    public AfDecimal getTdrImpAz() {
        return readPackedAsDecimal(Pos.TDR_IMP_AZ, Len.Int.TDR_IMP_AZ, Len.Fract.TDR_IMP_AZ);
    }

    public byte[] getTdrImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_IMP_AZ, Pos.TDR_IMP_AZ);
        return buffer;
    }

    public void setTdrImpAzNull(String tdrImpAzNull) {
        writeString(Pos.TDR_IMP_AZ_NULL, tdrImpAzNull, Len.TDR_IMP_AZ_NULL);
    }

    /**Original name: TDR-IMP-AZ-NULL<br>*/
    public String getTdrImpAzNull() {
        return readString(Pos.TDR_IMP_AZ_NULL, Len.TDR_IMP_AZ_NULL);
    }

    public String getTdrImpAzNullFormatted() {
        return Functions.padBlanks(getTdrImpAzNull(), Len.TDR_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_IMP_AZ = 1;
        public static final int TDR_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_IMP_AZ = 8;
        public static final int TDR_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
