package it.accenture.jnais.ws.enums;

/**Original name: IABV0008-LENGTH-DATA-GATES<br>
 * Variable: IABV0008-LENGTH-DATA-GATES from copybook IABV0008<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0008LengthDataGates {

    //==== PROPERTIES ====
    private String value = "10K";
    public static final String DATA1K = "1K";
    public static final String DATA2K = "2K";
    public static final String DATA3K = "3K";
    public static final String DATA10K = "10K";

    //==== METHODS ====
    public String getLengthDataGates() {
        return this.value;
    }
}
