package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-PREST-FAIVL<br>
 * Variable: WRST-RIS-PREST-FAIVL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisPrestFaivl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisPrestFaivl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_PREST_FAIVL;
    }

    public void setWrstRisPrestFaivl(AfDecimal wrstRisPrestFaivl) {
        writeDecimalAsPacked(Pos.WRST_RIS_PREST_FAIVL, wrstRisPrestFaivl.copy());
    }

    public void setWrstRisPrestFaivlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_PREST_FAIVL, Pos.WRST_RIS_PREST_FAIVL);
    }

    /**Original name: WRST-RIS-PREST-FAIVL<br>*/
    public AfDecimal getWrstRisPrestFaivl() {
        return readPackedAsDecimal(Pos.WRST_RIS_PREST_FAIVL, Len.Int.WRST_RIS_PREST_FAIVL, Len.Fract.WRST_RIS_PREST_FAIVL);
    }

    public byte[] getWrstRisPrestFaivlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_PREST_FAIVL, Pos.WRST_RIS_PREST_FAIVL);
        return buffer;
    }

    public void initWrstRisPrestFaivlSpaces() {
        fill(Pos.WRST_RIS_PREST_FAIVL, Len.WRST_RIS_PREST_FAIVL, Types.SPACE_CHAR);
    }

    public void setWrstRisPrestFaivlNull(String wrstRisPrestFaivlNull) {
        writeString(Pos.WRST_RIS_PREST_FAIVL_NULL, wrstRisPrestFaivlNull, Len.WRST_RIS_PREST_FAIVL_NULL);
    }

    /**Original name: WRST-RIS-PREST-FAIVL-NULL<br>*/
    public String getWrstRisPrestFaivlNull() {
        return readString(Pos.WRST_RIS_PREST_FAIVL_NULL, Len.WRST_RIS_PREST_FAIVL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_PREST_FAIVL = 1;
        public static final int WRST_RIS_PREST_FAIVL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_PREST_FAIVL = 8;
        public static final int WRST_RIS_PREST_FAIVL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_PREST_FAIVL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_PREST_FAIVL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
