package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WS-VALORE-APP<br>
 * Variable: WS-VALORE-APP from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsValoreApp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsValoreApp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_VALORE_APP;
    }

    public void setWsValoreApp(long wsValoreApp) {
        writeLong(Pos.WS_VALORE_APP, wsValoreApp, Len.Int.WS_VALORE_APP, SignType.NO_SIGN);
    }

    public void setWsValoreAppFormatted(String wsValoreApp) {
        writeString(Pos.WS_VALORE_APP, Trunc.toUnsignedNumeric(wsValoreApp, Len.WS_VALORE_APP), Len.WS_VALORE_APP);
    }

    /**Original name: WS-VALORE-APP<br>*/
    public long getWsValoreApp() {
        return readNumDispUnsignedLong(Pos.WS_VALORE_APP, Len.WS_VALORE_APP);
    }

    /**Original name: WS-VALORE-STR<br>*/
    public String getWsValoreStr() {
        return readString(Pos.WS_VALORE_STR, Len.WS_VALORE_STR);
    }

    public String getWsValoreStrFormatted() {
        return Functions.padBlanks(getWsValoreStr(), Len.WS_VALORE_STR);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_VALORE_APP = 1;
        public static final int WS_VALORE_STR = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_VALORE_APP = 18;
        public static final int WS_VALORE_STR = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WS_VALORE_APP = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
