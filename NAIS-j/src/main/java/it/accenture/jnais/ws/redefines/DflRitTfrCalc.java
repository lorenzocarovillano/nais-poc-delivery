package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RIT-TFR-CALC<br>
 * Variable: DFL-RIT-TFR-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflRitTfrCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflRitTfrCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RIT_TFR_CALC;
    }

    public void setDflRitTfrCalc(AfDecimal dflRitTfrCalc) {
        writeDecimalAsPacked(Pos.DFL_RIT_TFR_CALC, dflRitTfrCalc.copy());
    }

    public void setDflRitTfrCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RIT_TFR_CALC, Pos.DFL_RIT_TFR_CALC);
    }

    /**Original name: DFL-RIT-TFR-CALC<br>*/
    public AfDecimal getDflRitTfrCalc() {
        return readPackedAsDecimal(Pos.DFL_RIT_TFR_CALC, Len.Int.DFL_RIT_TFR_CALC, Len.Fract.DFL_RIT_TFR_CALC);
    }

    public byte[] getDflRitTfrCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RIT_TFR_CALC, Pos.DFL_RIT_TFR_CALC);
        return buffer;
    }

    public void setDflRitTfrCalcNull(String dflRitTfrCalcNull) {
        writeString(Pos.DFL_RIT_TFR_CALC_NULL, dflRitTfrCalcNull, Len.DFL_RIT_TFR_CALC_NULL);
    }

    /**Original name: DFL-RIT-TFR-CALC-NULL<br>*/
    public String getDflRitTfrCalcNull() {
        return readString(Pos.DFL_RIT_TFR_CALC_NULL, Len.DFL_RIT_TFR_CALC_NULL);
    }

    public String getDflRitTfrCalcNullFormatted() {
        return Functions.padBlanks(getDflRitTfrCalcNull(), Len.DFL_RIT_TFR_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RIT_TFR_CALC = 1;
        public static final int DFL_RIT_TFR_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RIT_TFR_CALC = 8;
        public static final int DFL_RIT_TFR_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RIT_TFR_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RIT_TFR_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
