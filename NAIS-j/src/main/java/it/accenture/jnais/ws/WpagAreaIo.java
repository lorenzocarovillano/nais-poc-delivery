package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.WpagDatiInput;

/**Original name: WPAG-AREA-IO<br>
 * Variable: WPAG-AREA-IO from program IDSS0160<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WpagAreaIo extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WPAG-DATI-INPUT
    private WpagDatiInput datiInput = new WpagDatiInput();
    /**Original name: WPAG-IB-OGGETTO<br>
	 * <pre>-- IB OGGETTO RESTITUITO IN OUTPUT</pre>*/
    private String ibOggetto = DefaultValues.stringVal(Len.IB_OGGETTO);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_AREA_IO;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWpagAreaIoBytes(buf);
    }

    public String getAreaEstrazioneIbFormatted() {
        return MarshalByteExt.bufferToStr(getWpagAreaIoBytes());
    }

    public void setWpagAreaIoBytes(byte[] buffer) {
        setWpagAreaIoBytes(buffer, 1);
    }

    public byte[] getWpagAreaIoBytes() {
        byte[] buffer = new byte[Len.WPAG_AREA_IO];
        return getWpagAreaIoBytes(buffer, 1);
    }

    public void setWpagAreaIoBytes(byte[] buffer, int offset) {
        int position = offset;
        datiInput.setDatiInputBytes(buffer, position);
        position += WpagDatiInput.Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
    }

    public byte[] getWpagAreaIoBytes(byte[] buffer, int offset) {
        int position = offset;
        datiInput.getDatiInputBytes(buffer, position);
        position += WpagDatiInput.Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        ibOggetto = MarshalByte.readString(buffer, position, Len.IB_OGGETTO);
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ibOggetto, Len.IB_OGGETTO);
        return buffer;
    }

    public void setIbOggetto(String ibOggetto) {
        this.ibOggetto = Functions.subString(ibOggetto, Len.IB_OGGETTO);
    }

    public String getIbOggetto() {
        return this.ibOggetto;
    }

    public WpagDatiInput getDatiInput() {
        return datiInput;
    }

    @Override
    public byte[] serialize() {
        return getWpagAreaIoBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IB_OGGETTO = 40;
        public static final int DATI_OUTPUT = IB_OGGETTO;
        public static final int WPAG_AREA_IO = WpagDatiInput.Len.DATI_INPUT + DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
