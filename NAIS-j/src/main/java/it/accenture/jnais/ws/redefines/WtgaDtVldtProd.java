package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-DT-VLDT-PROD<br>
 * Variable: WTGA-DT-VLDT-PROD from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaDtVldtProd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaDtVldtProd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_DT_VLDT_PROD;
    }

    public void setWtgaDtVldtProd(int wtgaDtVldtProd) {
        writeIntAsPacked(Pos.WTGA_DT_VLDT_PROD, wtgaDtVldtProd, Len.Int.WTGA_DT_VLDT_PROD);
    }

    public void setWtgaDtVldtProdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_DT_VLDT_PROD, Pos.WTGA_DT_VLDT_PROD);
    }

    /**Original name: WTGA-DT-VLDT-PROD<br>*/
    public int getWtgaDtVldtProd() {
        return readPackedAsInt(Pos.WTGA_DT_VLDT_PROD, Len.Int.WTGA_DT_VLDT_PROD);
    }

    public byte[] getWtgaDtVldtProdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_DT_VLDT_PROD, Pos.WTGA_DT_VLDT_PROD);
        return buffer;
    }

    public void initWtgaDtVldtProdSpaces() {
        fill(Pos.WTGA_DT_VLDT_PROD, Len.WTGA_DT_VLDT_PROD, Types.SPACE_CHAR);
    }

    public void setWtgaDtVldtProdNull(String wtgaDtVldtProdNull) {
        writeString(Pos.WTGA_DT_VLDT_PROD_NULL, wtgaDtVldtProdNull, Len.WTGA_DT_VLDT_PROD_NULL);
    }

    /**Original name: WTGA-DT-VLDT-PROD-NULL<br>*/
    public String getWtgaDtVldtProdNull() {
        return readString(Pos.WTGA_DT_VLDT_PROD_NULL, Len.WTGA_DT_VLDT_PROD_NULL);
    }

    public String getWtgaDtVldtProdNullFormatted() {
        return Functions.padBlanks(getWtgaDtVldtProdNull(), Len.WTGA_DT_VLDT_PROD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_DT_VLDT_PROD = 1;
        public static final int WTGA_DT_VLDT_PROD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_DT_VLDT_PROD = 5;
        public static final int WTGA_DT_VLDT_PROD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_DT_VLDT_PROD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
