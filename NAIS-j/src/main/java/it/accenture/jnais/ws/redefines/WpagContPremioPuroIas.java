package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-PREMIO-PURO-IAS<br>
 * Variable: WPAG-CONT-PREMIO-PURO-IAS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContPremioPuroIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContPremioPuroIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_PREMIO_PURO_IAS;
    }

    public void setWpagContPremioPuroIas(AfDecimal wpagContPremioPuroIas) {
        writeDecimalAsPacked(Pos.WPAG_CONT_PREMIO_PURO_IAS, wpagContPremioPuroIas.copy());
    }

    public void setWpagContPremioPuroIasFormatted(String wpagContPremioPuroIas) {
        setWpagContPremioPuroIas(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_PREMIO_PURO_IAS + Len.Fract.WPAG_CONT_PREMIO_PURO_IAS, Len.Fract.WPAG_CONT_PREMIO_PURO_IAS, wpagContPremioPuroIas));
    }

    public void setWpagContPremioPuroIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_PREMIO_PURO_IAS, Pos.WPAG_CONT_PREMIO_PURO_IAS);
    }

    /**Original name: WPAG-CONT-PREMIO-PURO-IAS<br>*/
    public AfDecimal getWpagContPremioPuroIas() {
        return readPackedAsDecimal(Pos.WPAG_CONT_PREMIO_PURO_IAS, Len.Int.WPAG_CONT_PREMIO_PURO_IAS, Len.Fract.WPAG_CONT_PREMIO_PURO_IAS);
    }

    public byte[] getWpagContPremioPuroIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_PREMIO_PURO_IAS, Pos.WPAG_CONT_PREMIO_PURO_IAS);
        return buffer;
    }

    public void initWpagContPremioPuroIasSpaces() {
        fill(Pos.WPAG_CONT_PREMIO_PURO_IAS, Len.WPAG_CONT_PREMIO_PURO_IAS, Types.SPACE_CHAR);
    }

    public void setWpagContPremioPuroIasNull(String wpagContPremioPuroIasNull) {
        writeString(Pos.WPAG_CONT_PREMIO_PURO_IAS_NULL, wpagContPremioPuroIasNull, Len.WPAG_CONT_PREMIO_PURO_IAS_NULL);
    }

    /**Original name: WPAG-CONT-PREMIO-PURO-IAS-NULL<br>*/
    public String getWpagContPremioPuroIasNull() {
        return readString(Pos.WPAG_CONT_PREMIO_PURO_IAS_NULL, Len.WPAG_CONT_PREMIO_PURO_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_PREMIO_PURO_IAS = 1;
        public static final int WPAG_CONT_PREMIO_PURO_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_PREMIO_PURO_IAS = 8;
        public static final int WPAG_CONT_PREMIO_PURO_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_PREMIO_PURO_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_PREMIO_PURO_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
