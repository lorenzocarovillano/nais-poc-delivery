package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RIT-TFR-DFZ<br>
 * Variable: WDFL-RIT-TFR-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflRitTfrDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflRitTfrDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RIT_TFR_DFZ;
    }

    public void setWdflRitTfrDfz(AfDecimal wdflRitTfrDfz) {
        writeDecimalAsPacked(Pos.WDFL_RIT_TFR_DFZ, wdflRitTfrDfz.copy());
    }

    public void setWdflRitTfrDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RIT_TFR_DFZ, Pos.WDFL_RIT_TFR_DFZ);
    }

    /**Original name: WDFL-RIT-TFR-DFZ<br>*/
    public AfDecimal getWdflRitTfrDfz() {
        return readPackedAsDecimal(Pos.WDFL_RIT_TFR_DFZ, Len.Int.WDFL_RIT_TFR_DFZ, Len.Fract.WDFL_RIT_TFR_DFZ);
    }

    public byte[] getWdflRitTfrDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RIT_TFR_DFZ, Pos.WDFL_RIT_TFR_DFZ);
        return buffer;
    }

    public void setWdflRitTfrDfzNull(String wdflRitTfrDfzNull) {
        writeString(Pos.WDFL_RIT_TFR_DFZ_NULL, wdflRitTfrDfzNull, Len.WDFL_RIT_TFR_DFZ_NULL);
    }

    /**Original name: WDFL-RIT-TFR-DFZ-NULL<br>*/
    public String getWdflRitTfrDfzNull() {
        return readString(Pos.WDFL_RIT_TFR_DFZ_NULL, Len.WDFL_RIT_TFR_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_TFR_DFZ = 1;
        public static final int WDFL_RIT_TFR_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_TFR_DFZ = 8;
        public static final int WDFL_RIT_TFR_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_TFR_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_TFR_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
