package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-PRE-NET<br>
 * Variable: WDTC-PRE-NET from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcPreNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcPreNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_PRE_NET;
    }

    public void setWdtcPreNet(AfDecimal wdtcPreNet) {
        writeDecimalAsPacked(Pos.WDTC_PRE_NET, wdtcPreNet.copy());
    }

    public void setWdtcPreNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_PRE_NET, Pos.WDTC_PRE_NET);
    }

    /**Original name: WDTC-PRE-NET<br>*/
    public AfDecimal getWdtcPreNet() {
        return readPackedAsDecimal(Pos.WDTC_PRE_NET, Len.Int.WDTC_PRE_NET, Len.Fract.WDTC_PRE_NET);
    }

    public byte[] getWdtcPreNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_PRE_NET, Pos.WDTC_PRE_NET);
        return buffer;
    }

    public void initWdtcPreNetSpaces() {
        fill(Pos.WDTC_PRE_NET, Len.WDTC_PRE_NET, Types.SPACE_CHAR);
    }

    public void setWdtcPreNetNull(String wdtcPreNetNull) {
        writeString(Pos.WDTC_PRE_NET_NULL, wdtcPreNetNull, Len.WDTC_PRE_NET_NULL);
    }

    /**Original name: WDTC-PRE-NET-NULL<br>*/
    public String getWdtcPreNetNull() {
        return readString(Pos.WDTC_PRE_NET_NULL, Len.WDTC_PRE_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_PRE_NET = 1;
        public static final int WDTC_PRE_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_PRE_NET = 8;
        public static final int WDTC_PRE_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_PRE_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_PRE_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
