package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-DT-MAN-COP<br>
 * Variable: P67-DT-MAN-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67DtManCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67DtManCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_DT_MAN_COP;
    }

    public void setP67DtManCop(int p67DtManCop) {
        writeIntAsPacked(Pos.P67_DT_MAN_COP, p67DtManCop, Len.Int.P67_DT_MAN_COP);
    }

    public void setP67DtManCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_DT_MAN_COP, Pos.P67_DT_MAN_COP);
    }

    /**Original name: P67-DT-MAN-COP<br>*/
    public int getP67DtManCop() {
        return readPackedAsInt(Pos.P67_DT_MAN_COP, Len.Int.P67_DT_MAN_COP);
    }

    public byte[] getP67DtManCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_DT_MAN_COP, Pos.P67_DT_MAN_COP);
        return buffer;
    }

    public void setP67DtManCopNull(String p67DtManCopNull) {
        writeString(Pos.P67_DT_MAN_COP_NULL, p67DtManCopNull, Len.P67_DT_MAN_COP_NULL);
    }

    /**Original name: P67-DT-MAN-COP-NULL<br>*/
    public String getP67DtManCopNull() {
        return readString(Pos.P67_DT_MAN_COP_NULL, Len.P67_DT_MAN_COP_NULL);
    }

    public String getP67DtManCopNullFormatted() {
        return Functions.padBlanks(getP67DtManCopNull(), Len.P67_DT_MAN_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_DT_MAN_COP = 1;
        public static final int P67_DT_MAN_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_DT_MAN_COP = 5;
        public static final int P67_DT_MAN_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_DT_MAN_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
