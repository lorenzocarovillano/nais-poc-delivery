package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.redefines.WstbTab;

/**Original name: WSTB-AREA-STB<br>
 * Variable: WSTB-AREA-STB from program LLBS0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WstbAreaStb {

    //==== PROPERTIES ====
    //Original name: WSTB-ELE-STB-MAX
    private short wstbEleStbMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WSTB-TAB
    private WstbTab wstbTab = new WstbTab();

    //==== METHODS ====
    public void setWstbEleStbMax(short wstbEleStbMax) {
        this.wstbEleStbMax = wstbEleStbMax;
    }

    public short getWstbEleStbMax() {
        return this.wstbEleStbMax;
    }

    public WstbTab getWstbTab() {
        return wstbTab;
    }
}
