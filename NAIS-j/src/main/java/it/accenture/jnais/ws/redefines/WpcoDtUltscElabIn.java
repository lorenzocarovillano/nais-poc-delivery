package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTSC-ELAB-IN<br>
 * Variable: WPCO-DT-ULTSC-ELAB-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltscElabIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltscElabIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTSC_ELAB_IN;
    }

    public void setWpcoDtUltscElabIn(int wpcoDtUltscElabIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULTSC_ELAB_IN, wpcoDtUltscElabIn, Len.Int.WPCO_DT_ULTSC_ELAB_IN);
    }

    public void setDpcoDtUltscElabInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTSC_ELAB_IN, Pos.WPCO_DT_ULTSC_ELAB_IN);
    }

    /**Original name: WPCO-DT-ULTSC-ELAB-IN<br>*/
    public int getWpcoDtUltscElabIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULTSC_ELAB_IN, Len.Int.WPCO_DT_ULTSC_ELAB_IN);
    }

    public byte[] getWpcoDtUltscElabInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTSC_ELAB_IN, Pos.WPCO_DT_ULTSC_ELAB_IN);
        return buffer;
    }

    public void setWpcoDtUltscElabInNull(String wpcoDtUltscElabInNull) {
        writeString(Pos.WPCO_DT_ULTSC_ELAB_IN_NULL, wpcoDtUltscElabInNull, Len.WPCO_DT_ULTSC_ELAB_IN_NULL);
    }

    /**Original name: WPCO-DT-ULTSC-ELAB-IN-NULL<br>*/
    public String getWpcoDtUltscElabInNull() {
        return readString(Pos.WPCO_DT_ULTSC_ELAB_IN_NULL, Len.WPCO_DT_ULTSC_ELAB_IN_NULL);
    }

    public String getWpcoDtUltscElabInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltscElabInNull(), Len.WPCO_DT_ULTSC_ELAB_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTSC_ELAB_IN = 1;
        public static final int WPCO_DT_ULTSC_ELAB_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTSC_ELAB_IN = 5;
        public static final int WPCO_DT_ULTSC_ELAB_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTSC_ELAB_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
