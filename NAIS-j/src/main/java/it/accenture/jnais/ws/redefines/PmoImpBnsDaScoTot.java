package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-IMP-BNS-DA-SCO-TOT<br>
 * Variable: PMO-IMP-BNS-DA-SCO-TOT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoImpBnsDaScoTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoImpBnsDaScoTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_IMP_BNS_DA_SCO_TOT;
    }

    public void setPmoImpBnsDaScoTot(AfDecimal pmoImpBnsDaScoTot) {
        writeDecimalAsPacked(Pos.PMO_IMP_BNS_DA_SCO_TOT, pmoImpBnsDaScoTot.copy());
    }

    public void setPmoImpBnsDaScoTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_IMP_BNS_DA_SCO_TOT, Pos.PMO_IMP_BNS_DA_SCO_TOT);
    }

    /**Original name: PMO-IMP-BNS-DA-SCO-TOT<br>*/
    public AfDecimal getPmoImpBnsDaScoTot() {
        return readPackedAsDecimal(Pos.PMO_IMP_BNS_DA_SCO_TOT, Len.Int.PMO_IMP_BNS_DA_SCO_TOT, Len.Fract.PMO_IMP_BNS_DA_SCO_TOT);
    }

    public byte[] getPmoImpBnsDaScoTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_IMP_BNS_DA_SCO_TOT, Pos.PMO_IMP_BNS_DA_SCO_TOT);
        return buffer;
    }

    public void initPmoImpBnsDaScoTotHighValues() {
        fill(Pos.PMO_IMP_BNS_DA_SCO_TOT, Len.PMO_IMP_BNS_DA_SCO_TOT, Types.HIGH_CHAR_VAL);
    }

    public void setPmoImpBnsDaScoTotNull(String pmoImpBnsDaScoTotNull) {
        writeString(Pos.PMO_IMP_BNS_DA_SCO_TOT_NULL, pmoImpBnsDaScoTotNull, Len.PMO_IMP_BNS_DA_SCO_TOT_NULL);
    }

    /**Original name: PMO-IMP-BNS-DA-SCO-TOT-NULL<br>*/
    public String getPmoImpBnsDaScoTotNull() {
        return readString(Pos.PMO_IMP_BNS_DA_SCO_TOT_NULL, Len.PMO_IMP_BNS_DA_SCO_TOT_NULL);
    }

    public String getPmoImpBnsDaScoTotNullFormatted() {
        return Functions.padBlanks(getPmoImpBnsDaScoTotNull(), Len.PMO_IMP_BNS_DA_SCO_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_IMP_BNS_DA_SCO_TOT = 1;
        public static final int PMO_IMP_BNS_DA_SCO_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_IMP_BNS_DA_SCO_TOT = 8;
        public static final int PMO_IMP_BNS_DA_SCO_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_IMP_BNS_DA_SCO_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_IMP_BNS_DA_SCO_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
