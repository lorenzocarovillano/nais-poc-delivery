package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-DUR-AA<br>
 * Variable: WTGA-DUR-AA from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaDurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaDurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_DUR_AA;
    }

    public void setWtgaDurAa(int wtgaDurAa) {
        writeIntAsPacked(Pos.WTGA_DUR_AA, wtgaDurAa, Len.Int.WTGA_DUR_AA);
    }

    public void setWtgaDurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_DUR_AA, Pos.WTGA_DUR_AA);
    }

    /**Original name: WTGA-DUR-AA<br>*/
    public int getWtgaDurAa() {
        return readPackedAsInt(Pos.WTGA_DUR_AA, Len.Int.WTGA_DUR_AA);
    }

    public byte[] getWtgaDurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_DUR_AA, Pos.WTGA_DUR_AA);
        return buffer;
    }

    public void initWtgaDurAaSpaces() {
        fill(Pos.WTGA_DUR_AA, Len.WTGA_DUR_AA, Types.SPACE_CHAR);
    }

    public void setWtgaDurAaNull(String wtgaDurAaNull) {
        writeString(Pos.WTGA_DUR_AA_NULL, wtgaDurAaNull, Len.WTGA_DUR_AA_NULL);
    }

    /**Original name: WTGA-DUR-AA-NULL<br>*/
    public String getWtgaDurAaNull() {
        return readString(Pos.WTGA_DUR_AA_NULL, Len.WTGA_DUR_AA_NULL);
    }

    public String getWtgaDurAaNullFormatted() {
        return Functions.padBlanks(getWtgaDurAaNull(), Len.WTGA_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_DUR_AA = 1;
        public static final int WTGA_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_DUR_AA = 3;
        public static final int WTGA_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
