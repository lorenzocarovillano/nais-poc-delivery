package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMPB-REMUN-ASS<br>
 * Variable: WTGA-IMPB-REMUN-ASS from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpbRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpbRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMPB_REMUN_ASS;
    }

    public void setWtgaImpbRemunAss(AfDecimal wtgaImpbRemunAss) {
        writeDecimalAsPacked(Pos.WTGA_IMPB_REMUN_ASS, wtgaImpbRemunAss.copy());
    }

    public void setWtgaImpbRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMPB_REMUN_ASS, Pos.WTGA_IMPB_REMUN_ASS);
    }

    /**Original name: WTGA-IMPB-REMUN-ASS<br>*/
    public AfDecimal getWtgaImpbRemunAss() {
        return readPackedAsDecimal(Pos.WTGA_IMPB_REMUN_ASS, Len.Int.WTGA_IMPB_REMUN_ASS, Len.Fract.WTGA_IMPB_REMUN_ASS);
    }

    public byte[] getWtgaImpbRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMPB_REMUN_ASS, Pos.WTGA_IMPB_REMUN_ASS);
        return buffer;
    }

    public void initWtgaImpbRemunAssSpaces() {
        fill(Pos.WTGA_IMPB_REMUN_ASS, Len.WTGA_IMPB_REMUN_ASS, Types.SPACE_CHAR);
    }

    public void setWtgaImpbRemunAssNull(String wtgaImpbRemunAssNull) {
        writeString(Pos.WTGA_IMPB_REMUN_ASS_NULL, wtgaImpbRemunAssNull, Len.WTGA_IMPB_REMUN_ASS_NULL);
    }

    /**Original name: WTGA-IMPB-REMUN-ASS-NULL<br>*/
    public String getWtgaImpbRemunAssNull() {
        return readString(Pos.WTGA_IMPB_REMUN_ASS_NULL, Len.WTGA_IMPB_REMUN_ASS_NULL);
    }

    public String getWtgaImpbRemunAssNullFormatted() {
        return Functions.padBlanks(getWtgaImpbRemunAssNull(), Len.WTGA_IMPB_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMPB_REMUN_ASS = 1;
        public static final int WTGA_IMPB_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMPB_REMUN_ASS = 8;
        public static final int WTGA_IMPB_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMPB_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMPB_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
