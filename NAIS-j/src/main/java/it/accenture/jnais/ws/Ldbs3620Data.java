package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import it.accenture.jnais.copy.Idbvtli3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndImpstSost;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS3620<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs3620Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-TRCH-LIQ
    private IndImpstSost indTrchLiq = new IndImpstSost();
    //Original name: IDBVTLI3
    private Idbvtli3 idbvtli3 = new Idbvtli3();
    //Original name: LDBV3621-ID-MOVI-CRZ
    private int ldbv3621IdMoviCrz = DefaultValues.INT_VAL;

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setLdbv3621Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV3621];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV3621);
        setLdbv3621Bytes(buffer, 1);
    }

    public String getLdbv3621Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv3621Bytes());
    }

    /**Original name: LDBV3621<br>*/
    public byte[] getLdbv3621Bytes() {
        byte[] buffer = new byte[Len.LDBV3621];
        return getLdbv3621Bytes(buffer, 1);
    }

    public void setLdbv3621Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv3621IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV3621_ID_MOVI_CRZ, 0);
    }

    public byte[] getLdbv3621Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv3621IdMoviCrz, Len.Int.LDBV3621_ID_MOVI_CRZ, 0);
        return buffer;
    }

    public void setLdbv3621IdMoviCrz(int ldbv3621IdMoviCrz) {
        this.ldbv3621IdMoviCrz = ldbv3621IdMoviCrz;
    }

    public int getLdbv3621IdMoviCrz() {
        return this.ldbv3621IdMoviCrz;
    }

    public Idbvtli3 getIdbvtli3() {
        return idbvtli3;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndImpstSost getIndTrchLiq() {
        return indTrchLiq;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int LDBV3621_ID_MOVI_CRZ = 5;
        public static final int LDBV3621 = LDBV3621_ID_MOVI_CRZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV3621_ID_MOVI_CRZ = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
