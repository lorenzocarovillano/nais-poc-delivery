package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMPST-SOST-662014<br>
 * Variable: WBEL-IMPST-SOST-662014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpstSost662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpstSost662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMPST_SOST662014;
    }

    public void setWbelImpstSost662014(AfDecimal wbelImpstSost662014) {
        writeDecimalAsPacked(Pos.WBEL_IMPST_SOST662014, wbelImpstSost662014.copy());
    }

    public void setWbelImpstSost662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMPST_SOST662014, Pos.WBEL_IMPST_SOST662014);
    }

    /**Original name: WBEL-IMPST-SOST-662014<br>*/
    public AfDecimal getWbelImpstSost662014() {
        return readPackedAsDecimal(Pos.WBEL_IMPST_SOST662014, Len.Int.WBEL_IMPST_SOST662014, Len.Fract.WBEL_IMPST_SOST662014);
    }

    public byte[] getWbelImpstSost662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMPST_SOST662014, Pos.WBEL_IMPST_SOST662014);
        return buffer;
    }

    public void initWbelImpstSost662014Spaces() {
        fill(Pos.WBEL_IMPST_SOST662014, Len.WBEL_IMPST_SOST662014, Types.SPACE_CHAR);
    }

    public void setWbelImpstSost662014Null(String wbelImpstSost662014Null) {
        writeString(Pos.WBEL_IMPST_SOST662014_NULL, wbelImpstSost662014Null, Len.WBEL_IMPST_SOST662014_NULL);
    }

    /**Original name: WBEL-IMPST-SOST-662014-NULL<br>*/
    public String getWbelImpstSost662014Null() {
        return readString(Pos.WBEL_IMPST_SOST662014_NULL, Len.WBEL_IMPST_SOST662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_SOST662014 = 1;
        public static final int WBEL_IMPST_SOST662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_SOST662014 = 8;
        public static final int WBEL_IMPST_SOST662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_SOST662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_SOST662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
