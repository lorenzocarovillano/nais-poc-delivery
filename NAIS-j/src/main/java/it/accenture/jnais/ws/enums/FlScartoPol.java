package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FL-SCARTO-POL<br>
 * Variable: FL-SCARTO-POL from program LCCS0033<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlScartoPol {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlScartoPol(char flScartoPol) {
        this.value = flScartoPol;
    }

    public char getFlScartoPol() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
