package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMP-INTR-RIT-PAG-C<br>
 * Variable: WDFL-IMP-INTR-RIT-PAG-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpIntrRitPagC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpIntrRitPagC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMP_INTR_RIT_PAG_C;
    }

    public void setWdflImpIntrRitPagC(AfDecimal wdflImpIntrRitPagC) {
        writeDecimalAsPacked(Pos.WDFL_IMP_INTR_RIT_PAG_C, wdflImpIntrRitPagC.copy());
    }

    public void setWdflImpIntrRitPagCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMP_INTR_RIT_PAG_C, Pos.WDFL_IMP_INTR_RIT_PAG_C);
    }

    /**Original name: WDFL-IMP-INTR-RIT-PAG-C<br>*/
    public AfDecimal getWdflImpIntrRitPagC() {
        return readPackedAsDecimal(Pos.WDFL_IMP_INTR_RIT_PAG_C, Len.Int.WDFL_IMP_INTR_RIT_PAG_C, Len.Fract.WDFL_IMP_INTR_RIT_PAG_C);
    }

    public byte[] getWdflImpIntrRitPagCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMP_INTR_RIT_PAG_C, Pos.WDFL_IMP_INTR_RIT_PAG_C);
        return buffer;
    }

    public void setWdflImpIntrRitPagCNull(String wdflImpIntrRitPagCNull) {
        writeString(Pos.WDFL_IMP_INTR_RIT_PAG_C_NULL, wdflImpIntrRitPagCNull, Len.WDFL_IMP_INTR_RIT_PAG_C_NULL);
    }

    /**Original name: WDFL-IMP-INTR-RIT-PAG-C-NULL<br>*/
    public String getWdflImpIntrRitPagCNull() {
        return readString(Pos.WDFL_IMP_INTR_RIT_PAG_C_NULL, Len.WDFL_IMP_INTR_RIT_PAG_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_INTR_RIT_PAG_C = 1;
        public static final int WDFL_IMP_INTR_RIT_PAG_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_INTR_RIT_PAG_C = 8;
        public static final int WDFL_IMP_INTR_RIT_PAG_C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_INTR_RIT_PAG_C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_INTR_RIT_PAG_C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
