package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-COD-SOVRAP<br>
 * Variable: WK-COD-SOVRAP from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkCodSovrap {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_COD_SOVRAP);
    public static final String SOPSAN = "SA";
    public static final String SOPPRO = "PR";
    public static final String SOPSPO = "SP";
    public static final String SOPTEC = "TE";
    public static final String SOPALT = "AL";

    //==== METHODS ====
    public void setWkCodSovrap(String wkCodSovrap) {
        this.value = Functions.subString(wkCodSovrap, Len.WK_COD_SOVRAP);
    }

    public String getWkCodSovrap() {
        return this.value;
    }

    public void setSopsan() {
        value = SOPSAN;
    }

    public void setSoppro() {
        value = SOPPRO;
    }

    public void setSopspo() {
        value = SOPSPO;
    }

    public void setSoptec() {
        value = SOPTEC;
    }

    public void setSopalt() {
        value = SOPALT;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_COD_SOVRAP = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
