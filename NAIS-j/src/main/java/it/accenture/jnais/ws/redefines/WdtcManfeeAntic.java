package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-MANFEE-ANTIC<br>
 * Variable: WDTC-MANFEE-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_MANFEE_ANTIC;
    }

    public void setWdtcManfeeAntic(AfDecimal wdtcManfeeAntic) {
        writeDecimalAsPacked(Pos.WDTC_MANFEE_ANTIC, wdtcManfeeAntic.copy());
    }

    public void setWdtcManfeeAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_MANFEE_ANTIC, Pos.WDTC_MANFEE_ANTIC);
    }

    /**Original name: WDTC-MANFEE-ANTIC<br>*/
    public AfDecimal getWdtcManfeeAntic() {
        return readPackedAsDecimal(Pos.WDTC_MANFEE_ANTIC, Len.Int.WDTC_MANFEE_ANTIC, Len.Fract.WDTC_MANFEE_ANTIC);
    }

    public byte[] getWdtcManfeeAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_MANFEE_ANTIC, Pos.WDTC_MANFEE_ANTIC);
        return buffer;
    }

    public void initWdtcManfeeAnticSpaces() {
        fill(Pos.WDTC_MANFEE_ANTIC, Len.WDTC_MANFEE_ANTIC, Types.SPACE_CHAR);
    }

    public void setWdtcManfeeAnticNull(String wdtcManfeeAnticNull) {
        writeString(Pos.WDTC_MANFEE_ANTIC_NULL, wdtcManfeeAnticNull, Len.WDTC_MANFEE_ANTIC_NULL);
    }

    /**Original name: WDTC-MANFEE-ANTIC-NULL<br>*/
    public String getWdtcManfeeAnticNull() {
        return readString(Pos.WDTC_MANFEE_ANTIC_NULL, Len.WDTC_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_MANFEE_ANTIC = 1;
        public static final int WDTC_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_MANFEE_ANTIC = 8;
        public static final int WDTC_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
