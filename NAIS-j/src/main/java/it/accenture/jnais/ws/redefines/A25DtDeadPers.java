package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: A25-DT-DEAD-PERS<br>
 * Variable: A25-DT-DEAD-PERS from program LDBS1300<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A25DtDeadPers extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A25DtDeadPers() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A25_DT_DEAD_PERS;
    }

    public void setA25DtDeadPers(int a25DtDeadPers) {
        writeIntAsPacked(Pos.A25_DT_DEAD_PERS, a25DtDeadPers, Len.Int.A25_DT_DEAD_PERS);
    }

    public void setA25DtDeadPersFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A25_DT_DEAD_PERS, Pos.A25_DT_DEAD_PERS);
    }

    /**Original name: A25-DT-DEAD-PERS<br>*/
    public int getA25DtDeadPers() {
        return readPackedAsInt(Pos.A25_DT_DEAD_PERS, Len.Int.A25_DT_DEAD_PERS);
    }

    public byte[] getA25DtDeadPersAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A25_DT_DEAD_PERS, Pos.A25_DT_DEAD_PERS);
        return buffer;
    }

    public void setA25DtDeadPersNull(String a25DtDeadPersNull) {
        writeString(Pos.A25_DT_DEAD_PERS_NULL, a25DtDeadPersNull, Len.A25_DT_DEAD_PERS_NULL);
    }

    /**Original name: A25-DT-DEAD-PERS-NULL<br>*/
    public String getA25DtDeadPersNull() {
        return readString(Pos.A25_DT_DEAD_PERS_NULL, Len.A25_DT_DEAD_PERS_NULL);
    }

    public String getA25DtDeadPersNullFormatted() {
        return Functions.padBlanks(getA25DtDeadPersNull(), Len.A25_DT_DEAD_PERS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A25_DT_DEAD_PERS = 1;
        public static final int A25_DT_DEAD_PERS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_DT_DEAD_PERS = 5;
        public static final int A25_DT_DEAD_PERS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_DT_DEAD_PERS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
