package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPOL-DIR-QUIET<br>
 * Variable: WPOL-DIR-QUIET from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDirQuiet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDirQuiet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DIR_QUIET;
    }

    public void setWpolDirQuiet(AfDecimal wpolDirQuiet) {
        writeDecimalAsPacked(Pos.WPOL_DIR_QUIET, wpolDirQuiet.copy());
    }

    public void setWpolDirQuietFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DIR_QUIET, Pos.WPOL_DIR_QUIET);
    }

    /**Original name: WPOL-DIR-QUIET<br>*/
    public AfDecimal getWpolDirQuiet() {
        return readPackedAsDecimal(Pos.WPOL_DIR_QUIET, Len.Int.WPOL_DIR_QUIET, Len.Fract.WPOL_DIR_QUIET);
    }

    public byte[] getWpolDirQuietAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DIR_QUIET, Pos.WPOL_DIR_QUIET);
        return buffer;
    }

    public void setWpolDirQuietNull(String wpolDirQuietNull) {
        writeString(Pos.WPOL_DIR_QUIET_NULL, wpolDirQuietNull, Len.WPOL_DIR_QUIET_NULL);
    }

    /**Original name: WPOL-DIR-QUIET-NULL<br>*/
    public String getWpolDirQuietNull() {
        return readString(Pos.WPOL_DIR_QUIET_NULL, Len.WPOL_DIR_QUIET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DIR_QUIET = 1;
        public static final int WPOL_DIR_QUIET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DIR_QUIET = 8;
        public static final int WPOL_DIR_QUIET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPOL_DIR_QUIET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DIR_QUIET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
