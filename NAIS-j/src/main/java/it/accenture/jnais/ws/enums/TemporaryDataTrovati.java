package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: TEMPORARY-DATA-TROVATI<br>
 * Variable: TEMPORARY-DATA-TROVATI from program IDSS0300<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TemporaryDataTrovati {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setTemporaryDataTrovati(char temporaryDataTrovati) {
        this.value = temporaryDataTrovati;
    }

    public char getTemporaryDataTrovati() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
