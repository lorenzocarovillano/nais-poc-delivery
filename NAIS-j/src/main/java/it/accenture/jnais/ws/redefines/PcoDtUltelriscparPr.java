package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTELRISCPAR-PR<br>
 * Variable: PCO-DT-ULTELRISCPAR-PR from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltelriscparPr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltelriscparPr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTELRISCPAR_PR;
    }

    public void setPcoDtUltelriscparPr(int pcoDtUltelriscparPr) {
        writeIntAsPacked(Pos.PCO_DT_ULTELRISCPAR_PR, pcoDtUltelriscparPr, Len.Int.PCO_DT_ULTELRISCPAR_PR);
    }

    public void setPcoDtUltelriscparPrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTELRISCPAR_PR, Pos.PCO_DT_ULTELRISCPAR_PR);
    }

    /**Original name: PCO-DT-ULTELRISCPAR-PR<br>*/
    public int getPcoDtUltelriscparPr() {
        return readPackedAsInt(Pos.PCO_DT_ULTELRISCPAR_PR, Len.Int.PCO_DT_ULTELRISCPAR_PR);
    }

    public byte[] getPcoDtUltelriscparPrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTELRISCPAR_PR, Pos.PCO_DT_ULTELRISCPAR_PR);
        return buffer;
    }

    public void initPcoDtUltelriscparPrHighValues() {
        fill(Pos.PCO_DT_ULTELRISCPAR_PR, Len.PCO_DT_ULTELRISCPAR_PR, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltelriscparPrNull(String pcoDtUltelriscparPrNull) {
        writeString(Pos.PCO_DT_ULTELRISCPAR_PR_NULL, pcoDtUltelriscparPrNull, Len.PCO_DT_ULTELRISCPAR_PR_NULL);
    }

    /**Original name: PCO-DT-ULTELRISCPAR-PR-NULL<br>*/
    public String getPcoDtUltelriscparPrNull() {
        return readString(Pos.PCO_DT_ULTELRISCPAR_PR_NULL, Len.PCO_DT_ULTELRISCPAR_PR_NULL);
    }

    public String getPcoDtUltelriscparPrNullFormatted() {
        return Functions.padBlanks(getPcoDtUltelriscparPrNull(), Len.PCO_DT_ULTELRISCPAR_PR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTELRISCPAR_PR = 1;
        public static final int PCO_DT_ULTELRISCPAR_PR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTELRISCPAR_PR = 5;
        public static final int PCO_DT_ULTELRISCPAR_PR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTELRISCPAR_PR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
