package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WVAS-IMP-MOVTO<br>
 * Variable: WVAS-IMP-MOVTO from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasImpMovto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasImpMovto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_IMP_MOVTO;
    }

    public void setWvasImpMovto(AfDecimal wvasImpMovto) {
        writeDecimalAsPacked(Pos.WVAS_IMP_MOVTO, wvasImpMovto.copy());
    }

    /**Original name: WVAS-IMP-MOVTO<br>*/
    public AfDecimal getWvasImpMovto() {
        return readPackedAsDecimal(Pos.WVAS_IMP_MOVTO, Len.Int.WVAS_IMP_MOVTO, Len.Fract.WVAS_IMP_MOVTO);
    }

    public void setWvasImpMovtoNull(String wvasImpMovtoNull) {
        writeString(Pos.WVAS_IMP_MOVTO_NULL, wvasImpMovtoNull, Len.WVAS_IMP_MOVTO_NULL);
    }

    /**Original name: WVAS-IMP-MOVTO-NULL<br>*/
    public String getWvasImpMovtoNull() {
        return readString(Pos.WVAS_IMP_MOVTO_NULL, Len.WVAS_IMP_MOVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_IMP_MOVTO = 1;
        public static final int WVAS_IMP_MOVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_IMP_MOVTO = 8;
        public static final int WVAS_IMP_MOVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WVAS_IMP_MOVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_IMP_MOVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
