package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-CAR-GEST<br>
 * Variable: WDTR-CAR-GEST from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_CAR_GEST;
    }

    public void setWdtrCarGest(AfDecimal wdtrCarGest) {
        writeDecimalAsPacked(Pos.WDTR_CAR_GEST, wdtrCarGest.copy());
    }

    /**Original name: WDTR-CAR-GEST<br>*/
    public AfDecimal getWdtrCarGest() {
        return readPackedAsDecimal(Pos.WDTR_CAR_GEST, Len.Int.WDTR_CAR_GEST, Len.Fract.WDTR_CAR_GEST);
    }

    public void setWdtrCarGestNull(String wdtrCarGestNull) {
        writeString(Pos.WDTR_CAR_GEST_NULL, wdtrCarGestNull, Len.WDTR_CAR_GEST_NULL);
    }

    /**Original name: WDTR-CAR-GEST-NULL<br>*/
    public String getWdtrCarGestNull() {
        return readString(Pos.WDTR_CAR_GEST_NULL, Len.WDTR_CAR_GEST_NULL);
    }

    public String getWdtrCarGestNullFormatted() {
        return Functions.padBlanks(getWdtrCarGestNull(), Len.WDTR_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_CAR_GEST = 1;
        public static final int WDTR_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_CAR_GEST = 8;
        public static final int WDTR_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
