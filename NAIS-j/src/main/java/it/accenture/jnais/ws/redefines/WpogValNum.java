package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPOG-VAL-NUM<br>
 * Variable: WPOG-VAL-NUM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpogValNum extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpogValNum() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOG_VAL_NUM;
    }

    public void setWpogValNum(long wpogValNum) {
        writeLongAsPacked(Pos.WPOG_VAL_NUM, wpogValNum, Len.Int.WPOG_VAL_NUM);
    }

    public void setWpogValNumFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOG_VAL_NUM, Pos.WPOG_VAL_NUM);
    }

    /**Original name: WPOG-VAL-NUM<br>*/
    public long getWpogValNum() {
        return readPackedAsLong(Pos.WPOG_VAL_NUM, Len.Int.WPOG_VAL_NUM);
    }

    public byte[] getWpogValNumAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOG_VAL_NUM, Pos.WPOG_VAL_NUM);
        return buffer;
    }

    public void initWpogValNumSpaces() {
        fill(Pos.WPOG_VAL_NUM, Len.WPOG_VAL_NUM, Types.SPACE_CHAR);
    }

    public void setWpogValNumNull(String wpogValNumNull) {
        writeString(Pos.WPOG_VAL_NUM_NULL, wpogValNumNull, Len.WPOG_VAL_NUM_NULL);
    }

    /**Original name: WPOG-VAL-NUM-NULL<br>*/
    public String getWpogValNumNull() {
        return readString(Pos.WPOG_VAL_NUM_NULL, Len.WPOG_VAL_NUM_NULL);
    }

    public String getWpogValNumNullFormatted() {
        return Functions.padBlanks(getWpogValNumNull(), Len.WPOG_VAL_NUM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOG_VAL_NUM = 1;
        public static final int WPOG_VAL_NUM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOG_VAL_NUM = 8;
        public static final int WPOG_VAL_NUM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOG_VAL_NUM = 14;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
