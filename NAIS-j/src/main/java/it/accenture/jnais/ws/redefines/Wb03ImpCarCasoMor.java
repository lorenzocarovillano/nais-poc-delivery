package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-IMP-CAR-CASO-MOR<br>
 * Variable: WB03-IMP-CAR-CASO-MOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03ImpCarCasoMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03ImpCarCasoMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_IMP_CAR_CASO_MOR;
    }

    public void setWb03ImpCarCasoMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_IMP_CAR_CASO_MOR, Pos.WB03_IMP_CAR_CASO_MOR);
    }

    /**Original name: WB03-IMP-CAR-CASO-MOR<br>*/
    public AfDecimal getWb03ImpCarCasoMor() {
        return readPackedAsDecimal(Pos.WB03_IMP_CAR_CASO_MOR, Len.Int.WB03_IMP_CAR_CASO_MOR, Len.Fract.WB03_IMP_CAR_CASO_MOR);
    }

    public byte[] getWb03ImpCarCasoMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_IMP_CAR_CASO_MOR, Pos.WB03_IMP_CAR_CASO_MOR);
        return buffer;
    }

    public void setWb03ImpCarCasoMorNull(String wb03ImpCarCasoMorNull) {
        writeString(Pos.WB03_IMP_CAR_CASO_MOR_NULL, wb03ImpCarCasoMorNull, Len.WB03_IMP_CAR_CASO_MOR_NULL);
    }

    /**Original name: WB03-IMP-CAR-CASO-MOR-NULL<br>*/
    public String getWb03ImpCarCasoMorNull() {
        return readString(Pos.WB03_IMP_CAR_CASO_MOR_NULL, Len.WB03_IMP_CAR_CASO_MOR_NULL);
    }

    public String getWb03ImpCarCasoMorNullFormatted() {
        return Functions.padBlanks(getWb03ImpCarCasoMorNull(), Len.WB03_IMP_CAR_CASO_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_IMP_CAR_CASO_MOR = 1;
        public static final int WB03_IMP_CAR_CASO_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_IMP_CAR_CASO_MOR = 8;
        public static final int WB03_IMP_CAR_CASO_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_IMP_CAR_CASO_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_IMP_CAR_CASO_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
