package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-SPE-PC<br>
 * Variable: WPMO-SPE-PC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoSpePc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoSpePc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_SPE_PC;
    }

    public void setWpmoSpePc(AfDecimal wpmoSpePc) {
        writeDecimalAsPacked(Pos.WPMO_SPE_PC, wpmoSpePc.copy());
    }

    public void setWpmoSpePcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_SPE_PC, Pos.WPMO_SPE_PC);
    }

    /**Original name: WPMO-SPE-PC<br>*/
    public AfDecimal getWpmoSpePc() {
        return readPackedAsDecimal(Pos.WPMO_SPE_PC, Len.Int.WPMO_SPE_PC, Len.Fract.WPMO_SPE_PC);
    }

    public byte[] getWpmoSpePcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_SPE_PC, Pos.WPMO_SPE_PC);
        return buffer;
    }

    public void initWpmoSpePcSpaces() {
        fill(Pos.WPMO_SPE_PC, Len.WPMO_SPE_PC, Types.SPACE_CHAR);
    }

    public void setWpmoSpePcNull(String wpmoSpePcNull) {
        writeString(Pos.WPMO_SPE_PC_NULL, wpmoSpePcNull, Len.WPMO_SPE_PC_NULL);
    }

    /**Original name: WPMO-SPE-PC-NULL<br>*/
    public String getWpmoSpePcNull() {
        return readString(Pos.WPMO_SPE_PC_NULL, Len.WPMO_SPE_PC_NULL);
    }

    public String getWpmoSpePcNullFormatted() {
        return Functions.padBlanks(getWpmoSpePcNull(), Len.WPMO_SPE_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_SPE_PC = 1;
        public static final int WPMO_SPE_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_SPE_PC = 4;
        public static final int WPMO_SPE_PC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_SPE_PC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_SPE_PC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
