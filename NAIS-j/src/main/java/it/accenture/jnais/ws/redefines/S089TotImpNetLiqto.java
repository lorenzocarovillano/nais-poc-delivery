package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMP-NET-LIQTO<br>
 * Variable: S089-TOT-IMP-NET-LIQTO from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpNetLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpNetLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMP_NET_LIQTO;
    }

    public void setWlquTotImpNetLiqto(AfDecimal wlquTotImpNetLiqto) {
        writeDecimalAsPacked(Pos.S089_TOT_IMP_NET_LIQTO, wlquTotImpNetLiqto.copy());
    }

    public void setWlquTotImpNetLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMP_NET_LIQTO, Pos.S089_TOT_IMP_NET_LIQTO);
    }

    /**Original name: WLQU-TOT-IMP-NET-LIQTO<br>*/
    public AfDecimal getWlquTotImpNetLiqto() {
        return readPackedAsDecimal(Pos.S089_TOT_IMP_NET_LIQTO, Len.Int.WLQU_TOT_IMP_NET_LIQTO, Len.Fract.WLQU_TOT_IMP_NET_LIQTO);
    }

    public byte[] getWlquTotImpNetLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMP_NET_LIQTO, Pos.S089_TOT_IMP_NET_LIQTO);
        return buffer;
    }

    public void initWlquTotImpNetLiqtoSpaces() {
        fill(Pos.S089_TOT_IMP_NET_LIQTO, Len.S089_TOT_IMP_NET_LIQTO, Types.SPACE_CHAR);
    }

    public void setWlquTotImpNetLiqtoNull(String wlquTotImpNetLiqtoNull) {
        writeString(Pos.S089_TOT_IMP_NET_LIQTO_NULL, wlquTotImpNetLiqtoNull, Len.WLQU_TOT_IMP_NET_LIQTO_NULL);
    }

    /**Original name: WLQU-TOT-IMP-NET-LIQTO-NULL<br>*/
    public String getWlquTotImpNetLiqtoNull() {
        return readString(Pos.S089_TOT_IMP_NET_LIQTO_NULL, Len.WLQU_TOT_IMP_NET_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_NET_LIQTO = 1;
        public static final int S089_TOT_IMP_NET_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_NET_LIQTO = 8;
        public static final int WLQU_TOT_IMP_NET_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_NET_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_NET_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
