package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-ULT-COMMIS-TRASFE<br>
 * Variable: WDFA-ULT-COMMIS-TRASFE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaUltCommisTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaUltCommisTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_ULT_COMMIS_TRASFE;
    }

    public void setWdfaUltCommisTrasfe(AfDecimal wdfaUltCommisTrasfe) {
        writeDecimalAsPacked(Pos.WDFA_ULT_COMMIS_TRASFE, wdfaUltCommisTrasfe.copy());
    }

    public void setWdfaUltCommisTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_ULT_COMMIS_TRASFE, Pos.WDFA_ULT_COMMIS_TRASFE);
    }

    /**Original name: WDFA-ULT-COMMIS-TRASFE<br>*/
    public AfDecimal getWdfaUltCommisTrasfe() {
        return readPackedAsDecimal(Pos.WDFA_ULT_COMMIS_TRASFE, Len.Int.WDFA_ULT_COMMIS_TRASFE, Len.Fract.WDFA_ULT_COMMIS_TRASFE);
    }

    public byte[] getWdfaUltCommisTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_ULT_COMMIS_TRASFE, Pos.WDFA_ULT_COMMIS_TRASFE);
        return buffer;
    }

    public void setWdfaUltCommisTrasfeNull(String wdfaUltCommisTrasfeNull) {
        writeString(Pos.WDFA_ULT_COMMIS_TRASFE_NULL, wdfaUltCommisTrasfeNull, Len.WDFA_ULT_COMMIS_TRASFE_NULL);
    }

    /**Original name: WDFA-ULT-COMMIS-TRASFE-NULL<br>*/
    public String getWdfaUltCommisTrasfeNull() {
        return readString(Pos.WDFA_ULT_COMMIS_TRASFE_NULL, Len.WDFA_ULT_COMMIS_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_ULT_COMMIS_TRASFE = 1;
        public static final int WDFA_ULT_COMMIS_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_ULT_COMMIS_TRASFE = 8;
        public static final int WDFA_ULT_COMMIS_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_ULT_COMMIS_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_ULT_COMMIS_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
