package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.AtgaTab;

/**Original name: ATGA-AREA-TGA<br>
 * Variable: ATGA-AREA-TGA from program LLBS0230<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AtgaAreaTga extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: ATGA-ELE-TGA-MAX
    private short atgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: ATGA-TAB
    private AtgaTab atgaTab = new AtgaTab();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ATGA_AREA_TGA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAtgaAreaTgaBytes(buf);
    }

    public String getAtgaAreaTgaFormatted() {
        return MarshalByteExt.bufferToStr(getAtgaAreaTgaBytes());
    }

    public void setAtgaAreaTgaBytes(byte[] buffer) {
        setAtgaAreaTgaBytes(buffer, 1);
    }

    public byte[] getAtgaAreaTgaBytes() {
        byte[] buffer = new byte[Len.ATGA_AREA_TGA];
        return getAtgaAreaTgaBytes(buffer, 1);
    }

    public void setAtgaAreaTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        atgaEleTgaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        atgaTab.setAtgaTabBytes(buffer, position);
    }

    public byte[] getAtgaAreaTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, atgaEleTgaMax);
        position += Types.SHORT_SIZE;
        atgaTab.getAtgaTabBytes(buffer, position);
        return buffer;
    }

    public void setAtgaEleTgaMax(short atgaEleTgaMax) {
        this.atgaEleTgaMax = atgaEleTgaMax;
    }

    public short getAtgaEleTgaMax() {
        return this.atgaEleTgaMax;
    }

    public AtgaTab getAtgaTab() {
        return atgaTab;
    }

    @Override
    public byte[] serialize() {
        return getAtgaAreaTgaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ATGA_ELE_TGA_MAX = 2;
        public static final int ATGA_AREA_TGA = ATGA_ELE_TGA_MAX + AtgaTab.Len.ATGA_TAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
