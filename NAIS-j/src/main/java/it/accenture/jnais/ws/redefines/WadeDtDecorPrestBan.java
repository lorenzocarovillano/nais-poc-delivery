package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WADE-DT-DECOR-PREST-BAN<br>
 * Variable: WADE-DT-DECOR-PREST-BAN from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeDtDecorPrestBan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeDtDecorPrestBan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_DT_DECOR_PREST_BAN;
    }

    public void setWadeDtDecorPrestBan(int wadeDtDecorPrestBan) {
        writeIntAsPacked(Pos.WADE_DT_DECOR_PREST_BAN, wadeDtDecorPrestBan, Len.Int.WADE_DT_DECOR_PREST_BAN);
    }

    public void setWadeDtDecorPrestBanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_DT_DECOR_PREST_BAN, Pos.WADE_DT_DECOR_PREST_BAN);
    }

    /**Original name: WADE-DT-DECOR-PREST-BAN<br>*/
    public int getWadeDtDecorPrestBan() {
        return readPackedAsInt(Pos.WADE_DT_DECOR_PREST_BAN, Len.Int.WADE_DT_DECOR_PREST_BAN);
    }

    public byte[] getWadeDtDecorPrestBanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_DT_DECOR_PREST_BAN, Pos.WADE_DT_DECOR_PREST_BAN);
        return buffer;
    }

    public void initWadeDtDecorPrestBanSpaces() {
        fill(Pos.WADE_DT_DECOR_PREST_BAN, Len.WADE_DT_DECOR_PREST_BAN, Types.SPACE_CHAR);
    }

    public void setWadeDtDecorPrestBanNull(String wadeDtDecorPrestBanNull) {
        writeString(Pos.WADE_DT_DECOR_PREST_BAN_NULL, wadeDtDecorPrestBanNull, Len.WADE_DT_DECOR_PREST_BAN_NULL);
    }

    /**Original name: WADE-DT-DECOR-PREST-BAN-NULL<br>*/
    public String getWadeDtDecorPrestBanNull() {
        return readString(Pos.WADE_DT_DECOR_PREST_BAN_NULL, Len.WADE_DT_DECOR_PREST_BAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_DT_DECOR_PREST_BAN = 1;
        public static final int WADE_DT_DECOR_PREST_BAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_DT_DECOR_PREST_BAN = 5;
        public static final int WADE_DT_DECOR_PREST_BAN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_DT_DECOR_PREST_BAN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
