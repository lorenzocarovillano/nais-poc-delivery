package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-580<br>
 * Variable: FLAG-580 from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Flag580 {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlag580(char flag580) {
        this.value = flag580;
    }

    public char getFlag580() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
