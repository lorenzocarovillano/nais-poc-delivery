package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ACCPRE-ACC-CALC<br>
 * Variable: DFL-ACCPRE-ACC-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAccpreAccCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAccpreAccCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ACCPRE_ACC_CALC;
    }

    public void setDflAccpreAccCalc(AfDecimal dflAccpreAccCalc) {
        writeDecimalAsPacked(Pos.DFL_ACCPRE_ACC_CALC, dflAccpreAccCalc.copy());
    }

    public void setDflAccpreAccCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ACCPRE_ACC_CALC, Pos.DFL_ACCPRE_ACC_CALC);
    }

    /**Original name: DFL-ACCPRE-ACC-CALC<br>*/
    public AfDecimal getDflAccpreAccCalc() {
        return readPackedAsDecimal(Pos.DFL_ACCPRE_ACC_CALC, Len.Int.DFL_ACCPRE_ACC_CALC, Len.Fract.DFL_ACCPRE_ACC_CALC);
    }

    public byte[] getDflAccpreAccCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ACCPRE_ACC_CALC, Pos.DFL_ACCPRE_ACC_CALC);
        return buffer;
    }

    public void setDflAccpreAccCalcNull(String dflAccpreAccCalcNull) {
        writeString(Pos.DFL_ACCPRE_ACC_CALC_NULL, dflAccpreAccCalcNull, Len.DFL_ACCPRE_ACC_CALC_NULL);
    }

    /**Original name: DFL-ACCPRE-ACC-CALC-NULL<br>*/
    public String getDflAccpreAccCalcNull() {
        return readString(Pos.DFL_ACCPRE_ACC_CALC_NULL, Len.DFL_ACCPRE_ACC_CALC_NULL);
    }

    public String getDflAccpreAccCalcNullFormatted() {
        return Functions.padBlanks(getDflAccpreAccCalcNull(), Len.DFL_ACCPRE_ACC_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_ACC_CALC = 1;
        public static final int DFL_ACCPRE_ACC_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_ACC_CALC = 8;
        public static final int DFL_ACCPRE_ACC_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_ACC_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_ACC_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
