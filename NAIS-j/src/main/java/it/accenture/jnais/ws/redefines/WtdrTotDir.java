package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-DIR<br>
 * Variable: WTDR-TOT-DIR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotDir extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotDir() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_DIR;
    }

    public void setWtdrTotDir(AfDecimal wtdrTotDir) {
        writeDecimalAsPacked(Pos.WTDR_TOT_DIR, wtdrTotDir.copy());
    }

    /**Original name: WTDR-TOT-DIR<br>*/
    public AfDecimal getWtdrTotDir() {
        return readPackedAsDecimal(Pos.WTDR_TOT_DIR, Len.Int.WTDR_TOT_DIR, Len.Fract.WTDR_TOT_DIR);
    }

    public void setWtdrTotDirNull(String wtdrTotDirNull) {
        writeString(Pos.WTDR_TOT_DIR_NULL, wtdrTotDirNull, Len.WTDR_TOT_DIR_NULL);
    }

    /**Original name: WTDR-TOT-DIR-NULL<br>*/
    public String getWtdrTotDirNull() {
        return readString(Pos.WTDR_TOT_DIR_NULL, Len.WTDR_TOT_DIR_NULL);
    }

    public String getWtdrTotDirNullFormatted() {
        return Functions.padBlanks(getWtdrTotDirNull(), Len.WTDR_TOT_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_DIR = 1;
        public static final int WTDR_TOT_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_DIR = 8;
        public static final int WTDR_TOT_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
