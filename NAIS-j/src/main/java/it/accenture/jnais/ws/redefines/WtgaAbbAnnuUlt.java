package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ABB-ANNU-ULT<br>
 * Variable: WTGA-ABB-ANNU-ULT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaAbbAnnuUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaAbbAnnuUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ABB_ANNU_ULT;
    }

    public void setWtgaAbbAnnuUlt(AfDecimal wtgaAbbAnnuUlt) {
        writeDecimalAsPacked(Pos.WTGA_ABB_ANNU_ULT, wtgaAbbAnnuUlt.copy());
    }

    public void setWtgaAbbAnnuUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ABB_ANNU_ULT, Pos.WTGA_ABB_ANNU_ULT);
    }

    /**Original name: WTGA-ABB-ANNU-ULT<br>*/
    public AfDecimal getWtgaAbbAnnuUlt() {
        return readPackedAsDecimal(Pos.WTGA_ABB_ANNU_ULT, Len.Int.WTGA_ABB_ANNU_ULT, Len.Fract.WTGA_ABB_ANNU_ULT);
    }

    public byte[] getWtgaAbbAnnuUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ABB_ANNU_ULT, Pos.WTGA_ABB_ANNU_ULT);
        return buffer;
    }

    public void initWtgaAbbAnnuUltSpaces() {
        fill(Pos.WTGA_ABB_ANNU_ULT, Len.WTGA_ABB_ANNU_ULT, Types.SPACE_CHAR);
    }

    public void setWtgaAbbAnnuUltNull(String wtgaAbbAnnuUltNull) {
        writeString(Pos.WTGA_ABB_ANNU_ULT_NULL, wtgaAbbAnnuUltNull, Len.WTGA_ABB_ANNU_ULT_NULL);
    }

    /**Original name: WTGA-ABB-ANNU-ULT-NULL<br>*/
    public String getWtgaAbbAnnuUltNull() {
        return readString(Pos.WTGA_ABB_ANNU_ULT_NULL, Len.WTGA_ABB_ANNU_ULT_NULL);
    }

    public String getWtgaAbbAnnuUltNullFormatted() {
        return Functions.padBlanks(getWtgaAbbAnnuUltNull(), Len.WTGA_ABB_ANNU_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ABB_ANNU_ULT = 1;
        public static final int WTGA_ABB_ANNU_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ABB_ANNU_ULT = 8;
        public static final int WTGA_ABB_ANNU_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_ABB_ANNU_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ABB_ANNU_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
