package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMP-LRD-CALC-CP<br>
 * Variable: S089-IMP-LRD-CALC-CP from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpLrdCalcCp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpLrdCalcCp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMP_LRD_CALC_CP;
    }

    public void setWlquImpLrdCalcCp(AfDecimal wlquImpLrdCalcCp) {
        writeDecimalAsPacked(Pos.S089_IMP_LRD_CALC_CP, wlquImpLrdCalcCp.copy());
    }

    public void setWlquImpLrdCalcCpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMP_LRD_CALC_CP, Pos.S089_IMP_LRD_CALC_CP);
    }

    /**Original name: WLQU-IMP-LRD-CALC-CP<br>*/
    public AfDecimal getWlquImpLrdCalcCp() {
        return readPackedAsDecimal(Pos.S089_IMP_LRD_CALC_CP, Len.Int.WLQU_IMP_LRD_CALC_CP, Len.Fract.WLQU_IMP_LRD_CALC_CP);
    }

    public byte[] getWlquImpLrdCalcCpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMP_LRD_CALC_CP, Pos.S089_IMP_LRD_CALC_CP);
        return buffer;
    }

    public void initWlquImpLrdCalcCpSpaces() {
        fill(Pos.S089_IMP_LRD_CALC_CP, Len.S089_IMP_LRD_CALC_CP, Types.SPACE_CHAR);
    }

    public void setWlquImpLrdCalcCpNull(String wlquImpLrdCalcCpNull) {
        writeString(Pos.S089_IMP_LRD_CALC_CP_NULL, wlquImpLrdCalcCpNull, Len.WLQU_IMP_LRD_CALC_CP_NULL);
    }

    /**Original name: WLQU-IMP-LRD-CALC-CP-NULL<br>*/
    public String getWlquImpLrdCalcCpNull() {
        return readString(Pos.S089_IMP_LRD_CALC_CP_NULL, Len.WLQU_IMP_LRD_CALC_CP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMP_LRD_CALC_CP = 1;
        public static final int S089_IMP_LRD_CALC_CP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMP_LRD_CALC_CP = 8;
        public static final int WLQU_IMP_LRD_CALC_CP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_LRD_CALC_CP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_LRD_CALC_CP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
