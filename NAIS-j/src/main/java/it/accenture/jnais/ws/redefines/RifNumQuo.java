package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIF-NUM-QUO<br>
 * Variable: RIF-NUM-QUO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RifNumQuo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RifNumQuo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIF_NUM_QUO;
    }

    public void setRifNumQuo(AfDecimal rifNumQuo) {
        writeDecimalAsPacked(Pos.RIF_NUM_QUO, rifNumQuo.copy());
    }

    public void setRifNumQuoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIF_NUM_QUO, Pos.RIF_NUM_QUO);
    }

    /**Original name: RIF-NUM-QUO<br>*/
    public AfDecimal getRifNumQuo() {
        return readPackedAsDecimal(Pos.RIF_NUM_QUO, Len.Int.RIF_NUM_QUO, Len.Fract.RIF_NUM_QUO);
    }

    public byte[] getRifNumQuoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIF_NUM_QUO, Pos.RIF_NUM_QUO);
        return buffer;
    }

    public void setRifNumQuoNull(String rifNumQuoNull) {
        writeString(Pos.RIF_NUM_QUO_NULL, rifNumQuoNull, Len.RIF_NUM_QUO_NULL);
    }

    /**Original name: RIF-NUM-QUO-NULL<br>*/
    public String getRifNumQuoNull() {
        return readString(Pos.RIF_NUM_QUO_NULL, Len.RIF_NUM_QUO_NULL);
    }

    public String getRifNumQuoNullFormatted() {
        return Functions.padBlanks(getRifNumQuoNull(), Len.RIF_NUM_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIF_NUM_QUO = 1;
        public static final int RIF_NUM_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIF_NUM_QUO = 7;
        public static final int RIF_NUM_QUO_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIF_NUM_QUO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RIF_NUM_QUO = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
