package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0580-TAB-PROD<br>
 * Variables: ISPC0580-TAB-PROD from copybook ISPC0580<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0580TabProd {

    //==== PROPERTIES ====
    //Original name: ISPC0580-TIPO-PRD
    private String tipoPrd = DefaultValues.stringVal(Len.TIPO_PRD);
    //Original name: ISPC0580-TIPO-GAR
    private String tipoGar = DefaultValues.stringVal(Len.TIPO_GAR);
    //Original name: ISPC0580-SOTTORAMO
    private String sottoramo = DefaultValues.stringVal(Len.SOTTORAMO);
    //Original name: ISPC0580-FORMAPREST
    private String formaprest = DefaultValues.stringVal(Len.FORMAPREST);
    //Original name: ISPC0580-FORMA
    private String forma = DefaultValues.stringVal(Len.FORMA);
    //Original name: ISPC0580-TIPOPREMIO
    private String tipopremio = DefaultValues.stringVal(Len.TIPOPREMIO);

    //==== METHODS ====
    public void setTabProdBytes(byte[] buffer, int offset) {
        int position = offset;
        tipoPrd = MarshalByte.readString(buffer, position, Len.TIPO_PRD);
        position += Len.TIPO_PRD;
        tipoGar = MarshalByte.readString(buffer, position, Len.TIPO_GAR);
        position += Len.TIPO_GAR;
        sottoramo = MarshalByte.readString(buffer, position, Len.SOTTORAMO);
        position += Len.SOTTORAMO;
        formaprest = MarshalByte.readString(buffer, position, Len.FORMAPREST);
        position += Len.FORMAPREST;
        forma = MarshalByte.readString(buffer, position, Len.FORMA);
        position += Len.FORMA;
        tipopremio = MarshalByte.readString(buffer, position, Len.TIPOPREMIO);
    }

    public byte[] getTabProdBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tipoPrd, Len.TIPO_PRD);
        position += Len.TIPO_PRD;
        MarshalByte.writeString(buffer, position, tipoGar, Len.TIPO_GAR);
        position += Len.TIPO_GAR;
        MarshalByte.writeString(buffer, position, sottoramo, Len.SOTTORAMO);
        position += Len.SOTTORAMO;
        MarshalByte.writeString(buffer, position, formaprest, Len.FORMAPREST);
        position += Len.FORMAPREST;
        MarshalByte.writeString(buffer, position, forma, Len.FORMA);
        position += Len.FORMA;
        MarshalByte.writeString(buffer, position, tipopremio, Len.TIPOPREMIO);
        return buffer;
    }

    public void initTabProdSpaces() {
        tipoPrd = "";
        tipoGar = "";
        sottoramo = "";
        formaprest = "";
        forma = "";
        tipopremio = "";
    }

    public void setTipoPrd(String tipoPrd) {
        this.tipoPrd = Functions.subString(tipoPrd, Len.TIPO_PRD);
    }

    public String getTipoPrd() {
        return this.tipoPrd;
    }

    public void setTipoGar(String tipoGar) {
        this.tipoGar = Functions.subString(tipoGar, Len.TIPO_GAR);
    }

    public String getTipoGar() {
        return this.tipoGar;
    }

    public void setSottoramo(String sottoramo) {
        this.sottoramo = Functions.subString(sottoramo, Len.SOTTORAMO);
    }

    public String getSottoramo() {
        return this.sottoramo;
    }

    public void setFormaprest(String formaprest) {
        this.formaprest = Functions.subString(formaprest, Len.FORMAPREST);
    }

    public String getFormaprest() {
        return this.formaprest;
    }

    public void setForma(String forma) {
        this.forma = Functions.subString(forma, Len.FORMA);
    }

    public String getForma() {
        return this.forma;
    }

    public void setTipopremio(String tipopremio) {
        this.tipopremio = Functions.subString(tipopremio, Len.TIPOPREMIO);
    }

    public String getTipopremio() {
        return this.tipopremio;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_PRD = 12;
        public static final int TIPO_GAR = 12;
        public static final int SOTTORAMO = 30;
        public static final int FORMAPREST = 70;
        public static final int FORMA = 70;
        public static final int TIPOPREMIO = 30;
        public static final int TAB_PROD = TIPO_PRD + TIPO_GAR + SOTTORAMO + FORMAPREST + FORMA + TIPOPREMIO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
