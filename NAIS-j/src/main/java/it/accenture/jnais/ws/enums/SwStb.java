package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-STB<br>
 * Variable: SW-STB from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwStb {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char OK = 'S';
    public static final char KO = 'N';

    //==== METHODS ====
    public void setSwStb(char swStb) {
        this.value = swStb;
    }

    public char getSwStb() {
        return this.value;
    }

    public boolean isOk() {
        return value == OK;
    }

    public void setOk() {
        value = OK;
    }

    public void setKo() {
        value = KO;
    }
}
