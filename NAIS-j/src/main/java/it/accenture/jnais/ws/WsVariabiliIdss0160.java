package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-VARIABILI<br>
 * Variable: WS-VARIABILI from program IDSS0160<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsVariabiliIdss0160 {

    //==== PROPERTIES ====
    //Original name: WS-TABELLA
    private String wsTabella = DefaultValues.stringVal(Len.WS_TABELLA);
    //Original name: WS-VALORE
    private String wsValore = DefaultValues.stringVal(Len.WS_VALORE);
    //Original name: WS-VALORE-ALL
    private String wsValoreAll = DefaultValues.stringVal(Len.WS_VALORE_ALL);
    //Original name: WS-VALORE-P
    private String wsValoreP = DefaultValues.stringVal(Len.WS_VALORE_P);
    //Original name: WS-KEY-OGGETTO
    private String wsKeyOggetto = DefaultValues.stringVal(Len.WS_KEY_OGGETTO);
    //Original name: WS-VALORE-NUM
    private long wsValoreNum = DefaultValues.LONG_VAL;
    //Original name: WS-LUNGHEZZA-VALORE
    private String wsLunghezzaValore = DefaultValues.stringVal(Len.WS_LUNGHEZZA_VALORE);
    //Original name: WS-LUNGHEZZA-CAMPO
    private String wsLunghezzaCampo = DefaultValues.stringVal(Len.WS_LUNGHEZZA_CAMPO);
    //Original name: WS-POSIZIONE-VAL
    private int wsPosizioneVal = DefaultValues.INT_VAL;
    //Original name: WS-POSIZIONE-IB
    private String wsPosizioneIb = DefaultValues.stringVal(Len.WS_POSIZIONE_IB);
    //Original name: WS-POSIZIONE-P
    private String wsPosizioneP = DefaultValues.stringVal(Len.WS_POSIZIONE_P);
    //Original name: WS-POSIZIONE-APP
    private String wsPosizioneApp = DefaultValues.stringVal(Len.WS_POSIZIONE_APP);
    //Original name: WS-LUNGHEZZA-P
    private String wsLunghezzaP = DefaultValues.stringVal(Len.WS_LUNGHEZZA_P);
    //Original name: WS-QTA-FLAG-K
    private short wsQtaFlagK = DefaultValues.BIN_SHORT_VAL;
    //Original name: WS-QTA-FLAG-P
    private short wsQtaFlagP = DefaultValues.BIN_SHORT_VAL;
    //Original name: WS-QTA-FLAG-J
    private short wsQtaFlagJ = DefaultValues.BIN_SHORT_VAL;
    //Original name: WKS-AREA-TABB-APPO
    private String wksAreaTabbAppo = DefaultValues.stringVal(Len.WKS_AREA_TABB_APPO);
    //Original name: WS-ID-NUM
    private String wsIdNum = DefaultValues.stringVal(Len.WS_ID_NUM);
    //Original name: WS-LUNGHEZZA-VALORE-PR
    private String wsLunghezzaValorePr = DefaultValues.stringVal(Len.WS_LUNGHEZZA_VALORE_PR);
    //Original name: WS-POSIZ-INI
    private String wsPosizIni = DefaultValues.stringVal(Len.WS_POSIZ_INI);

    //==== METHODS ====
    public void setWsTabella(String wsTabella) {
        this.wsTabella = Functions.subString(wsTabella, Len.WS_TABELLA);
    }

    public String getWsTabella() {
        return this.wsTabella;
    }

    public String getWsTabellaFormatted() {
        return Functions.padBlanks(getWsTabella(), Len.WS_TABELLA);
    }

    public void setWsValore(String wsValore) {
        this.wsValore = Functions.subString(wsValore, Len.WS_VALORE);
    }

    public String getWsValore() {
        return this.wsValore;
    }

    public String getWsValoreFormatted() {
        return Functions.padBlanks(getWsValore(), Len.WS_VALORE);
    }

    public void setWsValoreAll(String wsValoreAll) {
        this.wsValoreAll = Functions.subString(wsValoreAll, Len.WS_VALORE_ALL);
    }

    public String getWsValoreAll() {
        return this.wsValoreAll;
    }

    public void setWsValoreP(String wsValoreP) {
        this.wsValoreP = Functions.subString(wsValoreP, Len.WS_VALORE_P);
    }

    public String getWsValoreP() {
        return this.wsValoreP;
    }

    public String getWsValorePFormatted() {
        return Functions.padBlanks(getWsValoreP(), Len.WS_VALORE_P);
    }

    public void setWsKeyOggetto(String wsKeyOggetto) {
        this.wsKeyOggetto = Functions.subString(wsKeyOggetto, Len.WS_KEY_OGGETTO);
    }

    public String getWsKeyOggetto() {
        return this.wsKeyOggetto;
    }

    public void setWsValoreNum(long wsValoreNum) {
        this.wsValoreNum = wsValoreNum;
    }

    public long getWsValoreNum() {
        return this.wsValoreNum;
    }

    public void setWsLunghezzaValore(int wsLunghezzaValore) {
        this.wsLunghezzaValore = NumericDisplay.asString(wsLunghezzaValore, Len.WS_LUNGHEZZA_VALORE);
    }

    public void setWsLunghezzaValoreFormatted(String wsLunghezzaValore) {
        this.wsLunghezzaValore = Trunc.toUnsignedNumeric(wsLunghezzaValore, Len.WS_LUNGHEZZA_VALORE);
    }

    public int getWsLunghezzaValore() {
        return NumericDisplay.asInt(this.wsLunghezzaValore);
    }

    public String getWsLunghezzaValoreFormatted() {
        return this.wsLunghezzaValore;
    }

    public void setWsLunghezzaCampo(int wsLunghezzaCampo) {
        this.wsLunghezzaCampo = NumericDisplay.asString(wsLunghezzaCampo, Len.WS_LUNGHEZZA_CAMPO);
    }

    public void setWsLunghezzaCampoFormatted(String wsLunghezzaCampo) {
        this.wsLunghezzaCampo = Trunc.toUnsignedNumeric(wsLunghezzaCampo, Len.WS_LUNGHEZZA_CAMPO);
    }

    public int getWsLunghezzaCampo() {
        return NumericDisplay.asInt(this.wsLunghezzaCampo);
    }

    public void setWsPosizioneVal(int wsPosizioneVal) {
        this.wsPosizioneVal = wsPosizioneVal;
    }

    public int getWsPosizioneVal() {
        return this.wsPosizioneVal;
    }

    public void setWsPosizioneIb(int wsPosizioneIb) {
        this.wsPosizioneIb = NumericDisplay.asString(wsPosizioneIb, Len.WS_POSIZIONE_IB);
    }

    public void setWsPosizioneIbFormatted(String wsPosizioneIb) {
        this.wsPosizioneIb = Trunc.toUnsignedNumeric(wsPosizioneIb, Len.WS_POSIZIONE_IB);
    }

    public int getWsPosizioneIb() {
        return NumericDisplay.asInt(this.wsPosizioneIb);
    }

    public String getWsPosizioneIbFormatted() {
        return this.wsPosizioneIb;
    }

    public void setWsPosizioneP(int wsPosizioneP) {
        this.wsPosizioneP = NumericDisplay.asString(wsPosizioneP, Len.WS_POSIZIONE_P);
    }

    public void setWsPosizionePFormatted(String wsPosizioneP) {
        this.wsPosizioneP = Trunc.toUnsignedNumeric(wsPosizioneP, Len.WS_POSIZIONE_P);
    }

    public int getWsPosizioneP() {
        return NumericDisplay.asInt(this.wsPosizioneP);
    }

    public void setWsPosizioneApp(int wsPosizioneApp) {
        this.wsPosizioneApp = NumericDisplay.asString(wsPosizioneApp, Len.WS_POSIZIONE_APP);
    }

    public void setWsPosizioneAppFormatted(String wsPosizioneApp) {
        this.wsPosizioneApp = Trunc.toUnsignedNumeric(wsPosizioneApp, Len.WS_POSIZIONE_APP);
    }

    public int getWsPosizioneApp() {
        return NumericDisplay.asInt(this.wsPosizioneApp);
    }

    public void setWsLunghezzaP(int wsLunghezzaP) {
        this.wsLunghezzaP = NumericDisplay.asString(wsLunghezzaP, Len.WS_LUNGHEZZA_P);
    }

    public void setWsLunghezzaPFormatted(String wsLunghezzaP) {
        this.wsLunghezzaP = Trunc.toUnsignedNumeric(wsLunghezzaP, Len.WS_LUNGHEZZA_P);
    }

    public int getWsLunghezzaP() {
        return NumericDisplay.asInt(this.wsLunghezzaP);
    }

    public void setWsQtaFlagK(short wsQtaFlagK) {
        this.wsQtaFlagK = wsQtaFlagK;
    }

    public short getWsQtaFlagK() {
        return this.wsQtaFlagK;
    }

    public void setWsQtaFlagP(short wsQtaFlagP) {
        this.wsQtaFlagP = wsQtaFlagP;
    }

    public short getWsQtaFlagP() {
        return this.wsQtaFlagP;
    }

    public void setWsQtaFlagJ(short wsQtaFlagJ) {
        this.wsQtaFlagJ = wsQtaFlagJ;
    }

    public short getWsQtaFlagJ() {
        return this.wsQtaFlagJ;
    }

    public void setWksAreaTabbAppo(String wksAreaTabbAppo) {
        this.wksAreaTabbAppo = Functions.subString(wksAreaTabbAppo, Len.WKS_AREA_TABB_APPO);
    }

    public String getWksAreaTabbAppo() {
        return this.wksAreaTabbAppo;
    }

    public String getWksAreaTabbAppoFormatted() {
        return Functions.padBlanks(getWksAreaTabbAppo(), Len.WKS_AREA_TABB_APPO);
    }

    public void setWsIdNum(int wsIdNum) {
        this.wsIdNum = NumericDisplay.asString(wsIdNum, Len.WS_ID_NUM);
    }

    public void setWsIdNumFormatted(String wsIdNum) {
        this.wsIdNum = Trunc.toUnsignedNumeric(wsIdNum, Len.WS_ID_NUM);
    }

    public int getWsIdNum() {
        return NumericDisplay.asInt(this.wsIdNum);
    }

    public String getWsIdNumFormatted() {
        return this.wsIdNum;
    }

    public void setWsLunghezzaValorePr(int wsLunghezzaValorePr) {
        this.wsLunghezzaValorePr = NumericDisplay.asString(wsLunghezzaValorePr, Len.WS_LUNGHEZZA_VALORE_PR);
    }

    public void setWsLunghezzaValorePrFormatted(String wsLunghezzaValorePr) {
        this.wsLunghezzaValorePr = Trunc.toUnsignedNumeric(wsLunghezzaValorePr, Len.WS_LUNGHEZZA_VALORE_PR);
    }

    public int getWsLunghezzaValorePr() {
        return NumericDisplay.asInt(this.wsLunghezzaValorePr);
    }

    public void setWsPosizIni(int wsPosizIni) {
        this.wsPosizIni = NumericDisplay.asString(wsPosizIni, Len.WS_POSIZ_INI);
    }

    public void setWsPosizIniFormatted(String wsPosizIni) {
        this.wsPosizIni = Trunc.toUnsignedNumeric(wsPosizIni, Len.WS_POSIZ_INI);
    }

    public int getWsPosizIni() {
        return NumericDisplay.asInt(this.wsPosizIni);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TABELLA = 30;
        public static final int WS_VALORE = 50;
        public static final int WS_VALORE_ALL = 50;
        public static final int WS_VALORE_P = 50;
        public static final int WS_KEY_OGGETTO = 40;
        public static final int WS_VALORE_NUM = 18;
        public static final int WS_LUNGHEZZA_VALORE = 5;
        public static final int WS_LUNGHEZZA_CAMPO = 5;
        public static final int WS_POSIZIONE_IB = 5;
        public static final int WS_POSIZIONE_P = 5;
        public static final int WS_POSIZIONE_APP = 5;
        public static final int WS_LUNGHEZZA_P = 5;
        public static final int WKS_AREA_TABB_APPO = 2500;
        public static final int WS_ID_NUM = 9;
        public static final int WS_LUNGHEZZA_VALORE_PR = 5;
        public static final int WS_POSIZ_INI = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
