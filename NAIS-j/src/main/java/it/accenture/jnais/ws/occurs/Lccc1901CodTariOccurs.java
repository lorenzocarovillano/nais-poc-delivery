package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC1901-COD-TARI-OCCURS<br>
 * Variables: LCCC1901-COD-TARI-OCCURS from copybook LCCC1901<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Lccc1901CodTariOccurs {

    //==== PROPERTIES ====
    //Original name: LCCC1901-COD-TARI
    private String lccc1901CodTari = DefaultValues.stringVal(Len.LCCC1901_COD_TARI);

    //==== METHODS ====
    public void setCodTariOccursBytes(byte[] buffer, int offset) {
        int position = offset;
        lccc1901CodTari = MarshalByte.readString(buffer, position, Len.LCCC1901_COD_TARI);
    }

    public byte[] getCodTariOccursBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, lccc1901CodTari, Len.LCCC1901_COD_TARI);
        return buffer;
    }

    public void initCodTariOccursSpaces() {
        lccc1901CodTari = "";
    }

    public void setLccc1901CodTari(String lccc1901CodTari) {
        this.lccc1901CodTari = Functions.subString(lccc1901CodTari, Len.LCCC1901_COD_TARI);
    }

    public String getLccc1901CodTari() {
        return this.lccc1901CodTari;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC1901_COD_TARI = 12;
        public static final int COD_TARI_OCCURS = LCCC1901_COD_TARI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
