package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0016-TIPO-ANNO<br>
 * Variable: IDSV0016-TIPO-ANNO from copybook IDSV0016<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0016TipoAnno {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char BISESTILE = 'B';
    public static final char STANDARD = 'S';

    //==== METHODS ====
    public void setIdsv0016TipoAnno(char idsv0016TipoAnno) {
        this.value = idsv0016TipoAnno;
    }

    public char getIdsv0016TipoAnno() {
        return this.value;
    }

    public boolean isBisestile() {
        return value == BISESTILE;
    }

    public void setBisestile() {
        value = BISESTILE;
    }

    public void setStandard() {
        value = STANDARD;
    }
}
