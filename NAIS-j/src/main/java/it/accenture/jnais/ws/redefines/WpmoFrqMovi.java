package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-FRQ-MOVI<br>
 * Variable: WPMO-FRQ-MOVI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoFrqMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoFrqMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_FRQ_MOVI;
    }

    public void setWpmoFrqMovi(int wpmoFrqMovi) {
        writeIntAsPacked(Pos.WPMO_FRQ_MOVI, wpmoFrqMovi, Len.Int.WPMO_FRQ_MOVI);
    }

    public void setWpmoFrqMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_FRQ_MOVI, Pos.WPMO_FRQ_MOVI);
    }

    /**Original name: WPMO-FRQ-MOVI<br>*/
    public int getWpmoFrqMovi() {
        return readPackedAsInt(Pos.WPMO_FRQ_MOVI, Len.Int.WPMO_FRQ_MOVI);
    }

    public byte[] getWpmoFrqMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_FRQ_MOVI, Pos.WPMO_FRQ_MOVI);
        return buffer;
    }

    public void initWpmoFrqMoviSpaces() {
        fill(Pos.WPMO_FRQ_MOVI, Len.WPMO_FRQ_MOVI, Types.SPACE_CHAR);
    }

    public void setWpmoFrqMoviNull(String wpmoFrqMoviNull) {
        writeString(Pos.WPMO_FRQ_MOVI_NULL, wpmoFrqMoviNull, Len.WPMO_FRQ_MOVI_NULL);
    }

    /**Original name: WPMO-FRQ-MOVI-NULL<br>*/
    public String getWpmoFrqMoviNull() {
        return readString(Pos.WPMO_FRQ_MOVI_NULL, Len.WPMO_FRQ_MOVI_NULL);
    }

    public String getWpmoFrqMoviNullFormatted() {
        return Functions.padBlanks(getWpmoFrqMoviNull(), Len.WPMO_FRQ_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_FRQ_MOVI = 1;
        public static final int WPMO_FRQ_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_FRQ_MOVI = 3;
        public static final int WPMO_FRQ_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_FRQ_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
