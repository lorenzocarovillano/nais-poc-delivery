package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B01-VAL-QUO-DT-CALC<br>
 * Variable: B01-VAL-QUO-DT-CALC from program IDBSB010<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B01ValQuoDtCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B01ValQuoDtCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B01_VAL_QUO_DT_CALC;
    }

    public void setB01ValQuoDtCalc(AfDecimal b01ValQuoDtCalc) {
        writeDecimalAsPacked(Pos.B01_VAL_QUO_DT_CALC, b01ValQuoDtCalc.copy());
    }

    public void setB01ValQuoDtCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B01_VAL_QUO_DT_CALC, Pos.B01_VAL_QUO_DT_CALC);
    }

    /**Original name: B01-VAL-QUO-DT-CALC<br>*/
    public AfDecimal getB01ValQuoDtCalc() {
        return readPackedAsDecimal(Pos.B01_VAL_QUO_DT_CALC, Len.Int.B01_VAL_QUO_DT_CALC, Len.Fract.B01_VAL_QUO_DT_CALC);
    }

    public byte[] getB01ValQuoDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B01_VAL_QUO_DT_CALC, Pos.B01_VAL_QUO_DT_CALC);
        return buffer;
    }

    public void setB01ValQuoDtCalcNull(String b01ValQuoDtCalcNull) {
        writeString(Pos.B01_VAL_QUO_DT_CALC_NULL, b01ValQuoDtCalcNull, Len.B01_VAL_QUO_DT_CALC_NULL);
    }

    /**Original name: B01-VAL-QUO-DT-CALC-NULL<br>*/
    public String getB01ValQuoDtCalcNull() {
        return readString(Pos.B01_VAL_QUO_DT_CALC_NULL, Len.B01_VAL_QUO_DT_CALC_NULL);
    }

    public String getB01ValQuoDtCalcNullFormatted() {
        return Functions.padBlanks(getB01ValQuoDtCalcNull(), Len.B01_VAL_QUO_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B01_VAL_QUO_DT_CALC = 1;
        public static final int B01_VAL_QUO_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B01_VAL_QUO_DT_CALC = 7;
        public static final int B01_VAL_QUO_DT_CALC_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B01_VAL_QUO_DT_CALC = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int B01_VAL_QUO_DT_CALC = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
