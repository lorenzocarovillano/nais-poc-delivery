package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-ANTIDUR-CALC-365<br>
 * Variable: WB03-ANTIDUR-CALC-365 from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03AntidurCalc365 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03AntidurCalc365() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_ANTIDUR_CALC365;
    }

    public void setWb03AntidurCalc365(AfDecimal wb03AntidurCalc365) {
        writeDecimalAsPacked(Pos.WB03_ANTIDUR_CALC365, wb03AntidurCalc365.copy());
    }

    public void setWb03AntidurCalc365FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_ANTIDUR_CALC365, Pos.WB03_ANTIDUR_CALC365);
    }

    /**Original name: WB03-ANTIDUR-CALC-365<br>*/
    public AfDecimal getWb03AntidurCalc365() {
        return readPackedAsDecimal(Pos.WB03_ANTIDUR_CALC365, Len.Int.WB03_ANTIDUR_CALC365, Len.Fract.WB03_ANTIDUR_CALC365);
    }

    public byte[] getWb03AntidurCalc365AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_ANTIDUR_CALC365, Pos.WB03_ANTIDUR_CALC365);
        return buffer;
    }

    public void setWb03AntidurCalc365Null(String wb03AntidurCalc365Null) {
        writeString(Pos.WB03_ANTIDUR_CALC365_NULL, wb03AntidurCalc365Null, Len.WB03_ANTIDUR_CALC365_NULL);
    }

    /**Original name: WB03-ANTIDUR-CALC-365-NULL<br>*/
    public String getWb03AntidurCalc365Null() {
        return readString(Pos.WB03_ANTIDUR_CALC365_NULL, Len.WB03_ANTIDUR_CALC365_NULL);
    }

    public String getWb03AntidurCalc365NullFormatted() {
        return Functions.padBlanks(getWb03AntidurCalc365Null(), Len.WB03_ANTIDUR_CALC365_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_ANTIDUR_CALC365 = 1;
        public static final int WB03_ANTIDUR_CALC365_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ANTIDUR_CALC365 = 6;
        public static final int WB03_ANTIDUR_CALC365_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ANTIDUR_CALC365 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_ANTIDUR_CALC365 = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
