package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ISPC0040-COD-TP-PREMIO<br>
 * Variable: ISPC0040-COD-TP-PREMIO from copybook ISPC0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0040CodTpPremio {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TARIFFA = 'T';
    public static final char INVENTARIO = 'I';
    public static final char PURO = 'P';

    //==== METHODS ====
    public void setCodTpPremio(char codTpPremio) {
        this.value = codTpPremio;
    }

    public char getCodTpPremio() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_TP_PREMIO = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
