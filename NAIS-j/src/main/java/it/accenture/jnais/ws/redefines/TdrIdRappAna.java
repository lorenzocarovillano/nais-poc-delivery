package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-ID-RAPP-ANA<br>
 * Variable: TDR-ID-RAPP-ANA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrIdRappAna extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrIdRappAna() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_ID_RAPP_ANA;
    }

    public void setTdrIdRappAna(int tdrIdRappAna) {
        writeIntAsPacked(Pos.TDR_ID_RAPP_ANA, tdrIdRappAna, Len.Int.TDR_ID_RAPP_ANA);
    }

    public void setTdrIdRappAnaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_ID_RAPP_ANA, Pos.TDR_ID_RAPP_ANA);
    }

    /**Original name: TDR-ID-RAPP-ANA<br>*/
    public int getTdrIdRappAna() {
        return readPackedAsInt(Pos.TDR_ID_RAPP_ANA, Len.Int.TDR_ID_RAPP_ANA);
    }

    public byte[] getTdrIdRappAnaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_ID_RAPP_ANA, Pos.TDR_ID_RAPP_ANA);
        return buffer;
    }

    public void setTdrIdRappAnaNull(String tdrIdRappAnaNull) {
        writeString(Pos.TDR_ID_RAPP_ANA_NULL, tdrIdRappAnaNull, Len.TDR_ID_RAPP_ANA_NULL);
    }

    /**Original name: TDR-ID-RAPP-ANA-NULL<br>*/
    public String getTdrIdRappAnaNull() {
        return readString(Pos.TDR_ID_RAPP_ANA_NULL, Len.TDR_ID_RAPP_ANA_NULL);
    }

    public String getTdrIdRappAnaNullFormatted() {
        return Functions.padBlanks(getTdrIdRappAnaNull(), Len.TDR_ID_RAPP_ANA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_ID_RAPP_ANA = 1;
        public static final int TDR_ID_RAPP_ANA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_ID_RAPP_ANA = 5;
        public static final int TDR_ID_RAPP_ANA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_ID_RAPP_ANA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
