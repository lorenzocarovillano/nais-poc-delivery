package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTELRISCPAR-PR<br>
 * Variable: WPCO-DT-ULTELRISCPAR-PR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltelriscparPr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltelriscparPr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTELRISCPAR_PR;
    }

    public void setWpcoDtUltelriscparPr(int wpcoDtUltelriscparPr) {
        writeIntAsPacked(Pos.WPCO_DT_ULTELRISCPAR_PR, wpcoDtUltelriscparPr, Len.Int.WPCO_DT_ULTELRISCPAR_PR);
    }

    public void setDpcoDtUltelriscparPrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTELRISCPAR_PR, Pos.WPCO_DT_ULTELRISCPAR_PR);
    }

    /**Original name: WPCO-DT-ULTELRISCPAR-PR<br>*/
    public int getWpcoDtUltelriscparPr() {
        return readPackedAsInt(Pos.WPCO_DT_ULTELRISCPAR_PR, Len.Int.WPCO_DT_ULTELRISCPAR_PR);
    }

    public byte[] getWpcoDtUltelriscparPrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTELRISCPAR_PR, Pos.WPCO_DT_ULTELRISCPAR_PR);
        return buffer;
    }

    public void setWpcoDtUltelriscparPrNull(String wpcoDtUltelriscparPrNull) {
        writeString(Pos.WPCO_DT_ULTELRISCPAR_PR_NULL, wpcoDtUltelriscparPrNull, Len.WPCO_DT_ULTELRISCPAR_PR_NULL);
    }

    /**Original name: WPCO-DT-ULTELRISCPAR-PR-NULL<br>*/
    public String getWpcoDtUltelriscparPrNull() {
        return readString(Pos.WPCO_DT_ULTELRISCPAR_PR_NULL, Len.WPCO_DT_ULTELRISCPAR_PR_NULL);
    }

    public String getWpcoDtUltelriscparPrNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltelriscparPrNull(), Len.WPCO_DT_ULTELRISCPAR_PR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTELRISCPAR_PR = 1;
        public static final int WPCO_DT_ULTELRISCPAR_PR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTELRISCPAR_PR = 5;
        public static final int WPCO_DT_ULTELRISCPAR_PR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTELRISCPAR_PR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
