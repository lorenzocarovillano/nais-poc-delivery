package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PC-RETR<br>
 * Variable: L3421-PC-RETR from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PcRetr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PcRetr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PC_RETR;
    }

    public void setL3421PcRetrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PC_RETR, Pos.L3421_PC_RETR);
    }

    /**Original name: L3421-PC-RETR<br>*/
    public AfDecimal getL3421PcRetr() {
        return readPackedAsDecimal(Pos.L3421_PC_RETR, Len.Int.L3421_PC_RETR, Len.Fract.L3421_PC_RETR);
    }

    public byte[] getL3421PcRetrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PC_RETR, Pos.L3421_PC_RETR);
        return buffer;
    }

    /**Original name: L3421-PC-RETR-NULL<br>*/
    public String getL3421PcRetrNull() {
        return readString(Pos.L3421_PC_RETR_NULL, Len.L3421_PC_RETR_NULL);
    }

    public String getL3421PcRetrNullFormatted() {
        return Functions.padBlanks(getL3421PcRetrNull(), Len.L3421_PC_RETR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PC_RETR = 1;
        public static final int L3421_PC_RETR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PC_RETR = 4;
        public static final int L3421_PC_RETR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PC_RETR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PC_RETR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
