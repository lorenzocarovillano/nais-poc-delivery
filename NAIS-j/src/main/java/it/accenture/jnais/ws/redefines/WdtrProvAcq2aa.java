package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-PROV-ACQ-2AA<br>
 * Variable: WDTR-PROV-ACQ-2AA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrProvAcq2aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrProvAcq2aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_PROV_ACQ2AA;
    }

    public void setWdtrProvAcq2aa(AfDecimal wdtrProvAcq2aa) {
        writeDecimalAsPacked(Pos.WDTR_PROV_ACQ2AA, wdtrProvAcq2aa.copy());
    }

    /**Original name: WDTR-PROV-ACQ-2AA<br>*/
    public AfDecimal getWdtrProvAcq2aa() {
        return readPackedAsDecimal(Pos.WDTR_PROV_ACQ2AA, Len.Int.WDTR_PROV_ACQ2AA, Len.Fract.WDTR_PROV_ACQ2AA);
    }

    public void setWdtrProvAcq2aaNull(String wdtrProvAcq2aaNull) {
        writeString(Pos.WDTR_PROV_ACQ2AA_NULL, wdtrProvAcq2aaNull, Len.WDTR_PROV_ACQ2AA_NULL);
    }

    /**Original name: WDTR-PROV-ACQ-2AA-NULL<br>*/
    public String getWdtrProvAcq2aaNull() {
        return readString(Pos.WDTR_PROV_ACQ2AA_NULL, Len.WDTR_PROV_ACQ2AA_NULL);
    }

    public String getWdtrProvAcq2aaNullFormatted() {
        return Functions.padBlanks(getWdtrProvAcq2aaNull(), Len.WDTR_PROV_ACQ2AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_PROV_ACQ2AA = 1;
        public static final int WDTR_PROV_ACQ2AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_PROV_ACQ2AA = 8;
        public static final int WDTR_PROV_ACQ2AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_PROV_ACQ2AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_PROV_ACQ2AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
