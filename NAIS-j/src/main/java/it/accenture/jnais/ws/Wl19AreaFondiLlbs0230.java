package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.Wl19TabellaLlbs0230;

/**Original name: WL19-AREA-FONDI<br>
 * Variable: WL19-AREA-FONDI from program LLBS0230<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Wl19AreaFondiLlbs0230 extends SerializableParameter {

    //==== PROPERTIES ====
    /**Original name: WL19-ELE-FND-MAX<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY 7 PER LA GESTIONE DELLE OCCURS
	 * ----------------------------------------------------------------*</pre>*/
    private short wl19EleFndMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WL19-TABELLA
    private Wl19TabellaLlbs0230 wl19Tabella = new Wl19TabellaLlbs0230();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL19_AREA_FONDI;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWl19AreaFondiBytes(buf);
    }

    public String getWl19AreaFondiFormatted() {
        return MarshalByteExt.bufferToStr(getWl19AreaFondiBytes());
    }

    public void setWl19AreaFondiBytes(byte[] buffer) {
        setWl19AreaFondiBytes(buffer, 1);
    }

    public byte[] getWl19AreaFondiBytes() {
        byte[] buffer = new byte[Len.WL19_AREA_FONDI];
        return getWl19AreaFondiBytes(buffer, 1);
    }

    public void setWl19AreaFondiBytes(byte[] buffer, int offset) {
        int position = offset;
        wl19EleFndMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wl19Tabella.setWl19TabellaBytes(buffer, position);
    }

    public byte[] getWl19AreaFondiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wl19EleFndMax);
        position += Types.SHORT_SIZE;
        wl19Tabella.getWl19TabellaBytes(buffer, position);
        return buffer;
    }

    public void setWl19EleFndMax(short wl19EleFndMax) {
        this.wl19EleFndMax = wl19EleFndMax;
    }

    public short getWl19EleFndMax() {
        return this.wl19EleFndMax;
    }

    public Wl19TabellaLlbs0230 getWl19Tabella() {
        return wl19Tabella;
    }

    @Override
    public byte[] serialize() {
        return getWl19AreaFondiBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WL19_ELE_FND_MAX = 2;
        public static final int WL19_AREA_FONDI = WL19_ELE_FND_MAX + Wl19TabellaLlbs0230.Len.WL19_TABELLA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
