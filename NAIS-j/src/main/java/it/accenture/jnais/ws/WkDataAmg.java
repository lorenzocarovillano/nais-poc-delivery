package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-DATA-AMG<br>
 * Variable: WK-DATA-AMG from program LOAS0670<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkDataAmg {

    //==== PROPERTIES ====
    //Original name: AAAA-SYS
    private String aaaaSys = "0000";
    //Original name: MM-SYS
    private String mmSys = "00";
    //Original name: GG-SYS
    private String ggSys = "00";

    //==== METHODS ====
    public void setWkDataAmgFormatted(String data) {
        byte[] buffer = new byte[Len.WK_DATA_AMG];
        MarshalByte.writeString(buffer, 1, data, Len.WK_DATA_AMG);
        setWkDataAmgBytes(buffer, 1);
    }

    public void setWkDataAmgBytes(byte[] buffer, int offset) {
        int position = offset;
        aaaaSys = MarshalByte.readString(buffer, position, Len.AAAA_SYS);
        position += Len.AAAA_SYS;
        mmSys = MarshalByte.readString(buffer, position, Len.MM_SYS);
        position += Len.MM_SYS;
        ggSys = MarshalByte.readString(buffer, position, Len.GG_SYS);
    }

    public void setAaaaSys(String aaaaSys) {
        this.aaaaSys = Functions.subString(aaaaSys, Len.AAAA_SYS);
    }

    public String getAaaaSys() {
        return this.aaaaSys;
    }

    public void setMmSys(String mmSys) {
        this.mmSys = Functions.subString(mmSys, Len.MM_SYS);
    }

    public String getMmSys() {
        return this.mmSys;
    }

    public void setGgSys(String ggSys) {
        this.ggSys = Functions.subString(ggSys, Len.GG_SYS);
    }

    public String getGgSys() {
        return this.ggSys;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA_SYS = 4;
        public static final int MM_SYS = 2;
        public static final int GG_SYS = 2;
        public static final int WK_DATA_AMG = AAAA_SYS + MM_SYS + GG_SYS;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
