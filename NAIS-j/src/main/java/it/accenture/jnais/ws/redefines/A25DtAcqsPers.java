package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: A25-DT-ACQS-PERS<br>
 * Variable: A25-DT-ACQS-PERS from program LDBS1300<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A25DtAcqsPers extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A25DtAcqsPers() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A25_DT_ACQS_PERS;
    }

    public void setA25DtAcqsPers(int a25DtAcqsPers) {
        writeIntAsPacked(Pos.A25_DT_ACQS_PERS, a25DtAcqsPers, Len.Int.A25_DT_ACQS_PERS);
    }

    public void setA25DtAcqsPersFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A25_DT_ACQS_PERS, Pos.A25_DT_ACQS_PERS);
    }

    /**Original name: A25-DT-ACQS-PERS<br>*/
    public int getA25DtAcqsPers() {
        return readPackedAsInt(Pos.A25_DT_ACQS_PERS, Len.Int.A25_DT_ACQS_PERS);
    }

    public byte[] getA25DtAcqsPersAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A25_DT_ACQS_PERS, Pos.A25_DT_ACQS_PERS);
        return buffer;
    }

    public void setA25DtAcqsPersNull(String a25DtAcqsPersNull) {
        writeString(Pos.A25_DT_ACQS_PERS_NULL, a25DtAcqsPersNull, Len.A25_DT_ACQS_PERS_NULL);
    }

    /**Original name: A25-DT-ACQS-PERS-NULL<br>*/
    public String getA25DtAcqsPersNull() {
        return readString(Pos.A25_DT_ACQS_PERS_NULL, Len.A25_DT_ACQS_PERS_NULL);
    }

    public String getA25DtAcqsPersNullFormatted() {
        return Functions.padBlanks(getA25DtAcqsPersNull(), Len.A25_DT_ACQS_PERS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A25_DT_ACQS_PERS = 1;
        public static final int A25_DT_ACQS_PERS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_DT_ACQS_PERS = 5;
        public static final int A25_DT_ACQS_PERS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_DT_ACQS_PERS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
