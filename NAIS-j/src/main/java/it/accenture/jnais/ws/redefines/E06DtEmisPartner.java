package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: E06-DT-EMIS-PARTNER<br>
 * Variable: E06-DT-EMIS-PARTNER from program IDBSE060<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E06DtEmisPartner extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E06DtEmisPartner() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E06_DT_EMIS_PARTNER;
    }

    public void setE06DtEmisPartner(int e06DtEmisPartner) {
        writeIntAsPacked(Pos.E06_DT_EMIS_PARTNER, e06DtEmisPartner, Len.Int.E06_DT_EMIS_PARTNER);
    }

    public void setE06DtEmisPartnerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E06_DT_EMIS_PARTNER, Pos.E06_DT_EMIS_PARTNER);
    }

    /**Original name: E06-DT-EMIS-PARTNER<br>*/
    public int getE06DtEmisPartner() {
        return readPackedAsInt(Pos.E06_DT_EMIS_PARTNER, Len.Int.E06_DT_EMIS_PARTNER);
    }

    public byte[] getE06DtEmisPartnerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E06_DT_EMIS_PARTNER, Pos.E06_DT_EMIS_PARTNER);
        return buffer;
    }

    public void setE06DtEmisPartnerNull(String e06DtEmisPartnerNull) {
        writeString(Pos.E06_DT_EMIS_PARTNER_NULL, e06DtEmisPartnerNull, Len.E06_DT_EMIS_PARTNER_NULL);
    }

    /**Original name: E06-DT-EMIS-PARTNER-NULL<br>*/
    public String getE06DtEmisPartnerNull() {
        return readString(Pos.E06_DT_EMIS_PARTNER_NULL, Len.E06_DT_EMIS_PARTNER_NULL);
    }

    public String getE06DtEmisPartnerNullFormatted() {
        return Functions.padBlanks(getE06DtEmisPartnerNull(), Len.E06_DT_EMIS_PARTNER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E06_DT_EMIS_PARTNER = 1;
        public static final int E06_DT_EMIS_PARTNER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E06_DT_EMIS_PARTNER = 5;
        public static final int E06_DT_EMIS_PARTNER_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E06_DT_EMIS_PARTNER = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
