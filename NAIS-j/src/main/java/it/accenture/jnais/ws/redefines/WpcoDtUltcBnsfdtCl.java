package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-BNSFDT-CL<br>
 * Variable: WPCO-DT-ULTC-BNSFDT-CL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcBnsfdtCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcBnsfdtCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_BNSFDT_CL;
    }

    public void setWpcoDtUltcBnsfdtCl(int wpcoDtUltcBnsfdtCl) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_BNSFDT_CL, wpcoDtUltcBnsfdtCl, Len.Int.WPCO_DT_ULTC_BNSFDT_CL);
    }

    public void setDpcoDtUltcBnsfdtClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_BNSFDT_CL, Pos.WPCO_DT_ULTC_BNSFDT_CL);
    }

    /**Original name: WPCO-DT-ULTC-BNSFDT-CL<br>*/
    public int getWpcoDtUltcBnsfdtCl() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_BNSFDT_CL, Len.Int.WPCO_DT_ULTC_BNSFDT_CL);
    }

    public byte[] getWpcoDtUltcBnsfdtClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_BNSFDT_CL, Pos.WPCO_DT_ULTC_BNSFDT_CL);
        return buffer;
    }

    public void setWpcoDtUltcBnsfdtClNull(String wpcoDtUltcBnsfdtClNull) {
        writeString(Pos.WPCO_DT_ULTC_BNSFDT_CL_NULL, wpcoDtUltcBnsfdtClNull, Len.WPCO_DT_ULTC_BNSFDT_CL_NULL);
    }

    /**Original name: WPCO-DT-ULTC-BNSFDT-CL-NULL<br>*/
    public String getWpcoDtUltcBnsfdtClNull() {
        return readString(Pos.WPCO_DT_ULTC_BNSFDT_CL_NULL, Len.WPCO_DT_ULTC_BNSFDT_CL_NULL);
    }

    public String getWpcoDtUltcBnsfdtClNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcBnsfdtClNull(), Len.WPCO_DT_ULTC_BNSFDT_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_BNSFDT_CL = 1;
        public static final int WPCO_DT_ULTC_BNSFDT_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_BNSFDT_CL = 5;
        public static final int WPCO_DT_ULTC_BNSFDT_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_BNSFDT_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
