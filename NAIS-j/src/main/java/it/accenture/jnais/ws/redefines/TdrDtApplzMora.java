package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-DT-APPLZ-MORA<br>
 * Variable: TDR-DT-APPLZ-MORA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrDtApplzMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrDtApplzMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_DT_APPLZ_MORA;
    }

    public void setTdrDtApplzMora(int tdrDtApplzMora) {
        writeIntAsPacked(Pos.TDR_DT_APPLZ_MORA, tdrDtApplzMora, Len.Int.TDR_DT_APPLZ_MORA);
    }

    public void setTdrDtApplzMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_DT_APPLZ_MORA, Pos.TDR_DT_APPLZ_MORA);
    }

    /**Original name: TDR-DT-APPLZ-MORA<br>*/
    public int getTdrDtApplzMora() {
        return readPackedAsInt(Pos.TDR_DT_APPLZ_MORA, Len.Int.TDR_DT_APPLZ_MORA);
    }

    public byte[] getTdrDtApplzMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_DT_APPLZ_MORA, Pos.TDR_DT_APPLZ_MORA);
        return buffer;
    }

    public void setTdrDtApplzMoraNull(String tdrDtApplzMoraNull) {
        writeString(Pos.TDR_DT_APPLZ_MORA_NULL, tdrDtApplzMoraNull, Len.TDR_DT_APPLZ_MORA_NULL);
    }

    /**Original name: TDR-DT-APPLZ-MORA-NULL<br>*/
    public String getTdrDtApplzMoraNull() {
        return readString(Pos.TDR_DT_APPLZ_MORA_NULL, Len.TDR_DT_APPLZ_MORA_NULL);
    }

    public String getTdrDtApplzMoraNullFormatted() {
        return Functions.padBlanks(getTdrDtApplzMoraNull(), Len.TDR_DT_APPLZ_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_DT_APPLZ_MORA = 1;
        public static final int TDR_DT_APPLZ_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_DT_APPLZ_MORA = 5;
        public static final int TDR_DT_APPLZ_MORA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_DT_APPLZ_MORA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
