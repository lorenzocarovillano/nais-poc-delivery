package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: A25-DT-END-VLDT-PERS<br>
 * Variable: A25-DT-END-VLDT-PERS from program LDBS1300<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A25DtEndVldtPers extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A25DtEndVldtPers() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A25_DT_END_VLDT_PERS;
    }

    public void setA25DtEndVldtPers(int a25DtEndVldtPers) {
        writeIntAsPacked(Pos.A25_DT_END_VLDT_PERS, a25DtEndVldtPers, Len.Int.A25_DT_END_VLDT_PERS);
    }

    public void setA25DtEndVldtPersFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A25_DT_END_VLDT_PERS, Pos.A25_DT_END_VLDT_PERS);
    }

    /**Original name: A25-DT-END-VLDT-PERS<br>*/
    public int getA25DtEndVldtPers() {
        return readPackedAsInt(Pos.A25_DT_END_VLDT_PERS, Len.Int.A25_DT_END_VLDT_PERS);
    }

    public byte[] getA25DtEndVldtPersAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A25_DT_END_VLDT_PERS, Pos.A25_DT_END_VLDT_PERS);
        return buffer;
    }

    public void setA25DtEndVldtPersNull(String a25DtEndVldtPersNull) {
        writeString(Pos.A25_DT_END_VLDT_PERS_NULL, a25DtEndVldtPersNull, Len.A25_DT_END_VLDT_PERS_NULL);
    }

    /**Original name: A25-DT-END-VLDT-PERS-NULL<br>*/
    public String getA25DtEndVldtPersNull() {
        return readString(Pos.A25_DT_END_VLDT_PERS_NULL, Len.A25_DT_END_VLDT_PERS_NULL);
    }

    public String getA25DtEndVldtPersNullFormatted() {
        return Functions.padBlanks(getA25DtEndVldtPersNull(), Len.A25_DT_END_VLDT_PERS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A25_DT_END_VLDT_PERS = 1;
        public static final int A25_DT_END_VLDT_PERS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_DT_END_VLDT_PERS = 5;
        public static final int A25_DT_END_VLDT_PERS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_DT_END_VLDT_PERS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
