package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-TGA-IMP-RICOR<br>
 * Variable: WPAG-TGA-IMP-RICOR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaImpRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTgaImpRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TGA_IMP_RICOR;
    }

    public void setWpagTgaImpRicor(AfDecimal wpagTgaImpRicor) {
        writeDecimalAsPacked(Pos.WPAG_TGA_IMP_RICOR, wpagTgaImpRicor.copy());
    }

    public void setWpagTgaImpRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TGA_IMP_RICOR, Pos.WPAG_TGA_IMP_RICOR);
    }

    /**Original name: WPAG-TGA-IMP-RICOR<br>*/
    public AfDecimal getWpagTgaImpRicor() {
        return readPackedAsDecimal(Pos.WPAG_TGA_IMP_RICOR, Len.Int.WPAG_TGA_IMP_RICOR, Len.Fract.WPAG_TGA_IMP_RICOR);
    }

    public byte[] getWpagTgaImpRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TGA_IMP_RICOR, Pos.WPAG_TGA_IMP_RICOR);
        return buffer;
    }

    public void initWpagTgaImpRicorSpaces() {
        fill(Pos.WPAG_TGA_IMP_RICOR, Len.WPAG_TGA_IMP_RICOR, Types.SPACE_CHAR);
    }

    public void setWpagTgaImpRicorNull(String wpagTgaImpRicorNull) {
        writeString(Pos.WPAG_TGA_IMP_RICOR_NULL, wpagTgaImpRicorNull, Len.WPAG_TGA_IMP_RICOR_NULL);
    }

    /**Original name: WPAG-TGA-IMP-RICOR-NULL<br>*/
    public String getWpagTgaImpRicorNull() {
        return readString(Pos.WPAG_TGA_IMP_RICOR_NULL, Len.WPAG_TGA_IMP_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMP_RICOR = 1;
        public static final int WPAG_TGA_IMP_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMP_RICOR = 8;
        public static final int WPAG_TGA_IMP_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_IMP_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_IMP_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
