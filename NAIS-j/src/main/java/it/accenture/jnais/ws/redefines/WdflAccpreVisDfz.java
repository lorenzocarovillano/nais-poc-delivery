package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ACCPRE-VIS-DFZ<br>
 * Variable: WDFL-ACCPRE-VIS-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAccpreVisDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAccpreVisDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ACCPRE_VIS_DFZ;
    }

    public void setWdflAccpreVisDfz(AfDecimal wdflAccpreVisDfz) {
        writeDecimalAsPacked(Pos.WDFL_ACCPRE_VIS_DFZ, wdflAccpreVisDfz.copy());
    }

    public void setWdflAccpreVisDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ACCPRE_VIS_DFZ, Pos.WDFL_ACCPRE_VIS_DFZ);
    }

    /**Original name: WDFL-ACCPRE-VIS-DFZ<br>*/
    public AfDecimal getWdflAccpreVisDfz() {
        return readPackedAsDecimal(Pos.WDFL_ACCPRE_VIS_DFZ, Len.Int.WDFL_ACCPRE_VIS_DFZ, Len.Fract.WDFL_ACCPRE_VIS_DFZ);
    }

    public byte[] getWdflAccpreVisDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ACCPRE_VIS_DFZ, Pos.WDFL_ACCPRE_VIS_DFZ);
        return buffer;
    }

    public void setWdflAccpreVisDfzNull(String wdflAccpreVisDfzNull) {
        writeString(Pos.WDFL_ACCPRE_VIS_DFZ_NULL, wdflAccpreVisDfzNull, Len.WDFL_ACCPRE_VIS_DFZ_NULL);
    }

    /**Original name: WDFL-ACCPRE-VIS-DFZ-NULL<br>*/
    public String getWdflAccpreVisDfzNull() {
        return readString(Pos.WDFL_ACCPRE_VIS_DFZ_NULL, Len.WDFL_ACCPRE_VIS_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_VIS_DFZ = 1;
        public static final int WDFL_ACCPRE_VIS_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_VIS_DFZ = 8;
        public static final int WDFL_ACCPRE_VIS_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_VIS_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_VIS_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
