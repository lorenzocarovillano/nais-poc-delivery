package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB05-DT-RIS<br>
 * Variable: WB05-DT-RIS from program LLBS0250<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb05DtRis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb05DtRis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB05_DT_RIS;
    }

    public void setWb05DtRis(int wb05DtRis) {
        writeIntAsPacked(Pos.WB05_DT_RIS, wb05DtRis, Len.Int.WB05_DT_RIS);
    }

    public void setWb05DtRisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB05_DT_RIS, Pos.WB05_DT_RIS);
    }

    /**Original name: WB05-DT-RIS<br>*/
    public int getWb05DtRis() {
        return readPackedAsInt(Pos.WB05_DT_RIS, Len.Int.WB05_DT_RIS);
    }

    public byte[] getWb05DtRisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB05_DT_RIS, Pos.WB05_DT_RIS);
        return buffer;
    }

    public void setWb05DtRisNull(String wb05DtRisNull) {
        writeString(Pos.WB05_DT_RIS_NULL, wb05DtRisNull, Len.WB05_DT_RIS_NULL);
    }

    /**Original name: WB05-DT-RIS-NULL<br>*/
    public String getWb05DtRisNull() {
        return readString(Pos.WB05_DT_RIS_NULL, Len.WB05_DT_RIS_NULL);
    }

    public String getWb05DtRisNullFormatted() {
        return Functions.padBlanks(getWb05DtRisNull(), Len.WB05_DT_RIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB05_DT_RIS = 1;
        public static final int WB05_DT_RIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB05_DT_RIS = 5;
        public static final int WB05_DT_RIS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB05_DT_RIS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
