package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IEAI9901-AREA<br>
 * Variable: IEAI9901-AREA from copybook IEAI9901<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ieai9901Area extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IEAI9901-COD-SERVIZIO-BE
    private String codServizioBe = DefaultValues.stringVal(Len.COD_SERVIZIO_BE);
    //Original name: IEAI9901-LABEL-ERR
    private String labelErr = DefaultValues.stringVal(Len.LABEL_ERR);
    //Original name: IEAI9901-COD-ERRORE
    private String codErrore = "000000";
    //Original name: IEAI9901-PARAMETRI-ERR
    private String parametriErr = DefaultValues.stringVal(Len.PARAMETRI_ERR);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IEAI9901_AREA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIeai9901AreaBytes(buf);
    }

    public String getIeai9901AreaFormatted() {
        return MarshalByteExt.bufferToStr(getIeai9901AreaBytes());
    }

    public void setIeai9901AreaBytes(byte[] buffer) {
        setIeai9901AreaBytes(buffer, 1);
    }

    public byte[] getIeai9901AreaBytes() {
        byte[] buffer = new byte[Len.IEAI9901_AREA];
        return getIeai9901AreaBytes(buffer, 1);
    }

    public void setIeai9901AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        codServizioBe = MarshalByte.readString(buffer, position, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        labelErr = MarshalByte.readString(buffer, position, Len.LABEL_ERR);
        position += Len.LABEL_ERR;
        codErrore = MarshalByte.readFixedString(buffer, position, Len.COD_ERRORE);
        position += Len.COD_ERRORE;
        parametriErr = MarshalByte.readString(buffer, position, Len.PARAMETRI_ERR);
    }

    public byte[] getIeai9901AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codServizioBe, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        MarshalByte.writeString(buffer, position, labelErr, Len.LABEL_ERR);
        position += Len.LABEL_ERR;
        MarshalByte.writeString(buffer, position, codErrore, Len.COD_ERRORE);
        position += Len.COD_ERRORE;
        MarshalByte.writeString(buffer, position, parametriErr, Len.PARAMETRI_ERR);
        return buffer;
    }

    public void setCodServizioBe(String codServizioBe) {
        this.codServizioBe = Functions.subString(codServizioBe, Len.COD_SERVIZIO_BE);
    }

    public String getCodServizioBe() {
        return this.codServizioBe;
    }

    public String getCodServizioBeFormatted() {
        return Functions.padBlanks(getCodServizioBe(), Len.COD_SERVIZIO_BE);
    }

    public void setLabelErr(String labelErr) {
        this.labelErr = Functions.subString(labelErr, Len.LABEL_ERR);
    }

    public String getLabelErr() {
        return this.labelErr;
    }

    public void setCodErrore(int codErrore) {
        this.codErrore = NumericDisplay.asString(codErrore, Len.COD_ERRORE);
    }

    public void setCodErroreFormatted(String codErrore) {
        this.codErrore = Trunc.toUnsignedNumeric(codErrore, Len.COD_ERRORE);
    }

    public int getCodErrore() {
        return NumericDisplay.asInt(this.codErrore);
    }

    public String getCodErroreFormatted() {
        return this.codErrore;
    }

    public String getCodErroreAsString() {
        return getCodErroreFormatted();
    }

    public void setParametriErr(String parametriErr) {
        this.parametriErr = Functions.subString(parametriErr, Len.PARAMETRI_ERR);
    }

    public String getParametriErr() {
        return this.parametriErr;
    }

    public String getParametriErrFormatted() {
        return Functions.padBlanks(getParametriErr(), Len.PARAMETRI_ERR);
    }

    @Override
    public byte[] serialize() {
        return getIeai9901AreaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_SERVIZIO_BE = 8;
        public static final int LABEL_ERR = 30;
        public static final int COD_ERRORE = 6;
        public static final int PARAMETRI_ERR = 100;
        public static final int IEAI9901_AREA = COD_SERVIZIO_BE + LABEL_ERR + COD_ERRORE + PARAMETRI_ERR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
