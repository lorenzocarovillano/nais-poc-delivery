package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-FRAZ-PRE-PP<br>
 * Variable: RST-FRAZ-PRE-PP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstFrazPrePp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstFrazPrePp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_FRAZ_PRE_PP;
    }

    public void setRstFrazPrePp(AfDecimal rstFrazPrePp) {
        writeDecimalAsPacked(Pos.RST_FRAZ_PRE_PP, rstFrazPrePp.copy());
    }

    public void setRstFrazPrePpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_FRAZ_PRE_PP, Pos.RST_FRAZ_PRE_PP);
    }

    /**Original name: RST-FRAZ-PRE-PP<br>*/
    public AfDecimal getRstFrazPrePp() {
        return readPackedAsDecimal(Pos.RST_FRAZ_PRE_PP, Len.Int.RST_FRAZ_PRE_PP, Len.Fract.RST_FRAZ_PRE_PP);
    }

    public byte[] getRstFrazPrePpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_FRAZ_PRE_PP, Pos.RST_FRAZ_PRE_PP);
        return buffer;
    }

    public void setRstFrazPrePpNull(String rstFrazPrePpNull) {
        writeString(Pos.RST_FRAZ_PRE_PP_NULL, rstFrazPrePpNull, Len.RST_FRAZ_PRE_PP_NULL);
    }

    /**Original name: RST-FRAZ-PRE-PP-NULL<br>*/
    public String getRstFrazPrePpNull() {
        return readString(Pos.RST_FRAZ_PRE_PP_NULL, Len.RST_FRAZ_PRE_PP_NULL);
    }

    public String getRstFrazPrePpNullFormatted() {
        return Functions.padBlanks(getRstFrazPrePpNull(), Len.RST_FRAZ_PRE_PP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_FRAZ_PRE_PP = 1;
        public static final int RST_FRAZ_PRE_PP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_FRAZ_PRE_PP = 8;
        public static final int RST_FRAZ_PRE_PP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_FRAZ_PRE_PP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_FRAZ_PRE_PP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
