package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WREC-OUT<br>
 * Variable: WREC-OUT from program LOAS0310<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrecOut extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrecOut() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WREC_OUT;
    }

    public void setWrecOut(String wrecOut) {
        writeString(Pos.WREC_OUT, wrecOut, Len.WREC_OUT);
    }

    public void setWrecOutFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WREC_OUT, Pos.WREC_OUT);
    }

    /**Original name: WREC-OUT<br>*/
    public String getWrecOut() {
        return readString(Pos.WREC_OUT, Len.WREC_OUT);
    }

    public byte[] getWrecOutAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WREC_OUT, Pos.WREC_OUT);
        return buffer;
    }

    public void setNumPolIndofWrecGen(String numPolIndofWrecGen) {
        writeString(Pos.NUM_POL_INDOF_WREC_GEN, numPolIndofWrecGen, Len.NUM_POL_INDOF_WREC_GEN);
    }

    /**Original name: WREC-NUM-POL-IND<br>*/
    public String getNumPolIndofWrecGen() {
        return readString(Pos.NUM_POL_INDOF_WREC_GEN, Len.NUM_POL_INDOF_WREC_GEN);
    }

    public void setMacrofunz(String macrofunz) {
        writeString(Pos.MACROFUNZ, macrofunz, Len.MACROFUNZ);
    }

    /**Original name: WREC-MACROFUNZ<br>*/
    public String getMacrofunz() {
        return readString(Pos.MACROFUNZ, Len.MACROFUNZ);
    }

    public void setFunzFormatted(String funz) {
        writeString(Pos.FUNZ, Trunc.toUnsignedNumeric(funz, Len.FUNZ), Len.FUNZ);
    }

    /**Original name: WREC-FUNZ<br>*/
    public short getFunz() {
        return readNumDispUnsignedShort(Pos.FUNZ, Len.FUNZ);
    }

    public void setCodCompFormatted(String codComp) {
        writeString(Pos.COD_COMP, Trunc.toUnsignedNumeric(codComp, Len.COD_COMP), Len.COD_COMP);
    }

    /**Original name: WREC-COD-COMP<br>*/
    public int getCodComp() {
        return readNumDispUnsignedInt(Pos.COD_COMP, Len.COD_COMP);
    }

    public void setDtRichDaFormatted(String dtRichDa) {
        writeString(Pos.DT_RICH_DA, Trunc.toUnsignedNumeric(dtRichDa, Len.DT_RICH_DA), Len.DT_RICH_DA);
    }

    /**Original name: WREC-DT-RICH-DA<br>*/
    public int getDtRichDa() {
        return readNumDispUnsignedInt(Pos.DT_RICH_DA, Len.DT_RICH_DA);
    }

    public void setDtRichAFormatted(String dtRichA) {
        writeString(Pos.DT_RICH_A, Trunc.toUnsignedNumeric(dtRichA, Len.DT_RICH_A), Len.DT_RICH_A);
    }

    /**Original name: WREC-DT-RICH-A<br>*/
    public int getDtRichA() {
        return readNumDispUnsignedInt(Pos.DT_RICH_A, Len.DT_RICH_A);
    }

    public void setRamo(String ramo) {
        writeString(Pos.RAMO, ramo, Len.RAMO);
    }

    /**Original name: WREC-RAMO<br>*/
    public String getRamo() {
        return readString(Pos.RAMO, Len.RAMO);
    }

    public void setNumPolColl(String numPolColl) {
        writeString(Pos.NUM_POL_COLL, numPolColl, Len.NUM_POL_COLL);
    }

    /**Original name: WREC-NUM-POL-COLL<br>*/
    public String getNumPolColl() {
        return readString(Pos.NUM_POL_COLL, Len.NUM_POL_COLL);
    }

    public void setNumAdesione(String numAdesione) {
        writeString(Pos.NUM_ADESIONE, numAdesione, Len.NUM_ADESIONE);
    }

    /**Original name: WREC-NUM-ADESIONE<br>*/
    public String getNumAdesione() {
        return readString(Pos.NUM_ADESIONE, Len.NUM_ADESIONE);
    }

    public void setDtCompRivaFormatted(String dtCompRiva) {
        writeString(Pos.DT_COMP_RIVA, Trunc.toUnsignedNumeric(dtCompRiva, Len.DT_COMP_RIVA), Len.DT_COMP_RIVA);
    }

    /**Original name: WREC-DT-COMP-RIVA<br>*/
    public int getDtCompRiva() {
        return readNumDispUnsignedInt(Pos.DT_COMP_RIVA, Len.DT_COMP_RIVA);
    }

    public void setNumPolIndofWrecDet(String numPolIndofWrecDet) {
        writeString(Pos.NUM_POL_INDOF_WREC_DET, numPolIndofWrecDet, Len.NUM_POL_INDOF_WREC_DET);
    }

    /**Original name: WREC-NUM-POL-IND<br>*/
    public String getNumPolIndofWrecDet() {
        return readString(Pos.NUM_POL_INDOF_WREC_DET, Len.NUM_POL_INDOF_WREC_DET);
    }

    public void setCodGarTrch(String codGarTrch) {
        writeString(Pos.COD_GAR_TRCH, codGarTrch, Len.COD_GAR_TRCH);
    }

    /**Original name: WREC-COD-GAR-TRCH<br>*/
    public String getCodGarTrch() {
        return readString(Pos.COD_GAR_TRCH, Len.COD_GAR_TRCH);
    }

    public void setNumTrch(String numTrch) {
        writeString(Pos.NUM_TRCH, numTrch, Len.NUM_TRCH);
    }

    /**Original name: WREC-NUM-TRCH<br>*/
    public String getNumTrch() {
        return readString(Pos.NUM_TRCH, Len.NUM_TRCH);
    }

    public void setDtDecorTrch(int dtDecorTrch) {
        writeInt(Pos.DT_DECOR_TRCH, dtDecorTrch, Len.Int.DT_DECOR_TRCH, SignType.NO_SIGN);
    }

    /**Original name: WREC-DT-DECOR-TRCH<br>*/
    public int getDtDecorTrch() {
        return readNumDispUnsignedInt(Pos.DT_DECOR_TRCH, Len.DT_DECOR_TRCH);
    }

    public void setDtUltRivaTrch(int dtUltRivaTrch) {
        writeInt(Pos.DT_ULT_RIVA_TRCH, dtUltRivaTrch, Len.Int.DT_ULT_RIVA_TRCH, SignType.NO_SIGN);
    }

    /**Original name: WREC-DT-ULT-RIVA-TRCH<br>*/
    public int getDtUltRivaTrch() {
        return readNumDispUnsignedInt(Pos.DT_ULT_RIVA_TRCH, Len.DT_ULT_RIVA_TRCH);
    }

    public void setPreRivto(AfDecimal preRivto) {
        writeDecimal(Pos.PRE_RIVTO, preRivto.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-PRE-RIVTO<br>*/
    public AfDecimal getPreRivto() {
        return readDecimal(Pos.PRE_RIVTO, Len.Int.PRE_RIVTO, Len.Fract.PRE_RIVTO, SignType.NO_SIGN);
    }

    public void setPrstzPrec(AfDecimal prstzPrec) {
        writeDecimal(Pos.PRSTZ_PREC, prstzPrec.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-PRSTZ-PREC<br>*/
    public AfDecimal getPrstzPrec() {
        return readDecimal(Pos.PRSTZ_PREC, Len.Int.PRSTZ_PREC, Len.Fract.PRSTZ_PREC, SignType.NO_SIGN);
    }

    public void setPrstzUlt(AfDecimal prstzUlt) {
        writeDecimal(Pos.PRSTZ_ULT, prstzUlt.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-PRSTZ-ULT<br>*/
    public AfDecimal getPrstzUlt() {
        return readDecimal(Pos.PRSTZ_ULT, Len.Int.PRSTZ_ULT, Len.Fract.PRSTZ_ULT, SignType.NO_SIGN);
    }

    public void setPrePpUlt(AfDecimal prePpUlt) {
        writeDecimal(Pos.PRE_PP_ULT, prePpUlt.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-PRE-PP-ULT<br>*/
    public AfDecimal getPrePpUlt() {
        return readDecimal(Pos.PRE_PP_ULT, Len.Int.PRE_PP_ULT, Len.Fract.PRE_PP_ULT, SignType.NO_SIGN);
    }

    public void setImpSoprSan(AfDecimal impSoprSan) {
        writeDecimal(Pos.IMP_SOPR_SAN, impSoprSan.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-IMP-SOPR-SAN<br>*/
    public AfDecimal getImpSoprSan() {
        return readDecimal(Pos.IMP_SOPR_SAN, Len.Int.IMP_SOPR_SAN, Len.Fract.IMP_SOPR_SAN, SignType.NO_SIGN);
    }

    public void setImpSoprProf(AfDecimal impSoprProf) {
        writeDecimal(Pos.IMP_SOPR_PROF, impSoprProf.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-IMP-SOPR-PROF<br>*/
    public AfDecimal getImpSoprProf() {
        return readDecimal(Pos.IMP_SOPR_PROF, Len.Int.IMP_SOPR_PROF, Len.Fract.IMP_SOPR_PROF, SignType.NO_SIGN);
    }

    public void setImpSoprSpo(AfDecimal impSoprSpo) {
        writeDecimal(Pos.IMP_SOPR_SPO, impSoprSpo.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-IMP-SOPR-SPO<br>*/
    public AfDecimal getImpSoprSpo() {
        return readDecimal(Pos.IMP_SOPR_SPO, Len.Int.IMP_SOPR_SPO, Len.Fract.IMP_SOPR_SPO, SignType.NO_SIGN);
    }

    public void setImpSoprTec(AfDecimal impSoprTec) {
        writeDecimal(Pos.IMP_SOPR_TEC, impSoprTec.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-IMP-SOPR-TEC<br>*/
    public AfDecimal getImpSoprTec() {
        return readDecimal(Pos.IMP_SOPR_TEC, Len.Int.IMP_SOPR_TEC, Len.Fract.IMP_SOPR_TEC, SignType.NO_SIGN);
    }

    public void setImpAltSopr(AfDecimal impAltSopr) {
        writeDecimal(Pos.IMP_ALT_SOPR, impAltSopr.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-IMP-ALT-SOPR<br>*/
    public AfDecimal getImpAltSopr() {
        return readDecimal(Pos.IMP_ALT_SOPR, Len.Int.IMP_ALT_SOPR, Len.Fract.IMP_ALT_SOPR, SignType.NO_SIGN);
    }

    public void setPreUniRivto(AfDecimal preUniRivto) {
        writeDecimal(Pos.PRE_UNI_RIVTO, preUniRivto.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-PRE-UNI-RIVTO<br>*/
    public AfDecimal getPreUniRivto() {
        return readDecimal(Pos.PRE_UNI_RIVTO, Len.Int.PRE_UNI_RIVTO, Len.Fract.PRE_UNI_RIVTO, SignType.NO_SIGN);
    }

    public void setPreInvrioUlt(AfDecimal preInvrioUlt) {
        writeDecimal(Pos.PRE_INVRIO_ULT, preInvrioUlt.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-PRE-INVRIO-ULT<br>*/
    public AfDecimal getPreInvrioUlt() {
        return readDecimal(Pos.PRE_INVRIO_ULT, Len.Int.PRE_INVRIO_ULT, Len.Fract.PRE_INVRIO_ULT, SignType.NO_SIGN);
    }

    public void setRisMat(AfDecimal risMat) {
        writeDecimal(Pos.RIS_MAT, risMat.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-RIS-MAT<br>*/
    public AfDecimal getRisMat() {
        return readDecimal(Pos.RIS_MAT, Len.Int.RIS_MAT, Len.Fract.RIS_MAT, SignType.NO_SIGN);
    }

    public void setCptInOpzRivto(AfDecimal cptInOpzRivto) {
        writeDecimal(Pos.CPT_IN_OPZ_RIVTO, cptInOpzRivto.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-CPT-IN-OPZ-RIVTO<br>*/
    public AfDecimal getCptInOpzRivto() {
        return readDecimal(Pos.CPT_IN_OPZ_RIVTO, Len.Int.CPT_IN_OPZ_RIVTO, Len.Fract.CPT_IN_OPZ_RIVTO, SignType.NO_SIGN);
    }

    public void setRendtoLrd(AfDecimal rendtoLrd) {
        writeDecimal(Pos.RENDTO_LRD, rendtoLrd.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-RENDTO-LRD<br>*/
    public AfDecimal getRendtoLrd() {
        return readDecimal(Pos.RENDTO_LRD, Len.Int.RENDTO_LRD, Len.Fract.RENDTO_LRD, SignType.NO_SIGN);
    }

    public void setPcRetr(AfDecimal pcRetr) {
        writeDecimal(Pos.PC_RETR, pcRetr.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-PC-RETR<br>*/
    public AfDecimal getPcRetr() {
        return readDecimal(Pos.PC_RETR, Len.Int.PC_RETR, Len.Fract.PC_RETR, SignType.NO_SIGN);
    }

    public void setMinTrnut(AfDecimal minTrnut) {
        writeDecimal(Pos.MIN_TRNUT, minTrnut.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-MIN-TRNUT<br>*/
    public AfDecimal getMinTrnut() {
        return readDecimal(Pos.MIN_TRNUT, Len.Int.MIN_TRNUT, Len.Fract.MIN_TRNUT, SignType.NO_SIGN);
    }

    public void setMinGarto(AfDecimal minGarto) {
        writeDecimal(Pos.MIN_GARTO, minGarto.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-MIN-GARTO<br>*/
    public AfDecimal getMinGarto() {
        return readDecimal(Pos.MIN_GARTO, Len.Int.MIN_GARTO, Len.Fract.MIN_GARTO, SignType.NO_SIGN);
    }

    public void setRendtoRetr(AfDecimal rendtoRetr) {
        writeDecimal(Pos.RENDTO_RETR, rendtoRetr.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-RENDTO-RETR<br>*/
    public AfDecimal getRendtoRetr() {
        return readDecimal(Pos.RENDTO_RETR, Len.Int.RENDTO_RETR, Len.Fract.RENDTO_RETR, SignType.NO_SIGN);
    }

    public void setRendtoNet(AfDecimal rendtoNet) {
        writeDecimal(Pos.RENDTO_NET, rendtoNet.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-RENDTO-NET<br>*/
    public AfDecimal getRendtoNet() {
        return readDecimal(Pos.RENDTO_NET, Len.Int.RENDTO_NET, Len.Fract.RENDTO_NET, SignType.NO_SIGN);
    }

    public void setCptGartoAScad(AfDecimal cptGartoAScad) {
        writeDecimal(Pos.CPT_GARTO_A_SCAD, cptGartoAScad.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-CPT-GARTO-A-SCAD<br>*/
    public AfDecimal getCptGartoAScad() {
        return readDecimal(Pos.CPT_GARTO_A_SCAD, Len.Int.CPT_GARTO_A_SCAD, Len.Fract.CPT_GARTO_A_SCAD, SignType.NO_SIGN);
    }

    public void setPreCasoMor(AfDecimal preCasoMor) {
        writeDecimal(Pos.PRE_CASO_MOR, preCasoMor.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-PRE-CASO-MOR<br>*/
    public AfDecimal getPreCasoMor() {
        return readDecimal(Pos.PRE_CASO_MOR, Len.Int.PRE_CASO_MOR, Len.Fract.PRE_CASO_MOR, SignType.NO_SIGN);
    }

    public void setAbbAnnuUlt(AfDecimal abbAnnuUlt) {
        writeDecimal(Pos.ABB_ANNU_ULT, abbAnnuUlt.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-ABB-ANNU-ULT<br>*/
    public AfDecimal getAbbAnnuUlt() {
        return readDecimal(Pos.ABB_ANNU_ULT, Len.Int.ABB_ANNU_ULT, Len.Fract.ABB_ANNU_ULT, SignType.NO_SIGN);
    }

    public void setCommGest(AfDecimal commGest) {
        writeDecimal(Pos.COMM_GEST, commGest.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-COMM-GEST<br>*/
    public AfDecimal getCommGest() {
        return readDecimal(Pos.COMM_GEST, Len.Int.COMM_GEST, Len.Fract.COMM_GEST, SignType.NO_SIGN);
    }

    public void setIncrPrstz(AfDecimal incrPrstz) {
        writeDecimal(Pos.INCR_PRSTZ, incrPrstz.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-INCR-PRSTZ<br>*/
    public AfDecimal getIncrPrstz() {
        return readDecimal(Pos.INCR_PRSTZ, Len.Int.INCR_PRSTZ, Len.Fract.INCR_PRSTZ, SignType.NO_SIGN);
    }

    public void setIncrPre(AfDecimal incrPre) {
        writeDecimal(Pos.INCR_PRE, incrPre.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-INCR-PRE<br>*/
    public AfDecimal getIncrPre() {
        return readDecimal(Pos.INCR_PRE, Len.Int.INCR_PRE, Len.Fract.INCR_PRE, SignType.NO_SIGN);
    }

    public void setPrstzAggUlt(AfDecimal prstzAggUlt) {
        writeDecimal(Pos.PRSTZ_AGG_ULT, prstzAggUlt.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-PRSTZ-AGG-ULT<br>*/
    public AfDecimal getPrstzAggUlt() {
        return readDecimal(Pos.PRSTZ_AGG_ULT, Len.Int.PRSTZ_AGG_ULT, Len.Fract.PRSTZ_AGG_ULT, SignType.NO_SIGN);
    }

    public void setManfeeRicor(AfDecimal manfeeRicor) {
        writeDecimal(Pos.MANFEE_RICOR, manfeeRicor.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-MANFEE-RICOR<br>*/
    public AfDecimal getManfeeRicor() {
        return readDecimal(Pos.MANFEE_RICOR, Len.Int.MANFEE_RICOR, Len.Fract.MANFEE_RICOR, SignType.NO_SIGN);
    }

    public void setDtVltTit(int dtVltTit) {
        writeInt(Pos.DT_VLT_TIT, dtVltTit, Len.Int.DT_VLT_TIT, SignType.NO_SIGN);
    }

    /**Original name: WREC-DT-VLT-TIT<br>*/
    public int getDtVltTit() {
        return readNumDispUnsignedInt(Pos.DT_VLT_TIT, Len.DT_VLT_TIT);
    }

    public void setNumGgRival(int numGgRival) {
        writeInt(Pos.NUM_GG_RIVAL, numGgRival, Len.Int.NUM_GG_RIVAL, SignType.NO_SIGN);
    }

    /**Original name: WREC-NUM-GG-RIVAL<br>*/
    public int getNumGgRival() {
        return readNumDispUnsignedInt(Pos.NUM_GG_RIVAL, Len.NUM_GG_RIVAL);
    }

    public void setIntMora(AfDecimal intMora) {
        writeDecimal(Pos.INT_MORA, intMora.copy(), SignType.NO_SIGN);
    }

    /**Original name: WREC-INT-MORA<br>*/
    public AfDecimal getIntMora() {
        return readDecimal(Pos.INT_MORA, Len.Int.INT_MORA, Len.Fract.INT_MORA, SignType.NO_SIGN);
    }

    public void setNumGgRitPag(int numGgRitPag) {
        writeInt(Pos.NUM_GG_RIT_PAG, numGgRitPag, Len.Int.NUM_GG_RIT_PAG, SignType.NO_SIGN);
    }

    /**Original name: WREC-NUM-GG-RIT-PAG<br>*/
    public int getNumGgRitPag() {
        return readNumDispUnsignedInt(Pos.NUM_GG_RIT_PAG, Len.NUM_GG_RIT_PAG);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WREC_OUT = 1;
        public static final int WREC_GEN = 1;
        public static final int NUM_POL_INDOF_WREC_GEN = WREC_GEN;
        public static final int MACROFUNZ = NUM_POL_INDOF_WREC_GEN + Len.NUM_POL_INDOF_WREC_GEN;
        public static final int FUNZ = MACROFUNZ + Len.MACROFUNZ;
        public static final int COD_COMP = FUNZ + Len.FUNZ;
        public static final int DT_RICH_DA = COD_COMP + Len.COD_COMP;
        public static final int DT_RICH_A = DT_RICH_DA + Len.DT_RICH_DA;
        public static final int RAMO = DT_RICH_A + Len.DT_RICH_A;
        public static final int NUM_POL_COLL = RAMO + Len.RAMO;
        public static final int NUM_ADESIONE = NUM_POL_COLL + Len.NUM_POL_COLL;
        public static final int DT_COMP_RIVA = NUM_ADESIONE + Len.NUM_ADESIONE;
        public static final int FLR1 = DT_COMP_RIVA + Len.DT_COMP_RIVA;
        public static final int WREC_DET = 1;
        public static final int NUM_POL_INDOF_WREC_DET = WREC_DET;
        public static final int COD_GAR_TRCH = NUM_POL_INDOF_WREC_DET + Len.NUM_POL_INDOF_WREC_DET;
        public static final int NUM_TRCH = COD_GAR_TRCH + Len.COD_GAR_TRCH;
        public static final int DT_DECOR_TRCH = NUM_TRCH + Len.NUM_TRCH;
        public static final int DT_ULT_RIVA_TRCH = DT_DECOR_TRCH + Len.DT_DECOR_TRCH;
        public static final int PRE_RIVTO = DT_ULT_RIVA_TRCH + Len.DT_ULT_RIVA_TRCH;
        public static final int PRSTZ_PREC = PRE_RIVTO + Len.PRE_RIVTO;
        public static final int PRSTZ_ULT = PRSTZ_PREC + Len.PRSTZ_PREC;
        public static final int PRE_PP_ULT = PRSTZ_ULT + Len.PRSTZ_ULT;
        public static final int IMP_SOPR_SAN = PRE_PP_ULT + Len.PRE_PP_ULT;
        public static final int IMP_SOPR_PROF = IMP_SOPR_SAN + Len.IMP_SOPR_SAN;
        public static final int IMP_SOPR_SPO = IMP_SOPR_PROF + Len.IMP_SOPR_PROF;
        public static final int IMP_SOPR_TEC = IMP_SOPR_SPO + Len.IMP_SOPR_SPO;
        public static final int IMP_ALT_SOPR = IMP_SOPR_TEC + Len.IMP_SOPR_TEC;
        public static final int PRE_UNI_RIVTO = IMP_ALT_SOPR + Len.IMP_ALT_SOPR;
        public static final int PRE_INVRIO_ULT = PRE_UNI_RIVTO + Len.PRE_UNI_RIVTO;
        public static final int RIS_MAT = PRE_INVRIO_ULT + Len.PRE_INVRIO_ULT;
        public static final int CPT_IN_OPZ_RIVTO = RIS_MAT + Len.RIS_MAT;
        public static final int RENDTO_LRD = CPT_IN_OPZ_RIVTO + Len.CPT_IN_OPZ_RIVTO;
        public static final int PC_RETR = RENDTO_LRD + Len.RENDTO_LRD;
        public static final int MIN_TRNUT = PC_RETR + Len.PC_RETR;
        public static final int MIN_GARTO = MIN_TRNUT + Len.MIN_TRNUT;
        public static final int RENDTO_RETR = MIN_GARTO + Len.MIN_GARTO;
        public static final int RENDTO_NET = RENDTO_RETR + Len.RENDTO_RETR;
        public static final int CPT_GARTO_A_SCAD = RENDTO_NET + Len.RENDTO_NET;
        public static final int PRE_CASO_MOR = CPT_GARTO_A_SCAD + Len.CPT_GARTO_A_SCAD;
        public static final int ABB_ANNU_ULT = PRE_CASO_MOR + Len.PRE_CASO_MOR;
        public static final int COMM_GEST = ABB_ANNU_ULT + Len.ABB_ANNU_ULT;
        public static final int INCR_PRSTZ = COMM_GEST + Len.COMM_GEST;
        public static final int INCR_PRE = INCR_PRSTZ + Len.INCR_PRSTZ;
        public static final int PRSTZ_AGG_ULT = INCR_PRE + Len.INCR_PRE;
        public static final int MANFEE_RICOR = PRSTZ_AGG_ULT + Len.PRSTZ_AGG_ULT;
        public static final int DT_VLT_TIT = MANFEE_RICOR + Len.MANFEE_RICOR;
        public static final int NUM_GG_RIVAL = DT_VLT_TIT + Len.DT_VLT_TIT;
        public static final int INT_MORA = NUM_GG_RIVAL + Len.NUM_GG_RIVAL;
        public static final int NUM_GG_RIT_PAG = INT_MORA + Len.INT_MORA;
        public static final int FLR2 = NUM_GG_RIT_PAG + Len.NUM_GG_RIT_PAG;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int NUM_POL_INDOF_WREC_GEN = 40;
        public static final int MACROFUNZ = 2;
        public static final int FUNZ = 4;
        public static final int COD_COMP = 5;
        public static final int DT_RICH_DA = 8;
        public static final int DT_RICH_A = 8;
        public static final int RAMO = 12;
        public static final int NUM_POL_COLL = 40;
        public static final int NUM_ADESIONE = 40;
        public static final int DT_COMP_RIVA = 8;
        public static final int NUM_POL_INDOF_WREC_DET = 40;
        public static final int COD_GAR_TRCH = 12;
        public static final int NUM_TRCH = 40;
        public static final int DT_DECOR_TRCH = 8;
        public static final int DT_ULT_RIVA_TRCH = 8;
        public static final int PRE_RIVTO = 15;
        public static final int PRSTZ_PREC = 15;
        public static final int PRSTZ_ULT = 15;
        public static final int PRE_PP_ULT = 15;
        public static final int IMP_SOPR_SAN = 15;
        public static final int IMP_SOPR_PROF = 15;
        public static final int IMP_SOPR_SPO = 15;
        public static final int IMP_SOPR_TEC = 15;
        public static final int IMP_ALT_SOPR = 15;
        public static final int PRE_UNI_RIVTO = 15;
        public static final int PRE_INVRIO_ULT = 15;
        public static final int RIS_MAT = 15;
        public static final int CPT_IN_OPZ_RIVTO = 15;
        public static final int RENDTO_LRD = 5;
        public static final int PC_RETR = 6;
        public static final int MIN_TRNUT = 6;
        public static final int MIN_GARTO = 5;
        public static final int RENDTO_RETR = 5;
        public static final int RENDTO_NET = 5;
        public static final int CPT_GARTO_A_SCAD = 15;
        public static final int PRE_CASO_MOR = 15;
        public static final int ABB_ANNU_ULT = 15;
        public static final int COMM_GEST = 15;
        public static final int INCR_PRSTZ = 15;
        public static final int INCR_PRE = 15;
        public static final int PRSTZ_AGG_ULT = 15;
        public static final int MANFEE_RICOR = 15;
        public static final int DT_VLT_TIT = 8;
        public static final int NUM_GG_RIVAL = 5;
        public static final int INT_MORA = 15;
        public static final int NUM_GG_RIT_PAG = 5;
        public static final int WREC_OUT = 497;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DT_DECOR_TRCH = 8;
            public static final int DT_ULT_RIVA_TRCH = 8;
            public static final int PRE_RIVTO = 12;
            public static final int PRSTZ_PREC = 12;
            public static final int PRSTZ_ULT = 12;
            public static final int PRE_PP_ULT = 12;
            public static final int IMP_SOPR_SAN = 12;
            public static final int IMP_SOPR_PROF = 12;
            public static final int IMP_SOPR_SPO = 12;
            public static final int IMP_SOPR_TEC = 12;
            public static final int IMP_ALT_SOPR = 12;
            public static final int PRE_UNI_RIVTO = 12;
            public static final int PRE_INVRIO_ULT = 12;
            public static final int RIS_MAT = 12;
            public static final int CPT_IN_OPZ_RIVTO = 12;
            public static final int RENDTO_LRD = 3;
            public static final int PC_RETR = 3;
            public static final int MIN_TRNUT = 3;
            public static final int MIN_GARTO = 3;
            public static final int RENDTO_RETR = 3;
            public static final int RENDTO_NET = 3;
            public static final int CPT_GARTO_A_SCAD = 12;
            public static final int PRE_CASO_MOR = 12;
            public static final int ABB_ANNU_ULT = 12;
            public static final int COMM_GEST = 12;
            public static final int INCR_PRSTZ = 12;
            public static final int INCR_PRE = 12;
            public static final int PRSTZ_AGG_ULT = 12;
            public static final int MANFEE_RICOR = 12;
            public static final int DT_VLT_TIT = 8;
            public static final int NUM_GG_RIVAL = 5;
            public static final int INT_MORA = 12;
            public static final int NUM_GG_RIT_PAG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_RIVTO = 3;
            public static final int PRSTZ_PREC = 3;
            public static final int PRSTZ_ULT = 3;
            public static final int PRE_PP_ULT = 3;
            public static final int IMP_SOPR_SAN = 3;
            public static final int IMP_SOPR_PROF = 3;
            public static final int IMP_SOPR_SPO = 3;
            public static final int IMP_SOPR_TEC = 3;
            public static final int IMP_ALT_SOPR = 3;
            public static final int PRE_UNI_RIVTO = 3;
            public static final int PRE_INVRIO_ULT = 3;
            public static final int RIS_MAT = 3;
            public static final int CPT_IN_OPZ_RIVTO = 3;
            public static final int RENDTO_LRD = 2;
            public static final int PC_RETR = 3;
            public static final int MIN_TRNUT = 3;
            public static final int MIN_GARTO = 2;
            public static final int RENDTO_RETR = 2;
            public static final int RENDTO_NET = 2;
            public static final int CPT_GARTO_A_SCAD = 3;
            public static final int PRE_CASO_MOR = 3;
            public static final int ABB_ANNU_ULT = 3;
            public static final int COMM_GEST = 3;
            public static final int INCR_PRSTZ = 3;
            public static final int INCR_PRE = 3;
            public static final int PRSTZ_AGG_ULT = 3;
            public static final int MANFEE_RICOR = 3;
            public static final int INT_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
