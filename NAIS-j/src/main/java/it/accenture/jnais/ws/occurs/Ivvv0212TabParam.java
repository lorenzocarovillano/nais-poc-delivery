package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVV0212-TAB-PARAM<br>
 * Variables: IVVV0212-TAB-PARAM from copybook IVVV0212<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvv0212TabParam {

    //==== PROPERTIES ====
    //Original name: IVVV0212-COD-PARAM
    private String codParam = DefaultValues.stringVal(Len.COD_PARAM);
    //Original name: IVVV0212-VAL-PARAM
    private String valParam = DefaultValues.stringVal(Len.VAL_PARAM);

    //==== METHODS ====
    public void setTabParamBytes(byte[] buffer, int offset) {
        int position = offset;
        codParam = MarshalByte.readString(buffer, position, Len.COD_PARAM);
        position += Len.COD_PARAM;
        valParam = MarshalByte.readString(buffer, position, Len.VAL_PARAM);
    }

    public byte[] getTabParamBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codParam, Len.COD_PARAM);
        position += Len.COD_PARAM;
        MarshalByte.writeString(buffer, position, valParam, Len.VAL_PARAM);
        return buffer;
    }

    public void initTabParamSpaces() {
        codParam = "";
        valParam = "";
    }

    public void setCodParam(String codParam) {
        this.codParam = Functions.subString(codParam, Len.COD_PARAM);
    }

    public String getCodParam() {
        return this.codParam;
    }

    public void setValParam(String valParam) {
        this.valParam = Functions.subString(valParam, Len.VAL_PARAM);
    }

    public String getValParam() {
        return this.valParam;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_PARAM = 30;
        public static final int VAL_PARAM = 100;
        public static final int TAB_PARAM = COD_PARAM + VAL_PARAM;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
