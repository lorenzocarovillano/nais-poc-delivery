package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndOggBlocco;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS2470<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs2470Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-OGG-BLOCCO
    private IndOggBlocco indOggBlocco = new IndOggBlocco();
    //Original name: L11-DT-EFF-DB
    private String l11DtEffDb = DefaultValues.stringVal(Len.L11_DT_EFF_DB);

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setL11DtEffDb(String l11DtEffDb) {
        this.l11DtEffDb = Functions.subString(l11DtEffDb, Len.L11_DT_EFF_DB);
    }

    public String getL11DtEffDb() {
        return this.l11DtEffDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndOggBlocco getIndOggBlocco() {
        return indOggBlocco;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L11_DT_EFF_DB = 10;
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
