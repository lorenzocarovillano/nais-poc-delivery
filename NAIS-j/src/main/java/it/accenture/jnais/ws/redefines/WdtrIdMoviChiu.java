package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-ID-MOVI-CHIU<br>
 * Variable: WDTR-ID-MOVI-CHIU from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_ID_MOVI_CHIU;
    }

    public void setWdtrIdMoviChiu(int wdtrIdMoviChiu) {
        writeIntAsPacked(Pos.WDTR_ID_MOVI_CHIU, wdtrIdMoviChiu, Len.Int.WDTR_ID_MOVI_CHIU);
    }

    /**Original name: WDTR-ID-MOVI-CHIU<br>*/
    public int getWdtrIdMoviChiu() {
        return readPackedAsInt(Pos.WDTR_ID_MOVI_CHIU, Len.Int.WDTR_ID_MOVI_CHIU);
    }

    public void setWdtrIdMoviChiuNull(String wdtrIdMoviChiuNull) {
        writeString(Pos.WDTR_ID_MOVI_CHIU_NULL, wdtrIdMoviChiuNull, Len.WDTR_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WDTR-ID-MOVI-CHIU-NULL<br>*/
    public String getWdtrIdMoviChiuNull() {
        return readString(Pos.WDTR_ID_MOVI_CHIU_NULL, Len.WDTR_ID_MOVI_CHIU_NULL);
    }

    public String getWdtrIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getWdtrIdMoviChiuNull(), Len.WDTR_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_ID_MOVI_CHIU = 1;
        public static final int WDTR_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_ID_MOVI_CHIU = 5;
        public static final int WDTR_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
