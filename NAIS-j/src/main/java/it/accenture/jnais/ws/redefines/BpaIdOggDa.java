package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: BPA-ID-OGG-DA<br>
 * Variable: BPA-ID-OGG-DA from program IABS0130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BpaIdOggDa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BpaIdOggDa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BPA_ID_OGG_DA;
    }

    public void setBpaIdOggDa(int bpaIdOggDa) {
        writeIntAsPacked(Pos.BPA_ID_OGG_DA, bpaIdOggDa, Len.Int.BPA_ID_OGG_DA);
    }

    public void setBpaIdOggDaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BPA_ID_OGG_DA, Pos.BPA_ID_OGG_DA);
    }

    /**Original name: BPA-ID-OGG-DA<br>*/
    public int getBpaIdOggDa() {
        return readPackedAsInt(Pos.BPA_ID_OGG_DA, Len.Int.BPA_ID_OGG_DA);
    }

    public byte[] getBpaIdOggDaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BPA_ID_OGG_DA, Pos.BPA_ID_OGG_DA);
        return buffer;
    }

    public void initBpaIdOggDaHighValues() {
        fill(Pos.BPA_ID_OGG_DA, Len.BPA_ID_OGG_DA, Types.HIGH_CHAR_VAL);
    }

    public void setBpaIdOggDaNull(String bpaIdOggDaNull) {
        writeString(Pos.BPA_ID_OGG_DA_NULL, bpaIdOggDaNull, Len.BPA_ID_OGG_DA_NULL);
    }

    /**Original name: BPA-ID-OGG-DA-NULL<br>*/
    public String getBpaIdOggDaNull() {
        return readString(Pos.BPA_ID_OGG_DA_NULL, Len.BPA_ID_OGG_DA_NULL);
    }

    public String getBpaIdOggDaNullFormatted() {
        return Functions.padBlanks(getBpaIdOggDaNull(), Len.BPA_ID_OGG_DA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BPA_ID_OGG_DA = 1;
        public static final int BPA_ID_OGG_DA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BPA_ID_OGG_DA = 5;
        public static final int BPA_ID_OGG_DA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BPA_ID_OGG_DA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
