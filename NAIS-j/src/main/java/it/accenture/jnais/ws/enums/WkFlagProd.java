package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-FLAG-PROD<br>
 * Variable: WK-FLAG-PROD from program LOAS0310<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFlagProd {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_FLAG_PROD);
    public static final String OK = "SI";
    public static final String KO = "NO";

    //==== METHODS ====
    public void setWkFlagProd(String wkFlagProd) {
        this.value = Functions.subString(wkFlagProd, Len.WK_FLAG_PROD);
    }

    public String getWkFlagProd() {
        return this.value;
    }

    public boolean isOk() {
        return value.equals(OK);
    }

    public void setOk() {
        value = OK;
    }

    public boolean isKo() {
        return value.equals(KO);
    }

    public void setKo() {
        value = KO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_FLAG_PROD = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
