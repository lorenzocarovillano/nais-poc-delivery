package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-PROV-RICOR<br>
 * Variable: TIT-TOT-PROV-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_PROV_RICOR;
    }

    public void setTitTotProvRicor(AfDecimal titTotProvRicor) {
        writeDecimalAsPacked(Pos.TIT_TOT_PROV_RICOR, titTotProvRicor.copy());
    }

    public void setTitTotProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_PROV_RICOR, Pos.TIT_TOT_PROV_RICOR);
    }

    /**Original name: TIT-TOT-PROV-RICOR<br>*/
    public AfDecimal getTitTotProvRicor() {
        return readPackedAsDecimal(Pos.TIT_TOT_PROV_RICOR, Len.Int.TIT_TOT_PROV_RICOR, Len.Fract.TIT_TOT_PROV_RICOR);
    }

    public byte[] getTitTotProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_PROV_RICOR, Pos.TIT_TOT_PROV_RICOR);
        return buffer;
    }

    public void setTitTotProvRicorNull(String titTotProvRicorNull) {
        writeString(Pos.TIT_TOT_PROV_RICOR_NULL, titTotProvRicorNull, Len.TIT_TOT_PROV_RICOR_NULL);
    }

    /**Original name: TIT-TOT-PROV-RICOR-NULL<br>*/
    public String getTitTotProvRicorNull() {
        return readString(Pos.TIT_TOT_PROV_RICOR_NULL, Len.TIT_TOT_PROV_RICOR_NULL);
    }

    public String getTitTotProvRicorNullFormatted() {
        return Functions.padBlanks(getTitTotProvRicorNull(), Len.TIT_TOT_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PROV_RICOR = 1;
        public static final int TIT_TOT_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PROV_RICOR = 8;
        public static final int TIT_TOT_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
