package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: E12-DT-EMIS<br>
 * Variable: E12-DT-EMIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E12DtEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E12DtEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E12_DT_EMIS;
    }

    public void setE12DtEmis(int e12DtEmis) {
        writeIntAsPacked(Pos.E12_DT_EMIS, e12DtEmis, Len.Int.E12_DT_EMIS);
    }

    public void setE12DtEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E12_DT_EMIS, Pos.E12_DT_EMIS);
    }

    /**Original name: E12-DT-EMIS<br>*/
    public int getE12DtEmis() {
        return readPackedAsInt(Pos.E12_DT_EMIS, Len.Int.E12_DT_EMIS);
    }

    public byte[] getE12DtEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E12_DT_EMIS, Pos.E12_DT_EMIS);
        return buffer;
    }

    public void setE12DtEmisNull(String e12DtEmisNull) {
        writeString(Pos.E12_DT_EMIS_NULL, e12DtEmisNull, Len.E12_DT_EMIS_NULL);
    }

    /**Original name: E12-DT-EMIS-NULL<br>*/
    public String getE12DtEmisNull() {
        return readString(Pos.E12_DT_EMIS_NULL, Len.E12_DT_EMIS_NULL);
    }

    public String getE12DtEmisNullFormatted() {
        return Functions.padBlanks(getE12DtEmisNull(), Len.E12_DT_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E12_DT_EMIS = 1;
        public static final int E12_DT_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E12_DT_EMIS = 5;
        public static final int E12_DT_EMIS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E12_DT_EMIS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
