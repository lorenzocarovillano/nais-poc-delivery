package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-EC-MRM-COLL<br>
 * Variable: WPCO-DT-ULT-EC-MRM-COLL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEcMrmColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEcMrmColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_EC_MRM_COLL;
    }

    public void setWpcoDtUltEcMrmColl(int wpcoDtUltEcMrmColl) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_EC_MRM_COLL, wpcoDtUltEcMrmColl, Len.Int.WPCO_DT_ULT_EC_MRM_COLL);
    }

    public void setDpcoDtUltEcMrmCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_EC_MRM_COLL, Pos.WPCO_DT_ULT_EC_MRM_COLL);
    }

    /**Original name: WPCO-DT-ULT-EC-MRM-COLL<br>*/
    public int getWpcoDtUltEcMrmColl() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_EC_MRM_COLL, Len.Int.WPCO_DT_ULT_EC_MRM_COLL);
    }

    public byte[] getWpcoDtUltEcMrmCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_EC_MRM_COLL, Pos.WPCO_DT_ULT_EC_MRM_COLL);
        return buffer;
    }

    public void setWpcoDtUltEcMrmCollNull(String wpcoDtUltEcMrmCollNull) {
        writeString(Pos.WPCO_DT_ULT_EC_MRM_COLL_NULL, wpcoDtUltEcMrmCollNull, Len.WPCO_DT_ULT_EC_MRM_COLL_NULL);
    }

    /**Original name: WPCO-DT-ULT-EC-MRM-COLL-NULL<br>*/
    public String getWpcoDtUltEcMrmCollNull() {
        return readString(Pos.WPCO_DT_ULT_EC_MRM_COLL_NULL, Len.WPCO_DT_ULT_EC_MRM_COLL_NULL);
    }

    public String getWpcoDtUltEcMrmCollNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEcMrmCollNull(), Len.WPCO_DT_ULT_EC_MRM_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_MRM_COLL = 1;
        public static final int WPCO_DT_ULT_EC_MRM_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_MRM_COLL = 5;
        public static final int WPCO_DT_ULT_EC_MRM_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_EC_MRM_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
