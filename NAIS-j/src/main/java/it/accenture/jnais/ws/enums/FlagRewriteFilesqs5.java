package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-REWRITE-FILESQS5<br>
 * Variable: FLAG-REWRITE-FILESQS5 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagRewriteFilesqs5 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagRewriteFilesqs5(char flagRewriteFilesqs5) {
        this.value = flagRewriteFilesqs5;
    }

    public char getFlagRewriteFilesqs5() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setNo() {
        value = NO;
    }
}
