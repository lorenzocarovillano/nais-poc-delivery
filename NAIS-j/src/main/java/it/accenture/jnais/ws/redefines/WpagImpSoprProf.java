package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-SOPR-PROF<br>
 * Variable: WPAG-IMP-SOPR-PROF from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_SOPR_PROF;
    }

    public void setWpagImpSoprProf(AfDecimal wpagImpSoprProf) {
        writeDecimalAsPacked(Pos.WPAG_IMP_SOPR_PROF, wpagImpSoprProf.copy());
    }

    public void setWpagImpSoprProfFormatted(String wpagImpSoprProf) {
        setWpagImpSoprProf(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_SOPR_PROF + Len.Fract.WPAG_IMP_SOPR_PROF, Len.Fract.WPAG_IMP_SOPR_PROF, wpagImpSoprProf));
    }

    public void setWpagImpSoprProfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_SOPR_PROF, Pos.WPAG_IMP_SOPR_PROF);
    }

    /**Original name: WPAG-IMP-SOPR-PROF<br>*/
    public AfDecimal getWpagImpSoprProf() {
        return readPackedAsDecimal(Pos.WPAG_IMP_SOPR_PROF, Len.Int.WPAG_IMP_SOPR_PROF, Len.Fract.WPAG_IMP_SOPR_PROF);
    }

    public byte[] getWpagImpSoprProfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_SOPR_PROF, Pos.WPAG_IMP_SOPR_PROF);
        return buffer;
    }

    public void initWpagImpSoprProfSpaces() {
        fill(Pos.WPAG_IMP_SOPR_PROF, Len.WPAG_IMP_SOPR_PROF, Types.SPACE_CHAR);
    }

    public void setWpagImpSoprProfNull(String wpagImpSoprProfNull) {
        writeString(Pos.WPAG_IMP_SOPR_PROF_NULL, wpagImpSoprProfNull, Len.WPAG_IMP_SOPR_PROF_NULL);
    }

    /**Original name: WPAG-IMP-SOPR-PROF-NULL<br>*/
    public String getWpagImpSoprProfNull() {
        return readString(Pos.WPAG_IMP_SOPR_PROF_NULL, Len.WPAG_IMP_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SOPR_PROF = 1;
        public static final int WPAG_IMP_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SOPR_PROF = 8;
        public static final int WPAG_IMP_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
