package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LOAS0870<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLoas0870 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-TGA
    private short ixTabTga = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TGA-W870
    private short ixTgaW870 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TIT-W870
    private short ixTitW870 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-GRZ
    private short ixGrz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-PMO
    private short ixPmo = DefaultValues.BIN_SHORT_VAL;
    //Original name: INDPMO
    private short indpmo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ERR
    private short ixTabErr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-FLAGS
    private short ixTabFlags = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIxTabTga(short ixTabTga) {
        this.ixTabTga = ixTabTga;
    }

    public short getIxTabTga() {
        return this.ixTabTga;
    }

    public void setIxTgaW870(short ixTgaW870) {
        this.ixTgaW870 = ixTgaW870;
    }

    public short getIxTgaW870() {
        return this.ixTgaW870;
    }

    public void setIxTitW870(short ixTitW870) {
        this.ixTitW870 = ixTitW870;
    }

    public short getIxTitW870() {
        return this.ixTitW870;
    }

    public void setIxGrz(short ixGrz) {
        this.ixGrz = ixGrz;
    }

    public short getIxGrz() {
        return this.ixGrz;
    }

    public void setIxPmo(short ixPmo) {
        this.ixPmo = ixPmo;
    }

    public short getIxPmo() {
        return this.ixPmo;
    }

    public void setIndpmo(short indpmo) {
        this.indpmo = indpmo;
    }

    public short getIndpmo() {
        return this.indpmo;
    }

    public void setIxTabErr(short ixTabErr) {
        this.ixTabErr = ixTabErr;
    }

    public short getIxTabErr() {
        return this.ixTabErr;
    }

    public void setIxTabFlags(short ixTabFlags) {
        this.ixTabFlags = ixTabFlags;
    }

    public short getIxTabFlags() {
        return this.ixTabFlags;
    }
}
