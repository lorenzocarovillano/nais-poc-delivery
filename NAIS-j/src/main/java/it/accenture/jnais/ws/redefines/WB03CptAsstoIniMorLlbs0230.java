package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-CPT-ASSTO-INI-MOR<br>
 * Variable: W-B03-CPT-ASSTO-INI-MOR from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CptAsstoIniMorLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CptAsstoIniMorLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_CPT_ASSTO_INI_MOR;
    }

    public void setwB03CptAsstoIniMor(AfDecimal wB03CptAsstoIniMor) {
        writeDecimalAsPacked(Pos.W_B03_CPT_ASSTO_INI_MOR, wB03CptAsstoIniMor.copy());
    }

    /**Original name: W-B03-CPT-ASSTO-INI-MOR<br>*/
    public AfDecimal getwB03CptAsstoIniMor() {
        return readPackedAsDecimal(Pos.W_B03_CPT_ASSTO_INI_MOR, Len.Int.W_B03_CPT_ASSTO_INI_MOR, Len.Fract.W_B03_CPT_ASSTO_INI_MOR);
    }

    public byte[] getwB03CptAsstoIniMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_CPT_ASSTO_INI_MOR, Pos.W_B03_CPT_ASSTO_INI_MOR);
        return buffer;
    }

    public void setwB03CptAsstoIniMorNull(String wB03CptAsstoIniMorNull) {
        writeString(Pos.W_B03_CPT_ASSTO_INI_MOR_NULL, wB03CptAsstoIniMorNull, Len.W_B03_CPT_ASSTO_INI_MOR_NULL);
    }

    /**Original name: W-B03-CPT-ASSTO-INI-MOR-NULL<br>*/
    public String getwB03CptAsstoIniMorNull() {
        return readString(Pos.W_B03_CPT_ASSTO_INI_MOR_NULL, Len.W_B03_CPT_ASSTO_INI_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_CPT_ASSTO_INI_MOR = 1;
        public static final int W_B03_CPT_ASSTO_INI_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_CPT_ASSTO_INI_MOR = 8;
        public static final int W_B03_CPT_ASSTO_INI_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_CPT_ASSTO_INI_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_CPT_ASSTO_INI_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
