package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-IMPST-PRVR-IPT<br>
 * Variable: BEL-IMPST-PRVR-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelImpstPrvrIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelImpstPrvrIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_IMPST_PRVR_IPT;
    }

    public void setBelImpstPrvrIpt(AfDecimal belImpstPrvrIpt) {
        writeDecimalAsPacked(Pos.BEL_IMPST_PRVR_IPT, belImpstPrvrIpt.copy());
    }

    public void setBelImpstPrvrIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_IMPST_PRVR_IPT, Pos.BEL_IMPST_PRVR_IPT);
    }

    /**Original name: BEL-IMPST-PRVR-IPT<br>*/
    public AfDecimal getBelImpstPrvrIpt() {
        return readPackedAsDecimal(Pos.BEL_IMPST_PRVR_IPT, Len.Int.BEL_IMPST_PRVR_IPT, Len.Fract.BEL_IMPST_PRVR_IPT);
    }

    public byte[] getBelImpstPrvrIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_IMPST_PRVR_IPT, Pos.BEL_IMPST_PRVR_IPT);
        return buffer;
    }

    public void setBelImpstPrvrIptNull(String belImpstPrvrIptNull) {
        writeString(Pos.BEL_IMPST_PRVR_IPT_NULL, belImpstPrvrIptNull, Len.BEL_IMPST_PRVR_IPT_NULL);
    }

    /**Original name: BEL-IMPST-PRVR-IPT-NULL<br>*/
    public String getBelImpstPrvrIptNull() {
        return readString(Pos.BEL_IMPST_PRVR_IPT_NULL, Len.BEL_IMPST_PRVR_IPT_NULL);
    }

    public String getBelImpstPrvrIptNullFormatted() {
        return Functions.padBlanks(getBelImpstPrvrIptNull(), Len.BEL_IMPST_PRVR_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_PRVR_IPT = 1;
        public static final int BEL_IMPST_PRVR_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_PRVR_IPT = 8;
        public static final int BEL_IMPST_PRVR_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_PRVR_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_PRVR_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
