package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-ULT-RIVAL<br>
 * Variable: B03-DT-ULT-RIVAL from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtUltRival extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtUltRival() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_ULT_RIVAL;
    }

    public void setB03DtUltRival(int b03DtUltRival) {
        writeIntAsPacked(Pos.B03_DT_ULT_RIVAL, b03DtUltRival, Len.Int.B03_DT_ULT_RIVAL);
    }

    public void setB03DtUltRivalFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_ULT_RIVAL, Pos.B03_DT_ULT_RIVAL);
    }

    /**Original name: B03-DT-ULT-RIVAL<br>*/
    public int getB03DtUltRival() {
        return readPackedAsInt(Pos.B03_DT_ULT_RIVAL, Len.Int.B03_DT_ULT_RIVAL);
    }

    public byte[] getB03DtUltRivalAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_ULT_RIVAL, Pos.B03_DT_ULT_RIVAL);
        return buffer;
    }

    public void setB03DtUltRivalNull(String b03DtUltRivalNull) {
        writeString(Pos.B03_DT_ULT_RIVAL_NULL, b03DtUltRivalNull, Len.B03_DT_ULT_RIVAL_NULL);
    }

    /**Original name: B03-DT-ULT-RIVAL-NULL<br>*/
    public String getB03DtUltRivalNull() {
        return readString(Pos.B03_DT_ULT_RIVAL_NULL, Len.B03_DT_ULT_RIVAL_NULL);
    }

    public String getB03DtUltRivalNullFormatted() {
        return Functions.padBlanks(getB03DtUltRivalNull(), Len.B03_DT_ULT_RIVAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_ULT_RIVAL = 1;
        public static final int B03_DT_ULT_RIVAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_ULT_RIVAL = 5;
        public static final int B03_DT_ULT_RIVAL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_ULT_RIVAL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
