package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-MATU-RES-K1<br>
 * Variable: WDFA-MATU-RES-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaMatuResK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaMatuResK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_MATU_RES_K1;
    }

    public void setWdfaMatuResK1(AfDecimal wdfaMatuResK1) {
        writeDecimalAsPacked(Pos.WDFA_MATU_RES_K1, wdfaMatuResK1.copy());
    }

    public void setWdfaMatuResK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_MATU_RES_K1, Pos.WDFA_MATU_RES_K1);
    }

    /**Original name: WDFA-MATU-RES-K1<br>*/
    public AfDecimal getWdfaMatuResK1() {
        return readPackedAsDecimal(Pos.WDFA_MATU_RES_K1, Len.Int.WDFA_MATU_RES_K1, Len.Fract.WDFA_MATU_RES_K1);
    }

    public byte[] getWdfaMatuResK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_MATU_RES_K1, Pos.WDFA_MATU_RES_K1);
        return buffer;
    }

    public void setWdfaMatuResK1Null(String wdfaMatuResK1Null) {
        writeString(Pos.WDFA_MATU_RES_K1_NULL, wdfaMatuResK1Null, Len.WDFA_MATU_RES_K1_NULL);
    }

    /**Original name: WDFA-MATU-RES-K1-NULL<br>*/
    public String getWdfaMatuResK1Null() {
        return readString(Pos.WDFA_MATU_RES_K1_NULL, Len.WDFA_MATU_RES_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_MATU_RES_K1 = 1;
        public static final int WDFA_MATU_RES_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_MATU_RES_K1 = 8;
        public static final int WDFA_MATU_RES_K1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_MATU_RES_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_MATU_RES_K1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
