package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003FlagCodaTs;
import it.accenture.jnais.ws.enums.Idsv0003FormatoDataDb;
import it.accenture.jnais.ws.enums.Idsv0003IdentitaChiamante;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.enums.Idsv0003ModalitaEsecutiva;
import it.accenture.jnais.ws.enums.Idsv0003Operazione;
import it.accenture.jnais.ws.enums.Idsv0003ReturnCode;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.enums.Idsv0003TipologiaOperazione;
import it.accenture.jnais.ws.enums.Idsv0003TrattamentoStoricita;

/**Original name: IDSV0003<br>
 * Variable: IDSV0003 from copybook IDSV0003<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Idsv0003 extends SerializableParameter {

    //==== PROPERTIES ====
    /**Original name: IDSV0003-MODALITA-ESECUTIVA<br>
	 * <pre> --   CAMPI SETTAGGI MODULI I-O
	 *       COPY IDSV0007 REPLACING ==IDSV0003== BY ==IDSV0003==.
	 *     inizio COPY IDSV0007.
	 *  --   CAMPI SETTAGGI MODULI I-O</pre>*/
    private Idsv0003ModalitaEsecutiva modalitaEsecutiva = new Idsv0003ModalitaEsecutiva();
    //Original name: IDSV0003-CODICE-COMPAGNIA-ANIA
    private int codiceCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: IDSV0003-COD-MAIN-BATCH
    private String codMainBatch = DefaultValues.stringVal(Len.COD_MAIN_BATCH);
    //Original name: IDSV0003-TIPO-MOVIMENTO
    private String tipoMovimento = DefaultValues.stringVal(Len.TIPO_MOVIMENTO);
    //Original name: IDSV0003-SESSIONE
    private String sessione = DefaultValues.stringVal(Len.SESSIONE);
    //Original name: IDSV0003-USER-NAME
    private String userName = DefaultValues.stringVal(Len.USER_NAME);
    //Original name: IDSV0003-DATA-INIZIO-EFFETTO
    private int dataInizioEffetto = DefaultValues.INT_VAL;
    //Original name: IDSV0003-DATA-FINE-EFFETTO
    private int dataFineEffetto = DefaultValues.INT_VAL;
    //Original name: IDSV0003-DATA-COMPETENZA
    private long dataCompetenza = DefaultValues.LONG_VAL;
    //Original name: IDSV0003-DATA-COMP-AGG-STOR
    private long dataCompAggStor = DefaultValues.LONG_VAL;
    //Original name: IDSV0003-TRATTAMENTO-STORICITA
    private Idsv0003TrattamentoStoricita trattamentoStoricita = new Idsv0003TrattamentoStoricita();
    //Original name: IDSV0003-FORMATO-DATA-DB
    private Idsv0003FormatoDataDb formatoDataDb = new Idsv0003FormatoDataDb();
    //Original name: IDSV0003-ID-MOVI-ANNULLATO
    private String idMoviAnnullato = DefaultValues.stringVal(Len.ID_MOVI_ANNULLATO);
    //Original name: IDSV0003-IDENTITA-CHIAMANTE
    private Idsv0003IdentitaChiamante identitaChiamante = new Idsv0003IdentitaChiamante();
    //Original name: IDSV0003-TIPOLOGIA-OPERAZIONE
    private Idsv0003TipologiaOperazione tipologiaOperazione = new Idsv0003TipologiaOperazione();
    //Original name: IDSV0003-LIVELLO-OPERAZIONE
    private Idsv0003LivelloOperazione livelloOperazione = new Idsv0003LivelloOperazione();
    /**Original name: IDSV0003-OPERAZIONE<br>
	 * <pre> -- CAMPI OPERAZIONE
	 *     COPY IDSV0008.
	 *     inizio COPY IDSV0008.</pre>*/
    private Idsv0003Operazione operazione = new Idsv0003Operazione();
    /**Original name: IDSV0003-FLAG-CODA-TS<br>
	 * <pre>    fine COPY IDSV0008.</pre>*/
    private Idsv0003FlagCodaTs flagCodaTs = new Idsv0003FlagCodaTs();
    /**Original name: IDSV0003-BUFFER-WHERE-COND<br>
	 * <pre>    fine COPY IDSV0007</pre>*/
    private String bufferWhereCond = DefaultValues.stringVal(Len.BUFFER_WHERE_COND);
    /**Original name: IDSV0003-RETURN-CODE<br>
	 * <pre> --   CAMPI ERRORI
	 *        COPY IDSV0006 REPLACING ==IDSV0003== BY ==IDSV0003==.
	 *      inizio COPY IDSV0006.
	 *  -- RETURN-CODE
	 *      COPY IDSV0004.
	 *      inizio COPY IDSV0004.</pre>*/
    private Idsv0003ReturnCode returnCode = new Idsv0003ReturnCode();
    /**Original name: IDSV0003-SQLCODE<br>
	 * <pre>   fine COPY IDSV0004</pre>*/
    private Idsv0003Sqlcode sqlcode = new Idsv0003Sqlcode();
    //Original name: IDSV0003-CAMPI-ESITO
    private Idsv0003CampiEsito campiEsito = new Idsv0003CampiEsito();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IDSV0003;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIdsv0003Bytes(buf);
    }

    public String getIdsv0003Formatted() {
        return MarshalByteExt.bufferToStr(getIdsv0003Bytes());
    }

    public void setIdsv0003Bytes(byte[] buffer) {
        setIdsv0003Bytes(buffer, 1);
    }

    public byte[] getIdsv0003Bytes() {
        byte[] buffer = new byte[Len.IDSV0003];
        return getIdsv0003Bytes(buffer, 1);
    }

    public void setIdsv0003Bytes(byte[] buffer, int offset) {
        int position = offset;
        modalitaEsecutiva.setModalitaEsecutiva(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        codiceCompagniaAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CODICE_COMPAGNIA_ANIA, 0);
        position += Len.CODICE_COMPAGNIA_ANIA;
        codMainBatch = MarshalByte.readString(buffer, position, Len.COD_MAIN_BATCH);
        position += Len.COD_MAIN_BATCH;
        tipoMovimento = MarshalByte.readFixedString(buffer, position, Len.TIPO_MOVIMENTO);
        position += Len.TIPO_MOVIMENTO;
        sessione = MarshalByte.readString(buffer, position, Len.SESSIONE);
        position += Len.SESSIONE;
        userName = MarshalByte.readString(buffer, position, Len.USER_NAME);
        position += Len.USER_NAME;
        dataInizioEffetto = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DATA_INIZIO_EFFETTO, 0);
        position += Len.DATA_INIZIO_EFFETTO;
        dataFineEffetto = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DATA_FINE_EFFETTO, 0);
        position += Len.DATA_FINE_EFFETTO;
        dataCompetenza = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DATA_COMPETENZA, 0);
        position += Len.DATA_COMPETENZA;
        dataCompAggStor = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DATA_COMP_AGG_STOR, 0);
        position += Len.DATA_COMP_AGG_STOR;
        trattamentoStoricita.setTrattamentoStoricita(MarshalByte.readString(buffer, position, Idsv0003TrattamentoStoricita.Len.TRATTAMENTO_STORICITA));
        position += Idsv0003TrattamentoStoricita.Len.TRATTAMENTO_STORICITA;
        formatoDataDb.setFormatoDataDb(MarshalByte.readString(buffer, position, Idsv0003FormatoDataDb.Len.FORMATO_DATA_DB));
        position += Idsv0003FormatoDataDb.Len.FORMATO_DATA_DB;
        idMoviAnnullato = MarshalByte.readFixedString(buffer, position, Len.ID_MOVI_ANNULLATO);
        position += Len.ID_MOVI_ANNULLATO;
        identitaChiamante.setIdentitaChiamante(MarshalByte.readString(buffer, position, Idsv0003IdentitaChiamante.Len.IDENTITA_CHIAMANTE));
        position += Idsv0003IdentitaChiamante.Len.IDENTITA_CHIAMANTE;
        tipologiaOperazione.setTipologiaOperazione(MarshalByte.readString(buffer, position, Idsv0003TipologiaOperazione.Len.TIPOLOGIA_OPERAZIONE));
        position += Idsv0003TipologiaOperazione.Len.TIPOLOGIA_OPERAZIONE;
        livelloOperazione.setLivelloOperazione(MarshalByte.readString(buffer, position, Idsv0003LivelloOperazione.Len.LIVELLO_OPERAZIONE));
        position += Idsv0003LivelloOperazione.Len.LIVELLO_OPERAZIONE;
        operazione.setOperazione(MarshalByte.readString(buffer, position, Idsv0003Operazione.Len.OPERAZIONE));
        position += Idsv0003Operazione.Len.OPERAZIONE;
        flagCodaTs.setFlagCodaTs(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        bufferWhereCond = MarshalByte.readString(buffer, position, Len.BUFFER_WHERE_COND);
        position += Len.BUFFER_WHERE_COND;
        returnCode.setReturnCode(MarshalByte.readString(buffer, position, Idsv0003ReturnCode.Len.RETURN_CODE));
        position += Idsv0003ReturnCode.Len.RETURN_CODE;
        sqlcode.setSqlcode(MarshalByte.readInt(buffer, position, Idsv0003Sqlcode.Len.SQLCODE));
        position += Idsv0003Sqlcode.Len.SQLCODE;
        campiEsito.setCampiEsitoBytes(buffer, position);
    }

    public byte[] getIdsv0003Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, modalitaEsecutiva.getModalitaEsecutiva());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, codiceCompagniaAnia, Len.Int.CODICE_COMPAGNIA_ANIA, 0);
        position += Len.CODICE_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, codMainBatch, Len.COD_MAIN_BATCH);
        position += Len.COD_MAIN_BATCH;
        MarshalByte.writeString(buffer, position, tipoMovimento, Len.TIPO_MOVIMENTO);
        position += Len.TIPO_MOVIMENTO;
        MarshalByte.writeString(buffer, position, sessione, Len.SESSIONE);
        position += Len.SESSIONE;
        MarshalByte.writeString(buffer, position, userName, Len.USER_NAME);
        position += Len.USER_NAME;
        MarshalByte.writeIntAsPacked(buffer, position, dataInizioEffetto, Len.Int.DATA_INIZIO_EFFETTO, 0);
        position += Len.DATA_INIZIO_EFFETTO;
        MarshalByte.writeIntAsPacked(buffer, position, dataFineEffetto, Len.Int.DATA_FINE_EFFETTO, 0);
        position += Len.DATA_FINE_EFFETTO;
        MarshalByte.writeLongAsPacked(buffer, position, dataCompetenza, Len.Int.DATA_COMPETENZA, 0);
        position += Len.DATA_COMPETENZA;
        MarshalByte.writeLongAsPacked(buffer, position, dataCompAggStor, Len.Int.DATA_COMP_AGG_STOR, 0);
        position += Len.DATA_COMP_AGG_STOR;
        MarshalByte.writeString(buffer, position, trattamentoStoricita.getTrattamentoStoricita(), Idsv0003TrattamentoStoricita.Len.TRATTAMENTO_STORICITA);
        position += Idsv0003TrattamentoStoricita.Len.TRATTAMENTO_STORICITA;
        MarshalByte.writeString(buffer, position, formatoDataDb.getFormatoDataDb(), Idsv0003FormatoDataDb.Len.FORMATO_DATA_DB);
        position += Idsv0003FormatoDataDb.Len.FORMATO_DATA_DB;
        MarshalByte.writeString(buffer, position, idMoviAnnullato, Len.ID_MOVI_ANNULLATO);
        position += Len.ID_MOVI_ANNULLATO;
        MarshalByte.writeString(buffer, position, identitaChiamante.getIdentitaChiamante(), Idsv0003IdentitaChiamante.Len.IDENTITA_CHIAMANTE);
        position += Idsv0003IdentitaChiamante.Len.IDENTITA_CHIAMANTE;
        MarshalByte.writeString(buffer, position, tipologiaOperazione.getTipologiaOperazione(), Idsv0003TipologiaOperazione.Len.TIPOLOGIA_OPERAZIONE);
        position += Idsv0003TipologiaOperazione.Len.TIPOLOGIA_OPERAZIONE;
        MarshalByte.writeString(buffer, position, livelloOperazione.getLivelloOperazione(), Idsv0003LivelloOperazione.Len.LIVELLO_OPERAZIONE);
        position += Idsv0003LivelloOperazione.Len.LIVELLO_OPERAZIONE;
        MarshalByte.writeString(buffer, position, operazione.getOperazione(), Idsv0003Operazione.Len.OPERAZIONE);
        position += Idsv0003Operazione.Len.OPERAZIONE;
        MarshalByte.writeChar(buffer, position, flagCodaTs.getFlagCodaTs());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, bufferWhereCond, Len.BUFFER_WHERE_COND);
        position += Len.BUFFER_WHERE_COND;
        MarshalByte.writeString(buffer, position, returnCode.getReturnCode(), Idsv0003ReturnCode.Len.RETURN_CODE);
        position += Idsv0003ReturnCode.Len.RETURN_CODE;
        MarshalByte.writeInt(buffer, position, sqlcode.getSqlcode(), Idsv0003Sqlcode.Len.SQLCODE);
        position += Idsv0003Sqlcode.Len.SQLCODE;
        campiEsito.getCampiEsitoBytes(buffer, position);
        return buffer;
    }

    public void setCodiceCompagniaAnia(int codiceCompagniaAnia) {
        this.codiceCompagniaAnia = codiceCompagniaAnia;
    }

    public int getCodiceCompagniaAnia() {
        return this.codiceCompagniaAnia;
    }

    public void setCodMainBatch(String codMainBatch) {
        this.codMainBatch = Functions.subString(codMainBatch, Len.COD_MAIN_BATCH);
    }

    public String getCodMainBatch() {
        return this.codMainBatch;
    }

    public void setTipoMovimento(int tipoMovimento) {
        this.tipoMovimento = NumericDisplay.asString(tipoMovimento, Len.TIPO_MOVIMENTO);
    }

    public void setTipoMovimentoFormatted(String tipoMovimento) {
        this.tipoMovimento = Trunc.toUnsignedNumeric(tipoMovimento, Len.TIPO_MOVIMENTO);
    }

    public int getTipoMovimento() {
        return NumericDisplay.asInt(this.tipoMovimento);
    }

    public String getTipoMovimentoFormatted() {
        return this.tipoMovimento;
    }

    public void setSessione(String sessione) {
        this.sessione = Functions.subString(sessione, Len.SESSIONE);
    }

    public String getSessione() {
        return this.sessione;
    }

    public void setUserName(String userName) {
        this.userName = Functions.subString(userName, Len.USER_NAME);
    }

    public String getUserName() {
        return this.userName;
    }

    public void setDataInizioEffetto(int dataInizioEffetto) {
        this.dataInizioEffetto = dataInizioEffetto;
    }

    public int getDataInizioEffetto() {
        return this.dataInizioEffetto;
    }

    public String getDataInizioEffettoFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getDataInizioEffetto()).toString();
    }

    public void setDataFineEffetto(int dataFineEffetto) {
        this.dataFineEffetto = dataFineEffetto;
    }

    public int getDataFineEffetto() {
        return this.dataFineEffetto;
    }

    public void setDataCompetenza(long dataCompetenza) {
        this.dataCompetenza = dataCompetenza;
    }

    public long getDataCompetenza() {
        return this.dataCompetenza;
    }

    public void setDataCompAggStor(long dataCompAggStor) {
        this.dataCompAggStor = dataCompAggStor;
    }

    public long getDataCompAggStor() {
        return this.dataCompAggStor;
    }

    public void setIdMoviAnnullatoFormatted(String idMoviAnnullato) {
        this.idMoviAnnullato = Trunc.toUnsignedNumeric(idMoviAnnullato, Len.ID_MOVI_ANNULLATO);
    }

    public int getIdMoviAnnullato() {
        return NumericDisplay.asInt(this.idMoviAnnullato);
    }

    public void setBufferWhereCond(String bufferWhereCond) {
        this.bufferWhereCond = Functions.subString(bufferWhereCond, Len.BUFFER_WHERE_COND);
    }

    public String getBufferWhereCond() {
        return this.bufferWhereCond;
    }

    public String getBufferWhereCondFormatted() {
        return Functions.padBlanks(getBufferWhereCond(), Len.BUFFER_WHERE_COND);
    }

    public Idsv0003CampiEsito getCampiEsito() {
        return campiEsito;
    }

    public Idsv0003FlagCodaTs getFlagCodaTs() {
        return flagCodaTs;
    }

    public Idsv0003FormatoDataDb getFormatoDataDb() {
        return formatoDataDb;
    }

    public Idsv0003IdentitaChiamante getIdentitaChiamante() {
        return identitaChiamante;
    }

    public Idsv0003LivelloOperazione getLivelloOperazione() {
        return livelloOperazione;
    }

    public Idsv0003ModalitaEsecutiva getModalitaEsecutiva() {
        return modalitaEsecutiva;
    }

    public Idsv0003Operazione getOperazione() {
        return operazione;
    }

    public Idsv0003ReturnCode getReturnCode() {
        return returnCode;
    }

    public Idsv0003Sqlcode getSqlcode() {
        return sqlcode;
    }

    public Idsv0003TipologiaOperazione getTipologiaOperazione() {
        return tipologiaOperazione;
    }

    public Idsv0003TrattamentoStoricita getTrattamentoStoricita() {
        return trattamentoStoricita;
    }

    @Override
    public byte[] serialize() {
        return getIdsv0003Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_COMPAGNIA_ANIA = 3;
        public static final int COD_MAIN_BATCH = 8;
        public static final int TIPO_MOVIMENTO = 5;
        public static final int SESSIONE = 20;
        public static final int USER_NAME = 20;
        public static final int DATA_INIZIO_EFFETTO = 5;
        public static final int DATA_FINE_EFFETTO = 5;
        public static final int DATA_COMPETENZA = 10;
        public static final int DATA_COMP_AGG_STOR = 10;
        public static final int ID_MOVI_ANNULLATO = 9;
        public static final int BUFFER_WHERE_COND = 300;
        public static final int IDSV0003 = Idsv0003ModalitaEsecutiva.Len.MODALITA_ESECUTIVA + CODICE_COMPAGNIA_ANIA + COD_MAIN_BATCH + TIPO_MOVIMENTO + SESSIONE + USER_NAME + DATA_INIZIO_EFFETTO + DATA_FINE_EFFETTO + DATA_COMPETENZA + DATA_COMP_AGG_STOR + Idsv0003TrattamentoStoricita.Len.TRATTAMENTO_STORICITA + Idsv0003FormatoDataDb.Len.FORMATO_DATA_DB + ID_MOVI_ANNULLATO + Idsv0003IdentitaChiamante.Len.IDENTITA_CHIAMANTE + Idsv0003TipologiaOperazione.Len.TIPOLOGIA_OPERAZIONE + Idsv0003LivelloOperazione.Len.LIVELLO_OPERAZIONE + Idsv0003Operazione.Len.OPERAZIONE + Idsv0003FlagCodaTs.Len.FLAG_CODA_TS + BUFFER_WHERE_COND + Idsv0003ReturnCode.Len.RETURN_CODE + Idsv0003Sqlcode.Len.SQLCODE + Idsv0003CampiEsito.Len.CAMPI_ESITO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CODICE_COMPAGNIA_ANIA = 5;
            public static final int DATA_INIZIO_EFFETTO = 8;
            public static final int DATA_FINE_EFFETTO = 8;
            public static final int DATA_COMPETENZA = 18;
            public static final int DATA_COMP_AGG_STOR = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
