package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IS-662014L<br>
 * Variable: WDFL-IS-662014L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIs662014l extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIs662014l() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IS662014L;
    }

    public void setWdflIs662014l(AfDecimal wdflIs662014l) {
        writeDecimalAsPacked(Pos.WDFL_IS662014L, wdflIs662014l.copy());
    }

    public void setWdflIs662014lFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IS662014L, Pos.WDFL_IS662014L);
    }

    /**Original name: WDFL-IS-662014L<br>*/
    public AfDecimal getWdflIs662014l() {
        return readPackedAsDecimal(Pos.WDFL_IS662014L, Len.Int.WDFL_IS662014L, Len.Fract.WDFL_IS662014L);
    }

    public byte[] getWdflIs662014lAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IS662014L, Pos.WDFL_IS662014L);
        return buffer;
    }

    public void setWdflIs662014lNull(String wdflIs662014lNull) {
        writeString(Pos.WDFL_IS662014L_NULL, wdflIs662014lNull, Len.WDFL_IS662014L_NULL);
    }

    /**Original name: WDFL-IS-662014L-NULL<br>*/
    public String getWdflIs662014lNull() {
        return readString(Pos.WDFL_IS662014L_NULL, Len.WDFL_IS662014L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IS662014L = 1;
        public static final int WDFL_IS662014L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IS662014L = 8;
        public static final int WDFL_IS662014L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IS662014L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IS662014L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
