package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-IMP-CAR-CASO-MOR<br>
 * Variable: W-B03-IMP-CAR-CASO-MOR from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03ImpCarCasoMorLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03ImpCarCasoMorLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_IMP_CAR_CASO_MOR;
    }

    public void setwB03ImpCarCasoMor(AfDecimal wB03ImpCarCasoMor) {
        writeDecimalAsPacked(Pos.W_B03_IMP_CAR_CASO_MOR, wB03ImpCarCasoMor.copy());
    }

    /**Original name: W-B03-IMP-CAR-CASO-MOR<br>*/
    public AfDecimal getwB03ImpCarCasoMor() {
        return readPackedAsDecimal(Pos.W_B03_IMP_CAR_CASO_MOR, Len.Int.W_B03_IMP_CAR_CASO_MOR, Len.Fract.W_B03_IMP_CAR_CASO_MOR);
    }

    public byte[] getwB03ImpCarCasoMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_IMP_CAR_CASO_MOR, Pos.W_B03_IMP_CAR_CASO_MOR);
        return buffer;
    }

    public void setwB03ImpCarCasoMorNull(String wB03ImpCarCasoMorNull) {
        writeString(Pos.W_B03_IMP_CAR_CASO_MOR_NULL, wB03ImpCarCasoMorNull, Len.W_B03_IMP_CAR_CASO_MOR_NULL);
    }

    /**Original name: W-B03-IMP-CAR-CASO-MOR-NULL<br>*/
    public String getwB03ImpCarCasoMorNull() {
        return readString(Pos.W_B03_IMP_CAR_CASO_MOR_NULL, Len.W_B03_IMP_CAR_CASO_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_IMP_CAR_CASO_MOR = 1;
        public static final int W_B03_IMP_CAR_CASO_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_IMP_CAR_CASO_MOR = 8;
        public static final int W_B03_IMP_CAR_CASO_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_IMP_CAR_CASO_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_IMP_CAR_CASO_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
