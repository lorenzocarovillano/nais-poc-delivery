package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-RIS-SPE-T<br>
 * Variable: B03-RIS-SPE-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03RisSpeT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03RisSpeT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_RIS_SPE_T;
    }

    public void setB03RisSpeT(AfDecimal b03RisSpeT) {
        writeDecimalAsPacked(Pos.B03_RIS_SPE_T, b03RisSpeT.copy());
    }

    public void setB03RisSpeTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_RIS_SPE_T, Pos.B03_RIS_SPE_T);
    }

    /**Original name: B03-RIS-SPE-T<br>*/
    public AfDecimal getB03RisSpeT() {
        return readPackedAsDecimal(Pos.B03_RIS_SPE_T, Len.Int.B03_RIS_SPE_T, Len.Fract.B03_RIS_SPE_T);
    }

    public byte[] getB03RisSpeTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_RIS_SPE_T, Pos.B03_RIS_SPE_T);
        return buffer;
    }

    public void setB03RisSpeTNull(String b03RisSpeTNull) {
        writeString(Pos.B03_RIS_SPE_T_NULL, b03RisSpeTNull, Len.B03_RIS_SPE_T_NULL);
    }

    /**Original name: B03-RIS-SPE-T-NULL<br>*/
    public String getB03RisSpeTNull() {
        return readString(Pos.B03_RIS_SPE_T_NULL, Len.B03_RIS_SPE_T_NULL);
    }

    public String getB03RisSpeTNullFormatted() {
        return Functions.padBlanks(getB03RisSpeTNull(), Len.B03_RIS_SPE_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_RIS_SPE_T = 1;
        public static final int B03_RIS_SPE_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_RIS_SPE_T = 8;
        public static final int B03_RIS_SPE_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_RIS_SPE_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_RIS_SPE_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
