package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-BNS-NON-GODUTO<br>
 * Variable: S089-BNS-NON-GODUTO from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089BnsNonGoduto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089BnsNonGoduto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_BNS_NON_GODUTO;
    }

    public void setWlquBnsNonGoduto(AfDecimal wlquBnsNonGoduto) {
        writeDecimalAsPacked(Pos.S089_BNS_NON_GODUTO, wlquBnsNonGoduto.copy());
    }

    public void setWlquBnsNonGodutoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_BNS_NON_GODUTO, Pos.S089_BNS_NON_GODUTO);
    }

    /**Original name: WLQU-BNS-NON-GODUTO<br>*/
    public AfDecimal getWlquBnsNonGoduto() {
        return readPackedAsDecimal(Pos.S089_BNS_NON_GODUTO, Len.Int.WLQU_BNS_NON_GODUTO, Len.Fract.WLQU_BNS_NON_GODUTO);
    }

    public byte[] getWlquBnsNonGodutoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_BNS_NON_GODUTO, Pos.S089_BNS_NON_GODUTO);
        return buffer;
    }

    public void initWlquBnsNonGodutoSpaces() {
        fill(Pos.S089_BNS_NON_GODUTO, Len.S089_BNS_NON_GODUTO, Types.SPACE_CHAR);
    }

    public void setWlquBnsNonGodutoNull(String wlquBnsNonGodutoNull) {
        writeString(Pos.S089_BNS_NON_GODUTO_NULL, wlquBnsNonGodutoNull, Len.WLQU_BNS_NON_GODUTO_NULL);
    }

    /**Original name: WLQU-BNS-NON-GODUTO-NULL<br>*/
    public String getWlquBnsNonGodutoNull() {
        return readString(Pos.S089_BNS_NON_GODUTO_NULL, Len.WLQU_BNS_NON_GODUTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_BNS_NON_GODUTO = 1;
        public static final int S089_BNS_NON_GODUTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_BNS_NON_GODUTO = 8;
        public static final int WLQU_BNS_NON_GODUTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_BNS_NON_GODUTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_BNS_NON_GODUTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
