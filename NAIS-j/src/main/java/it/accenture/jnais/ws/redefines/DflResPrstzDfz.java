package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RES-PRSTZ-DFZ<br>
 * Variable: DFL-RES-PRSTZ-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflResPrstzDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflResPrstzDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RES_PRSTZ_DFZ;
    }

    public void setDflResPrstzDfz(AfDecimal dflResPrstzDfz) {
        writeDecimalAsPacked(Pos.DFL_RES_PRSTZ_DFZ, dflResPrstzDfz.copy());
    }

    public void setDflResPrstzDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RES_PRSTZ_DFZ, Pos.DFL_RES_PRSTZ_DFZ);
    }

    /**Original name: DFL-RES-PRSTZ-DFZ<br>*/
    public AfDecimal getDflResPrstzDfz() {
        return readPackedAsDecimal(Pos.DFL_RES_PRSTZ_DFZ, Len.Int.DFL_RES_PRSTZ_DFZ, Len.Fract.DFL_RES_PRSTZ_DFZ);
    }

    public byte[] getDflResPrstzDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RES_PRSTZ_DFZ, Pos.DFL_RES_PRSTZ_DFZ);
        return buffer;
    }

    public void setDflResPrstzDfzNull(String dflResPrstzDfzNull) {
        writeString(Pos.DFL_RES_PRSTZ_DFZ_NULL, dflResPrstzDfzNull, Len.DFL_RES_PRSTZ_DFZ_NULL);
    }

    /**Original name: DFL-RES-PRSTZ-DFZ-NULL<br>*/
    public String getDflResPrstzDfzNull() {
        return readString(Pos.DFL_RES_PRSTZ_DFZ_NULL, Len.DFL_RES_PRSTZ_DFZ_NULL);
    }

    public String getDflResPrstzDfzNullFormatted() {
        return Functions.padBlanks(getDflResPrstzDfzNull(), Len.DFL_RES_PRSTZ_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRSTZ_DFZ = 1;
        public static final int DFL_RES_PRSTZ_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRSTZ_DFZ = 8;
        public static final int DFL_RES_PRSTZ_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRSTZ_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRSTZ_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
