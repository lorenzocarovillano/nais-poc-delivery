package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-IMP-BNS-DA-SCO<br>
 * Variable: PMO-IMP-BNS-DA-SCO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoImpBnsDaSco extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoImpBnsDaSco() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_IMP_BNS_DA_SCO;
    }

    public void setPmoImpBnsDaSco(AfDecimal pmoImpBnsDaSco) {
        writeDecimalAsPacked(Pos.PMO_IMP_BNS_DA_SCO, pmoImpBnsDaSco.copy());
    }

    public void setPmoImpBnsDaScoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_IMP_BNS_DA_SCO, Pos.PMO_IMP_BNS_DA_SCO);
    }

    /**Original name: PMO-IMP-BNS-DA-SCO<br>*/
    public AfDecimal getPmoImpBnsDaSco() {
        return readPackedAsDecimal(Pos.PMO_IMP_BNS_DA_SCO, Len.Int.PMO_IMP_BNS_DA_SCO, Len.Fract.PMO_IMP_BNS_DA_SCO);
    }

    public byte[] getPmoImpBnsDaScoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_IMP_BNS_DA_SCO, Pos.PMO_IMP_BNS_DA_SCO);
        return buffer;
    }

    public void initPmoImpBnsDaScoHighValues() {
        fill(Pos.PMO_IMP_BNS_DA_SCO, Len.PMO_IMP_BNS_DA_SCO, Types.HIGH_CHAR_VAL);
    }

    public void setPmoImpBnsDaScoNull(String pmoImpBnsDaScoNull) {
        writeString(Pos.PMO_IMP_BNS_DA_SCO_NULL, pmoImpBnsDaScoNull, Len.PMO_IMP_BNS_DA_SCO_NULL);
    }

    /**Original name: PMO-IMP-BNS-DA-SCO-NULL<br>*/
    public String getPmoImpBnsDaScoNull() {
        return readString(Pos.PMO_IMP_BNS_DA_SCO_NULL, Len.PMO_IMP_BNS_DA_SCO_NULL);
    }

    public String getPmoImpBnsDaScoNullFormatted() {
        return Functions.padBlanks(getPmoImpBnsDaScoNull(), Len.PMO_IMP_BNS_DA_SCO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_IMP_BNS_DA_SCO = 1;
        public static final int PMO_IMP_BNS_DA_SCO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_IMP_BNS_DA_SCO = 8;
        public static final int PMO_IMP_BNS_DA_SCO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_IMP_BNS_DA_SCO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_IMP_BNS_DA_SCO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
