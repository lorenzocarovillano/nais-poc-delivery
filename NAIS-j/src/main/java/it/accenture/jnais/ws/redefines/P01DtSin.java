package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P01-DT-SIN<br>
 * Variable: P01-DT-SIN from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P01DtSin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P01DtSin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P01_DT_SIN;
    }

    public void setP01DtSin(int p01DtSin) {
        writeIntAsPacked(Pos.P01_DT_SIN, p01DtSin, Len.Int.P01_DT_SIN);
    }

    public void setP01DtSinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P01_DT_SIN, Pos.P01_DT_SIN);
    }

    /**Original name: P01-DT-SIN<br>*/
    public int getP01DtSin() {
        return readPackedAsInt(Pos.P01_DT_SIN, Len.Int.P01_DT_SIN);
    }

    public byte[] getP01DtSinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P01_DT_SIN, Pos.P01_DT_SIN);
        return buffer;
    }

    public void setP01DtSinNull(String p01DtSinNull) {
        writeString(Pos.P01_DT_SIN_NULL, p01DtSinNull, Len.P01_DT_SIN_NULL);
    }

    /**Original name: P01-DT-SIN-NULL<br>*/
    public String getP01DtSinNull() {
        return readString(Pos.P01_DT_SIN_NULL, Len.P01_DT_SIN_NULL);
    }

    public String getP01DtSinNullFormatted() {
        return Functions.padBlanks(getP01DtSinNull(), Len.P01_DT_SIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P01_DT_SIN = 1;
        public static final int P01_DT_SIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P01_DT_SIN = 5;
        public static final int P01_DT_SIN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P01_DT_SIN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
