package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-RIS-MAT-CHIU-PREC<br>
 * Variable: B03-RIS-MAT-CHIU-PREC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03RisMatChiuPrec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03RisMatChiuPrec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_RIS_MAT_CHIU_PREC;
    }

    public void setB03RisMatChiuPrec(AfDecimal b03RisMatChiuPrec) {
        writeDecimalAsPacked(Pos.B03_RIS_MAT_CHIU_PREC, b03RisMatChiuPrec.copy());
    }

    public void setB03RisMatChiuPrecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_RIS_MAT_CHIU_PREC, Pos.B03_RIS_MAT_CHIU_PREC);
    }

    /**Original name: B03-RIS-MAT-CHIU-PREC<br>*/
    public AfDecimal getB03RisMatChiuPrec() {
        return readPackedAsDecimal(Pos.B03_RIS_MAT_CHIU_PREC, Len.Int.B03_RIS_MAT_CHIU_PREC, Len.Fract.B03_RIS_MAT_CHIU_PREC);
    }

    public byte[] getB03RisMatChiuPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_RIS_MAT_CHIU_PREC, Pos.B03_RIS_MAT_CHIU_PREC);
        return buffer;
    }

    public void setB03RisMatChiuPrecNull(String b03RisMatChiuPrecNull) {
        writeString(Pos.B03_RIS_MAT_CHIU_PREC_NULL, b03RisMatChiuPrecNull, Len.B03_RIS_MAT_CHIU_PREC_NULL);
    }

    /**Original name: B03-RIS-MAT-CHIU-PREC-NULL<br>*/
    public String getB03RisMatChiuPrecNull() {
        return readString(Pos.B03_RIS_MAT_CHIU_PREC_NULL, Len.B03_RIS_MAT_CHIU_PREC_NULL);
    }

    public String getB03RisMatChiuPrecNullFormatted() {
        return Functions.padBlanks(getB03RisMatChiuPrecNull(), Len.B03_RIS_MAT_CHIU_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_RIS_MAT_CHIU_PREC = 1;
        public static final int B03_RIS_MAT_CHIU_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_RIS_MAT_CHIU_PREC = 8;
        public static final int B03_RIS_MAT_CHIU_PREC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_RIS_MAT_CHIU_PREC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_RIS_MAT_CHIU_PREC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
