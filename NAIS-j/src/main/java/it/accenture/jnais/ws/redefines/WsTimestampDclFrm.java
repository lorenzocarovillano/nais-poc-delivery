package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WS-TIMESTAMP-DCL-FRM<br>
 * Variable: WS-TIMESTAMP-DCL-FRM from program IEAS9700<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsTimestampDclFrm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsTimestampDclFrm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_TIMESTAMP_DCL_FRM;
    }

    public void setWsTimestampDclFrm(String wsTimestampDclFrm) {
        writeString(Pos.WS_TIMESTAMP_DCL_FRM, wsTimestampDclFrm, Len.WS_TIMESTAMP_DCL_FRM);
    }

    /**Original name: WS-TIMESTAMP-DCL-FRM<br>
	 * <pre>---appoggio per formattazione timestamp------------------------*</pre>*/
    public String getWsTimestampDclFrm() {
        return readString(Pos.WS_TIMESTAMP_DCL_FRM, Len.WS_TIMESTAMP_DCL_FRM);
    }

    public void setWnTimestampDclFrm(long wnTimestampDclFrm) {
        writeLong(Pos.WN_TIMESTAMP_DCL_FRM, wnTimestampDclFrm, Len.Int.WN_TIMESTAMP_DCL_FRM, SignType.NO_SIGN);
    }

    /**Original name: WN-TIMESTAMP-DCL-FRM<br>*/
    public long getWnTimestampDclFrm() {
        return readNumDispUnsignedLong(Pos.WN_TIMESTAMP_DCL_FRM, Len.WN_TIMESTAMP_DCL_FRM);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_TIMESTAMP_DCL_FRM = 1;
        public static final int WN_TIMESTAMP_DCL_FRM = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TIMESTAMP_DCL_FRM = 18;
        public static final int WN_TIMESTAMP_DCL_FRM = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WN_TIMESTAMP_DCL_FRM = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
