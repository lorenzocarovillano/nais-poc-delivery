package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.Idsv0141ReturnCode;
import it.accenture.jnais.ws.enums.Idsv0141TipoDatoMitt;

/**Original name: IDSS0140-LINK<br>
 * Variable: IDSS0140-LINK from program IDSS0140<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Idss0140Link extends SerializableParameter {

    //==== PROPERTIES ====
    /**Original name: IDSV0141-TIPO-DATO-MITT<br>
	 * <pre> MODULI DI CONVERSIONE GENERICI</pre>*/
    private Idsv0141TipoDatoMitt tipoDatoMitt = new Idsv0141TipoDatoMitt();
    //Original name: IDSV0141-LUNGHEZZA-DATO-STR
    private int lunghezzaDatoStr = DefaultValues.INT_VAL;
    //Original name: IDSV0141-LUNGHEZZA-DATO-MITT
    private int lunghezzaDatoMitt = DefaultValues.INT_VAL;
    //Original name: IDSV0141-PRECISIONE-DATO-MITT
    private short precisioneDatoMitt = DefaultValues.SHORT_VAL;
    //Original name: IDSV0141-CAMPO-MITT
    private String campoMitt = DefaultValues.stringVal(Len.CAMPO_MITT);
    //Original name: IDSV0141-CAMPO-DEST
    private AfDecimal campoDest = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);
    /**Original name: IDSV0141-RETURN-CODE<br>
	 * <pre>-- return code</pre>*/
    private Idsv0141ReturnCode returnCode = new Idsv0141ReturnCode();
    //Original name: IDSV0141-DESCRIZ-ERR-DB2
    private String descrizErrDb2 = DefaultValues.stringVal(Len.DESCRIZ_ERR_DB2);
    //Original name: IDSV0141-COD-SERVIZIO-BE
    private String codServizioBe = DefaultValues.stringVal(Len.COD_SERVIZIO_BE);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IDSS0140_LINK;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIdss0140LinkBytes(buf);
    }

    public String getAreaIdsv0141Formatted() {
        return MarshalByteExt.bufferToStr(getIdss0140LinkBytes());
    }

    public void setIdss0140LinkBytes(byte[] buffer) {
        setIdss0140LinkBytes(buffer, 1);
    }

    public byte[] getIdss0140LinkBytes() {
        byte[] buffer = new byte[Len.IDSS0140_LINK];
        return getIdss0140LinkBytes(buffer, 1);
    }

    public void setIdss0140LinkBytes(byte[] buffer, int offset) {
        int position = offset;
        tipoDatoMitt.setTipoDatoMitt(MarshalByte.readString(buffer, position, Idsv0141TipoDatoMitt.Len.TIPO_DATO_MITT));
        position += Idsv0141TipoDatoMitt.Len.TIPO_DATO_MITT;
        lunghezzaDatoStr = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LUNGHEZZA_DATO_STR, 0);
        position += Len.LUNGHEZZA_DATO_STR;
        lunghezzaDatoMitt = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LUNGHEZZA_DATO_MITT, 0);
        position += Len.LUNGHEZZA_DATO_MITT;
        precisioneDatoMitt = MarshalByte.readPackedAsShort(buffer, position, Len.Int.PRECISIONE_DATO_MITT, 0);
        position += Len.PRECISIONE_DATO_MITT;
        campoMitt = MarshalByte.readString(buffer, position, Len.CAMPO_MITT);
        position += Len.CAMPO_MITT;
        campoDest.assign(MarshalByte.readDecimal(buffer, position, Len.Int.CAMPO_DEST, Len.Fract.CAMPO_DEST));
        position += Len.CAMPO_DEST;
        returnCode.setReturnCode(MarshalByte.readString(buffer, position, Idsv0141ReturnCode.Len.RETURN_CODE));
        position += Idsv0141ReturnCode.Len.RETURN_CODE;
        setCampiEsitoBytes(buffer, position);
    }

    public byte[] getIdss0140LinkBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tipoDatoMitt.getTipoDatoMitt(), Idsv0141TipoDatoMitt.Len.TIPO_DATO_MITT);
        position += Idsv0141TipoDatoMitt.Len.TIPO_DATO_MITT;
        MarshalByte.writeIntAsPacked(buffer, position, lunghezzaDatoStr, Len.Int.LUNGHEZZA_DATO_STR, 0);
        position += Len.LUNGHEZZA_DATO_STR;
        MarshalByte.writeIntAsPacked(buffer, position, lunghezzaDatoMitt, Len.Int.LUNGHEZZA_DATO_MITT, 0);
        position += Len.LUNGHEZZA_DATO_MITT;
        MarshalByte.writeShortAsPacked(buffer, position, precisioneDatoMitt, Len.Int.PRECISIONE_DATO_MITT, 0);
        position += Len.PRECISIONE_DATO_MITT;
        MarshalByte.writeString(buffer, position, campoMitt, Len.CAMPO_MITT);
        position += Len.CAMPO_MITT;
        MarshalByte.writeDecimal(buffer, position, campoDest.copy());
        position += Len.CAMPO_DEST;
        MarshalByte.writeString(buffer, position, returnCode.getReturnCode(), Idsv0141ReturnCode.Len.RETURN_CODE);
        position += Idsv0141ReturnCode.Len.RETURN_CODE;
        getCampiEsitoBytes(buffer, position);
        return buffer;
    }

    public void setLunghezzaDatoStr(int lunghezzaDatoStr) {
        this.lunghezzaDatoStr = lunghezzaDatoStr;
    }

    public int getLunghezzaDatoStr() {
        return this.lunghezzaDatoStr;
    }

    public void setLunghezzaDatoMitt(int lunghezzaDatoMitt) {
        this.lunghezzaDatoMitt = lunghezzaDatoMitt;
    }

    public int getLunghezzaDatoMitt() {
        return this.lunghezzaDatoMitt;
    }

    public void setPrecisioneDatoMitt(short precisioneDatoMitt) {
        this.precisioneDatoMitt = precisioneDatoMitt;
    }

    public short getPrecisioneDatoMitt() {
        return this.precisioneDatoMitt;
    }

    public void setCampoMitt(String campoMitt) {
        this.campoMitt = Functions.subString(campoMitt, Len.CAMPO_MITT);
    }

    public String getCampoMitt() {
        return this.campoMitt;
    }

    public String getCampoMittFormatted() {
        return Functions.padBlanks(getCampoMitt(), Len.CAMPO_MITT);
    }

    public void setCampoDest(AfDecimal campoDest) {
        this.campoDest.assign(campoDest);
    }

    public AfDecimal getCampoDest() {
        return this.campoDest.copy();
    }

    public void setCampiEsitoBytes(byte[] buffer, int offset) {
        int position = offset;
        descrizErrDb2 = MarshalByte.readString(buffer, position, Len.DESCRIZ_ERR_DB2);
        position += Len.DESCRIZ_ERR_DB2;
        codServizioBe = MarshalByte.readString(buffer, position, Len.COD_SERVIZIO_BE);
    }

    public byte[] getCampiEsitoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, descrizErrDb2, Len.DESCRIZ_ERR_DB2);
        position += Len.DESCRIZ_ERR_DB2;
        MarshalByte.writeString(buffer, position, codServizioBe, Len.COD_SERVIZIO_BE);
        return buffer;
    }

    public void setDescrizErrDb2(String descrizErrDb2) {
        this.descrizErrDb2 = Functions.subString(descrizErrDb2, Len.DESCRIZ_ERR_DB2);
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setCodServizioBe(String codServizioBe) {
        this.codServizioBe = Functions.subString(codServizioBe, Len.COD_SERVIZIO_BE);
    }

    public String getCodServizioBe() {
        return this.codServizioBe;
    }

    public Idsv0141ReturnCode getReturnCode() {
        return returnCode;
    }

    public Idsv0141TipoDatoMitt getTipoDatoMitt() {
        return tipoDatoMitt;
    }

    @Override
    public byte[] serialize() {
        return getIdss0140LinkBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LUNGHEZZA_DATO_STR = 3;
        public static final int LUNGHEZZA_DATO_MITT = 3;
        public static final int PRECISIONE_DATO_MITT = 2;
        public static final int CAMPO_MITT = 40;
        public static final int CAMPO_DEST = 18;
        public static final int DESCRIZ_ERR_DB2 = 200;
        public static final int COD_SERVIZIO_BE = 8;
        public static final int CAMPI_ESITO = DESCRIZ_ERR_DB2 + COD_SERVIZIO_BE;
        public static final int IDSS0140_LINK = Idsv0141TipoDatoMitt.Len.TIPO_DATO_MITT + LUNGHEZZA_DATO_STR + LUNGHEZZA_DATO_MITT + PRECISIONE_DATO_MITT + CAMPO_MITT + CAMPO_DEST + Idsv0141ReturnCode.Len.RETURN_CODE + CAMPI_ESITO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LUNGHEZZA_DATO_STR = 5;
            public static final int LUNGHEZZA_DATO_MITT = 5;
            public static final int PRECISIONE_DATO_MITT = 2;
            public static final int CAMPO_DEST = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int CAMPO_DEST = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
