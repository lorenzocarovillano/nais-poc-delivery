package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMP-NET-TRASFERITO<br>
 * Variable: WDFA-IMP-NET-TRASFERITO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpNetTrasferito extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpNetTrasferito() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMP_NET_TRASFERITO;
    }

    public void setWdfaImpNetTrasferito(AfDecimal wdfaImpNetTrasferito) {
        writeDecimalAsPacked(Pos.WDFA_IMP_NET_TRASFERITO, wdfaImpNetTrasferito.copy());
    }

    public void setWdfaImpNetTrasferitoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMP_NET_TRASFERITO, Pos.WDFA_IMP_NET_TRASFERITO);
    }

    /**Original name: WDFA-IMP-NET-TRASFERITO<br>*/
    public AfDecimal getWdfaImpNetTrasferito() {
        return readPackedAsDecimal(Pos.WDFA_IMP_NET_TRASFERITO, Len.Int.WDFA_IMP_NET_TRASFERITO, Len.Fract.WDFA_IMP_NET_TRASFERITO);
    }

    public byte[] getWdfaImpNetTrasferitoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMP_NET_TRASFERITO, Pos.WDFA_IMP_NET_TRASFERITO);
        return buffer;
    }

    public void setWdfaImpNetTrasferitoNull(String wdfaImpNetTrasferitoNull) {
        writeString(Pos.WDFA_IMP_NET_TRASFERITO_NULL, wdfaImpNetTrasferitoNull, Len.WDFA_IMP_NET_TRASFERITO_NULL);
    }

    /**Original name: WDFA-IMP-NET-TRASFERITO-NULL<br>*/
    public String getWdfaImpNetTrasferitoNull() {
        return readString(Pos.WDFA_IMP_NET_TRASFERITO_NULL, Len.WDFA_IMP_NET_TRASFERITO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_NET_TRASFERITO = 1;
        public static final int WDFA_IMP_NET_TRASFERITO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_NET_TRASFERITO = 8;
        public static final int WDFA_IMP_NET_TRASFERITO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_NET_TRASFERITO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_NET_TRASFERITO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
