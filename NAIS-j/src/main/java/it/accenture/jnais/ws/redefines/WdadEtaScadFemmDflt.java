package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDAD-ETA-SCAD-FEMM-DFLT<br>
 * Variable: WDAD-ETA-SCAD-FEMM-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadEtaScadFemmDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadEtaScadFemmDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_ETA_SCAD_FEMM_DFLT;
    }

    public void setWdadEtaScadFemmDflt(int wdadEtaScadFemmDflt) {
        writeIntAsPacked(Pos.WDAD_ETA_SCAD_FEMM_DFLT, wdadEtaScadFemmDflt, Len.Int.WDAD_ETA_SCAD_FEMM_DFLT);
    }

    public void setWdadEtaScadFemmDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_ETA_SCAD_FEMM_DFLT, Pos.WDAD_ETA_SCAD_FEMM_DFLT);
    }

    /**Original name: WDAD-ETA-SCAD-FEMM-DFLT<br>*/
    public int getWdadEtaScadFemmDflt() {
        return readPackedAsInt(Pos.WDAD_ETA_SCAD_FEMM_DFLT, Len.Int.WDAD_ETA_SCAD_FEMM_DFLT);
    }

    public byte[] getWdadEtaScadFemmDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_ETA_SCAD_FEMM_DFLT, Pos.WDAD_ETA_SCAD_FEMM_DFLT);
        return buffer;
    }

    public void setWdadEtaScadFemmDfltNull(String wdadEtaScadFemmDfltNull) {
        writeString(Pos.WDAD_ETA_SCAD_FEMM_DFLT_NULL, wdadEtaScadFemmDfltNull, Len.WDAD_ETA_SCAD_FEMM_DFLT_NULL);
    }

    /**Original name: WDAD-ETA-SCAD-FEMM-DFLT-NULL<br>*/
    public String getWdadEtaScadFemmDfltNull() {
        return readString(Pos.WDAD_ETA_SCAD_FEMM_DFLT_NULL, Len.WDAD_ETA_SCAD_FEMM_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_ETA_SCAD_FEMM_DFLT = 1;
        public static final int WDAD_ETA_SCAD_FEMM_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_ETA_SCAD_FEMM_DFLT = 3;
        public static final int WDAD_ETA_SCAD_FEMM_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_ETA_SCAD_FEMM_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
