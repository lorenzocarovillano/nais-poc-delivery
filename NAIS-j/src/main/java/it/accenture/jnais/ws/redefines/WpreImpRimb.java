package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPRE-IMP-RIMB<br>
 * Variable: WPRE-IMP-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreImpRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpreImpRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_IMP_RIMB;
    }

    public void setWpreImpRimb(AfDecimal wpreImpRimb) {
        writeDecimalAsPacked(Pos.WPRE_IMP_RIMB, wpreImpRimb.copy());
    }

    public void setWpreImpRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_IMP_RIMB, Pos.WPRE_IMP_RIMB);
    }

    /**Original name: WPRE-IMP-RIMB<br>*/
    public AfDecimal getWpreImpRimb() {
        return readPackedAsDecimal(Pos.WPRE_IMP_RIMB, Len.Int.WPRE_IMP_RIMB, Len.Fract.WPRE_IMP_RIMB);
    }

    public byte[] getWpreImpRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_IMP_RIMB, Pos.WPRE_IMP_RIMB);
        return buffer;
    }

    public void initWpreImpRimbSpaces() {
        fill(Pos.WPRE_IMP_RIMB, Len.WPRE_IMP_RIMB, Types.SPACE_CHAR);
    }

    public void setWpreImpRimbNull(String wpreImpRimbNull) {
        writeString(Pos.WPRE_IMP_RIMB_NULL, wpreImpRimbNull, Len.WPRE_IMP_RIMB_NULL);
    }

    /**Original name: WPRE-IMP-RIMB-NULL<br>*/
    public String getWpreImpRimbNull() {
        return readString(Pos.WPRE_IMP_RIMB_NULL, Len.WPRE_IMP_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_IMP_RIMB = 1;
        public static final int WPRE_IMP_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_IMP_RIMB = 8;
        public static final int WPRE_IMP_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPRE_IMP_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_IMP_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
