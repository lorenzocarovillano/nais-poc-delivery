package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-IMP-FIN-REVOLVING<br>
 * Variable: P67-IMP-FIN-REVOLVING from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67ImpFinRevolving extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67ImpFinRevolving() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_IMP_FIN_REVOLVING;
    }

    public void setP67ImpFinRevolving(AfDecimal p67ImpFinRevolving) {
        writeDecimalAsPacked(Pos.P67_IMP_FIN_REVOLVING, p67ImpFinRevolving.copy());
    }

    public void setP67ImpFinRevolvingFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_IMP_FIN_REVOLVING, Pos.P67_IMP_FIN_REVOLVING);
    }

    /**Original name: P67-IMP-FIN-REVOLVING<br>*/
    public AfDecimal getP67ImpFinRevolving() {
        return readPackedAsDecimal(Pos.P67_IMP_FIN_REVOLVING, Len.Int.P67_IMP_FIN_REVOLVING, Len.Fract.P67_IMP_FIN_REVOLVING);
    }

    public byte[] getP67ImpFinRevolvingAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_IMP_FIN_REVOLVING, Pos.P67_IMP_FIN_REVOLVING);
        return buffer;
    }

    public void setP67ImpFinRevolvingNull(String p67ImpFinRevolvingNull) {
        writeString(Pos.P67_IMP_FIN_REVOLVING_NULL, p67ImpFinRevolvingNull, Len.P67_IMP_FIN_REVOLVING_NULL);
    }

    /**Original name: P67-IMP-FIN-REVOLVING-NULL<br>*/
    public String getP67ImpFinRevolvingNull() {
        return readString(Pos.P67_IMP_FIN_REVOLVING_NULL, Len.P67_IMP_FIN_REVOLVING_NULL);
    }

    public String getP67ImpFinRevolvingNullFormatted() {
        return Functions.padBlanks(getP67ImpFinRevolvingNull(), Len.P67_IMP_FIN_REVOLVING_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_IMP_FIN_REVOLVING = 1;
        public static final int P67_IMP_FIN_REVOLVING_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_IMP_FIN_REVOLVING = 8;
        public static final int P67_IMP_FIN_REVOLVING_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_IMP_FIN_REVOLVING = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_IMP_FIN_REVOLVING = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
