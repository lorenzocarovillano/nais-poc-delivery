package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-BOLLO-DETT-C<br>
 * Variable: WDFL-IMPST-BOLLO-DETT-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstBolloDettC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstBolloDettC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_BOLLO_DETT_C;
    }

    public void setWdflImpstBolloDettC(AfDecimal wdflImpstBolloDettC) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_BOLLO_DETT_C, wdflImpstBolloDettC.copy());
    }

    public void setWdflImpstBolloDettCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_BOLLO_DETT_C, Pos.WDFL_IMPST_BOLLO_DETT_C);
    }

    /**Original name: WDFL-IMPST-BOLLO-DETT-C<br>*/
    public AfDecimal getWdflImpstBolloDettC() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_BOLLO_DETT_C, Len.Int.WDFL_IMPST_BOLLO_DETT_C, Len.Fract.WDFL_IMPST_BOLLO_DETT_C);
    }

    public byte[] getWdflImpstBolloDettCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_BOLLO_DETT_C, Pos.WDFL_IMPST_BOLLO_DETT_C);
        return buffer;
    }

    public void setWdflImpstBolloDettCNull(String wdflImpstBolloDettCNull) {
        writeString(Pos.WDFL_IMPST_BOLLO_DETT_C_NULL, wdflImpstBolloDettCNull, Len.WDFL_IMPST_BOLLO_DETT_C_NULL);
    }

    /**Original name: WDFL-IMPST-BOLLO-DETT-C-NULL<br>*/
    public String getWdflImpstBolloDettCNull() {
        return readString(Pos.WDFL_IMPST_BOLLO_DETT_C_NULL, Len.WDFL_IMPST_BOLLO_DETT_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_BOLLO_DETT_C = 1;
        public static final int WDFL_IMPST_BOLLO_DETT_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_BOLLO_DETT_C = 8;
        public static final int WDFL_IMPST_BOLLO_DETT_C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_BOLLO_DETT_C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_BOLLO_DETT_C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
