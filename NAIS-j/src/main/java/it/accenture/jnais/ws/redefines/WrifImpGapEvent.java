package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRIF-IMP-GAP-EVENT<br>
 * Variable: WRIF-IMP-GAP-EVENT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrifImpGapEvent extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrifImpGapEvent() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIF_IMP_GAP_EVENT;
    }

    public void setWrifImpGapEvent(AfDecimal wrifImpGapEvent) {
        writeDecimalAsPacked(Pos.WRIF_IMP_GAP_EVENT, wrifImpGapEvent.copy());
    }

    public void setWrifImpGapEventFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIF_IMP_GAP_EVENT, Pos.WRIF_IMP_GAP_EVENT);
    }

    /**Original name: WRIF-IMP-GAP-EVENT<br>*/
    public AfDecimal getWrifImpGapEvent() {
        return readPackedAsDecimal(Pos.WRIF_IMP_GAP_EVENT, Len.Int.WRIF_IMP_GAP_EVENT, Len.Fract.WRIF_IMP_GAP_EVENT);
    }

    public byte[] getWrifImpGapEventAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIF_IMP_GAP_EVENT, Pos.WRIF_IMP_GAP_EVENT);
        return buffer;
    }

    public void initWrifImpGapEventSpaces() {
        fill(Pos.WRIF_IMP_GAP_EVENT, Len.WRIF_IMP_GAP_EVENT, Types.SPACE_CHAR);
    }

    public void setWrifImpGapEventNull(String wrifImpGapEventNull) {
        writeString(Pos.WRIF_IMP_GAP_EVENT_NULL, wrifImpGapEventNull, Len.WRIF_IMP_GAP_EVENT_NULL);
    }

    /**Original name: WRIF-IMP-GAP-EVENT-NULL<br>*/
    public String getWrifImpGapEventNull() {
        return readString(Pos.WRIF_IMP_GAP_EVENT_NULL, Len.WRIF_IMP_GAP_EVENT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIF_IMP_GAP_EVENT = 1;
        public static final int WRIF_IMP_GAP_EVENT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIF_IMP_GAP_EVENT = 8;
        public static final int WRIF_IMP_GAP_EVENT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRIF_IMP_GAP_EVENT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIF_IMP_GAP_EVENT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
