package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.AreaLdbv1131;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Idsv8888;
import it.accenture.jnais.copy.Lccvpogz;
import it.accenture.jnais.copy.ParamOggLoas0800;
import it.accenture.jnais.copy.WkIspcMax;
import it.accenture.jnais.ws.enums.WkManfee;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTpDato;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.enums.WsTpTrch;
import it.accenture.jnais.ws.occurs.WpogTabParamOgg;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS0800<br>
 * Generated as a class for rule WS.<br>*/
public class Loas0800Data {

    //==== PROPERTIES ====
    public static final int WPOG_TAB_PARAM_OGG_MAXOCCURS = 100;
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IDSV8888
    private Idsv8888 idsv8888 = new Idsv8888();
    //Original name: PARAM-OGG
    private ParamOggLoas0800 paramOgg = new ParamOggLoas0800();
    //Original name: AREA-LDBV1131
    private AreaLdbv1131 areaLdbv1131 = new AreaLdbv1131();
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *  COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LOAS0800";
    //Original name: IX-INDICI
    private IxIndiciLoas0800 ixIndici = new IxIndiciLoas0800();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: WK-GESTIONE-MSG-ERR
    private WkGestioneMsgErr wkGestioneMsgErr = new WkGestioneMsgErr();
    //Original name: IO-A2K-LCCC0003
    private IoA2kLccc0003 ioA2kLccc0003 = new IoA2kLccc0003();
    //Original name: IN-RCODE
    private String inRcode = DefaultValues.stringVal(Len.IN_RCODE);
    /**Original name: WK-CURRENT-DATE<br>
	 * <pre> --> Area di accept della data dalla variabile di sistema DATE</pre>*/
    private String wkCurrentDate = "00000000";
    //Original name: AREA-IO-LCCS0029
    private AreaIoLccs0029 areaIoLccs0029 = new AreaIoLccs0029();
    /**Original name: WK-MANFEE<br>
	 * <pre>----------------------------------------------------------------*
	 *      VARIABILI PER GESTIONE MANAGEMENT FEE
	 * ----------------------------------------------------------------*
	 *  --> Indicatore del calcolo del Management Fee</pre>*/
    private WkManfee wkManfee = new WkManfee();
    //Original name: WK-PERMANFEE
    private String wkPermanfee = DefaultValues.stringVal(Len.WK_PERMANFEE);
    //Original name: WK-MESIDIFFMFEE
    private String wkMesidiffmfee = DefaultValues.stringVal(Len.WK_MESIDIFFMFEE);
    //Original name: WK-DECADELMFEE
    private String wkDecadelmfee = DefaultValues.stringVal(Len.WK_DECADELMFEE);
    //Original name: WK-TOT-MANFEE
    private AfDecimal wkTotManfee = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-DT-RICOR-SUCC
    private String wkDtRicorSucc = "00000000";
    //Original name: WK-DT-CALCOLATA
    private String wkDtCalcolata = "00000000";
    //Original name: WK-DT-ULT-EROG-MF
    private String wkDtUltErogMf = "00000000";
    //Original name: WK-APPO-DT
    private WkAppoDt wkAppoDt = new WkAppoDt();
    /**Original name: LDBS1130<br>
	 * <pre>  --> MODULO LDBS1130</pre>*/
    private String ldbs1130 = "LDBS1130";
    //Original name: AREA-IO-ISPC0211
    private AreaIoIsps0211 areaIoIspc0211 = new AreaIoIsps0211();
    //Original name: WK-ISPC-MAX
    private WkIspcMax wkIspcMax = new WkIspcMax();
    /**Original name: WS-TP-TRCH<br>
	 * <pre>*****************************************************************
	 *     TP_TRCH
	 * *****************************************************************</pre>*/
    private WsTpTrch wsTpTrch = new WsTpTrch();
    /**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    /**Original name: WS-TP-DATO<br>
	 * <pre>*****************************************************************
	 *     TP_D (Tipo Dato)
	 * *****************************************************************</pre>*/
    private WsTpDato wsTpDato = new WsTpDato();
    //Original name: LCCVPOGZ
    private Lccvpogz lccvpogz = new Lccvpogz();
    //Original name: WPOG-ELE-PARAM-OGG-MAX
    private short wpogEleParamOggMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPOG-TAB-PARAM-OGG
    private WpogTabParamOgg[] wpogTabParamOgg = new WpogTabParamOgg[WPOG_TAB_PARAM_OGG_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Loas0800Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wpogTabParamOggIdx = 1; wpogTabParamOggIdx <= WPOG_TAB_PARAM_OGG_MAXOCCURS; wpogTabParamOggIdx++) {
            wpogTabParamOgg[wpogTabParamOggIdx - 1] = new WpogTabParamOgg();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setInRcodeFromBuffer(byte[] buffer) {
        inRcode = MarshalByte.readFixedString(buffer, 1, Len.IN_RCODE);
    }

    public String getInRcodeFormatted() {
        return this.inRcode;
    }

    public void setWkCurrentDateFormatted(String wkCurrentDate) {
        this.wkCurrentDate = Trunc.toUnsignedNumeric(wkCurrentDate, Len.WK_CURRENT_DATE);
    }

    public int getWkCurrentDate() {
        return NumericDisplay.asInt(this.wkCurrentDate);
    }

    public void setWkPermanfee(int wkPermanfee) {
        this.wkPermanfee = NumericDisplay.asString(wkPermanfee, Len.WK_PERMANFEE);
    }

    public void setWkPermanfeeFormatted(String wkPermanfee) {
        this.wkPermanfee = Trunc.toUnsignedNumeric(wkPermanfee, Len.WK_PERMANFEE);
    }

    public int getWkPermanfee() {
        return NumericDisplay.asInt(this.wkPermanfee);
    }

    public void setWkMesidiffmfee(int wkMesidiffmfee) {
        this.wkMesidiffmfee = NumericDisplay.asString(wkMesidiffmfee, Len.WK_MESIDIFFMFEE);
    }

    public void setWkMesidiffmfeeFormatted(String wkMesidiffmfee) {
        this.wkMesidiffmfee = Trunc.toUnsignedNumeric(wkMesidiffmfee, Len.WK_MESIDIFFMFEE);
    }

    public int getWkMesidiffmfee() {
        return NumericDisplay.asInt(this.wkMesidiffmfee);
    }

    public String getWkMesidiffmfeeFormatted() {
        return this.wkMesidiffmfee;
    }

    public void setWkDecadelmfee(int wkDecadelmfee) {
        this.wkDecadelmfee = NumericDisplay.asString(wkDecadelmfee, Len.WK_DECADELMFEE);
    }

    public void setWkDecadelmfeeFormatted(String wkDecadelmfee) {
        this.wkDecadelmfee = Trunc.toUnsignedNumeric(wkDecadelmfee, Len.WK_DECADELMFEE);
    }

    public int getWkDecadelmfee() {
        return NumericDisplay.asInt(this.wkDecadelmfee);
    }

    public void setWkTotManfee(AfDecimal wkTotManfee) {
        this.wkTotManfee.assign(wkTotManfee);
    }

    public AfDecimal getWkTotManfee() {
        return this.wkTotManfee.copy();
    }

    public void setWkDtRicorSuccFormatted(String wkDtRicorSucc) {
        this.wkDtRicorSucc = Trunc.toUnsignedNumeric(wkDtRicorSucc, Len.WK_DT_RICOR_SUCC);
    }

    public int getWkDtRicorSucc() {
        return NumericDisplay.asInt(this.wkDtRicorSucc);
    }

    public String getWkDtRicorSuccFormatted() {
        return this.wkDtRicorSucc;
    }

    public void setWkDtCalcolata(int wkDtCalcolata) {
        this.wkDtCalcolata = NumericDisplay.asString(wkDtCalcolata, Len.WK_DT_CALCOLATA);
    }

    public void setWkDtCalcolataFormatted(String wkDtCalcolata) {
        this.wkDtCalcolata = Trunc.toUnsignedNumeric(wkDtCalcolata, Len.WK_DT_CALCOLATA);
    }

    public void setWkDtCalcolataFromBuffer(byte[] buffer) {
        wkDtCalcolata = MarshalByte.readFixedString(buffer, 1, Len.WK_DT_CALCOLATA);
    }

    public int getWkDtCalcolata() {
        return NumericDisplay.asInt(this.wkDtCalcolata);
    }

    public String getWkDtCalcolataFormatted() {
        return this.wkDtCalcolata;
    }

    public void setWkDtUltErogMfFormatted(String wkDtUltErogMf) {
        this.wkDtUltErogMf = Trunc.toUnsignedNumeric(wkDtUltErogMf, Len.WK_DT_ULT_EROG_MF);
    }

    public int getWkDtUltErogMf() {
        return NumericDisplay.asInt(this.wkDtUltErogMf);
    }

    public String getLdbs1130() {
        return this.ldbs1130;
    }

    public void setWpogEleParamOggMax(short wpogEleParamOggMax) {
        this.wpogEleParamOggMax = wpogEleParamOggMax;
    }

    public short getWpogEleParamOggMax() {
        return this.wpogEleParamOggMax;
    }

    public AreaIoIsps0211 getAreaIoIspc0211() {
        return areaIoIspc0211;
    }

    public AreaIoLccs0029 getAreaIoLccs0029() {
        return areaIoLccs0029;
    }

    public AreaLdbv1131 getAreaLdbv1131() {
        return areaLdbv1131;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Idsv8888 getIdsv8888() {
        return idsv8888;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public IoA2kLccc0003 getIoA2kLccc0003() {
        return ioA2kLccc0003;
    }

    public IxIndiciLoas0800 getIxIndici() {
        return ixIndici;
    }

    public Lccvpogz getLccvpogz() {
        return lccvpogz;
    }

    public ParamOggLoas0800 getParamOgg() {
        return paramOgg;
    }

    public WkAppoDt getWkAppoDt() {
        return wkAppoDt;
    }

    public WkGestioneMsgErr getWkGestioneMsgErr() {
        return wkGestioneMsgErr;
    }

    public WkIspcMax getWkIspcMax() {
        return wkIspcMax;
    }

    public WkManfee getWkManfee() {
        return wkManfee;
    }

    public WpogTabParamOgg getWpogTabParamOgg(int idx) {
        return wpogTabParamOgg[idx - 1];
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTpDato getWsTpDato() {
        return wsTpDato;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    public WsTpTrch getWsTpTrch() {
        return wsTpTrch;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IN_RCODE = 2;
        public static final int WK_DATA_CALCOLATA = 8;
        public static final int WK_CURRENT_DATE = 8;
        public static final int WK_GG_INC = 5;
        public static final int WK_PERMANFEE = 5;
        public static final int WK_MESIDIFFMFEE = 5;
        public static final int WK_DECADELMFEE = 5;
        public static final int WK_NUM_ADD_FRAZ = 2;
        public static final int WK_DT_RICOR_SUCC = 8;
        public static final int WK_DT_CALCOLATA = 8;
        public static final int WK_DT_ULT_EROG_MF = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
