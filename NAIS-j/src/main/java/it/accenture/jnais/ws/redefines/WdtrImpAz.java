package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-IMP-AZ<br>
 * Variable: WDTR-IMP-AZ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_IMP_AZ;
    }

    public void setWdtrImpAz(AfDecimal wdtrImpAz) {
        writeDecimalAsPacked(Pos.WDTR_IMP_AZ, wdtrImpAz.copy());
    }

    /**Original name: WDTR-IMP-AZ<br>*/
    public AfDecimal getWdtrImpAz() {
        return readPackedAsDecimal(Pos.WDTR_IMP_AZ, Len.Int.WDTR_IMP_AZ, Len.Fract.WDTR_IMP_AZ);
    }

    public void setWdtrImpAzNull(String wdtrImpAzNull) {
        writeString(Pos.WDTR_IMP_AZ_NULL, wdtrImpAzNull, Len.WDTR_IMP_AZ_NULL);
    }

    /**Original name: WDTR-IMP-AZ-NULL<br>*/
    public String getWdtrImpAzNull() {
        return readString(Pos.WDTR_IMP_AZ_NULL, Len.WDTR_IMP_AZ_NULL);
    }

    public String getWdtrImpAzNullFormatted() {
        return Functions.padBlanks(getWdtrImpAzNull(), Len.WDTR_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_IMP_AZ = 1;
        public static final int WDTR_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_IMP_AZ = 8;
        public static final int WDTR_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
