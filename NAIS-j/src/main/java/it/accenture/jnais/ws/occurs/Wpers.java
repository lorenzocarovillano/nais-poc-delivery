package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPERS<br>
 * Variables: WPERS from program LLBS0230<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wpers {

    //==== PROPERTIES ====
    //Original name: WA25-IND-CLI
    private char indCli = DefaultValues.CHAR_VAL;
    //Original name: WA25-COD-FISC
    private String codFisc = DefaultValues.stringVal(Len.COD_FISC);
    //Original name: WA25-COD-PRT-IVA
    private String codPrtIva = DefaultValues.stringVal(Len.COD_PRT_IVA);

    //==== METHODS ====
    public void setIndCli(char indCli) {
        this.indCli = indCli;
    }

    public char getIndCli() {
        return this.indCli;
    }

    public void setCodFisc(String codFisc) {
        this.codFisc = Functions.subString(codFisc, Len.COD_FISC);
    }

    public String getCodFisc() {
        return this.codFisc;
    }

    public String getCodFiscFormatted() {
        return Functions.padBlanks(getCodFisc(), Len.COD_FISC);
    }

    public void setCodPrtIva(String codPrtIva) {
        this.codPrtIva = Functions.subString(codPrtIva, Len.COD_PRT_IVA);
    }

    public String getCodPrtIva() {
        return this.codPrtIva;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_FISC = 16;
        public static final int COD_PRT_IVA = 11;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
