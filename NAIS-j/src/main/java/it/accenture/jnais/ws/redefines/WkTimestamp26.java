package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WK-TIMESTAMP-26<br>
 * Variable: WK-TIMESTAMP-26 from program LLBM0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkTimestamp26 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkTimestamp26() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_TIMESTAMP26;
    }

    public String getWkDataSistemaFormatted() {
        return readFixedString(Pos.WK_DATA_SISTEMA, Len.WK_DATA_SISTEMA);
    }

    public void setGgSys(String ggSys) {
        writeString(Pos.GG_SYS, ggSys, Len.GG_SYS);
    }

    /**Original name: GG-SYS<br>*/
    public String getGgSys() {
        return readString(Pos.GG_SYS, Len.GG_SYS);
    }

    public void setMmSys(String mmSys) {
        writeString(Pos.MM_SYS, mmSys, Len.MM_SYS);
    }

    /**Original name: MM-SYS<br>*/
    public String getMmSys() {
        return readString(Pos.MM_SYS, Len.MM_SYS);
    }

    public void setAaaaSys(String aaaaSys) {
        writeString(Pos.AAAA_SYS, aaaaSys, Len.AAAA_SYS);
    }

    /**Original name: AAAA-SYS<br>*/
    public String getAaaaSys() {
        return readString(Pos.AAAA_SYS, Len.AAAA_SYS);
    }

    public String getWkOraSistemaFormatted() {
        return readFixedString(Pos.WK_ORA_SISTEMA, Len.WK_ORA_SISTEMA);
    }

    public void setHhSys(String hhSys) {
        writeString(Pos.HH_SYS, hhSys, Len.HH_SYS);
    }

    /**Original name: HH-SYS<br>*/
    public String getHhSys() {
        return readString(Pos.HH_SYS, Len.HH_SYS);
    }

    public void setMiSys(String miSys) {
        writeString(Pos.MI_SYS, miSys, Len.MI_SYS);
    }

    /**Original name: MI-SYS<br>*/
    public String getMiSys() {
        return readString(Pos.MI_SYS, Len.MI_SYS);
    }

    public void setSsSys(String ssSys) {
        writeString(Pos.SS_SYS, ssSys, Len.SS_SYS);
    }

    /**Original name: SS-SYS<br>*/
    public String getSsSys() {
        return readString(Pos.SS_SYS, Len.SS_SYS);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_TIMESTAMP26 = 1;
        public static final int WK_TIMESTAMP26_TS = 1;
        public static final int WK_DATA_SISTEMA = WK_TIMESTAMP26_TS;
        public static final int GG_SYS = WK_DATA_SISTEMA;
        public static final int FLR1 = GG_SYS + Len.GG_SYS;
        public static final int MM_SYS = FLR1 + Len.FLR1;
        public static final int FLR2 = MM_SYS + Len.MM_SYS;
        public static final int AAAA_SYS = FLR2 + Len.FLR1;
        public static final int WK_ORA_SISTEMA = AAAA_SYS + Len.AAAA_SYS;
        public static final int HH_SYS = WK_ORA_SISTEMA;
        public static final int FLR3 = HH_SYS + Len.HH_SYS;
        public static final int MI_SYS = FLR3 + Len.FLR1;
        public static final int FLR4 = MI_SYS + Len.MI_SYS;
        public static final int SS_SYS = FLR4 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GG_SYS = 2;
        public static final int FLR1 = 1;
        public static final int MM_SYS = 2;
        public static final int AAAA_SYS = 4;
        public static final int HH_SYS = 2;
        public static final int MI_SYS = 2;
        public static final int WK_TIMESTAMP26 = 26;
        public static final int WK_DATA_SISTEMA = GG_SYS + MM_SYS + AAAA_SYS + 2 * FLR1;
        public static final int SS_SYS = 2;
        public static final int WK_ORA_SISTEMA = HH_SYS + MI_SYS + SS_SYS + 2 * FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
