package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-RP-IN<br>
 * Variable: PCO-DT-ULT-BOLL-RP-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollRpIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollRpIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_RP_IN;
    }

    public void setPcoDtUltBollRpIn(int pcoDtUltBollRpIn) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_RP_IN, pcoDtUltBollRpIn, Len.Int.PCO_DT_ULT_BOLL_RP_IN);
    }

    public void setPcoDtUltBollRpInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RP_IN, Pos.PCO_DT_ULT_BOLL_RP_IN);
    }

    /**Original name: PCO-DT-ULT-BOLL-RP-IN<br>*/
    public int getPcoDtUltBollRpIn() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_RP_IN, Len.Int.PCO_DT_ULT_BOLL_RP_IN);
    }

    public byte[] getPcoDtUltBollRpInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RP_IN, Pos.PCO_DT_ULT_BOLL_RP_IN);
        return buffer;
    }

    public void initPcoDtUltBollRpInHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_RP_IN, Len.PCO_DT_ULT_BOLL_RP_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollRpInNull(String pcoDtUltBollRpInNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_RP_IN_NULL, pcoDtUltBollRpInNull, Len.PCO_DT_ULT_BOLL_RP_IN_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-RP-IN-NULL<br>*/
    public String getPcoDtUltBollRpInNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_RP_IN_NULL, Len.PCO_DT_ULT_BOLL_RP_IN_NULL);
    }

    public String getPcoDtUltBollRpInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollRpInNull(), Len.PCO_DT_ULT_BOLL_RP_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RP_IN = 1;
        public static final int PCO_DT_ULT_BOLL_RP_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RP_IN = 5;
        public static final int PCO_DT_ULT_BOLL_RP_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_RP_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
