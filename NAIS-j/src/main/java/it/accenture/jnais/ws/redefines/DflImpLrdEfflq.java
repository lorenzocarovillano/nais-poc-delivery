package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMP-LRD-EFFLQ<br>
 * Variable: DFL-IMP-LRD-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpLrdEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpLrdEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMP_LRD_EFFLQ;
    }

    public void setDflImpLrdEfflq(AfDecimal dflImpLrdEfflq) {
        writeDecimalAsPacked(Pos.DFL_IMP_LRD_EFFLQ, dflImpLrdEfflq.copy());
    }

    public void setDflImpLrdEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMP_LRD_EFFLQ, Pos.DFL_IMP_LRD_EFFLQ);
    }

    /**Original name: DFL-IMP-LRD-EFFLQ<br>*/
    public AfDecimal getDflImpLrdEfflq() {
        return readPackedAsDecimal(Pos.DFL_IMP_LRD_EFFLQ, Len.Int.DFL_IMP_LRD_EFFLQ, Len.Fract.DFL_IMP_LRD_EFFLQ);
    }

    public byte[] getDflImpLrdEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMP_LRD_EFFLQ, Pos.DFL_IMP_LRD_EFFLQ);
        return buffer;
    }

    public void setDflImpLrdEfflqNull(String dflImpLrdEfflqNull) {
        writeString(Pos.DFL_IMP_LRD_EFFLQ_NULL, dflImpLrdEfflqNull, Len.DFL_IMP_LRD_EFFLQ_NULL);
    }

    /**Original name: DFL-IMP-LRD-EFFLQ-NULL<br>*/
    public String getDflImpLrdEfflqNull() {
        return readString(Pos.DFL_IMP_LRD_EFFLQ_NULL, Len.DFL_IMP_LRD_EFFLQ_NULL);
    }

    public String getDflImpLrdEfflqNullFormatted() {
        return Functions.padBlanks(getDflImpLrdEfflqNull(), Len.DFL_IMP_LRD_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMP_LRD_EFFLQ = 1;
        public static final int DFL_IMP_LRD_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMP_LRD_EFFLQ = 8;
        public static final int DFL_IMP_LRD_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMP_LRD_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMP_LRD_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
