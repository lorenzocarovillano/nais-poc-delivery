package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: FILLER-COM-DATA<br>
 * Variable: FILLER-COM-DATA from program LCCS0004<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class FillerComData {

    //==== PROPERTIES ====
    //Original name: C-AA1
    private String aa1 = DefaultValues.stringVal(Len.AA1);
    //Original name: C-AA2
    private String aa2 = DefaultValues.stringVal(Len.AA2);

    //==== METHODS ====
    public void setcAaaaFormatted(String cAaaa) {
        int position = 1;
        byte[] buffer = getFillerComDataBytes();
        MarshalByte.writeString(buffer, position, Trunc.toNumeric(cAaaa, Len.C_AAAA), Len.C_AAAA);
        setFillerComDataBytes(buffer);
    }

    /**Original name: C-AAAA<br>*/
    public short getcAaaa() {
        int position = 1;
        return MarshalByte.readShort(getFillerComDataBytes(), position, Len.Int.C_AAAA, SignType.NO_SIGN);
    }

    public void setFillerComDataBytes(byte[] buffer) {
        setFillerComDataBytes(buffer, 1);
    }

    /**Original name: FILLER-COM-DATA<br>*/
    public byte[] getFillerComDataBytes() {
        byte[] buffer = new byte[Len.FILLER_COM_DATA];
        return getFillerComDataBytes(buffer, 1);
    }

    public void setFillerComDataBytes(byte[] buffer, int offset) {
        int position = offset;
        aa1 = MarshalByte.readFixedString(buffer, position, Len.AA1);
        position += Len.AA1;
        aa2 = MarshalByte.readFixedString(buffer, position, Len.AA2);
    }

    public byte[] getFillerComDataBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aa1, Len.AA1);
        position += Len.AA1;
        MarshalByte.writeString(buffer, position, aa2, Len.AA2);
        return buffer;
    }

    public void setAa1Formatted(String aa1) {
        this.aa1 = Trunc.toUnsignedNumeric(aa1, Len.AA1);
    }

    public short getAa1() {
        return NumericDisplay.asShort(this.aa1);
    }

    public void setAa2Formatted(String aa2) {
        this.aa2 = Trunc.toUnsignedNumeric(aa2, Len.AA2);
    }

    public short getAa2() {
        return NumericDisplay.asShort(this.aa2);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA1 = 2;
        public static final int AA2 = 2;
        public static final int FILLER_COM_DATA = AA1 + AA2;
        public static final int C_AAAA = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int C_AAAA = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
