package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-CAR-IAS<br>
 * Variable: TIT-TOT-CAR-IAS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotCarIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotCarIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_CAR_IAS;
    }

    public void setTitTotCarIas(AfDecimal titTotCarIas) {
        writeDecimalAsPacked(Pos.TIT_TOT_CAR_IAS, titTotCarIas.copy());
    }

    public void setTitTotCarIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_CAR_IAS, Pos.TIT_TOT_CAR_IAS);
    }

    /**Original name: TIT-TOT-CAR-IAS<br>*/
    public AfDecimal getTitTotCarIas() {
        return readPackedAsDecimal(Pos.TIT_TOT_CAR_IAS, Len.Int.TIT_TOT_CAR_IAS, Len.Fract.TIT_TOT_CAR_IAS);
    }

    public byte[] getTitTotCarIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_CAR_IAS, Pos.TIT_TOT_CAR_IAS);
        return buffer;
    }

    public void setTitTotCarIasNull(String titTotCarIasNull) {
        writeString(Pos.TIT_TOT_CAR_IAS_NULL, titTotCarIasNull, Len.TIT_TOT_CAR_IAS_NULL);
    }

    /**Original name: TIT-TOT-CAR-IAS-NULL<br>*/
    public String getTitTotCarIasNull() {
        return readString(Pos.TIT_TOT_CAR_IAS_NULL, Len.TIT_TOT_CAR_IAS_NULL);
    }

    public String getTitTotCarIasNullFormatted() {
        return Functions.padBlanks(getTitTotCarIasNull(), Len.TIT_TOT_CAR_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_CAR_IAS = 1;
        public static final int TIT_TOT_CAR_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_CAR_IAS = 8;
        public static final int TIT_TOT_CAR_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_CAR_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_CAR_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
