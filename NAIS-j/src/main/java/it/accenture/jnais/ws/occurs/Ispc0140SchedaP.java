package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Lccc006TrattDati;

/**Original name: ISPC0140-SCHEDA-P<br>
 * Variables: ISPC0140-SCHEDA-P from copybook ISPC0140<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0140SchedaP {

    //==== PROPERTIES ====
    public static final int AREA_VARIABILI_P_MAXOCCURS = 100;
    //Original name: ISPC0140-TIPO-LIVELLO-P
    private char tipoLivelloP = DefaultValues.CHAR_VAL;
    //Original name: ISPC0140-CODICE-LIVELLO-P
    private String codiceLivelloP = DefaultValues.stringVal(Len.CODICE_LIVELLO_P);
    //Original name: ISPC0140-FLAG-MOD-CALC-PRE-P
    private Lccc006TrattDati flagModCalcPreP = new Lccc006TrattDati();
    //Original name: ISPC0140-NUM-COMPON-MAX-ELE-P
    private String numComponMaxEleP = DefaultValues.stringVal(Len.NUM_COMPON_MAX_ELE_P);
    //Original name: ISPC0140-AREA-VARIABILI-P
    private Ispc0140AreaVariabiliP[] areaVariabiliP = new Ispc0140AreaVariabiliP[AREA_VARIABILI_P_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ispc0140SchedaP() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int areaVariabiliPIdx = 1; areaVariabiliPIdx <= AREA_VARIABILI_P_MAXOCCURS; areaVariabiliPIdx++) {
            areaVariabiliP[areaVariabiliPIdx - 1] = new Ispc0140AreaVariabiliP();
        }
    }

    public void setSchedaPBytes(byte[] buffer, int offset) {
        int position = offset;
        tipoLivelloP = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        codiceLivelloP = MarshalByte.readString(buffer, position, Len.CODICE_LIVELLO_P);
        position += Len.CODICE_LIVELLO_P;
        flagModCalcPreP.setFlagModCalcPreP(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        numComponMaxEleP = MarshalByte.readFixedString(buffer, position, Len.NUM_COMPON_MAX_ELE_P);
        position += Len.NUM_COMPON_MAX_ELE_P;
        for (int idx = 1; idx <= AREA_VARIABILI_P_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                areaVariabiliP[idx - 1].setAreaVariabiliPBytes(buffer, position);
                position += Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;
            }
            else {
                areaVariabiliP[idx - 1].initAreaVariabiliPSpaces();
                position += Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;
            }
        }
    }

    public byte[] getSchedaPBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, tipoLivelloP);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codiceLivelloP, Len.CODICE_LIVELLO_P);
        position += Len.CODICE_LIVELLO_P;
        MarshalByte.writeChar(buffer, position, flagModCalcPreP.getFlagModCalcPreP());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, numComponMaxEleP, Len.NUM_COMPON_MAX_ELE_P);
        position += Len.NUM_COMPON_MAX_ELE_P;
        for (int idx = 1; idx <= AREA_VARIABILI_P_MAXOCCURS; idx++) {
            areaVariabiliP[idx - 1].getAreaVariabiliPBytes(buffer, position);
            position += Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;
        }
        return buffer;
    }

    public void initSchedaPSpaces() {
        tipoLivelloP = Types.SPACE_CHAR;
        codiceLivelloP = "";
        flagModCalcPreP.setFlagModCalcPreP(Types.SPACE_CHAR);
        numComponMaxEleP = "";
        for (int idx = 1; idx <= AREA_VARIABILI_P_MAXOCCURS; idx++) {
            areaVariabiliP[idx - 1].initAreaVariabiliPSpaces();
        }
    }

    public void setTipoLivelloP(char tipoLivelloP) {
        this.tipoLivelloP = tipoLivelloP;
    }

    public char getTipoLivelloP() {
        return this.tipoLivelloP;
    }

    public void setCodiceLivelloP(String codiceLivelloP) {
        this.codiceLivelloP = Functions.subString(codiceLivelloP, Len.CODICE_LIVELLO_P);
    }

    public String getCodiceLivelloP() {
        return this.codiceLivelloP;
    }

    public void setIspc0140NumComponMaxEleP(short ispc0140NumComponMaxEleP) {
        this.numComponMaxEleP = NumericDisplay.asString(ispc0140NumComponMaxEleP, Len.NUM_COMPON_MAX_ELE_P);
    }

    public short getIspc0140NumComponMaxEleP() {
        return NumericDisplay.asShort(this.numComponMaxEleP);
    }

    public Ispc0140AreaVariabiliP getAreaVariabiliP(int idx) {
        return areaVariabiliP[idx - 1];
    }

    public Lccc006TrattDati getFlagModCalcPreP() {
        return flagModCalcPreP;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_LIVELLO_P = 1;
        public static final int CODICE_LIVELLO_P = 12;
        public static final int NUM_COMPON_MAX_ELE_P = 3;
        public static final int SCHEDA_P = TIPO_LIVELLO_P + CODICE_LIVELLO_P + Lccc006TrattDati.Len.FLAG_MOD_CALC_PRE_P + NUM_COMPON_MAX_ELE_P + Ispc0140SchedaP.AREA_VARIABILI_P_MAXOCCURS * Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
