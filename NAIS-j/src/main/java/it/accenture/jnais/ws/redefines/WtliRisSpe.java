package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-RIS-SPE<br>
 * Variable: WTLI-RIS-SPE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliRisSpe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliRisSpe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_RIS_SPE;
    }

    public void setWtliRisSpe(AfDecimal wtliRisSpe) {
        writeDecimalAsPacked(Pos.WTLI_RIS_SPE, wtliRisSpe.copy());
    }

    public void setWtliRisSpeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_RIS_SPE, Pos.WTLI_RIS_SPE);
    }

    /**Original name: WTLI-RIS-SPE<br>*/
    public AfDecimal getWtliRisSpe() {
        return readPackedAsDecimal(Pos.WTLI_RIS_SPE, Len.Int.WTLI_RIS_SPE, Len.Fract.WTLI_RIS_SPE);
    }

    public byte[] getWtliRisSpeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_RIS_SPE, Pos.WTLI_RIS_SPE);
        return buffer;
    }

    public void initWtliRisSpeSpaces() {
        fill(Pos.WTLI_RIS_SPE, Len.WTLI_RIS_SPE, Types.SPACE_CHAR);
    }

    public void setWtliRisSpeNull(String wtliRisSpeNull) {
        writeString(Pos.WTLI_RIS_SPE_NULL, wtliRisSpeNull, Len.WTLI_RIS_SPE_NULL);
    }

    /**Original name: WTLI-RIS-SPE-NULL<br>*/
    public String getWtliRisSpeNull() {
        return readString(Pos.WTLI_RIS_SPE_NULL, Len.WTLI_RIS_SPE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_RIS_SPE = 1;
        public static final int WTLI_RIS_SPE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_RIS_SPE = 8;
        public static final int WTLI_RIS_SPE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_RIS_SPE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_RIS_SPE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
