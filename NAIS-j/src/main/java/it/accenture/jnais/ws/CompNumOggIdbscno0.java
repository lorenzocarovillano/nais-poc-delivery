package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.ICompNumOgg;
import it.accenture.jnais.ws.redefines.CnoLunghezzaDato;

/**Original name: COMP-NUM-OGG<br>
 * Variable: COMP-NUM-OGG from copybook IDBVCNO1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class CompNumOggIdbscno0 extends SerializableParameter implements ICompNumOgg {

    //==== PROPERTIES ====
    //Original name: CNO-COD-COMPAGNIA-ANIA
    private int cnoCodCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: CNO-FORMA-ASSICURATIVA
    private String cnoFormaAssicurativa = DefaultValues.stringVal(Len.CNO_FORMA_ASSICURATIVA);
    //Original name: CNO-COD-OGGETTO
    private String cnoCodOggetto = DefaultValues.stringVal(Len.CNO_COD_OGGETTO);
    //Original name: CNO-POSIZIONE
    private int cnoPosizione = DefaultValues.INT_VAL;
    //Original name: CNO-COD-STR-DATO
    private String cnoCodStrDato = DefaultValues.stringVal(Len.CNO_COD_STR_DATO);
    //Original name: CNO-COD-DATO
    private String cnoCodDato = DefaultValues.stringVal(Len.CNO_COD_DATO);
    //Original name: CNO-VALORE-DEFAULT
    private String cnoValoreDefault = DefaultValues.stringVal(Len.CNO_VALORE_DEFAULT);
    //Original name: CNO-LUNGHEZZA-DATO
    private CnoLunghezzaDato cnoLunghezzaDato = new CnoLunghezzaDato();
    //Original name: CNO-FLAG-KEY-ULT-PROGR
    private char cnoFlagKeyUltProgr = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.COMP_NUM_OGG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setCompNumOggBytes(buf);
    }

    public void setCompNumOggBytes(byte[] buffer) {
        setCompNumOggBytes(buffer, 1);
    }

    public byte[] getCompNumOggBytes() {
        byte[] buffer = new byte[Len.COMP_NUM_OGG];
        return getCompNumOggBytes(buffer, 1);
    }

    public void setCompNumOggBytes(byte[] buffer, int offset) {
        int position = offset;
        cnoCodCompagniaAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CNO_COD_COMPAGNIA_ANIA, 0);
        position += Len.CNO_COD_COMPAGNIA_ANIA;
        cnoFormaAssicurativa = MarshalByte.readString(buffer, position, Len.CNO_FORMA_ASSICURATIVA);
        position += Len.CNO_FORMA_ASSICURATIVA;
        cnoCodOggetto = MarshalByte.readString(buffer, position, Len.CNO_COD_OGGETTO);
        position += Len.CNO_COD_OGGETTO;
        cnoPosizione = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CNO_POSIZIONE, 0);
        position += Len.CNO_POSIZIONE;
        cnoCodStrDato = MarshalByte.readString(buffer, position, Len.CNO_COD_STR_DATO);
        position += Len.CNO_COD_STR_DATO;
        cnoCodDato = MarshalByte.readString(buffer, position, Len.CNO_COD_DATO);
        position += Len.CNO_COD_DATO;
        cnoValoreDefault = MarshalByte.readString(buffer, position, Len.CNO_VALORE_DEFAULT);
        position += Len.CNO_VALORE_DEFAULT;
        cnoLunghezzaDato.setCnoLunghezzaDatoFromBuffer(buffer, position);
        position += CnoLunghezzaDato.Len.CNO_LUNGHEZZA_DATO;
        cnoFlagKeyUltProgr = MarshalByte.readChar(buffer, position);
    }

    public byte[] getCompNumOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, cnoCodCompagniaAnia, Len.Int.CNO_COD_COMPAGNIA_ANIA, 0);
        position += Len.CNO_COD_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, cnoFormaAssicurativa, Len.CNO_FORMA_ASSICURATIVA);
        position += Len.CNO_FORMA_ASSICURATIVA;
        MarshalByte.writeString(buffer, position, cnoCodOggetto, Len.CNO_COD_OGGETTO);
        position += Len.CNO_COD_OGGETTO;
        MarshalByte.writeIntAsPacked(buffer, position, cnoPosizione, Len.Int.CNO_POSIZIONE, 0);
        position += Len.CNO_POSIZIONE;
        MarshalByte.writeString(buffer, position, cnoCodStrDato, Len.CNO_COD_STR_DATO);
        position += Len.CNO_COD_STR_DATO;
        MarshalByte.writeString(buffer, position, cnoCodDato, Len.CNO_COD_DATO);
        position += Len.CNO_COD_DATO;
        MarshalByte.writeString(buffer, position, cnoValoreDefault, Len.CNO_VALORE_DEFAULT);
        position += Len.CNO_VALORE_DEFAULT;
        cnoLunghezzaDato.getCnoLunghezzaDatoAsBuffer(buffer, position);
        position += CnoLunghezzaDato.Len.CNO_LUNGHEZZA_DATO;
        MarshalByte.writeChar(buffer, position, cnoFlagKeyUltProgr);
        return buffer;
    }

    public void setCnoCodCompagniaAnia(int cnoCodCompagniaAnia) {
        this.cnoCodCompagniaAnia = cnoCodCompagniaAnia;
    }

    public int getCnoCodCompagniaAnia() {
        return this.cnoCodCompagniaAnia;
    }

    public void setCnoFormaAssicurativa(String cnoFormaAssicurativa) {
        this.cnoFormaAssicurativa = Functions.subString(cnoFormaAssicurativa, Len.CNO_FORMA_ASSICURATIVA);
    }

    public String getCnoFormaAssicurativa() {
        return this.cnoFormaAssicurativa;
    }

    public void setCnoCodOggetto(String cnoCodOggetto) {
        this.cnoCodOggetto = Functions.subString(cnoCodOggetto, Len.CNO_COD_OGGETTO);
    }

    public String getCnoCodOggetto() {
        return this.cnoCodOggetto;
    }

    public void setCnoPosizione(int cnoPosizione) {
        this.cnoPosizione = cnoPosizione;
    }

    public int getCnoPosizione() {
        return this.cnoPosizione;
    }

    public void setCnoCodStrDato(String cnoCodStrDato) {
        this.cnoCodStrDato = Functions.subString(cnoCodStrDato, Len.CNO_COD_STR_DATO);
    }

    public String getCnoCodStrDato() {
        return this.cnoCodStrDato;
    }

    public void setCnoCodDato(String cnoCodDato) {
        this.cnoCodDato = Functions.subString(cnoCodDato, Len.CNO_COD_DATO);
    }

    public String getCnoCodDato() {
        return this.cnoCodDato;
    }

    public void setCnoValoreDefault(String cnoValoreDefault) {
        this.cnoValoreDefault = Functions.subString(cnoValoreDefault, Len.CNO_VALORE_DEFAULT);
    }

    public String getCnoValoreDefault() {
        return this.cnoValoreDefault;
    }

    public void setCnoFlagKeyUltProgr(char cnoFlagKeyUltProgr) {
        this.cnoFlagKeyUltProgr = cnoFlagKeyUltProgr;
    }

    public char getCnoFlagKeyUltProgr() {
        return this.cnoFlagKeyUltProgr;
    }

    public CnoLunghezzaDato getCnoLunghezzaDato() {
        return cnoLunghezzaDato;
    }

    @Override
    public int getCodCompagniaAnia() {
        throw new FieldNotMappedException("codCompagniaAnia");
    }

    @Override
    public void setCodCompagniaAnia(int codCompagniaAnia) {
        throw new FieldNotMappedException("codCompagniaAnia");
    }

    @Override
    public String getCodDato() {
        throw new FieldNotMappedException("codDato");
    }

    @Override
    public void setCodDato(String codDato) {
        throw new FieldNotMappedException("codDato");
    }

    @Override
    public String getCodDatoObj() {
        return getCodDato();
    }

    @Override
    public void setCodDatoObj(String codDatoObj) {
        setCodDato(codDatoObj);
    }

    @Override
    public String getCodOggetto() {
        throw new FieldNotMappedException("codOggetto");
    }

    @Override
    public void setCodOggetto(String codOggetto) {
        throw new FieldNotMappedException("codOggetto");
    }

    @Override
    public String getCodStrDato() {
        throw new FieldNotMappedException("codStrDato");
    }

    @Override
    public void setCodStrDato(String codStrDato) {
        throw new FieldNotMappedException("codStrDato");
    }

    @Override
    public String getCodStrDatoObj() {
        return getCodStrDato();
    }

    @Override
    public void setCodStrDatoObj(String codStrDatoObj) {
        setCodStrDato(codStrDatoObj);
    }

    @Override
    public char getFlagKeyUltProgr() {
        throw new FieldNotMappedException("flagKeyUltProgr");
    }

    @Override
    public void setFlagKeyUltProgr(char flagKeyUltProgr) {
        throw new FieldNotMappedException("flagKeyUltProgr");
    }

    @Override
    public Character getFlagKeyUltProgrObj() {
        return ((Character)getFlagKeyUltProgr());
    }

    @Override
    public void setFlagKeyUltProgrObj(Character flagKeyUltProgrObj) {
        setFlagKeyUltProgr(((char)flagKeyUltProgrObj));
    }

    @Override
    public String getFormaAssicurativa() {
        throw new FieldNotMappedException("formaAssicurativa");
    }

    @Override
    public void setFormaAssicurativa(String formaAssicurativa) {
        throw new FieldNotMappedException("formaAssicurativa");
    }

    @Override
    public int getLunghezzaDato() {
        throw new FieldNotMappedException("lunghezzaDato");
    }

    @Override
    public void setLunghezzaDato(int lunghezzaDato) {
        throw new FieldNotMappedException("lunghezzaDato");
    }

    @Override
    public Integer getLunghezzaDatoObj() {
        return ((Integer)getLunghezzaDato());
    }

    @Override
    public void setLunghezzaDatoObj(Integer lunghezzaDatoObj) {
        setLunghezzaDato(((int)lunghezzaDatoObj));
    }

    @Override
    public int getPosizione() {
        throw new FieldNotMappedException("posizione");
    }

    @Override
    public void setPosizione(int posizione) {
        throw new FieldNotMappedException("posizione");
    }

    @Override
    public String getValoreDefault() {
        throw new FieldNotMappedException("valoreDefault");
    }

    @Override
    public void setValoreDefault(String valoreDefault) {
        throw new FieldNotMappedException("valoreDefault");
    }

    @Override
    public String getValoreDefaultObj() {
        return getValoreDefault();
    }

    @Override
    public void setValoreDefaultObj(String valoreDefaultObj) {
        setValoreDefault(valoreDefaultObj);
    }

    @Override
    public byte[] serialize() {
        return getCompNumOggBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CNO_COD_COMPAGNIA_ANIA = 3;
        public static final int CNO_FORMA_ASSICURATIVA = 2;
        public static final int CNO_COD_OGGETTO = 30;
        public static final int CNO_POSIZIONE = 3;
        public static final int CNO_COD_STR_DATO = 30;
        public static final int CNO_COD_DATO = 30;
        public static final int CNO_VALORE_DEFAULT = 50;
        public static final int CNO_FLAG_KEY_ULT_PROGR = 1;
        public static final int COMP_NUM_OGG = CNO_COD_COMPAGNIA_ANIA + CNO_FORMA_ASSICURATIVA + CNO_COD_OGGETTO + CNO_POSIZIONE + CNO_COD_STR_DATO + CNO_COD_DATO + CNO_VALORE_DEFAULT + CnoLunghezzaDato.Len.CNO_LUNGHEZZA_DATO + CNO_FLAG_KEY_ULT_PROGR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CNO_COD_COMPAGNIA_ANIA = 5;
            public static final int CNO_POSIZIONE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
