package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: CAO-TP-MOVI<br>
 * Variable: CAO-TP-MOVI from program IVVS0212<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CaoTpMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public CaoTpMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.CAO_TP_MOVI;
    }

    public void setCaoTpMovi(int caoTpMovi) {
        writeIntAsPacked(Pos.CAO_TP_MOVI, caoTpMovi, Len.Int.CAO_TP_MOVI);
    }

    /**Original name: CAO-TP-MOVI<br>*/
    public int getCaoTpMovi() {
        return readPackedAsInt(Pos.CAO_TP_MOVI, Len.Int.CAO_TP_MOVI);
    }

    public void setCaoTpMoviNull(String caoTpMoviNull) {
        writeString(Pos.CAO_TP_MOVI_NULL, caoTpMoviNull, Len.CAO_TP_MOVI_NULL);
    }

    /**Original name: CAO-TP-MOVI-NULL<br>*/
    public String getCaoTpMoviNull() {
        return readString(Pos.CAO_TP_MOVI_NULL, Len.CAO_TP_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int CAO_TP_MOVI = 1;
        public static final int CAO_TP_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int CAO_TP_MOVI = 3;
        public static final int CAO_TP_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CAO_TP_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
