package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-IMP-VOLO<br>
 * Variable: DAD-IMP-VOLO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_IMP_VOLO;
    }

    public void setDadImpVolo(AfDecimal dadImpVolo) {
        writeDecimalAsPacked(Pos.DAD_IMP_VOLO, dadImpVolo.copy());
    }

    public void setDadImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_IMP_VOLO, Pos.DAD_IMP_VOLO);
    }

    /**Original name: DAD-IMP-VOLO<br>*/
    public AfDecimal getDadImpVolo() {
        return readPackedAsDecimal(Pos.DAD_IMP_VOLO, Len.Int.DAD_IMP_VOLO, Len.Fract.DAD_IMP_VOLO);
    }

    public byte[] getDadImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_IMP_VOLO, Pos.DAD_IMP_VOLO);
        return buffer;
    }

    public void setDadImpVoloNull(String dadImpVoloNull) {
        writeString(Pos.DAD_IMP_VOLO_NULL, dadImpVoloNull, Len.DAD_IMP_VOLO_NULL);
    }

    /**Original name: DAD-IMP-VOLO-NULL<br>*/
    public String getDadImpVoloNull() {
        return readString(Pos.DAD_IMP_VOLO_NULL, Len.DAD_IMP_VOLO_NULL);
    }

    public String getDadImpVoloNullFormatted() {
        return Functions.padBlanks(getDadImpVoloNull(), Len.DAD_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_IMP_VOLO = 1;
        public static final int DAD_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_IMP_VOLO = 8;
        public static final int DAD_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DAD_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
