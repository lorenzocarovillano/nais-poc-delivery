package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-COMMIS-DI-TRASFE<br>
 * Variable: WDFA-COMMIS-DI-TRASFE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaCommisDiTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaCommisDiTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_COMMIS_DI_TRASFE;
    }

    public void setWdfaCommisDiTrasfe(AfDecimal wdfaCommisDiTrasfe) {
        writeDecimalAsPacked(Pos.WDFA_COMMIS_DI_TRASFE, wdfaCommisDiTrasfe.copy());
    }

    public void setWdfaCommisDiTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_COMMIS_DI_TRASFE, Pos.WDFA_COMMIS_DI_TRASFE);
    }

    /**Original name: WDFA-COMMIS-DI-TRASFE<br>*/
    public AfDecimal getWdfaCommisDiTrasfe() {
        return readPackedAsDecimal(Pos.WDFA_COMMIS_DI_TRASFE, Len.Int.WDFA_COMMIS_DI_TRASFE, Len.Fract.WDFA_COMMIS_DI_TRASFE);
    }

    public byte[] getWdfaCommisDiTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_COMMIS_DI_TRASFE, Pos.WDFA_COMMIS_DI_TRASFE);
        return buffer;
    }

    public void setWdfaCommisDiTrasfeNull(String wdfaCommisDiTrasfeNull) {
        writeString(Pos.WDFA_COMMIS_DI_TRASFE_NULL, wdfaCommisDiTrasfeNull, Len.WDFA_COMMIS_DI_TRASFE_NULL);
    }

    /**Original name: WDFA-COMMIS-DI-TRASFE-NULL<br>*/
    public String getWdfaCommisDiTrasfeNull() {
        return readString(Pos.WDFA_COMMIS_DI_TRASFE_NULL, Len.WDFA_COMMIS_DI_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_COMMIS_DI_TRASFE = 1;
        public static final int WDFA_COMMIS_DI_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_COMMIS_DI_TRASFE = 8;
        public static final int WDFA_COMMIS_DI_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_COMMIS_DI_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_COMMIS_DI_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
