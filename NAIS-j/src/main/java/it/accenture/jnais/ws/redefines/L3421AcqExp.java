package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-ACQ-EXP<br>
 * Variable: L3421-ACQ-EXP from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421AcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421AcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_ACQ_EXP;
    }

    public void setL3421AcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_ACQ_EXP, Pos.L3421_ACQ_EXP);
    }

    /**Original name: L3421-ACQ-EXP<br>*/
    public AfDecimal getL3421AcqExp() {
        return readPackedAsDecimal(Pos.L3421_ACQ_EXP, Len.Int.L3421_ACQ_EXP, Len.Fract.L3421_ACQ_EXP);
    }

    public byte[] getL3421AcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_ACQ_EXP, Pos.L3421_ACQ_EXP);
        return buffer;
    }

    /**Original name: L3421-ACQ-EXP-NULL<br>*/
    public String getL3421AcqExpNull() {
        return readString(Pos.L3421_ACQ_EXP_NULL, Len.L3421_ACQ_EXP_NULL);
    }

    public String getL3421AcqExpNullFormatted() {
        return Functions.padBlanks(getL3421AcqExpNull(), Len.L3421_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_ACQ_EXP = 1;
        public static final int L3421_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_ACQ_EXP = 8;
        public static final int L3421_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
