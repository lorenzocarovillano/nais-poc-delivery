package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;

/**Original name: TRACCIATO-FILE-GRAV<br>
 * Variable: TRACCIATO-FILE-GRAV from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TracciatoFileGrav {

    //==== PROPERTIES ====
    //Original name: IN-NUM-CNTRL
    private String numCntrl = DefaultValues.stringVal(Len.NUM_CNTRL);
    //Original name: FILLER-TRACCIATO-FILE-GRAV
    private char flr1 = DefaultValues.CHAR_VAL;
    //Original name: IN-GRAV-CNTRL
    private String gravCntrl = DefaultValues.stringVal(Len.GRAV_CNTRL);
    //Original name: FILLER-TRACCIATO-FILE-GRAV-1
    private char flr2 = DefaultValues.CHAR_VAL;
    //Original name: IN-TP-CNTRL
    private char tpCntrl = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setTracciatoFileGravFormatted(String data) {
        byte[] buffer = new byte[Len.TRACCIATO_FILE_GRAV];
        MarshalByte.writeString(buffer, 1, data, Len.TRACCIATO_FILE_GRAV);
        setTracciatoFileGravBytes(buffer, 1);
    }

    public void setTracciatoFileGravBytes(byte[] buffer, int offset) {
        int position = offset;
        numCntrl = MarshalByte.readFixedString(buffer, position, Len.NUM_CNTRL);
        position += Len.NUM_CNTRL;
        flr1 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        gravCntrl = MarshalByte.readFixedString(buffer, position, Len.GRAV_CNTRL);
        position += Len.GRAV_CNTRL;
        flr2 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpCntrl = MarshalByte.readChar(buffer, position);
    }

    public String getNumCntrlFormatted() {
        return this.numCntrl;
    }

    public void setFlr1(char flr1) {
        this.flr1 = flr1;
    }

    public char getFlr1() {
        return this.flr1;
    }

    public String getGravCntrlFormatted() {
        return this.gravCntrl;
    }

    public void setFlr2(char flr2) {
        this.flr2 = flr2;
    }

    public char getFlr2() {
        return this.flr2;
    }

    public void setTpCntrl(char tpCntrl) {
        this.tpCntrl = tpCntrl;
    }

    public char getTpCntrl() {
        return this.tpCntrl;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NUM_CNTRL = 9;
        public static final int GRAV_CNTRL = 9;
        public static final int FLR1 = 1;
        public static final int TP_CNTRL = 1;
        public static final int TRACCIATO_FILE_GRAV = NUM_CNTRL + GRAV_CNTRL + TP_CNTRL + 2 * FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
