package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-IMP-RAT-REVOLVING<br>
 * Variable: WP67-IMP-RAT-REVOLVING from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67ImpRatRevolving extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67ImpRatRevolving() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_IMP_RAT_REVOLVING;
    }

    public void setWp67ImpRatRevolving(AfDecimal wp67ImpRatRevolving) {
        writeDecimalAsPacked(Pos.WP67_IMP_RAT_REVOLVING, wp67ImpRatRevolving.copy());
    }

    public void setWp67ImpRatRevolvingFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_IMP_RAT_REVOLVING, Pos.WP67_IMP_RAT_REVOLVING);
    }

    /**Original name: WP67-IMP-RAT-REVOLVING<br>*/
    public AfDecimal getWp67ImpRatRevolving() {
        return readPackedAsDecimal(Pos.WP67_IMP_RAT_REVOLVING, Len.Int.WP67_IMP_RAT_REVOLVING, Len.Fract.WP67_IMP_RAT_REVOLVING);
    }

    public byte[] getWp67ImpRatRevolvingAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_IMP_RAT_REVOLVING, Pos.WP67_IMP_RAT_REVOLVING);
        return buffer;
    }

    public void setWp67ImpRatRevolvingNull(String wp67ImpRatRevolvingNull) {
        writeString(Pos.WP67_IMP_RAT_REVOLVING_NULL, wp67ImpRatRevolvingNull, Len.WP67_IMP_RAT_REVOLVING_NULL);
    }

    /**Original name: WP67-IMP-RAT-REVOLVING-NULL<br>*/
    public String getWp67ImpRatRevolvingNull() {
        return readString(Pos.WP67_IMP_RAT_REVOLVING_NULL, Len.WP67_IMP_RAT_REVOLVING_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_IMP_RAT_REVOLVING = 1;
        public static final int WP67_IMP_RAT_REVOLVING_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_IMP_RAT_REVOLVING = 8;
        public static final int WP67_IMP_RAT_REVOLVING_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_IMP_RAT_REVOLVING = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_IMP_RAT_REVOLVING = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
