package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-MM-CNBZ-K1<br>
 * Variable: DFA-MM-CNBZ-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaMmCnbzK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaMmCnbzK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_MM_CNBZ_K1;
    }

    public void setDfaMmCnbzK1(short dfaMmCnbzK1) {
        writeShortAsPacked(Pos.DFA_MM_CNBZ_K1, dfaMmCnbzK1, Len.Int.DFA_MM_CNBZ_K1);
    }

    public void setDfaMmCnbzK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_MM_CNBZ_K1, Pos.DFA_MM_CNBZ_K1);
    }

    /**Original name: DFA-MM-CNBZ-K1<br>*/
    public short getDfaMmCnbzK1() {
        return readPackedAsShort(Pos.DFA_MM_CNBZ_K1, Len.Int.DFA_MM_CNBZ_K1);
    }

    public byte[] getDfaMmCnbzK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_MM_CNBZ_K1, Pos.DFA_MM_CNBZ_K1);
        return buffer;
    }

    public void setDfaMmCnbzK1Null(String dfaMmCnbzK1Null) {
        writeString(Pos.DFA_MM_CNBZ_K1_NULL, dfaMmCnbzK1Null, Len.DFA_MM_CNBZ_K1_NULL);
    }

    /**Original name: DFA-MM-CNBZ-K1-NULL<br>*/
    public String getDfaMmCnbzK1Null() {
        return readString(Pos.DFA_MM_CNBZ_K1_NULL, Len.DFA_MM_CNBZ_K1_NULL);
    }

    public String getDfaMmCnbzK1NullFormatted() {
        return Functions.padBlanks(getDfaMmCnbzK1Null(), Len.DFA_MM_CNBZ_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_MM_CNBZ_K1 = 1;
        public static final int DFA_MM_CNBZ_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_MM_CNBZ_K1 = 3;
        public static final int DFA_MM_CNBZ_K1_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_MM_CNBZ_K1 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
