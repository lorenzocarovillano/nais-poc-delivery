package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WVAS-ID-RICH-DIS-FND<br>
 * Variable: WVAS-ID-RICH-DIS-FND from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasIdRichDisFnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasIdRichDisFnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_ID_RICH_DIS_FND;
    }

    public void setWvasIdRichDisFnd(int wvasIdRichDisFnd) {
        writeIntAsPacked(Pos.WVAS_ID_RICH_DIS_FND, wvasIdRichDisFnd, Len.Int.WVAS_ID_RICH_DIS_FND);
    }

    /**Original name: WVAS-ID-RICH-DIS-FND<br>*/
    public int getWvasIdRichDisFnd() {
        return readPackedAsInt(Pos.WVAS_ID_RICH_DIS_FND, Len.Int.WVAS_ID_RICH_DIS_FND);
    }

    public void setWvasIdRichDisFndNull(String wvasIdRichDisFndNull) {
        writeString(Pos.WVAS_ID_RICH_DIS_FND_NULL, wvasIdRichDisFndNull, Len.WVAS_ID_RICH_DIS_FND_NULL);
    }

    /**Original name: WVAS-ID-RICH-DIS-FND-NULL<br>*/
    public String getWvasIdRichDisFndNull() {
        return readString(Pos.WVAS_ID_RICH_DIS_FND_NULL, Len.WVAS_ID_RICH_DIS_FND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_ID_RICH_DIS_FND = 1;
        public static final int WVAS_ID_RICH_DIS_FND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_ID_RICH_DIS_FND = 5;
        public static final int WVAS_ID_RICH_DIS_FND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_ID_RICH_DIS_FND = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
