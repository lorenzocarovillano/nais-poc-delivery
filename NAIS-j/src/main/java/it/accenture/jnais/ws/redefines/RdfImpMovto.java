package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RDF-IMP-MOVTO<br>
 * Variable: RDF-IMP-MOVTO from program IDBSRDF0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RdfImpMovto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RdfImpMovto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RDF_IMP_MOVTO;
    }

    public void setRdfImpMovto(AfDecimal rdfImpMovto) {
        writeDecimalAsPacked(Pos.RDF_IMP_MOVTO, rdfImpMovto.copy());
    }

    public void setRdfImpMovtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RDF_IMP_MOVTO, Pos.RDF_IMP_MOVTO);
    }

    /**Original name: RDF-IMP-MOVTO<br>*/
    public AfDecimal getRdfImpMovto() {
        return readPackedAsDecimal(Pos.RDF_IMP_MOVTO, Len.Int.RDF_IMP_MOVTO, Len.Fract.RDF_IMP_MOVTO);
    }

    public byte[] getRdfImpMovtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RDF_IMP_MOVTO, Pos.RDF_IMP_MOVTO);
        return buffer;
    }

    public void setRdfImpMovtoNull(String rdfImpMovtoNull) {
        writeString(Pos.RDF_IMP_MOVTO_NULL, rdfImpMovtoNull, Len.RDF_IMP_MOVTO_NULL);
    }

    /**Original name: RDF-IMP-MOVTO-NULL<br>*/
    public String getRdfImpMovtoNull() {
        return readString(Pos.RDF_IMP_MOVTO_NULL, Len.RDF_IMP_MOVTO_NULL);
    }

    public String getRdfImpMovtoNullFormatted() {
        return Functions.padBlanks(getRdfImpMovtoNull(), Len.RDF_IMP_MOVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RDF_IMP_MOVTO = 1;
        public static final int RDF_IMP_MOVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_IMP_MOVTO = 8;
        public static final int RDF_IMP_MOVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_IMP_MOVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RDF_IMP_MOVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
