package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3500<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3500Data {

    //==== PROPERTIES ====
    //Original name: WK-PGM-CALL
    private String wkPgmCall = DefaultValues.stringVal(Len.WK_PGM_CALL);
    //Original name: WK-CALL-PGM
    private String wkCallPgm = DefaultValues.stringVal(Len.WK_CALL_PGM);
    //Original name: WK-APP-DATA-EFFETTO
    private int wkAppDataEffetto = DefaultValues.INT_VAL;
    //Original name: D-CRIST
    private DCristIdbsp610 dCrist = new DCristIdbsp610();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setWkPgmCall(String wkPgmCall) {
        this.wkPgmCall = Functions.subString(wkPgmCall, Len.WK_PGM_CALL);
    }

    public String getWkPgmCall() {
        return this.wkPgmCall;
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkAppDataEffetto(int wkAppDataEffetto) {
        this.wkAppDataEffetto = wkAppDataEffetto;
    }

    public int getWkAppDataEffetto() {
        return this.wkAppDataEffetto;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public DCristIdbsp610 getdCrist() {
        return dCrist;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_PGM_CALL = 8;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
