package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import org.apache.commons.lang3.ArrayUtils;

/**Original name: IDSV0003-OPERAZIONE<br>
 * Variable: IDSV0003-OPERAZIONE from copybook IDSV0003<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0003Operazione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.OPERAZIONE);
    private static final String[] SELECT = new String[] {"SELECT", "SE"};
    public static final String AGGIORNAMENTO_STORICO = "AGG_STORICO";
    public static final String AGG_STORICO_SOLO_INS = "AGG_SOLO_INSERT";
    private static final String[] INSERT = new String[] {"INSERT", "IN"};
    private static final String[] UPDATE = new String[] {"UPDATE", "UP"};
    private static final String[] DELETE = new String[] {"DELETE", "DF"};
    private static final String[] DELETE_LOGICA = new String[] {"DELETE_LOGICA", "DL"};
    private static final String[] OPEN_CURSOR = new String[] {"OPEN_CURSOR", "OP"};
    private static final String[] CLOSE_CURSOR = new String[] {"CLOSE_CURSOR", "CL"};
    public static final String FETCH_FIRST = "FF";
    private static final String[] FETCH_NEXT = new String[] {"FETCH", "FN"};
    public static final String FETCH_FIRST_MULTIPLE = "FFM";
    public static final String FETCH_NEXT_MULTIPLE = "FNM";
    public static final String PRESA_IN_CARICO = "PRESA_IN_CARICO";

    //==== METHODS ====
    public void setOperazione(String operazione) {
        this.value = Functions.subString(operazione, Len.OPERAZIONE);
    }

    public String getOperazione() {
        return this.value;
    }

    public String getOperazioneFormatted() {
        return Functions.padBlanks(getOperazione(), Len.OPERAZIONE);
    }

    public boolean isSelect() {
        return ArrayUtils.contains(SELECT, value);
    }

    public void setSelect() {
        value = SELECT[0];
    }

    public boolean isAggiornamentoStorico() {
        return value.equals(AGGIORNAMENTO_STORICO);
    }

    public void setIdsi0011AggiornamentoStorico() {
        value = AGGIORNAMENTO_STORICO;
    }

    public boolean isAggStoricoSoloIns() {
        return value.equals(AGG_STORICO_SOLO_INS);
    }

    public void setIdsi0011AggStoricoSoloIns() {
        value = AGG_STORICO_SOLO_INS;
    }

    public boolean isInsert() {
        return ArrayUtils.contains(INSERT, value);
    }

    public void setInsert() {
        value = INSERT[0];
    }

    public boolean isUpdate() {
        return ArrayUtils.contains(UPDATE, value);
    }

    public void setIdsi0011Update() {
        value = UPDATE[0];
    }

    public boolean isDelete() {
        return ArrayUtils.contains(DELETE, value);
    }

    public void setIdsi0011Delete() {
        value = DELETE[0];
    }

    public boolean isDeleteLogica() {
        return ArrayUtils.contains(DELETE_LOGICA, value);
    }

    public void setIdsi0011DeleteLogica() {
        value = DELETE_LOGICA[0];
    }

    public boolean isOpenCursor() {
        return ArrayUtils.contains(OPEN_CURSOR, value);
    }

    public boolean isCloseCursor() {
        return ArrayUtils.contains(CLOSE_CURSOR, value);
    }

    public void setIdsv0003CloseCursor() {
        value = CLOSE_CURSOR[0];
    }

    public boolean isFetchFirst() {
        return value.equals(FETCH_FIRST);
    }

    public void setFetchFirst() {
        value = FETCH_FIRST;
    }

    public boolean isFetchNext() {
        return ArrayUtils.contains(FETCH_NEXT, value);
    }

    public void setFetchNext() {
        value = FETCH_NEXT[0];
    }

    public boolean isFetchFirstMultiple() {
        return value.equals(FETCH_FIRST_MULTIPLE);
    }

    public boolean isFetchNextMultiple() {
        return value.equals(FETCH_NEXT_MULTIPLE);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OPERAZIONE = 15;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
