package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P88-ID-MOVI-CHIU<br>
 * Variable: P88-ID-MOVI-CHIU from program IDBSP880<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P88IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P88IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P88_ID_MOVI_CHIU;
    }

    public void setP88IdMoviChiu(int p88IdMoviChiu) {
        writeIntAsPacked(Pos.P88_ID_MOVI_CHIU, p88IdMoviChiu, Len.Int.P88_ID_MOVI_CHIU);
    }

    public void setP88IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P88_ID_MOVI_CHIU, Pos.P88_ID_MOVI_CHIU);
    }

    /**Original name: P88-ID-MOVI-CHIU<br>*/
    public int getP88IdMoviChiu() {
        return readPackedAsInt(Pos.P88_ID_MOVI_CHIU, Len.Int.P88_ID_MOVI_CHIU);
    }

    public byte[] getP88IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P88_ID_MOVI_CHIU, Pos.P88_ID_MOVI_CHIU);
        return buffer;
    }

    public void setP88IdMoviChiuNull(String p88IdMoviChiuNull) {
        writeString(Pos.P88_ID_MOVI_CHIU_NULL, p88IdMoviChiuNull, Len.P88_ID_MOVI_CHIU_NULL);
    }

    /**Original name: P88-ID-MOVI-CHIU-NULL<br>*/
    public String getP88IdMoviChiuNull() {
        return readString(Pos.P88_ID_MOVI_CHIU_NULL, Len.P88_ID_MOVI_CHIU_NULL);
    }

    public String getP88IdMoviChiuNullFormatted() {
        return Functions.padBlanks(getP88IdMoviChiuNull(), Len.P88_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P88_ID_MOVI_CHIU = 1;
        public static final int P88_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P88_ID_MOVI_CHIU = 5;
        public static final int P88_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P88_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
