package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-SCON<br>
 * Variable: WPAG-IMP-SCON from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_SCON;
    }

    public void setWpagImpScon(AfDecimal wpagImpScon) {
        writeDecimalAsPacked(Pos.WPAG_IMP_SCON, wpagImpScon.copy());
    }

    public void setWpagImpSconFormatted(String wpagImpScon) {
        setWpagImpScon(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_SCON + Len.Fract.WPAG_IMP_SCON, Len.Fract.WPAG_IMP_SCON, wpagImpScon));
    }

    public void setWpagImpSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_SCON, Pos.WPAG_IMP_SCON);
    }

    /**Original name: WPAG-IMP-SCON<br>*/
    public AfDecimal getWpagImpScon() {
        return readPackedAsDecimal(Pos.WPAG_IMP_SCON, Len.Int.WPAG_IMP_SCON, Len.Fract.WPAG_IMP_SCON);
    }

    public byte[] getWpagImpSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_SCON, Pos.WPAG_IMP_SCON);
        return buffer;
    }

    public void initWpagImpSconSpaces() {
        fill(Pos.WPAG_IMP_SCON, Len.WPAG_IMP_SCON, Types.SPACE_CHAR);
    }

    public void setWpagImpSconNull(String wpagImpSconNull) {
        writeString(Pos.WPAG_IMP_SCON_NULL, wpagImpSconNull, Len.WPAG_IMP_SCON_NULL);
    }

    /**Original name: WPAG-IMP-SCON-NULL<br>*/
    public String getWpagImpSconNull() {
        return readString(Pos.WPAG_IMP_SCON_NULL, Len.WPAG_IMP_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SCON = 1;
        public static final int WPAG_IMP_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SCON = 8;
        public static final int WPAG_IMP_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
