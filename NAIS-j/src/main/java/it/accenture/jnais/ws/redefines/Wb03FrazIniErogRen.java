package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-FRAZ-INI-EROG-REN<br>
 * Variable: WB03-FRAZ-INI-EROG-REN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03FrazIniErogRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03FrazIniErogRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_FRAZ_INI_EROG_REN;
    }

    public void setWb03FrazIniErogRen(int wb03FrazIniErogRen) {
        writeIntAsPacked(Pos.WB03_FRAZ_INI_EROG_REN, wb03FrazIniErogRen, Len.Int.WB03_FRAZ_INI_EROG_REN);
    }

    public void setWb03FrazIniErogRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_FRAZ_INI_EROG_REN, Pos.WB03_FRAZ_INI_EROG_REN);
    }

    /**Original name: WB03-FRAZ-INI-EROG-REN<br>*/
    public int getWb03FrazIniErogRen() {
        return readPackedAsInt(Pos.WB03_FRAZ_INI_EROG_REN, Len.Int.WB03_FRAZ_INI_EROG_REN);
    }

    public byte[] getWb03FrazIniErogRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_FRAZ_INI_EROG_REN, Pos.WB03_FRAZ_INI_EROG_REN);
        return buffer;
    }

    public void setWb03FrazIniErogRenNull(String wb03FrazIniErogRenNull) {
        writeString(Pos.WB03_FRAZ_INI_EROG_REN_NULL, wb03FrazIniErogRenNull, Len.WB03_FRAZ_INI_EROG_REN_NULL);
    }

    /**Original name: WB03-FRAZ-INI-EROG-REN-NULL<br>*/
    public String getWb03FrazIniErogRenNull() {
        return readString(Pos.WB03_FRAZ_INI_EROG_REN_NULL, Len.WB03_FRAZ_INI_EROG_REN_NULL);
    }

    public String getWb03FrazIniErogRenNullFormatted() {
        return Functions.padBlanks(getWb03FrazIniErogRenNull(), Len.WB03_FRAZ_INI_EROG_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_FRAZ_INI_EROG_REN = 1;
        public static final int WB03_FRAZ_INI_EROG_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_FRAZ_INI_EROG_REN = 3;
        public static final int WB03_FRAZ_INI_EROG_REN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_FRAZ_INI_EROG_REN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
