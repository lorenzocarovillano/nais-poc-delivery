package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WallTabAssetAll;

/**Original name: WALL-AREA-ASSET<br>
 * Variable: WALL-AREA-ASSET from program LVES0268<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WallAreaAssetLves0268 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_ASSET_ALL_MAXOCCURS = 220;
    /**Original name: WALL-ELE-ASSET-ALL-MAX<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY 7 PER LA GESTIONE DELLE OCCURS
	 * ----------------------------------------------------------------*</pre>*/
    private short eleAssetAllMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WALL-TAB-ASSET-ALL
    private WallTabAssetAll[] tabAssetAll = new WallTabAssetAll[TAB_ASSET_ALL_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WallAreaAssetLves0268() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WALL_AREA_ASSET;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWallAreaAssetBytes(buf);
    }

    public void init() {
        for (int tabAssetAllIdx = 1; tabAssetAllIdx <= TAB_ASSET_ALL_MAXOCCURS; tabAssetAllIdx++) {
            tabAssetAll[tabAssetAllIdx - 1] = new WallTabAssetAll();
        }
    }

    public String getWallAreaAssetFormatted() {
        return MarshalByteExt.bufferToStr(getWallAreaAssetBytes());
    }

    public void setWallAreaAssetBytes(byte[] buffer) {
        setWallAreaAssetBytes(buffer, 1);
    }

    public byte[] getWallAreaAssetBytes() {
        byte[] buffer = new byte[Len.WALL_AREA_ASSET];
        return getWallAreaAssetBytes(buffer, 1);
    }

    public void setWallAreaAssetBytes(byte[] buffer, int offset) {
        int position = offset;
        eleAssetAllMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setTabellaBytes(buffer, position);
    }

    public byte[] getWallAreaAssetBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleAssetAllMax);
        position += Types.SHORT_SIZE;
        getTabellaBytes(buffer, position);
        return buffer;
    }

    public void setEleAssetAllMax(short eleAssetAllMax) {
        this.eleAssetAllMax = eleAssetAllMax;
    }

    public short getEleAssetAllMax() {
        return this.eleAssetAllMax;
    }

    public void setTabellaBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_ASSET_ALL_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabAssetAll[idx - 1].setTabAssetAllBytes(buffer, position);
                position += WallTabAssetAll.Len.TAB_ASSET_ALL;
            }
            else {
                tabAssetAll[idx - 1].initTabAssetAllSpaces();
                position += WallTabAssetAll.Len.TAB_ASSET_ALL;
            }
        }
    }

    public byte[] getTabellaBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_ASSET_ALL_MAXOCCURS; idx++) {
            tabAssetAll[idx - 1].getTabAssetAllBytes(buffer, position);
            position += WallTabAssetAll.Len.TAB_ASSET_ALL;
        }
        return buffer;
    }

    public WallTabAssetAll getTabAssetAll(int idx) {
        return tabAssetAll[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWallAreaAssetBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_ASSET_ALL_MAX = 2;
        public static final int TABELLA = WallAreaAssetLves0268.TAB_ASSET_ALL_MAXOCCURS * WallTabAssetAll.Len.TAB_ASSET_ALL;
        public static final int WALL_AREA_ASSET = ELE_ASSET_ALL_MAX + TABELLA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
