package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndImpstSost;
import it.accenture.jnais.copy.Ldbv6191;
import it.accenture.jnais.copy.RichDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS6190<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs6190Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-IMPST-SOST
    private IndImpstSost indImpstSost = new IndImpstSost();
    //Original name: IMPST-SOST-DB
    private RichDb impstSostDb = new RichDb();
    //Original name: LDBV6191
    private Ldbv6191 ldbv6191 = new Ldbv6191();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public RichDb getImpstSostDb() {
        return impstSostDb;
    }

    public IndImpstSost getIndImpstSost() {
        return indImpstSost;
    }

    public Ldbv6191 getLdbv6191() {
        return ldbv6191;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
