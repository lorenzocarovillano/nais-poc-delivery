package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-PRE-TOT<br>
 * Variable: TIT-TOT-PRE-TOT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotPreTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotPreTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_PRE_TOT;
    }

    public void setTitTotPreTot(AfDecimal titTotPreTot) {
        writeDecimalAsPacked(Pos.TIT_TOT_PRE_TOT, titTotPreTot.copy());
    }

    public void setTitTotPreTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_PRE_TOT, Pos.TIT_TOT_PRE_TOT);
    }

    /**Original name: TIT-TOT-PRE-TOT<br>*/
    public AfDecimal getTitTotPreTot() {
        return readPackedAsDecimal(Pos.TIT_TOT_PRE_TOT, Len.Int.TIT_TOT_PRE_TOT, Len.Fract.TIT_TOT_PRE_TOT);
    }

    public byte[] getTitTotPreTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_PRE_TOT, Pos.TIT_TOT_PRE_TOT);
        return buffer;
    }

    public void setTitTotPreTotNull(String titTotPreTotNull) {
        writeString(Pos.TIT_TOT_PRE_TOT_NULL, titTotPreTotNull, Len.TIT_TOT_PRE_TOT_NULL);
    }

    /**Original name: TIT-TOT-PRE-TOT-NULL<br>*/
    public String getTitTotPreTotNull() {
        return readString(Pos.TIT_TOT_PRE_TOT_NULL, Len.TIT_TOT_PRE_TOT_NULL);
    }

    public String getTitTotPreTotNullFormatted() {
        return Functions.padBlanks(getTitTotPreTotNull(), Len.TIT_TOT_PRE_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PRE_TOT = 1;
        public static final int TIT_TOT_PRE_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PRE_TOT = 8;
        public static final int TIT_TOT_PRE_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
