package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-STAT-BLOCCO<br>
 * Variable: WS-TP-STAT-BLOCCO from program LCCS0024<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpStatBlocco {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WS_TP_STAT_BLOCCO);
    public static final String ATTIVO = "AT";
    public static final String NON_ATTIVO = "NA";

    //==== METHODS ====
    public void setWsTpStatBlocco(String wsTpStatBlocco) {
        this.value = Functions.subString(wsTpStatBlocco, Len.WS_TP_STAT_BLOCCO);
    }

    public String getWsTpStatBlocco() {
        return this.value;
    }

    public void setAttivo() {
        value = ATTIVO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_STAT_BLOCCO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
