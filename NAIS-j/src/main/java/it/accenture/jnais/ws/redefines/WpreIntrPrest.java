package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPRE-INTR-PREST<br>
 * Variable: WPRE-INTR-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpreIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_INTR_PREST;
    }

    public void setWpreIntrPrest(AfDecimal wpreIntrPrest) {
        writeDecimalAsPacked(Pos.WPRE_INTR_PREST, wpreIntrPrest.copy());
    }

    public void setWpreIntrPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_INTR_PREST, Pos.WPRE_INTR_PREST);
    }

    /**Original name: WPRE-INTR-PREST<br>*/
    public AfDecimal getWpreIntrPrest() {
        return readPackedAsDecimal(Pos.WPRE_INTR_PREST, Len.Int.WPRE_INTR_PREST, Len.Fract.WPRE_INTR_PREST);
    }

    public byte[] getWpreIntrPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_INTR_PREST, Pos.WPRE_INTR_PREST);
        return buffer;
    }

    public void initWpreIntrPrestSpaces() {
        fill(Pos.WPRE_INTR_PREST, Len.WPRE_INTR_PREST, Types.SPACE_CHAR);
    }

    public void setWpreIntrPrestNull(String wpreIntrPrestNull) {
        writeString(Pos.WPRE_INTR_PREST_NULL, wpreIntrPrestNull, Len.WPRE_INTR_PREST_NULL);
    }

    /**Original name: WPRE-INTR-PREST-NULL<br>*/
    public String getWpreIntrPrestNull() {
        return readString(Pos.WPRE_INTR_PREST_NULL, Len.WPRE_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_INTR_PREST = 1;
        public static final int WPRE_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_INTR_PREST = 4;
        public static final int WPRE_INTR_PREST_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPRE_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
