package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP61-IMPB-VIS-RP-P62014<br>
 * Variable: WP61-IMPB-VIS-RP-P62014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp61ImpbVisRpP62014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp61ImpbVisRpP62014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP61_IMPB_VIS_RP_P62014;
    }

    public void setWp61ImpbVisRpP62014(AfDecimal wp61ImpbVisRpP62014) {
        writeDecimalAsPacked(Pos.WP61_IMPB_VIS_RP_P62014, wp61ImpbVisRpP62014.copy());
    }

    public void setWp61ImpbVisRpP62014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP61_IMPB_VIS_RP_P62014, Pos.WP61_IMPB_VIS_RP_P62014);
    }

    /**Original name: WP61-IMPB-VIS-RP-P62014<br>*/
    public AfDecimal getWp61ImpbVisRpP62014() {
        return readPackedAsDecimal(Pos.WP61_IMPB_VIS_RP_P62014, Len.Int.WP61_IMPB_VIS_RP_P62014, Len.Fract.WP61_IMPB_VIS_RP_P62014);
    }

    public byte[] getWp61ImpbVisRpP62014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP61_IMPB_VIS_RP_P62014, Pos.WP61_IMPB_VIS_RP_P62014);
        return buffer;
    }

    public void setWp61ImpbVisRpP62014Null(String wp61ImpbVisRpP62014Null) {
        writeString(Pos.WP61_IMPB_VIS_RP_P62014_NULL, wp61ImpbVisRpP62014Null, Len.WP61_IMPB_VIS_RP_P62014_NULL);
    }

    /**Original name: WP61-IMPB-VIS-RP-P62014-NULL<br>*/
    public String getWp61ImpbVisRpP62014Null() {
        return readString(Pos.WP61_IMPB_VIS_RP_P62014_NULL, Len.WP61_IMPB_VIS_RP_P62014_NULL);
    }

    public String getDp61ImpbVisRpP62014NullFormatted() {
        return Functions.padBlanks(getWp61ImpbVisRpP62014Null(), Len.WP61_IMPB_VIS_RP_P62014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP61_IMPB_VIS_RP_P62014 = 1;
        public static final int WP61_IMPB_VIS_RP_P62014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_IMPB_VIS_RP_P62014 = 8;
        public static final int WP61_IMPB_VIS_RP_P62014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP61_IMPB_VIS_RP_P62014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_IMPB_VIS_RP_P62014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
