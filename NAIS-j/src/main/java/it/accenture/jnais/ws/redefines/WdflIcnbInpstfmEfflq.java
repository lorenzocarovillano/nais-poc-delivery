package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ICNB-INPSTFM-EFFLQ<br>
 * Variable: WDFL-ICNB-INPSTFM-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIcnbInpstfmEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIcnbInpstfmEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ICNB_INPSTFM_EFFLQ;
    }

    public void setWdflIcnbInpstfmEfflq(AfDecimal wdflIcnbInpstfmEfflq) {
        writeDecimalAsPacked(Pos.WDFL_ICNB_INPSTFM_EFFLQ, wdflIcnbInpstfmEfflq.copy());
    }

    public void setWdflIcnbInpstfmEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ICNB_INPSTFM_EFFLQ, Pos.WDFL_ICNB_INPSTFM_EFFLQ);
    }

    /**Original name: WDFL-ICNB-INPSTFM-EFFLQ<br>*/
    public AfDecimal getWdflIcnbInpstfmEfflq() {
        return readPackedAsDecimal(Pos.WDFL_ICNB_INPSTFM_EFFLQ, Len.Int.WDFL_ICNB_INPSTFM_EFFLQ, Len.Fract.WDFL_ICNB_INPSTFM_EFFLQ);
    }

    public byte[] getWdflIcnbInpstfmEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ICNB_INPSTFM_EFFLQ, Pos.WDFL_ICNB_INPSTFM_EFFLQ);
        return buffer;
    }

    public void setWdflIcnbInpstfmEfflqNull(String wdflIcnbInpstfmEfflqNull) {
        writeString(Pos.WDFL_ICNB_INPSTFM_EFFLQ_NULL, wdflIcnbInpstfmEfflqNull, Len.WDFL_ICNB_INPSTFM_EFFLQ_NULL);
    }

    /**Original name: WDFL-ICNB-INPSTFM-EFFLQ-NULL<br>*/
    public String getWdflIcnbInpstfmEfflqNull() {
        return readString(Pos.WDFL_ICNB_INPSTFM_EFFLQ_NULL, Len.WDFL_ICNB_INPSTFM_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ICNB_INPSTFM_EFFLQ = 1;
        public static final int WDFL_ICNB_INPSTFM_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ICNB_INPSTFM_EFFLQ = 8;
        public static final int WDFL_ICNB_INPSTFM_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ICNB_INPSTFM_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ICNB_INPSTFM_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
