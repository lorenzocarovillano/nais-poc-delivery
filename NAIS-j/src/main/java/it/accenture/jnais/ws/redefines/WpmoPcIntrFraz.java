package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-PC-INTR-FRAZ<br>
 * Variable: WPMO-PC-INTR-FRAZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoPcIntrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoPcIntrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_PC_INTR_FRAZ;
    }

    public void setWpmoPcIntrFraz(AfDecimal wpmoPcIntrFraz) {
        writeDecimalAsPacked(Pos.WPMO_PC_INTR_FRAZ, wpmoPcIntrFraz.copy());
    }

    public void setWpmoPcIntrFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_PC_INTR_FRAZ, Pos.WPMO_PC_INTR_FRAZ);
    }

    /**Original name: WPMO-PC-INTR-FRAZ<br>*/
    public AfDecimal getWpmoPcIntrFraz() {
        return readPackedAsDecimal(Pos.WPMO_PC_INTR_FRAZ, Len.Int.WPMO_PC_INTR_FRAZ, Len.Fract.WPMO_PC_INTR_FRAZ);
    }

    public byte[] getWpmoPcIntrFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_PC_INTR_FRAZ, Pos.WPMO_PC_INTR_FRAZ);
        return buffer;
    }

    public void initWpmoPcIntrFrazSpaces() {
        fill(Pos.WPMO_PC_INTR_FRAZ, Len.WPMO_PC_INTR_FRAZ, Types.SPACE_CHAR);
    }

    public void setWpmoPcIntrFrazNull(String wpmoPcIntrFrazNull) {
        writeString(Pos.WPMO_PC_INTR_FRAZ_NULL, wpmoPcIntrFrazNull, Len.WPMO_PC_INTR_FRAZ_NULL);
    }

    /**Original name: WPMO-PC-INTR-FRAZ-NULL<br>*/
    public String getWpmoPcIntrFrazNull() {
        return readString(Pos.WPMO_PC_INTR_FRAZ_NULL, Len.WPMO_PC_INTR_FRAZ_NULL);
    }

    public String getWpmoPcIntrFrazNullFormatted() {
        return Functions.padBlanks(getWpmoPcIntrFrazNull(), Len.WPMO_PC_INTR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_PC_INTR_FRAZ = 1;
        public static final int WPMO_PC_INTR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_PC_INTR_FRAZ = 4;
        public static final int WPMO_PC_INTR_FRAZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_PC_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_PC_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
