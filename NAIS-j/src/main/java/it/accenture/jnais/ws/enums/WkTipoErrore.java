package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-TIPO-ERRORE<br>
 * Variable: WK-TIPO-ERRORE from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkTipoErrore {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_TIPO_ERRORE);
    public static final String BLOCCANTE = "BL";
    public static final String NON_BLOCCANTE = "NB";
    public static final String FATALE = "FT";

    //==== METHODS ====
    public void setWkTipoErrore(String wkTipoErrore) {
        this.value = Functions.subString(wkTipoErrore, Len.WK_TIPO_ERRORE);
    }

    public String getWkTipoErrore() {
        return this.value;
    }

    public boolean isBloccante() {
        return value.equals(BLOCCANTE);
    }

    public void setBloccante() {
        value = BLOCCANTE;
    }

    public void setNonBloccante() {
        value = NON_BLOCCANTE;
    }

    public boolean isFatale() {
        return value.equals(FATALE);
    }

    public void setFatale() {
        value = FATALE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TIPO_ERRORE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
