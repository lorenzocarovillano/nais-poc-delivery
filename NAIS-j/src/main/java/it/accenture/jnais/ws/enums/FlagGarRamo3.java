package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-GAR-RAMO3<br>
 * Variable: FLAG-GAR-RAMO3 from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagGarRamo3 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagGarRamo3(char flagGarRamo3) {
        this.value = flagGarRamo3;
    }

    public char getFlagGarRamo3() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
