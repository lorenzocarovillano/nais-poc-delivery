package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FL-TIPO-POLIZZA<br>
 * Variable: FL-TIPO-POLIZZA from program LVVS2800<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlTipoPolizza {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char COLLETTIVA = 'C';
    public static final char INDIVIDUALE = 'I';

    //==== METHODS ====
    public void setFlTipoPolizza(char flTipoPolizza) {
        this.value = flTipoPolizza;
    }

    public char getFlTipoPolizza() {
        return this.value;
    }

    public void setCollettiva() {
        value = COLLETTIVA;
    }

    public boolean isIndividuale() {
        return value == INDIVIDUALE;
    }

    public void setIndividuale() {
        value = INDIVIDUALE;
    }
}
