package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-PER-RAT-FINANZ<br>
 * Variable: WP67-PER-RAT-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67PerRatFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67PerRatFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_PER_RAT_FINANZ;
    }

    public void setWp67PerRatFinanz(int wp67PerRatFinanz) {
        writeIntAsPacked(Pos.WP67_PER_RAT_FINANZ, wp67PerRatFinanz, Len.Int.WP67_PER_RAT_FINANZ);
    }

    public void setWp67PerRatFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_PER_RAT_FINANZ, Pos.WP67_PER_RAT_FINANZ);
    }

    /**Original name: WP67-PER-RAT-FINANZ<br>*/
    public int getWp67PerRatFinanz() {
        return readPackedAsInt(Pos.WP67_PER_RAT_FINANZ, Len.Int.WP67_PER_RAT_FINANZ);
    }

    public byte[] getWp67PerRatFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_PER_RAT_FINANZ, Pos.WP67_PER_RAT_FINANZ);
        return buffer;
    }

    public void setWp67PerRatFinanzNull(String wp67PerRatFinanzNull) {
        writeString(Pos.WP67_PER_RAT_FINANZ_NULL, wp67PerRatFinanzNull, Len.WP67_PER_RAT_FINANZ_NULL);
    }

    /**Original name: WP67-PER-RAT-FINANZ-NULL<br>*/
    public String getWp67PerRatFinanzNull() {
        return readString(Pos.WP67_PER_RAT_FINANZ_NULL, Len.WP67_PER_RAT_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_PER_RAT_FINANZ = 1;
        public static final int WP67_PER_RAT_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_PER_RAT_FINANZ = 3;
        public static final int WP67_PER_RAT_FINANZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_PER_RAT_FINANZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
