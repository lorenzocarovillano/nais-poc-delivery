package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-BNS-GIA-LIQTO<br>
 * Variable: L3421-BNS-GIA-LIQTO from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421BnsGiaLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421BnsGiaLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_BNS_GIA_LIQTO;
    }

    public void setL3421BnsGiaLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_BNS_GIA_LIQTO, Pos.L3421_BNS_GIA_LIQTO);
    }

    /**Original name: L3421-BNS-GIA-LIQTO<br>*/
    public AfDecimal getL3421BnsGiaLiqto() {
        return readPackedAsDecimal(Pos.L3421_BNS_GIA_LIQTO, Len.Int.L3421_BNS_GIA_LIQTO, Len.Fract.L3421_BNS_GIA_LIQTO);
    }

    public byte[] getL3421BnsGiaLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_BNS_GIA_LIQTO, Pos.L3421_BNS_GIA_LIQTO);
        return buffer;
    }

    /**Original name: L3421-BNS-GIA-LIQTO-NULL<br>*/
    public String getL3421BnsGiaLiqtoNull() {
        return readString(Pos.L3421_BNS_GIA_LIQTO_NULL, Len.L3421_BNS_GIA_LIQTO_NULL);
    }

    public String getL3421BnsGiaLiqtoNullFormatted() {
        return Functions.padBlanks(getL3421BnsGiaLiqtoNull(), Len.L3421_BNS_GIA_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_BNS_GIA_LIQTO = 1;
        public static final int L3421_BNS_GIA_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_BNS_GIA_LIQTO = 8;
        public static final int L3421_BNS_GIA_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_BNS_GIA_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_BNS_GIA_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
