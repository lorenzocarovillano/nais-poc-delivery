package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-PC-REN-K2<br>
 * Variable: S089-PC-REN-K2 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089PcRenK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089PcRenK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_PC_REN_K2;
    }

    public void setWlquPcRenK2(AfDecimal wlquPcRenK2) {
        writeDecimalAsPacked(Pos.S089_PC_REN_K2, wlquPcRenK2.copy());
    }

    public void setWlquPcRenK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_PC_REN_K2, Pos.S089_PC_REN_K2);
    }

    /**Original name: WLQU-PC-REN-K2<br>*/
    public AfDecimal getWlquPcRenK2() {
        return readPackedAsDecimal(Pos.S089_PC_REN_K2, Len.Int.WLQU_PC_REN_K2, Len.Fract.WLQU_PC_REN_K2);
    }

    public byte[] getWlquPcRenK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_PC_REN_K2, Pos.S089_PC_REN_K2);
        return buffer;
    }

    public void initWlquPcRenK2Spaces() {
        fill(Pos.S089_PC_REN_K2, Len.S089_PC_REN_K2, Types.SPACE_CHAR);
    }

    public void setWlquPcRenK2Null(String wlquPcRenK2Null) {
        writeString(Pos.S089_PC_REN_K2_NULL, wlquPcRenK2Null, Len.WLQU_PC_REN_K2_NULL);
    }

    /**Original name: WLQU-PC-REN-K2-NULL<br>*/
    public String getWlquPcRenK2Null() {
        return readString(Pos.S089_PC_REN_K2_NULL, Len.WLQU_PC_REN_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_PC_REN_K2 = 1;
        public static final int S089_PC_REN_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_PC_REN_K2 = 4;
        public static final int WLQU_PC_REN_K2_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_PC_REN_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_PC_REN_K2 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
