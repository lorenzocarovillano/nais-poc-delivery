package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WOCO-ID-MOVI-CHIU<br>
 * Variable: WOCO-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_ID_MOVI_CHIU;
    }

    public void setWocoIdMoviChiu(int wocoIdMoviChiu) {
        writeIntAsPacked(Pos.WOCO_ID_MOVI_CHIU, wocoIdMoviChiu, Len.Int.WOCO_ID_MOVI_CHIU);
    }

    public void setWocoIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_ID_MOVI_CHIU, Pos.WOCO_ID_MOVI_CHIU);
    }

    /**Original name: WOCO-ID-MOVI-CHIU<br>*/
    public int getWocoIdMoviChiu() {
        return readPackedAsInt(Pos.WOCO_ID_MOVI_CHIU, Len.Int.WOCO_ID_MOVI_CHIU);
    }

    public byte[] getWocoIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_ID_MOVI_CHIU, Pos.WOCO_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWocoIdMoviChiuSpaces() {
        fill(Pos.WOCO_ID_MOVI_CHIU, Len.WOCO_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWocoIdMoviChiuNull(String wocoIdMoviChiuNull) {
        writeString(Pos.WOCO_ID_MOVI_CHIU_NULL, wocoIdMoviChiuNull, Len.WOCO_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WOCO-ID-MOVI-CHIU-NULL<br>*/
    public String getWocoIdMoviChiuNull() {
        return readString(Pos.WOCO_ID_MOVI_CHIU_NULL, Len.WOCO_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_ID_MOVI_CHIU = 1;
        public static final int WOCO_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_ID_MOVI_CHIU = 5;
        public static final int WOCO_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
