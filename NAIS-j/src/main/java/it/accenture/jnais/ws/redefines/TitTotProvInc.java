package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-PROV-INC<br>
 * Variable: TIT-TOT-PROV-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_PROV_INC;
    }

    public void setTitTotProvInc(AfDecimal titTotProvInc) {
        writeDecimalAsPacked(Pos.TIT_TOT_PROV_INC, titTotProvInc.copy());
    }

    public void setTitTotProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_PROV_INC, Pos.TIT_TOT_PROV_INC);
    }

    /**Original name: TIT-TOT-PROV-INC<br>*/
    public AfDecimal getTitTotProvInc() {
        return readPackedAsDecimal(Pos.TIT_TOT_PROV_INC, Len.Int.TIT_TOT_PROV_INC, Len.Fract.TIT_TOT_PROV_INC);
    }

    public byte[] getTitTotProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_PROV_INC, Pos.TIT_TOT_PROV_INC);
        return buffer;
    }

    public void setTitTotProvIncNull(String titTotProvIncNull) {
        writeString(Pos.TIT_TOT_PROV_INC_NULL, titTotProvIncNull, Len.TIT_TOT_PROV_INC_NULL);
    }

    /**Original name: TIT-TOT-PROV-INC-NULL<br>*/
    public String getTitTotProvIncNull() {
        return readString(Pos.TIT_TOT_PROV_INC_NULL, Len.TIT_TOT_PROV_INC_NULL);
    }

    public String getTitTotProvIncNullFormatted() {
        return Functions.padBlanks(getTitTotProvIncNull(), Len.TIT_TOT_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PROV_INC = 1;
        public static final int TIT_TOT_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PROV_INC = 8;
        public static final int TIT_TOT_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
