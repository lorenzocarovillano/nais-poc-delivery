package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-DIR-VERS-AGG<br>
 * Variable: POL-DIR-VERS-AGG from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDirVersAgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolDirVersAgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_DIR_VERS_AGG;
    }

    public void setPolDirVersAgg(AfDecimal polDirVersAgg) {
        writeDecimalAsPacked(Pos.POL_DIR_VERS_AGG, polDirVersAgg.copy());
    }

    public void setPolDirVersAggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_DIR_VERS_AGG, Pos.POL_DIR_VERS_AGG);
    }

    /**Original name: POL-DIR-VERS-AGG<br>*/
    public AfDecimal getPolDirVersAgg() {
        return readPackedAsDecimal(Pos.POL_DIR_VERS_AGG, Len.Int.POL_DIR_VERS_AGG, Len.Fract.POL_DIR_VERS_AGG);
    }

    public byte[] getPolDirVersAggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_DIR_VERS_AGG, Pos.POL_DIR_VERS_AGG);
        return buffer;
    }

    public void setPolDirVersAggNull(String polDirVersAggNull) {
        writeString(Pos.POL_DIR_VERS_AGG_NULL, polDirVersAggNull, Len.POL_DIR_VERS_AGG_NULL);
    }

    /**Original name: POL-DIR-VERS-AGG-NULL<br>*/
    public String getPolDirVersAggNull() {
        return readString(Pos.POL_DIR_VERS_AGG_NULL, Len.POL_DIR_VERS_AGG_NULL);
    }

    public String getPolDirVersAggNullFormatted() {
        return Functions.padBlanks(getPolDirVersAggNull(), Len.POL_DIR_VERS_AGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_DIR_VERS_AGG = 1;
        public static final int POL_DIR_VERS_AGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_DIR_VERS_AGG = 8;
        public static final int POL_DIR_VERS_AGG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_DIR_VERS_AGG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int POL_DIR_VERS_AGG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
