package it.accenture.jnais.ws;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: COSTANTI<br>
 * Variable: COSTANTI from program IEAS9900<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Costanti {

    //==== PROPERTIES ====
    //Original name: WN-ACCESSI
    private short wnAccessi = ((short)13);

    //==== METHODS ====
    public short getWnAccessi() {
        return this.wnAccessi;
    }
}
