package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ALQ-CNBT-INPSTFM-D<br>
 * Variable: WDFL-ALQ-CNBT-INPSTFM-D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAlqCnbtInpstfmD extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAlqCnbtInpstfmD() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ALQ_CNBT_INPSTFM_D;
    }

    public void setWdflAlqCnbtInpstfmD(AfDecimal wdflAlqCnbtInpstfmD) {
        writeDecimalAsPacked(Pos.WDFL_ALQ_CNBT_INPSTFM_D, wdflAlqCnbtInpstfmD.copy());
    }

    public void setWdflAlqCnbtInpstfmDFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ALQ_CNBT_INPSTFM_D, Pos.WDFL_ALQ_CNBT_INPSTFM_D);
    }

    /**Original name: WDFL-ALQ-CNBT-INPSTFM-D<br>*/
    public AfDecimal getWdflAlqCnbtInpstfmD() {
        return readPackedAsDecimal(Pos.WDFL_ALQ_CNBT_INPSTFM_D, Len.Int.WDFL_ALQ_CNBT_INPSTFM_D, Len.Fract.WDFL_ALQ_CNBT_INPSTFM_D);
    }

    public byte[] getWdflAlqCnbtInpstfmDAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ALQ_CNBT_INPSTFM_D, Pos.WDFL_ALQ_CNBT_INPSTFM_D);
        return buffer;
    }

    public void setWdflAlqCnbtInpstfmDNull(String wdflAlqCnbtInpstfmDNull) {
        writeString(Pos.WDFL_ALQ_CNBT_INPSTFM_D_NULL, wdflAlqCnbtInpstfmDNull, Len.WDFL_ALQ_CNBT_INPSTFM_D_NULL);
    }

    /**Original name: WDFL-ALQ-CNBT-INPSTFM-D-NULL<br>*/
    public String getWdflAlqCnbtInpstfmDNull() {
        return readString(Pos.WDFL_ALQ_CNBT_INPSTFM_D_NULL, Len.WDFL_ALQ_CNBT_INPSTFM_D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_CNBT_INPSTFM_D = 1;
        public static final int WDFL_ALQ_CNBT_INPSTFM_D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_CNBT_INPSTFM_D = 4;
        public static final int WDFL_ALQ_CNBT_INPSTFM_D_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_CNBT_INPSTFM_D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_CNBT_INPSTFM_D = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
