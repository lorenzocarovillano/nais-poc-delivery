package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-MANFEE-REC<br>
 * Variable: WTDR-TOT-MANFEE-REC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotManfeeRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotManfeeRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_MANFEE_REC;
    }

    public void setWtdrTotManfeeRec(AfDecimal wtdrTotManfeeRec) {
        writeDecimalAsPacked(Pos.WTDR_TOT_MANFEE_REC, wtdrTotManfeeRec.copy());
    }

    /**Original name: WTDR-TOT-MANFEE-REC<br>*/
    public AfDecimal getWtdrTotManfeeRec() {
        return readPackedAsDecimal(Pos.WTDR_TOT_MANFEE_REC, Len.Int.WTDR_TOT_MANFEE_REC, Len.Fract.WTDR_TOT_MANFEE_REC);
    }

    public void setWtdrTotManfeeRecNull(String wtdrTotManfeeRecNull) {
        writeString(Pos.WTDR_TOT_MANFEE_REC_NULL, wtdrTotManfeeRecNull, Len.WTDR_TOT_MANFEE_REC_NULL);
    }

    /**Original name: WTDR-TOT-MANFEE-REC-NULL<br>*/
    public String getWtdrTotManfeeRecNull() {
        return readString(Pos.WTDR_TOT_MANFEE_REC_NULL, Len.WTDR_TOT_MANFEE_REC_NULL);
    }

    public String getWtdrTotManfeeRecNullFormatted() {
        return Functions.padBlanks(getWtdrTotManfeeRecNull(), Len.WTDR_TOT_MANFEE_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_MANFEE_REC = 1;
        public static final int WTDR_TOT_MANFEE_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_MANFEE_REC = 8;
        public static final int WTDR_TOT_MANFEE_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_MANFEE_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_MANFEE_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
