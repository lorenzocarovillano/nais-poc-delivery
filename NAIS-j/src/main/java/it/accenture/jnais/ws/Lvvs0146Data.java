package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import it.accenture.jnais.ws.enums.FlagComunTrov;
import it.accenture.jnais.ws.enums.FlagCurMov;
import it.accenture.jnais.ws.enums.FlagUltimaLettura;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0002;
import it.accenture.jnais.ws.enums.WsTpFrmAssva;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0146<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0146Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0146";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "LDBS7120";
    //Original name: LDBS6040
    private String ldbs6040 = "LDBS6040";
    //Original name: LDBV7121
    private Ldbv5141 ldbv7121 = new Ldbv5141();
    /**Original name: WS-TP-FRM-ASSVA<br>
	 * <pre>*****************************************************************
	 *     TP_FRM_ASSVA (Forma Assicurativa)
	 * *****************************************************************</pre>*/
    private WsTpFrmAssva wsTpFrmAssva = new WsTpFrmAssva();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0002 wsMovimento = new WsMovimentoLvvs0002();
    //Original name: MOVI
    private MoviLdbs1530 movi = new MoviLdbs1530();
    //Original name: WS-TRCH-POS
    private WsTrchPosLvvs0146 wsTrchPos = new WsTrchPosLvvs0146();
    //Original name: WS-TRCH-NEG
    private WsTrchNegLvvs0146 wsTrchNeg = new WsTrchNegLvvs0146();
    //Original name: WS-CUM-PRE-ATT
    private AfDecimal wsCumPreAtt = new AfDecimal("0", 15, 3);
    //Original name: WK-ID-MOVI-COMUN
    private int wkIdMoviComun = DefaultValues.INT_VAL;
    //Original name: WK-DT-INI-EFF-TEMP
    private int wkDtIniEffTemp = DefaultValues.INT_VAL;
    //Original name: WK-DT-CPTZ-TEMP
    private long wkDtCptzTemp = DefaultValues.LONG_VAL;
    //Original name: WK-DATA-EFF-PREC
    private int wkDataEffPrec = DefaultValues.INT_VAL;
    //Original name: WK-DATA-CPTZ-PREC
    private long wkDataCptzPrec = DefaultValues.LONG_VAL;
    //Original name: FLAG-ULTIMA-LETTURA
    private FlagUltimaLettura flagUltimaLettura = new FlagUltimaLettura();
    //Original name: FLAG-CUR-MOV
    private FlagCurMov flagCurMov = new FlagCurMov();
    //Original name: FLAG-COMUN-TROV
    private FlagComunTrov flagComunTrov = new FlagComunTrov();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public String getLdbs6040() {
        return this.ldbs6040;
    }

    public void setWsCumPreAtt(AfDecimal wsCumPreAtt) {
        this.wsCumPreAtt.assign(wsCumPreAtt);
    }

    public AfDecimal getWsCumPreAtt() {
        return this.wsCumPreAtt.copy();
    }

    public void setWkIdMoviComun(int wkIdMoviComun) {
        this.wkIdMoviComun = wkIdMoviComun;
    }

    public int getWkIdMoviComun() {
        return this.wkIdMoviComun;
    }

    public void setWkDtIniEffTemp(int wkDtIniEffTemp) {
        this.wkDtIniEffTemp = wkDtIniEffTemp;
    }

    public int getWkDtIniEffTemp() {
        return this.wkDtIniEffTemp;
    }

    public void setWkDtCptzTemp(long wkDtCptzTemp) {
        this.wkDtCptzTemp = wkDtCptzTemp;
    }

    public long getWkDtCptzTemp() {
        return this.wkDtCptzTemp;
    }

    public void setWkDataEffPrec(int wkDataEffPrec) {
        this.wkDataEffPrec = wkDataEffPrec;
    }

    public int getWkDataEffPrec() {
        return this.wkDataEffPrec;
    }

    public void setWkDataCptzPrec(long wkDataCptzPrec) {
        this.wkDataCptzPrec = wkDataCptzPrec;
    }

    public long getWkDataCptzPrec() {
        return this.wkDataCptzPrec;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public FlagComunTrov getFlagComunTrov() {
        return flagComunTrov;
    }

    public FlagCurMov getFlagCurMov() {
        return flagCurMov;
    }

    public FlagUltimaLettura getFlagUltimaLettura() {
        return flagUltimaLettura;
    }

    public Ldbv5141 getLdbv7121() {
        return ldbv7121;
    }

    public MoviLdbs1530 getMovi() {
        return movi;
    }

    public WsMovimentoLvvs0002 getWsMovimento() {
        return wsMovimento;
    }

    public WsTpFrmAssva getWsTpFrmAssva() {
        return wsTpFrmAssva;
    }

    public WsTrchNegLvvs0146 getWsTrchNeg() {
        return wsTrchNeg;
    }

    public WsTrchPosLvvs0146 getWsTrchPos() {
        return wsTrchPos;
    }
}
