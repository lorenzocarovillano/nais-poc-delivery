package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADA-PRECISIONE-DATO<br>
 * Variable: ADA-PRECISIONE-DATO from program IDSS0020<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdaPrecisioneDato extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdaPrecisioneDato() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADA_PRECISIONE_DATO;
    }

    public void setAdaPrecisioneDato(short adaPrecisioneDato) {
        writeShortAsPacked(Pos.ADA_PRECISIONE_DATO, adaPrecisioneDato, Len.Int.ADA_PRECISIONE_DATO);
    }

    public void setAdaPrecisioneDatoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADA_PRECISIONE_DATO, Pos.ADA_PRECISIONE_DATO);
    }

    /**Original name: ADA-PRECISIONE-DATO<br>*/
    public short getAdaPrecisioneDato() {
        return readPackedAsShort(Pos.ADA_PRECISIONE_DATO, Len.Int.ADA_PRECISIONE_DATO);
    }

    public byte[] getAdaPrecisioneDatoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADA_PRECISIONE_DATO, Pos.ADA_PRECISIONE_DATO);
        return buffer;
    }

    public void setAdaPrecisioneDatoNull(String adaPrecisioneDatoNull) {
        writeString(Pos.ADA_PRECISIONE_DATO_NULL, adaPrecisioneDatoNull, Len.ADA_PRECISIONE_DATO_NULL);
    }

    /**Original name: ADA-PRECISIONE-DATO-NULL<br>*/
    public String getAdaPrecisioneDatoNull() {
        return readString(Pos.ADA_PRECISIONE_DATO_NULL, Len.ADA_PRECISIONE_DATO_NULL);
    }

    public String getAdaPrecisioneDatoNullFormatted() {
        return Functions.padBlanks(getAdaPrecisioneDatoNull(), Len.ADA_PRECISIONE_DATO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADA_PRECISIONE_DATO = 1;
        public static final int ADA_PRECISIONE_DATO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADA_PRECISIONE_DATO = 2;
        public static final int ADA_PRECISIONE_DATO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADA_PRECISIONE_DATO = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
