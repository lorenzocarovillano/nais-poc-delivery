package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.WsEsito;
import it.accenture.jnais.ws.occurs.WsTabellaParametri;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IEAS9800<br>
 * Generated as a class for rule WS.<br>*/
public class Ieas9800Data {

    //==== PROPERTIES ====
    public static final int TABELLA_PARAMETRI_MAXOCCURS = 10;
    /**Original name: WS-ESITO<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *                    FLAG PER ESITO ELABORAZIONE                 *
	 *                                                                *
	 * ****************************************************************</pre>*/
    private WsEsito esito = new WsEsito();
    //Original name: WS-INDICI
    private WsIndici indici = new WsIndici();
    /**Original name: WS-LIMITE-ELE-PARAMETRI<br>
	 * <pre>****************************************************************
	 *  COSTANTI
	 * ****************************************************************</pre>*/
    private short limiteEleParametri = ((short)10);
    //Original name: WS-LIMITE-STR-PARAMETRO
    private short limiteStrParametro = ((short)100);
    //Original name: WS-LIMITE-STR-DESC-ERR
    private short limiteStrDescErr = ((short)200);
    //Original name: WS-TABELLA-PARAMETRI
    private WsTabellaParametri[] tabellaParametri = new WsTabellaParametri[TABELLA_PARAMETRI_MAXOCCURS];
    //Original name: WS-DESC-ERRORE
    private String descErrore = DefaultValues.stringVal(Len.DESC_ERRORE);

    //==== CONSTRUCTORS ====
    public Ieas9800Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabellaParametriIdx = 1; tabellaParametriIdx <= TABELLA_PARAMETRI_MAXOCCURS; tabellaParametriIdx++) {
            tabellaParametri[tabellaParametriIdx - 1] = new WsTabellaParametri();
        }
    }

    public short getLimiteEleParametri() {
        return this.limiteEleParametri;
    }

    public short getLimiteStrParametro() {
        return this.limiteStrParametro;
    }

    public short getLimiteStrDescErr() {
        return this.limiteStrDescErr;
    }

    public void initListaParametriSpaces() {
        for (int idx = 1; idx <= TABELLA_PARAMETRI_MAXOCCURS; idx++) {
            tabellaParametri[idx - 1].initTabellaParametriSpaces();
        }
    }

    public void setDescErrore(String descErrore) {
        this.descErrore = Functions.subString(descErrore, Len.DESC_ERRORE);
    }

    public void setDescErroreSubstring(String replacement, int start, int length) {
        descErrore = Functions.setSubstring(descErrore, replacement, start, length);
    }

    public String getDescErrore() {
        return this.descErrore;
    }

    public WsEsito getEsito() {
        return esito;
    }

    public WsIndici getIndici() {
        return indici;
    }

    public WsTabellaParametri getTabellaParametri(int idx) {
        return tabellaParametri[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESC_ERRORE = 200;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
