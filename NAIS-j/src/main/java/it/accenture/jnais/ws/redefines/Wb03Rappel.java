package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-RAPPEL<br>
 * Variable: WB03-RAPPEL from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03Rappel extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03Rappel() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_RAPPEL;
    }

    public void setWb03Rappel(AfDecimal wb03Rappel) {
        writeDecimalAsPacked(Pos.WB03_RAPPEL, wb03Rappel.copy());
    }

    public void setWb03RappelFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_RAPPEL, Pos.WB03_RAPPEL);
    }

    /**Original name: WB03-RAPPEL<br>*/
    public AfDecimal getWb03Rappel() {
        return readPackedAsDecimal(Pos.WB03_RAPPEL, Len.Int.WB03_RAPPEL, Len.Fract.WB03_RAPPEL);
    }

    public byte[] getWb03RappelAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_RAPPEL, Pos.WB03_RAPPEL);
        return buffer;
    }

    public void setWb03RappelNull(String wb03RappelNull) {
        writeString(Pos.WB03_RAPPEL_NULL, wb03RappelNull, Len.WB03_RAPPEL_NULL);
    }

    /**Original name: WB03-RAPPEL-NULL<br>*/
    public String getWb03RappelNull() {
        return readString(Pos.WB03_RAPPEL_NULL, Len.WB03_RAPPEL_NULL);
    }

    public String getWb03RappelNullFormatted() {
        return Functions.padBlanks(getWb03RappelNull(), Len.WB03_RAPPEL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_RAPPEL = 1;
        public static final int WB03_RAPPEL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_RAPPEL = 8;
        public static final int WB03_RAPPEL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_RAPPEL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_RAPPEL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
