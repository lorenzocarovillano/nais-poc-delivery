package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPST-BOLLO-TOT-V<br>
 * Variable: S089-IMPST-BOLLO-TOT-V from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpstBolloTotV extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpstBolloTotV() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPST_BOLLO_TOT_V;
    }

    public void setWlquImpstBolloTotV(AfDecimal wlquImpstBolloTotV) {
        writeDecimalAsPacked(Pos.S089_IMPST_BOLLO_TOT_V, wlquImpstBolloTotV.copy());
    }

    public void setWlquImpstBolloTotVFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPST_BOLLO_TOT_V, Pos.S089_IMPST_BOLLO_TOT_V);
    }

    /**Original name: WLQU-IMPST-BOLLO-TOT-V<br>*/
    public AfDecimal getWlquImpstBolloTotV() {
        return readPackedAsDecimal(Pos.S089_IMPST_BOLLO_TOT_V, Len.Int.WLQU_IMPST_BOLLO_TOT_V, Len.Fract.WLQU_IMPST_BOLLO_TOT_V);
    }

    public byte[] getWlquImpstBolloTotVAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPST_BOLLO_TOT_V, Pos.S089_IMPST_BOLLO_TOT_V);
        return buffer;
    }

    public void initWlquImpstBolloTotVSpaces() {
        fill(Pos.S089_IMPST_BOLLO_TOT_V, Len.S089_IMPST_BOLLO_TOT_V, Types.SPACE_CHAR);
    }

    public void setWlquImpstBolloTotVNull(String wlquImpstBolloTotVNull) {
        writeString(Pos.S089_IMPST_BOLLO_TOT_V_NULL, wlquImpstBolloTotVNull, Len.WLQU_IMPST_BOLLO_TOT_V_NULL);
    }

    /**Original name: WLQU-IMPST-BOLLO-TOT-V-NULL<br>*/
    public String getWlquImpstBolloTotVNull() {
        return readString(Pos.S089_IMPST_BOLLO_TOT_V_NULL, Len.WLQU_IMPST_BOLLO_TOT_V_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPST_BOLLO_TOT_V = 1;
        public static final int S089_IMPST_BOLLO_TOT_V_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPST_BOLLO_TOT_V = 8;
        public static final int WLQU_IMPST_BOLLO_TOT_V_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_BOLLO_TOT_V = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_BOLLO_TOT_V = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
