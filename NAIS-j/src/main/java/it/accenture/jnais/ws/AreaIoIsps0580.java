package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ispc0580DatiInput;
import it.accenture.jnais.ws.occurs.Ispc0211TabErrori;
import it.accenture.jnais.ws.occurs.Ispc0580TabProd;

/**Original name: AREA-IO-ISPS0580<br>
 * Variable: AREA-IO-ISPS0580 from program ISPS0580<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoIsps0580 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_PROD_MAXOCCURS = 250;
    public static final int TAB_ERRORI_MAXOCCURS = 10;
    //Original name: ISPC0580-DATI-INPUT
    private Ispc0580DatiInput datiInput = new Ispc0580DatiInput();
    //Original name: ISPC0580-NUM-MAX-PROD
    private String numMaxProd = DefaultValues.stringVal(Len.NUM_MAX_PROD);
    //Original name: ISPC0580-TAB-PROD
    private Ispc0580TabProd[] tabProd = new Ispc0580TabProd[TAB_PROD_MAXOCCURS];
    //Original name: ISPC0580-ESITO
    private String esito = DefaultValues.stringVal(Len.ESITO);
    //Original name: ISPC0580-ERR-NUM-ELE
    private String errNumEle = DefaultValues.stringVal(Len.ERR_NUM_ELE);
    //Original name: ISPC0580-TAB-ERRORI
    private Ispc0211TabErrori[] tabErrori = new Ispc0211TabErrori[TAB_ERRORI_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public AreaIoIsps0580() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IO_ISPS0580;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIoIsps0580Bytes(buf);
    }

    public void init() {
        for (int tabProdIdx = 1; tabProdIdx <= TAB_PROD_MAXOCCURS; tabProdIdx++) {
            tabProd[tabProdIdx - 1] = new Ispc0580TabProd();
        }
        for (int tabErroriIdx = 1; tabErroriIdx <= TAB_ERRORI_MAXOCCURS; tabErroriIdx++) {
            tabErrori[tabErroriIdx - 1] = new Ispc0211TabErrori();
        }
    }

    public String getAreaIoIsps0580Formatted() {
        return MarshalByteExt.bufferToStr(getAreaIoIsps0580Bytes());
    }

    public void setAreaIoIsps0580Bytes(byte[] buffer) {
        setAreaIoIsps0580Bytes(buffer, 1);
    }

    public byte[] getAreaIoIsps0580Bytes() {
        byte[] buffer = new byte[Len.AREA_IO_ISPS0580];
        return getAreaIoIsps0580Bytes(buffer, 1);
    }

    public void setAreaIoIsps0580Bytes(byte[] buffer, int offset) {
        int position = offset;
        datiInput.setDatiInputBytes(buffer, position);
        position += Ispc0580DatiInput.Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
        position += Len.DATI_OUTPUT;
        setAreaErroriBytes(buffer, position);
    }

    public byte[] getAreaIoIsps0580Bytes(byte[] buffer, int offset) {
        int position = offset;
        datiInput.getDatiInputBytes(buffer, position);
        position += Ispc0580DatiInput.Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        position += Len.DATI_OUTPUT;
        getAreaErroriBytes(buffer, position);
        return buffer;
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        numMaxProd = MarshalByte.readFixedString(buffer, position, Len.NUM_MAX_PROD);
        position += Len.NUM_MAX_PROD;
        for (int idx = 1; idx <= TAB_PROD_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabProd[idx - 1].setTabProdBytes(buffer, position);
                position += Ispc0580TabProd.Len.TAB_PROD;
            }
            else {
                tabProd[idx - 1].initTabProdSpaces();
                position += Ispc0580TabProd.Len.TAB_PROD;
            }
        }
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, numMaxProd, Len.NUM_MAX_PROD);
        position += Len.NUM_MAX_PROD;
        for (int idx = 1; idx <= TAB_PROD_MAXOCCURS; idx++) {
            tabProd[idx - 1].getTabProdBytes(buffer, position);
            position += Ispc0580TabProd.Len.TAB_PROD;
        }
        return buffer;
    }

    public void setNumMaxProdFormatted(String numMaxProd) {
        this.numMaxProd = Trunc.toUnsignedNumeric(numMaxProd, Len.NUM_MAX_PROD);
    }

    public short getNumMaxProd() {
        return NumericDisplay.asShort(this.numMaxProd);
    }

    /**Original name: ISPC0580-AREA-ERRORI<br>*/
    public byte[] getAreaErroriBytes() {
        byte[] buffer = new byte[Len.AREA_ERRORI];
        return getAreaErroriBytes(buffer, 1);
    }

    public void setAreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        esito = MarshalByte.readFixedString(buffer, position, Len.ESITO);
        position += Len.ESITO;
        errNumEle = MarshalByte.readFixedString(buffer, position, Len.ERR_NUM_ELE);
        position += Len.ERR_NUM_ELE;
        for (int idx = 1; idx <= TAB_ERRORI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabErrori[idx - 1].setIspc0211TabErroriBytes(buffer, position);
                position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
            }
            else {
                tabErrori[idx - 1].initIspc0211TabErroriSpaces();
                position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
            }
        }
    }

    public byte[] getAreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, esito, Len.ESITO);
        position += Len.ESITO;
        MarshalByte.writeString(buffer, position, errNumEle, Len.ERR_NUM_ELE);
        position += Len.ERR_NUM_ELE;
        for (int idx = 1; idx <= TAB_ERRORI_MAXOCCURS; idx++) {
            tabErrori[idx - 1].getIspc0211TabErroriBytes(buffer, position);
            position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
        }
        return buffer;
    }

    public void setEsitoFormatted(String esito) {
        this.esito = Trunc.toUnsignedNumeric(esito, Len.ESITO);
    }

    public short getEsito() {
        return NumericDisplay.asShort(this.esito);
    }

    public void setErrNumEleFormatted(String errNumEle) {
        this.errNumEle = Trunc.toUnsignedNumeric(errNumEle, Len.ERR_NUM_ELE);
    }

    public short getErrNumEle() {
        return NumericDisplay.asShort(this.errNumEle);
    }

    public Ispc0580DatiInput getDatiInput() {
        return datiInput;
    }

    public Ispc0211TabErrori getTabErrori(int idx) {
        return tabErrori[idx - 1];
    }

    public Ispc0580TabProd getTabProd(int idx) {
        return tabProd[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getAreaIoIsps0580Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NUM_MAX_PROD = 3;
        public static final int DATI_OUTPUT = NUM_MAX_PROD + AreaIoIsps0580.TAB_PROD_MAXOCCURS * Ispc0580TabProd.Len.TAB_PROD;
        public static final int ESITO = 2;
        public static final int ERR_NUM_ELE = 3;
        public static final int AREA_ERRORI = ESITO + ERR_NUM_ELE + AreaIoIsps0580.TAB_ERRORI_MAXOCCURS * Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
        public static final int AREA_IO_ISPS0580 = Ispc0580DatiInput.Len.DATI_INPUT + DATI_OUTPUT + AREA_ERRORI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
