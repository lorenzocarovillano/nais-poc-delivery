package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTSC-ELAB-IN<br>
 * Variable: PCO-DT-ULTSC-ELAB-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltscElabIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltscElabIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTSC_ELAB_IN;
    }

    public void setPcoDtUltscElabIn(int pcoDtUltscElabIn) {
        writeIntAsPacked(Pos.PCO_DT_ULTSC_ELAB_IN, pcoDtUltscElabIn, Len.Int.PCO_DT_ULTSC_ELAB_IN);
    }

    public void setPcoDtUltscElabInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTSC_ELAB_IN, Pos.PCO_DT_ULTSC_ELAB_IN);
    }

    /**Original name: PCO-DT-ULTSC-ELAB-IN<br>*/
    public int getPcoDtUltscElabIn() {
        return readPackedAsInt(Pos.PCO_DT_ULTSC_ELAB_IN, Len.Int.PCO_DT_ULTSC_ELAB_IN);
    }

    public byte[] getPcoDtUltscElabInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTSC_ELAB_IN, Pos.PCO_DT_ULTSC_ELAB_IN);
        return buffer;
    }

    public void initPcoDtUltscElabInHighValues() {
        fill(Pos.PCO_DT_ULTSC_ELAB_IN, Len.PCO_DT_ULTSC_ELAB_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltscElabInNull(String pcoDtUltscElabInNull) {
        writeString(Pos.PCO_DT_ULTSC_ELAB_IN_NULL, pcoDtUltscElabInNull, Len.PCO_DT_ULTSC_ELAB_IN_NULL);
    }

    /**Original name: PCO-DT-ULTSC-ELAB-IN-NULL<br>*/
    public String getPcoDtUltscElabInNull() {
        return readString(Pos.PCO_DT_ULTSC_ELAB_IN_NULL, Len.PCO_DT_ULTSC_ELAB_IN_NULL);
    }

    public String getPcoDtUltscElabInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltscElabInNull(), Len.PCO_DT_ULTSC_ELAB_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTSC_ELAB_IN = 1;
        public static final int PCO_DT_ULTSC_ELAB_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTSC_ELAB_IN = 5;
        public static final int PCO_DT_ULTSC_ELAB_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTSC_ELAB_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
