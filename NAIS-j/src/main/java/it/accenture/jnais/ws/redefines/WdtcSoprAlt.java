package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-SOPR-ALT<br>
 * Variable: WDTC-SOPR-ALT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcSoprAlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcSoprAlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_SOPR_ALT;
    }

    public void setWdtcSoprAlt(AfDecimal wdtcSoprAlt) {
        writeDecimalAsPacked(Pos.WDTC_SOPR_ALT, wdtcSoprAlt.copy());
    }

    public void setWdtcSoprAltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_SOPR_ALT, Pos.WDTC_SOPR_ALT);
    }

    /**Original name: WDTC-SOPR-ALT<br>*/
    public AfDecimal getWdtcSoprAlt() {
        return readPackedAsDecimal(Pos.WDTC_SOPR_ALT, Len.Int.WDTC_SOPR_ALT, Len.Fract.WDTC_SOPR_ALT);
    }

    public byte[] getWdtcSoprAltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_SOPR_ALT, Pos.WDTC_SOPR_ALT);
        return buffer;
    }

    public void initWdtcSoprAltSpaces() {
        fill(Pos.WDTC_SOPR_ALT, Len.WDTC_SOPR_ALT, Types.SPACE_CHAR);
    }

    public void setWdtcSoprAltNull(String wdtcSoprAltNull) {
        writeString(Pos.WDTC_SOPR_ALT_NULL, wdtcSoprAltNull, Len.WDTC_SOPR_ALT_NULL);
    }

    /**Original name: WDTC-SOPR-ALT-NULL<br>*/
    public String getWdtcSoprAltNull() {
        return readString(Pos.WDTC_SOPR_ALT_NULL, Len.WDTC_SOPR_ALT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_SOPR_ALT = 1;
        public static final int WDTC_SOPR_ALT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_SOPR_ALT = 8;
        public static final int WDTC_SOPR_ALT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_SOPR_ALT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_SOPR_ALT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
