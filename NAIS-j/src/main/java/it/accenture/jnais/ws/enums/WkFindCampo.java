package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-FIND-CAMPO<br>
 * Variable: WK-FIND-CAMPO from program IDSS0160<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFindCampo {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkFindCampo(char wkFindCampo) {
        this.value = wkFindCampo;
    }

    public char getWkFindCampo() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
