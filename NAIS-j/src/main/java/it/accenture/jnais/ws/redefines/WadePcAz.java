package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-PC-AZ<br>
 * Variable: WADE-PC-AZ from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadePcAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadePcAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_PC_AZ;
    }

    public void setWadePcAz(AfDecimal wadePcAz) {
        writeDecimalAsPacked(Pos.WADE_PC_AZ, wadePcAz.copy());
    }

    public void setWadePcAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_PC_AZ, Pos.WADE_PC_AZ);
    }

    /**Original name: WADE-PC-AZ<br>*/
    public AfDecimal getWadePcAz() {
        return readPackedAsDecimal(Pos.WADE_PC_AZ, Len.Int.WADE_PC_AZ, Len.Fract.WADE_PC_AZ);
    }

    public byte[] getWadePcAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_PC_AZ, Pos.WADE_PC_AZ);
        return buffer;
    }

    public void initWadePcAzSpaces() {
        fill(Pos.WADE_PC_AZ, Len.WADE_PC_AZ, Types.SPACE_CHAR);
    }

    public void setWadePcAzNull(String wadePcAzNull) {
        writeString(Pos.WADE_PC_AZ_NULL, wadePcAzNull, Len.WADE_PC_AZ_NULL);
    }

    /**Original name: WADE-PC-AZ-NULL<br>*/
    public String getWadePcAzNull() {
        return readString(Pos.WADE_PC_AZ_NULL, Len.WADE_PC_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_PC_AZ = 1;
        public static final int WADE_PC_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_PC_AZ = 4;
        public static final int WADE_PC_AZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_PC_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_PC_AZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
