package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-SOPR-SPO<br>
 * Variable: WTDR-TOT-SOPR-SPO from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_SOPR_SPO;
    }

    public void setWtdrTotSoprSpo(AfDecimal wtdrTotSoprSpo) {
        writeDecimalAsPacked(Pos.WTDR_TOT_SOPR_SPO, wtdrTotSoprSpo.copy());
    }

    /**Original name: WTDR-TOT-SOPR-SPO<br>*/
    public AfDecimal getWtdrTotSoprSpo() {
        return readPackedAsDecimal(Pos.WTDR_TOT_SOPR_SPO, Len.Int.WTDR_TOT_SOPR_SPO, Len.Fract.WTDR_TOT_SOPR_SPO);
    }

    public void setWtdrTotSoprSpoNull(String wtdrTotSoprSpoNull) {
        writeString(Pos.WTDR_TOT_SOPR_SPO_NULL, wtdrTotSoprSpoNull, Len.WTDR_TOT_SOPR_SPO_NULL);
    }

    /**Original name: WTDR-TOT-SOPR-SPO-NULL<br>*/
    public String getWtdrTotSoprSpoNull() {
        return readString(Pos.WTDR_TOT_SOPR_SPO_NULL, Len.WTDR_TOT_SOPR_SPO_NULL);
    }

    public String getWtdrTotSoprSpoNullFormatted() {
        return Functions.padBlanks(getWtdrTotSoprSpoNull(), Len.WTDR_TOT_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SOPR_SPO = 1;
        public static final int WTDR_TOT_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SOPR_SPO = 8;
        public static final int WTDR_TOT_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
