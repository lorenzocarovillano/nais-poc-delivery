package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-QTZO-IN<br>
 * Variable: PCO-DT-ULT-QTZO-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltQtzoIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltQtzoIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_QTZO_IN;
    }

    public void setPcoDtUltQtzoIn(int pcoDtUltQtzoIn) {
        writeIntAsPacked(Pos.PCO_DT_ULT_QTZO_IN, pcoDtUltQtzoIn, Len.Int.PCO_DT_ULT_QTZO_IN);
    }

    public void setPcoDtUltQtzoInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_QTZO_IN, Pos.PCO_DT_ULT_QTZO_IN);
    }

    /**Original name: PCO-DT-ULT-QTZO-IN<br>*/
    public int getPcoDtUltQtzoIn() {
        return readPackedAsInt(Pos.PCO_DT_ULT_QTZO_IN, Len.Int.PCO_DT_ULT_QTZO_IN);
    }

    public byte[] getPcoDtUltQtzoInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_QTZO_IN, Pos.PCO_DT_ULT_QTZO_IN);
        return buffer;
    }

    public void initPcoDtUltQtzoInHighValues() {
        fill(Pos.PCO_DT_ULT_QTZO_IN, Len.PCO_DT_ULT_QTZO_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltQtzoInNull(String pcoDtUltQtzoInNull) {
        writeString(Pos.PCO_DT_ULT_QTZO_IN_NULL, pcoDtUltQtzoInNull, Len.PCO_DT_ULT_QTZO_IN_NULL);
    }

    /**Original name: PCO-DT-ULT-QTZO-IN-NULL<br>*/
    public String getPcoDtUltQtzoInNull() {
        return readString(Pos.PCO_DT_ULT_QTZO_IN_NULL, Len.PCO_DT_ULT_QTZO_IN_NULL);
    }

    public String getPcoDtUltQtzoInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltQtzoInNull(), Len.PCO_DT_ULT_QTZO_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_QTZO_IN = 1;
        public static final int PCO_DT_ULT_QTZO_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_QTZO_IN = 5;
        public static final int PCO_DT_ULT_QTZO_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_QTZO_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
