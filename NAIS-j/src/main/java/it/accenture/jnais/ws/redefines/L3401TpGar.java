package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-TP-GAR<br>
 * Variable: L3401-TP-GAR from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401TpGar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401TpGar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_TP_GAR;
    }

    public void setL3401TpGar(short l3401TpGar) {
        writeShortAsPacked(Pos.L3401_TP_GAR, l3401TpGar, Len.Int.L3401_TP_GAR);
    }

    public void setL3401TpGarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_TP_GAR, Pos.L3401_TP_GAR);
    }

    /**Original name: L3401-TP-GAR<br>*/
    public short getL3401TpGar() {
        return readPackedAsShort(Pos.L3401_TP_GAR, Len.Int.L3401_TP_GAR);
    }

    public byte[] getL3401TpGarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_TP_GAR, Pos.L3401_TP_GAR);
        return buffer;
    }

    /**Original name: L3401-TP-GAR-NULL<br>*/
    public String getL3401TpGarNull() {
        return readString(Pos.L3401_TP_GAR_NULL, Len.L3401_TP_GAR_NULL);
    }

    public String getL3401TpGarNullFormatted() {
        return Functions.padBlanks(getL3401TpGarNull(), Len.L3401_TP_GAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_TP_GAR = 1;
        public static final int L3401_TP_GAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_TP_GAR = 2;
        public static final int L3401_TP_GAR_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_TP_GAR = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
