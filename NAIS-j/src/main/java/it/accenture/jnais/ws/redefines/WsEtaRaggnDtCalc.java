package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WS-ETA-RAGGN-DT-CALC<br>
 * Variable: WS-ETA-RAGGN-DT-CALC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsEtaRaggnDtCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsEtaRaggnDtCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_ETA_RAGGN_DT_CALC;
    }

    public void setWsEtaRaggnDtCalc(AfDecimal wsEtaRaggnDtCalc) {
        writeDecimal(Pos.WS_ETA_RAGGN_DT_CALC, wsEtaRaggnDtCalc.copy());
    }

    /**Original name: WS-ETA-RAGGN-DT-CALC<br>*/
    public AfDecimal getWsEtaRaggnDtCalc() {
        return readDecimal(Pos.WS_ETA_RAGGN_DT_CALC, Len.Int.WS_ETA_RAGGN_DT_CALC, Len.Fract.WS_ETA_RAGGN_DT_CALC);
    }

    public void setCalcNumFormatted(String calcNum) {
        writeString(Pos.CALC_NUM, Trunc.toUnsignedNumeric(calcNum, Len.CALC_NUM), Len.CALC_NUM);
    }

    /**Original name: WS-ETA-RAGGN-DT-CALC-NUM<br>*/
    public short getCalcNum() {
        return readNumDispUnsignedShort(Pos.CALC_NUM, Len.CALC_NUM);
    }

    public void setCalcDec(short calcDec) {
        writeShort(Pos.CALC_DEC, calcDec, Len.Int.CALC_DEC, SignType.NO_SIGN);
    }

    public void setCalcDecFormatted(String calcDec) {
        writeString(Pos.CALC_DEC, Trunc.toUnsignedNumeric(calcDec, Len.CALC_DEC), Len.CALC_DEC);
    }

    /**Original name: WS-ETA-RAGGN-DT-CALC-DEC<br>*/
    public short getCalcDec() {
        return readNumDispUnsignedShort(Pos.CALC_DEC, Len.CALC_DEC);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_ETA_RAGGN_DT_CALC = 1;
        public static final int WS_ETA_RAGGN_DT_CALC_V = 1;
        public static final int CALC_NUM = WS_ETA_RAGGN_DT_CALC_V;
        public static final int CALC_DEC = CALC_NUM + Len.CALC_NUM;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int CALC_NUM = 4;
        public static final int WS_ETA_RAGGN_DT_CALC = 7;
        public static final int CALC_DEC = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WS_ETA_RAGGN_DT_CALC = 4;
            public static final int CALC_DEC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WS_ETA_RAGGN_DT_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
