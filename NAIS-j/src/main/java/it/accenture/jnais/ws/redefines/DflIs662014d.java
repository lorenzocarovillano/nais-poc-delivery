package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IS-662014D<br>
 * Variable: DFL-IS-662014D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIs662014d extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIs662014d() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IS662014D;
    }

    public void setDflIs662014d(AfDecimal dflIs662014d) {
        writeDecimalAsPacked(Pos.DFL_IS662014D, dflIs662014d.copy());
    }

    public void setDflIs662014dFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IS662014D, Pos.DFL_IS662014D);
    }

    /**Original name: DFL-IS-662014D<br>*/
    public AfDecimal getDflIs662014d() {
        return readPackedAsDecimal(Pos.DFL_IS662014D, Len.Int.DFL_IS662014D, Len.Fract.DFL_IS662014D);
    }

    public byte[] getDflIs662014dAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IS662014D, Pos.DFL_IS662014D);
        return buffer;
    }

    public void setDflIs662014dNull(String dflIs662014dNull) {
        writeString(Pos.DFL_IS662014D_NULL, dflIs662014dNull, Len.DFL_IS662014D_NULL);
    }

    /**Original name: DFL-IS-662014D-NULL<br>*/
    public String getDflIs662014dNull() {
        return readString(Pos.DFL_IS662014D_NULL, Len.DFL_IS662014D_NULL);
    }

    public String getDflIs662014dNullFormatted() {
        return Functions.padBlanks(getDflIs662014dNull(), Len.DFL_IS662014D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IS662014D = 1;
        public static final int DFL_IS662014D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IS662014D = 8;
        public static final int DFL_IS662014D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IS662014D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IS662014D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
