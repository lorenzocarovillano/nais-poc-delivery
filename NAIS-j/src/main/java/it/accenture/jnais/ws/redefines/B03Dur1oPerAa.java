package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DUR-1O-PER-AA<br>
 * Variable: B03-DUR-1O-PER-AA from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03Dur1oPerAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03Dur1oPerAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DUR1O_PER_AA;
    }

    public void setB03Dur1oPerAa(int b03Dur1oPerAa) {
        writeIntAsPacked(Pos.B03_DUR1O_PER_AA, b03Dur1oPerAa, Len.Int.B03_DUR1O_PER_AA);
    }

    public void setB03Dur1oPerAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DUR1O_PER_AA, Pos.B03_DUR1O_PER_AA);
    }

    /**Original name: B03-DUR-1O-PER-AA<br>*/
    public int getB03Dur1oPerAa() {
        return readPackedAsInt(Pos.B03_DUR1O_PER_AA, Len.Int.B03_DUR1O_PER_AA);
    }

    public byte[] getB03Dur1oPerAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DUR1O_PER_AA, Pos.B03_DUR1O_PER_AA);
        return buffer;
    }

    public void setB03Dur1oPerAaNull(String b03Dur1oPerAaNull) {
        writeString(Pos.B03_DUR1O_PER_AA_NULL, b03Dur1oPerAaNull, Len.B03_DUR1O_PER_AA_NULL);
    }

    /**Original name: B03-DUR-1O-PER-AA-NULL<br>*/
    public String getB03Dur1oPerAaNull() {
        return readString(Pos.B03_DUR1O_PER_AA_NULL, Len.B03_DUR1O_PER_AA_NULL);
    }

    public String getB03Dur1oPerAaNullFormatted() {
        return Functions.padBlanks(getB03Dur1oPerAaNull(), Len.B03_DUR1O_PER_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DUR1O_PER_AA = 1;
        public static final int B03_DUR1O_PER_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DUR1O_PER_AA = 3;
        public static final int B03_DUR1O_PER_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DUR1O_PER_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
