package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-INCR-PRSTZ<br>
 * Variable: TGA-INCR-PRSTZ from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaIncrPrstz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaIncrPrstz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_INCR_PRSTZ;
    }

    public void setTgaIncrPrstz(AfDecimal tgaIncrPrstz) {
        writeDecimalAsPacked(Pos.TGA_INCR_PRSTZ, tgaIncrPrstz.copy());
    }

    public void setTgaIncrPrstzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_INCR_PRSTZ, Pos.TGA_INCR_PRSTZ);
    }

    /**Original name: TGA-INCR-PRSTZ<br>*/
    public AfDecimal getTgaIncrPrstz() {
        return readPackedAsDecimal(Pos.TGA_INCR_PRSTZ, Len.Int.TGA_INCR_PRSTZ, Len.Fract.TGA_INCR_PRSTZ);
    }

    public byte[] getTgaIncrPrstzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_INCR_PRSTZ, Pos.TGA_INCR_PRSTZ);
        return buffer;
    }

    public void setTgaIncrPrstzNull(String tgaIncrPrstzNull) {
        writeString(Pos.TGA_INCR_PRSTZ_NULL, tgaIncrPrstzNull, Len.TGA_INCR_PRSTZ_NULL);
    }

    /**Original name: TGA-INCR-PRSTZ-NULL<br>*/
    public String getTgaIncrPrstzNull() {
        return readString(Pos.TGA_INCR_PRSTZ_NULL, Len.TGA_INCR_PRSTZ_NULL);
    }

    public String getTgaIncrPrstzNullFormatted() {
        return Functions.padBlanks(getTgaIncrPrstzNull(), Len.TGA_INCR_PRSTZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_INCR_PRSTZ = 1;
        public static final int TGA_INCR_PRSTZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_INCR_PRSTZ = 8;
        public static final int TGA_INCR_PRSTZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_INCR_PRSTZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_INCR_PRSTZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
