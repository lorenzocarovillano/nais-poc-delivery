package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-QTZ-SP-Z-OPZ-DT-CA<br>
 * Variable: B03-QTZ-SP-Z-OPZ-DT-CA from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03QtzSpZOpzDtCa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03QtzSpZOpzDtCa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_QTZ_SP_Z_OPZ_DT_CA;
    }

    public void setB03QtzSpZOpzDtCa(AfDecimal b03QtzSpZOpzDtCa) {
        writeDecimalAsPacked(Pos.B03_QTZ_SP_Z_OPZ_DT_CA, b03QtzSpZOpzDtCa.copy());
    }

    public void setB03QtzSpZOpzDtCaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_QTZ_SP_Z_OPZ_DT_CA, Pos.B03_QTZ_SP_Z_OPZ_DT_CA);
    }

    /**Original name: B03-QTZ-SP-Z-OPZ-DT-CA<br>*/
    public AfDecimal getB03QtzSpZOpzDtCa() {
        return readPackedAsDecimal(Pos.B03_QTZ_SP_Z_OPZ_DT_CA, Len.Int.B03_QTZ_SP_Z_OPZ_DT_CA, Len.Fract.B03_QTZ_SP_Z_OPZ_DT_CA);
    }

    public byte[] getB03QtzSpZOpzDtCaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_QTZ_SP_Z_OPZ_DT_CA, Pos.B03_QTZ_SP_Z_OPZ_DT_CA);
        return buffer;
    }

    public void setB03QtzSpZOpzDtCaNull(String b03QtzSpZOpzDtCaNull) {
        writeString(Pos.B03_QTZ_SP_Z_OPZ_DT_CA_NULL, b03QtzSpZOpzDtCaNull, Len.B03_QTZ_SP_Z_OPZ_DT_CA_NULL);
    }

    /**Original name: B03-QTZ-SP-Z-OPZ-DT-CA-NULL<br>*/
    public String getB03QtzSpZOpzDtCaNull() {
        return readString(Pos.B03_QTZ_SP_Z_OPZ_DT_CA_NULL, Len.B03_QTZ_SP_Z_OPZ_DT_CA_NULL);
    }

    public String getB03QtzSpZOpzDtCaNullFormatted() {
        return Functions.padBlanks(getB03QtzSpZOpzDtCaNull(), Len.B03_QTZ_SP_Z_OPZ_DT_CA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_QTZ_SP_Z_OPZ_DT_CA = 1;
        public static final int B03_QTZ_SP_Z_OPZ_DT_CA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_QTZ_SP_Z_OPZ_DT_CA = 7;
        public static final int B03_QTZ_SP_Z_OPZ_DT_CA_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_QTZ_SP_Z_OPZ_DT_CA = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_QTZ_SP_Z_OPZ_DT_CA = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
