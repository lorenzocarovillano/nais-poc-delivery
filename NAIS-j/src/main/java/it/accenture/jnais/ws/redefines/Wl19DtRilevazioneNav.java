package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WL19-DT-RILEVAZIONE-NAV<br>
 * Variable: WL19-DT-RILEVAZIONE-NAV from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl19DtRilevazioneNav extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl19DtRilevazioneNav() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL19_DT_RILEVAZIONE_NAV;
    }

    public void setDl19DtRilevazioneNav(int dl19DtRilevazioneNav) {
        writeIntAsPacked(Pos.WL19_DT_RILEVAZIONE_NAV, dl19DtRilevazioneNav, Len.Int.DL19_DT_RILEVAZIONE_NAV);
    }

    public void setWl19DtRilevazioneNavFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL19_DT_RILEVAZIONE_NAV, Pos.WL19_DT_RILEVAZIONE_NAV);
    }

    /**Original name: DL19-DT-RILEVAZIONE-NAV<br>*/
    public int getDl19DtRilevazioneNav() {
        return readPackedAsInt(Pos.WL19_DT_RILEVAZIONE_NAV, Len.Int.DL19_DT_RILEVAZIONE_NAV);
    }

    public byte[] getWl19DtRilevazioneNavAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL19_DT_RILEVAZIONE_NAV, Pos.WL19_DT_RILEVAZIONE_NAV);
        return buffer;
    }

    public void initWl19DtRilevazioneNavSpaces() {
        fill(Pos.WL19_DT_RILEVAZIONE_NAV, Len.WL19_DT_RILEVAZIONE_NAV, Types.SPACE_CHAR);
    }

    public void setWl19DtRilevazioneNavNull(String wl19DtRilevazioneNavNull) {
        writeString(Pos.WL19_DT_RILEVAZIONE_NAV_NULL, wl19DtRilevazioneNavNull, Len.WL19_DT_RILEVAZIONE_NAV_NULL);
    }

    /**Original name: WL19-DT-RILEVAZIONE-NAV-NULL<br>*/
    public String getWl19DtRilevazioneNavNull() {
        return readString(Pos.WL19_DT_RILEVAZIONE_NAV_NULL, Len.WL19_DT_RILEVAZIONE_NAV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL19_DT_RILEVAZIONE_NAV = 1;
        public static final int WL19_DT_RILEVAZIONE_NAV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL19_DT_RILEVAZIONE_NAV = 5;
        public static final int WL19_DT_RILEVAZIONE_NAV_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DL19_DT_RILEVAZIONE_NAV = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
