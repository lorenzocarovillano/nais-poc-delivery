package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-DT-APPLZ-MORA<br>
 * Variable: WTIT-DT-APPLZ-MORA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitDtApplzMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitDtApplzMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_DT_APPLZ_MORA;
    }

    public void setWtitDtApplzMora(int wtitDtApplzMora) {
        writeIntAsPacked(Pos.WTIT_DT_APPLZ_MORA, wtitDtApplzMora, Len.Int.WTIT_DT_APPLZ_MORA);
    }

    public void setWtitDtApplzMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_DT_APPLZ_MORA, Pos.WTIT_DT_APPLZ_MORA);
    }

    /**Original name: WTIT-DT-APPLZ-MORA<br>*/
    public int getWtitDtApplzMora() {
        return readPackedAsInt(Pos.WTIT_DT_APPLZ_MORA, Len.Int.WTIT_DT_APPLZ_MORA);
    }

    public byte[] getWtitDtApplzMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_DT_APPLZ_MORA, Pos.WTIT_DT_APPLZ_MORA);
        return buffer;
    }

    public void initWtitDtApplzMoraSpaces() {
        fill(Pos.WTIT_DT_APPLZ_MORA, Len.WTIT_DT_APPLZ_MORA, Types.SPACE_CHAR);
    }

    public void setWtitDtApplzMoraNull(String wtitDtApplzMoraNull) {
        writeString(Pos.WTIT_DT_APPLZ_MORA_NULL, wtitDtApplzMoraNull, Len.WTIT_DT_APPLZ_MORA_NULL);
    }

    /**Original name: WTIT-DT-APPLZ-MORA-NULL<br>*/
    public String getWtitDtApplzMoraNull() {
        return readString(Pos.WTIT_DT_APPLZ_MORA_NULL, Len.WTIT_DT_APPLZ_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_DT_APPLZ_MORA = 1;
        public static final int WTIT_DT_APPLZ_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_DT_APPLZ_MORA = 5;
        public static final int WTIT_DT_APPLZ_MORA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_DT_APPLZ_MORA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
