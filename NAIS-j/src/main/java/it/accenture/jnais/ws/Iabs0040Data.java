package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import it.accenture.jnais.copy.BtcBatchDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndAnagDato;
import it.accenture.jnais.ws.enums.FlagOperazione;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IABS0040<br>
 * Generated as a class for rule WS.<br>*/
public class Iabs0040Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: FLAG-OPERAZIONE
    private FlagOperazione flagOperazione = new FlagOperazione();
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-BTC-BATCH
    private IndAnagDato indBtcBatch = new IndAnagDato();
    //Original name: BTC-BATCH-DB
    private BtcBatchDb btcBatchDb = new BtcBatchDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setAreaWhereConditionFormatted(String data) {
        byte[] buffer = new byte[Len.AREA_WHERE_CONDITION];
        MarshalByte.writeString(buffer, 1, data, Len.AREA_WHERE_CONDITION);
        setAreaWhereConditionBytes(buffer, 1);
    }

    public void setAreaWhereConditionBytes(byte[] buffer, int offset) {
        int position = offset;
        flagOperazione.setFlagOperazione(MarshalByte.readString(buffer, position, FlagOperazione.Len.FLAG_OPERAZIONE));
    }

    public BtcBatchDb getBtcBatchDb() {
        return btcBatchDb;
    }

    public FlagOperazione getFlagOperazione() {
        return flagOperazione;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndAnagDato getIndBtcBatch() {
        return indBtcBatch;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AREA_WHERE_CONDITION = FlagOperazione.Len.FLAG_OPERAZIONE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
