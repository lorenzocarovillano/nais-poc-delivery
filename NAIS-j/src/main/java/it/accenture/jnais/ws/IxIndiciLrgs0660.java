package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLrgs0660 {

    //==== PROPERTIES ====
    //Original name: IX-IND
    private short ind = DefaultValues.SHORT_VAL;
    //Original name: IX-REC-5
    private String rec5 = DefaultValues.stringVal(Len.REC5);
    //Original name: IX-REC-8
    private String rec8 = DefaultValues.stringVal(Len.REC8);
    //Original name: IX-REC-3
    private short rec3 = DefaultValues.SHORT_VAL;
    //Original name: IX-REC-10
    private short rec10 = DefaultValues.SHORT_VAL;
    //Original name: IX-REC-9
    private short rec9 = DefaultValues.SHORT_VAL;
    //Original name: IX-IND-P84
    private String indP84 = DefaultValues.stringVal(Len.IND_P84);
    //Original name: IX-TAB-P85
    private short tabP85 = DefaultValues.SHORT_VAL;
    //Original name: IX-CNTR-34
    private short cntr34 = DefaultValues.SHORT_VAL;
    //Original name: IX-580
    private short ix580 = DefaultValues.SHORT_VAL;
    //Original name: IX-GRAV
    private short grav = DefaultValues.SHORT_VAL;
    //Original name: IX-C37-TAB
    private short c37Tab = DefaultValues.SHORT_VAL;
    //Original name: IX-C35-TAB
    private short c35Tab = DefaultValues.SHORT_VAL;
    //Original name: IX-REC9-TP-MOVI
    private short rec9TpMovi = DefaultValues.SHORT_VAL;

    //==== METHODS ====
    public void setInd(short ind) {
        this.ind = ind;
    }

    public short getInd() {
        return this.ind;
    }

    public void setRec5(short rec5) {
        this.rec5 = NumericDisplay.asString(rec5, Len.REC5);
    }

    public void setRec5Formatted(String rec5) {
        this.rec5 = Trunc.toUnsignedNumeric(rec5, Len.REC5);
    }

    public short getRec5() {
        return NumericDisplay.asShort(this.rec5);
    }

    public void setRec8(short rec8) {
        this.rec8 = NumericDisplay.asString(rec8, Len.REC8);
    }

    public void setRec8Formatted(String rec8) {
        this.rec8 = Trunc.toUnsignedNumeric(rec8, Len.REC8);
    }

    public short getRec8() {
        return NumericDisplay.asShort(this.rec8);
    }

    public void setRec3(short rec3) {
        this.rec3 = rec3;
    }

    public short getRec3() {
        return this.rec3;
    }

    public void setRec10(short rec10) {
        this.rec10 = rec10;
    }

    public short getRec10() {
        return this.rec10;
    }

    public void setRec9(short rec9) {
        this.rec9 = rec9;
    }

    public short getRec9() {
        return this.rec9;
    }

    public void setIndP84Formatted(String indP84) {
        this.indP84 = Trunc.toUnsignedNumeric(indP84, Len.IND_P84);
    }

    public short getIndP84() {
        return NumericDisplay.asShort(this.indP84);
    }

    public void setTabP85(short tabP85) {
        this.tabP85 = tabP85;
    }

    public short getTabP85() {
        return this.tabP85;
    }

    public void setCntr34(short cntr34) {
        this.cntr34 = cntr34;
    }

    public short getCntr34() {
        return this.cntr34;
    }

    public void setIx580(short ix580) {
        this.ix580 = ix580;
    }

    public short getIx580() {
        return this.ix580;
    }

    public void setGrav(short grav) {
        this.grav = grav;
    }

    public short getGrav() {
        return this.grav;
    }

    public void setC37Tab(short c37Tab) {
        this.c37Tab = c37Tab;
    }

    public short getC37Tab() {
        return this.c37Tab;
    }

    public void setC35Tab(short c35Tab) {
        this.c35Tab = c35Tab;
    }

    public short getC35Tab() {
        return this.c35Tab;
    }

    public void setRec9TpMovi(short rec9TpMovi) {
        this.rec9TpMovi = rec9TpMovi;
    }

    public short getRec9TpMovi() {
        return this.rec9TpMovi;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int REC5 = 4;
        public static final int REC8 = 4;
        public static final int IND_P84 = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
