package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.copy.Lccvtli1;
import it.accenture.jnais.copy.WtliDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WTLI-TAB-TRCH-LIQ<br>
 * Variables: WTLI-TAB-TRCH-LIQ from copybook LCCVTLIB<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WtliTabTrchLiq implements ICopyable<WtliTabTrchLiq> {

    //==== PROPERTIES ====
    //Original name: LCCVTLI1
    private Lccvtli1 lccvtli1 = new Lccvtli1();

    //==== CONSTRUCTORS ====
    public WtliTabTrchLiq() {
    }

    public WtliTabTrchLiq(WtliTabTrchLiq wtliTabTrchLiq) {
        this();
        this.lccvtli1 = ((Lccvtli1)wtliTabTrchLiq.lccvtli1.copy());
    }

    //==== METHODS ====
    public void setWtliTabTrchLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvtli1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvtli1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvtli1.Len.Int.ID_PTF, 0));
        position += Lccvtli1.Len.ID_PTF;
        lccvtli1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWtliTabTrchLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvtli1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvtli1.getIdPtf(), Lccvtli1.Len.Int.ID_PTF, 0);
        position += Lccvtli1.Len.ID_PTF;
        lccvtli1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public WtliTabTrchLiq initWtliTabTrchLiqSpaces() {
        lccvtli1.initLccvtli1Spaces();
        return this;
    }

    public Lccvtli1 getLccvtli1() {
        return lccvtli1;
    }

    public WtliTabTrchLiq copy() {
        return new WtliTabTrchLiq(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_TAB_TRCH_LIQ = WpolStatus.Len.STATUS + Lccvtli1.Len.ID_PTF + WtliDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
