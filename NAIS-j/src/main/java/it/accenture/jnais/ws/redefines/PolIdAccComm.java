package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-ID-ACC-COMM<br>
 * Variable: POL-ID-ACC-COMM from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolIdAccComm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolIdAccComm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_ID_ACC_COMM;
    }

    public void setPolIdAccComm(int polIdAccComm) {
        writeIntAsPacked(Pos.POL_ID_ACC_COMM, polIdAccComm, Len.Int.POL_ID_ACC_COMM);
    }

    public void setPolIdAccCommFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_ID_ACC_COMM, Pos.POL_ID_ACC_COMM);
    }

    /**Original name: POL-ID-ACC-COMM<br>*/
    public int getPolIdAccComm() {
        return readPackedAsInt(Pos.POL_ID_ACC_COMM, Len.Int.POL_ID_ACC_COMM);
    }

    public byte[] getPolIdAccCommAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_ID_ACC_COMM, Pos.POL_ID_ACC_COMM);
        return buffer;
    }

    public void setPolIdAccCommNull(String polIdAccCommNull) {
        writeString(Pos.POL_ID_ACC_COMM_NULL, polIdAccCommNull, Len.POL_ID_ACC_COMM_NULL);
    }

    /**Original name: POL-ID-ACC-COMM-NULL<br>*/
    public String getPolIdAccCommNull() {
        return readString(Pos.POL_ID_ACC_COMM_NULL, Len.POL_ID_ACC_COMM_NULL);
    }

    public String getPolIdAccCommNullFormatted() {
        return Functions.padBlanks(getPolIdAccCommNull(), Len.POL_ID_ACC_COMM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_ID_ACC_COMM = 1;
        public static final int POL_ID_ACC_COMM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_ID_ACC_COMM = 5;
        public static final int POL_ID_ACC_COMM_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_ID_ACC_COMM = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
