package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDAD-IMP-PRE-DFLT<br>
 * Variable: WDAD-IMP-PRE-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadImpPreDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadImpPreDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_IMP_PRE_DFLT;
    }

    public void setWdadImpPreDflt(AfDecimal wdadImpPreDflt) {
        writeDecimalAsPacked(Pos.WDAD_IMP_PRE_DFLT, wdadImpPreDflt.copy());
    }

    public void setWdadImpPreDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_IMP_PRE_DFLT, Pos.WDAD_IMP_PRE_DFLT);
    }

    /**Original name: WDAD-IMP-PRE-DFLT<br>*/
    public AfDecimal getWdadImpPreDflt() {
        return readPackedAsDecimal(Pos.WDAD_IMP_PRE_DFLT, Len.Int.WDAD_IMP_PRE_DFLT, Len.Fract.WDAD_IMP_PRE_DFLT);
    }

    public byte[] getWdadImpPreDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_IMP_PRE_DFLT, Pos.WDAD_IMP_PRE_DFLT);
        return buffer;
    }

    public void setWdadImpPreDfltNull(String wdadImpPreDfltNull) {
        writeString(Pos.WDAD_IMP_PRE_DFLT_NULL, wdadImpPreDfltNull, Len.WDAD_IMP_PRE_DFLT_NULL);
    }

    /**Original name: WDAD-IMP-PRE-DFLT-NULL<br>*/
    public String getWdadImpPreDfltNull() {
        return readString(Pos.WDAD_IMP_PRE_DFLT_NULL, Len.WDAD_IMP_PRE_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_PRE_DFLT = 1;
        public static final int WDAD_IMP_PRE_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_PRE_DFLT = 8;
        public static final int WDAD_IMP_PRE_DFLT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_PRE_DFLT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_PRE_DFLT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
