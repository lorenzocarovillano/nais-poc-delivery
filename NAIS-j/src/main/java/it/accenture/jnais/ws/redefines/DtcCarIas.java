package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-CAR-IAS<br>
 * Variable: DTC-CAR-IAS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcCarIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcCarIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_CAR_IAS;
    }

    public void setDtcCarIas(AfDecimal dtcCarIas) {
        writeDecimalAsPacked(Pos.DTC_CAR_IAS, dtcCarIas.copy());
    }

    public void setDtcCarIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_CAR_IAS, Pos.DTC_CAR_IAS);
    }

    /**Original name: DTC-CAR-IAS<br>*/
    public AfDecimal getDtcCarIas() {
        return readPackedAsDecimal(Pos.DTC_CAR_IAS, Len.Int.DTC_CAR_IAS, Len.Fract.DTC_CAR_IAS);
    }

    public byte[] getDtcCarIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_CAR_IAS, Pos.DTC_CAR_IAS);
        return buffer;
    }

    public void setDtcCarIasNull(String dtcCarIasNull) {
        writeString(Pos.DTC_CAR_IAS_NULL, dtcCarIasNull, Len.DTC_CAR_IAS_NULL);
    }

    /**Original name: DTC-CAR-IAS-NULL<br>*/
    public String getDtcCarIasNull() {
        return readString(Pos.DTC_CAR_IAS_NULL, Len.DTC_CAR_IAS_NULL);
    }

    public String getDtcCarIasNullFormatted() {
        return Functions.padBlanks(getDtcCarIasNull(), Len.DTC_CAR_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_CAR_IAS = 1;
        public static final int DTC_CAR_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_CAR_IAS = 8;
        public static final int DTC_CAR_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_CAR_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_CAR_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
