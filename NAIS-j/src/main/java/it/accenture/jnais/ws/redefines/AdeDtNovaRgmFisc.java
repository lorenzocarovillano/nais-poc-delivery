package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-DT-NOVA-RGM-FISC<br>
 * Variable: ADE-DT-NOVA-RGM-FISC from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeDtNovaRgmFisc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeDtNovaRgmFisc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_DT_NOVA_RGM_FISC;
    }

    public void setAdeDtNovaRgmFisc(int adeDtNovaRgmFisc) {
        writeIntAsPacked(Pos.ADE_DT_NOVA_RGM_FISC, adeDtNovaRgmFisc, Len.Int.ADE_DT_NOVA_RGM_FISC);
    }

    public void setAdeDtNovaRgmFiscFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_DT_NOVA_RGM_FISC, Pos.ADE_DT_NOVA_RGM_FISC);
    }

    /**Original name: ADE-DT-NOVA-RGM-FISC<br>*/
    public int getAdeDtNovaRgmFisc() {
        return readPackedAsInt(Pos.ADE_DT_NOVA_RGM_FISC, Len.Int.ADE_DT_NOVA_RGM_FISC);
    }

    public byte[] getAdeDtNovaRgmFiscAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_DT_NOVA_RGM_FISC, Pos.ADE_DT_NOVA_RGM_FISC);
        return buffer;
    }

    public void setAdeDtNovaRgmFiscNull(String adeDtNovaRgmFiscNull) {
        writeString(Pos.ADE_DT_NOVA_RGM_FISC_NULL, adeDtNovaRgmFiscNull, Len.ADE_DT_NOVA_RGM_FISC_NULL);
    }

    /**Original name: ADE-DT-NOVA-RGM-FISC-NULL<br>*/
    public String getAdeDtNovaRgmFiscNull() {
        return readString(Pos.ADE_DT_NOVA_RGM_FISC_NULL, Len.ADE_DT_NOVA_RGM_FISC_NULL);
    }

    public String getAdeDtNovaRgmFiscNullFormatted() {
        return Functions.padBlanks(getAdeDtNovaRgmFiscNull(), Len.ADE_DT_NOVA_RGM_FISC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_DT_NOVA_RGM_FISC = 1;
        public static final int ADE_DT_NOVA_RGM_FISC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_DT_NOVA_RGM_FISC = 5;
        public static final int ADE_DT_NOVA_RGM_FISC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_DT_NOVA_RGM_FISC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
