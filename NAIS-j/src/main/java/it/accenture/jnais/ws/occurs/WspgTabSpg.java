package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvspg1;
import it.accenture.jnais.copy.WspgDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WSPG-TAB-SPG<br>
 * Variables: WSPG-TAB-SPG from copybook LCCVSPGA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WspgTabSpg {

    //==== PROPERTIES ====
    //Original name: LCCVSPG1
    private Lccvspg1 lccvspg1 = new Lccvspg1();

    //==== METHODS ====
    public void setVspgTabSpgBytes(byte[] buffer) {
        setWspgTabSpgBytes(buffer, 1);
    }

    public byte[] getTabSpgBytes() {
        byte[] buffer = new byte[Len.WSPG_TAB_SPG];
        return getWspgTabSpgBytes(buffer, 1);
    }

    public void setWspgTabSpgBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvspg1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvspg1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvspg1.Len.Int.ID_PTF, 0));
        position += Lccvspg1.Len.ID_PTF;
        lccvspg1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWspgTabSpgBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvspg1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvspg1.getIdPtf(), Lccvspg1.Len.Int.ID_PTF, 0);
        position += Lccvspg1.Len.ID_PTF;
        lccvspg1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWspgTabSpgSpaces() {
        lccvspg1.initLccvspg1Spaces();
    }

    public Lccvspg1 getLccvspg1() {
        return lccvspg1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WSPG_TAB_SPG = WpolStatus.Len.STATUS + Lccvspg1.Len.ID_PTF + WspgDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
