package it.accenture.jnais.ws;

/**Original name: WK-COSTANTI-TABELLE<br>
 * Variable: WK-COSTANTI-TABELLE from program LOAS0310<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkCostantiTabelle {

    //==== PROPERTIES ====
    //Original name: WK-MAX-ADE
    private short maxAde = ((short)1);
    //Original name: WK-MAX-TIT-W870
    private short maxTitW870 = ((short)12);
    //Original name: WK-MAX-VAR
    private short maxVar = ((short)70);

    //==== METHODS ====
    public short getMaxAde() {
        return this.maxAde;
    }

    public short getMaxTitW870() {
        return this.maxTitW870;
    }

    public short getMaxVar() {
        return this.maxVar;
    }
}
