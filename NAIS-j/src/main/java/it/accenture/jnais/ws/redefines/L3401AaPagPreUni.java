package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-AA-PAG-PRE-UNI<br>
 * Variable: L3401-AA-PAG-PRE-UNI from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401AaPagPreUni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401AaPagPreUni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_AA_PAG_PRE_UNI;
    }

    public void setL3401AaPagPreUni(int l3401AaPagPreUni) {
        writeIntAsPacked(Pos.L3401_AA_PAG_PRE_UNI, l3401AaPagPreUni, Len.Int.L3401_AA_PAG_PRE_UNI);
    }

    public void setL3401AaPagPreUniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_AA_PAG_PRE_UNI, Pos.L3401_AA_PAG_PRE_UNI);
    }

    /**Original name: L3401-AA-PAG-PRE-UNI<br>*/
    public int getL3401AaPagPreUni() {
        return readPackedAsInt(Pos.L3401_AA_PAG_PRE_UNI, Len.Int.L3401_AA_PAG_PRE_UNI);
    }

    public byte[] getL3401AaPagPreUniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_AA_PAG_PRE_UNI, Pos.L3401_AA_PAG_PRE_UNI);
        return buffer;
    }

    /**Original name: L3401-AA-PAG-PRE-UNI-NULL<br>*/
    public String getL3401AaPagPreUniNull() {
        return readString(Pos.L3401_AA_PAG_PRE_UNI_NULL, Len.L3401_AA_PAG_PRE_UNI_NULL);
    }

    public String getL3401AaPagPreUniNullFormatted() {
        return Functions.padBlanks(getL3401AaPagPreUniNull(), Len.L3401_AA_PAG_PRE_UNI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_AA_PAG_PRE_UNI = 1;
        public static final int L3401_AA_PAG_PRE_UNI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_AA_PAG_PRE_UNI = 3;
        public static final int L3401_AA_PAG_PRE_UNI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_AA_PAG_PRE_UNI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
