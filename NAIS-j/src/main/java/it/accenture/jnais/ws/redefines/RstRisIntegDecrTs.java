package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-INTEG-DECR-TS<br>
 * Variable: RST-RIS-INTEG-DECR-TS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisIntegDecrTs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisIntegDecrTs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_INTEG_DECR_TS;
    }

    public void setRstRisIntegDecrTs(AfDecimal rstRisIntegDecrTs) {
        writeDecimalAsPacked(Pos.RST_RIS_INTEG_DECR_TS, rstRisIntegDecrTs.copy());
    }

    public void setRstRisIntegDecrTsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_INTEG_DECR_TS, Pos.RST_RIS_INTEG_DECR_TS);
    }

    /**Original name: RST-RIS-INTEG-DECR-TS<br>*/
    public AfDecimal getRstRisIntegDecrTs() {
        return readPackedAsDecimal(Pos.RST_RIS_INTEG_DECR_TS, Len.Int.RST_RIS_INTEG_DECR_TS, Len.Fract.RST_RIS_INTEG_DECR_TS);
    }

    public byte[] getRstRisIntegDecrTsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_INTEG_DECR_TS, Pos.RST_RIS_INTEG_DECR_TS);
        return buffer;
    }

    public void setRstRisIntegDecrTsNull(String rstRisIntegDecrTsNull) {
        writeString(Pos.RST_RIS_INTEG_DECR_TS_NULL, rstRisIntegDecrTsNull, Len.RST_RIS_INTEG_DECR_TS_NULL);
    }

    /**Original name: RST-RIS-INTEG-DECR-TS-NULL<br>*/
    public String getRstRisIntegDecrTsNull() {
        return readString(Pos.RST_RIS_INTEG_DECR_TS_NULL, Len.RST_RIS_INTEG_DECR_TS_NULL);
    }

    public String getRstRisIntegDecrTsNullFormatted() {
        return Functions.padBlanks(getRstRisIntegDecrTsNull(), Len.RST_RIS_INTEG_DECR_TS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_INTEG_DECR_TS = 1;
        public static final int RST_RIS_INTEG_DECR_TS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_INTEG_DECR_TS = 8;
        public static final int RST_RIS_INTEG_DECR_TS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_INTEG_DECR_TS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_INTEG_DECR_TS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
