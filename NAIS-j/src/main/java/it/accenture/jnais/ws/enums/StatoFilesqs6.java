package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: STATO-FILESQS6<br>
 * Variable: STATO-FILESQS6 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class StatoFilesqs6 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char APERTO = 'A';
    public static final char CHIUSO = 'C';

    //==== METHODS ====
    public void setStatoFilesqs6(char statoFilesqs6) {
        this.value = statoFilesqs6;
    }

    public char getStatoFilesqs6() {
        return this.value;
    }

    public boolean isFilesqs6Aperto() {
        return value == APERTO;
    }

    public void setFilesqs6Aperto() {
        value = APERTO;
    }

    public void setFilesqs6Chiuso() {
        value = CHIUSO;
    }
}
