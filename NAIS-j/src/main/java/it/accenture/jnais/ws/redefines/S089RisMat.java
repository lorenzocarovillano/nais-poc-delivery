package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-RIS-MAT<br>
 * Variable: S089-RIS-MAT from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089RisMat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089RisMat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_RIS_MAT;
    }

    public void setWlquRisMat(AfDecimal wlquRisMat) {
        writeDecimalAsPacked(Pos.S089_RIS_MAT, wlquRisMat.copy());
    }

    public void setWlquRisMatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_RIS_MAT, Pos.S089_RIS_MAT);
    }

    /**Original name: WLQU-RIS-MAT<br>*/
    public AfDecimal getWlquRisMat() {
        return readPackedAsDecimal(Pos.S089_RIS_MAT, Len.Int.WLQU_RIS_MAT, Len.Fract.WLQU_RIS_MAT);
    }

    public byte[] getWlquRisMatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_RIS_MAT, Pos.S089_RIS_MAT);
        return buffer;
    }

    public void initWlquRisMatSpaces() {
        fill(Pos.S089_RIS_MAT, Len.S089_RIS_MAT, Types.SPACE_CHAR);
    }

    public void setWlquRisMatNull(String wlquRisMatNull) {
        writeString(Pos.S089_RIS_MAT_NULL, wlquRisMatNull, Len.WLQU_RIS_MAT_NULL);
    }

    /**Original name: WLQU-RIS-MAT-NULL<br>*/
    public String getWlquRisMatNull() {
        return readString(Pos.S089_RIS_MAT_NULL, Len.WLQU_RIS_MAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_RIS_MAT = 1;
        public static final int S089_RIS_MAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_RIS_MAT = 8;
        public static final int WLQU_RIS_MAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_RIS_MAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_RIS_MAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
