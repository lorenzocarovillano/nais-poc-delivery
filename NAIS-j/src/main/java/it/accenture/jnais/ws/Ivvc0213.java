package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.C214DatiLivello;
import it.accenture.jnais.copy.C214TabOutput;
import it.accenture.jnais.ws.enums.C214FlgArea;
import it.accenture.jnais.ws.enums.C214ModalitaIdAde;
import it.accenture.jnais.ws.enums.C214ModalitaIdGrz;
import it.accenture.jnais.ws.enums.C214ModalitaIdPol;
import it.accenture.jnais.ws.enums.C214ModalitaIdTga;
import it.accenture.jnais.ws.enums.C214ModalitaIdTit;
import it.accenture.jnais.ws.enums.C214SaltaValorizzazione;
import it.accenture.jnais.ws.occurs.C214TabInfo;

/**Original name: AREA-IO-CALCOLI<br>
 * Variable: AREA-IO-CALCOLI from program IVVS0216<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ivvc0213 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_INFO_MAXOCCURS = 10;
    /**Original name: C214-COD-PARAMETRO<br>
	 * <pre>----------------------------------------------------------------*
	 *  AREA CHIAMATA MODULI DI CALCOLO VALORIZZATORE
	 * ----------------------------------------------------------------*</pre>*/
    private String codParametro = DefaultValues.stringVal(Len.COD_PARAMETRO);
    //Original name: C214-ID-POLIZZA
    private String idPolizza = DefaultValues.stringVal(Len.ID_POLIZZA);
    //Original name: C214-ID-ADESIONE
    private String idAdesione = DefaultValues.stringVal(Len.ID_ADESIONE);
    //Original name: C214-ID-GARANZIA
    private String idGaranzia = DefaultValues.stringVal(Len.ID_GARANZIA);
    //Original name: C214-ID-TRANCHE
    private String idTranche = DefaultValues.stringVal(Len.ID_TRANCHE);
    //Original name: C214-ID-TIT-CONT
    private String idTitCont = DefaultValues.stringVal(Len.ID_TIT_CONT);
    //Original name: C214-TIPO-MOVIMENTO
    private String tipoMovimento = DefaultValues.stringVal(Len.TIPO_MOVIMENTO);
    //Original name: C214-TIPO-MOVI-ORIG
    private String tipoMoviOrig = DefaultValues.stringVal(Len.TIPO_MOVI_ORIG);
    //Original name: C214-IX-TABB
    private short ixTabb = DefaultValues.SHORT_VAL;
    //Original name: C214-DATA-EFFETTO
    private String dataEffetto = DefaultValues.stringVal(Len.DATA_EFFETTO);
    //Original name: C214-DATA-COMPETENZA
    private long dataCompetenza = DefaultValues.LONG_VAL;
    //Original name: C214-DATA-COMP-AGG-STOR
    private long dataCompAggStor = DefaultValues.LONG_VAL;
    //Original name: C214-COD-COMPAGNIA-ANIA
    private String codCompagniaAnia = DefaultValues.stringVal(Len.COD_COMPAGNIA_ANIA);
    //Original name: C214-ID-POL-GAR
    private String idPolGar = DefaultValues.stringVal(Len.ID_POL_GAR);
    //Original name: C214-DATI-LIVELLO
    private C214DatiLivello datiLivello = new C214DatiLivello();
    //Original name: C214-WHERE-COND
    private String whereCond = DefaultValues.stringVal(Len.WHERE_COND);
    //Original name: C214-LUNGHEZZA-TOT
    private int lunghezzaTot = DefaultValues.INT_VAL;
    //Original name: C214-ELE-INFO-MAX
    private short eleInfoMax = DefaultValues.SHORT_VAL;
    //Original name: C214-TAB-INFO
    private C214TabInfo[] tabInfo = new C214TabInfo[TAB_INFO_MAXOCCURS];
    //Original name: C214-BUFFER-DATI
    private String bufferDati = DefaultValues.stringVal(Len.BUFFER_DATI);
    //Original name: C214-MODALITA-ID-POL
    private C214ModalitaIdPol modalitaIdPol = new C214ModalitaIdPol();
    //Original name: C214-MODALITA-ID-ADE
    private C214ModalitaIdAde modalitaIdAde = new C214ModalitaIdAde();
    //Original name: C214-MODALITA-ID-GRZ
    private C214ModalitaIdGrz modalitaIdGrz = new C214ModalitaIdGrz();
    //Original name: C214-MODALITA-ID-TGA
    private C214ModalitaIdTga modalitaIdTga = new C214ModalitaIdTga();
    //Original name: C214-MODALITA-ID-TIT
    private C214ModalitaIdTit modalitaIdTit = new C214ModalitaIdTit();
    //Original name: C214-SALTA-VALORIZZAZIONE
    private C214SaltaValorizzazione saltaValorizzazione = new C214SaltaValorizzazione();
    //Original name: C214-TAB-OUTPUT
    private C214TabOutput tabOutput = new C214TabOutput();
    //Original name: C214-FLG-AREA
    private C214FlgArea flgArea = new C214FlgArea();
    //Original name: FILLER-AREA-IO-CALCOLI
    private String flr1 = DefaultValues.stringVal(Len.FLR1);

    //==== CONSTRUCTORS ====
    public Ivvc0213() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IVVC0213;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIvvc0213Bytes(buf);
    }

    public void init() {
        for (int tabInfoIdx = 1; tabInfoIdx <= TAB_INFO_MAXOCCURS; tabInfoIdx++) {
            tabInfo[tabInfoIdx - 1] = new C214TabInfo();
        }
    }

    public String getIvvc0213Formatted() {
        return MarshalByteExt.bufferToStr(getIvvc0213Bytes());
    }

    public void setIvvc0213Bytes(byte[] buffer) {
        setIvvc0213Bytes(buffer, 1);
    }

    public byte[] getIvvc0213Bytes() {
        byte[] buffer = new byte[Len.IVVC0213];
        return getIvvc0213Bytes(buffer, 1);
    }

    public void setIvvc0213Bytes(byte[] buffer, int offset) {
        int position = offset;
        codParametro = MarshalByte.readString(buffer, position, Len.COD_PARAMETRO);
        position += Len.COD_PARAMETRO;
        idPolizza = MarshalByte.readFixedString(buffer, position, Len.ID_POLIZZA);
        position += Len.ID_POLIZZA;
        idAdesione = MarshalByte.readFixedString(buffer, position, Len.ID_ADESIONE);
        position += Len.ID_ADESIONE;
        idGaranzia = MarshalByte.readFixedString(buffer, position, Len.ID_GARANZIA);
        position += Len.ID_GARANZIA;
        idTranche = MarshalByte.readFixedString(buffer, position, Len.ID_TRANCHE);
        position += Len.ID_TRANCHE;
        idTitCont = MarshalByte.readFixedString(buffer, position, Len.ID_TIT_CONT);
        position += Len.ID_TIT_CONT;
        tipoMovimento = MarshalByte.readFixedString(buffer, position, Len.TIPO_MOVIMENTO);
        position += Len.TIPO_MOVIMENTO;
        tipoMoviOrig = MarshalByte.readFixedString(buffer, position, Len.TIPO_MOVI_ORIG);
        position += Len.TIPO_MOVI_ORIG;
        ixTabb = MarshalByte.readShort(buffer, position, Len.IX_TABB, SignType.NO_SIGN);
        position += Len.IX_TABB;
        dataEffetto = MarshalByte.readFixedString(buffer, position, Len.DATA_EFFETTO);
        position += Len.DATA_EFFETTO;
        dataCompetenza = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DATA_COMPETENZA, 0);
        position += Len.DATA_COMPETENZA;
        dataCompAggStor = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DATA_COMP_AGG_STOR, 0);
        position += Len.DATA_COMP_AGG_STOR;
        codCompagniaAnia = MarshalByte.readFixedString(buffer, position, Len.COD_COMPAGNIA_ANIA);
        position += Len.COD_COMPAGNIA_ANIA;
        setTabLivelloBytes(buffer, position);
        position += Len.TAB_LIVELLO;
        whereCond = MarshalByte.readString(buffer, position, Len.WHERE_COND);
        position += Len.WHERE_COND;
        lunghezzaTot = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LUNGHEZZA_TOT, 0);
        position += Len.LUNGHEZZA_TOT;
        eleInfoMax = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_INFO_MAX, 0);
        position += Len.ELE_INFO_MAX;
        for (int idx = 1; idx <= TAB_INFO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabInfo[idx - 1].setTabInfoBytes(buffer, position);
                position += C214TabInfo.Len.TAB_INFO;
            }
            else {
                tabInfo[idx - 1].initTabInfoSpaces();
                position += C214TabInfo.Len.TAB_INFO;
            }
        }
        bufferDati = MarshalByte.readString(buffer, position, Len.BUFFER_DATI);
        position += Len.BUFFER_DATI;
        modalitaIdPol.setModalitaIdPol(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        modalitaIdAde.setModalitaIdAde(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        modalitaIdGrz.setModalitaIdGrz(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        modalitaIdTga.setModalitaIdTga(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        modalitaIdTit.setModalitaIdTit(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        saltaValorizzazione.setSaltaValorizzazione(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        tabOutput.setTabOutputBytes(buffer, position);
        position += C214TabOutput.Len.TAB_OUTPUT;
        flgArea.setFlgArea(MarshalByte.readString(buffer, position, C214FlgArea.Len.FLG_AREA));
        position += C214FlgArea.Len.FLG_AREA;
        flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
    }

    public byte[] getIvvc0213Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codParametro, Len.COD_PARAMETRO);
        position += Len.COD_PARAMETRO;
        MarshalByte.writeString(buffer, position, idPolizza, Len.ID_POLIZZA);
        position += Len.ID_POLIZZA;
        MarshalByte.writeString(buffer, position, idAdesione, Len.ID_ADESIONE);
        position += Len.ID_ADESIONE;
        MarshalByte.writeString(buffer, position, idGaranzia, Len.ID_GARANZIA);
        position += Len.ID_GARANZIA;
        MarshalByte.writeString(buffer, position, idTranche, Len.ID_TRANCHE);
        position += Len.ID_TRANCHE;
        MarshalByte.writeString(buffer, position, idTitCont, Len.ID_TIT_CONT);
        position += Len.ID_TIT_CONT;
        MarshalByte.writeString(buffer, position, tipoMovimento, Len.TIPO_MOVIMENTO);
        position += Len.TIPO_MOVIMENTO;
        MarshalByte.writeString(buffer, position, tipoMoviOrig, Len.TIPO_MOVI_ORIG);
        position += Len.TIPO_MOVI_ORIG;
        MarshalByte.writeShort(buffer, position, ixTabb, Len.IX_TABB, SignType.NO_SIGN);
        position += Len.IX_TABB;
        MarshalByte.writeString(buffer, position, dataEffetto, Len.DATA_EFFETTO);
        position += Len.DATA_EFFETTO;
        MarshalByte.writeLongAsPacked(buffer, position, dataCompetenza, Len.Int.DATA_COMPETENZA, 0);
        position += Len.DATA_COMPETENZA;
        MarshalByte.writeLongAsPacked(buffer, position, dataCompAggStor, Len.Int.DATA_COMP_AGG_STOR, 0);
        position += Len.DATA_COMP_AGG_STOR;
        MarshalByte.writeString(buffer, position, codCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
        position += Len.COD_COMPAGNIA_ANIA;
        getTabLivelloBytes(buffer, position);
        position += Len.TAB_LIVELLO;
        MarshalByte.writeString(buffer, position, whereCond, Len.WHERE_COND);
        position += Len.WHERE_COND;
        MarshalByte.writeIntAsPacked(buffer, position, lunghezzaTot, Len.Int.LUNGHEZZA_TOT, 0);
        position += Len.LUNGHEZZA_TOT;
        MarshalByte.writeShortAsPacked(buffer, position, eleInfoMax, Len.Int.ELE_INFO_MAX, 0);
        position += Len.ELE_INFO_MAX;
        for (int idx = 1; idx <= TAB_INFO_MAXOCCURS; idx++) {
            tabInfo[idx - 1].getTabInfoBytes(buffer, position);
            position += C214TabInfo.Len.TAB_INFO;
        }
        MarshalByte.writeString(buffer, position, bufferDati, Len.BUFFER_DATI);
        position += Len.BUFFER_DATI;
        MarshalByte.writeChar(buffer, position, modalitaIdPol.getModalitaIdPol());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, modalitaIdAde.getModalitaIdAde());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, modalitaIdGrz.getModalitaIdGrz());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, modalitaIdTga.getModalitaIdTga());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, modalitaIdTit.getModalitaIdTit());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, saltaValorizzazione.getSaltaValorizzazione());
        position += Types.CHAR_SIZE;
        tabOutput.getTabOutputBytes(buffer, position);
        position += C214TabOutput.Len.TAB_OUTPUT;
        MarshalByte.writeString(buffer, position, flgArea.getFlgArea(), C214FlgArea.Len.FLG_AREA);
        position += C214FlgArea.Len.FLG_AREA;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        return buffer;
    }

    public void setCodParametro(String codParametro) {
        this.codParametro = Functions.subString(codParametro, Len.COD_PARAMETRO);
    }

    public String getCodParametro() {
        return this.codParametro;
    }

    public String getCodParametroFormatted() {
        return Functions.padBlanks(getCodParametro(), Len.COD_PARAMETRO);
    }

    public void setIdPolizza(int idPolizza) {
        this.idPolizza = NumericDisplay.asString(idPolizza, Len.ID_POLIZZA);
    }

    public int getIdPolizza() {
        return NumericDisplay.asInt(this.idPolizza);
    }

    public String getIdPolizzaFormatted() {
        return this.idPolizza;
    }

    public void setIdAdesione(int idAdesione) {
        this.idAdesione = NumericDisplay.asString(idAdesione, Len.ID_ADESIONE);
    }

    public int getIdAdesione() {
        return NumericDisplay.asInt(this.idAdesione);
    }

    public String getIdAdesioneFormatted() {
        return this.idAdesione;
    }

    public void setIdGaranziaFormatted(String idGaranzia) {
        this.idGaranzia = Trunc.toUnsignedNumeric(idGaranzia, Len.ID_GARANZIA);
    }

    public int getIdGaranzia() {
        return NumericDisplay.asInt(this.idGaranzia);
    }

    public String getIdGaranziaFormatted() {
        return this.idGaranzia;
    }

    public void setIdTrancheFormatted(String idTranche) {
        this.idTranche = Trunc.toUnsignedNumeric(idTranche, Len.ID_TRANCHE);
    }

    public int getIdTranche() {
        return NumericDisplay.asInt(this.idTranche);
    }

    public String getIdTrancheFormatted() {
        return this.idTranche;
    }

    public void setIdTitCont(int idTitCont) {
        this.idTitCont = NumericDisplay.asString(idTitCont, Len.ID_TIT_CONT);
    }

    public int getIdTitCont() {
        return NumericDisplay.asInt(this.idTitCont);
    }

    public void setTipoMovimentoFormatted(String tipoMovimento) {
        this.tipoMovimento = Trunc.toUnsignedNumeric(tipoMovimento, Len.TIPO_MOVIMENTO);
    }

    public int getTipoMovimento() {
        return NumericDisplay.asInt(this.tipoMovimento);
    }

    public String getTipoMovimentoFormatted() {
        return this.tipoMovimento;
    }

    public void setTipoMoviOrigFormatted(String tipoMoviOrig) {
        this.tipoMoviOrig = Trunc.toUnsignedNumeric(tipoMoviOrig, Len.TIPO_MOVI_ORIG);
    }

    public int getTipoMoviOrig() {
        return NumericDisplay.asInt(this.tipoMoviOrig);
    }

    public String getTipoMoviOrigFormatted() {
        return this.tipoMoviOrig;
    }

    public void setIxTabb(short ixTabb) {
        this.ixTabb = ixTabb;
    }

    public short getIxTabb() {
        return this.ixTabb;
    }

    public void setDataEffetto(int dataEffetto) {
        this.dataEffetto = NumericDisplay.asString(dataEffetto, Len.DATA_EFFETTO);
    }

    public int getDataEffetto() {
        return NumericDisplay.asInt(this.dataEffetto);
    }

    public String getDataEffettoFormatted() {
        return this.dataEffetto;
    }

    public void setDataCompetenza(long dataCompetenza) {
        this.dataCompetenza = dataCompetenza;
    }

    public long getDataCompetenza() {
        return this.dataCompetenza;
    }

    public void setDataCompAggStor(long dataCompAggStor) {
        this.dataCompAggStor = dataCompAggStor;
    }

    public long getDataCompAggStor() {
        return this.dataCompAggStor;
    }

    public void setCodCompagniaAnia(int codCompagniaAnia) {
        this.codCompagniaAnia = NumericDisplay.asString(codCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
    }

    public int getCodCompagniaAnia() {
        return NumericDisplay.asInt(this.codCompagniaAnia);
    }

    public void setTabLivelloBytes(byte[] buffer, int offset) {
        int position = offset;
        idPolGar = MarshalByte.readFixedString(buffer, position, Len.ID_POL_GAR);
        position += Len.ID_POL_GAR;
        datiLivello.setDatiLivelloBytes(buffer, position);
    }

    public byte[] getTabLivelloBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, idPolGar, Len.ID_POL_GAR);
        position += Len.ID_POL_GAR;
        datiLivello.getDatiLivelloBytes(buffer, position);
        return buffer;
    }

    public void setIdPolGarFormatted(String idPolGar) {
        this.idPolGar = Trunc.toUnsignedNumeric(idPolGar, Len.ID_POL_GAR);
    }

    public int getIdPolGar() {
        return NumericDisplay.asInt(this.idPolGar);
    }

    public void setWhereCond(String whereCond) {
        this.whereCond = Functions.subString(whereCond, Len.WHERE_COND);
    }

    public String getWhereCond() {
        return this.whereCond;
    }

    public String getWhereCondFormatted() {
        return Functions.padBlanks(getWhereCond(), Len.WHERE_COND);
    }

    public void setLunghezzaTot(int lunghezzaTot) {
        this.lunghezzaTot = lunghezzaTot;
    }

    public int getLunghezzaTot() {
        return this.lunghezzaTot;
    }

    public void setEleInfoMax(short eleInfoMax) {
        this.eleInfoMax = eleInfoMax;
    }

    public short getEleInfoMax() {
        return this.eleInfoMax;
    }

    public void setBufferDati(String bufferDati) {
        this.bufferDati = Functions.subString(bufferDati, Len.BUFFER_DATI);
    }

    public void setBufferDatiSubstring(String replacement, int start, int length) {
        bufferDati = Functions.setSubstring(bufferDati, replacement, start, length);
    }

    public String getBufferDati() {
        return this.bufferDati;
    }

    public String getBufferDatiFormatted() {
        return Functions.padBlanks(getBufferDati(), Len.BUFFER_DATI);
    }

    public void setFlr1(String flr1) {
        this.flr1 = Functions.subString(flr1, Len.FLR1);
    }

    public String getFlr1() {
        return this.flr1;
    }

    public C214DatiLivello getDatiLivello() {
        return datiLivello;
    }

    public C214FlgArea getFlgArea() {
        return flgArea;
    }

    public C214ModalitaIdAde getModalitaIdAde() {
        return modalitaIdAde;
    }

    public C214ModalitaIdGrz getModalitaIdGrz() {
        return modalitaIdGrz;
    }

    public C214ModalitaIdPol getModalitaIdPol() {
        return modalitaIdPol;
    }

    public C214ModalitaIdTga getModalitaIdTga() {
        return modalitaIdTga;
    }

    public C214SaltaValorizzazione getSaltaValorizzazione() {
        return saltaValorizzazione;
    }

    public C214TabInfo getTabInfo(int idx) {
        return tabInfo[idx - 1];
    }

    public C214TabOutput getTabOutput() {
        return tabOutput;
    }

    @Override
    public byte[] serialize() {
        return getIvvc0213Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_PARAMETRO = 20;
        public static final int ID_POLIZZA = 9;
        public static final int ID_ADESIONE = 9;
        public static final int ID_GARANZIA = 9;
        public static final int ID_TRANCHE = 9;
        public static final int ID_TIT_CONT = 9;
        public static final int TIPO_MOVIMENTO = 5;
        public static final int TIPO_MOVI_ORIG = 5;
        public static final int IX_TABB = 4;
        public static final int DATA_EFFETTO = 8;
        public static final int DATA_COMPETENZA = 10;
        public static final int DATA_COMP_AGG_STOR = 10;
        public static final int COD_COMPAGNIA_ANIA = 5;
        public static final int ID_POL_GAR = 9;
        public static final int TAB_LIVELLO = ID_POL_GAR + C214DatiLivello.Len.DATI_LIVELLO;
        public static final int WHERE_COND = 300;
        public static final int LUNGHEZZA_TOT = 5;
        public static final int ELE_INFO_MAX = 3;
        public static final int BUFFER_DATI = 110000;
        public static final int FLR1 = 203;
        public static final int IVVC0213 = COD_PARAMETRO + ID_POLIZZA + ID_ADESIONE + ID_GARANZIA + ID_TRANCHE + ID_TIT_CONT + TIPO_MOVIMENTO + TIPO_MOVI_ORIG + IX_TABB + DATA_EFFETTO + DATA_COMPETENZA + DATA_COMP_AGG_STOR + COD_COMPAGNIA_ANIA + TAB_LIVELLO + WHERE_COND + LUNGHEZZA_TOT + ELE_INFO_MAX + Ivvc0213.TAB_INFO_MAXOCCURS * C214TabInfo.Len.TAB_INFO + BUFFER_DATI + C214ModalitaIdPol.Len.MODALITA_ID_POL + C214ModalitaIdAde.Len.MODALITA_ID_ADE + C214ModalitaIdGrz.Len.MODALITA_ID_GRZ + C214ModalitaIdTga.Len.MODALITA_ID_TGA + C214ModalitaIdTit.Len.MODALITA_ID_TIT + C214SaltaValorizzazione.Len.SALTA_VALORIZZAZIONE + C214TabOutput.Len.TAB_OUTPUT + C214FlgArea.Len.FLG_AREA + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DATA_COMPETENZA = 18;
            public static final int DATA_COMP_AGG_STOR = 18;
            public static final int LUNGHEZZA_TOT = 9;
            public static final int ELE_INFO_MAX = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
