package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IBtcParallelism;
import it.accenture.jnais.ws.redefines.BpaDtEnd;
import it.accenture.jnais.ws.redefines.BpaDtStart;
import it.accenture.jnais.ws.redefines.BpaIdOggA;
import it.accenture.jnais.ws.redefines.BpaIdOggDa;
import it.accenture.jnais.ws.redefines.BpaNumRowSchedule;

/**Original name: BTC-PARALLELISM<br>
 * Variable: BTC-PARALLELISM from copybook IDBVBPA1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BtcParallelism extends SerializableParameter implements IBtcParallelism {

    //==== PROPERTIES ====
    //Original name: BPA-COD-COMP-ANIA
    private int bpaCodCompAnia = DefaultValues.BIN_INT_VAL;
    //Original name: BPA-PROTOCOL
    private String bpaProtocol = DefaultValues.stringVal(Len.BPA_PROTOCOL);
    //Original name: BPA-PROG-PROTOCOL
    private int bpaProgProtocol = DefaultValues.BIN_INT_VAL;
    //Original name: BPA-COD-BATCH-STATE
    private char bpaCodBatchState = DefaultValues.CHAR_VAL;
    //Original name: BPA-DT-INS
    private long bpaDtIns = DefaultValues.LONG_VAL;
    //Original name: BPA-USER-INS
    private String bpaUserIns = DefaultValues.stringVal(Len.BPA_USER_INS);
    //Original name: BPA-DT-START
    private BpaDtStart bpaDtStart = new BpaDtStart();
    //Original name: BPA-DT-END
    private BpaDtEnd bpaDtEnd = new BpaDtEnd();
    //Original name: BPA-USER-START
    private String bpaUserStart = DefaultValues.stringVal(Len.BPA_USER_START);
    //Original name: BPA-DESC-PARALLELISM-LEN
    private short bpaDescParallelismLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: BPA-DESC-PARALLELISM
    private String bpaDescParallelism = DefaultValues.stringVal(Len.BPA_DESC_PARALLELISM);
    //Original name: BPA-ID-OGG-DA
    private BpaIdOggDa bpaIdOggDa = new BpaIdOggDa();
    //Original name: BPA-ID-OGG-A
    private BpaIdOggA bpaIdOggA = new BpaIdOggA();
    //Original name: BPA-TP-OGG
    private String bpaTpOgg = DefaultValues.stringVal(Len.BPA_TP_OGG);
    //Original name: BPA-NUM-ROW-SCHEDULE
    private BpaNumRowSchedule bpaNumRowSchedule = new BpaNumRowSchedule();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_PARALLELISM;
    }

    @Override
    public void deserialize(byte[] buf) {
        setBtcParallelismBytes(buf);
    }

    public String getBtcParallelismFormatted() {
        return MarshalByteExt.bufferToStr(getBtcParallelismBytes());
    }

    public void setBtcParallelismBytes(byte[] buffer) {
        setBtcParallelismBytes(buffer, 1);
    }

    public byte[] getBtcParallelismBytes() {
        byte[] buffer = new byte[Len.BTC_PARALLELISM];
        return getBtcParallelismBytes(buffer, 1);
    }

    public void setBtcParallelismBytes(byte[] buffer, int offset) {
        int position = offset;
        bpaCodCompAnia = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        bpaProtocol = MarshalByte.readString(buffer, position, Len.BPA_PROTOCOL);
        position += Len.BPA_PROTOCOL;
        bpaProgProtocol = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        bpaCodBatchState = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bpaDtIns = MarshalByte.readPackedAsLong(buffer, position, Len.Int.BPA_DT_INS, 0);
        position += Len.BPA_DT_INS;
        bpaUserIns = MarshalByte.readString(buffer, position, Len.BPA_USER_INS);
        position += Len.BPA_USER_INS;
        bpaDtStart.setBpaDtStartFromBuffer(buffer, position);
        position += BpaDtStart.Len.BPA_DT_START;
        bpaDtEnd.setBpaDtEndFromBuffer(buffer, position);
        position += BpaDtEnd.Len.BPA_DT_END;
        bpaUserStart = MarshalByte.readString(buffer, position, Len.BPA_USER_START);
        position += Len.BPA_USER_START;
        setBpaDescParallelismVcharBytes(buffer, position);
        position += Len.BPA_DESC_PARALLELISM_VCHAR;
        bpaIdOggDa.setBpaIdOggDaFromBuffer(buffer, position);
        position += BpaIdOggDa.Len.BPA_ID_OGG_DA;
        bpaIdOggA.setBpaIdOggAFromBuffer(buffer, position);
        position += BpaIdOggA.Len.BPA_ID_OGG_A;
        bpaTpOgg = MarshalByte.readString(buffer, position, Len.BPA_TP_OGG);
        position += Len.BPA_TP_OGG;
        bpaNumRowSchedule.setBpaNumRowScheduleFromBuffer(buffer, position);
    }

    public byte[] getBtcParallelismBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, bpaCodCompAnia);
        position += Types.INT_SIZE;
        MarshalByte.writeString(buffer, position, bpaProtocol, Len.BPA_PROTOCOL);
        position += Len.BPA_PROTOCOL;
        MarshalByte.writeBinaryInt(buffer, position, bpaProgProtocol);
        position += Types.INT_SIZE;
        MarshalByte.writeChar(buffer, position, bpaCodBatchState);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, bpaDtIns, Len.Int.BPA_DT_INS, 0);
        position += Len.BPA_DT_INS;
        MarshalByte.writeString(buffer, position, bpaUserIns, Len.BPA_USER_INS);
        position += Len.BPA_USER_INS;
        bpaDtStart.getBpaDtStartAsBuffer(buffer, position);
        position += BpaDtStart.Len.BPA_DT_START;
        bpaDtEnd.getBpaDtEndAsBuffer(buffer, position);
        position += BpaDtEnd.Len.BPA_DT_END;
        MarshalByte.writeString(buffer, position, bpaUserStart, Len.BPA_USER_START);
        position += Len.BPA_USER_START;
        getBpaDescParallelismVcharBytes(buffer, position);
        position += Len.BPA_DESC_PARALLELISM_VCHAR;
        bpaIdOggDa.getBpaIdOggDaAsBuffer(buffer, position);
        position += BpaIdOggDa.Len.BPA_ID_OGG_DA;
        bpaIdOggA.getBpaIdOggAAsBuffer(buffer, position);
        position += BpaIdOggA.Len.BPA_ID_OGG_A;
        MarshalByte.writeString(buffer, position, bpaTpOgg, Len.BPA_TP_OGG);
        position += Len.BPA_TP_OGG;
        bpaNumRowSchedule.getBpaNumRowScheduleAsBuffer(buffer, position);
        return buffer;
    }

    public void initBtcParallelismHighValues() {
        bpaCodCompAnia = Types.HIGH_INT_VAL;
        bpaProtocol = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.BPA_PROTOCOL);
        bpaProgProtocol = Types.HIGH_INT_VAL;
        bpaCodBatchState = Types.HIGH_CHAR_VAL;
        bpaDtIns = Types.HIGH_LONG_VAL;
        bpaUserIns = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.BPA_USER_INS);
        bpaDtStart.initBpaDtStartHighValues();
        bpaDtEnd.initBpaDtEndHighValues();
        bpaUserStart = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.BPA_USER_START);
        initBpaDescParallelismVcharHighValues();
        bpaIdOggDa.initBpaIdOggDaHighValues();
        bpaIdOggA.initBpaIdOggAHighValues();
        bpaTpOgg = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.BPA_TP_OGG);
        bpaNumRowSchedule.initBpaNumRowScheduleHighValues();
    }

    @Override
    public void setBpaCodCompAnia(int bpaCodCompAnia) {
        this.bpaCodCompAnia = bpaCodCompAnia;
    }

    @Override
    public int getBpaCodCompAnia() {
        return this.bpaCodCompAnia;
    }

    @Override
    public void setBpaProtocol(String bpaProtocol) {
        this.bpaProtocol = Functions.subString(bpaProtocol, Len.BPA_PROTOCOL);
    }

    @Override
    public String getBpaProtocol() {
        return this.bpaProtocol;
    }

    public String getBpaProtocolFormatted() {
        return Functions.padBlanks(getBpaProtocol(), Len.BPA_PROTOCOL);
    }

    @Override
    public void setBpaProgProtocol(int bpaProgProtocol) {
        this.bpaProgProtocol = bpaProgProtocol;
    }

    @Override
    public int getBpaProgProtocol() {
        return this.bpaProgProtocol;
    }

    public void setBpaCodBatchState(char bpaCodBatchState) {
        this.bpaCodBatchState = bpaCodBatchState;
    }

    public char getBpaCodBatchState() {
        return this.bpaCodBatchState;
    }

    public void setBpaDtIns(long bpaDtIns) {
        this.bpaDtIns = bpaDtIns;
    }

    public long getBpaDtIns() {
        return this.bpaDtIns;
    }

    public void setBpaUserIns(String bpaUserIns) {
        this.bpaUserIns = Functions.subString(bpaUserIns, Len.BPA_USER_INS);
    }

    public String getBpaUserIns() {
        return this.bpaUserIns;
    }

    public void setBpaUserStart(String bpaUserStart) {
        this.bpaUserStart = Functions.subString(bpaUserStart, Len.BPA_USER_START);
    }

    public String getBpaUserStart() {
        return this.bpaUserStart;
    }

    public void setBpaDescParallelismVcharFormatted(String data) {
        byte[] buffer = new byte[Len.BPA_DESC_PARALLELISM_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.BPA_DESC_PARALLELISM_VCHAR);
        setBpaDescParallelismVcharBytes(buffer, 1);
    }

    public String getBpaDescParallelismVcharFormatted() {
        return MarshalByteExt.bufferToStr(getBpaDescParallelismVcharBytes());
    }

    /**Original name: BPA-DESC-PARALLELISM-VCHAR<br>*/
    public byte[] getBpaDescParallelismVcharBytes() {
        byte[] buffer = new byte[Len.BPA_DESC_PARALLELISM_VCHAR];
        return getBpaDescParallelismVcharBytes(buffer, 1);
    }

    public void setBpaDescParallelismVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        bpaDescParallelismLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        bpaDescParallelism = MarshalByte.readString(buffer, position, Len.BPA_DESC_PARALLELISM);
    }

    public byte[] getBpaDescParallelismVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, bpaDescParallelismLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, bpaDescParallelism, Len.BPA_DESC_PARALLELISM);
        return buffer;
    }

    public void initBpaDescParallelismVcharHighValues() {
        bpaDescParallelismLen = Types.HIGH_SHORT_VAL;
        bpaDescParallelism = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.BPA_DESC_PARALLELISM);
    }

    public void setBpaDescParallelismLen(short bpaDescParallelismLen) {
        this.bpaDescParallelismLen = bpaDescParallelismLen;
    }

    public short getBpaDescParallelismLen() {
        return this.bpaDescParallelismLen;
    }

    public void setBpaDescParallelism(String bpaDescParallelism) {
        this.bpaDescParallelism = Functions.subString(bpaDescParallelism, Len.BPA_DESC_PARALLELISM);
    }

    public String getBpaDescParallelism() {
        return this.bpaDescParallelism;
    }

    public void setBpaTpOgg(String bpaTpOgg) {
        this.bpaTpOgg = Functions.subString(bpaTpOgg, Len.BPA_TP_OGG);
    }

    public String getBpaTpOgg() {
        return this.bpaTpOgg;
    }

    public String getBpaTpOggFormatted() {
        return Functions.padBlanks(getBpaTpOgg(), Len.BPA_TP_OGG);
    }

    public BpaDtEnd getBpaDtEnd() {
        return bpaDtEnd;
    }

    public BpaDtStart getBpaDtStart() {
        return bpaDtStart;
    }

    public BpaIdOggA getBpaIdOggA() {
        return bpaIdOggA;
    }

    public BpaIdOggDa getBpaIdOggDa() {
        return bpaIdOggDa;
    }

    public BpaNumRowSchedule getBpaNumRowSchedule() {
        return bpaNumRowSchedule;
    }

    @Override
    public char getCodBatchState() {
        throw new FieldNotMappedException("codBatchState");
    }

    @Override
    public void setCodBatchState(char codBatchState) {
        throw new FieldNotMappedException("codBatchState");
    }

    @Override
    public String getDescParallelismVchar() {
        throw new FieldNotMappedException("descParallelismVchar");
    }

    @Override
    public void setDescParallelismVchar(String descParallelismVchar) {
        throw new FieldNotMappedException("descParallelismVchar");
    }

    @Override
    public String getDescParallelismVcharObj() {
        return getDescParallelismVchar();
    }

    @Override
    public void setDescParallelismVcharObj(String descParallelismVcharObj) {
        setDescParallelismVchar(descParallelismVcharObj);
    }

    @Override
    public String getDtEndDb() {
        throw new FieldNotMappedException("dtEndDb");
    }

    @Override
    public void setDtEndDb(String dtEndDb) {
        throw new FieldNotMappedException("dtEndDb");
    }

    @Override
    public String getDtEndDbObj() {
        return getDtEndDb();
    }

    @Override
    public void setDtEndDbObj(String dtEndDbObj) {
        setDtEndDb(dtEndDbObj);
    }

    @Override
    public String getDtInsDb() {
        throw new FieldNotMappedException("dtInsDb");
    }

    @Override
    public void setDtInsDb(String dtInsDb) {
        throw new FieldNotMappedException("dtInsDb");
    }

    @Override
    public String getDtStartDb() {
        throw new FieldNotMappedException("dtStartDb");
    }

    @Override
    public void setDtStartDb(String dtStartDb) {
        throw new FieldNotMappedException("dtStartDb");
    }

    @Override
    public String getDtStartDbObj() {
        return getDtStartDb();
    }

    @Override
    public void setDtStartDbObj(String dtStartDbObj) {
        setDtStartDb(dtStartDbObj);
    }

    @Override
    public char getIabv0002State01() {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public char getIabv0002State02() {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public char getIabv0002State03() {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public char getIabv0002State04() {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public char getIabv0002State05() {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public char getIabv0002State06() {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public char getIabv0002State07() {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public char getIabv0002State08() {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public char getIabv0002State09() {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public char getIabv0002State10() {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public int getIdOggA() {
        throw new FieldNotMappedException("idOggA");
    }

    @Override
    public void setIdOggA(int idOggA) {
        throw new FieldNotMappedException("idOggA");
    }

    @Override
    public Integer getIdOggAObj() {
        return ((Integer)getIdOggA());
    }

    @Override
    public void setIdOggAObj(Integer idOggAObj) {
        setIdOggA(((int)idOggAObj));
    }

    @Override
    public int getIdOggDa() {
        throw new FieldNotMappedException("idOggDa");
    }

    @Override
    public void setIdOggDa(int idOggDa) {
        throw new FieldNotMappedException("idOggDa");
    }

    @Override
    public Integer getIdOggDaObj() {
        return ((Integer)getIdOggDa());
    }

    @Override
    public void setIdOggDaObj(Integer idOggDaObj) {
        setIdOggDa(((int)idOggDaObj));
    }

    @Override
    public int getNumRowSchedule() {
        throw new FieldNotMappedException("numRowSchedule");
    }

    @Override
    public void setNumRowSchedule(int numRowSchedule) {
        throw new FieldNotMappedException("numRowSchedule");
    }

    @Override
    public Integer getNumRowScheduleObj() {
        return ((Integer)getNumRowSchedule());
    }

    @Override
    public void setNumRowScheduleObj(Integer numRowScheduleObj) {
        setNumRowSchedule(((int)numRowScheduleObj));
    }

    @Override
    public String getTpOgg() {
        throw new FieldNotMappedException("tpOgg");
    }

    @Override
    public void setTpOgg(String tpOgg) {
        throw new FieldNotMappedException("tpOgg");
    }

    @Override
    public String getTpOggObj() {
        return getTpOgg();
    }

    @Override
    public void setTpOggObj(String tpOggObj) {
        setTpOgg(tpOggObj);
    }

    @Override
    public String getUserIns() {
        throw new FieldNotMappedException("userIns");
    }

    @Override
    public void setUserIns(String userIns) {
        throw new FieldNotMappedException("userIns");
    }

    @Override
    public String getUserStart() {
        throw new FieldNotMappedException("userStart");
    }

    @Override
    public void setUserStart(String userStart) {
        throw new FieldNotMappedException("userStart");
    }

    @Override
    public String getUserStartObj() {
        return getUserStart();
    }

    @Override
    public void setUserStartObj(String userStartObj) {
        setUserStart(userStartObj);
    }

    @Override
    public byte[] serialize() {
        return getBtcParallelismBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BPA_COD_COMP_ANIA = 4;
        public static final int BPA_PROTOCOL = 50;
        public static final int BPA_PROG_PROTOCOL = 4;
        public static final int BPA_COD_BATCH_STATE = 1;
        public static final int BPA_DT_INS = 10;
        public static final int BPA_USER_INS = 30;
        public static final int BPA_USER_START = 30;
        public static final int BPA_DESC_PARALLELISM_LEN = 2;
        public static final int BPA_DESC_PARALLELISM = 250;
        public static final int BPA_DESC_PARALLELISM_VCHAR = BPA_DESC_PARALLELISM_LEN + BPA_DESC_PARALLELISM;
        public static final int BPA_TP_OGG = 2;
        public static final int BTC_PARALLELISM = BPA_COD_COMP_ANIA + BPA_PROTOCOL + BPA_PROG_PROTOCOL + BPA_COD_BATCH_STATE + BPA_DT_INS + BPA_USER_INS + BpaDtStart.Len.BPA_DT_START + BpaDtEnd.Len.BPA_DT_END + BPA_USER_START + BPA_DESC_PARALLELISM_VCHAR + BpaIdOggDa.Len.BPA_ID_OGG_DA + BpaIdOggA.Len.BPA_ID_OGG_A + BPA_TP_OGG + BpaNumRowSchedule.Len.BPA_NUM_ROW_SCHEDULE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BPA_DT_INS = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
