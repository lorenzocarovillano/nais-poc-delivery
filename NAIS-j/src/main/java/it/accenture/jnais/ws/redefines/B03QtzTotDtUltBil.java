package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-QTZ-TOT-DT-ULT-BIL<br>
 * Variable: B03-QTZ-TOT-DT-ULT-BIL from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03QtzTotDtUltBil extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03QtzTotDtUltBil() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_QTZ_TOT_DT_ULT_BIL;
    }

    public void setB03QtzTotDtUltBil(AfDecimal b03QtzTotDtUltBil) {
        writeDecimalAsPacked(Pos.B03_QTZ_TOT_DT_ULT_BIL, b03QtzTotDtUltBil.copy());
    }

    public void setB03QtzTotDtUltBilFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_QTZ_TOT_DT_ULT_BIL, Pos.B03_QTZ_TOT_DT_ULT_BIL);
    }

    /**Original name: B03-QTZ-TOT-DT-ULT-BIL<br>*/
    public AfDecimal getB03QtzTotDtUltBil() {
        return readPackedAsDecimal(Pos.B03_QTZ_TOT_DT_ULT_BIL, Len.Int.B03_QTZ_TOT_DT_ULT_BIL, Len.Fract.B03_QTZ_TOT_DT_ULT_BIL);
    }

    public byte[] getB03QtzTotDtUltBilAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_QTZ_TOT_DT_ULT_BIL, Pos.B03_QTZ_TOT_DT_ULT_BIL);
        return buffer;
    }

    public void setB03QtzTotDtUltBilNull(String b03QtzTotDtUltBilNull) {
        writeString(Pos.B03_QTZ_TOT_DT_ULT_BIL_NULL, b03QtzTotDtUltBilNull, Len.B03_QTZ_TOT_DT_ULT_BIL_NULL);
    }

    /**Original name: B03-QTZ-TOT-DT-ULT-BIL-NULL<br>*/
    public String getB03QtzTotDtUltBilNull() {
        return readString(Pos.B03_QTZ_TOT_DT_ULT_BIL_NULL, Len.B03_QTZ_TOT_DT_ULT_BIL_NULL);
    }

    public String getB03QtzTotDtUltBilNullFormatted() {
        return Functions.padBlanks(getB03QtzTotDtUltBilNull(), Len.B03_QTZ_TOT_DT_ULT_BIL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_QTZ_TOT_DT_ULT_BIL = 1;
        public static final int B03_QTZ_TOT_DT_ULT_BIL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_QTZ_TOT_DT_ULT_BIL = 7;
        public static final int B03_QTZ_TOT_DT_ULT_BIL_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_QTZ_TOT_DT_ULT_BIL = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_QTZ_TOT_DT_ULT_BIL = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
