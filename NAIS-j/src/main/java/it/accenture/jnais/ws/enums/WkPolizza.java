package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-POLIZZA<br>
 * Variable: WK-POLIZZA from program LVVS0095<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkPolizza {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char OK = 'S';
    public static final char KO = 'N';

    //==== METHODS ====
    public void setWkPolizza(char wkPolizza) {
        this.value = wkPolizza;
    }

    public char getWkPolizza() {
        return this.value;
    }

    public boolean isOk() {
        return value == OK;
    }

    public void setOk() {
        value = OK;
    }

    public void setKo() {
        value = KO;
    }
}
