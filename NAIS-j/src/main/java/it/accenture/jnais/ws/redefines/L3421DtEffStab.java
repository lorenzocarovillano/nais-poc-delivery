package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-DT-EFF-STAB<br>
 * Variable: L3421-DT-EFF-STAB from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421DtEffStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421DtEffStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_DT_EFF_STAB;
    }

    public void setL3421DtEffStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_DT_EFF_STAB, Pos.L3421_DT_EFF_STAB);
    }

    /**Original name: L3421-DT-EFF-STAB<br>*/
    public int getL3421DtEffStab() {
        return readPackedAsInt(Pos.L3421_DT_EFF_STAB, Len.Int.L3421_DT_EFF_STAB);
    }

    public byte[] getL3421DtEffStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_DT_EFF_STAB, Pos.L3421_DT_EFF_STAB);
        return buffer;
    }

    /**Original name: L3421-DT-EFF-STAB-NULL<br>*/
    public String getL3421DtEffStabNull() {
        return readString(Pos.L3421_DT_EFF_STAB_NULL, Len.L3421_DT_EFF_STAB_NULL);
    }

    public String getL3421DtEffStabNullFormatted() {
        return Functions.padBlanks(getL3421DtEffStabNull(), Len.L3421_DT_EFF_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_DT_EFF_STAB = 1;
        public static final int L3421_DT_EFF_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_DT_EFF_STAB = 5;
        public static final int L3421_DT_EFF_STAB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_DT_EFF_STAB = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
