package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: DATO-INPUT-TROVATO<br>
 * Variable: DATO-INPUT-TROVATO from copybook IDSV0501<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class DatoInputTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setDatoInputTrovato(char datoInputTrovato) {
        this.value = datoInputTrovato;
    }

    public char getDatoInputTrovato() {
        return this.value;
    }

    public void setDatoInputTrovatoSi() {
        value = SI;
    }

    public boolean isDatoInputTrovatoNo() {
        return value == NO;
    }

    public void setDatoInputTrovatoNo() {
        value = NO;
    }
}
