package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import org.apache.commons.lang3.ArrayUtils;

/**Original name: IJCCMQ04-VAR-AMBIENTE<br>
 * Variable: IJCCMQ04-VAR-AMBIENTE from copybook IJCCMQ04<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class Ijccmq04VarAmbiente extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final String UNIX = "UNIX";
    public static final String HOST = "HOST";
    public static final String MQ = "MQ";
    public static final String CSOCKET = "C-SOCKET";
    public static final String JCICS_COO = "JCICSCOO";
    public static final String CALL_COBOL = "COB";
    public static final String CALL_C = "C";
    public static final String PUT_A = "JCALL08";
    public static final String PUT_B = "JCALL05";
    public static final String PUT_C = "TST2IN";
    public static final String PUT_D = "JCALL06";
    public static final String GET_A = "JRET08";
    public static final String GET_B = "JRET05";
    public static final String GET_C = "TST2OU";
    public static final String GET_D = "JRET06";
    public static final char OP_PERSISTENZA = 'S';
    public static final char OP_NO_PERSISTENZA = 'N';
    private static final char[] OP_VALIDA = new char[] {'S', 'N'};
    public static final char OW_SI_WAIT = 'S';
    public static final char OW_NO_WAIT = 'N';
    private static final char[] OW_VALIDA = new char[] {'S', 'N'};
    public static final char OS_SYNCPOINT = 'S';
    public static final char OS_NO_SYNCPOINT = 'N';
    private static final char[] OS_VALIDA = new char[] {'S', 'N'};
    public static final char AR_ILLIMITATA = 'I';
    public static final char AR_LIMITATA = 'L';
    private static final char[] AR_VALIDA = new char[] {'I', 'L'};
    public static final char FL_COMPRESSORE_C_SI = 'S';
    public static final char FL_COMPRESSORE_C_NO = 'N';

    //==== CONSTRUCTORS ====
    public Ijccmq04VarAmbiente() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IJCCMQ04_VAR_AMBIENTE;
    }

    public void setCodCompAnia(int codCompAnia) {
        writeInt(Pos.COD_COMP_ANIA, codCompAnia, Len.Int.COD_COMP_ANIA, SignType.NO_SIGN);
    }

    public void setCodCompAniaFormatted(String codCompAnia) {
        writeString(Pos.COD_COMP_ANIA, Trunc.toUnsignedNumeric(codCompAnia, Len.COD_COMP_ANIA), Len.COD_COMP_ANIA);
    }

    /**Original name: IJCCMQ04-COD-COMP-ANIA<br>*/
    public int getCodCompAnia() {
        return readNumDispUnsignedInt(Pos.COD_COMP_ANIA, Len.COD_COMP_ANIA);
    }

    public void setAmbiente(String ambiente) {
        writeString(Pos.AMBIENTE, ambiente, Len.AMBIENTE);
    }

    /**Original name: IJCCMQ04-AMBIENTE<br>*/
    public String getAmbiente() {
        return readString(Pos.AMBIENTE, Len.AMBIENTE);
    }

    public void setPiattaforma(String piattaforma) {
        writeString(Pos.PIATTAFORMA, piattaforma, Len.PIATTAFORMA);
    }

    /**Original name: IJCCMQ04-PIATTAFORMA<br>*/
    public String getPiattaforma() {
        return readString(Pos.PIATTAFORMA, Len.PIATTAFORMA);
    }

    public void setCobToJava(String cobToJava) {
        writeString(Pos.COB_TO_JAVA, cobToJava, Len.COB_TO_JAVA);
    }

    /**Original name: IJCCMQ04-COB-TO-JAVA<br>*/
    public String getCobToJava() {
        return readString(Pos.COB_TO_JAVA, Len.COB_TO_JAVA);
    }

    public String getCobToJavaFormatted() {
        return Functions.padBlanks(getCobToJava(), Len.COB_TO_JAVA);
    }

    public boolean isMq() {
        return getCobToJava().equals(MQ);
    }

    public boolean isCsocket() {
        return getCobToJava().equals(CSOCKET);
    }

    public boolean isJcicsCoo() {
        return getCobToJava().equals(JCICS_COO);
    }

    public void setTpUtilizzoApi(String tpUtilizzoApi) {
        writeString(Pos.TP_UTILIZZO_API, tpUtilizzoApi, Len.TP_UTILIZZO_API);
    }

    /**Original name: IJCCMQ04-TP-UTILIZZO-API<br>*/
    public String getTpUtilizzoApi() {
        return readString(Pos.TP_UTILIZZO_API, Len.TP_UTILIZZO_API);
    }

    public String getTpUtilizzoApiFormatted() {
        return Functions.padBlanks(getTpUtilizzoApi(), Len.TP_UTILIZZO_API);
    }

    public boolean isCallCobol() {
        return getTpUtilizzoApi().equals(CALL_COBOL);
    }

    public boolean isCallC() {
        return getTpUtilizzoApi().equals(CALL_C);
    }

    public void setQueueManager(String queueManager) {
        writeString(Pos.QUEUE_MANAGER, queueManager, Len.QUEUE_MANAGER);
    }

    /**Original name: IJCCMQ04-QUEUE-MANAGER<br>*/
    public String getQueueManager() {
        return readString(Pos.QUEUE_MANAGER, Len.QUEUE_MANAGER);
    }

    public void setCodaPut(String codaPut) {
        writeString(Pos.CODA_PUT, codaPut, Len.CODA_PUT);
    }

    /**Original name: IJCCMQ04-CODA-PUT<br>*/
    public String getCodaPut() {
        return readString(Pos.CODA_PUT, Len.CODA_PUT);
    }

    public void setCodaGet(String codaGet) {
        writeString(Pos.CODA_GET, codaGet, Len.CODA_GET);
    }

    /**Original name: IJCCMQ04-CODA-GET<br>*/
    public String getCodaGet() {
        return readString(Pos.CODA_GET, Len.CODA_GET);
    }

    public void setOpzPersistenza(char opzPersistenza) {
        writeChar(Pos.OPZ_PERSISTENZA, opzPersistenza);
    }

    /**Original name: IJCCMQ04-OPZ-PERSISTENZA<br>*/
    public char getOpzPersistenza() {
        return readChar(Pos.OPZ_PERSISTENZA);
    }

    public boolean isOpValida() {
        return ArrayUtils.contains(OP_VALIDA, getOpzPersistenza());
    }

    public void setOpzWait(char opzWait) {
        writeChar(Pos.OPZ_WAIT, opzWait);
    }

    /**Original name: IJCCMQ04-OPZ-WAIT<br>*/
    public char getOpzWait() {
        return readChar(Pos.OPZ_WAIT);
    }

    public boolean isOwValida() {
        return ArrayUtils.contains(OW_VALIDA, getOpzWait());
    }

    public void setOpzSyncpoint(char opzSyncpoint) {
        writeChar(Pos.OPZ_SYNCPOINT, opzSyncpoint);
    }

    /**Original name: IJCCMQ04-OPZ-SYNCPOINT<br>*/
    public char getOpzSyncpoint() {
        return readChar(Pos.OPZ_SYNCPOINT);
    }

    public boolean isOsValida() {
        return ArrayUtils.contains(OS_VALIDA, getOpzSyncpoint());
    }

    public void setAttesaRisposta(char attesaRisposta) {
        writeChar(Pos.ATTESA_RISPOSTA, attesaRisposta);
    }

    /**Original name: IJCCMQ04-ATTESA-RISPOSTA<br>*/
    public char getAttesaRisposta() {
        return readChar(Pos.ATTESA_RISPOSTA);
    }

    public boolean isArValida() {
        return ArrayUtils.contains(AR_VALIDA, getAttesaRisposta());
    }

    public void setTempoAttesa1(int tempoAttesa1) {
        writeBinaryInt(Pos.TEMPO_ATTESA1, tempoAttesa1);
    }

    /**Original name: IJCCMQ04-TEMPO-ATTESA-1<br>*/
    public int getTempoAttesa1() {
        return readBinaryInt(Pos.TEMPO_ATTESA1);
    }

    public void setTempoAttesa2(int tempoAttesa2) {
        writeBinaryInt(Pos.TEMPO_ATTESA2, tempoAttesa2);
    }

    /**Original name: IJCCMQ04-TEMPO-ATTESA-2<br>*/
    public int getTempoAttesa2() {
        return readBinaryInt(Pos.TEMPO_ATTESA2);
    }

    public void setTempoExpiry(int tempoExpiry) {
        writeBinaryInt(Pos.TEMPO_EXPIRY, tempoExpiry);
    }

    /**Original name: IJCCMQ04-TEMPO-EXPIRY<br>*/
    public int getTempoExpiry() {
        return readBinaryInt(Pos.TEMPO_EXPIRY);
    }

    public void setCsocketIpAddress(String csocketIpAddress) {
        writeString(Pos.CSOCKET_IP_ADDRESS, csocketIpAddress, Len.CSOCKET_IP_ADDRESS);
    }

    /**Original name: IJCCMQ04-CSOCKET-IP-ADDRESS<br>*/
    public String getCsocketIpAddress() {
        return readString(Pos.CSOCKET_IP_ADDRESS, Len.CSOCKET_IP_ADDRESS);
    }

    public void setCsocketPortNum(int csocketPortNum) {
        writeInt(Pos.CSOCKET_PORT_NUM, csocketPortNum, Len.Int.CSOCKET_PORT_NUM, SignType.NO_SIGN);
    }

    public void setCsocketPortNumFormatted(String csocketPortNum) {
        writeString(Pos.CSOCKET_PORT_NUM, Trunc.toUnsignedNumeric(csocketPortNum, Len.CSOCKET_PORT_NUM), Len.CSOCKET_PORT_NUM);
    }

    /**Original name: IJCCMQ04-CSOCKET-PORT-NUM<br>*/
    public int getCsocketPortNum() {
        return readNumDispUnsignedInt(Pos.CSOCKET_PORT_NUM, Len.CSOCKET_PORT_NUM);
    }

    public void setFlCompressoreC(char flCompressoreC) {
        writeChar(Pos.FL_COMPRESSORE_C, flCompressoreC);
    }

    /**Original name: IJCCMQ04-FL-COMPRESSORE-C<br>*/
    public char getFlCompressoreC() {
        return readChar(Pos.FL_COMPRESSORE_C);
    }

    public boolean isFlCompressoreCSi() {
        return getFlCompressoreC() == FL_COMPRESSORE_C_SI;
    }

    public boolean isFlCompressoreCNo() {
        return getFlCompressoreC() == FL_COMPRESSORE_C_NO;
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int IJCCMQ04_VAR_AMBIENTE = 1;
        public static final int COD_COMP_ANIA = IJCCMQ04_VAR_AMBIENTE;
        public static final int AMBIENTE = COD_COMP_ANIA + Len.COD_COMP_ANIA;
        public static final int PIATTAFORMA = AMBIENTE + Len.AMBIENTE;
        public static final int COB_TO_JAVA = PIATTAFORMA + Len.PIATTAFORMA;
        public static final int TP_UTILIZZO_API = COB_TO_JAVA + Len.COB_TO_JAVA;
        public static final int QUEUE_MANAGER = TP_UTILIZZO_API + Len.TP_UTILIZZO_API;
        public static final int CODA_PUT = QUEUE_MANAGER + Len.QUEUE_MANAGER;
        public static final int CODA_GET = CODA_PUT + Len.CODA_PUT;
        public static final int OPZ_PERSISTENZA = CODA_GET + Len.CODA_GET;
        public static final int OPZ_WAIT = OPZ_PERSISTENZA + Len.OPZ_PERSISTENZA;
        public static final int OPZ_SYNCPOINT = OPZ_WAIT + Len.OPZ_WAIT;
        public static final int ATTESA_RISPOSTA = OPZ_SYNCPOINT + Len.OPZ_SYNCPOINT;
        public static final int TEMPO_ATTESA1 = ATTESA_RISPOSTA + Len.ATTESA_RISPOSTA;
        public static final int TEMPO_ATTESA2 = TEMPO_ATTESA1 + Len.TEMPO_ATTESA1;
        public static final int TEMPO_EXPIRY = TEMPO_ATTESA2 + Len.TEMPO_ATTESA2;
        public static final int CSOCKET_IP_ADDRESS = TEMPO_EXPIRY + Len.TEMPO_EXPIRY;
        public static final int CSOCKET_PORT_NUM = CSOCKET_IP_ADDRESS + Len.CSOCKET_IP_ADDRESS;
        public static final int FL_COMPRESSORE_C = CSOCKET_PORT_NUM + Len.CSOCKET_PORT_NUM;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_COMP_ANIA = 5;
        public static final int AMBIENTE = 10;
        public static final int PIATTAFORMA = 10;
        public static final int COB_TO_JAVA = 10;
        public static final int TP_UTILIZZO_API = 3;
        public static final int QUEUE_MANAGER = 48;
        public static final int CODA_PUT = 48;
        public static final int CODA_GET = 48;
        public static final int OPZ_PERSISTENZA = 1;
        public static final int OPZ_WAIT = 1;
        public static final int OPZ_SYNCPOINT = 1;
        public static final int ATTESA_RISPOSTA = 1;
        public static final int TEMPO_ATTESA1 = 4;
        public static final int TEMPO_ATTESA2 = 4;
        public static final int TEMPO_EXPIRY = 4;
        public static final int CSOCKET_IP_ADDRESS = 20;
        public static final int CSOCKET_PORT_NUM = 5;
        public static final int FL_COMPRESSORE_C = 1;
        public static final int IJCCMQ04_VAR_AMBIENTE = COD_COMP_ANIA + AMBIENTE + PIATTAFORMA + COB_TO_JAVA + TP_UTILIZZO_API + QUEUE_MANAGER + CODA_PUT + CODA_GET + OPZ_PERSISTENZA + OPZ_WAIT + OPZ_SYNCPOINT + ATTESA_RISPOSTA + TEMPO_ATTESA1 + TEMPO_ATTESA2 + TEMPO_EXPIRY + CSOCKET_IP_ADDRESS + CSOCKET_PORT_NUM + FL_COMPRESSORE_C;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_COMP_ANIA = 5;
            public static final int CSOCKET_PORT_NUM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
