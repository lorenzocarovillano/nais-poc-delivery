package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Lccvpco1;
import it.accenture.jnais.copy.ParamComp;
import it.accenture.jnais.ws.enums.WcomFlagOverflow;
import it.accenture.jnais.ws.enums.WsMovimentoLoas0350;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS0350<br>
 * Generated as a class for rule WS.<br>*/
public class Loas0350Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *    COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LOAS0350";
    //Original name: WK-TABELLA
    private String wkTabella = "";
    //Original name: WS-FORMA1
    private String wsForma1 = DefaultValues.stringVal(Len.WS_FORMA1);
    //Original name: WS-FORMA2
    private String wsForma2 = DefaultValues.stringVal(Len.WS_FORMA2);
    //Original name: IX-TAB-PCO
    private short ixTabPco = ((short)0);
    /**Original name: WCOM-FLAG-OVERFLOW<br>
	 * <pre>--> GESTIONE FLAG PER OVERFLOW</pre>*/
    private WcomFlagOverflow wcomFlagOverflow = new WcomFlagOverflow();
    //Original name: WPCO-ELE-PAR-COMP-MAX
    private int wpcoEleParCompMax = DefaultValues.BIN_INT_VAL;
    //Original name: LCCVPCO1
    private Lccvpco1 lccvpco1 = new Lccvpco1();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>    COPY LCCVXMV0.
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLoas0350 wsMovimento = new WsMovimentoLoas0350();
    //Original name: PARAM-COMP
    private ParamComp paramComp = new ParamComp();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public void setWsForma1(String wsForma1) {
        this.wsForma1 = Functions.subString(wsForma1, Len.WS_FORMA1);
    }

    public String getWsForma1() {
        return this.wsForma1;
    }

    public void setWsForma2(String wsForma2) {
        this.wsForma2 = Functions.subString(wsForma2, Len.WS_FORMA2);
    }

    public String getWsForma2() {
        return this.wsForma2;
    }

    public void setIxTabPco(short ixTabPco) {
        this.ixTabPco = ixTabPco;
    }

    public short getIxTabPco() {
        return this.ixTabPco;
    }

    public void setWpcoEleParCompMax(int wpcoEleParCompMax) {
        this.wpcoEleParCompMax = wpcoEleParCompMax;
    }

    public int getWpcoEleParCompMax() {
        return this.wpcoEleParCompMax;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Lccvpco1 getLccvpco1() {
        return lccvpco1;
    }

    public ParamComp getParamComp() {
        return paramComp;
    }

    public WcomFlagOverflow getWcomFlagOverflow() {
        return wcomFlagOverflow;
    }

    public WsMovimentoLoas0350 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_FORMA1 = 2;
        public static final int WS_FORMA2 = 2;
        public static final int WK_TABELLA = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
