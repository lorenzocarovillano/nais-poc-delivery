package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-PREC-TROVATO<br>
 * Variable: WK-PREC-TROVATO from program LOAS0310<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkPrecTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char NO = 'N';
    public static final char SI = 'S';

    //==== METHODS ====
    public void setWkPrecTrovato(char wkPrecTrovato) {
        this.value = wkPrecTrovato;
    }

    public char getWkPrecTrovato() {
        return this.value;
    }

    public void setNo() {
        value = NO;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }
}
