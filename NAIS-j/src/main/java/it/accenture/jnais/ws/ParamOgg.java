package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.PogIdMoviChiu;
import it.accenture.jnais.ws.redefines.PogValDt;
import it.accenture.jnais.ws.redefines.PogValImp;
import it.accenture.jnais.ws.redefines.PogValNum;
import it.accenture.jnais.ws.redefines.PogValPc;
import it.accenture.jnais.ws.redefines.PogValTs;

/**Original name: PARAM-OGG<br>
 * Variable: PARAM-OGG from copybook IDBVPOG1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ParamOgg extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: POG-ID-PARAM-OGG
    private int pogIdParamOgg = DefaultValues.INT_VAL;
    //Original name: POG-ID-OGG
    private int pogIdOgg = DefaultValues.INT_VAL;
    //Original name: POG-TP-OGG
    private String pogTpOgg = DefaultValues.stringVal(Len.POG_TP_OGG);
    //Original name: POG-ID-MOVI-CRZ
    private int pogIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: POG-ID-MOVI-CHIU
    private PogIdMoviChiu pogIdMoviChiu = new PogIdMoviChiu();
    //Original name: POG-DT-INI-EFF
    private int pogDtIniEff = DefaultValues.INT_VAL;
    //Original name: POG-DT-END-EFF
    private int pogDtEndEff = DefaultValues.INT_VAL;
    //Original name: POG-COD-COMP-ANIA
    private int pogCodCompAnia = DefaultValues.INT_VAL;
    //Original name: POG-COD-PARAM
    private String pogCodParam = DefaultValues.stringVal(Len.POG_COD_PARAM);
    //Original name: POG-TP-PARAM
    private char pogTpParam = DefaultValues.CHAR_VAL;
    //Original name: POG-TP-D
    private String pogTpD = DefaultValues.stringVal(Len.POG_TP_D);
    //Original name: POG-VAL-IMP
    private PogValImp pogValImp = new PogValImp();
    //Original name: POG-VAL-DT
    private PogValDt pogValDt = new PogValDt();
    //Original name: POG-VAL-TS
    private PogValTs pogValTs = new PogValTs();
    //Original name: POG-VAL-TXT-LEN
    private short pogValTxtLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: POG-VAL-TXT
    private String pogValTxt = DefaultValues.stringVal(Len.POG_VAL_TXT);
    //Original name: POG-VAL-FL
    private char pogValFl = DefaultValues.CHAR_VAL;
    //Original name: POG-VAL-NUM
    private PogValNum pogValNum = new PogValNum();
    //Original name: POG-VAL-PC
    private PogValPc pogValPc = new PogValPc();
    //Original name: POG-DS-RIGA
    private long pogDsRiga = DefaultValues.LONG_VAL;
    //Original name: POG-DS-OPER-SQL
    private char pogDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: POG-DS-VER
    private int pogDsVer = DefaultValues.INT_VAL;
    //Original name: POG-DS-TS-INI-CPTZ
    private long pogDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: POG-DS-TS-END-CPTZ
    private long pogDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: POG-DS-UTENTE
    private String pogDsUtente = DefaultValues.stringVal(Len.POG_DS_UTENTE);
    //Original name: POG-DS-STATO-ELAB
    private char pogDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PARAM_OGG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setParamOggBytes(buf);
    }

    public void setParamOggFormatted(String data) {
        byte[] buffer = new byte[Len.PARAM_OGG];
        MarshalByte.writeString(buffer, 1, data, Len.PARAM_OGG);
        setParamOggBytes(buffer, 1);
    }

    public String getParamOggFormatted() {
        return MarshalByteExt.bufferToStr(getParamOggBytes());
    }

    public void setParamOggBytes(byte[] buffer) {
        setParamOggBytes(buffer, 1);
    }

    public byte[] getParamOggBytes() {
        byte[] buffer = new byte[Len.PARAM_OGG];
        return getParamOggBytes(buffer, 1);
    }

    public void setParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        pogIdParamOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POG_ID_PARAM_OGG, 0);
        position += Len.POG_ID_PARAM_OGG;
        pogIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POG_ID_OGG, 0);
        position += Len.POG_ID_OGG;
        pogTpOgg = MarshalByte.readString(buffer, position, Len.POG_TP_OGG);
        position += Len.POG_TP_OGG;
        pogIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POG_ID_MOVI_CRZ, 0);
        position += Len.POG_ID_MOVI_CRZ;
        pogIdMoviChiu.setPogIdMoviChiuFromBuffer(buffer, position);
        position += PogIdMoviChiu.Len.POG_ID_MOVI_CHIU;
        pogDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POG_DT_INI_EFF, 0);
        position += Len.POG_DT_INI_EFF;
        pogDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POG_DT_END_EFF, 0);
        position += Len.POG_DT_END_EFF;
        pogCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POG_COD_COMP_ANIA, 0);
        position += Len.POG_COD_COMP_ANIA;
        pogCodParam = MarshalByte.readString(buffer, position, Len.POG_COD_PARAM);
        position += Len.POG_COD_PARAM;
        pogTpParam = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pogTpD = MarshalByte.readString(buffer, position, Len.POG_TP_D);
        position += Len.POG_TP_D;
        pogValImp.setPogValImpFromBuffer(buffer, position);
        position += PogValImp.Len.POG_VAL_IMP;
        pogValDt.setPogValDtFromBuffer(buffer, position);
        position += PogValDt.Len.POG_VAL_DT;
        pogValTs.setPogValTsFromBuffer(buffer, position);
        position += PogValTs.Len.POG_VAL_TS;
        setPogValTxtVcharBytes(buffer, position);
        position += Len.POG_VAL_TXT_VCHAR;
        pogValFl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pogValNum.setPogValNumFromBuffer(buffer, position);
        position += PogValNum.Len.POG_VAL_NUM;
        pogValPc.setPogValPcFromBuffer(buffer, position);
        position += PogValPc.Len.POG_VAL_PC;
        pogDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.POG_DS_RIGA, 0);
        position += Len.POG_DS_RIGA;
        pogDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pogDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POG_DS_VER, 0);
        position += Len.POG_DS_VER;
        pogDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.POG_DS_TS_INI_CPTZ, 0);
        position += Len.POG_DS_TS_INI_CPTZ;
        pogDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.POG_DS_TS_END_CPTZ, 0);
        position += Len.POG_DS_TS_END_CPTZ;
        pogDsUtente = MarshalByte.readString(buffer, position, Len.POG_DS_UTENTE);
        position += Len.POG_DS_UTENTE;
        pogDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, pogIdParamOgg, Len.Int.POG_ID_PARAM_OGG, 0);
        position += Len.POG_ID_PARAM_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, pogIdOgg, Len.Int.POG_ID_OGG, 0);
        position += Len.POG_ID_OGG;
        MarshalByte.writeString(buffer, position, pogTpOgg, Len.POG_TP_OGG);
        position += Len.POG_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, pogIdMoviCrz, Len.Int.POG_ID_MOVI_CRZ, 0);
        position += Len.POG_ID_MOVI_CRZ;
        pogIdMoviChiu.getPogIdMoviChiuAsBuffer(buffer, position);
        position += PogIdMoviChiu.Len.POG_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, pogDtIniEff, Len.Int.POG_DT_INI_EFF, 0);
        position += Len.POG_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, pogDtEndEff, Len.Int.POG_DT_END_EFF, 0);
        position += Len.POG_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, pogCodCompAnia, Len.Int.POG_COD_COMP_ANIA, 0);
        position += Len.POG_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, pogCodParam, Len.POG_COD_PARAM);
        position += Len.POG_COD_PARAM;
        MarshalByte.writeChar(buffer, position, pogTpParam);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, pogTpD, Len.POG_TP_D);
        position += Len.POG_TP_D;
        pogValImp.getPogValImpAsBuffer(buffer, position);
        position += PogValImp.Len.POG_VAL_IMP;
        pogValDt.getPogValDtAsBuffer(buffer, position);
        position += PogValDt.Len.POG_VAL_DT;
        pogValTs.getPogValTsAsBuffer(buffer, position);
        position += PogValTs.Len.POG_VAL_TS;
        getPogValTxtVcharBytes(buffer, position);
        position += Len.POG_VAL_TXT_VCHAR;
        MarshalByte.writeChar(buffer, position, pogValFl);
        position += Types.CHAR_SIZE;
        pogValNum.getPogValNumAsBuffer(buffer, position);
        position += PogValNum.Len.POG_VAL_NUM;
        pogValPc.getPogValPcAsBuffer(buffer, position);
        position += PogValPc.Len.POG_VAL_PC;
        MarshalByte.writeLongAsPacked(buffer, position, pogDsRiga, Len.Int.POG_DS_RIGA, 0);
        position += Len.POG_DS_RIGA;
        MarshalByte.writeChar(buffer, position, pogDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, pogDsVer, Len.Int.POG_DS_VER, 0);
        position += Len.POG_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, pogDsTsIniCptz, Len.Int.POG_DS_TS_INI_CPTZ, 0);
        position += Len.POG_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, pogDsTsEndCptz, Len.Int.POG_DS_TS_END_CPTZ, 0);
        position += Len.POG_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, pogDsUtente, Len.POG_DS_UTENTE);
        position += Len.POG_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, pogDsStatoElab);
        return buffer;
    }

    public void setPogIdParamOgg(int pogIdParamOgg) {
        this.pogIdParamOgg = pogIdParamOgg;
    }

    public int getPogIdParamOgg() {
        return this.pogIdParamOgg;
    }

    public void setPogIdOgg(int pogIdOgg) {
        this.pogIdOgg = pogIdOgg;
    }

    public int getPogIdOgg() {
        return this.pogIdOgg;
    }

    public void setPogTpOgg(String pogTpOgg) {
        this.pogTpOgg = Functions.subString(pogTpOgg, Len.POG_TP_OGG);
    }

    public String getPogTpOgg() {
        return this.pogTpOgg;
    }

    public void setPogIdMoviCrz(int pogIdMoviCrz) {
        this.pogIdMoviCrz = pogIdMoviCrz;
    }

    public int getPogIdMoviCrz() {
        return this.pogIdMoviCrz;
    }

    public void setPogDtIniEff(int pogDtIniEff) {
        this.pogDtIniEff = pogDtIniEff;
    }

    public int getPogDtIniEff() {
        return this.pogDtIniEff;
    }

    public void setPogDtEndEff(int pogDtEndEff) {
        this.pogDtEndEff = pogDtEndEff;
    }

    public int getPogDtEndEff() {
        return this.pogDtEndEff;
    }

    public void setPogCodCompAnia(int pogCodCompAnia) {
        this.pogCodCompAnia = pogCodCompAnia;
    }

    public int getPogCodCompAnia() {
        return this.pogCodCompAnia;
    }

    public void setPogCodParam(String pogCodParam) {
        this.pogCodParam = Functions.subString(pogCodParam, Len.POG_COD_PARAM);
    }

    public String getPogCodParam() {
        return this.pogCodParam;
    }

    public String getPogCodParamFormatted() {
        return Functions.padBlanks(getPogCodParam(), Len.POG_COD_PARAM);
    }

    public void setPogTpParam(char pogTpParam) {
        this.pogTpParam = pogTpParam;
    }

    public char getPogTpParam() {
        return this.pogTpParam;
    }

    public void setPogTpD(String pogTpD) {
        this.pogTpD = Functions.subString(pogTpD, Len.POG_TP_D);
    }

    public String getPogTpD() {
        return this.pogTpD;
    }

    public String getPogTpDFormatted() {
        return Functions.padBlanks(getPogTpD(), Len.POG_TP_D);
    }

    public void setPogValTxtVcharFormatted(String data) {
        byte[] buffer = new byte[Len.POG_VAL_TXT_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.POG_VAL_TXT_VCHAR);
        setPogValTxtVcharBytes(buffer, 1);
    }

    public String getPogValTxtVcharFormatted() {
        return MarshalByteExt.bufferToStr(getPogValTxtVcharBytes());
    }

    /**Original name: POG-VAL-TXT-VCHAR<br>*/
    public byte[] getPogValTxtVcharBytes() {
        byte[] buffer = new byte[Len.POG_VAL_TXT_VCHAR];
        return getPogValTxtVcharBytes(buffer, 1);
    }

    public void setPogValTxtVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        pogValTxtLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        pogValTxt = MarshalByte.readString(buffer, position, Len.POG_VAL_TXT);
    }

    public byte[] getPogValTxtVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, pogValTxtLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, pogValTxt, Len.POG_VAL_TXT);
        return buffer;
    }

    public void setPogValTxtLen(short pogValTxtLen) {
        this.pogValTxtLen = pogValTxtLen;
    }

    public short getPogValTxtLen() {
        return this.pogValTxtLen;
    }

    public void setPogValTxt(String pogValTxt) {
        this.pogValTxt = Functions.subString(pogValTxt, Len.POG_VAL_TXT);
    }

    public String getPogValTxt() {
        return this.pogValTxt;
    }

    public String getPogValTxtFormatted() {
        return Functions.padBlanks(getPogValTxt(), Len.POG_VAL_TXT);
    }

    public void setPogValFl(char pogValFl) {
        this.pogValFl = pogValFl;
    }

    public char getPogValFl() {
        return this.pogValFl;
    }

    public void setPogDsRiga(long pogDsRiga) {
        this.pogDsRiga = pogDsRiga;
    }

    public long getPogDsRiga() {
        return this.pogDsRiga;
    }

    public void setPogDsOperSql(char pogDsOperSql) {
        this.pogDsOperSql = pogDsOperSql;
    }

    public void setPogDsOperSqlFormatted(String pogDsOperSql) {
        setPogDsOperSql(Functions.charAt(pogDsOperSql, Types.CHAR_SIZE));
    }

    public char getPogDsOperSql() {
        return this.pogDsOperSql;
    }

    public void setPogDsVer(int pogDsVer) {
        this.pogDsVer = pogDsVer;
    }

    public int getPogDsVer() {
        return this.pogDsVer;
    }

    public void setPogDsTsIniCptz(long pogDsTsIniCptz) {
        this.pogDsTsIniCptz = pogDsTsIniCptz;
    }

    public long getPogDsTsIniCptz() {
        return this.pogDsTsIniCptz;
    }

    public void setPogDsTsEndCptz(long pogDsTsEndCptz) {
        this.pogDsTsEndCptz = pogDsTsEndCptz;
    }

    public long getPogDsTsEndCptz() {
        return this.pogDsTsEndCptz;
    }

    public void setPogDsUtente(String pogDsUtente) {
        this.pogDsUtente = Functions.subString(pogDsUtente, Len.POG_DS_UTENTE);
    }

    public String getPogDsUtente() {
        return this.pogDsUtente;
    }

    public void setPogDsStatoElab(char pogDsStatoElab) {
        this.pogDsStatoElab = pogDsStatoElab;
    }

    public void setPogDsStatoElabFormatted(String pogDsStatoElab) {
        setPogDsStatoElab(Functions.charAt(pogDsStatoElab, Types.CHAR_SIZE));
    }

    public char getPogDsStatoElab() {
        return this.pogDsStatoElab;
    }

    public PogIdMoviChiu getPogIdMoviChiu() {
        return pogIdMoviChiu;
    }

    public PogValDt getPogValDt() {
        return pogValDt;
    }

    public PogValImp getPogValImp() {
        return pogValImp;
    }

    public PogValNum getPogValNum() {
        return pogValNum;
    }

    public PogValPc getPogValPc() {
        return pogValPc;
    }

    public PogValTs getPogValTs() {
        return pogValTs;
    }

    @Override
    public byte[] serialize() {
        return getParamOggBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int POG_ID_PARAM_OGG = 5;
        public static final int POG_ID_OGG = 5;
        public static final int POG_TP_OGG = 2;
        public static final int POG_ID_MOVI_CRZ = 5;
        public static final int POG_DT_INI_EFF = 5;
        public static final int POG_DT_END_EFF = 5;
        public static final int POG_COD_COMP_ANIA = 3;
        public static final int POG_COD_PARAM = 20;
        public static final int POG_TP_PARAM = 1;
        public static final int POG_TP_D = 2;
        public static final int POG_VAL_TXT_LEN = 2;
        public static final int POG_VAL_TXT = 100;
        public static final int POG_VAL_TXT_VCHAR = POG_VAL_TXT_LEN + POG_VAL_TXT;
        public static final int POG_VAL_FL = 1;
        public static final int POG_DS_RIGA = 6;
        public static final int POG_DS_OPER_SQL = 1;
        public static final int POG_DS_VER = 5;
        public static final int POG_DS_TS_INI_CPTZ = 10;
        public static final int POG_DS_TS_END_CPTZ = 10;
        public static final int POG_DS_UTENTE = 20;
        public static final int POG_DS_STATO_ELAB = 1;
        public static final int PARAM_OGG = POG_ID_PARAM_OGG + POG_ID_OGG + POG_TP_OGG + POG_ID_MOVI_CRZ + PogIdMoviChiu.Len.POG_ID_MOVI_CHIU + POG_DT_INI_EFF + POG_DT_END_EFF + POG_COD_COMP_ANIA + POG_COD_PARAM + POG_TP_PARAM + POG_TP_D + PogValImp.Len.POG_VAL_IMP + PogValDt.Len.POG_VAL_DT + PogValTs.Len.POG_VAL_TS + POG_VAL_TXT_VCHAR + POG_VAL_FL + PogValNum.Len.POG_VAL_NUM + PogValPc.Len.POG_VAL_PC + POG_DS_RIGA + POG_DS_OPER_SQL + POG_DS_VER + POG_DS_TS_INI_CPTZ + POG_DS_TS_END_CPTZ + POG_DS_UTENTE + POG_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POG_ID_PARAM_OGG = 9;
            public static final int POG_ID_OGG = 9;
            public static final int POG_ID_MOVI_CRZ = 9;
            public static final int POG_DT_INI_EFF = 8;
            public static final int POG_DT_END_EFF = 8;
            public static final int POG_COD_COMP_ANIA = 5;
            public static final int POG_DS_RIGA = 10;
            public static final int POG_DS_VER = 9;
            public static final int POG_DS_TS_INI_CPTZ = 18;
            public static final int POG_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
