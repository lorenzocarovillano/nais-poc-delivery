package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-PER-RAT-FINANZ<br>
 * Variable: P67-PER-RAT-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67PerRatFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67PerRatFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_PER_RAT_FINANZ;
    }

    public void setP67PerRatFinanz(int p67PerRatFinanz) {
        writeIntAsPacked(Pos.P67_PER_RAT_FINANZ, p67PerRatFinanz, Len.Int.P67_PER_RAT_FINANZ);
    }

    public void setP67PerRatFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_PER_RAT_FINANZ, Pos.P67_PER_RAT_FINANZ);
    }

    /**Original name: P67-PER-RAT-FINANZ<br>*/
    public int getP67PerRatFinanz() {
        return readPackedAsInt(Pos.P67_PER_RAT_FINANZ, Len.Int.P67_PER_RAT_FINANZ);
    }

    public byte[] getP67PerRatFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_PER_RAT_FINANZ, Pos.P67_PER_RAT_FINANZ);
        return buffer;
    }

    public void setP67PerRatFinanzNull(String p67PerRatFinanzNull) {
        writeString(Pos.P67_PER_RAT_FINANZ_NULL, p67PerRatFinanzNull, Len.P67_PER_RAT_FINANZ_NULL);
    }

    /**Original name: P67-PER-RAT-FINANZ-NULL<br>*/
    public String getP67PerRatFinanzNull() {
        return readString(Pos.P67_PER_RAT_FINANZ_NULL, Len.P67_PER_RAT_FINANZ_NULL);
    }

    public String getP67PerRatFinanzNullFormatted() {
        return Functions.padBlanks(getP67PerRatFinanzNull(), Len.P67_PER_RAT_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_PER_RAT_FINANZ = 1;
        public static final int P67_PER_RAT_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_PER_RAT_FINANZ = 3;
        public static final int P67_PER_RAT_FINANZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_PER_RAT_FINANZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
