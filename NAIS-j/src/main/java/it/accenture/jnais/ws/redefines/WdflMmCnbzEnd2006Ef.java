package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFL-MM-CNBZ-END2006-EF<br>
 * Variable: WDFL-MM-CNBZ-END2006-EF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflMmCnbzEnd2006Ef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflMmCnbzEnd2006Ef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_MM_CNBZ_END2006_EF;
    }

    public void setWdflMmCnbzEnd2006Ef(int wdflMmCnbzEnd2006Ef) {
        writeIntAsPacked(Pos.WDFL_MM_CNBZ_END2006_EF, wdflMmCnbzEnd2006Ef, Len.Int.WDFL_MM_CNBZ_END2006_EF);
    }

    public void setWdflMmCnbzEnd2006EfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_MM_CNBZ_END2006_EF, Pos.WDFL_MM_CNBZ_END2006_EF);
    }

    /**Original name: WDFL-MM-CNBZ-END2006-EF<br>*/
    public int getWdflMmCnbzEnd2006Ef() {
        return readPackedAsInt(Pos.WDFL_MM_CNBZ_END2006_EF, Len.Int.WDFL_MM_CNBZ_END2006_EF);
    }

    public byte[] getWdflMmCnbzEnd2006EfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_MM_CNBZ_END2006_EF, Pos.WDFL_MM_CNBZ_END2006_EF);
        return buffer;
    }

    public void setWdflMmCnbzEnd2006EfNull(String wdflMmCnbzEnd2006EfNull) {
        writeString(Pos.WDFL_MM_CNBZ_END2006_EF_NULL, wdflMmCnbzEnd2006EfNull, Len.WDFL_MM_CNBZ_END2006_EF_NULL);
    }

    /**Original name: WDFL-MM-CNBZ-END2006-EF-NULL<br>*/
    public String getWdflMmCnbzEnd2006EfNull() {
        return readString(Pos.WDFL_MM_CNBZ_END2006_EF_NULL, Len.WDFL_MM_CNBZ_END2006_EF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_MM_CNBZ_END2006_EF = 1;
        public static final int WDFL_MM_CNBZ_END2006_EF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_MM_CNBZ_END2006_EF = 3;
        public static final int WDFL_MM_CNBZ_END2006_EF_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_MM_CNBZ_END2006_EF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
