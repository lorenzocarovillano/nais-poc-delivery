package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-RIT-ACC-IPT<br>
 * Variable: WBEL-RIT-ACC-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelRitAccIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelRitAccIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_RIT_ACC_IPT;
    }

    public void setWbelRitAccIpt(AfDecimal wbelRitAccIpt) {
        writeDecimalAsPacked(Pos.WBEL_RIT_ACC_IPT, wbelRitAccIpt.copy());
    }

    public void setWbelRitAccIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_RIT_ACC_IPT, Pos.WBEL_RIT_ACC_IPT);
    }

    /**Original name: WBEL-RIT-ACC-IPT<br>*/
    public AfDecimal getWbelRitAccIpt() {
        return readPackedAsDecimal(Pos.WBEL_RIT_ACC_IPT, Len.Int.WBEL_RIT_ACC_IPT, Len.Fract.WBEL_RIT_ACC_IPT);
    }

    public byte[] getWbelRitAccIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_RIT_ACC_IPT, Pos.WBEL_RIT_ACC_IPT);
        return buffer;
    }

    public void initWbelRitAccIptSpaces() {
        fill(Pos.WBEL_RIT_ACC_IPT, Len.WBEL_RIT_ACC_IPT, Types.SPACE_CHAR);
    }

    public void setWbelRitAccIptNull(String wbelRitAccIptNull) {
        writeString(Pos.WBEL_RIT_ACC_IPT_NULL, wbelRitAccIptNull, Len.WBEL_RIT_ACC_IPT_NULL);
    }

    /**Original name: WBEL-RIT-ACC-IPT-NULL<br>*/
    public String getWbelRitAccIptNull() {
        return readString(Pos.WBEL_RIT_ACC_IPT_NULL, Len.WBEL_RIT_ACC_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_RIT_ACC_IPT = 1;
        public static final int WBEL_RIT_ACC_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_RIT_ACC_IPT = 8;
        public static final int WBEL_RIT_ACC_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_RIT_ACC_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_RIT_ACC_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
