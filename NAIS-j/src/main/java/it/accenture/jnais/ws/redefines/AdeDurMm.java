package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-DUR-MM<br>
 * Variable: ADE-DUR-MM from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeDurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeDurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_DUR_MM;
    }

    public void setAdeDurMm(int adeDurMm) {
        writeIntAsPacked(Pos.ADE_DUR_MM, adeDurMm, Len.Int.ADE_DUR_MM);
    }

    public void setAdeDurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_DUR_MM, Pos.ADE_DUR_MM);
    }

    /**Original name: ADE-DUR-MM<br>*/
    public int getAdeDurMm() {
        return readPackedAsInt(Pos.ADE_DUR_MM, Len.Int.ADE_DUR_MM);
    }

    public byte[] getAdeDurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_DUR_MM, Pos.ADE_DUR_MM);
        return buffer;
    }

    public void setAdeDurMmNull(String adeDurMmNull) {
        writeString(Pos.ADE_DUR_MM_NULL, adeDurMmNull, Len.ADE_DUR_MM_NULL);
    }

    /**Original name: ADE-DUR-MM-NULL<br>*/
    public String getAdeDurMmNull() {
        return readString(Pos.ADE_DUR_MM_NULL, Len.ADE_DUR_MM_NULL);
    }

    public String getAdeDurMmNullFormatted() {
        return Functions.padBlanks(getAdeDurMmNull(), Len.ADE_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_DUR_MM = 1;
        public static final int ADE_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_DUR_MM = 3;
        public static final int ADE_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
