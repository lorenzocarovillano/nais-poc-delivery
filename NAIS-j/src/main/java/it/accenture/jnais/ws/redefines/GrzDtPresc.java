package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-DT-PRESC<br>
 * Variable: GRZ-DT-PRESC from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzDtPresc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzDtPresc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_DT_PRESC;
    }

    public void setGrzDtPresc(int grzDtPresc) {
        writeIntAsPacked(Pos.GRZ_DT_PRESC, grzDtPresc, Len.Int.GRZ_DT_PRESC);
    }

    public void setGrzDtPrescFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_DT_PRESC, Pos.GRZ_DT_PRESC);
    }

    /**Original name: GRZ-DT-PRESC<br>*/
    public int getGrzDtPresc() {
        return readPackedAsInt(Pos.GRZ_DT_PRESC, Len.Int.GRZ_DT_PRESC);
    }

    public byte[] getGrzDtPrescAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_DT_PRESC, Pos.GRZ_DT_PRESC);
        return buffer;
    }

    public void setGrzDtPrescNull(String grzDtPrescNull) {
        writeString(Pos.GRZ_DT_PRESC_NULL, grzDtPrescNull, Len.GRZ_DT_PRESC_NULL);
    }

    /**Original name: GRZ-DT-PRESC-NULL<br>*/
    public String getGrzDtPrescNull() {
        return readString(Pos.GRZ_DT_PRESC_NULL, Len.GRZ_DT_PRESC_NULL);
    }

    public String getGrzDtPrescNullFormatted() {
        return Functions.padBlanks(getGrzDtPrescNull(), Len.GRZ_DT_PRESC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_DT_PRESC = 1;
        public static final int GRZ_DT_PRESC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_DT_PRESC = 5;
        public static final int GRZ_DT_PRESC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_DT_PRESC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
