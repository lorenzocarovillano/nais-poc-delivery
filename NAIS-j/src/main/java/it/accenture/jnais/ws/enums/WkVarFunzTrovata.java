package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-VAR-FUNZ-TROVATA<br>
 * Variable: WK-VAR-FUNZ-TROVATA from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkVarFunzTrovata {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_VAR_FUNZ_TROVATA);
    public static final String NO = "NO";
    public static final String SI = "SI";

    //==== METHODS ====
    public void setWkVarFunzTrovata(String wkVarFunzTrovata) {
        this.value = Functions.subString(wkVarFunzTrovata, Len.WK_VAR_FUNZ_TROVATA);
    }

    public String getWkVarFunzTrovata() {
        return this.value;
    }

    public boolean isNo() {
        return value.equals(NO);
    }

    public void setNo() {
        value = NO;
    }

    public void setSi() {
        value = SI;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_VAR_FUNZ_TROVATA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
