package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-DT-ELAB<br>
 * Variable: RST-DT-ELAB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstDtElab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstDtElab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_DT_ELAB;
    }

    public void setRstDtElab(int rstDtElab) {
        writeIntAsPacked(Pos.RST_DT_ELAB, rstDtElab, Len.Int.RST_DT_ELAB);
    }

    public void setRstDtElabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_DT_ELAB, Pos.RST_DT_ELAB);
    }

    /**Original name: RST-DT-ELAB<br>*/
    public int getRstDtElab() {
        return readPackedAsInt(Pos.RST_DT_ELAB, Len.Int.RST_DT_ELAB);
    }

    public byte[] getRstDtElabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_DT_ELAB, Pos.RST_DT_ELAB);
        return buffer;
    }

    public void setRstDtElabNull(String rstDtElabNull) {
        writeString(Pos.RST_DT_ELAB_NULL, rstDtElabNull, Len.RST_DT_ELAB_NULL);
    }

    /**Original name: RST-DT-ELAB-NULL<br>*/
    public String getRstDtElabNull() {
        return readString(Pos.RST_DT_ELAB_NULL, Len.RST_DT_ELAB_NULL);
    }

    public String getRstDtElabNullFormatted() {
        return Functions.padBlanks(getRstDtElabNull(), Len.RST_DT_ELAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_DT_ELAB = 1;
        public static final int RST_DT_ELAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_DT_ELAB = 5;
        public static final int RST_DT_ELAB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_DT_ELAB = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
