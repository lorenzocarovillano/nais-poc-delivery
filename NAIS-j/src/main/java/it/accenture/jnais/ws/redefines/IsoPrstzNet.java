package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-PRSTZ-NET<br>
 * Variable: ISO-PRSTZ-NET from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoPrstzNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoPrstzNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_PRSTZ_NET;
    }

    public void setIsoPrstzNet(AfDecimal isoPrstzNet) {
        writeDecimalAsPacked(Pos.ISO_PRSTZ_NET, isoPrstzNet.copy());
    }

    public void setIsoPrstzNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_PRSTZ_NET, Pos.ISO_PRSTZ_NET);
    }

    /**Original name: ISO-PRSTZ-NET<br>*/
    public AfDecimal getIsoPrstzNet() {
        return readPackedAsDecimal(Pos.ISO_PRSTZ_NET, Len.Int.ISO_PRSTZ_NET, Len.Fract.ISO_PRSTZ_NET);
    }

    public byte[] getIsoPrstzNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_PRSTZ_NET, Pos.ISO_PRSTZ_NET);
        return buffer;
    }

    public void setIsoPrstzNetNull(String isoPrstzNetNull) {
        writeString(Pos.ISO_PRSTZ_NET_NULL, isoPrstzNetNull, Len.ISO_PRSTZ_NET_NULL);
    }

    /**Original name: ISO-PRSTZ-NET-NULL<br>*/
    public String getIsoPrstzNetNull() {
        return readString(Pos.ISO_PRSTZ_NET_NULL, Len.ISO_PRSTZ_NET_NULL);
    }

    public String getIsoPrstzNetNullFormatted() {
        return Functions.padBlanks(getIsoPrstzNetNull(), Len.ISO_PRSTZ_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_PRSTZ_NET = 1;
        public static final int ISO_PRSTZ_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_PRSTZ_NET = 8;
        public static final int ISO_PRSTZ_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_PRSTZ_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISO_PRSTZ_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
