package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-MONT-DAL2007<br>
 * Variable: LQU-MONT-DAL2007 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquMontDal2007 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquMontDal2007() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_MONT_DAL2007;
    }

    public void setLquMontDal2007(AfDecimal lquMontDal2007) {
        writeDecimalAsPacked(Pos.LQU_MONT_DAL2007, lquMontDal2007.copy());
    }

    public void setLquMontDal2007FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_MONT_DAL2007, Pos.LQU_MONT_DAL2007);
    }

    /**Original name: LQU-MONT-DAL2007<br>*/
    public AfDecimal getLquMontDal2007() {
        return readPackedAsDecimal(Pos.LQU_MONT_DAL2007, Len.Int.LQU_MONT_DAL2007, Len.Fract.LQU_MONT_DAL2007);
    }

    public byte[] getLquMontDal2007AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_MONT_DAL2007, Pos.LQU_MONT_DAL2007);
        return buffer;
    }

    public void setLquMontDal2007Null(String lquMontDal2007Null) {
        writeString(Pos.LQU_MONT_DAL2007_NULL, lquMontDal2007Null, Len.LQU_MONT_DAL2007_NULL);
    }

    /**Original name: LQU-MONT-DAL2007-NULL<br>*/
    public String getLquMontDal2007Null() {
        return readString(Pos.LQU_MONT_DAL2007_NULL, Len.LQU_MONT_DAL2007_NULL);
    }

    public String getLquMontDal2007NullFormatted() {
        return Functions.padBlanks(getLquMontDal2007Null(), Len.LQU_MONT_DAL2007_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_MONT_DAL2007 = 1;
        public static final int LQU_MONT_DAL2007_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_MONT_DAL2007 = 8;
        public static final int LQU_MONT_DAL2007_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_MONT_DAL2007 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_MONT_DAL2007 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
