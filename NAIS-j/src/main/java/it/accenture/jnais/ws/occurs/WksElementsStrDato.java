package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WKS-ELEMENTS-STR-DATO<br>
 * Variables: WKS-ELEMENTS-STR-DATO from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WksElementsStrDato {

    //==== PROPERTIES ====
    //Original name: WKS-CODICE-DATO
    private String codiceDato = DefaultValues.stringVal(Len.CODICE_DATO);
    //Original name: WKS-TIPO-DATO
    private String tipoDato = DefaultValues.stringVal(Len.TIPO_DATO);
    //Original name: WKS-INI-POSI
    private long iniPosi = DefaultValues.BIN_LONG_VAL;
    //Original name: WKS-LUN-DATO
    private long lunDato = DefaultValues.BIN_LONG_VAL;
    //Original name: WKS-LUNGHEZZA-DATO-NOM
    private long lunghezzaDatoNom = DefaultValues.BIN_LONG_VAL;
    //Original name: WKS-PRECISIONE-DATO
    private int precisioneDato = DefaultValues.BIN_INT_VAL;

    //==== METHODS ====
    public void setCodiceDato(String codiceDato) {
        this.codiceDato = Functions.subString(codiceDato, Len.CODICE_DATO);
    }

    public String getCodiceDato() {
        return this.codiceDato;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = Functions.subString(tipoDato, Len.TIPO_DATO);
    }

    public String getTipoDato() {
        return this.tipoDato;
    }

    public void setIniPosi(long iniPosi) {
        this.iniPosi = iniPosi;
    }

    public long getIniPosi() {
        return this.iniPosi;
    }

    public void setLunDato(long lunDato) {
        this.lunDato = lunDato;
    }

    public long getLunDato() {
        return this.lunDato;
    }

    public void setLunghezzaDatoNom(long lunghezzaDatoNom) {
        this.lunghezzaDatoNom = lunghezzaDatoNom;
    }

    public long getLunghezzaDatoNom() {
        return this.lunghezzaDatoNom;
    }

    public void setPrecisioneDato(int precisioneDato) {
        this.precisioneDato = precisioneDato;
    }

    public int getPrecisioneDato() {
        return this.precisioneDato;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_DATO = 30;
        public static final int TIPO_DATO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
