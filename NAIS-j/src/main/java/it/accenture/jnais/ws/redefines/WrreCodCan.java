package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRRE-COD-CAN<br>
 * Variable: WRRE-COD-CAN from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrreCodCan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrreCodCan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRRE_COD_CAN;
    }

    public void setWrreCodCan(int wrreCodCan) {
        writeIntAsPacked(Pos.WRRE_COD_CAN, wrreCodCan, Len.Int.WRRE_COD_CAN);
    }

    public void setWrreCodCanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRRE_COD_CAN, Pos.WRRE_COD_CAN);
    }

    /**Original name: WRRE-COD-CAN<br>*/
    public int getWrreCodCan() {
        return readPackedAsInt(Pos.WRRE_COD_CAN, Len.Int.WRRE_COD_CAN);
    }

    public byte[] getWrreCodCanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRRE_COD_CAN, Pos.WRRE_COD_CAN);
        return buffer;
    }

    public void initWrreCodCanSpaces() {
        fill(Pos.WRRE_COD_CAN, Len.WRRE_COD_CAN, Types.SPACE_CHAR);
    }

    public void setWrreCodCanNull(String wrreCodCanNull) {
        writeString(Pos.WRRE_COD_CAN_NULL, wrreCodCanNull, Len.WRRE_COD_CAN_NULL);
    }

    /**Original name: WRRE-COD-CAN-NULL<br>*/
    public String getWrreCodCanNull() {
        return readString(Pos.WRRE_COD_CAN_NULL, Len.WRRE_COD_CAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRRE_COD_CAN = 1;
        public static final int WRRE_COD_CAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRRE_COD_CAN = 3;
        public static final int WRRE_COD_CAN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRRE_COD_CAN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
