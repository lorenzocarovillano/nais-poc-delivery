package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ACCPRE-VIS-EFFLQ<br>
 * Variable: WDFL-ACCPRE-VIS-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAccpreVisEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAccpreVisEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ACCPRE_VIS_EFFLQ;
    }

    public void setWdflAccpreVisEfflq(AfDecimal wdflAccpreVisEfflq) {
        writeDecimalAsPacked(Pos.WDFL_ACCPRE_VIS_EFFLQ, wdflAccpreVisEfflq.copy());
    }

    public void setWdflAccpreVisEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ACCPRE_VIS_EFFLQ, Pos.WDFL_ACCPRE_VIS_EFFLQ);
    }

    /**Original name: WDFL-ACCPRE-VIS-EFFLQ<br>*/
    public AfDecimal getWdflAccpreVisEfflq() {
        return readPackedAsDecimal(Pos.WDFL_ACCPRE_VIS_EFFLQ, Len.Int.WDFL_ACCPRE_VIS_EFFLQ, Len.Fract.WDFL_ACCPRE_VIS_EFFLQ);
    }

    public byte[] getWdflAccpreVisEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ACCPRE_VIS_EFFLQ, Pos.WDFL_ACCPRE_VIS_EFFLQ);
        return buffer;
    }

    public void setWdflAccpreVisEfflqNull(String wdflAccpreVisEfflqNull) {
        writeString(Pos.WDFL_ACCPRE_VIS_EFFLQ_NULL, wdflAccpreVisEfflqNull, Len.WDFL_ACCPRE_VIS_EFFLQ_NULL);
    }

    /**Original name: WDFL-ACCPRE-VIS-EFFLQ-NULL<br>*/
    public String getWdflAccpreVisEfflqNull() {
        return readString(Pos.WDFL_ACCPRE_VIS_EFFLQ_NULL, Len.WDFL_ACCPRE_VIS_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_VIS_EFFLQ = 1;
        public static final int WDFL_ACCPRE_VIS_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_VIS_EFFLQ = 8;
        public static final int WDFL_ACCPRE_VIS_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_VIS_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_VIS_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
