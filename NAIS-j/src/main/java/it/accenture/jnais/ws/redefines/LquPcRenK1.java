package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-PC-REN-K1<br>
 * Variable: LQU-PC-REN-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquPcRenK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquPcRenK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_PC_REN_K1;
    }

    public void setLquPcRenK1(AfDecimal lquPcRenK1) {
        writeDecimalAsPacked(Pos.LQU_PC_REN_K1, lquPcRenK1.copy());
    }

    public void setLquPcRenK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_PC_REN_K1, Pos.LQU_PC_REN_K1);
    }

    /**Original name: LQU-PC-REN-K1<br>*/
    public AfDecimal getLquPcRenK1() {
        return readPackedAsDecimal(Pos.LQU_PC_REN_K1, Len.Int.LQU_PC_REN_K1, Len.Fract.LQU_PC_REN_K1);
    }

    public byte[] getLquPcRenK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_PC_REN_K1, Pos.LQU_PC_REN_K1);
        return buffer;
    }

    public void setLquPcRenK1Null(String lquPcRenK1Null) {
        writeString(Pos.LQU_PC_REN_K1_NULL, lquPcRenK1Null, Len.LQU_PC_REN_K1_NULL);
    }

    /**Original name: LQU-PC-REN-K1-NULL<br>*/
    public String getLquPcRenK1Null() {
        return readString(Pos.LQU_PC_REN_K1_NULL, Len.LQU_PC_REN_K1_NULL);
    }

    public String getLquPcRenK1NullFormatted() {
        return Functions.padBlanks(getLquPcRenK1Null(), Len.LQU_PC_REN_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_PC_REN_K1 = 1;
        public static final int LQU_PC_REN_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_PC_REN_K1 = 4;
        public static final int LQU_PC_REN_K1_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_PC_REN_K1 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_PC_REN_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
