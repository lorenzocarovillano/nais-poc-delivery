package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.redefines.WpmoTab;

/**Original name: WPMO-AREA-PARAM-MOV<br>
 * Variable: WPMO-AREA-PARAM-MOV from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WpmoAreaParamMov {

    //==== PROPERTIES ====
    //Original name: WPMO-ELE-PARAM-MOV-MAX
    private short wpmoEleParamMovMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPMO-TAB
    private WpmoTab wpmoTab = new WpmoTab();

    //==== METHODS ====
    public void setWpmoAreaParamMovFormatted(String data) {
        byte[] buffer = new byte[Len.WPMO_AREA_PARAM_MOV];
        MarshalByte.writeString(buffer, 1, data, Len.WPMO_AREA_PARAM_MOV);
        setWpmoAreaParamMovBytes(buffer, 1);
    }

    public void setWpmoAreaParamMovBytes(byte[] buffer, int offset) {
        int position = offset;
        wpmoEleParamMovMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wpmoTab.setWpmoTabBytes(buffer, position);
    }

    public byte[] getWpmoAreaParamMovBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpmoEleParamMovMax);
        position += Types.SHORT_SIZE;
        wpmoTab.getWpmoTabBytes(buffer, position);
        return buffer;
    }

    public void setWpmoEleParamMovMax(short wpmoEleParamMovMax) {
        this.wpmoEleParamMovMax = wpmoEleParamMovMax;
    }

    public short getWpmoEleParamMovMax() {
        return this.wpmoEleParamMovMax;
    }

    public WpmoTab getWpmoTab() {
        return wpmoTab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_ELE_PARAM_MOV_MAX = 2;
        public static final int WPMO_AREA_PARAM_MOV = WPMO_ELE_PARAM_MOV_MAX + WpmoTab.Len.WPMO_TAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
