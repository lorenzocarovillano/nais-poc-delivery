package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-SOPR-SANIT<br>
 * Variable: WPAG-CONT-SOPR-SANIT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContSoprSanit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContSoprSanit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_SOPR_SANIT;
    }

    public void setWpagContSoprSanit(AfDecimal wpagContSoprSanit) {
        writeDecimalAsPacked(Pos.WPAG_CONT_SOPR_SANIT, wpagContSoprSanit.copy());
    }

    public void setWpagContSoprSanitFormatted(String wpagContSoprSanit) {
        setWpagContSoprSanit(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_SOPR_SANIT + Len.Fract.WPAG_CONT_SOPR_SANIT, Len.Fract.WPAG_CONT_SOPR_SANIT, wpagContSoprSanit));
    }

    public void setWpagContSoprSanitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_SOPR_SANIT, Pos.WPAG_CONT_SOPR_SANIT);
    }

    /**Original name: WPAG-CONT-SOPR-SANIT<br>*/
    public AfDecimal getWpagContSoprSanit() {
        return readPackedAsDecimal(Pos.WPAG_CONT_SOPR_SANIT, Len.Int.WPAG_CONT_SOPR_SANIT, Len.Fract.WPAG_CONT_SOPR_SANIT);
    }

    public byte[] getWpagContSoprSanitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_SOPR_SANIT, Pos.WPAG_CONT_SOPR_SANIT);
        return buffer;
    }

    public void initWpagContSoprSanitSpaces() {
        fill(Pos.WPAG_CONT_SOPR_SANIT, Len.WPAG_CONT_SOPR_SANIT, Types.SPACE_CHAR);
    }

    public void setWpagContSoprSanitNull(String wpagContSoprSanitNull) {
        writeString(Pos.WPAG_CONT_SOPR_SANIT_NULL, wpagContSoprSanitNull, Len.WPAG_CONT_SOPR_SANIT_NULL);
    }

    /**Original name: WPAG-CONT-SOPR-SANIT-NULL<br>*/
    public String getWpagContSoprSanitNull() {
        return readString(Pos.WPAG_CONT_SOPR_SANIT_NULL, Len.WPAG_CONT_SOPR_SANIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_SOPR_SANIT = 1;
        public static final int WPAG_CONT_SOPR_SANIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_SOPR_SANIT = 8;
        public static final int WPAG_CONT_SOPR_SANIT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_SOPR_SANIT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_SOPR_SANIT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
