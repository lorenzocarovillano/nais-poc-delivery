package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-PRE-NET<br>
 * Variable: TIT-TOT-PRE-NET from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotPreNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotPreNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_PRE_NET;
    }

    public void setTitTotPreNet(AfDecimal titTotPreNet) {
        writeDecimalAsPacked(Pos.TIT_TOT_PRE_NET, titTotPreNet.copy());
    }

    public void setTitTotPreNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_PRE_NET, Pos.TIT_TOT_PRE_NET);
    }

    /**Original name: TIT-TOT-PRE-NET<br>*/
    public AfDecimal getTitTotPreNet() {
        return readPackedAsDecimal(Pos.TIT_TOT_PRE_NET, Len.Int.TIT_TOT_PRE_NET, Len.Fract.TIT_TOT_PRE_NET);
    }

    public byte[] getTitTotPreNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_PRE_NET, Pos.TIT_TOT_PRE_NET);
        return buffer;
    }

    public void setTitTotPreNetNull(String titTotPreNetNull) {
        writeString(Pos.TIT_TOT_PRE_NET_NULL, titTotPreNetNull, Len.TIT_TOT_PRE_NET_NULL);
    }

    /**Original name: TIT-TOT-PRE-NET-NULL<br>*/
    public String getTitTotPreNetNull() {
        return readString(Pos.TIT_TOT_PRE_NET_NULL, Len.TIT_TOT_PRE_NET_NULL);
    }

    public String getTitTotPreNetNullFormatted() {
        return Functions.padBlanks(getTitTotPreNetNull(), Len.TIT_TOT_PRE_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PRE_NET = 1;
        public static final int TIT_TOT_PRE_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PRE_NET = 8;
        public static final int TIT_TOT_PRE_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PRE_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PRE_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
