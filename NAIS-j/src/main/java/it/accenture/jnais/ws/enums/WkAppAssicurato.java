package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-APP-ASSICURATO<br>
 * Variable: WK-APP-ASSICURATO from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkAppAssicurato {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_APP_ASSICURATO);
    public static final String PRIMO_ASS = "PRIMO";
    public static final String SECONDO_ASS = "SECON";
    public static final String TERZO_ASS = "TERZO";
    public static final String TP_OGG_PO = "TPOPO";
    public static final String TP_OGG_AD = "TPOAD";
    public static final String TP_DEC_PO = "DECPO";
    public static final String TP_DEC_AD = "DECAD";

    //==== METHODS ====
    public void setWkAppAssicurato(String wkAppAssicurato) {
        this.value = Functions.subString(wkAppAssicurato, Len.WK_APP_ASSICURATO);
    }

    public String getWkAppAssicurato() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_APP_ASSICURATO = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
