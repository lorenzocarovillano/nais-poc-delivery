package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-IMPST-SOST<br>
 * Variable: ISO-IMPST-SOST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoImpstSost extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoImpstSost() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_IMPST_SOST;
    }

    public void setIsoImpstSost(AfDecimal isoImpstSost) {
        writeDecimalAsPacked(Pos.ISO_IMPST_SOST, isoImpstSost.copy());
    }

    public void setIsoImpstSostFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_IMPST_SOST, Pos.ISO_IMPST_SOST);
    }

    /**Original name: ISO-IMPST-SOST<br>*/
    public AfDecimal getIsoImpstSost() {
        return readPackedAsDecimal(Pos.ISO_IMPST_SOST, Len.Int.ISO_IMPST_SOST, Len.Fract.ISO_IMPST_SOST);
    }

    public byte[] getIsoImpstSostAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_IMPST_SOST, Pos.ISO_IMPST_SOST);
        return buffer;
    }

    public void setIsoImpstSostNull(String isoImpstSostNull) {
        writeString(Pos.ISO_IMPST_SOST_NULL, isoImpstSostNull, Len.ISO_IMPST_SOST_NULL);
    }

    /**Original name: ISO-IMPST-SOST-NULL<br>*/
    public String getIsoImpstSostNull() {
        return readString(Pos.ISO_IMPST_SOST_NULL, Len.ISO_IMPST_SOST_NULL);
    }

    public String getIsoImpstSostNullFormatted() {
        return Functions.padBlanks(getIsoImpstSostNull(), Len.ISO_IMPST_SOST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_IMPST_SOST = 1;
        public static final int ISO_IMPST_SOST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_IMPST_SOST = 8;
        public static final int ISO_IMPST_SOST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_IMPST_SOST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISO_IMPST_SOST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
