package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-COMMIS-INTER<br>
 * Variable: WDTC-COMMIS-INTER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_COMMIS_INTER;
    }

    public void setWdtcCommisInter(AfDecimal wdtcCommisInter) {
        writeDecimalAsPacked(Pos.WDTC_COMMIS_INTER, wdtcCommisInter.copy());
    }

    public void setWdtcCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_COMMIS_INTER, Pos.WDTC_COMMIS_INTER);
    }

    /**Original name: WDTC-COMMIS-INTER<br>*/
    public AfDecimal getWdtcCommisInter() {
        return readPackedAsDecimal(Pos.WDTC_COMMIS_INTER, Len.Int.WDTC_COMMIS_INTER, Len.Fract.WDTC_COMMIS_INTER);
    }

    public byte[] getWdtcCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_COMMIS_INTER, Pos.WDTC_COMMIS_INTER);
        return buffer;
    }

    public void initWdtcCommisInterSpaces() {
        fill(Pos.WDTC_COMMIS_INTER, Len.WDTC_COMMIS_INTER, Types.SPACE_CHAR);
    }

    public void setWdtcCommisInterNull(String wdtcCommisInterNull) {
        writeString(Pos.WDTC_COMMIS_INTER_NULL, wdtcCommisInterNull, Len.WDTC_COMMIS_INTER_NULL);
    }

    /**Original name: WDTC-COMMIS-INTER-NULL<br>*/
    public String getWdtcCommisInterNull() {
        return readString(Pos.WDTC_COMMIS_INTER_NULL, Len.WDTC_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_COMMIS_INTER = 1;
        public static final int WDTC_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_COMMIS_INTER = 8;
        public static final int WDTC_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
