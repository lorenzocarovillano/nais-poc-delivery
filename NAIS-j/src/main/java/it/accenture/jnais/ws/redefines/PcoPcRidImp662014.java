package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-PC-RID-IMP-662014<br>
 * Variable: PCO-PC-RID-IMP-662014 from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoPcRidImp662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoPcRidImp662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_PC_RID_IMP662014;
    }

    public void setPcoPcRidImp662014(AfDecimal pcoPcRidImp662014) {
        writeDecimalAsPacked(Pos.PCO_PC_RID_IMP662014, pcoPcRidImp662014.copy());
    }

    public void setPcoPcRidImp662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_PC_RID_IMP662014, Pos.PCO_PC_RID_IMP662014);
    }

    /**Original name: PCO-PC-RID-IMP-662014<br>*/
    public AfDecimal getPcoPcRidImp662014() {
        return readPackedAsDecimal(Pos.PCO_PC_RID_IMP662014, Len.Int.PCO_PC_RID_IMP662014, Len.Fract.PCO_PC_RID_IMP662014);
    }

    public byte[] getPcoPcRidImp662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_PC_RID_IMP662014, Pos.PCO_PC_RID_IMP662014);
        return buffer;
    }

    public void initPcoPcRidImp662014HighValues() {
        fill(Pos.PCO_PC_RID_IMP662014, Len.PCO_PC_RID_IMP662014, Types.HIGH_CHAR_VAL);
    }

    public void setPcoPcRidImp662014Null(String pcoPcRidImp662014Null) {
        writeString(Pos.PCO_PC_RID_IMP662014_NULL, pcoPcRidImp662014Null, Len.PCO_PC_RID_IMP662014_NULL);
    }

    /**Original name: PCO-PC-RID-IMP-662014-NULL<br>*/
    public String getPcoPcRidImp662014Null() {
        return readString(Pos.PCO_PC_RID_IMP662014_NULL, Len.PCO_PC_RID_IMP662014_NULL);
    }

    public String getPcoPcRidImp662014NullFormatted() {
        return Functions.padBlanks(getPcoPcRidImp662014Null(), Len.PCO_PC_RID_IMP662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_PC_RID_IMP662014 = 1;
        public static final int PCO_PC_RID_IMP662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_PC_RID_IMP662014 = 4;
        public static final int PCO_PC_RID_IMP662014_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_PC_RID_IMP662014 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_PC_RID_IMP662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
