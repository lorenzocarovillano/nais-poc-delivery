package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ACCPRE-ACC-EFFLQ<br>
 * Variable: DFL-ACCPRE-ACC-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAccpreAccEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAccpreAccEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ACCPRE_ACC_EFFLQ;
    }

    public void setDflAccpreAccEfflq(AfDecimal dflAccpreAccEfflq) {
        writeDecimalAsPacked(Pos.DFL_ACCPRE_ACC_EFFLQ, dflAccpreAccEfflq.copy());
    }

    public void setDflAccpreAccEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ACCPRE_ACC_EFFLQ, Pos.DFL_ACCPRE_ACC_EFFLQ);
    }

    /**Original name: DFL-ACCPRE-ACC-EFFLQ<br>*/
    public AfDecimal getDflAccpreAccEfflq() {
        return readPackedAsDecimal(Pos.DFL_ACCPRE_ACC_EFFLQ, Len.Int.DFL_ACCPRE_ACC_EFFLQ, Len.Fract.DFL_ACCPRE_ACC_EFFLQ);
    }

    public byte[] getDflAccpreAccEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ACCPRE_ACC_EFFLQ, Pos.DFL_ACCPRE_ACC_EFFLQ);
        return buffer;
    }

    public void setDflAccpreAccEfflqNull(String dflAccpreAccEfflqNull) {
        writeString(Pos.DFL_ACCPRE_ACC_EFFLQ_NULL, dflAccpreAccEfflqNull, Len.DFL_ACCPRE_ACC_EFFLQ_NULL);
    }

    /**Original name: DFL-ACCPRE-ACC-EFFLQ-NULL<br>*/
    public String getDflAccpreAccEfflqNull() {
        return readString(Pos.DFL_ACCPRE_ACC_EFFLQ_NULL, Len.DFL_ACCPRE_ACC_EFFLQ_NULL);
    }

    public String getDflAccpreAccEfflqNullFormatted() {
        return Functions.padBlanks(getDflAccpreAccEfflqNull(), Len.DFL_ACCPRE_ACC_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_ACC_EFFLQ = 1;
        public static final int DFL_ACCPRE_ACC_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_ACC_EFFLQ = 8;
        public static final int DFL_ACCPRE_ACC_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_ACC_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_ACC_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
