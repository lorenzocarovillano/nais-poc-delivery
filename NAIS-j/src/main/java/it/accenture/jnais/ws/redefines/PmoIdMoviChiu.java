package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-ID-MOVI-CHIU<br>
 * Variable: PMO-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_ID_MOVI_CHIU;
    }

    public void setPmoIdMoviChiu(int pmoIdMoviChiu) {
        writeIntAsPacked(Pos.PMO_ID_MOVI_CHIU, pmoIdMoviChiu, Len.Int.PMO_ID_MOVI_CHIU);
    }

    public void setPmoIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_ID_MOVI_CHIU, Pos.PMO_ID_MOVI_CHIU);
    }

    /**Original name: PMO-ID-MOVI-CHIU<br>*/
    public int getPmoIdMoviChiu() {
        return readPackedAsInt(Pos.PMO_ID_MOVI_CHIU, Len.Int.PMO_ID_MOVI_CHIU);
    }

    public byte[] getPmoIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_ID_MOVI_CHIU, Pos.PMO_ID_MOVI_CHIU);
        return buffer;
    }

    public void initPmoIdMoviChiuHighValues() {
        fill(Pos.PMO_ID_MOVI_CHIU, Len.PMO_ID_MOVI_CHIU, Types.HIGH_CHAR_VAL);
    }

    public void setPmoIdMoviChiuNull(String pmoIdMoviChiuNull) {
        writeString(Pos.PMO_ID_MOVI_CHIU_NULL, pmoIdMoviChiuNull, Len.PMO_ID_MOVI_CHIU_NULL);
    }

    /**Original name: PMO-ID-MOVI-CHIU-NULL<br>*/
    public String getPmoIdMoviChiuNull() {
        return readString(Pos.PMO_ID_MOVI_CHIU_NULL, Len.PMO_ID_MOVI_CHIU_NULL);
    }

    public String getPmoIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getPmoIdMoviChiuNull(), Len.PMO_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_ID_MOVI_CHIU = 1;
        public static final int PMO_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_ID_MOVI_CHIU = 5;
        public static final int PMO_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
