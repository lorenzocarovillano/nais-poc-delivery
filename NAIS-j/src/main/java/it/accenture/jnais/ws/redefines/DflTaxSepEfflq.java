package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-TAX-SEP-EFFLQ<br>
 * Variable: DFL-TAX-SEP-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflTaxSepEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflTaxSepEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_TAX_SEP_EFFLQ;
    }

    public void setDflTaxSepEfflq(AfDecimal dflTaxSepEfflq) {
        writeDecimalAsPacked(Pos.DFL_TAX_SEP_EFFLQ, dflTaxSepEfflq.copy());
    }

    public void setDflTaxSepEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_TAX_SEP_EFFLQ, Pos.DFL_TAX_SEP_EFFLQ);
    }

    /**Original name: DFL-TAX-SEP-EFFLQ<br>*/
    public AfDecimal getDflTaxSepEfflq() {
        return readPackedAsDecimal(Pos.DFL_TAX_SEP_EFFLQ, Len.Int.DFL_TAX_SEP_EFFLQ, Len.Fract.DFL_TAX_SEP_EFFLQ);
    }

    public byte[] getDflTaxSepEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_TAX_SEP_EFFLQ, Pos.DFL_TAX_SEP_EFFLQ);
        return buffer;
    }

    public void setDflTaxSepEfflqNull(String dflTaxSepEfflqNull) {
        writeString(Pos.DFL_TAX_SEP_EFFLQ_NULL, dflTaxSepEfflqNull, Len.DFL_TAX_SEP_EFFLQ_NULL);
    }

    /**Original name: DFL-TAX-SEP-EFFLQ-NULL<br>*/
    public String getDflTaxSepEfflqNull() {
        return readString(Pos.DFL_TAX_SEP_EFFLQ_NULL, Len.DFL_TAX_SEP_EFFLQ_NULL);
    }

    public String getDflTaxSepEfflqNullFormatted() {
        return Functions.padBlanks(getDflTaxSepEfflqNull(), Len.DFL_TAX_SEP_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_TAX_SEP_EFFLQ = 1;
        public static final int DFL_TAX_SEP_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_TAX_SEP_EFFLQ = 8;
        public static final int DFL_TAX_SEP_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_TAX_SEP_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_TAX_SEP_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
