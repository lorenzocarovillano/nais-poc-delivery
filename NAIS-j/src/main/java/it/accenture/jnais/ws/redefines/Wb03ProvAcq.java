package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PROV-ACQ<br>
 * Variable: WB03-PROV-ACQ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03ProvAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03ProvAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PROV_ACQ;
    }

    public void setWb03ProvAcq(AfDecimal wb03ProvAcq) {
        writeDecimalAsPacked(Pos.WB03_PROV_ACQ, wb03ProvAcq.copy());
    }

    public void setWb03ProvAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PROV_ACQ, Pos.WB03_PROV_ACQ);
    }

    /**Original name: WB03-PROV-ACQ<br>*/
    public AfDecimal getWb03ProvAcq() {
        return readPackedAsDecimal(Pos.WB03_PROV_ACQ, Len.Int.WB03_PROV_ACQ, Len.Fract.WB03_PROV_ACQ);
    }

    public byte[] getWb03ProvAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PROV_ACQ, Pos.WB03_PROV_ACQ);
        return buffer;
    }

    public void setWb03ProvAcqNull(String wb03ProvAcqNull) {
        writeString(Pos.WB03_PROV_ACQ_NULL, wb03ProvAcqNull, Len.WB03_PROV_ACQ_NULL);
    }

    /**Original name: WB03-PROV-ACQ-NULL<br>*/
    public String getWb03ProvAcqNull() {
        return readString(Pos.WB03_PROV_ACQ_NULL, Len.WB03_PROV_ACQ_NULL);
    }

    public String getWb03ProvAcqNullFormatted() {
        return Functions.padBlanks(getWb03ProvAcqNull(), Len.WB03_PROV_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PROV_ACQ = 1;
        public static final int WB03_PROV_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PROV_ACQ = 8;
        public static final int WB03_PROV_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PROV_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
