package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WOCO-DT-ULT-PRE-PAG<br>
 * Variable: WOCO-DT-ULT-PRE-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoDtUltPrePag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoDtUltPrePag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_DT_ULT_PRE_PAG;
    }

    public void setWocoDtUltPrePag(int wocoDtUltPrePag) {
        writeIntAsPacked(Pos.WOCO_DT_ULT_PRE_PAG, wocoDtUltPrePag, Len.Int.WOCO_DT_ULT_PRE_PAG);
    }

    public void setWocoDtUltPrePagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_DT_ULT_PRE_PAG, Pos.WOCO_DT_ULT_PRE_PAG);
    }

    /**Original name: WOCO-DT-ULT-PRE-PAG<br>*/
    public int getWocoDtUltPrePag() {
        return readPackedAsInt(Pos.WOCO_DT_ULT_PRE_PAG, Len.Int.WOCO_DT_ULT_PRE_PAG);
    }

    public byte[] getWocoDtUltPrePagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_DT_ULT_PRE_PAG, Pos.WOCO_DT_ULT_PRE_PAG);
        return buffer;
    }

    public void initWocoDtUltPrePagSpaces() {
        fill(Pos.WOCO_DT_ULT_PRE_PAG, Len.WOCO_DT_ULT_PRE_PAG, Types.SPACE_CHAR);
    }

    public void setWocoDtUltPrePagNull(String wocoDtUltPrePagNull) {
        writeString(Pos.WOCO_DT_ULT_PRE_PAG_NULL, wocoDtUltPrePagNull, Len.WOCO_DT_ULT_PRE_PAG_NULL);
    }

    /**Original name: WOCO-DT-ULT-PRE-PAG-NULL<br>*/
    public String getWocoDtUltPrePagNull() {
        return readString(Pos.WOCO_DT_ULT_PRE_PAG_NULL, Len.WOCO_DT_ULT_PRE_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_DT_ULT_PRE_PAG = 1;
        public static final int WOCO_DT_ULT_PRE_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_DT_ULT_PRE_PAG = 5;
        public static final int WOCO_DT_ULT_PRE_PAG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_DT_ULT_PRE_PAG = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
