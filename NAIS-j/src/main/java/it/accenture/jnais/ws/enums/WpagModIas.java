package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WPAG-MOD-IAS<br>
 * Variable: WPAG-MOD-IAS from copybook LVEC0069<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpagModIas {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_MOD_IAS = 'S';
    public static final char NO_MOD_IAS = 'N';

    //==== METHODS ====
    public void setModIas(char modIas) {
        this.value = modIas;
    }

    public char getModIas() {
        return this.value;
    }

    public void setSiModIas() {
        value = SI_MOD_IAS;
    }

    public void setNoModIas() {
        value = NO_MOD_IAS;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MOD_IAS = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
