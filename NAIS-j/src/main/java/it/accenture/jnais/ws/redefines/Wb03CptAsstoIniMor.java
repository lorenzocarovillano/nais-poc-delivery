package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CPT-ASSTO-INI-MOR<br>
 * Variable: WB03-CPT-ASSTO-INI-MOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CptAsstoIniMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CptAsstoIniMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CPT_ASSTO_INI_MOR;
    }

    public void setWb03CptAsstoIniMor(AfDecimal wb03CptAsstoIniMor) {
        writeDecimalAsPacked(Pos.WB03_CPT_ASSTO_INI_MOR, wb03CptAsstoIniMor.copy());
    }

    public void setWb03CptAsstoIniMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CPT_ASSTO_INI_MOR, Pos.WB03_CPT_ASSTO_INI_MOR);
    }

    /**Original name: WB03-CPT-ASSTO-INI-MOR<br>*/
    public AfDecimal getWb03CptAsstoIniMor() {
        return readPackedAsDecimal(Pos.WB03_CPT_ASSTO_INI_MOR, Len.Int.WB03_CPT_ASSTO_INI_MOR, Len.Fract.WB03_CPT_ASSTO_INI_MOR);
    }

    public byte[] getWb03CptAsstoIniMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CPT_ASSTO_INI_MOR, Pos.WB03_CPT_ASSTO_INI_MOR);
        return buffer;
    }

    public void setWb03CptAsstoIniMorNull(String wb03CptAsstoIniMorNull) {
        writeString(Pos.WB03_CPT_ASSTO_INI_MOR_NULL, wb03CptAsstoIniMorNull, Len.WB03_CPT_ASSTO_INI_MOR_NULL);
    }

    /**Original name: WB03-CPT-ASSTO-INI-MOR-NULL<br>*/
    public String getWb03CptAsstoIniMorNull() {
        return readString(Pos.WB03_CPT_ASSTO_INI_MOR_NULL, Len.WB03_CPT_ASSTO_INI_MOR_NULL);
    }

    public String getWb03CptAsstoIniMorNullFormatted() {
        return Functions.padBlanks(getWb03CptAsstoIniMorNull(), Len.WB03_CPT_ASSTO_INI_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CPT_ASSTO_INI_MOR = 1;
        public static final int WB03_CPT_ASSTO_INI_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CPT_ASSTO_INI_MOR = 8;
        public static final int WB03_CPT_ASSTO_INI_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CPT_ASSTO_INI_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CPT_ASSTO_INI_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
