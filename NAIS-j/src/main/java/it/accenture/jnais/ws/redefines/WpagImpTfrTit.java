package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-TFR-TIT<br>
 * Variable: WPAG-IMP-TFR-TIT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpTfrTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpTfrTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_TFR_TIT;
    }

    public void setWpagImpTfrTit(AfDecimal wpagImpTfrTit) {
        writeDecimalAsPacked(Pos.WPAG_IMP_TFR_TIT, wpagImpTfrTit.copy());
    }

    public void setWpagImpTfrTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_TFR_TIT, Pos.WPAG_IMP_TFR_TIT);
    }

    /**Original name: WPAG-IMP-TFR-TIT<br>*/
    public AfDecimal getWpagImpTfrTit() {
        return readPackedAsDecimal(Pos.WPAG_IMP_TFR_TIT, Len.Int.WPAG_IMP_TFR_TIT, Len.Fract.WPAG_IMP_TFR_TIT);
    }

    public byte[] getWpagImpTfrTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_TFR_TIT, Pos.WPAG_IMP_TFR_TIT);
        return buffer;
    }

    public void initWpagImpTfrTitSpaces() {
        fill(Pos.WPAG_IMP_TFR_TIT, Len.WPAG_IMP_TFR_TIT, Types.SPACE_CHAR);
    }

    public void setWpagImpTfrTitNull(String wpagImpTfrTitNull) {
        writeString(Pos.WPAG_IMP_TFR_TIT_NULL, wpagImpTfrTitNull, Len.WPAG_IMP_TFR_TIT_NULL);
    }

    /**Original name: WPAG-IMP-TFR-TIT-NULL<br>*/
    public String getWpagImpTfrTitNull() {
        return readString(Pos.WPAG_IMP_TFR_TIT_NULL, Len.WPAG_IMP_TFR_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_TFR_TIT = 1;
        public static final int WPAG_IMP_TFR_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_TFR_TIT = 8;
        public static final int WPAG_IMP_TFR_TIT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_TFR_TIT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_TFR_TIT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
