package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-FRAZ-PRE-PP<br>
 * Variable: WRST-FRAZ-PRE-PP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstFrazPrePp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstFrazPrePp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_FRAZ_PRE_PP;
    }

    public void setWrstFrazPrePp(AfDecimal wrstFrazPrePp) {
        writeDecimalAsPacked(Pos.WRST_FRAZ_PRE_PP, wrstFrazPrePp.copy());
    }

    public void setWrstFrazPrePpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_FRAZ_PRE_PP, Pos.WRST_FRAZ_PRE_PP);
    }

    /**Original name: WRST-FRAZ-PRE-PP<br>*/
    public AfDecimal getWrstFrazPrePp() {
        return readPackedAsDecimal(Pos.WRST_FRAZ_PRE_PP, Len.Int.WRST_FRAZ_PRE_PP, Len.Fract.WRST_FRAZ_PRE_PP);
    }

    public byte[] getWrstFrazPrePpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_FRAZ_PRE_PP, Pos.WRST_FRAZ_PRE_PP);
        return buffer;
    }

    public void initWrstFrazPrePpSpaces() {
        fill(Pos.WRST_FRAZ_PRE_PP, Len.WRST_FRAZ_PRE_PP, Types.SPACE_CHAR);
    }

    public void setWrstFrazPrePpNull(String wrstFrazPrePpNull) {
        writeString(Pos.WRST_FRAZ_PRE_PP_NULL, wrstFrazPrePpNull, Len.WRST_FRAZ_PRE_PP_NULL);
    }

    /**Original name: WRST-FRAZ-PRE-PP-NULL<br>*/
    public String getWrstFrazPrePpNull() {
        return readString(Pos.WRST_FRAZ_PRE_PP_NULL, Len.WRST_FRAZ_PRE_PP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_FRAZ_PRE_PP = 1;
        public static final int WRST_FRAZ_PRE_PP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_FRAZ_PRE_PP = 8;
        public static final int WRST_FRAZ_PRE_PP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_FRAZ_PRE_PP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_FRAZ_PRE_PP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
