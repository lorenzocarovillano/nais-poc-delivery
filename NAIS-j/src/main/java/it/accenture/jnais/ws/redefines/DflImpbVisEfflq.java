package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-VIS-EFFLQ<br>
 * Variable: DFL-IMPB-VIS-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbVisEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbVisEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_VIS_EFFLQ;
    }

    public void setDflImpbVisEfflq(AfDecimal dflImpbVisEfflq) {
        writeDecimalAsPacked(Pos.DFL_IMPB_VIS_EFFLQ, dflImpbVisEfflq.copy());
    }

    public void setDflImpbVisEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_VIS_EFFLQ, Pos.DFL_IMPB_VIS_EFFLQ);
    }

    /**Original name: DFL-IMPB-VIS-EFFLQ<br>*/
    public AfDecimal getDflImpbVisEfflq() {
        return readPackedAsDecimal(Pos.DFL_IMPB_VIS_EFFLQ, Len.Int.DFL_IMPB_VIS_EFFLQ, Len.Fract.DFL_IMPB_VIS_EFFLQ);
    }

    public byte[] getDflImpbVisEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_VIS_EFFLQ, Pos.DFL_IMPB_VIS_EFFLQ);
        return buffer;
    }

    public void setDflImpbVisEfflqNull(String dflImpbVisEfflqNull) {
        writeString(Pos.DFL_IMPB_VIS_EFFLQ_NULL, dflImpbVisEfflqNull, Len.DFL_IMPB_VIS_EFFLQ_NULL);
    }

    /**Original name: DFL-IMPB-VIS-EFFLQ-NULL<br>*/
    public String getDflImpbVisEfflqNull() {
        return readString(Pos.DFL_IMPB_VIS_EFFLQ_NULL, Len.DFL_IMPB_VIS_EFFLQ_NULL);
    }

    public String getDflImpbVisEfflqNullFormatted() {
        return Functions.padBlanks(getDflImpbVisEfflqNull(), Len.DFL_IMPB_VIS_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS_EFFLQ = 1;
        public static final int DFL_IMPB_VIS_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS_EFFLQ = 8;
        public static final int DFL_IMPB_VIS_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
