package it.accenture.jnais.ws;

/**Original name: WKS-INDICI<br>
 * Variable: WKS-INDICI from program IVVS0216<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WksIndici {

    //==== PROPERTIES ====
    //Original name: CONT-FETCH
    private int contFetch = 0;
    //Original name: IX-TAB-UNIV
    private int ixTabUniv = 0;
    //Original name: IX-TAB-ADE
    private int ixTabAde = 0;
    //Original name: IX-TAB-ALL
    private int ixTabAll = 0;
    //Original name: IX-TAB-BEP
    private int ixTabBep = 0;
    //Original name: IX-TAB-CLT
    private int ixTabClt = 0;
    //Original name: IX-TAB-COG
    private int ixTabCog = 0;
    //Original name: IX-TAB-DCO
    private int ixTabDco = 0;
    //Original name: IX-TAB-DFA
    private int ixTabDfa = 0;
    //Original name: IX-TAB-DEQ
    private int ixTabDeq = 0;
    //Original name: IX-TAB-DER
    private int ixTabDer = 0;
    //Original name: IX-TAB-DTC
    private int ixTabDtc = 0;
    //Original name: IX-TAB-GRZ
    private int ixTabGrz = 0;
    //Original name: IX-TAB-GOP
    private int ixTabGop = 0;
    //Original name: IX-TAB-ISO
    private int ixTabIso = 0;
    //Original name: IX-TAB-MOV
    private int ixTabMov = 0;
    //Original name: IX-TAB-NOT
    private int ixTabNot = 0;
    //Original name: IX-TAB-OCS
    private int ixTabOcs = 0;
    //Original name: IX-TAB-PGE
    private int ixTabPge = 0;
    //Original name: IX-TAB-PCO
    private int ixTabPco = 0;
    //Original name: IX-TAB-PCA
    private int ixTabPca = 0;
    //Original name: IX-TAB-PMO
    private int ixTabPmo = 0;
    //Original name: IX-TAB-POG
    private int ixTabPog = 0;
    //Original name: IX-TAB-PLI
    private int ixTabPli = 0;
    //Original name: IX-TAB-POL
    private int ixTabPol = 0;
    //Original name: IX-TAB-PRE
    private int ixTabPre = 0;
    //Original name: IX-TAB-PVT
    private int ixTabPvt = 0;
    //Original name: IX-TAB-QUE
    private int ixTabQue = 0;
    //Original name: IX-TAB-RAN
    private int ixTabRan = 0;
    //Original name: IX-TAB-E15
    private int ixTabE15 = 0;
    //Original name: IX-TAB-RRE
    private int ixTabRre = 0;
    //Original name: IX-TAB-RIC
    private int ixTabRic = 0;
    //Original name: IX-TAB-RFI
    private int ixTabRfi = 0;
    //Original name: IX-TAB-RST
    private int ixTabRst = 0;
    //Original name: IX-TAB-RPG
    private int ixTabRpg = 0;
    //Original name: IX-TAB-STB
    private int ixTabStb = 0;
    //Original name: IX-TAB-STW
    private int ixTabStw = 0;
    //Original name: IX-TAB-SDI
    private int ixTabSdi = 0;
    //Original name: IX-TAB-TIT
    private int ixTabTit = 0;
    //Original name: IX-TAB-TGA
    private int ixTabTga = 0;
    //Original name: IX-TAB-TOP
    private int ixTabTop = 0;
    //Original name: IX-TAB-TLI
    private int ixTabTli = 0;
    //Original name: IX-TAB-SPG
    private int ixTabSpg = 0;
    //Original name: IX-TAB-BEL
    private int ixTabBel = 0;
    //Original name: IX-TAB-LQU
    private int ixTabLqu = 0;
    //Original name: IX-TAB-DAD
    private int ixTabDad = 0;
    //Original name: IX-TAB-MFZ
    private int ixTabMfz = 0;
    //Original name: IX-TAB-RIF
    private int ixTabRif = 0;
    //Original name: IX-TAB-TDR
    private int ixTabTdr = 0;
    //Original name: IX-TAB-DTR
    private int ixTabDtr = 0;
    //Original name: IX-TAB-OCO
    private int ixTabOco = 0;
    //Original name: IX-TAB-RCA
    private int ixTabRca = 0;
    //Original name: IX-TAB-DLQ
    private int ixTabDlq = 0;
    //Original name: IX-TAB-DFL
    private int ixTabDfl = 0;
    //Original name: IX-TAB-GRL
    private int ixTabGrl = 0;
    //Original name: IX-TAB-L19
    private int ixTabL19 = 0;
    //Original name: IX-TAB-E12
    private int ixTabE12 = 0;
    //Original name: IX-TAB-L30
    private int ixTabL30 = 0;
    //Original name: IX-TAB-L23
    private int ixTabL23 = 0;
    //Original name: IX-TAB-P61
    private int ixTabP61 = 0;
    //Original name: IX-TAB-P67
    private int ixTabP67 = 0;
    //Original name: IX-DATO
    private int ixDato = 0;
    //Original name: IX-TABB
    private int ixTabb = 0;
    //Original name: IX-APP-DEQ
    private int ixAppDeq = 0;
    //Original name: IX-APP-QUE
    private int ixAppQue = 0;
    //Original name: IX-AP-TABB
    private int ixApTabb = 0;
    //Original name: IX-AP-DATO
    private int ixApDato = 0;
    //Original name: IX-IDSS0020
    private int ixIdss0020 = 0;
    //Original name: IX-CONTA
    private int ixConta = 0;
    //Original name: IX-VAL-VAR
    private int ixValVar = 0;
    //Original name: IX-COD-VAR
    private int ixCodVar = 0;
    //Original name: IX-COD-VAR-OUT
    private int ixCodVarOut = 0;
    //Original name: IX-COD-VAR-OUT-SAL
    private int ixCodVarOutSal = 0;
    //Original name: IX-COD-MVV
    private int ixCodMvv = 0;
    //Original name: IX-INTERN
    private int ixIntern = 0;
    //Original name: IX-GUIDA-GRZ
    private int ixGuidaGrz = 0;
    //Original name: IX-DOM
    private int ixDom = 0;
    //Original name: IX-CTRL-FITT
    private int ixCtrlFitt = 0;
    //Original name: IX-VAR-LIS
    private int ixVarLis = 0;
    //Original name: IX-TAB-P01
    private int ixTabP01 = 0;

    //==== METHODS ====
    public void setContFetch(int contFetch) {
        this.contFetch = contFetch;
    }

    public int getContFetch() {
        return this.contFetch;
    }

    public void setIxTabUniv(int ixTabUniv) {
        this.ixTabUniv = ixTabUniv;
    }

    public int getIxTabUniv() {
        return this.ixTabUniv;
    }

    public void setIxTabAde(int ixTabAde) {
        this.ixTabAde = ixTabAde;
    }

    public int getIxTabAde() {
        return this.ixTabAde;
    }

    public void setIxTabAll(int ixTabAll) {
        this.ixTabAll = ixTabAll;
    }

    public int getIxTabAll() {
        return this.ixTabAll;
    }

    public void setIxTabBep(int ixTabBep) {
        this.ixTabBep = ixTabBep;
    }

    public int getIxTabBep() {
        return this.ixTabBep;
    }

    public void setIxTabClt(int ixTabClt) {
        this.ixTabClt = ixTabClt;
    }

    public int getIxTabClt() {
        return this.ixTabClt;
    }

    public void setIxTabCog(int ixTabCog) {
        this.ixTabCog = ixTabCog;
    }

    public int getIxTabCog() {
        return this.ixTabCog;
    }

    public void setIxTabDco(int ixTabDco) {
        this.ixTabDco = ixTabDco;
    }

    public int getIxTabDco() {
        return this.ixTabDco;
    }

    public void setIxTabDfa(int ixTabDfa) {
        this.ixTabDfa = ixTabDfa;
    }

    public int getIxTabDfa() {
        return this.ixTabDfa;
    }

    public void setIxTabDeq(int ixTabDeq) {
        this.ixTabDeq = ixTabDeq;
    }

    public int getIxTabDeq() {
        return this.ixTabDeq;
    }

    public void setIxTabDer(int ixTabDer) {
        this.ixTabDer = ixTabDer;
    }

    public int getIxTabDer() {
        return this.ixTabDer;
    }

    public void setIxTabDtc(int ixTabDtc) {
        this.ixTabDtc = ixTabDtc;
    }

    public int getIxTabDtc() {
        return this.ixTabDtc;
    }

    public void setIxTabGrz(int ixTabGrz) {
        this.ixTabGrz = ixTabGrz;
    }

    public int getIxTabGrz() {
        return this.ixTabGrz;
    }

    public void setIxTabGop(int ixTabGop) {
        this.ixTabGop = ixTabGop;
    }

    public int getIxTabGop() {
        return this.ixTabGop;
    }

    public void setIxTabIso(int ixTabIso) {
        this.ixTabIso = ixTabIso;
    }

    public int getIxTabIso() {
        return this.ixTabIso;
    }

    public void setIxTabMov(int ixTabMov) {
        this.ixTabMov = ixTabMov;
    }

    public int getIxTabMov() {
        return this.ixTabMov;
    }

    public void setIxTabNot(int ixTabNot) {
        this.ixTabNot = ixTabNot;
    }

    public int getIxTabNot() {
        return this.ixTabNot;
    }

    public void setIxTabOcs(int ixTabOcs) {
        this.ixTabOcs = ixTabOcs;
    }

    public int getIxTabOcs() {
        return this.ixTabOcs;
    }

    public void setIxTabPge(int ixTabPge) {
        this.ixTabPge = ixTabPge;
    }

    public int getIxTabPge() {
        return this.ixTabPge;
    }

    public void setIxTabPco(int ixTabPco) {
        this.ixTabPco = ixTabPco;
    }

    public int getIxTabPco() {
        return this.ixTabPco;
    }

    public void setIxTabPca(int ixTabPca) {
        this.ixTabPca = ixTabPca;
    }

    public int getIxTabPca() {
        return this.ixTabPca;
    }

    public void setIxTabPmo(int ixTabPmo) {
        this.ixTabPmo = ixTabPmo;
    }

    public int getIxTabPmo() {
        return this.ixTabPmo;
    }

    public void setIxTabPog(int ixTabPog) {
        this.ixTabPog = ixTabPog;
    }

    public int getIxTabPog() {
        return this.ixTabPog;
    }

    public void setIxTabPli(int ixTabPli) {
        this.ixTabPli = ixTabPli;
    }

    public int getIxTabPli() {
        return this.ixTabPli;
    }

    public void setIxTabPol(int ixTabPol) {
        this.ixTabPol = ixTabPol;
    }

    public int getIxTabPol() {
        return this.ixTabPol;
    }

    public void setIxTabPre(int ixTabPre) {
        this.ixTabPre = ixTabPre;
    }

    public int getIxTabPre() {
        return this.ixTabPre;
    }

    public void setIxTabPvt(int ixTabPvt) {
        this.ixTabPvt = ixTabPvt;
    }

    public int getIxTabPvt() {
        return this.ixTabPvt;
    }

    public void setIxTabQue(int ixTabQue) {
        this.ixTabQue = ixTabQue;
    }

    public int getIxTabQue() {
        return this.ixTabQue;
    }

    public void setIxTabRan(int ixTabRan) {
        this.ixTabRan = ixTabRan;
    }

    public int getIxTabRan() {
        return this.ixTabRan;
    }

    public void setIxTabE15(int ixTabE15) {
        this.ixTabE15 = ixTabE15;
    }

    public int getIxTabE15() {
        return this.ixTabE15;
    }

    public void setIxTabRre(int ixTabRre) {
        this.ixTabRre = ixTabRre;
    }

    public int getIxTabRre() {
        return this.ixTabRre;
    }

    public void setIxTabRic(int ixTabRic) {
        this.ixTabRic = ixTabRic;
    }

    public int getIxTabRic() {
        return this.ixTabRic;
    }

    public void setIxTabRfi(int ixTabRfi) {
        this.ixTabRfi = ixTabRfi;
    }

    public int getIxTabRfi() {
        return this.ixTabRfi;
    }

    public void setIxTabRst(int ixTabRst) {
        this.ixTabRst = ixTabRst;
    }

    public int getIxTabRst() {
        return this.ixTabRst;
    }

    public void setIxTabRpg(int ixTabRpg) {
        this.ixTabRpg = ixTabRpg;
    }

    public int getIxTabRpg() {
        return this.ixTabRpg;
    }

    public void setIxTabStb(int ixTabStb) {
        this.ixTabStb = ixTabStb;
    }

    public int getIxTabStb() {
        return this.ixTabStb;
    }

    public void setIxTabStw(int ixTabStw) {
        this.ixTabStw = ixTabStw;
    }

    public int getIxTabStw() {
        return this.ixTabStw;
    }

    public void setIxTabSdi(int ixTabSdi) {
        this.ixTabSdi = ixTabSdi;
    }

    public int getIxTabSdi() {
        return this.ixTabSdi;
    }

    public void setIxTabTit(int ixTabTit) {
        this.ixTabTit = ixTabTit;
    }

    public int getIxTabTit() {
        return this.ixTabTit;
    }

    public void setIxTabTga(int ixTabTga) {
        this.ixTabTga = ixTabTga;
    }

    public int getIxTabTga() {
        return this.ixTabTga;
    }

    public void setIxTabTop(int ixTabTop) {
        this.ixTabTop = ixTabTop;
    }

    public int getIxTabTop() {
        return this.ixTabTop;
    }

    public void setIxTabTli(int ixTabTli) {
        this.ixTabTli = ixTabTli;
    }

    public int getIxTabTli() {
        return this.ixTabTli;
    }

    public void setIxTabSpg(int ixTabSpg) {
        this.ixTabSpg = ixTabSpg;
    }

    public int getIxTabSpg() {
        return this.ixTabSpg;
    }

    public void setIxTabBel(int ixTabBel) {
        this.ixTabBel = ixTabBel;
    }

    public int getIxTabBel() {
        return this.ixTabBel;
    }

    public void setIxTabLqu(int ixTabLqu) {
        this.ixTabLqu = ixTabLqu;
    }

    public int getIxTabLqu() {
        return this.ixTabLqu;
    }

    public void setIxTabDad(int ixTabDad) {
        this.ixTabDad = ixTabDad;
    }

    public int getIxTabDad() {
        return this.ixTabDad;
    }

    public void setIxTabMfz(int ixTabMfz) {
        this.ixTabMfz = ixTabMfz;
    }

    public int getIxTabMfz() {
        return this.ixTabMfz;
    }

    public void setIxTabRif(int ixTabRif) {
        this.ixTabRif = ixTabRif;
    }

    public int getIxTabRif() {
        return this.ixTabRif;
    }

    public void setIxTabTdr(int ixTabTdr) {
        this.ixTabTdr = ixTabTdr;
    }

    public int getIxTabTdr() {
        return this.ixTabTdr;
    }

    public void setIxTabDtr(int ixTabDtr) {
        this.ixTabDtr = ixTabDtr;
    }

    public int getIxTabDtr() {
        return this.ixTabDtr;
    }

    public void setIxTabOco(int ixTabOco) {
        this.ixTabOco = ixTabOco;
    }

    public int getIxTabOco() {
        return this.ixTabOco;
    }

    public void setIxTabRca(int ixTabRca) {
        this.ixTabRca = ixTabRca;
    }

    public int getIxTabRca() {
        return this.ixTabRca;
    }

    public void setIxTabDlq(int ixTabDlq) {
        this.ixTabDlq = ixTabDlq;
    }

    public int getIxTabDlq() {
        return this.ixTabDlq;
    }

    public void setIxTabDfl(int ixTabDfl) {
        this.ixTabDfl = ixTabDfl;
    }

    public int getIxTabDfl() {
        return this.ixTabDfl;
    }

    public void setIxTabGrl(int ixTabGrl) {
        this.ixTabGrl = ixTabGrl;
    }

    public int getIxTabGrl() {
        return this.ixTabGrl;
    }

    public void setIxTabL19(int ixTabL19) {
        this.ixTabL19 = ixTabL19;
    }

    public int getIxTabL19() {
        return this.ixTabL19;
    }

    public void setIxTabE12(int ixTabE12) {
        this.ixTabE12 = ixTabE12;
    }

    public int getIxTabE12() {
        return this.ixTabE12;
    }

    public void setIxTabL30(int ixTabL30) {
        this.ixTabL30 = ixTabL30;
    }

    public int getIxTabL30() {
        return this.ixTabL30;
    }

    public void setIxTabL23(int ixTabL23) {
        this.ixTabL23 = ixTabL23;
    }

    public int getIxTabL23() {
        return this.ixTabL23;
    }

    public void setIxTabP61(int ixTabP61) {
        this.ixTabP61 = ixTabP61;
    }

    public int getIxTabP61() {
        return this.ixTabP61;
    }

    public void setIxTabP67(int ixTabP67) {
        this.ixTabP67 = ixTabP67;
    }

    public int getIxTabP67() {
        return this.ixTabP67;
    }

    public void setIxDato(int ixDato) {
        this.ixDato = ixDato;
    }

    public int getIxDato() {
        return this.ixDato;
    }

    public void setIxTabb(int ixTabb) {
        this.ixTabb = ixTabb;
    }

    public int getIxTabb() {
        return this.ixTabb;
    }

    public void setIxAppDeq(int ixAppDeq) {
        this.ixAppDeq = ixAppDeq;
    }

    public int getIxAppDeq() {
        return this.ixAppDeq;
    }

    public void setIxAppQue(int ixAppQue) {
        this.ixAppQue = ixAppQue;
    }

    public int getIxAppQue() {
        return this.ixAppQue;
    }

    public void setIxApTabb(int ixApTabb) {
        this.ixApTabb = ixApTabb;
    }

    public int getIxApTabb() {
        return this.ixApTabb;
    }

    public void setIxApDato(int ixApDato) {
        this.ixApDato = ixApDato;
    }

    public int getIxApDato() {
        return this.ixApDato;
    }

    public void setIxIdss0020(int ixIdss0020) {
        this.ixIdss0020 = ixIdss0020;
    }

    public int getIxIdss0020() {
        return this.ixIdss0020;
    }

    public void setIxConta(int ixConta) {
        this.ixConta = ixConta;
    }

    public int getIxConta() {
        return this.ixConta;
    }

    public void setIxValVar(int ixValVar) {
        this.ixValVar = ixValVar;
    }

    public int getIxValVar() {
        return this.ixValVar;
    }

    public void setIxCodVar(int ixCodVar) {
        this.ixCodVar = ixCodVar;
    }

    public int getIxCodVar() {
        return this.ixCodVar;
    }

    public void setIxCodVarOut(int ixCodVarOut) {
        this.ixCodVarOut = ixCodVarOut;
    }

    public int getIxCodVarOut() {
        return this.ixCodVarOut;
    }

    public void setIxCodVarOutSal(int ixCodVarOutSal) {
        this.ixCodVarOutSal = ixCodVarOutSal;
    }

    public int getIxCodVarOutSal() {
        return this.ixCodVarOutSal;
    }

    public void setIxCodMvv(int ixCodMvv) {
        this.ixCodMvv = ixCodMvv;
    }

    public int getIxCodMvv() {
        return this.ixCodMvv;
    }

    public void setIxIntern(int ixIntern) {
        this.ixIntern = ixIntern;
    }

    public int getIxIntern() {
        return this.ixIntern;
    }

    public void setIxGuidaGrz(int ixGuidaGrz) {
        this.ixGuidaGrz = ixGuidaGrz;
    }

    public int getIxGuidaGrz() {
        return this.ixGuidaGrz;
    }

    public void setIxDom(int ixDom) {
        this.ixDom = ixDom;
    }

    public int getIxDom() {
        return this.ixDom;
    }

    public void setIxCtrlFitt(int ixCtrlFitt) {
        this.ixCtrlFitt = ixCtrlFitt;
    }

    public int getIxCtrlFitt() {
        return this.ixCtrlFitt;
    }

    public void setIxVarLis(int ixVarLis) {
        this.ixVarLis = ixVarLis;
    }

    public int getIxVarLis() {
        return this.ixVarLis;
    }

    public void setIxTabP01(int ixTabP01) {
        this.ixTabP01 = ixTabP01;
    }

    public int getIxTabP01() {
        return this.ixTabP01;
    }
}
