package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WK-DIFF-PROR<br>
 * Variable: WK-DIFF-PROR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkDiffPror extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkDiffPror() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_DIFF_PROR;
    }

    public void setWkDiffPror(AfDecimal wkDiffPror) {
        writeDecimal(Pos.WK_DIFF_PROR, wkDiffPror.copy());
    }

    /**Original name: WK-DIFF-PROR<br>*/
    public AfDecimal getWkDiffPror() {
        return readDecimal(Pos.WK_DIFF_PROR, Len.Int.WK_DIFF_PROR, Len.Fract.WK_DIFF_PROR);
    }

    public String getAaFormatted() {
        return readFixedString(Pos.AA, Len.AA);
    }

    public String getMmFormatted() {
        return readFixedString(Pos.MM, Len.MM);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_DIFF_PROR = 1;
        public static final int WK_DIFF_PROR_V = 1;
        public static final int AA = WK_DIFF_PROR_V;
        public static final int MM = AA + Len.AA;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int AA = 2;
        public static final int WK_DIFF_PROR = 4;
        public static final int MM = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_DIFF_PROR = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WK_DIFF_PROR = 2;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
