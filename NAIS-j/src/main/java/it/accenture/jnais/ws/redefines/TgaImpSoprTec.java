package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-SOPR-TEC<br>
 * Variable: TGA-IMP-SOPR-TEC from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_SOPR_TEC;
    }

    public void setTgaImpSoprTec(AfDecimal tgaImpSoprTec) {
        writeDecimalAsPacked(Pos.TGA_IMP_SOPR_TEC, tgaImpSoprTec.copy());
    }

    public void setTgaImpSoprTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_SOPR_TEC, Pos.TGA_IMP_SOPR_TEC);
    }

    /**Original name: TGA-IMP-SOPR-TEC<br>*/
    public AfDecimal getTgaImpSoprTec() {
        return readPackedAsDecimal(Pos.TGA_IMP_SOPR_TEC, Len.Int.TGA_IMP_SOPR_TEC, Len.Fract.TGA_IMP_SOPR_TEC);
    }

    public byte[] getTgaImpSoprTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_SOPR_TEC, Pos.TGA_IMP_SOPR_TEC);
        return buffer;
    }

    public void setTgaImpSoprTecNull(String tgaImpSoprTecNull) {
        writeString(Pos.TGA_IMP_SOPR_TEC_NULL, tgaImpSoprTecNull, Len.TGA_IMP_SOPR_TEC_NULL);
    }

    /**Original name: TGA-IMP-SOPR-TEC-NULL<br>*/
    public String getTgaImpSoprTecNull() {
        return readString(Pos.TGA_IMP_SOPR_TEC_NULL, Len.TGA_IMP_SOPR_TEC_NULL);
    }

    public String getTgaImpSoprTecNullFormatted() {
        return Functions.padBlanks(getTgaImpSoprTecNull(), Len.TGA_IMP_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_SOPR_TEC = 1;
        public static final int TGA_IMP_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_SOPR_TEC = 8;
        public static final int TGA_IMP_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
