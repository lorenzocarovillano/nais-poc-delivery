package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-REN-INI-TS-TEC-0<br>
 * Variable: WTGA-REN-INI-TS-TEC-0 from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaRenIniTsTec0 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaRenIniTsTec0() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_REN_INI_TS_TEC0;
    }

    public void setWtgaRenIniTsTec0(AfDecimal wtgaRenIniTsTec0) {
        writeDecimalAsPacked(Pos.WTGA_REN_INI_TS_TEC0, wtgaRenIniTsTec0.copy());
    }

    public void setWtgaRenIniTsTec0FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_REN_INI_TS_TEC0, Pos.WTGA_REN_INI_TS_TEC0);
    }

    /**Original name: WTGA-REN-INI-TS-TEC-0<br>*/
    public AfDecimal getWtgaRenIniTsTec0() {
        return readPackedAsDecimal(Pos.WTGA_REN_INI_TS_TEC0, Len.Int.WTGA_REN_INI_TS_TEC0, Len.Fract.WTGA_REN_INI_TS_TEC0);
    }

    public byte[] getWtgaRenIniTsTec0AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_REN_INI_TS_TEC0, Pos.WTGA_REN_INI_TS_TEC0);
        return buffer;
    }

    public void initWtgaRenIniTsTec0Spaces() {
        fill(Pos.WTGA_REN_INI_TS_TEC0, Len.WTGA_REN_INI_TS_TEC0, Types.SPACE_CHAR);
    }

    public void setWtgaRenIniTsTec0Null(String wtgaRenIniTsTec0Null) {
        writeString(Pos.WTGA_REN_INI_TS_TEC0_NULL, wtgaRenIniTsTec0Null, Len.WTGA_REN_INI_TS_TEC0_NULL);
    }

    /**Original name: WTGA-REN-INI-TS-TEC-0-NULL<br>*/
    public String getWtgaRenIniTsTec0Null() {
        return readString(Pos.WTGA_REN_INI_TS_TEC0_NULL, Len.WTGA_REN_INI_TS_TEC0_NULL);
    }

    public String getWtgaRenIniTsTec0NullFormatted() {
        return Functions.padBlanks(getWtgaRenIniTsTec0Null(), Len.WTGA_REN_INI_TS_TEC0_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_REN_INI_TS_TEC0 = 1;
        public static final int WTGA_REN_INI_TS_TEC0_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_REN_INI_TS_TEC0 = 8;
        public static final int WTGA_REN_INI_TS_TEC0_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_REN_INI_TS_TEC0 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_REN_INI_TS_TEC0 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
