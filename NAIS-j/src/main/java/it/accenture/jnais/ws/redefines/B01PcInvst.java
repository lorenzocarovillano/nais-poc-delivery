package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B01-PC-INVST<br>
 * Variable: B01-PC-INVST from program IDBSB010<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B01PcInvst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B01PcInvst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B01_PC_INVST;
    }

    public void setB01PcInvst(AfDecimal b01PcInvst) {
        writeDecimalAsPacked(Pos.B01_PC_INVST, b01PcInvst.copy());
    }

    public void setB01PcInvstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B01_PC_INVST, Pos.B01_PC_INVST);
    }

    /**Original name: B01-PC-INVST<br>*/
    public AfDecimal getB01PcInvst() {
        return readPackedAsDecimal(Pos.B01_PC_INVST, Len.Int.B01_PC_INVST, Len.Fract.B01_PC_INVST);
    }

    public byte[] getB01PcInvstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B01_PC_INVST, Pos.B01_PC_INVST);
        return buffer;
    }

    public void setB01PcInvstNull(String b01PcInvstNull) {
        writeString(Pos.B01_PC_INVST_NULL, b01PcInvstNull, Len.B01_PC_INVST_NULL);
    }

    /**Original name: B01-PC-INVST-NULL<br>*/
    public String getB01PcInvstNull() {
        return readString(Pos.B01_PC_INVST_NULL, Len.B01_PC_INVST_NULL);
    }

    public String getB01PcInvstNullFormatted() {
        return Functions.padBlanks(getB01PcInvstNull(), Len.B01_PC_INVST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B01_PC_INVST = 1;
        public static final int B01_PC_INVST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B01_PC_INVST = 4;
        public static final int B01_PC_INVST_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B01_PC_INVST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int B01_PC_INVST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
