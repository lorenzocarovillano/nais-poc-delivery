package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRE-TARI-INI<br>
 * Variable: WPAG-PRE-TARI-INI from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPreTariIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPreTariIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRE_TARI_INI;
    }

    public void setWpagPreTariIni(AfDecimal wpagPreTariIni) {
        writeDecimalAsPacked(Pos.WPAG_PRE_TARI_INI, wpagPreTariIni.copy());
    }

    public void setWpagPreTariIniFormatted(String wpagPreTariIni) {
        setWpagPreTariIni(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRE_TARI_INI + Len.Fract.WPAG_PRE_TARI_INI, Len.Fract.WPAG_PRE_TARI_INI, wpagPreTariIni));
    }

    public void setWpagPreTariIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRE_TARI_INI, Pos.WPAG_PRE_TARI_INI);
    }

    /**Original name: WPAG-PRE-TARI-INI<br>*/
    public AfDecimal getWpagPreTariIni() {
        return readPackedAsDecimal(Pos.WPAG_PRE_TARI_INI, Len.Int.WPAG_PRE_TARI_INI, Len.Fract.WPAG_PRE_TARI_INI);
    }

    public byte[] getWpagPreTariIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRE_TARI_INI, Pos.WPAG_PRE_TARI_INI);
        return buffer;
    }

    public void initWpagPreTariIniSpaces() {
        fill(Pos.WPAG_PRE_TARI_INI, Len.WPAG_PRE_TARI_INI, Types.SPACE_CHAR);
    }

    public void setWpagPreTariIniNull(String wpagPreTariIniNull) {
        writeString(Pos.WPAG_PRE_TARI_INI_NULL, wpagPreTariIniNull, Len.WPAG_PRE_TARI_INI_NULL);
    }

    /**Original name: WPAG-PRE-TARI-INI-NULL<br>*/
    public String getWpagPreTariIniNull() {
        return readString(Pos.WPAG_PRE_TARI_INI_NULL, Len.WPAG_PRE_TARI_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_TARI_INI = 1;
        public static final int WPAG_PRE_TARI_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_TARI_INI = 8;
        public static final int WPAG_PRE_TARI_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_TARI_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_TARI_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
