package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDAD-PC-TFR<br>
 * Variable: WDAD-PC-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadPcTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadPcTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_PC_TFR;
    }

    public void setWdadPcTfr(AfDecimal wdadPcTfr) {
        writeDecimalAsPacked(Pos.WDAD_PC_TFR, wdadPcTfr.copy());
    }

    public void setWdadPcTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_PC_TFR, Pos.WDAD_PC_TFR);
    }

    /**Original name: WDAD-PC-TFR<br>*/
    public AfDecimal getWdadPcTfr() {
        return readPackedAsDecimal(Pos.WDAD_PC_TFR, Len.Int.WDAD_PC_TFR, Len.Fract.WDAD_PC_TFR);
    }

    public byte[] getWdadPcTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_PC_TFR, Pos.WDAD_PC_TFR);
        return buffer;
    }

    public void setWdadPcTfrNull(String wdadPcTfrNull) {
        writeString(Pos.WDAD_PC_TFR_NULL, wdadPcTfrNull, Len.WDAD_PC_TFR_NULL);
    }

    /**Original name: WDAD-PC-TFR-NULL<br>*/
    public String getWdadPcTfrNull() {
        return readString(Pos.WDAD_PC_TFR_NULL, Len.WDAD_PC_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_PC_TFR = 1;
        public static final int WDAD_PC_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_PC_TFR = 4;
        public static final int WDAD_PC_TFR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDAD_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
