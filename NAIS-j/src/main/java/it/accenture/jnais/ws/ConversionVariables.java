package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: CONVERSION-VARIABLES<br>
 * Variable: CONVERSION-VARIABLES from program IDSS0020<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConversionVariables {

    //==== PROPERTIES ====
    //Original name: WK-CTRL-LUNG-CAMPO-TIPO
    private String ctrlLungCampoTipo = "";
    //Original name: WK-CAMPO-TIPO-COMP
    private String campoTipoComp = "C0";
    //Original name: WK-CAMPO-TIPO-COMP1
    private String campoTipoComp1 = "C1";
    //Original name: WK-CAMPO-TIPO-COMP2
    private String campoTipoComp2 = "C2";
    //Original name: WK-CAMPO-TIPO-COMP3
    private String campoTipoComp3 = "C3";
    //Original name: WK-CAMPO-TIPO-COMP4
    private String campoTipoComp4 = "C4";
    //Original name: WK-CAMPO-TIPO-COMP5
    private String campoTipoComp5 = "C5";
    //Original name: WK-CTRL-LUNG-CAMPO-LUNG
    private int ctrlLungCampoLung = DefaultValues.INT_VAL;
    //Original name: WK-CTRL-LUNG-CAMPO-LUNG-EFF
    private int ctrlLungCampoLungEff = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setCtrlLungCampoTipo(String ctrlLungCampoTipo) {
        this.ctrlLungCampoTipo = Functions.subString(ctrlLungCampoTipo, Len.CTRL_LUNG_CAMPO_TIPO);
    }

    public String getCtrlLungCampoTipo() {
        return this.ctrlLungCampoTipo;
    }

    public String getCampoTipoComp() {
        return this.campoTipoComp;
    }

    public String getCampoTipoComp1() {
        return this.campoTipoComp1;
    }

    public String getCampoTipoComp2() {
        return this.campoTipoComp2;
    }

    public String getCampoTipoComp3() {
        return this.campoTipoComp3;
    }

    public String getCampoTipoComp4() {
        return this.campoTipoComp4;
    }

    public String getCampoTipoComp5() {
        return this.campoTipoComp5;
    }

    public void setCtrlLungCampoLung(int ctrlLungCampoLung) {
        this.ctrlLungCampoLung = ctrlLungCampoLung;
    }

    public int getCtrlLungCampoLung() {
        return this.ctrlLungCampoLung;
    }

    public void setCtrlLungCampoLungEff(int ctrlLungCampoLungEff) {
        this.ctrlLungCampoLungEff = ctrlLungCampoLungEff;
    }

    public int getCtrlLungCampoLungEff() {
        return this.ctrlLungCampoLungEff;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CTRL_LUNG_CAMPO_TIPO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
