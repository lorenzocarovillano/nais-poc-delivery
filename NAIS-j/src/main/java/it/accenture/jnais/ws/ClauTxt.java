package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.CltIdMoviChiu;

/**Original name: CLAU-TXT<br>
 * Variable: CLAU-TXT from copybook IDBVCLT1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ClauTxt extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: CLT-ID-CLAU-TXT
    private int cltIdClauTxt = DefaultValues.INT_VAL;
    //Original name: CLT-ID-OGG
    private int cltIdOgg = DefaultValues.INT_VAL;
    //Original name: CLT-TP-OGG
    private String cltTpOgg = DefaultValues.stringVal(Len.CLT_TP_OGG);
    //Original name: CLT-ID-MOVI-CRZ
    private int cltIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: CLT-ID-MOVI-CHIU
    private CltIdMoviChiu cltIdMoviChiu = new CltIdMoviChiu();
    //Original name: CLT-DT-INI-EFF
    private int cltDtIniEff = DefaultValues.INT_VAL;
    //Original name: CLT-DT-END-EFF
    private int cltDtEndEff = DefaultValues.INT_VAL;
    //Original name: CLT-COD-COMP-ANIA
    private int cltCodCompAnia = DefaultValues.INT_VAL;
    //Original name: CLT-TP-CLAU
    private String cltTpClau = DefaultValues.stringVal(Len.CLT_TP_CLAU);
    //Original name: CLT-COD-CLAU
    private String cltCodClau = DefaultValues.stringVal(Len.CLT_COD_CLAU);
    //Original name: CLT-DESC-BREVE
    private String cltDescBreve = DefaultValues.stringVal(Len.CLT_DESC_BREVE);
    //Original name: CLT-DESC-LNG-LEN
    private short cltDescLngLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: CLT-DESC-LNG
    private String cltDescLng = DefaultValues.stringVal(Len.CLT_DESC_LNG);
    //Original name: CLT-DS-RIGA
    private long cltDsRiga = DefaultValues.LONG_VAL;
    //Original name: CLT-DS-OPER-SQL
    private char cltDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: CLT-DS-VER
    private int cltDsVer = DefaultValues.INT_VAL;
    //Original name: CLT-DS-TS-INI-CPTZ
    private long cltDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: CLT-DS-TS-END-CPTZ
    private long cltDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: CLT-DS-UTENTE
    private String cltDsUtente = DefaultValues.stringVal(Len.CLT_DS_UTENTE);
    //Original name: CLT-DS-STATO-ELAB
    private char cltDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.CLAU_TXT;
    }

    @Override
    public void deserialize(byte[] buf) {
        setClauTxtBytes(buf);
    }

    public void setClauTxtFormatted(String data) {
        byte[] buffer = new byte[Len.CLAU_TXT];
        MarshalByte.writeString(buffer, 1, data, Len.CLAU_TXT);
        setClauTxtBytes(buffer, 1);
    }

    public String getClauTxtFormatted() {
        return MarshalByteExt.bufferToStr(getClauTxtBytes());
    }

    public void setClauTxtBytes(byte[] buffer) {
        setClauTxtBytes(buffer, 1);
    }

    public byte[] getClauTxtBytes() {
        byte[] buffer = new byte[Len.CLAU_TXT];
        return getClauTxtBytes(buffer, 1);
    }

    public void setClauTxtBytes(byte[] buffer, int offset) {
        int position = offset;
        cltIdClauTxt = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CLT_ID_CLAU_TXT, 0);
        position += Len.CLT_ID_CLAU_TXT;
        cltIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CLT_ID_OGG, 0);
        position += Len.CLT_ID_OGG;
        cltTpOgg = MarshalByte.readString(buffer, position, Len.CLT_TP_OGG);
        position += Len.CLT_TP_OGG;
        cltIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CLT_ID_MOVI_CRZ, 0);
        position += Len.CLT_ID_MOVI_CRZ;
        cltIdMoviChiu.setCltIdMoviChiuFromBuffer(buffer, position);
        position += CltIdMoviChiu.Len.CLT_ID_MOVI_CHIU;
        cltDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CLT_DT_INI_EFF, 0);
        position += Len.CLT_DT_INI_EFF;
        cltDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CLT_DT_END_EFF, 0);
        position += Len.CLT_DT_END_EFF;
        cltCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CLT_COD_COMP_ANIA, 0);
        position += Len.CLT_COD_COMP_ANIA;
        cltTpClau = MarshalByte.readString(buffer, position, Len.CLT_TP_CLAU);
        position += Len.CLT_TP_CLAU;
        cltCodClau = MarshalByte.readString(buffer, position, Len.CLT_COD_CLAU);
        position += Len.CLT_COD_CLAU;
        cltDescBreve = MarshalByte.readString(buffer, position, Len.CLT_DESC_BREVE);
        position += Len.CLT_DESC_BREVE;
        setCltDescLngVcharBytes(buffer, position);
        position += Len.CLT_DESC_LNG_VCHAR;
        cltDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.CLT_DS_RIGA, 0);
        position += Len.CLT_DS_RIGA;
        cltDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        cltDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CLT_DS_VER, 0);
        position += Len.CLT_DS_VER;
        cltDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.CLT_DS_TS_INI_CPTZ, 0);
        position += Len.CLT_DS_TS_INI_CPTZ;
        cltDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.CLT_DS_TS_END_CPTZ, 0);
        position += Len.CLT_DS_TS_END_CPTZ;
        cltDsUtente = MarshalByte.readString(buffer, position, Len.CLT_DS_UTENTE);
        position += Len.CLT_DS_UTENTE;
        cltDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getClauTxtBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, cltIdClauTxt, Len.Int.CLT_ID_CLAU_TXT, 0);
        position += Len.CLT_ID_CLAU_TXT;
        MarshalByte.writeIntAsPacked(buffer, position, cltIdOgg, Len.Int.CLT_ID_OGG, 0);
        position += Len.CLT_ID_OGG;
        MarshalByte.writeString(buffer, position, cltTpOgg, Len.CLT_TP_OGG);
        position += Len.CLT_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, cltIdMoviCrz, Len.Int.CLT_ID_MOVI_CRZ, 0);
        position += Len.CLT_ID_MOVI_CRZ;
        cltIdMoviChiu.getCltIdMoviChiuAsBuffer(buffer, position);
        position += CltIdMoviChiu.Len.CLT_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, cltDtIniEff, Len.Int.CLT_DT_INI_EFF, 0);
        position += Len.CLT_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, cltDtEndEff, Len.Int.CLT_DT_END_EFF, 0);
        position += Len.CLT_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, cltCodCompAnia, Len.Int.CLT_COD_COMP_ANIA, 0);
        position += Len.CLT_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, cltTpClau, Len.CLT_TP_CLAU);
        position += Len.CLT_TP_CLAU;
        MarshalByte.writeString(buffer, position, cltCodClau, Len.CLT_COD_CLAU);
        position += Len.CLT_COD_CLAU;
        MarshalByte.writeString(buffer, position, cltDescBreve, Len.CLT_DESC_BREVE);
        position += Len.CLT_DESC_BREVE;
        getCltDescLngVcharBytes(buffer, position);
        position += Len.CLT_DESC_LNG_VCHAR;
        MarshalByte.writeLongAsPacked(buffer, position, cltDsRiga, Len.Int.CLT_DS_RIGA, 0);
        position += Len.CLT_DS_RIGA;
        MarshalByte.writeChar(buffer, position, cltDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, cltDsVer, Len.Int.CLT_DS_VER, 0);
        position += Len.CLT_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, cltDsTsIniCptz, Len.Int.CLT_DS_TS_INI_CPTZ, 0);
        position += Len.CLT_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, cltDsTsEndCptz, Len.Int.CLT_DS_TS_END_CPTZ, 0);
        position += Len.CLT_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, cltDsUtente, Len.CLT_DS_UTENTE);
        position += Len.CLT_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, cltDsStatoElab);
        return buffer;
    }

    public void setCltIdClauTxt(int cltIdClauTxt) {
        this.cltIdClauTxt = cltIdClauTxt;
    }

    public int getCltIdClauTxt() {
        return this.cltIdClauTxt;
    }

    public void setCltIdOgg(int cltIdOgg) {
        this.cltIdOgg = cltIdOgg;
    }

    public int getCltIdOgg() {
        return this.cltIdOgg;
    }

    public void setCltTpOgg(String cltTpOgg) {
        this.cltTpOgg = Functions.subString(cltTpOgg, Len.CLT_TP_OGG);
    }

    public String getCltTpOgg() {
        return this.cltTpOgg;
    }

    public void setCltIdMoviCrz(int cltIdMoviCrz) {
        this.cltIdMoviCrz = cltIdMoviCrz;
    }

    public int getCltIdMoviCrz() {
        return this.cltIdMoviCrz;
    }

    public void setCltDtIniEff(int cltDtIniEff) {
        this.cltDtIniEff = cltDtIniEff;
    }

    public int getCltDtIniEff() {
        return this.cltDtIniEff;
    }

    public void setCltDtEndEff(int cltDtEndEff) {
        this.cltDtEndEff = cltDtEndEff;
    }

    public int getCltDtEndEff() {
        return this.cltDtEndEff;
    }

    public void setCltCodCompAnia(int cltCodCompAnia) {
        this.cltCodCompAnia = cltCodCompAnia;
    }

    public int getCltCodCompAnia() {
        return this.cltCodCompAnia;
    }

    public void setCltTpClau(String cltTpClau) {
        this.cltTpClau = Functions.subString(cltTpClau, Len.CLT_TP_CLAU);
    }

    public String getCltTpClau() {
        return this.cltTpClau;
    }

    public String getCltTpClauFormatted() {
        return Functions.padBlanks(getCltTpClau(), Len.CLT_TP_CLAU);
    }

    public void setCltCodClau(String cltCodClau) {
        this.cltCodClau = Functions.subString(cltCodClau, Len.CLT_COD_CLAU);
    }

    public String getCltCodClau() {
        return this.cltCodClau;
    }

    public String getCltCodClauFormatted() {
        return Functions.padBlanks(getCltCodClau(), Len.CLT_COD_CLAU);
    }

    public void setCltDescBreve(String cltDescBreve) {
        this.cltDescBreve = Functions.subString(cltDescBreve, Len.CLT_DESC_BREVE);
    }

    public String getCltDescBreve() {
        return this.cltDescBreve;
    }

    public void setCltDescLngVcharFormatted(String data) {
        byte[] buffer = new byte[Len.CLT_DESC_LNG_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.CLT_DESC_LNG_VCHAR);
        setCltDescLngVcharBytes(buffer, 1);
    }

    public String getCltDescLngVcharFormatted() {
        return MarshalByteExt.bufferToStr(getCltDescLngVcharBytes());
    }

    /**Original name: CLT-DESC-LNG-VCHAR<br>*/
    public byte[] getCltDescLngVcharBytes() {
        byte[] buffer = new byte[Len.CLT_DESC_LNG_VCHAR];
        return getCltDescLngVcharBytes(buffer, 1);
    }

    public void setCltDescLngVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        cltDescLngLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        cltDescLng = MarshalByte.readString(buffer, position, Len.CLT_DESC_LNG);
    }

    public byte[] getCltDescLngVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, cltDescLngLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, cltDescLng, Len.CLT_DESC_LNG);
        return buffer;
    }

    public void setCltDescLngLen(short cltDescLngLen) {
        this.cltDescLngLen = cltDescLngLen;
    }

    public short getCltDescLngLen() {
        return this.cltDescLngLen;
    }

    public void setCltDescLng(String cltDescLng) {
        this.cltDescLng = Functions.subString(cltDescLng, Len.CLT_DESC_LNG);
    }

    public String getCltDescLng() {
        return this.cltDescLng;
    }

    public void setCltDsRiga(long cltDsRiga) {
        this.cltDsRiga = cltDsRiga;
    }

    public long getCltDsRiga() {
        return this.cltDsRiga;
    }

    public void setCltDsOperSql(char cltDsOperSql) {
        this.cltDsOperSql = cltDsOperSql;
    }

    public void setCltDsOperSqlFormatted(String cltDsOperSql) {
        setCltDsOperSql(Functions.charAt(cltDsOperSql, Types.CHAR_SIZE));
    }

    public char getCltDsOperSql() {
        return this.cltDsOperSql;
    }

    public void setCltDsVer(int cltDsVer) {
        this.cltDsVer = cltDsVer;
    }

    public int getCltDsVer() {
        return this.cltDsVer;
    }

    public void setCltDsTsIniCptz(long cltDsTsIniCptz) {
        this.cltDsTsIniCptz = cltDsTsIniCptz;
    }

    public long getCltDsTsIniCptz() {
        return this.cltDsTsIniCptz;
    }

    public void setCltDsTsEndCptz(long cltDsTsEndCptz) {
        this.cltDsTsEndCptz = cltDsTsEndCptz;
    }

    public long getCltDsTsEndCptz() {
        return this.cltDsTsEndCptz;
    }

    public void setCltDsUtente(String cltDsUtente) {
        this.cltDsUtente = Functions.subString(cltDsUtente, Len.CLT_DS_UTENTE);
    }

    public String getCltDsUtente() {
        return this.cltDsUtente;
    }

    public void setCltDsStatoElab(char cltDsStatoElab) {
        this.cltDsStatoElab = cltDsStatoElab;
    }

    public void setCltDsStatoElabFormatted(String cltDsStatoElab) {
        setCltDsStatoElab(Functions.charAt(cltDsStatoElab, Types.CHAR_SIZE));
    }

    public char getCltDsStatoElab() {
        return this.cltDsStatoElab;
    }

    public CltIdMoviChiu getCltIdMoviChiu() {
        return cltIdMoviChiu;
    }

    @Override
    public byte[] serialize() {
        return getClauTxtBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CLT_ID_CLAU_TXT = 5;
        public static final int CLT_ID_OGG = 5;
        public static final int CLT_TP_OGG = 2;
        public static final int CLT_ID_MOVI_CRZ = 5;
        public static final int CLT_DT_INI_EFF = 5;
        public static final int CLT_DT_END_EFF = 5;
        public static final int CLT_COD_COMP_ANIA = 3;
        public static final int CLT_TP_CLAU = 3;
        public static final int CLT_COD_CLAU = 20;
        public static final int CLT_DESC_BREVE = 30;
        public static final int CLT_DESC_LNG_LEN = 2;
        public static final int CLT_DESC_LNG = 250;
        public static final int CLT_DESC_LNG_VCHAR = CLT_DESC_LNG_LEN + CLT_DESC_LNG;
        public static final int CLT_DS_RIGA = 6;
        public static final int CLT_DS_OPER_SQL = 1;
        public static final int CLT_DS_VER = 5;
        public static final int CLT_DS_TS_INI_CPTZ = 10;
        public static final int CLT_DS_TS_END_CPTZ = 10;
        public static final int CLT_DS_UTENTE = 20;
        public static final int CLT_DS_STATO_ELAB = 1;
        public static final int CLAU_TXT = CLT_ID_CLAU_TXT + CLT_ID_OGG + CLT_TP_OGG + CLT_ID_MOVI_CRZ + CltIdMoviChiu.Len.CLT_ID_MOVI_CHIU + CLT_DT_INI_EFF + CLT_DT_END_EFF + CLT_COD_COMP_ANIA + CLT_TP_CLAU + CLT_COD_CLAU + CLT_DESC_BREVE + CLT_DESC_LNG_VCHAR + CLT_DS_RIGA + CLT_DS_OPER_SQL + CLT_DS_VER + CLT_DS_TS_INI_CPTZ + CLT_DS_TS_END_CPTZ + CLT_DS_UTENTE + CLT_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CLT_ID_CLAU_TXT = 9;
            public static final int CLT_ID_OGG = 9;
            public static final int CLT_ID_MOVI_CRZ = 9;
            public static final int CLT_DT_INI_EFF = 8;
            public static final int CLT_DT_END_EFF = 8;
            public static final int CLT_COD_COMP_ANIA = 5;
            public static final int CLT_DS_RIGA = 10;
            public static final int CLT_DS_VER = 9;
            public static final int CLT_DS_TS_INI_CPTZ = 18;
            public static final int CLT_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
