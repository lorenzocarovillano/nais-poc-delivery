package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-DT-RICOR-PREC<br>
 * Variable: WPMO-DT-RICOR-PREC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoDtRicorPrec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoDtRicorPrec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_DT_RICOR_PREC;
    }

    public void setWpmoDtRicorPrec(int wpmoDtRicorPrec) {
        writeIntAsPacked(Pos.WPMO_DT_RICOR_PREC, wpmoDtRicorPrec, Len.Int.WPMO_DT_RICOR_PREC);
    }

    public void setWpmoDtRicorPrecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_DT_RICOR_PREC, Pos.WPMO_DT_RICOR_PREC);
    }

    /**Original name: WPMO-DT-RICOR-PREC<br>*/
    public int getWpmoDtRicorPrec() {
        return readPackedAsInt(Pos.WPMO_DT_RICOR_PREC, Len.Int.WPMO_DT_RICOR_PREC);
    }

    public byte[] getWpmoDtRicorPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_DT_RICOR_PREC, Pos.WPMO_DT_RICOR_PREC);
        return buffer;
    }

    public void initWpmoDtRicorPrecSpaces() {
        fill(Pos.WPMO_DT_RICOR_PREC, Len.WPMO_DT_RICOR_PREC, Types.SPACE_CHAR);
    }

    public void setWpmoDtRicorPrecNull(String wpmoDtRicorPrecNull) {
        writeString(Pos.WPMO_DT_RICOR_PREC_NULL, wpmoDtRicorPrecNull, Len.WPMO_DT_RICOR_PREC_NULL);
    }

    /**Original name: WPMO-DT-RICOR-PREC-NULL<br>*/
    public String getWpmoDtRicorPrecNull() {
        return readString(Pos.WPMO_DT_RICOR_PREC_NULL, Len.WPMO_DT_RICOR_PREC_NULL);
    }

    public String getWpmoDtRicorPrecNullFormatted() {
        return Functions.padBlanks(getWpmoDtRicorPrecNull(), Len.WPMO_DT_RICOR_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_DT_RICOR_PREC = 1;
        public static final int WPMO_DT_RICOR_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_DT_RICOR_PREC = 5;
        public static final int WPMO_DT_RICOR_PREC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_DT_RICOR_PREC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
