package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMP-NET-DFZ<br>
 * Variable: DFL-IMP-NET-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpNetDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpNetDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMP_NET_DFZ;
    }

    public void setDflImpNetDfz(AfDecimal dflImpNetDfz) {
        writeDecimalAsPacked(Pos.DFL_IMP_NET_DFZ, dflImpNetDfz.copy());
    }

    public void setDflImpNetDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMP_NET_DFZ, Pos.DFL_IMP_NET_DFZ);
    }

    /**Original name: DFL-IMP-NET-DFZ<br>*/
    public AfDecimal getDflImpNetDfz() {
        return readPackedAsDecimal(Pos.DFL_IMP_NET_DFZ, Len.Int.DFL_IMP_NET_DFZ, Len.Fract.DFL_IMP_NET_DFZ);
    }

    public byte[] getDflImpNetDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMP_NET_DFZ, Pos.DFL_IMP_NET_DFZ);
        return buffer;
    }

    public void setDflImpNetDfzNull(String dflImpNetDfzNull) {
        writeString(Pos.DFL_IMP_NET_DFZ_NULL, dflImpNetDfzNull, Len.DFL_IMP_NET_DFZ_NULL);
    }

    /**Original name: DFL-IMP-NET-DFZ-NULL<br>*/
    public String getDflImpNetDfzNull() {
        return readString(Pos.DFL_IMP_NET_DFZ_NULL, Len.DFL_IMP_NET_DFZ_NULL);
    }

    public String getDflImpNetDfzNullFormatted() {
        return Functions.padBlanks(getDflImpNetDfzNull(), Len.DFL_IMP_NET_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMP_NET_DFZ = 1;
        public static final int DFL_IMP_NET_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMP_NET_DFZ = 8;
        public static final int DFL_IMP_NET_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMP_NET_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMP_NET_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
