package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-SOPR-ALT<br>
 * Variable: WTDR-TOT-SOPR-ALT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotSoprAlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotSoprAlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_SOPR_ALT;
    }

    public void setWtdrTotSoprAlt(AfDecimal wtdrTotSoprAlt) {
        writeDecimalAsPacked(Pos.WTDR_TOT_SOPR_ALT, wtdrTotSoprAlt.copy());
    }

    /**Original name: WTDR-TOT-SOPR-ALT<br>*/
    public AfDecimal getWtdrTotSoprAlt() {
        return readPackedAsDecimal(Pos.WTDR_TOT_SOPR_ALT, Len.Int.WTDR_TOT_SOPR_ALT, Len.Fract.WTDR_TOT_SOPR_ALT);
    }

    public void setWtdrTotSoprAltNull(String wtdrTotSoprAltNull) {
        writeString(Pos.WTDR_TOT_SOPR_ALT_NULL, wtdrTotSoprAltNull, Len.WTDR_TOT_SOPR_ALT_NULL);
    }

    /**Original name: WTDR-TOT-SOPR-ALT-NULL<br>*/
    public String getWtdrTotSoprAltNull() {
        return readString(Pos.WTDR_TOT_SOPR_ALT_NULL, Len.WTDR_TOT_SOPR_ALT_NULL);
    }

    public String getWtdrTotSoprAltNullFormatted() {
        return Functions.padBlanks(getWtdrTotSoprAltNull(), Len.WTDR_TOT_SOPR_ALT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SOPR_ALT = 1;
        public static final int WTDR_TOT_SOPR_ALT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SOPR_ALT = 8;
        public static final int WTDR_TOT_SOPR_ALT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SOPR_ALT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SOPR_ALT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
