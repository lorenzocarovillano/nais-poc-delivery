package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-PC-GAR-NORISK-MARS<br>
 * Variable: WPCO-PC-GAR-NORISK-MARS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoPcGarNoriskMars extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoPcGarNoriskMars() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_PC_GAR_NORISK_MARS;
    }

    public void setWpcoPcGarNoriskMars(AfDecimal wpcoPcGarNoriskMars) {
        writeDecimalAsPacked(Pos.WPCO_PC_GAR_NORISK_MARS, wpcoPcGarNoriskMars.copy());
    }

    public void setDpcoPcGarNoriskMarsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_PC_GAR_NORISK_MARS, Pos.WPCO_PC_GAR_NORISK_MARS);
    }

    /**Original name: WPCO-PC-GAR-NORISK-MARS<br>*/
    public AfDecimal getWpcoPcGarNoriskMars() {
        return readPackedAsDecimal(Pos.WPCO_PC_GAR_NORISK_MARS, Len.Int.WPCO_PC_GAR_NORISK_MARS, Len.Fract.WPCO_PC_GAR_NORISK_MARS);
    }

    public byte[] getWpcoPcGarNoriskMarsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_PC_GAR_NORISK_MARS, Pos.WPCO_PC_GAR_NORISK_MARS);
        return buffer;
    }

    public void setWpcoPcGarNoriskMarsNull(String wpcoPcGarNoriskMarsNull) {
        writeString(Pos.WPCO_PC_GAR_NORISK_MARS_NULL, wpcoPcGarNoriskMarsNull, Len.WPCO_PC_GAR_NORISK_MARS_NULL);
    }

    /**Original name: WPCO-PC-GAR-NORISK-MARS-NULL<br>*/
    public String getWpcoPcGarNoriskMarsNull() {
        return readString(Pos.WPCO_PC_GAR_NORISK_MARS_NULL, Len.WPCO_PC_GAR_NORISK_MARS_NULL);
    }

    public String getWpcoPcGarNoriskMarsNullFormatted() {
        return Functions.padBlanks(getWpcoPcGarNoriskMarsNull(), Len.WPCO_PC_GAR_NORISK_MARS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_PC_GAR_NORISK_MARS = 1;
        public static final int WPCO_PC_GAR_NORISK_MARS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_PC_GAR_NORISK_MARS = 4;
        public static final int WPCO_PC_GAR_NORISK_MARS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_PC_GAR_NORISK_MARS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_PC_GAR_NORISK_MARS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
