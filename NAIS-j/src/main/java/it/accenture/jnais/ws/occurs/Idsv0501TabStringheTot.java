package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0501-TAB-STRINGHE-TOT<br>
 * Variables: IDSV0501-TAB-STRINGHE-TOT from copybook IDSV0501<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idsv0501TabStringheTot {

    //==== PROPERTIES ====
    //Original name: IDSV0501-STRINGA-TOT
    private String idsv0501StringaTot = DefaultValues.stringVal(Len.IDSV0501_STRINGA_TOT);

    //==== METHODS ====
    public byte[] getIdsv0501TabStringheTotBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, idsv0501StringaTot, Len.IDSV0501_STRINGA_TOT);
        return buffer;
    }

    public void setIdsv0501StringaTot(String idsv0501StringaTot) {
        this.idsv0501StringaTot = Functions.subString(idsv0501StringaTot, Len.IDSV0501_STRINGA_TOT);
    }

    public void setIdsv0501StringaTotSubstring(String replacement, int start, int length) {
        idsv0501StringaTot = Functions.setSubstring(idsv0501StringaTot, replacement, start, length);
    }

    public String getIdsv0501StringaTot() {
        return this.idsv0501StringaTot;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSV0501_STRINGA_TOT = 60;
        public static final int IDSV0501_TAB_STRINGHE_TOT = IDSV0501_STRINGA_TOT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
