package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-RISCPAR<br>
 * Variable: WB03-RISCPAR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03Riscpar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03Riscpar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_RISCPAR;
    }

    public void setWb03RiscparFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_RISCPAR, Pos.WB03_RISCPAR);
    }

    /**Original name: WB03-RISCPAR<br>*/
    public AfDecimal getWb03Riscpar() {
        return readPackedAsDecimal(Pos.WB03_RISCPAR, Len.Int.WB03_RISCPAR, Len.Fract.WB03_RISCPAR);
    }

    public byte[] getWb03RiscparAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_RISCPAR, Pos.WB03_RISCPAR);
        return buffer;
    }

    public void setWb03RiscparNull(String wb03RiscparNull) {
        writeString(Pos.WB03_RISCPAR_NULL, wb03RiscparNull, Len.WB03_RISCPAR_NULL);
    }

    /**Original name: WB03-RISCPAR-NULL<br>*/
    public String getWb03RiscparNull() {
        return readString(Pos.WB03_RISCPAR_NULL, Len.WB03_RISCPAR_NULL);
    }

    public String getWb03RiscparNullFormatted() {
        return Functions.padBlanks(getWb03RiscparNull(), Len.WB03_RISCPAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_RISCPAR = 1;
        public static final int WB03_RISCPAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_RISCPAR = 8;
        public static final int WB03_RISCPAR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_RISCPAR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_RISCPAR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
