package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-AA-CNBZ-K2<br>
 * Variable: WDFA-AA-CNBZ-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaAaCnbzK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaAaCnbzK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_AA_CNBZ_K2;
    }

    public void setWdfaAaCnbzK2(short wdfaAaCnbzK2) {
        writeShortAsPacked(Pos.WDFA_AA_CNBZ_K2, wdfaAaCnbzK2, Len.Int.WDFA_AA_CNBZ_K2);
    }

    public void setWdfaAaCnbzK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_AA_CNBZ_K2, Pos.WDFA_AA_CNBZ_K2);
    }

    /**Original name: WDFA-AA-CNBZ-K2<br>*/
    public short getWdfaAaCnbzK2() {
        return readPackedAsShort(Pos.WDFA_AA_CNBZ_K2, Len.Int.WDFA_AA_CNBZ_K2);
    }

    public byte[] getWdfaAaCnbzK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_AA_CNBZ_K2, Pos.WDFA_AA_CNBZ_K2);
        return buffer;
    }

    public void setWdfaAaCnbzK2Null(String wdfaAaCnbzK2Null) {
        writeString(Pos.WDFA_AA_CNBZ_K2_NULL, wdfaAaCnbzK2Null, Len.WDFA_AA_CNBZ_K2_NULL);
    }

    /**Original name: WDFA-AA-CNBZ-K2-NULL<br>*/
    public String getWdfaAaCnbzK2Null() {
        return readString(Pos.WDFA_AA_CNBZ_K2_NULL, Len.WDFA_AA_CNBZ_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_AA_CNBZ_K2 = 1;
        public static final int WDFA_AA_CNBZ_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_AA_CNBZ_K2 = 3;
        public static final int WDFA_AA_CNBZ_K2_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_AA_CNBZ_K2 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
