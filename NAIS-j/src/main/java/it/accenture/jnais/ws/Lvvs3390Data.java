package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3390<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3390Data {

    //==== PROPERTIES ====
    public static final int DTGA_TAB_TRAN_MAXOCCURS = 1250;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS3390";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    //Original name: DETT-TIT-CONT
    private DettTitContIdbsdtc0 dettTitCont = new DettTitContIdbsdtc0();
    //Original name: TIT-CONT
    private TitContIdbstit0 titCont = new TitContIdbstit0();
    //Original name: DTGA-ELE-TGA-MAX
    private short dtgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTGA-TAB-TRAN
    private LazyArrayCopy<W1tgaTabTran> dtgaTabTran = new LazyArrayCopy<W1tgaTabTran>(new W1tgaTabTran(), 1, DTGA_TAB_TRAN_MAXOCCURS);
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setDtgaEleTgaMax(short dtgaEleTgaMax) {
        this.dtgaEleTgaMax = dtgaEleTgaMax;
    }

    public short getDtgaEleTgaMax() {
        return this.dtgaEleTgaMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public DettTitContIdbsdtc0 getDettTitCont() {
        return dettTitCont;
    }

    public LazyArrayCopy<W1tgaTabTran> getDtgaTabTranObj() {
        return dtgaTabTran;
    }

    public TitContIdbstit0 getTitCont() {
        return titCont;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
