package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: TAB-GIORNI<br>
 * Variable: TAB-GIORNI from program LCCS0004<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TabGiorni extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int T_GG_MAXOCCURS = 12;

    //==== CONSTRUCTORS ====
    public TabGiorni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TAB_GIORNI;
    }

    @Override
    public void init() {
        int position = 1;
        writeString(position, "312831303130313130313031", Len.TAB_GIORNI);
    }

    public void settGg(int tGgIdx, short tGg) {
        int position = Pos.tGg(tGgIdx - 1);
        writeShort(position, tGg, Len.Int.T_GG, SignType.NO_SIGN);
    }

    /**Original name: T-GG<br>*/
    public short gettGg(int tGgIdx) {
        int position = Pos.tGg(tGgIdx - 1);
        return readNumDispUnsignedShort(position, Len.T_GG);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TAB_GIORNI = 1;
        public static final int FILLER_WS1 = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int tGg(int idx) {
            return FILLER_WS1 + idx * Len.T_GG;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int T_GG = 2;
        public static final int TAB_GIORNI = 24;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int T_GG = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
