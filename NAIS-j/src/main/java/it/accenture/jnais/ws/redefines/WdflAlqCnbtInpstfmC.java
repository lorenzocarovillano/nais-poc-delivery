package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ALQ-CNBT-INPSTFM-C<br>
 * Variable: WDFL-ALQ-CNBT-INPSTFM-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAlqCnbtInpstfmC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAlqCnbtInpstfmC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ALQ_CNBT_INPSTFM_C;
    }

    public void setWdflAlqCnbtInpstfmC(AfDecimal wdflAlqCnbtInpstfmC) {
        writeDecimalAsPacked(Pos.WDFL_ALQ_CNBT_INPSTFM_C, wdflAlqCnbtInpstfmC.copy());
    }

    public void setWdflAlqCnbtInpstfmCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ALQ_CNBT_INPSTFM_C, Pos.WDFL_ALQ_CNBT_INPSTFM_C);
    }

    /**Original name: WDFL-ALQ-CNBT-INPSTFM-C<br>*/
    public AfDecimal getWdflAlqCnbtInpstfmC() {
        return readPackedAsDecimal(Pos.WDFL_ALQ_CNBT_INPSTFM_C, Len.Int.WDFL_ALQ_CNBT_INPSTFM_C, Len.Fract.WDFL_ALQ_CNBT_INPSTFM_C);
    }

    public byte[] getWdflAlqCnbtInpstfmCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ALQ_CNBT_INPSTFM_C, Pos.WDFL_ALQ_CNBT_INPSTFM_C);
        return buffer;
    }

    public void setWdflAlqCnbtInpstfmCNull(String wdflAlqCnbtInpstfmCNull) {
        writeString(Pos.WDFL_ALQ_CNBT_INPSTFM_C_NULL, wdflAlqCnbtInpstfmCNull, Len.WDFL_ALQ_CNBT_INPSTFM_C_NULL);
    }

    /**Original name: WDFL-ALQ-CNBT-INPSTFM-C-NULL<br>*/
    public String getWdflAlqCnbtInpstfmCNull() {
        return readString(Pos.WDFL_ALQ_CNBT_INPSTFM_C_NULL, Len.WDFL_ALQ_CNBT_INPSTFM_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_CNBT_INPSTFM_C = 1;
        public static final int WDFL_ALQ_CNBT_INPSTFM_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_CNBT_INPSTFM_C = 4;
        public static final int WDFL_ALQ_CNBT_INPSTFM_C_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_CNBT_INPSTFM_C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_CNBT_INPSTFM_C = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
