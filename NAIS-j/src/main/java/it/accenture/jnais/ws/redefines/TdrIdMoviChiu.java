package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-ID-MOVI-CHIU<br>
 * Variable: TDR-ID-MOVI-CHIU from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_ID_MOVI_CHIU;
    }

    public void setTdrIdMoviChiu(int tdrIdMoviChiu) {
        writeIntAsPacked(Pos.TDR_ID_MOVI_CHIU, tdrIdMoviChiu, Len.Int.TDR_ID_MOVI_CHIU);
    }

    public void setTdrIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_ID_MOVI_CHIU, Pos.TDR_ID_MOVI_CHIU);
    }

    /**Original name: TDR-ID-MOVI-CHIU<br>*/
    public int getTdrIdMoviChiu() {
        return readPackedAsInt(Pos.TDR_ID_MOVI_CHIU, Len.Int.TDR_ID_MOVI_CHIU);
    }

    public byte[] getTdrIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_ID_MOVI_CHIU, Pos.TDR_ID_MOVI_CHIU);
        return buffer;
    }

    public void setTdrIdMoviChiuNull(String tdrIdMoviChiuNull) {
        writeString(Pos.TDR_ID_MOVI_CHIU_NULL, tdrIdMoviChiuNull, Len.TDR_ID_MOVI_CHIU_NULL);
    }

    /**Original name: TDR-ID-MOVI-CHIU-NULL<br>*/
    public String getTdrIdMoviChiuNull() {
        return readString(Pos.TDR_ID_MOVI_CHIU_NULL, Len.TDR_ID_MOVI_CHIU_NULL);
    }

    public String getTdrIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getTdrIdMoviChiuNull(), Len.TDR_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_ID_MOVI_CHIU = 1;
        public static final int TDR_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_ID_MOVI_CHIU = 5;
        public static final int TDR_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
