package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CAR-GEST-NON-SCON<br>
 * Variable: B03-CAR-GEST-NON-SCON from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CarGestNonScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CarGestNonScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CAR_GEST_NON_SCON;
    }

    public void setB03CarGestNonScon(AfDecimal b03CarGestNonScon) {
        writeDecimalAsPacked(Pos.B03_CAR_GEST_NON_SCON, b03CarGestNonScon.copy());
    }

    public void setB03CarGestNonSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CAR_GEST_NON_SCON, Pos.B03_CAR_GEST_NON_SCON);
    }

    /**Original name: B03-CAR-GEST-NON-SCON<br>*/
    public AfDecimal getB03CarGestNonScon() {
        return readPackedAsDecimal(Pos.B03_CAR_GEST_NON_SCON, Len.Int.B03_CAR_GEST_NON_SCON, Len.Fract.B03_CAR_GEST_NON_SCON);
    }

    public byte[] getB03CarGestNonSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CAR_GEST_NON_SCON, Pos.B03_CAR_GEST_NON_SCON);
        return buffer;
    }

    public void setB03CarGestNonSconNull(String b03CarGestNonSconNull) {
        writeString(Pos.B03_CAR_GEST_NON_SCON_NULL, b03CarGestNonSconNull, Len.B03_CAR_GEST_NON_SCON_NULL);
    }

    /**Original name: B03-CAR-GEST-NON-SCON-NULL<br>*/
    public String getB03CarGestNonSconNull() {
        return readString(Pos.B03_CAR_GEST_NON_SCON_NULL, Len.B03_CAR_GEST_NON_SCON_NULL);
    }

    public String getB03CarGestNonSconNullFormatted() {
        return Functions.padBlanks(getB03CarGestNonSconNull(), Len.B03_CAR_GEST_NON_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CAR_GEST_NON_SCON = 1;
        public static final int B03_CAR_GEST_NON_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CAR_GEST_NON_SCON = 8;
        public static final int B03_CAR_GEST_NON_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CAR_GEST_NON_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CAR_GEST_NON_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
