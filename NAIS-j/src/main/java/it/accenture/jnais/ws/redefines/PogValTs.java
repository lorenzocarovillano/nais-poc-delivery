package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: POG-VAL-TS<br>
 * Variable: POG-VAL-TS from program LDBS1130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PogValTs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PogValTs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POG_VAL_TS;
    }

    public void setPogValTs(AfDecimal pogValTs) {
        writeDecimalAsPacked(Pos.POG_VAL_TS, pogValTs.copy());
    }

    public void setPogValTsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POG_VAL_TS, Pos.POG_VAL_TS);
    }

    /**Original name: POG-VAL-TS<br>*/
    public AfDecimal getPogValTs() {
        return readPackedAsDecimal(Pos.POG_VAL_TS, Len.Int.POG_VAL_TS, Len.Fract.POG_VAL_TS);
    }

    public byte[] getPogValTsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POG_VAL_TS, Pos.POG_VAL_TS);
        return buffer;
    }

    public void setPogValTsNull(String pogValTsNull) {
        writeString(Pos.POG_VAL_TS_NULL, pogValTsNull, Len.POG_VAL_TS_NULL);
    }

    /**Original name: POG-VAL-TS-NULL<br>*/
    public String getPogValTsNull() {
        return readString(Pos.POG_VAL_TS_NULL, Len.POG_VAL_TS_NULL);
    }

    public String getPogValTsNullFormatted() {
        return Functions.padBlanks(getPogValTsNull(), Len.POG_VAL_TS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POG_VAL_TS = 1;
        public static final int POG_VAL_TS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POG_VAL_TS = 8;
        public static final int POG_VAL_TS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POG_VAL_TS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int POG_VAL_TS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
