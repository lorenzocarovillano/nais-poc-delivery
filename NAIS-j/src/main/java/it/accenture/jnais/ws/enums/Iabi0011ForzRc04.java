package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IABI0011-FORZ-RC-04<br>
 * Variable: IABI0011-FORZ-RC-04 from copybook IABI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabi0011ForzRc04 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char YES = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setForzRc04(char forzRc04) {
        this.value = forzRc04;
    }

    public char getForzRc04() {
        return this.value;
    }

    public boolean isYes() {
        return value == YES;
    }

    public boolean isNo() {
        return value == NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FORZ_RC04 = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
