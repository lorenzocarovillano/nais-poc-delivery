package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.W670DatiGgincassoContesto;
import it.accenture.jnais.ws.occurs.W670DatiTit;

/**Original name: W670-AREA-LOAS0670<br>
 * Variable: W670-AREA-LOAS0670 from program LOAS0670<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class W670AreaLoas0670 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int DATI_TIT_MAXOCCURS = 1250;
    //Original name: W670-DATI-GGINCASSO-CONTESTO
    private W670DatiGgincassoContesto datiGgincassoContesto = new W670DatiGgincassoContesto();
    //Original name: W670-GGINCASSO
    private String ggincasso = DefaultValues.stringVal(Len.GGINCASSO);
    //Original name: W670-NUM-MAX-ELE
    private short numMaxEle = DefaultValues.SHORT_VAL;
    //Original name: W670-DATI-TIT
    private LazyArrayCopy<W670DatiTit> datiTit = new LazyArrayCopy<W670DatiTit>(new W670DatiTit(), 1, DATI_TIT_MAXOCCURS);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W670_AREA_LOAS0670;
    }

    @Override
    public void deserialize(byte[] buf) {
        setW670AreaLoas0670Bytes(buf);
    }

    public String getW670AreaLoas0670Formatted() {
        return getArea670VarInpFormatted();
    }

    public void setW670AreaLoas0670Bytes(byte[] buffer) {
        setW670AreaLoas0670Bytes(buffer, 1);
    }

    public byte[] getW670AreaLoas0670Bytes() {
        byte[] buffer = new byte[Len.W670_AREA_LOAS0670];
        return getW670AreaLoas0670Bytes(buffer, 1);
    }

    public void setW670AreaLoas0670Bytes(byte[] buffer, int offset) {
        int position = offset;
        setArea670VarInpBytes(buffer, position);
    }

    public byte[] getW670AreaLoas0670Bytes(byte[] buffer, int offset) {
        int position = offset;
        getArea670VarInpBytes(buffer, position);
        return buffer;
    }

    public String getArea670VarInpFormatted() {
        return MarshalByteExt.bufferToStr(getArea670VarInpBytes());
    }

    /**Original name: W670-AREA-670-VAR-INP<br>
	 * <pre>----------------------------------------------------------------*
	 *    PROCESSO DI POST-VENDITA - PORTAFOGLIO VITA
	 *    AREA VARIABILI E COSTANTI DI INPUT
	 *    SERVIZIO DI RIVALUTAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getArea670VarInpBytes() {
        byte[] buffer = new byte[Len.AREA670_VAR_INP];
        return getArea670VarInpBytes(buffer, 1);
    }

    public void setArea670VarInpBytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiGgincBytes(buffer, position);
        position += Len.DATI_GGINC;
        setAreaTitoloBytes(buffer, position);
    }

    public byte[] getArea670VarInpBytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiGgincBytes(buffer, position);
        position += Len.DATI_GGINC;
        getAreaTitoloBytes(buffer, position);
        return buffer;
    }

    public void setDatiGgincBytes(byte[] buffer, int offset) {
        int position = offset;
        datiGgincassoContesto.setDatiGgincassoContesto(MarshalByte.readString(buffer, position, W670DatiGgincassoContesto.Len.DATI_GGINCASSO_CONTESTO));
        position += W670DatiGgincassoContesto.Len.DATI_GGINCASSO_CONTESTO;
        ggincasso = MarshalByte.readFixedString(buffer, position, Len.GGINCASSO);
    }

    public byte[] getDatiGgincBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, datiGgincassoContesto.getDatiGgincassoContesto(), W670DatiGgincassoContesto.Len.DATI_GGINCASSO_CONTESTO);
        position += W670DatiGgincassoContesto.Len.DATI_GGINCASSO_CONTESTO;
        MarshalByte.writeString(buffer, position, ggincasso, Len.GGINCASSO);
        return buffer;
    }

    public void setGgincasso(int ggincasso) {
        this.ggincasso = NumericDisplay.asString(ggincasso, Len.GGINCASSO);
    }

    public int getGgincasso() {
        return NumericDisplay.asInt(this.ggincasso);
    }

    public void setAreaTitoloBytes(byte[] buffer, int offset) {
        int position = offset;
        numMaxEle = MarshalByte.readShort(buffer, position, Len.NUM_MAX_ELE);
        position += Len.NUM_MAX_ELE;
        for (int idx = 1; idx <= DATI_TIT_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                datiTit.get(idx - 1).setDatiTitBytes(buffer, position);
                position += W670DatiTit.Len.DATI_TIT;
            }
            else {
                W670DatiTit temp_datiTit = new W670DatiTit();
                temp_datiTit.initDatiTitSpaces();
                getDatiTitObj().fill(temp_datiTit);
                position += W670DatiTit.Len.DATI_TIT * (DATI_TIT_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public byte[] getAreaTitoloBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeShort(buffer, position, numMaxEle, Len.NUM_MAX_ELE);
        position += Len.NUM_MAX_ELE;
        for (int idx = 1; idx <= DATI_TIT_MAXOCCURS; idx++) {
            datiTit.get(idx - 1).getDatiTitBytes(buffer, position);
            position += W670DatiTit.Len.DATI_TIT;
        }
        return buffer;
    }

    public void setNumMaxEle(short numMaxEle) {
        this.numMaxEle = numMaxEle;
    }

    public short getNumMaxEle() {
        return this.numMaxEle;
    }

    public W670DatiGgincassoContesto getDatiGgincassoContesto() {
        return datiGgincassoContesto;
    }

    public W670DatiTit getDatiTit(int idx) {
        return datiTit.get(idx - 1);
    }

    public LazyArrayCopy<W670DatiTit> getDatiTitObj() {
        return datiTit;
    }

    @Override
    public byte[] serialize() {
        return getW670AreaLoas0670Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GGINCASSO = 5;
        public static final int DATI_GGINC = W670DatiGgincassoContesto.Len.DATI_GGINCASSO_CONTESTO + GGINCASSO;
        public static final int NUM_MAX_ELE = 4;
        public static final int AREA_TITOLO = NUM_MAX_ELE + W670AreaLoas0670.DATI_TIT_MAXOCCURS * W670DatiTit.Len.DATI_TIT;
        public static final int AREA670_VAR_INP = DATI_GGINC + AREA_TITOLO;
        public static final int W670_AREA_LOAS0670 = AREA670_VAR_INP;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
