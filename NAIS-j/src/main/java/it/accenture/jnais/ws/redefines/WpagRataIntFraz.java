package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-INT-FRAZ<br>
 * Variable: WPAG-RATA-INT-FRAZ from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataIntFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataIntFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_INT_FRAZ;
    }

    public void setWpagRataIntFraz(AfDecimal wpagRataIntFraz) {
        writeDecimalAsPacked(Pos.WPAG_RATA_INT_FRAZ, wpagRataIntFraz.copy());
    }

    public void setWpagRataIntFrazFormatted(String wpagRataIntFraz) {
        setWpagRataIntFraz(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_INT_FRAZ + Len.Fract.WPAG_RATA_INT_FRAZ, Len.Fract.WPAG_RATA_INT_FRAZ, wpagRataIntFraz));
    }

    public void setWpagRataIntFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_INT_FRAZ, Pos.WPAG_RATA_INT_FRAZ);
    }

    /**Original name: WPAG-RATA-INT-FRAZ<br>*/
    public AfDecimal getWpagRataIntFraz() {
        return readPackedAsDecimal(Pos.WPAG_RATA_INT_FRAZ, Len.Int.WPAG_RATA_INT_FRAZ, Len.Fract.WPAG_RATA_INT_FRAZ);
    }

    public byte[] getWpagRataIntFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_INT_FRAZ, Pos.WPAG_RATA_INT_FRAZ);
        return buffer;
    }

    public void initWpagRataIntFrazSpaces() {
        fill(Pos.WPAG_RATA_INT_FRAZ, Len.WPAG_RATA_INT_FRAZ, Types.SPACE_CHAR);
    }

    public void setWpagRataIntFrazNull(String wpagRataIntFrazNull) {
        writeString(Pos.WPAG_RATA_INT_FRAZ_NULL, wpagRataIntFrazNull, Len.WPAG_RATA_INT_FRAZ_NULL);
    }

    /**Original name: WPAG-RATA-INT-FRAZ-NULL<br>*/
    public String getWpagRataIntFrazNull() {
        return readString(Pos.WPAG_RATA_INT_FRAZ_NULL, Len.WPAG_RATA_INT_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_INT_FRAZ = 1;
        public static final int WPAG_RATA_INT_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_INT_FRAZ = 8;
        public static final int WPAG_RATA_INT_FRAZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_INT_FRAZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_INT_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
