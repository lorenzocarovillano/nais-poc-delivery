package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-DT-9999MMGG<br>
 * Variable: WS-DT-9999MMGG from program LVVS0114<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDt9999mmggLvvs0114 {

    //==== PROPERTIES ====
    //Original name: WS-DT-AA
    private String aa = DefaultValues.stringVal(Len.AA);
    //Original name: WS-DT-MM
    private String mm = DefaultValues.stringVal(Len.MM);
    //Original name: WS-DT-GG
    private String gg = DefaultValues.stringVal(Len.GG);

    //==== METHODS ====
    public void setWsDt9999mmggFormatted(String data) {
        byte[] buffer = new byte[Len.WS_DT9999MMGG];
        MarshalByte.writeString(buffer, 1, data, Len.WS_DT9999MMGG);
        setWsDt9999mmggBytes(buffer, 1);
    }

    public void setWsDt9999mmggBytes(byte[] buffer, int offset) {
        int position = offset;
        aa = MarshalByte.readFixedString(buffer, position, Len.AA);
        position += Len.AA;
        mm = MarshalByte.readFixedString(buffer, position, Len.MM);
        position += Len.MM;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
    }

    public short getAa() {
        return NumericDisplay.asShort(this.aa);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WS_DT9999MMGG = AA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
