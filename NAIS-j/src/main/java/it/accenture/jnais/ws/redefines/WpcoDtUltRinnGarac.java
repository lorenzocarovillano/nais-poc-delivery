package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-RINN-GARAC<br>
 * Variable: WPCO-DT-ULT-RINN-GARAC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltRinnGarac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltRinnGarac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_RINN_GARAC;
    }

    public void setWpcoDtUltRinnGarac(int wpcoDtUltRinnGarac) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_RINN_GARAC, wpcoDtUltRinnGarac, Len.Int.WPCO_DT_ULT_RINN_GARAC);
    }

    public void setDpcoDtUltRinnGaracFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_RINN_GARAC, Pos.WPCO_DT_ULT_RINN_GARAC);
    }

    /**Original name: WPCO-DT-ULT-RINN-GARAC<br>*/
    public int getWpcoDtUltRinnGarac() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_RINN_GARAC, Len.Int.WPCO_DT_ULT_RINN_GARAC);
    }

    public byte[] getWpcoDtUltRinnGaracAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_RINN_GARAC, Pos.WPCO_DT_ULT_RINN_GARAC);
        return buffer;
    }

    public void setWpcoDtUltRinnGaracNull(String wpcoDtUltRinnGaracNull) {
        writeString(Pos.WPCO_DT_ULT_RINN_GARAC_NULL, wpcoDtUltRinnGaracNull, Len.WPCO_DT_ULT_RINN_GARAC_NULL);
    }

    /**Original name: WPCO-DT-ULT-RINN-GARAC-NULL<br>*/
    public String getWpcoDtUltRinnGaracNull() {
        return readString(Pos.WPCO_DT_ULT_RINN_GARAC_NULL, Len.WPCO_DT_ULT_RINN_GARAC_NULL);
    }

    public String getWpcoDtUltRinnGaracNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltRinnGaracNull(), Len.WPCO_DT_ULT_RINN_GARAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RINN_GARAC = 1;
        public static final int WPCO_DT_ULT_RINN_GARAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RINN_GARAC = 5;
        public static final int WPCO_DT_ULT_RINN_GARAC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_RINN_GARAC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
