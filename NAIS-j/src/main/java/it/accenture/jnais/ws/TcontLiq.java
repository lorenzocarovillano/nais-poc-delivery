package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.TclDtVlt;
import it.accenture.jnais.ws.redefines.TclIdMoviChiu;
import it.accenture.jnais.ws.redefines.TclImpbAcc;
import it.accenture.jnais.ws.redefines.TclImpbImpst252;
import it.accenture.jnais.ws.redefines.TclImpbImpstPrvr;
import it.accenture.jnais.ws.redefines.TclImpbIs;
import it.accenture.jnais.ws.redefines.TclImpbIs1382011;
import it.accenture.jnais.ws.redefines.TclImpbIs662014;
import it.accenture.jnais.ws.redefines.TclImpbTfr;
import it.accenture.jnais.ws.redefines.TclImpbVis;
import it.accenture.jnais.ws.redefines.TclImpbVis1382011;
import it.accenture.jnais.ws.redefines.TclImpbVis662014;
import it.accenture.jnais.ws.redefines.TclImpCortvo;
import it.accenture.jnais.ws.redefines.TclImpDirLiq;
import it.accenture.jnais.ws.redefines.TclImpEfflq;
import it.accenture.jnais.ws.redefines.TclImpIntrPrest;
import it.accenture.jnais.ws.redefines.TclImpIntrRitPag;
import it.accenture.jnais.ws.redefines.TclImpIs;
import it.accenture.jnais.ws.redefines.TclImpLrdLiqto;
import it.accenture.jnais.ws.redefines.TclImpNetLiqto;
import it.accenture.jnais.ws.redefines.TclImpPrest;
import it.accenture.jnais.ws.redefines.TclImpRat;
import it.accenture.jnais.ws.redefines.TclImpRimb;
import it.accenture.jnais.ws.redefines.TclImpRitAcc;
import it.accenture.jnais.ws.redefines.TclImpRitTfr;
import it.accenture.jnais.ws.redefines.TclImpRitVis;
import it.accenture.jnais.ws.redefines.TclImpst252;
import it.accenture.jnais.ws.redefines.TclImpstPrvr;
import it.accenture.jnais.ws.redefines.TclImpstSost1382011;
import it.accenture.jnais.ws.redefines.TclImpstSost662014;
import it.accenture.jnais.ws.redefines.TclImpstVis1382011;
import it.accenture.jnais.ws.redefines.TclImpstVis662014;
import it.accenture.jnais.ws.redefines.TclImpUti;

/**Original name: TCONT-LIQ<br>
 * Variable: TCONT-LIQ from copybook IDBVTCL1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class TcontLiq extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: TCL-ID-TCONT-LIQ
    private int tclIdTcontLiq = DefaultValues.INT_VAL;
    //Original name: TCL-ID-PERC-LIQ
    private int tclIdPercLiq = DefaultValues.INT_VAL;
    //Original name: TCL-ID-BNFICR-LIQ
    private int tclIdBnficrLiq = DefaultValues.INT_VAL;
    //Original name: TCL-ID-MOVI-CRZ
    private int tclIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: TCL-ID-MOVI-CHIU
    private TclIdMoviChiu tclIdMoviChiu = new TclIdMoviChiu();
    //Original name: TCL-DT-INI-EFF
    private int tclDtIniEff = DefaultValues.INT_VAL;
    //Original name: TCL-DT-END-EFF
    private int tclDtEndEff = DefaultValues.INT_VAL;
    //Original name: TCL-COD-COMP-ANIA
    private int tclCodCompAnia = DefaultValues.INT_VAL;
    //Original name: TCL-DT-VLT
    private TclDtVlt tclDtVlt = new TclDtVlt();
    //Original name: TCL-IMP-LRD-LIQTO
    private TclImpLrdLiqto tclImpLrdLiqto = new TclImpLrdLiqto();
    //Original name: TCL-IMP-PREST
    private TclImpPrest tclImpPrest = new TclImpPrest();
    //Original name: TCL-IMP-INTR-PREST
    private TclImpIntrPrest tclImpIntrPrest = new TclImpIntrPrest();
    //Original name: TCL-IMP-RAT
    private TclImpRat tclImpRat = new TclImpRat();
    //Original name: TCL-IMP-UTI
    private TclImpUti tclImpUti = new TclImpUti();
    //Original name: TCL-IMP-RIT-TFR
    private TclImpRitTfr tclImpRitTfr = new TclImpRitTfr();
    //Original name: TCL-IMP-RIT-ACC
    private TclImpRitAcc tclImpRitAcc = new TclImpRitAcc();
    //Original name: TCL-IMP-RIT-VIS
    private TclImpRitVis tclImpRitVis = new TclImpRitVis();
    //Original name: TCL-IMPB-TFR
    private TclImpbTfr tclImpbTfr = new TclImpbTfr();
    //Original name: TCL-IMPB-ACC
    private TclImpbAcc tclImpbAcc = new TclImpbAcc();
    //Original name: TCL-IMPB-VIS
    private TclImpbVis tclImpbVis = new TclImpbVis();
    //Original name: TCL-IMP-RIMB
    private TclImpRimb tclImpRimb = new TclImpRimb();
    //Original name: TCL-IMP-CORTVO
    private TclImpCortvo tclImpCortvo = new TclImpCortvo();
    //Original name: TCL-IMPB-IMPST-PRVR
    private TclImpbImpstPrvr tclImpbImpstPrvr = new TclImpbImpstPrvr();
    //Original name: TCL-IMPST-PRVR
    private TclImpstPrvr tclImpstPrvr = new TclImpstPrvr();
    //Original name: TCL-IMPB-IMPST-252
    private TclImpbImpst252 tclImpbImpst252 = new TclImpbImpst252();
    //Original name: TCL-IMPST-252
    private TclImpst252 tclImpst252 = new TclImpst252();
    //Original name: TCL-IMP-IS
    private TclImpIs tclImpIs = new TclImpIs();
    //Original name: TCL-IMP-DIR-LIQ
    private TclImpDirLiq tclImpDirLiq = new TclImpDirLiq();
    //Original name: TCL-IMP-NET-LIQTO
    private TclImpNetLiqto tclImpNetLiqto = new TclImpNetLiqto();
    //Original name: TCL-IMP-EFFLQ
    private TclImpEfflq tclImpEfflq = new TclImpEfflq();
    //Original name: TCL-COD-DVS
    private String tclCodDvs = DefaultValues.stringVal(Len.TCL_COD_DVS);
    //Original name: TCL-TP-MEZ-PAG-ACCR
    private String tclTpMezPagAccr = DefaultValues.stringVal(Len.TCL_TP_MEZ_PAG_ACCR);
    //Original name: TCL-ESTR-CNT-CORR-ACCR
    private String tclEstrCntCorrAccr = DefaultValues.stringVal(Len.TCL_ESTR_CNT_CORR_ACCR);
    //Original name: TCL-DS-RIGA
    private long tclDsRiga = DefaultValues.LONG_VAL;
    //Original name: TCL-DS-OPER-SQL
    private char tclDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: TCL-DS-VER
    private int tclDsVer = DefaultValues.INT_VAL;
    //Original name: TCL-DS-TS-INI-CPTZ
    private long tclDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: TCL-DS-TS-END-CPTZ
    private long tclDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: TCL-DS-UTENTE
    private String tclDsUtente = DefaultValues.stringVal(Len.TCL_DS_UTENTE);
    //Original name: TCL-DS-STATO-ELAB
    private char tclDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: TCL-TP-STAT-TIT
    private String tclTpStatTit = DefaultValues.stringVal(Len.TCL_TP_STAT_TIT);
    //Original name: TCL-IMPB-VIS-1382011
    private TclImpbVis1382011 tclImpbVis1382011 = new TclImpbVis1382011();
    //Original name: TCL-IMPST-VIS-1382011
    private TclImpstVis1382011 tclImpstVis1382011 = new TclImpstVis1382011();
    //Original name: TCL-IMPST-SOST-1382011
    private TclImpstSost1382011 tclImpstSost1382011 = new TclImpstSost1382011();
    //Original name: TCL-IMP-INTR-RIT-PAG
    private TclImpIntrRitPag tclImpIntrRitPag = new TclImpIntrRitPag();
    //Original name: TCL-IMPB-IS
    private TclImpbIs tclImpbIs = new TclImpbIs();
    //Original name: TCL-IMPB-IS-1382011
    private TclImpbIs1382011 tclImpbIs1382011 = new TclImpbIs1382011();
    //Original name: TCL-IMPB-VIS-662014
    private TclImpbVis662014 tclImpbVis662014 = new TclImpbVis662014();
    //Original name: TCL-IMPST-VIS-662014
    private TclImpstVis662014 tclImpstVis662014 = new TclImpstVis662014();
    //Original name: TCL-IMPB-IS-662014
    private TclImpbIs662014 tclImpbIs662014 = new TclImpbIs662014();
    //Original name: TCL-IMPST-SOST-662014
    private TclImpstSost662014 tclImpstSost662014 = new TclImpstSost662014();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCONT_LIQ;
    }

    @Override
    public void deserialize(byte[] buf) {
        setTcontLiqBytes(buf);
    }

    public void setTcontLiqFormatted(String data) {
        byte[] buffer = new byte[Len.TCONT_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.TCONT_LIQ);
        setTcontLiqBytes(buffer, 1);
    }

    public String getTcontLiqFormatted() {
        return MarshalByteExt.bufferToStr(getTcontLiqBytes());
    }

    public void setTcontLiqBytes(byte[] buffer) {
        setTcontLiqBytes(buffer, 1);
    }

    public byte[] getTcontLiqBytes() {
        byte[] buffer = new byte[Len.TCONT_LIQ];
        return getTcontLiqBytes(buffer, 1);
    }

    public void setTcontLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        tclIdTcontLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TCL_ID_TCONT_LIQ, 0);
        position += Len.TCL_ID_TCONT_LIQ;
        tclIdPercLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TCL_ID_PERC_LIQ, 0);
        position += Len.TCL_ID_PERC_LIQ;
        tclIdBnficrLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TCL_ID_BNFICR_LIQ, 0);
        position += Len.TCL_ID_BNFICR_LIQ;
        tclIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TCL_ID_MOVI_CRZ, 0);
        position += Len.TCL_ID_MOVI_CRZ;
        tclIdMoviChiu.setTclIdMoviChiuFromBuffer(buffer, position);
        position += TclIdMoviChiu.Len.TCL_ID_MOVI_CHIU;
        tclDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TCL_DT_INI_EFF, 0);
        position += Len.TCL_DT_INI_EFF;
        tclDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TCL_DT_END_EFF, 0);
        position += Len.TCL_DT_END_EFF;
        tclCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TCL_COD_COMP_ANIA, 0);
        position += Len.TCL_COD_COMP_ANIA;
        tclDtVlt.setTclDtVltFromBuffer(buffer, position);
        position += TclDtVlt.Len.TCL_DT_VLT;
        tclImpLrdLiqto.setTclImpLrdLiqtoFromBuffer(buffer, position);
        position += TclImpLrdLiqto.Len.TCL_IMP_LRD_LIQTO;
        tclImpPrest.setTclImpPrestFromBuffer(buffer, position);
        position += TclImpPrest.Len.TCL_IMP_PREST;
        tclImpIntrPrest.setTclImpIntrPrestFromBuffer(buffer, position);
        position += TclImpIntrPrest.Len.TCL_IMP_INTR_PREST;
        tclImpRat.setTclImpRatFromBuffer(buffer, position);
        position += TclImpRat.Len.TCL_IMP_RAT;
        tclImpUti.setTclImpUtiFromBuffer(buffer, position);
        position += TclImpUti.Len.TCL_IMP_UTI;
        tclImpRitTfr.setTclImpRitTfrFromBuffer(buffer, position);
        position += TclImpRitTfr.Len.TCL_IMP_RIT_TFR;
        tclImpRitAcc.setTclImpRitAccFromBuffer(buffer, position);
        position += TclImpRitAcc.Len.TCL_IMP_RIT_ACC;
        tclImpRitVis.setTclImpRitVisFromBuffer(buffer, position);
        position += TclImpRitVis.Len.TCL_IMP_RIT_VIS;
        tclImpbTfr.setTclImpbTfrFromBuffer(buffer, position);
        position += TclImpbTfr.Len.TCL_IMPB_TFR;
        tclImpbAcc.setTclImpbAccFromBuffer(buffer, position);
        position += TclImpbAcc.Len.TCL_IMPB_ACC;
        tclImpbVis.setTclImpbVisFromBuffer(buffer, position);
        position += TclImpbVis.Len.TCL_IMPB_VIS;
        tclImpRimb.setTclImpRimbFromBuffer(buffer, position);
        position += TclImpRimb.Len.TCL_IMP_RIMB;
        tclImpCortvo.setTclImpCortvoFromBuffer(buffer, position);
        position += TclImpCortvo.Len.TCL_IMP_CORTVO;
        tclImpbImpstPrvr.setTclImpbImpstPrvrFromBuffer(buffer, position);
        position += TclImpbImpstPrvr.Len.TCL_IMPB_IMPST_PRVR;
        tclImpstPrvr.setTclImpstPrvrFromBuffer(buffer, position);
        position += TclImpstPrvr.Len.TCL_IMPST_PRVR;
        tclImpbImpst252.setTclImpbImpst252FromBuffer(buffer, position);
        position += TclImpbImpst252.Len.TCL_IMPB_IMPST252;
        tclImpst252.setTclImpst252FromBuffer(buffer, position);
        position += TclImpst252.Len.TCL_IMPST252;
        tclImpIs.setTclImpIsFromBuffer(buffer, position);
        position += TclImpIs.Len.TCL_IMP_IS;
        tclImpDirLiq.setTclImpDirLiqFromBuffer(buffer, position);
        position += TclImpDirLiq.Len.TCL_IMP_DIR_LIQ;
        tclImpNetLiqto.setTclImpNetLiqtoFromBuffer(buffer, position);
        position += TclImpNetLiqto.Len.TCL_IMP_NET_LIQTO;
        tclImpEfflq.setTclImpEfflqFromBuffer(buffer, position);
        position += TclImpEfflq.Len.TCL_IMP_EFFLQ;
        tclCodDvs = MarshalByte.readString(buffer, position, Len.TCL_COD_DVS);
        position += Len.TCL_COD_DVS;
        tclTpMezPagAccr = MarshalByte.readString(buffer, position, Len.TCL_TP_MEZ_PAG_ACCR);
        position += Len.TCL_TP_MEZ_PAG_ACCR;
        tclEstrCntCorrAccr = MarshalByte.readString(buffer, position, Len.TCL_ESTR_CNT_CORR_ACCR);
        position += Len.TCL_ESTR_CNT_CORR_ACCR;
        tclDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TCL_DS_RIGA, 0);
        position += Len.TCL_DS_RIGA;
        tclDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tclDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TCL_DS_VER, 0);
        position += Len.TCL_DS_VER;
        tclDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TCL_DS_TS_INI_CPTZ, 0);
        position += Len.TCL_DS_TS_INI_CPTZ;
        tclDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TCL_DS_TS_END_CPTZ, 0);
        position += Len.TCL_DS_TS_END_CPTZ;
        tclDsUtente = MarshalByte.readString(buffer, position, Len.TCL_DS_UTENTE);
        position += Len.TCL_DS_UTENTE;
        tclDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tclTpStatTit = MarshalByte.readString(buffer, position, Len.TCL_TP_STAT_TIT);
        position += Len.TCL_TP_STAT_TIT;
        tclImpbVis1382011.setTclImpbVis1382011FromBuffer(buffer, position);
        position += TclImpbVis1382011.Len.TCL_IMPB_VIS1382011;
        tclImpstVis1382011.setTclImpstVis1382011FromBuffer(buffer, position);
        position += TclImpstVis1382011.Len.TCL_IMPST_VIS1382011;
        tclImpstSost1382011.setTclImpstSost1382011FromBuffer(buffer, position);
        position += TclImpstSost1382011.Len.TCL_IMPST_SOST1382011;
        tclImpIntrRitPag.setTclImpIntrRitPagFromBuffer(buffer, position);
        position += TclImpIntrRitPag.Len.TCL_IMP_INTR_RIT_PAG;
        tclImpbIs.setTclImpbIsFromBuffer(buffer, position);
        position += TclImpbIs.Len.TCL_IMPB_IS;
        tclImpbIs1382011.setTclImpbIs1382011FromBuffer(buffer, position);
        position += TclImpbIs1382011.Len.TCL_IMPB_IS1382011;
        tclImpbVis662014.setTclImpbVis662014FromBuffer(buffer, position);
        position += TclImpbVis662014.Len.TCL_IMPB_VIS662014;
        tclImpstVis662014.setTclImpstVis662014FromBuffer(buffer, position);
        position += TclImpstVis662014.Len.TCL_IMPST_VIS662014;
        tclImpbIs662014.setTclImpbIs662014FromBuffer(buffer, position);
        position += TclImpbIs662014.Len.TCL_IMPB_IS662014;
        tclImpstSost662014.setTclImpstSost662014FromBuffer(buffer, position);
    }

    public byte[] getTcontLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, tclIdTcontLiq, Len.Int.TCL_ID_TCONT_LIQ, 0);
        position += Len.TCL_ID_TCONT_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, tclIdPercLiq, Len.Int.TCL_ID_PERC_LIQ, 0);
        position += Len.TCL_ID_PERC_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, tclIdBnficrLiq, Len.Int.TCL_ID_BNFICR_LIQ, 0);
        position += Len.TCL_ID_BNFICR_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, tclIdMoviCrz, Len.Int.TCL_ID_MOVI_CRZ, 0);
        position += Len.TCL_ID_MOVI_CRZ;
        tclIdMoviChiu.getTclIdMoviChiuAsBuffer(buffer, position);
        position += TclIdMoviChiu.Len.TCL_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, tclDtIniEff, Len.Int.TCL_DT_INI_EFF, 0);
        position += Len.TCL_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, tclDtEndEff, Len.Int.TCL_DT_END_EFF, 0);
        position += Len.TCL_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, tclCodCompAnia, Len.Int.TCL_COD_COMP_ANIA, 0);
        position += Len.TCL_COD_COMP_ANIA;
        tclDtVlt.getTclDtVltAsBuffer(buffer, position);
        position += TclDtVlt.Len.TCL_DT_VLT;
        tclImpLrdLiqto.getTclImpLrdLiqtoAsBuffer(buffer, position);
        position += TclImpLrdLiqto.Len.TCL_IMP_LRD_LIQTO;
        tclImpPrest.getTclImpPrestAsBuffer(buffer, position);
        position += TclImpPrest.Len.TCL_IMP_PREST;
        tclImpIntrPrest.getTclImpIntrPrestAsBuffer(buffer, position);
        position += TclImpIntrPrest.Len.TCL_IMP_INTR_PREST;
        tclImpRat.getTclImpRatAsBuffer(buffer, position);
        position += TclImpRat.Len.TCL_IMP_RAT;
        tclImpUti.getTclImpUtiAsBuffer(buffer, position);
        position += TclImpUti.Len.TCL_IMP_UTI;
        tclImpRitTfr.getTclImpRitTfrAsBuffer(buffer, position);
        position += TclImpRitTfr.Len.TCL_IMP_RIT_TFR;
        tclImpRitAcc.getTclImpRitAccAsBuffer(buffer, position);
        position += TclImpRitAcc.Len.TCL_IMP_RIT_ACC;
        tclImpRitVis.getTclImpRitVisAsBuffer(buffer, position);
        position += TclImpRitVis.Len.TCL_IMP_RIT_VIS;
        tclImpbTfr.getTclImpbTfrAsBuffer(buffer, position);
        position += TclImpbTfr.Len.TCL_IMPB_TFR;
        tclImpbAcc.getTclImpbAccAsBuffer(buffer, position);
        position += TclImpbAcc.Len.TCL_IMPB_ACC;
        tclImpbVis.getTclImpbVisAsBuffer(buffer, position);
        position += TclImpbVis.Len.TCL_IMPB_VIS;
        tclImpRimb.getTclImpRimbAsBuffer(buffer, position);
        position += TclImpRimb.Len.TCL_IMP_RIMB;
        tclImpCortvo.getTclImpCortvoAsBuffer(buffer, position);
        position += TclImpCortvo.Len.TCL_IMP_CORTVO;
        tclImpbImpstPrvr.getTclImpbImpstPrvrAsBuffer(buffer, position);
        position += TclImpbImpstPrvr.Len.TCL_IMPB_IMPST_PRVR;
        tclImpstPrvr.getTclImpstPrvrAsBuffer(buffer, position);
        position += TclImpstPrvr.Len.TCL_IMPST_PRVR;
        tclImpbImpst252.getTclImpbImpst252AsBuffer(buffer, position);
        position += TclImpbImpst252.Len.TCL_IMPB_IMPST252;
        tclImpst252.getTclImpst252AsBuffer(buffer, position);
        position += TclImpst252.Len.TCL_IMPST252;
        tclImpIs.getTclImpIsAsBuffer(buffer, position);
        position += TclImpIs.Len.TCL_IMP_IS;
        tclImpDirLiq.getTclImpDirLiqAsBuffer(buffer, position);
        position += TclImpDirLiq.Len.TCL_IMP_DIR_LIQ;
        tclImpNetLiqto.getTclImpNetLiqtoAsBuffer(buffer, position);
        position += TclImpNetLiqto.Len.TCL_IMP_NET_LIQTO;
        tclImpEfflq.getTclImpEfflqAsBuffer(buffer, position);
        position += TclImpEfflq.Len.TCL_IMP_EFFLQ;
        MarshalByte.writeString(buffer, position, tclCodDvs, Len.TCL_COD_DVS);
        position += Len.TCL_COD_DVS;
        MarshalByte.writeString(buffer, position, tclTpMezPagAccr, Len.TCL_TP_MEZ_PAG_ACCR);
        position += Len.TCL_TP_MEZ_PAG_ACCR;
        MarshalByte.writeString(buffer, position, tclEstrCntCorrAccr, Len.TCL_ESTR_CNT_CORR_ACCR);
        position += Len.TCL_ESTR_CNT_CORR_ACCR;
        MarshalByte.writeLongAsPacked(buffer, position, tclDsRiga, Len.Int.TCL_DS_RIGA, 0);
        position += Len.TCL_DS_RIGA;
        MarshalByte.writeChar(buffer, position, tclDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, tclDsVer, Len.Int.TCL_DS_VER, 0);
        position += Len.TCL_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, tclDsTsIniCptz, Len.Int.TCL_DS_TS_INI_CPTZ, 0);
        position += Len.TCL_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, tclDsTsEndCptz, Len.Int.TCL_DS_TS_END_CPTZ, 0);
        position += Len.TCL_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, tclDsUtente, Len.TCL_DS_UTENTE);
        position += Len.TCL_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, tclDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tclTpStatTit, Len.TCL_TP_STAT_TIT);
        position += Len.TCL_TP_STAT_TIT;
        tclImpbVis1382011.getTclImpbVis1382011AsBuffer(buffer, position);
        position += TclImpbVis1382011.Len.TCL_IMPB_VIS1382011;
        tclImpstVis1382011.getTclImpstVis1382011AsBuffer(buffer, position);
        position += TclImpstVis1382011.Len.TCL_IMPST_VIS1382011;
        tclImpstSost1382011.getTclImpstSost1382011AsBuffer(buffer, position);
        position += TclImpstSost1382011.Len.TCL_IMPST_SOST1382011;
        tclImpIntrRitPag.getTclImpIntrRitPagAsBuffer(buffer, position);
        position += TclImpIntrRitPag.Len.TCL_IMP_INTR_RIT_PAG;
        tclImpbIs.getTclImpbIsAsBuffer(buffer, position);
        position += TclImpbIs.Len.TCL_IMPB_IS;
        tclImpbIs1382011.getTclImpbIs1382011AsBuffer(buffer, position);
        position += TclImpbIs1382011.Len.TCL_IMPB_IS1382011;
        tclImpbVis662014.getTclImpbVis662014AsBuffer(buffer, position);
        position += TclImpbVis662014.Len.TCL_IMPB_VIS662014;
        tclImpstVis662014.getTclImpstVis662014AsBuffer(buffer, position);
        position += TclImpstVis662014.Len.TCL_IMPST_VIS662014;
        tclImpbIs662014.getTclImpbIs662014AsBuffer(buffer, position);
        position += TclImpbIs662014.Len.TCL_IMPB_IS662014;
        tclImpstSost662014.getTclImpstSost662014AsBuffer(buffer, position);
        return buffer;
    }

    public void setTclIdTcontLiq(int tclIdTcontLiq) {
        this.tclIdTcontLiq = tclIdTcontLiq;
    }

    public int getTclIdTcontLiq() {
        return this.tclIdTcontLiq;
    }

    public void setTclIdPercLiq(int tclIdPercLiq) {
        this.tclIdPercLiq = tclIdPercLiq;
    }

    public int getTclIdPercLiq() {
        return this.tclIdPercLiq;
    }

    public void setTclIdBnficrLiq(int tclIdBnficrLiq) {
        this.tclIdBnficrLiq = tclIdBnficrLiq;
    }

    public int getTclIdBnficrLiq() {
        return this.tclIdBnficrLiq;
    }

    public void setTclIdMoviCrz(int tclIdMoviCrz) {
        this.tclIdMoviCrz = tclIdMoviCrz;
    }

    public int getTclIdMoviCrz() {
        return this.tclIdMoviCrz;
    }

    public void setTclDtIniEff(int tclDtIniEff) {
        this.tclDtIniEff = tclDtIniEff;
    }

    public int getTclDtIniEff() {
        return this.tclDtIniEff;
    }

    public void setTclDtEndEff(int tclDtEndEff) {
        this.tclDtEndEff = tclDtEndEff;
    }

    public int getTclDtEndEff() {
        return this.tclDtEndEff;
    }

    public void setTclCodCompAnia(int tclCodCompAnia) {
        this.tclCodCompAnia = tclCodCompAnia;
    }

    public int getTclCodCompAnia() {
        return this.tclCodCompAnia;
    }

    public void setTclCodDvs(String tclCodDvs) {
        this.tclCodDvs = Functions.subString(tclCodDvs, Len.TCL_COD_DVS);
    }

    public String getTclCodDvs() {
        return this.tclCodDvs;
    }

    public String getTclCodDvsFormatted() {
        return Functions.padBlanks(getTclCodDvs(), Len.TCL_COD_DVS);
    }

    public void setTclTpMezPagAccr(String tclTpMezPagAccr) {
        this.tclTpMezPagAccr = Functions.subString(tclTpMezPagAccr, Len.TCL_TP_MEZ_PAG_ACCR);
    }

    public String getTclTpMezPagAccr() {
        return this.tclTpMezPagAccr;
    }

    public String getTclTpMezPagAccrFormatted() {
        return Functions.padBlanks(getTclTpMezPagAccr(), Len.TCL_TP_MEZ_PAG_ACCR);
    }

    public void setTclEstrCntCorrAccr(String tclEstrCntCorrAccr) {
        this.tclEstrCntCorrAccr = Functions.subString(tclEstrCntCorrAccr, Len.TCL_ESTR_CNT_CORR_ACCR);
    }

    public String getTclEstrCntCorrAccr() {
        return this.tclEstrCntCorrAccr;
    }

    public String getTclEstrCntCorrAccrFormatted() {
        return Functions.padBlanks(getTclEstrCntCorrAccr(), Len.TCL_ESTR_CNT_CORR_ACCR);
    }

    public void setTclDsRiga(long tclDsRiga) {
        this.tclDsRiga = tclDsRiga;
    }

    public long getTclDsRiga() {
        return this.tclDsRiga;
    }

    public void setTclDsOperSql(char tclDsOperSql) {
        this.tclDsOperSql = tclDsOperSql;
    }

    public void setTclDsOperSqlFormatted(String tclDsOperSql) {
        setTclDsOperSql(Functions.charAt(tclDsOperSql, Types.CHAR_SIZE));
    }

    public char getTclDsOperSql() {
        return this.tclDsOperSql;
    }

    public void setTclDsVer(int tclDsVer) {
        this.tclDsVer = tclDsVer;
    }

    public int getTclDsVer() {
        return this.tclDsVer;
    }

    public void setTclDsTsIniCptz(long tclDsTsIniCptz) {
        this.tclDsTsIniCptz = tclDsTsIniCptz;
    }

    public long getTclDsTsIniCptz() {
        return this.tclDsTsIniCptz;
    }

    public void setTclDsTsEndCptz(long tclDsTsEndCptz) {
        this.tclDsTsEndCptz = tclDsTsEndCptz;
    }

    public long getTclDsTsEndCptz() {
        return this.tclDsTsEndCptz;
    }

    public void setTclDsUtente(String tclDsUtente) {
        this.tclDsUtente = Functions.subString(tclDsUtente, Len.TCL_DS_UTENTE);
    }

    public String getTclDsUtente() {
        return this.tclDsUtente;
    }

    public void setTclDsStatoElab(char tclDsStatoElab) {
        this.tclDsStatoElab = tclDsStatoElab;
    }

    public void setTclDsStatoElabFormatted(String tclDsStatoElab) {
        setTclDsStatoElab(Functions.charAt(tclDsStatoElab, Types.CHAR_SIZE));
    }

    public char getTclDsStatoElab() {
        return this.tclDsStatoElab;
    }

    public void setTclTpStatTit(String tclTpStatTit) {
        this.tclTpStatTit = Functions.subString(tclTpStatTit, Len.TCL_TP_STAT_TIT);
    }

    public String getTclTpStatTit() {
        return this.tclTpStatTit;
    }

    public String getTclTpStatTitFormatted() {
        return Functions.padBlanks(getTclTpStatTit(), Len.TCL_TP_STAT_TIT);
    }

    public TclDtVlt getTclDtVlt() {
        return tclDtVlt;
    }

    public TclIdMoviChiu getTclIdMoviChiu() {
        return tclIdMoviChiu;
    }

    public TclImpCortvo getTclImpCortvo() {
        return tclImpCortvo;
    }

    public TclImpDirLiq getTclImpDirLiq() {
        return tclImpDirLiq;
    }

    public TclImpEfflq getTclImpEfflq() {
        return tclImpEfflq;
    }

    public TclImpIntrPrest getTclImpIntrPrest() {
        return tclImpIntrPrest;
    }

    public TclImpIntrRitPag getTclImpIntrRitPag() {
        return tclImpIntrRitPag;
    }

    public TclImpIs getTclImpIs() {
        return tclImpIs;
    }

    public TclImpLrdLiqto getTclImpLrdLiqto() {
        return tclImpLrdLiqto;
    }

    public TclImpNetLiqto getTclImpNetLiqto() {
        return tclImpNetLiqto;
    }

    public TclImpPrest getTclImpPrest() {
        return tclImpPrest;
    }

    public TclImpRat getTclImpRat() {
        return tclImpRat;
    }

    public TclImpRimb getTclImpRimb() {
        return tclImpRimb;
    }

    public TclImpRitAcc getTclImpRitAcc() {
        return tclImpRitAcc;
    }

    public TclImpRitTfr getTclImpRitTfr() {
        return tclImpRitTfr;
    }

    public TclImpRitVis getTclImpRitVis() {
        return tclImpRitVis;
    }

    public TclImpUti getTclImpUti() {
        return tclImpUti;
    }

    public TclImpbAcc getTclImpbAcc() {
        return tclImpbAcc;
    }

    public TclImpbImpst252 getTclImpbImpst252() {
        return tclImpbImpst252;
    }

    public TclImpbImpstPrvr getTclImpbImpstPrvr() {
        return tclImpbImpstPrvr;
    }

    public TclImpbIs1382011 getTclImpbIs1382011() {
        return tclImpbIs1382011;
    }

    public TclImpbIs662014 getTclImpbIs662014() {
        return tclImpbIs662014;
    }

    public TclImpbIs getTclImpbIs() {
        return tclImpbIs;
    }

    public TclImpbTfr getTclImpbTfr() {
        return tclImpbTfr;
    }

    public TclImpbVis1382011 getTclImpbVis1382011() {
        return tclImpbVis1382011;
    }

    public TclImpbVis662014 getTclImpbVis662014() {
        return tclImpbVis662014;
    }

    public TclImpbVis getTclImpbVis() {
        return tclImpbVis;
    }

    public TclImpst252 getTclImpst252() {
        return tclImpst252;
    }

    public TclImpstPrvr getTclImpstPrvr() {
        return tclImpstPrvr;
    }

    public TclImpstSost1382011 getTclImpstSost1382011() {
        return tclImpstSost1382011;
    }

    public TclImpstSost662014 getTclImpstSost662014() {
        return tclImpstSost662014;
    }

    public TclImpstVis1382011 getTclImpstVis1382011() {
        return tclImpstVis1382011;
    }

    public TclImpstVis662014 getTclImpstVis662014() {
        return tclImpstVis662014;
    }

    @Override
    public byte[] serialize() {
        return getTcontLiqBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_ID_TCONT_LIQ = 5;
        public static final int TCL_ID_PERC_LIQ = 5;
        public static final int TCL_ID_BNFICR_LIQ = 5;
        public static final int TCL_ID_MOVI_CRZ = 5;
        public static final int TCL_DT_INI_EFF = 5;
        public static final int TCL_DT_END_EFF = 5;
        public static final int TCL_COD_COMP_ANIA = 3;
        public static final int TCL_COD_DVS = 20;
        public static final int TCL_TP_MEZ_PAG_ACCR = 2;
        public static final int TCL_ESTR_CNT_CORR_ACCR = 20;
        public static final int TCL_DS_RIGA = 6;
        public static final int TCL_DS_OPER_SQL = 1;
        public static final int TCL_DS_VER = 5;
        public static final int TCL_DS_TS_INI_CPTZ = 10;
        public static final int TCL_DS_TS_END_CPTZ = 10;
        public static final int TCL_DS_UTENTE = 20;
        public static final int TCL_DS_STATO_ELAB = 1;
        public static final int TCL_TP_STAT_TIT = 2;
        public static final int TCONT_LIQ = TCL_ID_TCONT_LIQ + TCL_ID_PERC_LIQ + TCL_ID_BNFICR_LIQ + TCL_ID_MOVI_CRZ + TclIdMoviChiu.Len.TCL_ID_MOVI_CHIU + TCL_DT_INI_EFF + TCL_DT_END_EFF + TCL_COD_COMP_ANIA + TclDtVlt.Len.TCL_DT_VLT + TclImpLrdLiqto.Len.TCL_IMP_LRD_LIQTO + TclImpPrest.Len.TCL_IMP_PREST + TclImpIntrPrest.Len.TCL_IMP_INTR_PREST + TclImpRat.Len.TCL_IMP_RAT + TclImpUti.Len.TCL_IMP_UTI + TclImpRitTfr.Len.TCL_IMP_RIT_TFR + TclImpRitAcc.Len.TCL_IMP_RIT_ACC + TclImpRitVis.Len.TCL_IMP_RIT_VIS + TclImpbTfr.Len.TCL_IMPB_TFR + TclImpbAcc.Len.TCL_IMPB_ACC + TclImpbVis.Len.TCL_IMPB_VIS + TclImpRimb.Len.TCL_IMP_RIMB + TclImpCortvo.Len.TCL_IMP_CORTVO + TclImpbImpstPrvr.Len.TCL_IMPB_IMPST_PRVR + TclImpstPrvr.Len.TCL_IMPST_PRVR + TclImpbImpst252.Len.TCL_IMPB_IMPST252 + TclImpst252.Len.TCL_IMPST252 + TclImpIs.Len.TCL_IMP_IS + TclImpDirLiq.Len.TCL_IMP_DIR_LIQ + TclImpNetLiqto.Len.TCL_IMP_NET_LIQTO + TclImpEfflq.Len.TCL_IMP_EFFLQ + TCL_COD_DVS + TCL_TP_MEZ_PAG_ACCR + TCL_ESTR_CNT_CORR_ACCR + TCL_DS_RIGA + TCL_DS_OPER_SQL + TCL_DS_VER + TCL_DS_TS_INI_CPTZ + TCL_DS_TS_END_CPTZ + TCL_DS_UTENTE + TCL_DS_STATO_ELAB + TCL_TP_STAT_TIT + TclImpbVis1382011.Len.TCL_IMPB_VIS1382011 + TclImpstVis1382011.Len.TCL_IMPST_VIS1382011 + TclImpstSost1382011.Len.TCL_IMPST_SOST1382011 + TclImpIntrRitPag.Len.TCL_IMP_INTR_RIT_PAG + TclImpbIs.Len.TCL_IMPB_IS + TclImpbIs1382011.Len.TCL_IMPB_IS1382011 + TclImpbVis662014.Len.TCL_IMPB_VIS662014 + TclImpstVis662014.Len.TCL_IMPST_VIS662014 + TclImpbIs662014.Len.TCL_IMPB_IS662014 + TclImpstSost662014.Len.TCL_IMPST_SOST662014;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_ID_TCONT_LIQ = 9;
            public static final int TCL_ID_PERC_LIQ = 9;
            public static final int TCL_ID_BNFICR_LIQ = 9;
            public static final int TCL_ID_MOVI_CRZ = 9;
            public static final int TCL_DT_INI_EFF = 8;
            public static final int TCL_DT_END_EFF = 8;
            public static final int TCL_COD_COMP_ANIA = 5;
            public static final int TCL_DS_RIGA = 10;
            public static final int TCL_DS_VER = 9;
            public static final int TCL_DS_TS_INI_CPTZ = 18;
            public static final int TCL_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
