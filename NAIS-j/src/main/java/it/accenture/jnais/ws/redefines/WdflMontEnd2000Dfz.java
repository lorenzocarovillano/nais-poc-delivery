package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-MONT-END2000-DFZ<br>
 * Variable: WDFL-MONT-END2000-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflMontEnd2000Dfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflMontEnd2000Dfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_MONT_END2000_DFZ;
    }

    public void setWdflMontEnd2000Dfz(AfDecimal wdflMontEnd2000Dfz) {
        writeDecimalAsPacked(Pos.WDFL_MONT_END2000_DFZ, wdflMontEnd2000Dfz.copy());
    }

    public void setWdflMontEnd2000DfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_MONT_END2000_DFZ, Pos.WDFL_MONT_END2000_DFZ);
    }

    /**Original name: WDFL-MONT-END2000-DFZ<br>*/
    public AfDecimal getWdflMontEnd2000Dfz() {
        return readPackedAsDecimal(Pos.WDFL_MONT_END2000_DFZ, Len.Int.WDFL_MONT_END2000_DFZ, Len.Fract.WDFL_MONT_END2000_DFZ);
    }

    public byte[] getWdflMontEnd2000DfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_MONT_END2000_DFZ, Pos.WDFL_MONT_END2000_DFZ);
        return buffer;
    }

    public void setWdflMontEnd2000DfzNull(String wdflMontEnd2000DfzNull) {
        writeString(Pos.WDFL_MONT_END2000_DFZ_NULL, wdflMontEnd2000DfzNull, Len.WDFL_MONT_END2000_DFZ_NULL);
    }

    /**Original name: WDFL-MONT-END2000-DFZ-NULL<br>*/
    public String getWdflMontEnd2000DfzNull() {
        return readString(Pos.WDFL_MONT_END2000_DFZ_NULL, Len.WDFL_MONT_END2000_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_END2000_DFZ = 1;
        public static final int WDFL_MONT_END2000_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_END2000_DFZ = 8;
        public static final int WDFL_MONT_END2000_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_END2000_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_END2000_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
