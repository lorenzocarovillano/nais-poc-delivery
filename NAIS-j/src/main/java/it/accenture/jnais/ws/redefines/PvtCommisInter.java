package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-COMMIS-INTER<br>
 * Variable: PVT-COMMIS-INTER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_COMMIS_INTER;
    }

    public void setPvtCommisInter(AfDecimal pvtCommisInter) {
        writeDecimalAsPacked(Pos.PVT_COMMIS_INTER, pvtCommisInter.copy());
    }

    public void setPvtCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_COMMIS_INTER, Pos.PVT_COMMIS_INTER);
    }

    /**Original name: PVT-COMMIS-INTER<br>*/
    public AfDecimal getPvtCommisInter() {
        return readPackedAsDecimal(Pos.PVT_COMMIS_INTER, Len.Int.PVT_COMMIS_INTER, Len.Fract.PVT_COMMIS_INTER);
    }

    public byte[] getPvtCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_COMMIS_INTER, Pos.PVT_COMMIS_INTER);
        return buffer;
    }

    public void setPvtCommisInterNull(String pvtCommisInterNull) {
        writeString(Pos.PVT_COMMIS_INTER_NULL, pvtCommisInterNull, Len.PVT_COMMIS_INTER_NULL);
    }

    /**Original name: PVT-COMMIS-INTER-NULL<br>*/
    public String getPvtCommisInterNull() {
        return readString(Pos.PVT_COMMIS_INTER_NULL, Len.PVT_COMMIS_INTER_NULL);
    }

    public String getPvtCommisInterNullFormatted() {
        return Functions.padBlanks(getPvtCommisInterNull(), Len.PVT_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_COMMIS_INTER = 1;
        public static final int PVT_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_COMMIS_INTER = 8;
        public static final int PVT_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PVT_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
