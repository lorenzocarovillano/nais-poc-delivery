package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-DT-END-COP<br>
 * Variable: TIT-DT-END-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitDtEndCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitDtEndCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_DT_END_COP;
    }

    public void setTitDtEndCop(int titDtEndCop) {
        writeIntAsPacked(Pos.TIT_DT_END_COP, titDtEndCop, Len.Int.TIT_DT_END_COP);
    }

    public void setTitDtEndCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_DT_END_COP, Pos.TIT_DT_END_COP);
    }

    /**Original name: TIT-DT-END-COP<br>*/
    public int getTitDtEndCop() {
        return readPackedAsInt(Pos.TIT_DT_END_COP, Len.Int.TIT_DT_END_COP);
    }

    public byte[] getTitDtEndCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_DT_END_COP, Pos.TIT_DT_END_COP);
        return buffer;
    }

    public void setTitDtEndCopNull(String titDtEndCopNull) {
        writeString(Pos.TIT_DT_END_COP_NULL, titDtEndCopNull, Len.TIT_DT_END_COP_NULL);
    }

    /**Original name: TIT-DT-END-COP-NULL<br>*/
    public String getTitDtEndCopNull() {
        return readString(Pos.TIT_DT_END_COP_NULL, Len.TIT_DT_END_COP_NULL);
    }

    public String getTitDtEndCopNullFormatted() {
        return Functions.padBlanks(getTitDtEndCopNull(), Len.TIT_DT_END_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_DT_END_COP = 1;
        public static final int TIT_DT_END_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_DT_END_COP = 5;
        public static final int TIT_DT_END_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_DT_END_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
