package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L23-CPT-VINCTO-PIGN<br>
 * Variable: L23-CPT-VINCTO-PIGN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L23CptVinctoPign extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L23CptVinctoPign() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L23_CPT_VINCTO_PIGN;
    }

    public void setL23CptVinctoPign(AfDecimal l23CptVinctoPign) {
        writeDecimalAsPacked(Pos.L23_CPT_VINCTO_PIGN, l23CptVinctoPign.copy());
    }

    public void setL23CptVinctoPignFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L23_CPT_VINCTO_PIGN, Pos.L23_CPT_VINCTO_PIGN);
    }

    /**Original name: L23-CPT-VINCTO-PIGN<br>*/
    public AfDecimal getL23CptVinctoPign() {
        return readPackedAsDecimal(Pos.L23_CPT_VINCTO_PIGN, Len.Int.L23_CPT_VINCTO_PIGN, Len.Fract.L23_CPT_VINCTO_PIGN);
    }

    public byte[] getL23CptVinctoPignAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L23_CPT_VINCTO_PIGN, Pos.L23_CPT_VINCTO_PIGN);
        return buffer;
    }

    public void setL23CptVinctoPignNull(String l23CptVinctoPignNull) {
        writeString(Pos.L23_CPT_VINCTO_PIGN_NULL, l23CptVinctoPignNull, Len.L23_CPT_VINCTO_PIGN_NULL);
    }

    /**Original name: L23-CPT-VINCTO-PIGN-NULL<br>*/
    public String getL23CptVinctoPignNull() {
        return readString(Pos.L23_CPT_VINCTO_PIGN_NULL, Len.L23_CPT_VINCTO_PIGN_NULL);
    }

    public String getL23CptVinctoPignNullFormatted() {
        return Functions.padBlanks(getL23CptVinctoPignNull(), Len.L23_CPT_VINCTO_PIGN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L23_CPT_VINCTO_PIGN = 1;
        public static final int L23_CPT_VINCTO_PIGN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L23_CPT_VINCTO_PIGN = 8;
        public static final int L23_CPT_VINCTO_PIGN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L23_CPT_VINCTO_PIGN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L23_CPT_VINCTO_PIGN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
