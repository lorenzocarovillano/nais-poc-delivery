package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PROV-INC<br>
 * Variable: TGA-PROV-INC from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PROV_INC;
    }

    public void setTgaProvInc(AfDecimal tgaProvInc) {
        writeDecimalAsPacked(Pos.TGA_PROV_INC, tgaProvInc.copy());
    }

    public void setTgaProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PROV_INC, Pos.TGA_PROV_INC);
    }

    /**Original name: TGA-PROV-INC<br>*/
    public AfDecimal getTgaProvInc() {
        return readPackedAsDecimal(Pos.TGA_PROV_INC, Len.Int.TGA_PROV_INC, Len.Fract.TGA_PROV_INC);
    }

    public byte[] getTgaProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PROV_INC, Pos.TGA_PROV_INC);
        return buffer;
    }

    public void setTgaProvIncNull(String tgaProvIncNull) {
        writeString(Pos.TGA_PROV_INC_NULL, tgaProvIncNull, Len.TGA_PROV_INC_NULL);
    }

    /**Original name: TGA-PROV-INC-NULL<br>*/
    public String getTgaProvIncNull() {
        return readString(Pos.TGA_PROV_INC_NULL, Len.TGA_PROV_INC_NULL);
    }

    public String getTgaProvIncNullFormatted() {
        return Functions.padBlanks(getTgaProvIncNull(), Len.TGA_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PROV_INC = 1;
        public static final int TGA_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PROV_INC = 8;
        public static final int TGA_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
