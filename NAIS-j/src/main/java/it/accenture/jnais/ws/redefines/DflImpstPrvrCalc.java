package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-PRVR-CALC<br>
 * Variable: DFL-IMPST-PRVR-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstPrvrCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstPrvrCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_PRVR_CALC;
    }

    public void setDflImpstPrvrCalc(AfDecimal dflImpstPrvrCalc) {
        writeDecimalAsPacked(Pos.DFL_IMPST_PRVR_CALC, dflImpstPrvrCalc.copy());
    }

    public void setDflImpstPrvrCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_PRVR_CALC, Pos.DFL_IMPST_PRVR_CALC);
    }

    /**Original name: DFL-IMPST-PRVR-CALC<br>*/
    public AfDecimal getDflImpstPrvrCalc() {
        return readPackedAsDecimal(Pos.DFL_IMPST_PRVR_CALC, Len.Int.DFL_IMPST_PRVR_CALC, Len.Fract.DFL_IMPST_PRVR_CALC);
    }

    public byte[] getDflImpstPrvrCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_PRVR_CALC, Pos.DFL_IMPST_PRVR_CALC);
        return buffer;
    }

    public void setDflImpstPrvrCalcNull(String dflImpstPrvrCalcNull) {
        writeString(Pos.DFL_IMPST_PRVR_CALC_NULL, dflImpstPrvrCalcNull, Len.DFL_IMPST_PRVR_CALC_NULL);
    }

    /**Original name: DFL-IMPST-PRVR-CALC-NULL<br>*/
    public String getDflImpstPrvrCalcNull() {
        return readString(Pos.DFL_IMPST_PRVR_CALC_NULL, Len.DFL_IMPST_PRVR_CALC_NULL);
    }

    public String getDflImpstPrvrCalcNullFormatted() {
        return Functions.padBlanks(getDflImpstPrvrCalcNull(), Len.DFL_IMPST_PRVR_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_PRVR_CALC = 1;
        public static final int DFL_IMPST_PRVR_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_PRVR_CALC = 8;
        public static final int DFL_IMPST_PRVR_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_PRVR_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_PRVR_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
