package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.AmmbFunzBloccoLccs0022;
import it.accenture.jnais.copy.AnaBlocco;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.OggBlocco;
import it.accenture.jnais.ws.enums.WkFlGravPresente;
import it.accenture.jnais.ws.enums.WkGravFunzBlocco;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import org.apache.commons.lang3.ArrayUtils;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0022<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0022Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI DI WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LCCS0022";
    /**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    //Original name: OGG-BLOCCO
    private OggBlocco oggBlocco = new OggBlocco();
    //Original name: AMMB-FUNZ-BLOCCO
    private AmmbFunzBloccoLccs0022 ammbFunzBlocco = new AmmbFunzBloccoLccs0022();
    //Original name: ANA-BLOCCO
    private AnaBlocco anaBlocco = new AnaBlocco();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IX-TAB-L11
    private short ixTabL11 = ((short)0);
    /**Original name: WK-CONT-ERR-BLOCC<br>
	 * <pre>   CONTATORE DEGLI ERRORI BLOCCANTI</pre>*/
    private short wkContErrBlocc = ((short)0);
    /**Original name: WK-CTRL-TP-STAT-BLOC<br>
	 * <pre>   CONTROLLO SU TIPO STATO BLOCCO</pre>*/
    private String wkCtrlTpStatBloc = DefaultValues.stringVal(Len.WK_CTRL_TP_STAT_BLOC);
    private static final String[] WK_TP_STAT_BLOC_VALID = new String[] {"AT", "NA"};
    /**Original name: WK-CTRL-TP-OGG<br>
	 * <pre>   CONTROLLO SU TIPO OGGETTO</pre>*/
    private String wkCtrlTpOgg = DefaultValues.stringVal(Len.WK_CTRL_TP_OGG);
    private static final String[] WK_TP_OGG_VALID = new String[] {"PO", "AD", "PV"};
    //Original name: WK-GRAV-FUNZ-BLOCCO
    private WkGravFunzBlocco wkGravFunzBlocco = new WkGravFunzBlocco();
    //Original name: WK-FL-GRAV-PRESENTE
    private WkFlGravPresente wkFlGravPresente = new WkFlGravPresente();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setIxTabL11(short ixTabL11) {
        this.ixTabL11 = ixTabL11;
    }

    public short getIxTabL11() {
        return this.ixTabL11;
    }

    public void setWkContErrBlocc(short wkContErrBlocc) {
        this.wkContErrBlocc = wkContErrBlocc;
    }

    public short getWkContErrBlocc() {
        return this.wkContErrBlocc;
    }

    public void setWkCtrlTpStatBloc(String wkCtrlTpStatBloc) {
        this.wkCtrlTpStatBloc = Functions.subString(wkCtrlTpStatBloc, Len.WK_CTRL_TP_STAT_BLOC);
    }

    public String getWkCtrlTpStatBloc() {
        return this.wkCtrlTpStatBloc;
    }

    public boolean isWkTpStatBlocValid() {
        return ArrayUtils.contains(WK_TP_STAT_BLOC_VALID, wkCtrlTpStatBloc);
    }

    public void setWkCtrlTpOgg(String wkCtrlTpOgg) {
        this.wkCtrlTpOgg = Functions.subString(wkCtrlTpOgg, Len.WK_CTRL_TP_OGG);
    }

    public String getWkCtrlTpOgg() {
        return this.wkCtrlTpOgg;
    }

    public boolean isWkTpOggValid() {
        return ArrayUtils.contains(WK_TP_OGG_VALID, wkCtrlTpOgg);
    }

    public AmmbFunzBloccoLccs0022 getAmmbFunzBlocco() {
        return ammbFunzBlocco;
    }

    public AnaBlocco getAnaBlocco() {
        return anaBlocco;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public OggBlocco getOggBlocco() {
        return oggBlocco;
    }

    public WkFlGravPresente getWkFlGravPresente() {
        return wkFlGravPresente;
    }

    public WkGravFunzBlocco getWkGravFunzBlocco() {
        return wkGravFunzBlocco;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_CTRL_TP_STAT_BLOC = 2;
        public static final int WK_CTRL_TP_OGG = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
