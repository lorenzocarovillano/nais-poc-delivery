package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-RIAT-I<br>
 * Variable: PCO-DT-ULT-BOLL-RIAT-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollRiatI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollRiatI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_RIAT_I;
    }

    public void setPcoDtUltBollRiatI(int pcoDtUltBollRiatI) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_RIAT_I, pcoDtUltBollRiatI, Len.Int.PCO_DT_ULT_BOLL_RIAT_I);
    }

    public void setPcoDtUltBollRiatIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RIAT_I, Pos.PCO_DT_ULT_BOLL_RIAT_I);
    }

    /**Original name: PCO-DT-ULT-BOLL-RIAT-I<br>*/
    public int getPcoDtUltBollRiatI() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_RIAT_I, Len.Int.PCO_DT_ULT_BOLL_RIAT_I);
    }

    public byte[] getPcoDtUltBollRiatIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RIAT_I, Pos.PCO_DT_ULT_BOLL_RIAT_I);
        return buffer;
    }

    public void initPcoDtUltBollRiatIHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_RIAT_I, Len.PCO_DT_ULT_BOLL_RIAT_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollRiatINull(String pcoDtUltBollRiatINull) {
        writeString(Pos.PCO_DT_ULT_BOLL_RIAT_I_NULL, pcoDtUltBollRiatINull, Len.PCO_DT_ULT_BOLL_RIAT_I_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-RIAT-I-NULL<br>*/
    public String getPcoDtUltBollRiatINull() {
        return readString(Pos.PCO_DT_ULT_BOLL_RIAT_I_NULL, Len.PCO_DT_ULT_BOLL_RIAT_I_NULL);
    }

    public String getPcoDtUltBollRiatINullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollRiatINull(), Len.PCO_DT_ULT_BOLL_RIAT_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RIAT_I = 1;
        public static final int PCO_DT_ULT_BOLL_RIAT_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RIAT_I = 5;
        public static final int PCO_DT_ULT_BOLL_RIAT_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_RIAT_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
