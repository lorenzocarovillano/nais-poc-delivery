package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-DUR-AA<br>
 * Variable: POL-DUR-AA from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolDurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_DUR_AA;
    }

    public void setPolDurAa(int polDurAa) {
        writeIntAsPacked(Pos.POL_DUR_AA, polDurAa, Len.Int.POL_DUR_AA);
    }

    public void setPolDurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_DUR_AA, Pos.POL_DUR_AA);
    }

    /**Original name: POL-DUR-AA<br>*/
    public int getPolDurAa() {
        return readPackedAsInt(Pos.POL_DUR_AA, Len.Int.POL_DUR_AA);
    }

    public byte[] getPolDurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_DUR_AA, Pos.POL_DUR_AA);
        return buffer;
    }

    public void setPolDurAaNull(String polDurAaNull) {
        writeString(Pos.POL_DUR_AA_NULL, polDurAaNull, Len.POL_DUR_AA_NULL);
    }

    /**Original name: POL-DUR-AA-NULL<br>*/
    public String getPolDurAaNull() {
        return readString(Pos.POL_DUR_AA_NULL, Len.POL_DUR_AA_NULL);
    }

    public String getPolDurAaNullFormatted() {
        return Functions.padBlanks(getPolDurAaNull(), Len.POL_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_DUR_AA = 1;
        public static final int POL_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_DUR_AA = 3;
        public static final int POL_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
