package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ETA-MM-3O-ASSTO<br>
 * Variable: TGA-ETA-MM-3O-ASSTO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaEtaMm3oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaEtaMm3oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ETA_MM3O_ASSTO;
    }

    public void setTgaEtaMm3oAssto(short tgaEtaMm3oAssto) {
        writeShortAsPacked(Pos.TGA_ETA_MM3O_ASSTO, tgaEtaMm3oAssto, Len.Int.TGA_ETA_MM3O_ASSTO);
    }

    public void setTgaEtaMm3oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ETA_MM3O_ASSTO, Pos.TGA_ETA_MM3O_ASSTO);
    }

    /**Original name: TGA-ETA-MM-3O-ASSTO<br>*/
    public short getTgaEtaMm3oAssto() {
        return readPackedAsShort(Pos.TGA_ETA_MM3O_ASSTO, Len.Int.TGA_ETA_MM3O_ASSTO);
    }

    public byte[] getTgaEtaMm3oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ETA_MM3O_ASSTO, Pos.TGA_ETA_MM3O_ASSTO);
        return buffer;
    }

    public void setTgaEtaMm3oAsstoNull(String tgaEtaMm3oAsstoNull) {
        writeString(Pos.TGA_ETA_MM3O_ASSTO_NULL, tgaEtaMm3oAsstoNull, Len.TGA_ETA_MM3O_ASSTO_NULL);
    }

    /**Original name: TGA-ETA-MM-3O-ASSTO-NULL<br>*/
    public String getTgaEtaMm3oAsstoNull() {
        return readString(Pos.TGA_ETA_MM3O_ASSTO_NULL, Len.TGA_ETA_MM3O_ASSTO_NULL);
    }

    public String getTgaEtaMm3oAsstoNullFormatted() {
        return Functions.padBlanks(getTgaEtaMm3oAsstoNull(), Len.TGA_ETA_MM3O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ETA_MM3O_ASSTO = 1;
        public static final int TGA_ETA_MM3O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ETA_MM3O_ASSTO = 2;
        public static final int TGA_ETA_MM3O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ETA_MM3O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
