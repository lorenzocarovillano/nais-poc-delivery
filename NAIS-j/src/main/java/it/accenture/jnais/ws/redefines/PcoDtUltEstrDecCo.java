package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ESTR-DEC-CO<br>
 * Variable: PCO-DT-ULT-ESTR-DEC-CO from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEstrDecCo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEstrDecCo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ESTR_DEC_CO;
    }

    public void setPcoDtUltEstrDecCo(int pcoDtUltEstrDecCo) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ESTR_DEC_CO, pcoDtUltEstrDecCo, Len.Int.PCO_DT_ULT_ESTR_DEC_CO);
    }

    public void setPcoDtUltEstrDecCoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ESTR_DEC_CO, Pos.PCO_DT_ULT_ESTR_DEC_CO);
    }

    /**Original name: PCO-DT-ULT-ESTR-DEC-CO<br>*/
    public int getPcoDtUltEstrDecCo() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ESTR_DEC_CO, Len.Int.PCO_DT_ULT_ESTR_DEC_CO);
    }

    public byte[] getPcoDtUltEstrDecCoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ESTR_DEC_CO, Pos.PCO_DT_ULT_ESTR_DEC_CO);
        return buffer;
    }

    public void initPcoDtUltEstrDecCoHighValues() {
        fill(Pos.PCO_DT_ULT_ESTR_DEC_CO, Len.PCO_DT_ULT_ESTR_DEC_CO, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEstrDecCoNull(String pcoDtUltEstrDecCoNull) {
        writeString(Pos.PCO_DT_ULT_ESTR_DEC_CO_NULL, pcoDtUltEstrDecCoNull, Len.PCO_DT_ULT_ESTR_DEC_CO_NULL);
    }

    /**Original name: PCO-DT-ULT-ESTR-DEC-CO-NULL<br>*/
    public String getPcoDtUltEstrDecCoNull() {
        return readString(Pos.PCO_DT_ULT_ESTR_DEC_CO_NULL, Len.PCO_DT_ULT_ESTR_DEC_CO_NULL);
    }

    public String getPcoDtUltEstrDecCoNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEstrDecCoNull(), Len.PCO_DT_ULT_ESTR_DEC_CO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ESTR_DEC_CO = 1;
        public static final int PCO_DT_ULT_ESTR_DEC_CO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ESTR_DEC_CO = 5;
        public static final int PCO_DT_ULT_ESTR_DEC_CO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ESTR_DEC_CO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
