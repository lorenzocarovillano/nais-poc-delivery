package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0503-ALFA-TROVATO<br>
 * Variable: IDSV0503-ALFA-TROVATO from copybook IDSV0503<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0503AlfaTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setIdsv0503AlfaTrovato(char idsv0503AlfaTrovato) {
        this.value = idsv0503AlfaTrovato;
    }

    public char getIdsv0503AlfaTrovato() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
