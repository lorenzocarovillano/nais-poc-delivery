package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-DT-VARZ-TP-IAS<br>
 * Variable: WGRZ-DT-VARZ-TP-IAS from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzDtVarzTpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzDtVarzTpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_DT_VARZ_TP_IAS;
    }

    public void setWgrzDtVarzTpIas(int wgrzDtVarzTpIas) {
        writeIntAsPacked(Pos.WGRZ_DT_VARZ_TP_IAS, wgrzDtVarzTpIas, Len.Int.WGRZ_DT_VARZ_TP_IAS);
    }

    public void setWgrzDtVarzTpIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_DT_VARZ_TP_IAS, Pos.WGRZ_DT_VARZ_TP_IAS);
    }

    /**Original name: WGRZ-DT-VARZ-TP-IAS<br>*/
    public int getWgrzDtVarzTpIas() {
        return readPackedAsInt(Pos.WGRZ_DT_VARZ_TP_IAS, Len.Int.WGRZ_DT_VARZ_TP_IAS);
    }

    public byte[] getWgrzDtVarzTpIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_DT_VARZ_TP_IAS, Pos.WGRZ_DT_VARZ_TP_IAS);
        return buffer;
    }

    public void initWgrzDtVarzTpIasSpaces() {
        fill(Pos.WGRZ_DT_VARZ_TP_IAS, Len.WGRZ_DT_VARZ_TP_IAS, Types.SPACE_CHAR);
    }

    public void setWgrzDtVarzTpIasNull(String wgrzDtVarzTpIasNull) {
        writeString(Pos.WGRZ_DT_VARZ_TP_IAS_NULL, wgrzDtVarzTpIasNull, Len.WGRZ_DT_VARZ_TP_IAS_NULL);
    }

    /**Original name: WGRZ-DT-VARZ-TP-IAS-NULL<br>*/
    public String getWgrzDtVarzTpIasNull() {
        return readString(Pos.WGRZ_DT_VARZ_TP_IAS_NULL, Len.WGRZ_DT_VARZ_TP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_VARZ_TP_IAS = 1;
        public static final int WGRZ_DT_VARZ_TP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_VARZ_TP_IAS = 5;
        public static final int WGRZ_DT_VARZ_TP_IAS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_DT_VARZ_TP_IAS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
