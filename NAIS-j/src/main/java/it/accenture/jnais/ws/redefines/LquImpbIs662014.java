package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-IS-662014<br>
 * Variable: LQU-IMPB-IS-662014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbIs662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbIs662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_IS662014;
    }

    public void setLquImpbIs662014(AfDecimal lquImpbIs662014) {
        writeDecimalAsPacked(Pos.LQU_IMPB_IS662014, lquImpbIs662014.copy());
    }

    public void setLquImpbIs662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_IS662014, Pos.LQU_IMPB_IS662014);
    }

    /**Original name: LQU-IMPB-IS-662014<br>*/
    public AfDecimal getLquImpbIs662014() {
        return readPackedAsDecimal(Pos.LQU_IMPB_IS662014, Len.Int.LQU_IMPB_IS662014, Len.Fract.LQU_IMPB_IS662014);
    }

    public byte[] getLquImpbIs662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_IS662014, Pos.LQU_IMPB_IS662014);
        return buffer;
    }

    public void setLquImpbIs662014Null(String lquImpbIs662014Null) {
        writeString(Pos.LQU_IMPB_IS662014_NULL, lquImpbIs662014Null, Len.LQU_IMPB_IS662014_NULL);
    }

    /**Original name: LQU-IMPB-IS-662014-NULL<br>*/
    public String getLquImpbIs662014Null() {
        return readString(Pos.LQU_IMPB_IS662014_NULL, Len.LQU_IMPB_IS662014_NULL);
    }

    public String getLquImpbIs662014NullFormatted() {
        return Functions.padBlanks(getLquImpbIs662014Null(), Len.LQU_IMPB_IS662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_IS662014 = 1;
        public static final int LQU_IMPB_IS662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_IS662014 = 8;
        public static final int LQU_IMPB_IS662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_IS662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_IS662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
