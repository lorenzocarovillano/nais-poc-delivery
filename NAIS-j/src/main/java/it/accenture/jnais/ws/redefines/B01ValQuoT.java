package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B01-VAL-QUO-T<br>
 * Variable: B01-VAL-QUO-T from program IDBSB010<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B01ValQuoT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B01ValQuoT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B01_VAL_QUO_T;
    }

    public void setB01ValQuoT(AfDecimal b01ValQuoT) {
        writeDecimalAsPacked(Pos.B01_VAL_QUO_T, b01ValQuoT.copy());
    }

    public void setB01ValQuoTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B01_VAL_QUO_T, Pos.B01_VAL_QUO_T);
    }

    /**Original name: B01-VAL-QUO-T<br>*/
    public AfDecimal getB01ValQuoT() {
        return readPackedAsDecimal(Pos.B01_VAL_QUO_T, Len.Int.B01_VAL_QUO_T, Len.Fract.B01_VAL_QUO_T);
    }

    public byte[] getB01ValQuoTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B01_VAL_QUO_T, Pos.B01_VAL_QUO_T);
        return buffer;
    }

    public void setB01ValQuoTNull(String b01ValQuoTNull) {
        writeString(Pos.B01_VAL_QUO_T_NULL, b01ValQuoTNull, Len.B01_VAL_QUO_T_NULL);
    }

    /**Original name: B01-VAL-QUO-T-NULL<br>*/
    public String getB01ValQuoTNull() {
        return readString(Pos.B01_VAL_QUO_T_NULL, Len.B01_VAL_QUO_T_NULL);
    }

    public String getB01ValQuoTNullFormatted() {
        return Functions.padBlanks(getB01ValQuoTNull(), Len.B01_VAL_QUO_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B01_VAL_QUO_T = 1;
        public static final int B01_VAL_QUO_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B01_VAL_QUO_T = 7;
        public static final int B01_VAL_QUO_T_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B01_VAL_QUO_T = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int B01_VAL_QUO_T = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
