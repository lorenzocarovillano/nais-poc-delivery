package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: STATO-FILESQS2<br>
 * Variable: STATO-FILESQS2 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class StatoFilesqs2 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char APERTO = 'A';
    public static final char CHIUSO = 'C';

    //==== METHODS ====
    public void setStatoFilesqs2(char statoFilesqs2) {
        this.value = statoFilesqs2;
    }

    public char getStatoFilesqs2() {
        return this.value;
    }

    public boolean isAperto() {
        return value == APERTO;
    }

    public void setAperto() {
        value = APERTO;
    }

    public void setChiuso() {
        value = CHIUSO;
    }
}
