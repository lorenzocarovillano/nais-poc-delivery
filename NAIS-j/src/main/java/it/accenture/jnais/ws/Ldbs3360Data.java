package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idbvode3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndOggDeroga;
import it.accenture.jnais.copy.Ldbv3361;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS3360<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs3360Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-OGG-DEROGA
    private IndOggDeroga indOggDeroga = new IndOggDeroga();
    //Original name: IDBVODE3
    private Idbvode3 idbvode3 = new Idbvode3();
    //Original name: LDBV3361
    private Ldbv3361 ldbv3361 = new Ldbv3361();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idbvode3 getIdbvode3() {
        return idbvode3;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndOggDeroga getIndOggDeroga() {
        return indOggDeroga;
    }

    public Ldbv3361 getLdbv3361() {
        return ldbv3361;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
