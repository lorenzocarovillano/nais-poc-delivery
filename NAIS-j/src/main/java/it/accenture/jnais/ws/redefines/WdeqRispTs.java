package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDEQ-RISP-TS<br>
 * Variable: WDEQ-RISP-TS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdeqRispTs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdeqRispTs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDEQ_RISP_TS;
    }

    public void setWdeqRispTs(AfDecimal wdeqRispTs) {
        writeDecimalAsPacked(Pos.WDEQ_RISP_TS, wdeqRispTs.copy());
    }

    public void setWdeqRispTsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDEQ_RISP_TS, Pos.WDEQ_RISP_TS);
    }

    /**Original name: WDEQ-RISP-TS<br>*/
    public AfDecimal getWdeqRispTs() {
        return readPackedAsDecimal(Pos.WDEQ_RISP_TS, Len.Int.WDEQ_RISP_TS, Len.Fract.WDEQ_RISP_TS);
    }

    public byte[] getWdeqRispTsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDEQ_RISP_TS, Pos.WDEQ_RISP_TS);
        return buffer;
    }

    public void initWdeqRispTsSpaces() {
        fill(Pos.WDEQ_RISP_TS, Len.WDEQ_RISP_TS, Types.SPACE_CHAR);
    }

    public void setWdeqRispTsNull(String wdeqRispTsNull) {
        writeString(Pos.WDEQ_RISP_TS_NULL, wdeqRispTsNull, Len.WDEQ_RISP_TS_NULL);
    }

    /**Original name: WDEQ-RISP-TS-NULL<br>*/
    public String getWdeqRispTsNull() {
        return readString(Pos.WDEQ_RISP_TS_NULL, Len.WDEQ_RISP_TS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDEQ_RISP_TS = 1;
        public static final int WDEQ_RISP_TS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDEQ_RISP_TS = 8;
        public static final int WDEQ_RISP_TS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDEQ_RISP_TS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDEQ_RISP_TS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
