package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-TAB<br>
 * Variable: WTGA-TAB from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaTabLlbs0230 extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_TRAN_MAXOCCURS = 1250;
    public static final char WTGA_ST_ADD = 'A';
    public static final char WTGA_ST_MOD = 'M';
    public static final char WTGA_ST_INV = 'I';
    public static final char WTGA_ST_DEL = 'D';
    public static final char WTGA_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public WtgaTabLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_TAB;
    }

    public String getWtgaTabFormatted() {
        return readFixedString(Pos.WTGA_TAB, Len.WTGA_TAB);
    }

    public byte[] getWtgaTabBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_TAB, Pos.WTGA_TAB);
        return buffer;
    }

    /**Original name: WTGA-TAB-TRAN<br>
	 * <pre>----------------------------------------------------------------*
	 *  IMENSIONAMENTO OCCURS PER LA TAB. TRCH_DI_GAR
	 *  FUNZIONALITÀ SETTATA PER LA CASISTICA 5
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getTabTranBytes(int tabTranIdx) {
        byte[] buffer = new byte[Len.TAB_TRAN];
        return getTabTranBytes(tabTranIdx, buffer, 1);
    }

    public byte[] getTabTranBytes(int tabTranIdx, byte[] buffer, int offset) {
        int position = Pos.wtgaTabTran(tabTranIdx - 1);
        getBytes(buffer, offset, Len.TAB_TRAN, position);
        return buffer;
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.wtgaStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: WTGA-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA TRCH_DI_GAR
	 *    ALIAS TGA
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.wtgaStatus(statusIdx - 1);
        return readChar(position);
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.wtgaIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: WTGA-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.wtgaIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdTrchDiGar(int idTrchDiGarIdx, int idTrchDiGar) {
        int position = Pos.wtgaIdTrchDiGar(idTrchDiGarIdx - 1);
        writeIntAsPacked(position, idTrchDiGar, Len.Int.ID_TRCH_DI_GAR);
    }

    /**Original name: WTGA-ID-TRCH-DI-GAR<br>*/
    public int getIdTrchDiGar(int idTrchDiGarIdx) {
        int position = Pos.wtgaIdTrchDiGar(idTrchDiGarIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_TRCH_DI_GAR);
    }

    public void setIdGar(int idGarIdx, int idGar) {
        int position = Pos.wtgaIdGar(idGarIdx - 1);
        writeIntAsPacked(position, idGar, Len.Int.ID_GAR);
    }

    /**Original name: WTGA-ID-GAR<br>*/
    public int getIdGar(int idGarIdx) {
        int position = Pos.wtgaIdGar(idGarIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_GAR);
    }

    public void setIdAdes(int idAdesIdx, int idAdes) {
        int position = Pos.wtgaIdAdes(idAdesIdx - 1);
        writeIntAsPacked(position, idAdes, Len.Int.ID_ADES);
    }

    /**Original name: WTGA-ID-ADES<br>*/
    public int getIdAdes(int idAdesIdx) {
        int position = Pos.wtgaIdAdes(idAdesIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_ADES);
    }

    public void setIdPoli(int idPoliIdx, int idPoli) {
        int position = Pos.wtgaIdPoli(idPoliIdx - 1);
        writeIntAsPacked(position, idPoli, Len.Int.ID_POLI);
    }

    /**Original name: WTGA-ID-POLI<br>*/
    public int getIdPoli(int idPoliIdx) {
        int position = Pos.wtgaIdPoli(idPoliIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_POLI);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.wtgaIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: WTGA-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.wtgaIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.wtgaIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: WTGA-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.wtgaIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setIdMoviChiuNull(int idMoviChiuNullIdx, String idMoviChiuNull) {
        int position = Pos.wtgaIdMoviChiuNull(idMoviChiuNullIdx - 1);
        writeString(position, idMoviChiuNull, Len.ID_MOVI_CHIU_NULL);
    }

    /**Original name: WTGA-ID-MOVI-CHIU-NULL<br>*/
    public String getIdMoviChiuNull(int idMoviChiuNullIdx) {
        int position = Pos.wtgaIdMoviChiuNull(idMoviChiuNullIdx - 1);
        return readString(position, Len.ID_MOVI_CHIU_NULL);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.wtgaDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: WTGA-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.wtgaDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.wtgaDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: WTGA-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.wtgaDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.wtgaCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: WTGA-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wtgaCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setDtDecor(int dtDecorIdx, int dtDecor) {
        int position = Pos.wtgaDtDecor(dtDecorIdx - 1);
        writeIntAsPacked(position, dtDecor, Len.Int.DT_DECOR);
    }

    /**Original name: WTGA-DT-DECOR<br>*/
    public int getDtDecor(int dtDecorIdx) {
        int position = Pos.wtgaDtDecor(dtDecorIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_DECOR);
    }

    public void setDtScad(int dtScadIdx, int dtScad) {
        int position = Pos.wtgaDtScad(dtScadIdx - 1);
        writeIntAsPacked(position, dtScad, Len.Int.DT_SCAD);
    }

    /**Original name: WTGA-DT-SCAD<br>*/
    public int getDtScad(int dtScadIdx) {
        int position = Pos.wtgaDtScad(dtScadIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_SCAD);
    }

    public void setDtScadNull(int dtScadNullIdx, String dtScadNull) {
        int position = Pos.wtgaDtScadNull(dtScadNullIdx - 1);
        writeString(position, dtScadNull, Len.DT_SCAD_NULL);
    }

    /**Original name: WTGA-DT-SCAD-NULL<br>*/
    public String getDtScadNull(int dtScadNullIdx) {
        int position = Pos.wtgaDtScadNull(dtScadNullIdx - 1);
        return readString(position, Len.DT_SCAD_NULL);
    }

    public String getDtScadNullFormatted(int dtScadNullIdx) {
        return Functions.padBlanks(getDtScadNull(dtScadNullIdx), Len.DT_SCAD_NULL);
    }

    public void setIbOgg(int ibOggIdx, String ibOgg) {
        int position = Pos.wtgaIbOgg(ibOggIdx - 1);
        writeString(position, ibOgg, Len.IB_OGG);
    }

    /**Original name: WTGA-IB-OGG<br>*/
    public String getIbOgg(int ibOggIdx) {
        int position = Pos.wtgaIbOgg(ibOggIdx - 1);
        return readString(position, Len.IB_OGG);
    }

    public void setIbOggNull(int ibOggNullIdx, String ibOggNull) {
        int position = Pos.wtgaIbOggNull(ibOggNullIdx - 1);
        writeString(position, ibOggNull, Len.IB_OGG_NULL);
    }

    /**Original name: WTGA-IB-OGG-NULL<br>*/
    public String getIbOggNull(int ibOggNullIdx) {
        int position = Pos.wtgaIbOggNull(ibOggNullIdx - 1);
        return readString(position, Len.IB_OGG_NULL);
    }

    public void setTpRgmFisc(int tpRgmFiscIdx, String tpRgmFisc) {
        int position = Pos.wtgaTpRgmFisc(tpRgmFiscIdx - 1);
        writeString(position, tpRgmFisc, Len.TP_RGM_FISC);
    }

    /**Original name: WTGA-TP-RGM-FISC<br>*/
    public String getTpRgmFisc(int tpRgmFiscIdx) {
        int position = Pos.wtgaTpRgmFisc(tpRgmFiscIdx - 1);
        return readString(position, Len.TP_RGM_FISC);
    }

    public String getTpRgmFiscFormatted(int tpRgmFiscIdx) {
        return Functions.padBlanks(getTpRgmFisc(tpRgmFiscIdx), Len.TP_RGM_FISC);
    }

    public void setDtEmis(int dtEmisIdx, int dtEmis) {
        int position = Pos.wtgaDtEmis(dtEmisIdx - 1);
        writeIntAsPacked(position, dtEmis, Len.Int.DT_EMIS);
    }

    /**Original name: WTGA-DT-EMIS<br>*/
    public int getDtEmis(int dtEmisIdx) {
        int position = Pos.wtgaDtEmis(dtEmisIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_EMIS);
    }

    public void setDtEmisNull(int dtEmisNullIdx, String dtEmisNull) {
        int position = Pos.wtgaDtEmisNull(dtEmisNullIdx - 1);
        writeString(position, dtEmisNull, Len.DT_EMIS_NULL);
    }

    /**Original name: WTGA-DT-EMIS-NULL<br>*/
    public String getDtEmisNull(int dtEmisNullIdx) {
        int position = Pos.wtgaDtEmisNull(dtEmisNullIdx - 1);
        return readString(position, Len.DT_EMIS_NULL);
    }

    public void setTpTrch(int tpTrchIdx, String tpTrch) {
        int position = Pos.wtgaTpTrch(tpTrchIdx - 1);
        writeString(position, tpTrch, Len.TP_TRCH);
    }

    /**Original name: WTGA-TP-TRCH<br>*/
    public String getTpTrch(int tpTrchIdx) {
        int position = Pos.wtgaTpTrch(tpTrchIdx - 1);
        return readString(position, Len.TP_TRCH);
    }

    public void setDurAa(int durAaIdx, int durAa) {
        int position = Pos.wtgaDurAa(durAaIdx - 1);
        writeIntAsPacked(position, durAa, Len.Int.DUR_AA);
    }

    /**Original name: WTGA-DUR-AA<br>*/
    public int getDurAa(int durAaIdx) {
        int position = Pos.wtgaDurAa(durAaIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_AA);
    }

    public void setDurAaNull(int durAaNullIdx, String durAaNull) {
        int position = Pos.wtgaDurAaNull(durAaNullIdx - 1);
        writeString(position, durAaNull, Len.DUR_AA_NULL);
    }

    /**Original name: WTGA-DUR-AA-NULL<br>*/
    public String getDurAaNull(int durAaNullIdx) {
        int position = Pos.wtgaDurAaNull(durAaNullIdx - 1);
        return readString(position, Len.DUR_AA_NULL);
    }

    public void setDurMm(int durMmIdx, int durMm) {
        int position = Pos.wtgaDurMm(durMmIdx - 1);
        writeIntAsPacked(position, durMm, Len.Int.DUR_MM);
    }

    /**Original name: WTGA-DUR-MM<br>*/
    public int getDurMm(int durMmIdx) {
        int position = Pos.wtgaDurMm(durMmIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_MM);
    }

    public void setDurMmNull(int durMmNullIdx, String durMmNull) {
        int position = Pos.wtgaDurMmNull(durMmNullIdx - 1);
        writeString(position, durMmNull, Len.DUR_MM_NULL);
    }

    /**Original name: WTGA-DUR-MM-NULL<br>*/
    public String getDurMmNull(int durMmNullIdx) {
        int position = Pos.wtgaDurMmNull(durMmNullIdx - 1);
        return readString(position, Len.DUR_MM_NULL);
    }

    public void setDurGg(int durGgIdx, int durGg) {
        int position = Pos.wtgaDurGg(durGgIdx - 1);
        writeIntAsPacked(position, durGg, Len.Int.DUR_GG);
    }

    /**Original name: WTGA-DUR-GG<br>*/
    public int getDurGg(int durGgIdx) {
        int position = Pos.wtgaDurGg(durGgIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_GG);
    }

    public void setDurGgNull(int durGgNullIdx, String durGgNull) {
        int position = Pos.wtgaDurGgNull(durGgNullIdx - 1);
        writeString(position, durGgNull, Len.DUR_GG_NULL);
    }

    /**Original name: WTGA-DUR-GG-NULL<br>*/
    public String getDurGgNull(int durGgNullIdx) {
        int position = Pos.wtgaDurGgNull(durGgNullIdx - 1);
        return readString(position, Len.DUR_GG_NULL);
    }

    public void setPreCasoMor(int preCasoMorIdx, AfDecimal preCasoMor) {
        int position = Pos.wtgaPreCasoMor(preCasoMorIdx - 1);
        writeDecimalAsPacked(position, preCasoMor.copy());
    }

    /**Original name: WTGA-PRE-CASO-MOR<br>*/
    public AfDecimal getPreCasoMor(int preCasoMorIdx) {
        int position = Pos.wtgaPreCasoMor(preCasoMorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_CASO_MOR, Len.Fract.PRE_CASO_MOR);
    }

    public void setPreCasoMorNull(int preCasoMorNullIdx, String preCasoMorNull) {
        int position = Pos.wtgaPreCasoMorNull(preCasoMorNullIdx - 1);
        writeString(position, preCasoMorNull, Len.PRE_CASO_MOR_NULL);
    }

    /**Original name: WTGA-PRE-CASO-MOR-NULL<br>*/
    public String getPreCasoMorNull(int preCasoMorNullIdx) {
        int position = Pos.wtgaPreCasoMorNull(preCasoMorNullIdx - 1);
        return readString(position, Len.PRE_CASO_MOR_NULL);
    }

    public void setPcIntrRiat(int pcIntrRiatIdx, AfDecimal pcIntrRiat) {
        int position = Pos.wtgaPcIntrRiat(pcIntrRiatIdx - 1);
        writeDecimalAsPacked(position, pcIntrRiat.copy());
    }

    /**Original name: WTGA-PC-INTR-RIAT<br>*/
    public AfDecimal getPcIntrRiat(int pcIntrRiatIdx) {
        int position = Pos.wtgaPcIntrRiat(pcIntrRiatIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_INTR_RIAT, Len.Fract.PC_INTR_RIAT);
    }

    public void setPcIntrRiatNull(int pcIntrRiatNullIdx, String pcIntrRiatNull) {
        int position = Pos.wtgaPcIntrRiatNull(pcIntrRiatNullIdx - 1);
        writeString(position, pcIntrRiatNull, Len.PC_INTR_RIAT_NULL);
    }

    /**Original name: WTGA-PC-INTR-RIAT-NULL<br>*/
    public String getPcIntrRiatNull(int pcIntrRiatNullIdx) {
        int position = Pos.wtgaPcIntrRiatNull(pcIntrRiatNullIdx - 1);
        return readString(position, Len.PC_INTR_RIAT_NULL);
    }

    public void setImpBnsAntic(int impBnsAnticIdx, AfDecimal impBnsAntic) {
        int position = Pos.wtgaImpBnsAntic(impBnsAnticIdx - 1);
        writeDecimalAsPacked(position, impBnsAntic.copy());
    }

    /**Original name: WTGA-IMP-BNS-ANTIC<br>*/
    public AfDecimal getImpBnsAntic(int impBnsAnticIdx) {
        int position = Pos.wtgaImpBnsAntic(impBnsAnticIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_BNS_ANTIC, Len.Fract.IMP_BNS_ANTIC);
    }

    public void setImpBnsAnticNull(int impBnsAnticNullIdx, String impBnsAnticNull) {
        int position = Pos.wtgaImpBnsAnticNull(impBnsAnticNullIdx - 1);
        writeString(position, impBnsAnticNull, Len.IMP_BNS_ANTIC_NULL);
    }

    /**Original name: WTGA-IMP-BNS-ANTIC-NULL<br>*/
    public String getImpBnsAnticNull(int impBnsAnticNullIdx) {
        int position = Pos.wtgaImpBnsAnticNull(impBnsAnticNullIdx - 1);
        return readString(position, Len.IMP_BNS_ANTIC_NULL);
    }

    public void setPreIniNet(int preIniNetIdx, AfDecimal preIniNet) {
        int position = Pos.wtgaPreIniNet(preIniNetIdx - 1);
        writeDecimalAsPacked(position, preIniNet.copy());
    }

    /**Original name: WTGA-PRE-INI-NET<br>*/
    public AfDecimal getPreIniNet(int preIniNetIdx) {
        int position = Pos.wtgaPreIniNet(preIniNetIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_INI_NET, Len.Fract.PRE_INI_NET);
    }

    public void setPreIniNetNull(int preIniNetNullIdx, String preIniNetNull) {
        int position = Pos.wtgaPreIniNetNull(preIniNetNullIdx - 1);
        writeString(position, preIniNetNull, Len.PRE_INI_NET_NULL);
    }

    /**Original name: WTGA-PRE-INI-NET-NULL<br>*/
    public String getPreIniNetNull(int preIniNetNullIdx) {
        int position = Pos.wtgaPreIniNetNull(preIniNetNullIdx - 1);
        return readString(position, Len.PRE_INI_NET_NULL);
    }

    public String getPreIniNetNullFormatted(int preIniNetNullIdx) {
        return Functions.padBlanks(getPreIniNetNull(preIniNetNullIdx), Len.PRE_INI_NET_NULL);
    }

    public void setPrePpIni(int prePpIniIdx, AfDecimal prePpIni) {
        int position = Pos.wtgaPrePpIni(prePpIniIdx - 1);
        writeDecimalAsPacked(position, prePpIni.copy());
    }

    /**Original name: WTGA-PRE-PP-INI<br>*/
    public AfDecimal getPrePpIni(int prePpIniIdx) {
        int position = Pos.wtgaPrePpIni(prePpIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_PP_INI, Len.Fract.PRE_PP_INI);
    }

    public void setPrePpIniNull(int prePpIniNullIdx, String prePpIniNull) {
        int position = Pos.wtgaPrePpIniNull(prePpIniNullIdx - 1);
        writeString(position, prePpIniNull, Len.PRE_PP_INI_NULL);
    }

    /**Original name: WTGA-PRE-PP-INI-NULL<br>*/
    public String getPrePpIniNull(int prePpIniNullIdx) {
        int position = Pos.wtgaPrePpIniNull(prePpIniNullIdx - 1);
        return readString(position, Len.PRE_PP_INI_NULL);
    }

    public String getPrePpIniNullFormatted(int prePpIniNullIdx) {
        return Functions.padBlanks(getPrePpIniNull(prePpIniNullIdx), Len.PRE_PP_INI_NULL);
    }

    public void setPrePpUlt(int prePpUltIdx, AfDecimal prePpUlt) {
        int position = Pos.wtgaPrePpUlt(prePpUltIdx - 1);
        writeDecimalAsPacked(position, prePpUlt.copy());
    }

    /**Original name: WTGA-PRE-PP-ULT<br>*/
    public AfDecimal getPrePpUlt(int prePpUltIdx) {
        int position = Pos.wtgaPrePpUlt(prePpUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_PP_ULT, Len.Fract.PRE_PP_ULT);
    }

    public void setPrePpUltNull(int prePpUltNullIdx, String prePpUltNull) {
        int position = Pos.wtgaPrePpUltNull(prePpUltNullIdx - 1);
        writeString(position, prePpUltNull, Len.PRE_PP_ULT_NULL);
    }

    /**Original name: WTGA-PRE-PP-ULT-NULL<br>*/
    public String getPrePpUltNull(int prePpUltNullIdx) {
        int position = Pos.wtgaPrePpUltNull(prePpUltNullIdx - 1);
        return readString(position, Len.PRE_PP_ULT_NULL);
    }

    public String getPrePpUltNullFormatted(int prePpUltNullIdx) {
        return Functions.padBlanks(getPrePpUltNull(prePpUltNullIdx), Len.PRE_PP_ULT_NULL);
    }

    public void setPreTariIni(int preTariIniIdx, AfDecimal preTariIni) {
        int position = Pos.wtgaPreTariIni(preTariIniIdx - 1);
        writeDecimalAsPacked(position, preTariIni.copy());
    }

    /**Original name: WTGA-PRE-TARI-INI<br>*/
    public AfDecimal getPreTariIni(int preTariIniIdx) {
        int position = Pos.wtgaPreTariIni(preTariIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_TARI_INI, Len.Fract.PRE_TARI_INI);
    }

    public void setPreTariIniNull(int preTariIniNullIdx, String preTariIniNull) {
        int position = Pos.wtgaPreTariIniNull(preTariIniNullIdx - 1);
        writeString(position, preTariIniNull, Len.PRE_TARI_INI_NULL);
    }

    /**Original name: WTGA-PRE-TARI-INI-NULL<br>*/
    public String getPreTariIniNull(int preTariIniNullIdx) {
        int position = Pos.wtgaPreTariIniNull(preTariIniNullIdx - 1);
        return readString(position, Len.PRE_TARI_INI_NULL);
    }

    public void setPreTariUlt(int preTariUltIdx, AfDecimal preTariUlt) {
        int position = Pos.wtgaPreTariUlt(preTariUltIdx - 1);
        writeDecimalAsPacked(position, preTariUlt.copy());
    }

    /**Original name: WTGA-PRE-TARI-ULT<br>*/
    public AfDecimal getPreTariUlt(int preTariUltIdx) {
        int position = Pos.wtgaPreTariUlt(preTariUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_TARI_ULT, Len.Fract.PRE_TARI_ULT);
    }

    public void setPreTariUltNull(int preTariUltNullIdx, String preTariUltNull) {
        int position = Pos.wtgaPreTariUltNull(preTariUltNullIdx - 1);
        writeString(position, preTariUltNull, Len.PRE_TARI_ULT_NULL);
    }

    /**Original name: WTGA-PRE-TARI-ULT-NULL<br>*/
    public String getPreTariUltNull(int preTariUltNullIdx) {
        int position = Pos.wtgaPreTariUltNull(preTariUltNullIdx - 1);
        return readString(position, Len.PRE_TARI_ULT_NULL);
    }

    public void setPreInvrioIni(int preInvrioIniIdx, AfDecimal preInvrioIni) {
        int position = Pos.wtgaPreInvrioIni(preInvrioIniIdx - 1);
        writeDecimalAsPacked(position, preInvrioIni.copy());
    }

    /**Original name: WTGA-PRE-INVRIO-INI<br>*/
    public AfDecimal getPreInvrioIni(int preInvrioIniIdx) {
        int position = Pos.wtgaPreInvrioIni(preInvrioIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_INVRIO_INI, Len.Fract.PRE_INVRIO_INI);
    }

    public void setPreInvrioIniNull(int preInvrioIniNullIdx, String preInvrioIniNull) {
        int position = Pos.wtgaPreInvrioIniNull(preInvrioIniNullIdx - 1);
        writeString(position, preInvrioIniNull, Len.PRE_INVRIO_INI_NULL);
    }

    /**Original name: WTGA-PRE-INVRIO-INI-NULL<br>*/
    public String getPreInvrioIniNull(int preInvrioIniNullIdx) {
        int position = Pos.wtgaPreInvrioIniNull(preInvrioIniNullIdx - 1);
        return readString(position, Len.PRE_INVRIO_INI_NULL);
    }

    public void setPreInvrioUlt(int preInvrioUltIdx, AfDecimal preInvrioUlt) {
        int position = Pos.wtgaPreInvrioUlt(preInvrioUltIdx - 1);
        writeDecimalAsPacked(position, preInvrioUlt.copy());
    }

    /**Original name: WTGA-PRE-INVRIO-ULT<br>*/
    public AfDecimal getPreInvrioUlt(int preInvrioUltIdx) {
        int position = Pos.wtgaPreInvrioUlt(preInvrioUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_INVRIO_ULT, Len.Fract.PRE_INVRIO_ULT);
    }

    public void setPreInvrioUltNull(int preInvrioUltNullIdx, String preInvrioUltNull) {
        int position = Pos.wtgaPreInvrioUltNull(preInvrioUltNullIdx - 1);
        writeString(position, preInvrioUltNull, Len.PRE_INVRIO_ULT_NULL);
    }

    /**Original name: WTGA-PRE-INVRIO-ULT-NULL<br>*/
    public String getPreInvrioUltNull(int preInvrioUltNullIdx) {
        int position = Pos.wtgaPreInvrioUltNull(preInvrioUltNullIdx - 1);
        return readString(position, Len.PRE_INVRIO_ULT_NULL);
    }

    public void setPreRivto(int preRivtoIdx, AfDecimal preRivto) {
        int position = Pos.wtgaPreRivto(preRivtoIdx - 1);
        writeDecimalAsPacked(position, preRivto.copy());
    }

    /**Original name: WTGA-PRE-RIVTO<br>*/
    public AfDecimal getPreRivto(int preRivtoIdx) {
        int position = Pos.wtgaPreRivto(preRivtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_RIVTO, Len.Fract.PRE_RIVTO);
    }

    public void setPreRivtoNull(int preRivtoNullIdx, String preRivtoNull) {
        int position = Pos.wtgaPreRivtoNull(preRivtoNullIdx - 1);
        writeString(position, preRivtoNull, Len.PRE_RIVTO_NULL);
    }

    /**Original name: WTGA-PRE-RIVTO-NULL<br>*/
    public String getPreRivtoNull(int preRivtoNullIdx) {
        int position = Pos.wtgaPreRivtoNull(preRivtoNullIdx - 1);
        return readString(position, Len.PRE_RIVTO_NULL);
    }

    public void setImpSoprProf(int impSoprProfIdx, AfDecimal impSoprProf) {
        int position = Pos.wtgaImpSoprProf(impSoprProfIdx - 1);
        writeDecimalAsPacked(position, impSoprProf.copy());
    }

    /**Original name: WTGA-IMP-SOPR-PROF<br>*/
    public AfDecimal getImpSoprProf(int impSoprProfIdx) {
        int position = Pos.wtgaImpSoprProf(impSoprProfIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_PROF, Len.Fract.IMP_SOPR_PROF);
    }

    public void setImpSoprProfNull(int impSoprProfNullIdx, String impSoprProfNull) {
        int position = Pos.wtgaImpSoprProfNull(impSoprProfNullIdx - 1);
        writeString(position, impSoprProfNull, Len.IMP_SOPR_PROF_NULL);
    }

    /**Original name: WTGA-IMP-SOPR-PROF-NULL<br>*/
    public String getImpSoprProfNull(int impSoprProfNullIdx) {
        int position = Pos.wtgaImpSoprProfNull(impSoprProfNullIdx - 1);
        return readString(position, Len.IMP_SOPR_PROF_NULL);
    }

    public void setImpSoprSan(int impSoprSanIdx, AfDecimal impSoprSan) {
        int position = Pos.wtgaImpSoprSan(impSoprSanIdx - 1);
        writeDecimalAsPacked(position, impSoprSan.copy());
    }

    /**Original name: WTGA-IMP-SOPR-SAN<br>*/
    public AfDecimal getImpSoprSan(int impSoprSanIdx) {
        int position = Pos.wtgaImpSoprSan(impSoprSanIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_SAN, Len.Fract.IMP_SOPR_SAN);
    }

    public void setImpSoprSanNull(int impSoprSanNullIdx, String impSoprSanNull) {
        int position = Pos.wtgaImpSoprSanNull(impSoprSanNullIdx - 1);
        writeString(position, impSoprSanNull, Len.IMP_SOPR_SAN_NULL);
    }

    /**Original name: WTGA-IMP-SOPR-SAN-NULL<br>*/
    public String getImpSoprSanNull(int impSoprSanNullIdx) {
        int position = Pos.wtgaImpSoprSanNull(impSoprSanNullIdx - 1);
        return readString(position, Len.IMP_SOPR_SAN_NULL);
    }

    public void setImpSoprSpo(int impSoprSpoIdx, AfDecimal impSoprSpo) {
        int position = Pos.wtgaImpSoprSpo(impSoprSpoIdx - 1);
        writeDecimalAsPacked(position, impSoprSpo.copy());
    }

    /**Original name: WTGA-IMP-SOPR-SPO<br>*/
    public AfDecimal getImpSoprSpo(int impSoprSpoIdx) {
        int position = Pos.wtgaImpSoprSpo(impSoprSpoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_SPO, Len.Fract.IMP_SOPR_SPO);
    }

    public void setImpSoprSpoNull(int impSoprSpoNullIdx, String impSoprSpoNull) {
        int position = Pos.wtgaImpSoprSpoNull(impSoprSpoNullIdx - 1);
        writeString(position, impSoprSpoNull, Len.IMP_SOPR_SPO_NULL);
    }

    /**Original name: WTGA-IMP-SOPR-SPO-NULL<br>*/
    public String getImpSoprSpoNull(int impSoprSpoNullIdx) {
        int position = Pos.wtgaImpSoprSpoNull(impSoprSpoNullIdx - 1);
        return readString(position, Len.IMP_SOPR_SPO_NULL);
    }

    public void setImpSoprTec(int impSoprTecIdx, AfDecimal impSoprTec) {
        int position = Pos.wtgaImpSoprTec(impSoprTecIdx - 1);
        writeDecimalAsPacked(position, impSoprTec.copy());
    }

    /**Original name: WTGA-IMP-SOPR-TEC<br>*/
    public AfDecimal getImpSoprTec(int impSoprTecIdx) {
        int position = Pos.wtgaImpSoprTec(impSoprTecIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_TEC, Len.Fract.IMP_SOPR_TEC);
    }

    public void setImpSoprTecNull(int impSoprTecNullIdx, String impSoprTecNull) {
        int position = Pos.wtgaImpSoprTecNull(impSoprTecNullIdx - 1);
        writeString(position, impSoprTecNull, Len.IMP_SOPR_TEC_NULL);
    }

    /**Original name: WTGA-IMP-SOPR-TEC-NULL<br>*/
    public String getImpSoprTecNull(int impSoprTecNullIdx) {
        int position = Pos.wtgaImpSoprTecNull(impSoprTecNullIdx - 1);
        return readString(position, Len.IMP_SOPR_TEC_NULL);
    }

    public void setImpAltSopr(int impAltSoprIdx, AfDecimal impAltSopr) {
        int position = Pos.wtgaImpAltSopr(impAltSoprIdx - 1);
        writeDecimalAsPacked(position, impAltSopr.copy());
    }

    /**Original name: WTGA-IMP-ALT-SOPR<br>*/
    public AfDecimal getImpAltSopr(int impAltSoprIdx) {
        int position = Pos.wtgaImpAltSopr(impAltSoprIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_ALT_SOPR, Len.Fract.IMP_ALT_SOPR);
    }

    public void setImpAltSoprNull(int impAltSoprNullIdx, String impAltSoprNull) {
        int position = Pos.wtgaImpAltSoprNull(impAltSoprNullIdx - 1);
        writeString(position, impAltSoprNull, Len.IMP_ALT_SOPR_NULL);
    }

    /**Original name: WTGA-IMP-ALT-SOPR-NULL<br>*/
    public String getImpAltSoprNull(int impAltSoprNullIdx) {
        int position = Pos.wtgaImpAltSoprNull(impAltSoprNullIdx - 1);
        return readString(position, Len.IMP_ALT_SOPR_NULL);
    }

    public void setPreStab(int preStabIdx, AfDecimal preStab) {
        int position = Pos.wtgaPreStab(preStabIdx - 1);
        writeDecimalAsPacked(position, preStab.copy());
    }

    /**Original name: WTGA-PRE-STAB<br>*/
    public AfDecimal getPreStab(int preStabIdx) {
        int position = Pos.wtgaPreStab(preStabIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_STAB, Len.Fract.PRE_STAB);
    }

    public void setPreStabNull(int preStabNullIdx, String preStabNull) {
        int position = Pos.wtgaPreStabNull(preStabNullIdx - 1);
        writeString(position, preStabNull, Len.PRE_STAB_NULL);
    }

    /**Original name: WTGA-PRE-STAB-NULL<br>*/
    public String getPreStabNull(int preStabNullIdx) {
        int position = Pos.wtgaPreStabNull(preStabNullIdx - 1);
        return readString(position, Len.PRE_STAB_NULL);
    }

    public void setDtEffStab(int dtEffStabIdx, int dtEffStab) {
        int position = Pos.wtgaDtEffStab(dtEffStabIdx - 1);
        writeIntAsPacked(position, dtEffStab, Len.Int.DT_EFF_STAB);
    }

    /**Original name: WTGA-DT-EFF-STAB<br>*/
    public int getDtEffStab(int dtEffStabIdx) {
        int position = Pos.wtgaDtEffStab(dtEffStabIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_EFF_STAB);
    }

    public void setDtEffStabNull(int dtEffStabNullIdx, String dtEffStabNull) {
        int position = Pos.wtgaDtEffStabNull(dtEffStabNullIdx - 1);
        writeString(position, dtEffStabNull, Len.DT_EFF_STAB_NULL);
    }

    /**Original name: WTGA-DT-EFF-STAB-NULL<br>*/
    public String getDtEffStabNull(int dtEffStabNullIdx) {
        int position = Pos.wtgaDtEffStabNull(dtEffStabNullIdx - 1);
        return readString(position, Len.DT_EFF_STAB_NULL);
    }

    public String getDtEffStabNullFormatted(int dtEffStabNullIdx) {
        return Functions.padBlanks(getDtEffStabNull(dtEffStabNullIdx), Len.DT_EFF_STAB_NULL);
    }

    public void setTsRivalFis(int tsRivalFisIdx, AfDecimal tsRivalFis) {
        int position = Pos.wtgaTsRivalFis(tsRivalFisIdx - 1);
        writeDecimalAsPacked(position, tsRivalFis.copy());
    }

    /**Original name: WTGA-TS-RIVAL-FIS<br>*/
    public AfDecimal getTsRivalFis(int tsRivalFisIdx) {
        int position = Pos.wtgaTsRivalFis(tsRivalFisIdx - 1);
        return readPackedAsDecimal(position, Len.Int.TS_RIVAL_FIS, Len.Fract.TS_RIVAL_FIS);
    }

    public void setTsRivalFisNull(int tsRivalFisNullIdx, String tsRivalFisNull) {
        int position = Pos.wtgaTsRivalFisNull(tsRivalFisNullIdx - 1);
        writeString(position, tsRivalFisNull, Len.TS_RIVAL_FIS_NULL);
    }

    /**Original name: WTGA-TS-RIVAL-FIS-NULL<br>*/
    public String getTsRivalFisNull(int tsRivalFisNullIdx) {
        int position = Pos.wtgaTsRivalFisNull(tsRivalFisNullIdx - 1);
        return readString(position, Len.TS_RIVAL_FIS_NULL);
    }

    public void setTsRivalIndiciz(int tsRivalIndicizIdx, AfDecimal tsRivalIndiciz) {
        int position = Pos.wtgaTsRivalIndiciz(tsRivalIndicizIdx - 1);
        writeDecimalAsPacked(position, tsRivalIndiciz.copy());
    }

    /**Original name: WTGA-TS-RIVAL-INDICIZ<br>*/
    public AfDecimal getTsRivalIndiciz(int tsRivalIndicizIdx) {
        int position = Pos.wtgaTsRivalIndiciz(tsRivalIndicizIdx - 1);
        return readPackedAsDecimal(position, Len.Int.TS_RIVAL_INDICIZ, Len.Fract.TS_RIVAL_INDICIZ);
    }

    public void setTsRivalIndicizNull(int tsRivalIndicizNullIdx, String tsRivalIndicizNull) {
        int position = Pos.wtgaTsRivalIndicizNull(tsRivalIndicizNullIdx - 1);
        writeString(position, tsRivalIndicizNull, Len.TS_RIVAL_INDICIZ_NULL);
    }

    /**Original name: WTGA-TS-RIVAL-INDICIZ-NULL<br>*/
    public String getTsRivalIndicizNull(int tsRivalIndicizNullIdx) {
        int position = Pos.wtgaTsRivalIndicizNull(tsRivalIndicizNullIdx - 1);
        return readString(position, Len.TS_RIVAL_INDICIZ_NULL);
    }

    public void setOldTsTec(int oldTsTecIdx, AfDecimal oldTsTec) {
        int position = Pos.wtgaOldTsTec(oldTsTecIdx - 1);
        writeDecimalAsPacked(position, oldTsTec.copy());
    }

    /**Original name: WTGA-OLD-TS-TEC<br>*/
    public AfDecimal getOldTsTec(int oldTsTecIdx) {
        int position = Pos.wtgaOldTsTec(oldTsTecIdx - 1);
        return readPackedAsDecimal(position, Len.Int.OLD_TS_TEC, Len.Fract.OLD_TS_TEC);
    }

    public void setOldTsTecNull(int oldTsTecNullIdx, String oldTsTecNull) {
        int position = Pos.wtgaOldTsTecNull(oldTsTecNullIdx - 1);
        writeString(position, oldTsTecNull, Len.OLD_TS_TEC_NULL);
    }

    /**Original name: WTGA-OLD-TS-TEC-NULL<br>*/
    public String getOldTsTecNull(int oldTsTecNullIdx) {
        int position = Pos.wtgaOldTsTecNull(oldTsTecNullIdx - 1);
        return readString(position, Len.OLD_TS_TEC_NULL);
    }

    public void setRatLrd(int ratLrdIdx, AfDecimal ratLrd) {
        int position = Pos.wtgaRatLrd(ratLrdIdx - 1);
        writeDecimalAsPacked(position, ratLrd.copy());
    }

    /**Original name: WTGA-RAT-LRD<br>*/
    public AfDecimal getRatLrd(int ratLrdIdx) {
        int position = Pos.wtgaRatLrd(ratLrdIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RAT_LRD, Len.Fract.RAT_LRD);
    }

    public void setRatLrdNull(int ratLrdNullIdx, String ratLrdNull) {
        int position = Pos.wtgaRatLrdNull(ratLrdNullIdx - 1);
        writeString(position, ratLrdNull, Len.RAT_LRD_NULL);
    }

    /**Original name: WTGA-RAT-LRD-NULL<br>*/
    public String getRatLrdNull(int ratLrdNullIdx) {
        int position = Pos.wtgaRatLrdNull(ratLrdNullIdx - 1);
        return readString(position, Len.RAT_LRD_NULL);
    }

    public void setPreLrd(int preLrdIdx, AfDecimal preLrd) {
        int position = Pos.wtgaPreLrd(preLrdIdx - 1);
        writeDecimalAsPacked(position, preLrd.copy());
    }

    /**Original name: WTGA-PRE-LRD<br>*/
    public AfDecimal getPreLrd(int preLrdIdx) {
        int position = Pos.wtgaPreLrd(preLrdIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_LRD, Len.Fract.PRE_LRD);
    }

    public void setPreLrdNull(int preLrdNullIdx, String preLrdNull) {
        int position = Pos.wtgaPreLrdNull(preLrdNullIdx - 1);
        writeString(position, preLrdNull, Len.PRE_LRD_NULL);
    }

    /**Original name: WTGA-PRE-LRD-NULL<br>*/
    public String getPreLrdNull(int preLrdNullIdx) {
        int position = Pos.wtgaPreLrdNull(preLrdNullIdx - 1);
        return readString(position, Len.PRE_LRD_NULL);
    }

    public String getPreLrdNullFormatted(int preLrdNullIdx) {
        return Functions.padBlanks(getPreLrdNull(preLrdNullIdx), Len.PRE_LRD_NULL);
    }

    public void setPrstzIni(int prstzIniIdx, AfDecimal prstzIni) {
        int position = Pos.wtgaPrstzIni(prstzIniIdx - 1);
        writeDecimalAsPacked(position, prstzIni.copy());
    }

    /**Original name: WTGA-PRSTZ-INI<br>*/
    public AfDecimal getPrstzIni(int prstzIniIdx) {
        int position = Pos.wtgaPrstzIni(prstzIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI, Len.Fract.PRSTZ_INI);
    }

    public void setPrstzIniNull(int prstzIniNullIdx, String prstzIniNull) {
        int position = Pos.wtgaPrstzIniNull(prstzIniNullIdx - 1);
        writeString(position, prstzIniNull, Len.PRSTZ_INI_NULL);
    }

    /**Original name: WTGA-PRSTZ-INI-NULL<br>*/
    public String getPrstzIniNull(int prstzIniNullIdx) {
        int position = Pos.wtgaPrstzIniNull(prstzIniNullIdx - 1);
        return readString(position, Len.PRSTZ_INI_NULL);
    }

    public String getPrstzIniNullFormatted(int prstzIniNullIdx) {
        return Functions.padBlanks(getPrstzIniNull(prstzIniNullIdx), Len.PRSTZ_INI_NULL);
    }

    public void setPrstzUlt(int prstzUltIdx, AfDecimal prstzUlt) {
        int position = Pos.wtgaPrstzUlt(prstzUltIdx - 1);
        writeDecimalAsPacked(position, prstzUlt.copy());
    }

    /**Original name: WTGA-PRSTZ-ULT<br>*/
    public AfDecimal getPrstzUlt(int prstzUltIdx) {
        int position = Pos.wtgaPrstzUlt(prstzUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_ULT, Len.Fract.PRSTZ_ULT);
    }

    public void setPrstzUltNull(int prstzUltNullIdx, String prstzUltNull) {
        int position = Pos.wtgaPrstzUltNull(prstzUltNullIdx - 1);
        writeString(position, prstzUltNull, Len.PRSTZ_ULT_NULL);
    }

    /**Original name: WTGA-PRSTZ-ULT-NULL<br>*/
    public String getPrstzUltNull(int prstzUltNullIdx) {
        int position = Pos.wtgaPrstzUltNull(prstzUltNullIdx - 1);
        return readString(position, Len.PRSTZ_ULT_NULL);
    }

    public String getPrstzUltNullFormatted(int prstzUltNullIdx) {
        return Functions.padBlanks(getPrstzUltNull(prstzUltNullIdx), Len.PRSTZ_ULT_NULL);
    }

    public void setCptInOpzRivto(int cptInOpzRivtoIdx, AfDecimal cptInOpzRivto) {
        int position = Pos.wtgaCptInOpzRivto(cptInOpzRivtoIdx - 1);
        writeDecimalAsPacked(position, cptInOpzRivto.copy());
    }

    /**Original name: WTGA-CPT-IN-OPZ-RIVTO<br>*/
    public AfDecimal getCptInOpzRivto(int cptInOpzRivtoIdx) {
        int position = Pos.wtgaCptInOpzRivto(cptInOpzRivtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.CPT_IN_OPZ_RIVTO, Len.Fract.CPT_IN_OPZ_RIVTO);
    }

    public void setCptInOpzRivtoNull(int cptInOpzRivtoNullIdx, String cptInOpzRivtoNull) {
        int position = Pos.wtgaCptInOpzRivtoNull(cptInOpzRivtoNullIdx - 1);
        writeString(position, cptInOpzRivtoNull, Len.CPT_IN_OPZ_RIVTO_NULL);
    }

    /**Original name: WTGA-CPT-IN-OPZ-RIVTO-NULL<br>*/
    public String getCptInOpzRivtoNull(int cptInOpzRivtoNullIdx) {
        int position = Pos.wtgaCptInOpzRivtoNull(cptInOpzRivtoNullIdx - 1);
        return readString(position, Len.CPT_IN_OPZ_RIVTO_NULL);
    }

    public void setPrstzIniStab(int prstzIniStabIdx, AfDecimal prstzIniStab) {
        int position = Pos.wtgaPrstzIniStab(prstzIniStabIdx - 1);
        writeDecimalAsPacked(position, prstzIniStab.copy());
    }

    /**Original name: WTGA-PRSTZ-INI-STAB<br>*/
    public AfDecimal getPrstzIniStab(int prstzIniStabIdx) {
        int position = Pos.wtgaPrstzIniStab(prstzIniStabIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI_STAB, Len.Fract.PRSTZ_INI_STAB);
    }

    public void setPrstzIniStabNull(int prstzIniStabNullIdx, String prstzIniStabNull) {
        int position = Pos.wtgaPrstzIniStabNull(prstzIniStabNullIdx - 1);
        writeString(position, prstzIniStabNull, Len.PRSTZ_INI_STAB_NULL);
    }

    /**Original name: WTGA-PRSTZ-INI-STAB-NULL<br>*/
    public String getPrstzIniStabNull(int prstzIniStabNullIdx) {
        int position = Pos.wtgaPrstzIniStabNull(prstzIniStabNullIdx - 1);
        return readString(position, Len.PRSTZ_INI_STAB_NULL);
    }

    public String getPrstzIniStabNullFormatted(int prstzIniStabNullIdx) {
        return Functions.padBlanks(getPrstzIniStabNull(prstzIniStabNullIdx), Len.PRSTZ_INI_STAB_NULL);
    }

    public void setCptRshMor(int cptRshMorIdx, AfDecimal cptRshMor) {
        int position = Pos.wtgaCptRshMor(cptRshMorIdx - 1);
        writeDecimalAsPacked(position, cptRshMor.copy());
    }

    /**Original name: WTGA-CPT-RSH-MOR<br>*/
    public AfDecimal getCptRshMor(int cptRshMorIdx) {
        int position = Pos.wtgaCptRshMor(cptRshMorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.CPT_RSH_MOR, Len.Fract.CPT_RSH_MOR);
    }

    public void setCptRshMorNull(int cptRshMorNullIdx, String cptRshMorNull) {
        int position = Pos.wtgaCptRshMorNull(cptRshMorNullIdx - 1);
        writeString(position, cptRshMorNull, Len.CPT_RSH_MOR_NULL);
    }

    /**Original name: WTGA-CPT-RSH-MOR-NULL<br>*/
    public String getCptRshMorNull(int cptRshMorNullIdx) {
        int position = Pos.wtgaCptRshMorNull(cptRshMorNullIdx - 1);
        return readString(position, Len.CPT_RSH_MOR_NULL);
    }

    public String getCptRshMorNullFormatted(int cptRshMorNullIdx) {
        return Functions.padBlanks(getCptRshMorNull(cptRshMorNullIdx), Len.CPT_RSH_MOR_NULL);
    }

    public void setPrstzRidIni(int prstzRidIniIdx, AfDecimal prstzRidIni) {
        int position = Pos.wtgaPrstzRidIni(prstzRidIniIdx - 1);
        writeDecimalAsPacked(position, prstzRidIni.copy());
    }

    /**Original name: WTGA-PRSTZ-RID-INI<br>*/
    public AfDecimal getPrstzRidIni(int prstzRidIniIdx) {
        int position = Pos.wtgaPrstzRidIni(prstzRidIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_RID_INI, Len.Fract.PRSTZ_RID_INI);
    }

    public void setPrstzRidIniNull(int prstzRidIniNullIdx, String prstzRidIniNull) {
        int position = Pos.wtgaPrstzRidIniNull(prstzRidIniNullIdx - 1);
        writeString(position, prstzRidIniNull, Len.PRSTZ_RID_INI_NULL);
    }

    /**Original name: WTGA-PRSTZ-RID-INI-NULL<br>*/
    public String getPrstzRidIniNull(int prstzRidIniNullIdx) {
        int position = Pos.wtgaPrstzRidIniNull(prstzRidIniNullIdx - 1);
        return readString(position, Len.PRSTZ_RID_INI_NULL);
    }

    public void setFlCarCont(int flCarContIdx, char flCarCont) {
        int position = Pos.wtgaFlCarCont(flCarContIdx - 1);
        writeChar(position, flCarCont);
    }

    /**Original name: WTGA-FL-CAR-CONT<br>*/
    public char getFlCarCont(int flCarContIdx) {
        int position = Pos.wtgaFlCarCont(flCarContIdx - 1);
        return readChar(position);
    }

    public void setFlCarContNull(int flCarContNullIdx, char flCarContNull) {
        int position = Pos.wtgaFlCarContNull(flCarContNullIdx - 1);
        writeChar(position, flCarContNull);
    }

    /**Original name: WTGA-FL-CAR-CONT-NULL<br>*/
    public char getFlCarContNull(int flCarContNullIdx) {
        int position = Pos.wtgaFlCarContNull(flCarContNullIdx - 1);
        return readChar(position);
    }

    public void setBnsGiaLiqto(int bnsGiaLiqtoIdx, AfDecimal bnsGiaLiqto) {
        int position = Pos.wtgaBnsGiaLiqto(bnsGiaLiqtoIdx - 1);
        writeDecimalAsPacked(position, bnsGiaLiqto.copy());
    }

    /**Original name: WTGA-BNS-GIA-LIQTO<br>*/
    public AfDecimal getBnsGiaLiqto(int bnsGiaLiqtoIdx) {
        int position = Pos.wtgaBnsGiaLiqto(bnsGiaLiqtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.BNS_GIA_LIQTO, Len.Fract.BNS_GIA_LIQTO);
    }

    public void setBnsGiaLiqtoNull(int bnsGiaLiqtoNullIdx, String bnsGiaLiqtoNull) {
        int position = Pos.wtgaBnsGiaLiqtoNull(bnsGiaLiqtoNullIdx - 1);
        writeString(position, bnsGiaLiqtoNull, Len.BNS_GIA_LIQTO_NULL);
    }

    /**Original name: WTGA-BNS-GIA-LIQTO-NULL<br>*/
    public String getBnsGiaLiqtoNull(int bnsGiaLiqtoNullIdx) {
        int position = Pos.wtgaBnsGiaLiqtoNull(bnsGiaLiqtoNullIdx - 1);
        return readString(position, Len.BNS_GIA_LIQTO_NULL);
    }

    public void setImpBns(int impBnsIdx, AfDecimal impBns) {
        int position = Pos.wtgaImpBns(impBnsIdx - 1);
        writeDecimalAsPacked(position, impBns.copy());
    }

    /**Original name: WTGA-IMP-BNS<br>*/
    public AfDecimal getImpBns(int impBnsIdx) {
        int position = Pos.wtgaImpBns(impBnsIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_BNS, Len.Fract.IMP_BNS);
    }

    public void setImpBnsNull(int impBnsNullIdx, String impBnsNull) {
        int position = Pos.wtgaImpBnsNull(impBnsNullIdx - 1);
        writeString(position, impBnsNull, Len.IMP_BNS_NULL);
    }

    /**Original name: WTGA-IMP-BNS-NULL<br>*/
    public String getImpBnsNull(int impBnsNullIdx) {
        int position = Pos.wtgaImpBnsNull(impBnsNullIdx - 1);
        return readString(position, Len.IMP_BNS_NULL);
    }

    public void setCodDvs(int codDvsIdx, String codDvs) {
        int position = Pos.wtgaCodDvs(codDvsIdx - 1);
        writeString(position, codDvs, Len.COD_DVS);
    }

    /**Original name: WTGA-COD-DVS<br>*/
    public String getCodDvs(int codDvsIdx) {
        int position = Pos.wtgaCodDvs(codDvsIdx - 1);
        return readString(position, Len.COD_DVS);
    }

    public void setPrstzIniNewfis(int prstzIniNewfisIdx, AfDecimal prstzIniNewfis) {
        int position = Pos.wtgaPrstzIniNewfis(prstzIniNewfisIdx - 1);
        writeDecimalAsPacked(position, prstzIniNewfis.copy());
    }

    /**Original name: WTGA-PRSTZ-INI-NEWFIS<br>*/
    public AfDecimal getPrstzIniNewfis(int prstzIniNewfisIdx) {
        int position = Pos.wtgaPrstzIniNewfis(prstzIniNewfisIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI_NEWFIS, Len.Fract.PRSTZ_INI_NEWFIS);
    }

    public void setPrstzIniNewfisNull(int prstzIniNewfisNullIdx, String prstzIniNewfisNull) {
        int position = Pos.wtgaPrstzIniNewfisNull(prstzIniNewfisNullIdx - 1);
        writeString(position, prstzIniNewfisNull, Len.PRSTZ_INI_NEWFIS_NULL);
    }

    /**Original name: WTGA-PRSTZ-INI-NEWFIS-NULL<br>*/
    public String getPrstzIniNewfisNull(int prstzIniNewfisNullIdx) {
        int position = Pos.wtgaPrstzIniNewfisNull(prstzIniNewfisNullIdx - 1);
        return readString(position, Len.PRSTZ_INI_NEWFIS_NULL);
    }

    public void setImpScon(int impSconIdx, AfDecimal impScon) {
        int position = Pos.wtgaImpScon(impSconIdx - 1);
        writeDecimalAsPacked(position, impScon.copy());
    }

    /**Original name: WTGA-IMP-SCON<br>*/
    public AfDecimal getImpScon(int impSconIdx) {
        int position = Pos.wtgaImpScon(impSconIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SCON, Len.Fract.IMP_SCON);
    }

    public void setImpSconNull(int impSconNullIdx, String impSconNull) {
        int position = Pos.wtgaImpSconNull(impSconNullIdx - 1);
        writeString(position, impSconNull, Len.IMP_SCON_NULL);
    }

    /**Original name: WTGA-IMP-SCON-NULL<br>*/
    public String getImpSconNull(int impSconNullIdx) {
        int position = Pos.wtgaImpSconNull(impSconNullIdx - 1);
        return readString(position, Len.IMP_SCON_NULL);
    }

    public void setAlqScon(int alqSconIdx, AfDecimal alqScon) {
        int position = Pos.wtgaAlqScon(alqSconIdx - 1);
        writeDecimalAsPacked(position, alqScon.copy());
    }

    /**Original name: WTGA-ALQ-SCON<br>*/
    public AfDecimal getAlqScon(int alqSconIdx) {
        int position = Pos.wtgaAlqScon(alqSconIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_SCON, Len.Fract.ALQ_SCON);
    }

    public void setAlqSconNull(int alqSconNullIdx, String alqSconNull) {
        int position = Pos.wtgaAlqSconNull(alqSconNullIdx - 1);
        writeString(position, alqSconNull, Len.ALQ_SCON_NULL);
    }

    /**Original name: WTGA-ALQ-SCON-NULL<br>*/
    public String getAlqSconNull(int alqSconNullIdx) {
        int position = Pos.wtgaAlqSconNull(alqSconNullIdx - 1);
        return readString(position, Len.ALQ_SCON_NULL);
    }

    public void setImpCarAcq(int impCarAcqIdx, AfDecimal impCarAcq) {
        int position = Pos.wtgaImpCarAcq(impCarAcqIdx - 1);
        writeDecimalAsPacked(position, impCarAcq.copy());
    }

    /**Original name: WTGA-IMP-CAR-ACQ<br>*/
    public AfDecimal getImpCarAcq(int impCarAcqIdx) {
        int position = Pos.wtgaImpCarAcq(impCarAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_CAR_ACQ, Len.Fract.IMP_CAR_ACQ);
    }

    public void setImpCarAcqNull(int impCarAcqNullIdx, String impCarAcqNull) {
        int position = Pos.wtgaImpCarAcqNull(impCarAcqNullIdx - 1);
        writeString(position, impCarAcqNull, Len.IMP_CAR_ACQ_NULL);
    }

    /**Original name: WTGA-IMP-CAR-ACQ-NULL<br>*/
    public String getImpCarAcqNull(int impCarAcqNullIdx) {
        int position = Pos.wtgaImpCarAcqNull(impCarAcqNullIdx - 1);
        return readString(position, Len.IMP_CAR_ACQ_NULL);
    }

    public void setImpCarInc(int impCarIncIdx, AfDecimal impCarInc) {
        int position = Pos.wtgaImpCarInc(impCarIncIdx - 1);
        writeDecimalAsPacked(position, impCarInc.copy());
    }

    /**Original name: WTGA-IMP-CAR-INC<br>*/
    public AfDecimal getImpCarInc(int impCarIncIdx) {
        int position = Pos.wtgaImpCarInc(impCarIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_CAR_INC, Len.Fract.IMP_CAR_INC);
    }

    public void setImpCarIncNull(int impCarIncNullIdx, String impCarIncNull) {
        int position = Pos.wtgaImpCarIncNull(impCarIncNullIdx - 1);
        writeString(position, impCarIncNull, Len.IMP_CAR_INC_NULL);
    }

    /**Original name: WTGA-IMP-CAR-INC-NULL<br>*/
    public String getImpCarIncNull(int impCarIncNullIdx) {
        int position = Pos.wtgaImpCarIncNull(impCarIncNullIdx - 1);
        return readString(position, Len.IMP_CAR_INC_NULL);
    }

    public String getImpCarIncNullFormatted(int impCarIncNullIdx) {
        return Functions.padBlanks(getImpCarIncNull(impCarIncNullIdx), Len.IMP_CAR_INC_NULL);
    }

    public void setImpCarGest(int impCarGestIdx, AfDecimal impCarGest) {
        int position = Pos.wtgaImpCarGest(impCarGestIdx - 1);
        writeDecimalAsPacked(position, impCarGest.copy());
    }

    /**Original name: WTGA-IMP-CAR-GEST<br>*/
    public AfDecimal getImpCarGest(int impCarGestIdx) {
        int position = Pos.wtgaImpCarGest(impCarGestIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_CAR_GEST, Len.Fract.IMP_CAR_GEST);
    }

    public void setImpCarGestNull(int impCarGestNullIdx, String impCarGestNull) {
        int position = Pos.wtgaImpCarGestNull(impCarGestNullIdx - 1);
        writeString(position, impCarGestNull, Len.IMP_CAR_GEST_NULL);
    }

    /**Original name: WTGA-IMP-CAR-GEST-NULL<br>*/
    public String getImpCarGestNull(int impCarGestNullIdx) {
        int position = Pos.wtgaImpCarGestNull(impCarGestNullIdx - 1);
        return readString(position, Len.IMP_CAR_GEST_NULL);
    }

    public String getImpCarGestNullFormatted(int impCarGestNullIdx) {
        return Functions.padBlanks(getImpCarGestNull(impCarGestNullIdx), Len.IMP_CAR_GEST_NULL);
    }

    public void setEtaAa1oAssto(int etaAa1oAsstoIdx, short etaAa1oAssto) {
        int position = Pos.wtgaEtaAa1oAssto(etaAa1oAsstoIdx - 1);
        writeShortAsPacked(position, etaAa1oAssto, Len.Int.ETA_AA1O_ASSTO);
    }

    /**Original name: WTGA-ETA-AA-1O-ASSTO<br>*/
    public short getEtaAa1oAssto(int etaAa1oAsstoIdx) {
        int position = Pos.wtgaEtaAa1oAssto(etaAa1oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_AA1O_ASSTO);
    }

    public void setEtaAa1oAsstoNull(int etaAa1oAsstoNullIdx, String etaAa1oAsstoNull) {
        int position = Pos.wtgaEtaAa1oAsstoNull(etaAa1oAsstoNullIdx - 1);
        writeString(position, etaAa1oAsstoNull, Len.ETA_AA1O_ASSTO_NULL);
    }

    /**Original name: WTGA-ETA-AA-1O-ASSTO-NULL<br>*/
    public String getEtaAa1oAsstoNull(int etaAa1oAsstoNullIdx) {
        int position = Pos.wtgaEtaAa1oAsstoNull(etaAa1oAsstoNullIdx - 1);
        return readString(position, Len.ETA_AA1O_ASSTO_NULL);
    }

    public String getEtaAa1oAsstoNullFormatted(int etaAa1oAsstoNullIdx) {
        return Functions.padBlanks(getEtaAa1oAsstoNull(etaAa1oAsstoNullIdx), Len.ETA_AA1O_ASSTO_NULL);
    }

    public void setEtaMm1oAssto(int etaMm1oAsstoIdx, short etaMm1oAssto) {
        int position = Pos.wtgaEtaMm1oAssto(etaMm1oAsstoIdx - 1);
        writeShortAsPacked(position, etaMm1oAssto, Len.Int.ETA_MM1O_ASSTO);
    }

    /**Original name: WTGA-ETA-MM-1O-ASSTO<br>*/
    public short getEtaMm1oAssto(int etaMm1oAsstoIdx) {
        int position = Pos.wtgaEtaMm1oAssto(etaMm1oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_MM1O_ASSTO);
    }

    public void setEtaMm1oAsstoNull(int etaMm1oAsstoNullIdx, String etaMm1oAsstoNull) {
        int position = Pos.wtgaEtaMm1oAsstoNull(etaMm1oAsstoNullIdx - 1);
        writeString(position, etaMm1oAsstoNull, Len.ETA_MM1O_ASSTO_NULL);
    }

    /**Original name: WTGA-ETA-MM-1O-ASSTO-NULL<br>*/
    public String getEtaMm1oAsstoNull(int etaMm1oAsstoNullIdx) {
        int position = Pos.wtgaEtaMm1oAsstoNull(etaMm1oAsstoNullIdx - 1);
        return readString(position, Len.ETA_MM1O_ASSTO_NULL);
    }

    public String getEtaMm1oAsstoNullFormatted(int etaMm1oAsstoNullIdx) {
        return Functions.padBlanks(getEtaMm1oAsstoNull(etaMm1oAsstoNullIdx), Len.ETA_MM1O_ASSTO_NULL);
    }

    public void setEtaAa2oAssto(int etaAa2oAsstoIdx, short etaAa2oAssto) {
        int position = Pos.wtgaEtaAa2oAssto(etaAa2oAsstoIdx - 1);
        writeShortAsPacked(position, etaAa2oAssto, Len.Int.ETA_AA2O_ASSTO);
    }

    /**Original name: WTGA-ETA-AA-2O-ASSTO<br>*/
    public short getEtaAa2oAssto(int etaAa2oAsstoIdx) {
        int position = Pos.wtgaEtaAa2oAssto(etaAa2oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_AA2O_ASSTO);
    }

    public void setEtaAa2oAsstoNull(int etaAa2oAsstoNullIdx, String etaAa2oAsstoNull) {
        int position = Pos.wtgaEtaAa2oAsstoNull(etaAa2oAsstoNullIdx - 1);
        writeString(position, etaAa2oAsstoNull, Len.ETA_AA2O_ASSTO_NULL);
    }

    /**Original name: WTGA-ETA-AA-2O-ASSTO-NULL<br>*/
    public String getEtaAa2oAsstoNull(int etaAa2oAsstoNullIdx) {
        int position = Pos.wtgaEtaAa2oAsstoNull(etaAa2oAsstoNullIdx - 1);
        return readString(position, Len.ETA_AA2O_ASSTO_NULL);
    }

    public void setEtaMm2oAssto(int etaMm2oAsstoIdx, short etaMm2oAssto) {
        int position = Pos.wtgaEtaMm2oAssto(etaMm2oAsstoIdx - 1);
        writeShortAsPacked(position, etaMm2oAssto, Len.Int.ETA_MM2O_ASSTO);
    }

    /**Original name: WTGA-ETA-MM-2O-ASSTO<br>*/
    public short getEtaMm2oAssto(int etaMm2oAsstoIdx) {
        int position = Pos.wtgaEtaMm2oAssto(etaMm2oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_MM2O_ASSTO);
    }

    public void setEtaMm2oAsstoNull(int etaMm2oAsstoNullIdx, String etaMm2oAsstoNull) {
        int position = Pos.wtgaEtaMm2oAsstoNull(etaMm2oAsstoNullIdx - 1);
        writeString(position, etaMm2oAsstoNull, Len.ETA_MM2O_ASSTO_NULL);
    }

    /**Original name: WTGA-ETA-MM-2O-ASSTO-NULL<br>*/
    public String getEtaMm2oAsstoNull(int etaMm2oAsstoNullIdx) {
        int position = Pos.wtgaEtaMm2oAsstoNull(etaMm2oAsstoNullIdx - 1);
        return readString(position, Len.ETA_MM2O_ASSTO_NULL);
    }

    public void setEtaAa3oAssto(int etaAa3oAsstoIdx, short etaAa3oAssto) {
        int position = Pos.wtgaEtaAa3oAssto(etaAa3oAsstoIdx - 1);
        writeShortAsPacked(position, etaAa3oAssto, Len.Int.ETA_AA3O_ASSTO);
    }

    /**Original name: WTGA-ETA-AA-3O-ASSTO<br>*/
    public short getEtaAa3oAssto(int etaAa3oAsstoIdx) {
        int position = Pos.wtgaEtaAa3oAssto(etaAa3oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_AA3O_ASSTO);
    }

    public void setEtaAa3oAsstoNull(int etaAa3oAsstoNullIdx, String etaAa3oAsstoNull) {
        int position = Pos.wtgaEtaAa3oAsstoNull(etaAa3oAsstoNullIdx - 1);
        writeString(position, etaAa3oAsstoNull, Len.ETA_AA3O_ASSTO_NULL);
    }

    /**Original name: WTGA-ETA-AA-3O-ASSTO-NULL<br>*/
    public String getEtaAa3oAsstoNull(int etaAa3oAsstoNullIdx) {
        int position = Pos.wtgaEtaAa3oAsstoNull(etaAa3oAsstoNullIdx - 1);
        return readString(position, Len.ETA_AA3O_ASSTO_NULL);
    }

    public void setEtaMm3oAssto(int etaMm3oAsstoIdx, short etaMm3oAssto) {
        int position = Pos.wtgaEtaMm3oAssto(etaMm3oAsstoIdx - 1);
        writeShortAsPacked(position, etaMm3oAssto, Len.Int.ETA_MM3O_ASSTO);
    }

    /**Original name: WTGA-ETA-MM-3O-ASSTO<br>*/
    public short getEtaMm3oAssto(int etaMm3oAsstoIdx) {
        int position = Pos.wtgaEtaMm3oAssto(etaMm3oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_MM3O_ASSTO);
    }

    public void setEtaMm3oAsstoNull(int etaMm3oAsstoNullIdx, String etaMm3oAsstoNull) {
        int position = Pos.wtgaEtaMm3oAsstoNull(etaMm3oAsstoNullIdx - 1);
        writeString(position, etaMm3oAsstoNull, Len.ETA_MM3O_ASSTO_NULL);
    }

    /**Original name: WTGA-ETA-MM-3O-ASSTO-NULL<br>*/
    public String getEtaMm3oAsstoNull(int etaMm3oAsstoNullIdx) {
        int position = Pos.wtgaEtaMm3oAsstoNull(etaMm3oAsstoNullIdx - 1);
        return readString(position, Len.ETA_MM3O_ASSTO_NULL);
    }

    public void setRendtoLrd(int rendtoLrdIdx, AfDecimal rendtoLrd) {
        int position = Pos.wtgaRendtoLrd(rendtoLrdIdx - 1);
        writeDecimalAsPacked(position, rendtoLrd.copy());
    }

    /**Original name: WTGA-RENDTO-LRD<br>*/
    public AfDecimal getRendtoLrd(int rendtoLrdIdx) {
        int position = Pos.wtgaRendtoLrd(rendtoLrdIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RENDTO_LRD, Len.Fract.RENDTO_LRD);
    }

    public void setRendtoLrdNull(int rendtoLrdNullIdx, String rendtoLrdNull) {
        int position = Pos.wtgaRendtoLrdNull(rendtoLrdNullIdx - 1);
        writeString(position, rendtoLrdNull, Len.RENDTO_LRD_NULL);
    }

    /**Original name: WTGA-RENDTO-LRD-NULL<br>*/
    public String getRendtoLrdNull(int rendtoLrdNullIdx) {
        int position = Pos.wtgaRendtoLrdNull(rendtoLrdNullIdx - 1);
        return readString(position, Len.RENDTO_LRD_NULL);
    }

    public void setPcRetr(int pcRetrIdx, AfDecimal pcRetr) {
        int position = Pos.wtgaPcRetr(pcRetrIdx - 1);
        writeDecimalAsPacked(position, pcRetr.copy());
    }

    /**Original name: WTGA-PC-RETR<br>*/
    public AfDecimal getPcRetr(int pcRetrIdx) {
        int position = Pos.wtgaPcRetr(pcRetrIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_RETR, Len.Fract.PC_RETR);
    }

    public void setPcRetrNull(int pcRetrNullIdx, String pcRetrNull) {
        int position = Pos.wtgaPcRetrNull(pcRetrNullIdx - 1);
        writeString(position, pcRetrNull, Len.PC_RETR_NULL);
    }

    /**Original name: WTGA-PC-RETR-NULL<br>*/
    public String getPcRetrNull(int pcRetrNullIdx) {
        int position = Pos.wtgaPcRetrNull(pcRetrNullIdx - 1);
        return readString(position, Len.PC_RETR_NULL);
    }

    public void setRendtoRetr(int rendtoRetrIdx, AfDecimal rendtoRetr) {
        int position = Pos.wtgaRendtoRetr(rendtoRetrIdx - 1);
        writeDecimalAsPacked(position, rendtoRetr.copy());
    }

    /**Original name: WTGA-RENDTO-RETR<br>*/
    public AfDecimal getRendtoRetr(int rendtoRetrIdx) {
        int position = Pos.wtgaRendtoRetr(rendtoRetrIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RENDTO_RETR, Len.Fract.RENDTO_RETR);
    }

    public void setRendtoRetrNull(int rendtoRetrNullIdx, String rendtoRetrNull) {
        int position = Pos.wtgaRendtoRetrNull(rendtoRetrNullIdx - 1);
        writeString(position, rendtoRetrNull, Len.RENDTO_RETR_NULL);
    }

    /**Original name: WTGA-RENDTO-RETR-NULL<br>*/
    public String getRendtoRetrNull(int rendtoRetrNullIdx) {
        int position = Pos.wtgaRendtoRetrNull(rendtoRetrNullIdx - 1);
        return readString(position, Len.RENDTO_RETR_NULL);
    }

    public void setMinGarto(int minGartoIdx, AfDecimal minGarto) {
        int position = Pos.wtgaMinGarto(minGartoIdx - 1);
        writeDecimalAsPacked(position, minGarto.copy());
    }

    /**Original name: WTGA-MIN-GARTO<br>*/
    public AfDecimal getMinGarto(int minGartoIdx) {
        int position = Pos.wtgaMinGarto(minGartoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MIN_GARTO, Len.Fract.MIN_GARTO);
    }

    public void setMinGartoNull(int minGartoNullIdx, String minGartoNull) {
        int position = Pos.wtgaMinGartoNull(minGartoNullIdx - 1);
        writeString(position, minGartoNull, Len.MIN_GARTO_NULL);
    }

    /**Original name: WTGA-MIN-GARTO-NULL<br>*/
    public String getMinGartoNull(int minGartoNullIdx) {
        int position = Pos.wtgaMinGartoNull(minGartoNullIdx - 1);
        return readString(position, Len.MIN_GARTO_NULL);
    }

    public void setMinTrnut(int minTrnutIdx, AfDecimal minTrnut) {
        int position = Pos.wtgaMinTrnut(minTrnutIdx - 1);
        writeDecimalAsPacked(position, minTrnut.copy());
    }

    /**Original name: WTGA-MIN-TRNUT<br>*/
    public AfDecimal getMinTrnut(int minTrnutIdx) {
        int position = Pos.wtgaMinTrnut(minTrnutIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MIN_TRNUT, Len.Fract.MIN_TRNUT);
    }

    public void setMinTrnutNull(int minTrnutNullIdx, String minTrnutNull) {
        int position = Pos.wtgaMinTrnutNull(minTrnutNullIdx - 1);
        writeString(position, minTrnutNull, Len.MIN_TRNUT_NULL);
    }

    /**Original name: WTGA-MIN-TRNUT-NULL<br>*/
    public String getMinTrnutNull(int minTrnutNullIdx) {
        int position = Pos.wtgaMinTrnutNull(minTrnutNullIdx - 1);
        return readString(position, Len.MIN_TRNUT_NULL);
    }

    public void setPreAttDiTrch(int preAttDiTrchIdx, AfDecimal preAttDiTrch) {
        int position = Pos.wtgaPreAttDiTrch(preAttDiTrchIdx - 1);
        writeDecimalAsPacked(position, preAttDiTrch.copy());
    }

    /**Original name: WTGA-PRE-ATT-DI-TRCH<br>*/
    public AfDecimal getPreAttDiTrch(int preAttDiTrchIdx) {
        int position = Pos.wtgaPreAttDiTrch(preAttDiTrchIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_ATT_DI_TRCH, Len.Fract.PRE_ATT_DI_TRCH);
    }

    public void setPreAttDiTrchNull(int preAttDiTrchNullIdx, String preAttDiTrchNull) {
        int position = Pos.wtgaPreAttDiTrchNull(preAttDiTrchNullIdx - 1);
        writeString(position, preAttDiTrchNull, Len.PRE_ATT_DI_TRCH_NULL);
    }

    /**Original name: WTGA-PRE-ATT-DI-TRCH-NULL<br>*/
    public String getPreAttDiTrchNull(int preAttDiTrchNullIdx) {
        int position = Pos.wtgaPreAttDiTrchNull(preAttDiTrchNullIdx - 1);
        return readString(position, Len.PRE_ATT_DI_TRCH_NULL);
    }

    public void setMatuEnd2000(int matuEnd2000Idx, AfDecimal matuEnd2000) {
        int position = Pos.wtgaMatuEnd2000(matuEnd2000Idx - 1);
        writeDecimalAsPacked(position, matuEnd2000.copy());
    }

    /**Original name: WTGA-MATU-END2000<br>*/
    public AfDecimal getMatuEnd2000(int matuEnd2000Idx) {
        int position = Pos.wtgaMatuEnd2000(matuEnd2000Idx - 1);
        return readPackedAsDecimal(position, Len.Int.MATU_END2000, Len.Fract.MATU_END2000);
    }

    public void setMatuEnd2000Null(int matuEnd2000NullIdx, String matuEnd2000Null) {
        int position = Pos.wtgaMatuEnd2000Null(matuEnd2000NullIdx - 1);
        writeString(position, matuEnd2000Null, Len.MATU_END2000_NULL);
    }

    /**Original name: WTGA-MATU-END2000-NULL<br>*/
    public String getMatuEnd2000Null(int matuEnd2000NullIdx) {
        int position = Pos.wtgaMatuEnd2000Null(matuEnd2000NullIdx - 1);
        return readString(position, Len.MATU_END2000_NULL);
    }

    public void setAbbTotIni(int abbTotIniIdx, AfDecimal abbTotIni) {
        int position = Pos.wtgaAbbTotIni(abbTotIniIdx - 1);
        writeDecimalAsPacked(position, abbTotIni.copy());
    }

    /**Original name: WTGA-ABB-TOT-INI<br>*/
    public AfDecimal getAbbTotIni(int abbTotIniIdx) {
        int position = Pos.wtgaAbbTotIni(abbTotIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ABB_TOT_INI, Len.Fract.ABB_TOT_INI);
    }

    public void setAbbTotIniNull(int abbTotIniNullIdx, String abbTotIniNull) {
        int position = Pos.wtgaAbbTotIniNull(abbTotIniNullIdx - 1);
        writeString(position, abbTotIniNull, Len.ABB_TOT_INI_NULL);
    }

    /**Original name: WTGA-ABB-TOT-INI-NULL<br>*/
    public String getAbbTotIniNull(int abbTotIniNullIdx) {
        int position = Pos.wtgaAbbTotIniNull(abbTotIniNullIdx - 1);
        return readString(position, Len.ABB_TOT_INI_NULL);
    }

    public void setAbbTotUlt(int abbTotUltIdx, AfDecimal abbTotUlt) {
        int position = Pos.wtgaAbbTotUlt(abbTotUltIdx - 1);
        writeDecimalAsPacked(position, abbTotUlt.copy());
    }

    /**Original name: WTGA-ABB-TOT-ULT<br>*/
    public AfDecimal getAbbTotUlt(int abbTotUltIdx) {
        int position = Pos.wtgaAbbTotUlt(abbTotUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ABB_TOT_ULT, Len.Fract.ABB_TOT_ULT);
    }

    public void setAbbTotUltNull(int abbTotUltNullIdx, String abbTotUltNull) {
        int position = Pos.wtgaAbbTotUltNull(abbTotUltNullIdx - 1);
        writeString(position, abbTotUltNull, Len.ABB_TOT_ULT_NULL);
    }

    /**Original name: WTGA-ABB-TOT-ULT-NULL<br>*/
    public String getAbbTotUltNull(int abbTotUltNullIdx) {
        int position = Pos.wtgaAbbTotUltNull(abbTotUltNullIdx - 1);
        return readString(position, Len.ABB_TOT_ULT_NULL);
    }

    public void setAbbAnnuUlt(int abbAnnuUltIdx, AfDecimal abbAnnuUlt) {
        int position = Pos.wtgaAbbAnnuUlt(abbAnnuUltIdx - 1);
        writeDecimalAsPacked(position, abbAnnuUlt.copy());
    }

    /**Original name: WTGA-ABB-ANNU-ULT<br>*/
    public AfDecimal getAbbAnnuUlt(int abbAnnuUltIdx) {
        int position = Pos.wtgaAbbAnnuUlt(abbAnnuUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ABB_ANNU_ULT, Len.Fract.ABB_ANNU_ULT);
    }

    public void setAbbAnnuUltNull(int abbAnnuUltNullIdx, String abbAnnuUltNull) {
        int position = Pos.wtgaAbbAnnuUltNull(abbAnnuUltNullIdx - 1);
        writeString(position, abbAnnuUltNull, Len.ABB_ANNU_ULT_NULL);
    }

    /**Original name: WTGA-ABB-ANNU-ULT-NULL<br>*/
    public String getAbbAnnuUltNull(int abbAnnuUltNullIdx) {
        int position = Pos.wtgaAbbAnnuUltNull(abbAnnuUltNullIdx - 1);
        return readString(position, Len.ABB_ANNU_ULT_NULL);
    }

    public void setDurAbb(int durAbbIdx, int durAbb) {
        int position = Pos.wtgaDurAbb(durAbbIdx - 1);
        writeIntAsPacked(position, durAbb, Len.Int.DUR_ABB);
    }

    /**Original name: WTGA-DUR-ABB<br>*/
    public int getDurAbb(int durAbbIdx) {
        int position = Pos.wtgaDurAbb(durAbbIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_ABB);
    }

    public void setDurAbbNull(int durAbbNullIdx, String durAbbNull) {
        int position = Pos.wtgaDurAbbNull(durAbbNullIdx - 1);
        writeString(position, durAbbNull, Len.DUR_ABB_NULL);
    }

    /**Original name: WTGA-DUR-ABB-NULL<br>*/
    public String getDurAbbNull(int durAbbNullIdx) {
        int position = Pos.wtgaDurAbbNull(durAbbNullIdx - 1);
        return readString(position, Len.DUR_ABB_NULL);
    }

    public void setTpAdegAbb(int tpAdegAbbIdx, char tpAdegAbb) {
        int position = Pos.wtgaTpAdegAbb(tpAdegAbbIdx - 1);
        writeChar(position, tpAdegAbb);
    }

    /**Original name: WTGA-TP-ADEG-ABB<br>*/
    public char getTpAdegAbb(int tpAdegAbbIdx) {
        int position = Pos.wtgaTpAdegAbb(tpAdegAbbIdx - 1);
        return readChar(position);
    }

    public void setTpAdegAbbNull(int tpAdegAbbNullIdx, char tpAdegAbbNull) {
        int position = Pos.wtgaTpAdegAbbNull(tpAdegAbbNullIdx - 1);
        writeChar(position, tpAdegAbbNull);
    }

    /**Original name: WTGA-TP-ADEG-ABB-NULL<br>*/
    public char getTpAdegAbbNull(int tpAdegAbbNullIdx) {
        int position = Pos.wtgaTpAdegAbbNull(tpAdegAbbNullIdx - 1);
        return readChar(position);
    }

    public void setModCalc(int modCalcIdx, String modCalc) {
        int position = Pos.wtgaModCalc(modCalcIdx - 1);
        writeString(position, modCalc, Len.MOD_CALC);
    }

    /**Original name: WTGA-MOD-CALC<br>*/
    public String getModCalc(int modCalcIdx) {
        int position = Pos.wtgaModCalc(modCalcIdx - 1);
        return readString(position, Len.MOD_CALC);
    }

    public void setModCalcNull(int modCalcNullIdx, String modCalcNull) {
        int position = Pos.wtgaModCalcNull(modCalcNullIdx - 1);
        writeString(position, modCalcNull, Len.MOD_CALC_NULL);
    }

    /**Original name: WTGA-MOD-CALC-NULL<br>*/
    public String getModCalcNull(int modCalcNullIdx) {
        int position = Pos.wtgaModCalcNull(modCalcNullIdx - 1);
        return readString(position, Len.MOD_CALC_NULL);
    }

    public void setImpAz(int impAzIdx, AfDecimal impAz) {
        int position = Pos.wtgaImpAz(impAzIdx - 1);
        writeDecimalAsPacked(position, impAz.copy());
    }

    /**Original name: WTGA-IMP-AZ<br>*/
    public AfDecimal getImpAz(int impAzIdx) {
        int position = Pos.wtgaImpAz(impAzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_AZ, Len.Fract.IMP_AZ);
    }

    public void setImpAzNull(int impAzNullIdx, String impAzNull) {
        int position = Pos.wtgaImpAzNull(impAzNullIdx - 1);
        writeString(position, impAzNull, Len.IMP_AZ_NULL);
    }

    /**Original name: WTGA-IMP-AZ-NULL<br>*/
    public String getImpAzNull(int impAzNullIdx) {
        int position = Pos.wtgaImpAzNull(impAzNullIdx - 1);
        return readString(position, Len.IMP_AZ_NULL);
    }

    public void setImpAder(int impAderIdx, AfDecimal impAder) {
        int position = Pos.wtgaImpAder(impAderIdx - 1);
        writeDecimalAsPacked(position, impAder.copy());
    }

    /**Original name: WTGA-IMP-ADER<br>*/
    public AfDecimal getImpAder(int impAderIdx) {
        int position = Pos.wtgaImpAder(impAderIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_ADER, Len.Fract.IMP_ADER);
    }

    public void setImpAderNull(int impAderNullIdx, String impAderNull) {
        int position = Pos.wtgaImpAderNull(impAderNullIdx - 1);
        writeString(position, impAderNull, Len.IMP_ADER_NULL);
    }

    /**Original name: WTGA-IMP-ADER-NULL<br>*/
    public String getImpAderNull(int impAderNullIdx) {
        int position = Pos.wtgaImpAderNull(impAderNullIdx - 1);
        return readString(position, Len.IMP_ADER_NULL);
    }

    public void setImpTfr(int impTfrIdx, AfDecimal impTfr) {
        int position = Pos.wtgaImpTfr(impTfrIdx - 1);
        writeDecimalAsPacked(position, impTfr.copy());
    }

    /**Original name: WTGA-IMP-TFR<br>*/
    public AfDecimal getImpTfr(int impTfrIdx) {
        int position = Pos.wtgaImpTfr(impTfrIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_TFR, Len.Fract.IMP_TFR);
    }

    public void setImpTfrNull(int impTfrNullIdx, String impTfrNull) {
        int position = Pos.wtgaImpTfrNull(impTfrNullIdx - 1);
        writeString(position, impTfrNull, Len.IMP_TFR_NULL);
    }

    /**Original name: WTGA-IMP-TFR-NULL<br>*/
    public String getImpTfrNull(int impTfrNullIdx) {
        int position = Pos.wtgaImpTfrNull(impTfrNullIdx - 1);
        return readString(position, Len.IMP_TFR_NULL);
    }

    public void setImpVolo(int impVoloIdx, AfDecimal impVolo) {
        int position = Pos.wtgaImpVolo(impVoloIdx - 1);
        writeDecimalAsPacked(position, impVolo.copy());
    }

    /**Original name: WTGA-IMP-VOLO<br>*/
    public AfDecimal getImpVolo(int impVoloIdx) {
        int position = Pos.wtgaImpVolo(impVoloIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_VOLO, Len.Fract.IMP_VOLO);
    }

    public void setImpVoloNull(int impVoloNullIdx, String impVoloNull) {
        int position = Pos.wtgaImpVoloNull(impVoloNullIdx - 1);
        writeString(position, impVoloNull, Len.IMP_VOLO_NULL);
    }

    /**Original name: WTGA-IMP-VOLO-NULL<br>*/
    public String getImpVoloNull(int impVoloNullIdx) {
        int position = Pos.wtgaImpVoloNull(impVoloNullIdx - 1);
        return readString(position, Len.IMP_VOLO_NULL);
    }

    public void setVisEnd2000(int visEnd2000Idx, AfDecimal visEnd2000) {
        int position = Pos.wtgaVisEnd2000(visEnd2000Idx - 1);
        writeDecimalAsPacked(position, visEnd2000.copy());
    }

    /**Original name: WTGA-VIS-END2000<br>*/
    public AfDecimal getVisEnd2000(int visEnd2000Idx) {
        int position = Pos.wtgaVisEnd2000(visEnd2000Idx - 1);
        return readPackedAsDecimal(position, Len.Int.VIS_END2000, Len.Fract.VIS_END2000);
    }

    public void setVisEnd2000Null(int visEnd2000NullIdx, String visEnd2000Null) {
        int position = Pos.wtgaVisEnd2000Null(visEnd2000NullIdx - 1);
        writeString(position, visEnd2000Null, Len.VIS_END2000_NULL);
    }

    /**Original name: WTGA-VIS-END2000-NULL<br>*/
    public String getVisEnd2000Null(int visEnd2000NullIdx) {
        int position = Pos.wtgaVisEnd2000Null(visEnd2000NullIdx - 1);
        return readString(position, Len.VIS_END2000_NULL);
    }

    public void setDtVldtProd(int dtVldtProdIdx, int dtVldtProd) {
        int position = Pos.wtgaDtVldtProd(dtVldtProdIdx - 1);
        writeIntAsPacked(position, dtVldtProd, Len.Int.DT_VLDT_PROD);
    }

    /**Original name: WTGA-DT-VLDT-PROD<br>*/
    public int getDtVldtProd(int dtVldtProdIdx) {
        int position = Pos.wtgaDtVldtProd(dtVldtProdIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_VLDT_PROD);
    }

    public void setDtVldtProdNull(int dtVldtProdNullIdx, String dtVldtProdNull) {
        int position = Pos.wtgaDtVldtProdNull(dtVldtProdNullIdx - 1);
        writeString(position, dtVldtProdNull, Len.DT_VLDT_PROD_NULL);
    }

    /**Original name: WTGA-DT-VLDT-PROD-NULL<br>*/
    public String getDtVldtProdNull(int dtVldtProdNullIdx) {
        int position = Pos.wtgaDtVldtProdNull(dtVldtProdNullIdx - 1);
        return readString(position, Len.DT_VLDT_PROD_NULL);
    }

    public void setDtIniValTar(int dtIniValTarIdx, int dtIniValTar) {
        int position = Pos.wtgaDtIniValTar(dtIniValTarIdx - 1);
        writeIntAsPacked(position, dtIniValTar, Len.Int.DT_INI_VAL_TAR);
    }

    /**Original name: WTGA-DT-INI-VAL-TAR<br>*/
    public int getDtIniValTar(int dtIniValTarIdx) {
        int position = Pos.wtgaDtIniValTar(dtIniValTarIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_VAL_TAR);
    }

    public void setDtIniValTarNull(int dtIniValTarNullIdx, String dtIniValTarNull) {
        int position = Pos.wtgaDtIniValTarNull(dtIniValTarNullIdx - 1);
        writeString(position, dtIniValTarNull, Len.DT_INI_VAL_TAR_NULL);
    }

    /**Original name: WTGA-DT-INI-VAL-TAR-NULL<br>*/
    public String getDtIniValTarNull(int dtIniValTarNullIdx) {
        int position = Pos.wtgaDtIniValTarNull(dtIniValTarNullIdx - 1);
        return readString(position, Len.DT_INI_VAL_TAR_NULL);
    }

    public void setImpbVisEnd2000(int impbVisEnd2000Idx, AfDecimal impbVisEnd2000) {
        int position = Pos.wtgaImpbVisEnd2000(impbVisEnd2000Idx - 1);
        writeDecimalAsPacked(position, impbVisEnd2000.copy());
    }

    /**Original name: WTGA-IMPB-VIS-END2000<br>*/
    public AfDecimal getImpbVisEnd2000(int impbVisEnd2000Idx) {
        int position = Pos.wtgaImpbVisEnd2000(impbVisEnd2000Idx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_VIS_END2000, Len.Fract.IMPB_VIS_END2000);
    }

    public void setImpbVisEnd2000Null(int impbVisEnd2000NullIdx, String impbVisEnd2000Null) {
        int position = Pos.wtgaImpbVisEnd2000Null(impbVisEnd2000NullIdx - 1);
        writeString(position, impbVisEnd2000Null, Len.IMPB_VIS_END2000_NULL);
    }

    /**Original name: WTGA-IMPB-VIS-END2000-NULL<br>*/
    public String getImpbVisEnd2000Null(int impbVisEnd2000NullIdx) {
        int position = Pos.wtgaImpbVisEnd2000Null(impbVisEnd2000NullIdx - 1);
        return readString(position, Len.IMPB_VIS_END2000_NULL);
    }

    public void setRenIniTsTec0(int renIniTsTec0Idx, AfDecimal renIniTsTec0) {
        int position = Pos.wtgaRenIniTsTec0(renIniTsTec0Idx - 1);
        writeDecimalAsPacked(position, renIniTsTec0.copy());
    }

    /**Original name: WTGA-REN-INI-TS-TEC-0<br>*/
    public AfDecimal getRenIniTsTec0(int renIniTsTec0Idx) {
        int position = Pos.wtgaRenIniTsTec0(renIniTsTec0Idx - 1);
        return readPackedAsDecimal(position, Len.Int.REN_INI_TS_TEC0, Len.Fract.REN_INI_TS_TEC0);
    }

    public void setRenIniTsTec0Null(int renIniTsTec0NullIdx, String renIniTsTec0Null) {
        int position = Pos.wtgaRenIniTsTec0Null(renIniTsTec0NullIdx - 1);
        writeString(position, renIniTsTec0Null, Len.REN_INI_TS_TEC0_NULL);
    }

    /**Original name: WTGA-REN-INI-TS-TEC-0-NULL<br>*/
    public String getRenIniTsTec0Null(int renIniTsTec0NullIdx) {
        int position = Pos.wtgaRenIniTsTec0Null(renIniTsTec0NullIdx - 1);
        return readString(position, Len.REN_INI_TS_TEC0_NULL);
    }

    public void setPcRipPre(int pcRipPreIdx, AfDecimal pcRipPre) {
        int position = Pos.wtgaPcRipPre(pcRipPreIdx - 1);
        writeDecimalAsPacked(position, pcRipPre.copy());
    }

    /**Original name: WTGA-PC-RIP-PRE<br>*/
    public AfDecimal getPcRipPre(int pcRipPreIdx) {
        int position = Pos.wtgaPcRipPre(pcRipPreIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_RIP_PRE, Len.Fract.PC_RIP_PRE);
    }

    public void setPcRipPreNull(int pcRipPreNullIdx, String pcRipPreNull) {
        int position = Pos.wtgaPcRipPreNull(pcRipPreNullIdx - 1);
        writeString(position, pcRipPreNull, Len.PC_RIP_PRE_NULL);
    }

    /**Original name: WTGA-PC-RIP-PRE-NULL<br>*/
    public String getPcRipPreNull(int pcRipPreNullIdx) {
        int position = Pos.wtgaPcRipPreNull(pcRipPreNullIdx - 1);
        return readString(position, Len.PC_RIP_PRE_NULL);
    }

    public void setFlImportiForz(int flImportiForzIdx, char flImportiForz) {
        int position = Pos.wtgaFlImportiForz(flImportiForzIdx - 1);
        writeChar(position, flImportiForz);
    }

    /**Original name: WTGA-FL-IMPORTI-FORZ<br>*/
    public char getFlImportiForz(int flImportiForzIdx) {
        int position = Pos.wtgaFlImportiForz(flImportiForzIdx - 1);
        return readChar(position);
    }

    public void setFlImportiForzNull(int flImportiForzNullIdx, char flImportiForzNull) {
        int position = Pos.wtgaFlImportiForzNull(flImportiForzNullIdx - 1);
        writeChar(position, flImportiForzNull);
    }

    /**Original name: WTGA-FL-IMPORTI-FORZ-NULL<br>*/
    public char getFlImportiForzNull(int flImportiForzNullIdx) {
        int position = Pos.wtgaFlImportiForzNull(flImportiForzNullIdx - 1);
        return readChar(position);
    }

    public void setPrstzIniNforz(int prstzIniNforzIdx, AfDecimal prstzIniNforz) {
        int position = Pos.wtgaPrstzIniNforz(prstzIniNforzIdx - 1);
        writeDecimalAsPacked(position, prstzIniNforz.copy());
    }

    /**Original name: WTGA-PRSTZ-INI-NFORZ<br>*/
    public AfDecimal getPrstzIniNforz(int prstzIniNforzIdx) {
        int position = Pos.wtgaPrstzIniNforz(prstzIniNforzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI_NFORZ, Len.Fract.PRSTZ_INI_NFORZ);
    }

    public void setPrstzIniNforzNull(int prstzIniNforzNullIdx, String prstzIniNforzNull) {
        int position = Pos.wtgaPrstzIniNforzNull(prstzIniNforzNullIdx - 1);
        writeString(position, prstzIniNforzNull, Len.PRSTZ_INI_NFORZ_NULL);
    }

    /**Original name: WTGA-PRSTZ-INI-NFORZ-NULL<br>*/
    public String getPrstzIniNforzNull(int prstzIniNforzNullIdx) {
        int position = Pos.wtgaPrstzIniNforzNull(prstzIniNforzNullIdx - 1);
        return readString(position, Len.PRSTZ_INI_NFORZ_NULL);
    }

    public void setVisEnd2000Nforz(int visEnd2000NforzIdx, AfDecimal visEnd2000Nforz) {
        int position = Pos.wtgaVisEnd2000Nforz(visEnd2000NforzIdx - 1);
        writeDecimalAsPacked(position, visEnd2000Nforz.copy());
    }

    /**Original name: WTGA-VIS-END2000-NFORZ<br>*/
    public AfDecimal getVisEnd2000Nforz(int visEnd2000NforzIdx) {
        int position = Pos.wtgaVisEnd2000Nforz(visEnd2000NforzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VIS_END2000_NFORZ, Len.Fract.VIS_END2000_NFORZ);
    }

    public void setVisEnd2000NforzNull(int visEnd2000NforzNullIdx, String visEnd2000NforzNull) {
        int position = Pos.wtgaVisEnd2000NforzNull(visEnd2000NforzNullIdx - 1);
        writeString(position, visEnd2000NforzNull, Len.VIS_END2000_NFORZ_NULL);
    }

    /**Original name: WTGA-VIS-END2000-NFORZ-NULL<br>*/
    public String getVisEnd2000NforzNull(int visEnd2000NforzNullIdx) {
        int position = Pos.wtgaVisEnd2000NforzNull(visEnd2000NforzNullIdx - 1);
        return readString(position, Len.VIS_END2000_NFORZ_NULL);
    }

    public void setIntrMora(int intrMoraIdx, AfDecimal intrMora) {
        int position = Pos.wtgaIntrMora(intrMoraIdx - 1);
        writeDecimalAsPacked(position, intrMora.copy());
    }

    /**Original name: WTGA-INTR-MORA<br>*/
    public AfDecimal getIntrMora(int intrMoraIdx) {
        int position = Pos.wtgaIntrMora(intrMoraIdx - 1);
        return readPackedAsDecimal(position, Len.Int.INTR_MORA, Len.Fract.INTR_MORA);
    }

    public void setIntrMoraNull(int intrMoraNullIdx, String intrMoraNull) {
        int position = Pos.wtgaIntrMoraNull(intrMoraNullIdx - 1);
        writeString(position, intrMoraNull, Len.INTR_MORA_NULL);
    }

    /**Original name: WTGA-INTR-MORA-NULL<br>*/
    public String getIntrMoraNull(int intrMoraNullIdx) {
        int position = Pos.wtgaIntrMoraNull(intrMoraNullIdx - 1);
        return readString(position, Len.INTR_MORA_NULL);
    }

    public void setManfeeAntic(int manfeeAnticIdx, AfDecimal manfeeAntic) {
        int position = Pos.wtgaManfeeAntic(manfeeAnticIdx - 1);
        writeDecimalAsPacked(position, manfeeAntic.copy());
    }

    /**Original name: WTGA-MANFEE-ANTIC<br>*/
    public AfDecimal getManfeeAntic(int manfeeAnticIdx) {
        int position = Pos.wtgaManfeeAntic(manfeeAnticIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MANFEE_ANTIC, Len.Fract.MANFEE_ANTIC);
    }

    public void setManfeeAnticNull(int manfeeAnticNullIdx, String manfeeAnticNull) {
        int position = Pos.wtgaManfeeAnticNull(manfeeAnticNullIdx - 1);
        writeString(position, manfeeAnticNull, Len.MANFEE_ANTIC_NULL);
    }

    /**Original name: WTGA-MANFEE-ANTIC-NULL<br>*/
    public String getManfeeAnticNull(int manfeeAnticNullIdx) {
        int position = Pos.wtgaManfeeAnticNull(manfeeAnticNullIdx - 1);
        return readString(position, Len.MANFEE_ANTIC_NULL);
    }

    public void setManfeeRicor(int manfeeRicorIdx, AfDecimal manfeeRicor) {
        int position = Pos.wtgaManfeeRicor(manfeeRicorIdx - 1);
        writeDecimalAsPacked(position, manfeeRicor.copy());
    }

    /**Original name: WTGA-MANFEE-RICOR<br>*/
    public AfDecimal getManfeeRicor(int manfeeRicorIdx) {
        int position = Pos.wtgaManfeeRicor(manfeeRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MANFEE_RICOR, Len.Fract.MANFEE_RICOR);
    }

    public void setManfeeRicorNull(int manfeeRicorNullIdx, String manfeeRicorNull) {
        int position = Pos.wtgaManfeeRicorNull(manfeeRicorNullIdx - 1);
        writeString(position, manfeeRicorNull, Len.MANFEE_RICOR_NULL);
    }

    /**Original name: WTGA-MANFEE-RICOR-NULL<br>*/
    public String getManfeeRicorNull(int manfeeRicorNullIdx) {
        int position = Pos.wtgaManfeeRicorNull(manfeeRicorNullIdx - 1);
        return readString(position, Len.MANFEE_RICOR_NULL);
    }

    public void setPreUniRivto(int preUniRivtoIdx, AfDecimal preUniRivto) {
        int position = Pos.wtgaPreUniRivto(preUniRivtoIdx - 1);
        writeDecimalAsPacked(position, preUniRivto.copy());
    }

    /**Original name: WTGA-PRE-UNI-RIVTO<br>*/
    public AfDecimal getPreUniRivto(int preUniRivtoIdx) {
        int position = Pos.wtgaPreUniRivto(preUniRivtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_UNI_RIVTO, Len.Fract.PRE_UNI_RIVTO);
    }

    public void setPreUniRivtoNull(int preUniRivtoNullIdx, String preUniRivtoNull) {
        int position = Pos.wtgaPreUniRivtoNull(preUniRivtoNullIdx - 1);
        writeString(position, preUniRivtoNull, Len.PRE_UNI_RIVTO_NULL);
    }

    /**Original name: WTGA-PRE-UNI-RIVTO-NULL<br>*/
    public String getPreUniRivtoNull(int preUniRivtoNullIdx) {
        int position = Pos.wtgaPreUniRivtoNull(preUniRivtoNullIdx - 1);
        return readString(position, Len.PRE_UNI_RIVTO_NULL);
    }

    public void setProv1aaAcq(int prov1aaAcqIdx, AfDecimal prov1aaAcq) {
        int position = Pos.wtgaProv1aaAcq(prov1aaAcqIdx - 1);
        writeDecimalAsPacked(position, prov1aaAcq.copy());
    }

    /**Original name: WTGA-PROV-1AA-ACQ<br>*/
    public AfDecimal getProv1aaAcq(int prov1aaAcqIdx) {
        int position = Pos.wtgaProv1aaAcq(prov1aaAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV1AA_ACQ, Len.Fract.PROV1AA_ACQ);
    }

    public void setProv1aaAcqNull(int prov1aaAcqNullIdx, String prov1aaAcqNull) {
        int position = Pos.wtgaProv1aaAcqNull(prov1aaAcqNullIdx - 1);
        writeString(position, prov1aaAcqNull, Len.PROV1AA_ACQ_NULL);
    }

    /**Original name: WTGA-PROV-1AA-ACQ-NULL<br>*/
    public String getProv1aaAcqNull(int prov1aaAcqNullIdx) {
        int position = Pos.wtgaProv1aaAcqNull(prov1aaAcqNullIdx - 1);
        return readString(position, Len.PROV1AA_ACQ_NULL);
    }

    public String getProv1aaAcqNullFormatted(int prov1aaAcqNullIdx) {
        return Functions.padBlanks(getProv1aaAcqNull(prov1aaAcqNullIdx), Len.PROV1AA_ACQ_NULL);
    }

    public void setProv2aaAcq(int prov2aaAcqIdx, AfDecimal prov2aaAcq) {
        int position = Pos.wtgaProv2aaAcq(prov2aaAcqIdx - 1);
        writeDecimalAsPacked(position, prov2aaAcq.copy());
    }

    /**Original name: WTGA-PROV-2AA-ACQ<br>*/
    public AfDecimal getProv2aaAcq(int prov2aaAcqIdx) {
        int position = Pos.wtgaProv2aaAcq(prov2aaAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV2AA_ACQ, Len.Fract.PROV2AA_ACQ);
    }

    public void setProv2aaAcqNull(int prov2aaAcqNullIdx, String prov2aaAcqNull) {
        int position = Pos.wtgaProv2aaAcqNull(prov2aaAcqNullIdx - 1);
        writeString(position, prov2aaAcqNull, Len.PROV2AA_ACQ_NULL);
    }

    /**Original name: WTGA-PROV-2AA-ACQ-NULL<br>*/
    public String getProv2aaAcqNull(int prov2aaAcqNullIdx) {
        int position = Pos.wtgaProv2aaAcqNull(prov2aaAcqNullIdx - 1);
        return readString(position, Len.PROV2AA_ACQ_NULL);
    }

    public String getProv2aaAcqNullFormatted(int prov2aaAcqNullIdx) {
        return Functions.padBlanks(getProv2aaAcqNull(prov2aaAcqNullIdx), Len.PROV2AA_ACQ_NULL);
    }

    public void setProvRicor(int provRicorIdx, AfDecimal provRicor) {
        int position = Pos.wtgaProvRicor(provRicorIdx - 1);
        writeDecimalAsPacked(position, provRicor.copy());
    }

    /**Original name: WTGA-PROV-RICOR<br>*/
    public AfDecimal getProvRicor(int provRicorIdx) {
        int position = Pos.wtgaProvRicor(provRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV_RICOR, Len.Fract.PROV_RICOR);
    }

    public void setProvRicorNull(int provRicorNullIdx, String provRicorNull) {
        int position = Pos.wtgaProvRicorNull(provRicorNullIdx - 1);
        writeString(position, provRicorNull, Len.PROV_RICOR_NULL);
    }

    /**Original name: WTGA-PROV-RICOR-NULL<br>*/
    public String getProvRicorNull(int provRicorNullIdx) {
        int position = Pos.wtgaProvRicorNull(provRicorNullIdx - 1);
        return readString(position, Len.PROV_RICOR_NULL);
    }

    public String getProvRicorNullFormatted(int provRicorNullIdx) {
        return Functions.padBlanks(getProvRicorNull(provRicorNullIdx), Len.PROV_RICOR_NULL);
    }

    public void setProvInc(int provIncIdx, AfDecimal provInc) {
        int position = Pos.wtgaProvInc(provIncIdx - 1);
        writeDecimalAsPacked(position, provInc.copy());
    }

    /**Original name: WTGA-PROV-INC<br>*/
    public AfDecimal getProvInc(int provIncIdx) {
        int position = Pos.wtgaProvInc(provIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV_INC, Len.Fract.PROV_INC);
    }

    public void setProvIncNull(int provIncNullIdx, String provIncNull) {
        int position = Pos.wtgaProvIncNull(provIncNullIdx - 1);
        writeString(position, provIncNull, Len.PROV_INC_NULL);
    }

    /**Original name: WTGA-PROV-INC-NULL<br>*/
    public String getProvIncNull(int provIncNullIdx) {
        int position = Pos.wtgaProvIncNull(provIncNullIdx - 1);
        return readString(position, Len.PROV_INC_NULL);
    }

    public String getProvIncNullFormatted(int provIncNullIdx) {
        return Functions.padBlanks(getProvIncNull(provIncNullIdx), Len.PROV_INC_NULL);
    }

    public void setAlqProvAcq(int alqProvAcqIdx, AfDecimal alqProvAcq) {
        int position = Pos.wtgaAlqProvAcq(alqProvAcqIdx - 1);
        writeDecimalAsPacked(position, alqProvAcq.copy());
    }

    /**Original name: WTGA-ALQ-PROV-ACQ<br>*/
    public AfDecimal getAlqProvAcq(int alqProvAcqIdx) {
        int position = Pos.wtgaAlqProvAcq(alqProvAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_PROV_ACQ, Len.Fract.ALQ_PROV_ACQ);
    }

    public void setAlqProvAcqNull(int alqProvAcqNullIdx, String alqProvAcqNull) {
        int position = Pos.wtgaAlqProvAcqNull(alqProvAcqNullIdx - 1);
        writeString(position, alqProvAcqNull, Len.ALQ_PROV_ACQ_NULL);
    }

    /**Original name: WTGA-ALQ-PROV-ACQ-NULL<br>*/
    public String getAlqProvAcqNull(int alqProvAcqNullIdx) {
        int position = Pos.wtgaAlqProvAcqNull(alqProvAcqNullIdx - 1);
        return readString(position, Len.ALQ_PROV_ACQ_NULL);
    }

    public void setAlqProvInc(int alqProvIncIdx, AfDecimal alqProvInc) {
        int position = Pos.wtgaAlqProvInc(alqProvIncIdx - 1);
        writeDecimalAsPacked(position, alqProvInc.copy());
    }

    /**Original name: WTGA-ALQ-PROV-INC<br>*/
    public AfDecimal getAlqProvInc(int alqProvIncIdx) {
        int position = Pos.wtgaAlqProvInc(alqProvIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_PROV_INC, Len.Fract.ALQ_PROV_INC);
    }

    public void setAlqProvIncNull(int alqProvIncNullIdx, String alqProvIncNull) {
        int position = Pos.wtgaAlqProvIncNull(alqProvIncNullIdx - 1);
        writeString(position, alqProvIncNull, Len.ALQ_PROV_INC_NULL);
    }

    /**Original name: WTGA-ALQ-PROV-INC-NULL<br>*/
    public String getAlqProvIncNull(int alqProvIncNullIdx) {
        int position = Pos.wtgaAlqProvIncNull(alqProvIncNullIdx - 1);
        return readString(position, Len.ALQ_PROV_INC_NULL);
    }

    public void setAlqProvRicor(int alqProvRicorIdx, AfDecimal alqProvRicor) {
        int position = Pos.wtgaAlqProvRicor(alqProvRicorIdx - 1);
        writeDecimalAsPacked(position, alqProvRicor.copy());
    }

    /**Original name: WTGA-ALQ-PROV-RICOR<br>*/
    public AfDecimal getAlqProvRicor(int alqProvRicorIdx) {
        int position = Pos.wtgaAlqProvRicor(alqProvRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_PROV_RICOR, Len.Fract.ALQ_PROV_RICOR);
    }

    public void setAlqProvRicorNull(int alqProvRicorNullIdx, String alqProvRicorNull) {
        int position = Pos.wtgaAlqProvRicorNull(alqProvRicorNullIdx - 1);
        writeString(position, alqProvRicorNull, Len.ALQ_PROV_RICOR_NULL);
    }

    /**Original name: WTGA-ALQ-PROV-RICOR-NULL<br>*/
    public String getAlqProvRicorNull(int alqProvRicorNullIdx) {
        int position = Pos.wtgaAlqProvRicorNull(alqProvRicorNullIdx - 1);
        return readString(position, Len.ALQ_PROV_RICOR_NULL);
    }

    public void setImpbProvAcq(int impbProvAcqIdx, AfDecimal impbProvAcq) {
        int position = Pos.wtgaImpbProvAcq(impbProvAcqIdx - 1);
        writeDecimalAsPacked(position, impbProvAcq.copy());
    }

    /**Original name: WTGA-IMPB-PROV-ACQ<br>*/
    public AfDecimal getImpbProvAcq(int impbProvAcqIdx) {
        int position = Pos.wtgaImpbProvAcq(impbProvAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_PROV_ACQ, Len.Fract.IMPB_PROV_ACQ);
    }

    public void setImpbProvAcqNull(int impbProvAcqNullIdx, String impbProvAcqNull) {
        int position = Pos.wtgaImpbProvAcqNull(impbProvAcqNullIdx - 1);
        writeString(position, impbProvAcqNull, Len.IMPB_PROV_ACQ_NULL);
    }

    /**Original name: WTGA-IMPB-PROV-ACQ-NULL<br>*/
    public String getImpbProvAcqNull(int impbProvAcqNullIdx) {
        int position = Pos.wtgaImpbProvAcqNull(impbProvAcqNullIdx - 1);
        return readString(position, Len.IMPB_PROV_ACQ_NULL);
    }

    public void setImpbProvInc(int impbProvIncIdx, AfDecimal impbProvInc) {
        int position = Pos.wtgaImpbProvInc(impbProvIncIdx - 1);
        writeDecimalAsPacked(position, impbProvInc.copy());
    }

    /**Original name: WTGA-IMPB-PROV-INC<br>*/
    public AfDecimal getImpbProvInc(int impbProvIncIdx) {
        int position = Pos.wtgaImpbProvInc(impbProvIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_PROV_INC, Len.Fract.IMPB_PROV_INC);
    }

    public void setImpbProvIncNull(int impbProvIncNullIdx, String impbProvIncNull) {
        int position = Pos.wtgaImpbProvIncNull(impbProvIncNullIdx - 1);
        writeString(position, impbProvIncNull, Len.IMPB_PROV_INC_NULL);
    }

    /**Original name: WTGA-IMPB-PROV-INC-NULL<br>*/
    public String getImpbProvIncNull(int impbProvIncNullIdx) {
        int position = Pos.wtgaImpbProvIncNull(impbProvIncNullIdx - 1);
        return readString(position, Len.IMPB_PROV_INC_NULL);
    }

    public void setImpbProvRicor(int impbProvRicorIdx, AfDecimal impbProvRicor) {
        int position = Pos.wtgaImpbProvRicor(impbProvRicorIdx - 1);
        writeDecimalAsPacked(position, impbProvRicor.copy());
    }

    /**Original name: WTGA-IMPB-PROV-RICOR<br>*/
    public AfDecimal getImpbProvRicor(int impbProvRicorIdx) {
        int position = Pos.wtgaImpbProvRicor(impbProvRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_PROV_RICOR, Len.Fract.IMPB_PROV_RICOR);
    }

    public void setImpbProvRicorNull(int impbProvRicorNullIdx, String impbProvRicorNull) {
        int position = Pos.wtgaImpbProvRicorNull(impbProvRicorNullIdx - 1);
        writeString(position, impbProvRicorNull, Len.IMPB_PROV_RICOR_NULL);
    }

    /**Original name: WTGA-IMPB-PROV-RICOR-NULL<br>*/
    public String getImpbProvRicorNull(int impbProvRicorNullIdx) {
        int position = Pos.wtgaImpbProvRicorNull(impbProvRicorNullIdx - 1);
        return readString(position, Len.IMPB_PROV_RICOR_NULL);
    }

    public void setFlProvForz(int flProvForzIdx, char flProvForz) {
        int position = Pos.wtgaFlProvForz(flProvForzIdx - 1);
        writeChar(position, flProvForz);
    }

    /**Original name: WTGA-FL-PROV-FORZ<br>*/
    public char getFlProvForz(int flProvForzIdx) {
        int position = Pos.wtgaFlProvForz(flProvForzIdx - 1);
        return readChar(position);
    }

    public void setFlProvForzNull(int flProvForzNullIdx, char flProvForzNull) {
        int position = Pos.wtgaFlProvForzNull(flProvForzNullIdx - 1);
        writeChar(position, flProvForzNull);
    }

    /**Original name: WTGA-FL-PROV-FORZ-NULL<br>*/
    public char getFlProvForzNull(int flProvForzNullIdx) {
        int position = Pos.wtgaFlProvForzNull(flProvForzNullIdx - 1);
        return readChar(position);
    }

    public void setPrstzAggIni(int prstzAggIniIdx, AfDecimal prstzAggIni) {
        int position = Pos.wtgaPrstzAggIni(prstzAggIniIdx - 1);
        writeDecimalAsPacked(position, prstzAggIni.copy());
    }

    /**Original name: WTGA-PRSTZ-AGG-INI<br>*/
    public AfDecimal getPrstzAggIni(int prstzAggIniIdx) {
        int position = Pos.wtgaPrstzAggIni(prstzAggIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_AGG_INI, Len.Fract.PRSTZ_AGG_INI);
    }

    public void setPrstzAggIniNull(int prstzAggIniNullIdx, String prstzAggIniNull) {
        int position = Pos.wtgaPrstzAggIniNull(prstzAggIniNullIdx - 1);
        writeString(position, prstzAggIniNull, Len.PRSTZ_AGG_INI_NULL);
    }

    /**Original name: WTGA-PRSTZ-AGG-INI-NULL<br>*/
    public String getPrstzAggIniNull(int prstzAggIniNullIdx) {
        int position = Pos.wtgaPrstzAggIniNull(prstzAggIniNullIdx - 1);
        return readString(position, Len.PRSTZ_AGG_INI_NULL);
    }

    public String getPrstzAggIniNullFormatted(int prstzAggIniNullIdx) {
        return Functions.padBlanks(getPrstzAggIniNull(prstzAggIniNullIdx), Len.PRSTZ_AGG_INI_NULL);
    }

    public void setIncrPre(int incrPreIdx, AfDecimal incrPre) {
        int position = Pos.wtgaIncrPre(incrPreIdx - 1);
        writeDecimalAsPacked(position, incrPre.copy());
    }

    /**Original name: WTGA-INCR-PRE<br>*/
    public AfDecimal getIncrPre(int incrPreIdx) {
        int position = Pos.wtgaIncrPre(incrPreIdx - 1);
        return readPackedAsDecimal(position, Len.Int.INCR_PRE, Len.Fract.INCR_PRE);
    }

    public void setIncrPreNull(int incrPreNullIdx, String incrPreNull) {
        int position = Pos.wtgaIncrPreNull(incrPreNullIdx - 1);
        writeString(position, incrPreNull, Len.INCR_PRE_NULL);
    }

    /**Original name: WTGA-INCR-PRE-NULL<br>*/
    public String getIncrPreNull(int incrPreNullIdx) {
        int position = Pos.wtgaIncrPreNull(incrPreNullIdx - 1);
        return readString(position, Len.INCR_PRE_NULL);
    }

    public void setIncrPrstz(int incrPrstzIdx, AfDecimal incrPrstz) {
        int position = Pos.wtgaIncrPrstz(incrPrstzIdx - 1);
        writeDecimalAsPacked(position, incrPrstz.copy());
    }

    /**Original name: WTGA-INCR-PRSTZ<br>*/
    public AfDecimal getIncrPrstz(int incrPrstzIdx) {
        int position = Pos.wtgaIncrPrstz(incrPrstzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.INCR_PRSTZ, Len.Fract.INCR_PRSTZ);
    }

    public void setIncrPrstzNull(int incrPrstzNullIdx, String incrPrstzNull) {
        int position = Pos.wtgaIncrPrstzNull(incrPrstzNullIdx - 1);
        writeString(position, incrPrstzNull, Len.INCR_PRSTZ_NULL);
    }

    /**Original name: WTGA-INCR-PRSTZ-NULL<br>*/
    public String getIncrPrstzNull(int incrPrstzNullIdx) {
        int position = Pos.wtgaIncrPrstzNull(incrPrstzNullIdx - 1);
        return readString(position, Len.INCR_PRSTZ_NULL);
    }

    public void setDtUltAdegPrePr(int dtUltAdegPrePrIdx, int dtUltAdegPrePr) {
        int position = Pos.wtgaDtUltAdegPrePr(dtUltAdegPrePrIdx - 1);
        writeIntAsPacked(position, dtUltAdegPrePr, Len.Int.DT_ULT_ADEG_PRE_PR);
    }

    /**Original name: WTGA-DT-ULT-ADEG-PRE-PR<br>*/
    public int getDtUltAdegPrePr(int dtUltAdegPrePrIdx) {
        int position = Pos.wtgaDtUltAdegPrePr(dtUltAdegPrePrIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_ULT_ADEG_PRE_PR);
    }

    public void setDtUltAdegPrePrNull(int dtUltAdegPrePrNullIdx, String dtUltAdegPrePrNull) {
        int position = Pos.wtgaDtUltAdegPrePrNull(dtUltAdegPrePrNullIdx - 1);
        writeString(position, dtUltAdegPrePrNull, Len.DT_ULT_ADEG_PRE_PR_NULL);
    }

    /**Original name: WTGA-DT-ULT-ADEG-PRE-PR-NULL<br>*/
    public String getDtUltAdegPrePrNull(int dtUltAdegPrePrNullIdx) {
        int position = Pos.wtgaDtUltAdegPrePrNull(dtUltAdegPrePrNullIdx - 1);
        return readString(position, Len.DT_ULT_ADEG_PRE_PR_NULL);
    }

    public String getDtUltAdegPrePrNullFormatted(int dtUltAdegPrePrNullIdx) {
        return Functions.padBlanks(getDtUltAdegPrePrNull(dtUltAdegPrePrNullIdx), Len.DT_ULT_ADEG_PRE_PR_NULL);
    }

    public void setPrstzAggUlt(int prstzAggUltIdx, AfDecimal prstzAggUlt) {
        int position = Pos.wtgaPrstzAggUlt(prstzAggUltIdx - 1);
        writeDecimalAsPacked(position, prstzAggUlt.copy());
    }

    /**Original name: WTGA-PRSTZ-AGG-ULT<br>*/
    public AfDecimal getPrstzAggUlt(int prstzAggUltIdx) {
        int position = Pos.wtgaPrstzAggUlt(prstzAggUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_AGG_ULT, Len.Fract.PRSTZ_AGG_ULT);
    }

    public void setPrstzAggUltNull(int prstzAggUltNullIdx, String prstzAggUltNull) {
        int position = Pos.wtgaPrstzAggUltNull(prstzAggUltNullIdx - 1);
        writeString(position, prstzAggUltNull, Len.PRSTZ_AGG_ULT_NULL);
    }

    /**Original name: WTGA-PRSTZ-AGG-ULT-NULL<br>*/
    public String getPrstzAggUltNull(int prstzAggUltNullIdx) {
        int position = Pos.wtgaPrstzAggUltNull(prstzAggUltNullIdx - 1);
        return readString(position, Len.PRSTZ_AGG_ULT_NULL);
    }

    public void setTsRivalNet(int tsRivalNetIdx, AfDecimal tsRivalNet) {
        int position = Pos.wtgaTsRivalNet(tsRivalNetIdx - 1);
        writeDecimalAsPacked(position, tsRivalNet.copy());
    }

    /**Original name: WTGA-TS-RIVAL-NET<br>*/
    public AfDecimal getTsRivalNet(int tsRivalNetIdx) {
        int position = Pos.wtgaTsRivalNet(tsRivalNetIdx - 1);
        return readPackedAsDecimal(position, Len.Int.TS_RIVAL_NET, Len.Fract.TS_RIVAL_NET);
    }

    public void setTsRivalNetNull(int tsRivalNetNullIdx, String tsRivalNetNull) {
        int position = Pos.wtgaTsRivalNetNull(tsRivalNetNullIdx - 1);
        writeString(position, tsRivalNetNull, Len.TS_RIVAL_NET_NULL);
    }

    /**Original name: WTGA-TS-RIVAL-NET-NULL<br>*/
    public String getTsRivalNetNull(int tsRivalNetNullIdx) {
        int position = Pos.wtgaTsRivalNetNull(tsRivalNetNullIdx - 1);
        return readString(position, Len.TS_RIVAL_NET_NULL);
    }

    public void setPrePattuito(int prePattuitoIdx, AfDecimal prePattuito) {
        int position = Pos.wtgaPrePattuito(prePattuitoIdx - 1);
        writeDecimalAsPacked(position, prePattuito.copy());
    }

    /**Original name: WTGA-PRE-PATTUITO<br>*/
    public AfDecimal getPrePattuito(int prePattuitoIdx) {
        int position = Pos.wtgaPrePattuito(prePattuitoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_PATTUITO, Len.Fract.PRE_PATTUITO);
    }

    public void setPrePattuitoNull(int prePattuitoNullIdx, String prePattuitoNull) {
        int position = Pos.wtgaPrePattuitoNull(prePattuitoNullIdx - 1);
        writeString(position, prePattuitoNull, Len.PRE_PATTUITO_NULL);
    }

    /**Original name: WTGA-PRE-PATTUITO-NULL<br>*/
    public String getPrePattuitoNull(int prePattuitoNullIdx) {
        int position = Pos.wtgaPrePattuitoNull(prePattuitoNullIdx - 1);
        return readString(position, Len.PRE_PATTUITO_NULL);
    }

    public String getPrePattuitoNullFormatted(int prePattuitoNullIdx) {
        return Functions.padBlanks(getPrePattuitoNull(prePattuitoNullIdx), Len.PRE_PATTUITO_NULL);
    }

    public void setTpRival(int tpRivalIdx, String tpRival) {
        int position = Pos.wtgaTpRival(tpRivalIdx - 1);
        writeString(position, tpRival, Len.TP_RIVAL);
    }

    /**Original name: WTGA-TP-RIVAL<br>*/
    public String getTpRival(int tpRivalIdx) {
        int position = Pos.wtgaTpRival(tpRivalIdx - 1);
        return readString(position, Len.TP_RIVAL);
    }

    public void setTpRivalNull(int tpRivalNullIdx, String tpRivalNull) {
        int position = Pos.wtgaTpRivalNull(tpRivalNullIdx - 1);
        writeString(position, tpRivalNull, Len.TP_RIVAL_NULL);
    }

    /**Original name: WTGA-TP-RIVAL-NULL<br>*/
    public String getTpRivalNull(int tpRivalNullIdx) {
        int position = Pos.wtgaTpRivalNull(tpRivalNullIdx - 1);
        return readString(position, Len.TP_RIVAL_NULL);
    }

    public void setRisMat(int risMatIdx, AfDecimal risMat) {
        int position = Pos.wtgaRisMat(risMatIdx - 1);
        writeDecimalAsPacked(position, risMat.copy());
    }

    /**Original name: WTGA-RIS-MAT<br>*/
    public AfDecimal getRisMat(int risMatIdx) {
        int position = Pos.wtgaRisMat(risMatIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RIS_MAT, Len.Fract.RIS_MAT);
    }

    public void setRisMatNull(int risMatNullIdx, String risMatNull) {
        int position = Pos.wtgaRisMatNull(risMatNullIdx - 1);
        writeString(position, risMatNull, Len.RIS_MAT_NULL);
    }

    /**Original name: WTGA-RIS-MAT-NULL<br>*/
    public String getRisMatNull(int risMatNullIdx) {
        int position = Pos.wtgaRisMatNull(risMatNullIdx - 1);
        return readString(position, Len.RIS_MAT_NULL);
    }

    public void setCptMinScad(int cptMinScadIdx, AfDecimal cptMinScad) {
        int position = Pos.wtgaCptMinScad(cptMinScadIdx - 1);
        writeDecimalAsPacked(position, cptMinScad.copy());
    }

    /**Original name: WTGA-CPT-MIN-SCAD<br>*/
    public AfDecimal getCptMinScad(int cptMinScadIdx) {
        int position = Pos.wtgaCptMinScad(cptMinScadIdx - 1);
        return readPackedAsDecimal(position, Len.Int.CPT_MIN_SCAD, Len.Fract.CPT_MIN_SCAD);
    }

    public void setCptMinScadNull(int cptMinScadNullIdx, String cptMinScadNull) {
        int position = Pos.wtgaCptMinScadNull(cptMinScadNullIdx - 1);
        writeString(position, cptMinScadNull, Len.CPT_MIN_SCAD_NULL);
    }

    /**Original name: WTGA-CPT-MIN-SCAD-NULL<br>*/
    public String getCptMinScadNull(int cptMinScadNullIdx) {
        int position = Pos.wtgaCptMinScadNull(cptMinScadNullIdx - 1);
        return readString(position, Len.CPT_MIN_SCAD_NULL);
    }

    public void setCommisGest(int commisGestIdx, AfDecimal commisGest) {
        int position = Pos.wtgaCommisGest(commisGestIdx - 1);
        writeDecimalAsPacked(position, commisGest.copy());
    }

    /**Original name: WTGA-COMMIS-GEST<br>*/
    public AfDecimal getCommisGest(int commisGestIdx) {
        int position = Pos.wtgaCommisGest(commisGestIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COMMIS_GEST, Len.Fract.COMMIS_GEST);
    }

    public void setCommisGestNull(int commisGestNullIdx, String commisGestNull) {
        int position = Pos.wtgaCommisGestNull(commisGestNullIdx - 1);
        writeString(position, commisGestNull, Len.COMMIS_GEST_NULL);
    }

    /**Original name: WTGA-COMMIS-GEST-NULL<br>*/
    public String getCommisGestNull(int commisGestNullIdx) {
        int position = Pos.wtgaCommisGestNull(commisGestNullIdx - 1);
        return readString(position, Len.COMMIS_GEST_NULL);
    }

    public void setTpManfeeAppl(int tpManfeeApplIdx, String tpManfeeAppl) {
        int position = Pos.wtgaTpManfeeAppl(tpManfeeApplIdx - 1);
        writeString(position, tpManfeeAppl, Len.TP_MANFEE_APPL);
    }

    /**Original name: WTGA-TP-MANFEE-APPL<br>*/
    public String getTpManfeeAppl(int tpManfeeApplIdx) {
        int position = Pos.wtgaTpManfeeAppl(tpManfeeApplIdx - 1);
        return readString(position, Len.TP_MANFEE_APPL);
    }

    public void setTpManfeeApplNull(int tpManfeeApplNullIdx, String tpManfeeApplNull) {
        int position = Pos.wtgaTpManfeeApplNull(tpManfeeApplNullIdx - 1);
        writeString(position, tpManfeeApplNull, Len.TP_MANFEE_APPL_NULL);
    }

    /**Original name: WTGA-TP-MANFEE-APPL-NULL<br>*/
    public String getTpManfeeApplNull(int tpManfeeApplNullIdx) {
        int position = Pos.wtgaTpManfeeApplNull(tpManfeeApplNullIdx - 1);
        return readString(position, Len.TP_MANFEE_APPL_NULL);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.wtgaDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: WTGA-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.wtgaDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.wtgaDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: WTGA-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.wtgaDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.wtgaDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: WTGA-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.wtgaDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.wtgaDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: WTGA-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.wtgaDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.wtgaDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: WTGA-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.wtgaDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.wtgaDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: WTGA-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.wtgaDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.wtgaDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: WTGA-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.wtgaDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setPcCommisGest(int pcCommisGestIdx, AfDecimal pcCommisGest) {
        int position = Pos.wtgaPcCommisGest(pcCommisGestIdx - 1);
        writeDecimalAsPacked(position, pcCommisGest.copy());
    }

    /**Original name: WTGA-PC-COMMIS-GEST<br>*/
    public AfDecimal getPcCommisGest(int pcCommisGestIdx) {
        int position = Pos.wtgaPcCommisGest(pcCommisGestIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_COMMIS_GEST, Len.Fract.PC_COMMIS_GEST);
    }

    public void setPcCommisGestNull(int pcCommisGestNullIdx, String pcCommisGestNull) {
        int position = Pos.wtgaPcCommisGestNull(pcCommisGestNullIdx - 1);
        writeString(position, pcCommisGestNull, Len.PC_COMMIS_GEST_NULL);
    }

    /**Original name: WTGA-PC-COMMIS-GEST-NULL<br>*/
    public String getPcCommisGestNull(int pcCommisGestNullIdx) {
        int position = Pos.wtgaPcCommisGestNull(pcCommisGestNullIdx - 1);
        return readString(position, Len.PC_COMMIS_GEST_NULL);
    }

    public void setNumGgRival(int numGgRivalIdx, int numGgRival) {
        int position = Pos.wtgaNumGgRival(numGgRivalIdx - 1);
        writeIntAsPacked(position, numGgRival, Len.Int.NUM_GG_RIVAL);
    }

    /**Original name: WTGA-NUM-GG-RIVAL<br>*/
    public int getNumGgRival(int numGgRivalIdx) {
        int position = Pos.wtgaNumGgRival(numGgRivalIdx - 1);
        return readPackedAsInt(position, Len.Int.NUM_GG_RIVAL);
    }

    public void setNumGgRivalNull(int numGgRivalNullIdx, String numGgRivalNull) {
        int position = Pos.wtgaNumGgRivalNull(numGgRivalNullIdx - 1);
        writeString(position, numGgRivalNull, Len.NUM_GG_RIVAL_NULL);
    }

    /**Original name: WTGA-NUM-GG-RIVAL-NULL<br>*/
    public String getNumGgRivalNull(int numGgRivalNullIdx) {
        int position = Pos.wtgaNumGgRivalNull(numGgRivalNullIdx - 1);
        return readString(position, Len.NUM_GG_RIVAL_NULL);
    }

    public void setImpTrasfe(int impTrasfeIdx, AfDecimal impTrasfe) {
        int position = Pos.wtgaImpTrasfe(impTrasfeIdx - 1);
        writeDecimalAsPacked(position, impTrasfe.copy());
    }

    /**Original name: WTGA-IMP-TRASFE<br>*/
    public AfDecimal getImpTrasfe(int impTrasfeIdx) {
        int position = Pos.wtgaImpTrasfe(impTrasfeIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_TRASFE, Len.Fract.IMP_TRASFE);
    }

    public void setImpTrasfeNull(int impTrasfeNullIdx, String impTrasfeNull) {
        int position = Pos.wtgaImpTrasfeNull(impTrasfeNullIdx - 1);
        writeString(position, impTrasfeNull, Len.IMP_TRASFE_NULL);
    }

    /**Original name: WTGA-IMP-TRASFE-NULL<br>*/
    public String getImpTrasfeNull(int impTrasfeNullIdx) {
        int position = Pos.wtgaImpTrasfeNull(impTrasfeNullIdx - 1);
        return readString(position, Len.IMP_TRASFE_NULL);
    }

    public void setImpTfrStrc(int impTfrStrcIdx, AfDecimal impTfrStrc) {
        int position = Pos.wtgaImpTfrStrc(impTfrStrcIdx - 1);
        writeDecimalAsPacked(position, impTfrStrc.copy());
    }

    /**Original name: WTGA-IMP-TFR-STRC<br>*/
    public AfDecimal getImpTfrStrc(int impTfrStrcIdx) {
        int position = Pos.wtgaImpTfrStrc(impTfrStrcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_TFR_STRC, Len.Fract.IMP_TFR_STRC);
    }

    public void setImpTfrStrcNull(int impTfrStrcNullIdx, String impTfrStrcNull) {
        int position = Pos.wtgaImpTfrStrcNull(impTfrStrcNullIdx - 1);
        writeString(position, impTfrStrcNull, Len.IMP_TFR_STRC_NULL);
    }

    /**Original name: WTGA-IMP-TFR-STRC-NULL<br>*/
    public String getImpTfrStrcNull(int impTfrStrcNullIdx) {
        int position = Pos.wtgaImpTfrStrcNull(impTfrStrcNullIdx - 1);
        return readString(position, Len.IMP_TFR_STRC_NULL);
    }

    public void setAcqExp(int acqExpIdx, AfDecimal acqExp) {
        int position = Pos.wtgaAcqExp(acqExpIdx - 1);
        writeDecimalAsPacked(position, acqExp.copy());
    }

    /**Original name: WTGA-ACQ-EXP<br>*/
    public AfDecimal getAcqExp(int acqExpIdx) {
        int position = Pos.wtgaAcqExp(acqExpIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ACQ_EXP, Len.Fract.ACQ_EXP);
    }

    public void setAcqExpNull(int acqExpNullIdx, String acqExpNull) {
        int position = Pos.wtgaAcqExpNull(acqExpNullIdx - 1);
        writeString(position, acqExpNull, Len.ACQ_EXP_NULL);
    }

    /**Original name: WTGA-ACQ-EXP-NULL<br>*/
    public String getAcqExpNull(int acqExpNullIdx) {
        int position = Pos.wtgaAcqExpNull(acqExpNullIdx - 1);
        return readString(position, Len.ACQ_EXP_NULL);
    }

    public String getAcqExpNullFormatted(int acqExpNullIdx) {
        return Functions.padBlanks(getAcqExpNull(acqExpNullIdx), Len.ACQ_EXP_NULL);
    }

    public void setRemunAss(int remunAssIdx, AfDecimal remunAss) {
        int position = Pos.wtgaRemunAss(remunAssIdx - 1);
        writeDecimalAsPacked(position, remunAss.copy());
    }

    /**Original name: WTGA-REMUN-ASS<br>*/
    public AfDecimal getRemunAss(int remunAssIdx) {
        int position = Pos.wtgaRemunAss(remunAssIdx - 1);
        return readPackedAsDecimal(position, Len.Int.REMUN_ASS, Len.Fract.REMUN_ASS);
    }

    public void setRemunAssNull(int remunAssNullIdx, String remunAssNull) {
        int position = Pos.wtgaRemunAssNull(remunAssNullIdx - 1);
        writeString(position, remunAssNull, Len.REMUN_ASS_NULL);
    }

    /**Original name: WTGA-REMUN-ASS-NULL<br>*/
    public String getRemunAssNull(int remunAssNullIdx) {
        int position = Pos.wtgaRemunAssNull(remunAssNullIdx - 1);
        return readString(position, Len.REMUN_ASS_NULL);
    }

    public String getRemunAssNullFormatted(int remunAssNullIdx) {
        return Functions.padBlanks(getRemunAssNull(remunAssNullIdx), Len.REMUN_ASS_NULL);
    }

    public void setCommisInter(int commisInterIdx, AfDecimal commisInter) {
        int position = Pos.wtgaCommisInter(commisInterIdx - 1);
        writeDecimalAsPacked(position, commisInter.copy());
    }

    /**Original name: WTGA-COMMIS-INTER<br>*/
    public AfDecimal getCommisInter(int commisInterIdx) {
        int position = Pos.wtgaCommisInter(commisInterIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COMMIS_INTER, Len.Fract.COMMIS_INTER);
    }

    public void setCommisInterNull(int commisInterNullIdx, String commisInterNull) {
        int position = Pos.wtgaCommisInterNull(commisInterNullIdx - 1);
        writeString(position, commisInterNull, Len.COMMIS_INTER_NULL);
    }

    /**Original name: WTGA-COMMIS-INTER-NULL<br>*/
    public String getCommisInterNull(int commisInterNullIdx) {
        int position = Pos.wtgaCommisInterNull(commisInterNullIdx - 1);
        return readString(position, Len.COMMIS_INTER_NULL);
    }

    public String getCommisInterNullFormatted(int commisInterNullIdx) {
        return Functions.padBlanks(getCommisInterNull(commisInterNullIdx), Len.COMMIS_INTER_NULL);
    }

    public void setAlqRemunAss(int alqRemunAssIdx, AfDecimal alqRemunAss) {
        int position = Pos.wtgaAlqRemunAss(alqRemunAssIdx - 1);
        writeDecimalAsPacked(position, alqRemunAss.copy());
    }

    /**Original name: WTGA-ALQ-REMUN-ASS<br>*/
    public AfDecimal getAlqRemunAss(int alqRemunAssIdx) {
        int position = Pos.wtgaAlqRemunAss(alqRemunAssIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_REMUN_ASS, Len.Fract.ALQ_REMUN_ASS);
    }

    public void setAlqRemunAssNull(int alqRemunAssNullIdx, String alqRemunAssNull) {
        int position = Pos.wtgaAlqRemunAssNull(alqRemunAssNullIdx - 1);
        writeString(position, alqRemunAssNull, Len.ALQ_REMUN_ASS_NULL);
    }

    /**Original name: WTGA-ALQ-REMUN-ASS-NULL<br>*/
    public String getAlqRemunAssNull(int alqRemunAssNullIdx) {
        int position = Pos.wtgaAlqRemunAssNull(alqRemunAssNullIdx - 1);
        return readString(position, Len.ALQ_REMUN_ASS_NULL);
    }

    public void setAlqCommisInter(int alqCommisInterIdx, AfDecimal alqCommisInter) {
        int position = Pos.wtgaAlqCommisInter(alqCommisInterIdx - 1);
        writeDecimalAsPacked(position, alqCommisInter.copy());
    }

    /**Original name: WTGA-ALQ-COMMIS-INTER<br>*/
    public AfDecimal getAlqCommisInter(int alqCommisInterIdx) {
        int position = Pos.wtgaAlqCommisInter(alqCommisInterIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_COMMIS_INTER, Len.Fract.ALQ_COMMIS_INTER);
    }

    public void setAlqCommisInterNull(int alqCommisInterNullIdx, String alqCommisInterNull) {
        int position = Pos.wtgaAlqCommisInterNull(alqCommisInterNullIdx - 1);
        writeString(position, alqCommisInterNull, Len.ALQ_COMMIS_INTER_NULL);
    }

    /**Original name: WTGA-ALQ-COMMIS-INTER-NULL<br>*/
    public String getAlqCommisInterNull(int alqCommisInterNullIdx) {
        int position = Pos.wtgaAlqCommisInterNull(alqCommisInterNullIdx - 1);
        return readString(position, Len.ALQ_COMMIS_INTER_NULL);
    }

    public void setImpbRemunAss(int impbRemunAssIdx, AfDecimal impbRemunAss) {
        int position = Pos.wtgaImpbRemunAss(impbRemunAssIdx - 1);
        writeDecimalAsPacked(position, impbRemunAss.copy());
    }

    /**Original name: WTGA-IMPB-REMUN-ASS<br>*/
    public AfDecimal getImpbRemunAss(int impbRemunAssIdx) {
        int position = Pos.wtgaImpbRemunAss(impbRemunAssIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_REMUN_ASS, Len.Fract.IMPB_REMUN_ASS);
    }

    public void setImpbRemunAssNull(int impbRemunAssNullIdx, String impbRemunAssNull) {
        int position = Pos.wtgaImpbRemunAssNull(impbRemunAssNullIdx - 1);
        writeString(position, impbRemunAssNull, Len.IMPB_REMUN_ASS_NULL);
    }

    /**Original name: WTGA-IMPB-REMUN-ASS-NULL<br>*/
    public String getImpbRemunAssNull(int impbRemunAssNullIdx) {
        int position = Pos.wtgaImpbRemunAssNull(impbRemunAssNullIdx - 1);
        return readString(position, Len.IMPB_REMUN_ASS_NULL);
    }

    public void setImpbCommisInter(int impbCommisInterIdx, AfDecimal impbCommisInter) {
        int position = Pos.wtgaImpbCommisInter(impbCommisInterIdx - 1);
        writeDecimalAsPacked(position, impbCommisInter.copy());
    }

    /**Original name: WTGA-IMPB-COMMIS-INTER<br>*/
    public AfDecimal getImpbCommisInter(int impbCommisInterIdx) {
        int position = Pos.wtgaImpbCommisInter(impbCommisInterIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_COMMIS_INTER, Len.Fract.IMPB_COMMIS_INTER);
    }

    public void setImpbCommisInterNull(int impbCommisInterNullIdx, String impbCommisInterNull) {
        int position = Pos.wtgaImpbCommisInterNull(impbCommisInterNullIdx - 1);
        writeString(position, impbCommisInterNull, Len.IMPB_COMMIS_INTER_NULL);
    }

    /**Original name: WTGA-IMPB-COMMIS-INTER-NULL<br>*/
    public String getImpbCommisInterNull(int impbCommisInterNullIdx) {
        int position = Pos.wtgaImpbCommisInterNull(impbCommisInterNullIdx - 1);
        return readString(position, Len.IMPB_COMMIS_INTER_NULL);
    }

    public void setCosRunAssva(int cosRunAssvaIdx, AfDecimal cosRunAssva) {
        int position = Pos.wtgaCosRunAssva(cosRunAssvaIdx - 1);
        writeDecimalAsPacked(position, cosRunAssva.copy());
    }

    /**Original name: WTGA-COS-RUN-ASSVA<br>*/
    public AfDecimal getCosRunAssva(int cosRunAssvaIdx) {
        int position = Pos.wtgaCosRunAssva(cosRunAssvaIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COS_RUN_ASSVA, Len.Fract.COS_RUN_ASSVA);
    }

    public void setCosRunAssvaNull(int cosRunAssvaNullIdx, String cosRunAssvaNull) {
        int position = Pos.wtgaCosRunAssvaNull(cosRunAssvaNullIdx - 1);
        writeString(position, cosRunAssvaNull, Len.COS_RUN_ASSVA_NULL);
    }

    /**Original name: WTGA-COS-RUN-ASSVA-NULL<br>*/
    public String getCosRunAssvaNull(int cosRunAssvaNullIdx) {
        int position = Pos.wtgaCosRunAssvaNull(cosRunAssvaNullIdx - 1);
        return readString(position, Len.COS_RUN_ASSVA_NULL);
    }

    public void setCosRunAssvaIdc(int cosRunAssvaIdcIdx, AfDecimal cosRunAssvaIdc) {
        int position = Pos.wtgaCosRunAssvaIdc(cosRunAssvaIdcIdx - 1);
        writeDecimalAsPacked(position, cosRunAssvaIdc.copy());
    }

    /**Original name: WTGA-COS-RUN-ASSVA-IDC<br>*/
    public AfDecimal getCosRunAssvaIdc(int cosRunAssvaIdcIdx) {
        int position = Pos.wtgaCosRunAssvaIdc(cosRunAssvaIdcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COS_RUN_ASSVA_IDC, Len.Fract.COS_RUN_ASSVA_IDC);
    }

    public void setCosRunAssvaIdcNull(int cosRunAssvaIdcNullIdx, String cosRunAssvaIdcNull) {
        int position = Pos.wtgaCosRunAssvaIdcNull(cosRunAssvaIdcNullIdx - 1);
        writeString(position, cosRunAssvaIdcNull, Len.COS_RUN_ASSVA_IDC_NULL);
    }

    /**Original name: WTGA-COS-RUN-ASSVA-IDC-NULL<br>*/
    public String getCosRunAssvaIdcNull(int cosRunAssvaIdcNullIdx) {
        int position = Pos.wtgaCosRunAssvaIdcNull(cosRunAssvaIdcNullIdx - 1);
        return readString(position, Len.COS_RUN_ASSVA_IDC_NULL);
    }

    public void setRestoTab(String restoTab) {
        writeString(Pos.RESTO_TAB, restoTab, Len.RESTO_TAB);
    }

    /**Original name: WTGA-RESTO-TAB<br>*/
    public String getRestoTab() {
        return readString(Pos.RESTO_TAB, Len.RESTO_TAB);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_TAB = 1;
        public static final int WTGA_TAB_R = 1;
        public static final int FLR1 = WTGA_TAB_R;
        public static final int RESTO_TAB = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wtgaTabTran(int idx) {
            return WTGA_TAB + idx * Len.TAB_TRAN;
        }

        public static int wtgaStatus(int idx) {
            return wtgaTabTran(idx);
        }

        public static int wtgaIdPtf(int idx) {
            return wtgaStatus(idx) + Len.STATUS;
        }

        public static int wtgaDati(int idx) {
            return wtgaIdPtf(idx) + Len.ID_PTF;
        }

        public static int wtgaIdTrchDiGar(int idx) {
            return wtgaDati(idx);
        }

        public static int wtgaIdGar(int idx) {
            return wtgaIdTrchDiGar(idx) + Len.ID_TRCH_DI_GAR;
        }

        public static int wtgaIdAdes(int idx) {
            return wtgaIdGar(idx) + Len.ID_GAR;
        }

        public static int wtgaIdPoli(int idx) {
            return wtgaIdAdes(idx) + Len.ID_ADES;
        }

        public static int wtgaIdMoviCrz(int idx) {
            return wtgaIdPoli(idx) + Len.ID_POLI;
        }

        public static int wtgaIdMoviChiu(int idx) {
            return wtgaIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int wtgaIdMoviChiuNull(int idx) {
            return wtgaIdMoviChiu(idx);
        }

        public static int wtgaDtIniEff(int idx) {
            return wtgaIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int wtgaDtEndEff(int idx) {
            return wtgaDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int wtgaCodCompAnia(int idx) {
            return wtgaDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int wtgaDtDecor(int idx) {
            return wtgaCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wtgaDtScad(int idx) {
            return wtgaDtDecor(idx) + Len.DT_DECOR;
        }

        public static int wtgaDtScadNull(int idx) {
            return wtgaDtScad(idx);
        }

        public static int wtgaIbOgg(int idx) {
            return wtgaDtScad(idx) + Len.DT_SCAD;
        }

        public static int wtgaIbOggNull(int idx) {
            return wtgaIbOgg(idx);
        }

        public static int wtgaTpRgmFisc(int idx) {
            return wtgaIbOgg(idx) + Len.IB_OGG;
        }

        public static int wtgaDtEmis(int idx) {
            return wtgaTpRgmFisc(idx) + Len.TP_RGM_FISC;
        }

        public static int wtgaDtEmisNull(int idx) {
            return wtgaDtEmis(idx);
        }

        public static int wtgaTpTrch(int idx) {
            return wtgaDtEmis(idx) + Len.DT_EMIS;
        }

        public static int wtgaDurAa(int idx) {
            return wtgaTpTrch(idx) + Len.TP_TRCH;
        }

        public static int wtgaDurAaNull(int idx) {
            return wtgaDurAa(idx);
        }

        public static int wtgaDurMm(int idx) {
            return wtgaDurAa(idx) + Len.DUR_AA;
        }

        public static int wtgaDurMmNull(int idx) {
            return wtgaDurMm(idx);
        }

        public static int wtgaDurGg(int idx) {
            return wtgaDurMm(idx) + Len.DUR_MM;
        }

        public static int wtgaDurGgNull(int idx) {
            return wtgaDurGg(idx);
        }

        public static int wtgaPreCasoMor(int idx) {
            return wtgaDurGg(idx) + Len.DUR_GG;
        }

        public static int wtgaPreCasoMorNull(int idx) {
            return wtgaPreCasoMor(idx);
        }

        public static int wtgaPcIntrRiat(int idx) {
            return wtgaPreCasoMor(idx) + Len.PRE_CASO_MOR;
        }

        public static int wtgaPcIntrRiatNull(int idx) {
            return wtgaPcIntrRiat(idx);
        }

        public static int wtgaImpBnsAntic(int idx) {
            return wtgaPcIntrRiat(idx) + Len.PC_INTR_RIAT;
        }

        public static int wtgaImpBnsAnticNull(int idx) {
            return wtgaImpBnsAntic(idx);
        }

        public static int wtgaPreIniNet(int idx) {
            return wtgaImpBnsAntic(idx) + Len.IMP_BNS_ANTIC;
        }

        public static int wtgaPreIniNetNull(int idx) {
            return wtgaPreIniNet(idx);
        }

        public static int wtgaPrePpIni(int idx) {
            return wtgaPreIniNet(idx) + Len.PRE_INI_NET;
        }

        public static int wtgaPrePpIniNull(int idx) {
            return wtgaPrePpIni(idx);
        }

        public static int wtgaPrePpUlt(int idx) {
            return wtgaPrePpIni(idx) + Len.PRE_PP_INI;
        }

        public static int wtgaPrePpUltNull(int idx) {
            return wtgaPrePpUlt(idx);
        }

        public static int wtgaPreTariIni(int idx) {
            return wtgaPrePpUlt(idx) + Len.PRE_PP_ULT;
        }

        public static int wtgaPreTariIniNull(int idx) {
            return wtgaPreTariIni(idx);
        }

        public static int wtgaPreTariUlt(int idx) {
            return wtgaPreTariIni(idx) + Len.PRE_TARI_INI;
        }

        public static int wtgaPreTariUltNull(int idx) {
            return wtgaPreTariUlt(idx);
        }

        public static int wtgaPreInvrioIni(int idx) {
            return wtgaPreTariUlt(idx) + Len.PRE_TARI_ULT;
        }

        public static int wtgaPreInvrioIniNull(int idx) {
            return wtgaPreInvrioIni(idx);
        }

        public static int wtgaPreInvrioUlt(int idx) {
            return wtgaPreInvrioIni(idx) + Len.PRE_INVRIO_INI;
        }

        public static int wtgaPreInvrioUltNull(int idx) {
            return wtgaPreInvrioUlt(idx);
        }

        public static int wtgaPreRivto(int idx) {
            return wtgaPreInvrioUlt(idx) + Len.PRE_INVRIO_ULT;
        }

        public static int wtgaPreRivtoNull(int idx) {
            return wtgaPreRivto(idx);
        }

        public static int wtgaImpSoprProf(int idx) {
            return wtgaPreRivto(idx) + Len.PRE_RIVTO;
        }

        public static int wtgaImpSoprProfNull(int idx) {
            return wtgaImpSoprProf(idx);
        }

        public static int wtgaImpSoprSan(int idx) {
            return wtgaImpSoprProf(idx) + Len.IMP_SOPR_PROF;
        }

        public static int wtgaImpSoprSanNull(int idx) {
            return wtgaImpSoprSan(idx);
        }

        public static int wtgaImpSoprSpo(int idx) {
            return wtgaImpSoprSan(idx) + Len.IMP_SOPR_SAN;
        }

        public static int wtgaImpSoprSpoNull(int idx) {
            return wtgaImpSoprSpo(idx);
        }

        public static int wtgaImpSoprTec(int idx) {
            return wtgaImpSoprSpo(idx) + Len.IMP_SOPR_SPO;
        }

        public static int wtgaImpSoprTecNull(int idx) {
            return wtgaImpSoprTec(idx);
        }

        public static int wtgaImpAltSopr(int idx) {
            return wtgaImpSoprTec(idx) + Len.IMP_SOPR_TEC;
        }

        public static int wtgaImpAltSoprNull(int idx) {
            return wtgaImpAltSopr(idx);
        }

        public static int wtgaPreStab(int idx) {
            return wtgaImpAltSopr(idx) + Len.IMP_ALT_SOPR;
        }

        public static int wtgaPreStabNull(int idx) {
            return wtgaPreStab(idx);
        }

        public static int wtgaDtEffStab(int idx) {
            return wtgaPreStab(idx) + Len.PRE_STAB;
        }

        public static int wtgaDtEffStabNull(int idx) {
            return wtgaDtEffStab(idx);
        }

        public static int wtgaTsRivalFis(int idx) {
            return wtgaDtEffStab(idx) + Len.DT_EFF_STAB;
        }

        public static int wtgaTsRivalFisNull(int idx) {
            return wtgaTsRivalFis(idx);
        }

        public static int wtgaTsRivalIndiciz(int idx) {
            return wtgaTsRivalFis(idx) + Len.TS_RIVAL_FIS;
        }

        public static int wtgaTsRivalIndicizNull(int idx) {
            return wtgaTsRivalIndiciz(idx);
        }

        public static int wtgaOldTsTec(int idx) {
            return wtgaTsRivalIndiciz(idx) + Len.TS_RIVAL_INDICIZ;
        }

        public static int wtgaOldTsTecNull(int idx) {
            return wtgaOldTsTec(idx);
        }

        public static int wtgaRatLrd(int idx) {
            return wtgaOldTsTec(idx) + Len.OLD_TS_TEC;
        }

        public static int wtgaRatLrdNull(int idx) {
            return wtgaRatLrd(idx);
        }

        public static int wtgaPreLrd(int idx) {
            return wtgaRatLrd(idx) + Len.RAT_LRD;
        }

        public static int wtgaPreLrdNull(int idx) {
            return wtgaPreLrd(idx);
        }

        public static int wtgaPrstzIni(int idx) {
            return wtgaPreLrd(idx) + Len.PRE_LRD;
        }

        public static int wtgaPrstzIniNull(int idx) {
            return wtgaPrstzIni(idx);
        }

        public static int wtgaPrstzUlt(int idx) {
            return wtgaPrstzIni(idx) + Len.PRSTZ_INI;
        }

        public static int wtgaPrstzUltNull(int idx) {
            return wtgaPrstzUlt(idx);
        }

        public static int wtgaCptInOpzRivto(int idx) {
            return wtgaPrstzUlt(idx) + Len.PRSTZ_ULT;
        }

        public static int wtgaCptInOpzRivtoNull(int idx) {
            return wtgaCptInOpzRivto(idx);
        }

        public static int wtgaPrstzIniStab(int idx) {
            return wtgaCptInOpzRivto(idx) + Len.CPT_IN_OPZ_RIVTO;
        }

        public static int wtgaPrstzIniStabNull(int idx) {
            return wtgaPrstzIniStab(idx);
        }

        public static int wtgaCptRshMor(int idx) {
            return wtgaPrstzIniStab(idx) + Len.PRSTZ_INI_STAB;
        }

        public static int wtgaCptRshMorNull(int idx) {
            return wtgaCptRshMor(idx);
        }

        public static int wtgaPrstzRidIni(int idx) {
            return wtgaCptRshMor(idx) + Len.CPT_RSH_MOR;
        }

        public static int wtgaPrstzRidIniNull(int idx) {
            return wtgaPrstzRidIni(idx);
        }

        public static int wtgaFlCarCont(int idx) {
            return wtgaPrstzRidIni(idx) + Len.PRSTZ_RID_INI;
        }

        public static int wtgaFlCarContNull(int idx) {
            return wtgaFlCarCont(idx);
        }

        public static int wtgaBnsGiaLiqto(int idx) {
            return wtgaFlCarCont(idx) + Len.FL_CAR_CONT;
        }

        public static int wtgaBnsGiaLiqtoNull(int idx) {
            return wtgaBnsGiaLiqto(idx);
        }

        public static int wtgaImpBns(int idx) {
            return wtgaBnsGiaLiqto(idx) + Len.BNS_GIA_LIQTO;
        }

        public static int wtgaImpBnsNull(int idx) {
            return wtgaImpBns(idx);
        }

        public static int wtgaCodDvs(int idx) {
            return wtgaImpBns(idx) + Len.IMP_BNS;
        }

        public static int wtgaPrstzIniNewfis(int idx) {
            return wtgaCodDvs(idx) + Len.COD_DVS;
        }

        public static int wtgaPrstzIniNewfisNull(int idx) {
            return wtgaPrstzIniNewfis(idx);
        }

        public static int wtgaImpScon(int idx) {
            return wtgaPrstzIniNewfis(idx) + Len.PRSTZ_INI_NEWFIS;
        }

        public static int wtgaImpSconNull(int idx) {
            return wtgaImpScon(idx);
        }

        public static int wtgaAlqScon(int idx) {
            return wtgaImpScon(idx) + Len.IMP_SCON;
        }

        public static int wtgaAlqSconNull(int idx) {
            return wtgaAlqScon(idx);
        }

        public static int wtgaImpCarAcq(int idx) {
            return wtgaAlqScon(idx) + Len.ALQ_SCON;
        }

        public static int wtgaImpCarAcqNull(int idx) {
            return wtgaImpCarAcq(idx);
        }

        public static int wtgaImpCarInc(int idx) {
            return wtgaImpCarAcq(idx) + Len.IMP_CAR_ACQ;
        }

        public static int wtgaImpCarIncNull(int idx) {
            return wtgaImpCarInc(idx);
        }

        public static int wtgaImpCarGest(int idx) {
            return wtgaImpCarInc(idx) + Len.IMP_CAR_INC;
        }

        public static int wtgaImpCarGestNull(int idx) {
            return wtgaImpCarGest(idx);
        }

        public static int wtgaEtaAa1oAssto(int idx) {
            return wtgaImpCarGest(idx) + Len.IMP_CAR_GEST;
        }

        public static int wtgaEtaAa1oAsstoNull(int idx) {
            return wtgaEtaAa1oAssto(idx);
        }

        public static int wtgaEtaMm1oAssto(int idx) {
            return wtgaEtaAa1oAssto(idx) + Len.ETA_AA1O_ASSTO;
        }

        public static int wtgaEtaMm1oAsstoNull(int idx) {
            return wtgaEtaMm1oAssto(idx);
        }

        public static int wtgaEtaAa2oAssto(int idx) {
            return wtgaEtaMm1oAssto(idx) + Len.ETA_MM1O_ASSTO;
        }

        public static int wtgaEtaAa2oAsstoNull(int idx) {
            return wtgaEtaAa2oAssto(idx);
        }

        public static int wtgaEtaMm2oAssto(int idx) {
            return wtgaEtaAa2oAssto(idx) + Len.ETA_AA2O_ASSTO;
        }

        public static int wtgaEtaMm2oAsstoNull(int idx) {
            return wtgaEtaMm2oAssto(idx);
        }

        public static int wtgaEtaAa3oAssto(int idx) {
            return wtgaEtaMm2oAssto(idx) + Len.ETA_MM2O_ASSTO;
        }

        public static int wtgaEtaAa3oAsstoNull(int idx) {
            return wtgaEtaAa3oAssto(idx);
        }

        public static int wtgaEtaMm3oAssto(int idx) {
            return wtgaEtaAa3oAssto(idx) + Len.ETA_AA3O_ASSTO;
        }

        public static int wtgaEtaMm3oAsstoNull(int idx) {
            return wtgaEtaMm3oAssto(idx);
        }

        public static int wtgaRendtoLrd(int idx) {
            return wtgaEtaMm3oAssto(idx) + Len.ETA_MM3O_ASSTO;
        }

        public static int wtgaRendtoLrdNull(int idx) {
            return wtgaRendtoLrd(idx);
        }

        public static int wtgaPcRetr(int idx) {
            return wtgaRendtoLrd(idx) + Len.RENDTO_LRD;
        }

        public static int wtgaPcRetrNull(int idx) {
            return wtgaPcRetr(idx);
        }

        public static int wtgaRendtoRetr(int idx) {
            return wtgaPcRetr(idx) + Len.PC_RETR;
        }

        public static int wtgaRendtoRetrNull(int idx) {
            return wtgaRendtoRetr(idx);
        }

        public static int wtgaMinGarto(int idx) {
            return wtgaRendtoRetr(idx) + Len.RENDTO_RETR;
        }

        public static int wtgaMinGartoNull(int idx) {
            return wtgaMinGarto(idx);
        }

        public static int wtgaMinTrnut(int idx) {
            return wtgaMinGarto(idx) + Len.MIN_GARTO;
        }

        public static int wtgaMinTrnutNull(int idx) {
            return wtgaMinTrnut(idx);
        }

        public static int wtgaPreAttDiTrch(int idx) {
            return wtgaMinTrnut(idx) + Len.MIN_TRNUT;
        }

        public static int wtgaPreAttDiTrchNull(int idx) {
            return wtgaPreAttDiTrch(idx);
        }

        public static int wtgaMatuEnd2000(int idx) {
            return wtgaPreAttDiTrch(idx) + Len.PRE_ATT_DI_TRCH;
        }

        public static int wtgaMatuEnd2000Null(int idx) {
            return wtgaMatuEnd2000(idx);
        }

        public static int wtgaAbbTotIni(int idx) {
            return wtgaMatuEnd2000(idx) + Len.MATU_END2000;
        }

        public static int wtgaAbbTotIniNull(int idx) {
            return wtgaAbbTotIni(idx);
        }

        public static int wtgaAbbTotUlt(int idx) {
            return wtgaAbbTotIni(idx) + Len.ABB_TOT_INI;
        }

        public static int wtgaAbbTotUltNull(int idx) {
            return wtgaAbbTotUlt(idx);
        }

        public static int wtgaAbbAnnuUlt(int idx) {
            return wtgaAbbTotUlt(idx) + Len.ABB_TOT_ULT;
        }

        public static int wtgaAbbAnnuUltNull(int idx) {
            return wtgaAbbAnnuUlt(idx);
        }

        public static int wtgaDurAbb(int idx) {
            return wtgaAbbAnnuUlt(idx) + Len.ABB_ANNU_ULT;
        }

        public static int wtgaDurAbbNull(int idx) {
            return wtgaDurAbb(idx);
        }

        public static int wtgaTpAdegAbb(int idx) {
            return wtgaDurAbb(idx) + Len.DUR_ABB;
        }

        public static int wtgaTpAdegAbbNull(int idx) {
            return wtgaTpAdegAbb(idx);
        }

        public static int wtgaModCalc(int idx) {
            return wtgaTpAdegAbb(idx) + Len.TP_ADEG_ABB;
        }

        public static int wtgaModCalcNull(int idx) {
            return wtgaModCalc(idx);
        }

        public static int wtgaImpAz(int idx) {
            return wtgaModCalc(idx) + Len.MOD_CALC;
        }

        public static int wtgaImpAzNull(int idx) {
            return wtgaImpAz(idx);
        }

        public static int wtgaImpAder(int idx) {
            return wtgaImpAz(idx) + Len.IMP_AZ;
        }

        public static int wtgaImpAderNull(int idx) {
            return wtgaImpAder(idx);
        }

        public static int wtgaImpTfr(int idx) {
            return wtgaImpAder(idx) + Len.IMP_ADER;
        }

        public static int wtgaImpTfrNull(int idx) {
            return wtgaImpTfr(idx);
        }

        public static int wtgaImpVolo(int idx) {
            return wtgaImpTfr(idx) + Len.IMP_TFR;
        }

        public static int wtgaImpVoloNull(int idx) {
            return wtgaImpVolo(idx);
        }

        public static int wtgaVisEnd2000(int idx) {
            return wtgaImpVolo(idx) + Len.IMP_VOLO;
        }

        public static int wtgaVisEnd2000Null(int idx) {
            return wtgaVisEnd2000(idx);
        }

        public static int wtgaDtVldtProd(int idx) {
            return wtgaVisEnd2000(idx) + Len.VIS_END2000;
        }

        public static int wtgaDtVldtProdNull(int idx) {
            return wtgaDtVldtProd(idx);
        }

        public static int wtgaDtIniValTar(int idx) {
            return wtgaDtVldtProd(idx) + Len.DT_VLDT_PROD;
        }

        public static int wtgaDtIniValTarNull(int idx) {
            return wtgaDtIniValTar(idx);
        }

        public static int wtgaImpbVisEnd2000(int idx) {
            return wtgaDtIniValTar(idx) + Len.DT_INI_VAL_TAR;
        }

        public static int wtgaImpbVisEnd2000Null(int idx) {
            return wtgaImpbVisEnd2000(idx);
        }

        public static int wtgaRenIniTsTec0(int idx) {
            return wtgaImpbVisEnd2000(idx) + Len.IMPB_VIS_END2000;
        }

        public static int wtgaRenIniTsTec0Null(int idx) {
            return wtgaRenIniTsTec0(idx);
        }

        public static int wtgaPcRipPre(int idx) {
            return wtgaRenIniTsTec0(idx) + Len.REN_INI_TS_TEC0;
        }

        public static int wtgaPcRipPreNull(int idx) {
            return wtgaPcRipPre(idx);
        }

        public static int wtgaFlImportiForz(int idx) {
            return wtgaPcRipPre(idx) + Len.PC_RIP_PRE;
        }

        public static int wtgaFlImportiForzNull(int idx) {
            return wtgaFlImportiForz(idx);
        }

        public static int wtgaPrstzIniNforz(int idx) {
            return wtgaFlImportiForz(idx) + Len.FL_IMPORTI_FORZ;
        }

        public static int wtgaPrstzIniNforzNull(int idx) {
            return wtgaPrstzIniNforz(idx);
        }

        public static int wtgaVisEnd2000Nforz(int idx) {
            return wtgaPrstzIniNforz(idx) + Len.PRSTZ_INI_NFORZ;
        }

        public static int wtgaVisEnd2000NforzNull(int idx) {
            return wtgaVisEnd2000Nforz(idx);
        }

        public static int wtgaIntrMora(int idx) {
            return wtgaVisEnd2000Nforz(idx) + Len.VIS_END2000_NFORZ;
        }

        public static int wtgaIntrMoraNull(int idx) {
            return wtgaIntrMora(idx);
        }

        public static int wtgaManfeeAntic(int idx) {
            return wtgaIntrMora(idx) + Len.INTR_MORA;
        }

        public static int wtgaManfeeAnticNull(int idx) {
            return wtgaManfeeAntic(idx);
        }

        public static int wtgaManfeeRicor(int idx) {
            return wtgaManfeeAntic(idx) + Len.MANFEE_ANTIC;
        }

        public static int wtgaManfeeRicorNull(int idx) {
            return wtgaManfeeRicor(idx);
        }

        public static int wtgaPreUniRivto(int idx) {
            return wtgaManfeeRicor(idx) + Len.MANFEE_RICOR;
        }

        public static int wtgaPreUniRivtoNull(int idx) {
            return wtgaPreUniRivto(idx);
        }

        public static int wtgaProv1aaAcq(int idx) {
            return wtgaPreUniRivto(idx) + Len.PRE_UNI_RIVTO;
        }

        public static int wtgaProv1aaAcqNull(int idx) {
            return wtgaProv1aaAcq(idx);
        }

        public static int wtgaProv2aaAcq(int idx) {
            return wtgaProv1aaAcq(idx) + Len.PROV1AA_ACQ;
        }

        public static int wtgaProv2aaAcqNull(int idx) {
            return wtgaProv2aaAcq(idx);
        }

        public static int wtgaProvRicor(int idx) {
            return wtgaProv2aaAcq(idx) + Len.PROV2AA_ACQ;
        }

        public static int wtgaProvRicorNull(int idx) {
            return wtgaProvRicor(idx);
        }

        public static int wtgaProvInc(int idx) {
            return wtgaProvRicor(idx) + Len.PROV_RICOR;
        }

        public static int wtgaProvIncNull(int idx) {
            return wtgaProvInc(idx);
        }

        public static int wtgaAlqProvAcq(int idx) {
            return wtgaProvInc(idx) + Len.PROV_INC;
        }

        public static int wtgaAlqProvAcqNull(int idx) {
            return wtgaAlqProvAcq(idx);
        }

        public static int wtgaAlqProvInc(int idx) {
            return wtgaAlqProvAcq(idx) + Len.ALQ_PROV_ACQ;
        }

        public static int wtgaAlqProvIncNull(int idx) {
            return wtgaAlqProvInc(idx);
        }

        public static int wtgaAlqProvRicor(int idx) {
            return wtgaAlqProvInc(idx) + Len.ALQ_PROV_INC;
        }

        public static int wtgaAlqProvRicorNull(int idx) {
            return wtgaAlqProvRicor(idx);
        }

        public static int wtgaImpbProvAcq(int idx) {
            return wtgaAlqProvRicor(idx) + Len.ALQ_PROV_RICOR;
        }

        public static int wtgaImpbProvAcqNull(int idx) {
            return wtgaImpbProvAcq(idx);
        }

        public static int wtgaImpbProvInc(int idx) {
            return wtgaImpbProvAcq(idx) + Len.IMPB_PROV_ACQ;
        }

        public static int wtgaImpbProvIncNull(int idx) {
            return wtgaImpbProvInc(idx);
        }

        public static int wtgaImpbProvRicor(int idx) {
            return wtgaImpbProvInc(idx) + Len.IMPB_PROV_INC;
        }

        public static int wtgaImpbProvRicorNull(int idx) {
            return wtgaImpbProvRicor(idx);
        }

        public static int wtgaFlProvForz(int idx) {
            return wtgaImpbProvRicor(idx) + Len.IMPB_PROV_RICOR;
        }

        public static int wtgaFlProvForzNull(int idx) {
            return wtgaFlProvForz(idx);
        }

        public static int wtgaPrstzAggIni(int idx) {
            return wtgaFlProvForz(idx) + Len.FL_PROV_FORZ;
        }

        public static int wtgaPrstzAggIniNull(int idx) {
            return wtgaPrstzAggIni(idx);
        }

        public static int wtgaIncrPre(int idx) {
            return wtgaPrstzAggIni(idx) + Len.PRSTZ_AGG_INI;
        }

        public static int wtgaIncrPreNull(int idx) {
            return wtgaIncrPre(idx);
        }

        public static int wtgaIncrPrstz(int idx) {
            return wtgaIncrPre(idx) + Len.INCR_PRE;
        }

        public static int wtgaIncrPrstzNull(int idx) {
            return wtgaIncrPrstz(idx);
        }

        public static int wtgaDtUltAdegPrePr(int idx) {
            return wtgaIncrPrstz(idx) + Len.INCR_PRSTZ;
        }

        public static int wtgaDtUltAdegPrePrNull(int idx) {
            return wtgaDtUltAdegPrePr(idx);
        }

        public static int wtgaPrstzAggUlt(int idx) {
            return wtgaDtUltAdegPrePr(idx) + Len.DT_ULT_ADEG_PRE_PR;
        }

        public static int wtgaPrstzAggUltNull(int idx) {
            return wtgaPrstzAggUlt(idx);
        }

        public static int wtgaTsRivalNet(int idx) {
            return wtgaPrstzAggUlt(idx) + Len.PRSTZ_AGG_ULT;
        }

        public static int wtgaTsRivalNetNull(int idx) {
            return wtgaTsRivalNet(idx);
        }

        public static int wtgaPrePattuito(int idx) {
            return wtgaTsRivalNet(idx) + Len.TS_RIVAL_NET;
        }

        public static int wtgaPrePattuitoNull(int idx) {
            return wtgaPrePattuito(idx);
        }

        public static int wtgaTpRival(int idx) {
            return wtgaPrePattuito(idx) + Len.PRE_PATTUITO;
        }

        public static int wtgaTpRivalNull(int idx) {
            return wtgaTpRival(idx);
        }

        public static int wtgaRisMat(int idx) {
            return wtgaTpRival(idx) + Len.TP_RIVAL;
        }

        public static int wtgaRisMatNull(int idx) {
            return wtgaRisMat(idx);
        }

        public static int wtgaCptMinScad(int idx) {
            return wtgaRisMat(idx) + Len.RIS_MAT;
        }

        public static int wtgaCptMinScadNull(int idx) {
            return wtgaCptMinScad(idx);
        }

        public static int wtgaCommisGest(int idx) {
            return wtgaCptMinScad(idx) + Len.CPT_MIN_SCAD;
        }

        public static int wtgaCommisGestNull(int idx) {
            return wtgaCommisGest(idx);
        }

        public static int wtgaTpManfeeAppl(int idx) {
            return wtgaCommisGest(idx) + Len.COMMIS_GEST;
        }

        public static int wtgaTpManfeeApplNull(int idx) {
            return wtgaTpManfeeAppl(idx);
        }

        public static int wtgaDsRiga(int idx) {
            return wtgaTpManfeeAppl(idx) + Len.TP_MANFEE_APPL;
        }

        public static int wtgaDsOperSql(int idx) {
            return wtgaDsRiga(idx) + Len.DS_RIGA;
        }

        public static int wtgaDsVer(int idx) {
            return wtgaDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int wtgaDsTsIniCptz(int idx) {
            return wtgaDsVer(idx) + Len.DS_VER;
        }

        public static int wtgaDsTsEndCptz(int idx) {
            return wtgaDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int wtgaDsUtente(int idx) {
            return wtgaDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int wtgaDsStatoElab(int idx) {
            return wtgaDsUtente(idx) + Len.DS_UTENTE;
        }

        public static int wtgaPcCommisGest(int idx) {
            return wtgaDsStatoElab(idx) + Len.DS_STATO_ELAB;
        }

        public static int wtgaPcCommisGestNull(int idx) {
            return wtgaPcCommisGest(idx);
        }

        public static int wtgaNumGgRival(int idx) {
            return wtgaPcCommisGest(idx) + Len.PC_COMMIS_GEST;
        }

        public static int wtgaNumGgRivalNull(int idx) {
            return wtgaNumGgRival(idx);
        }

        public static int wtgaImpTrasfe(int idx) {
            return wtgaNumGgRival(idx) + Len.NUM_GG_RIVAL;
        }

        public static int wtgaImpTrasfeNull(int idx) {
            return wtgaImpTrasfe(idx);
        }

        public static int wtgaImpTfrStrc(int idx) {
            return wtgaImpTrasfe(idx) + Len.IMP_TRASFE;
        }

        public static int wtgaImpTfrStrcNull(int idx) {
            return wtgaImpTfrStrc(idx);
        }

        public static int wtgaAcqExp(int idx) {
            return wtgaImpTfrStrc(idx) + Len.IMP_TFR_STRC;
        }

        public static int wtgaAcqExpNull(int idx) {
            return wtgaAcqExp(idx);
        }

        public static int wtgaRemunAss(int idx) {
            return wtgaAcqExp(idx) + Len.ACQ_EXP;
        }

        public static int wtgaRemunAssNull(int idx) {
            return wtgaRemunAss(idx);
        }

        public static int wtgaCommisInter(int idx) {
            return wtgaRemunAss(idx) + Len.REMUN_ASS;
        }

        public static int wtgaCommisInterNull(int idx) {
            return wtgaCommisInter(idx);
        }

        public static int wtgaAlqRemunAss(int idx) {
            return wtgaCommisInter(idx) + Len.COMMIS_INTER;
        }

        public static int wtgaAlqRemunAssNull(int idx) {
            return wtgaAlqRemunAss(idx);
        }

        public static int wtgaAlqCommisInter(int idx) {
            return wtgaAlqRemunAss(idx) + Len.ALQ_REMUN_ASS;
        }

        public static int wtgaAlqCommisInterNull(int idx) {
            return wtgaAlqCommisInter(idx);
        }

        public static int wtgaImpbRemunAss(int idx) {
            return wtgaAlqCommisInter(idx) + Len.ALQ_COMMIS_INTER;
        }

        public static int wtgaImpbRemunAssNull(int idx) {
            return wtgaImpbRemunAss(idx);
        }

        public static int wtgaImpbCommisInter(int idx) {
            return wtgaImpbRemunAss(idx) + Len.IMPB_REMUN_ASS;
        }

        public static int wtgaImpbCommisInterNull(int idx) {
            return wtgaImpbCommisInter(idx);
        }

        public static int wtgaCosRunAssva(int idx) {
            return wtgaImpbCommisInter(idx) + Len.IMPB_COMMIS_INTER;
        }

        public static int wtgaCosRunAssvaNull(int idx) {
            return wtgaCosRunAssva(idx);
        }

        public static int wtgaCosRunAssvaIdc(int idx) {
            return wtgaCosRunAssva(idx) + Len.COS_RUN_ASSVA;
        }

        public static int wtgaCosRunAssvaIdcNull(int idx) {
            return wtgaCosRunAssvaIdc(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_TRCH_DI_GAR = 5;
        public static final int ID_GAR = 5;
        public static final int ID_ADES = 5;
        public static final int ID_POLI = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int DT_DECOR = 5;
        public static final int DT_SCAD = 5;
        public static final int IB_OGG = 40;
        public static final int TP_RGM_FISC = 2;
        public static final int DT_EMIS = 5;
        public static final int TP_TRCH = 2;
        public static final int DUR_AA = 3;
        public static final int DUR_MM = 3;
        public static final int DUR_GG = 3;
        public static final int PRE_CASO_MOR = 8;
        public static final int PC_INTR_RIAT = 4;
        public static final int IMP_BNS_ANTIC = 8;
        public static final int PRE_INI_NET = 8;
        public static final int PRE_PP_INI = 8;
        public static final int PRE_PP_ULT = 8;
        public static final int PRE_TARI_INI = 8;
        public static final int PRE_TARI_ULT = 8;
        public static final int PRE_INVRIO_INI = 8;
        public static final int PRE_INVRIO_ULT = 8;
        public static final int PRE_RIVTO = 8;
        public static final int IMP_SOPR_PROF = 8;
        public static final int IMP_SOPR_SAN = 8;
        public static final int IMP_SOPR_SPO = 8;
        public static final int IMP_SOPR_TEC = 8;
        public static final int IMP_ALT_SOPR = 8;
        public static final int PRE_STAB = 8;
        public static final int DT_EFF_STAB = 5;
        public static final int TS_RIVAL_FIS = 8;
        public static final int TS_RIVAL_INDICIZ = 8;
        public static final int OLD_TS_TEC = 8;
        public static final int RAT_LRD = 8;
        public static final int PRE_LRD = 8;
        public static final int PRSTZ_INI = 8;
        public static final int PRSTZ_ULT = 8;
        public static final int CPT_IN_OPZ_RIVTO = 8;
        public static final int PRSTZ_INI_STAB = 8;
        public static final int CPT_RSH_MOR = 8;
        public static final int PRSTZ_RID_INI = 8;
        public static final int FL_CAR_CONT = 1;
        public static final int BNS_GIA_LIQTO = 8;
        public static final int IMP_BNS = 8;
        public static final int COD_DVS = 20;
        public static final int PRSTZ_INI_NEWFIS = 8;
        public static final int IMP_SCON = 8;
        public static final int ALQ_SCON = 4;
        public static final int IMP_CAR_ACQ = 8;
        public static final int IMP_CAR_INC = 8;
        public static final int IMP_CAR_GEST = 8;
        public static final int ETA_AA1O_ASSTO = 2;
        public static final int ETA_MM1O_ASSTO = 2;
        public static final int ETA_AA2O_ASSTO = 2;
        public static final int ETA_MM2O_ASSTO = 2;
        public static final int ETA_AA3O_ASSTO = 2;
        public static final int ETA_MM3O_ASSTO = 2;
        public static final int RENDTO_LRD = 8;
        public static final int PC_RETR = 4;
        public static final int RENDTO_RETR = 8;
        public static final int MIN_GARTO = 8;
        public static final int MIN_TRNUT = 8;
        public static final int PRE_ATT_DI_TRCH = 8;
        public static final int MATU_END2000 = 8;
        public static final int ABB_TOT_INI = 8;
        public static final int ABB_TOT_ULT = 8;
        public static final int ABB_ANNU_ULT = 8;
        public static final int DUR_ABB = 4;
        public static final int TP_ADEG_ABB = 1;
        public static final int MOD_CALC = 2;
        public static final int IMP_AZ = 8;
        public static final int IMP_ADER = 8;
        public static final int IMP_TFR = 8;
        public static final int IMP_VOLO = 8;
        public static final int VIS_END2000 = 8;
        public static final int DT_VLDT_PROD = 5;
        public static final int DT_INI_VAL_TAR = 5;
        public static final int IMPB_VIS_END2000 = 8;
        public static final int REN_INI_TS_TEC0 = 8;
        public static final int PC_RIP_PRE = 4;
        public static final int FL_IMPORTI_FORZ = 1;
        public static final int PRSTZ_INI_NFORZ = 8;
        public static final int VIS_END2000_NFORZ = 8;
        public static final int INTR_MORA = 8;
        public static final int MANFEE_ANTIC = 8;
        public static final int MANFEE_RICOR = 8;
        public static final int PRE_UNI_RIVTO = 8;
        public static final int PROV1AA_ACQ = 8;
        public static final int PROV2AA_ACQ = 8;
        public static final int PROV_RICOR = 8;
        public static final int PROV_INC = 8;
        public static final int ALQ_PROV_ACQ = 4;
        public static final int ALQ_PROV_INC = 4;
        public static final int ALQ_PROV_RICOR = 4;
        public static final int IMPB_PROV_ACQ = 8;
        public static final int IMPB_PROV_INC = 8;
        public static final int IMPB_PROV_RICOR = 8;
        public static final int FL_PROV_FORZ = 1;
        public static final int PRSTZ_AGG_INI = 8;
        public static final int INCR_PRE = 8;
        public static final int INCR_PRSTZ = 8;
        public static final int DT_ULT_ADEG_PRE_PR = 5;
        public static final int PRSTZ_AGG_ULT = 8;
        public static final int TS_RIVAL_NET = 8;
        public static final int PRE_PATTUITO = 8;
        public static final int TP_RIVAL = 2;
        public static final int RIS_MAT = 8;
        public static final int CPT_MIN_SCAD = 8;
        public static final int COMMIS_GEST = 8;
        public static final int TP_MANFEE_APPL = 2;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int PC_COMMIS_GEST = 4;
        public static final int NUM_GG_RIVAL = 3;
        public static final int IMP_TRASFE = 8;
        public static final int IMP_TFR_STRC = 8;
        public static final int ACQ_EXP = 8;
        public static final int REMUN_ASS = 8;
        public static final int COMMIS_INTER = 8;
        public static final int ALQ_REMUN_ASS = 4;
        public static final int ALQ_COMMIS_INTER = 4;
        public static final int IMPB_REMUN_ASS = 8;
        public static final int IMPB_COMMIS_INTER = 8;
        public static final int COS_RUN_ASSVA = 8;
        public static final int COS_RUN_ASSVA_IDC = 8;
        public static final int DATI = ID_TRCH_DI_GAR + ID_GAR + ID_ADES + ID_POLI + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + DT_DECOR + DT_SCAD + IB_OGG + TP_RGM_FISC + DT_EMIS + TP_TRCH + DUR_AA + DUR_MM + DUR_GG + PRE_CASO_MOR + PC_INTR_RIAT + IMP_BNS_ANTIC + PRE_INI_NET + PRE_PP_INI + PRE_PP_ULT + PRE_TARI_INI + PRE_TARI_ULT + PRE_INVRIO_INI + PRE_INVRIO_ULT + PRE_RIVTO + IMP_SOPR_PROF + IMP_SOPR_SAN + IMP_SOPR_SPO + IMP_SOPR_TEC + IMP_ALT_SOPR + PRE_STAB + DT_EFF_STAB + TS_RIVAL_FIS + TS_RIVAL_INDICIZ + OLD_TS_TEC + RAT_LRD + PRE_LRD + PRSTZ_INI + PRSTZ_ULT + CPT_IN_OPZ_RIVTO + PRSTZ_INI_STAB + CPT_RSH_MOR + PRSTZ_RID_INI + FL_CAR_CONT + BNS_GIA_LIQTO + IMP_BNS + COD_DVS + PRSTZ_INI_NEWFIS + IMP_SCON + ALQ_SCON + IMP_CAR_ACQ + IMP_CAR_INC + IMP_CAR_GEST + ETA_AA1O_ASSTO + ETA_MM1O_ASSTO + ETA_AA2O_ASSTO + ETA_MM2O_ASSTO + ETA_AA3O_ASSTO + ETA_MM3O_ASSTO + RENDTO_LRD + PC_RETR + RENDTO_RETR + MIN_GARTO + MIN_TRNUT + PRE_ATT_DI_TRCH + MATU_END2000 + ABB_TOT_INI + ABB_TOT_ULT + ABB_ANNU_ULT + DUR_ABB + TP_ADEG_ABB + MOD_CALC + IMP_AZ + IMP_ADER + IMP_TFR + IMP_VOLO + VIS_END2000 + DT_VLDT_PROD + DT_INI_VAL_TAR + IMPB_VIS_END2000 + REN_INI_TS_TEC0 + PC_RIP_PRE + FL_IMPORTI_FORZ + PRSTZ_INI_NFORZ + VIS_END2000_NFORZ + INTR_MORA + MANFEE_ANTIC + MANFEE_RICOR + PRE_UNI_RIVTO + PROV1AA_ACQ + PROV2AA_ACQ + PROV_RICOR + PROV_INC + ALQ_PROV_ACQ + ALQ_PROV_INC + ALQ_PROV_RICOR + IMPB_PROV_ACQ + IMPB_PROV_INC + IMPB_PROV_RICOR + FL_PROV_FORZ + PRSTZ_AGG_INI + INCR_PRE + INCR_PRSTZ + DT_ULT_ADEG_PRE_PR + PRSTZ_AGG_ULT + TS_RIVAL_NET + PRE_PATTUITO + TP_RIVAL + RIS_MAT + CPT_MIN_SCAD + COMMIS_GEST + TP_MANFEE_APPL + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + PC_COMMIS_GEST + NUM_GG_RIVAL + IMP_TRASFE + IMP_TFR_STRC + ACQ_EXP + REMUN_ASS + COMMIS_INTER + ALQ_REMUN_ASS + ALQ_COMMIS_INTER + IMPB_REMUN_ASS + IMPB_COMMIS_INTER + COS_RUN_ASSVA + COS_RUN_ASSVA_IDC;
        public static final int TAB_TRAN = STATUS + ID_PTF + DATI;
        public static final int FLR1 = 911;
        public static final int WTGA_TAB = WtgaTabLlbs0230.TAB_TRAN_MAXOCCURS * TAB_TRAN;
        public static final int RESTO_TAB = 1137839;
        public static final int PRE_LRD_NULL = 8;
        public static final int ID_MOVI_CHIU_NULL = 5;
        public static final int DT_SCAD_NULL = 5;
        public static final int IB_OGG_NULL = 40;
        public static final int DT_EMIS_NULL = 5;
        public static final int DUR_AA_NULL = 3;
        public static final int DUR_MM_NULL = 3;
        public static final int DUR_GG_NULL = 3;
        public static final int PRE_CASO_MOR_NULL = 8;
        public static final int PC_INTR_RIAT_NULL = 4;
        public static final int IMP_BNS_ANTIC_NULL = 8;
        public static final int PRE_INI_NET_NULL = 8;
        public static final int PRE_PP_INI_NULL = 8;
        public static final int PRE_PP_ULT_NULL = 8;
        public static final int PRE_TARI_INI_NULL = 8;
        public static final int PRE_TARI_ULT_NULL = 8;
        public static final int PRE_INVRIO_INI_NULL = 8;
        public static final int PRE_INVRIO_ULT_NULL = 8;
        public static final int PRE_RIVTO_NULL = 8;
        public static final int IMP_SOPR_PROF_NULL = 8;
        public static final int IMP_SOPR_SAN_NULL = 8;
        public static final int IMP_SOPR_SPO_NULL = 8;
        public static final int IMP_SOPR_TEC_NULL = 8;
        public static final int IMP_ALT_SOPR_NULL = 8;
        public static final int PRE_STAB_NULL = 8;
        public static final int DT_EFF_STAB_NULL = 5;
        public static final int TS_RIVAL_FIS_NULL = 8;
        public static final int TS_RIVAL_INDICIZ_NULL = 8;
        public static final int OLD_TS_TEC_NULL = 8;
        public static final int RAT_LRD_NULL = 8;
        public static final int PRSTZ_INI_NULL = 8;
        public static final int PRSTZ_ULT_NULL = 8;
        public static final int CPT_IN_OPZ_RIVTO_NULL = 8;
        public static final int PRSTZ_INI_STAB_NULL = 8;
        public static final int CPT_RSH_MOR_NULL = 8;
        public static final int PRSTZ_RID_INI_NULL = 8;
        public static final int BNS_GIA_LIQTO_NULL = 8;
        public static final int IMP_BNS_NULL = 8;
        public static final int PRSTZ_INI_NEWFIS_NULL = 8;
        public static final int IMP_SCON_NULL = 8;
        public static final int ALQ_SCON_NULL = 4;
        public static final int IMP_CAR_ACQ_NULL = 8;
        public static final int IMP_CAR_INC_NULL = 8;
        public static final int IMP_CAR_GEST_NULL = 8;
        public static final int ETA_AA1O_ASSTO_NULL = 2;
        public static final int ETA_MM1O_ASSTO_NULL = 2;
        public static final int ETA_AA2O_ASSTO_NULL = 2;
        public static final int ETA_MM2O_ASSTO_NULL = 2;
        public static final int ETA_AA3O_ASSTO_NULL = 2;
        public static final int ETA_MM3O_ASSTO_NULL = 2;
        public static final int RENDTO_LRD_NULL = 8;
        public static final int PC_RETR_NULL = 4;
        public static final int RENDTO_RETR_NULL = 8;
        public static final int MIN_GARTO_NULL = 8;
        public static final int MIN_TRNUT_NULL = 8;
        public static final int PRE_ATT_DI_TRCH_NULL = 8;
        public static final int MATU_END2000_NULL = 8;
        public static final int ABB_TOT_INI_NULL = 8;
        public static final int ABB_TOT_ULT_NULL = 8;
        public static final int ABB_ANNU_ULT_NULL = 8;
        public static final int DUR_ABB_NULL = 4;
        public static final int MOD_CALC_NULL = 2;
        public static final int IMP_AZ_NULL = 8;
        public static final int IMP_ADER_NULL = 8;
        public static final int IMP_TFR_NULL = 8;
        public static final int IMP_VOLO_NULL = 8;
        public static final int VIS_END2000_NULL = 8;
        public static final int DT_VLDT_PROD_NULL = 5;
        public static final int DT_INI_VAL_TAR_NULL = 5;
        public static final int IMPB_VIS_END2000_NULL = 8;
        public static final int REN_INI_TS_TEC0_NULL = 8;
        public static final int PC_RIP_PRE_NULL = 4;
        public static final int PRSTZ_INI_NFORZ_NULL = 8;
        public static final int VIS_END2000_NFORZ_NULL = 8;
        public static final int INTR_MORA_NULL = 8;
        public static final int MANFEE_ANTIC_NULL = 8;
        public static final int MANFEE_RICOR_NULL = 8;
        public static final int PRE_UNI_RIVTO_NULL = 8;
        public static final int PROV1AA_ACQ_NULL = 8;
        public static final int PROV2AA_ACQ_NULL = 8;
        public static final int PROV_RICOR_NULL = 8;
        public static final int PROV_INC_NULL = 8;
        public static final int ALQ_PROV_ACQ_NULL = 4;
        public static final int ALQ_PROV_INC_NULL = 4;
        public static final int ALQ_PROV_RICOR_NULL = 4;
        public static final int IMPB_PROV_ACQ_NULL = 8;
        public static final int IMPB_PROV_INC_NULL = 8;
        public static final int IMPB_PROV_RICOR_NULL = 8;
        public static final int PRSTZ_AGG_INI_NULL = 8;
        public static final int INCR_PRE_NULL = 8;
        public static final int INCR_PRSTZ_NULL = 8;
        public static final int DT_ULT_ADEG_PRE_PR_NULL = 5;
        public static final int PRSTZ_AGG_ULT_NULL = 8;
        public static final int TS_RIVAL_NET_NULL = 8;
        public static final int PRE_PATTUITO_NULL = 8;
        public static final int TP_RIVAL_NULL = 2;
        public static final int RIS_MAT_NULL = 8;
        public static final int PC_COMMIS_GEST_NULL = 4;
        public static final int NUM_GG_RIVAL_NULL = 3;
        public static final int IMP_TRASFE_NULL = 8;
        public static final int IMP_TFR_STRC_NULL = 8;
        public static final int ACQ_EXP_NULL = 8;
        public static final int REMUN_ASS_NULL = 8;
        public static final int COMMIS_INTER_NULL = 8;
        public static final int ALQ_REMUN_ASS_NULL = 4;
        public static final int ALQ_COMMIS_INTER_NULL = 4;
        public static final int IMPB_REMUN_ASS_NULL = 8;
        public static final int IMPB_COMMIS_INTER_NULL = 8;
        public static final int COS_RUN_ASSVA_NULL = 8;
        public static final int COS_RUN_ASSVA_IDC_NULL = 8;
        public static final int CPT_MIN_SCAD_NULL = 8;
        public static final int COMMIS_GEST_NULL = 8;
        public static final int TP_MANFEE_APPL_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_TRCH_DI_GAR = 9;
            public static final int ID_GAR = 9;
            public static final int ID_ADES = 9;
            public static final int ID_POLI = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_DECOR = 8;
            public static final int DT_SCAD = 8;
            public static final int DT_EMIS = 8;
            public static final int DUR_AA = 5;
            public static final int DUR_MM = 5;
            public static final int DUR_GG = 5;
            public static final int PRE_CASO_MOR = 12;
            public static final int PC_INTR_RIAT = 3;
            public static final int IMP_BNS_ANTIC = 12;
            public static final int PRE_INI_NET = 12;
            public static final int PRE_PP_INI = 12;
            public static final int PRE_PP_ULT = 12;
            public static final int PRE_TARI_INI = 12;
            public static final int PRE_TARI_ULT = 12;
            public static final int PRE_INVRIO_INI = 12;
            public static final int PRE_INVRIO_ULT = 12;
            public static final int PRE_RIVTO = 12;
            public static final int IMP_SOPR_PROF = 12;
            public static final int IMP_SOPR_SAN = 12;
            public static final int IMP_SOPR_SPO = 12;
            public static final int IMP_SOPR_TEC = 12;
            public static final int IMP_ALT_SOPR = 12;
            public static final int PRE_STAB = 12;
            public static final int DT_EFF_STAB = 8;
            public static final int TS_RIVAL_FIS = 5;
            public static final int TS_RIVAL_INDICIZ = 5;
            public static final int OLD_TS_TEC = 5;
            public static final int RAT_LRD = 12;
            public static final int PRE_LRD = 12;
            public static final int PRSTZ_INI = 12;
            public static final int PRSTZ_ULT = 12;
            public static final int CPT_IN_OPZ_RIVTO = 12;
            public static final int PRSTZ_INI_STAB = 12;
            public static final int CPT_RSH_MOR = 12;
            public static final int PRSTZ_RID_INI = 12;
            public static final int BNS_GIA_LIQTO = 12;
            public static final int IMP_BNS = 12;
            public static final int PRSTZ_INI_NEWFIS = 12;
            public static final int IMP_SCON = 12;
            public static final int ALQ_SCON = 3;
            public static final int IMP_CAR_ACQ = 12;
            public static final int IMP_CAR_INC = 12;
            public static final int IMP_CAR_GEST = 12;
            public static final int ETA_AA1O_ASSTO = 3;
            public static final int ETA_MM1O_ASSTO = 3;
            public static final int ETA_AA2O_ASSTO = 3;
            public static final int ETA_MM2O_ASSTO = 3;
            public static final int ETA_AA3O_ASSTO = 3;
            public static final int ETA_MM3O_ASSTO = 3;
            public static final int RENDTO_LRD = 5;
            public static final int PC_RETR = 3;
            public static final int RENDTO_RETR = 5;
            public static final int MIN_GARTO = 5;
            public static final int MIN_TRNUT = 5;
            public static final int PRE_ATT_DI_TRCH = 12;
            public static final int MATU_END2000 = 12;
            public static final int ABB_TOT_INI = 12;
            public static final int ABB_TOT_ULT = 12;
            public static final int ABB_ANNU_ULT = 12;
            public static final int DUR_ABB = 6;
            public static final int IMP_AZ = 12;
            public static final int IMP_ADER = 12;
            public static final int IMP_TFR = 12;
            public static final int IMP_VOLO = 12;
            public static final int VIS_END2000 = 12;
            public static final int DT_VLDT_PROD = 8;
            public static final int DT_INI_VAL_TAR = 8;
            public static final int IMPB_VIS_END2000 = 12;
            public static final int REN_INI_TS_TEC0 = 12;
            public static final int PC_RIP_PRE = 3;
            public static final int PRSTZ_INI_NFORZ = 12;
            public static final int VIS_END2000_NFORZ = 12;
            public static final int INTR_MORA = 12;
            public static final int MANFEE_ANTIC = 12;
            public static final int MANFEE_RICOR = 12;
            public static final int PRE_UNI_RIVTO = 12;
            public static final int PROV1AA_ACQ = 12;
            public static final int PROV2AA_ACQ = 12;
            public static final int PROV_RICOR = 12;
            public static final int PROV_INC = 12;
            public static final int ALQ_PROV_ACQ = 3;
            public static final int ALQ_PROV_INC = 3;
            public static final int ALQ_PROV_RICOR = 3;
            public static final int IMPB_PROV_ACQ = 12;
            public static final int IMPB_PROV_INC = 12;
            public static final int IMPB_PROV_RICOR = 12;
            public static final int PRSTZ_AGG_INI = 12;
            public static final int INCR_PRE = 12;
            public static final int INCR_PRSTZ = 12;
            public static final int DT_ULT_ADEG_PRE_PR = 8;
            public static final int PRSTZ_AGG_ULT = 12;
            public static final int TS_RIVAL_NET = 5;
            public static final int PRE_PATTUITO = 12;
            public static final int RIS_MAT = 12;
            public static final int CPT_MIN_SCAD = 12;
            public static final int COMMIS_GEST = 12;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int PC_COMMIS_GEST = 3;
            public static final int NUM_GG_RIVAL = 5;
            public static final int IMP_TRASFE = 12;
            public static final int IMP_TFR_STRC = 12;
            public static final int ACQ_EXP = 12;
            public static final int REMUN_ASS = 12;
            public static final int COMMIS_INTER = 12;
            public static final int ALQ_REMUN_ASS = 3;
            public static final int ALQ_COMMIS_INTER = 3;
            public static final int IMPB_REMUN_ASS = 12;
            public static final int IMPB_COMMIS_INTER = 12;
            public static final int COS_RUN_ASSVA = 12;
            public static final int COS_RUN_ASSVA_IDC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_CASO_MOR = 3;
            public static final int PC_INTR_RIAT = 3;
            public static final int IMP_BNS_ANTIC = 3;
            public static final int PRE_INI_NET = 3;
            public static final int PRE_PP_INI = 3;
            public static final int PRE_PP_ULT = 3;
            public static final int PRE_TARI_INI = 3;
            public static final int PRE_TARI_ULT = 3;
            public static final int PRE_INVRIO_INI = 3;
            public static final int PRE_INVRIO_ULT = 3;
            public static final int PRE_RIVTO = 3;
            public static final int IMP_SOPR_PROF = 3;
            public static final int IMP_SOPR_SAN = 3;
            public static final int IMP_SOPR_SPO = 3;
            public static final int IMP_SOPR_TEC = 3;
            public static final int IMP_ALT_SOPR = 3;
            public static final int PRE_STAB = 3;
            public static final int TS_RIVAL_FIS = 9;
            public static final int TS_RIVAL_INDICIZ = 9;
            public static final int OLD_TS_TEC = 9;
            public static final int RAT_LRD = 3;
            public static final int PRE_LRD = 3;
            public static final int PRSTZ_INI = 3;
            public static final int PRSTZ_ULT = 3;
            public static final int CPT_IN_OPZ_RIVTO = 3;
            public static final int PRSTZ_INI_STAB = 3;
            public static final int CPT_RSH_MOR = 3;
            public static final int PRSTZ_RID_INI = 3;
            public static final int BNS_GIA_LIQTO = 3;
            public static final int IMP_BNS = 3;
            public static final int PRSTZ_INI_NEWFIS = 3;
            public static final int IMP_SCON = 3;
            public static final int ALQ_SCON = 3;
            public static final int IMP_CAR_ACQ = 3;
            public static final int IMP_CAR_INC = 3;
            public static final int IMP_CAR_GEST = 3;
            public static final int RENDTO_LRD = 9;
            public static final int PC_RETR = 3;
            public static final int RENDTO_RETR = 9;
            public static final int MIN_GARTO = 9;
            public static final int MIN_TRNUT = 9;
            public static final int PRE_ATT_DI_TRCH = 3;
            public static final int MATU_END2000 = 3;
            public static final int ABB_TOT_INI = 3;
            public static final int ABB_TOT_ULT = 3;
            public static final int ABB_ANNU_ULT = 3;
            public static final int IMP_AZ = 3;
            public static final int IMP_ADER = 3;
            public static final int IMP_TFR = 3;
            public static final int IMP_VOLO = 3;
            public static final int VIS_END2000 = 3;
            public static final int IMPB_VIS_END2000 = 3;
            public static final int REN_INI_TS_TEC0 = 3;
            public static final int PC_RIP_PRE = 3;
            public static final int PRSTZ_INI_NFORZ = 3;
            public static final int VIS_END2000_NFORZ = 3;
            public static final int INTR_MORA = 3;
            public static final int MANFEE_ANTIC = 3;
            public static final int MANFEE_RICOR = 3;
            public static final int PRE_UNI_RIVTO = 3;
            public static final int PROV1AA_ACQ = 3;
            public static final int PROV2AA_ACQ = 3;
            public static final int PROV_RICOR = 3;
            public static final int PROV_INC = 3;
            public static final int ALQ_PROV_ACQ = 3;
            public static final int ALQ_PROV_INC = 3;
            public static final int ALQ_PROV_RICOR = 3;
            public static final int IMPB_PROV_ACQ = 3;
            public static final int IMPB_PROV_INC = 3;
            public static final int IMPB_PROV_RICOR = 3;
            public static final int PRSTZ_AGG_INI = 3;
            public static final int INCR_PRE = 3;
            public static final int INCR_PRSTZ = 3;
            public static final int PRSTZ_AGG_ULT = 3;
            public static final int TS_RIVAL_NET = 9;
            public static final int PRE_PATTUITO = 3;
            public static final int RIS_MAT = 3;
            public static final int CPT_MIN_SCAD = 3;
            public static final int COMMIS_GEST = 3;
            public static final int PC_COMMIS_GEST = 3;
            public static final int IMP_TRASFE = 3;
            public static final int IMP_TFR_STRC = 3;
            public static final int ACQ_EXP = 3;
            public static final int REMUN_ASS = 3;
            public static final int COMMIS_INTER = 3;
            public static final int ALQ_REMUN_ASS = 3;
            public static final int ALQ_COMMIS_INTER = 3;
            public static final int IMPB_REMUN_ASS = 3;
            public static final int IMPB_COMMIS_INTER = 3;
            public static final int COS_RUN_ASSVA = 3;
            public static final int COS_RUN_ASSVA_IDC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
