package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-COS-RUN-ASSVA<br>
 * Variable: WTGA-COS-RUN-ASSVA from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaCosRunAssva extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaCosRunAssva() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_COS_RUN_ASSVA;
    }

    public void setWtgaCosRunAssva(AfDecimal wtgaCosRunAssva) {
        writeDecimalAsPacked(Pos.WTGA_COS_RUN_ASSVA, wtgaCosRunAssva.copy());
    }

    public void setWtgaCosRunAssvaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_COS_RUN_ASSVA, Pos.WTGA_COS_RUN_ASSVA);
    }

    /**Original name: WTGA-COS-RUN-ASSVA<br>*/
    public AfDecimal getWtgaCosRunAssva() {
        return readPackedAsDecimal(Pos.WTGA_COS_RUN_ASSVA, Len.Int.WTGA_COS_RUN_ASSVA, Len.Fract.WTGA_COS_RUN_ASSVA);
    }

    public byte[] getWtgaCosRunAssvaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_COS_RUN_ASSVA, Pos.WTGA_COS_RUN_ASSVA);
        return buffer;
    }

    public void initWtgaCosRunAssvaSpaces() {
        fill(Pos.WTGA_COS_RUN_ASSVA, Len.WTGA_COS_RUN_ASSVA, Types.SPACE_CHAR);
    }

    public void setWtgaCosRunAssvaNull(String wtgaCosRunAssvaNull) {
        writeString(Pos.WTGA_COS_RUN_ASSVA_NULL, wtgaCosRunAssvaNull, Len.WTGA_COS_RUN_ASSVA_NULL);
    }

    /**Original name: WTGA-COS-RUN-ASSVA-NULL<br>*/
    public String getWtgaCosRunAssvaNull() {
        return readString(Pos.WTGA_COS_RUN_ASSVA_NULL, Len.WTGA_COS_RUN_ASSVA_NULL);
    }

    public String getWtgaCosRunAssvaNullFormatted() {
        return Functions.padBlanks(getWtgaCosRunAssvaNull(), Len.WTGA_COS_RUN_ASSVA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_COS_RUN_ASSVA = 1;
        public static final int WTGA_COS_RUN_ASSVA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_COS_RUN_ASSVA = 8;
        public static final int WTGA_COS_RUN_ASSVA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_COS_RUN_ASSVA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_COS_RUN_ASSVA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
