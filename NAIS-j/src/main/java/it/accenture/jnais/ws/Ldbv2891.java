package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LDBV2891<br>
 * Variable: LDBV2891 from copybook LDBV2891<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv2891 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV2891-ID-POL
    private int idPol = DefaultValues.INT_VAL;
    //Original name: LDBV2891-IMPB-VIS-END2000
    private AfDecimal impbVisEnd2000 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV2891;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv2891Bytes(buf);
    }

    public void setLdbv2891Bytes(byte[] buffer) {
        setLdbv2891Bytes(buffer, 1);
    }

    public byte[] getLdbv2891Bytes() {
        byte[] buffer = new byte[Len.LDBV2891];
        return getLdbv2891Bytes(buffer, 1);
    }

    public void setLdbv2891Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPol = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POL, 0);
        position += Len.ID_POL;
        impbVisEnd2000.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_VIS_END2000, Len.Fract.IMPB_VIS_END2000));
    }

    public byte[] getLdbv2891Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPol, Len.Int.ID_POL, 0);
        position += Len.ID_POL;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbVisEnd2000.copy());
        return buffer;
    }

    public void setIdPol(int idPol) {
        this.idPol = idPol;
    }

    public int getIdPol() {
        return this.idPol;
    }

    public void setImpbVisEnd2000(AfDecimal impbVisEnd2000) {
        this.impbVisEnd2000.assign(impbVisEnd2000);
    }

    public AfDecimal getImpbVisEnd2000() {
        return this.impbVisEnd2000.copy();
    }

    @Override
    public byte[] serialize() {
        return getLdbv2891Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POL = 5;
        public static final int IMPB_VIS_END2000 = 8;
        public static final int LDBV2891 = ID_POL + IMPB_VIS_END2000;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POL = 9;
            public static final int IMPB_VIS_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMPB_VIS_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
