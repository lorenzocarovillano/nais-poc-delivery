package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PROV-ACQ-RICOR<br>
 * Variable: WB03-PROV-ACQ-RICOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03ProvAcqRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03ProvAcqRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PROV_ACQ_RICOR;
    }

    public void setWb03ProvAcqRicor(AfDecimal wb03ProvAcqRicor) {
        writeDecimalAsPacked(Pos.WB03_PROV_ACQ_RICOR, wb03ProvAcqRicor.copy());
    }

    public void setWb03ProvAcqRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PROV_ACQ_RICOR, Pos.WB03_PROV_ACQ_RICOR);
    }

    /**Original name: WB03-PROV-ACQ-RICOR<br>*/
    public AfDecimal getWb03ProvAcqRicor() {
        return readPackedAsDecimal(Pos.WB03_PROV_ACQ_RICOR, Len.Int.WB03_PROV_ACQ_RICOR, Len.Fract.WB03_PROV_ACQ_RICOR);
    }

    public byte[] getWb03ProvAcqRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PROV_ACQ_RICOR, Pos.WB03_PROV_ACQ_RICOR);
        return buffer;
    }

    public void setWb03ProvAcqRicorNull(String wb03ProvAcqRicorNull) {
        writeString(Pos.WB03_PROV_ACQ_RICOR_NULL, wb03ProvAcqRicorNull, Len.WB03_PROV_ACQ_RICOR_NULL);
    }

    /**Original name: WB03-PROV-ACQ-RICOR-NULL<br>*/
    public String getWb03ProvAcqRicorNull() {
        return readString(Pos.WB03_PROV_ACQ_RICOR_NULL, Len.WB03_PROV_ACQ_RICOR_NULL);
    }

    public String getWb03ProvAcqRicorNullFormatted() {
        return Functions.padBlanks(getWb03ProvAcqRicorNull(), Len.WB03_PROV_ACQ_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PROV_ACQ_RICOR = 1;
        public static final int WB03_PROV_ACQ_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PROV_ACQ_RICOR = 8;
        public static final int WB03_PROV_ACQ_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PROV_ACQ_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PROV_ACQ_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
