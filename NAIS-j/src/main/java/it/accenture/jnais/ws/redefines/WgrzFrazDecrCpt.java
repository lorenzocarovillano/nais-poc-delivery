package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-FRAZ-DECR-CPT<br>
 * Variable: WGRZ-FRAZ-DECR-CPT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzFrazDecrCpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzFrazDecrCpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_FRAZ_DECR_CPT;
    }

    public void setWgrzFrazDecrCpt(int wgrzFrazDecrCpt) {
        writeIntAsPacked(Pos.WGRZ_FRAZ_DECR_CPT, wgrzFrazDecrCpt, Len.Int.WGRZ_FRAZ_DECR_CPT);
    }

    public void setWgrzFrazDecrCptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_FRAZ_DECR_CPT, Pos.WGRZ_FRAZ_DECR_CPT);
    }

    /**Original name: WGRZ-FRAZ-DECR-CPT<br>*/
    public int getWgrzFrazDecrCpt() {
        return readPackedAsInt(Pos.WGRZ_FRAZ_DECR_CPT, Len.Int.WGRZ_FRAZ_DECR_CPT);
    }

    public byte[] getWgrzFrazDecrCptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_FRAZ_DECR_CPT, Pos.WGRZ_FRAZ_DECR_CPT);
        return buffer;
    }

    public void initWgrzFrazDecrCptSpaces() {
        fill(Pos.WGRZ_FRAZ_DECR_CPT, Len.WGRZ_FRAZ_DECR_CPT, Types.SPACE_CHAR);
    }

    public void setWgrzFrazDecrCptNull(String wgrzFrazDecrCptNull) {
        writeString(Pos.WGRZ_FRAZ_DECR_CPT_NULL, wgrzFrazDecrCptNull, Len.WGRZ_FRAZ_DECR_CPT_NULL);
    }

    /**Original name: WGRZ-FRAZ-DECR-CPT-NULL<br>*/
    public String getWgrzFrazDecrCptNull() {
        return readString(Pos.WGRZ_FRAZ_DECR_CPT_NULL, Len.WGRZ_FRAZ_DECR_CPT_NULL);
    }

    public String getWgrzFrazDecrCptNullFormatted() {
        return Functions.padBlanks(getWgrzFrazDecrCptNull(), Len.WGRZ_FRAZ_DECR_CPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_FRAZ_DECR_CPT = 1;
        public static final int WGRZ_FRAZ_DECR_CPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_FRAZ_DECR_CPT = 3;
        public static final int WGRZ_FRAZ_DECR_CPT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_FRAZ_DECR_CPT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
