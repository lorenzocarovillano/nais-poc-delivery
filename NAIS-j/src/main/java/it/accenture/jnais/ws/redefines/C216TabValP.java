package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: C216-TAB-VAL-P<br>
 * Variable: C216-TAB-VAL-P from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class C216TabValP extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_VARIABILI_P_MAXOCCURS = 100;
    public static final int TAB_LIVELLO_P_MAXOCCURS = 3;

    //==== CONSTRUCTORS ====
    public C216TabValP() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.C216_TAB_VAL_P;
    }

    public String getIvvc0216TabValPFormatted() {
        return readFixedString(Pos.C216_TAB_VAL_P, Len.C216_TAB_VAL_P);
    }

    public void setC216TabValPBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.C216_TAB_VAL_P, Pos.C216_TAB_VAL_P);
    }

    public byte[] getC216TabValPBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.C216_TAB_VAL_P, Pos.C216_TAB_VAL_P);
        return buffer;
    }

    public void setIvvc0216IdPolP(int ivvc0216IdPolPIdx, int ivvc0216IdPolP) {
        int position = Pos.c216IdPolP(ivvc0216IdPolPIdx - 1);
        writeInt(position, ivvc0216IdPolP, Len.Int.IVVC0216_ID_POL_P, SignType.NO_SIGN);
    }

    public void setIvvc0216IdPolPFormatted(int ivvc0216IdPolPIdx, String ivvc0216IdPolP) {
        int position = Pos.c216IdPolP(ivvc0216IdPolPIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ivvc0216IdPolP, Len.ID_POL_P), Len.ID_POL_P);
    }

    /**Original name: C216-ID-POL-P<br>*/
    public int getIdPolP(int idPolPIdx) {
        int position = Pos.c216IdPolP(idPolPIdx - 1);
        return readNumDispUnsignedInt(position, Len.ID_POL_P);
    }

    public String getIdPolPFormatted(int idPolPIdx) {
        int position = Pos.c216IdPolP(idPolPIdx - 1);
        return readFixedString(position, Len.ID_POL_P);
    }

    public void setIvvc0216CodTipoOpzioneP(int ivvc0216CodTipoOpzionePIdx, String ivvc0216CodTipoOpzioneP) {
        int position = Pos.c216CodTipoOpzioneP(ivvc0216CodTipoOpzionePIdx - 1);
        writeString(position, ivvc0216CodTipoOpzioneP, Len.COD_TIPO_OPZIONE_P);
    }

    /**Original name: C216-COD-TIPO-OPZIONE-P<br>*/
    public String getCodTipoOpzioneP(int codTipoOpzionePIdx) {
        int position = Pos.c216CodTipoOpzioneP(codTipoOpzionePIdx - 1);
        return readString(position, Len.COD_TIPO_OPZIONE_P);
    }

    public String getCodTipoOpzionePFormatted(int codTipoOpzionePIdx) {
        return Functions.padBlanks(getCodTipoOpzioneP(codTipoOpzionePIdx), Len.COD_TIPO_OPZIONE_P);
    }

    public void setIvvc0216TpLivelloP(int ivvc0216TpLivelloPIdx, char ivvc0216TpLivelloP) {
        int position = Pos.c216TpLivelloP(ivvc0216TpLivelloPIdx - 1);
        writeChar(position, ivvc0216TpLivelloP);
    }

    /**Original name: C216-TP-LIVELLO-P<br>*/
    public char getTpLivelloP(int tpLivelloPIdx) {
        int position = Pos.c216TpLivelloP(tpLivelloPIdx - 1);
        return readChar(position);
    }

    public void setIvvc0216CodLivelloP(int ivvc0216CodLivelloPIdx, String ivvc0216CodLivelloP) {
        int position = Pos.c216CodLivelloP(ivvc0216CodLivelloPIdx - 1);
        writeString(position, ivvc0216CodLivelloP, Len.COD_LIVELLO_P);
    }

    /**Original name: C216-COD-LIVELLO-P<br>*/
    public String getCodLivelloP(int codLivelloPIdx) {
        int position = Pos.c216CodLivelloP(codLivelloPIdx - 1);
        return readString(position, Len.COD_LIVELLO_P);
    }

    public void setIvvc0216IdLivelloP(int ivvc0216IdLivelloPIdx, int ivvc0216IdLivelloP) {
        int position = Pos.c216IdLivelloP(ivvc0216IdLivelloPIdx - 1);
        writeInt(position, ivvc0216IdLivelloP, Len.Int.IVVC0216_ID_LIVELLO_P, SignType.NO_SIGN);
    }

    public void setIvvc0216IdLivelloPFormatted(int ivvc0216IdLivelloPIdx, String ivvc0216IdLivelloP) {
        int position = Pos.c216IdLivelloP(ivvc0216IdLivelloPIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ivvc0216IdLivelloP, Len.ID_LIVELLO_P), Len.ID_LIVELLO_P);
    }

    /**Original name: IVVC0216-ID-LIVELLO-P<br>*/
    public int getIvvc0216IdLivelloP(int ivvc0216IdLivelloPIdx) {
        int position = Pos.c216IdLivelloP(ivvc0216IdLivelloPIdx - 1);
        return readNumDispUnsignedInt(position, Len.ID_LIVELLO_P);
    }

    public String getIdLivelloPFormatted(int idLivelloPIdx) {
        int position = Pos.c216IdLivelloP(idLivelloPIdx - 1);
        return readFixedString(position, Len.ID_LIVELLO_P);
    }

    public void setIvvc0216DtInizProdP(int ivvc0216DtInizProdPIdx, String ivvc0216DtInizProdP) {
        int position = Pos.c216DtInizProdP(ivvc0216DtInizProdPIdx - 1);
        writeString(position, ivvc0216DtInizProdP, Len.DT_INIZ_PROD_P);
    }

    /**Original name: C216-DT-INIZ-PROD-P<br>*/
    public String getDtInizProdP(int dtInizProdPIdx) {
        int position = Pos.c216DtInizProdP(dtInizProdPIdx - 1);
        return readString(position, Len.DT_INIZ_PROD_P);
    }

    public void setIvvc0216CodRgmFiscP(int ivvc0216CodRgmFiscPIdx, String ivvc0216CodRgmFiscP) {
        int position = Pos.c216CodRgmFiscP(ivvc0216CodRgmFiscPIdx - 1);
        writeString(position, ivvc0216CodRgmFiscP, Len.COD_RGM_FISC_P);
    }

    /**Original name: C216-COD-RGM-FISC-P<br>*/
    public String getCodRgmFiscP(int codRgmFiscPIdx) {
        int position = Pos.c216CodRgmFiscP(codRgmFiscPIdx - 1);
        return readString(position, Len.COD_RGM_FISC_P);
    }

    public void setIvvc0216NomeServizioP(int ivvc0216NomeServizioPIdx, String ivvc0216NomeServizioP) {
        int position = Pos.c216NomeServizioP(ivvc0216NomeServizioPIdx - 1);
        writeString(position, ivvc0216NomeServizioP, Len.NOME_SERVIZIO_P);
    }

    /**Original name: C216-NOME-SERVIZIO-P<br>*/
    public String getNomeServizioP(int nomeServizioPIdx) {
        int position = Pos.c216NomeServizioP(nomeServizioPIdx - 1);
        return readString(position, Len.NOME_SERVIZIO_P);
    }

    public void setEleVariabiliMaxP(int eleVariabiliMaxPIdx, short eleVariabiliMaxP) {
        int position = Pos.c216EleVariabiliMaxP(eleVariabiliMaxPIdx - 1);
        writeShortAsPacked(position, eleVariabiliMaxP, Len.Int.ELE_VARIABILI_MAX_P);
    }

    /**Original name: C216-ELE-VARIABILI-MAX-P<br>*/
    public short getEleVariabiliMaxP(int eleVariabiliMaxPIdx) {
        int position = Pos.c216EleVariabiliMaxP(eleVariabiliMaxPIdx - 1);
        return readPackedAsShort(position, Len.Int.ELE_VARIABILI_MAX_P);
    }

    public void setTabVariabiliPBytes(int tabVariabiliPIdx1, int tabVariabiliPIdx2, byte[] buffer) {
        setTabVariabiliPBytes(tabVariabiliPIdx1, tabVariabiliPIdx2, buffer, 1);
    }

    /**Original name: IVVC0216-TAB-VARIABILI-P<br>*/
    public byte[] getIvvc0216TabVariabiliPBytes(int ivvc0216TabVariabiliPIdx1, int ivvc0216TabVariabiliPIdx2) {
        byte[] buffer = new byte[Len.TAB_VARIABILI_P];
        return getIvvc0216TabVariabiliPBytes(ivvc0216TabVariabiliPIdx1, ivvc0216TabVariabiliPIdx2, buffer, 1);
    }

    public void setTabVariabiliPBytes(int tabVariabiliPIdx1, int tabVariabiliPIdx2, byte[] buffer, int offset) {
        int position = Pos.c216TabVariabiliP(tabVariabiliPIdx1 - 1, tabVariabiliPIdx2 - 1);
        setBytes(buffer, offset, Len.TAB_VARIABILI_P, position);
    }

    public byte[] getIvvc0216TabVariabiliPBytes(int ivvc0216TabVariabiliPIdx1, int ivvc0216TabVariabiliPIdx2, byte[] buffer, int offset) {
        int position = Pos.c216TabVariabiliP(ivvc0216TabVariabiliPIdx1 - 1, ivvc0216TabVariabiliPIdx2 - 1);
        getBytes(buffer, offset, Len.TAB_VARIABILI_P, position);
        return buffer;
    }

    public void initAreaVariabilePSpaces(int areaVariabilePIdx1, int areaVariabilePIdx2) {
        fill(Pos.c216AreaVariabileP((areaVariabilePIdx1 - 1), (areaVariabilePIdx2 - 1)), Len.AREA_VARIABILE_P, Types.SPACE_CHAR);
    }

    public void setCodVariabileP(int codVariabilePIdx1, int codVariabilePIdx2, String codVariabileP) {
        int position = Pos.c216CodVariabileP(codVariabilePIdx1 - 1, codVariabilePIdx2 - 1);
        writeString(position, codVariabileP, Len.COD_VARIABILE_P);
    }

    /**Original name: C216-COD-VARIABILE-P<br>*/
    public String getCodVariabileP(int codVariabilePIdx1, int codVariabilePIdx2) {
        int position = Pos.c216CodVariabileP(codVariabilePIdx1 - 1, codVariabilePIdx2 - 1);
        return readString(position, Len.COD_VARIABILE_P);
    }

    public String getCodVariabilePFormatted(int codVariabilePIdx1, int codVariabilePIdx2) {
        return Functions.padBlanks(getCodVariabileP(codVariabilePIdx1, codVariabilePIdx2), Len.COD_VARIABILE_P);
    }

    public void setTpDatoP(int tpDatoPIdx1, int tpDatoPIdx2, char tpDatoP) {
        int position = Pos.c216TpDatoP(tpDatoPIdx1 - 1, tpDatoPIdx2 - 1);
        writeChar(position, tpDatoP);
    }

    public void setTpDatoPFormatted(int tpDatoPIdx1, int tpDatoPIdx2, String tpDatoP) {
        setTpDatoP(tpDatoPIdx1, tpDatoPIdx2, Functions.charAt(tpDatoP, Types.CHAR_SIZE));
    }

    /**Original name: C216-TP-DATO-P<br>*/
    public char getTpDatoP(int tpDatoPIdx1, int tpDatoPIdx2) {
        int position = Pos.c216TpDatoP(tpDatoPIdx1 - 1, tpDatoPIdx2 - 1);
        return readChar(position);
    }

    public void setIvvc0216ValGenericoP(int ivvc0216ValGenericoPIdx1, int ivvc0216ValGenericoPIdx2, String ivvc0216ValGenericoP) {
        int position = Pos.c216ValGenericoP(ivvc0216ValGenericoPIdx1 - 1, ivvc0216ValGenericoPIdx2 - 1);
        writeString(position, ivvc0216ValGenericoP, Len.VAL_GENERICO_P);
    }

    /**Original name: IVVC0216-VAL-GENERICO-P<br>*/
    public String getIvvc0216ValGenericoP(int ivvc0216ValGenericoPIdx1, int ivvc0216ValGenericoPIdx2) {
        int position = Pos.c216ValGenericoP(ivvc0216ValGenericoPIdx1 - 1, ivvc0216ValGenericoPIdx2 - 1);
        return readString(position, Len.VAL_GENERICO_P);
    }

    public void setValImpP(int valImpPIdx1, int valImpPIdx2, AfDecimal valImpP) {
        int position = Pos.c216ValImpP(valImpPIdx1 - 1, valImpPIdx2 - 1);
        writeDecimal(position, valImpP.copy());
    }

    /**Original name: C216-VAL-IMP-P<br>*/
    public AfDecimal getValImpP(int valImpPIdx1, int valImpPIdx2) {
        int position = Pos.c216ValImpP(valImpPIdx1 - 1, valImpPIdx2 - 1);
        return readDecimal(position, Len.Int.VAL_IMP_P, Len.Fract.VAL_IMP_P);
    }

    public void setValPercP(int valPercPIdx1, int valPercPIdx2, AfDecimal valPercP) {
        int position = Pos.c216ValPercP(valPercPIdx1 - 1, valPercPIdx2 - 1);
        writeDecimal(position, valPercP.copy(), SignType.NO_SIGN);
    }

    /**Original name: C216-VAL-PERC-P<br>*/
    public AfDecimal getValPercP(int valPercPIdx1, int valPercPIdx2) {
        int position = Pos.c216ValPercP(valPercPIdx1 - 1, valPercPIdx2 - 1);
        return readDecimal(position, Len.Int.VAL_PERC_P, Len.Fract.VAL_PERC_P, SignType.NO_SIGN);
    }

    public void setValStrP(int valStrPIdx1, int valStrPIdx2, String valStrP) {
        int position = Pos.c216ValStrP(valStrPIdx1 - 1, valStrPIdx2 - 1);
        writeString(position, valStrP, Len.VAL_STR_P);
    }

    /**Original name: C216-VAL-STR-P<br>*/
    public String getValStrP(int valStrPIdx1, int valStrPIdx2) {
        int position = Pos.c216ValStrP(valStrPIdx1 - 1, valStrPIdx2 - 1);
        return readString(position, Len.VAL_STR_P);
    }

    public void setIvvc0216RestoTabValP(String ivvc0216RestoTabValP) {
        writeString(Pos.RESTO_TAB_VAL_P, ivvc0216RestoTabValP, Len.IVVC0216_RESTO_TAB_VAL_P);
    }

    /**Original name: IVVC0216-RESTO-TAB-VAL-P<br>*/
    public String getIvvc0216RestoTabValP() {
        return readString(Pos.RESTO_TAB_VAL_P, Len.IVVC0216_RESTO_TAB_VAL_P);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int C216_TAB_VAL_P = 1;
        public static final int C216_TAB_VAL_R_P = 1;
        public static final int FLR1 = C216_TAB_VAL_R_P;
        public static final int RESTO_TAB_VAL_P = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int c216TabLivelloP(int idx) {
            return C216_TAB_VAL_P + idx * Len.TAB_LIVELLO_P;
        }

        public static int c216IdPolP(int idx) {
            return c216TabLivelloP(idx);
        }

        public static int c216DatiLivelloP(int idx) {
            return c216IdPolP(idx) + Len.ID_POL_P;
        }

        public static int c216CodTipoOpzioneP(int idx) {
            return c216DatiLivelloP(idx);
        }

        public static int c216TpLivelloP(int idx) {
            return c216CodTipoOpzioneP(idx) + Len.COD_TIPO_OPZIONE_P;
        }

        public static int c216CodLivelloP(int idx) {
            return c216TpLivelloP(idx) + Len.TP_LIVELLO_P;
        }

        public static int c216IdLivelloP(int idx) {
            return c216CodLivelloP(idx) + Len.COD_LIVELLO_P;
        }

        public static int c216DtInizProdP(int idx) {
            return c216IdLivelloP(idx) + Len.ID_LIVELLO_P;
        }

        public static int c216CodRgmFiscP(int idx) {
            return c216DtInizProdP(idx) + Len.DT_INIZ_PROD_P;
        }

        public static int c216NomeServizioP(int idx) {
            return c216CodRgmFiscP(idx) + Len.COD_RGM_FISC_P;
        }

        public static int c216AreaVariabiliP(int idx) {
            return c216NomeServizioP(idx) + Len.NOME_SERVIZIO_P;
        }

        public static int c216EleVariabiliMaxP(int idx) {
            return c216AreaVariabiliP(idx);
        }

        public static int c216TabVariabiliP(int idx1, int idx2) {
            return c216EleVariabiliMaxP(idx1) + Len.ELE_VARIABILI_MAX_P + idx2 * Len.TAB_VARIABILI_P;
        }

        public static int c216AreaVariabileP(int idx1, int idx2) {
            return c216TabVariabiliP(idx1, idx2);
        }

        public static int c216CodVariabileP(int idx1, int idx2) {
            return c216AreaVariabileP(idx1, idx2);
        }

        public static int c216TpDatoP(int idx1, int idx2) {
            return c216CodVariabileP(idx1, idx2) + Len.COD_VARIABILE_P;
        }

        public static int c216ValGenericoP(int idx1, int idx2) {
            return c216TpDatoP(idx1, idx2) + Len.TP_DATO_P;
        }

        public static int c216ValImpGenP(int idx1, int idx2) {
            return c216ValGenericoP(idx1, idx2);
        }

        public static int c216ValImpP(int idx1, int idx2) {
            return c216ValImpGenP(idx1, idx2);
        }

        public static int c216ValImpFillerP(int idx1, int idx2) {
            return c216ValImpP(idx1, idx2) + Len.VAL_IMP_P;
        }

        public static int c216ValPercGenP(int idx1, int idx2) {
            return c216ValGenericoP(idx1, idx2);
        }

        public static int c216ValPercP(int idx1, int idx2) {
            return c216ValPercGenP(idx1, idx2);
        }

        public static int c216ValPercFillerP(int idx1, int idx2) {
            return c216ValPercP(idx1, idx2) + Len.VAL_PERC_P;
        }

        public static int c216ValStrGenP(int idx1, int idx2) {
            return c216ValGenericoP(idx1, idx2);
        }

        public static int c216ValStrP(int idx1, int idx2) {
            return c216ValStrGenP(idx1, idx2);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POL_P = 9;
        public static final int COD_TIPO_OPZIONE_P = 2;
        public static final int TP_LIVELLO_P = 1;
        public static final int COD_LIVELLO_P = 12;
        public static final int ID_LIVELLO_P = 9;
        public static final int DT_INIZ_PROD_P = 8;
        public static final int COD_RGM_FISC_P = 2;
        public static final int NOME_SERVIZIO_P = 8;
        public static final int ELE_VARIABILI_MAX_P = 3;
        public static final int COD_VARIABILE_P = 12;
        public static final int TP_DATO_P = 1;
        public static final int VAL_GENERICO_P = 60;
        public static final int AREA_VARIABILE_P = COD_VARIABILE_P + TP_DATO_P + VAL_GENERICO_P;
        public static final int TAB_VARIABILI_P = AREA_VARIABILE_P;
        public static final int AREA_VARIABILI_P = ELE_VARIABILI_MAX_P + C216TabValP.TAB_VARIABILI_P_MAXOCCURS * TAB_VARIABILI_P;
        public static final int DATI_LIVELLO_P = COD_TIPO_OPZIONE_P + TP_LIVELLO_P + COD_LIVELLO_P + ID_LIVELLO_P + DT_INIZ_PROD_P + COD_RGM_FISC_P + NOME_SERVIZIO_P + AREA_VARIABILI_P;
        public static final int TAB_LIVELLO_P = ID_POL_P + DATI_LIVELLO_P;
        public static final int VAL_IMP_P = 18;
        public static final int VAL_PERC_P = 14;
        public static final int FLR1 = 7354;
        public static final int C216_TAB_VAL_P = C216TabValP.TAB_LIVELLO_P_MAXOCCURS * TAB_LIVELLO_P;
        public static final int VAL_STR_P = 60;
        public static final int IVVC0216_RESTO_TAB_VAL_P = 14708;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IVVC0216_ID_POL_P = 9;
            public static final int IVVC0216_ID_LIVELLO_P = 9;
            public static final int ELE_VARIABILI_MAX_P = 4;
            public static final int VAL_IMP_P = 11;
            public static final int VAL_PERC_P = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAL_IMP_P = 7;
            public static final int VAL_PERC_P = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
