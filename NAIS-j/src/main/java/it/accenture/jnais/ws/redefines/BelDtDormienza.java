package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-DT-DORMIENZA<br>
 * Variable: BEL-DT-DORMIENZA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelDtDormienza extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelDtDormienza() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_DT_DORMIENZA;
    }

    public void setBelDtDormienza(int belDtDormienza) {
        writeIntAsPacked(Pos.BEL_DT_DORMIENZA, belDtDormienza, Len.Int.BEL_DT_DORMIENZA);
    }

    public void setBelDtDormienzaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_DT_DORMIENZA, Pos.BEL_DT_DORMIENZA);
    }

    /**Original name: BEL-DT-DORMIENZA<br>*/
    public int getBelDtDormienza() {
        return readPackedAsInt(Pos.BEL_DT_DORMIENZA, Len.Int.BEL_DT_DORMIENZA);
    }

    public byte[] getBelDtDormienzaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_DT_DORMIENZA, Pos.BEL_DT_DORMIENZA);
        return buffer;
    }

    public void setBelDtDormienzaNull(String belDtDormienzaNull) {
        writeString(Pos.BEL_DT_DORMIENZA_NULL, belDtDormienzaNull, Len.BEL_DT_DORMIENZA_NULL);
    }

    /**Original name: BEL-DT-DORMIENZA-NULL<br>*/
    public String getBelDtDormienzaNull() {
        return readString(Pos.BEL_DT_DORMIENZA_NULL, Len.BEL_DT_DORMIENZA_NULL);
    }

    public String getBelDtDormienzaNullFormatted() {
        return Functions.padBlanks(getBelDtDormienzaNull(), Len.BEL_DT_DORMIENZA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_DT_DORMIENZA = 1;
        public static final int BEL_DT_DORMIENZA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_DT_DORMIENZA = 5;
        public static final int BEL_DT_DORMIENZA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_DT_DORMIENZA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
