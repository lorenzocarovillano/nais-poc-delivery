package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-SPESE-MEDICHE<br>
 * Variable: WPAG-RATA-SPESE-MEDICHE from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataSpeseMediche extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataSpeseMediche() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_SPESE_MEDICHE;
    }

    public void setWpagRataSpeseMediche(AfDecimal wpagRataSpeseMediche) {
        writeDecimalAsPacked(Pos.WPAG_RATA_SPESE_MEDICHE, wpagRataSpeseMediche.copy());
    }

    public void setWpagRataSpeseMedicheFormatted(String wpagRataSpeseMediche) {
        setWpagRataSpeseMediche(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_SPESE_MEDICHE + Len.Fract.WPAG_RATA_SPESE_MEDICHE, Len.Fract.WPAG_RATA_SPESE_MEDICHE, wpagRataSpeseMediche));
    }

    public void setWpagRataSpeseMedicheFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_SPESE_MEDICHE, Pos.WPAG_RATA_SPESE_MEDICHE);
    }

    /**Original name: WPAG-RATA-SPESE-MEDICHE<br>*/
    public AfDecimal getWpagRataSpeseMediche() {
        return readPackedAsDecimal(Pos.WPAG_RATA_SPESE_MEDICHE, Len.Int.WPAG_RATA_SPESE_MEDICHE, Len.Fract.WPAG_RATA_SPESE_MEDICHE);
    }

    public byte[] getWpagRataSpeseMedicheAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_SPESE_MEDICHE, Pos.WPAG_RATA_SPESE_MEDICHE);
        return buffer;
    }

    public void initWpagRataSpeseMedicheSpaces() {
        fill(Pos.WPAG_RATA_SPESE_MEDICHE, Len.WPAG_RATA_SPESE_MEDICHE, Types.SPACE_CHAR);
    }

    public void setWpagRataSpeseMedicheNull(String wpagRataSpeseMedicheNull) {
        writeString(Pos.WPAG_RATA_SPESE_MEDICHE_NULL, wpagRataSpeseMedicheNull, Len.WPAG_RATA_SPESE_MEDICHE_NULL);
    }

    /**Original name: WPAG-RATA-SPESE-MEDICHE-NULL<br>*/
    public String getWpagRataSpeseMedicheNull() {
        return readString(Pos.WPAG_RATA_SPESE_MEDICHE_NULL, Len.WPAG_RATA_SPESE_MEDICHE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_SPESE_MEDICHE = 1;
        public static final int WPAG_RATA_SPESE_MEDICHE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_SPESE_MEDICHE = 8;
        public static final int WPAG_RATA_SPESE_MEDICHE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_SPESE_MEDICHE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_SPESE_MEDICHE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
