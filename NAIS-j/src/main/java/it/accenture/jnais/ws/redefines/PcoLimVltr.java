package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-LIM-VLTR<br>
 * Variable: PCO-LIM-VLTR from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoLimVltr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoLimVltr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_LIM_VLTR;
    }

    public void setPcoLimVltr(AfDecimal pcoLimVltr) {
        writeDecimalAsPacked(Pos.PCO_LIM_VLTR, pcoLimVltr.copy());
    }

    public void setPcoLimVltrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_LIM_VLTR, Pos.PCO_LIM_VLTR);
    }

    /**Original name: PCO-LIM-VLTR<br>*/
    public AfDecimal getPcoLimVltr() {
        return readPackedAsDecimal(Pos.PCO_LIM_VLTR, Len.Int.PCO_LIM_VLTR, Len.Fract.PCO_LIM_VLTR);
    }

    public byte[] getPcoLimVltrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_LIM_VLTR, Pos.PCO_LIM_VLTR);
        return buffer;
    }

    public void initPcoLimVltrHighValues() {
        fill(Pos.PCO_LIM_VLTR, Len.PCO_LIM_VLTR, Types.HIGH_CHAR_VAL);
    }

    public void setPcoLimVltrNull(String pcoLimVltrNull) {
        writeString(Pos.PCO_LIM_VLTR_NULL, pcoLimVltrNull, Len.PCO_LIM_VLTR_NULL);
    }

    /**Original name: PCO-LIM-VLTR-NULL<br>*/
    public String getPcoLimVltrNull() {
        return readString(Pos.PCO_LIM_VLTR_NULL, Len.PCO_LIM_VLTR_NULL);
    }

    public String getPcoLimVltrNullFormatted() {
        return Functions.padBlanks(getPcoLimVltrNull(), Len.PCO_LIM_VLTR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_LIM_VLTR = 1;
        public static final int PCO_LIM_VLTR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_LIM_VLTR = 8;
        public static final int PCO_LIM_VLTR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_LIM_VLTR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_LIM_VLTR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
