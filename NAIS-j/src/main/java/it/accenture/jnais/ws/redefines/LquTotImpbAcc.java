package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMPB-ACC<br>
 * Variable: LQU-TOT-IMPB-ACC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpbAcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpbAcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMPB_ACC;
    }

    public void setLquTotImpbAcc(AfDecimal lquTotImpbAcc) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMPB_ACC, lquTotImpbAcc.copy());
    }

    public void setLquTotImpbAccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMPB_ACC, Pos.LQU_TOT_IMPB_ACC);
    }

    /**Original name: LQU-TOT-IMPB-ACC<br>*/
    public AfDecimal getLquTotImpbAcc() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMPB_ACC, Len.Int.LQU_TOT_IMPB_ACC, Len.Fract.LQU_TOT_IMPB_ACC);
    }

    public byte[] getLquTotImpbAccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMPB_ACC, Pos.LQU_TOT_IMPB_ACC);
        return buffer;
    }

    public void setLquTotImpbAccNull(String lquTotImpbAccNull) {
        writeString(Pos.LQU_TOT_IMPB_ACC_NULL, lquTotImpbAccNull, Len.LQU_TOT_IMPB_ACC_NULL);
    }

    /**Original name: LQU-TOT-IMPB-ACC-NULL<br>*/
    public String getLquTotImpbAccNull() {
        return readString(Pos.LQU_TOT_IMPB_ACC_NULL, Len.LQU_TOT_IMPB_ACC_NULL);
    }

    public String getLquTotImpbAccNullFormatted() {
        return Functions.padBlanks(getLquTotImpbAccNull(), Len.LQU_TOT_IMPB_ACC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMPB_ACC = 1;
        public static final int LQU_TOT_IMPB_ACC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMPB_ACC = 8;
        public static final int LQU_TOT_IMPB_ACC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMPB_ACC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMPB_ACC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
