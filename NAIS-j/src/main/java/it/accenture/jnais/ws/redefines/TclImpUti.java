package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-UTI<br>
 * Variable: TCL-IMP-UTI from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpUti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpUti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_UTI;
    }

    public void setTclImpUti(AfDecimal tclImpUti) {
        writeDecimalAsPacked(Pos.TCL_IMP_UTI, tclImpUti.copy());
    }

    public void setTclImpUtiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_UTI, Pos.TCL_IMP_UTI);
    }

    /**Original name: TCL-IMP-UTI<br>*/
    public AfDecimal getTclImpUti() {
        return readPackedAsDecimal(Pos.TCL_IMP_UTI, Len.Int.TCL_IMP_UTI, Len.Fract.TCL_IMP_UTI);
    }

    public byte[] getTclImpUtiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_UTI, Pos.TCL_IMP_UTI);
        return buffer;
    }

    public void setTclImpUtiNull(String tclImpUtiNull) {
        writeString(Pos.TCL_IMP_UTI_NULL, tclImpUtiNull, Len.TCL_IMP_UTI_NULL);
    }

    /**Original name: TCL-IMP-UTI-NULL<br>*/
    public String getTclImpUtiNull() {
        return readString(Pos.TCL_IMP_UTI_NULL, Len.TCL_IMP_UTI_NULL);
    }

    public String getTclImpUtiNullFormatted() {
        return Functions.padBlanks(getTclImpUtiNull(), Len.TCL_IMP_UTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_UTI = 1;
        public static final int TCL_IMP_UTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_UTI = 8;
        public static final int TCL_IMP_UTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_UTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_UTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
