package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPVT-REMUN-ASS<br>
 * Variable: WPVT-REMUN-ASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_REMUN_ASS;
    }

    public void setWpvtRemunAss(AfDecimal wpvtRemunAss) {
        writeDecimalAsPacked(Pos.WPVT_REMUN_ASS, wpvtRemunAss.copy());
    }

    public void setWpvtRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_REMUN_ASS, Pos.WPVT_REMUN_ASS);
    }

    /**Original name: WPVT-REMUN-ASS<br>*/
    public AfDecimal getWpvtRemunAss() {
        return readPackedAsDecimal(Pos.WPVT_REMUN_ASS, Len.Int.WPVT_REMUN_ASS, Len.Fract.WPVT_REMUN_ASS);
    }

    public byte[] getWpvtRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_REMUN_ASS, Pos.WPVT_REMUN_ASS);
        return buffer;
    }

    public void initWpvtRemunAssSpaces() {
        fill(Pos.WPVT_REMUN_ASS, Len.WPVT_REMUN_ASS, Types.SPACE_CHAR);
    }

    public void setWpvtRemunAssNull(String wpvtRemunAssNull) {
        writeString(Pos.WPVT_REMUN_ASS_NULL, wpvtRemunAssNull, Len.WPVT_REMUN_ASS_NULL);
    }

    /**Original name: WPVT-REMUN-ASS-NULL<br>*/
    public String getWpvtRemunAssNull() {
        return readString(Pos.WPVT_REMUN_ASS_NULL, Len.WPVT_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_REMUN_ASS = 1;
        public static final int WPVT_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_REMUN_ASS = 8;
        public static final int WPVT_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPVT_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
