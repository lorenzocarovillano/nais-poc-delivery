package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-ID-RICH-INVST-FND<br>
 * Variable: VAS-ID-RICH-INVST-FND from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasIdRichInvstFnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasIdRichInvstFnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_ID_RICH_INVST_FND;
    }

    public void setVasIdRichInvstFnd(int vasIdRichInvstFnd) {
        writeIntAsPacked(Pos.VAS_ID_RICH_INVST_FND, vasIdRichInvstFnd, Len.Int.VAS_ID_RICH_INVST_FND);
    }

    public void setVasIdRichInvstFndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_ID_RICH_INVST_FND, Pos.VAS_ID_RICH_INVST_FND);
    }

    /**Original name: VAS-ID-RICH-INVST-FND<br>*/
    public int getVasIdRichInvstFnd() {
        return readPackedAsInt(Pos.VAS_ID_RICH_INVST_FND, Len.Int.VAS_ID_RICH_INVST_FND);
    }

    public byte[] getVasIdRichInvstFndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_ID_RICH_INVST_FND, Pos.VAS_ID_RICH_INVST_FND);
        return buffer;
    }

    public void setVasIdRichInvstFndNull(String vasIdRichInvstFndNull) {
        writeString(Pos.VAS_ID_RICH_INVST_FND_NULL, vasIdRichInvstFndNull, Len.VAS_ID_RICH_INVST_FND_NULL);
    }

    /**Original name: VAS-ID-RICH-INVST-FND-NULL<br>*/
    public String getVasIdRichInvstFndNull() {
        return readString(Pos.VAS_ID_RICH_INVST_FND_NULL, Len.VAS_ID_RICH_INVST_FND_NULL);
    }

    public String getVasIdRichInvstFndNullFormatted() {
        return Functions.padBlanks(getVasIdRichInvstFndNull(), Len.VAS_ID_RICH_INVST_FND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_ID_RICH_INVST_FND = 1;
        public static final int VAS_ID_RICH_INVST_FND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_ID_RICH_INVST_FND = 5;
        public static final int VAS_ID_RICH_INVST_FND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_ID_RICH_INVST_FND = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
