package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-VAL-RISC-BENE<br>
 * Variable: P67-VAL-RISC-BENE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67ValRiscBene extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67ValRiscBene() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_VAL_RISC_BENE;
    }

    public void setP67ValRiscBene(AfDecimal p67ValRiscBene) {
        writeDecimalAsPacked(Pos.P67_VAL_RISC_BENE, p67ValRiscBene.copy());
    }

    public void setP67ValRiscBeneFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_VAL_RISC_BENE, Pos.P67_VAL_RISC_BENE);
    }

    /**Original name: P67-VAL-RISC-BENE<br>*/
    public AfDecimal getP67ValRiscBene() {
        return readPackedAsDecimal(Pos.P67_VAL_RISC_BENE, Len.Int.P67_VAL_RISC_BENE, Len.Fract.P67_VAL_RISC_BENE);
    }

    public byte[] getP67ValRiscBeneAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_VAL_RISC_BENE, Pos.P67_VAL_RISC_BENE);
        return buffer;
    }

    public void setP67ValRiscBeneNull(String p67ValRiscBeneNull) {
        writeString(Pos.P67_VAL_RISC_BENE_NULL, p67ValRiscBeneNull, Len.P67_VAL_RISC_BENE_NULL);
    }

    /**Original name: P67-VAL-RISC-BENE-NULL<br>*/
    public String getP67ValRiscBeneNull() {
        return readString(Pos.P67_VAL_RISC_BENE_NULL, Len.P67_VAL_RISC_BENE_NULL);
    }

    public String getP67ValRiscBeneNullFormatted() {
        return Functions.padBlanks(getP67ValRiscBeneNull(), Len.P67_VAL_RISC_BENE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_VAL_RISC_BENE = 1;
        public static final int P67_VAL_RISC_BENE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_VAL_RISC_BENE = 8;
        public static final int P67_VAL_RISC_BENE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_VAL_RISC_BENE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_VAL_RISC_BENE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
