package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-MANFEE-ANTIC<br>
 * Variable: WTGA-MANFEE-ANTIC from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_MANFEE_ANTIC;
    }

    public void setWtgaManfeeAntic(AfDecimal wtgaManfeeAntic) {
        writeDecimalAsPacked(Pos.WTGA_MANFEE_ANTIC, wtgaManfeeAntic.copy());
    }

    public void setWtgaManfeeAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_MANFEE_ANTIC, Pos.WTGA_MANFEE_ANTIC);
    }

    /**Original name: WTGA-MANFEE-ANTIC<br>*/
    public AfDecimal getWtgaManfeeAntic() {
        return readPackedAsDecimal(Pos.WTGA_MANFEE_ANTIC, Len.Int.WTGA_MANFEE_ANTIC, Len.Fract.WTGA_MANFEE_ANTIC);
    }

    public byte[] getWtgaManfeeAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_MANFEE_ANTIC, Pos.WTGA_MANFEE_ANTIC);
        return buffer;
    }

    public void initWtgaManfeeAnticSpaces() {
        fill(Pos.WTGA_MANFEE_ANTIC, Len.WTGA_MANFEE_ANTIC, Types.SPACE_CHAR);
    }

    public void setWtgaManfeeAnticNull(String wtgaManfeeAnticNull) {
        writeString(Pos.WTGA_MANFEE_ANTIC_NULL, wtgaManfeeAnticNull, Len.WTGA_MANFEE_ANTIC_NULL);
    }

    /**Original name: WTGA-MANFEE-ANTIC-NULL<br>*/
    public String getWtgaManfeeAnticNull() {
        return readString(Pos.WTGA_MANFEE_ANTIC_NULL, Len.WTGA_MANFEE_ANTIC_NULL);
    }

    public String getWtgaManfeeAnticNullFormatted() {
        return Functions.padBlanks(getWtgaManfeeAnticNull(), Len.WTGA_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_MANFEE_ANTIC = 1;
        public static final int WTGA_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_MANFEE_ANTIC = 8;
        public static final int WTGA_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
