package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: CAMPO-DECIMALI<br>
 * Variable: CAMPO-DECIMALI from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CampoDecimali extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELE_STRINGA_DECIMALI_MAXOCCURS = 18;

    //==== CONSTRUCTORS ====
    public CampoDecimali() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.CAMPO_DECIMALI;
    }

    public void setCampoDecimali(String campoDecimali) {
        writeString(Pos.CAMPO_DECIMALI, campoDecimali, Len.CAMPO_DECIMALI);
    }

    /**Original name: CAMPO-DECIMALI<br>*/
    public String getCampoDecimali() {
        return readString(Pos.CAMPO_DECIMALI, Len.CAMPO_DECIMALI);
    }

    public String getCampoDecimaliFormatted() {
        return Functions.padBlanks(getCampoDecimali(), Len.CAMPO_DECIMALI);
    }

    public void setEleStringaDecimali(int eleStringaDecimaliIdx, char eleStringaDecimali) {
        int position = Pos.eleStringaDecimali(eleStringaDecimaliIdx - 1);
        writeChar(position, eleStringaDecimali);
    }

    /**Original name: ELE-STRINGA-DECIMALI<br>*/
    public char getEleStringaDecimali(int eleStringaDecimaliIdx) {
        int position = Pos.eleStringaDecimali(eleStringaDecimaliIdx - 1);
        return readChar(position);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int CAMPO_DECIMALI = 1;
        public static final int ARRAY_STRINGA_DECIMALI = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int eleStringaDecimali(int idx) {
            return ARRAY_STRINGA_DECIMALI + idx * Len.ELE_STRINGA_DECIMALI;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_STRINGA_DECIMALI = 1;
        public static final int CAMPO_DECIMALI = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
