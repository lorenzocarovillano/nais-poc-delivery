package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-DT-CAMBIO-VLT<br>
 * Variable: WTIT-DT-CAMBIO-VLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitDtCambioVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitDtCambioVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_DT_CAMBIO_VLT;
    }

    public void setWtitDtCambioVlt(int wtitDtCambioVlt) {
        writeIntAsPacked(Pos.WTIT_DT_CAMBIO_VLT, wtitDtCambioVlt, Len.Int.WTIT_DT_CAMBIO_VLT);
    }

    public void setWtitDtCambioVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_DT_CAMBIO_VLT, Pos.WTIT_DT_CAMBIO_VLT);
    }

    /**Original name: WTIT-DT-CAMBIO-VLT<br>*/
    public int getWtitDtCambioVlt() {
        return readPackedAsInt(Pos.WTIT_DT_CAMBIO_VLT, Len.Int.WTIT_DT_CAMBIO_VLT);
    }

    public byte[] getWtitDtCambioVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_DT_CAMBIO_VLT, Pos.WTIT_DT_CAMBIO_VLT);
        return buffer;
    }

    public void initWtitDtCambioVltSpaces() {
        fill(Pos.WTIT_DT_CAMBIO_VLT, Len.WTIT_DT_CAMBIO_VLT, Types.SPACE_CHAR);
    }

    public void setWtitDtCambioVltNull(String wtitDtCambioVltNull) {
        writeString(Pos.WTIT_DT_CAMBIO_VLT_NULL, wtitDtCambioVltNull, Len.WTIT_DT_CAMBIO_VLT_NULL);
    }

    /**Original name: WTIT-DT-CAMBIO-VLT-NULL<br>*/
    public String getWtitDtCambioVltNull() {
        return readString(Pos.WTIT_DT_CAMBIO_VLT_NULL, Len.WTIT_DT_CAMBIO_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_DT_CAMBIO_VLT = 1;
        public static final int WTIT_DT_CAMBIO_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_DT_CAMBIO_VLT = 5;
        public static final int WTIT_DT_CAMBIO_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_DT_CAMBIO_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
