package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-TP-MOVI<br>
 * Variable: WPMO-TP-MOVI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoTpMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoTpMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_TP_MOVI;
    }

    public void setWpmoTpMovi(int wpmoTpMovi) {
        writeIntAsPacked(Pos.WPMO_TP_MOVI, wpmoTpMovi, Len.Int.WPMO_TP_MOVI);
    }

    public void setWpmoTpMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_TP_MOVI, Pos.WPMO_TP_MOVI);
    }

    /**Original name: WPMO-TP-MOVI<br>*/
    public int getWpmoTpMovi() {
        return readPackedAsInt(Pos.WPMO_TP_MOVI, Len.Int.WPMO_TP_MOVI);
    }

    public byte[] getWpmoTpMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_TP_MOVI, Pos.WPMO_TP_MOVI);
        return buffer;
    }

    public void initWpmoTpMoviSpaces() {
        fill(Pos.WPMO_TP_MOVI, Len.WPMO_TP_MOVI, Types.SPACE_CHAR);
    }

    public void setWpmoTpMoviNull(String wpmoTpMoviNull) {
        writeString(Pos.WPMO_TP_MOVI_NULL, wpmoTpMoviNull, Len.WPMO_TP_MOVI_NULL);
    }

    /**Original name: WPMO-TP-MOVI-NULL<br>*/
    public String getWpmoTpMoviNull() {
        return readString(Pos.WPMO_TP_MOVI_NULL, Len.WPMO_TP_MOVI_NULL);
    }

    public String getWpmoTpMoviNullFormatted() {
        return Functions.padBlanks(getWpmoTpMoviNull(), Len.WPMO_TP_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_TP_MOVI = 1;
        public static final int WPMO_TP_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_TP_MOVI = 3;
        public static final int WPMO_TP_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_TP_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
