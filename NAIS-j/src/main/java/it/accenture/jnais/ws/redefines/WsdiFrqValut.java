package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WSDI-FRQ-VALUT<br>
 * Variable: WSDI-FRQ-VALUT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsdiFrqValut extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsdiFrqValut() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSDI_FRQ_VALUT;
    }

    public void setWsdiFrqValut(int wsdiFrqValut) {
        writeIntAsPacked(Pos.WSDI_FRQ_VALUT, wsdiFrqValut, Len.Int.WSDI_FRQ_VALUT);
    }

    public void setWsdiFrqValutFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSDI_FRQ_VALUT, Pos.WSDI_FRQ_VALUT);
    }

    /**Original name: WSDI-FRQ-VALUT<br>*/
    public int getWsdiFrqValut() {
        return readPackedAsInt(Pos.WSDI_FRQ_VALUT, Len.Int.WSDI_FRQ_VALUT);
    }

    public byte[] getWsdiFrqValutAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSDI_FRQ_VALUT, Pos.WSDI_FRQ_VALUT);
        return buffer;
    }

    public void initWsdiFrqValutSpaces() {
        fill(Pos.WSDI_FRQ_VALUT, Len.WSDI_FRQ_VALUT, Types.SPACE_CHAR);
    }

    public void setWsdiFrqValutNull(String wsdiFrqValutNull) {
        writeString(Pos.WSDI_FRQ_VALUT_NULL, wsdiFrqValutNull, Len.WSDI_FRQ_VALUT_NULL);
    }

    /**Original name: WSDI-FRQ-VALUT-NULL<br>*/
    public String getWsdiFrqValutNull() {
        return readString(Pos.WSDI_FRQ_VALUT_NULL, Len.WSDI_FRQ_VALUT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSDI_FRQ_VALUT = 1;
        public static final int WSDI_FRQ_VALUT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WSDI_FRQ_VALUT = 3;
        public static final int WSDI_FRQ_VALUT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WSDI_FRQ_VALUT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
