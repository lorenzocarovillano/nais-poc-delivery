package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-EC-IL-COLL<br>
 * Variable: WPCO-DT-ULT-EC-IL-COLL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEcIlColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEcIlColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_EC_IL_COLL;
    }

    public void setWpcoDtUltEcIlColl(int wpcoDtUltEcIlColl) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_EC_IL_COLL, wpcoDtUltEcIlColl, Len.Int.WPCO_DT_ULT_EC_IL_COLL);
    }

    public void setDpcoDtUltEcIlCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_EC_IL_COLL, Pos.WPCO_DT_ULT_EC_IL_COLL);
    }

    /**Original name: WPCO-DT-ULT-EC-IL-COLL<br>*/
    public int getWpcoDtUltEcIlColl() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_EC_IL_COLL, Len.Int.WPCO_DT_ULT_EC_IL_COLL);
    }

    public byte[] getWpcoDtUltEcIlCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_EC_IL_COLL, Pos.WPCO_DT_ULT_EC_IL_COLL);
        return buffer;
    }

    public void setWpcoDtUltEcIlCollNull(String wpcoDtUltEcIlCollNull) {
        writeString(Pos.WPCO_DT_ULT_EC_IL_COLL_NULL, wpcoDtUltEcIlCollNull, Len.WPCO_DT_ULT_EC_IL_COLL_NULL);
    }

    /**Original name: WPCO-DT-ULT-EC-IL-COLL-NULL<br>*/
    public String getWpcoDtUltEcIlCollNull() {
        return readString(Pos.WPCO_DT_ULT_EC_IL_COLL_NULL, Len.WPCO_DT_ULT_EC_IL_COLL_NULL);
    }

    public String getWpcoDtUltEcIlCollNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEcIlCollNull(), Len.WPCO_DT_ULT_EC_IL_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_IL_COLL = 1;
        public static final int WPCO_DT_ULT_EC_IL_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_IL_COLL = 5;
        public static final int WPCO_DT_ULT_EC_IL_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_EC_IL_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
