package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.occurs.WranTabRappAnag;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0015<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0015Data {

    //==== PROPERTIES ====
    public static final int DRAN_TAB_RAPP_ANAG_MAXOCCURS = 100;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0015";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-RAN
    private short ixTabRan = DefaultValues.BIN_SHORT_VAL;
    //Original name: INPUT-LVVS0000
    private InputLvvs0000 inputLvvs0000 = new InputLvvs0000();
    //Original name: DRAN-ELE-RAPP-ANAG-MAX
    private short dranEleRappAnagMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DRAN-TAB-RAPP-ANAG
    private WranTabRappAnag[] dranTabRappAnag = new WranTabRappAnag[DRAN_TAB_RAPP_ANAG_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();

    //==== CONSTRUCTORS ====
    public Lvvs0015Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dranTabRappAnagIdx = 1; dranTabRappAnagIdx <= DRAN_TAB_RAPP_ANAG_MAXOCCURS; dranTabRappAnagIdx++) {
            dranTabRappAnag[dranTabRappAnagIdx - 1] = new WranTabRappAnag();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabRan(short ixTabRan) {
        this.ixTabRan = ixTabRan;
    }

    public short getIxTabRan() {
        return this.ixTabRan;
    }

    public void setDranAreaRappAnagFormatted(String data) {
        byte[] buffer = new byte[Len.DRAN_AREA_RAPP_ANAG];
        MarshalByte.writeString(buffer, 1, data, Len.DRAN_AREA_RAPP_ANAG);
        setDranAreaRappAnagBytes(buffer, 1);
    }

    public void setDranAreaRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        dranEleRappAnagMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DRAN_TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dranTabRappAnag[idx - 1].setWranTabRappAnagBytes(buffer, position);
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
            else {
                dranTabRappAnag[idx - 1].initWranTabRappAnagSpaces();
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
        }
    }

    public void setDranEleRappAnagMax(short dranEleRappAnagMax) {
        this.dranEleRappAnagMax = dranEleRappAnagMax;
    }

    public short getDranEleRappAnagMax() {
        return this.dranEleRappAnagMax;
    }

    public WranTabRappAnag getDranTabRappAnag(int idx) {
        return dranTabRappAnag[idx - 1];
    }

    public InputLvvs0000 getInputLvvs0000() {
        return inputLvvs0000;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DRAN_ELE_RAPP_ANAG_MAX = 2;
        public static final int DRAN_AREA_RAPP_ANAG = DRAN_ELE_RAPP_ANAG_MAX + Lvvs0015Data.DRAN_TAB_RAPP_ANAG_MAXOCCURS * WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
