package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRE-CASO-MOR<br>
 * Variable: WPAG-PRE-CASO-MOR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPreCasoMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPreCasoMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRE_CASO_MOR;
    }

    public void setWpagPreCasoMor(AfDecimal wpagPreCasoMor) {
        writeDecimalAsPacked(Pos.WPAG_PRE_CASO_MOR, wpagPreCasoMor.copy());
    }

    public void setWpagPreCasoMorFormatted(String wpagPreCasoMor) {
        setWpagPreCasoMor(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRE_CASO_MOR + Len.Fract.WPAG_PRE_CASO_MOR, Len.Fract.WPAG_PRE_CASO_MOR, wpagPreCasoMor));
    }

    public void setWpagPreCasoMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRE_CASO_MOR, Pos.WPAG_PRE_CASO_MOR);
    }

    /**Original name: WPAG-PRE-CASO-MOR<br>*/
    public AfDecimal getWpagPreCasoMor() {
        return readPackedAsDecimal(Pos.WPAG_PRE_CASO_MOR, Len.Int.WPAG_PRE_CASO_MOR, Len.Fract.WPAG_PRE_CASO_MOR);
    }

    public byte[] getWpagPreCasoMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRE_CASO_MOR, Pos.WPAG_PRE_CASO_MOR);
        return buffer;
    }

    public void initWpagPreCasoMorSpaces() {
        fill(Pos.WPAG_PRE_CASO_MOR, Len.WPAG_PRE_CASO_MOR, Types.SPACE_CHAR);
    }

    public void setWpagPreCasoMorNull(String wpagPreCasoMorNull) {
        writeString(Pos.WPAG_PRE_CASO_MOR_NULL, wpagPreCasoMorNull, Len.WPAG_PRE_CASO_MOR_NULL);
    }

    /**Original name: WPAG-PRE-CASO-MOR-NULL<br>*/
    public String getWpagPreCasoMorNull() {
        return readString(Pos.WPAG_PRE_CASO_MOR_NULL, Len.WPAG_PRE_CASO_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_CASO_MOR = 1;
        public static final int WPAG_PRE_CASO_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_CASO_MOR = 8;
        public static final int WPAG_PRE_CASO_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_CASO_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_CASO_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
