package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-ALQ-MARG-RIS<br>
 * Variable: WB03-ALQ-MARG-RIS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03AlqMargRis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03AlqMargRis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_ALQ_MARG_RIS;
    }

    public void setWb03AlqMargRis(AfDecimal wb03AlqMargRis) {
        writeDecimalAsPacked(Pos.WB03_ALQ_MARG_RIS, wb03AlqMargRis.copy());
    }

    public void setWb03AlqMargRisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_ALQ_MARG_RIS, Pos.WB03_ALQ_MARG_RIS);
    }

    /**Original name: WB03-ALQ-MARG-RIS<br>*/
    public AfDecimal getWb03AlqMargRis() {
        return readPackedAsDecimal(Pos.WB03_ALQ_MARG_RIS, Len.Int.WB03_ALQ_MARG_RIS, Len.Fract.WB03_ALQ_MARG_RIS);
    }

    public byte[] getWb03AlqMargRisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_ALQ_MARG_RIS, Pos.WB03_ALQ_MARG_RIS);
        return buffer;
    }

    public void setWb03AlqMargRisNull(String wb03AlqMargRisNull) {
        writeString(Pos.WB03_ALQ_MARG_RIS_NULL, wb03AlqMargRisNull, Len.WB03_ALQ_MARG_RIS_NULL);
    }

    /**Original name: WB03-ALQ-MARG-RIS-NULL<br>*/
    public String getWb03AlqMargRisNull() {
        return readString(Pos.WB03_ALQ_MARG_RIS_NULL, Len.WB03_ALQ_MARG_RIS_NULL);
    }

    public String getWb03AlqMargRisNullFormatted() {
        return Functions.padBlanks(getWb03AlqMargRisNull(), Len.WB03_ALQ_MARG_RIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_ALQ_MARG_RIS = 1;
        public static final int WB03_ALQ_MARG_RIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ALQ_MARG_RIS = 4;
        public static final int WB03_ALQ_MARG_RIS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ALQ_MARG_RIS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_ALQ_MARG_RIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
