package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMP-NET-EFFLQ<br>
 * Variable: DFL-IMP-NET-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpNetEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpNetEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMP_NET_EFFLQ;
    }

    public void setDflImpNetEfflq(AfDecimal dflImpNetEfflq) {
        writeDecimalAsPacked(Pos.DFL_IMP_NET_EFFLQ, dflImpNetEfflq.copy());
    }

    public void setDflImpNetEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMP_NET_EFFLQ, Pos.DFL_IMP_NET_EFFLQ);
    }

    /**Original name: DFL-IMP-NET-EFFLQ<br>*/
    public AfDecimal getDflImpNetEfflq() {
        return readPackedAsDecimal(Pos.DFL_IMP_NET_EFFLQ, Len.Int.DFL_IMP_NET_EFFLQ, Len.Fract.DFL_IMP_NET_EFFLQ);
    }

    public byte[] getDflImpNetEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMP_NET_EFFLQ, Pos.DFL_IMP_NET_EFFLQ);
        return buffer;
    }

    public void setDflImpNetEfflqNull(String dflImpNetEfflqNull) {
        writeString(Pos.DFL_IMP_NET_EFFLQ_NULL, dflImpNetEfflqNull, Len.DFL_IMP_NET_EFFLQ_NULL);
    }

    /**Original name: DFL-IMP-NET-EFFLQ-NULL<br>*/
    public String getDflImpNetEfflqNull() {
        return readString(Pos.DFL_IMP_NET_EFFLQ_NULL, Len.DFL_IMP_NET_EFFLQ_NULL);
    }

    public String getDflImpNetEfflqNullFormatted() {
        return Functions.padBlanks(getDflImpNetEfflqNull(), Len.DFL_IMP_NET_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMP_NET_EFFLQ = 1;
        public static final int DFL_IMP_NET_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMP_NET_EFFLQ = 8;
        public static final int DFL_IMP_NET_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMP_NET_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMP_NET_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
