package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-FRQ-COSTI-STORNATI<br>
 * Variable: PCO-FRQ-COSTI-STORNATI from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoFrqCostiStornati extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoFrqCostiStornati() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_FRQ_COSTI_STORNATI;
    }

    public void setPcoFrqCostiStornati(int pcoFrqCostiStornati) {
        writeIntAsPacked(Pos.PCO_FRQ_COSTI_STORNATI, pcoFrqCostiStornati, Len.Int.PCO_FRQ_COSTI_STORNATI);
    }

    public void setPcoFrqCostiStornatiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_FRQ_COSTI_STORNATI, Pos.PCO_FRQ_COSTI_STORNATI);
    }

    /**Original name: PCO-FRQ-COSTI-STORNATI<br>*/
    public int getPcoFrqCostiStornati() {
        return readPackedAsInt(Pos.PCO_FRQ_COSTI_STORNATI, Len.Int.PCO_FRQ_COSTI_STORNATI);
    }

    public byte[] getPcoFrqCostiStornatiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_FRQ_COSTI_STORNATI, Pos.PCO_FRQ_COSTI_STORNATI);
        return buffer;
    }

    public void initPcoFrqCostiStornatiHighValues() {
        fill(Pos.PCO_FRQ_COSTI_STORNATI, Len.PCO_FRQ_COSTI_STORNATI, Types.HIGH_CHAR_VAL);
    }

    public void setPcoFrqCostiStornatiNull(String pcoFrqCostiStornatiNull) {
        writeString(Pos.PCO_FRQ_COSTI_STORNATI_NULL, pcoFrqCostiStornatiNull, Len.PCO_FRQ_COSTI_STORNATI_NULL);
    }

    /**Original name: PCO-FRQ-COSTI-STORNATI-NULL<br>*/
    public String getPcoFrqCostiStornatiNull() {
        return readString(Pos.PCO_FRQ_COSTI_STORNATI_NULL, Len.PCO_FRQ_COSTI_STORNATI_NULL);
    }

    public String getPcoFrqCostiStornatiNullFormatted() {
        return Functions.padBlanks(getPcoFrqCostiStornatiNull(), Len.PCO_FRQ_COSTI_STORNATI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_FRQ_COSTI_STORNATI = 1;
        public static final int PCO_FRQ_COSTI_STORNATI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_FRQ_COSTI_STORNATI = 3;
        public static final int PCO_FRQ_COSTI_STORNATI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_FRQ_COSTI_STORNATI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
