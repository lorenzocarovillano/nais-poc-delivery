package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-DT-1O-UTLZ-C-REV<br>
 * Variable: WP67-DT-1O-UTLZ-C-REV from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67Dt1oUtlzCRev extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67Dt1oUtlzCRev() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_DT1O_UTLZ_C_REV;
    }

    public void setWp67Dt1oUtlzCRev(int wp67Dt1oUtlzCRev) {
        writeIntAsPacked(Pos.WP67_DT1O_UTLZ_C_REV, wp67Dt1oUtlzCRev, Len.Int.WP67_DT1O_UTLZ_C_REV);
    }

    public void setWp67Dt1oUtlzCRevFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_DT1O_UTLZ_C_REV, Pos.WP67_DT1O_UTLZ_C_REV);
    }

    /**Original name: WP67-DT-1O-UTLZ-C-REV<br>*/
    public int getWp67Dt1oUtlzCRev() {
        return readPackedAsInt(Pos.WP67_DT1O_UTLZ_C_REV, Len.Int.WP67_DT1O_UTLZ_C_REV);
    }

    public byte[] getWp67Dt1oUtlzCRevAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_DT1O_UTLZ_C_REV, Pos.WP67_DT1O_UTLZ_C_REV);
        return buffer;
    }

    public void setWp67Dt1oUtlzCRevNull(String wp67Dt1oUtlzCRevNull) {
        writeString(Pos.WP67_DT1O_UTLZ_C_REV_NULL, wp67Dt1oUtlzCRevNull, Len.WP67_DT1O_UTLZ_C_REV_NULL);
    }

    /**Original name: WP67-DT-1O-UTLZ-C-REV-NULL<br>*/
    public String getWp67Dt1oUtlzCRevNull() {
        return readString(Pos.WP67_DT1O_UTLZ_C_REV_NULL, Len.WP67_DT1O_UTLZ_C_REV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_DT1O_UTLZ_C_REV = 1;
        public static final int WP67_DT1O_UTLZ_C_REV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_DT1O_UTLZ_C_REV = 5;
        public static final int WP67_DT1O_UTLZ_C_REV_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_DT1O_UTLZ_C_REV = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
