package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP61-MONT-LRD-END2000<br>
 * Variable: WP61-MONT-LRD-END2000 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp61MontLrdEnd2000 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp61MontLrdEnd2000() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP61_MONT_LRD_END2000;
    }

    public void setWp61MontLrdEnd2000(AfDecimal wp61MontLrdEnd2000) {
        writeDecimalAsPacked(Pos.WP61_MONT_LRD_END2000, wp61MontLrdEnd2000.copy());
    }

    public void setWp61MontLrdEnd2000FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP61_MONT_LRD_END2000, Pos.WP61_MONT_LRD_END2000);
    }

    /**Original name: WP61-MONT-LRD-END2000<br>*/
    public AfDecimal getWp61MontLrdEnd2000() {
        return readPackedAsDecimal(Pos.WP61_MONT_LRD_END2000, Len.Int.WP61_MONT_LRD_END2000, Len.Fract.WP61_MONT_LRD_END2000);
    }

    public byte[] getWp61MontLrdEnd2000AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP61_MONT_LRD_END2000, Pos.WP61_MONT_LRD_END2000);
        return buffer;
    }

    public void setWp61MontLrdEnd2000Null(String wp61MontLrdEnd2000Null) {
        writeString(Pos.WP61_MONT_LRD_END2000_NULL, wp61MontLrdEnd2000Null, Len.WP61_MONT_LRD_END2000_NULL);
    }

    /**Original name: WP61-MONT-LRD-END2000-NULL<br>*/
    public String getWp61MontLrdEnd2000Null() {
        return readString(Pos.WP61_MONT_LRD_END2000_NULL, Len.WP61_MONT_LRD_END2000_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP61_MONT_LRD_END2000 = 1;
        public static final int WP61_MONT_LRD_END2000_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_MONT_LRD_END2000 = 8;
        public static final int WP61_MONT_LRD_END2000_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP61_MONT_LRD_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_MONT_LRD_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
