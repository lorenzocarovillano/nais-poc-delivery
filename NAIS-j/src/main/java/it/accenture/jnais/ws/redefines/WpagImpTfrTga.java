package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-TFR-TGA<br>
 * Variable: WPAG-IMP-TFR-TGA from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpTfrTga extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpTfrTga() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_TFR_TGA;
    }

    public void setWpagImpTfrTga(AfDecimal wpagImpTfrTga) {
        writeDecimalAsPacked(Pos.WPAG_IMP_TFR_TGA, wpagImpTfrTga.copy());
    }

    public void setWpagImpTfrTgaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_TFR_TGA, Pos.WPAG_IMP_TFR_TGA);
    }

    /**Original name: WPAG-IMP-TFR-TGA<br>*/
    public AfDecimal getWpagImpTfrTga() {
        return readPackedAsDecimal(Pos.WPAG_IMP_TFR_TGA, Len.Int.WPAG_IMP_TFR_TGA, Len.Fract.WPAG_IMP_TFR_TGA);
    }

    public byte[] getWpagImpTfrTgaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_TFR_TGA, Pos.WPAG_IMP_TFR_TGA);
        return buffer;
    }

    public void initWpagImpTfrTgaSpaces() {
        fill(Pos.WPAG_IMP_TFR_TGA, Len.WPAG_IMP_TFR_TGA, Types.SPACE_CHAR);
    }

    public void setWpagImpTfrTgaNull(String wpagImpTfrTgaNull) {
        writeString(Pos.WPAG_IMP_TFR_TGA_NULL, wpagImpTfrTgaNull, Len.WPAG_IMP_TFR_TGA_NULL);
    }

    /**Original name: WPAG-IMP-TFR-TGA-NULL<br>*/
    public String getWpagImpTfrTgaNull() {
        return readString(Pos.WPAG_IMP_TFR_TGA_NULL, Len.WPAG_IMP_TFR_TGA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_TFR_TGA = 1;
        public static final int WPAG_IMP_TFR_TGA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_TFR_TGA = 8;
        public static final int WPAG_IMP_TFR_TGA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_TFR_TGA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_TFR_TGA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
