package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L23-DT-PROVV-SEQ<br>
 * Variable: L23-DT-PROVV-SEQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L23DtProvvSeq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L23DtProvvSeq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L23_DT_PROVV_SEQ;
    }

    public void setL23DtProvvSeq(int l23DtProvvSeq) {
        writeIntAsPacked(Pos.L23_DT_PROVV_SEQ, l23DtProvvSeq, Len.Int.L23_DT_PROVV_SEQ);
    }

    public void setL23DtProvvSeqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L23_DT_PROVV_SEQ, Pos.L23_DT_PROVV_SEQ);
    }

    /**Original name: L23-DT-PROVV-SEQ<br>*/
    public int getL23DtProvvSeq() {
        return readPackedAsInt(Pos.L23_DT_PROVV_SEQ, Len.Int.L23_DT_PROVV_SEQ);
    }

    public byte[] getL23DtProvvSeqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L23_DT_PROVV_SEQ, Pos.L23_DT_PROVV_SEQ);
        return buffer;
    }

    public void setL23DtProvvSeqNull(String l23DtProvvSeqNull) {
        writeString(Pos.L23_DT_PROVV_SEQ_NULL, l23DtProvvSeqNull, Len.L23_DT_PROVV_SEQ_NULL);
    }

    /**Original name: L23-DT-PROVV-SEQ-NULL<br>*/
    public String getL23DtProvvSeqNull() {
        return readString(Pos.L23_DT_PROVV_SEQ_NULL, Len.L23_DT_PROVV_SEQ_NULL);
    }

    public String getL23DtProvvSeqNullFormatted() {
        return Functions.padBlanks(getL23DtProvvSeqNull(), Len.L23_DT_PROVV_SEQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L23_DT_PROVV_SEQ = 1;
        public static final int L23_DT_PROVV_SEQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L23_DT_PROVV_SEQ = 5;
        public static final int L23_DT_PROVV_SEQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L23_DT_PROVV_SEQ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
