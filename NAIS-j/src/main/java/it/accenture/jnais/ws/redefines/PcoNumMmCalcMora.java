package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-NUM-MM-CALC-MORA<br>
 * Variable: PCO-NUM-MM-CALC-MORA from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoNumMmCalcMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoNumMmCalcMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_NUM_MM_CALC_MORA;
    }

    public void setPcoNumMmCalcMora(int pcoNumMmCalcMora) {
        writeIntAsPacked(Pos.PCO_NUM_MM_CALC_MORA, pcoNumMmCalcMora, Len.Int.PCO_NUM_MM_CALC_MORA);
    }

    public void setPcoNumMmCalcMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_NUM_MM_CALC_MORA, Pos.PCO_NUM_MM_CALC_MORA);
    }

    /**Original name: PCO-NUM-MM-CALC-MORA<br>*/
    public int getPcoNumMmCalcMora() {
        return readPackedAsInt(Pos.PCO_NUM_MM_CALC_MORA, Len.Int.PCO_NUM_MM_CALC_MORA);
    }

    public byte[] getPcoNumMmCalcMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_NUM_MM_CALC_MORA, Pos.PCO_NUM_MM_CALC_MORA);
        return buffer;
    }

    public void initPcoNumMmCalcMoraHighValues() {
        fill(Pos.PCO_NUM_MM_CALC_MORA, Len.PCO_NUM_MM_CALC_MORA, Types.HIGH_CHAR_VAL);
    }

    public void setPcoNumMmCalcMoraNull(String pcoNumMmCalcMoraNull) {
        writeString(Pos.PCO_NUM_MM_CALC_MORA_NULL, pcoNumMmCalcMoraNull, Len.PCO_NUM_MM_CALC_MORA_NULL);
    }

    /**Original name: PCO-NUM-MM-CALC-MORA-NULL<br>*/
    public String getPcoNumMmCalcMoraNull() {
        return readString(Pos.PCO_NUM_MM_CALC_MORA_NULL, Len.PCO_NUM_MM_CALC_MORA_NULL);
    }

    public String getPcoNumMmCalcMoraNullFormatted() {
        return Functions.padBlanks(getPcoNumMmCalcMoraNull(), Len.PCO_NUM_MM_CALC_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_NUM_MM_CALC_MORA = 1;
        public static final int PCO_NUM_MM_CALC_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_NUM_MM_CALC_MORA = 3;
        public static final int PCO_NUM_MM_CALC_MORA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_NUM_MM_CALC_MORA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
