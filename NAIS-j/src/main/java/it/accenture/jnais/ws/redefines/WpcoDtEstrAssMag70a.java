package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ESTR-ASS-MAG70A<br>
 * Variable: WPCO-DT-ESTR-ASS-MAG70A from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtEstrAssMag70a extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtEstrAssMag70a() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ESTR_ASS_MAG70A;
    }

    public void setWpcoDtEstrAssMag70a(int wpcoDtEstrAssMag70a) {
        writeIntAsPacked(Pos.WPCO_DT_ESTR_ASS_MAG70A, wpcoDtEstrAssMag70a, Len.Int.WPCO_DT_ESTR_ASS_MAG70A);
    }

    public void setDpcoDtEstrAssMag70aFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ESTR_ASS_MAG70A, Pos.WPCO_DT_ESTR_ASS_MAG70A);
    }

    /**Original name: WPCO-DT-ESTR-ASS-MAG70A<br>*/
    public int getWpcoDtEstrAssMag70a() {
        return readPackedAsInt(Pos.WPCO_DT_ESTR_ASS_MAG70A, Len.Int.WPCO_DT_ESTR_ASS_MAG70A);
    }

    public byte[] getWpcoDtEstrAssMag70aAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ESTR_ASS_MAG70A, Pos.WPCO_DT_ESTR_ASS_MAG70A);
        return buffer;
    }

    public void setWpcoDtEstrAssMag70aNull(String wpcoDtEstrAssMag70aNull) {
        writeString(Pos.WPCO_DT_ESTR_ASS_MAG70A_NULL, wpcoDtEstrAssMag70aNull, Len.WPCO_DT_ESTR_ASS_MAG70A_NULL);
    }

    /**Original name: WPCO-DT-ESTR-ASS-MAG70A-NULL<br>*/
    public String getWpcoDtEstrAssMag70aNull() {
        return readString(Pos.WPCO_DT_ESTR_ASS_MAG70A_NULL, Len.WPCO_DT_ESTR_ASS_MAG70A_NULL);
    }

    public String getWpcoDtEstrAssMag70aNullFormatted() {
        return Functions.padBlanks(getWpcoDtEstrAssMag70aNull(), Len.WPCO_DT_ESTR_ASS_MAG70A_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ESTR_ASS_MAG70A = 1;
        public static final int WPCO_DT_ESTR_ASS_MAG70A_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ESTR_ASS_MAG70A = 5;
        public static final int WPCO_DT_ESTR_ASS_MAG70A_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ESTR_ASS_MAG70A = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
