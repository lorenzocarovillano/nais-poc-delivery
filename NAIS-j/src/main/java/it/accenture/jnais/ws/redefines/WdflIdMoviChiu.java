package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFL-ID-MOVI-CHIU<br>
 * Variable: WDFL-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ID_MOVI_CHIU;
    }

    public void setWdflIdMoviChiu(int wdflIdMoviChiu) {
        writeIntAsPacked(Pos.WDFL_ID_MOVI_CHIU, wdflIdMoviChiu, Len.Int.WDFL_ID_MOVI_CHIU);
    }

    public void setWdflIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ID_MOVI_CHIU, Pos.WDFL_ID_MOVI_CHIU);
    }

    /**Original name: WDFL-ID-MOVI-CHIU<br>*/
    public int getWdflIdMoviChiu() {
        return readPackedAsInt(Pos.WDFL_ID_MOVI_CHIU, Len.Int.WDFL_ID_MOVI_CHIU);
    }

    public byte[] getWdflIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ID_MOVI_CHIU, Pos.WDFL_ID_MOVI_CHIU);
        return buffer;
    }

    public void setWdflIdMoviChiuNull(String wdflIdMoviChiuNull) {
        writeString(Pos.WDFL_ID_MOVI_CHIU_NULL, wdflIdMoviChiuNull, Len.WDFL_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WDFL-ID-MOVI-CHIU-NULL<br>*/
    public String getWdflIdMoviChiuNull() {
        return readString(Pos.WDFL_ID_MOVI_CHIU_NULL, Len.WDFL_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ID_MOVI_CHIU = 1;
        public static final int WDFL_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ID_MOVI_CHIU = 5;
        public static final int WDFL_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
