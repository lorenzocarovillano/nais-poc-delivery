package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-PC-OPZ<br>
 * Variable: GRZ-PC-OPZ from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzPcOpz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzPcOpz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_PC_OPZ;
    }

    public void setGrzPcOpz(AfDecimal grzPcOpz) {
        writeDecimalAsPacked(Pos.GRZ_PC_OPZ, grzPcOpz.copy());
    }

    public void setGrzPcOpzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_PC_OPZ, Pos.GRZ_PC_OPZ);
    }

    /**Original name: GRZ-PC-OPZ<br>*/
    public AfDecimal getGrzPcOpz() {
        return readPackedAsDecimal(Pos.GRZ_PC_OPZ, Len.Int.GRZ_PC_OPZ, Len.Fract.GRZ_PC_OPZ);
    }

    public byte[] getGrzPcOpzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_PC_OPZ, Pos.GRZ_PC_OPZ);
        return buffer;
    }

    public void setGrzPcOpzNull(String grzPcOpzNull) {
        writeString(Pos.GRZ_PC_OPZ_NULL, grzPcOpzNull, Len.GRZ_PC_OPZ_NULL);
    }

    /**Original name: GRZ-PC-OPZ-NULL<br>*/
    public String getGrzPcOpzNull() {
        return readString(Pos.GRZ_PC_OPZ_NULL, Len.GRZ_PC_OPZ_NULL);
    }

    public String getGrzPcOpzNullFormatted() {
        return Functions.padBlanks(getGrzPcOpzNull(), Len.GRZ_PC_OPZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_PC_OPZ = 1;
        public static final int GRZ_PC_OPZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_PC_OPZ = 4;
        public static final int GRZ_PC_OPZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_PC_OPZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int GRZ_PC_OPZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
