package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABI0011-TEMPORARY-TABLE<br>
 * Variable: IABI0011-TEMPORARY-TABLE from copybook IABI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabi0011TemporaryTable {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TEMPORARY_TABLE);
    public static final String NO_TEMP_TABLE = "NOTABLE";
    public static final String STATIC_TEMP_TABLE = "STATIC";
    public static final String SESSION_TEMP_TABLE = "SESSION";

    //==== METHODS ====
    public void setTemporaryTable(String temporaryTable) {
        this.value = Functions.subString(temporaryTable, Len.TEMPORARY_TABLE);
    }

    public String getTemporaryTable() {
        return this.value;
    }

    public String getTemporaryTableFormatted() {
        return Functions.padBlanks(getTemporaryTable(), Len.TEMPORARY_TABLE);
    }

    public boolean isNoTempTable() {
        return value.equals(NO_TEMP_TABLE);
    }

    public boolean isStaticTempTable() {
        return value.equals(STATIC_TEMP_TABLE);
    }

    public boolean isSessionTempTable() {
        return value.equals(SESSION_TEMP_TABLE);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TEMPORARY_TABLE = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
