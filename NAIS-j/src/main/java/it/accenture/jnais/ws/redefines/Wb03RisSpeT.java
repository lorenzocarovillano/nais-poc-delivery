package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-RIS-SPE-T<br>
 * Variable: WB03-RIS-SPE-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03RisSpeT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03RisSpeT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_RIS_SPE_T;
    }

    public void setWb03RisSpeT(AfDecimal wb03RisSpeT) {
        writeDecimalAsPacked(Pos.WB03_RIS_SPE_T, wb03RisSpeT.copy());
    }

    public void setWb03RisSpeTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_RIS_SPE_T, Pos.WB03_RIS_SPE_T);
    }

    /**Original name: WB03-RIS-SPE-T<br>*/
    public AfDecimal getWb03RisSpeT() {
        return readPackedAsDecimal(Pos.WB03_RIS_SPE_T, Len.Int.WB03_RIS_SPE_T, Len.Fract.WB03_RIS_SPE_T);
    }

    public byte[] getWb03RisSpeTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_RIS_SPE_T, Pos.WB03_RIS_SPE_T);
        return buffer;
    }

    public void setWb03RisSpeTNull(String wb03RisSpeTNull) {
        writeString(Pos.WB03_RIS_SPE_T_NULL, wb03RisSpeTNull, Len.WB03_RIS_SPE_T_NULL);
    }

    /**Original name: WB03-RIS-SPE-T-NULL<br>*/
    public String getWb03RisSpeTNull() {
        return readString(Pos.WB03_RIS_SPE_T_NULL, Len.WB03_RIS_SPE_T_NULL);
    }

    public String getWb03RisSpeTNullFormatted() {
        return Functions.padBlanks(getWb03RisSpeTNull(), Len.WB03_RIS_SPE_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_RIS_SPE_T = 1;
        public static final int WB03_RIS_SPE_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_RIS_SPE_T = 8;
        public static final int WB03_RIS_SPE_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_RIS_SPE_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_RIS_SPE_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
