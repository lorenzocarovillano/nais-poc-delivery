package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-MOVIMENTO<br>
 * Variable: WS-MOVIMENTO from copybook LCCVXMVZ<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsMovimentoLvvs0101 {

    //==== PROPERTIES ====
    private String value = "00000";
    public static final String VARIA_CONTRA = "02001";
    public static final String VARIA_PIANOV = "02002";
    public static final String SOSPE_PIANOV = "02003";
    public static final String RIPRE_PIANOV = "02004";
    public static final String VARIA_RECAPI = "02006";
    public static final String VARIA_STRAIN = "02005";
    public static final String ASSOC_CONVEN = "02007";
    public static final String ESCLU_CONVEN = "02008";
    public static final String VARIA_CONVEN = "02009";
    public static final String VARIA_BENEFI = "02010";
    public static final String VARIA_MODALI = "02011";
    public static final String VARIA_COORDI = "02013";
    public static final String ATTIV_VINCOL = "02015";
    public static final String CESSA_VINCOL = "02016";
    public static final String CESSA_IPEGNO = "02017";
    public static final String CESSA_CPEGNO = "02018";
    public static final String VARIA_PIARIS = "02019";
    public static final String SOSPE_PIARIS = "02020";
    public static final String RIPRE_PIARIS = "02021";
    public static final String INCLU_SOVRAP = "02022";
    public static final String VARIA_SOVRAP = "02023";
    public static final String ESCLU_SOVRAP = "02024";
    public static final String VARIA_TRIACC = "02025";
    public static final String VARIA_CONTAR = "02026";
    public static final String VARIA_CEDOLA = "02027";
    public static final String COMUN_SWITCH_FND = "02028";
    public static final String VARIA_ADEGUA = "02029";
    public static final String VARIA_PARFAT = "02030";
    public static final String VARIA_DATGEN = "02031";
    public static final String VARIA_CLAUSO = "02032";
    public static final String VARIA_BENEF_DFLT = "02033";
    public static final String VARIA_CONTRA_DECES = "02034";
    public static final String VARIA_ETA_COMPU = "02035";
    public static final String FORZA_DT_VALUTA = "02036";
    public static final String VARIA_PREN_TRASF_AGE = "02038";
    public static final String VARIA_BENEF_DFLT_ADES = "02039";
    public static final String RETTIFICA_ANAGRAFICA = "02042";
    public static final String VARIA_PERCIPENTI_DISPOSTI = "02043";
    public static final String RETT_MOV_IN_QUOTE = "02044";
    public static final String INTERESSI_DI_MORA = "02045";
    public static final String ATT_SERVIZIO_BIG_CHANCE = "02048";
    public static final String VAR_SERVIZIO_BIG_CHANCE = "02049";
    public static final String DIS_SERVIZIO_BIG_CHANCE = "02050";
    public static final String ATT_SERVIZIO_CONS_RENDI = "02051";
    public static final String DIS_SERVIZIO_CONS_RENDI = "02052";
    public static final String VAR_RIPART_PREMI = "02053";
    public static final String AMPLIA_POLI = "02055";
    public static final String VAR_DT_CRTF_FSC = "02056";
    public static final String GEST_COORDINATE_BENEF_SCAD = "02057";
    public static final String ATTIVAZ_RISC_PARZ_PROGRAMMATI = "02058";
    public static final String QUICK_PORT_PARZ = "02061";
    public static final String QUICK_PORT_TOT = "02062";
    public static final String BATCH_MASSIVO = "02063";
    public static final String ESCLUS_BATCH_MASSIVO = "02064";
    public static final String ANNULL_ESCLUS_BATCH_MASSIVO = "02065";
    public static final String PRENOTAZ_BATCH_SW_MASSIVO = "02066";
    public static final String RIBILANCIAMENTO_AUTOMATICO = "02067";
    public static final String RIBILANCIAMENTO_PERIODICO = "02068";
    public static final String AGG_AST_ALLOC_RIBIL = "02069";
    public static final String SWITCH_LINEE_INVST = "02071";
    public static final String SWITCH_MASSIVO_PARZIALE = "02072";
    public static final String VARIA_TIT_EFF = "02074";
    public static final String SWITCH_RIBILANCIAMENTO_IFP = "02075";
    public static final String SWITCH_GAP_EVENT = "02076";
    public static final String SWITCH_SUP_SOGLIA = "02077";
    public static final String SWITCH_MASSIVO_FNZ = "02078";
    public static final String BLOCCO_DISP_GAP_EVENT = "02079";
    public static final String BLOCCO_DISP_GEST_SCARTI_FNZ = "02080";
    public static final String RECUP_STRATEGIE_NON_PROT = "02081";
    public static final String GESTI_PRESTI = "02101";
    public static final String RIMBO_PRESTI = "02102";
    public static final String CALC_COSTO_OPER = "02128";
    public static final String VARIA_ASCADE = "02201";
    public static final String VARIA_DIFFER = "02202";
    public static final String VARIA_PROROG = "02203";
    public static final String VARIA_CONVCA = "02204";
    public static final String VARIA_TABAGI = "02205";
    public static final String VARIA_SUBAGE_PROD = "02206";
    public static final String VARIA_SUBAGE_PROD_MASS = "02207";
    public static final String VARIA_FONDO_SPEC = "02208";
    public static final String TRASFER_AGENZIA = "02209";
    public static final String VARIA_ETA_SCAD = "02210";
    public static final String PREN_VARIA_ETA_SCAD = "02214";
    public static final String TRASFER_TESTA_POS = "02211";
    public static final String VARIA_CONVRE = "02212";
    public static final String VARIAZIONE_RID = "02213";
    public static final String VAR_DATI_TRASF = "02215";
    public static final String INS_CNBT_NDED = "02216";
    public static final String VAR_CNBT_NDED = "02217";
    public static final String CAN_CNBT_NDED = "02218";
    public static final String PRENOT_CNBT_NDED = "02219";
    public static final String VARIA_DT_PRESCR = "02220";
    public static final String VARIA_PAGATORE = "02221";
    public static final String ATTIV_FONTI_CONTRB = "02222";
    public static final String ASSOC_MATR_AGENTE = "02223";
    public static final String VARIA_MATR_AGENTE = "02224";
    public static final String ESCLU_MATR_AGENTE = "02225";
    public static final String VARIA_LEG_RAPPR = "02226";
    public static final String POL_FREDM_SLG_CC = "02227";
    public static final String RIATTIVAZ_SERVIZIO_FREEDOM = "02229";
    public static final String VARIA_PAGRID = "02232";
    public static final String VARIA_TITOLARE_EFFETTIVO = "02236";
    public static final String PROVV3_IVASS = "02237";
    public static final String AGG_ANAG_NAIS_TP_DATI_DOC = "02240";
    public static final String INCL_ASSICURATO = "02241";
    public static final String ESCL_ASSICURATO = "02242";
    public static final String ATTIVA_REDDITO_PROGRAMMATO = "02301";
    public static final String ATTIVA_TAKE_PROFIT = "02302";
    public static final String ATTIVA_STOP_LOSS = "02303";
    public static final String ATTIVA_PASSO_PASSO = "02304";
    public static final String ATTIVA_BENEFICIO_CONTR = "02305";
    public static final String VARIAZ_REDDITO_PROGRAMMATO = "02306";
    public static final String VARIAZ_TAKE_PROFIT = "02307";
    public static final String VARIAZ_STOP_LOSS = "02308";
    public static final String VARIAZ_PASSO_PASSO = "02309";
    public static final String VARIAZ_BENEFICIO_CONTR = "02310";
    public static final String CESSAZ_REDDITO_PROGRAMMATO = "02311";
    public static final String CESSAZ_TAKE_PROFIT = "02312";
    public static final String CESSAZ_STOP_LOSS = "02313";
    public static final String CESSAZ_PASSO_PASSO = "02314";
    public static final String CESSAZ_BENEFICIO_CONTR = "02315";
    public static final String RPP_REDDITO_PROGRAMMATO = "02316";
    public static final String LIQUI_RPP_REDDITO_PROGR = "02317";
    public static final String RPP_TAKE_PROFIT = "02318";
    public static final String LIQUI_RPP_TAKE_PROFIT = "02319";
    public static final String SW_STOP_LOSS = "02320";
    public static final String SW_PASSO_PASSO = "02321";
    public static final String RPP_BENEFICIO_CONTR = "02322";
    public static final String LIQUI_RPP_BENEFICIO_CONTR = "02323";
    public static final String COMUN_RISTOT_INCAPIENZA = "02324";
    public static final String LIQUI_RISTOT_INCAPIENZA = "02325";
    public static final String SW_PASSO_PASSO_NON_EROG = "02326";
    public static final String RPP_REDDITO_PROG_NON_EROG = "02327";
    public static final String CALC_VAL_RISC_RPP = "02328";
    public static final String CALC_VAL_RISC_TAKE_PROFIT = "02329";
    public static final String RPP_TAKE_PROFIT_NON_EROG = "02330";
    public static final String VARIAZ_PERC_CEDOLA = "02331";
    public static final String COMUN_RISTOT_INCAP = "02332";
    public static final String LIQUI_RISTOT_INCAP = "02333";
    public static final String PRENOT_APPENDICE_PRESTAZ = "02336";
    public static final String PRENOT_REPORT_NOMINATIVO = "02337";
    public static final String COMUN_PERIOD_SWITCH = "02338";
    public static final String VARIA_MODALI_COLL = "02339";
    public static final String VARIA_COORDI_COLL = "02340";
    public static final String LIQUI_RECADE = "05001";
    public static final String LIQUI_RECIND = "05002";
    public static final String COMUN_RISPAR_ADE = "05003";
    public static final String COMUN_RISPAR_TRA = "05004";
    public static final String COMUN_RISPAR_IND = "05005";
    public static final String LIQUI_RISPAR_POLIND = "05006";
    public static final String LIQUI_RISPAR_TRA = "05007";
    public static final String LIQUI_RISPAR_ADE = "05008";
    public static final String LIQUI_RISTOT_ADE = "05009";
    public static final String LIQUI_RISTOT_IND = "05010";
    public static final String LIQUI_RISTOT_TRA = "05011";
    public static final String LIQUI_TRASF_COLL = "05012";
    public static final String LIQUI_TRASF_INDIVI = "05013";
    public static final String SCANT_ADESIO = "05014";
    public static final String SCANT_INDIVI = "05015";
    public static final String LIQUI_SINADE = "05017";
    public static final String LIQUI_SININD = "05018";
    public static final String COLIQ_SCAIND = "05019";
    public static final String COLIQ_SCADES = "05020";
    public static final String LIQUI_RIMTRA = "05021";
    public static final String COPAG_RENADE = "05022";
    public static final String COPAG_RENIND = "05023";
    public static final String LIQUI_RISADE = "05024";
    public static final String LIQUI_RISPOL = "05025";
    public static final String LIQUI_SCAPOL = "05026";
    public static final String LIQUI_SCADES = "05027";
    public static final String LIQUI_RECES_POL_COL = "05028";
    public static final String LIQUI_RISC_TOT_COL = "05029";
    public static final String LIQUI_REVOC_POL_COL = "05030";
    public static final String PREV_RIS_TOT_ADE = "05031";
    public static final String PREV_RIS_TOT_IND = "05032";
    public static final String PREV_RIS_TOT_COL = "05033";
    public static final String PREV_SCAD_ADE = "05034";
    public static final String PREV_SCAD_IND = "05035";
    public static final String PREV_ANT_RISO_ADE = "05036";
    public static final String PREV_PENS_ADE = "05037";
    public static final String PREV_ANT_ADE = "05038";
    public static final String PREV_ANT_RISO_IND = "05039";
    public static final String PREV_PENS_IND = "05040";
    public static final String PREV_ANT_IND = "05041";
    public static final String SWITCH_OUT = "05042";
    public static final String INT_LIQUIDAZIONE = "05043";
    public static final String LIQUI_RIMB_PRE_ADE = "05044";
    public static final String LIQUI_RISPAR_RPA = "05045";
    public static final String LIQUI_SINISTRO_TERM_FISSO_SCAD = "05050";
    public static final String LIQUI_DISDETTA = "05055";
    public static final String LIQUI_EST_ANTIC_INDIVI = "05056";
    public static final String AUTORIZZA_LIQUI = "05100";
    public static final String RETTIFICA_LIQUI = "05101";
    public static final String SOSPENDI_LIQUI = "05102";
    public static final String CHIUSURA_POLIZZA_GIPSY = "05103";

    //==== METHODS ====
    public void setWsMovimento(int wsMovimento) {
        this.value = NumericDisplay.asString(wsMovimento, Len.WS_MOVIMENTO);
    }

    public void setWsMovimentoFormatted(String wsMovimento) {
        this.value = Trunc.toUnsignedNumeric(wsMovimento, Len.WS_MOVIMENTO);
    }

    public int getWsMovimento() {
        return NumericDisplay.asInt(this.value);
    }

    public String getWsMovimentoFormatted() {
        return this.value;
    }

    public boolean isRppRedditoProgrammato() {
        return getWsMovimentoFormatted().equals(RPP_REDDITO_PROGRAMMATO);
    }

    public void setRppRedditoProgrammato() {
        setWsMovimentoFormatted(RPP_REDDITO_PROGRAMMATO);
    }

    public boolean isLiquiRppRedditoProgr() {
        return getWsMovimentoFormatted().equals(LIQUI_RPP_REDDITO_PROGR);
    }

    public boolean isRppTakeProfit() {
        return getWsMovimentoFormatted().equals(RPP_TAKE_PROFIT);
    }

    public void setRppTakeProfit() {
        setWsMovimentoFormatted(RPP_TAKE_PROFIT);
    }

    public boolean isLiquiRppTakeProfit() {
        return getWsMovimentoFormatted().equals(LIQUI_RPP_TAKE_PROFIT);
    }

    public boolean isLiquiRppBeneficioContr() {
        return getWsMovimentoFormatted().equals(LIQUI_RPP_BENEFICIO_CONTR);
    }

    public boolean isLiquiRistotIncapienza() {
        return getWsMovimentoFormatted().equals(LIQUI_RISTOT_INCAPIENZA);
    }

    public boolean isLiquiRistotIncap() {
        return getWsMovimentoFormatted().equals(LIQUI_RISTOT_INCAP);
    }

    public boolean isComunRisparAde() {
        return getWsMovimentoFormatted().equals(COMUN_RISPAR_ADE);
    }

    public void setComunRisparAde() {
        setWsMovimentoFormatted(COMUN_RISPAR_ADE);
    }

    public boolean isComunRisparTra() {
        return getWsMovimentoFormatted().equals(COMUN_RISPAR_TRA);
    }

    public boolean isComunRisparInd() {
        return getWsMovimentoFormatted().equals(COMUN_RISPAR_IND);
    }

    public void setComunRisparInd() {
        setWsMovimentoFormatted(COMUN_RISPAR_IND);
    }

    public boolean isLiquiRisparPolind() {
        return getWsMovimentoFormatted().equals(LIQUI_RISPAR_POLIND);
    }

    public boolean isLiquiRisparTra() {
        return getWsMovimentoFormatted().equals(LIQUI_RISPAR_TRA);
    }

    public boolean isLiquiRisparAde() {
        return getWsMovimentoFormatted().equals(LIQUI_RISPAR_ADE);
    }

    public boolean isLiquiRistotInd() {
        return getWsMovimentoFormatted().equals(LIQUI_RISTOT_IND);
    }

    public boolean isLiquiRistotTra() {
        return getWsMovimentoFormatted().equals(LIQUI_RISTOT_TRA);
    }

    public boolean isLiquiTrasfIndivi() {
        return getWsMovimentoFormatted().equals(LIQUI_TRASF_INDIVI);
    }

    public boolean isScantIndivi() {
        return getWsMovimentoFormatted().equals(SCANT_INDIVI);
    }

    public boolean isLiquiSinind() {
        return getWsMovimentoFormatted().equals(LIQUI_SININD);
    }

    public boolean isColiqScaind() {
        return getWsMovimentoFormatted().equals(COLIQ_SCAIND);
    }

    public boolean isLiquiRispol() {
        return getWsMovimentoFormatted().equals(LIQUI_RISPOL);
    }

    public boolean isLiquiScapol() {
        return getWsMovimentoFormatted().equals(LIQUI_SCAPOL);
    }

    public boolean isSwitchOut() {
        return getWsMovimentoFormatted().equals(SWITCH_OUT);
    }

    public boolean isLiquiRisparRpa() {
        return getWsMovimentoFormatted().equals(LIQUI_RISPAR_RPA);
    }

    public boolean isLiquiSinistroTermFissoScad() {
        return getWsMovimentoFormatted().equals(LIQUI_SINISTRO_TERM_FISSO_SCAD);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_MOVIMENTO = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
