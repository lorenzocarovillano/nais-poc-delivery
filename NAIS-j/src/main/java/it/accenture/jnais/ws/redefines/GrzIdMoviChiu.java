package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-ID-MOVI-CHIU<br>
 * Variable: GRZ-ID-MOVI-CHIU from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_ID_MOVI_CHIU;
    }

    public void setGrzIdMoviChiu(int grzIdMoviChiu) {
        writeIntAsPacked(Pos.GRZ_ID_MOVI_CHIU, grzIdMoviChiu, Len.Int.GRZ_ID_MOVI_CHIU);
    }

    public void setGrzIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_ID_MOVI_CHIU, Pos.GRZ_ID_MOVI_CHIU);
    }

    /**Original name: GRZ-ID-MOVI-CHIU<br>*/
    public int getGrzIdMoviChiu() {
        return readPackedAsInt(Pos.GRZ_ID_MOVI_CHIU, Len.Int.GRZ_ID_MOVI_CHIU);
    }

    public byte[] getGrzIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_ID_MOVI_CHIU, Pos.GRZ_ID_MOVI_CHIU);
        return buffer;
    }

    public void setGrzIdMoviChiuNull(String grzIdMoviChiuNull) {
        writeString(Pos.GRZ_ID_MOVI_CHIU_NULL, grzIdMoviChiuNull, Len.GRZ_ID_MOVI_CHIU_NULL);
    }

    /**Original name: GRZ-ID-MOVI-CHIU-NULL<br>*/
    public String getGrzIdMoviChiuNull() {
        return readString(Pos.GRZ_ID_MOVI_CHIU_NULL, Len.GRZ_ID_MOVI_CHIU_NULL);
    }

    public String getGrzIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getGrzIdMoviChiuNull(), Len.GRZ_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_ID_MOVI_CHIU = 1;
        public static final int GRZ_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_ID_MOVI_CHIU = 5;
        public static final int GRZ_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
