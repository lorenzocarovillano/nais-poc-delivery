package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ALL-DT-END-VLDT<br>
 * Variable: ALL-DT-END-VLDT from program LCCS1900<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AllDtEndVldt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AllDtEndVldt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ALL_DT_END_VLDT;
    }

    public void setAllDtEndVldt(int allDtEndVldt) {
        writeIntAsPacked(Pos.ALL_DT_END_VLDT, allDtEndVldt, Len.Int.ALL_DT_END_VLDT);
    }

    public void setAllDtEndVldtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ALL_DT_END_VLDT, Pos.ALL_DT_END_VLDT);
    }

    /**Original name: ALL-DT-END-VLDT<br>*/
    public int getAllDtEndVldt() {
        return readPackedAsInt(Pos.ALL_DT_END_VLDT, Len.Int.ALL_DT_END_VLDT);
    }

    public byte[] getAllDtEndVldtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ALL_DT_END_VLDT, Pos.ALL_DT_END_VLDT);
        return buffer;
    }

    public void setAllDtEndVldtNull(String allDtEndVldtNull) {
        writeString(Pos.ALL_DT_END_VLDT_NULL, allDtEndVldtNull, Len.ALL_DT_END_VLDT_NULL);
    }

    /**Original name: ALL-DT-END-VLDT-NULL<br>*/
    public String getAllDtEndVldtNull() {
        return readString(Pos.ALL_DT_END_VLDT_NULL, Len.ALL_DT_END_VLDT_NULL);
    }

    public String getAllDtEndVldtNullFormatted() {
        return Functions.padBlanks(getAllDtEndVldtNull(), Len.ALL_DT_END_VLDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ALL_DT_END_VLDT = 1;
        public static final int ALL_DT_END_VLDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ALL_DT_END_VLDT = 5;
        public static final int ALL_DT_END_VLDT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ALL_DT_END_VLDT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
