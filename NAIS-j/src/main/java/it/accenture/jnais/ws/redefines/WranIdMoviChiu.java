package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRAN-ID-MOVI-CHIU<br>
 * Variable: WRAN-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WranIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WranIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRAN_ID_MOVI_CHIU;
    }

    public void setWranIdMoviChiu(int wranIdMoviChiu) {
        writeIntAsPacked(Pos.WRAN_ID_MOVI_CHIU, wranIdMoviChiu, Len.Int.WRAN_ID_MOVI_CHIU);
    }

    public void setWranIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRAN_ID_MOVI_CHIU, Pos.WRAN_ID_MOVI_CHIU);
    }

    /**Original name: WRAN-ID-MOVI-CHIU<br>*/
    public int getWranIdMoviChiu() {
        return readPackedAsInt(Pos.WRAN_ID_MOVI_CHIU, Len.Int.WRAN_ID_MOVI_CHIU);
    }

    public byte[] getWranIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRAN_ID_MOVI_CHIU, Pos.WRAN_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWranIdMoviChiuSpaces() {
        fill(Pos.WRAN_ID_MOVI_CHIU, Len.WRAN_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWranIdMoviChiuNull(String wranIdMoviChiuNull) {
        writeString(Pos.WRAN_ID_MOVI_CHIU_NULL, wranIdMoviChiuNull, Len.WRAN_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WRAN-ID-MOVI-CHIU-NULL<br>*/
    public String getWranIdMoviChiuNull() {
        return readString(Pos.WRAN_ID_MOVI_CHIU_NULL, Len.WRAN_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRAN_ID_MOVI_CHIU = 1;
        public static final int WRAN_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRAN_ID_MOVI_CHIU = 5;
        public static final int WRAN_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRAN_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
