package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CPT-DT-RIDZ<br>
 * Variable: B03-CPT-DT-RIDZ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CptDtRidz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CptDtRidz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CPT_DT_RIDZ;
    }

    public void setB03CptDtRidz(AfDecimal b03CptDtRidz) {
        writeDecimalAsPacked(Pos.B03_CPT_DT_RIDZ, b03CptDtRidz.copy());
    }

    public void setB03CptDtRidzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CPT_DT_RIDZ, Pos.B03_CPT_DT_RIDZ);
    }

    /**Original name: B03-CPT-DT-RIDZ<br>*/
    public AfDecimal getB03CptDtRidz() {
        return readPackedAsDecimal(Pos.B03_CPT_DT_RIDZ, Len.Int.B03_CPT_DT_RIDZ, Len.Fract.B03_CPT_DT_RIDZ);
    }

    public byte[] getB03CptDtRidzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CPT_DT_RIDZ, Pos.B03_CPT_DT_RIDZ);
        return buffer;
    }

    public void setB03CptDtRidzNull(String b03CptDtRidzNull) {
        writeString(Pos.B03_CPT_DT_RIDZ_NULL, b03CptDtRidzNull, Len.B03_CPT_DT_RIDZ_NULL);
    }

    /**Original name: B03-CPT-DT-RIDZ-NULL<br>*/
    public String getB03CptDtRidzNull() {
        return readString(Pos.B03_CPT_DT_RIDZ_NULL, Len.B03_CPT_DT_RIDZ_NULL);
    }

    public String getB03CptDtRidzNullFormatted() {
        return Functions.padBlanks(getB03CptDtRidzNull(), Len.B03_CPT_DT_RIDZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CPT_DT_RIDZ = 1;
        public static final int B03_CPT_DT_RIDZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CPT_DT_RIDZ = 8;
        public static final int B03_CPT_DT_RIDZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CPT_DT_RIDZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CPT_DT_RIDZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
