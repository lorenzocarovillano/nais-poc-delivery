package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-FINE-ELEMENTI<br>
 * Variable: SW-FINE-ELEMENTI from program IVVS0212<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwFineElementi {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char LEGGI_ELEMENTI = 'N';
    public static final char FINE_ELEMENTI = 'S';

    //==== METHODS ====
    public void setSwFineElementi(char swFineElementi) {
        this.value = swFineElementi;
    }

    public char getSwFineElementi() {
        return this.value;
    }

    public void setLeggiElementi() {
        value = LEGGI_ELEMENTI;
    }

    public boolean isFineElementi() {
        return value == FINE_ELEMENTI;
    }

    public void setFineElementi() {
        value = FINE_ELEMENTI;
    }
}
