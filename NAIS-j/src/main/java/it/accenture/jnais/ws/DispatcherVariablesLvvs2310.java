package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.occurs.WpogTabParamOgg;

/**Original name: DISPATCHER-VARIABLES<br>
 * Variable: DISPATCHER-VARIABLES from program LVVS2310<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DispatcherVariablesLvvs2310 {

    //==== PROPERTIES ====
    public static final int DPOG_TAB_PARAM_OGG_MAXOCCURS = 200;
    //Original name: DPOG-ELE-PARAM-OGG-MAX
    private short dpogEleParamOggMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DPOG-TAB-PARAM-OGG
    private WpogTabParamOgg[] dpogTabParamOgg = new WpogTabParamOgg[DPOG_TAB_PARAM_OGG_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public DispatcherVariablesLvvs2310() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dpogTabParamOggIdx = 1; dpogTabParamOggIdx <= DPOG_TAB_PARAM_OGG_MAXOCCURS; dpogTabParamOggIdx++) {
            dpogTabParamOgg[dpogTabParamOggIdx - 1] = new WpogTabParamOgg();
        }
    }

    public void setDpogAreaParamOggFormatted(String data) {
        byte[] buffer = new byte[Len.DPOG_AREA_PARAM_OGG];
        MarshalByte.writeString(buffer, 1, data, Len.DPOG_AREA_PARAM_OGG);
        setDpogAreaParamOggBytes(buffer, 1);
    }

    public void setDpogAreaParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        dpogEleParamOggMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DPOG_TAB_PARAM_OGG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dpogTabParamOgg[idx - 1].setWpogTabParamOggBytes(buffer, position);
                position += WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
            }
            else {
                dpogTabParamOgg[idx - 1].initWpogTabParamOggSpaces();
                position += WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
            }
        }
    }

    public void setDpogEleParamOggMax(short dpogEleParamOggMax) {
        this.dpogEleParamOggMax = dpogEleParamOggMax;
    }

    public short getDpogEleParamOggMax() {
        return this.dpogEleParamOggMax;
    }

    public WpogTabParamOgg getDpogTabParamOgg(int idx) {
        return dpogTabParamOgg[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DPOG_ELE_PARAM_OGG_MAX = 2;
        public static final int DPOG_AREA_PARAM_OGG = DPOG_ELE_PARAM_OGG_MAX + DispatcherVariablesLvvs2310.DPOG_TAB_PARAM_OGG_MAXOCCURS * WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
