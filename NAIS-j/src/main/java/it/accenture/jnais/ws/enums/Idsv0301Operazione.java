package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0301-OPERAZIONE<br>
 * Variable: IDSV0301-OPERAZIONE from copybook IDSV0301<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0301Operazione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.OPERAZIONE);
    public static final String READ = "RE";
    public static final String READ_NEXT = "RN";
    public static final String WRITE = "WR";
    public static final String DECLARE = "DC";

    //==== METHODS ====
    public void setOperazione(String operazione) {
        this.value = Functions.subString(operazione, Len.OPERAZIONE);
    }

    public String getOperazione() {
        return this.value;
    }

    public boolean isRead() {
        return value.equals(READ);
    }

    public void setRead() {
        value = READ;
    }

    public void setReadNext() {
        value = READ_NEXT;
    }

    public boolean isWrite() {
        return value.equals(WRITE);
    }

    public void setIdsv0301Write() {
        value = WRITE;
    }

    public boolean isDeclare() {
        return value.equals(DECLARE);
    }

    public void setIdsv0301Declare() {
        value = DECLARE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OPERAZIONE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
