package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPST-SOST-K2<br>
 * Variable: WDFA-IMPST-SOST-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpstSostK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpstSostK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPST_SOST_K2;
    }

    public void setWdfaImpstSostK2(AfDecimal wdfaImpstSostK2) {
        writeDecimalAsPacked(Pos.WDFA_IMPST_SOST_K2, wdfaImpstSostK2.copy());
    }

    public void setWdfaImpstSostK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPST_SOST_K2, Pos.WDFA_IMPST_SOST_K2);
    }

    /**Original name: WDFA-IMPST-SOST-K2<br>*/
    public AfDecimal getWdfaImpstSostK2() {
        return readPackedAsDecimal(Pos.WDFA_IMPST_SOST_K2, Len.Int.WDFA_IMPST_SOST_K2, Len.Fract.WDFA_IMPST_SOST_K2);
    }

    public byte[] getWdfaImpstSostK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPST_SOST_K2, Pos.WDFA_IMPST_SOST_K2);
        return buffer;
    }

    public void setWdfaImpstSostK2Null(String wdfaImpstSostK2Null) {
        writeString(Pos.WDFA_IMPST_SOST_K2_NULL, wdfaImpstSostK2Null, Len.WDFA_IMPST_SOST_K2_NULL);
    }

    /**Original name: WDFA-IMPST-SOST-K2-NULL<br>*/
    public String getWdfaImpstSostK2Null() {
        return readString(Pos.WDFA_IMPST_SOST_K2_NULL, Len.WDFA_IMPST_SOST_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_SOST_K2 = 1;
        public static final int WDFA_IMPST_SOST_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_SOST_K2 = 8;
        public static final int WDFA_IMPST_SOST_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_SOST_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_SOST_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
