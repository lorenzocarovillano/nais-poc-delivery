package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: TROVATA-VAR<br>
 * Variable: TROVATA-VAR from program LVVS0039<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TrovataVar {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setTrovataVar(char trovataVar) {
        this.value = trovataVar;
    }

    public char getTrovataVar() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
