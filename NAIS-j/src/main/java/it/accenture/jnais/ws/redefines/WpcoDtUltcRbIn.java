package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-RB-IN<br>
 * Variable: WPCO-DT-ULTC-RB-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcRbIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcRbIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_RB_IN;
    }

    public void setWpcoDtUltcRbIn(int wpcoDtUltcRbIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_RB_IN, wpcoDtUltcRbIn, Len.Int.WPCO_DT_ULTC_RB_IN);
    }

    public void setDpcoDtUltcRbInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_RB_IN, Pos.WPCO_DT_ULTC_RB_IN);
    }

    /**Original name: WPCO-DT-ULTC-RB-IN<br>*/
    public int getWpcoDtUltcRbIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_RB_IN, Len.Int.WPCO_DT_ULTC_RB_IN);
    }

    public byte[] getWpcoDtUltcRbInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_RB_IN, Pos.WPCO_DT_ULTC_RB_IN);
        return buffer;
    }

    public void setWpcoDtUltcRbInNull(String wpcoDtUltcRbInNull) {
        writeString(Pos.WPCO_DT_ULTC_RB_IN_NULL, wpcoDtUltcRbInNull, Len.WPCO_DT_ULTC_RB_IN_NULL);
    }

    /**Original name: WPCO-DT-ULTC-RB-IN-NULL<br>*/
    public String getWpcoDtUltcRbInNull() {
        return readString(Pos.WPCO_DT_ULTC_RB_IN_NULL, Len.WPCO_DT_ULTC_RB_IN_NULL);
    }

    public String getWpcoDtUltcRbInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcRbInNull(), Len.WPCO_DT_ULTC_RB_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_RB_IN = 1;
        public static final int WPCO_DT_ULTC_RB_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_RB_IN = 5;
        public static final int WPCO_DT_ULTC_RB_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_RB_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
