package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IVVC0211-STEP-ELAB<br>
 * Variable: IVVC0211-STEP-ELAB from copybook IVVC0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ivvc0211StepElab {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char PRE_CONV = '0';
    public static final char POST_CONV = '1';
    public static final char IN_CONV = '2';
    public static final char PROCESSO_ADESIONE = '3';
    public static final char CONTROLLO_INIZIATIVA = '4';
    public static final char ATTIVAZ_INIZIATIVA = '5';
    public static final char PRE_CALC_INIZIATIVA = '6';
    public static final char POST_CALC_INIZIATIVA = '7';

    //==== METHODS ====
    public void setStepElab(char stepElab) {
        this.value = stepElab;
    }

    public char getStepElab() {
        return this.value;
    }

    public boolean isPreConv() {
        return value == PRE_CONV;
    }

    public boolean isPostConv() {
        return value == POST_CONV;
    }

    public boolean isInConv() {
        return value == IN_CONV;
    }

    public void setS211InConv() {
        value = IN_CONV;
    }

    public boolean isPreCalcIniziativa() {
        return value == PRE_CALC_INIZIATIVA;
    }

    public boolean isPostCalcIniziativa() {
        return value == POST_CALC_INIZIATIVA;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int STEP_ELAB = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
