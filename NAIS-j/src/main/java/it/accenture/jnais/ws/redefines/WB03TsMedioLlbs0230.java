package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-TS-MEDIO<br>
 * Variable: W-B03-TS-MEDIO from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03TsMedioLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03TsMedioLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_TS_MEDIO;
    }

    public void setwB03TsMedio(AfDecimal wB03TsMedio) {
        writeDecimalAsPacked(Pos.W_B03_TS_MEDIO, wB03TsMedio.copy());
    }

    /**Original name: W-B03-TS-MEDIO<br>*/
    public AfDecimal getwB03TsMedio() {
        return readPackedAsDecimal(Pos.W_B03_TS_MEDIO, Len.Int.W_B03_TS_MEDIO, Len.Fract.W_B03_TS_MEDIO);
    }

    public byte[] getwB03TsMedioAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_TS_MEDIO, Pos.W_B03_TS_MEDIO);
        return buffer;
    }

    public void setwB03TsMedioNull(String wB03TsMedioNull) {
        writeString(Pos.W_B03_TS_MEDIO_NULL, wB03TsMedioNull, Len.W_B03_TS_MEDIO_NULL);
    }

    /**Original name: W-B03-TS-MEDIO-NULL<br>*/
    public String getwB03TsMedioNull() {
        return readString(Pos.W_B03_TS_MEDIO_NULL, Len.W_B03_TS_MEDIO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_TS_MEDIO = 1;
        public static final int W_B03_TS_MEDIO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_TS_MEDIO = 8;
        public static final int W_B03_TS_MEDIO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_TS_MEDIO = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_TS_MEDIO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
