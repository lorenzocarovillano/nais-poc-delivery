package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WPAG-DT-END-COP-TIT<br>
 * Variable: WPAG-DT-END-COP-TIT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagDtEndCopTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagDtEndCopTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_DT_END_COP_TIT;
    }

    public void setWpagDtEndCopTit(int wpagDtEndCopTit) {
        writeIntAsPacked(Pos.WPAG_DT_END_COP_TIT, wpagDtEndCopTit, Len.Int.WPAG_DT_END_COP_TIT);
    }

    public void setWpagDtEndCopTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_DT_END_COP_TIT, Pos.WPAG_DT_END_COP_TIT);
    }

    /**Original name: WPAG-DT-END-COP-TIT<br>
	 * <pre>--- Data fine copertura titolo contabile</pre>*/
    public int getWpagDtEndCopTit() {
        return readPackedAsInt(Pos.WPAG_DT_END_COP_TIT, Len.Int.WPAG_DT_END_COP_TIT);
    }

    public byte[] getWpagDtEndCopTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_DT_END_COP_TIT, Pos.WPAG_DT_END_COP_TIT);
        return buffer;
    }

    public void setWpagDtEndCopTitNull(String wpagDtEndCopTitNull) {
        writeString(Pos.WPAG_DT_END_COP_TIT_NULL, wpagDtEndCopTitNull, Len.WPAG_DT_END_COP_TIT_NULL);
    }

    /**Original name: WPAG-DT-END-COP-TIT-NULL<br>*/
    public String getWpagDtEndCopTitNull() {
        return readString(Pos.WPAG_DT_END_COP_TIT_NULL, Len.WPAG_DT_END_COP_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_DT_END_COP_TIT = 1;
        public static final int WPAG_DT_END_COP_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_DT_END_COP_TIT = 5;
        public static final int WPAG_DT_END_COP_TIT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_DT_END_COP_TIT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
