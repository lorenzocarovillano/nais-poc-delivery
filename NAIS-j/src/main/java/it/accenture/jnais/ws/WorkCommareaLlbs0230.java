package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccc00011;
import it.accenture.jnais.copy.Lccvp631;
import it.accenture.jnais.copy.Lccvp671;
import it.accenture.jnais.copy.Lccvpco1;
import it.accenture.jnais.copy.Lccvpol1;
import it.accenture.jnais.copy.Lstc0072;
import it.accenture.jnais.copy.WpcoDati;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.occurs.S089TabLiq;
import it.accenture.jnais.ws.occurs.WadeTabAdes;
import it.accenture.jnais.ws.occurs.WdfaTabDfa;
import it.accenture.jnais.ws.occurs.WdtcTabDtc;
import it.accenture.jnais.ws.occurs.WmovTabMovi;
import it.accenture.jnais.ws.occurs.WpogTabParamOgg;
import it.accenture.jnais.ws.occurs.WrreTabRappRete;
import it.accenture.jnais.ws.occurs.WrstTabRst;
import it.accenture.jnais.ws.occurs.WtitTabTitCont;

/**Original name: WORK-COMMAREA<br>
 * Variable: WORK-COMMAREA from program LLBS0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkCommareaLlbs0230 {

    //==== PROPERTIES ====
    public static final int WDFA_TAB_DFA_MAXOCCURS = 10;
    public static final int WADE_TAB_ADE_MAXOCCURS = 1;
    public static final int WTIT_TAB_TIT_CONT_MAXOCCURS = 200;
    public static final int WDTC_TAB_DTC_MAXOCCURS = 100;
    public static final int WLQU_TAB_LIQ_MAXOCCURS = 30;
    public static final int WRST_TAB_RST_MAXOCCURS = 10;
    public static final int WMOV_TAB_MOV_MAXOCCURS = 10;
    public static final int WPOG_TAB_PARAM_OGG_MAXOCCURS = 100;
    public static final int WRRE_TAB_RRE_MAXOCCURS = 10;
    //Original name: WPOL-ELE-POL-MAX
    private short wpolElePolMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1 lccvpol1 = new Lccvpol1();
    //Original name: WADE-ELE-ADE-MAX
    private short wadeEleAdeMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WADE-TAB-ADE
    private WadeTabAdes[] wadeTabAde = new WadeTabAdes[WADE_TAB_ADE_MAXOCCURS];
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia = new WgrzAreaGaranziaLccs0005();
    //Original name: WTGA-AREA-TGA
    private WtgaAreaTga wtgaAreaTga = new WtgaAreaTga();
    //Original name: ATGA-AREA-TGA
    private AtgaAreaTga atgaAreaTga = new AtgaAreaTga();
    //Original name: WSTB-AREA-STB
    private WstbAreaStb wstbAreaStb = new WstbAreaStb();
    //Original name: WTIT-ELE-TIT-MAX
    private short wtitEleTitMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTIT-TAB-TIT-CONT
    private WtitTabTitCont[] wtitTabTitCont = new WtitTabTitCont[WTIT_TAB_TIT_CONT_MAXOCCURS];
    //Original name: WDTC-ELE-DTC-MAX
    private short wdtcEleDtcMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WDTC-TAB-DTC
    private WdtcTabDtc[] wdtcTabDtc = new WdtcTabDtc[WDTC_TAB_DTC_MAXOCCURS];
    //Original name: WPMO-AREA-PMO
    private WpmoAreaParamMov wpmoAreaPmo = new WpmoAreaParamMov();
    //Original name: WLQU-ELE-LQU-MAX
    private short wlquEleLquMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WLQU-TAB-LIQ
    private S089TabLiq[] wlquTabLiq = new S089TabLiq[WLQU_TAB_LIQ_MAXOCCURS];
    //Original name: WRST-ELE-RST-MAX
    private short wrstEleRstMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRST-TAB-RST
    private WrstTabRst[] wrstTabRst = new WrstTabRst[WRST_TAB_RST_MAXOCCURS];
    //Original name: WMOV-ELE-MOV-MAX
    private short wmovEleMovMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WMOV-TAB-MOV
    private WmovTabMovi[] wmovTabMov = new WmovTabMovi[WMOV_TAB_MOV_MAXOCCURS];
    //Original name: WDFA-ELE-DFA-MAX
    private short wdfaEleDfaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WDFA-TAB-DFA
    private WdfaTabDfa[] wdfaTabDfa = new WdfaTabDfa[WDFA_TAB_DFA_MAXOCCURS];
    //Original name: WPOG-ELE-POG-MAX
    private short wpogElePogMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPOG-TAB-PARAM-OGG
    private WpogTabParamOgg[] wpogTabParamOgg = new WpogTabParamOgg[WPOG_TAB_PARAM_OGG_MAXOCCURS];
    //Original name: WRRE-ELE-RRE-MAX
    private short wrreEleRreMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRRE-TAB-RRE
    private WrreTabRappRete[] wrreTabRre = new WrreTabRappRete[WRRE_TAB_RRE_MAXOCCURS];
    //Original name: WL19-AREA-FONDI
    private Wl19AreaFondiLlbs0230 wl19AreaFondi = new Wl19AreaFondiLlbs0230();
    //Original name: LCCVP631
    private Lccvp631 lccvp631 = new Lccvp631();
    //Original name: LCCVP671
    private Lccvp671 lccvp671 = new Lccvp671();
    //Original name: LSTC0072
    private Lstc0072 lstc0072 = new Lstc0072();
    //Original name: LCCC0001
    private Lccc00011 lccc0001 = new Lccc00011();
    //Original name: WPCO-ELE-PARA-COM-MAX
    private int wpcoEleParaComMax = DefaultValues.BIN_INT_VAL;
    //Original name: LCCVPCO1
    private Lccvpco1 lccvpco1 = new Lccvpco1();

    //==== CONSTRUCTORS ====
    public WorkCommareaLlbs0230() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wadeTabAdeIdx = 1; wadeTabAdeIdx <= WADE_TAB_ADE_MAXOCCURS; wadeTabAdeIdx++) {
            wadeTabAde[wadeTabAdeIdx - 1] = new WadeTabAdes();
        }
        for (int wtitTabTitContIdx = 1; wtitTabTitContIdx <= WTIT_TAB_TIT_CONT_MAXOCCURS; wtitTabTitContIdx++) {
            wtitTabTitCont[wtitTabTitContIdx - 1] = new WtitTabTitCont();
        }
        for (int wdtcTabDtcIdx = 1; wdtcTabDtcIdx <= WDTC_TAB_DTC_MAXOCCURS; wdtcTabDtcIdx++) {
            wdtcTabDtc[wdtcTabDtcIdx - 1] = new WdtcTabDtc();
        }
        for (int wlquTabLiqIdx = 1; wlquTabLiqIdx <= WLQU_TAB_LIQ_MAXOCCURS; wlquTabLiqIdx++) {
            wlquTabLiq[wlquTabLiqIdx - 1] = new S089TabLiq();
        }
        for (int wrstTabRstIdx = 1; wrstTabRstIdx <= WRST_TAB_RST_MAXOCCURS; wrstTabRstIdx++) {
            wrstTabRst[wrstTabRstIdx - 1] = new WrstTabRst();
        }
        for (int wmovTabMovIdx = 1; wmovTabMovIdx <= WMOV_TAB_MOV_MAXOCCURS; wmovTabMovIdx++) {
            wmovTabMov[wmovTabMovIdx - 1] = new WmovTabMovi();
        }
        for (int wdfaTabDfaIdx = 1; wdfaTabDfaIdx <= WDFA_TAB_DFA_MAXOCCURS; wdfaTabDfaIdx++) {
            wdfaTabDfa[wdfaTabDfaIdx - 1] = new WdfaTabDfa();
        }
        for (int wpogTabParamOggIdx = 1; wpogTabParamOggIdx <= WPOG_TAB_PARAM_OGG_MAXOCCURS; wpogTabParamOggIdx++) {
            wpogTabParamOgg[wpogTabParamOggIdx - 1] = new WpogTabParamOgg();
        }
        for (int wrreTabRreIdx = 1; wrreTabRreIdx <= WRRE_TAB_RRE_MAXOCCURS; wrreTabRreIdx++) {
            wrreTabRre[wrreTabRreIdx - 1] = new WrreTabRappRete();
        }
    }

    public String getWpolAreaPolizzaFormatted() {
        return MarshalByteExt.bufferToStr(getWpolAreaPolizzaBytes());
    }

    /**Original name: WPOL-AREA-POLIZZA<br>
	 * <pre>--  AREA POLIZZA</pre>*/
    public byte[] getWpolAreaPolizzaBytes() {
        byte[] buffer = new byte[Len.WPOL_AREA_POLIZZA];
        return getWpolAreaPolizzaBytes(buffer, 1);
    }

    public byte[] getWpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpolElePolMax);
        position += Types.SHORT_SIZE;
        getWpolTabPolBytes(buffer, position);
        return buffer;
    }

    public void setWpolElePolMax(short wpolElePolMax) {
        this.wpolElePolMax = wpolElePolMax;
    }

    public short getWpolElePolMax() {
        return this.wpolElePolMax;
    }

    public byte[] getWpolTabPolBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvpol1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvpol1.getIdPtf(), Lccvpol1.Len.Int.ID_PTF, 0);
        position += Lccvpol1.Len.ID_PTF;
        lccvpol1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public String getWadeAreaAdesioneFormatted() {
        return MarshalByteExt.bufferToStr(getWadeAreaAdesioneBytes());
    }

    /**Original name: WADE-AREA-ADESIONE<br>
	 * <pre>-->  AREA ADESIONE</pre>*/
    public byte[] getWadeAreaAdesioneBytes() {
        byte[] buffer = new byte[Len.WADE_AREA_ADESIONE];
        return getWadeAreaAdesioneBytes(buffer, 1);
    }

    public byte[] getWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wadeEleAdeMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WADE_TAB_ADE_MAXOCCURS; idx++) {
            wadeTabAde[idx - 1].getWadeTabAdesBytes(buffer, position);
            position += WadeTabAdes.Len.WADE_TAB_ADES;
        }
        return buffer;
    }

    public void setWadeEleAdeMax(short wadeEleAdeMax) {
        this.wadeEleAdeMax = wadeEleAdeMax;
    }

    public short getWadeEleAdeMax() {
        return this.wadeEleAdeMax;
    }

    public void setWtitEleTitMax(short wtitEleTitMax) {
        this.wtitEleTitMax = wtitEleTitMax;
    }

    public short getWtitEleTitMax() {
        return this.wtitEleTitMax;
    }

    public void setWdtcEleDtcMax(short wdtcEleDtcMax) {
        this.wdtcEleDtcMax = wdtcEleDtcMax;
    }

    public short getWdtcEleDtcMax() {
        return this.wdtcEleDtcMax;
    }

    public void setWlquEleLquMax(short wlquEleLquMax) {
        this.wlquEleLquMax = wlquEleLquMax;
    }

    public short getWlquEleLquMax() {
        return this.wlquEleLquMax;
    }

    public void setWrstEleRstMax(short wrstEleRstMax) {
        this.wrstEleRstMax = wrstEleRstMax;
    }

    public short getWrstEleRstMax() {
        return this.wrstEleRstMax;
    }

    public void setWmovEleMovMax(short wmovEleMovMax) {
        this.wmovEleMovMax = wmovEleMovMax;
    }

    public short getWmovEleMovMax() {
        return this.wmovEleMovMax;
    }

    public void setWdfaEleDfaMax(short wdfaEleDfaMax) {
        this.wdfaEleDfaMax = wdfaEleDfaMax;
    }

    public short getWdfaEleDfaMax() {
        return this.wdfaEleDfaMax;
    }

    public void setWpogElePogMax(short wpogElePogMax) {
        this.wpogElePogMax = wpogElePogMax;
    }

    public short getWpogElePogMax() {
        return this.wpogElePogMax;
    }

    public void setWrreEleRreMax(short wrreEleRreMax) {
        this.wrreEleRreMax = wrreEleRreMax;
    }

    public short getWrreEleRreMax() {
        return this.wrreEleRreMax;
    }

    public String getWpcoAreaParaComFormatted() {
        return MarshalByteExt.bufferToStr(getWpcoAreaParaComBytes());
    }

    /**Original name: WPCO-AREA-PARA-COM<br>
	 * <pre>--> PARAMETRO_COMPAGNIA</pre>*/
    public byte[] getWpcoAreaParaComBytes() {
        byte[] buffer = new byte[Len.WPCO_AREA_PARA_COM];
        return getWpcoAreaParaComBytes(buffer, 1);
    }

    public byte[] getWpcoAreaParaComBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryUnsignedShort(buffer, position, wpcoEleParaComMax);
        position += Types.SHORT_SIZE;
        getWpcoTabParaComBytes(buffer, position);
        return buffer;
    }

    public void setWpcoEleParaComMax(int wpcoEleParaComMax) {
        this.wpcoEleParaComMax = wpcoEleParaComMax;
    }

    public int getWpcoEleParaComMax() {
        return this.wpcoEleParaComMax;
    }

    public byte[] getWpcoTabParaComBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvpco1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        lccvpco1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public AtgaAreaTga getAtgaAreaTga() {
        return atgaAreaTga;
    }

    public Lccc00011 getLccc0001() {
        return lccc0001;
    }

    public Lccvp631 getLccvp631() {
        return lccvp631;
    }

    public Lccvp671 getLccvp671() {
        return lccvp671;
    }

    public Lccvpco1 getLccvpco1() {
        return lccvpco1;
    }

    public Lccvpol1 getLccvpol1() {
        return lccvpol1;
    }

    public Lstc0072 getLstc0072() {
        return lstc0072;
    }

    public WadeTabAdes getWadeTabAde(int idx) {
        return wadeTabAde[idx - 1];
    }

    public WdfaTabDfa getWdfaTabDfa(int idx) {
        return wdfaTabDfa[idx - 1];
    }

    public WdtcTabDtc getWdtcTabDtc(int idx) {
        return wdtcTabDtc[idx - 1];
    }

    public WgrzAreaGaranziaLccs0005 getWgrzAreaGaranzia() {
        return wgrzAreaGaranzia;
    }

    public Wl19AreaFondiLlbs0230 getWl19AreaFondi() {
        return wl19AreaFondi;
    }

    public S089TabLiq getWlquTabLiq(int idx) {
        return wlquTabLiq[idx - 1];
    }

    public WmovTabMovi getWmovTabMov(int idx) {
        return wmovTabMov[idx - 1];
    }

    public WpmoAreaParamMov getWpmoAreaPmo() {
        return wpmoAreaPmo;
    }

    public WpogTabParamOgg getWpogTabParamOgg(int idx) {
        return wpogTabParamOgg[idx - 1];
    }

    public WrreTabRappRete getWrreTabRre(int idx) {
        return wrreTabRre[idx - 1];
    }

    public WrstTabRst getWrstTabRst(int idx) {
        return wrstTabRst[idx - 1];
    }

    public WstbAreaStb getWstbAreaStb() {
        return wstbAreaStb;
    }

    public WtgaAreaTga getWtgaAreaTga() {
        return wtgaAreaTga;
    }

    public WtitTabTitCont getWtitTabTitCont(int idx) {
        return wtitTabTitCont[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_ELE_POL_MAX = 2;
        public static final int WPOL_TAB_POL = WpolStatus.Len.STATUS + Lccvpol1.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int WPOL_AREA_POLIZZA = WPOL_ELE_POL_MAX + WPOL_TAB_POL;
        public static final int WADE_ELE_ADE_MAX = 2;
        public static final int WADE_AREA_ADESIONE = WADE_ELE_ADE_MAX + WorkCommareaLlbs0230.WADE_TAB_ADE_MAXOCCURS * WadeTabAdes.Len.WADE_TAB_ADES;
        public static final int WPCO_ELE_PARA_COM_MAX = 2;
        public static final int WPCO_TAB_PARA_COM = WpolStatus.Len.STATUS + WpcoDati.Len.DATI;
        public static final int WPCO_AREA_PARA_COM = WPCO_ELE_PARA_COM_MAX + WPCO_TAB_PARA_COM;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
