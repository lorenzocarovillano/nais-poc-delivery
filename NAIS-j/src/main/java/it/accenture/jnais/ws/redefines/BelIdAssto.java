package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-ID-ASSTO<br>
 * Variable: BEL-ID-ASSTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelIdAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelIdAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_ID_ASSTO;
    }

    public void setBelIdAssto(int belIdAssto) {
        writeIntAsPacked(Pos.BEL_ID_ASSTO, belIdAssto, Len.Int.BEL_ID_ASSTO);
    }

    public void setBelIdAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_ID_ASSTO, Pos.BEL_ID_ASSTO);
    }

    /**Original name: BEL-ID-ASSTO<br>*/
    public int getBelIdAssto() {
        return readPackedAsInt(Pos.BEL_ID_ASSTO, Len.Int.BEL_ID_ASSTO);
    }

    public byte[] getBelIdAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_ID_ASSTO, Pos.BEL_ID_ASSTO);
        return buffer;
    }

    public void setBelIdAsstoNull(String belIdAsstoNull) {
        writeString(Pos.BEL_ID_ASSTO_NULL, belIdAsstoNull, Len.BEL_ID_ASSTO_NULL);
    }

    /**Original name: BEL-ID-ASSTO-NULL<br>*/
    public String getBelIdAsstoNull() {
        return readString(Pos.BEL_ID_ASSTO_NULL, Len.BEL_ID_ASSTO_NULL);
    }

    public String getBelIdAsstoNullFormatted() {
        return Functions.padBlanks(getBelIdAsstoNull(), Len.BEL_ID_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_ID_ASSTO = 1;
        public static final int BEL_ID_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_ID_ASSTO = 5;
        public static final int BEL_ID_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_ID_ASSTO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
