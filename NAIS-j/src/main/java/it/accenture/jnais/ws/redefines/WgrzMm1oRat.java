package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-MM-1O-RAT<br>
 * Variable: WGRZ-MM-1O-RAT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzMm1oRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzMm1oRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_MM1O_RAT;
    }

    public void setWgrzMm1oRat(short wgrzMm1oRat) {
        writeShortAsPacked(Pos.WGRZ_MM1O_RAT, wgrzMm1oRat, Len.Int.WGRZ_MM1O_RAT);
    }

    public void setWgrzMm1oRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_MM1O_RAT, Pos.WGRZ_MM1O_RAT);
    }

    /**Original name: WGRZ-MM-1O-RAT<br>*/
    public short getWgrzMm1oRat() {
        return readPackedAsShort(Pos.WGRZ_MM1O_RAT, Len.Int.WGRZ_MM1O_RAT);
    }

    public byte[] getWgrzMm1oRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_MM1O_RAT, Pos.WGRZ_MM1O_RAT);
        return buffer;
    }

    public void initWgrzMm1oRatSpaces() {
        fill(Pos.WGRZ_MM1O_RAT, Len.WGRZ_MM1O_RAT, Types.SPACE_CHAR);
    }

    public void setWgrzMm1oRatNull(String wgrzMm1oRatNull) {
        writeString(Pos.WGRZ_MM1O_RAT_NULL, wgrzMm1oRatNull, Len.WGRZ_MM1O_RAT_NULL);
    }

    /**Original name: WGRZ-MM-1O-RAT-NULL<br>*/
    public String getWgrzMm1oRatNull() {
        return readString(Pos.WGRZ_MM1O_RAT_NULL, Len.WGRZ_MM1O_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_MM1O_RAT = 1;
        public static final int WGRZ_MM1O_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_MM1O_RAT = 2;
        public static final int WGRZ_MM1O_RAT_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_MM1O_RAT = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
