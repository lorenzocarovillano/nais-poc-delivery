package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WALL-DT-END-VLDT<br>
 * Variable: WALL-DT-END-VLDT from program LCCS1900<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WallDtEndVldt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WallDtEndVldt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WALL_DT_END_VLDT;
    }

    public void setWallDtEndVldt(int wallDtEndVldt) {
        writeIntAsPacked(Pos.WALL_DT_END_VLDT, wallDtEndVldt, Len.Int.WALL_DT_END_VLDT);
    }

    public void setWallDtEndVldtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WALL_DT_END_VLDT, Pos.WALL_DT_END_VLDT);
    }

    /**Original name: WALL-DT-END-VLDT<br>*/
    public int getWallDtEndVldt() {
        return readPackedAsInt(Pos.WALL_DT_END_VLDT, Len.Int.WALL_DT_END_VLDT);
    }

    public byte[] getWallDtEndVldtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WALL_DT_END_VLDT, Pos.WALL_DT_END_VLDT);
        return buffer;
    }

    public void initWallDtEndVldtSpaces() {
        fill(Pos.WALL_DT_END_VLDT, Len.WALL_DT_END_VLDT, Types.SPACE_CHAR);
    }

    public void setWallDtEndVldtNull(String wallDtEndVldtNull) {
        writeString(Pos.WALL_DT_END_VLDT_NULL, wallDtEndVldtNull, Len.WALL_DT_END_VLDT_NULL);
    }

    /**Original name: WALL-DT-END-VLDT-NULL<br>*/
    public String getWallDtEndVldtNull() {
        return readString(Pos.WALL_DT_END_VLDT_NULL, Len.WALL_DT_END_VLDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WALL_DT_END_VLDT = 1;
        public static final int WALL_DT_END_VLDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WALL_DT_END_VLDT = 5;
        public static final int WALL_DT_END_VLDT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WALL_DT_END_VLDT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
