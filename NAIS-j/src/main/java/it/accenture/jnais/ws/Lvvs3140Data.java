package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvp611;
import it.accenture.jnais.copy.Lccvp6111;
import it.accenture.jnais.copy.Wp61Dati;
import it.accenture.jnais.ws.enums.FlagComunTrov;
import it.accenture.jnais.ws.enums.FlagCurMov;
import it.accenture.jnais.ws.enums.FlagFineLetturaP61;
import it.accenture.jnais.ws.enums.FlTipoPolizza;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs2800;
import it.accenture.jnais.ws.enums.WsMoviOriColl;
import it.accenture.jnais.ws.enums.WsMoviOriCollCom;
import it.accenture.jnais.ws.enums.WsMoviOrig;
import it.accenture.jnais.ws.redefines.WkDataFineLvvs2750;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3140<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3140Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS3140";
    //Original name: IDBSP610
    private String idbsp610 = "IDBSP610";
    //Original name: LDBS6040
    private String ldbs6040 = "LDBS6040";
    //Original name: LDBSH650
    private String ldbsh650 = "LDBSH650";
    //Original name: WK-PGM-CALLED
    private String wkPgmCalled = "";
    /**Original name: WK-DATA-FINE<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private WkDataFineLvvs2750 wkDataFine = new WkDataFineLvvs2750();
    //Original name: WK-TOTALE
    private AfDecimal wkTotale = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);
    //Original name: MOVI
    private MoviLdbs1530 movi = new MoviLdbs1530();
    //Original name: D-CRIST
    private DCristIdbsp610 dCrist = new DCristIdbsp610();
    //Original name: POLI
    private PoliIdbspol0 poli = new PoliIdbspol0();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>---------------------------------------------------------------*
	 *   LISTA TABELLE E TIPOLOGICHE
	 * ---------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs2800 wsMovimento = new WsMovimentoLvvs2800();
    //Original name: DP61-ELE-D-CRIST-MAX
    private short dp61EleDCristMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVP611
    private Lccvp6111 lccvp6111 = new Lccvp6111();
    //Original name: WP61-ELE-D-CRIST-MAX
    private short wp61EleDCristMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVP611
    private Lccvp611 lccvp6112 = new Lccvp611();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-P61
    private String ixTabP61 = "000000000";
    /**Original name: FLAG-CUR-MOV<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG
	 * ----------------------------------------------------------------*</pre>*/
    private FlagCurMov flagCurMov = new FlagCurMov();
    /**Original name: FLAG-FINE-LETTURA-P61<br>
	 * <pre>01 FLAG-ULTIMA-LETTURA               PIC X(01).
	 *    88 SI-ULTIMA-LETTURA              VALUE 'S'.
	 *    88 NO-ULTIMA-LETTURA              VALUE 'N'.</pre>*/
    private FlagFineLetturaP61 flagFineLetturaP61 = new FlagFineLetturaP61();
    //Original name: FLAG-COMUN-TROV
    private FlagComunTrov flagComunTrov = new FlagComunTrov();
    //Original name: FL-TIPO-POLIZZA
    private FlTipoPolizza flTipoPolizza = new FlTipoPolizza();
    //Original name: WS-MOVI-ORIG
    private WsMoviOrig wsMoviOrig = new WsMoviOrig();
    //Original name: WS-MOVI-ORI-COLL
    private WsMoviOriColl wsMoviOriColl = new WsMoviOriColl();
    //Original name: WS-MOVI-ORI-COLL-COM
    private WsMoviOriCollCom wsMoviOriCollCom = new WsMoviOriCollCom();
    //Original name: WK-VAR-MOVI-COMUN
    private WkVarMoviComunLvvs3140 wkVarMoviComun = new WkVarMoviComunLvvs3140();
    //Original name: INT-REGISTER1
    private int intRegister1 = DefaultValues.INT_VAL;

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getIdbsp610() {
        return this.idbsp610;
    }

    public String getLdbs6040() {
        return this.ldbs6040;
    }

    public String getLdbsh650() {
        return this.ldbsh650;
    }

    public void setWkPgmCalled(String wkPgmCalled) {
        this.wkPgmCalled = Functions.subString(wkPgmCalled, Len.WK_PGM_CALLED);
    }

    public String getWkPgmCalled() {
        return this.wkPgmCalled;
    }

    public void setWkTotale(AfDecimal wkTotale) {
        this.wkTotale.assign(wkTotale);
    }

    public AfDecimal getWkTotale() {
        return this.wkTotale.copy();
    }

    public void setDp61AreaDCristFormatted(String data) {
        byte[] buffer = new byte[Len.DP61_AREA_D_CRIST];
        MarshalByte.writeString(buffer, 1, data, Len.DP61_AREA_D_CRIST);
        setDp61AreaDCristBytes(buffer, 1);
    }

    public void setDp61AreaDCristBytes(byte[] buffer, int offset) {
        int position = offset;
        dp61EleDCristMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDp61TabDcristBytes(buffer, position);
    }

    public void setDp61EleDCristMax(short dp61EleDCristMax) {
        this.dp61EleDCristMax = dp61EleDCristMax;
    }

    public short getDp61EleDCristMax() {
        return this.dp61EleDCristMax;
    }

    public void setDp61TabDcristBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp6111.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp6111.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp6111.Len.Int.ID_PTF, 0));
        position += Lccvp6111.Len.ID_PTF;
        lccvp6111.getDati().setDatiBytes(buffer, position);
    }

    public void setWp61EleDCristMax(short wp61EleDCristMax) {
        this.wp61EleDCristMax = wp61EleDCristMax;
    }

    public short getWp61EleDCristMax() {
        return this.wp61EleDCristMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabP61(int ixTabP61) {
        this.ixTabP61 = NumericDisplay.asString(ixTabP61, Len.IX_TAB_P61);
    }

    public void setIxTabP61Formatted(String ixTabP61) {
        this.ixTabP61 = Trunc.toUnsignedNumeric(ixTabP61, Len.IX_TAB_P61);
    }

    public int getIxTabP61() {
        return NumericDisplay.asInt(this.ixTabP61);
    }

    public void setIntRegister1(int intRegister1) {
        this.intRegister1 = intRegister1;
    }

    public int getIntRegister1() {
        return this.intRegister1;
    }

    public FlTipoPolizza getFlTipoPolizza() {
        return flTipoPolizza;
    }

    public FlagComunTrov getFlagComunTrov() {
        return flagComunTrov;
    }

    public FlagCurMov getFlagCurMov() {
        return flagCurMov;
    }

    public FlagFineLetturaP61 getFlagFineLetturaP61() {
        return flagFineLetturaP61;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvp6111 getLccvp6111() {
        return lccvp6111;
    }

    public Lccvp611 getLccvp6112() {
        return lccvp6112;
    }

    public MoviLdbs1530 getMovi() {
        return movi;
    }

    public PoliIdbspol0 getPoli() {
        return poli;
    }

    public WkDataFineLvvs2750 getWkDataFine() {
        return wkDataFine;
    }

    public WkVarMoviComunLvvs3140 getWkVarMoviComun() {
        return wkVarMoviComun;
    }

    public WsMoviOriColl getWsMoviOriColl() {
        return wsMoviOriColl;
    }

    public WsMoviOriCollCom getWsMoviOriCollCom() {
        return wsMoviOriCollCom;
    }

    public WsMoviOrig getWsMoviOrig() {
        return wsMoviOrig;
    }

    public WsMovimentoLvvs2800 getWsMovimento() {
        return wsMovimento;
    }

    public DCristIdbsp610 getdCrist() {
        return dCrist;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IX_TAB_P61 = 9;
        public static final int WK_PGM_CALLED = 8;
        public static final int DP61_ELE_D_CRIST_MAX = 2;
        public static final int DP61_TAB_DCRIST = WpolStatus.Len.STATUS + Lccvp6111.Len.ID_PTF + Wp61Dati.Len.DATI;
        public static final int DP61_AREA_D_CRIST = DP61_ELE_D_CRIST_MAX + DP61_TAB_DCRIST;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
