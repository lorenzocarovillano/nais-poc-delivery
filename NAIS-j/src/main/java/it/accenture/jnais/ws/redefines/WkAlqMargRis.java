package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-ALQ-MARG-RIS<br>
 * Variable: WK-ALQ-MARG-RIS from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkAlqMargRis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkAlqMargRis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_ALQ_MARG_RIS;
    }

    public void setWkAlqMargRis(AfDecimal wkAlqMargRis) {
        writeDecimalAsPacked(Pos.WK_ALQ_MARG_RIS, wkAlqMargRis.copy());
    }

    /**Original name: WK-ALQ-MARG-RIS<br>*/
    public AfDecimal getWkAlqMargRis() {
        return readPackedAsDecimal(Pos.WK_ALQ_MARG_RIS, Len.Int.WK_ALQ_MARG_RIS, Len.Fract.WK_ALQ_MARG_RIS);
    }

    public void setWkAlqMargRisNull(String wkAlqMargRisNull) {
        writeString(Pos.WK_ALQ_MARG_RIS_NULL, wkAlqMargRisNull, Len.WK_ALQ_MARG_RIS_NULL);
    }

    /**Original name: WK-ALQ-MARG-RIS-NULL<br>*/
    public String getWkAlqMargRisNull() {
        return readString(Pos.WK_ALQ_MARG_RIS_NULL, Len.WK_ALQ_MARG_RIS_NULL);
    }

    public String getWkAlqMargRisNullFormatted() {
        return Functions.padBlanks(getWkAlqMargRisNull(), Len.WK_ALQ_MARG_RIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_ALQ_MARG_RIS = 1;
        public static final int WK_ALQ_MARG_RIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_ALQ_MARG_RIS = 4;
        public static final int WK_ALQ_MARG_RIS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WK_ALQ_MARG_RIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_ALQ_MARG_RIS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
