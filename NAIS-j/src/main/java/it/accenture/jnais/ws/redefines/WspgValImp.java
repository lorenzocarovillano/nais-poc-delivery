package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WSPG-VAL-IMP<br>
 * Variable: WSPG-VAL-IMP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WspgValImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WspgValImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSPG_VAL_IMP;
    }

    public void setWspgValImp(AfDecimal wspgValImp) {
        writeDecimalAsPacked(Pos.WSPG_VAL_IMP, wspgValImp.copy());
    }

    public void setWspgValImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSPG_VAL_IMP, Pos.WSPG_VAL_IMP);
    }

    /**Original name: WSPG-VAL-IMP<br>*/
    public AfDecimal getWspgValImp() {
        return readPackedAsDecimal(Pos.WSPG_VAL_IMP, Len.Int.WSPG_VAL_IMP, Len.Fract.WSPG_VAL_IMP);
    }

    public byte[] getWspgValImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSPG_VAL_IMP, Pos.WSPG_VAL_IMP);
        return buffer;
    }

    public void initWspgValImpSpaces() {
        fill(Pos.WSPG_VAL_IMP, Len.WSPG_VAL_IMP, Types.SPACE_CHAR);
    }

    public void setWspgValImpNull(String wspgValImpNull) {
        writeString(Pos.WSPG_VAL_IMP_NULL, wspgValImpNull, Len.WSPG_VAL_IMP_NULL);
    }

    /**Original name: WSPG-VAL-IMP-NULL<br>*/
    public String getWspgValImpNull() {
        return readString(Pos.WSPG_VAL_IMP_NULL, Len.WSPG_VAL_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSPG_VAL_IMP = 1;
        public static final int WSPG_VAL_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WSPG_VAL_IMP = 8;
        public static final int WSPG_VAL_IMP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WSPG_VAL_IMP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WSPG_VAL_IMP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
