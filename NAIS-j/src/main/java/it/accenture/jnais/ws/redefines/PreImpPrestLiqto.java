package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-IMP-PREST-LIQTO<br>
 * Variable: PRE-IMP-PREST-LIQTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreImpPrestLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreImpPrestLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_IMP_PREST_LIQTO;
    }

    public void setPreImpPrestLiqto(AfDecimal preImpPrestLiqto) {
        writeDecimalAsPacked(Pos.PRE_IMP_PREST_LIQTO, preImpPrestLiqto.copy());
    }

    public void setPreImpPrestLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_IMP_PREST_LIQTO, Pos.PRE_IMP_PREST_LIQTO);
    }

    /**Original name: PRE-IMP-PREST-LIQTO<br>*/
    public AfDecimal getPreImpPrestLiqto() {
        return readPackedAsDecimal(Pos.PRE_IMP_PREST_LIQTO, Len.Int.PRE_IMP_PREST_LIQTO, Len.Fract.PRE_IMP_PREST_LIQTO);
    }

    public byte[] getPreImpPrestLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_IMP_PREST_LIQTO, Pos.PRE_IMP_PREST_LIQTO);
        return buffer;
    }

    public void setPreImpPrestLiqtoNull(String preImpPrestLiqtoNull) {
        writeString(Pos.PRE_IMP_PREST_LIQTO_NULL, preImpPrestLiqtoNull, Len.PRE_IMP_PREST_LIQTO_NULL);
    }

    /**Original name: PRE-IMP-PREST-LIQTO-NULL<br>*/
    public String getPreImpPrestLiqtoNull() {
        return readString(Pos.PRE_IMP_PREST_LIQTO_NULL, Len.PRE_IMP_PREST_LIQTO_NULL);
    }

    public String getPreImpPrestLiqtoNullFormatted() {
        return Functions.padBlanks(getPreImpPrestLiqtoNull(), Len.PRE_IMP_PREST_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_IMP_PREST_LIQTO = 1;
        public static final int PRE_IMP_PREST_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_IMP_PREST_LIQTO = 8;
        public static final int PRE_IMP_PREST_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_IMP_PREST_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_IMP_PREST_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
