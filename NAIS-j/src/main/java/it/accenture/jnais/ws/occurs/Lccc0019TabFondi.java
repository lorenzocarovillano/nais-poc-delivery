package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0019-TAB-FONDI<br>
 * Variables: LCCC0019-TAB-FONDI from copybook LCCC0019<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Lccc0019TabFondi {

    //==== PROPERTIES ====
    //Original name: LCCC0019-IMP-PLATFOND
    private AfDecimal impPlatfond = new AfDecimal(DefaultValues.DEC_VAL, 18, 3);
    //Original name: LCCC0019-COD-FONDO
    private String codFondo = DefaultValues.stringVal(Len.COD_FONDO);
    //Original name: LCCC0019-PERC-FONDO
    private AfDecimal percFondo = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LCCC0019-PERC-FONDO-OLD
    private AfDecimal percFondoOld = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);

    //==== METHODS ====
    public void setTabFondiBytes(byte[] buffer, int offset) {
        int position = offset;
        impPlatfond.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_PLATFOND, Len.Fract.IMP_PLATFOND));
        position += Len.IMP_PLATFOND;
        codFondo = MarshalByte.readString(buffer, position, Len.COD_FONDO);
        position += Len.COD_FONDO;
        percFondo.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PERC_FONDO, Len.Fract.PERC_FONDO));
        position += Len.PERC_FONDO;
        percFondoOld.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PERC_FONDO_OLD, Len.Fract.PERC_FONDO_OLD));
    }

    public byte[] getTabFondiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimalAsPacked(buffer, position, impPlatfond.copy());
        position += Len.IMP_PLATFOND;
        MarshalByte.writeString(buffer, position, codFondo, Len.COD_FONDO);
        position += Len.COD_FONDO;
        MarshalByte.writeDecimalAsPacked(buffer, position, percFondo.copy());
        position += Len.PERC_FONDO;
        MarshalByte.writeDecimalAsPacked(buffer, position, percFondoOld.copy());
        return buffer;
    }

    public void initTabFondiSpaces() {
        impPlatfond.setNaN();
        codFondo = "";
        percFondo.setNaN();
        percFondoOld.setNaN();
    }

    public void setImpPlatfond(AfDecimal impPlatfond) {
        this.impPlatfond.assign(impPlatfond);
    }

    public AfDecimal getImpPlatfond() {
        return this.impPlatfond.copy();
    }

    public void setCodFondo(String codFondo) {
        this.codFondo = Functions.subString(codFondo, Len.COD_FONDO);
    }

    public String getCodFondo() {
        return this.codFondo;
    }

    public void setPercFondo(AfDecimal percFondo) {
        this.percFondo.assign(percFondo);
    }

    public AfDecimal getPercFondo() {
        return this.percFondo.copy();
    }

    public void setPercFondoOld(AfDecimal percFondoOld) {
        this.percFondoOld.assign(percFondoOld);
    }

    public AfDecimal getPercFondoOld() {
        return this.percFondoOld.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IMP_PLATFOND = 10;
        public static final int COD_FONDO = 12;
        public static final int PERC_FONDO = 4;
        public static final int PERC_FONDO_OLD = 4;
        public static final int TAB_FONDI = IMP_PLATFOND + COD_FONDO + PERC_FONDO + PERC_FONDO_OLD;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IMP_PLATFOND = 15;
            public static final int PERC_FONDO = 3;
            public static final int PERC_FONDO_OLD = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMP_PLATFOND = 3;
            public static final int PERC_FONDO = 3;
            public static final int PERC_FONDO_OLD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
