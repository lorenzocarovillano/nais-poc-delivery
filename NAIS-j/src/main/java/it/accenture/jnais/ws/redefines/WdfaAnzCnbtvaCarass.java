package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-ANZ-CNBTVA-CARASS<br>
 * Variable: WDFA-ANZ-CNBTVA-CARASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaAnzCnbtvaCarass extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaAnzCnbtvaCarass() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_ANZ_CNBTVA_CARASS;
    }

    public void setWdfaAnzCnbtvaCarass(short wdfaAnzCnbtvaCarass) {
        writeShortAsPacked(Pos.WDFA_ANZ_CNBTVA_CARASS, wdfaAnzCnbtvaCarass, Len.Int.WDFA_ANZ_CNBTVA_CARASS);
    }

    public void setWdfaAnzCnbtvaCarassFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_ANZ_CNBTVA_CARASS, Pos.WDFA_ANZ_CNBTVA_CARASS);
    }

    /**Original name: WDFA-ANZ-CNBTVA-CARASS<br>*/
    public short getWdfaAnzCnbtvaCarass() {
        return readPackedAsShort(Pos.WDFA_ANZ_CNBTVA_CARASS, Len.Int.WDFA_ANZ_CNBTVA_CARASS);
    }

    public byte[] getWdfaAnzCnbtvaCarassAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_ANZ_CNBTVA_CARASS, Pos.WDFA_ANZ_CNBTVA_CARASS);
        return buffer;
    }

    public void setWdfaAnzCnbtvaCarassNull(String wdfaAnzCnbtvaCarassNull) {
        writeString(Pos.WDFA_ANZ_CNBTVA_CARASS_NULL, wdfaAnzCnbtvaCarassNull, Len.WDFA_ANZ_CNBTVA_CARASS_NULL);
    }

    /**Original name: WDFA-ANZ-CNBTVA-CARASS-NULL<br>*/
    public String getWdfaAnzCnbtvaCarassNull() {
        return readString(Pos.WDFA_ANZ_CNBTVA_CARASS_NULL, Len.WDFA_ANZ_CNBTVA_CARASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_ANZ_CNBTVA_CARASS = 1;
        public static final int WDFA_ANZ_CNBTVA_CARASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_ANZ_CNBTVA_CARASS = 3;
        public static final int WDFA_ANZ_CNBTVA_CARASS_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_ANZ_CNBTVA_CARASS = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
