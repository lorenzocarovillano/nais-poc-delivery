package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCDG-FND-X-CDG-VV<br>
 * Variables: WCDG-FND-X-CDG-VV from copybook IVVC0224<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WcdgFndXCdgVv {

    //==== PROPERTIES ====
    //Original name: WCDG-COD-FND-VV
    private String codFndVv = DefaultValues.stringVal(Len.COD_FND_VV);
    //Original name: WCDG-CDG-REST-VV
    private AfDecimal cdgRestVv = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);

    //==== METHODS ====
    public void setFndXCdgVvBytes(byte[] buffer, int offset) {
        int position = offset;
        codFndVv = MarshalByte.readString(buffer, position, Len.COD_FND_VV);
        position += Len.COD_FND_VV;
        cdgRestVv.assign(MarshalByte.readDecimal(buffer, position, Len.Int.CDG_REST_VV, Len.Fract.CDG_REST_VV));
    }

    public byte[] getFndXCdgVvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codFndVv, Len.COD_FND_VV);
        position += Len.COD_FND_VV;
        MarshalByte.writeDecimal(buffer, position, cdgRestVv.copy());
        return buffer;
    }

    public void initFndXCdgVvSpaces() {
        codFndVv = "";
        cdgRestVv.setNaN();
    }

    public void setCodFndVv(String codFndVv) {
        this.codFndVv = Functions.subString(codFndVv, Len.COD_FND_VV);
    }

    public String getCodFndVv() {
        return this.codFndVv;
    }

    public void setCdgRestVv(AfDecimal cdgRestVv) {
        this.cdgRestVv.assign(cdgRestVv);
    }

    public AfDecimal getCdgRestVv() {
        return this.cdgRestVv.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_FND_VV = 20;
        public static final int CDG_REST_VV = 18;
        public static final int FND_X_CDG_VV = COD_FND_VV + CDG_REST_VV;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CDG_REST_VV = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int CDG_REST_VV = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
