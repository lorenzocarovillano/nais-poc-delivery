package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-TRATTA-TRANCHE<br>
 * Variable: FLAG-TRATTA-TRANCHE from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTrattaTranche {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagTrattaTranche(char flagTrattaTranche) {
        this.value = flagTrattaTranche;
    }

    public char getFlagTrattaTranche() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
