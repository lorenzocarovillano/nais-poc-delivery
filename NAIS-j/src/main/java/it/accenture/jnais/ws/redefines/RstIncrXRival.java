package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-INCR-X-RIVAL<br>
 * Variable: RST-INCR-X-RIVAL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstIncrXRival extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstIncrXRival() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_INCR_X_RIVAL;
    }

    public void setRstIncrXRival(AfDecimal rstIncrXRival) {
        writeDecimalAsPacked(Pos.RST_INCR_X_RIVAL, rstIncrXRival.copy());
    }

    public void setRstIncrXRivalFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_INCR_X_RIVAL, Pos.RST_INCR_X_RIVAL);
    }

    /**Original name: RST-INCR-X-RIVAL<br>*/
    public AfDecimal getRstIncrXRival() {
        return readPackedAsDecimal(Pos.RST_INCR_X_RIVAL, Len.Int.RST_INCR_X_RIVAL, Len.Fract.RST_INCR_X_RIVAL);
    }

    public byte[] getRstIncrXRivalAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_INCR_X_RIVAL, Pos.RST_INCR_X_RIVAL);
        return buffer;
    }

    public void setRstIncrXRivalNull(String rstIncrXRivalNull) {
        writeString(Pos.RST_INCR_X_RIVAL_NULL, rstIncrXRivalNull, Len.RST_INCR_X_RIVAL_NULL);
    }

    /**Original name: RST-INCR-X-RIVAL-NULL<br>*/
    public String getRstIncrXRivalNull() {
        return readString(Pos.RST_INCR_X_RIVAL_NULL, Len.RST_INCR_X_RIVAL_NULL);
    }

    public String getRstIncrXRivalNullFormatted() {
        return Functions.padBlanks(getRstIncrXRivalNull(), Len.RST_INCR_X_RIVAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_INCR_X_RIVAL = 1;
        public static final int RST_INCR_X_RIVAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_INCR_X_RIVAL = 8;
        public static final int RST_INCR_X_RIVAL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_INCR_X_RIVAL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_INCR_X_RIVAL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
