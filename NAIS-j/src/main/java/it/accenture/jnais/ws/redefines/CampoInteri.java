package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: CAMPO-INTERI<br>
 * Variable: CAMPO-INTERI from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CampoInteri extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELE_STRINGA_INTERI_MAXOCCURS = 18;

    //==== CONSTRUCTORS ====
    public CampoInteri() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.CAMPO_INTERI;
    }

    public void setCampoInteri(String campoInteri) {
        writeString(Pos.CAMPO_INTERI, campoInteri, Len.CAMPO_INTERI);
    }

    /**Original name: CAMPO-INTERI<br>*/
    public String getCampoInteri() {
        return readString(Pos.CAMPO_INTERI, Len.CAMPO_INTERI);
    }

    public String getCampoInteriFormatted() {
        return Functions.padBlanks(getCampoInteri(), Len.CAMPO_INTERI);
    }

    public void setEleStringaInteri(int eleStringaInteriIdx, char eleStringaInteri) {
        int position = Pos.eleStringaInteri(eleStringaInteriIdx - 1);
        writeChar(position, eleStringaInteri);
    }

    /**Original name: ELE-STRINGA-INTERI<br>*/
    public char getEleStringaInteri(int eleStringaInteriIdx) {
        int position = Pos.eleStringaInteri(eleStringaInteriIdx - 1);
        return readChar(position);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int CAMPO_INTERI = 1;
        public static final int ARRAY_STRINGA_INTERI = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int eleStringaInteri(int idx) {
            return ARRAY_STRINGA_INTERI + idx * Len.ELE_STRINGA_INTERI;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_STRINGA_INTERI = 1;
        public static final int CAMPO_INTERI = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
