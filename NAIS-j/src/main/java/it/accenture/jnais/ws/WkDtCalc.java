package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-DT-CALC<br>
 * Variable: WK-DT-CALC from program LCCS0320<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkDtCalc {

    //==== PROPERTIES ====
    //Original name: WK-DT-CALC-AAAA
    private String aaaa = "0000";
    //Original name: WK-DT-CALC-MM
    private String mm = "00";
    //Original name: WK-DT-CALC-GG
    private String gg = "00";

    //==== METHODS ====
    public void setWkDtCalcFormatted(String data) {
        byte[] buffer = new byte[Len.WK_DT_CALC];
        MarshalByte.writeString(buffer, 1, data, Len.WK_DT_CALC);
        setWkDtCalcBytes(buffer, 1);
    }

    public byte[] getWkDtCalcBytes() {
        byte[] buffer = new byte[Len.WK_DT_CALC];
        return getWkDtCalcBytes(buffer, 1);
    }

    public void setWkDtCalcBytes(byte[] buffer, int offset) {
        int position = offset;
        aaaa = MarshalByte.readFixedString(buffer, position, Len.AAAA);
        position += Len.AAAA;
        mm = MarshalByte.readFixedString(buffer, position, Len.MM);
        position += Len.MM;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
    }

    public byte[] getWkDtCalcBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aaaa, Len.AAAA);
        position += Len.AAAA;
        MarshalByte.writeString(buffer, position, mm, Len.MM);
        position += Len.MM;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        return buffer;
    }

    public void setGgFormatted(String gg) {
        this.gg = Trunc.toUnsignedNumeric(gg, Len.GG);
    }

    public short getGg() {
        return NumericDisplay.asShort(this.gg);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WK_DT_CALC = AAAA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
