package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-PRVR-EFFLQ<br>
 * Variable: DFL-IMPST-PRVR-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstPrvrEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstPrvrEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_PRVR_EFFLQ;
    }

    public void setDflImpstPrvrEfflq(AfDecimal dflImpstPrvrEfflq) {
        writeDecimalAsPacked(Pos.DFL_IMPST_PRVR_EFFLQ, dflImpstPrvrEfflq.copy());
    }

    public void setDflImpstPrvrEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_PRVR_EFFLQ, Pos.DFL_IMPST_PRVR_EFFLQ);
    }

    /**Original name: DFL-IMPST-PRVR-EFFLQ<br>*/
    public AfDecimal getDflImpstPrvrEfflq() {
        return readPackedAsDecimal(Pos.DFL_IMPST_PRVR_EFFLQ, Len.Int.DFL_IMPST_PRVR_EFFLQ, Len.Fract.DFL_IMPST_PRVR_EFFLQ);
    }

    public byte[] getDflImpstPrvrEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_PRVR_EFFLQ, Pos.DFL_IMPST_PRVR_EFFLQ);
        return buffer;
    }

    public void setDflImpstPrvrEfflqNull(String dflImpstPrvrEfflqNull) {
        writeString(Pos.DFL_IMPST_PRVR_EFFLQ_NULL, dflImpstPrvrEfflqNull, Len.DFL_IMPST_PRVR_EFFLQ_NULL);
    }

    /**Original name: DFL-IMPST-PRVR-EFFLQ-NULL<br>*/
    public String getDflImpstPrvrEfflqNull() {
        return readString(Pos.DFL_IMPST_PRVR_EFFLQ_NULL, Len.DFL_IMPST_PRVR_EFFLQ_NULL);
    }

    public String getDflImpstPrvrEfflqNullFormatted() {
        return Functions.padBlanks(getDflImpstPrvrEfflqNull(), Len.DFL_IMPST_PRVR_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_PRVR_EFFLQ = 1;
        public static final int DFL_IMPST_PRVR_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_PRVR_EFFLQ = 8;
        public static final int DFL_IMPST_PRVR_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_PRVR_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_PRVR_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
