package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvrst1;
import it.accenture.jnais.copy.WrstDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WRST-TAB-RST<br>
 * Variables: WRST-TAB-RST from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WrstTabRst {

    //==== PROPERTIES ====
    //Original name: LCCVRST1
    private Lccvrst1 lccvrst1 = new Lccvrst1();

    //==== METHODS ====
    public void setWrstTabRstBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvrst1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvrst1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvrst1.Len.Int.ID_PTF, 0));
        position += Lccvrst1.Len.ID_PTF;
        lccvrst1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWrstTabRstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvrst1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvrst1.getIdPtf(), Lccvrst1.Len.Int.ID_PTF, 0);
        position += Lccvrst1.Len.ID_PTF;
        lccvrst1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWrstTabRstSpaces() {
        lccvrst1.initLccvrst1Spaces();
    }

    public Lccvrst1 getLccvrst1() {
        return lccvrst1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_TAB_RST = WpolStatus.Len.STATUS + Lccvrst1.Len.ID_PTF + WrstDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
