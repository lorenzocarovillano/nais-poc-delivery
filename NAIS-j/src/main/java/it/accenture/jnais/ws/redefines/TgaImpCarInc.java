package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-CAR-INC<br>
 * Variable: TGA-IMP-CAR-INC from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpCarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpCarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_CAR_INC;
    }

    public void setTgaImpCarInc(AfDecimal tgaImpCarInc) {
        writeDecimalAsPacked(Pos.TGA_IMP_CAR_INC, tgaImpCarInc.copy());
    }

    public void setTgaImpCarIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_CAR_INC, Pos.TGA_IMP_CAR_INC);
    }

    /**Original name: TGA-IMP-CAR-INC<br>*/
    public AfDecimal getTgaImpCarInc() {
        return readPackedAsDecimal(Pos.TGA_IMP_CAR_INC, Len.Int.TGA_IMP_CAR_INC, Len.Fract.TGA_IMP_CAR_INC);
    }

    public byte[] getTgaImpCarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_CAR_INC, Pos.TGA_IMP_CAR_INC);
        return buffer;
    }

    public void setTgaImpCarIncNull(String tgaImpCarIncNull) {
        writeString(Pos.TGA_IMP_CAR_INC_NULL, tgaImpCarIncNull, Len.TGA_IMP_CAR_INC_NULL);
    }

    /**Original name: TGA-IMP-CAR-INC-NULL<br>*/
    public String getTgaImpCarIncNull() {
        return readString(Pos.TGA_IMP_CAR_INC_NULL, Len.TGA_IMP_CAR_INC_NULL);
    }

    public String getTgaImpCarIncNullFormatted() {
        return Functions.padBlanks(getTgaImpCarIncNull(), Len.TGA_IMP_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_CAR_INC = 1;
        public static final int TGA_IMP_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_CAR_INC = 8;
        public static final int TGA_IMP_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
