package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Ispc0040CodTpPremio;

/**Original name: ISPC0040-TIPO-PREMIO<br>
 * Variables: ISPC0040-TIPO-PREMIO from copybook ISPC0040<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0040TipoPremio {

    //==== PROPERTIES ====
    //Original name: ISPC0040-COD-TP-PREMIO
    private Ispc0040CodTpPremio codTpPremio = new Ispc0040CodTpPremio();
    //Original name: ISPC0040-DESCR-TP-PREMIO
    private String descrTpPremio = DefaultValues.stringVal(Len.DESCR_TP_PREMIO);

    //==== METHODS ====
    public void setTipoPremioBytes(byte[] buffer, int offset) {
        int position = offset;
        codTpPremio.setCodTpPremio(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        descrTpPremio = MarshalByte.readString(buffer, position, Len.DESCR_TP_PREMIO);
    }

    public byte[] getTipoPremioBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, codTpPremio.getCodTpPremio());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, descrTpPremio, Len.DESCR_TP_PREMIO);
        return buffer;
    }

    public void initTipoPremioSpaces() {
        codTpPremio.setCodTpPremio(Types.SPACE_CHAR);
        descrTpPremio = "";
    }

    public void setDescrTpPremio(String descrTpPremio) {
        this.descrTpPremio = Functions.subString(descrTpPremio, Len.DESCR_TP_PREMIO);
    }

    public String getDescrTpPremio() {
        return this.descrTpPremio;
    }

    public Ispc0040CodTpPremio getCodTpPremio() {
        return codTpPremio;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESCR_TP_PREMIO = 30;
        public static final int TIPO_PREMIO = Ispc0040CodTpPremio.Len.COD_TP_PREMIO + DESCR_TP_PREMIO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
