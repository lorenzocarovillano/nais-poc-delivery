package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-RAMO-I<br>
 * Variable: FLAG-RAMO-I from program LVVS2780<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagRamoI {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagRamoI(char flagRamoI) {
        this.value = flagRamoI;
    }

    public char getFlagRamoI() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
