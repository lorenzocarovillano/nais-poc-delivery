package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMPB-REMUN-ASS<br>
 * Variable: L3421-IMPB-REMUN-ASS from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpbRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpbRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMPB_REMUN_ASS;
    }

    public void setL3421ImpbRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMPB_REMUN_ASS, Pos.L3421_IMPB_REMUN_ASS);
    }

    /**Original name: L3421-IMPB-REMUN-ASS<br>*/
    public AfDecimal getL3421ImpbRemunAss() {
        return readPackedAsDecimal(Pos.L3421_IMPB_REMUN_ASS, Len.Int.L3421_IMPB_REMUN_ASS, Len.Fract.L3421_IMPB_REMUN_ASS);
    }

    public byte[] getL3421ImpbRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMPB_REMUN_ASS, Pos.L3421_IMPB_REMUN_ASS);
        return buffer;
    }

    /**Original name: L3421-IMPB-REMUN-ASS-NULL<br>*/
    public String getL3421ImpbRemunAssNull() {
        return readString(Pos.L3421_IMPB_REMUN_ASS_NULL, Len.L3421_IMPB_REMUN_ASS_NULL);
    }

    public String getL3421ImpbRemunAssNullFormatted() {
        return Functions.padBlanks(getL3421ImpbRemunAssNull(), Len.L3421_IMPB_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMPB_REMUN_ASS = 1;
        public static final int L3421_IMPB_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMPB_REMUN_ASS = 8;
        public static final int L3421_IMPB_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMPB_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMPB_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
