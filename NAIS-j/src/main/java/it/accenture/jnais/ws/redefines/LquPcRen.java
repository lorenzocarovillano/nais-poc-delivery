package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-PC-REN<br>
 * Variable: LQU-PC-REN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquPcRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquPcRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_PC_REN;
    }

    public void setLquPcRen(AfDecimal lquPcRen) {
        writeDecimalAsPacked(Pos.LQU_PC_REN, lquPcRen.copy());
    }

    public void setLquPcRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_PC_REN, Pos.LQU_PC_REN);
    }

    /**Original name: LQU-PC-REN<br>*/
    public AfDecimal getLquPcRen() {
        return readPackedAsDecimal(Pos.LQU_PC_REN, Len.Int.LQU_PC_REN, Len.Fract.LQU_PC_REN);
    }

    public byte[] getLquPcRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_PC_REN, Pos.LQU_PC_REN);
        return buffer;
    }

    public void setLquPcRenNull(String lquPcRenNull) {
        writeString(Pos.LQU_PC_REN_NULL, lquPcRenNull, Len.LQU_PC_REN_NULL);
    }

    /**Original name: LQU-PC-REN-NULL<br>*/
    public String getLquPcRenNull() {
        return readString(Pos.LQU_PC_REN_NULL, Len.LQU_PC_REN_NULL);
    }

    public String getLquPcRenNullFormatted() {
        return Functions.padBlanks(getLquPcRenNull(), Len.LQU_PC_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_PC_REN = 1;
        public static final int LQU_PC_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_PC_REN = 4;
        public static final int LQU_PC_REN_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_PC_REN = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_PC_REN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
