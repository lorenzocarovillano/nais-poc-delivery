package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-ID-RAPP-RETE<br>
 * Variable: TIT-ID-RAPP-RETE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitIdRappRete extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitIdRappRete() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_ID_RAPP_RETE;
    }

    public void setTitIdRappRete(int titIdRappRete) {
        writeIntAsPacked(Pos.TIT_ID_RAPP_RETE, titIdRappRete, Len.Int.TIT_ID_RAPP_RETE);
    }

    public void setTitIdRappReteFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_ID_RAPP_RETE, Pos.TIT_ID_RAPP_RETE);
    }

    /**Original name: TIT-ID-RAPP-RETE<br>*/
    public int getTitIdRappRete() {
        return readPackedAsInt(Pos.TIT_ID_RAPP_RETE, Len.Int.TIT_ID_RAPP_RETE);
    }

    public byte[] getTitIdRappReteAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_ID_RAPP_RETE, Pos.TIT_ID_RAPP_RETE);
        return buffer;
    }

    public void setTitIdRappReteNull(String titIdRappReteNull) {
        writeString(Pos.TIT_ID_RAPP_RETE_NULL, titIdRappReteNull, Len.TIT_ID_RAPP_RETE_NULL);
    }

    /**Original name: TIT-ID-RAPP-RETE-NULL<br>*/
    public String getTitIdRappReteNull() {
        return readString(Pos.TIT_ID_RAPP_RETE_NULL, Len.TIT_ID_RAPP_RETE_NULL);
    }

    public String getTitIdRappReteNullFormatted() {
        return Functions.padBlanks(getTitIdRappReteNull(), Len.TIT_ID_RAPP_RETE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_ID_RAPP_RETE = 1;
        public static final int TIT_ID_RAPP_RETE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_ID_RAPP_RETE = 5;
        public static final int TIT_ID_RAPP_RETE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_ID_RAPP_RETE = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
