package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-QTZ-SP-Z-OPZ-DT-CA<br>
 * Variable: WB03-QTZ-SP-Z-OPZ-DT-CA from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03QtzSpZOpzDtCa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03QtzSpZOpzDtCa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_QTZ_SP_Z_OPZ_DT_CA;
    }

    public void setWb03QtzSpZOpzDtCaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_QTZ_SP_Z_OPZ_DT_CA, Pos.WB03_QTZ_SP_Z_OPZ_DT_CA);
    }

    /**Original name: WB03-QTZ-SP-Z-OPZ-DT-CA<br>*/
    public AfDecimal getWb03QtzSpZOpzDtCa() {
        return readPackedAsDecimal(Pos.WB03_QTZ_SP_Z_OPZ_DT_CA, Len.Int.WB03_QTZ_SP_Z_OPZ_DT_CA, Len.Fract.WB03_QTZ_SP_Z_OPZ_DT_CA);
    }

    public byte[] getWb03QtzSpZOpzDtCaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_QTZ_SP_Z_OPZ_DT_CA, Pos.WB03_QTZ_SP_Z_OPZ_DT_CA);
        return buffer;
    }

    public void setWb03QtzSpZOpzDtCaNull(String wb03QtzSpZOpzDtCaNull) {
        writeString(Pos.WB03_QTZ_SP_Z_OPZ_DT_CA_NULL, wb03QtzSpZOpzDtCaNull, Len.WB03_QTZ_SP_Z_OPZ_DT_CA_NULL);
    }

    /**Original name: WB03-QTZ-SP-Z-OPZ-DT-CA-NULL<br>*/
    public String getWb03QtzSpZOpzDtCaNull() {
        return readString(Pos.WB03_QTZ_SP_Z_OPZ_DT_CA_NULL, Len.WB03_QTZ_SP_Z_OPZ_DT_CA_NULL);
    }

    public String getWb03QtzSpZOpzDtCaNullFormatted() {
        return Functions.padBlanks(getWb03QtzSpZOpzDtCaNull(), Len.WB03_QTZ_SP_Z_OPZ_DT_CA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_SP_Z_OPZ_DT_CA = 1;
        public static final int WB03_QTZ_SP_Z_OPZ_DT_CA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_SP_Z_OPZ_DT_CA = 7;
        public static final int WB03_QTZ_SP_Z_OPZ_DT_CA_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_SP_Z_OPZ_DT_CA = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_SP_Z_OPZ_DT_CA = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
