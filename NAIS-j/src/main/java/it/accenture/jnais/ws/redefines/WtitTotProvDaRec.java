package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-PROV-DA-REC<br>
 * Variable: WTIT-TOT-PROV-DA-REC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotProvDaRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotProvDaRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_PROV_DA_REC;
    }

    public void setWtitTotProvDaRec(AfDecimal wtitTotProvDaRec) {
        writeDecimalAsPacked(Pos.WTIT_TOT_PROV_DA_REC, wtitTotProvDaRec.copy());
    }

    public void setWtitTotProvDaRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_PROV_DA_REC, Pos.WTIT_TOT_PROV_DA_REC);
    }

    /**Original name: WTIT-TOT-PROV-DA-REC<br>*/
    public AfDecimal getWtitTotProvDaRec() {
        return readPackedAsDecimal(Pos.WTIT_TOT_PROV_DA_REC, Len.Int.WTIT_TOT_PROV_DA_REC, Len.Fract.WTIT_TOT_PROV_DA_REC);
    }

    public byte[] getWtitTotProvDaRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_PROV_DA_REC, Pos.WTIT_TOT_PROV_DA_REC);
        return buffer;
    }

    public void initWtitTotProvDaRecSpaces() {
        fill(Pos.WTIT_TOT_PROV_DA_REC, Len.WTIT_TOT_PROV_DA_REC, Types.SPACE_CHAR);
    }

    public void setWtitTotProvDaRecNull(String wtitTotProvDaRecNull) {
        writeString(Pos.WTIT_TOT_PROV_DA_REC_NULL, wtitTotProvDaRecNull, Len.WTIT_TOT_PROV_DA_REC_NULL);
    }

    /**Original name: WTIT-TOT-PROV-DA-REC-NULL<br>*/
    public String getWtitTotProvDaRecNull() {
        return readString(Pos.WTIT_TOT_PROV_DA_REC_NULL, Len.WTIT_TOT_PROV_DA_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PROV_DA_REC = 1;
        public static final int WTIT_TOT_PROV_DA_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PROV_DA_REC = 8;
        public static final int WTIT_TOT_PROV_DA_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PROV_DA_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PROV_DA_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
