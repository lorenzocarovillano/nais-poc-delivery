package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-MANFEE-RICOR<br>
 * Variable: L3421-MANFEE-RICOR from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ManfeeRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ManfeeRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_MANFEE_RICOR;
    }

    public void setL3421ManfeeRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_MANFEE_RICOR, Pos.L3421_MANFEE_RICOR);
    }

    /**Original name: L3421-MANFEE-RICOR<br>*/
    public AfDecimal getL3421ManfeeRicor() {
        return readPackedAsDecimal(Pos.L3421_MANFEE_RICOR, Len.Int.L3421_MANFEE_RICOR, Len.Fract.L3421_MANFEE_RICOR);
    }

    public byte[] getL3421ManfeeRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_MANFEE_RICOR, Pos.L3421_MANFEE_RICOR);
        return buffer;
    }

    /**Original name: L3421-MANFEE-RICOR-NULL<br>*/
    public String getL3421ManfeeRicorNull() {
        return readString(Pos.L3421_MANFEE_RICOR_NULL, Len.L3421_MANFEE_RICOR_NULL);
    }

    public String getL3421ManfeeRicorNullFormatted() {
        return Functions.padBlanks(getL3421ManfeeRicorNull(), Len.L3421_MANFEE_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_MANFEE_RICOR = 1;
        public static final int L3421_MANFEE_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_MANFEE_RICOR = 8;
        public static final int L3421_MANFEE_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_MANFEE_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_MANFEE_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
