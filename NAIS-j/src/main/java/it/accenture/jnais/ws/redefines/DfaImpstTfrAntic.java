package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPST-TFR-ANTIC<br>
 * Variable: DFA-IMPST-TFR-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpstTfrAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpstTfrAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPST_TFR_ANTIC;
    }

    public void setDfaImpstTfrAntic(AfDecimal dfaImpstTfrAntic) {
        writeDecimalAsPacked(Pos.DFA_IMPST_TFR_ANTIC, dfaImpstTfrAntic.copy());
    }

    public void setDfaImpstTfrAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPST_TFR_ANTIC, Pos.DFA_IMPST_TFR_ANTIC);
    }

    /**Original name: DFA-IMPST-TFR-ANTIC<br>*/
    public AfDecimal getDfaImpstTfrAntic() {
        return readPackedAsDecimal(Pos.DFA_IMPST_TFR_ANTIC, Len.Int.DFA_IMPST_TFR_ANTIC, Len.Fract.DFA_IMPST_TFR_ANTIC);
    }

    public byte[] getDfaImpstTfrAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPST_TFR_ANTIC, Pos.DFA_IMPST_TFR_ANTIC);
        return buffer;
    }

    public void setDfaImpstTfrAnticNull(String dfaImpstTfrAnticNull) {
        writeString(Pos.DFA_IMPST_TFR_ANTIC_NULL, dfaImpstTfrAnticNull, Len.DFA_IMPST_TFR_ANTIC_NULL);
    }

    /**Original name: DFA-IMPST-TFR-ANTIC-NULL<br>*/
    public String getDfaImpstTfrAnticNull() {
        return readString(Pos.DFA_IMPST_TFR_ANTIC_NULL, Len.DFA_IMPST_TFR_ANTIC_NULL);
    }

    public String getDfaImpstTfrAnticNullFormatted() {
        return Functions.padBlanks(getDfaImpstTfrAnticNull(), Len.DFA_IMPST_TFR_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_TFR_ANTIC = 1;
        public static final int DFA_IMPST_TFR_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_TFR_ANTIC = 8;
        public static final int DFA_IMPST_TFR_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_TFR_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_TFR_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
