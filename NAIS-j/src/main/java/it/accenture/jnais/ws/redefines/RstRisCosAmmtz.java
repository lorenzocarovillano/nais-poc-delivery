package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-COS-AMMTZ<br>
 * Variable: RST-RIS-COS-AMMTZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisCosAmmtz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisCosAmmtz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_COS_AMMTZ;
    }

    public void setRstRisCosAmmtz(AfDecimal rstRisCosAmmtz) {
        writeDecimalAsPacked(Pos.RST_RIS_COS_AMMTZ, rstRisCosAmmtz.copy());
    }

    public void setRstRisCosAmmtzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_COS_AMMTZ, Pos.RST_RIS_COS_AMMTZ);
    }

    /**Original name: RST-RIS-COS-AMMTZ<br>*/
    public AfDecimal getRstRisCosAmmtz() {
        return readPackedAsDecimal(Pos.RST_RIS_COS_AMMTZ, Len.Int.RST_RIS_COS_AMMTZ, Len.Fract.RST_RIS_COS_AMMTZ);
    }

    public byte[] getRstRisCosAmmtzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_COS_AMMTZ, Pos.RST_RIS_COS_AMMTZ);
        return buffer;
    }

    public void setRstRisCosAmmtzNull(String rstRisCosAmmtzNull) {
        writeString(Pos.RST_RIS_COS_AMMTZ_NULL, rstRisCosAmmtzNull, Len.RST_RIS_COS_AMMTZ_NULL);
    }

    /**Original name: RST-RIS-COS-AMMTZ-NULL<br>*/
    public String getRstRisCosAmmtzNull() {
        return readString(Pos.RST_RIS_COS_AMMTZ_NULL, Len.RST_RIS_COS_AMMTZ_NULL);
    }

    public String getRstRisCosAmmtzNullFormatted() {
        return Functions.padBlanks(getRstRisCosAmmtzNull(), Len.RST_RIS_COS_AMMTZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_COS_AMMTZ = 1;
        public static final int RST_RIS_COS_AMMTZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_COS_AMMTZ = 8;
        public static final int RST_RIS_COS_AMMTZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_COS_AMMTZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_COS_AMMTZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
