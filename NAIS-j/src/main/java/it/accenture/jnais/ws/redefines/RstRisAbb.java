package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-ABB<br>
 * Variable: RST-RIS-ABB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisAbb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisAbb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_ABB;
    }

    public void setRstRisAbb(AfDecimal rstRisAbb) {
        writeDecimalAsPacked(Pos.RST_RIS_ABB, rstRisAbb.copy());
    }

    public void setRstRisAbbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_ABB, Pos.RST_RIS_ABB);
    }

    /**Original name: RST-RIS-ABB<br>*/
    public AfDecimal getRstRisAbb() {
        return readPackedAsDecimal(Pos.RST_RIS_ABB, Len.Int.RST_RIS_ABB, Len.Fract.RST_RIS_ABB);
    }

    public byte[] getRstRisAbbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_ABB, Pos.RST_RIS_ABB);
        return buffer;
    }

    public void setRstRisAbbNull(String rstRisAbbNull) {
        writeString(Pos.RST_RIS_ABB_NULL, rstRisAbbNull, Len.RST_RIS_ABB_NULL);
    }

    /**Original name: RST-RIS-ABB-NULL<br>*/
    public String getRstRisAbbNull() {
        return readString(Pos.RST_RIS_ABB_NULL, Len.RST_RIS_ABB_NULL);
    }

    public String getRstRisAbbNullFormatted() {
        return Functions.padBlanks(getRstRisAbbNull(), Len.RST_RIS_ABB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_ABB = 1;
        public static final int RST_RIS_ABB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_ABB = 8;
        public static final int RST_RIS_ABB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_ABB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_ABB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
