package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0024-ID-BATCH<br>
 * Variable: LCCC0024-ID-BATCH from program LCCS0024<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Lccc0024IdBatch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Lccc0024IdBatch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0024_ID_BATCH;
    }

    public void setLccc0024IdBatch(int lccc0024IdBatch) {
        writeIntAsPacked(Pos.LCCC0024_ID_BATCH, lccc0024IdBatch, Len.Int.LCCC0024_ID_BATCH);
    }

    public void setLccc0024IdBatchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LCCC0024_ID_BATCH, Pos.LCCC0024_ID_BATCH);
    }

    /**Original name: LCCC0024-ID-BATCH<br>
	 * <pre>          07 LCCC0024-PER-ELAB-DA         PIC S9(08)V COMP-3.
	 *           07 LCCC0024-PER-ELAB-A          PIC S9(08)V COMP-3.</pre>*/
    public int getLccc0024IdBatch() {
        return readPackedAsInt(Pos.LCCC0024_ID_BATCH, Len.Int.LCCC0024_ID_BATCH);
    }

    public byte[] getLccc0024IdBatchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LCCC0024_ID_BATCH, Pos.LCCC0024_ID_BATCH);
        return buffer;
    }

    public void initLccc0024IdBatchSpaces() {
        fill(Pos.LCCC0024_ID_BATCH, Len.LCCC0024_ID_BATCH, Types.SPACE_CHAR);
    }

    public void setLccc0024IdBatchNull(String lccc0024IdBatchNull) {
        writeString(Pos.LCCC0024_ID_BATCH_NULL, lccc0024IdBatchNull, Len.LCCC0024_ID_BATCH_NULL);
    }

    /**Original name: LCCC0024-ID-BATCH-NULL<br>*/
    public String getLccc0024IdBatchNull() {
        return readString(Pos.LCCC0024_ID_BATCH_NULL, Len.LCCC0024_ID_BATCH_NULL);
    }

    public String getLccc0024IdBatchNullFormatted() {
        return Functions.padBlanks(getLccc0024IdBatchNull(), Len.LCCC0024_ID_BATCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LCCC0024_ID_BATCH = 1;
        public static final int LCCC0024_ID_BATCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0024_ID_BATCH = 5;
        public static final int LCCC0024_ID_BATCH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LCCC0024_ID_BATCH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
