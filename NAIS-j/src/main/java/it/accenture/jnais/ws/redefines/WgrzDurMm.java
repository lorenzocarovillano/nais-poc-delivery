package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-DUR-MM<br>
 * Variable: WGRZ-DUR-MM from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzDurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzDurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_DUR_MM;
    }

    public void setWgrzDurMm(int wgrzDurMm) {
        writeIntAsPacked(Pos.WGRZ_DUR_MM, wgrzDurMm, Len.Int.WGRZ_DUR_MM);
    }

    public void setWgrzDurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_DUR_MM, Pos.WGRZ_DUR_MM);
    }

    /**Original name: WGRZ-DUR-MM<br>*/
    public int getWgrzDurMm() {
        return readPackedAsInt(Pos.WGRZ_DUR_MM, Len.Int.WGRZ_DUR_MM);
    }

    public byte[] getWgrzDurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_DUR_MM, Pos.WGRZ_DUR_MM);
        return buffer;
    }

    public void initWgrzDurMmSpaces() {
        fill(Pos.WGRZ_DUR_MM, Len.WGRZ_DUR_MM, Types.SPACE_CHAR);
    }

    public void setWgrzDurMmNull(String wgrzDurMmNull) {
        writeString(Pos.WGRZ_DUR_MM_NULL, wgrzDurMmNull, Len.WGRZ_DUR_MM_NULL);
    }

    /**Original name: WGRZ-DUR-MM-NULL<br>*/
    public String getWgrzDurMmNull() {
        return readString(Pos.WGRZ_DUR_MM_NULL, Len.WGRZ_DUR_MM_NULL);
    }

    public String getWgrzDurMmNullFormatted() {
        return Functions.padBlanks(getWgrzDurMmNull(), Len.WGRZ_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_DUR_MM = 1;
        public static final int WGRZ_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_DUR_MM = 3;
        public static final int WGRZ_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
