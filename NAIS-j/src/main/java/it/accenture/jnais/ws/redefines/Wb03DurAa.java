package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-AA<br>
 * Variable: WB03-DUR-AA from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR_AA;
    }

    public void setWb03DurAa(int wb03DurAa) {
        writeIntAsPacked(Pos.WB03_DUR_AA, wb03DurAa, Len.Int.WB03_DUR_AA);
    }

    public void setWb03DurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR_AA, Pos.WB03_DUR_AA);
    }

    /**Original name: WB03-DUR-AA<br>*/
    public int getWb03DurAa() {
        return readPackedAsInt(Pos.WB03_DUR_AA, Len.Int.WB03_DUR_AA);
    }

    public byte[] getWb03DurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR_AA, Pos.WB03_DUR_AA);
        return buffer;
    }

    public void setWb03DurAaNull(String wb03DurAaNull) {
        writeString(Pos.WB03_DUR_AA_NULL, wb03DurAaNull, Len.WB03_DUR_AA_NULL);
    }

    /**Original name: WB03-DUR-AA-NULL<br>*/
    public String getWb03DurAaNull() {
        return readString(Pos.WB03_DUR_AA_NULL, Len.WB03_DUR_AA_NULL);
    }

    public String getWb03DurAaNullFormatted() {
        return Functions.padBlanks(getWb03DurAaNull(), Len.WB03_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR_AA = 1;
        public static final int WB03_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR_AA = 3;
        public static final int WB03_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
