package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.AdesDb;
import it.accenture.jnais.copy.Idbvstb3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndAdes;
import it.accenture.jnais.copy.IndDettTitCont;
import it.accenture.jnais.copy.WlbRecPrenLdbm0250;
import it.accenture.jnais.ws.enums.FlagAccessoXRangeLdbm0170;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBM0250<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbm0250Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: WLB-REC-PREN
    private WlbRecPrenLdbm0250 wlbRecPren = new WlbRecPrenLdbm0250();
    //Original name: WS-TP-FRM-ASSVA1
    private String wsTpFrmAssva1 = DefaultValues.stringVal(Len.WS_TP_FRM_ASSVA1);
    //Original name: WS-TP-FRM-ASSVA2
    private String wsTpFrmAssva2 = DefaultValues.stringVal(Len.WS_TP_FRM_ASSVA2);
    //Original name: WS-DT-N
    private WsDtN wsDtN = new WsDtN();
    //Original name: WS-DT-X
    private WsDtX wsDtX = new WsDtX();
    //Original name: WS-DT-PTF-X
    private String wsDtPtfX = DefaultValues.stringVal(Len.WS_DT_PTF_X);
    //Original name: WS-DT-TS-PTF
    private long wsDtTsPtf = DefaultValues.LONG_VAL;
    //Original name: WS-DT-TS-PTF-PRE
    private long wsDtTsPtfPre = DefaultValues.LONG_VAL;
    //Original name: FLAG-ACCESSO-X-RANGE
    private FlagAccessoXRangeLdbm0170 flagAccessoXRange = new FlagAccessoXRangeLdbm0170();
    //Original name: IND-ADES
    private IndAdes indAdes = new IndAdes();
    //Original name: ADES-DB
    private AdesDb adesDb = new AdesDb();
    //Original name: IND-POLI
    private IndDettTitCont indPoli = new IndDettTitCont();
    //Original name: POLI-DB
    private AdesDb poliDb = new AdesDb();
    //Original name: IND-STB-ID-MOVI-CHIU
    private short indStbIdMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IDBVSTB3
    private Idbvstb3 idbvstb3 = new Idbvstb3();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWsTpFrmAssva1(String wsTpFrmAssva1) {
        this.wsTpFrmAssva1 = Functions.subString(wsTpFrmAssva1, Len.WS_TP_FRM_ASSVA1);
    }

    public String getWsTpFrmAssva1() {
        return this.wsTpFrmAssva1;
    }

    public void setWsTpFrmAssva2(String wsTpFrmAssva2) {
        this.wsTpFrmAssva2 = Functions.subString(wsTpFrmAssva2, Len.WS_TP_FRM_ASSVA2);
    }

    public String getWsTpFrmAssva2() {
        return this.wsTpFrmAssva2;
    }

    public void setWsDtPtfX(String wsDtPtfX) {
        this.wsDtPtfX = Functions.subString(wsDtPtfX, Len.WS_DT_PTF_X);
    }

    public String getWsDtPtfX() {
        return this.wsDtPtfX;
    }

    public void setWsDtTsPtf(long wsDtTsPtf) {
        this.wsDtTsPtf = wsDtTsPtf;
    }

    public long getWsDtTsPtf() {
        return this.wsDtTsPtf;
    }

    public void setWsDtTsPtfPre(long wsDtTsPtfPre) {
        this.wsDtTsPtfPre = wsDtTsPtfPre;
    }

    public long getWsDtTsPtfPre() {
        return this.wsDtTsPtfPre;
    }

    public void setIndStbIdMoviChiu(short indStbIdMoviChiu) {
        this.indStbIdMoviChiu = indStbIdMoviChiu;
    }

    public short getIndStbIdMoviChiu() {
        return this.indStbIdMoviChiu;
    }

    public AdesDb getAdesDb() {
        return adesDb;
    }

    public FlagAccessoXRangeLdbm0170 getFlagAccessoXRange() {
        return flagAccessoXRange;
    }

    public Idbvstb3 getIdbvstb3() {
        return idbvstb3;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndAdes getIndAdes() {
        return indAdes;
    }

    public IndDettTitCont getIndPoli() {
        return indPoli;
    }

    public AdesDb getPoliDb() {
        return poliDb;
    }

    public WlbRecPrenLdbm0250 getWlbRecPren() {
        return wlbRecPren;
    }

    public WsDtN getWsDtN() {
        return wsDtN;
    }

    public WsDtX getWsDtX() {
        return wsDtX;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_FRM_ASSVA1 = 2;
        public static final int WS_TP_FRM_ASSVA2 = 2;
        public static final int WS_DT_PTF_X = 10;
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int WS_TIMESTAMP = 18;
        public static final int WCOM_DT_SYS = 8;
        public static final int WCOM_TIMESTAMP_SYS = 18;
        public static final int WS_DT_COMPETENZA8 = 8;
        public static final int WS_LT40 = 2;
        public static final int WS_LT_ORA_COMPETENZA8 = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
