package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.Idbvbjs3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndParamOgg;
import it.accenture.jnais.ws.enums.FlagAccesso;
import it.accenture.jnais.ws.enums.FlagAccessoXRange;
import it.accenture.jnais.ws.redefines.WkIdJobX;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IABS0060<br>
 * Generated as a class for rule WS.<br>*/
public class Iabs0060Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: FLAG-ACCESSO
    private FlagAccesso flagAccesso = new FlagAccesso();
    //Original name: FLAG-ACCESSO-X-RANGE
    private FlagAccessoXRange flagAccessoXRange = new FlagAccessoXRange();
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: WK-ID-JOB-X
    private WkIdJobX wkIdJobX = new WkIdJobX();
    //Original name: IND-BTC-JOB-SCHEDULE
    private IndParamOgg indBtcJobSchedule = new IndParamOgg();
    //Original name: IDBVBJS3
    private Idbvbjs3 idbvbjs3 = new Idbvbjs3();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public FlagAccesso getFlagAccesso() {
        return flagAccesso;
    }

    public FlagAccessoXRange getFlagAccessoXRange() {
        return flagAccessoXRange;
    }

    public Idbvbjs3 getIdbvbjs3() {
        return idbvbjs3;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndParamOgg getIndBtcJobSchedule() {
        return indBtcJobSchedule;
    }

    public WkIdJobX getWkIdJobX() {
        return wkIdJobX;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 300;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
