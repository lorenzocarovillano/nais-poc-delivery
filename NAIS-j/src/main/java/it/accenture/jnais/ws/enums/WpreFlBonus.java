package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WPRE-FL-BONUS<br>
 * Variable: WPRE-FL-BONUS from copybook LOAC0560<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpreFlBonus {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char RICORR = 'R';
    public static final char FEDELTA = 'F';
    public static final char ENTRAMBE = 'E';

    //==== METHODS ====
    public void setFlBonus(char flBonus) {
        this.value = flBonus;
    }

    public char getFlBonus() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_BONUS = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
