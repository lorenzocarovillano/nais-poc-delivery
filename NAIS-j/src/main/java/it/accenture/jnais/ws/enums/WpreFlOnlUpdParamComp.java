package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WPRE-FL-ONL-UPD-PARAM-COMP<br>
 * Variable: WPRE-FL-ONL-UPD-PARAM-COMP from copybook LOAC0560<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpreFlOnlUpdParamComp {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlOnlUpdParamComp(char flOnlUpdParamComp) {
        this.value = flOnlUpdParamComp;
    }

    public char getFlOnlUpdParamComp() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_ONL_UPD_PARAM_COMP = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
