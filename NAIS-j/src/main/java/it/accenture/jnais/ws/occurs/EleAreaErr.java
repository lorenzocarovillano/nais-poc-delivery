package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: ELE-AREA-ERR<br>
 * Variables: ELE-AREA-ERR from program LRGS0660<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class EleAreaErr implements ICopyable<EleAreaErr> {

    //==== PROPERTIES ====
    //Original name: CONTROLLO
    private String controllo = DefaultValues.stringVal(Len.CONTROLLO);
    //Original name: GRAVITA
    private String gravita = DefaultValues.stringVal(Len.GRAVITA);

    //==== CONSTRUCTORS ====
    public EleAreaErr() {
    }

    public EleAreaErr(EleAreaErr eleAreaErr) {
        this();
        this.controllo = eleAreaErr.controllo;
        this.gravita = eleAreaErr.gravita;
    }

    //==== METHODS ====
    public void setControlloFormatted(String controllo) {
        this.controllo = Trunc.toUnsignedNumeric(controllo, Len.CONTROLLO);
    }

    public int getControllo() {
        return NumericDisplay.asInt(this.controllo);
    }

    public void setGravitaFormatted(String gravita) {
        this.gravita = Trunc.toUnsignedNumeric(gravita, Len.GRAVITA);
    }

    public int getGravita() {
        return NumericDisplay.asInt(this.gravita);
    }

    public EleAreaErr copy() {
        return new EleAreaErr(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CONTROLLO = 9;
        public static final int GRAVITA = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
