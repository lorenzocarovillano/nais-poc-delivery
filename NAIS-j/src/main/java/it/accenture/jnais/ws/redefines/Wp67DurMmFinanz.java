package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-DUR-MM-FINANZ<br>
 * Variable: WP67-DUR-MM-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67DurMmFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67DurMmFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_DUR_MM_FINANZ;
    }

    public void setWp67DurMmFinanz(int wp67DurMmFinanz) {
        writeIntAsPacked(Pos.WP67_DUR_MM_FINANZ, wp67DurMmFinanz, Len.Int.WP67_DUR_MM_FINANZ);
    }

    public void setWp67DurMmFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_DUR_MM_FINANZ, Pos.WP67_DUR_MM_FINANZ);
    }

    /**Original name: WP67-DUR-MM-FINANZ<br>*/
    public int getWp67DurMmFinanz() {
        return readPackedAsInt(Pos.WP67_DUR_MM_FINANZ, Len.Int.WP67_DUR_MM_FINANZ);
    }

    public byte[] getWp67DurMmFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_DUR_MM_FINANZ, Pos.WP67_DUR_MM_FINANZ);
        return buffer;
    }

    public void setWp67DurMmFinanzNull(String wp67DurMmFinanzNull) {
        writeString(Pos.WP67_DUR_MM_FINANZ_NULL, wp67DurMmFinanzNull, Len.WP67_DUR_MM_FINANZ_NULL);
    }

    /**Original name: WP67-DUR-MM-FINANZ-NULL<br>*/
    public String getWp67DurMmFinanzNull() {
        return readString(Pos.WP67_DUR_MM_FINANZ_NULL, Len.WP67_DUR_MM_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_DUR_MM_FINANZ = 1;
        public static final int WP67_DUR_MM_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_DUR_MM_FINANZ = 3;
        public static final int WP67_DUR_MM_FINANZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_DUR_MM_FINANZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
