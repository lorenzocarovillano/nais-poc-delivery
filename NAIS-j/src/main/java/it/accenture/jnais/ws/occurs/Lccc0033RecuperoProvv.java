package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0033-RECUPERO-PROVV<br>
 * Variables: LCCC0033-RECUPERO-PROVV from copybook LCCC0033<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Lccc0033RecuperoProvv {

    //==== PROPERTIES ====
    //Original name: LCCC0033-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LCCC0033-IB-OGG
    private String ibOgg = DefaultValues.stringVal(Len.IB_OGG);
    //Original name: LCCC0033-STATO
    private String stato = DefaultValues.stringVal(Len.STATO);
    //Original name: LCCC0033-CAUSALE
    private String causale = DefaultValues.stringVal(Len.CAUSALE);
    //Original name: LCCC0033-DT-DECORRENZA
    private int dtDecorrenza = DefaultValues.INT_VAL;
    //Original name: LCCC0033-DT-STORNO
    private int dtStorno = DefaultValues.INT_VAL;
    //Original name: LCCC0033-IMP-PREMIO-ANNUO
    private AfDecimal impPremioAnnuo = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    public void setRecuperoProvvBytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        ibOgg = MarshalByte.readString(buffer, position, Len.IB_OGG);
        position += Len.IB_OGG;
        stato = MarshalByte.readString(buffer, position, Len.STATO);
        position += Len.STATO;
        causale = MarshalByte.readString(buffer, position, Len.CAUSALE);
        position += Len.CAUSALE;
        dtDecorrenza = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DECORRENZA, 0);
        position += Len.DT_DECORRENZA;
        dtStorno = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_STORNO, 0);
        position += Len.DT_STORNO;
        impPremioAnnuo.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_PREMIO_ANNUO, Len.Fract.IMP_PREMIO_ANNUO));
    }

    public byte[] getRecuperoProvvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeString(buffer, position, ibOgg, Len.IB_OGG);
        position += Len.IB_OGG;
        MarshalByte.writeString(buffer, position, stato, Len.STATO);
        position += Len.STATO;
        MarshalByte.writeString(buffer, position, causale, Len.CAUSALE);
        position += Len.CAUSALE;
        MarshalByte.writeIntAsPacked(buffer, position, dtDecorrenza, Len.Int.DT_DECORRENZA, 0);
        position += Len.DT_DECORRENZA;
        MarshalByte.writeIntAsPacked(buffer, position, dtStorno, Len.Int.DT_STORNO, 0);
        position += Len.DT_STORNO;
        MarshalByte.writeDecimalAsPacked(buffer, position, impPremioAnnuo.copy());
        return buffer;
    }

    public void initRecuperoProvvSpaces() {
        idPoli = Types.INVALID_INT_VAL;
        ibOgg = "";
        stato = "";
        causale = "";
        dtDecorrenza = Types.INVALID_INT_VAL;
        dtStorno = Types.INVALID_INT_VAL;
        impPremioAnnuo.setNaN();
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setIbOgg(String ibOgg) {
        this.ibOgg = Functions.subString(ibOgg, Len.IB_OGG);
    }

    public String getIbOgg() {
        return this.ibOgg;
    }

    public void setStato(String stato) {
        this.stato = Functions.subString(stato, Len.STATO);
    }

    public String getStato() {
        return this.stato;
    }

    public void setCausale(String causale) {
        this.causale = Functions.subString(causale, Len.CAUSALE);
    }

    public String getCausale() {
        return this.causale;
    }

    public void setDtDecorrenza(int dtDecorrenza) {
        this.dtDecorrenza = dtDecorrenza;
    }

    public int getDtDecorrenza() {
        return this.dtDecorrenza;
    }

    public void setDtStorno(int dtStorno) {
        this.dtStorno = dtStorno;
    }

    public int getDtStorno() {
        return this.dtStorno;
    }

    public void setImpPremioAnnuo(AfDecimal impPremioAnnuo) {
        this.impPremioAnnuo.assign(impPremioAnnuo);
    }

    public AfDecimal getImpPremioAnnuo() {
        return this.impPremioAnnuo.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POLI = 5;
        public static final int IB_OGG = 40;
        public static final int STATO = 2;
        public static final int CAUSALE = 2;
        public static final int DT_DECORRENZA = 5;
        public static final int DT_STORNO = 5;
        public static final int IMP_PREMIO_ANNUO = 8;
        public static final int RECUPERO_PROVV = ID_POLI + IB_OGG + STATO + CAUSALE + DT_DECORRENZA + DT_STORNO + IMP_PREMIO_ANNUO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int DT_DECORRENZA = 8;
            public static final int DT_STORNO = 8;
            public static final int IMP_PREMIO_ANNUO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMP_PREMIO_ANNUO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
