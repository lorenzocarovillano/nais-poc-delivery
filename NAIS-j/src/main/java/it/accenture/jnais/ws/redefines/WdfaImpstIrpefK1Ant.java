package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPST-IRPEF-K1-ANT<br>
 * Variable: WDFA-IMPST-IRPEF-K1-ANT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpstIrpefK1Ant extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpstIrpefK1Ant() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPST_IRPEF_K1_ANT;
    }

    public void setWdfaImpstIrpefK1Ant(AfDecimal wdfaImpstIrpefK1Ant) {
        writeDecimalAsPacked(Pos.WDFA_IMPST_IRPEF_K1_ANT, wdfaImpstIrpefK1Ant.copy());
    }

    public void setWdfaImpstIrpefK1AntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPST_IRPEF_K1_ANT, Pos.WDFA_IMPST_IRPEF_K1_ANT);
    }

    /**Original name: WDFA-IMPST-IRPEF-K1-ANT<br>*/
    public AfDecimal getWdfaImpstIrpefK1Ant() {
        return readPackedAsDecimal(Pos.WDFA_IMPST_IRPEF_K1_ANT, Len.Int.WDFA_IMPST_IRPEF_K1_ANT, Len.Fract.WDFA_IMPST_IRPEF_K1_ANT);
    }

    public byte[] getWdfaImpstIrpefK1AntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPST_IRPEF_K1_ANT, Pos.WDFA_IMPST_IRPEF_K1_ANT);
        return buffer;
    }

    public void setWdfaImpstIrpefK1AntNull(String wdfaImpstIrpefK1AntNull) {
        writeString(Pos.WDFA_IMPST_IRPEF_K1_ANT_NULL, wdfaImpstIrpefK1AntNull, Len.WDFA_IMPST_IRPEF_K1_ANT_NULL);
    }

    /**Original name: WDFA-IMPST-IRPEF-K1-ANT-NULL<br>*/
    public String getWdfaImpstIrpefK1AntNull() {
        return readString(Pos.WDFA_IMPST_IRPEF_K1_ANT_NULL, Len.WDFA_IMPST_IRPEF_K1_ANT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_IRPEF_K1_ANT = 1;
        public static final int WDFA_IMPST_IRPEF_K1_ANT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_IRPEF_K1_ANT = 8;
        public static final int WDFA_IMPST_IRPEF_K1_ANT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_IRPEF_K1_ANT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_IRPEF_K1_ANT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
