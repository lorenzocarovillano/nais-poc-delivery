package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0221-TAB-DOMANDA<br>
 * Variables: IVVC0221-TAB-DOMANDA from copybook IVVC0221<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0221TabDomanda {

    //==== PROPERTIES ====
    public static final int TAB_RISPOSTA_MAXOCCURS = 10;
    //Original name: IVVC0221-COD-DOMANDA
    private String codDomanda = DefaultValues.stringVal(Len.COD_DOMANDA);
    //Original name: IVVC0221-FILLER-DOM
    private char fillerDom = DefaultValues.CHAR_VAL;
    //Original name: IVVC0221-MAX-RISPOSTA
    private String maxRisposta = DefaultValues.stringVal(Len.MAX_RISPOSTA);
    //Original name: IVVC0221-FILLER-3
    private char filler3 = DefaultValues.CHAR_VAL;
    //Original name: IVVC0221-TAB-RISPOSTA
    private Ivvc0221TabRisposta[] tabRisposta = new Ivvc0221TabRisposta[TAB_RISPOSTA_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ivvc0221TabDomanda() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabRispostaIdx = 1; tabRispostaIdx <= TAB_RISPOSTA_MAXOCCURS; tabRispostaIdx++) {
            tabRisposta[tabRispostaIdx - 1] = new Ivvc0221TabRisposta();
        }
    }

    public void setTabDomandaBytes(byte[] buffer, int offset) {
        int position = offset;
        codDomanda = MarshalByte.readString(buffer, position, Len.COD_DOMANDA);
        position += Len.COD_DOMANDA;
        fillerDom = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        maxRisposta = MarshalByte.readFixedString(buffer, position, Len.MAX_RISPOSTA);
        position += Len.MAX_RISPOSTA;
        filler3 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        for (int idx = 1; idx <= TAB_RISPOSTA_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabRisposta[idx - 1].setTabRispostaBytes(buffer, position);
                position += Ivvc0221TabRisposta.Len.TAB_RISPOSTA;
            }
            else {
                tabRisposta[idx - 1].initTabRispostaSpaces();
                position += Ivvc0221TabRisposta.Len.TAB_RISPOSTA;
            }
        }
    }

    public void initTabDomandaSpaces() {
        codDomanda = "";
        fillerDom = Types.SPACE_CHAR;
        maxRisposta = "";
        filler3 = Types.SPACE_CHAR;
        for (int idx = 1; idx <= TAB_RISPOSTA_MAXOCCURS; idx++) {
            tabRisposta[idx - 1].initTabRispostaSpaces();
        }
    }

    public void setCodDomanda(String codDomanda) {
        this.codDomanda = Functions.subString(codDomanda, Len.COD_DOMANDA);
    }

    public String getCodDomanda() {
        return this.codDomanda;
    }

    public void setFillerDom(char fillerDom) {
        this.fillerDom = fillerDom;
    }

    public char getFillerDom() {
        return this.fillerDom;
    }

    public void setFiller3(char filler3) {
        this.filler3 = filler3;
    }

    public char getFiller3() {
        return this.filler3;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_DOMANDA = 5;
        public static final int MAX_RISPOSTA = 2;
        public static final int FILLER_DOM = 1;
        public static final int FILLER3 = 1;
        public static final int TAB_DOMANDA = COD_DOMANDA + FILLER_DOM + MAX_RISPOSTA + FILLER3 + Ivvc0221TabDomanda.TAB_RISPOSTA_MAXOCCURS * Ivvc0221TabRisposta.Len.TAB_RISPOSTA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
