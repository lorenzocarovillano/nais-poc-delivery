package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-FRQ-MOVI<br>
 * Variable: PMO-FRQ-MOVI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoFrqMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoFrqMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_FRQ_MOVI;
    }

    public void setPmoFrqMovi(int pmoFrqMovi) {
        writeIntAsPacked(Pos.PMO_FRQ_MOVI, pmoFrqMovi, Len.Int.PMO_FRQ_MOVI);
    }

    public void setPmoFrqMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_FRQ_MOVI, Pos.PMO_FRQ_MOVI);
    }

    /**Original name: PMO-FRQ-MOVI<br>*/
    public int getPmoFrqMovi() {
        return readPackedAsInt(Pos.PMO_FRQ_MOVI, Len.Int.PMO_FRQ_MOVI);
    }

    public byte[] getPmoFrqMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_FRQ_MOVI, Pos.PMO_FRQ_MOVI);
        return buffer;
    }

    public void initPmoFrqMoviHighValues() {
        fill(Pos.PMO_FRQ_MOVI, Len.PMO_FRQ_MOVI, Types.HIGH_CHAR_VAL);
    }

    public void setPmoFrqMoviNull(String pmoFrqMoviNull) {
        writeString(Pos.PMO_FRQ_MOVI_NULL, pmoFrqMoviNull, Len.PMO_FRQ_MOVI_NULL);
    }

    /**Original name: PMO-FRQ-MOVI-NULL<br>*/
    public String getPmoFrqMoviNull() {
        return readString(Pos.PMO_FRQ_MOVI_NULL, Len.PMO_FRQ_MOVI_NULL);
    }

    public String getPmoFrqMoviNullFormatted() {
        return Functions.padBlanks(getPmoFrqMoviNull(), Len.PMO_FRQ_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_FRQ_MOVI = 1;
        public static final int PMO_FRQ_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_FRQ_MOVI = 3;
        public static final int PMO_FRQ_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_FRQ_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
