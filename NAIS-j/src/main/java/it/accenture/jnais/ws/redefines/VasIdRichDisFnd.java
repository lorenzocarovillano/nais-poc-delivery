package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-ID-RICH-DIS-FND<br>
 * Variable: VAS-ID-RICH-DIS-FND from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasIdRichDisFnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasIdRichDisFnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_ID_RICH_DIS_FND;
    }

    public void setVasIdRichDisFnd(int vasIdRichDisFnd) {
        writeIntAsPacked(Pos.VAS_ID_RICH_DIS_FND, vasIdRichDisFnd, Len.Int.VAS_ID_RICH_DIS_FND);
    }

    public void setVasIdRichDisFndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_ID_RICH_DIS_FND, Pos.VAS_ID_RICH_DIS_FND);
    }

    /**Original name: VAS-ID-RICH-DIS-FND<br>*/
    public int getVasIdRichDisFnd() {
        return readPackedAsInt(Pos.VAS_ID_RICH_DIS_FND, Len.Int.VAS_ID_RICH_DIS_FND);
    }

    public byte[] getVasIdRichDisFndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_ID_RICH_DIS_FND, Pos.VAS_ID_RICH_DIS_FND);
        return buffer;
    }

    public void setVasIdRichDisFndNull(String vasIdRichDisFndNull) {
        writeString(Pos.VAS_ID_RICH_DIS_FND_NULL, vasIdRichDisFndNull, Len.VAS_ID_RICH_DIS_FND_NULL);
    }

    /**Original name: VAS-ID-RICH-DIS-FND-NULL<br>*/
    public String getVasIdRichDisFndNull() {
        return readString(Pos.VAS_ID_RICH_DIS_FND_NULL, Len.VAS_ID_RICH_DIS_FND_NULL);
    }

    public String getVasIdRichDisFndNullFormatted() {
        return Functions.padBlanks(getVasIdRichDisFndNull(), Len.VAS_ID_RICH_DIS_FND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_ID_RICH_DIS_FND = 1;
        public static final int VAS_ID_RICH_DIS_FND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_ID_RICH_DIS_FND = 5;
        public static final int VAS_ID_RICH_DIS_FND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_ID_RICH_DIS_FND = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
