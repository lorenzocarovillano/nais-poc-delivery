package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IIMPST-PRVR-CALC<br>
 * Variable: DFL-IIMPST-PRVR-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIimpstPrvrCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIimpstPrvrCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IIMPST_PRVR_CALC;
    }

    public void setDflIimpstPrvrCalc(AfDecimal dflIimpstPrvrCalc) {
        writeDecimalAsPacked(Pos.DFL_IIMPST_PRVR_CALC, dflIimpstPrvrCalc.copy());
    }

    public void setDflIimpstPrvrCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IIMPST_PRVR_CALC, Pos.DFL_IIMPST_PRVR_CALC);
    }

    /**Original name: DFL-IIMPST-PRVR-CALC<br>*/
    public AfDecimal getDflIimpstPrvrCalc() {
        return readPackedAsDecimal(Pos.DFL_IIMPST_PRVR_CALC, Len.Int.DFL_IIMPST_PRVR_CALC, Len.Fract.DFL_IIMPST_PRVR_CALC);
    }

    public byte[] getDflIimpstPrvrCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IIMPST_PRVR_CALC, Pos.DFL_IIMPST_PRVR_CALC);
        return buffer;
    }

    public void setDflIimpstPrvrCalcNull(String dflIimpstPrvrCalcNull) {
        writeString(Pos.DFL_IIMPST_PRVR_CALC_NULL, dflIimpstPrvrCalcNull, Len.DFL_IIMPST_PRVR_CALC_NULL);
    }

    /**Original name: DFL-IIMPST-PRVR-CALC-NULL<br>*/
    public String getDflIimpstPrvrCalcNull() {
        return readString(Pos.DFL_IIMPST_PRVR_CALC_NULL, Len.DFL_IIMPST_PRVR_CALC_NULL);
    }

    public String getDflIimpstPrvrCalcNullFormatted() {
        return Functions.padBlanks(getDflIimpstPrvrCalcNull(), Len.DFL_IIMPST_PRVR_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST_PRVR_CALC = 1;
        public static final int DFL_IIMPST_PRVR_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST_PRVR_CALC = 8;
        public static final int DFL_IIMPST_PRVR_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST_PRVR_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST_PRVR_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
