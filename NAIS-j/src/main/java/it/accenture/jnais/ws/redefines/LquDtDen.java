package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-DT-DEN<br>
 * Variable: LQU-DT-DEN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquDtDen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquDtDen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_DT_DEN;
    }

    public void setLquDtDen(int lquDtDen) {
        writeIntAsPacked(Pos.LQU_DT_DEN, lquDtDen, Len.Int.LQU_DT_DEN);
    }

    public void setLquDtDenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_DT_DEN, Pos.LQU_DT_DEN);
    }

    /**Original name: LQU-DT-DEN<br>*/
    public int getLquDtDen() {
        return readPackedAsInt(Pos.LQU_DT_DEN, Len.Int.LQU_DT_DEN);
    }

    public byte[] getLquDtDenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_DT_DEN, Pos.LQU_DT_DEN);
        return buffer;
    }

    public void setLquDtDenNull(String lquDtDenNull) {
        writeString(Pos.LQU_DT_DEN_NULL, lquDtDenNull, Len.LQU_DT_DEN_NULL);
    }

    /**Original name: LQU-DT-DEN-NULL<br>*/
    public String getLquDtDenNull() {
        return readString(Pos.LQU_DT_DEN_NULL, Len.LQU_DT_DEN_NULL);
    }

    public String getLquDtDenNullFormatted() {
        return Functions.padBlanks(getLquDtDenNull(), Len.LQU_DT_DEN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_DT_DEN = 1;
        public static final int LQU_DT_DEN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_DT_DEN = 5;
        public static final int LQU_DT_DEN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_DT_DEN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
