package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMPST-252-IPT<br>
 * Variable: WBEL-IMPST-252-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpst252Ipt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpst252Ipt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMPST252_IPT;
    }

    public void setWbelImpst252Ipt(AfDecimal wbelImpst252Ipt) {
        writeDecimalAsPacked(Pos.WBEL_IMPST252_IPT, wbelImpst252Ipt.copy());
    }

    public void setWbelImpst252IptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMPST252_IPT, Pos.WBEL_IMPST252_IPT);
    }

    /**Original name: WBEL-IMPST-252-IPT<br>*/
    public AfDecimal getWbelImpst252Ipt() {
        return readPackedAsDecimal(Pos.WBEL_IMPST252_IPT, Len.Int.WBEL_IMPST252_IPT, Len.Fract.WBEL_IMPST252_IPT);
    }

    public byte[] getWbelImpst252IptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMPST252_IPT, Pos.WBEL_IMPST252_IPT);
        return buffer;
    }

    public void initWbelImpst252IptSpaces() {
        fill(Pos.WBEL_IMPST252_IPT, Len.WBEL_IMPST252_IPT, Types.SPACE_CHAR);
    }

    public void setWbelImpst252IptNull(String wbelImpst252IptNull) {
        writeString(Pos.WBEL_IMPST252_IPT_NULL, wbelImpst252IptNull, Len.WBEL_IMPST252_IPT_NULL);
    }

    /**Original name: WBEL-IMPST-252-IPT-NULL<br>*/
    public String getWbelImpst252IptNull() {
        return readString(Pos.WBEL_IMPST252_IPT_NULL, Len.WBEL_IMPST252_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST252_IPT = 1;
        public static final int WBEL_IMPST252_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST252_IPT = 8;
        public static final int WBEL_IMPST252_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST252_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST252_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
