package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-EC-MRM-IND<br>
 * Variable: PCO-DT-ULT-EC-MRM-IND from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEcMrmInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEcMrmInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_EC_MRM_IND;
    }

    public void setPcoDtUltEcMrmInd(int pcoDtUltEcMrmInd) {
        writeIntAsPacked(Pos.PCO_DT_ULT_EC_MRM_IND, pcoDtUltEcMrmInd, Len.Int.PCO_DT_ULT_EC_MRM_IND);
    }

    public void setPcoDtUltEcMrmIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_EC_MRM_IND, Pos.PCO_DT_ULT_EC_MRM_IND);
    }

    /**Original name: PCO-DT-ULT-EC-MRM-IND<br>*/
    public int getPcoDtUltEcMrmInd() {
        return readPackedAsInt(Pos.PCO_DT_ULT_EC_MRM_IND, Len.Int.PCO_DT_ULT_EC_MRM_IND);
    }

    public byte[] getPcoDtUltEcMrmIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_EC_MRM_IND, Pos.PCO_DT_ULT_EC_MRM_IND);
        return buffer;
    }

    public void initPcoDtUltEcMrmIndHighValues() {
        fill(Pos.PCO_DT_ULT_EC_MRM_IND, Len.PCO_DT_ULT_EC_MRM_IND, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEcMrmIndNull(String pcoDtUltEcMrmIndNull) {
        writeString(Pos.PCO_DT_ULT_EC_MRM_IND_NULL, pcoDtUltEcMrmIndNull, Len.PCO_DT_ULT_EC_MRM_IND_NULL);
    }

    /**Original name: PCO-DT-ULT-EC-MRM-IND-NULL<br>*/
    public String getPcoDtUltEcMrmIndNull() {
        return readString(Pos.PCO_DT_ULT_EC_MRM_IND_NULL, Len.PCO_DT_ULT_EC_MRM_IND_NULL);
    }

    public String getPcoDtUltEcMrmIndNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEcMrmIndNull(), Len.PCO_DT_ULT_EC_MRM_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_MRM_IND = 1;
        public static final int PCO_DT_ULT_EC_MRM_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_MRM_IND = 5;
        public static final int PCO_DT_ULT_EC_MRM_IND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_EC_MRM_IND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
