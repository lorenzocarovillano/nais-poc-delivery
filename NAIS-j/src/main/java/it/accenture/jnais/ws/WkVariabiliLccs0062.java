package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WK-VARIABILI<br>
 * Variable: WK-VARIABILI from program LCCS0062<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVariabiliLccs0062 {

    //==== PROPERTIES ====
    //Original name: WK-DT-INFERIORE
    private int dtInferiore = 0;
    //Original name: WK-DT-SUPERIORE
    private int dtSuperiore = 0;
    //Original name: WK-DATA-ULT-PCO
    private int dataUltPco = 0;
    //Original name: WK-COUNT-RATE
    private int countRate = 0;
    //Original name: WK-MESI
    private int mesi = 0;

    //==== METHODS ====
    public void setDtInferiore(int dtInferiore) {
        this.dtInferiore = dtInferiore;
    }

    public int getDtInferiore() {
        return this.dtInferiore;
    }

    public String getDtInferioreFormatted() {
        return PicFormatter.display(new PicParams("9(8)").setUsage(PicUsage.PACKED)).format(getDtInferiore()).toString();
    }

    public void setDtSuperiore(int dtSuperiore) {
        this.dtSuperiore = dtSuperiore;
    }

    public int getDtSuperiore() {
        return this.dtSuperiore;
    }

    public String getDtSuperioreFormatted() {
        return PicFormatter.display(new PicParams("9(8)").setUsage(PicUsage.PACKED)).format(getDtSuperiore()).toString();
    }

    public void setDataUltPco(int dataUltPco) {
        this.dataUltPco = dataUltPco;
    }

    public int getDataUltPco() {
        return this.dataUltPco;
    }

    public void setCountRate(int countRate) {
        this.countRate = countRate;
    }

    public int getCountRate() {
        return this.countRate;
    }

    public void setMesi(int mesi) {
        this.mesi = mesi;
    }

    public int getMesi() {
        return this.mesi;
    }
}
