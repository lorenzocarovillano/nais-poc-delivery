package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B01-VAL-QUO-INI<br>
 * Variable: B01-VAL-QUO-INI from program IDBSB010<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B01ValQuoIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B01ValQuoIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B01_VAL_QUO_INI;
    }

    public void setB01ValQuoIni(AfDecimal b01ValQuoIni) {
        writeDecimalAsPacked(Pos.B01_VAL_QUO_INI, b01ValQuoIni.copy());
    }

    public void setB01ValQuoIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B01_VAL_QUO_INI, Pos.B01_VAL_QUO_INI);
    }

    /**Original name: B01-VAL-QUO-INI<br>*/
    public AfDecimal getB01ValQuoIni() {
        return readPackedAsDecimal(Pos.B01_VAL_QUO_INI, Len.Int.B01_VAL_QUO_INI, Len.Fract.B01_VAL_QUO_INI);
    }

    public byte[] getB01ValQuoIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B01_VAL_QUO_INI, Pos.B01_VAL_QUO_INI);
        return buffer;
    }

    public void setB01ValQuoIniNull(String b01ValQuoIniNull) {
        writeString(Pos.B01_VAL_QUO_INI_NULL, b01ValQuoIniNull, Len.B01_VAL_QUO_INI_NULL);
    }

    /**Original name: B01-VAL-QUO-INI-NULL<br>*/
    public String getB01ValQuoIniNull() {
        return readString(Pos.B01_VAL_QUO_INI_NULL, Len.B01_VAL_QUO_INI_NULL);
    }

    public String getB01ValQuoIniNullFormatted() {
        return Functions.padBlanks(getB01ValQuoIniNull(), Len.B01_VAL_QUO_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B01_VAL_QUO_INI = 1;
        public static final int B01_VAL_QUO_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B01_VAL_QUO_INI = 7;
        public static final int B01_VAL_QUO_INI_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B01_VAL_QUO_INI = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int B01_VAL_QUO_INI = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
