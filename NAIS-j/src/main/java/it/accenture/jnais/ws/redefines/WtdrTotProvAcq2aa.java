package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-PROV-ACQ-2AA<br>
 * Variable: WTDR-TOT-PROV-ACQ-2AA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotProvAcq2aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotProvAcq2aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_PROV_ACQ2AA;
    }

    public void setWtdrTotProvAcq2aa(AfDecimal wtdrTotProvAcq2aa) {
        writeDecimalAsPacked(Pos.WTDR_TOT_PROV_ACQ2AA, wtdrTotProvAcq2aa.copy());
    }

    /**Original name: WTDR-TOT-PROV-ACQ-2AA<br>*/
    public AfDecimal getWtdrTotProvAcq2aa() {
        return readPackedAsDecimal(Pos.WTDR_TOT_PROV_ACQ2AA, Len.Int.WTDR_TOT_PROV_ACQ2AA, Len.Fract.WTDR_TOT_PROV_ACQ2AA);
    }

    public void setWtdrTotProvAcq2aaNull(String wtdrTotProvAcq2aaNull) {
        writeString(Pos.WTDR_TOT_PROV_ACQ2AA_NULL, wtdrTotProvAcq2aaNull, Len.WTDR_TOT_PROV_ACQ2AA_NULL);
    }

    /**Original name: WTDR-TOT-PROV-ACQ-2AA-NULL<br>*/
    public String getWtdrTotProvAcq2aaNull() {
        return readString(Pos.WTDR_TOT_PROV_ACQ2AA_NULL, Len.WTDR_TOT_PROV_ACQ2AA_NULL);
    }

    public String getWtdrTotProvAcq2aaNullFormatted() {
        return Functions.padBlanks(getWtdrTotProvAcq2aaNull(), Len.WTDR_TOT_PROV_ACQ2AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PROV_ACQ2AA = 1;
        public static final int WTDR_TOT_PROV_ACQ2AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PROV_ACQ2AA = 8;
        public static final int WTDR_TOT_PROV_ACQ2AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PROV_ACQ2AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PROV_ACQ2AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
