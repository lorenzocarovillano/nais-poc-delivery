package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Idsv8888Lccs0450;
import it.accenture.jnais.copy.Ldbv0011;
import it.accenture.jnais.copy.Ldbv4911;
import it.accenture.jnais.copy.QuotzAggFndLccs0450;
import it.accenture.jnais.copy.QuotzFndUnitLccs0450;
import it.accenture.jnais.copy.RichDisFndLccs0450;
import it.accenture.jnais.copy.RichInvstFnd;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.copy.ValAst;
import it.accenture.jnais.ws.enums.FlagCurTga;
import it.accenture.jnais.ws.enums.FlagCurVas;
import it.accenture.jnais.ws.enums.FlagFondo;
import it.accenture.jnais.ws.enums.FlagQtzAgg;
import it.accenture.jnais.ws.enums.FlagTpTrch;
import it.accenture.jnais.ws.enums.WsTpTrch;
import it.accenture.jnais.ws.occurs.WgrzTabGar;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0450<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0450Data {

    //==== PROPERTIES ====
    public static final int WGAR_TAB_GAR_MAXOCCURS = 20;
    //Original name: WK-PGM
    private String wkPgm = "LCCS0450";
    /**Original name: PGM-LDBS0130<br>
	 * <pre>----------------------------------------------------------------*
	 *     PGM CHIAMATI
	 * ----------------------------------------------------------------*</pre>*/
    private String pgmLdbs0130 = "LDBS0130";
    //Original name: PGM-IDBSRIF0
    private String pgmIdbsrif0 = "IDBSRIF0";
    //Original name: PGM-IDBSRDF0
    private String pgmIdbsrdf0 = "IDBSRDF0";
    //Original name: PGM-LDBS4910
    private String pgmLdbs4910 = "LDBS4910";
    //Original name: PGM-LDBS2080
    private String pgmLdbs2080 = "LDBS2080";
    //Original name: PGM-IDBSL410
    private String pgmIdbsl410 = "IDBSL410";
    //Original name: IO-A2K-LCCC0003
    private IoA2kLccc0003 ioA2kLccc0003 = new IoA2kLccc0003();
    //Original name: IN-RCODE
    private String inRcode = "00";
    //Original name: TRCH-DI-GAR
    private TrchDiGarIvvs0216 trchDiGar = new TrchDiGarIvvs0216();
    //Original name: RICH-DIS-FND
    private RichDisFndLccs0450 richDisFnd = new RichDisFndLccs0450();
    //Original name: RICH-INVST-FND
    private RichInvstFnd richInvstFnd = new RichInvstFnd();
    //Original name: VAL-AST
    private ValAst valAst = new ValAst();
    //Original name: QUOTZ-FND-UNIT
    private QuotzFndUnitLccs0450 quotzFndUnit = new QuotzFndUnitLccs0450();
    //Original name: QUOTZ-AGG-FND
    private QuotzAggFndLccs0450 quotzAggFnd = new QuotzAggFndLccs0450();
    //Original name: LDBV0011
    private Ldbv0011 ldbv0011 = new Ldbv0011();
    //Original name: LDBV4911
    private Ldbv4911 ldbv4911 = new Ldbv4911();
    /**Original name: VALORE-POSITIVO<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String valorePositivo = "VP";
    //Original name: ANNULLO-POSITIVO
    private String annulloPositivo = "AP";
    //Original name: VALORE-NEGATIVO
    private String valoreNegativo = "VN";
    //Original name: ANNULLO-NEGATIVO
    private String annulloNegativo = "AN";
    //Original name: VAS-LIQUIDAZIONE
    private String vasLiquidazione = "LQ";
    //Original name: ANNULLO-LIQUIDAZIONE
    private String annulloLiquidazione = "AL";
    //Original name: MAX-VAL-L19
    private int maxValL19 = 250;
    //Original name: FLAG-CUR-TGA
    private FlagCurTga flagCurTga = new FlagCurTga();
    //Original name: FLAG-CUR-VAS
    private FlagCurVas flagCurVas = new FlagCurVas();
    //Original name: FLAG-QTZ-AGG
    private FlagQtzAgg flagQtzAgg = new FlagQtzAgg();
    //Original name: FLAG-TP-TRCH
    private FlagTpTrch flagTpTrch = new FlagTpTrch();
    //Original name: FLAG-FONDO
    private FlagFondo flagFondo = new FlagFondo();
    //Original name: VARIABILI
    private Variabili variabili = new Variabili();
    //Original name: WGAR-ELE-GAR-MAX
    private short wgarEleGarMax = ((short)0);
    //Original name: WGAR-TAB-GAR
    private WgrzTabGar[] wgarTabGar = new WgrzTabGar[WGAR_TAB_GAR_MAXOCCURS];
    //Original name: INDICI
    private IndiciLccs0450 indici = new IndiciLccs0450();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: IDSV8888
    private Idsv8888Lccs0450 idsv8888 = new Idsv8888Lccs0450();
    /**Original name: WS-TP-TRCH<br>
	 * <pre>----------------------------------------------------------------*
	 *     TIPOLOGICHE DI PORTAFOGLIO
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_TRCH
	 * *****************************************************************</pre>*/
    private WsTpTrch wsTpTrch = new WsTpTrch();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: INT-REGISTER1
    private short intRegister1 = DefaultValues.SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lccs0450Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wgarTabGarIdx = 1; wgarTabGarIdx <= WGAR_TAB_GAR_MAXOCCURS; wgarTabGarIdx++) {
            wgarTabGar[wgarTabGarIdx - 1] = new WgrzTabGar();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getPgmLdbs0130() {
        return this.pgmLdbs0130;
    }

    public String getPgmLdbs0130Formatted() {
        return Functions.padBlanks(getPgmLdbs0130(), Len.PGM_LDBS0130);
    }

    public String getPgmIdbsrif0() {
        return this.pgmIdbsrif0;
    }

    public String getPgmIdbsrif0Formatted() {
        return Functions.padBlanks(getPgmIdbsrif0(), Len.PGM_IDBSRIF0);
    }

    public String getPgmIdbsrdf0() {
        return this.pgmIdbsrdf0;
    }

    public String getPgmIdbsrdf0Formatted() {
        return Functions.padBlanks(getPgmIdbsrdf0(), Len.PGM_IDBSRDF0);
    }

    public String getPgmLdbs4910() {
        return this.pgmLdbs4910;
    }

    public String getPgmLdbs4910Formatted() {
        return Functions.padBlanks(getPgmLdbs4910(), Len.PGM_LDBS4910);
    }

    public String getPgmLdbs2080() {
        return this.pgmLdbs2080;
    }

    public String getPgmLdbs2080Formatted() {
        return Functions.padBlanks(getPgmLdbs2080(), Len.PGM_LDBS2080);
    }

    public String getPgmIdbsl410() {
        return this.pgmIdbsl410;
    }

    public String getPgmIdbsl410Formatted() {
        return Functions.padBlanks(getPgmIdbsl410(), Len.PGM_IDBSL410);
    }

    public void setInRcodeFormatted(String inRcode) {
        this.inRcode = Trunc.toUnsignedNumeric(inRcode, Len.IN_RCODE);
    }

    public void setInRcodeFromBuffer(byte[] buffer) {
        inRcode = MarshalByte.readFixedString(buffer, 1, Len.IN_RCODE);
    }

    public short getInRcode() {
        return NumericDisplay.asShort(this.inRcode);
    }

    public String getInRcodeFormatted() {
        return this.inRcode;
    }

    public String getValorePositivo() {
        return this.valorePositivo;
    }

    public String getAnnulloPositivo() {
        return this.annulloPositivo;
    }

    public String getValoreNegativo() {
        return this.valoreNegativo;
    }

    public String getAnnulloNegativo() {
        return this.annulloNegativo;
    }

    public String getVasLiquidazione() {
        return this.vasLiquidazione;
    }

    public String getAnnulloLiquidazione() {
        return this.annulloLiquidazione;
    }

    public int getMaxValL19() {
        return this.maxValL19;
    }

    public void setWgarEleGarMax(short wgarEleGarMax) {
        this.wgarEleGarMax = wgarEleGarMax;
    }

    public short getWgarEleGarMax() {
        return this.wgarEleGarMax;
    }

    public void setIntRegister1(short intRegister1) {
        this.intRegister1 = intRegister1;
    }

    public short getIntRegister1() {
        return this.intRegister1;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public FlagCurTga getFlagCurTga() {
        return flagCurTga;
    }

    public FlagCurVas getFlagCurVas() {
        return flagCurVas;
    }

    public FlagFondo getFlagFondo() {
        return flagFondo;
    }

    public FlagQtzAgg getFlagQtzAgg() {
        return flagQtzAgg;
    }

    public FlagTpTrch getFlagTpTrch() {
        return flagTpTrch;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Idsv8888Lccs0450 getIdsv8888() {
        return idsv8888;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public IndiciLccs0450 getIndici() {
        return indici;
    }

    public IoA2kLccc0003 getIoA2kLccc0003() {
        return ioA2kLccc0003;
    }

    public Ldbv0011 getLdbv0011() {
        return ldbv0011;
    }

    public Ldbv4911 getLdbv4911() {
        return ldbv4911;
    }

    public QuotzAggFndLccs0450 getQuotzAggFnd() {
        return quotzAggFnd;
    }

    public QuotzFndUnitLccs0450 getQuotzFndUnit() {
        return quotzFndUnit;
    }

    public RichDisFndLccs0450 getRichDisFnd() {
        return richDisFnd;
    }

    public RichInvstFnd getRichInvstFnd() {
        return richInvstFnd;
    }

    public TrchDiGarIvvs0216 getTrchDiGar() {
        return trchDiGar;
    }

    public ValAst getValAst() {
        return valAst;
    }

    public Variabili getVariabili() {
        return variabili;
    }

    public WgrzTabGar getWgarTabGar(int idx) {
        return wgarTabGar[idx - 1];
    }

    public WsTpTrch getWsTpTrch() {
        return wsTpTrch;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IN_RCODE = 2;
        public static final int PGM_LDBS0130 = 8;
        public static final int PGM_LDBS4910 = 8;
        public static final int PGM_LDBS2080 = 8;
        public static final int PGM_IDBSRIF0 = 8;
        public static final int PGM_IDBSRDF0 = 8;
        public static final int PGM_IDBSL410 = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
