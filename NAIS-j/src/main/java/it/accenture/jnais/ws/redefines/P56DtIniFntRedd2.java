package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-DT-INI-FNT-REDD-2<br>
 * Variable: P56-DT-INI-FNT-REDD-2 from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56DtIniFntRedd2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56DtIniFntRedd2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_DT_INI_FNT_REDD2;
    }

    public void setP56DtIniFntRedd2(int p56DtIniFntRedd2) {
        writeIntAsPacked(Pos.P56_DT_INI_FNT_REDD2, p56DtIniFntRedd2, Len.Int.P56_DT_INI_FNT_REDD2);
    }

    public void setP56DtIniFntRedd2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_DT_INI_FNT_REDD2, Pos.P56_DT_INI_FNT_REDD2);
    }

    /**Original name: P56-DT-INI-FNT-REDD-2<br>*/
    public int getP56DtIniFntRedd2() {
        return readPackedAsInt(Pos.P56_DT_INI_FNT_REDD2, Len.Int.P56_DT_INI_FNT_REDD2);
    }

    public byte[] getP56DtIniFntRedd2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_DT_INI_FNT_REDD2, Pos.P56_DT_INI_FNT_REDD2);
        return buffer;
    }

    public void setP56DtIniFntRedd2Null(String p56DtIniFntRedd2Null) {
        writeString(Pos.P56_DT_INI_FNT_REDD2_NULL, p56DtIniFntRedd2Null, Len.P56_DT_INI_FNT_REDD2_NULL);
    }

    /**Original name: P56-DT-INI-FNT-REDD-2-NULL<br>*/
    public String getP56DtIniFntRedd2Null() {
        return readString(Pos.P56_DT_INI_FNT_REDD2_NULL, Len.P56_DT_INI_FNT_REDD2_NULL);
    }

    public String getP56DtIniFntRedd2NullFormatted() {
        return Functions.padBlanks(getP56DtIniFntRedd2Null(), Len.P56_DT_INI_FNT_REDD2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_DT_INI_FNT_REDD2 = 1;
        public static final int P56_DT_INI_FNT_REDD2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_DT_INI_FNT_REDD2 = 5;
        public static final int P56_DT_INI_FNT_REDD2_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_DT_INI_FNT_REDD2 = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
