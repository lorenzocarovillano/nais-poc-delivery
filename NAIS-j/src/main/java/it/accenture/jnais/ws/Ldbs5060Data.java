package it.accenture.jnais.ws;

import it.accenture.jnais.copy.DettTitContDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndParamMovi;
import it.accenture.jnais.copy.Ldbv5061;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS5060<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs5060Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-PARAM-MOVI
    private IndParamMovi indParamMovi = new IndParamMovi();
    //Original name: PARAM-MOVI-DB
    private DettTitContDb paramMoviDb = new DettTitContDb();
    //Original name: LDBV5061
    private Ldbv5061 ldbv5061 = new Ldbv5061();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndParamMovi getIndParamMovi() {
        return indParamMovi;
    }

    public Ldbv5061 getLdbv5061() {
        return ldbv5061;
    }

    public DettTitContDb getParamMoviDb() {
        return paramMoviDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
