package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-MM-PREAMM<br>
 * Variable: WP67-MM-PREAMM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67MmPreamm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67MmPreamm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_MM_PREAMM;
    }

    public void setWp67MmPreamm(int wp67MmPreamm) {
        writeIntAsPacked(Pos.WP67_MM_PREAMM, wp67MmPreamm, Len.Int.WP67_MM_PREAMM);
    }

    public void setWp67MmPreammFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_MM_PREAMM, Pos.WP67_MM_PREAMM);
    }

    /**Original name: WP67-MM-PREAMM<br>*/
    public int getWp67MmPreamm() {
        return readPackedAsInt(Pos.WP67_MM_PREAMM, Len.Int.WP67_MM_PREAMM);
    }

    public byte[] getWp67MmPreammAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_MM_PREAMM, Pos.WP67_MM_PREAMM);
        return buffer;
    }

    public void setWp67MmPreammNull(String wp67MmPreammNull) {
        writeString(Pos.WP67_MM_PREAMM_NULL, wp67MmPreammNull, Len.WP67_MM_PREAMM_NULL);
    }

    /**Original name: WP67-MM-PREAMM-NULL<br>*/
    public String getWp67MmPreammNull() {
        return readString(Pos.WP67_MM_PREAMM_NULL, Len.WP67_MM_PREAMM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_MM_PREAMM = 1;
        public static final int WP67_MM_PREAMM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_MM_PREAMM = 3;
        public static final int WP67_MM_PREAMM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_MM_PREAMM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
