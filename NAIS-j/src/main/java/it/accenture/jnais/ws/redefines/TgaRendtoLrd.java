package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-RENDTO-LRD<br>
 * Variable: TGA-RENDTO-LRD from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaRendtoLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaRendtoLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_RENDTO_LRD;
    }

    public void setTgaRendtoLrd(AfDecimal tgaRendtoLrd) {
        writeDecimalAsPacked(Pos.TGA_RENDTO_LRD, tgaRendtoLrd.copy());
    }

    public void setTgaRendtoLrdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_RENDTO_LRD, Pos.TGA_RENDTO_LRD);
    }

    /**Original name: TGA-RENDTO-LRD<br>*/
    public AfDecimal getTgaRendtoLrd() {
        return readPackedAsDecimal(Pos.TGA_RENDTO_LRD, Len.Int.TGA_RENDTO_LRD, Len.Fract.TGA_RENDTO_LRD);
    }

    public byte[] getTgaRendtoLrdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_RENDTO_LRD, Pos.TGA_RENDTO_LRD);
        return buffer;
    }

    public void setTgaRendtoLrdNull(String tgaRendtoLrdNull) {
        writeString(Pos.TGA_RENDTO_LRD_NULL, tgaRendtoLrdNull, Len.TGA_RENDTO_LRD_NULL);
    }

    /**Original name: TGA-RENDTO-LRD-NULL<br>*/
    public String getTgaRendtoLrdNull() {
        return readString(Pos.TGA_RENDTO_LRD_NULL, Len.TGA_RENDTO_LRD_NULL);
    }

    public String getTgaRendtoLrdNullFormatted() {
        return Functions.padBlanks(getTgaRendtoLrdNull(), Len.TGA_RENDTO_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_RENDTO_LRD = 1;
        public static final int TGA_RENDTO_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_RENDTO_LRD = 8;
        public static final int TGA_RENDTO_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_RENDTO_LRD = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_RENDTO_LRD = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
