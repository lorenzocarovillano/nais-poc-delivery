package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PRSTZ-INI-STAB<br>
 * Variable: L3421-PRSTZ-INI-STAB from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PrstzIniStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PrstzIniStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PRSTZ_INI_STAB;
    }

    public void setL3421PrstzIniStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PRSTZ_INI_STAB, Pos.L3421_PRSTZ_INI_STAB);
    }

    /**Original name: L3421-PRSTZ-INI-STAB<br>*/
    public AfDecimal getL3421PrstzIniStab() {
        return readPackedAsDecimal(Pos.L3421_PRSTZ_INI_STAB, Len.Int.L3421_PRSTZ_INI_STAB, Len.Fract.L3421_PRSTZ_INI_STAB);
    }

    public byte[] getL3421PrstzIniStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PRSTZ_INI_STAB, Pos.L3421_PRSTZ_INI_STAB);
        return buffer;
    }

    /**Original name: L3421-PRSTZ-INI-STAB-NULL<br>*/
    public String getL3421PrstzIniStabNull() {
        return readString(Pos.L3421_PRSTZ_INI_STAB_NULL, Len.L3421_PRSTZ_INI_STAB_NULL);
    }

    public String getL3421PrstzIniStabNullFormatted() {
        return Functions.padBlanks(getL3421PrstzIniStabNull(), Len.L3421_PRSTZ_INI_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PRSTZ_INI_STAB = 1;
        public static final int L3421_PRSTZ_INI_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PRSTZ_INI_STAB = 8;
        public static final int L3421_PRSTZ_INI_STAB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PRSTZ_INI_STAB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PRSTZ_INI_STAB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
