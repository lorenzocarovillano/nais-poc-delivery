package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-DUR-GG<br>
 * Variable: WGRZ-DUR-GG from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzDurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzDurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_DUR_GG;
    }

    public void setWgrzDurGg(int wgrzDurGg) {
        writeIntAsPacked(Pos.WGRZ_DUR_GG, wgrzDurGg, Len.Int.WGRZ_DUR_GG);
    }

    public void setWgrzDurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_DUR_GG, Pos.WGRZ_DUR_GG);
    }

    /**Original name: WGRZ-DUR-GG<br>*/
    public int getWgrzDurGg() {
        return readPackedAsInt(Pos.WGRZ_DUR_GG, Len.Int.WGRZ_DUR_GG);
    }

    public byte[] getWgrzDurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_DUR_GG, Pos.WGRZ_DUR_GG);
        return buffer;
    }

    public void initWgrzDurGgSpaces() {
        fill(Pos.WGRZ_DUR_GG, Len.WGRZ_DUR_GG, Types.SPACE_CHAR);
    }

    public void setWgrzDurGgNull(String wgrzDurGgNull) {
        writeString(Pos.WGRZ_DUR_GG_NULL, wgrzDurGgNull, Len.WGRZ_DUR_GG_NULL);
    }

    /**Original name: WGRZ-DUR-GG-NULL<br>*/
    public String getWgrzDurGgNull() {
        return readString(Pos.WGRZ_DUR_GG_NULL, Len.WGRZ_DUR_GG_NULL);
    }

    public String getWgrzDurGgNullFormatted() {
        return Functions.padBlanks(getWgrzDurGgNull(), Len.WGRZ_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_DUR_GG = 1;
        public static final int WGRZ_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_DUR_GG = 3;
        public static final int WGRZ_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
