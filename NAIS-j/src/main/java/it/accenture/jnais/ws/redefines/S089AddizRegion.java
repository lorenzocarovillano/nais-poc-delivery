package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-ADDIZ-REGION<br>
 * Variable: S089-ADDIZ-REGION from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089AddizRegion extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089AddizRegion() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_ADDIZ_REGION;
    }

    public void setWlquAddizRegion(AfDecimal wlquAddizRegion) {
        writeDecimalAsPacked(Pos.S089_ADDIZ_REGION, wlquAddizRegion.copy());
    }

    public void setWlquAddizRegionFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_ADDIZ_REGION, Pos.S089_ADDIZ_REGION);
    }

    /**Original name: WLQU-ADDIZ-REGION<br>*/
    public AfDecimal getWlquAddizRegion() {
        return readPackedAsDecimal(Pos.S089_ADDIZ_REGION, Len.Int.WLQU_ADDIZ_REGION, Len.Fract.WLQU_ADDIZ_REGION);
    }

    public byte[] getWlquAddizRegionAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_ADDIZ_REGION, Pos.S089_ADDIZ_REGION);
        return buffer;
    }

    public void initWlquAddizRegionSpaces() {
        fill(Pos.S089_ADDIZ_REGION, Len.S089_ADDIZ_REGION, Types.SPACE_CHAR);
    }

    public void setWlquAddizRegionNull(String wlquAddizRegionNull) {
        writeString(Pos.S089_ADDIZ_REGION_NULL, wlquAddizRegionNull, Len.WLQU_ADDIZ_REGION_NULL);
    }

    /**Original name: WLQU-ADDIZ-REGION-NULL<br>*/
    public String getWlquAddizRegionNull() {
        return readString(Pos.S089_ADDIZ_REGION_NULL, Len.WLQU_ADDIZ_REGION_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_ADDIZ_REGION = 1;
        public static final int S089_ADDIZ_REGION_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_ADDIZ_REGION = 8;
        public static final int WLQU_ADDIZ_REGION_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_ADDIZ_REGION = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_ADDIZ_REGION = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
