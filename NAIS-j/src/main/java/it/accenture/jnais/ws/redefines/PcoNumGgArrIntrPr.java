package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-NUM-GG-ARR-INTR-PR<br>
 * Variable: PCO-NUM-GG-ARR-INTR-PR from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoNumGgArrIntrPr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoNumGgArrIntrPr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_NUM_GG_ARR_INTR_PR;
    }

    public void setPcoNumGgArrIntrPr(int pcoNumGgArrIntrPr) {
        writeIntAsPacked(Pos.PCO_NUM_GG_ARR_INTR_PR, pcoNumGgArrIntrPr, Len.Int.PCO_NUM_GG_ARR_INTR_PR);
    }

    public void setPcoNumGgArrIntrPrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_NUM_GG_ARR_INTR_PR, Pos.PCO_NUM_GG_ARR_INTR_PR);
    }

    /**Original name: PCO-NUM-GG-ARR-INTR-PR<br>*/
    public int getPcoNumGgArrIntrPr() {
        return readPackedAsInt(Pos.PCO_NUM_GG_ARR_INTR_PR, Len.Int.PCO_NUM_GG_ARR_INTR_PR);
    }

    public byte[] getPcoNumGgArrIntrPrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_NUM_GG_ARR_INTR_PR, Pos.PCO_NUM_GG_ARR_INTR_PR);
        return buffer;
    }

    public void initPcoNumGgArrIntrPrHighValues() {
        fill(Pos.PCO_NUM_GG_ARR_INTR_PR, Len.PCO_NUM_GG_ARR_INTR_PR, Types.HIGH_CHAR_VAL);
    }

    public void setPcoNumGgArrIntrPrNull(String pcoNumGgArrIntrPrNull) {
        writeString(Pos.PCO_NUM_GG_ARR_INTR_PR_NULL, pcoNumGgArrIntrPrNull, Len.PCO_NUM_GG_ARR_INTR_PR_NULL);
    }

    /**Original name: PCO-NUM-GG-ARR-INTR-PR-NULL<br>*/
    public String getPcoNumGgArrIntrPrNull() {
        return readString(Pos.PCO_NUM_GG_ARR_INTR_PR_NULL, Len.PCO_NUM_GG_ARR_INTR_PR_NULL);
    }

    public String getPcoNumGgArrIntrPrNullFormatted() {
        return Functions.padBlanks(getPcoNumGgArrIntrPrNull(), Len.PCO_NUM_GG_ARR_INTR_PR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_NUM_GG_ARR_INTR_PR = 1;
        public static final int PCO_NUM_GG_ARR_INTR_PR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_NUM_GG_ARR_INTR_PR = 3;
        public static final int PCO_NUM_GG_ARR_INTR_PR_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_NUM_GG_ARR_INTR_PR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
