package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPB-VIS-ANTIC<br>
 * Variable: DFA-IMPB-VIS-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpbVisAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpbVisAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPB_VIS_ANTIC;
    }

    public void setDfaImpbVisAntic(AfDecimal dfaImpbVisAntic) {
        writeDecimalAsPacked(Pos.DFA_IMPB_VIS_ANTIC, dfaImpbVisAntic.copy());
    }

    public void setDfaImpbVisAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPB_VIS_ANTIC, Pos.DFA_IMPB_VIS_ANTIC);
    }

    /**Original name: DFA-IMPB-VIS-ANTIC<br>*/
    public AfDecimal getDfaImpbVisAntic() {
        return readPackedAsDecimal(Pos.DFA_IMPB_VIS_ANTIC, Len.Int.DFA_IMPB_VIS_ANTIC, Len.Fract.DFA_IMPB_VIS_ANTIC);
    }

    public byte[] getDfaImpbVisAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPB_VIS_ANTIC, Pos.DFA_IMPB_VIS_ANTIC);
        return buffer;
    }

    public void setDfaImpbVisAnticNull(String dfaImpbVisAnticNull) {
        writeString(Pos.DFA_IMPB_VIS_ANTIC_NULL, dfaImpbVisAnticNull, Len.DFA_IMPB_VIS_ANTIC_NULL);
    }

    /**Original name: DFA-IMPB-VIS-ANTIC-NULL<br>*/
    public String getDfaImpbVisAnticNull() {
        return readString(Pos.DFA_IMPB_VIS_ANTIC_NULL, Len.DFA_IMPB_VIS_ANTIC_NULL);
    }

    public String getDfaImpbVisAnticNullFormatted() {
        return Functions.padBlanks(getDfaImpbVisAnticNull(), Len.DFA_IMPB_VIS_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_VIS_ANTIC = 1;
        public static final int DFA_IMPB_VIS_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_VIS_ANTIC = 8;
        public static final int DFA_IMPB_VIS_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_VIS_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_VIS_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
