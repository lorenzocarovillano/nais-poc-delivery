package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-SOPR-PROF<br>
 * Variable: WTGA-IMP-SOPR-PROF from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_SOPR_PROF;
    }

    public void setWtgaImpSoprProf(AfDecimal wtgaImpSoprProf) {
        writeDecimalAsPacked(Pos.WTGA_IMP_SOPR_PROF, wtgaImpSoprProf.copy());
    }

    public void setWtgaImpSoprProfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_SOPR_PROF, Pos.WTGA_IMP_SOPR_PROF);
    }

    /**Original name: WTGA-IMP-SOPR-PROF<br>*/
    public AfDecimal getWtgaImpSoprProf() {
        return readPackedAsDecimal(Pos.WTGA_IMP_SOPR_PROF, Len.Int.WTGA_IMP_SOPR_PROF, Len.Fract.WTGA_IMP_SOPR_PROF);
    }

    public byte[] getWtgaImpSoprProfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_SOPR_PROF, Pos.WTGA_IMP_SOPR_PROF);
        return buffer;
    }

    public void initWtgaImpSoprProfSpaces() {
        fill(Pos.WTGA_IMP_SOPR_PROF, Len.WTGA_IMP_SOPR_PROF, Types.SPACE_CHAR);
    }

    public void setWtgaImpSoprProfNull(String wtgaImpSoprProfNull) {
        writeString(Pos.WTGA_IMP_SOPR_PROF_NULL, wtgaImpSoprProfNull, Len.WTGA_IMP_SOPR_PROF_NULL);
    }

    /**Original name: WTGA-IMP-SOPR-PROF-NULL<br>*/
    public String getWtgaImpSoprProfNull() {
        return readString(Pos.WTGA_IMP_SOPR_PROF_NULL, Len.WTGA_IMP_SOPR_PROF_NULL);
    }

    public String getWtgaImpSoprProfNullFormatted() {
        return Functions.padBlanks(getWtgaImpSoprProfNull(), Len.WTGA_IMP_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_SOPR_PROF = 1;
        public static final int WTGA_IMP_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_SOPR_PROF = 8;
        public static final int WTGA_IMP_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
