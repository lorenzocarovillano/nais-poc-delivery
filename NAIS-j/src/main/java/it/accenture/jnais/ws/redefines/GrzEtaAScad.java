package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-ETA-A-SCAD<br>
 * Variable: GRZ-ETA-A-SCAD from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzEtaAScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzEtaAScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_ETA_A_SCAD;
    }

    public void setGrzEtaAScad(int grzEtaAScad) {
        writeIntAsPacked(Pos.GRZ_ETA_A_SCAD, grzEtaAScad, Len.Int.GRZ_ETA_A_SCAD);
    }

    public void setGrzEtaAScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_ETA_A_SCAD, Pos.GRZ_ETA_A_SCAD);
    }

    /**Original name: GRZ-ETA-A-SCAD<br>*/
    public int getGrzEtaAScad() {
        return readPackedAsInt(Pos.GRZ_ETA_A_SCAD, Len.Int.GRZ_ETA_A_SCAD);
    }

    public byte[] getGrzEtaAScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_ETA_A_SCAD, Pos.GRZ_ETA_A_SCAD);
        return buffer;
    }

    public void setGrzEtaAScadNull(String grzEtaAScadNull) {
        writeString(Pos.GRZ_ETA_A_SCAD_NULL, grzEtaAScadNull, Len.GRZ_ETA_A_SCAD_NULL);
    }

    /**Original name: GRZ-ETA-A-SCAD-NULL<br>*/
    public String getGrzEtaAScadNull() {
        return readString(Pos.GRZ_ETA_A_SCAD_NULL, Len.GRZ_ETA_A_SCAD_NULL);
    }

    public String getGrzEtaAScadNullFormatted() {
        return Functions.padBlanks(getGrzEtaAScadNull(), Len.GRZ_ETA_A_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_ETA_A_SCAD = 1;
        public static final int GRZ_ETA_A_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_ETA_A_SCAD = 3;
        public static final int GRZ_ETA_A_SCAD_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_ETA_A_SCAD = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
