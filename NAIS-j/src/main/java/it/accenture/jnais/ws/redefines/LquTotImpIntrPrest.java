package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMP-INTR-PREST<br>
 * Variable: LQU-TOT-IMP-INTR-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMP_INTR_PREST;
    }

    public void setLquTotImpIntrPrest(AfDecimal lquTotImpIntrPrest) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMP_INTR_PREST, lquTotImpIntrPrest.copy());
    }

    public void setLquTotImpIntrPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMP_INTR_PREST, Pos.LQU_TOT_IMP_INTR_PREST);
    }

    /**Original name: LQU-TOT-IMP-INTR-PREST<br>*/
    public AfDecimal getLquTotImpIntrPrest() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMP_INTR_PREST, Len.Int.LQU_TOT_IMP_INTR_PREST, Len.Fract.LQU_TOT_IMP_INTR_PREST);
    }

    public byte[] getLquTotImpIntrPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMP_INTR_PREST, Pos.LQU_TOT_IMP_INTR_PREST);
        return buffer;
    }

    public void setLquTotImpIntrPrestNull(String lquTotImpIntrPrestNull) {
        writeString(Pos.LQU_TOT_IMP_INTR_PREST_NULL, lquTotImpIntrPrestNull, Len.LQU_TOT_IMP_INTR_PREST_NULL);
    }

    /**Original name: LQU-TOT-IMP-INTR-PREST-NULL<br>*/
    public String getLquTotImpIntrPrestNull() {
        return readString(Pos.LQU_TOT_IMP_INTR_PREST_NULL, Len.LQU_TOT_IMP_INTR_PREST_NULL);
    }

    public String getLquTotImpIntrPrestNullFormatted() {
        return Functions.padBlanks(getLquTotImpIntrPrestNull(), Len.LQU_TOT_IMP_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_INTR_PREST = 1;
        public static final int LQU_TOT_IMP_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_INTR_PREST = 8;
        public static final int LQU_TOT_IMP_INTR_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_INTR_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
