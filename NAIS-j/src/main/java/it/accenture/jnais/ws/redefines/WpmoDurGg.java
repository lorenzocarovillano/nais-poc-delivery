package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-DUR-GG<br>
 * Variable: WPMO-DUR-GG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoDurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoDurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_DUR_GG;
    }

    public void setWpmoDurGg(int wpmoDurGg) {
        writeIntAsPacked(Pos.WPMO_DUR_GG, wpmoDurGg, Len.Int.WPMO_DUR_GG);
    }

    public void setWpmoDurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_DUR_GG, Pos.WPMO_DUR_GG);
    }

    /**Original name: WPMO-DUR-GG<br>*/
    public int getWpmoDurGg() {
        return readPackedAsInt(Pos.WPMO_DUR_GG, Len.Int.WPMO_DUR_GG);
    }

    public byte[] getWpmoDurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_DUR_GG, Pos.WPMO_DUR_GG);
        return buffer;
    }

    public void initWpmoDurGgSpaces() {
        fill(Pos.WPMO_DUR_GG, Len.WPMO_DUR_GG, Types.SPACE_CHAR);
    }

    public void setWpmoDurGgNull(String wpmoDurGgNull) {
        writeString(Pos.WPMO_DUR_GG_NULL, wpmoDurGgNull, Len.WPMO_DUR_GG_NULL);
    }

    /**Original name: WPMO-DUR-GG-NULL<br>*/
    public String getWpmoDurGgNull() {
        return readString(Pos.WPMO_DUR_GG_NULL, Len.WPMO_DUR_GG_NULL);
    }

    public String getWpmoDurGgNullFormatted() {
        return Functions.padBlanks(getWpmoDurGgNull(), Len.WPMO_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_DUR_GG = 1;
        public static final int WPMO_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_DUR_GG = 3;
        public static final int WPMO_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
