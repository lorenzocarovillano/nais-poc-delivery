package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPVT-PROV-1AA-ACQ<br>
 * Variable: WPVT-PROV-1AA-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtProv1aaAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtProv1aaAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_PROV1AA_ACQ;
    }

    public void setWpvtProv1aaAcq(AfDecimal wpvtProv1aaAcq) {
        writeDecimalAsPacked(Pos.WPVT_PROV1AA_ACQ, wpvtProv1aaAcq.copy());
    }

    public void setWpvtProv1aaAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_PROV1AA_ACQ, Pos.WPVT_PROV1AA_ACQ);
    }

    /**Original name: WPVT-PROV-1AA-ACQ<br>*/
    public AfDecimal getWpvtProv1aaAcq() {
        return readPackedAsDecimal(Pos.WPVT_PROV1AA_ACQ, Len.Int.WPVT_PROV1AA_ACQ, Len.Fract.WPVT_PROV1AA_ACQ);
    }

    public byte[] getWpvtProv1aaAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_PROV1AA_ACQ, Pos.WPVT_PROV1AA_ACQ);
        return buffer;
    }

    public void initWpvtProv1aaAcqSpaces() {
        fill(Pos.WPVT_PROV1AA_ACQ, Len.WPVT_PROV1AA_ACQ, Types.SPACE_CHAR);
    }

    public void setWpvtProv1aaAcqNull(String wpvtProv1aaAcqNull) {
        writeString(Pos.WPVT_PROV1AA_ACQ_NULL, wpvtProv1aaAcqNull, Len.WPVT_PROV1AA_ACQ_NULL);
    }

    /**Original name: WPVT-PROV-1AA-ACQ-NULL<br>*/
    public String getWpvtProv1aaAcqNull() {
        return readString(Pos.WPVT_PROV1AA_ACQ_NULL, Len.WPVT_PROV1AA_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_PROV1AA_ACQ = 1;
        public static final int WPVT_PROV1AA_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_PROV1AA_ACQ = 8;
        public static final int WPVT_PROV1AA_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPVT_PROV1AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_PROV1AA_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
