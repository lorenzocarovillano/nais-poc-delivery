package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WBEL-DT-VLT<br>
 * Variable: WBEL-DT-VLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelDtVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelDtVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_DT_VLT;
    }

    public void setWbelDtVlt(int wbelDtVlt) {
        writeIntAsPacked(Pos.WBEL_DT_VLT, wbelDtVlt, Len.Int.WBEL_DT_VLT);
    }

    public void setWbelDtVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_DT_VLT, Pos.WBEL_DT_VLT);
    }

    /**Original name: WBEL-DT-VLT<br>*/
    public int getWbelDtVlt() {
        return readPackedAsInt(Pos.WBEL_DT_VLT, Len.Int.WBEL_DT_VLT);
    }

    public byte[] getWbelDtVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_DT_VLT, Pos.WBEL_DT_VLT);
        return buffer;
    }

    public void initWbelDtVltSpaces() {
        fill(Pos.WBEL_DT_VLT, Len.WBEL_DT_VLT, Types.SPACE_CHAR);
    }

    public void setWbelDtVltNull(String wbelDtVltNull) {
        writeString(Pos.WBEL_DT_VLT_NULL, wbelDtVltNull, Len.WBEL_DT_VLT_NULL);
    }

    /**Original name: WBEL-DT-VLT-NULL<br>*/
    public String getWbelDtVltNull() {
        return readString(Pos.WBEL_DT_VLT_NULL, Len.WBEL_DT_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_DT_VLT = 1;
        public static final int WBEL_DT_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_DT_VLT = 5;
        public static final int WBEL_DT_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_DT_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
