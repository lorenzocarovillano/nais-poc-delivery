package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Ispc0040CodiceAdesione;

/**Original name: ISPC0040-TIPO-ADESIONE<br>
 * Variables: ISPC0040-TIPO-ADESIONE from copybook ISPC0040<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0040TipoAdesione {

    //==== PROPERTIES ====
    //Original name: ISPC0040-CODICE-ADESIONE
    private Ispc0040CodiceAdesione codiceAdesione = new Ispc0040CodiceAdesione();
    //Original name: ISPC0040-DESCR-ADESIONE
    private String descrAdesione = DefaultValues.stringVal(Len.DESCR_ADESIONE);

    //==== METHODS ====
    public void setTipoAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        codiceAdesione.setCodiceAdesione(MarshalByte.readString(buffer, position, Ispc0040CodiceAdesione.Len.CODICE_ADESIONE));
        position += Ispc0040CodiceAdesione.Len.CODICE_ADESIONE;
        descrAdesione = MarshalByte.readString(buffer, position, Len.DESCR_ADESIONE);
    }

    public byte[] getTipoAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codiceAdesione.getCodiceAdesione(), Ispc0040CodiceAdesione.Len.CODICE_ADESIONE);
        position += Ispc0040CodiceAdesione.Len.CODICE_ADESIONE;
        MarshalByte.writeString(buffer, position, descrAdesione, Len.DESCR_ADESIONE);
        return buffer;
    }

    public void initTipoAdesioneSpaces() {
        codiceAdesione.setCodiceAdesione("");
        descrAdesione = "";
    }

    public void setDescrAdesione(String descrAdesione) {
        this.descrAdesione = Functions.subString(descrAdesione, Len.DESCR_ADESIONE);
    }

    public String getDescrAdesione() {
        return this.descrAdesione;
    }

    public Ispc0040CodiceAdesione getCodiceAdesione() {
        return codiceAdesione;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESCR_ADESIONE = 30;
        public static final int TIPO_ADESIONE = Ispc0040CodiceAdesione.Len.CODICE_ADESIONE + DESCR_ADESIONE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
