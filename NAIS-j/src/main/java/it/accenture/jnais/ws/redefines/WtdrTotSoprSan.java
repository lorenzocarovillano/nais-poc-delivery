package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-SOPR-SAN<br>
 * Variable: WTDR-TOT-SOPR-SAN from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_SOPR_SAN;
    }

    public void setWtdrTotSoprSan(AfDecimal wtdrTotSoprSan) {
        writeDecimalAsPacked(Pos.WTDR_TOT_SOPR_SAN, wtdrTotSoprSan.copy());
    }

    /**Original name: WTDR-TOT-SOPR-SAN<br>*/
    public AfDecimal getWtdrTotSoprSan() {
        return readPackedAsDecimal(Pos.WTDR_TOT_SOPR_SAN, Len.Int.WTDR_TOT_SOPR_SAN, Len.Fract.WTDR_TOT_SOPR_SAN);
    }

    public void setWtdrTotSoprSanNull(String wtdrTotSoprSanNull) {
        writeString(Pos.WTDR_TOT_SOPR_SAN_NULL, wtdrTotSoprSanNull, Len.WTDR_TOT_SOPR_SAN_NULL);
    }

    /**Original name: WTDR-TOT-SOPR-SAN-NULL<br>*/
    public String getWtdrTotSoprSanNull() {
        return readString(Pos.WTDR_TOT_SOPR_SAN_NULL, Len.WTDR_TOT_SOPR_SAN_NULL);
    }

    public String getWtdrTotSoprSanNullFormatted() {
        return Functions.padBlanks(getWtdrTotSoprSanNull(), Len.WTDR_TOT_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SOPR_SAN = 1;
        public static final int WTDR_TOT_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SOPR_SAN = 8;
        public static final int WTDR_TOT_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
