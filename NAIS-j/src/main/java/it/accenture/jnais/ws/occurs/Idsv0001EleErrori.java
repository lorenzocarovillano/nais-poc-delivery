package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IDSV0001-ELE-ERRORI<br>
 * Variables: IDSV0001-ELE-ERRORI from copybook IDSV0001<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idsv0001EleErrori {

    //==== PROPERTIES ====
    //Original name: IDSV0001-DESC-ERRORE
    private String descErrore = DefaultValues.stringVal(Len.DESC_ERRORE);
    //Original name: IDSV0001-COD-ERRORE
    private String codErrore = DefaultValues.stringVal(Len.COD_ERRORE);
    //Original name: IDSV0001-LIV-GRAVITA-BE
    private String livGravitaBe = DefaultValues.stringVal(Len.LIV_GRAVITA_BE);
    //Original name: IDSV0001-TIPO-TRATT-FE
    private char tipoTrattFe = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setEleErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        descErrore = MarshalByte.readString(buffer, position, Len.DESC_ERRORE);
        position += Len.DESC_ERRORE;
        codErrore = MarshalByte.readFixedString(buffer, position, Len.COD_ERRORE);
        position += Len.COD_ERRORE;
        livGravitaBe = MarshalByte.readFixedString(buffer, position, Len.LIV_GRAVITA_BE);
        position += Len.LIV_GRAVITA_BE;
        tipoTrattFe = MarshalByte.readChar(buffer, position);
    }

    public byte[] getEleErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, descErrore, Len.DESC_ERRORE);
        position += Len.DESC_ERRORE;
        MarshalByte.writeString(buffer, position, codErrore, Len.COD_ERRORE);
        position += Len.COD_ERRORE;
        MarshalByte.writeString(buffer, position, livGravitaBe, Len.LIV_GRAVITA_BE);
        position += Len.LIV_GRAVITA_BE;
        MarshalByte.writeChar(buffer, position, tipoTrattFe);
        return buffer;
    }

    public void initEleErroriSpaces() {
        descErrore = "";
        codErrore = "";
        livGravitaBe = "";
        tipoTrattFe = Types.SPACE_CHAR;
    }

    public void setDescErrore(String descErrore) {
        this.descErrore = Functions.subString(descErrore, Len.DESC_ERRORE);
    }

    public String getDescErrore() {
        return this.descErrore;
    }

    public String getIdsv0001DescErroreFormatted() {
        return Functions.padBlanks(getDescErrore(), Len.DESC_ERRORE);
    }

    public void setIdsv0001CodErrore(int idsv0001CodErrore) {
        this.codErrore = NumericDisplay.asString(idsv0001CodErrore, Len.COD_ERRORE);
    }

    public void setIdsv0001CodErroreFormatted(String idsv0001CodErrore) {
        this.codErrore = Trunc.toUnsignedNumeric(idsv0001CodErrore, Len.COD_ERRORE);
    }

    public int getIdsv0001CodErrore() {
        return NumericDisplay.asInt(this.codErrore);
    }

    public void setIdsv0001LivGravitaBe(short idsv0001LivGravitaBe) {
        this.livGravitaBe = NumericDisplay.asString(idsv0001LivGravitaBe, Len.LIV_GRAVITA_BE);
    }

    public void setIdsv0001LivGravitaBeFormatted(String idsv0001LivGravitaBe) {
        this.livGravitaBe = Trunc.toUnsignedNumeric(idsv0001LivGravitaBe, Len.LIV_GRAVITA_BE);
    }

    public short getIdsv0001LivGravitaBe() {
        return NumericDisplay.asShort(this.livGravitaBe);
    }

    public String getIdsv0001LivGravitaBeFormatted() {
        return this.livGravitaBe;
    }

    public void setTipoTrattFe(char tipoTrattFe) {
        this.tipoTrattFe = tipoTrattFe;
    }

    public char getTipoTrattFe() {
        return this.tipoTrattFe;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESC_ERRORE = 200;
        public static final int COD_ERRORE = 6;
        public static final int LIV_GRAVITA_BE = 1;
        public static final int TIPO_TRATT_FE = 1;
        public static final int ELE_ERRORI = DESC_ERRORE + COD_ERRORE + LIV_GRAVITA_BE + TIPO_TRATT_FE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
