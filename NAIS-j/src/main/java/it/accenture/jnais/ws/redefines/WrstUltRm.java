package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-ULT-RM<br>
 * Variable: WRST-ULT-RM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstUltRm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstUltRm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_ULT_RM;
    }

    public void setWrstUltRm(AfDecimal wrstUltRm) {
        writeDecimalAsPacked(Pos.WRST_ULT_RM, wrstUltRm.copy());
    }

    public void setWrstUltRmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_ULT_RM, Pos.WRST_ULT_RM);
    }

    /**Original name: WRST-ULT-RM<br>*/
    public AfDecimal getWrstUltRm() {
        return readPackedAsDecimal(Pos.WRST_ULT_RM, Len.Int.WRST_ULT_RM, Len.Fract.WRST_ULT_RM);
    }

    public byte[] getWrstUltRmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_ULT_RM, Pos.WRST_ULT_RM);
        return buffer;
    }

    public void initWrstUltRmSpaces() {
        fill(Pos.WRST_ULT_RM, Len.WRST_ULT_RM, Types.SPACE_CHAR);
    }

    public void setWrstUltRmNull(String wrstUltRmNull) {
        writeString(Pos.WRST_ULT_RM_NULL, wrstUltRmNull, Len.WRST_ULT_RM_NULL);
    }

    /**Original name: WRST-ULT-RM-NULL<br>*/
    public String getWrstUltRmNull() {
        return readString(Pos.WRST_ULT_RM_NULL, Len.WRST_ULT_RM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_ULT_RM = 1;
        public static final int WRST_ULT_RM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_ULT_RM = 8;
        public static final int WRST_ULT_RM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_ULT_RM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_ULT_RM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
