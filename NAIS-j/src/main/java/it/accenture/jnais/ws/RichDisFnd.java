package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.RdfCommisGest;
import it.accenture.jnais.ws.redefines.RdfCosRunAssvaIdc;
import it.accenture.jnais.ws.redefines.RdfDtCambioVlt;
import it.accenture.jnais.ws.redefines.RdfDtDis;
import it.accenture.jnais.ws.redefines.RdfDtDisCalc;
import it.accenture.jnais.ws.redefines.RdfIdMoviChiu;
import it.accenture.jnais.ws.redefines.RdfImpMovto;
import it.accenture.jnais.ws.redefines.RdfNumQuo;
import it.accenture.jnais.ws.redefines.RdfNumQuoCdgFnz;
import it.accenture.jnais.ws.redefines.RdfNumQuoCdgtotFnz;
import it.accenture.jnais.ws.redefines.RdfPc;

/**Original name: RICH-DIS-FND<br>
 * Variable: RICH-DIS-FND from copybook IDBVRDF1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class RichDisFnd extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: RDF-ID-RICH-DIS-FND
    private int rdfIdRichDisFnd = DefaultValues.INT_VAL;
    //Original name: RDF-ID-MOVI-FINRIO
    private int rdfIdMoviFinrio = DefaultValues.INT_VAL;
    //Original name: RDF-ID-MOVI-CRZ
    private int rdfIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: RDF-ID-MOVI-CHIU
    private RdfIdMoviChiu rdfIdMoviChiu = new RdfIdMoviChiu();
    //Original name: RDF-DT-INI-EFF
    private int rdfDtIniEff = DefaultValues.INT_VAL;
    //Original name: RDF-DT-END-EFF
    private int rdfDtEndEff = DefaultValues.INT_VAL;
    //Original name: RDF-COD-COMP-ANIA
    private int rdfCodCompAnia = DefaultValues.INT_VAL;
    //Original name: RDF-COD-FND
    private String rdfCodFnd = DefaultValues.stringVal(Len.RDF_COD_FND);
    //Original name: RDF-NUM-QUO
    private RdfNumQuo rdfNumQuo = new RdfNumQuo();
    //Original name: RDF-PC
    private RdfPc rdfPc = new RdfPc();
    //Original name: RDF-IMP-MOVTO
    private RdfImpMovto rdfImpMovto = new RdfImpMovto();
    //Original name: RDF-DT-DIS
    private RdfDtDis rdfDtDis = new RdfDtDis();
    //Original name: RDF-COD-TARI
    private String rdfCodTari = DefaultValues.stringVal(Len.RDF_COD_TARI);
    //Original name: RDF-TP-STAT
    private String rdfTpStat = DefaultValues.stringVal(Len.RDF_TP_STAT);
    //Original name: RDF-TP-MOD-DIS
    private String rdfTpModDis = DefaultValues.stringVal(Len.RDF_TP_MOD_DIS);
    //Original name: RDF-COD-DIV
    private String rdfCodDiv = DefaultValues.stringVal(Len.RDF_COD_DIV);
    //Original name: RDF-DT-CAMBIO-VLT
    private RdfDtCambioVlt rdfDtCambioVlt = new RdfDtCambioVlt();
    //Original name: RDF-TP-FND
    private char rdfTpFnd = DefaultValues.CHAR_VAL;
    //Original name: RDF-DS-RIGA
    private long rdfDsRiga = DefaultValues.LONG_VAL;
    //Original name: RDF-DS-OPER-SQL
    private char rdfDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: RDF-DS-VER
    private int rdfDsVer = DefaultValues.INT_VAL;
    //Original name: RDF-DS-TS-INI-CPTZ
    private long rdfDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: RDF-DS-TS-END-CPTZ
    private long rdfDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: RDF-DS-UTENTE
    private String rdfDsUtente = DefaultValues.stringVal(Len.RDF_DS_UTENTE);
    //Original name: RDF-DS-STATO-ELAB
    private char rdfDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: RDF-DT-DIS-CALC
    private RdfDtDisCalc rdfDtDisCalc = new RdfDtDisCalc();
    //Original name: RDF-FL-CALC-DIS
    private char rdfFlCalcDis = DefaultValues.CHAR_VAL;
    //Original name: RDF-COMMIS-GEST
    private RdfCommisGest rdfCommisGest = new RdfCommisGest();
    //Original name: RDF-NUM-QUO-CDG-FNZ
    private RdfNumQuoCdgFnz rdfNumQuoCdgFnz = new RdfNumQuoCdgFnz();
    //Original name: RDF-NUM-QUO-CDGTOT-FNZ
    private RdfNumQuoCdgtotFnz rdfNumQuoCdgtotFnz = new RdfNumQuoCdgtotFnz();
    //Original name: RDF-COS-RUN-ASSVA-IDC
    private RdfCosRunAssvaIdc rdfCosRunAssvaIdc = new RdfCosRunAssvaIdc();
    //Original name: RDF-FL-SWM-BP2S
    private char rdfFlSwmBp2s = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RICH_DIS_FND;
    }

    @Override
    public void deserialize(byte[] buf) {
        setRichDisFndBytes(buf);
    }

    public void setRichDisFndFormatted(String data) {
        byte[] buffer = new byte[Len.RICH_DIS_FND];
        MarshalByte.writeString(buffer, 1, data, Len.RICH_DIS_FND);
        setRichDisFndBytes(buffer, 1);
    }

    public String getRichDisFndFormatted() {
        return MarshalByteExt.bufferToStr(getRichDisFndBytes());
    }

    public void setRichDisFndBytes(byte[] buffer) {
        setRichDisFndBytes(buffer, 1);
    }

    public byte[] getRichDisFndBytes() {
        byte[] buffer = new byte[Len.RICH_DIS_FND];
        return getRichDisFndBytes(buffer, 1);
    }

    public void setRichDisFndBytes(byte[] buffer, int offset) {
        int position = offset;
        rdfIdRichDisFnd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RDF_ID_RICH_DIS_FND, 0);
        position += Len.RDF_ID_RICH_DIS_FND;
        rdfIdMoviFinrio = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RDF_ID_MOVI_FINRIO, 0);
        position += Len.RDF_ID_MOVI_FINRIO;
        rdfIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RDF_ID_MOVI_CRZ, 0);
        position += Len.RDF_ID_MOVI_CRZ;
        rdfIdMoviChiu.setRdfIdMoviChiuFromBuffer(buffer, position);
        position += RdfIdMoviChiu.Len.RDF_ID_MOVI_CHIU;
        rdfDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RDF_DT_INI_EFF, 0);
        position += Len.RDF_DT_INI_EFF;
        rdfDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RDF_DT_END_EFF, 0);
        position += Len.RDF_DT_END_EFF;
        rdfCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RDF_COD_COMP_ANIA, 0);
        position += Len.RDF_COD_COMP_ANIA;
        rdfCodFnd = MarshalByte.readString(buffer, position, Len.RDF_COD_FND);
        position += Len.RDF_COD_FND;
        rdfNumQuo.setRdfNumQuoFromBuffer(buffer, position);
        position += RdfNumQuo.Len.RDF_NUM_QUO;
        rdfPc.setRdfPcFromBuffer(buffer, position);
        position += RdfPc.Len.RDF_PC;
        rdfImpMovto.setRdfImpMovtoFromBuffer(buffer, position);
        position += RdfImpMovto.Len.RDF_IMP_MOVTO;
        rdfDtDis.setRdfDtDisFromBuffer(buffer, position);
        position += RdfDtDis.Len.RDF_DT_DIS;
        rdfCodTari = MarshalByte.readString(buffer, position, Len.RDF_COD_TARI);
        position += Len.RDF_COD_TARI;
        rdfTpStat = MarshalByte.readString(buffer, position, Len.RDF_TP_STAT);
        position += Len.RDF_TP_STAT;
        rdfTpModDis = MarshalByte.readString(buffer, position, Len.RDF_TP_MOD_DIS);
        position += Len.RDF_TP_MOD_DIS;
        rdfCodDiv = MarshalByte.readString(buffer, position, Len.RDF_COD_DIV);
        position += Len.RDF_COD_DIV;
        rdfDtCambioVlt.setRdfDtCambioVltFromBuffer(buffer, position);
        position += RdfDtCambioVlt.Len.RDF_DT_CAMBIO_VLT;
        rdfTpFnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rdfDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RDF_DS_RIGA, 0);
        position += Len.RDF_DS_RIGA;
        rdfDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rdfDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RDF_DS_VER, 0);
        position += Len.RDF_DS_VER;
        rdfDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RDF_DS_TS_INI_CPTZ, 0);
        position += Len.RDF_DS_TS_INI_CPTZ;
        rdfDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RDF_DS_TS_END_CPTZ, 0);
        position += Len.RDF_DS_TS_END_CPTZ;
        rdfDsUtente = MarshalByte.readString(buffer, position, Len.RDF_DS_UTENTE);
        position += Len.RDF_DS_UTENTE;
        rdfDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rdfDtDisCalc.setRdfDtDisCalcFromBuffer(buffer, position);
        position += RdfDtDisCalc.Len.RDF_DT_DIS_CALC;
        rdfFlCalcDis = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rdfCommisGest.setRdfCommisGestFromBuffer(buffer, position);
        position += RdfCommisGest.Len.RDF_COMMIS_GEST;
        rdfNumQuoCdgFnz.setRdfNumQuoCdgFnzFromBuffer(buffer, position);
        position += RdfNumQuoCdgFnz.Len.RDF_NUM_QUO_CDG_FNZ;
        rdfNumQuoCdgtotFnz.setRdfNumQuoCdgtotFnzFromBuffer(buffer, position);
        position += RdfNumQuoCdgtotFnz.Len.RDF_NUM_QUO_CDGTOT_FNZ;
        rdfCosRunAssvaIdc.setRdfCosRunAssvaIdcFromBuffer(buffer, position);
        position += RdfCosRunAssvaIdc.Len.RDF_COS_RUN_ASSVA_IDC;
        rdfFlSwmBp2s = MarshalByte.readChar(buffer, position);
    }

    public byte[] getRichDisFndBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, rdfIdRichDisFnd, Len.Int.RDF_ID_RICH_DIS_FND, 0);
        position += Len.RDF_ID_RICH_DIS_FND;
        MarshalByte.writeIntAsPacked(buffer, position, rdfIdMoviFinrio, Len.Int.RDF_ID_MOVI_FINRIO, 0);
        position += Len.RDF_ID_MOVI_FINRIO;
        MarshalByte.writeIntAsPacked(buffer, position, rdfIdMoviCrz, Len.Int.RDF_ID_MOVI_CRZ, 0);
        position += Len.RDF_ID_MOVI_CRZ;
        rdfIdMoviChiu.getRdfIdMoviChiuAsBuffer(buffer, position);
        position += RdfIdMoviChiu.Len.RDF_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, rdfDtIniEff, Len.Int.RDF_DT_INI_EFF, 0);
        position += Len.RDF_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, rdfDtEndEff, Len.Int.RDF_DT_END_EFF, 0);
        position += Len.RDF_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, rdfCodCompAnia, Len.Int.RDF_COD_COMP_ANIA, 0);
        position += Len.RDF_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, rdfCodFnd, Len.RDF_COD_FND);
        position += Len.RDF_COD_FND;
        rdfNumQuo.getRdfNumQuoAsBuffer(buffer, position);
        position += RdfNumQuo.Len.RDF_NUM_QUO;
        rdfPc.getRdfPcAsBuffer(buffer, position);
        position += RdfPc.Len.RDF_PC;
        rdfImpMovto.getRdfImpMovtoAsBuffer(buffer, position);
        position += RdfImpMovto.Len.RDF_IMP_MOVTO;
        rdfDtDis.getRdfDtDisAsBuffer(buffer, position);
        position += RdfDtDis.Len.RDF_DT_DIS;
        MarshalByte.writeString(buffer, position, rdfCodTari, Len.RDF_COD_TARI);
        position += Len.RDF_COD_TARI;
        MarshalByte.writeString(buffer, position, rdfTpStat, Len.RDF_TP_STAT);
        position += Len.RDF_TP_STAT;
        MarshalByte.writeString(buffer, position, rdfTpModDis, Len.RDF_TP_MOD_DIS);
        position += Len.RDF_TP_MOD_DIS;
        MarshalByte.writeString(buffer, position, rdfCodDiv, Len.RDF_COD_DIV);
        position += Len.RDF_COD_DIV;
        rdfDtCambioVlt.getRdfDtCambioVltAsBuffer(buffer, position);
        position += RdfDtCambioVlt.Len.RDF_DT_CAMBIO_VLT;
        MarshalByte.writeChar(buffer, position, rdfTpFnd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, rdfDsRiga, Len.Int.RDF_DS_RIGA, 0);
        position += Len.RDF_DS_RIGA;
        MarshalByte.writeChar(buffer, position, rdfDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, rdfDsVer, Len.Int.RDF_DS_VER, 0);
        position += Len.RDF_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, rdfDsTsIniCptz, Len.Int.RDF_DS_TS_INI_CPTZ, 0);
        position += Len.RDF_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, rdfDsTsEndCptz, Len.Int.RDF_DS_TS_END_CPTZ, 0);
        position += Len.RDF_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, rdfDsUtente, Len.RDF_DS_UTENTE);
        position += Len.RDF_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, rdfDsStatoElab);
        position += Types.CHAR_SIZE;
        rdfDtDisCalc.getRdfDtDisCalcAsBuffer(buffer, position);
        position += RdfDtDisCalc.Len.RDF_DT_DIS_CALC;
        MarshalByte.writeChar(buffer, position, rdfFlCalcDis);
        position += Types.CHAR_SIZE;
        rdfCommisGest.getRdfCommisGestAsBuffer(buffer, position);
        position += RdfCommisGest.Len.RDF_COMMIS_GEST;
        rdfNumQuoCdgFnz.getRdfNumQuoCdgFnzAsBuffer(buffer, position);
        position += RdfNumQuoCdgFnz.Len.RDF_NUM_QUO_CDG_FNZ;
        rdfNumQuoCdgtotFnz.getRdfNumQuoCdgtotFnzAsBuffer(buffer, position);
        position += RdfNumQuoCdgtotFnz.Len.RDF_NUM_QUO_CDGTOT_FNZ;
        rdfCosRunAssvaIdc.getRdfCosRunAssvaIdcAsBuffer(buffer, position);
        position += RdfCosRunAssvaIdc.Len.RDF_COS_RUN_ASSVA_IDC;
        MarshalByte.writeChar(buffer, position, rdfFlSwmBp2s);
        return buffer;
    }

    public void setRdfIdRichDisFnd(int rdfIdRichDisFnd) {
        this.rdfIdRichDisFnd = rdfIdRichDisFnd;
    }

    public int getRdfIdRichDisFnd() {
        return this.rdfIdRichDisFnd;
    }

    public void setRdfIdMoviFinrio(int rdfIdMoviFinrio) {
        this.rdfIdMoviFinrio = rdfIdMoviFinrio;
    }

    public int getRdfIdMoviFinrio() {
        return this.rdfIdMoviFinrio;
    }

    public void setRdfIdMoviCrz(int rdfIdMoviCrz) {
        this.rdfIdMoviCrz = rdfIdMoviCrz;
    }

    public int getRdfIdMoviCrz() {
        return this.rdfIdMoviCrz;
    }

    public void setRdfDtIniEff(int rdfDtIniEff) {
        this.rdfDtIniEff = rdfDtIniEff;
    }

    public int getRdfDtIniEff() {
        return this.rdfDtIniEff;
    }

    public void setRdfDtEndEff(int rdfDtEndEff) {
        this.rdfDtEndEff = rdfDtEndEff;
    }

    public int getRdfDtEndEff() {
        return this.rdfDtEndEff;
    }

    public void setRdfCodCompAnia(int rdfCodCompAnia) {
        this.rdfCodCompAnia = rdfCodCompAnia;
    }

    public int getRdfCodCompAnia() {
        return this.rdfCodCompAnia;
    }

    public void setRdfCodFnd(String rdfCodFnd) {
        this.rdfCodFnd = Functions.subString(rdfCodFnd, Len.RDF_COD_FND);
    }

    public String getRdfCodFnd() {
        return this.rdfCodFnd;
    }

    public String getRdfCodFndFormatted() {
        return Functions.padBlanks(getRdfCodFnd(), Len.RDF_COD_FND);
    }

    public void setRdfCodTari(String rdfCodTari) {
        this.rdfCodTari = Functions.subString(rdfCodTari, Len.RDF_COD_TARI);
    }

    public String getRdfCodTari() {
        return this.rdfCodTari;
    }

    public String getRdfCodTariFormatted() {
        return Functions.padBlanks(getRdfCodTari(), Len.RDF_COD_TARI);
    }

    public void setRdfTpStat(String rdfTpStat) {
        this.rdfTpStat = Functions.subString(rdfTpStat, Len.RDF_TP_STAT);
    }

    public String getRdfTpStat() {
        return this.rdfTpStat;
    }

    public String getRdfTpStatFormatted() {
        return Functions.padBlanks(getRdfTpStat(), Len.RDF_TP_STAT);
    }

    public void setRdfTpModDis(String rdfTpModDis) {
        this.rdfTpModDis = Functions.subString(rdfTpModDis, Len.RDF_TP_MOD_DIS);
    }

    public String getRdfTpModDis() {
        return this.rdfTpModDis;
    }

    public String getRdfTpModDisFormatted() {
        return Functions.padBlanks(getRdfTpModDis(), Len.RDF_TP_MOD_DIS);
    }

    public void setRdfCodDiv(String rdfCodDiv) {
        this.rdfCodDiv = Functions.subString(rdfCodDiv, Len.RDF_COD_DIV);
    }

    public String getRdfCodDiv() {
        return this.rdfCodDiv;
    }

    public void setRdfTpFnd(char rdfTpFnd) {
        this.rdfTpFnd = rdfTpFnd;
    }

    public char getRdfTpFnd() {
        return this.rdfTpFnd;
    }

    public void setRdfDsRiga(long rdfDsRiga) {
        this.rdfDsRiga = rdfDsRiga;
    }

    public long getRdfDsRiga() {
        return this.rdfDsRiga;
    }

    public void setRdfDsOperSql(char rdfDsOperSql) {
        this.rdfDsOperSql = rdfDsOperSql;
    }

    public void setRdfDsOperSqlFormatted(String rdfDsOperSql) {
        setRdfDsOperSql(Functions.charAt(rdfDsOperSql, Types.CHAR_SIZE));
    }

    public char getRdfDsOperSql() {
        return this.rdfDsOperSql;
    }

    public void setRdfDsVer(int rdfDsVer) {
        this.rdfDsVer = rdfDsVer;
    }

    public int getRdfDsVer() {
        return this.rdfDsVer;
    }

    public void setRdfDsTsIniCptz(long rdfDsTsIniCptz) {
        this.rdfDsTsIniCptz = rdfDsTsIniCptz;
    }

    public long getRdfDsTsIniCptz() {
        return this.rdfDsTsIniCptz;
    }

    public void setRdfDsTsEndCptz(long rdfDsTsEndCptz) {
        this.rdfDsTsEndCptz = rdfDsTsEndCptz;
    }

    public long getRdfDsTsEndCptz() {
        return this.rdfDsTsEndCptz;
    }

    public void setRdfDsUtente(String rdfDsUtente) {
        this.rdfDsUtente = Functions.subString(rdfDsUtente, Len.RDF_DS_UTENTE);
    }

    public String getRdfDsUtente() {
        return this.rdfDsUtente;
    }

    public void setRdfDsStatoElab(char rdfDsStatoElab) {
        this.rdfDsStatoElab = rdfDsStatoElab;
    }

    public void setRdfDsStatoElabFormatted(String rdfDsStatoElab) {
        setRdfDsStatoElab(Functions.charAt(rdfDsStatoElab, Types.CHAR_SIZE));
    }

    public char getRdfDsStatoElab() {
        return this.rdfDsStatoElab;
    }

    public void setRdfFlCalcDis(char rdfFlCalcDis) {
        this.rdfFlCalcDis = rdfFlCalcDis;
    }

    public char getRdfFlCalcDis() {
        return this.rdfFlCalcDis;
    }

    public void setRdfFlSwmBp2s(char rdfFlSwmBp2s) {
        this.rdfFlSwmBp2s = rdfFlSwmBp2s;
    }

    public char getRdfFlSwmBp2s() {
        return this.rdfFlSwmBp2s;
    }

    public RdfCommisGest getRdfCommisGest() {
        return rdfCommisGest;
    }

    public RdfCosRunAssvaIdc getRdfCosRunAssvaIdc() {
        return rdfCosRunAssvaIdc;
    }

    public RdfDtCambioVlt getRdfDtCambioVlt() {
        return rdfDtCambioVlt;
    }

    public RdfDtDis getRdfDtDis() {
        return rdfDtDis;
    }

    public RdfDtDisCalc getRdfDtDisCalc() {
        return rdfDtDisCalc;
    }

    public RdfIdMoviChiu getRdfIdMoviChiu() {
        return rdfIdMoviChiu;
    }

    public RdfImpMovto getRdfImpMovto() {
        return rdfImpMovto;
    }

    public RdfNumQuo getRdfNumQuo() {
        return rdfNumQuo;
    }

    public RdfNumQuoCdgFnz getRdfNumQuoCdgFnz() {
        return rdfNumQuoCdgFnz;
    }

    public RdfNumQuoCdgtotFnz getRdfNumQuoCdgtotFnz() {
        return rdfNumQuoCdgtotFnz;
    }

    public RdfPc getRdfPc() {
        return rdfPc;
    }

    @Override
    public byte[] serialize() {
        return getRichDisFndBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_ID_RICH_DIS_FND = 5;
        public static final int RDF_ID_MOVI_FINRIO = 5;
        public static final int RDF_ID_MOVI_CRZ = 5;
        public static final int RDF_DT_INI_EFF = 5;
        public static final int RDF_DT_END_EFF = 5;
        public static final int RDF_COD_COMP_ANIA = 3;
        public static final int RDF_COD_FND = 20;
        public static final int RDF_COD_TARI = 12;
        public static final int RDF_TP_STAT = 2;
        public static final int RDF_TP_MOD_DIS = 2;
        public static final int RDF_COD_DIV = 20;
        public static final int RDF_TP_FND = 1;
        public static final int RDF_DS_RIGA = 6;
        public static final int RDF_DS_OPER_SQL = 1;
        public static final int RDF_DS_VER = 5;
        public static final int RDF_DS_TS_INI_CPTZ = 10;
        public static final int RDF_DS_TS_END_CPTZ = 10;
        public static final int RDF_DS_UTENTE = 20;
        public static final int RDF_DS_STATO_ELAB = 1;
        public static final int RDF_FL_CALC_DIS = 1;
        public static final int RDF_FL_SWM_BP2S = 1;
        public static final int RICH_DIS_FND = RDF_ID_RICH_DIS_FND + RDF_ID_MOVI_FINRIO + RDF_ID_MOVI_CRZ + RdfIdMoviChiu.Len.RDF_ID_MOVI_CHIU + RDF_DT_INI_EFF + RDF_DT_END_EFF + RDF_COD_COMP_ANIA + RDF_COD_FND + RdfNumQuo.Len.RDF_NUM_QUO + RdfPc.Len.RDF_PC + RdfImpMovto.Len.RDF_IMP_MOVTO + RdfDtDis.Len.RDF_DT_DIS + RDF_COD_TARI + RDF_TP_STAT + RDF_TP_MOD_DIS + RDF_COD_DIV + RdfDtCambioVlt.Len.RDF_DT_CAMBIO_VLT + RDF_TP_FND + RDF_DS_RIGA + RDF_DS_OPER_SQL + RDF_DS_VER + RDF_DS_TS_INI_CPTZ + RDF_DS_TS_END_CPTZ + RDF_DS_UTENTE + RDF_DS_STATO_ELAB + RdfDtDisCalc.Len.RDF_DT_DIS_CALC + RDF_FL_CALC_DIS + RdfCommisGest.Len.RDF_COMMIS_GEST + RdfNumQuoCdgFnz.Len.RDF_NUM_QUO_CDG_FNZ + RdfNumQuoCdgtotFnz.Len.RDF_NUM_QUO_CDGTOT_FNZ + RdfCosRunAssvaIdc.Len.RDF_COS_RUN_ASSVA_IDC + RDF_FL_SWM_BP2S;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_ID_RICH_DIS_FND = 9;
            public static final int RDF_ID_MOVI_FINRIO = 9;
            public static final int RDF_ID_MOVI_CRZ = 9;
            public static final int RDF_DT_INI_EFF = 8;
            public static final int RDF_DT_END_EFF = 8;
            public static final int RDF_COD_COMP_ANIA = 5;
            public static final int RDF_DS_RIGA = 10;
            public static final int RDF_DS_VER = 9;
            public static final int RDF_DS_TS_INI_CPTZ = 18;
            public static final int RDF_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
