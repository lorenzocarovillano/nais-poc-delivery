package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-MM-CNBZ-K3<br>
 * Variable: DFA-MM-CNBZ-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaMmCnbzK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaMmCnbzK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_MM_CNBZ_K3;
    }

    public void setDfaMmCnbzK3(short dfaMmCnbzK3) {
        writeShortAsPacked(Pos.DFA_MM_CNBZ_K3, dfaMmCnbzK3, Len.Int.DFA_MM_CNBZ_K3);
    }

    public void setDfaMmCnbzK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_MM_CNBZ_K3, Pos.DFA_MM_CNBZ_K3);
    }

    /**Original name: DFA-MM-CNBZ-K3<br>*/
    public short getDfaMmCnbzK3() {
        return readPackedAsShort(Pos.DFA_MM_CNBZ_K3, Len.Int.DFA_MM_CNBZ_K3);
    }

    public byte[] getDfaMmCnbzK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_MM_CNBZ_K3, Pos.DFA_MM_CNBZ_K3);
        return buffer;
    }

    public void setDfaMmCnbzK3Null(String dfaMmCnbzK3Null) {
        writeString(Pos.DFA_MM_CNBZ_K3_NULL, dfaMmCnbzK3Null, Len.DFA_MM_CNBZ_K3_NULL);
    }

    /**Original name: DFA-MM-CNBZ-K3-NULL<br>*/
    public String getDfaMmCnbzK3Null() {
        return readString(Pos.DFA_MM_CNBZ_K3_NULL, Len.DFA_MM_CNBZ_K3_NULL);
    }

    public String getDfaMmCnbzK3NullFormatted() {
        return Functions.padBlanks(getDfaMmCnbzK3Null(), Len.DFA_MM_CNBZ_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_MM_CNBZ_K3 = 1;
        public static final int DFA_MM_CNBZ_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_MM_CNBZ_K3 = 3;
        public static final int DFA_MM_CNBZ_K3_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_MM_CNBZ_K3 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
