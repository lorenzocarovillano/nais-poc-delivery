package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-IMP-ADER<br>
 * Variable: WTDR-IMP-ADER from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_IMP_ADER;
    }

    public void setWtdrImpAder(AfDecimal wtdrImpAder) {
        writeDecimalAsPacked(Pos.WTDR_IMP_ADER, wtdrImpAder.copy());
    }

    /**Original name: WTDR-IMP-ADER<br>*/
    public AfDecimal getWtdrImpAder() {
        return readPackedAsDecimal(Pos.WTDR_IMP_ADER, Len.Int.WTDR_IMP_ADER, Len.Fract.WTDR_IMP_ADER);
    }

    public void setWtdrImpAderNull(String wtdrImpAderNull) {
        writeString(Pos.WTDR_IMP_ADER_NULL, wtdrImpAderNull, Len.WTDR_IMP_ADER_NULL);
    }

    /**Original name: WTDR-IMP-ADER-NULL<br>*/
    public String getWtdrImpAderNull() {
        return readString(Pos.WTDR_IMP_ADER_NULL, Len.WTDR_IMP_ADER_NULL);
    }

    public String getWtdrImpAderNullFormatted() {
        return Functions.padBlanks(getWtdrImpAderNull(), Len.WTDR_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_ADER = 1;
        public static final int WTDR_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_ADER = 8;
        public static final int WTDR_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
