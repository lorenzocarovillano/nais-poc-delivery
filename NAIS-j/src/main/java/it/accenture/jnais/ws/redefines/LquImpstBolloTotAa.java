package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-BOLLO-TOT-AA<br>
 * Variable: LQU-IMPST-BOLLO-TOT-AA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpstBolloTotAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpstBolloTotAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST_BOLLO_TOT_AA;
    }

    public void setLquImpstBolloTotAa(AfDecimal lquImpstBolloTotAa) {
        writeDecimalAsPacked(Pos.LQU_IMPST_BOLLO_TOT_AA, lquImpstBolloTotAa.copy());
    }

    public void setLquImpstBolloTotAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST_BOLLO_TOT_AA, Pos.LQU_IMPST_BOLLO_TOT_AA);
    }

    /**Original name: LQU-IMPST-BOLLO-TOT-AA<br>*/
    public AfDecimal getLquImpstBolloTotAa() {
        return readPackedAsDecimal(Pos.LQU_IMPST_BOLLO_TOT_AA, Len.Int.LQU_IMPST_BOLLO_TOT_AA, Len.Fract.LQU_IMPST_BOLLO_TOT_AA);
    }

    public byte[] getLquImpstBolloTotAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST_BOLLO_TOT_AA, Pos.LQU_IMPST_BOLLO_TOT_AA);
        return buffer;
    }

    public void setLquImpstBolloTotAaNull(String lquImpstBolloTotAaNull) {
        writeString(Pos.LQU_IMPST_BOLLO_TOT_AA_NULL, lquImpstBolloTotAaNull, Len.LQU_IMPST_BOLLO_TOT_AA_NULL);
    }

    /**Original name: LQU-IMPST-BOLLO-TOT-AA-NULL<br>*/
    public String getLquImpstBolloTotAaNull() {
        return readString(Pos.LQU_IMPST_BOLLO_TOT_AA_NULL, Len.LQU_IMPST_BOLLO_TOT_AA_NULL);
    }

    public String getLquImpstBolloTotAaNullFormatted() {
        return Functions.padBlanks(getLquImpstBolloTotAaNull(), Len.LQU_IMPST_BOLLO_TOT_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_BOLLO_TOT_AA = 1;
        public static final int LQU_IMPST_BOLLO_TOT_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_BOLLO_TOT_AA = 8;
        public static final int LQU_IMPST_BOLLO_TOT_AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_BOLLO_TOT_AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_BOLLO_TOT_AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
