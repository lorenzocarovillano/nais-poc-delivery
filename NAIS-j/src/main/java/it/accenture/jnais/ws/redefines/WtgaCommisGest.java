package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-COMMIS-GEST<br>
 * Variable: WTGA-COMMIS-GEST from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaCommisGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaCommisGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_COMMIS_GEST;
    }

    public void setWtgaCommisGest(AfDecimal wtgaCommisGest) {
        writeDecimalAsPacked(Pos.WTGA_COMMIS_GEST, wtgaCommisGest.copy());
    }

    public void setWtgaCommisGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_COMMIS_GEST, Pos.WTGA_COMMIS_GEST);
    }

    /**Original name: WTGA-COMMIS-GEST<br>*/
    public AfDecimal getWtgaCommisGest() {
        return readPackedAsDecimal(Pos.WTGA_COMMIS_GEST, Len.Int.WTGA_COMMIS_GEST, Len.Fract.WTGA_COMMIS_GEST);
    }

    public byte[] getWtgaCommisGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_COMMIS_GEST, Pos.WTGA_COMMIS_GEST);
        return buffer;
    }

    public void initWtgaCommisGestSpaces() {
        fill(Pos.WTGA_COMMIS_GEST, Len.WTGA_COMMIS_GEST, Types.SPACE_CHAR);
    }

    public void setWtgaCommisGestNull(String wtgaCommisGestNull) {
        writeString(Pos.WTGA_COMMIS_GEST_NULL, wtgaCommisGestNull, Len.WTGA_COMMIS_GEST_NULL);
    }

    /**Original name: WTGA-COMMIS-GEST-NULL<br>*/
    public String getWtgaCommisGestNull() {
        return readString(Pos.WTGA_COMMIS_GEST_NULL, Len.WTGA_COMMIS_GEST_NULL);
    }

    public String getWtgaCommisGestNullFormatted() {
        return Functions.padBlanks(getWtgaCommisGestNull(), Len.WTGA_COMMIS_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_COMMIS_GEST = 1;
        public static final int WTGA_COMMIS_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_COMMIS_GEST = 8;
        public static final int WTGA_COMMIS_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_COMMIS_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
