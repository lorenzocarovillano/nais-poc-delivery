package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMP-IS<br>
 * Variable: S089-TOT-IMP-IS from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMP_IS;
    }

    public void setWlquTotImpIs(AfDecimal wlquTotImpIs) {
        writeDecimalAsPacked(Pos.S089_TOT_IMP_IS, wlquTotImpIs.copy());
    }

    public void setWlquTotImpIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMP_IS, Pos.S089_TOT_IMP_IS);
    }

    /**Original name: WLQU-TOT-IMP-IS<br>*/
    public AfDecimal getWlquTotImpIs() {
        return readPackedAsDecimal(Pos.S089_TOT_IMP_IS, Len.Int.WLQU_TOT_IMP_IS, Len.Fract.WLQU_TOT_IMP_IS);
    }

    public byte[] getWlquTotImpIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMP_IS, Pos.S089_TOT_IMP_IS);
        return buffer;
    }

    public void initWlquTotImpIsSpaces() {
        fill(Pos.S089_TOT_IMP_IS, Len.S089_TOT_IMP_IS, Types.SPACE_CHAR);
    }

    public void setWlquTotImpIsNull(String wlquTotImpIsNull) {
        writeString(Pos.S089_TOT_IMP_IS_NULL, wlquTotImpIsNull, Len.WLQU_TOT_IMP_IS_NULL);
    }

    /**Original name: WLQU-TOT-IMP-IS-NULL<br>*/
    public String getWlquTotImpIsNull() {
        return readString(Pos.S089_TOT_IMP_IS_NULL, Len.WLQU_TOT_IMP_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_IS = 1;
        public static final int S089_TOT_IMP_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_IS = 8;
        public static final int WLQU_TOT_IMP_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
