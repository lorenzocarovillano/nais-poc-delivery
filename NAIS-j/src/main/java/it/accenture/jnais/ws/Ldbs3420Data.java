package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IStatOggBusTrchDiGar;
import it.accenture.jnais.copy.Idbvstb3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndTrchDiGar;
import it.accenture.jnais.copy.StatOggBus;
import it.accenture.jnais.copy.TrchDiGarDb;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS3420<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs3420Data implements IStatOggBusTrchDiGar {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-TRCH-DI-GAR
    private IndTrchDiGar indTrchDiGar = new IndTrchDiGar();
    //Original name: IND-STB-ID-MOVI-CHIU
    private short indStbIdMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: TRCH-DI-GAR-DB
    private TrchDiGarDb trchDiGarDb = new TrchDiGarDb();
    //Original name: IDBVSTB3
    private Idbvstb3 idbvstb3 = new Idbvstb3();
    //Original name: TRCH-DI-GAR
    private TrchDiGarIvvs0216 trchDiGar = new TrchDiGarIvvs0216();
    //Original name: STAT-OGG-BUS
    private StatOggBus statOggBus = new StatOggBus();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndStbIdMoviChiu(short indStbIdMoviChiu) {
        this.indStbIdMoviChiu = indStbIdMoviChiu;
    }

    public short getIndStbIdMoviChiu() {
        return this.indStbIdMoviChiu;
    }

    public Idbvstb3 getIdbvstb3() {
        return idbvstb3;
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndTrchDiGar getIndTrchDiGar() {
        return indTrchDiGar;
    }

    @Override
    public int getLc601IdMoviCrz() {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public void setLc601IdMoviCrz(int lc601IdMoviCrz) {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public int getLc601IdTrchDiGar() {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public void setLc601IdTrchDiGar(int lc601IdTrchDiGar) {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public int getLdbi0731IdAdes() {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public void setLdbi0731IdAdes(int ldbi0731IdAdes) {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public int getLdbi0731IdPoli() {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public void setLdbi0731IdPoli(int ldbi0731IdPoli) {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public int getLdbi0731IdTrch() {
        throw new FieldNotMappedException("ldbi0731IdTrch");
    }

    @Override
    public void setLdbi0731IdTrch(int ldbi0731IdTrch) {
        throw new FieldNotMappedException("ldbi0731IdTrch");
    }

    @Override
    public String getLdbi0731TpStatBus() {
        throw new FieldNotMappedException("ldbi0731TpStatBus");
    }

    @Override
    public void setLdbi0731TpStatBus(String ldbi0731TpStatBus) {
        throw new FieldNotMappedException("ldbi0731TpStatBus");
    }

    @Override
    public String getLdbv1361TpCaus01() {
        throw new FieldNotMappedException("ldbv1361TpCaus01");
    }

    @Override
    public void setLdbv1361TpCaus01(String ldbv1361TpCaus01) {
        throw new FieldNotMappedException("ldbv1361TpCaus01");
    }

    @Override
    public String getLdbv1361TpCaus02() {
        throw new FieldNotMappedException("ldbv1361TpCaus02");
    }

    @Override
    public void setLdbv1361TpCaus02(String ldbv1361TpCaus02) {
        throw new FieldNotMappedException("ldbv1361TpCaus02");
    }

    @Override
    public String getLdbv1361TpCaus03() {
        throw new FieldNotMappedException("ldbv1361TpCaus03");
    }

    @Override
    public void setLdbv1361TpCaus03(String ldbv1361TpCaus03) {
        throw new FieldNotMappedException("ldbv1361TpCaus03");
    }

    @Override
    public String getLdbv1361TpCaus04() {
        throw new FieldNotMappedException("ldbv1361TpCaus04");
    }

    @Override
    public void setLdbv1361TpCaus04(String ldbv1361TpCaus04) {
        throw new FieldNotMappedException("ldbv1361TpCaus04");
    }

    @Override
    public String getLdbv1361TpCaus05() {
        throw new FieldNotMappedException("ldbv1361TpCaus05");
    }

    @Override
    public void setLdbv1361TpCaus05(String ldbv1361TpCaus05) {
        throw new FieldNotMappedException("ldbv1361TpCaus05");
    }

    @Override
    public String getLdbv1361TpCaus06() {
        throw new FieldNotMappedException("ldbv1361TpCaus06");
    }

    @Override
    public void setLdbv1361TpCaus06(String ldbv1361TpCaus06) {
        throw new FieldNotMappedException("ldbv1361TpCaus06");
    }

    @Override
    public String getLdbv1361TpCaus07() {
        throw new FieldNotMappedException("ldbv1361TpCaus07");
    }

    @Override
    public void setLdbv1361TpCaus07(String ldbv1361TpCaus07) {
        throw new FieldNotMappedException("ldbv1361TpCaus07");
    }

    @Override
    public String getLdbv1361TpStatBus01() {
        throw new FieldNotMappedException("ldbv1361TpStatBus01");
    }

    @Override
    public void setLdbv1361TpStatBus01(String ldbv1361TpStatBus01) {
        throw new FieldNotMappedException("ldbv1361TpStatBus01");
    }

    @Override
    public String getLdbv1361TpStatBus02() {
        throw new FieldNotMappedException("ldbv1361TpStatBus02");
    }

    @Override
    public void setLdbv1361TpStatBus02(String ldbv1361TpStatBus02) {
        throw new FieldNotMappedException("ldbv1361TpStatBus02");
    }

    @Override
    public String getLdbv1361TpStatBus03() {
        throw new FieldNotMappedException("ldbv1361TpStatBus03");
    }

    @Override
    public void setLdbv1361TpStatBus03(String ldbv1361TpStatBus03) {
        throw new FieldNotMappedException("ldbv1361TpStatBus03");
    }

    @Override
    public String getLdbv1361TpStatBus04() {
        throw new FieldNotMappedException("ldbv1361TpStatBus04");
    }

    @Override
    public void setLdbv1361TpStatBus04(String ldbv1361TpStatBus04) {
        throw new FieldNotMappedException("ldbv1361TpStatBus04");
    }

    @Override
    public String getLdbv1361TpStatBus05() {
        throw new FieldNotMappedException("ldbv1361TpStatBus05");
    }

    @Override
    public void setLdbv1361TpStatBus05(String ldbv1361TpStatBus05) {
        throw new FieldNotMappedException("ldbv1361TpStatBus05");
    }

    @Override
    public String getLdbv1361TpStatBus06() {
        throw new FieldNotMappedException("ldbv1361TpStatBus06");
    }

    @Override
    public void setLdbv1361TpStatBus06(String ldbv1361TpStatBus06) {
        throw new FieldNotMappedException("ldbv1361TpStatBus06");
    }

    @Override
    public String getLdbv1361TpStatBus07() {
        throw new FieldNotMappedException("ldbv1361TpStatBus07");
    }

    @Override
    public void setLdbv1361TpStatBus07(String ldbv1361TpStatBus07) {
        throw new FieldNotMappedException("ldbv1361TpStatBus07");
    }

    @Override
    public int getLdbv2911IdAdes() {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public void setLdbv2911IdAdes(int ldbv2911IdAdes) {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public int getLdbv2911IdPoli() {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public void setLdbv2911IdPoli(int ldbv2911IdPoli) {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public AfDecimal getLdbv2911PrstzIniNewfis() {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public void setLdbv2911PrstzIniNewfis(AfDecimal ldbv2911PrstzIniNewfis) {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public int getLdbv3021IdOgg() {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public void setLdbv3021IdOgg(int ldbv3021IdOgg) {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public String getLdbv3021TpCausBus10() {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public void setLdbv3021TpCausBus10(String ldbv3021TpCausBus10) {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public String getLdbv3021TpCausBus11() {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public void setLdbv3021TpCausBus11(String ldbv3021TpCausBus11) {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public String getLdbv3021TpCausBus12() {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public void setLdbv3021TpCausBus12(String ldbv3021TpCausBus12) {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public String getLdbv3021TpCausBus13() {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public void setLdbv3021TpCausBus13(String ldbv3021TpCausBus13) {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public String getLdbv3021TpCausBus14() {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public void setLdbv3021TpCausBus14(String ldbv3021TpCausBus14) {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public String getLdbv3021TpCausBus15() {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public void setLdbv3021TpCausBus15(String ldbv3021TpCausBus15) {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public String getLdbv3021TpCausBus16() {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public void setLdbv3021TpCausBus16(String ldbv3021TpCausBus16) {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public String getLdbv3021TpCausBus17() {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public void setLdbv3021TpCausBus17(String ldbv3021TpCausBus17) {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public String getLdbv3021TpCausBus18() {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public void setLdbv3021TpCausBus18(String ldbv3021TpCausBus18) {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public String getLdbv3021TpCausBus19() {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public void setLdbv3021TpCausBus19(String ldbv3021TpCausBus19) {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public String getLdbv3021TpCausBus1() {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public void setLdbv3021TpCausBus1(String ldbv3021TpCausBus1) {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public String getLdbv3021TpCausBus20() {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public void setLdbv3021TpCausBus20(String ldbv3021TpCausBus20) {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public String getLdbv3021TpCausBus2() {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public void setLdbv3021TpCausBus2(String ldbv3021TpCausBus2) {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public String getLdbv3021TpCausBus3() {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public void setLdbv3021TpCausBus3(String ldbv3021TpCausBus3) {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public String getLdbv3021TpCausBus4() {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public void setLdbv3021TpCausBus4(String ldbv3021TpCausBus4) {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public String getLdbv3021TpCausBus5() {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public void setLdbv3021TpCausBus5(String ldbv3021TpCausBus5) {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public String getLdbv3021TpCausBus6() {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public void setLdbv3021TpCausBus6(String ldbv3021TpCausBus6) {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public String getLdbv3021TpCausBus7() {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public void setLdbv3021TpCausBus7(String ldbv3021TpCausBus7) {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public String getLdbv3021TpCausBus8() {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public void setLdbv3021TpCausBus8(String ldbv3021TpCausBus8) {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public String getLdbv3021TpCausBus9() {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public void setLdbv3021TpCausBus9(String ldbv3021TpCausBus9) {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public String getLdbv3021TpOgg() {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public void setLdbv3021TpOgg(String ldbv3021TpOgg) {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public String getLdbv3021TpStatBus() {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public void setLdbv3021TpStatBus(String ldbv3021TpStatBus) {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public int getLdbv3421IdAdes() {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public void setLdbv3421IdAdes(int ldbv3421IdAdes) {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public int getLdbv3421IdGar() {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public void setLdbv3421IdGar(int ldbv3421IdGar) {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public int getLdbv3421IdPoli() {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public void setLdbv3421IdPoli(int ldbv3421IdPoli) {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public String getLdbv3421TpOgg() {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public void setLdbv3421TpOgg(String ldbv3421TpOgg) {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public String getLdbv3421TpStatBus1() {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public void setLdbv3421TpStatBus1(String ldbv3421TpStatBus1) {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public String getLdbv3421TpStatBus2() {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public void setLdbv3421TpStatBus2(String ldbv3421TpStatBus2) {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public int getLdbvd511IdAdes() {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public void setLdbvd511IdAdes(int ldbvd511IdAdes) {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public int getLdbvd511IdPoli() {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public void setLdbvd511IdPoli(int ldbvd511IdPoli) {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public String getLdbvd511TpCaus() {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public void setLdbvd511TpCaus(String ldbvd511TpCaus) {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public String getLdbvd511TpOgg() {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public void setLdbvd511TpOgg(String ldbvd511TpOgg) {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public String getLdbvd511TpStatBus() {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public void setLdbvd511TpStatBus(String ldbvd511TpStatBus) {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public int getLdbve251IdAdes() {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public void setLdbve251IdAdes(int ldbve251IdAdes) {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public int getLdbve251IdMovi() {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public void setLdbve251IdMovi(int ldbve251IdMovi) {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public AfDecimal getLdbve251ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public void setLdbve251ImpbVisEnd2000(AfDecimal ldbve251ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public String getLdbve251TpTrch10() {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public void setLdbve251TpTrch10(String ldbve251TpTrch10) {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public String getLdbve251TpTrch11() {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public void setLdbve251TpTrch11(String ldbve251TpTrch11) {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public String getLdbve251TpTrch12() {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public void setLdbve251TpTrch12(String ldbve251TpTrch12) {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public String getLdbve251TpTrch13() {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public void setLdbve251TpTrch13(String ldbve251TpTrch13) {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public String getLdbve251TpTrch14() {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public void setLdbve251TpTrch14(String ldbve251TpTrch14) {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public String getLdbve251TpTrch15() {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public void setLdbve251TpTrch15(String ldbve251TpTrch15) {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public String getLdbve251TpTrch16() {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public void setLdbve251TpTrch16(String ldbve251TpTrch16) {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public String getLdbve251TpTrch1() {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public void setLdbve251TpTrch1(String ldbve251TpTrch1) {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public String getLdbve251TpTrch2() {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public void setLdbve251TpTrch2(String ldbve251TpTrch2) {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public String getLdbve251TpTrch3() {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public void setLdbve251TpTrch3(String ldbve251TpTrch3) {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public String getLdbve251TpTrch4() {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public void setLdbve251TpTrch4(String ldbve251TpTrch4) {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public String getLdbve251TpTrch5() {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public void setLdbve251TpTrch5(String ldbve251TpTrch5) {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public String getLdbve251TpTrch6() {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public void setLdbve251TpTrch6(String ldbve251TpTrch6) {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public String getLdbve251TpTrch7() {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public void setLdbve251TpTrch7(String ldbve251TpTrch7) {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public String getLdbve251TpTrch8() {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public void setLdbve251TpTrch8(String ldbve251TpTrch8) {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public String getLdbve251TpTrch9() {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public void setLdbve251TpTrch9(String ldbve251TpTrch9) {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public int getLdbve261IdAdes() {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public void setLdbve261IdAdes(int ldbve261IdAdes) {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public AfDecimal getLdbve261ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public void setLdbve261ImpbVisEnd2000(AfDecimal ldbve261ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public String getLdbve261TpTrch10() {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public void setLdbve261TpTrch10(String ldbve261TpTrch10) {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public String getLdbve261TpTrch11() {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public void setLdbve261TpTrch11(String ldbve261TpTrch11) {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public String getLdbve261TpTrch12() {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public void setLdbve261TpTrch12(String ldbve261TpTrch12) {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public String getLdbve261TpTrch13() {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public void setLdbve261TpTrch13(String ldbve261TpTrch13) {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public String getLdbve261TpTrch14() {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public void setLdbve261TpTrch14(String ldbve261TpTrch14) {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public String getLdbve261TpTrch15() {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public void setLdbve261TpTrch15(String ldbve261TpTrch15) {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public String getLdbve261TpTrch16() {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public void setLdbve261TpTrch16(String ldbve261TpTrch16) {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public String getLdbve261TpTrch1() {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public void setLdbve261TpTrch1(String ldbve261TpTrch1) {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public String getLdbve261TpTrch2() {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public void setLdbve261TpTrch2(String ldbve261TpTrch2) {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public String getLdbve261TpTrch3() {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public void setLdbve261TpTrch3(String ldbve261TpTrch3) {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public String getLdbve261TpTrch4() {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public void setLdbve261TpTrch4(String ldbve261TpTrch4) {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public String getLdbve261TpTrch5() {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public void setLdbve261TpTrch5(String ldbve261TpTrch5) {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public String getLdbve261TpTrch6() {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public void setLdbve261TpTrch6(String ldbve261TpTrch6) {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public String getLdbve261TpTrch7() {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public void setLdbve261TpTrch7(String ldbve261TpTrch7) {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public String getLdbve261TpTrch8() {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public void setLdbve261TpTrch8(String ldbve261TpTrch8) {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public String getLdbve261TpTrch9() {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    @Override
    public void setLdbve261TpTrch9(String ldbve261TpTrch9) {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    public StatOggBus getStatOggBus() {
        return statOggBus;
    }

    @Override
    public int getStbCodCompAnia() {
        return statOggBus.getStbCodCompAnia();
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        this.statOggBus.setStbCodCompAnia(stbCodCompAnia);
    }

    @Override
    public char getStbDsOperSql() {
        return statOggBus.getStbDsOperSql();
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        this.statOggBus.setStbDsOperSql(stbDsOperSql);
    }

    @Override
    public long getStbDsRiga() {
        return statOggBus.getStbDsRiga();
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        this.statOggBus.setStbDsRiga(stbDsRiga);
    }

    @Override
    public char getStbDsStatoElab() {
        return statOggBus.getStbDsStatoElab();
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        this.statOggBus.setStbDsStatoElab(stbDsStatoElab);
    }

    @Override
    public long getStbDsTsEndCptz() {
        return statOggBus.getStbDsTsEndCptz();
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        this.statOggBus.setStbDsTsEndCptz(stbDsTsEndCptz);
    }

    @Override
    public long getStbDsTsIniCptz() {
        return statOggBus.getStbDsTsIniCptz();
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        this.statOggBus.setStbDsTsIniCptz(stbDsTsIniCptz);
    }

    @Override
    public String getStbDsUtente() {
        return statOggBus.getStbDsUtente();
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        this.statOggBus.setStbDsUtente(stbDsUtente);
    }

    @Override
    public int getStbDsVer() {
        return statOggBus.getStbDsVer();
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        this.statOggBus.setStbDsVer(stbDsVer);
    }

    @Override
    public String getStbDtEndEffDb() {
        return idbvstb3.getStbDtEndEffDb();
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        this.idbvstb3.setStbDtEndEffDb(stbDtEndEffDb);
    }

    @Override
    public String getStbDtIniEffDb() {
        return idbvstb3.getStbDtIniEffDb();
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        this.idbvstb3.setStbDtIniEffDb(stbDtIniEffDb);
    }

    @Override
    public int getStbIdMoviChiu() {
        return statOggBus.getStbIdMoviChiu().getStbIdMoviChiu();
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        this.statOggBus.getStbIdMoviChiu().setStbIdMoviChiu(stbIdMoviChiu);
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        if (getIndStbIdMoviChiu() >= 0) {
            return ((Integer)getStbIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        if (stbIdMoviChiuObj != null) {
            setStbIdMoviChiu(((int)stbIdMoviChiuObj));
            setIndStbIdMoviChiu(((short)0));
        }
        else {
            setIndStbIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getStbIdMoviCrz() {
        return statOggBus.getStbIdMoviCrz();
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        this.statOggBus.setStbIdMoviCrz(stbIdMoviCrz);
    }

    @Override
    public int getStbIdOgg() {
        return statOggBus.getStbIdOgg();
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        this.statOggBus.setStbIdOgg(stbIdOgg);
    }

    @Override
    public int getStbIdStatOggBus() {
        return statOggBus.getStbIdStatOggBus();
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        this.statOggBus.setStbIdStatOggBus(stbIdStatOggBus);
    }

    @Override
    public String getStbTpCaus() {
        return statOggBus.getStbTpCaus();
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        this.statOggBus.setStbTpCaus(stbTpCaus);
    }

    @Override
    public String getStbTpOgg() {
        return statOggBus.getStbTpOgg();
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        this.statOggBus.setStbTpOgg(stbTpOgg);
    }

    @Override
    public String getStbTpStatBus() {
        return statOggBus.getStbTpStatBus();
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        this.statOggBus.setStbTpStatBus(stbTpStatBus);
    }

    @Override
    public AfDecimal getTgaAbbAnnuUlt() {
        return trchDiGar.getTgaAbbAnnuUlt().getTgaAbbAnnuUlt();
    }

    @Override
    public void setTgaAbbAnnuUlt(AfDecimal tgaAbbAnnuUlt) {
        this.trchDiGar.getTgaAbbAnnuUlt().setTgaAbbAnnuUlt(tgaAbbAnnuUlt.copy());
    }

    @Override
    public AfDecimal getTgaAbbAnnuUltObj() {
        if (indTrchDiGar.getAbbAnnuUlt() >= 0) {
            return getTgaAbbAnnuUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbAnnuUltObj(AfDecimal tgaAbbAnnuUltObj) {
        if (tgaAbbAnnuUltObj != null) {
            setTgaAbbAnnuUlt(new AfDecimal(tgaAbbAnnuUltObj, 15, 3));
            indTrchDiGar.setAbbAnnuUlt(((short)0));
        }
        else {
            indTrchDiGar.setAbbAnnuUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAbbTotIni() {
        return trchDiGar.getTgaAbbTotIni().getTgaAbbTotIni();
    }

    @Override
    public void setTgaAbbTotIni(AfDecimal tgaAbbTotIni) {
        this.trchDiGar.getTgaAbbTotIni().setTgaAbbTotIni(tgaAbbTotIni.copy());
    }

    @Override
    public AfDecimal getTgaAbbTotIniObj() {
        if (indTrchDiGar.getAbbTotIni() >= 0) {
            return getTgaAbbTotIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbTotIniObj(AfDecimal tgaAbbTotIniObj) {
        if (tgaAbbTotIniObj != null) {
            setTgaAbbTotIni(new AfDecimal(tgaAbbTotIniObj, 15, 3));
            indTrchDiGar.setAbbTotIni(((short)0));
        }
        else {
            indTrchDiGar.setAbbTotIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAbbTotUlt() {
        return trchDiGar.getTgaAbbTotUlt().getTgaAbbTotUlt();
    }

    @Override
    public void setTgaAbbTotUlt(AfDecimal tgaAbbTotUlt) {
        this.trchDiGar.getTgaAbbTotUlt().setTgaAbbTotUlt(tgaAbbTotUlt.copy());
    }

    @Override
    public AfDecimal getTgaAbbTotUltObj() {
        if (indTrchDiGar.getAbbTotUlt() >= 0) {
            return getTgaAbbTotUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbTotUltObj(AfDecimal tgaAbbTotUltObj) {
        if (tgaAbbTotUltObj != null) {
            setTgaAbbTotUlt(new AfDecimal(tgaAbbTotUltObj, 15, 3));
            indTrchDiGar.setAbbTotUlt(((short)0));
        }
        else {
            indTrchDiGar.setAbbTotUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAcqExp() {
        return trchDiGar.getTgaAcqExp().getTgaAcqExp();
    }

    @Override
    public void setTgaAcqExp(AfDecimal tgaAcqExp) {
        this.trchDiGar.getTgaAcqExp().setTgaAcqExp(tgaAcqExp.copy());
    }

    @Override
    public AfDecimal getTgaAcqExpObj() {
        if (indTrchDiGar.getAcqExp() >= 0) {
            return getTgaAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAcqExpObj(AfDecimal tgaAcqExpObj) {
        if (tgaAcqExpObj != null) {
            setTgaAcqExp(new AfDecimal(tgaAcqExpObj, 15, 3));
            indTrchDiGar.setAcqExp(((short)0));
        }
        else {
            indTrchDiGar.setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqCommisInter() {
        return trchDiGar.getTgaAlqCommisInter().getTgaAlqCommisInter();
    }

    @Override
    public void setTgaAlqCommisInter(AfDecimal tgaAlqCommisInter) {
        this.trchDiGar.getTgaAlqCommisInter().setTgaAlqCommisInter(tgaAlqCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaAlqCommisInterObj() {
        if (indTrchDiGar.getAlqCommisInter() >= 0) {
            return getTgaAlqCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqCommisInterObj(AfDecimal tgaAlqCommisInterObj) {
        if (tgaAlqCommisInterObj != null) {
            setTgaAlqCommisInter(new AfDecimal(tgaAlqCommisInterObj, 6, 3));
            indTrchDiGar.setAlqCommisInter(((short)0));
        }
        else {
            indTrchDiGar.setAlqCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvAcq() {
        return trchDiGar.getTgaAlqProvAcq().getTgaAlqProvAcq();
    }

    @Override
    public void setTgaAlqProvAcq(AfDecimal tgaAlqProvAcq) {
        this.trchDiGar.getTgaAlqProvAcq().setTgaAlqProvAcq(tgaAlqProvAcq.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvAcqObj() {
        if (indTrchDiGar.getAlqProvAcq() >= 0) {
            return getTgaAlqProvAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvAcqObj(AfDecimal tgaAlqProvAcqObj) {
        if (tgaAlqProvAcqObj != null) {
            setTgaAlqProvAcq(new AfDecimal(tgaAlqProvAcqObj, 6, 3));
            indTrchDiGar.setAlqProvAcq(((short)0));
        }
        else {
            indTrchDiGar.setAlqProvAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvInc() {
        return trchDiGar.getTgaAlqProvInc().getTgaAlqProvInc();
    }

    @Override
    public void setTgaAlqProvInc(AfDecimal tgaAlqProvInc) {
        this.trchDiGar.getTgaAlqProvInc().setTgaAlqProvInc(tgaAlqProvInc.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvIncObj() {
        if (indTrchDiGar.getAlqProvInc() >= 0) {
            return getTgaAlqProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvIncObj(AfDecimal tgaAlqProvIncObj) {
        if (tgaAlqProvIncObj != null) {
            setTgaAlqProvInc(new AfDecimal(tgaAlqProvIncObj, 6, 3));
            indTrchDiGar.setAlqProvInc(((short)0));
        }
        else {
            indTrchDiGar.setAlqProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvRicor() {
        return trchDiGar.getTgaAlqProvRicor().getTgaAlqProvRicor();
    }

    @Override
    public void setTgaAlqProvRicor(AfDecimal tgaAlqProvRicor) {
        this.trchDiGar.getTgaAlqProvRicor().setTgaAlqProvRicor(tgaAlqProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvRicorObj() {
        if (indTrchDiGar.getAlqProvRicor() >= 0) {
            return getTgaAlqProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvRicorObj(AfDecimal tgaAlqProvRicorObj) {
        if (tgaAlqProvRicorObj != null) {
            setTgaAlqProvRicor(new AfDecimal(tgaAlqProvRicorObj, 6, 3));
            indTrchDiGar.setAlqProvRicor(((short)0));
        }
        else {
            indTrchDiGar.setAlqProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqRemunAss() {
        return trchDiGar.getTgaAlqRemunAss().getTgaAlqRemunAss();
    }

    @Override
    public void setTgaAlqRemunAss(AfDecimal tgaAlqRemunAss) {
        this.trchDiGar.getTgaAlqRemunAss().setTgaAlqRemunAss(tgaAlqRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaAlqRemunAssObj() {
        if (indTrchDiGar.getAlqRemunAss() >= 0) {
            return getTgaAlqRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqRemunAssObj(AfDecimal tgaAlqRemunAssObj) {
        if (tgaAlqRemunAssObj != null) {
            setTgaAlqRemunAss(new AfDecimal(tgaAlqRemunAssObj, 6, 3));
            indTrchDiGar.setAlqRemunAss(((short)0));
        }
        else {
            indTrchDiGar.setAlqRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqScon() {
        return trchDiGar.getTgaAlqScon().getTgaAlqScon();
    }

    @Override
    public void setTgaAlqScon(AfDecimal tgaAlqScon) {
        this.trchDiGar.getTgaAlqScon().setTgaAlqScon(tgaAlqScon.copy());
    }

    @Override
    public AfDecimal getTgaAlqSconObj() {
        if (indTrchDiGar.getAlqScon() >= 0) {
            return getTgaAlqScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqSconObj(AfDecimal tgaAlqSconObj) {
        if (tgaAlqSconObj != null) {
            setTgaAlqScon(new AfDecimal(tgaAlqSconObj, 6, 3));
            indTrchDiGar.setAlqScon(((short)0));
        }
        else {
            indTrchDiGar.setAlqScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqto() {
        return trchDiGar.getTgaBnsGiaLiqto().getTgaBnsGiaLiqto();
    }

    @Override
    public void setTgaBnsGiaLiqto(AfDecimal tgaBnsGiaLiqto) {
        this.trchDiGar.getTgaBnsGiaLiqto().setTgaBnsGiaLiqto(tgaBnsGiaLiqto.copy());
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqtoObj() {
        if (indTrchDiGar.getBnsGiaLiqto() >= 0) {
            return getTgaBnsGiaLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaBnsGiaLiqtoObj(AfDecimal tgaBnsGiaLiqtoObj) {
        if (tgaBnsGiaLiqtoObj != null) {
            setTgaBnsGiaLiqto(new AfDecimal(tgaBnsGiaLiqtoObj, 15, 3));
            indTrchDiGar.setBnsGiaLiqto(((short)0));
        }
        else {
            indTrchDiGar.setBnsGiaLiqto(((short)-1));
        }
    }

    @Override
    public int getTgaCodCompAnia() {
        return trchDiGar.getTgaCodCompAnia();
    }

    @Override
    public void setTgaCodCompAnia(int tgaCodCompAnia) {
        this.trchDiGar.setTgaCodCompAnia(tgaCodCompAnia);
    }

    @Override
    public String getTgaCodDvs() {
        return trchDiGar.getTgaCodDvs();
    }

    @Override
    public void setTgaCodDvs(String tgaCodDvs) {
        this.trchDiGar.setTgaCodDvs(tgaCodDvs);
    }

    @Override
    public AfDecimal getTgaCommisGest() {
        return trchDiGar.getTgaCommisGest().getTgaCommisGest();
    }

    @Override
    public void setTgaCommisGest(AfDecimal tgaCommisGest) {
        this.trchDiGar.getTgaCommisGest().setTgaCommisGest(tgaCommisGest.copy());
    }

    @Override
    public AfDecimal getTgaCommisGestObj() {
        if (indTrchDiGar.getCommisGest() >= 0) {
            return getTgaCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCommisGestObj(AfDecimal tgaCommisGestObj) {
        if (tgaCommisGestObj != null) {
            setTgaCommisGest(new AfDecimal(tgaCommisGestObj, 15, 3));
            indTrchDiGar.setCommisGest(((short)0));
        }
        else {
            indTrchDiGar.setCommisGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCommisInter() {
        return trchDiGar.getTgaCommisInter().getTgaCommisInter();
    }

    @Override
    public void setTgaCommisInter(AfDecimal tgaCommisInter) {
        this.trchDiGar.getTgaCommisInter().setTgaCommisInter(tgaCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaCommisInterObj() {
        if (indTrchDiGar.getCommisInter() >= 0) {
            return getTgaCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCommisInterObj(AfDecimal tgaCommisInterObj) {
        if (tgaCommisInterObj != null) {
            setTgaCommisInter(new AfDecimal(tgaCommisInterObj, 15, 3));
            indTrchDiGar.setCommisInter(((short)0));
        }
        else {
            indTrchDiGar.setCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCosRunAssva() {
        return trchDiGar.getTgaCosRunAssva().getTgaCosRunAssva();
    }

    @Override
    public void setTgaCosRunAssva(AfDecimal tgaCosRunAssva) {
        this.trchDiGar.getTgaCosRunAssva().setTgaCosRunAssva(tgaCosRunAssva.copy());
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdc() {
        return trchDiGar.getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdc();
    }

    @Override
    public void setTgaCosRunAssvaIdc(AfDecimal tgaCosRunAssvaIdc) {
        this.trchDiGar.getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdc(tgaCosRunAssvaIdc.copy());
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdcObj() {
        if (indTrchDiGar.getCosRunAssvaIdc() >= 0) {
            return getTgaCosRunAssvaIdc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCosRunAssvaIdcObj(AfDecimal tgaCosRunAssvaIdcObj) {
        if (tgaCosRunAssvaIdcObj != null) {
            setTgaCosRunAssvaIdc(new AfDecimal(tgaCosRunAssvaIdcObj, 15, 3));
            indTrchDiGar.setCosRunAssvaIdc(((short)0));
        }
        else {
            indTrchDiGar.setCosRunAssvaIdc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCosRunAssvaObj() {
        if (indTrchDiGar.getCosRunAssva() >= 0) {
            return getTgaCosRunAssva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCosRunAssvaObj(AfDecimal tgaCosRunAssvaObj) {
        if (tgaCosRunAssvaObj != null) {
            setTgaCosRunAssva(new AfDecimal(tgaCosRunAssvaObj, 15, 3));
            indTrchDiGar.setCosRunAssva(((short)0));
        }
        else {
            indTrchDiGar.setCosRunAssva(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptInOpzRivto() {
        return trchDiGar.getTgaCptInOpzRivto().getTgaCptInOpzRivto();
    }

    @Override
    public void setTgaCptInOpzRivto(AfDecimal tgaCptInOpzRivto) {
        this.trchDiGar.getTgaCptInOpzRivto().setTgaCptInOpzRivto(tgaCptInOpzRivto.copy());
    }

    @Override
    public AfDecimal getTgaCptInOpzRivtoObj() {
        if (indTrchDiGar.getCptInOpzRivto() >= 0) {
            return getTgaCptInOpzRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptInOpzRivtoObj(AfDecimal tgaCptInOpzRivtoObj) {
        if (tgaCptInOpzRivtoObj != null) {
            setTgaCptInOpzRivto(new AfDecimal(tgaCptInOpzRivtoObj, 15, 3));
            indTrchDiGar.setCptInOpzRivto(((short)0));
        }
        else {
            indTrchDiGar.setCptInOpzRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptMinScad() {
        return trchDiGar.getTgaCptMinScad().getTgaCptMinScad();
    }

    @Override
    public void setTgaCptMinScad(AfDecimal tgaCptMinScad) {
        this.trchDiGar.getTgaCptMinScad().setTgaCptMinScad(tgaCptMinScad.copy());
    }

    @Override
    public AfDecimal getTgaCptMinScadObj() {
        if (indTrchDiGar.getCptMinScad() >= 0) {
            return getTgaCptMinScad();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptMinScadObj(AfDecimal tgaCptMinScadObj) {
        if (tgaCptMinScadObj != null) {
            setTgaCptMinScad(new AfDecimal(tgaCptMinScadObj, 15, 3));
            indTrchDiGar.setCptMinScad(((short)0));
        }
        else {
            indTrchDiGar.setCptMinScad(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptRshMor() {
        return trchDiGar.getTgaCptRshMor().getTgaCptRshMor();
    }

    @Override
    public void setTgaCptRshMor(AfDecimal tgaCptRshMor) {
        this.trchDiGar.getTgaCptRshMor().setTgaCptRshMor(tgaCptRshMor.copy());
    }

    @Override
    public AfDecimal getTgaCptRshMorObj() {
        if (indTrchDiGar.getCptRshMor() >= 0) {
            return getTgaCptRshMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptRshMorObj(AfDecimal tgaCptRshMorObj) {
        if (tgaCptRshMorObj != null) {
            setTgaCptRshMor(new AfDecimal(tgaCptRshMorObj, 15, 3));
            indTrchDiGar.setCptRshMor(((short)0));
        }
        else {
            indTrchDiGar.setCptRshMor(((short)-1));
        }
    }

    @Override
    public char getTgaDsOperSql() {
        return trchDiGar.getTgaDsOperSql();
    }

    @Override
    public void setTgaDsOperSql(char tgaDsOperSql) {
        this.trchDiGar.setTgaDsOperSql(tgaDsOperSql);
    }

    @Override
    public long getTgaDsRiga() {
        return trchDiGar.getTgaDsRiga();
    }

    @Override
    public void setTgaDsRiga(long tgaDsRiga) {
        this.trchDiGar.setTgaDsRiga(tgaDsRiga);
    }

    @Override
    public char getTgaDsStatoElab() {
        return trchDiGar.getTgaDsStatoElab();
    }

    @Override
    public void setTgaDsStatoElab(char tgaDsStatoElab) {
        this.trchDiGar.setTgaDsStatoElab(tgaDsStatoElab);
    }

    @Override
    public long getTgaDsTsEndCptz() {
        return trchDiGar.getTgaDsTsEndCptz();
    }

    @Override
    public void setTgaDsTsEndCptz(long tgaDsTsEndCptz) {
        this.trchDiGar.setTgaDsTsEndCptz(tgaDsTsEndCptz);
    }

    @Override
    public long getTgaDsTsIniCptz() {
        return trchDiGar.getTgaDsTsIniCptz();
    }

    @Override
    public void setTgaDsTsIniCptz(long tgaDsTsIniCptz) {
        this.trchDiGar.setTgaDsTsIniCptz(tgaDsTsIniCptz);
    }

    @Override
    public String getTgaDsUtente() {
        return trchDiGar.getTgaDsUtente();
    }

    @Override
    public void setTgaDsUtente(String tgaDsUtente) {
        this.trchDiGar.setTgaDsUtente(tgaDsUtente);
    }

    @Override
    public int getTgaDsVer() {
        return trchDiGar.getTgaDsVer();
    }

    @Override
    public void setTgaDsVer(int tgaDsVer) {
        this.trchDiGar.setTgaDsVer(tgaDsVer);
    }

    @Override
    public String getTgaDtDecorDb() {
        return trchDiGarDb.getDecorDb();
    }

    @Override
    public void setTgaDtDecorDb(String tgaDtDecorDb) {
        this.trchDiGarDb.setDecorDb(tgaDtDecorDb);
    }

    @Override
    public String getTgaDtEffStabDb() {
        return trchDiGarDb.getEffStabDb();
    }

    @Override
    public void setTgaDtEffStabDb(String tgaDtEffStabDb) {
        this.trchDiGarDb.setEffStabDb(tgaDtEffStabDb);
    }

    @Override
    public String getTgaDtEffStabDbObj() {
        if (indTrchDiGar.getDtEffStab() >= 0) {
            return getTgaDtEffStabDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtEffStabDbObj(String tgaDtEffStabDbObj) {
        if (tgaDtEffStabDbObj != null) {
            setTgaDtEffStabDb(tgaDtEffStabDbObj);
            indTrchDiGar.setDtEffStab(((short)0));
        }
        else {
            indTrchDiGar.setDtEffStab(((short)-1));
        }
    }

    @Override
    public String getTgaDtEmisDb() {
        return trchDiGarDb.getEmisDb();
    }

    @Override
    public void setTgaDtEmisDb(String tgaDtEmisDb) {
        this.trchDiGarDb.setEmisDb(tgaDtEmisDb);
    }

    @Override
    public String getTgaDtEmisDbObj() {
        if (indTrchDiGar.getDtEmis() >= 0) {
            return getTgaDtEmisDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtEmisDbObj(String tgaDtEmisDbObj) {
        if (tgaDtEmisDbObj != null) {
            setTgaDtEmisDb(tgaDtEmisDbObj);
            indTrchDiGar.setDtEmis(((short)0));
        }
        else {
            indTrchDiGar.setDtEmis(((short)-1));
        }
    }

    @Override
    public String getTgaDtEndEffDb() {
        return trchDiGarDb.getEndEffDb();
    }

    @Override
    public void setTgaDtEndEffDb(String tgaDtEndEffDb) {
        this.trchDiGarDb.setEndEffDb(tgaDtEndEffDb);
    }

    @Override
    public String getTgaDtIniEffDb() {
        return trchDiGarDb.getIniEffDb();
    }

    @Override
    public void setTgaDtIniEffDb(String tgaDtIniEffDb) {
        this.trchDiGarDb.setIniEffDb(tgaDtIniEffDb);
    }

    @Override
    public String getTgaDtIniValTarDb() {
        return trchDiGarDb.getIniValTarDb();
    }

    @Override
    public void setTgaDtIniValTarDb(String tgaDtIniValTarDb) {
        this.trchDiGarDb.setIniValTarDb(tgaDtIniValTarDb);
    }

    @Override
    public String getTgaDtIniValTarDbObj() {
        if (indTrchDiGar.getDtIniValTar() >= 0) {
            return getTgaDtIniValTarDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtIniValTarDbObj(String tgaDtIniValTarDbObj) {
        if (tgaDtIniValTarDbObj != null) {
            setTgaDtIniValTarDb(tgaDtIniValTarDbObj);
            indTrchDiGar.setDtIniValTar(((short)0));
        }
        else {
            indTrchDiGar.setDtIniValTar(((short)-1));
        }
    }

    @Override
    public String getTgaDtScadDb() {
        return trchDiGarDb.getScadDb();
    }

    @Override
    public void setTgaDtScadDb(String tgaDtScadDb) {
        this.trchDiGarDb.setScadDb(tgaDtScadDb);
    }

    @Override
    public String getTgaDtScadDbObj() {
        if (indTrchDiGar.getDtScad() >= 0) {
            return getTgaDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtScadDbObj(String tgaDtScadDbObj) {
        if (tgaDtScadDbObj != null) {
            setTgaDtScadDb(tgaDtScadDbObj);
            indTrchDiGar.setDtScad(((short)0));
        }
        else {
            indTrchDiGar.setDtScad(((short)-1));
        }
    }

    @Override
    public String getTgaDtUltAdegPrePrDb() {
        return trchDiGarDb.getUltAdegPrePrDb();
    }

    @Override
    public void setTgaDtUltAdegPrePrDb(String tgaDtUltAdegPrePrDb) {
        this.trchDiGarDb.setUltAdegPrePrDb(tgaDtUltAdegPrePrDb);
    }

    @Override
    public String getTgaDtUltAdegPrePrDbObj() {
        if (indTrchDiGar.getDtUltAdegPrePr() >= 0) {
            return getTgaDtUltAdegPrePrDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtUltAdegPrePrDbObj(String tgaDtUltAdegPrePrDbObj) {
        if (tgaDtUltAdegPrePrDbObj != null) {
            setTgaDtUltAdegPrePrDb(tgaDtUltAdegPrePrDbObj);
            indTrchDiGar.setDtUltAdegPrePr(((short)0));
        }
        else {
            indTrchDiGar.setDtUltAdegPrePr(((short)-1));
        }
    }

    @Override
    public String getTgaDtVldtProdDb() {
        return trchDiGarDb.getVldtProdDb();
    }

    @Override
    public void setTgaDtVldtProdDb(String tgaDtVldtProdDb) {
        this.trchDiGarDb.setVldtProdDb(tgaDtVldtProdDb);
    }

    @Override
    public String getTgaDtVldtProdDbObj() {
        if (indTrchDiGar.getDtVldtProd() >= 0) {
            return getTgaDtVldtProdDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtVldtProdDbObj(String tgaDtVldtProdDbObj) {
        if (tgaDtVldtProdDbObj != null) {
            setTgaDtVldtProdDb(tgaDtVldtProdDbObj);
            indTrchDiGar.setDtVldtProd(((short)0));
        }
        else {
            indTrchDiGar.setDtVldtProd(((short)-1));
        }
    }

    @Override
    public int getTgaDurAa() {
        return trchDiGar.getTgaDurAa().getTgaDurAa();
    }

    @Override
    public void setTgaDurAa(int tgaDurAa) {
        this.trchDiGar.getTgaDurAa().setTgaDurAa(tgaDurAa);
    }

    @Override
    public Integer getTgaDurAaObj() {
        if (indTrchDiGar.getDurAa() >= 0) {
            return ((Integer)getTgaDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurAaObj(Integer tgaDurAaObj) {
        if (tgaDurAaObj != null) {
            setTgaDurAa(((int)tgaDurAaObj));
            indTrchDiGar.setDurAa(((short)0));
        }
        else {
            indTrchDiGar.setDurAa(((short)-1));
        }
    }

    @Override
    public int getTgaDurAbb() {
        return trchDiGar.getTgaDurAbb().getTgaDurAbb();
    }

    @Override
    public void setTgaDurAbb(int tgaDurAbb) {
        this.trchDiGar.getTgaDurAbb().setTgaDurAbb(tgaDurAbb);
    }

    @Override
    public Integer getTgaDurAbbObj() {
        if (indTrchDiGar.getDurAbb() >= 0) {
            return ((Integer)getTgaDurAbb());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurAbbObj(Integer tgaDurAbbObj) {
        if (tgaDurAbbObj != null) {
            setTgaDurAbb(((int)tgaDurAbbObj));
            indTrchDiGar.setDurAbb(((short)0));
        }
        else {
            indTrchDiGar.setDurAbb(((short)-1));
        }
    }

    @Override
    public int getTgaDurGg() {
        return trchDiGar.getTgaDurGg().getTgaDurGg();
    }

    @Override
    public void setTgaDurGg(int tgaDurGg) {
        this.trchDiGar.getTgaDurGg().setTgaDurGg(tgaDurGg);
    }

    @Override
    public Integer getTgaDurGgObj() {
        if (indTrchDiGar.getDurGg() >= 0) {
            return ((Integer)getTgaDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurGgObj(Integer tgaDurGgObj) {
        if (tgaDurGgObj != null) {
            setTgaDurGg(((int)tgaDurGgObj));
            indTrchDiGar.setDurGg(((short)0));
        }
        else {
            indTrchDiGar.setDurGg(((short)-1));
        }
    }

    @Override
    public int getTgaDurMm() {
        return trchDiGar.getTgaDurMm().getTgaDurMm();
    }

    @Override
    public void setTgaDurMm(int tgaDurMm) {
        this.trchDiGar.getTgaDurMm().setTgaDurMm(tgaDurMm);
    }

    @Override
    public Integer getTgaDurMmObj() {
        if (indTrchDiGar.getDurMm() >= 0) {
            return ((Integer)getTgaDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurMmObj(Integer tgaDurMmObj) {
        if (tgaDurMmObj != null) {
            setTgaDurMm(((int)tgaDurMmObj));
            indTrchDiGar.setDurMm(((short)0));
        }
        else {
            indTrchDiGar.setDurMm(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa1oAssto() {
        return trchDiGar.getTgaEtaAa1oAssto().getTgaEtaAa1oAssto();
    }

    @Override
    public void setTgaEtaAa1oAssto(short tgaEtaAa1oAssto) {
        this.trchDiGar.getTgaEtaAa1oAssto().setTgaEtaAa1oAssto(tgaEtaAa1oAssto);
    }

    @Override
    public Short getTgaEtaAa1oAsstoObj() {
        if (indTrchDiGar.getEtaAa1oAssto() >= 0) {
            return ((Short)getTgaEtaAa1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa1oAsstoObj(Short tgaEtaAa1oAsstoObj) {
        if (tgaEtaAa1oAsstoObj != null) {
            setTgaEtaAa1oAssto(((short)tgaEtaAa1oAsstoObj));
            indTrchDiGar.setEtaAa1oAssto(((short)0));
        }
        else {
            indTrchDiGar.setEtaAa1oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa2oAssto() {
        return trchDiGar.getTgaEtaAa2oAssto().getTgaEtaAa2oAssto();
    }

    @Override
    public void setTgaEtaAa2oAssto(short tgaEtaAa2oAssto) {
        this.trchDiGar.getTgaEtaAa2oAssto().setTgaEtaAa2oAssto(tgaEtaAa2oAssto);
    }

    @Override
    public Short getTgaEtaAa2oAsstoObj() {
        if (indTrchDiGar.getEtaAa2oAssto() >= 0) {
            return ((Short)getTgaEtaAa2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa2oAsstoObj(Short tgaEtaAa2oAsstoObj) {
        if (tgaEtaAa2oAsstoObj != null) {
            setTgaEtaAa2oAssto(((short)tgaEtaAa2oAsstoObj));
            indTrchDiGar.setEtaAa2oAssto(((short)0));
        }
        else {
            indTrchDiGar.setEtaAa2oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa3oAssto() {
        return trchDiGar.getTgaEtaAa3oAssto().getTgaEtaAa3oAssto();
    }

    @Override
    public void setTgaEtaAa3oAssto(short tgaEtaAa3oAssto) {
        this.trchDiGar.getTgaEtaAa3oAssto().setTgaEtaAa3oAssto(tgaEtaAa3oAssto);
    }

    @Override
    public Short getTgaEtaAa3oAsstoObj() {
        if (indTrchDiGar.getEtaAa3oAssto() >= 0) {
            return ((Short)getTgaEtaAa3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa3oAsstoObj(Short tgaEtaAa3oAsstoObj) {
        if (tgaEtaAa3oAsstoObj != null) {
            setTgaEtaAa3oAssto(((short)tgaEtaAa3oAsstoObj));
            indTrchDiGar.setEtaAa3oAssto(((short)0));
        }
        else {
            indTrchDiGar.setEtaAa3oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm1oAssto() {
        return trchDiGar.getTgaEtaMm1oAssto().getTgaEtaMm1oAssto();
    }

    @Override
    public void setTgaEtaMm1oAssto(short tgaEtaMm1oAssto) {
        this.trchDiGar.getTgaEtaMm1oAssto().setTgaEtaMm1oAssto(tgaEtaMm1oAssto);
    }

    @Override
    public Short getTgaEtaMm1oAsstoObj() {
        if (indTrchDiGar.getEtaMm1oAssto() >= 0) {
            return ((Short)getTgaEtaMm1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm1oAsstoObj(Short tgaEtaMm1oAsstoObj) {
        if (tgaEtaMm1oAsstoObj != null) {
            setTgaEtaMm1oAssto(((short)tgaEtaMm1oAsstoObj));
            indTrchDiGar.setEtaMm1oAssto(((short)0));
        }
        else {
            indTrchDiGar.setEtaMm1oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm2oAssto() {
        return trchDiGar.getTgaEtaMm2oAssto().getTgaEtaMm2oAssto();
    }

    @Override
    public void setTgaEtaMm2oAssto(short tgaEtaMm2oAssto) {
        this.trchDiGar.getTgaEtaMm2oAssto().setTgaEtaMm2oAssto(tgaEtaMm2oAssto);
    }

    @Override
    public Short getTgaEtaMm2oAsstoObj() {
        if (indTrchDiGar.getEtaMm2oAssto() >= 0) {
            return ((Short)getTgaEtaMm2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm2oAsstoObj(Short tgaEtaMm2oAsstoObj) {
        if (tgaEtaMm2oAsstoObj != null) {
            setTgaEtaMm2oAssto(((short)tgaEtaMm2oAsstoObj));
            indTrchDiGar.setEtaMm2oAssto(((short)0));
        }
        else {
            indTrchDiGar.setEtaMm2oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm3oAssto() {
        return trchDiGar.getTgaEtaMm3oAssto().getTgaEtaMm3oAssto();
    }

    @Override
    public void setTgaEtaMm3oAssto(short tgaEtaMm3oAssto) {
        this.trchDiGar.getTgaEtaMm3oAssto().setTgaEtaMm3oAssto(tgaEtaMm3oAssto);
    }

    @Override
    public Short getTgaEtaMm3oAsstoObj() {
        if (indTrchDiGar.getEtaMm3oAssto() >= 0) {
            return ((Short)getTgaEtaMm3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm3oAsstoObj(Short tgaEtaMm3oAsstoObj) {
        if (tgaEtaMm3oAsstoObj != null) {
            setTgaEtaMm3oAssto(((short)tgaEtaMm3oAsstoObj));
            indTrchDiGar.setEtaMm3oAssto(((short)0));
        }
        else {
            indTrchDiGar.setEtaMm3oAssto(((short)-1));
        }
    }

    @Override
    public char getTgaFlCarCont() {
        return trchDiGar.getTgaFlCarCont();
    }

    @Override
    public void setTgaFlCarCont(char tgaFlCarCont) {
        this.trchDiGar.setTgaFlCarCont(tgaFlCarCont);
    }

    @Override
    public Character getTgaFlCarContObj() {
        if (indTrchDiGar.getFlCarCont() >= 0) {
            return ((Character)getTgaFlCarCont());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlCarContObj(Character tgaFlCarContObj) {
        if (tgaFlCarContObj != null) {
            setTgaFlCarCont(((char)tgaFlCarContObj));
            indTrchDiGar.setFlCarCont(((short)0));
        }
        else {
            indTrchDiGar.setFlCarCont(((short)-1));
        }
    }

    @Override
    public char getTgaFlImportiForz() {
        return trchDiGar.getTgaFlImportiForz();
    }

    @Override
    public void setTgaFlImportiForz(char tgaFlImportiForz) {
        this.trchDiGar.setTgaFlImportiForz(tgaFlImportiForz);
    }

    @Override
    public Character getTgaFlImportiForzObj() {
        if (indTrchDiGar.getFlImportiForz() >= 0) {
            return ((Character)getTgaFlImportiForz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlImportiForzObj(Character tgaFlImportiForzObj) {
        if (tgaFlImportiForzObj != null) {
            setTgaFlImportiForz(((char)tgaFlImportiForzObj));
            indTrchDiGar.setFlImportiForz(((short)0));
        }
        else {
            indTrchDiGar.setFlImportiForz(((short)-1));
        }
    }

    @Override
    public char getTgaFlProvForz() {
        return trchDiGar.getTgaFlProvForz();
    }

    @Override
    public void setTgaFlProvForz(char tgaFlProvForz) {
        this.trchDiGar.setTgaFlProvForz(tgaFlProvForz);
    }

    @Override
    public Character getTgaFlProvForzObj() {
        if (indTrchDiGar.getFlProvForz() >= 0) {
            return ((Character)getTgaFlProvForz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlProvForzObj(Character tgaFlProvForzObj) {
        if (tgaFlProvForzObj != null) {
            setTgaFlProvForz(((char)tgaFlProvForzObj));
            indTrchDiGar.setFlProvForz(((short)0));
        }
        else {
            indTrchDiGar.setFlProvForz(((short)-1));
        }
    }

    @Override
    public String getTgaIbOgg() {
        return trchDiGar.getTgaIbOgg();
    }

    @Override
    public void setTgaIbOgg(String tgaIbOgg) {
        this.trchDiGar.setTgaIbOgg(tgaIbOgg);
    }

    @Override
    public String getTgaIbOggObj() {
        if (indTrchDiGar.getIbOgg() >= 0) {
            return getTgaIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIbOggObj(String tgaIbOggObj) {
        if (tgaIbOggObj != null) {
            setTgaIbOgg(tgaIbOggObj);
            indTrchDiGar.setIbOgg(((short)0));
        }
        else {
            indTrchDiGar.setIbOgg(((short)-1));
        }
    }

    @Override
    public int getTgaIdAdes() {
        return trchDiGar.getTgaIdAdes();
    }

    @Override
    public void setTgaIdAdes(int tgaIdAdes) {
        this.trchDiGar.setTgaIdAdes(tgaIdAdes);
    }

    @Override
    public int getTgaIdGar() {
        return trchDiGar.getTgaIdGar();
    }

    @Override
    public void setTgaIdGar(int tgaIdGar) {
        this.trchDiGar.setTgaIdGar(tgaIdGar);
    }

    @Override
    public int getTgaIdMoviChiu() {
        return trchDiGar.getTgaIdMoviChiu().getTgaIdMoviChiu();
    }

    @Override
    public void setTgaIdMoviChiu(int tgaIdMoviChiu) {
        this.trchDiGar.getTgaIdMoviChiu().setTgaIdMoviChiu(tgaIdMoviChiu);
    }

    @Override
    public Integer getTgaIdMoviChiuObj() {
        if (indTrchDiGar.getIdMoviChiu() >= 0) {
            return ((Integer)getTgaIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIdMoviChiuObj(Integer tgaIdMoviChiuObj) {
        if (tgaIdMoviChiuObj != null) {
            setTgaIdMoviChiu(((int)tgaIdMoviChiuObj));
            indTrchDiGar.setIdMoviChiu(((short)0));
        }
        else {
            indTrchDiGar.setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getTgaIdMoviCrz() {
        return trchDiGar.getTgaIdMoviCrz();
    }

    @Override
    public void setTgaIdMoviCrz(int tgaIdMoviCrz) {
        this.trchDiGar.setTgaIdMoviCrz(tgaIdMoviCrz);
    }

    @Override
    public int getTgaIdPoli() {
        return trchDiGar.getTgaIdPoli();
    }

    @Override
    public void setTgaIdPoli(int tgaIdPoli) {
        this.trchDiGar.setTgaIdPoli(tgaIdPoli);
    }

    @Override
    public int getTgaIdTrchDiGar() {
        return trchDiGar.getTgaIdTrchDiGar();
    }

    @Override
    public void setTgaIdTrchDiGar(int tgaIdTrchDiGar) {
        this.trchDiGar.setTgaIdTrchDiGar(tgaIdTrchDiGar);
    }

    @Override
    public AfDecimal getTgaImpAder() {
        return trchDiGar.getTgaImpAder().getTgaImpAder();
    }

    @Override
    public void setTgaImpAder(AfDecimal tgaImpAder) {
        this.trchDiGar.getTgaImpAder().setTgaImpAder(tgaImpAder.copy());
    }

    @Override
    public AfDecimal getTgaImpAderObj() {
        if (indTrchDiGar.getImpAder() >= 0) {
            return getTgaImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAderObj(AfDecimal tgaImpAderObj) {
        if (tgaImpAderObj != null) {
            setTgaImpAder(new AfDecimal(tgaImpAderObj, 15, 3));
            indTrchDiGar.setImpAder(((short)0));
        }
        else {
            indTrchDiGar.setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpAltSopr() {
        return trchDiGar.getTgaImpAltSopr().getTgaImpAltSopr();
    }

    @Override
    public void setTgaImpAltSopr(AfDecimal tgaImpAltSopr) {
        this.trchDiGar.getTgaImpAltSopr().setTgaImpAltSopr(tgaImpAltSopr.copy());
    }

    @Override
    public AfDecimal getTgaImpAltSoprObj() {
        if (indTrchDiGar.getImpAltSopr() >= 0) {
            return getTgaImpAltSopr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAltSoprObj(AfDecimal tgaImpAltSoprObj) {
        if (tgaImpAltSoprObj != null) {
            setTgaImpAltSopr(new AfDecimal(tgaImpAltSoprObj, 15, 3));
            indTrchDiGar.setImpAltSopr(((short)0));
        }
        else {
            indTrchDiGar.setImpAltSopr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpAz() {
        return trchDiGar.getTgaImpAz().getTgaImpAz();
    }

    @Override
    public void setTgaImpAz(AfDecimal tgaImpAz) {
        this.trchDiGar.getTgaImpAz().setTgaImpAz(tgaImpAz.copy());
    }

    @Override
    public AfDecimal getTgaImpAzObj() {
        if (indTrchDiGar.getImpAz() >= 0) {
            return getTgaImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAzObj(AfDecimal tgaImpAzObj) {
        if (tgaImpAzObj != null) {
            setTgaImpAz(new AfDecimal(tgaImpAzObj, 15, 3));
            indTrchDiGar.setImpAz(((short)0));
        }
        else {
            indTrchDiGar.setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpBns() {
        return trchDiGar.getTgaImpBns().getTgaImpBns();
    }

    @Override
    public AfDecimal getTgaImpBnsAntic() {
        return trchDiGar.getTgaImpBnsAntic().getTgaImpBnsAntic();
    }

    @Override
    public void setTgaImpBnsAntic(AfDecimal tgaImpBnsAntic) {
        this.trchDiGar.getTgaImpBnsAntic().setTgaImpBnsAntic(tgaImpBnsAntic.copy());
    }

    @Override
    public AfDecimal getTgaImpBnsAnticObj() {
        if (indTrchDiGar.getImpBnsAntic() >= 0) {
            return getTgaImpBnsAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpBnsAnticObj(AfDecimal tgaImpBnsAnticObj) {
        if (tgaImpBnsAnticObj != null) {
            setTgaImpBnsAntic(new AfDecimal(tgaImpBnsAnticObj, 15, 3));
            indTrchDiGar.setImpBnsAntic(((short)0));
        }
        else {
            indTrchDiGar.setImpBnsAntic(((short)-1));
        }
    }

    @Override
    public void setTgaImpBns(AfDecimal tgaImpBns) {
        this.trchDiGar.getTgaImpBns().setTgaImpBns(tgaImpBns.copy());
    }

    @Override
    public AfDecimal getTgaImpBnsObj() {
        if (indTrchDiGar.getImpBns() >= 0) {
            return getTgaImpBns();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpBnsObj(AfDecimal tgaImpBnsObj) {
        if (tgaImpBnsObj != null) {
            setTgaImpBns(new AfDecimal(tgaImpBnsObj, 15, 3));
            indTrchDiGar.setImpBns(((short)0));
        }
        else {
            indTrchDiGar.setImpBns(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarAcq() {
        return trchDiGar.getTgaImpCarAcq().getTgaImpCarAcq();
    }

    @Override
    public void setTgaImpCarAcq(AfDecimal tgaImpCarAcq) {
        this.trchDiGar.getTgaImpCarAcq().setTgaImpCarAcq(tgaImpCarAcq.copy());
    }

    @Override
    public AfDecimal getTgaImpCarAcqObj() {
        if (indTrchDiGar.getImpCarAcq() >= 0) {
            return getTgaImpCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarAcqObj(AfDecimal tgaImpCarAcqObj) {
        if (tgaImpCarAcqObj != null) {
            setTgaImpCarAcq(new AfDecimal(tgaImpCarAcqObj, 15, 3));
            indTrchDiGar.setImpCarAcq(((short)0));
        }
        else {
            indTrchDiGar.setImpCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarGest() {
        return trchDiGar.getTgaImpCarGest().getTgaImpCarGest();
    }

    @Override
    public void setTgaImpCarGest(AfDecimal tgaImpCarGest) {
        this.trchDiGar.getTgaImpCarGest().setTgaImpCarGest(tgaImpCarGest.copy());
    }

    @Override
    public AfDecimal getTgaImpCarGestObj() {
        if (indTrchDiGar.getImpCarGest() >= 0) {
            return getTgaImpCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarGestObj(AfDecimal tgaImpCarGestObj) {
        if (tgaImpCarGestObj != null) {
            setTgaImpCarGest(new AfDecimal(tgaImpCarGestObj, 15, 3));
            indTrchDiGar.setImpCarGest(((short)0));
        }
        else {
            indTrchDiGar.setImpCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarInc() {
        return trchDiGar.getTgaImpCarInc().getTgaImpCarInc();
    }

    @Override
    public void setTgaImpCarInc(AfDecimal tgaImpCarInc) {
        this.trchDiGar.getTgaImpCarInc().setTgaImpCarInc(tgaImpCarInc.copy());
    }

    @Override
    public AfDecimal getTgaImpCarIncObj() {
        if (indTrchDiGar.getImpCarInc() >= 0) {
            return getTgaImpCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarIncObj(AfDecimal tgaImpCarIncObj) {
        if (tgaImpCarIncObj != null) {
            setTgaImpCarInc(new AfDecimal(tgaImpCarIncObj, 15, 3));
            indTrchDiGar.setImpCarInc(((short)0));
        }
        else {
            indTrchDiGar.setImpCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpScon() {
        return trchDiGar.getTgaImpScon().getTgaImpScon();
    }

    @Override
    public void setTgaImpScon(AfDecimal tgaImpScon) {
        this.trchDiGar.getTgaImpScon().setTgaImpScon(tgaImpScon.copy());
    }

    @Override
    public AfDecimal getTgaImpSconObj() {
        if (indTrchDiGar.getImpScon() >= 0) {
            return getTgaImpScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSconObj(AfDecimal tgaImpSconObj) {
        if (tgaImpSconObj != null) {
            setTgaImpScon(new AfDecimal(tgaImpSconObj, 15, 3));
            indTrchDiGar.setImpScon(((short)0));
        }
        else {
            indTrchDiGar.setImpScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprProf() {
        return trchDiGar.getTgaImpSoprProf().getTgaImpSoprProf();
    }

    @Override
    public void setTgaImpSoprProf(AfDecimal tgaImpSoprProf) {
        this.trchDiGar.getTgaImpSoprProf().setTgaImpSoprProf(tgaImpSoprProf.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprProfObj() {
        if (indTrchDiGar.getImpSoprProf() >= 0) {
            return getTgaImpSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprProfObj(AfDecimal tgaImpSoprProfObj) {
        if (tgaImpSoprProfObj != null) {
            setTgaImpSoprProf(new AfDecimal(tgaImpSoprProfObj, 15, 3));
            indTrchDiGar.setImpSoprProf(((short)0));
        }
        else {
            indTrchDiGar.setImpSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprSan() {
        return trchDiGar.getTgaImpSoprSan().getTgaImpSoprSan();
    }

    @Override
    public void setTgaImpSoprSan(AfDecimal tgaImpSoprSan) {
        this.trchDiGar.getTgaImpSoprSan().setTgaImpSoprSan(tgaImpSoprSan.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprSanObj() {
        if (indTrchDiGar.getImpSoprSan() >= 0) {
            return getTgaImpSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprSanObj(AfDecimal tgaImpSoprSanObj) {
        if (tgaImpSoprSanObj != null) {
            setTgaImpSoprSan(new AfDecimal(tgaImpSoprSanObj, 15, 3));
            indTrchDiGar.setImpSoprSan(((short)0));
        }
        else {
            indTrchDiGar.setImpSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprSpo() {
        return trchDiGar.getTgaImpSoprSpo().getTgaImpSoprSpo();
    }

    @Override
    public void setTgaImpSoprSpo(AfDecimal tgaImpSoprSpo) {
        this.trchDiGar.getTgaImpSoprSpo().setTgaImpSoprSpo(tgaImpSoprSpo.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprSpoObj() {
        if (indTrchDiGar.getImpSoprSpo() >= 0) {
            return getTgaImpSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprSpoObj(AfDecimal tgaImpSoprSpoObj) {
        if (tgaImpSoprSpoObj != null) {
            setTgaImpSoprSpo(new AfDecimal(tgaImpSoprSpoObj, 15, 3));
            indTrchDiGar.setImpSoprSpo(((short)0));
        }
        else {
            indTrchDiGar.setImpSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprTec() {
        return trchDiGar.getTgaImpSoprTec().getTgaImpSoprTec();
    }

    @Override
    public void setTgaImpSoprTec(AfDecimal tgaImpSoprTec) {
        this.trchDiGar.getTgaImpSoprTec().setTgaImpSoprTec(tgaImpSoprTec.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprTecObj() {
        if (indTrchDiGar.getImpSoprTec() >= 0) {
            return getTgaImpSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprTecObj(AfDecimal tgaImpSoprTecObj) {
        if (tgaImpSoprTecObj != null) {
            setTgaImpSoprTec(new AfDecimal(tgaImpSoprTecObj, 15, 3));
            indTrchDiGar.setImpSoprTec(((short)0));
        }
        else {
            indTrchDiGar.setImpSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTfr() {
        return trchDiGar.getTgaImpTfr().getTgaImpTfr();
    }

    @Override
    public void setTgaImpTfr(AfDecimal tgaImpTfr) {
        this.trchDiGar.getTgaImpTfr().setTgaImpTfr(tgaImpTfr.copy());
    }

    @Override
    public AfDecimal getTgaImpTfrObj() {
        if (indTrchDiGar.getImpTfr() >= 0) {
            return getTgaImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTfrObj(AfDecimal tgaImpTfrObj) {
        if (tgaImpTfrObj != null) {
            setTgaImpTfr(new AfDecimal(tgaImpTfrObj, 15, 3));
            indTrchDiGar.setImpTfr(((short)0));
        }
        else {
            indTrchDiGar.setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTfrStrc() {
        return trchDiGar.getTgaImpTfrStrc().getTgaImpTfrStrc();
    }

    @Override
    public void setTgaImpTfrStrc(AfDecimal tgaImpTfrStrc) {
        this.trchDiGar.getTgaImpTfrStrc().setTgaImpTfrStrc(tgaImpTfrStrc.copy());
    }

    @Override
    public AfDecimal getTgaImpTfrStrcObj() {
        if (indTrchDiGar.getImpTfrStrc() >= 0) {
            return getTgaImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTfrStrcObj(AfDecimal tgaImpTfrStrcObj) {
        if (tgaImpTfrStrcObj != null) {
            setTgaImpTfrStrc(new AfDecimal(tgaImpTfrStrcObj, 15, 3));
            indTrchDiGar.setImpTfrStrc(((short)0));
        }
        else {
            indTrchDiGar.setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTrasfe() {
        return trchDiGar.getTgaImpTrasfe().getTgaImpTrasfe();
    }

    @Override
    public void setTgaImpTrasfe(AfDecimal tgaImpTrasfe) {
        this.trchDiGar.getTgaImpTrasfe().setTgaImpTrasfe(tgaImpTrasfe.copy());
    }

    @Override
    public AfDecimal getTgaImpTrasfeObj() {
        if (indTrchDiGar.getImpTrasfe() >= 0) {
            return getTgaImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTrasfeObj(AfDecimal tgaImpTrasfeObj) {
        if (tgaImpTrasfeObj != null) {
            setTgaImpTrasfe(new AfDecimal(tgaImpTrasfeObj, 15, 3));
            indTrchDiGar.setImpTrasfe(((short)0));
        }
        else {
            indTrchDiGar.setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpVolo() {
        return trchDiGar.getTgaImpVolo().getTgaImpVolo();
    }

    @Override
    public void setTgaImpVolo(AfDecimal tgaImpVolo) {
        this.trchDiGar.getTgaImpVolo().setTgaImpVolo(tgaImpVolo.copy());
    }

    @Override
    public AfDecimal getTgaImpVoloObj() {
        if (indTrchDiGar.getImpVolo() >= 0) {
            return getTgaImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpVoloObj(AfDecimal tgaImpVoloObj) {
        if (tgaImpVoloObj != null) {
            setTgaImpVolo(new AfDecimal(tgaImpVoloObj, 15, 3));
            indTrchDiGar.setImpVolo(((short)0));
        }
        else {
            indTrchDiGar.setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbCommisInter() {
        return trchDiGar.getTgaImpbCommisInter().getTgaImpbCommisInter();
    }

    @Override
    public void setTgaImpbCommisInter(AfDecimal tgaImpbCommisInter) {
        this.trchDiGar.getTgaImpbCommisInter().setTgaImpbCommisInter(tgaImpbCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaImpbCommisInterObj() {
        if (indTrchDiGar.getImpbCommisInter() >= 0) {
            return getTgaImpbCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbCommisInterObj(AfDecimal tgaImpbCommisInterObj) {
        if (tgaImpbCommisInterObj != null) {
            setTgaImpbCommisInter(new AfDecimal(tgaImpbCommisInterObj, 15, 3));
            indTrchDiGar.setImpbCommisInter(((short)0));
        }
        else {
            indTrchDiGar.setImpbCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvAcq() {
        return trchDiGar.getTgaImpbProvAcq().getTgaImpbProvAcq();
    }

    @Override
    public void setTgaImpbProvAcq(AfDecimal tgaImpbProvAcq) {
        this.trchDiGar.getTgaImpbProvAcq().setTgaImpbProvAcq(tgaImpbProvAcq.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvAcqObj() {
        if (indTrchDiGar.getImpbProvAcq() >= 0) {
            return getTgaImpbProvAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvAcqObj(AfDecimal tgaImpbProvAcqObj) {
        if (tgaImpbProvAcqObj != null) {
            setTgaImpbProvAcq(new AfDecimal(tgaImpbProvAcqObj, 15, 3));
            indTrchDiGar.setImpbProvAcq(((short)0));
        }
        else {
            indTrchDiGar.setImpbProvAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvInc() {
        return trchDiGar.getTgaImpbProvInc().getTgaImpbProvInc();
    }

    @Override
    public void setTgaImpbProvInc(AfDecimal tgaImpbProvInc) {
        this.trchDiGar.getTgaImpbProvInc().setTgaImpbProvInc(tgaImpbProvInc.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvIncObj() {
        if (indTrchDiGar.getImpbProvInc() >= 0) {
            return getTgaImpbProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvIncObj(AfDecimal tgaImpbProvIncObj) {
        if (tgaImpbProvIncObj != null) {
            setTgaImpbProvInc(new AfDecimal(tgaImpbProvIncObj, 15, 3));
            indTrchDiGar.setImpbProvInc(((short)0));
        }
        else {
            indTrchDiGar.setImpbProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvRicor() {
        return trchDiGar.getTgaImpbProvRicor().getTgaImpbProvRicor();
    }

    @Override
    public void setTgaImpbProvRicor(AfDecimal tgaImpbProvRicor) {
        this.trchDiGar.getTgaImpbProvRicor().setTgaImpbProvRicor(tgaImpbProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvRicorObj() {
        if (indTrchDiGar.getImpbProvRicor() >= 0) {
            return getTgaImpbProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvRicorObj(AfDecimal tgaImpbProvRicorObj) {
        if (tgaImpbProvRicorObj != null) {
            setTgaImpbProvRicor(new AfDecimal(tgaImpbProvRicorObj, 15, 3));
            indTrchDiGar.setImpbProvRicor(((short)0));
        }
        else {
            indTrchDiGar.setImpbProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbRemunAss() {
        return trchDiGar.getTgaImpbRemunAss().getTgaImpbRemunAss();
    }

    @Override
    public void setTgaImpbRemunAss(AfDecimal tgaImpbRemunAss) {
        this.trchDiGar.getTgaImpbRemunAss().setTgaImpbRemunAss(tgaImpbRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaImpbRemunAssObj() {
        if (indTrchDiGar.getImpbRemunAss() >= 0) {
            return getTgaImpbRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbRemunAssObj(AfDecimal tgaImpbRemunAssObj) {
        if (tgaImpbRemunAssObj != null) {
            setTgaImpbRemunAss(new AfDecimal(tgaImpbRemunAssObj, 15, 3));
            indTrchDiGar.setImpbRemunAss(((short)0));
        }
        else {
            indTrchDiGar.setImpbRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000() {
        return trchDiGar.getTgaImpbVisEnd2000().getTgaImpbVisEnd2000();
    }

    @Override
    public void setTgaImpbVisEnd2000(AfDecimal tgaImpbVisEnd2000) {
        this.trchDiGar.getTgaImpbVisEnd2000().setTgaImpbVisEnd2000(tgaImpbVisEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000Obj() {
        if (indTrchDiGar.getImpbVisEnd2000() >= 0) {
            return getTgaImpbVisEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbVisEnd2000Obj(AfDecimal tgaImpbVisEnd2000Obj) {
        if (tgaImpbVisEnd2000Obj != null) {
            setTgaImpbVisEnd2000(new AfDecimal(tgaImpbVisEnd2000Obj, 15, 3));
            indTrchDiGar.setImpbVisEnd2000(((short)0));
        }
        else {
            indTrchDiGar.setImpbVisEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIncrPre() {
        return trchDiGar.getTgaIncrPre().getTgaIncrPre();
    }

    @Override
    public void setTgaIncrPre(AfDecimal tgaIncrPre) {
        this.trchDiGar.getTgaIncrPre().setTgaIncrPre(tgaIncrPre.copy());
    }

    @Override
    public AfDecimal getTgaIncrPreObj() {
        if (indTrchDiGar.getIncrPre() >= 0) {
            return getTgaIncrPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIncrPreObj(AfDecimal tgaIncrPreObj) {
        if (tgaIncrPreObj != null) {
            setTgaIncrPre(new AfDecimal(tgaIncrPreObj, 15, 3));
            indTrchDiGar.setIncrPre(((short)0));
        }
        else {
            indTrchDiGar.setIncrPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIncrPrstz() {
        return trchDiGar.getTgaIncrPrstz().getTgaIncrPrstz();
    }

    @Override
    public void setTgaIncrPrstz(AfDecimal tgaIncrPrstz) {
        this.trchDiGar.getTgaIncrPrstz().setTgaIncrPrstz(tgaIncrPrstz.copy());
    }

    @Override
    public AfDecimal getTgaIncrPrstzObj() {
        if (indTrchDiGar.getIncrPrstz() >= 0) {
            return getTgaIncrPrstz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIncrPrstzObj(AfDecimal tgaIncrPrstzObj) {
        if (tgaIncrPrstzObj != null) {
            setTgaIncrPrstz(new AfDecimal(tgaIncrPrstzObj, 15, 3));
            indTrchDiGar.setIncrPrstz(((short)0));
        }
        else {
            indTrchDiGar.setIncrPrstz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIntrMora() {
        return trchDiGar.getTgaIntrMora().getTgaIntrMora();
    }

    @Override
    public void setTgaIntrMora(AfDecimal tgaIntrMora) {
        this.trchDiGar.getTgaIntrMora().setTgaIntrMora(tgaIntrMora.copy());
    }

    @Override
    public AfDecimal getTgaIntrMoraObj() {
        if (indTrchDiGar.getIntrMora() >= 0) {
            return getTgaIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIntrMoraObj(AfDecimal tgaIntrMoraObj) {
        if (tgaIntrMoraObj != null) {
            setTgaIntrMora(new AfDecimal(tgaIntrMoraObj, 15, 3));
            indTrchDiGar.setIntrMora(((short)0));
        }
        else {
            indTrchDiGar.setIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaManfeeAntic() {
        return trchDiGar.getTgaManfeeAntic().getTgaManfeeAntic();
    }

    @Override
    public void setTgaManfeeAntic(AfDecimal tgaManfeeAntic) {
        this.trchDiGar.getTgaManfeeAntic().setTgaManfeeAntic(tgaManfeeAntic.copy());
    }

    @Override
    public AfDecimal getTgaManfeeAnticObj() {
        if (indTrchDiGar.getManfeeAntic() >= 0) {
            return getTgaManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaManfeeAnticObj(AfDecimal tgaManfeeAnticObj) {
        if (tgaManfeeAnticObj != null) {
            setTgaManfeeAntic(new AfDecimal(tgaManfeeAnticObj, 15, 3));
            indTrchDiGar.setManfeeAntic(((short)0));
        }
        else {
            indTrchDiGar.setManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaManfeeRicor() {
        return trchDiGar.getTgaManfeeRicor().getTgaManfeeRicor();
    }

    @Override
    public void setTgaManfeeRicor(AfDecimal tgaManfeeRicor) {
        this.trchDiGar.getTgaManfeeRicor().setTgaManfeeRicor(tgaManfeeRicor.copy());
    }

    @Override
    public AfDecimal getTgaManfeeRicorObj() {
        if (indTrchDiGar.getManfeeRicor() >= 0) {
            return getTgaManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaManfeeRicorObj(AfDecimal tgaManfeeRicorObj) {
        if (tgaManfeeRicorObj != null) {
            setTgaManfeeRicor(new AfDecimal(tgaManfeeRicorObj, 15, 3));
            indTrchDiGar.setManfeeRicor(((short)0));
        }
        else {
            indTrchDiGar.setManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMatuEnd2000() {
        return trchDiGar.getTgaMatuEnd2000().getTgaMatuEnd2000();
    }

    @Override
    public void setTgaMatuEnd2000(AfDecimal tgaMatuEnd2000) {
        this.trchDiGar.getTgaMatuEnd2000().setTgaMatuEnd2000(tgaMatuEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaMatuEnd2000Obj() {
        if (indTrchDiGar.getMatuEnd2000() >= 0) {
            return getTgaMatuEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMatuEnd2000Obj(AfDecimal tgaMatuEnd2000Obj) {
        if (tgaMatuEnd2000Obj != null) {
            setTgaMatuEnd2000(new AfDecimal(tgaMatuEnd2000Obj, 15, 3));
            indTrchDiGar.setMatuEnd2000(((short)0));
        }
        else {
            indTrchDiGar.setMatuEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMinGarto() {
        return trchDiGar.getTgaMinGarto().getTgaMinGarto();
    }

    @Override
    public void setTgaMinGarto(AfDecimal tgaMinGarto) {
        this.trchDiGar.getTgaMinGarto().setTgaMinGarto(tgaMinGarto.copy());
    }

    @Override
    public AfDecimal getTgaMinGartoObj() {
        if (indTrchDiGar.getMinGarto() >= 0) {
            return getTgaMinGarto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMinGartoObj(AfDecimal tgaMinGartoObj) {
        if (tgaMinGartoObj != null) {
            setTgaMinGarto(new AfDecimal(tgaMinGartoObj, 14, 9));
            indTrchDiGar.setMinGarto(((short)0));
        }
        else {
            indTrchDiGar.setMinGarto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMinTrnut() {
        return trchDiGar.getTgaMinTrnut().getTgaMinTrnut();
    }

    @Override
    public void setTgaMinTrnut(AfDecimal tgaMinTrnut) {
        this.trchDiGar.getTgaMinTrnut().setTgaMinTrnut(tgaMinTrnut.copy());
    }

    @Override
    public AfDecimal getTgaMinTrnutObj() {
        if (indTrchDiGar.getMinTrnut() >= 0) {
            return getTgaMinTrnut();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMinTrnutObj(AfDecimal tgaMinTrnutObj) {
        if (tgaMinTrnutObj != null) {
            setTgaMinTrnut(new AfDecimal(tgaMinTrnutObj, 14, 9));
            indTrchDiGar.setMinTrnut(((short)0));
        }
        else {
            indTrchDiGar.setMinTrnut(((short)-1));
        }
    }

    @Override
    public String getTgaModCalc() {
        return trchDiGar.getTgaModCalc();
    }

    @Override
    public void setTgaModCalc(String tgaModCalc) {
        this.trchDiGar.setTgaModCalc(tgaModCalc);
    }

    @Override
    public String getTgaModCalcObj() {
        if (indTrchDiGar.getModCalc() >= 0) {
            return getTgaModCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaModCalcObj(String tgaModCalcObj) {
        if (tgaModCalcObj != null) {
            setTgaModCalc(tgaModCalcObj);
            indTrchDiGar.setModCalc(((short)0));
        }
        else {
            indTrchDiGar.setModCalc(((short)-1));
        }
    }

    @Override
    public int getTgaNumGgRival() {
        return trchDiGar.getTgaNumGgRival().getTgaNumGgRival();
    }

    @Override
    public void setTgaNumGgRival(int tgaNumGgRival) {
        this.trchDiGar.getTgaNumGgRival().setTgaNumGgRival(tgaNumGgRival);
    }

    @Override
    public Integer getTgaNumGgRivalObj() {
        if (indTrchDiGar.getNumGgRival() >= 0) {
            return ((Integer)getTgaNumGgRival());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaNumGgRivalObj(Integer tgaNumGgRivalObj) {
        if (tgaNumGgRivalObj != null) {
            setTgaNumGgRival(((int)tgaNumGgRivalObj));
            indTrchDiGar.setNumGgRival(((short)0));
        }
        else {
            indTrchDiGar.setNumGgRival(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaOldTsTec() {
        return trchDiGar.getTgaOldTsTec().getTgaOldTsTec();
    }

    @Override
    public void setTgaOldTsTec(AfDecimal tgaOldTsTec) {
        this.trchDiGar.getTgaOldTsTec().setTgaOldTsTec(tgaOldTsTec.copy());
    }

    @Override
    public AfDecimal getTgaOldTsTecObj() {
        if (indTrchDiGar.getOldTsTec() >= 0) {
            return getTgaOldTsTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaOldTsTecObj(AfDecimal tgaOldTsTecObj) {
        if (tgaOldTsTecObj != null) {
            setTgaOldTsTec(new AfDecimal(tgaOldTsTecObj, 14, 9));
            indTrchDiGar.setOldTsTec(((short)0));
        }
        else {
            indTrchDiGar.setOldTsTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcCommisGest() {
        return trchDiGar.getTgaPcCommisGest().getTgaPcCommisGest();
    }

    @Override
    public void setTgaPcCommisGest(AfDecimal tgaPcCommisGest) {
        this.trchDiGar.getTgaPcCommisGest().setTgaPcCommisGest(tgaPcCommisGest.copy());
    }

    @Override
    public AfDecimal getTgaPcCommisGestObj() {
        if (indTrchDiGar.getPcCommisGest() >= 0) {
            return getTgaPcCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcCommisGestObj(AfDecimal tgaPcCommisGestObj) {
        if (tgaPcCommisGestObj != null) {
            setTgaPcCommisGest(new AfDecimal(tgaPcCommisGestObj, 6, 3));
            indTrchDiGar.setPcCommisGest(((short)0));
        }
        else {
            indTrchDiGar.setPcCommisGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcIntrRiat() {
        return trchDiGar.getTgaPcIntrRiat().getTgaPcIntrRiat();
    }

    @Override
    public void setTgaPcIntrRiat(AfDecimal tgaPcIntrRiat) {
        this.trchDiGar.getTgaPcIntrRiat().setTgaPcIntrRiat(tgaPcIntrRiat.copy());
    }

    @Override
    public AfDecimal getTgaPcIntrRiatObj() {
        if (indTrchDiGar.getPcIntrRiat() >= 0) {
            return getTgaPcIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcIntrRiatObj(AfDecimal tgaPcIntrRiatObj) {
        if (tgaPcIntrRiatObj != null) {
            setTgaPcIntrRiat(new AfDecimal(tgaPcIntrRiatObj, 6, 3));
            indTrchDiGar.setPcIntrRiat(((short)0));
        }
        else {
            indTrchDiGar.setPcIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcRetr() {
        return trchDiGar.getTgaPcRetr().getTgaPcRetr();
    }

    @Override
    public void setTgaPcRetr(AfDecimal tgaPcRetr) {
        this.trchDiGar.getTgaPcRetr().setTgaPcRetr(tgaPcRetr.copy());
    }

    @Override
    public AfDecimal getTgaPcRetrObj() {
        if (indTrchDiGar.getPcRetr() >= 0) {
            return getTgaPcRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcRetrObj(AfDecimal tgaPcRetrObj) {
        if (tgaPcRetrObj != null) {
            setTgaPcRetr(new AfDecimal(tgaPcRetrObj, 6, 3));
            indTrchDiGar.setPcRetr(((short)0));
        }
        else {
            indTrchDiGar.setPcRetr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcRipPre() {
        return trchDiGar.getTgaPcRipPre().getTgaPcRipPre();
    }

    @Override
    public void setTgaPcRipPre(AfDecimal tgaPcRipPre) {
        this.trchDiGar.getTgaPcRipPre().setTgaPcRipPre(tgaPcRipPre.copy());
    }

    @Override
    public AfDecimal getTgaPcRipPreObj() {
        if (indTrchDiGar.getPcRipPre() >= 0) {
            return getTgaPcRipPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcRipPreObj(AfDecimal tgaPcRipPreObj) {
        if (tgaPcRipPreObj != null) {
            setTgaPcRipPre(new AfDecimal(tgaPcRipPreObj, 6, 3));
            indTrchDiGar.setPcRipPre(((short)0));
        }
        else {
            indTrchDiGar.setPcRipPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreAttDiTrch() {
        return trchDiGar.getTgaPreAttDiTrch().getTgaPreAttDiTrch();
    }

    @Override
    public void setTgaPreAttDiTrch(AfDecimal tgaPreAttDiTrch) {
        this.trchDiGar.getTgaPreAttDiTrch().setTgaPreAttDiTrch(tgaPreAttDiTrch.copy());
    }

    @Override
    public AfDecimal getTgaPreAttDiTrchObj() {
        if (indTrchDiGar.getPreAttDiTrch() >= 0) {
            return getTgaPreAttDiTrch();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreAttDiTrchObj(AfDecimal tgaPreAttDiTrchObj) {
        if (tgaPreAttDiTrchObj != null) {
            setTgaPreAttDiTrch(new AfDecimal(tgaPreAttDiTrchObj, 15, 3));
            indTrchDiGar.setPreAttDiTrch(((short)0));
        }
        else {
            indTrchDiGar.setPreAttDiTrch(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreCasoMor() {
        return trchDiGar.getTgaPreCasoMor().getTgaPreCasoMor();
    }

    @Override
    public void setTgaPreCasoMor(AfDecimal tgaPreCasoMor) {
        this.trchDiGar.getTgaPreCasoMor().setTgaPreCasoMor(tgaPreCasoMor.copy());
    }

    @Override
    public AfDecimal getTgaPreCasoMorObj() {
        if (indTrchDiGar.getPreCasoMor() >= 0) {
            return getTgaPreCasoMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreCasoMorObj(AfDecimal tgaPreCasoMorObj) {
        if (tgaPreCasoMorObj != null) {
            setTgaPreCasoMor(new AfDecimal(tgaPreCasoMorObj, 15, 3));
            indTrchDiGar.setPreCasoMor(((short)0));
        }
        else {
            indTrchDiGar.setPreCasoMor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreIniNet() {
        return trchDiGar.getTgaPreIniNet().getTgaPreIniNet();
    }

    @Override
    public void setTgaPreIniNet(AfDecimal tgaPreIniNet) {
        this.trchDiGar.getTgaPreIniNet().setTgaPreIniNet(tgaPreIniNet.copy());
    }

    @Override
    public AfDecimal getTgaPreIniNetObj() {
        if (indTrchDiGar.getPreIniNet() >= 0) {
            return getTgaPreIniNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreIniNetObj(AfDecimal tgaPreIniNetObj) {
        if (tgaPreIniNetObj != null) {
            setTgaPreIniNet(new AfDecimal(tgaPreIniNetObj, 15, 3));
            indTrchDiGar.setPreIniNet(((short)0));
        }
        else {
            indTrchDiGar.setPreIniNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreInvrioIni() {
        return trchDiGar.getTgaPreInvrioIni().getTgaPreInvrioIni();
    }

    @Override
    public void setTgaPreInvrioIni(AfDecimal tgaPreInvrioIni) {
        this.trchDiGar.getTgaPreInvrioIni().setTgaPreInvrioIni(tgaPreInvrioIni.copy());
    }

    @Override
    public AfDecimal getTgaPreInvrioIniObj() {
        if (indTrchDiGar.getPreInvrioIni() >= 0) {
            return getTgaPreInvrioIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreInvrioIniObj(AfDecimal tgaPreInvrioIniObj) {
        if (tgaPreInvrioIniObj != null) {
            setTgaPreInvrioIni(new AfDecimal(tgaPreInvrioIniObj, 15, 3));
            indTrchDiGar.setPreInvrioIni(((short)0));
        }
        else {
            indTrchDiGar.setPreInvrioIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreInvrioUlt() {
        return trchDiGar.getTgaPreInvrioUlt().getTgaPreInvrioUlt();
    }

    @Override
    public void setTgaPreInvrioUlt(AfDecimal tgaPreInvrioUlt) {
        this.trchDiGar.getTgaPreInvrioUlt().setTgaPreInvrioUlt(tgaPreInvrioUlt.copy());
    }

    @Override
    public AfDecimal getTgaPreInvrioUltObj() {
        if (indTrchDiGar.getPreInvrioUlt() >= 0) {
            return getTgaPreInvrioUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreInvrioUltObj(AfDecimal tgaPreInvrioUltObj) {
        if (tgaPreInvrioUltObj != null) {
            setTgaPreInvrioUlt(new AfDecimal(tgaPreInvrioUltObj, 15, 3));
            indTrchDiGar.setPreInvrioUlt(((short)0));
        }
        else {
            indTrchDiGar.setPreInvrioUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreLrd() {
        return trchDiGar.getTgaPreLrd().getTgaPreLrd();
    }

    @Override
    public void setTgaPreLrd(AfDecimal tgaPreLrd) {
        this.trchDiGar.getTgaPreLrd().setTgaPreLrd(tgaPreLrd.copy());
    }

    @Override
    public AfDecimal getTgaPreLrdObj() {
        if (indTrchDiGar.getPreLrd() >= 0) {
            return getTgaPreLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreLrdObj(AfDecimal tgaPreLrdObj) {
        if (tgaPreLrdObj != null) {
            setTgaPreLrd(new AfDecimal(tgaPreLrdObj, 15, 3));
            indTrchDiGar.setPreLrd(((short)0));
        }
        else {
            indTrchDiGar.setPreLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePattuito() {
        return trchDiGar.getTgaPrePattuito().getTgaPrePattuito();
    }

    @Override
    public void setTgaPrePattuito(AfDecimal tgaPrePattuito) {
        this.trchDiGar.getTgaPrePattuito().setTgaPrePattuito(tgaPrePattuito.copy());
    }

    @Override
    public AfDecimal getTgaPrePattuitoObj() {
        if (indTrchDiGar.getPrePattuito() >= 0) {
            return getTgaPrePattuito();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePattuitoObj(AfDecimal tgaPrePattuitoObj) {
        if (tgaPrePattuitoObj != null) {
            setTgaPrePattuito(new AfDecimal(tgaPrePattuitoObj, 15, 3));
            indTrchDiGar.setPrePattuito(((short)0));
        }
        else {
            indTrchDiGar.setPrePattuito(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePpIni() {
        return trchDiGar.getTgaPrePpIni().getTgaPrePpIni();
    }

    @Override
    public void setTgaPrePpIni(AfDecimal tgaPrePpIni) {
        this.trchDiGar.getTgaPrePpIni().setTgaPrePpIni(tgaPrePpIni.copy());
    }

    @Override
    public AfDecimal getTgaPrePpIniObj() {
        if (indTrchDiGar.getPrePpIni() >= 0) {
            return getTgaPrePpIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePpIniObj(AfDecimal tgaPrePpIniObj) {
        if (tgaPrePpIniObj != null) {
            setTgaPrePpIni(new AfDecimal(tgaPrePpIniObj, 15, 3));
            indTrchDiGar.setPrePpIni(((short)0));
        }
        else {
            indTrchDiGar.setPrePpIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePpUlt() {
        return trchDiGar.getTgaPrePpUlt().getTgaPrePpUlt();
    }

    @Override
    public void setTgaPrePpUlt(AfDecimal tgaPrePpUlt) {
        this.trchDiGar.getTgaPrePpUlt().setTgaPrePpUlt(tgaPrePpUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrePpUltObj() {
        if (indTrchDiGar.getPrePpUlt() >= 0) {
            return getTgaPrePpUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePpUltObj(AfDecimal tgaPrePpUltObj) {
        if (tgaPrePpUltObj != null) {
            setTgaPrePpUlt(new AfDecimal(tgaPrePpUltObj, 15, 3));
            indTrchDiGar.setPrePpUlt(((short)0));
        }
        else {
            indTrchDiGar.setPrePpUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreRivto() {
        return trchDiGar.getTgaPreRivto().getTgaPreRivto();
    }

    @Override
    public void setTgaPreRivto(AfDecimal tgaPreRivto) {
        this.trchDiGar.getTgaPreRivto().setTgaPreRivto(tgaPreRivto.copy());
    }

    @Override
    public AfDecimal getTgaPreRivtoObj() {
        if (indTrchDiGar.getPreRivto() >= 0) {
            return getTgaPreRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreRivtoObj(AfDecimal tgaPreRivtoObj) {
        if (tgaPreRivtoObj != null) {
            setTgaPreRivto(new AfDecimal(tgaPreRivtoObj, 15, 3));
            indTrchDiGar.setPreRivto(((short)0));
        }
        else {
            indTrchDiGar.setPreRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreStab() {
        return trchDiGar.getTgaPreStab().getTgaPreStab();
    }

    @Override
    public void setTgaPreStab(AfDecimal tgaPreStab) {
        this.trchDiGar.getTgaPreStab().setTgaPreStab(tgaPreStab.copy());
    }

    @Override
    public AfDecimal getTgaPreStabObj() {
        if (indTrchDiGar.getPreStab() >= 0) {
            return getTgaPreStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreStabObj(AfDecimal tgaPreStabObj) {
        if (tgaPreStabObj != null) {
            setTgaPreStab(new AfDecimal(tgaPreStabObj, 15, 3));
            indTrchDiGar.setPreStab(((short)0));
        }
        else {
            indTrchDiGar.setPreStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreTariIni() {
        return trchDiGar.getTgaPreTariIni().getTgaPreTariIni();
    }

    @Override
    public void setTgaPreTariIni(AfDecimal tgaPreTariIni) {
        this.trchDiGar.getTgaPreTariIni().setTgaPreTariIni(tgaPreTariIni.copy());
    }

    @Override
    public AfDecimal getTgaPreTariIniObj() {
        if (indTrchDiGar.getPreTariIni() >= 0) {
            return getTgaPreTariIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreTariIniObj(AfDecimal tgaPreTariIniObj) {
        if (tgaPreTariIniObj != null) {
            setTgaPreTariIni(new AfDecimal(tgaPreTariIniObj, 15, 3));
            indTrchDiGar.setPreTariIni(((short)0));
        }
        else {
            indTrchDiGar.setPreTariIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreTariUlt() {
        return trchDiGar.getTgaPreTariUlt().getTgaPreTariUlt();
    }

    @Override
    public void setTgaPreTariUlt(AfDecimal tgaPreTariUlt) {
        this.trchDiGar.getTgaPreTariUlt().setTgaPreTariUlt(tgaPreTariUlt.copy());
    }

    @Override
    public AfDecimal getTgaPreTariUltObj() {
        if (indTrchDiGar.getPreTariUlt() >= 0) {
            return getTgaPreTariUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreTariUltObj(AfDecimal tgaPreTariUltObj) {
        if (tgaPreTariUltObj != null) {
            setTgaPreTariUlt(new AfDecimal(tgaPreTariUltObj, 15, 3));
            indTrchDiGar.setPreTariUlt(((short)0));
        }
        else {
            indTrchDiGar.setPreTariUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreUniRivto() {
        return trchDiGar.getTgaPreUniRivto().getTgaPreUniRivto();
    }

    @Override
    public void setTgaPreUniRivto(AfDecimal tgaPreUniRivto) {
        this.trchDiGar.getTgaPreUniRivto().setTgaPreUniRivto(tgaPreUniRivto.copy());
    }

    @Override
    public AfDecimal getTgaPreUniRivtoObj() {
        if (indTrchDiGar.getPreUniRivto() >= 0) {
            return getTgaPreUniRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreUniRivtoObj(AfDecimal tgaPreUniRivtoObj) {
        if (tgaPreUniRivtoObj != null) {
            setTgaPreUniRivto(new AfDecimal(tgaPreUniRivtoObj, 15, 3));
            indTrchDiGar.setPreUniRivto(((short)0));
        }
        else {
            indTrchDiGar.setPreUniRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProv1aaAcq() {
        return trchDiGar.getTgaProv1aaAcq().getTgaProv1aaAcq();
    }

    @Override
    public void setTgaProv1aaAcq(AfDecimal tgaProv1aaAcq) {
        this.trchDiGar.getTgaProv1aaAcq().setTgaProv1aaAcq(tgaProv1aaAcq.copy());
    }

    @Override
    public AfDecimal getTgaProv1aaAcqObj() {
        if (indTrchDiGar.getProv1aaAcq() >= 0) {
            return getTgaProv1aaAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProv1aaAcqObj(AfDecimal tgaProv1aaAcqObj) {
        if (tgaProv1aaAcqObj != null) {
            setTgaProv1aaAcq(new AfDecimal(tgaProv1aaAcqObj, 15, 3));
            indTrchDiGar.setProv1aaAcq(((short)0));
        }
        else {
            indTrchDiGar.setProv1aaAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProv2aaAcq() {
        return trchDiGar.getTgaProv2aaAcq().getTgaProv2aaAcq();
    }

    @Override
    public void setTgaProv2aaAcq(AfDecimal tgaProv2aaAcq) {
        this.trchDiGar.getTgaProv2aaAcq().setTgaProv2aaAcq(tgaProv2aaAcq.copy());
    }

    @Override
    public AfDecimal getTgaProv2aaAcqObj() {
        if (indTrchDiGar.getProv2aaAcq() >= 0) {
            return getTgaProv2aaAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProv2aaAcqObj(AfDecimal tgaProv2aaAcqObj) {
        if (tgaProv2aaAcqObj != null) {
            setTgaProv2aaAcq(new AfDecimal(tgaProv2aaAcqObj, 15, 3));
            indTrchDiGar.setProv2aaAcq(((short)0));
        }
        else {
            indTrchDiGar.setProv2aaAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProvInc() {
        return trchDiGar.getTgaProvInc().getTgaProvInc();
    }

    @Override
    public void setTgaProvInc(AfDecimal tgaProvInc) {
        this.trchDiGar.getTgaProvInc().setTgaProvInc(tgaProvInc.copy());
    }

    @Override
    public AfDecimal getTgaProvIncObj() {
        if (indTrchDiGar.getProvInc() >= 0) {
            return getTgaProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProvIncObj(AfDecimal tgaProvIncObj) {
        if (tgaProvIncObj != null) {
            setTgaProvInc(new AfDecimal(tgaProvIncObj, 15, 3));
            indTrchDiGar.setProvInc(((short)0));
        }
        else {
            indTrchDiGar.setProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProvRicor() {
        return trchDiGar.getTgaProvRicor().getTgaProvRicor();
    }

    @Override
    public void setTgaProvRicor(AfDecimal tgaProvRicor) {
        this.trchDiGar.getTgaProvRicor().setTgaProvRicor(tgaProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaProvRicorObj() {
        if (indTrchDiGar.getProvRicor() >= 0) {
            return getTgaProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProvRicorObj(AfDecimal tgaProvRicorObj) {
        if (tgaProvRicorObj != null) {
            setTgaProvRicor(new AfDecimal(tgaProvRicorObj, 15, 3));
            indTrchDiGar.setProvRicor(((short)0));
        }
        else {
            indTrchDiGar.setProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzAggIni() {
        return trchDiGar.getTgaPrstzAggIni().getTgaPrstzAggIni();
    }

    @Override
    public void setTgaPrstzAggIni(AfDecimal tgaPrstzAggIni) {
        this.trchDiGar.getTgaPrstzAggIni().setTgaPrstzAggIni(tgaPrstzAggIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzAggIniObj() {
        if (indTrchDiGar.getPrstzAggIni() >= 0) {
            return getTgaPrstzAggIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzAggIniObj(AfDecimal tgaPrstzAggIniObj) {
        if (tgaPrstzAggIniObj != null) {
            setTgaPrstzAggIni(new AfDecimal(tgaPrstzAggIniObj, 15, 3));
            indTrchDiGar.setPrstzAggIni(((short)0));
        }
        else {
            indTrchDiGar.setPrstzAggIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzAggUlt() {
        return trchDiGar.getTgaPrstzAggUlt().getTgaPrstzAggUlt();
    }

    @Override
    public void setTgaPrstzAggUlt(AfDecimal tgaPrstzAggUlt) {
        this.trchDiGar.getTgaPrstzAggUlt().setTgaPrstzAggUlt(tgaPrstzAggUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrstzAggUltObj() {
        if (indTrchDiGar.getPrstzAggUlt() >= 0) {
            return getTgaPrstzAggUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzAggUltObj(AfDecimal tgaPrstzAggUltObj) {
        if (tgaPrstzAggUltObj != null) {
            setTgaPrstzAggUlt(new AfDecimal(tgaPrstzAggUltObj, 15, 3));
            indTrchDiGar.setPrstzAggUlt(((short)0));
        }
        else {
            indTrchDiGar.setPrstzAggUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIni() {
        return trchDiGar.getTgaPrstzIni().getTgaPrstzIni();
    }

    @Override
    public void setTgaPrstzIni(AfDecimal tgaPrstzIni) {
        this.trchDiGar.getTgaPrstzIni().setTgaPrstzIni(tgaPrstzIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfis() {
        return trchDiGar.getTgaPrstzIniNewfis().getTgaPrstzIniNewfis();
    }

    @Override
    public void setTgaPrstzIniNewfis(AfDecimal tgaPrstzIniNewfis) {
        this.trchDiGar.getTgaPrstzIniNewfis().setTgaPrstzIniNewfis(tgaPrstzIniNewfis.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfisObj() {
        if (indTrchDiGar.getPrstzIniNewfis() >= 0) {
            return getTgaPrstzIniNewfis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniNewfisObj(AfDecimal tgaPrstzIniNewfisObj) {
        if (tgaPrstzIniNewfisObj != null) {
            setTgaPrstzIniNewfis(new AfDecimal(tgaPrstzIniNewfisObj, 15, 3));
            indTrchDiGar.setPrstzIniNewfis(((short)0));
        }
        else {
            indTrchDiGar.setPrstzIniNewfis(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniNforz() {
        return trchDiGar.getTgaPrstzIniNforz().getTgaPrstzIniNforz();
    }

    @Override
    public void setTgaPrstzIniNforz(AfDecimal tgaPrstzIniNforz) {
        this.trchDiGar.getTgaPrstzIniNforz().setTgaPrstzIniNforz(tgaPrstzIniNforz.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNforzObj() {
        if (indTrchDiGar.getPrstzIniNforz() >= 0) {
            return getTgaPrstzIniNforz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniNforzObj(AfDecimal tgaPrstzIniNforzObj) {
        if (tgaPrstzIniNforzObj != null) {
            setTgaPrstzIniNforz(new AfDecimal(tgaPrstzIniNforzObj, 15, 3));
            indTrchDiGar.setPrstzIniNforz(((short)0));
        }
        else {
            indTrchDiGar.setPrstzIniNforz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniObj() {
        if (indTrchDiGar.getPrstzIni() >= 0) {
            return getTgaPrstzIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniObj(AfDecimal tgaPrstzIniObj) {
        if (tgaPrstzIniObj != null) {
            setTgaPrstzIni(new AfDecimal(tgaPrstzIniObj, 15, 3));
            indTrchDiGar.setPrstzIni(((short)0));
        }
        else {
            indTrchDiGar.setPrstzIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniStab() {
        return trchDiGar.getTgaPrstzIniStab().getTgaPrstzIniStab();
    }

    @Override
    public void setTgaPrstzIniStab(AfDecimal tgaPrstzIniStab) {
        this.trchDiGar.getTgaPrstzIniStab().setTgaPrstzIniStab(tgaPrstzIniStab.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniStabObj() {
        if (indTrchDiGar.getPrstzIniStab() >= 0) {
            return getTgaPrstzIniStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniStabObj(AfDecimal tgaPrstzIniStabObj) {
        if (tgaPrstzIniStabObj != null) {
            setTgaPrstzIniStab(new AfDecimal(tgaPrstzIniStabObj, 15, 3));
            indTrchDiGar.setPrstzIniStab(((short)0));
        }
        else {
            indTrchDiGar.setPrstzIniStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzRidIni() {
        return trchDiGar.getTgaPrstzRidIni().getTgaPrstzRidIni();
    }

    @Override
    public void setTgaPrstzRidIni(AfDecimal tgaPrstzRidIni) {
        this.trchDiGar.getTgaPrstzRidIni().setTgaPrstzRidIni(tgaPrstzRidIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzRidIniObj() {
        if (indTrchDiGar.getPrstzRidIni() >= 0) {
            return getTgaPrstzRidIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzRidIniObj(AfDecimal tgaPrstzRidIniObj) {
        if (tgaPrstzRidIniObj != null) {
            setTgaPrstzRidIni(new AfDecimal(tgaPrstzRidIniObj, 15, 3));
            indTrchDiGar.setPrstzRidIni(((short)0));
        }
        else {
            indTrchDiGar.setPrstzRidIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzUlt() {
        return trchDiGar.getTgaPrstzUlt().getTgaPrstzUlt();
    }

    @Override
    public void setTgaPrstzUlt(AfDecimal tgaPrstzUlt) {
        this.trchDiGar.getTgaPrstzUlt().setTgaPrstzUlt(tgaPrstzUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrstzUltObj() {
        if (indTrchDiGar.getPrstzUlt() >= 0) {
            return getTgaPrstzUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzUltObj(AfDecimal tgaPrstzUltObj) {
        if (tgaPrstzUltObj != null) {
            setTgaPrstzUlt(new AfDecimal(tgaPrstzUltObj, 15, 3));
            indTrchDiGar.setPrstzUlt(((short)0));
        }
        else {
            indTrchDiGar.setPrstzUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRatLrd() {
        return trchDiGar.getTgaRatLrd().getTgaRatLrd();
    }

    @Override
    public void setTgaRatLrd(AfDecimal tgaRatLrd) {
        this.trchDiGar.getTgaRatLrd().setTgaRatLrd(tgaRatLrd.copy());
    }

    @Override
    public AfDecimal getTgaRatLrdObj() {
        if (indTrchDiGar.getRatLrd() >= 0) {
            return getTgaRatLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRatLrdObj(AfDecimal tgaRatLrdObj) {
        if (tgaRatLrdObj != null) {
            setTgaRatLrd(new AfDecimal(tgaRatLrdObj, 15, 3));
            indTrchDiGar.setRatLrd(((short)0));
        }
        else {
            indTrchDiGar.setRatLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRemunAss() {
        return trchDiGar.getTgaRemunAss().getTgaRemunAss();
    }

    @Override
    public void setTgaRemunAss(AfDecimal tgaRemunAss) {
        this.trchDiGar.getTgaRemunAss().setTgaRemunAss(tgaRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaRemunAssObj() {
        if (indTrchDiGar.getRemunAss() >= 0) {
            return getTgaRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRemunAssObj(AfDecimal tgaRemunAssObj) {
        if (tgaRemunAssObj != null) {
            setTgaRemunAss(new AfDecimal(tgaRemunAssObj, 15, 3));
            indTrchDiGar.setRemunAss(((short)0));
        }
        else {
            indTrchDiGar.setRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0() {
        return trchDiGar.getTgaRenIniTsTec0().getTgaRenIniTsTec0();
    }

    @Override
    public void setTgaRenIniTsTec0(AfDecimal tgaRenIniTsTec0) {
        this.trchDiGar.getTgaRenIniTsTec0().setTgaRenIniTsTec0(tgaRenIniTsTec0.copy());
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0Obj() {
        if (indTrchDiGar.getRenIniTsTec0() >= 0) {
            return getTgaRenIniTsTec0();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRenIniTsTec0Obj(AfDecimal tgaRenIniTsTec0Obj) {
        if (tgaRenIniTsTec0Obj != null) {
            setTgaRenIniTsTec0(new AfDecimal(tgaRenIniTsTec0Obj, 15, 3));
            indTrchDiGar.setRenIniTsTec0(((short)0));
        }
        else {
            indTrchDiGar.setRenIniTsTec0(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRendtoLrd() {
        return trchDiGar.getTgaRendtoLrd().getTgaRendtoLrd();
    }

    @Override
    public void setTgaRendtoLrd(AfDecimal tgaRendtoLrd) {
        this.trchDiGar.getTgaRendtoLrd().setTgaRendtoLrd(tgaRendtoLrd.copy());
    }

    @Override
    public AfDecimal getTgaRendtoLrdObj() {
        if (indTrchDiGar.getRendtoLrd() >= 0) {
            return getTgaRendtoLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRendtoLrdObj(AfDecimal tgaRendtoLrdObj) {
        if (tgaRendtoLrdObj != null) {
            setTgaRendtoLrd(new AfDecimal(tgaRendtoLrdObj, 14, 9));
            indTrchDiGar.setRendtoLrd(((short)0));
        }
        else {
            indTrchDiGar.setRendtoLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRendtoRetr() {
        return trchDiGar.getTgaRendtoRetr().getTgaRendtoRetr();
    }

    @Override
    public void setTgaRendtoRetr(AfDecimal tgaRendtoRetr) {
        this.trchDiGar.getTgaRendtoRetr().setTgaRendtoRetr(tgaRendtoRetr.copy());
    }

    @Override
    public AfDecimal getTgaRendtoRetrObj() {
        if (indTrchDiGar.getRendtoRetr() >= 0) {
            return getTgaRendtoRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRendtoRetrObj(AfDecimal tgaRendtoRetrObj) {
        if (tgaRendtoRetrObj != null) {
            setTgaRendtoRetr(new AfDecimal(tgaRendtoRetrObj, 14, 9));
            indTrchDiGar.setRendtoRetr(((short)0));
        }
        else {
            indTrchDiGar.setRendtoRetr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRisMat() {
        return trchDiGar.getTgaRisMat().getTgaRisMat();
    }

    @Override
    public void setTgaRisMat(AfDecimal tgaRisMat) {
        this.trchDiGar.getTgaRisMat().setTgaRisMat(tgaRisMat.copy());
    }

    @Override
    public AfDecimal getTgaRisMatObj() {
        if (indTrchDiGar.getRisMat() >= 0) {
            return getTgaRisMat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRisMatObj(AfDecimal tgaRisMatObj) {
        if (tgaRisMatObj != null) {
            setTgaRisMat(new AfDecimal(tgaRisMatObj, 15, 3));
            indTrchDiGar.setRisMat(((short)0));
        }
        else {
            indTrchDiGar.setRisMat(((short)-1));
        }
    }

    @Override
    public char getTgaTpAdegAbb() {
        return trchDiGar.getTgaTpAdegAbb();
    }

    @Override
    public void setTgaTpAdegAbb(char tgaTpAdegAbb) {
        this.trchDiGar.setTgaTpAdegAbb(tgaTpAdegAbb);
    }

    @Override
    public Character getTgaTpAdegAbbObj() {
        if (indTrchDiGar.getTpAdegAbb() >= 0) {
            return ((Character)getTgaTpAdegAbb());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpAdegAbbObj(Character tgaTpAdegAbbObj) {
        if (tgaTpAdegAbbObj != null) {
            setTgaTpAdegAbb(((char)tgaTpAdegAbbObj));
            indTrchDiGar.setTpAdegAbb(((short)0));
        }
        else {
            indTrchDiGar.setTpAdegAbb(((short)-1));
        }
    }

    @Override
    public String getTgaTpManfeeAppl() {
        return trchDiGar.getTgaTpManfeeAppl();
    }

    @Override
    public void setTgaTpManfeeAppl(String tgaTpManfeeAppl) {
        this.trchDiGar.setTgaTpManfeeAppl(tgaTpManfeeAppl);
    }

    @Override
    public String getTgaTpManfeeApplObj() {
        if (indTrchDiGar.getTpManfeeAppl() >= 0) {
            return getTgaTpManfeeAppl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpManfeeApplObj(String tgaTpManfeeApplObj) {
        if (tgaTpManfeeApplObj != null) {
            setTgaTpManfeeAppl(tgaTpManfeeApplObj);
            indTrchDiGar.setTpManfeeAppl(((short)0));
        }
        else {
            indTrchDiGar.setTpManfeeAppl(((short)-1));
        }
    }

    @Override
    public String getTgaTpRgmFisc() {
        return trchDiGar.getTgaTpRgmFisc();
    }

    @Override
    public void setTgaTpRgmFisc(String tgaTpRgmFisc) {
        this.trchDiGar.setTgaTpRgmFisc(tgaTpRgmFisc);
    }

    @Override
    public String getTgaTpRival() {
        return trchDiGar.getTgaTpRival();
    }

    @Override
    public void setTgaTpRival(String tgaTpRival) {
        this.trchDiGar.setTgaTpRival(tgaTpRival);
    }

    @Override
    public String getTgaTpRivalObj() {
        if (indTrchDiGar.getTpRival() >= 0) {
            return getTgaTpRival();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpRivalObj(String tgaTpRivalObj) {
        if (tgaTpRivalObj != null) {
            setTgaTpRival(tgaTpRivalObj);
            indTrchDiGar.setTpRival(((short)0));
        }
        else {
            indTrchDiGar.setTpRival(((short)-1));
        }
    }

    @Override
    public String getTgaTpTrch() {
        return trchDiGar.getTgaTpTrch();
    }

    @Override
    public void setTgaTpTrch(String tgaTpTrch) {
        this.trchDiGar.setTgaTpTrch(tgaTpTrch);
    }

    @Override
    public AfDecimal getTgaTsRivalFis() {
        return trchDiGar.getTgaTsRivalFis().getTgaTsRivalFis();
    }

    @Override
    public void setTgaTsRivalFis(AfDecimal tgaTsRivalFis) {
        this.trchDiGar.getTgaTsRivalFis().setTgaTsRivalFis(tgaTsRivalFis.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalFisObj() {
        if (indTrchDiGar.getTsRivalFis() >= 0) {
            return getTgaTsRivalFis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalFisObj(AfDecimal tgaTsRivalFisObj) {
        if (tgaTsRivalFisObj != null) {
            setTgaTsRivalFis(new AfDecimal(tgaTsRivalFisObj, 14, 9));
            indTrchDiGar.setTsRivalFis(((short)0));
        }
        else {
            indTrchDiGar.setTsRivalFis(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaTsRivalIndiciz() {
        return trchDiGar.getTgaTsRivalIndiciz().getTgaTsRivalIndiciz();
    }

    @Override
    public void setTgaTsRivalIndiciz(AfDecimal tgaTsRivalIndiciz) {
        this.trchDiGar.getTgaTsRivalIndiciz().setTgaTsRivalIndiciz(tgaTsRivalIndiciz.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalIndicizObj() {
        if (indTrchDiGar.getTsRivalIndiciz() >= 0) {
            return getTgaTsRivalIndiciz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalIndicizObj(AfDecimal tgaTsRivalIndicizObj) {
        if (tgaTsRivalIndicizObj != null) {
            setTgaTsRivalIndiciz(new AfDecimal(tgaTsRivalIndicizObj, 14, 9));
            indTrchDiGar.setTsRivalIndiciz(((short)0));
        }
        else {
            indTrchDiGar.setTsRivalIndiciz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaTsRivalNet() {
        return trchDiGar.getTgaTsRivalNet().getTgaTsRivalNet();
    }

    @Override
    public void setTgaTsRivalNet(AfDecimal tgaTsRivalNet) {
        this.trchDiGar.getTgaTsRivalNet().setTgaTsRivalNet(tgaTsRivalNet.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalNetObj() {
        if (indTrchDiGar.getTsRivalNet() >= 0) {
            return getTgaTsRivalNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalNetObj(AfDecimal tgaTsRivalNetObj) {
        if (tgaTsRivalNetObj != null) {
            setTgaTsRivalNet(new AfDecimal(tgaTsRivalNetObj, 14, 9));
            indTrchDiGar.setTsRivalNet(((short)0));
        }
        else {
            indTrchDiGar.setTsRivalNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaVisEnd2000() {
        return trchDiGar.getTgaVisEnd2000().getTgaVisEnd2000();
    }

    @Override
    public void setTgaVisEnd2000(AfDecimal tgaVisEnd2000) {
        this.trchDiGar.getTgaVisEnd2000().setTgaVisEnd2000(tgaVisEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaVisEnd2000Nforz() {
        return trchDiGar.getTgaVisEnd2000Nforz().getTgaVisEnd2000Nforz();
    }

    @Override
    public void setTgaVisEnd2000Nforz(AfDecimal tgaVisEnd2000Nforz) {
        this.trchDiGar.getTgaVisEnd2000Nforz().setTgaVisEnd2000Nforz(tgaVisEnd2000Nforz.copy());
    }

    @Override
    public AfDecimal getTgaVisEnd2000NforzObj() {
        if (indTrchDiGar.getVisEnd2000Nforz() >= 0) {
            return getTgaVisEnd2000Nforz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaVisEnd2000NforzObj(AfDecimal tgaVisEnd2000NforzObj) {
        if (tgaVisEnd2000NforzObj != null) {
            setTgaVisEnd2000Nforz(new AfDecimal(tgaVisEnd2000NforzObj, 15, 3));
            indTrchDiGar.setVisEnd2000Nforz(((short)0));
        }
        else {
            indTrchDiGar.setVisEnd2000Nforz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaVisEnd2000Obj() {
        if (indTrchDiGar.getVisEnd2000() >= 0) {
            return getTgaVisEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaVisEnd2000Obj(AfDecimal tgaVisEnd2000Obj) {
        if (tgaVisEnd2000Obj != null) {
            setTgaVisEnd2000(new AfDecimal(tgaVisEnd2000Obj, 15, 3));
            indTrchDiGar.setVisEnd2000(((short)0));
        }
        else {
            indTrchDiGar.setVisEnd2000(((short)-1));
        }
    }

    public TrchDiGarIvvs0216 getTrchDiGar() {
        return trchDiGar;
    }

    public TrchDiGarDb getTrchDiGarDb() {
        return trchDiGarDb;
    }

    @Override
    public int getWkIdAdesA() {
        throw new FieldNotMappedException("wkIdAdesA");
    }

    @Override
    public void setWkIdAdesA(int wkIdAdesA) {
        throw new FieldNotMappedException("wkIdAdesA");
    }

    @Override
    public int getWkIdAdesDa() {
        throw new FieldNotMappedException("wkIdAdesDa");
    }

    @Override
    public void setWkIdAdesDa(int wkIdAdesDa) {
        throw new FieldNotMappedException("wkIdAdesDa");
    }

    @Override
    public int getWkIdGarA() {
        throw new FieldNotMappedException("wkIdGarA");
    }

    @Override
    public void setWkIdGarA(int wkIdGarA) {
        throw new FieldNotMappedException("wkIdGarA");
    }

    @Override
    public int getWkIdGarDa() {
        throw new FieldNotMappedException("wkIdGarDa");
    }

    @Override
    public void setWkIdGarDa(int wkIdGarDa) {
        throw new FieldNotMappedException("wkIdGarDa");
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public String getWsDtInfinito1() {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
