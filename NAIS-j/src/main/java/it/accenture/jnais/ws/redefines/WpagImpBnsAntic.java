package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-BNS-ANTIC<br>
 * Variable: WPAG-IMP-BNS-ANTIC from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpBnsAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpBnsAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_BNS_ANTIC;
    }

    public void setWpagImpBnsAntic(AfDecimal wpagImpBnsAntic) {
        writeDecimalAsPacked(Pos.WPAG_IMP_BNS_ANTIC, wpagImpBnsAntic.copy());
    }

    public void setWpagImpBnsAnticFormatted(String wpagImpBnsAntic) {
        setWpagImpBnsAntic(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_BNS_ANTIC + Len.Fract.WPAG_IMP_BNS_ANTIC, Len.Fract.WPAG_IMP_BNS_ANTIC, wpagImpBnsAntic));
    }

    public void setWpagImpBnsAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_BNS_ANTIC, Pos.WPAG_IMP_BNS_ANTIC);
    }

    /**Original name: WPAG-IMP-BNS-ANTIC<br>*/
    public AfDecimal getWpagImpBnsAntic() {
        return readPackedAsDecimal(Pos.WPAG_IMP_BNS_ANTIC, Len.Int.WPAG_IMP_BNS_ANTIC, Len.Fract.WPAG_IMP_BNS_ANTIC);
    }

    public byte[] getWpagImpBnsAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_BNS_ANTIC, Pos.WPAG_IMP_BNS_ANTIC);
        return buffer;
    }

    public void initWpagImpBnsAnticSpaces() {
        fill(Pos.WPAG_IMP_BNS_ANTIC, Len.WPAG_IMP_BNS_ANTIC, Types.SPACE_CHAR);
    }

    public void setWpagImpBnsAnticNull(String wpagImpBnsAnticNull) {
        writeString(Pos.WPAG_IMP_BNS_ANTIC_NULL, wpagImpBnsAnticNull, Len.WPAG_IMP_BNS_ANTIC_NULL);
    }

    /**Original name: WPAG-IMP-BNS-ANTIC-NULL<br>*/
    public String getWpagImpBnsAnticNull() {
        return readString(Pos.WPAG_IMP_BNS_ANTIC_NULL, Len.WPAG_IMP_BNS_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_BNS_ANTIC = 1;
        public static final int WPAG_IMP_BNS_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_BNS_ANTIC = 8;
        public static final int WPAG_IMP_BNS_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_BNS_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_BNS_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
