package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-RIS-ZIL-T<br>
 * Variable: WB03-RIS-ZIL-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03RisZilT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03RisZilT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_RIS_ZIL_T;
    }

    public void setWb03RisZilT(AfDecimal wb03RisZilT) {
        writeDecimalAsPacked(Pos.WB03_RIS_ZIL_T, wb03RisZilT.copy());
    }

    public void setWb03RisZilTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_RIS_ZIL_T, Pos.WB03_RIS_ZIL_T);
    }

    /**Original name: WB03-RIS-ZIL-T<br>*/
    public AfDecimal getWb03RisZilT() {
        return readPackedAsDecimal(Pos.WB03_RIS_ZIL_T, Len.Int.WB03_RIS_ZIL_T, Len.Fract.WB03_RIS_ZIL_T);
    }

    public byte[] getWb03RisZilTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_RIS_ZIL_T, Pos.WB03_RIS_ZIL_T);
        return buffer;
    }

    public void setWb03RisZilTNull(String wb03RisZilTNull) {
        writeString(Pos.WB03_RIS_ZIL_T_NULL, wb03RisZilTNull, Len.WB03_RIS_ZIL_T_NULL);
    }

    /**Original name: WB03-RIS-ZIL-T-NULL<br>*/
    public String getWb03RisZilTNull() {
        return readString(Pos.WB03_RIS_ZIL_T_NULL, Len.WB03_RIS_ZIL_T_NULL);
    }

    public String getWb03RisZilTNullFormatted() {
        return Functions.padBlanks(getWb03RisZilTNull(), Len.WB03_RIS_ZIL_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_RIS_ZIL_T = 1;
        public static final int WB03_RIS_ZIL_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_RIS_ZIL_T = 8;
        public static final int WB03_RIS_ZIL_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_RIS_ZIL_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_RIS_ZIL_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
