package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.modernsystems.ctu.data.NumericDisplay;

import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Ldbv7851;
import it.accenture.jnais.copy.Movi;
import it.accenture.jnais.ws.enums.FlagMovimento;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.occurs.WtgaTabTran;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0003<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0003Data {

    //==== PROPERTIES ====
    public static final int DTGA_TAB_TRAN_MAXOCCURS = 20;
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: PGM-LDBSE060<br>
	 * <pre>--  Accesso MOVI</pre>*/
    private String pgmLdbse060 = "LDBSE060";
    //Original name: WK-PGM-CALLED
    private String wkPgmCalled = "";
    //Original name: WK-DATA-OUTPUT
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-MOV-DT-EFF
    private int wkMovDtEff = DefaultValues.INT_VAL;
    //Original name: WK-MOV-DS-TS-CPTZ
    private long wkMovDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WK-DATA-OUT
    private String wkDataOut = "00000000";
    /**Original name: FLAG-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG
	 * ----------------------------------------------------------------*</pre>*/
    private FlagMovimento flagMovimento = new FlagMovimento();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *   COPY TIPOLOGICHE
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    /**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    //Original name: POLI
    private PoliIdbspol0 poli = new PoliIdbspol0();
    //Original name: ADES
    private Ades ades = new Ades();
    //Original name: MOVI
    private Movi movi = new Movi();
    //Original name: PARAM-MOVI
    private ParamMoviLdbs1470 paramMovi = new ParamMoviLdbs1470();
    //Original name: LDBVE061
    private Ldbve061 ldbve061 = new Ldbve061();
    //Original name: LDBV7851
    private Ldbv7851 ldbv7851 = new Ldbv7851();
    //Original name: INPUT-LVVS0000
    private InputLvvs0000 inputLvvs0000 = new InputLvvs0000();
    //Original name: DTGA-ELE-TRCH-MAX
    private short dtgaEleTrchMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTGA-TAB-TRAN
    private WtgaTabTran[] dtgaTabTran = new WtgaTabTran[DTGA_TAB_TRAN_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0003Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dtgaTabTranIdx = 1; dtgaTabTranIdx <= DTGA_TAB_TRAN_MAXOCCURS; dtgaTabTranIdx++) {
            dtgaTabTran[dtgaTabTranIdx - 1] = new WtgaTabTran();
        }
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public String getPgmLdbse060() {
        return this.pgmLdbse060;
    }

    public void setWkPgmCalled(String wkPgmCalled) {
        this.wkPgmCalled = Functions.subString(wkPgmCalled, Len.WK_PGM_CALLED);
    }

    public String getWkPgmCalled() {
        return this.wkPgmCalled;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setWkMovDtEff(int wkMovDtEff) {
        this.wkMovDtEff = wkMovDtEff;
    }

    public int getWkMovDtEff() {
        return this.wkMovDtEff;
    }

    public String getWkMovDtEffFormatted() {
        return PicFormatter.display(new PicParams("S9(8)V").setUsage(PicUsage.PACKED)).format(getWkMovDtEff()).toString();
    }

    public void setWkMovDsTsCptz(long wkMovDsTsCptz) {
        this.wkMovDsTsCptz = wkMovDsTsCptz;
    }

    public long getWkMovDsTsCptz() {
        return this.wkMovDsTsCptz;
    }

    public void setWkDataOut(int wkDataOut) {
        this.wkDataOut = NumericDisplay.asString(wkDataOut, Len.WK_DATA_OUT);
    }

    public int getWkDataOut() {
        return NumericDisplay.asInt(this.wkDataOut);
    }

    public String getWkDataOutFormatted() {
        return this.wkDataOut;
    }

    public void setDtgaAreaTrchFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TRCH];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TRCH);
        setDtgaAreaTrchBytes(buffer, 1);
    }

    public void setDtgaAreaTrchBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleTrchMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DTGA_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dtgaTabTran[idx - 1].setTabTranBytes(buffer, position);
                position += WtgaTabTran.Len.TAB_TRAN;
            }
            else {
                dtgaTabTran[idx - 1].initTabTranSpaces();
                position += WtgaTabTran.Len.TAB_TRAN;
            }
        }
    }

    public void setDtgaEleTrchMax(short dtgaEleTrchMax) {
        this.dtgaEleTrchMax = dtgaEleTrchMax;
    }

    public short getDtgaEleTrchMax() {
        return this.dtgaEleTrchMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public Ades getAdes() {
        return ades;
    }

    public WtgaTabTran getDtgaTabTran(int idx) {
        return dtgaTabTran[idx - 1];
    }

    public FlagMovimento getFlagMovimento() {
        return flagMovimento;
    }

    public InputLvvs0000 getInputLvvs0000() {
        return inputLvvs0000;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Ldbv7851 getLdbv7851() {
        return ldbv7851;
    }

    public Ldbve061 getLdbve061() {
        return ldbve061;
    }

    public Movi getMovi() {
        return movi;
    }

    public ParamMoviLdbs1470 getParamMovi() {
        return paramMovi;
    }

    public PoliIdbspol0 getPoli() {
        return poli;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_LABEL = 30;
        public static final int WK_DATA_OUT = 8;
        public static final int WK_PGM_CALLED = 8;
        public static final int DTGA_ELE_TRCH_MAX = 2;
        public static final int DTGA_AREA_TRCH = DTGA_ELE_TRCH_MAX + Lvvs0003Data.DTGA_TAB_TRAN_MAXOCCURS * WtgaTabTran.Len.TAB_TRAN;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
