package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-ALQ-TFR<br>
 * Variable: WDFA-ALQ-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaAlqTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaAlqTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_ALQ_TFR;
    }

    public void setWdfaAlqTfr(AfDecimal wdfaAlqTfr) {
        writeDecimalAsPacked(Pos.WDFA_ALQ_TFR, wdfaAlqTfr.copy());
    }

    public void setWdfaAlqTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_ALQ_TFR, Pos.WDFA_ALQ_TFR);
    }

    /**Original name: WDFA-ALQ-TFR<br>*/
    public AfDecimal getWdfaAlqTfr() {
        return readPackedAsDecimal(Pos.WDFA_ALQ_TFR, Len.Int.WDFA_ALQ_TFR, Len.Fract.WDFA_ALQ_TFR);
    }

    public byte[] getWdfaAlqTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_ALQ_TFR, Pos.WDFA_ALQ_TFR);
        return buffer;
    }

    public void setWdfaAlqTfrNull(String wdfaAlqTfrNull) {
        writeString(Pos.WDFA_ALQ_TFR_NULL, wdfaAlqTfrNull, Len.WDFA_ALQ_TFR_NULL);
    }

    /**Original name: WDFA-ALQ-TFR-NULL<br>*/
    public String getWdfaAlqTfrNull() {
        return readString(Pos.WDFA_ALQ_TFR_NULL, Len.WDFA_ALQ_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_ALQ_TFR = 1;
        public static final int WDFA_ALQ_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_ALQ_TFR = 4;
        public static final int WDFA_ALQ_TFR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_ALQ_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_ALQ_TFR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
