package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTSC-ELAB-CL<br>
 * Variable: WPCO-DT-ULTSC-ELAB-CL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltscElabCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltscElabCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTSC_ELAB_CL;
    }

    public void setWpcoDtUltscElabCl(int wpcoDtUltscElabCl) {
        writeIntAsPacked(Pos.WPCO_DT_ULTSC_ELAB_CL, wpcoDtUltscElabCl, Len.Int.WPCO_DT_ULTSC_ELAB_CL);
    }

    public void setDpcoDtUltscElabClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTSC_ELAB_CL, Pos.WPCO_DT_ULTSC_ELAB_CL);
    }

    /**Original name: WPCO-DT-ULTSC-ELAB-CL<br>*/
    public int getWpcoDtUltscElabCl() {
        return readPackedAsInt(Pos.WPCO_DT_ULTSC_ELAB_CL, Len.Int.WPCO_DT_ULTSC_ELAB_CL);
    }

    public byte[] getWpcoDtUltscElabClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTSC_ELAB_CL, Pos.WPCO_DT_ULTSC_ELAB_CL);
        return buffer;
    }

    public void setWpcoDtUltscElabClNull(String wpcoDtUltscElabClNull) {
        writeString(Pos.WPCO_DT_ULTSC_ELAB_CL_NULL, wpcoDtUltscElabClNull, Len.WPCO_DT_ULTSC_ELAB_CL_NULL);
    }

    /**Original name: WPCO-DT-ULTSC-ELAB-CL-NULL<br>*/
    public String getWpcoDtUltscElabClNull() {
        return readString(Pos.WPCO_DT_ULTSC_ELAB_CL_NULL, Len.WPCO_DT_ULTSC_ELAB_CL_NULL);
    }

    public String getWpcoDtUltscElabClNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltscElabClNull(), Len.WPCO_DT_ULTSC_ELAB_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTSC_ELAB_CL = 1;
        public static final int WPCO_DT_ULTSC_ELAB_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTSC_ELAB_CL = 5;
        public static final int WPCO_DT_ULTSC_ELAB_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTSC_ELAB_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
