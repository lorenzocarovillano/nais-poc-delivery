package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RPTO-PRE<br>
 * Variable: WRST-RPTO-PRE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRptoPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRptoPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RPTO_PRE;
    }

    public void setWrstRptoPre(AfDecimal wrstRptoPre) {
        writeDecimalAsPacked(Pos.WRST_RPTO_PRE, wrstRptoPre.copy());
    }

    public void setWrstRptoPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RPTO_PRE, Pos.WRST_RPTO_PRE);
    }

    /**Original name: WRST-RPTO-PRE<br>*/
    public AfDecimal getWrstRptoPre() {
        return readPackedAsDecimal(Pos.WRST_RPTO_PRE, Len.Int.WRST_RPTO_PRE, Len.Fract.WRST_RPTO_PRE);
    }

    public byte[] getWrstRptoPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RPTO_PRE, Pos.WRST_RPTO_PRE);
        return buffer;
    }

    public void initWrstRptoPreSpaces() {
        fill(Pos.WRST_RPTO_PRE, Len.WRST_RPTO_PRE, Types.SPACE_CHAR);
    }

    public void setWrstRptoPreNull(String wrstRptoPreNull) {
        writeString(Pos.WRST_RPTO_PRE_NULL, wrstRptoPreNull, Len.WRST_RPTO_PRE_NULL);
    }

    /**Original name: WRST-RPTO-PRE-NULL<br>*/
    public String getWrstRptoPreNull() {
        return readString(Pos.WRST_RPTO_PRE_NULL, Len.WRST_RPTO_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RPTO_PRE = 1;
        public static final int WRST_RPTO_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RPTO_PRE = 8;
        public static final int WRST_RPTO_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RPTO_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RPTO_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
