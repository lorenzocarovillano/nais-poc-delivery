package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvade1Lvvs0010;
import it.accenture.jnais.copy.WadeDati;
import it.accenture.jnais.ws.enums.WkPrestito;
import it.accenture.jnais.ws.enums.WkTpPrestito;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.occurs.WtitTabTitCont;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0740<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0740Data {

    //==== PROPERTIES ====
    public static final int DTIT_TAB_TIT_CONT_MAXOCCURS = 200;
    //Original name: WK-APPO-DATA
    private int wkAppoData = 0;
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    //Original name: WK-PRESTITO
    private WkPrestito wkPrestito = new WkPrestito();
    //Original name: WK-DT-MAX
    private int wkDtMax = DefaultValues.INT_VAL;
    //Original name: DADE-ELE-ADES-MAX
    private short dadeEleAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVADE1
    private Lccvade1Lvvs0010 lccvade1 = new Lccvade1Lvvs0010();
    //Original name: DTIT-TAB-TIT-CONT
    private WtitTabTitCont[] dtitTabTitCont = new WtitTabTitCont[DTIT_TAB_TIT_CONT_MAXOCCURS];
    //Original name: WK-TP-PRESTITO
    private WkTpPrestito wkTpPrestito = new WkTpPrestito();
    //Original name: PREST
    private PrestIdbspre0 prest = new PrestIdbspre0();
    //Original name: TIT-CONT
    private TitContIdbstit0 titCont = new TitContIdbstit0();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-INDICI
    private IxIndiciLvvs0740 ixIndici = new IxIndiciLvvs0740();

    //==== CONSTRUCTORS ====
    public Lvvs0740Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dtitTabTitContIdx = 1; dtitTabTitContIdx <= DTIT_TAB_TIT_CONT_MAXOCCURS; dtitTabTitContIdx++) {
            dtitTabTitCont[dtitTabTitContIdx - 1] = new WtitTabTitCont();
        }
    }

    public void setWkAppoData(int wkAppoData) {
        this.wkAppoData = wkAppoData;
    }

    public int getWkAppoData() {
        return this.wkAppoData;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkDtMax(int wkDtMax) {
        this.wkDtMax = wkDtMax;
    }

    public int getWkDtMax() {
        return this.wkDtMax;
    }

    public void setDadeAreaAdesFormatted(String data) {
        byte[] buffer = new byte[Len.DADE_AREA_ADES];
        MarshalByte.writeString(buffer, 1, data, Len.DADE_AREA_ADES);
        setDadeAreaAdesBytes(buffer, 1);
    }

    public void setDadeAreaAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        dadeEleAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDadeTabAdesBytes(buffer, position);
    }

    public void setDadeEleAdesMax(short dadeEleAdesMax) {
        this.dadeEleAdesMax = dadeEleAdesMax;
    }

    public short getDadeEleAdesMax() {
        return this.dadeEleAdesMax;
    }

    public void setDadeTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvade1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvade1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvade1Lvvs0010.Len.Int.ID_PTF, 0));
        position += Lccvade1Lvvs0010.Len.ID_PTF;
        lccvade1.getDati().setDatiBytes(buffer, position);
    }

    public WtitTabTitCont getDtitTabTitCont(int idx) {
        return dtitTabTitCont[idx - 1];
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndiciLvvs0740 getIxIndici() {
        return ixIndici;
    }

    public Lccvade1Lvvs0010 getLccvade1() {
        return lccvade1;
    }

    public PrestIdbspre0 getPrest() {
        return prest;
    }

    public TitContIdbstit0 getTitCont() {
        return titCont;
    }

    public WkPrestito getWkPrestito() {
        return wkPrestito;
    }

    public WkTpPrestito getWkTpPrestito() {
        return wkTpPrestito;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_APPO_DATA = 8;
        public static final int WK_DT_MAX = 8;
        public static final int WK_CALL_PGM = 8;
        public static final int DADE_ELE_ADES_MAX = 2;
        public static final int DADE_TAB_ADES = WpolStatus.Len.STATUS + Lccvade1Lvvs0010.Len.ID_PTF + WadeDati.Len.DATI;
        public static final int DADE_AREA_ADES = DADE_ELE_ADES_MAX + DADE_TAB_ADES;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
