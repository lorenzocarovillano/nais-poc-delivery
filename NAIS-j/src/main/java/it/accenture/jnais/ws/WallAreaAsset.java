package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.ptr.WallTabella;

/**Original name: WALL-AREA-ASSET<br>
 * Variable: WALL-AREA-ASSET from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WallAreaAsset extends SerializableParameter {

    //==== PROPERTIES ====
    /**Original name: WALL-ELE-ASSET-ALL-MAX<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY 7 PER LA GESTIONE DELLE OCCURS
	 * ----------------------------------------------------------------*</pre>*/
    private short eleAssetAllMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WALL-TABELLA
    private WallTabella tabella = new WallTabella();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WALL_AREA_ASSET;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWallAreaAssetBytes(buf);
    }

    public void setWallAreaAssetBytes(byte[] buffer) {
        setWallAreaAssetBytes(buffer, 1);
    }

    public byte[] getWallAreaAssetBytes() {
        byte[] buffer = new byte[Len.WALL_AREA_ASSET];
        return getWallAreaAssetBytes(buffer, 1);
    }

    public void setWallAreaAssetBytes(byte[] buffer, int offset) {
        int position = offset;
        eleAssetAllMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        tabella.setTabellaBytes(buffer, position);
    }

    public byte[] getWallAreaAssetBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleAssetAllMax);
        position += Types.SHORT_SIZE;
        tabella.getTabellaBytes(buffer, position);
        return buffer;
    }

    public void setEleAssetAllMax(short eleAssetAllMax) {
        this.eleAssetAllMax = eleAssetAllMax;
    }

    public short getEleAssetAllMax() {
        return this.eleAssetAllMax;
    }

    public WallTabella getTabella() {
        return tabella;
    }

    @Override
    public byte[] serialize() {
        return getWallAreaAssetBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_ASSET_ALL_MAX = 2;
        public static final int WALL_AREA_ASSET = ELE_ASSET_ALL_MAX + WallTabella.Len.TABELLA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
