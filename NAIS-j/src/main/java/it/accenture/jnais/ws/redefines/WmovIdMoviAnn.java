package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WMOV-ID-MOVI-ANN<br>
 * Variable: WMOV-ID-MOVI-ANN from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmovIdMoviAnn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmovIdMoviAnn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMOV_ID_MOVI_ANN;
    }

    public void setWmovIdMoviAnn(int wmovIdMoviAnn) {
        writeIntAsPacked(Pos.WMOV_ID_MOVI_ANN, wmovIdMoviAnn, Len.Int.WMOV_ID_MOVI_ANN);
    }

    public void setWmovIdMoviAnnFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMOV_ID_MOVI_ANN, Pos.WMOV_ID_MOVI_ANN);
    }

    /**Original name: WMOV-ID-MOVI-ANN<br>*/
    public int getWmovIdMoviAnn() {
        return readPackedAsInt(Pos.WMOV_ID_MOVI_ANN, Len.Int.WMOV_ID_MOVI_ANN);
    }

    public byte[] getWmovIdMoviAnnAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMOV_ID_MOVI_ANN, Pos.WMOV_ID_MOVI_ANN);
        return buffer;
    }

    public void setWmovIdMoviAnnNull(String wmovIdMoviAnnNull) {
        writeString(Pos.WMOV_ID_MOVI_ANN_NULL, wmovIdMoviAnnNull, Len.WMOV_ID_MOVI_ANN_NULL);
    }

    /**Original name: WMOV-ID-MOVI-ANN-NULL<br>*/
    public String getWmovIdMoviAnnNull() {
        return readString(Pos.WMOV_ID_MOVI_ANN_NULL, Len.WMOV_ID_MOVI_ANN_NULL);
    }

    public String getWmovIdMoviAnnNullFormatted() {
        return Functions.padBlanks(getWmovIdMoviAnnNull(), Len.WMOV_ID_MOVI_ANN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMOV_ID_MOVI_ANN = 1;
        public static final int WMOV_ID_MOVI_ANN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMOV_ID_MOVI_ANN = 5;
        public static final int WMOV_ID_MOVI_ANN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMOV_ID_MOVI_ANN = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
