package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WS-DATA-NUM<br>
 * Variable: WS-DATA-NUM from program LCCS0003<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsDataNum extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsDataNum() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_DATA_NUM;
    }

    public void setWsDataNum(int wsDataNum) {
        writeInt(Pos.WS_DATA_NUM, wsDataNum, Len.Int.WS_DATA_NUM, SignType.NO_SIGN);
    }

    /**Original name: WS-DATA-NUM<br>*/
    public int getWsDataNum() {
        return readNumDispUnsignedInt(Pos.WS_DATA_NUM, Len.WS_DATA_NUM);
    }

    public String getWsDataNumFormatted() {
        return readFixedString(Pos.WS_DATA_NUM, Len.WS_DATA_NUM);
    }

    /**Original name: WS-DATA-NUM-AA<br>*/
    public short getNumAa() {
        return readNumDispUnsignedShort(Pos.NUM_AA, Len.NUM_AA);
    }

    /**Original name: WS-DATA-NUM-MMGG<br>*/
    public byte[] getNumMmggBytes() {
        byte[] buffer = new byte[Len.NUM_MMGG];
        return getNumMmggBytes(buffer, 1);
    }

    public byte[] getNumMmggBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.NUM_MMGG, Pos.NUM_MMGG);
        return buffer;
    }

    public void setNumMmFormatted(String numMm) {
        writeString(Pos.NUM_MM, Trunc.toUnsignedNumeric(numMm, Len.NUM_MM), Len.NUM_MM);
    }

    /**Original name: WS-DATA-NUM-MM<br>*/
    public short getNumMm() {
        return readNumDispUnsignedShort(Pos.NUM_MM, Len.NUM_MM);
    }

    public void setNumGg(short numGg) {
        writeShort(Pos.NUM_GG, numGg, Len.Int.NUM_GG, SignType.NO_SIGN);
    }

    public void setNumGgFormatted(String numGg) {
        writeString(Pos.NUM_GG, Trunc.toUnsignedNumeric(numGg, Len.NUM_GG), Len.NUM_GG);
    }

    /**Original name: WS-DATA-NUM-GG<br>*/
    public short getNumGg() {
        return readNumDispUnsignedShort(Pos.NUM_GG, Len.NUM_GG);
    }

    public String getNumGgFormatted() {
        return readFixedString(Pos.NUM_GG, Len.NUM_GG);
    }

    public void setNumAammFormatted(String numAamm) {
        writeString(Pos.NUM_AAMM, Trunc.toUnsignedNumeric(numAamm, Len.NUM_AAMM), Len.NUM_AAMM);
    }

    /**Original name: WS-DATA-NUM-AAMM<br>*/
    public int getNumAamm() {
        return readNumDispUnsignedInt(Pos.NUM_AAMM, Len.NUM_AAMM);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_DATA_NUM = 1;
        public static final int FILLER_WS2 = 1;
        public static final int NUM_AA = FILLER_WS2;
        public static final int NUM_MMGG = NUM_AA + Len.NUM_AA;
        public static final int NUM_MM = NUM_MMGG;
        public static final int NUM_GG = NUM_MM + Len.NUM_MM;
        public static final int FILLER_WS3 = 1;
        public static final int NUM_AAMM = FILLER_WS3;
        public static final int FLR1 = NUM_AAMM + Len.NUM_AAMM;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int NUM_AA = 4;
        public static final int NUM_MM = 2;
        public static final int NUM_AAMM = 6;
        public static final int WS_DATA_NUM = 8;
        public static final int NUM_GG = 2;
        public static final int NUM_MMGG = NUM_MM + NUM_GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WS_DATA_NUM = 8;
            public static final int NUM_GG = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
