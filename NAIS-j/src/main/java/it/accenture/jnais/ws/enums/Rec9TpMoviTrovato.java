package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: REC-9-TP-MOVI-TROVATO<br>
 * Variable: REC-9-TP-MOVI-TROVATO from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Rec9TpMoviTrovato {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setRec9TpMoviTrovato(char rec9TpMoviTrovato) {
        this.value = rec9TpMoviTrovato;
    }

    public char getRec9TpMoviTrovato() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
