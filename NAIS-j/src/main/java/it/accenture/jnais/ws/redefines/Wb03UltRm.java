package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-ULT-RM<br>
 * Variable: WB03-ULT-RM from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03UltRm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03UltRm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_ULT_RM;
    }

    public void setWb03UltRmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_ULT_RM, Pos.WB03_ULT_RM);
    }

    /**Original name: WB03-ULT-RM<br>*/
    public AfDecimal getWb03UltRm() {
        return readPackedAsDecimal(Pos.WB03_ULT_RM, Len.Int.WB03_ULT_RM, Len.Fract.WB03_ULT_RM);
    }

    public byte[] getWb03UltRmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_ULT_RM, Pos.WB03_ULT_RM);
        return buffer;
    }

    public void setWb03UltRmNull(String wb03UltRmNull) {
        writeString(Pos.WB03_ULT_RM_NULL, wb03UltRmNull, Len.WB03_ULT_RM_NULL);
    }

    /**Original name: WB03-ULT-RM-NULL<br>*/
    public String getWb03UltRmNull() {
        return readString(Pos.WB03_ULT_RM_NULL, Len.WB03_ULT_RM_NULL);
    }

    public String getWb03UltRmNullFormatted() {
        return Functions.padBlanks(getWb03UltRmNull(), Len.WB03_ULT_RM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_ULT_RM = 1;
        public static final int WB03_ULT_RM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ULT_RM = 8;
        public static final int WB03_ULT_RM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ULT_RM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_ULT_RM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
