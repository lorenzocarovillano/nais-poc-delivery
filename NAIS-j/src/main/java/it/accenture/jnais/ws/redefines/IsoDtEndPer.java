package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-DT-END-PER<br>
 * Variable: ISO-DT-END-PER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoDtEndPer extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoDtEndPer() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_DT_END_PER;
    }

    public void setIsoDtEndPer(int isoDtEndPer) {
        writeIntAsPacked(Pos.ISO_DT_END_PER, isoDtEndPer, Len.Int.ISO_DT_END_PER);
    }

    public void setIsoDtEndPerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_DT_END_PER, Pos.ISO_DT_END_PER);
    }

    /**Original name: ISO-DT-END-PER<br>*/
    public int getIsoDtEndPer() {
        return readPackedAsInt(Pos.ISO_DT_END_PER, Len.Int.ISO_DT_END_PER);
    }

    public byte[] getIsoDtEndPerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_DT_END_PER, Pos.ISO_DT_END_PER);
        return buffer;
    }

    public void setIsoDtEndPerNull(String isoDtEndPerNull) {
        writeString(Pos.ISO_DT_END_PER_NULL, isoDtEndPerNull, Len.ISO_DT_END_PER_NULL);
    }

    /**Original name: ISO-DT-END-PER-NULL<br>*/
    public String getIsoDtEndPerNull() {
        return readString(Pos.ISO_DT_END_PER_NULL, Len.ISO_DT_END_PER_NULL);
    }

    public String getIsoDtEndPerNullFormatted() {
        return Functions.padBlanks(getIsoDtEndPerNull(), Len.ISO_DT_END_PER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_DT_END_PER = 1;
        public static final int ISO_DT_END_PER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_DT_END_PER = 5;
        public static final int ISO_DT_END_PER_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_DT_END_PER = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
