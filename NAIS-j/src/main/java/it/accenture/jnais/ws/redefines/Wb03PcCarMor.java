package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PC-CAR-MOR<br>
 * Variable: WB03-PC-CAR-MOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PcCarMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PcCarMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PC_CAR_MOR;
    }

    public void setWb03PcCarMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PC_CAR_MOR, Pos.WB03_PC_CAR_MOR);
    }

    /**Original name: WB03-PC-CAR-MOR<br>*/
    public AfDecimal getWb03PcCarMor() {
        return readPackedAsDecimal(Pos.WB03_PC_CAR_MOR, Len.Int.WB03_PC_CAR_MOR, Len.Fract.WB03_PC_CAR_MOR);
    }

    public byte[] getWb03PcCarMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PC_CAR_MOR, Pos.WB03_PC_CAR_MOR);
        return buffer;
    }

    public void setWb03PcCarMorNull(String wb03PcCarMorNull) {
        writeString(Pos.WB03_PC_CAR_MOR_NULL, wb03PcCarMorNull, Len.WB03_PC_CAR_MOR_NULL);
    }

    /**Original name: WB03-PC-CAR-MOR-NULL<br>*/
    public String getWb03PcCarMorNull() {
        return readString(Pos.WB03_PC_CAR_MOR_NULL, Len.WB03_PC_CAR_MOR_NULL);
    }

    public String getWb03PcCarMorNullFormatted() {
        return Functions.padBlanks(getWb03PcCarMorNull(), Len.WB03_PC_CAR_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PC_CAR_MOR = 1;
        public static final int WB03_PC_CAR_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PC_CAR_MOR = 4;
        public static final int WB03_PC_CAR_MOR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PC_CAR_MOR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PC_CAR_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
