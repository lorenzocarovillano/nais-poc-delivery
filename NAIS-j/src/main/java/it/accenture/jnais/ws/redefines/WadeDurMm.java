package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WADE-DUR-MM<br>
 * Variable: WADE-DUR-MM from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeDurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeDurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_DUR_MM;
    }

    public void setWadeDurMm(int wadeDurMm) {
        writeIntAsPacked(Pos.WADE_DUR_MM, wadeDurMm, Len.Int.WADE_DUR_MM);
    }

    public void setWadeDurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_DUR_MM, Pos.WADE_DUR_MM);
    }

    /**Original name: WADE-DUR-MM<br>*/
    public int getWadeDurMm() {
        return readPackedAsInt(Pos.WADE_DUR_MM, Len.Int.WADE_DUR_MM);
    }

    public byte[] getWadeDurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_DUR_MM, Pos.WADE_DUR_MM);
        return buffer;
    }

    public void initWadeDurMmSpaces() {
        fill(Pos.WADE_DUR_MM, Len.WADE_DUR_MM, Types.SPACE_CHAR);
    }

    public void setWadeDurMmNull(String wadeDurMmNull) {
        writeString(Pos.WADE_DUR_MM_NULL, wadeDurMmNull, Len.WADE_DUR_MM_NULL);
    }

    /**Original name: WADE-DUR-MM-NULL<br>*/
    public String getWadeDurMmNull() {
        return readString(Pos.WADE_DUR_MM_NULL, Len.WADE_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_DUR_MM = 1;
        public static final int WADE_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_DUR_MM = 3;
        public static final int WADE_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
