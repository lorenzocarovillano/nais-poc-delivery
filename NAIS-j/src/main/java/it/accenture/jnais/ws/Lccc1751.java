package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LCCC1751<br>
 * Variable: LCCC1751 from copybook LCCC1751<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Lccc1751 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LCCC1751-DATA-INFERIORE
    private String dataInferiore = DefaultValues.stringVal(Len.DATA_INFERIORE);
    //Original name: LCCC1751-DATA-SUPERIORE
    private String dataSuperiore = DefaultValues.stringVal(Len.DATA_SUPERIORE);
    //Original name: LCCC1751-ETA-MM-ASSICURATO
    private String etaMmAssicurato = "00";
    //Original name: LCCC1751-ETA-AA-ASSICURATO
    private String etaAaAssicurato = "00000";
    //Original name: LCCC1751-RETURN-CODE
    private String returnCode = DefaultValues.stringVal(Len.RETURN_CODE);
    //Original name: LCCC1751-DESCRIZ-ERR
    private String descrizErr = DefaultValues.stringVal(Len.DESCRIZ_ERR);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC1751;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLccc1751Bytes(buf);
    }

    public String getLccc1751Formatted() {
        return MarshalByteExt.bufferToStr(getLccc1751Bytes());
    }

    public void setLccc1751Bytes(byte[] buffer) {
        setLccc1751Bytes(buffer, 1);
    }

    public byte[] getLccc1751Bytes() {
        byte[] buffer = new byte[Len.LCCC1751];
        return getLccc1751Bytes(buffer, 1);
    }

    public void setLccc1751Bytes(byte[] buffer, int offset) {
        int position = offset;
        setInputBytes(buffer, position);
        position += Len.INPUT;
        setOutputBytes(buffer, position);
    }

    public byte[] getLccc1751Bytes(byte[] buffer, int offset) {
        int position = offset;
        getInputBytes(buffer, position);
        position += Len.INPUT;
        getOutputBytes(buffer, position);
        return buffer;
    }

    public void setInputBytes(byte[] buffer, int offset) {
        int position = offset;
        dataInferiore = MarshalByte.readFixedString(buffer, position, Len.DATA_INFERIORE);
        position += Len.DATA_INFERIORE;
        dataSuperiore = MarshalByte.readFixedString(buffer, position, Len.DATA_SUPERIORE);
    }

    public byte[] getInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, dataInferiore, Len.DATA_INFERIORE);
        position += Len.DATA_INFERIORE;
        MarshalByte.writeString(buffer, position, dataSuperiore, Len.DATA_SUPERIORE);
        return buffer;
    }

    public void setDataInferiore(int dataInferiore) {
        this.dataInferiore = NumericDisplay.asString(dataInferiore, Len.DATA_INFERIORE);
    }

    public int getDataInferiore() {
        return NumericDisplay.asInt(this.dataInferiore);
    }

    public String getDataInferioreFormatted() {
        return this.dataInferiore;
    }

    public void setDataSuperioreFormatted(String dataSuperiore) {
        this.dataSuperiore = Trunc.toUnsignedNumeric(dataSuperiore, Len.DATA_SUPERIORE);
    }

    public int getDataSuperiore() {
        return NumericDisplay.asInt(this.dataSuperiore);
    }

    public String getDataSuperioreFormatted() {
        return this.dataSuperiore;
    }

    public void setOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        etaMmAssicurato = MarshalByte.readFixedString(buffer, position, Len.ETA_MM_ASSICURATO);
        position += Len.ETA_MM_ASSICURATO;
        etaAaAssicurato = MarshalByte.readFixedString(buffer, position, Len.ETA_AA_ASSICURATO);
        position += Len.ETA_AA_ASSICURATO;
        returnCode = MarshalByte.readString(buffer, position, Len.RETURN_CODE);
        position += Len.RETURN_CODE;
        descrizErr = MarshalByte.readString(buffer, position, Len.DESCRIZ_ERR);
    }

    public byte[] getOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, etaMmAssicurato, Len.ETA_MM_ASSICURATO);
        position += Len.ETA_MM_ASSICURATO;
        MarshalByte.writeString(buffer, position, etaAaAssicurato, Len.ETA_AA_ASSICURATO);
        position += Len.ETA_AA_ASSICURATO;
        MarshalByte.writeString(buffer, position, returnCode, Len.RETURN_CODE);
        position += Len.RETURN_CODE;
        MarshalByte.writeString(buffer, position, descrizErr, Len.DESCRIZ_ERR);
        return buffer;
    }

    public void setEtaMmAssicurato(short etaMmAssicurato) {
        this.etaMmAssicurato = NumericDisplay.asString(etaMmAssicurato, Len.ETA_MM_ASSICURATO);
    }

    public void setEtaMmAssicuratoFormatted(String etaMmAssicurato) {
        this.etaMmAssicurato = Trunc.toUnsignedNumeric(etaMmAssicurato, Len.ETA_MM_ASSICURATO);
    }

    public short getEtaMmAssicurato() {
        return NumericDisplay.asShort(this.etaMmAssicurato);
    }

    public void setEtaAaAssicurato(int etaAaAssicurato) {
        this.etaAaAssicurato = NumericDisplay.asString(etaAaAssicurato, Len.ETA_AA_ASSICURATO);
    }

    public void setEtaAaAssicuratoFormatted(String etaAaAssicurato) {
        this.etaAaAssicurato = Trunc.toUnsignedNumeric(etaAaAssicurato, Len.ETA_AA_ASSICURATO);
    }

    public int getEtaAaAssicurato() {
        return NumericDisplay.asInt(this.etaAaAssicurato);
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = Functions.subString(returnCode, Len.RETURN_CODE);
    }

    public String getReturnCode() {
        return this.returnCode;
    }

    public void setDescrizErr(String descrizErr) {
        this.descrizErr = Functions.subString(descrizErr, Len.DESCRIZ_ERR);
    }

    public String getDescrizErr() {
        return this.descrizErr;
    }

    public String getDescrizErrFormatted() {
        return Functions.padBlanks(getDescrizErr(), Len.DESCRIZ_ERR);
    }

    @Override
    public byte[] serialize() {
        return getLccc1751Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DATA_INFERIORE = 8;
        public static final int DATA_SUPERIORE = 8;
        public static final int INPUT = DATA_INFERIORE + DATA_SUPERIORE;
        public static final int ETA_MM_ASSICURATO = 2;
        public static final int ETA_AA_ASSICURATO = 5;
        public static final int RETURN_CODE = 2;
        public static final int DESCRIZ_ERR = 200;
        public static final int OUTPUT = ETA_MM_ASSICURATO + ETA_AA_ASSICURATO + RETURN_CODE + DESCRIZ_ERR;
        public static final int LCCC1751 = INPUT + OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
