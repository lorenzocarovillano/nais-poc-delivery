package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: IDSV0003-SQLCODE<br>
 * Variable: IDSV0003-SQLCODE from copybook IDSV0003<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0003Sqlcode {

    //==== PROPERTIES ====
    private int value = DefaultValues.INT_VAL;
    public static final int SUCCESSFUL_SQL = 0;
    public static final int NOT_FOUND = 100;
    public static final int OPER_NF_X_UNDONE = 883;
    public static final int DUPLICATE_KEY = -803;
    public static final int MORE_THAN_ONE_ROW = -811;
    public static final int SAVEPOINT_NOT_FOUND = -880;
    private static final int[] DEADLOCK_TIMEOUT = new int[] {-911, -913};
    public static final int CONNECTION_ERROR = -924;
    public static final int NEGATIVI_MIN = -999999999;
    public static final int NEGATIVI_MAX = -1;
    public static final int POSITIVI_MIN = 1;
    public static final int POSITIVI_MAX = 999999999;

    //==== METHODS ====
    public void setSqlcode(int sqlcode) {
        this.value = sqlcode;
    }

    public int getSqlcode() {
        return this.value;
    }

    public String getSqlcodeFormatted() {
        return NumericDisplaySigned.asString(getSqlcode(), Len.SQLCODE);
    }

    public String getSqlcodeAsString() {
        return getSqlcodeFormatted();
    }

    public boolean isSuccessfulSql() {
        return value == SUCCESSFUL_SQL;
    }

    public void setSuccessfulSql() {
        value = SUCCESSFUL_SQL;
    }

    public boolean isNotFound() {
        return value == NOT_FOUND;
    }

    public void setNotFound() {
        value = NOT_FOUND;
    }

    public boolean isIdsv0003OperNfXUndone() {
        return value == OPER_NF_X_UNDONE;
    }

    public boolean isIdsv0003Negativi() {
        int fld = value;
        return fld >= NEGATIVI_MIN && fld <= NEGATIVI_MAX;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int SQLCODE = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
