package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-MONT-DAL2007-CALC<br>
 * Variable: WDFL-MONT-DAL2007-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflMontDal2007Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflMontDal2007Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_MONT_DAL2007_CALC;
    }

    public void setWdflMontDal2007Calc(AfDecimal wdflMontDal2007Calc) {
        writeDecimalAsPacked(Pos.WDFL_MONT_DAL2007_CALC, wdflMontDal2007Calc.copy());
    }

    public void setWdflMontDal2007CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_MONT_DAL2007_CALC, Pos.WDFL_MONT_DAL2007_CALC);
    }

    /**Original name: WDFL-MONT-DAL2007-CALC<br>*/
    public AfDecimal getWdflMontDal2007Calc() {
        return readPackedAsDecimal(Pos.WDFL_MONT_DAL2007_CALC, Len.Int.WDFL_MONT_DAL2007_CALC, Len.Fract.WDFL_MONT_DAL2007_CALC);
    }

    public byte[] getWdflMontDal2007CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_MONT_DAL2007_CALC, Pos.WDFL_MONT_DAL2007_CALC);
        return buffer;
    }

    public void setWdflMontDal2007CalcNull(String wdflMontDal2007CalcNull) {
        writeString(Pos.WDFL_MONT_DAL2007_CALC_NULL, wdflMontDal2007CalcNull, Len.WDFL_MONT_DAL2007_CALC_NULL);
    }

    /**Original name: WDFL-MONT-DAL2007-CALC-NULL<br>*/
    public String getWdflMontDal2007CalcNull() {
        return readString(Pos.WDFL_MONT_DAL2007_CALC_NULL, Len.WDFL_MONT_DAL2007_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_DAL2007_CALC = 1;
        public static final int WDFL_MONT_DAL2007_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_DAL2007_CALC = 8;
        public static final int WDFL_MONT_DAL2007_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_DAL2007_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_DAL2007_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
