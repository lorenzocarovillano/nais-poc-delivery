package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-DT-SCAD<br>
 * Variable: GRZ-DT-SCAD from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzDtScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzDtScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_DT_SCAD;
    }

    public void setGrzDtScad(int grzDtScad) {
        writeIntAsPacked(Pos.GRZ_DT_SCAD, grzDtScad, Len.Int.GRZ_DT_SCAD);
    }

    public void setGrzDtScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_DT_SCAD, Pos.GRZ_DT_SCAD);
    }

    /**Original name: GRZ-DT-SCAD<br>*/
    public int getGrzDtScad() {
        return readPackedAsInt(Pos.GRZ_DT_SCAD, Len.Int.GRZ_DT_SCAD);
    }

    public byte[] getGrzDtScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_DT_SCAD, Pos.GRZ_DT_SCAD);
        return buffer;
    }

    public void setGrzDtScadNull(String grzDtScadNull) {
        writeString(Pos.GRZ_DT_SCAD_NULL, grzDtScadNull, Len.GRZ_DT_SCAD_NULL);
    }

    /**Original name: GRZ-DT-SCAD-NULL<br>*/
    public String getGrzDtScadNull() {
        return readString(Pos.GRZ_DT_SCAD_NULL, Len.GRZ_DT_SCAD_NULL);
    }

    public String getGrzDtScadNullFormatted() {
        return Functions.padBlanks(getGrzDtScadNull(), Len.GRZ_DT_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_DT_SCAD = 1;
        public static final int GRZ_DT_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_DT_SCAD = 5;
        public static final int GRZ_DT_SCAD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_DT_SCAD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
