package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.enums.WkIdCodLivFlagGrz;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.occurs.WgrzTabGar;
import it.accenture.jnais.ws.occurs.WpmoTabParamMov;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS1890<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs1890Data {

    //==== PROPERTIES ====
    public static final int DGRZ_TAB_GAR_MAXOCCURS = 20;
    public static final int DPMO_TAB_PARAM_MOV_MAXOCCURS = 100;
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*
	 * ---------------------------------------------------------------*
	 *   COPY TIPOLOGICHE
	 * ---------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    //Original name: DGRZ-ELE-GAR-MAX
    private short dgrzEleGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DGRZ-TAB-GAR
    private WgrzTabGar[] dgrzTabGar = new WgrzTabGar[DGRZ_TAB_GAR_MAXOCCURS];
    //Original name: DPMO-ELE-PMO-MAX
    private short dpmoElePmoMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DPMO-TAB-PARAM-MOV
    private WpmoTabParamMov[] dpmoTabParamMov = new WpmoTabParamMov[DPMO_TAB_PARAM_MOV_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-INDICI
    private IxIndiciLvvs0116 ixIndici = new IxIndiciLvvs0116();
    //Original name: WK-ID-COD-LIV-FLAG-GRZ
    private WkIdCodLivFlagGrz wkIdCodLivFlagGrz = new WkIdCodLivFlagGrz();
    //Original name: WK-PMO-SCADENZA
    private short wkPmoScadenza = ((short)6009);

    //==== CONSTRUCTORS ====
    public Lvvs1890Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dgrzTabGarIdx = 1; dgrzTabGarIdx <= DGRZ_TAB_GAR_MAXOCCURS; dgrzTabGarIdx++) {
            dgrzTabGar[dgrzTabGarIdx - 1] = new WgrzTabGar();
        }
        for (int dpmoTabParamMovIdx = 1; dpmoTabParamMovIdx <= DPMO_TAB_PARAM_MOV_MAXOCCURS; dpmoTabParamMovIdx++) {
            dpmoTabParamMov[dpmoTabParamMovIdx - 1] = new WpmoTabParamMov();
        }
    }

    public void setDgrzAreaGraFormatted(String data) {
        byte[] buffer = new byte[Len.DGRZ_AREA_GRA];
        MarshalByte.writeString(buffer, 1, data, Len.DGRZ_AREA_GRA);
        setDgrzAreaGraBytes(buffer, 1);
    }

    public void setDgrzAreaGraBytes(byte[] buffer, int offset) {
        int position = offset;
        dgrzEleGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DGRZ_TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dgrzTabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                dgrzTabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public void setDgrzEleGarMax(short dgrzEleGarMax) {
        this.dgrzEleGarMax = dgrzEleGarMax;
    }

    public short getDgrzEleGarMax() {
        return this.dgrzEleGarMax;
    }

    public void setDpmoAreaPmoFormatted(String data) {
        byte[] buffer = new byte[Len.DPMO_AREA_PMO];
        MarshalByte.writeString(buffer, 1, data, Len.DPMO_AREA_PMO);
        setDpmoAreaPmoBytes(buffer, 1);
    }

    public void setDpmoAreaPmoBytes(byte[] buffer, int offset) {
        int position = offset;
        dpmoElePmoMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DPMO_TAB_PARAM_MOV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dpmoTabParamMov[idx - 1].setWpmoTabParamMovBytes(buffer, position);
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
            else {
                dpmoTabParamMov[idx - 1].initWpmoTabParamMovSpaces();
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
        }
    }

    public void setDpmoElePmoMax(short dpmoElePmoMax) {
        this.dpmoElePmoMax = dpmoElePmoMax;
    }

    public short getDpmoElePmoMax() {
        return this.dpmoElePmoMax;
    }

    public short getWkPmoScadenza() {
        return this.wkPmoScadenza;
    }

    public WgrzTabGar getDgrzTabGar(int idx) {
        return dgrzTabGar[idx - 1];
    }

    public WpmoTabParamMov getDpmoTabParamMov(int idx) {
        return dpmoTabParamMov[idx - 1];
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndiciLvvs0116 getIxIndici() {
        return ixIndici;
    }

    public WkIdCodLivFlagGrz getWkIdCodLivFlagGrz() {
        return wkIdCodLivFlagGrz;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DGRZ_ELE_GAR_MAX = 2;
        public static final int DGRZ_AREA_GRA = DGRZ_ELE_GAR_MAX + Lvvs1890Data.DGRZ_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int DPMO_ELE_PMO_MAX = 2;
        public static final int DPMO_AREA_PMO = DPMO_ELE_PMO_MAX + Lvvs1890Data.DPMO_TAB_PARAM_MOV_MAXOCCURS * WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
