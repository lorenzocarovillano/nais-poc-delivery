package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-MARSOL<br>
 * Variable: WPCO-DT-ULTC-MARSOL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcMarsol extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcMarsol() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_MARSOL;
    }

    public void setWpcoDtUltcMarsol(int wpcoDtUltcMarsol) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_MARSOL, wpcoDtUltcMarsol, Len.Int.WPCO_DT_ULTC_MARSOL);
    }

    public void setDpcoDtUltcMarsolFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_MARSOL, Pos.WPCO_DT_ULTC_MARSOL);
    }

    /**Original name: WPCO-DT-ULTC-MARSOL<br>*/
    public int getWpcoDtUltcMarsol() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_MARSOL, Len.Int.WPCO_DT_ULTC_MARSOL);
    }

    public byte[] getWpcoDtUltcMarsolAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_MARSOL, Pos.WPCO_DT_ULTC_MARSOL);
        return buffer;
    }

    public void setWpcoDtUltcMarsolNull(String wpcoDtUltcMarsolNull) {
        writeString(Pos.WPCO_DT_ULTC_MARSOL_NULL, wpcoDtUltcMarsolNull, Len.WPCO_DT_ULTC_MARSOL_NULL);
    }

    /**Original name: WPCO-DT-ULTC-MARSOL-NULL<br>*/
    public String getWpcoDtUltcMarsolNull() {
        return readString(Pos.WPCO_DT_ULTC_MARSOL_NULL, Len.WPCO_DT_ULTC_MARSOL_NULL);
    }

    public String getWpcoDtUltcMarsolNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcMarsolNull(), Len.WPCO_DT_ULTC_MARSOL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_MARSOL = 1;
        public static final int WPCO_DT_ULTC_MARSOL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_MARSOL = 5;
        public static final int WPCO_DT_ULTC_MARSOL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_MARSOL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
