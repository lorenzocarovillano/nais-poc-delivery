package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: AREA-LDBV2271<br>
 * Variable: AREA-LDBV2271 from copybook LDBV2271<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaLdbv2271 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV2271-ID-OGG
    private int ldbv2271IdOgg = DefaultValues.INT_VAL;
    //Original name: LDBV2271-TP-OGG
    private String ldbv2271TpOgg = DefaultValues.stringVal(Len.LDBV2271_TP_OGG);
    //Original name: LDBV2271-TP-LIQ
    private String ldbv2271TpLiq = DefaultValues.stringVal(Len.LDBV2271_TP_LIQ);
    //Original name: LDBV2271-IMP-LRD-LIQTO
    private AfDecimal ldbv2271ImpLrdLiqto = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_LDBV2271;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaLdbv2271Bytes(buf);
    }

    public String getAreaLdbv2271Formatted() {
        return MarshalByteExt.bufferToStr(getAreaLdbv2271Bytes());
    }

    public void setAreaLdbv2271Bytes(byte[] buffer) {
        setAreaLdbv2271Bytes(buffer, 1);
    }

    public byte[] getAreaLdbv2271Bytes() {
        byte[] buffer = new byte[Len.AREA_LDBV2271];
        return getAreaLdbv2271Bytes(buffer, 1);
    }

    public void setAreaLdbv2271Bytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaLdbv2271IBytes(buffer, position);
        position += Len.AREA_LDBV2271_I;
        setAreaLdbv2271OBytes(buffer, position);
    }

    public byte[] getAreaLdbv2271Bytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaLdbv2271IBytes(buffer, position);
        position += Len.AREA_LDBV2271_I;
        getAreaLdbv2271OBytes(buffer, position);
        return buffer;
    }

    public void setAreaLdbv2271IBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv2271IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV2271_ID_OGG, 0);
        position += Len.LDBV2271_ID_OGG;
        ldbv2271TpOgg = MarshalByte.readString(buffer, position, Len.LDBV2271_TP_OGG);
        position += Len.LDBV2271_TP_OGG;
        ldbv2271TpLiq = MarshalByte.readString(buffer, position, Len.LDBV2271_TP_LIQ);
    }

    public byte[] getAreaLdbv2271IBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv2271IdOgg, Len.Int.LDBV2271_ID_OGG, 0);
        position += Len.LDBV2271_ID_OGG;
        MarshalByte.writeString(buffer, position, ldbv2271TpOgg, Len.LDBV2271_TP_OGG);
        position += Len.LDBV2271_TP_OGG;
        MarshalByte.writeString(buffer, position, ldbv2271TpLiq, Len.LDBV2271_TP_LIQ);
        return buffer;
    }

    public void setLdbv2271IdOgg(int ldbv2271IdOgg) {
        this.ldbv2271IdOgg = ldbv2271IdOgg;
    }

    public int getLdbv2271IdOgg() {
        return this.ldbv2271IdOgg;
    }

    public void setLdbv2271TpOgg(String ldbv2271TpOgg) {
        this.ldbv2271TpOgg = Functions.subString(ldbv2271TpOgg, Len.LDBV2271_TP_OGG);
    }

    public String getLdbv2271TpOgg() {
        return this.ldbv2271TpOgg;
    }

    public void setLdbv2271TpLiq(String ldbv2271TpLiq) {
        this.ldbv2271TpLiq = Functions.subString(ldbv2271TpLiq, Len.LDBV2271_TP_LIQ);
    }

    public String getLdbv2271TpLiq() {
        return this.ldbv2271TpLiq;
    }

    public void setAreaLdbv2271OBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv2271ImpLrdLiqto.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LDBV2271_IMP_LRD_LIQTO, Len.Fract.LDBV2271_IMP_LRD_LIQTO));
    }

    public byte[] getAreaLdbv2271OBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimalAsPacked(buffer, position, ldbv2271ImpLrdLiqto.copy());
        return buffer;
    }

    public void setLdbv2271ImpLrdLiqto(AfDecimal ldbv2271ImpLrdLiqto) {
        this.ldbv2271ImpLrdLiqto.assign(ldbv2271ImpLrdLiqto);
    }

    public AfDecimal getLdbv2271ImpLrdLiqto() {
        return this.ldbv2271ImpLrdLiqto.copy();
    }

    @Override
    public byte[] serialize() {
        return getAreaLdbv2271Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV2271_ID_OGG = 5;
        public static final int LDBV2271_TP_OGG = 2;
        public static final int LDBV2271_TP_LIQ = 2;
        public static final int AREA_LDBV2271_I = LDBV2271_ID_OGG + LDBV2271_TP_OGG + LDBV2271_TP_LIQ;
        public static final int LDBV2271_IMP_LRD_LIQTO = 8;
        public static final int AREA_LDBV2271_O = LDBV2271_IMP_LRD_LIQTO;
        public static final int AREA_LDBV2271 = AREA_LDBV2271_I + AREA_LDBV2271_O;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV2271_ID_OGG = 9;
            public static final int LDBV2271_IMP_LRD_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LDBV2271_IMP_LRD_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
