package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-INI-NET<br>
 * Variable: WTGA-PRE-INI-NET from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPreIniNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPreIniNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_INI_NET;
    }

    public void setWtgaPreIniNet(AfDecimal wtgaPreIniNet) {
        writeDecimalAsPacked(Pos.WTGA_PRE_INI_NET, wtgaPreIniNet.copy());
    }

    public void setWtgaPreIniNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_INI_NET, Pos.WTGA_PRE_INI_NET);
    }

    /**Original name: WTGA-PRE-INI-NET<br>*/
    public AfDecimal getWtgaPreIniNet() {
        return readPackedAsDecimal(Pos.WTGA_PRE_INI_NET, Len.Int.WTGA_PRE_INI_NET, Len.Fract.WTGA_PRE_INI_NET);
    }

    public byte[] getWtgaPreIniNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_INI_NET, Pos.WTGA_PRE_INI_NET);
        return buffer;
    }

    public void initWtgaPreIniNetSpaces() {
        fill(Pos.WTGA_PRE_INI_NET, Len.WTGA_PRE_INI_NET, Types.SPACE_CHAR);
    }

    public void setWtgaPreIniNetNull(String wtgaPreIniNetNull) {
        writeString(Pos.WTGA_PRE_INI_NET_NULL, wtgaPreIniNetNull, Len.WTGA_PRE_INI_NET_NULL);
    }

    /**Original name: WTGA-PRE-INI-NET-NULL<br>*/
    public String getWtgaPreIniNetNull() {
        return readString(Pos.WTGA_PRE_INI_NET_NULL, Len.WTGA_PRE_INI_NET_NULL);
    }

    public String getWtgaPreIniNetNullFormatted() {
        return Functions.padBlanks(getWtgaPreIniNetNull(), Len.WTGA_PRE_INI_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_INI_NET = 1;
        public static final int WTGA_PRE_INI_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_INI_NET = 8;
        public static final int WTGA_PRE_INI_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_INI_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_INI_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
