package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-PRSTZ-PREC<br>
 * Variable: ISO-PRSTZ-PREC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoPrstzPrec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoPrstzPrec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_PRSTZ_PREC;
    }

    public void setIsoPrstzPrec(AfDecimal isoPrstzPrec) {
        writeDecimalAsPacked(Pos.ISO_PRSTZ_PREC, isoPrstzPrec.copy());
    }

    public void setIsoPrstzPrecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_PRSTZ_PREC, Pos.ISO_PRSTZ_PREC);
    }

    /**Original name: ISO-PRSTZ-PREC<br>*/
    public AfDecimal getIsoPrstzPrec() {
        return readPackedAsDecimal(Pos.ISO_PRSTZ_PREC, Len.Int.ISO_PRSTZ_PREC, Len.Fract.ISO_PRSTZ_PREC);
    }

    public byte[] getIsoPrstzPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_PRSTZ_PREC, Pos.ISO_PRSTZ_PREC);
        return buffer;
    }

    public void setIsoPrstzPrecNull(String isoPrstzPrecNull) {
        writeString(Pos.ISO_PRSTZ_PREC_NULL, isoPrstzPrecNull, Len.ISO_PRSTZ_PREC_NULL);
    }

    /**Original name: ISO-PRSTZ-PREC-NULL<br>*/
    public String getIsoPrstzPrecNull() {
        return readString(Pos.ISO_PRSTZ_PREC_NULL, Len.ISO_PRSTZ_PREC_NULL);
    }

    public String getIsoPrstzPrecNullFormatted() {
        return Functions.padBlanks(getIsoPrstzPrecNull(), Len.ISO_PRSTZ_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_PRSTZ_PREC = 1;
        public static final int ISO_PRSTZ_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_PRSTZ_PREC = 8;
        public static final int ISO_PRSTZ_PREC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_PRSTZ_PREC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISO_PRSTZ_PREC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
