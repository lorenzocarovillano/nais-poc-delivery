package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-ULT-RIVAL<br>
 * Variable: WB03-DT-ULT-RIVAL from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtUltRival extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtUltRival() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_ULT_RIVAL;
    }

    public void setWb03DtUltRival(int wb03DtUltRival) {
        writeIntAsPacked(Pos.WB03_DT_ULT_RIVAL, wb03DtUltRival, Len.Int.WB03_DT_ULT_RIVAL);
    }

    public void setWb03DtUltRivalFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_ULT_RIVAL, Pos.WB03_DT_ULT_RIVAL);
    }

    /**Original name: WB03-DT-ULT-RIVAL<br>*/
    public int getWb03DtUltRival() {
        return readPackedAsInt(Pos.WB03_DT_ULT_RIVAL, Len.Int.WB03_DT_ULT_RIVAL);
    }

    public byte[] getWb03DtUltRivalAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_ULT_RIVAL, Pos.WB03_DT_ULT_RIVAL);
        return buffer;
    }

    public void setWb03DtUltRivalNull(String wb03DtUltRivalNull) {
        writeString(Pos.WB03_DT_ULT_RIVAL_NULL, wb03DtUltRivalNull, Len.WB03_DT_ULT_RIVAL_NULL);
    }

    /**Original name: WB03-DT-ULT-RIVAL-NULL<br>*/
    public String getWb03DtUltRivalNull() {
        return readString(Pos.WB03_DT_ULT_RIVAL_NULL, Len.WB03_DT_ULT_RIVAL_NULL);
    }

    public String getWb03DtUltRivalNullFormatted() {
        return Functions.padBlanks(getWb03DtUltRivalNull(), Len.WB03_DT_ULT_RIVAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_ULT_RIVAL = 1;
        public static final int WB03_DT_ULT_RIVAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_ULT_RIVAL = 5;
        public static final int WB03_DT_ULT_RIVAL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_ULT_RIVAL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
