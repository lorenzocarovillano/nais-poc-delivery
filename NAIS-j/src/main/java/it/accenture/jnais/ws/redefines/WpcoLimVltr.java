package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-LIM-VLTR<br>
 * Variable: WPCO-LIM-VLTR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoLimVltr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoLimVltr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_LIM_VLTR;
    }

    public void setWpcoLimVltr(AfDecimal wpcoLimVltr) {
        writeDecimalAsPacked(Pos.WPCO_LIM_VLTR, wpcoLimVltr.copy());
    }

    public void setDpcoLimVltrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_LIM_VLTR, Pos.WPCO_LIM_VLTR);
    }

    /**Original name: WPCO-LIM-VLTR<br>*/
    public AfDecimal getWpcoLimVltr() {
        return readPackedAsDecimal(Pos.WPCO_LIM_VLTR, Len.Int.WPCO_LIM_VLTR, Len.Fract.WPCO_LIM_VLTR);
    }

    public byte[] getWpcoLimVltrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_LIM_VLTR, Pos.WPCO_LIM_VLTR);
        return buffer;
    }

    public void setWpcoLimVltrNull(String wpcoLimVltrNull) {
        writeString(Pos.WPCO_LIM_VLTR_NULL, wpcoLimVltrNull, Len.WPCO_LIM_VLTR_NULL);
    }

    /**Original name: WPCO-LIM-VLTR-NULL<br>*/
    public String getWpcoLimVltrNull() {
        return readString(Pos.WPCO_LIM_VLTR_NULL, Len.WPCO_LIM_VLTR_NULL);
    }

    public String getWpcoLimVltrNullFormatted() {
        return Functions.padBlanks(getWpcoLimVltrNull(), Len.WPCO_LIM_VLTR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_LIM_VLTR = 1;
        public static final int WPCO_LIM_VLTR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_LIM_VLTR = 8;
        public static final int WPCO_LIM_VLTR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_LIM_VLTR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_LIM_VLTR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
