package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDAD-IMP-TFR<br>
 * Variable: WDAD-IMP-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_IMP_TFR;
    }

    public void setWdadImpTfr(AfDecimal wdadImpTfr) {
        writeDecimalAsPacked(Pos.WDAD_IMP_TFR, wdadImpTfr.copy());
    }

    public void setWdadImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_IMP_TFR, Pos.WDAD_IMP_TFR);
    }

    /**Original name: WDAD-IMP-TFR<br>*/
    public AfDecimal getWdadImpTfr() {
        return readPackedAsDecimal(Pos.WDAD_IMP_TFR, Len.Int.WDAD_IMP_TFR, Len.Fract.WDAD_IMP_TFR);
    }

    public byte[] getWdadImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_IMP_TFR, Pos.WDAD_IMP_TFR);
        return buffer;
    }

    public void setWdadImpTfrNull(String wdadImpTfrNull) {
        writeString(Pos.WDAD_IMP_TFR_NULL, wdadImpTfrNull, Len.WDAD_IMP_TFR_NULL);
    }

    /**Original name: WDAD-IMP-TFR-NULL<br>*/
    public String getWdadImpTfrNull() {
        return readString(Pos.WDAD_IMP_TFR_NULL, Len.WDAD_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_TFR = 1;
        public static final int WDAD_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_TFR = 8;
        public static final int WDAD_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
