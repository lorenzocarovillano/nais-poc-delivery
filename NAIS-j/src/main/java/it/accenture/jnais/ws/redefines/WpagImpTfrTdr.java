package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-TFR-TDR<br>
 * Variable: WPAG-IMP-TFR-TDR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpTfrTdr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpTfrTdr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_TFR_TDR;
    }

    public void setWpagImpTfrTdr(AfDecimal wpagImpTfrTdr) {
        writeDecimalAsPacked(Pos.WPAG_IMP_TFR_TDR, wpagImpTfrTdr.copy());
    }

    public void setWpagImpTfrTdrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_TFR_TDR, Pos.WPAG_IMP_TFR_TDR);
    }

    /**Original name: WPAG-IMP-TFR-TDR<br>*/
    public AfDecimal getWpagImpTfrTdr() {
        return readPackedAsDecimal(Pos.WPAG_IMP_TFR_TDR, Len.Int.WPAG_IMP_TFR_TDR, Len.Fract.WPAG_IMP_TFR_TDR);
    }

    public byte[] getWpagImpTfrTdrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_TFR_TDR, Pos.WPAG_IMP_TFR_TDR);
        return buffer;
    }

    public void initWpagImpTfrTdrSpaces() {
        fill(Pos.WPAG_IMP_TFR_TDR, Len.WPAG_IMP_TFR_TDR, Types.SPACE_CHAR);
    }

    public void setWpagImpTfrTdrNull(String wpagImpTfrTdrNull) {
        writeString(Pos.WPAG_IMP_TFR_TDR_NULL, wpagImpTfrTdrNull, Len.WPAG_IMP_TFR_TDR_NULL);
    }

    /**Original name: WPAG-IMP-TFR-TDR-NULL<br>*/
    public String getWpagImpTfrTdrNull() {
        return readString(Pos.WPAG_IMP_TFR_TDR_NULL, Len.WPAG_IMP_TFR_TDR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_TFR_TDR = 1;
        public static final int WPAG_IMP_TFR_TDR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_TFR_TDR = 8;
        public static final int WPAG_IMP_TFR_TDR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_TFR_TDR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_TFR_TDR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
