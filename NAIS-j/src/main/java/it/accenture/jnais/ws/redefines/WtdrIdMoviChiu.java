package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-ID-MOVI-CHIU<br>
 * Variable: WTDR-ID-MOVI-CHIU from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_ID_MOVI_CHIU;
    }

    public void setWtdrIdMoviChiu(int wtdrIdMoviChiu) {
        writeIntAsPacked(Pos.WTDR_ID_MOVI_CHIU, wtdrIdMoviChiu, Len.Int.WTDR_ID_MOVI_CHIU);
    }

    /**Original name: WTDR-ID-MOVI-CHIU<br>*/
    public int getWtdrIdMoviChiu() {
        return readPackedAsInt(Pos.WTDR_ID_MOVI_CHIU, Len.Int.WTDR_ID_MOVI_CHIU);
    }

    public void setWtdrIdMoviChiuNull(String wtdrIdMoviChiuNull) {
        writeString(Pos.WTDR_ID_MOVI_CHIU_NULL, wtdrIdMoviChiuNull, Len.WTDR_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WTDR-ID-MOVI-CHIU-NULL<br>*/
    public String getWtdrIdMoviChiuNull() {
        return readString(Pos.WTDR_ID_MOVI_CHIU_NULL, Len.WTDR_ID_MOVI_CHIU_NULL);
    }

    public String getWtdrIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getWtdrIdMoviChiuNull(), Len.WTDR_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_ID_MOVI_CHIU = 1;
        public static final int WTDR_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_ID_MOVI_CHIU = 5;
        public static final int WTDR_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
