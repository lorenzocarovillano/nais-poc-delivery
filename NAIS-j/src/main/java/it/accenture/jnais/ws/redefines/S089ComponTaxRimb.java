package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-COMPON-TAX-RIMB<br>
 * Variable: S089-COMPON-TAX-RIMB from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ComponTaxRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ComponTaxRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_COMPON_TAX_RIMB;
    }

    public void setWlquComponTaxRimb(AfDecimal wlquComponTaxRimb) {
        writeDecimalAsPacked(Pos.S089_COMPON_TAX_RIMB, wlquComponTaxRimb.copy());
    }

    public void setWlquComponTaxRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_COMPON_TAX_RIMB, Pos.S089_COMPON_TAX_RIMB);
    }

    /**Original name: WLQU-COMPON-TAX-RIMB<br>*/
    public AfDecimal getWlquComponTaxRimb() {
        return readPackedAsDecimal(Pos.S089_COMPON_TAX_RIMB, Len.Int.WLQU_COMPON_TAX_RIMB, Len.Fract.WLQU_COMPON_TAX_RIMB);
    }

    public byte[] getWlquComponTaxRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_COMPON_TAX_RIMB, Pos.S089_COMPON_TAX_RIMB);
        return buffer;
    }

    public void initWlquComponTaxRimbSpaces() {
        fill(Pos.S089_COMPON_TAX_RIMB, Len.S089_COMPON_TAX_RIMB, Types.SPACE_CHAR);
    }

    public void setWlquComponTaxRimbNull(String wlquComponTaxRimbNull) {
        writeString(Pos.S089_COMPON_TAX_RIMB_NULL, wlquComponTaxRimbNull, Len.WLQU_COMPON_TAX_RIMB_NULL);
    }

    /**Original name: WLQU-COMPON-TAX-RIMB-NULL<br>*/
    public String getWlquComponTaxRimbNull() {
        return readString(Pos.S089_COMPON_TAX_RIMB_NULL, Len.WLQU_COMPON_TAX_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_COMPON_TAX_RIMB = 1;
        public static final int S089_COMPON_TAX_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_COMPON_TAX_RIMB = 8;
        public static final int WLQU_COMPON_TAX_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_COMPON_TAX_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_COMPON_TAX_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
