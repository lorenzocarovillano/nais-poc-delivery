package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-IMP-ADER<br>
 * Variable: TDR-IMP-ADER from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_IMP_ADER;
    }

    public void setTdrImpAder(AfDecimal tdrImpAder) {
        writeDecimalAsPacked(Pos.TDR_IMP_ADER, tdrImpAder.copy());
    }

    public void setTdrImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_IMP_ADER, Pos.TDR_IMP_ADER);
    }

    /**Original name: TDR-IMP-ADER<br>*/
    public AfDecimal getTdrImpAder() {
        return readPackedAsDecimal(Pos.TDR_IMP_ADER, Len.Int.TDR_IMP_ADER, Len.Fract.TDR_IMP_ADER);
    }

    public byte[] getTdrImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_IMP_ADER, Pos.TDR_IMP_ADER);
        return buffer;
    }

    public void setTdrImpAderNull(String tdrImpAderNull) {
        writeString(Pos.TDR_IMP_ADER_NULL, tdrImpAderNull, Len.TDR_IMP_ADER_NULL);
    }

    /**Original name: TDR-IMP-ADER-NULL<br>*/
    public String getTdrImpAderNull() {
        return readString(Pos.TDR_IMP_ADER_NULL, Len.TDR_IMP_ADER_NULL);
    }

    public String getTdrImpAderNullFormatted() {
        return Functions.padBlanks(getTdrImpAderNull(), Len.TDR_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_IMP_ADER = 1;
        public static final int TDR_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_IMP_ADER = 8;
        public static final int TDR_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
