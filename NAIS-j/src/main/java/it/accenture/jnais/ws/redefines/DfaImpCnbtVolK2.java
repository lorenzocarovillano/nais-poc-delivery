package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-CNBT-VOL-K2<br>
 * Variable: DFA-IMP-CNBT-VOL-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpCnbtVolK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpCnbtVolK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_CNBT_VOL_K2;
    }

    public void setDfaImpCnbtVolK2(AfDecimal dfaImpCnbtVolK2) {
        writeDecimalAsPacked(Pos.DFA_IMP_CNBT_VOL_K2, dfaImpCnbtVolK2.copy());
    }

    public void setDfaImpCnbtVolK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_CNBT_VOL_K2, Pos.DFA_IMP_CNBT_VOL_K2);
    }

    /**Original name: DFA-IMP-CNBT-VOL-K2<br>*/
    public AfDecimal getDfaImpCnbtVolK2() {
        return readPackedAsDecimal(Pos.DFA_IMP_CNBT_VOL_K2, Len.Int.DFA_IMP_CNBT_VOL_K2, Len.Fract.DFA_IMP_CNBT_VOL_K2);
    }

    public byte[] getDfaImpCnbtVolK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_CNBT_VOL_K2, Pos.DFA_IMP_CNBT_VOL_K2);
        return buffer;
    }

    public void setDfaImpCnbtVolK2Null(String dfaImpCnbtVolK2Null) {
        writeString(Pos.DFA_IMP_CNBT_VOL_K2_NULL, dfaImpCnbtVolK2Null, Len.DFA_IMP_CNBT_VOL_K2_NULL);
    }

    /**Original name: DFA-IMP-CNBT-VOL-K2-NULL<br>*/
    public String getDfaImpCnbtVolK2Null() {
        return readString(Pos.DFA_IMP_CNBT_VOL_K2_NULL, Len.DFA_IMP_CNBT_VOL_K2_NULL);
    }

    public String getDfaImpCnbtVolK2NullFormatted() {
        return Functions.padBlanks(getDfaImpCnbtVolK2Null(), Len.DFA_IMP_CNBT_VOL_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_VOL_K2 = 1;
        public static final int DFA_IMP_CNBT_VOL_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_VOL_K2 = 8;
        public static final int DFA_IMP_CNBT_VOL_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_VOL_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_VOL_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
