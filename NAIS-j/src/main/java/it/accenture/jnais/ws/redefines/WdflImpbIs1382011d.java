package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-IS-1382011D<br>
 * Variable: WDFL-IMPB-IS-1382011D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbIs1382011d extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbIs1382011d() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_IS1382011D;
    }

    public void setWdflImpbIs1382011d(AfDecimal wdflImpbIs1382011d) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_IS1382011D, wdflImpbIs1382011d.copy());
    }

    public void setWdflImpbIs1382011dFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_IS1382011D, Pos.WDFL_IMPB_IS1382011D);
    }

    /**Original name: WDFL-IMPB-IS-1382011D<br>*/
    public AfDecimal getWdflImpbIs1382011d() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_IS1382011D, Len.Int.WDFL_IMPB_IS1382011D, Len.Fract.WDFL_IMPB_IS1382011D);
    }

    public byte[] getWdflImpbIs1382011dAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_IS1382011D, Pos.WDFL_IMPB_IS1382011D);
        return buffer;
    }

    public void setWdflImpbIs1382011dNull(String wdflImpbIs1382011dNull) {
        writeString(Pos.WDFL_IMPB_IS1382011D_NULL, wdflImpbIs1382011dNull, Len.WDFL_IMPB_IS1382011D_NULL);
    }

    /**Original name: WDFL-IMPB-IS-1382011D-NULL<br>*/
    public String getWdflImpbIs1382011dNull() {
        return readString(Pos.WDFL_IMPB_IS1382011D_NULL, Len.WDFL_IMPB_IS1382011D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS1382011D = 1;
        public static final int WDFL_IMPB_IS1382011D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS1382011D = 8;
        public static final int WDFL_IMPB_IS1382011D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS1382011D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS1382011D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
