package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: STATO-FILESQS5<br>
 * Variable: STATO-FILESQS5 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class StatoFilesqs5 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char APERTO = 'A';
    public static final char CHIUSO = 'C';

    //==== METHODS ====
    public void setStatoFilesqs5(char statoFilesqs5) {
        this.value = statoFilesqs5;
    }

    public char getStatoFilesqs5() {
        return this.value;
    }

    public boolean isAperto() {
        return value == APERTO;
    }

    public void setAperto() {
        value = APERTO;
    }

    public void setChiuso() {
        value = CHIUSO;
    }
}
