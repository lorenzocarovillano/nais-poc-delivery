package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RDF-PC<br>
 * Variable: RDF-PC from program IDBSRDF0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RdfPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RdfPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RDF_PC;
    }

    public void setRdfPc(AfDecimal rdfPc) {
        writeDecimalAsPacked(Pos.RDF_PC, rdfPc.copy());
    }

    public void setRdfPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RDF_PC, Pos.RDF_PC);
    }

    /**Original name: RDF-PC<br>*/
    public AfDecimal getRdfPc() {
        return readPackedAsDecimal(Pos.RDF_PC, Len.Int.RDF_PC, Len.Fract.RDF_PC);
    }

    public byte[] getRdfPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RDF_PC, Pos.RDF_PC);
        return buffer;
    }

    public void setRdfPcNull(String rdfPcNull) {
        writeString(Pos.RDF_PC_NULL, rdfPcNull, Len.RDF_PC_NULL);
    }

    /**Original name: RDF-PC-NULL<br>*/
    public String getRdfPcNull() {
        return readString(Pos.RDF_PC_NULL, Len.RDF_PC_NULL);
    }

    public String getRdfPcNullFormatted() {
        return Functions.padBlanks(getRdfPcNull(), Len.RDF_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RDF_PC = 1;
        public static final int RDF_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_PC = 4;
        public static final int RDF_PC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_PC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RDF_PC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
