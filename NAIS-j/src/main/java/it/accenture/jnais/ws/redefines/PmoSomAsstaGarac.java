package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-SOM-ASSTA-GARAC<br>
 * Variable: PMO-SOM-ASSTA-GARAC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoSomAsstaGarac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoSomAsstaGarac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_SOM_ASSTA_GARAC;
    }

    public void setPmoSomAsstaGarac(AfDecimal pmoSomAsstaGarac) {
        writeDecimalAsPacked(Pos.PMO_SOM_ASSTA_GARAC, pmoSomAsstaGarac.copy());
    }

    public void setPmoSomAsstaGaracFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_SOM_ASSTA_GARAC, Pos.PMO_SOM_ASSTA_GARAC);
    }

    /**Original name: PMO-SOM-ASSTA-GARAC<br>*/
    public AfDecimal getPmoSomAsstaGarac() {
        return readPackedAsDecimal(Pos.PMO_SOM_ASSTA_GARAC, Len.Int.PMO_SOM_ASSTA_GARAC, Len.Fract.PMO_SOM_ASSTA_GARAC);
    }

    public byte[] getPmoSomAsstaGaracAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_SOM_ASSTA_GARAC, Pos.PMO_SOM_ASSTA_GARAC);
        return buffer;
    }

    public void initPmoSomAsstaGaracHighValues() {
        fill(Pos.PMO_SOM_ASSTA_GARAC, Len.PMO_SOM_ASSTA_GARAC, Types.HIGH_CHAR_VAL);
    }

    public void setPmoSomAsstaGaracNull(String pmoSomAsstaGaracNull) {
        writeString(Pos.PMO_SOM_ASSTA_GARAC_NULL, pmoSomAsstaGaracNull, Len.PMO_SOM_ASSTA_GARAC_NULL);
    }

    /**Original name: PMO-SOM-ASSTA-GARAC-NULL<br>*/
    public String getPmoSomAsstaGaracNull() {
        return readString(Pos.PMO_SOM_ASSTA_GARAC_NULL, Len.PMO_SOM_ASSTA_GARAC_NULL);
    }

    public String getPmoSomAsstaGaracNullFormatted() {
        return Functions.padBlanks(getPmoSomAsstaGaracNull(), Len.PMO_SOM_ASSTA_GARAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_SOM_ASSTA_GARAC = 1;
        public static final int PMO_SOM_ASSTA_GARAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_SOM_ASSTA_GARAC = 8;
        public static final int PMO_SOM_ASSTA_GARAC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_SOM_ASSTA_GARAC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_SOM_ASSTA_GARAC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
