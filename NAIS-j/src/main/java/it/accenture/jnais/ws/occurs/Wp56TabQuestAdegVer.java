package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvp561;
import it.accenture.jnais.copy.Wp56Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WP56-TAB-QUEST-ADEG-VER<br>
 * Variables: WP56-TAB-QUEST-ADEG-VER from copybook LCCVP56A<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wp56TabQuestAdegVer {

    //==== PROPERTIES ====
    //Original name: LCCVP561
    private Lccvp561 lccvp561 = new Lccvp561();

    //==== METHODS ====
    public void setWp56TabQuestAdegVerBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp561.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp561.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp561.Len.Int.ID_PTF, 0));
        position += Lccvp561.Len.ID_PTF;
        lccvp561.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp56TabQuestAdegVerBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp561.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp561.getIdPtf(), Lccvp561.Len.Int.ID_PTF, 0);
        position += Lccvp561.Len.ID_PTF;
        lccvp561.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWp56TabQuestAdegVerSpaces() {
        lccvp561.initLccvp561Spaces();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WP56_TAB_QUEST_ADEG_VER = WpolStatus.Len.STATUS + Lccvp561.Len.ID_PTF + Wp56Dati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
