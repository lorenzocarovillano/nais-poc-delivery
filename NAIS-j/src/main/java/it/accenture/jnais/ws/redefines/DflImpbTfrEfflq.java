package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-TFR-EFFLQ<br>
 * Variable: DFL-IMPB-TFR-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbTfrEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbTfrEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_TFR_EFFLQ;
    }

    public void setDflImpbTfrEfflq(AfDecimal dflImpbTfrEfflq) {
        writeDecimalAsPacked(Pos.DFL_IMPB_TFR_EFFLQ, dflImpbTfrEfflq.copy());
    }

    public void setDflImpbTfrEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_TFR_EFFLQ, Pos.DFL_IMPB_TFR_EFFLQ);
    }

    /**Original name: DFL-IMPB-TFR-EFFLQ<br>*/
    public AfDecimal getDflImpbTfrEfflq() {
        return readPackedAsDecimal(Pos.DFL_IMPB_TFR_EFFLQ, Len.Int.DFL_IMPB_TFR_EFFLQ, Len.Fract.DFL_IMPB_TFR_EFFLQ);
    }

    public byte[] getDflImpbTfrEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_TFR_EFFLQ, Pos.DFL_IMPB_TFR_EFFLQ);
        return buffer;
    }

    public void setDflImpbTfrEfflqNull(String dflImpbTfrEfflqNull) {
        writeString(Pos.DFL_IMPB_TFR_EFFLQ_NULL, dflImpbTfrEfflqNull, Len.DFL_IMPB_TFR_EFFLQ_NULL);
    }

    /**Original name: DFL-IMPB-TFR-EFFLQ-NULL<br>*/
    public String getDflImpbTfrEfflqNull() {
        return readString(Pos.DFL_IMPB_TFR_EFFLQ_NULL, Len.DFL_IMPB_TFR_EFFLQ_NULL);
    }

    public String getDflImpbTfrEfflqNullFormatted() {
        return Functions.padBlanks(getDflImpbTfrEfflqNull(), Len.DFL_IMPB_TFR_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TFR_EFFLQ = 1;
        public static final int DFL_IMPB_TFR_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TFR_EFFLQ = 8;
        public static final int DFL_IMPB_TFR_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TFR_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TFR_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
