package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-SCARTA-TGA<br>
 * Variable: SW-SCARTA-TGA from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwScartaTga {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TGA = 'S';
    public static final char TGA_NO = 'N';

    //==== METHODS ====
    public void setSwScartaTga(char swScartaTga) {
        this.value = swScartaTga;
    }

    public char getSwScartaTga() {
        return this.value;
    }

    public void setTgaNo() {
        value = TGA_NO;
    }
}
