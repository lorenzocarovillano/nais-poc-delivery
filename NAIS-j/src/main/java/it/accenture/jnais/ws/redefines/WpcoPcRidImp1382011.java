package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-PC-RID-IMP-1382011<br>
 * Variable: WPCO-PC-RID-IMP-1382011 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoPcRidImp1382011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoPcRidImp1382011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_PC_RID_IMP1382011;
    }

    public void setWpcoPcRidImp1382011(AfDecimal wpcoPcRidImp1382011) {
        writeDecimalAsPacked(Pos.WPCO_PC_RID_IMP1382011, wpcoPcRidImp1382011.copy());
    }

    public void setDpcoPcRidImp1382011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_PC_RID_IMP1382011, Pos.WPCO_PC_RID_IMP1382011);
    }

    /**Original name: WPCO-PC-RID-IMP-1382011<br>*/
    public AfDecimal getWpcoPcRidImp1382011() {
        return readPackedAsDecimal(Pos.WPCO_PC_RID_IMP1382011, Len.Int.WPCO_PC_RID_IMP1382011, Len.Fract.WPCO_PC_RID_IMP1382011);
    }

    public byte[] getWpcoPcRidImp1382011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_PC_RID_IMP1382011, Pos.WPCO_PC_RID_IMP1382011);
        return buffer;
    }

    public void setWpcoPcRidImp1382011Null(String wpcoPcRidImp1382011Null) {
        writeString(Pos.WPCO_PC_RID_IMP1382011_NULL, wpcoPcRidImp1382011Null, Len.WPCO_PC_RID_IMP1382011_NULL);
    }

    /**Original name: WPCO-PC-RID-IMP-1382011-NULL<br>*/
    public String getWpcoPcRidImp1382011Null() {
        return readString(Pos.WPCO_PC_RID_IMP1382011_NULL, Len.WPCO_PC_RID_IMP1382011_NULL);
    }

    public String getWpcoPcRidImp1382011NullFormatted() {
        return Functions.padBlanks(getWpcoPcRidImp1382011Null(), Len.WPCO_PC_RID_IMP1382011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_PC_RID_IMP1382011 = 1;
        public static final int WPCO_PC_RID_IMP1382011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_PC_RID_IMP1382011 = 4;
        public static final int WPCO_PC_RID_IMP1382011_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_PC_RID_IMP1382011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_PC_RID_IMP1382011 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
