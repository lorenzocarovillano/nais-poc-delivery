package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-ALQ-SCON<br>
 * Variable: L3421-ALQ-SCON from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421AlqScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421AlqScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_ALQ_SCON;
    }

    public void setL3421AlqSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_ALQ_SCON, Pos.L3421_ALQ_SCON);
    }

    /**Original name: L3421-ALQ-SCON<br>*/
    public AfDecimal getL3421AlqScon() {
        return readPackedAsDecimal(Pos.L3421_ALQ_SCON, Len.Int.L3421_ALQ_SCON, Len.Fract.L3421_ALQ_SCON);
    }

    public byte[] getL3421AlqSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_ALQ_SCON, Pos.L3421_ALQ_SCON);
        return buffer;
    }

    /**Original name: L3421-ALQ-SCON-NULL<br>*/
    public String getL3421AlqSconNull() {
        return readString(Pos.L3421_ALQ_SCON_NULL, Len.L3421_ALQ_SCON_NULL);
    }

    public String getL3421AlqSconNullFormatted() {
        return Functions.padBlanks(getL3421AlqSconNull(), Len.L3421_ALQ_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_ALQ_SCON = 1;
        public static final int L3421_ALQ_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_ALQ_SCON = 4;
        public static final int L3421_ALQ_SCON_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_ALQ_SCON = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_ALQ_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
