package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P01-DT-EST-FINANZ<br>
 * Variable: P01-DT-EST-FINANZ from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P01DtEstFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P01DtEstFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P01_DT_EST_FINANZ;
    }

    public void setP01DtEstFinanz(int p01DtEstFinanz) {
        writeIntAsPacked(Pos.P01_DT_EST_FINANZ, p01DtEstFinanz, Len.Int.P01_DT_EST_FINANZ);
    }

    public void setP01DtEstFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P01_DT_EST_FINANZ, Pos.P01_DT_EST_FINANZ);
    }

    /**Original name: P01-DT-EST-FINANZ<br>*/
    public int getP01DtEstFinanz() {
        return readPackedAsInt(Pos.P01_DT_EST_FINANZ, Len.Int.P01_DT_EST_FINANZ);
    }

    public byte[] getP01DtEstFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P01_DT_EST_FINANZ, Pos.P01_DT_EST_FINANZ);
        return buffer;
    }

    public void setP01DtEstFinanzNull(String p01DtEstFinanzNull) {
        writeString(Pos.P01_DT_EST_FINANZ_NULL, p01DtEstFinanzNull, Len.P01_DT_EST_FINANZ_NULL);
    }

    /**Original name: P01-DT-EST-FINANZ-NULL<br>*/
    public String getP01DtEstFinanzNull() {
        return readString(Pos.P01_DT_EST_FINANZ_NULL, Len.P01_DT_EST_FINANZ_NULL);
    }

    public String getP01DtEstFinanzNullFormatted() {
        return Functions.padBlanks(getP01DtEstFinanzNull(), Len.P01_DT_EST_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P01_DT_EST_FINANZ = 1;
        public static final int P01_DT_EST_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P01_DT_EST_FINANZ = 5;
        public static final int P01_DT_EST_FINANZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P01_DT_EST_FINANZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
