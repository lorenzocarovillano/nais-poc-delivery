package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: LDBV0371-FRAZ<br>
 * Variable: LDBV0371-FRAZ from program LDBS0370<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ldbv0371Fraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Ldbv0371Fraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV0371_FRAZ;
    }

    public void setLdbv0371Fraz(int ldbv0371Fraz) {
        writeIntAsPacked(Pos.LDBV0371_FRAZ, ldbv0371Fraz, Len.Int.LDBV0371_FRAZ);
    }

    public void setLdbv0371FrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LDBV0371_FRAZ, Pos.LDBV0371_FRAZ);
    }

    /**Original name: LDBV0371-FRAZ<br>*/
    public int getLdbv0371Fraz() {
        return readPackedAsInt(Pos.LDBV0371_FRAZ, Len.Int.LDBV0371_FRAZ);
    }

    public byte[] getLdbv0371FrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LDBV0371_FRAZ, Pos.LDBV0371_FRAZ);
        return buffer;
    }

    public void setLdbv0371FrazNull(String ldbv0371FrazNull) {
        writeString(Pos.LDBV0371_FRAZ_NULL, ldbv0371FrazNull, Len.LDBV0371_FRAZ_NULL);
    }

    /**Original name: LDBV0371-FRAZ-NULL<br>*/
    public String getLdbv0371FrazNull() {
        return readString(Pos.LDBV0371_FRAZ_NULL, Len.LDBV0371_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LDBV0371_FRAZ = 1;
        public static final int LDBV0371_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV0371_FRAZ = 3;
        public static final int LDBV0371_FRAZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV0371_FRAZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
