package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-INCR-PRE<br>
 * Variable: TGA-INCR-PRE from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaIncrPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaIncrPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_INCR_PRE;
    }

    public void setTgaIncrPre(AfDecimal tgaIncrPre) {
        writeDecimalAsPacked(Pos.TGA_INCR_PRE, tgaIncrPre.copy());
    }

    public void setTgaIncrPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_INCR_PRE, Pos.TGA_INCR_PRE);
    }

    /**Original name: TGA-INCR-PRE<br>*/
    public AfDecimal getTgaIncrPre() {
        return readPackedAsDecimal(Pos.TGA_INCR_PRE, Len.Int.TGA_INCR_PRE, Len.Fract.TGA_INCR_PRE);
    }

    public byte[] getTgaIncrPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_INCR_PRE, Pos.TGA_INCR_PRE);
        return buffer;
    }

    public void setTgaIncrPreNull(String tgaIncrPreNull) {
        writeString(Pos.TGA_INCR_PRE_NULL, tgaIncrPreNull, Len.TGA_INCR_PRE_NULL);
    }

    /**Original name: TGA-INCR-PRE-NULL<br>*/
    public String getTgaIncrPreNull() {
        return readString(Pos.TGA_INCR_PRE_NULL, Len.TGA_INCR_PRE_NULL);
    }

    public String getTgaIncrPreNullFormatted() {
        return Functions.padBlanks(getTgaIncrPreNull(), Len.TGA_INCR_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_INCR_PRE = 1;
        public static final int TGA_INCR_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_INCR_PRE = 8;
        public static final int TGA_INCR_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_INCR_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_INCR_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
