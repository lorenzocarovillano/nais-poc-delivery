package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MFZ-DT-ELAB<br>
 * Variable: MFZ-DT-ELAB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MfzDtElab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MfzDtElab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MFZ_DT_ELAB;
    }

    public void setMfzDtElab(int mfzDtElab) {
        writeIntAsPacked(Pos.MFZ_DT_ELAB, mfzDtElab, Len.Int.MFZ_DT_ELAB);
    }

    public void setMfzDtElabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MFZ_DT_ELAB, Pos.MFZ_DT_ELAB);
    }

    /**Original name: MFZ-DT-ELAB<br>*/
    public int getMfzDtElab() {
        return readPackedAsInt(Pos.MFZ_DT_ELAB, Len.Int.MFZ_DT_ELAB);
    }

    public byte[] getMfzDtElabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MFZ_DT_ELAB, Pos.MFZ_DT_ELAB);
        return buffer;
    }

    public void setMfzDtElabNull(String mfzDtElabNull) {
        writeString(Pos.MFZ_DT_ELAB_NULL, mfzDtElabNull, Len.MFZ_DT_ELAB_NULL);
    }

    /**Original name: MFZ-DT-ELAB-NULL<br>*/
    public String getMfzDtElabNull() {
        return readString(Pos.MFZ_DT_ELAB_NULL, Len.MFZ_DT_ELAB_NULL);
    }

    public String getMfzDtElabNullFormatted() {
        return Functions.padBlanks(getMfzDtElabNull(), Len.MFZ_DT_ELAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MFZ_DT_ELAB = 1;
        public static final int MFZ_DT_ELAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MFZ_DT_ELAB = 5;
        public static final int MFZ_DT_ELAB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MFZ_DT_ELAB = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
