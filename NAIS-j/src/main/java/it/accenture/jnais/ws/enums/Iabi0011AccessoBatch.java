package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABI0011-ACCESSO-BATCH<br>
 * Variable: IABI0011-ACCESSO-BATCH from copybook IABI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabi0011AccessoBatch {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.ACCESSO_BATCH);
    public static final String ALL_BATCH = "ALLBTC";
    public static final String X_DT_EFF_MAX = "MAXEFF";
    public static final String X_DT_EFF_MIN = "MINEFF";
    public static final String X_DT_INS_MAX = "MAXINS";
    public static final String X_DT_INS_MIN = "MININS";

    //==== METHODS ====
    public void setAccessoBatch(String accessoBatch) {
        this.value = Functions.subString(accessoBatch, Len.ACCESSO_BATCH);
    }

    public String getAccessoBatch() {
        return this.value;
    }

    public String getAccessoBatchFormatted() {
        return Functions.padBlanks(getAccessoBatch(), Len.ACCESSO_BATCH);
    }

    public boolean isAllBatch() {
        return value.equals(ALL_BATCH);
    }

    public boolean isxDtEffMax() {
        return value.equals(X_DT_EFF_MAX);
    }

    public boolean isxDtEffMin() {
        return value.equals(X_DT_EFF_MIN);
    }

    public boolean isxDtInsMax() {
        return value.equals(X_DT_INS_MAX);
    }

    public boolean isxDtInsMin() {
        return value.equals(X_DT_INS_MIN);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ACCESSO_BATCH = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
