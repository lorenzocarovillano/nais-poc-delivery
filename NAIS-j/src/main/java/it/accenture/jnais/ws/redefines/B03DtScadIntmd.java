package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-SCAD-INTMD<br>
 * Variable: B03-DT-SCAD-INTMD from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtScadIntmd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtScadIntmd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_SCAD_INTMD;
    }

    public void setB03DtScadIntmd(int b03DtScadIntmd) {
        writeIntAsPacked(Pos.B03_DT_SCAD_INTMD, b03DtScadIntmd, Len.Int.B03_DT_SCAD_INTMD);
    }

    public void setB03DtScadIntmdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_SCAD_INTMD, Pos.B03_DT_SCAD_INTMD);
    }

    /**Original name: B03-DT-SCAD-INTMD<br>*/
    public int getB03DtScadIntmd() {
        return readPackedAsInt(Pos.B03_DT_SCAD_INTMD, Len.Int.B03_DT_SCAD_INTMD);
    }

    public byte[] getB03DtScadIntmdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_SCAD_INTMD, Pos.B03_DT_SCAD_INTMD);
        return buffer;
    }

    public void setB03DtScadIntmdNull(String b03DtScadIntmdNull) {
        writeString(Pos.B03_DT_SCAD_INTMD_NULL, b03DtScadIntmdNull, Len.B03_DT_SCAD_INTMD_NULL);
    }

    /**Original name: B03-DT-SCAD-INTMD-NULL<br>*/
    public String getB03DtScadIntmdNull() {
        return readString(Pos.B03_DT_SCAD_INTMD_NULL, Len.B03_DT_SCAD_INTMD_NULL);
    }

    public String getB03DtScadIntmdNullFormatted() {
        return Functions.padBlanks(getB03DtScadIntmdNull(), Len.B03_DT_SCAD_INTMD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_SCAD_INTMD = 1;
        public static final int B03_DT_SCAD_INTMD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_SCAD_INTMD = 5;
        public static final int B03_DT_SCAD_INTMD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_SCAD_INTMD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
