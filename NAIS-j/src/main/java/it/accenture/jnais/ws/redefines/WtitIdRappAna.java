package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-ID-RAPP-ANA<br>
 * Variable: WTIT-ID-RAPP-ANA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitIdRappAna extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitIdRappAna() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_ID_RAPP_ANA;
    }

    public void setWtitIdRappAna(int wtitIdRappAna) {
        writeIntAsPacked(Pos.WTIT_ID_RAPP_ANA, wtitIdRappAna, Len.Int.WTIT_ID_RAPP_ANA);
    }

    public void setWtitIdRappAnaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_ID_RAPP_ANA, Pos.WTIT_ID_RAPP_ANA);
    }

    /**Original name: WTIT-ID-RAPP-ANA<br>*/
    public int getWtitIdRappAna() {
        return readPackedAsInt(Pos.WTIT_ID_RAPP_ANA, Len.Int.WTIT_ID_RAPP_ANA);
    }

    public byte[] getWtitIdRappAnaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_ID_RAPP_ANA, Pos.WTIT_ID_RAPP_ANA);
        return buffer;
    }

    public void initWtitIdRappAnaSpaces() {
        fill(Pos.WTIT_ID_RAPP_ANA, Len.WTIT_ID_RAPP_ANA, Types.SPACE_CHAR);
    }

    public void setWtitIdRappAnaNull(String wtitIdRappAnaNull) {
        writeString(Pos.WTIT_ID_RAPP_ANA_NULL, wtitIdRappAnaNull, Len.WTIT_ID_RAPP_ANA_NULL);
    }

    /**Original name: WTIT-ID-RAPP-ANA-NULL<br>*/
    public String getWtitIdRappAnaNull() {
        return readString(Pos.WTIT_ID_RAPP_ANA_NULL, Len.WTIT_ID_RAPP_ANA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_ID_RAPP_ANA = 1;
        public static final int WTIT_ID_RAPP_ANA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_ID_RAPP_ANA = 5;
        public static final int WTIT_ID_RAPP_ANA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_ID_RAPP_ANA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
