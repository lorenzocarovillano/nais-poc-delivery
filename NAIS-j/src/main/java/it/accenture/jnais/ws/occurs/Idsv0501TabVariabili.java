package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0501-TAB-VARIABILI<br>
 * Variables: IDSV0501-TAB-VARIABILI from copybook IDSV0501<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idsv0501TabVariabili {

    //==== PROPERTIES ====
    //Original name: IDSV0501-TP-DATO
    private char tpDato = DefaultValues.CHAR_VAL;
    //Original name: IDSV0501-VAL-NUM
    private long valNum = DefaultValues.LONG_VAL;
    //Original name: IDSV0501-VAL-IMP
    private AfDecimal valImp = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);
    //Original name: IDSV0501-VAL-PERC
    private AfDecimal valPerc = new AfDecimal(DefaultValues.DEC_VAL, 18, 9);
    //Original name: IDSV0501-VAL-STR
    private String valStr = DefaultValues.stringVal(Len.VAL_STR);

    //==== METHODS ====
    public void setIdsv0501TpDato(char idsv0501TpDato) {
        this.tpDato = idsv0501TpDato;
    }

    public void setIdsv0501TpDatoFormatted(String idsv0501TpDato) {
        setIdsv0501TpDato(Functions.charAt(idsv0501TpDato, Types.CHAR_SIZE));
    }

    public char getIdsv0501TpDato() {
        return this.tpDato;
    }

    public void setIdsv0501ValNum(long idsv0501ValNum) {
        this.valNum = idsv0501ValNum;
    }

    public long getIdsv0501ValNum() {
        return this.valNum;
    }

    public void setIdsv0501ValImp(AfDecimal idsv0501ValImp) {
        this.valImp.assign(idsv0501ValImp);
    }

    public AfDecimal getIdsv0501ValImp() {
        return this.valImp.copy();
    }

    public void setIdsv0501ValPerc(AfDecimal idsv0501ValPerc) {
        this.valPerc.assign(idsv0501ValPerc);
    }

    public AfDecimal getIdsv0501ValPerc() {
        return this.valPerc.copy();
    }

    public void setIdsv0501ValStr(String idsv0501ValStr) {
        this.valStr = Functions.subString(idsv0501ValStr, Len.VAL_STR);
    }

    public String getIdsv0501ValStr() {
        return this.valStr;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int VAL_STR = 60;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
