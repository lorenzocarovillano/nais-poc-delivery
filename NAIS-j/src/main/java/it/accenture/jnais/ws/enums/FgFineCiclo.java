package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FG-FINE-CICLO<br>
 * Variable: FG-FINE-CICLO from copybook LVEC0202<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FgFineCiclo {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char CICLO = 'S';
    public static final char CICLO_NO = 'N';

    //==== METHODS ====
    public void setFgFineCiclo(char fgFineCiclo) {
        this.value = fgFineCiclo;
    }

    public char getFgFineCiclo() {
        return this.value;
    }

    public boolean isCiclo() {
        return value == CICLO;
    }

    public void setCiclo() {
        value = CICLO;
    }

    public void setCicloNo() {
        value = CICLO_NO;
    }
}
