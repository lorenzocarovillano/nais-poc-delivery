package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0010;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSL710<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsl710Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-L71-COD-RAMO
    private short indL71CodRamo = DefaultValues.BIN_SHORT_VAL;
    //Original name: L71-DT-ULT-ELAB-DB
    private String l71DtUltElabDb = DefaultValues.stringVal(Len.L71_DT_ULT_ELAB_DB);

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndL71CodRamo(short indL71CodRamo) {
        this.indL71CodRamo = indL71CodRamo;
    }

    public short getIndL71CodRamo() {
        return this.indL71CodRamo;
    }

    public void setL71DtUltElabDb(String l71DtUltElabDb) {
        this.l71DtUltElabDb = Functions.subString(l71DtUltElabDb, Len.L71_DT_ULT_ELAB_DB);
    }

    public String getL71DtUltElabDb() {
        return this.l71DtUltElabDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L71_DT_ULT_ELAB_DB = 10;
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
