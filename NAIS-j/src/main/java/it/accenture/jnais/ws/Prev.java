package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.L27IdOgg;

/**Original name: PREV<br>
 * Variable: PREV from copybook IDBVL271<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Prev extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: L27-ID-PREV
    private int l27IdPrev = DefaultValues.INT_VAL;
    //Original name: L27-COD-COMP-ANIA
    private int l27CodCompAnia = DefaultValues.INT_VAL;
    //Original name: L27-STAT-PREV
    private String l27StatPrev = DefaultValues.stringVal(Len.L27_STAT_PREV);
    //Original name: L27-ID-OGG
    private L27IdOgg l27IdOgg = new L27IdOgg();
    //Original name: L27-TP-OGG
    private String l27TpOgg = DefaultValues.stringVal(Len.L27_TP_OGG);
    //Original name: L27-IB-OGG
    private String l27IbOgg = DefaultValues.stringVal(Len.L27_IB_OGG);
    //Original name: L27-DS-OPER-SQL
    private char l27DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: L27-DS-VER
    private int l27DsVer = DefaultValues.INT_VAL;
    //Original name: L27-DS-TS-CPTZ
    private long l27DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: L27-DS-UTENTE
    private String l27DsUtente = DefaultValues.stringVal(Len.L27_DS_UTENTE);
    //Original name: L27-DS-STATO-ELAB
    private char l27DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PREV;
    }

    @Override
    public void deserialize(byte[] buf) {
        setPrevBytes(buf);
    }

    public void setPrevBytes(byte[] buffer) {
        setPrevBytes(buffer, 1);
    }

    public byte[] getPrevBytes() {
        byte[] buffer = new byte[Len.PREV];
        return getPrevBytes(buffer, 1);
    }

    public void setPrevBytes(byte[] buffer, int offset) {
        int position = offset;
        l27IdPrev = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L27_ID_PREV, 0);
        position += Len.L27_ID_PREV;
        l27CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L27_COD_COMP_ANIA, 0);
        position += Len.L27_COD_COMP_ANIA;
        l27StatPrev = MarshalByte.readString(buffer, position, Len.L27_STAT_PREV);
        position += Len.L27_STAT_PREV;
        l27IdOgg.setL27IdOggFromBuffer(buffer, position);
        position += L27IdOgg.Len.L27_ID_OGG;
        l27TpOgg = MarshalByte.readString(buffer, position, Len.L27_TP_OGG);
        position += Len.L27_TP_OGG;
        l27IbOgg = MarshalByte.readString(buffer, position, Len.L27_IB_OGG);
        position += Len.L27_IB_OGG;
        l27DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l27DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L27_DS_VER, 0);
        position += Len.L27_DS_VER;
        l27DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L27_DS_TS_CPTZ, 0);
        position += Len.L27_DS_TS_CPTZ;
        l27DsUtente = MarshalByte.readString(buffer, position, Len.L27_DS_UTENTE);
        position += Len.L27_DS_UTENTE;
        l27DsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getPrevBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, l27IdPrev, Len.Int.L27_ID_PREV, 0);
        position += Len.L27_ID_PREV;
        MarshalByte.writeIntAsPacked(buffer, position, l27CodCompAnia, Len.Int.L27_COD_COMP_ANIA, 0);
        position += Len.L27_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, l27StatPrev, Len.L27_STAT_PREV);
        position += Len.L27_STAT_PREV;
        l27IdOgg.getL27IdOggAsBuffer(buffer, position);
        position += L27IdOgg.Len.L27_ID_OGG;
        MarshalByte.writeString(buffer, position, l27TpOgg, Len.L27_TP_OGG);
        position += Len.L27_TP_OGG;
        MarshalByte.writeString(buffer, position, l27IbOgg, Len.L27_IB_OGG);
        position += Len.L27_IB_OGG;
        MarshalByte.writeChar(buffer, position, l27DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, l27DsVer, Len.Int.L27_DS_VER, 0);
        position += Len.L27_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, l27DsTsCptz, Len.Int.L27_DS_TS_CPTZ, 0);
        position += Len.L27_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, l27DsUtente, Len.L27_DS_UTENTE);
        position += Len.L27_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, l27DsStatoElab);
        return buffer;
    }

    public void setL27IdPrev(int l27IdPrev) {
        this.l27IdPrev = l27IdPrev;
    }

    public int getL27IdPrev() {
        return this.l27IdPrev;
    }

    public void setL27CodCompAnia(int l27CodCompAnia) {
        this.l27CodCompAnia = l27CodCompAnia;
    }

    public int getL27CodCompAnia() {
        return this.l27CodCompAnia;
    }

    public void setL27StatPrev(String l27StatPrev) {
        this.l27StatPrev = Functions.subString(l27StatPrev, Len.L27_STAT_PREV);
    }

    public String getL27StatPrev() {
        return this.l27StatPrev;
    }

    public void setL27TpOgg(String l27TpOgg) {
        this.l27TpOgg = Functions.subString(l27TpOgg, Len.L27_TP_OGG);
    }

    public String getL27TpOgg() {
        return this.l27TpOgg;
    }

    public String getL27TpOggFormatted() {
        return Functions.padBlanks(getL27TpOgg(), Len.L27_TP_OGG);
    }

    public void setL27IbOgg(String l27IbOgg) {
        this.l27IbOgg = Functions.subString(l27IbOgg, Len.L27_IB_OGG);
    }

    public String getL27IbOgg() {
        return this.l27IbOgg;
    }

    public void setL27DsOperSql(char l27DsOperSql) {
        this.l27DsOperSql = l27DsOperSql;
    }

    public void setL27DsOperSqlFormatted(String l27DsOperSql) {
        setL27DsOperSql(Functions.charAt(l27DsOperSql, Types.CHAR_SIZE));
    }

    public char getL27DsOperSql() {
        return this.l27DsOperSql;
    }

    public void setL27DsVer(int l27DsVer) {
        this.l27DsVer = l27DsVer;
    }

    public int getL27DsVer() {
        return this.l27DsVer;
    }

    public void setL27DsTsCptz(long l27DsTsCptz) {
        this.l27DsTsCptz = l27DsTsCptz;
    }

    public long getL27DsTsCptz() {
        return this.l27DsTsCptz;
    }

    public void setL27DsUtente(String l27DsUtente) {
        this.l27DsUtente = Functions.subString(l27DsUtente, Len.L27_DS_UTENTE);
    }

    public String getL27DsUtente() {
        return this.l27DsUtente;
    }

    public void setL27DsStatoElab(char l27DsStatoElab) {
        this.l27DsStatoElab = l27DsStatoElab;
    }

    public void setL27DsStatoElabFormatted(String l27DsStatoElab) {
        setL27DsStatoElab(Functions.charAt(l27DsStatoElab, Types.CHAR_SIZE));
    }

    public char getL27DsStatoElab() {
        return this.l27DsStatoElab;
    }

    public L27IdOgg getL27IdOgg() {
        return l27IdOgg;
    }

    @Override
    public byte[] serialize() {
        return getPrevBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L27_ID_PREV = 5;
        public static final int L27_COD_COMP_ANIA = 3;
        public static final int L27_STAT_PREV = 2;
        public static final int L27_TP_OGG = 2;
        public static final int L27_IB_OGG = 40;
        public static final int L27_DS_OPER_SQL = 1;
        public static final int L27_DS_VER = 5;
        public static final int L27_DS_TS_CPTZ = 10;
        public static final int L27_DS_UTENTE = 20;
        public static final int L27_DS_STATO_ELAB = 1;
        public static final int PREV = L27_ID_PREV + L27_COD_COMP_ANIA + L27_STAT_PREV + L27IdOgg.Len.L27_ID_OGG + L27_TP_OGG + L27_IB_OGG + L27_DS_OPER_SQL + L27_DS_VER + L27_DS_TS_CPTZ + L27_DS_UTENTE + L27_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L27_ID_PREV = 9;
            public static final int L27_COD_COMP_ANIA = 5;
            public static final int L27_DS_VER = 9;
            public static final int L27_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
