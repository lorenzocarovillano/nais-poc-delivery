package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTGZ-CED<br>
 * Variable: WPCO-DT-ULTGZ-CED from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltgzCed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltgzCed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTGZ_CED;
    }

    public void setWpcoDtUltgzCed(int wpcoDtUltgzCed) {
        writeIntAsPacked(Pos.WPCO_DT_ULTGZ_CED, wpcoDtUltgzCed, Len.Int.WPCO_DT_ULTGZ_CED);
    }

    public void setDpcoDtUltgzCedFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTGZ_CED, Pos.WPCO_DT_ULTGZ_CED);
    }

    /**Original name: WPCO-DT-ULTGZ-CED<br>*/
    public int getWpcoDtUltgzCed() {
        return readPackedAsInt(Pos.WPCO_DT_ULTGZ_CED, Len.Int.WPCO_DT_ULTGZ_CED);
    }

    public byte[] getWpcoDtUltgzCedAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTGZ_CED, Pos.WPCO_DT_ULTGZ_CED);
        return buffer;
    }

    public void setWpcoDtUltgzCedNull(String wpcoDtUltgzCedNull) {
        writeString(Pos.WPCO_DT_ULTGZ_CED_NULL, wpcoDtUltgzCedNull, Len.WPCO_DT_ULTGZ_CED_NULL);
    }

    /**Original name: WPCO-DT-ULTGZ-CED-NULL<br>*/
    public String getWpcoDtUltgzCedNull() {
        return readString(Pos.WPCO_DT_ULTGZ_CED_NULL, Len.WPCO_DT_ULTGZ_CED_NULL);
    }

    public String getWpcoDtUltgzCedNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltgzCedNull(), Len.WPCO_DT_ULTGZ_CED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTGZ_CED = 1;
        public static final int WPCO_DT_ULTGZ_CED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTGZ_CED = 5;
        public static final int WPCO_DT_ULTGZ_CED_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTGZ_CED = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
