package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-DT-1A-CNBZ<br>
 * Variable: WDFA-DT-1A-CNBZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaDt1aCnbz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaDt1aCnbz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_DT1A_CNBZ;
    }

    public void setWdfaDt1aCnbz(int wdfaDt1aCnbz) {
        writeIntAsPacked(Pos.WDFA_DT1A_CNBZ, wdfaDt1aCnbz, Len.Int.WDFA_DT1A_CNBZ);
    }

    public void setWdfaDt1aCnbzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_DT1A_CNBZ, Pos.WDFA_DT1A_CNBZ);
    }

    /**Original name: WDFA-DT-1A-CNBZ<br>*/
    public int getWdfaDt1aCnbz() {
        return readPackedAsInt(Pos.WDFA_DT1A_CNBZ, Len.Int.WDFA_DT1A_CNBZ);
    }

    public byte[] getWdfaDt1aCnbzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_DT1A_CNBZ, Pos.WDFA_DT1A_CNBZ);
        return buffer;
    }

    public void setWdfaDt1aCnbzNull(String wdfaDt1aCnbzNull) {
        writeString(Pos.WDFA_DT1A_CNBZ_NULL, wdfaDt1aCnbzNull, Len.WDFA_DT1A_CNBZ_NULL);
    }

    /**Original name: WDFA-DT-1A-CNBZ-NULL<br>*/
    public String getWdfaDt1aCnbzNull() {
        return readString(Pos.WDFA_DT1A_CNBZ_NULL, Len.WDFA_DT1A_CNBZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_DT1A_CNBZ = 1;
        public static final int WDFA_DT1A_CNBZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_DT1A_CNBZ = 5;
        public static final int WDFA_DT1A_CNBZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_DT1A_CNBZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
