package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-FLAG-FINE-VALORE<br>
 * Variable: WS-FLAG-FINE-VALORE from program IDSS0160<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFlagFineValore {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_FINE_VALORE = 'S';
    public static final char NO_FINE_VALORE = 'N';

    //==== METHODS ====
    public void setWsFlagFineValore(char wsFlagFineValore) {
        this.value = wsFlagFineValore;
    }

    public char getWsFlagFineValore() {
        return this.value;
    }

    public boolean isSiFineValore() {
        return value == SI_FINE_VALORE;
    }

    public void setSiFineValore() {
        value = SI_FINE_VALORE;
    }

    public void setNoFineValore() {
        value = NO_FINE_VALORE;
    }
}
