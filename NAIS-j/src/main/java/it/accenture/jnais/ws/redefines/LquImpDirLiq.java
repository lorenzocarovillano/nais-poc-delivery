package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-DIR-LIQ<br>
 * Variable: LQU-IMP-DIR-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpDirLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpDirLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_DIR_LIQ;
    }

    public void setLquImpDirLiq(AfDecimal lquImpDirLiq) {
        writeDecimalAsPacked(Pos.LQU_IMP_DIR_LIQ, lquImpDirLiq.copy());
    }

    public void setLquImpDirLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_DIR_LIQ, Pos.LQU_IMP_DIR_LIQ);
    }

    /**Original name: LQU-IMP-DIR-LIQ<br>*/
    public AfDecimal getLquImpDirLiq() {
        return readPackedAsDecimal(Pos.LQU_IMP_DIR_LIQ, Len.Int.LQU_IMP_DIR_LIQ, Len.Fract.LQU_IMP_DIR_LIQ);
    }

    public byte[] getLquImpDirLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_DIR_LIQ, Pos.LQU_IMP_DIR_LIQ);
        return buffer;
    }

    public void setLquImpDirLiqNull(String lquImpDirLiqNull) {
        writeString(Pos.LQU_IMP_DIR_LIQ_NULL, lquImpDirLiqNull, Len.LQU_IMP_DIR_LIQ_NULL);
    }

    /**Original name: LQU-IMP-DIR-LIQ-NULL<br>*/
    public String getLquImpDirLiqNull() {
        return readString(Pos.LQU_IMP_DIR_LIQ_NULL, Len.LQU_IMP_DIR_LIQ_NULL);
    }

    public String getLquImpDirLiqNullFormatted() {
        return Functions.padBlanks(getLquImpDirLiqNull(), Len.LQU_IMP_DIR_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_DIR_LIQ = 1;
        public static final int LQU_IMP_DIR_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_DIR_LIQ = 8;
        public static final int LQU_IMP_DIR_LIQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_DIR_LIQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_DIR_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
