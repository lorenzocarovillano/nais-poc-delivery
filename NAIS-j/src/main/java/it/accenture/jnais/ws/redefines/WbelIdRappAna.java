package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WBEL-ID-RAPP-ANA<br>
 * Variable: WBEL-ID-RAPP-ANA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelIdRappAna extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelIdRappAna() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_ID_RAPP_ANA;
    }

    public void setWbelIdRappAna(int wbelIdRappAna) {
        writeIntAsPacked(Pos.WBEL_ID_RAPP_ANA, wbelIdRappAna, Len.Int.WBEL_ID_RAPP_ANA);
    }

    public void setWbelIdRappAnaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_ID_RAPP_ANA, Pos.WBEL_ID_RAPP_ANA);
    }

    /**Original name: WBEL-ID-RAPP-ANA<br>*/
    public int getWbelIdRappAna() {
        return readPackedAsInt(Pos.WBEL_ID_RAPP_ANA, Len.Int.WBEL_ID_RAPP_ANA);
    }

    public byte[] getWbelIdRappAnaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_ID_RAPP_ANA, Pos.WBEL_ID_RAPP_ANA);
        return buffer;
    }

    public void initWbelIdRappAnaSpaces() {
        fill(Pos.WBEL_ID_RAPP_ANA, Len.WBEL_ID_RAPP_ANA, Types.SPACE_CHAR);
    }

    public void setWbelIdRappAnaNull(String wbelIdRappAnaNull) {
        writeString(Pos.WBEL_ID_RAPP_ANA_NULL, wbelIdRappAnaNull, Len.WBEL_ID_RAPP_ANA_NULL);
    }

    /**Original name: WBEL-ID-RAPP-ANA-NULL<br>*/
    public String getWbelIdRappAnaNull() {
        return readString(Pos.WBEL_ID_RAPP_ANA_NULL, Len.WBEL_ID_RAPP_ANA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_ID_RAPP_ANA = 1;
        public static final int WBEL_ID_RAPP_ANA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_ID_RAPP_ANA = 5;
        public static final int WBEL_ID_RAPP_ANA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_ID_RAPP_ANA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
