package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-RIS-RIASTA-ECC<br>
 * Variable: W-B03-RIS-RIASTA-ECC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03RisRiastaEccLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03RisRiastaEccLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_RIS_RIASTA_ECC;
    }

    public void setwB03RisRiastaEcc(AfDecimal wB03RisRiastaEcc) {
        writeDecimalAsPacked(Pos.W_B03_RIS_RIASTA_ECC, wB03RisRiastaEcc.copy());
    }

    /**Original name: W-B03-RIS-RIASTA-ECC<br>*/
    public AfDecimal getwB03RisRiastaEcc() {
        return readPackedAsDecimal(Pos.W_B03_RIS_RIASTA_ECC, Len.Int.W_B03_RIS_RIASTA_ECC, Len.Fract.W_B03_RIS_RIASTA_ECC);
    }

    public byte[] getwB03RisRiastaEccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_RIS_RIASTA_ECC, Pos.W_B03_RIS_RIASTA_ECC);
        return buffer;
    }

    public void setwB03RisRiastaEccNull(String wB03RisRiastaEccNull) {
        writeString(Pos.W_B03_RIS_RIASTA_ECC_NULL, wB03RisRiastaEccNull, Len.W_B03_RIS_RIASTA_ECC_NULL);
    }

    /**Original name: W-B03-RIS-RIASTA-ECC-NULL<br>*/
    public String getwB03RisRiastaEccNull() {
        return readString(Pos.W_B03_RIS_RIASTA_ECC_NULL, Len.W_B03_RIS_RIASTA_ECC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_RIASTA_ECC = 1;
        public static final int W_B03_RIS_RIASTA_ECC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_RIASTA_ECC = 8;
        public static final int W_B03_RIS_RIASTA_ECC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_RIASTA_ECC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_RIASTA_ECC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
