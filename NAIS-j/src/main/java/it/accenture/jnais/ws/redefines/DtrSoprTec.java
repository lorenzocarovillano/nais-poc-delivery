package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-SOPR-TEC<br>
 * Variable: DTR-SOPR-TEC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_SOPR_TEC;
    }

    public void setDtrSoprTec(AfDecimal dtrSoprTec) {
        writeDecimalAsPacked(Pos.DTR_SOPR_TEC, dtrSoprTec.copy());
    }

    public void setDtrSoprTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_SOPR_TEC, Pos.DTR_SOPR_TEC);
    }

    /**Original name: DTR-SOPR-TEC<br>*/
    public AfDecimal getDtrSoprTec() {
        return readPackedAsDecimal(Pos.DTR_SOPR_TEC, Len.Int.DTR_SOPR_TEC, Len.Fract.DTR_SOPR_TEC);
    }

    public byte[] getDtrSoprTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_SOPR_TEC, Pos.DTR_SOPR_TEC);
        return buffer;
    }

    public void setDtrSoprTecNull(String dtrSoprTecNull) {
        writeString(Pos.DTR_SOPR_TEC_NULL, dtrSoprTecNull, Len.DTR_SOPR_TEC_NULL);
    }

    /**Original name: DTR-SOPR-TEC-NULL<br>*/
    public String getDtrSoprTecNull() {
        return readString(Pos.DTR_SOPR_TEC_NULL, Len.DTR_SOPR_TEC_NULL);
    }

    public String getDtrSoprTecNullFormatted() {
        return Functions.padBlanks(getDtrSoprTecNull(), Len.DTR_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_SOPR_TEC = 1;
        public static final int DTR_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_SOPR_TEC = 8;
        public static final int DTR_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
