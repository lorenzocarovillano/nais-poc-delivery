package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMP-INTR-RIT-PAG-L<br>
 * Variable: DFL-IMP-INTR-RIT-PAG-L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpIntrRitPagL extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpIntrRitPagL() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMP_INTR_RIT_PAG_L;
    }

    public void setDflImpIntrRitPagL(AfDecimal dflImpIntrRitPagL) {
        writeDecimalAsPacked(Pos.DFL_IMP_INTR_RIT_PAG_L, dflImpIntrRitPagL.copy());
    }

    public void setDflImpIntrRitPagLFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMP_INTR_RIT_PAG_L, Pos.DFL_IMP_INTR_RIT_PAG_L);
    }

    /**Original name: DFL-IMP-INTR-RIT-PAG-L<br>*/
    public AfDecimal getDflImpIntrRitPagL() {
        return readPackedAsDecimal(Pos.DFL_IMP_INTR_RIT_PAG_L, Len.Int.DFL_IMP_INTR_RIT_PAG_L, Len.Fract.DFL_IMP_INTR_RIT_PAG_L);
    }

    public byte[] getDflImpIntrRitPagLAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMP_INTR_RIT_PAG_L, Pos.DFL_IMP_INTR_RIT_PAG_L);
        return buffer;
    }

    public void setDflImpIntrRitPagLNull(String dflImpIntrRitPagLNull) {
        writeString(Pos.DFL_IMP_INTR_RIT_PAG_L_NULL, dflImpIntrRitPagLNull, Len.DFL_IMP_INTR_RIT_PAG_L_NULL);
    }

    /**Original name: DFL-IMP-INTR-RIT-PAG-L-NULL<br>*/
    public String getDflImpIntrRitPagLNull() {
        return readString(Pos.DFL_IMP_INTR_RIT_PAG_L_NULL, Len.DFL_IMP_INTR_RIT_PAG_L_NULL);
    }

    public String getDflImpIntrRitPagLNullFormatted() {
        return Functions.padBlanks(getDflImpIntrRitPagLNull(), Len.DFL_IMP_INTR_RIT_PAG_L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMP_INTR_RIT_PAG_L = 1;
        public static final int DFL_IMP_INTR_RIT_PAG_L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMP_INTR_RIT_PAG_L = 8;
        public static final int DFL_IMP_INTR_RIT_PAG_L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMP_INTR_RIT_PAG_L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMP_INTR_RIT_PAG_L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
