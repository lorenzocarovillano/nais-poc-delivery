package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-PC-SERV-VAL<br>
 * Variable: PMO-PC-SERV-VAL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoPcServVal extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoPcServVal() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_PC_SERV_VAL;
    }

    public void setPmoPcServVal(AfDecimal pmoPcServVal) {
        writeDecimalAsPacked(Pos.PMO_PC_SERV_VAL, pmoPcServVal.copy());
    }

    public void setPmoPcServValFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_PC_SERV_VAL, Pos.PMO_PC_SERV_VAL);
    }

    /**Original name: PMO-PC-SERV-VAL<br>*/
    public AfDecimal getPmoPcServVal() {
        return readPackedAsDecimal(Pos.PMO_PC_SERV_VAL, Len.Int.PMO_PC_SERV_VAL, Len.Fract.PMO_PC_SERV_VAL);
    }

    public byte[] getPmoPcServValAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_PC_SERV_VAL, Pos.PMO_PC_SERV_VAL);
        return buffer;
    }

    public void initPmoPcServValHighValues() {
        fill(Pos.PMO_PC_SERV_VAL, Len.PMO_PC_SERV_VAL, Types.HIGH_CHAR_VAL);
    }

    public void setPmoPcServValNull(String pmoPcServValNull) {
        writeString(Pos.PMO_PC_SERV_VAL_NULL, pmoPcServValNull, Len.PMO_PC_SERV_VAL_NULL);
    }

    /**Original name: PMO-PC-SERV-VAL-NULL<br>*/
    public String getPmoPcServValNull() {
        return readString(Pos.PMO_PC_SERV_VAL_NULL, Len.PMO_PC_SERV_VAL_NULL);
    }

    public String getPmoPcServValNullFormatted() {
        return Functions.padBlanks(getPmoPcServValNull(), Len.PMO_PC_SERV_VAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_PC_SERV_VAL = 1;
        public static final int PMO_PC_SERV_VAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_PC_SERV_VAL = 4;
        public static final int PMO_PC_SERV_VAL_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_PC_SERV_VAL = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_PC_SERV_VAL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
