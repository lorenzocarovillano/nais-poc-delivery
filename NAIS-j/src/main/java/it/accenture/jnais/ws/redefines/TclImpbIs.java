package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPB-IS<br>
 * Variable: TCL-IMPB-IS from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpbIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpbIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPB_IS;
    }

    public void setTclImpbIs(AfDecimal tclImpbIs) {
        writeDecimalAsPacked(Pos.TCL_IMPB_IS, tclImpbIs.copy());
    }

    public void setTclImpbIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPB_IS, Pos.TCL_IMPB_IS);
    }

    /**Original name: TCL-IMPB-IS<br>*/
    public AfDecimal getTclImpbIs() {
        return readPackedAsDecimal(Pos.TCL_IMPB_IS, Len.Int.TCL_IMPB_IS, Len.Fract.TCL_IMPB_IS);
    }

    public byte[] getTclImpbIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPB_IS, Pos.TCL_IMPB_IS);
        return buffer;
    }

    public void setTclImpbIsNull(String tclImpbIsNull) {
        writeString(Pos.TCL_IMPB_IS_NULL, tclImpbIsNull, Len.TCL_IMPB_IS_NULL);
    }

    /**Original name: TCL-IMPB-IS-NULL<br>*/
    public String getTclImpbIsNull() {
        return readString(Pos.TCL_IMPB_IS_NULL, Len.TCL_IMPB_IS_NULL);
    }

    public String getTclImpbIsNullFormatted() {
        return Functions.padBlanks(getTclImpbIsNull(), Len.TCL_IMPB_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_IS = 1;
        public static final int TCL_IMPB_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_IS = 8;
        public static final int TCL_IMPB_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
