package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WSPG-VAL-PC<br>
 * Variable: WSPG-VAL-PC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WspgValPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WspgValPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSPG_VAL_PC;
    }

    public void setWspgValPc(AfDecimal wspgValPc) {
        writeDecimalAsPacked(Pos.WSPG_VAL_PC, wspgValPc.copy());
    }

    public void setWspgValPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSPG_VAL_PC, Pos.WSPG_VAL_PC);
    }

    /**Original name: WSPG-VAL-PC<br>*/
    public AfDecimal getWspgValPc() {
        return readPackedAsDecimal(Pos.WSPG_VAL_PC, Len.Int.WSPG_VAL_PC, Len.Fract.WSPG_VAL_PC);
    }

    public byte[] getWspgValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSPG_VAL_PC, Pos.WSPG_VAL_PC);
        return buffer;
    }

    public void initWspgValPcSpaces() {
        fill(Pos.WSPG_VAL_PC, Len.WSPG_VAL_PC, Types.SPACE_CHAR);
    }

    public void setWspgValPcNull(String wspgValPcNull) {
        writeString(Pos.WSPG_VAL_PC_NULL, wspgValPcNull, Len.WSPG_VAL_PC_NULL);
    }

    /**Original name: WSPG-VAL-PC-NULL<br>*/
    public String getWspgValPcNull() {
        return readString(Pos.WSPG_VAL_PC_NULL, Len.WSPG_VAL_PC_NULL);
    }

    public String getWspgValPcNullFormatted() {
        return Functions.padBlanks(getWspgValPcNull(), Len.WSPG_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSPG_VAL_PC = 1;
        public static final int WSPG_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WSPG_VAL_PC = 8;
        public static final int WSPG_VAL_PC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WSPG_VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WSPG_VAL_PC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
