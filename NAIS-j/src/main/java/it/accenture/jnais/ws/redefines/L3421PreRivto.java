package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PRE-RIVTO<br>
 * Variable: L3421-PRE-RIVTO from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PreRivto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PreRivto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PRE_RIVTO;
    }

    public void setL3421PreRivtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PRE_RIVTO, Pos.L3421_PRE_RIVTO);
    }

    /**Original name: L3421-PRE-RIVTO<br>*/
    public AfDecimal getL3421PreRivto() {
        return readPackedAsDecimal(Pos.L3421_PRE_RIVTO, Len.Int.L3421_PRE_RIVTO, Len.Fract.L3421_PRE_RIVTO);
    }

    public byte[] getL3421PreRivtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PRE_RIVTO, Pos.L3421_PRE_RIVTO);
        return buffer;
    }

    /**Original name: L3421-PRE-RIVTO-NULL<br>*/
    public String getL3421PreRivtoNull() {
        return readString(Pos.L3421_PRE_RIVTO_NULL, Len.L3421_PRE_RIVTO_NULL);
    }

    public String getL3421PreRivtoNullFormatted() {
        return Functions.padBlanks(getL3421PreRivtoNull(), Len.L3421_PRE_RIVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PRE_RIVTO = 1;
        public static final int L3421_PRE_RIVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PRE_RIVTO = 8;
        public static final int L3421_PRE_RIVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PRE_RIVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PRE_RIVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
