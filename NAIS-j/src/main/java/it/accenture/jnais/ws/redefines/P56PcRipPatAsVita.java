package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-PC-RIP-PAT-AS-VITA<br>
 * Variable: P56-PC-RIP-PAT-AS-VITA from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56PcRipPatAsVita extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56PcRipPatAsVita() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_PC_RIP_PAT_AS_VITA;
    }

    public void setP56PcRipPatAsVita(AfDecimal p56PcRipPatAsVita) {
        writeDecimalAsPacked(Pos.P56_PC_RIP_PAT_AS_VITA, p56PcRipPatAsVita.copy());
    }

    public void setP56PcRipPatAsVitaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_PC_RIP_PAT_AS_VITA, Pos.P56_PC_RIP_PAT_AS_VITA);
    }

    /**Original name: P56-PC-RIP-PAT-AS-VITA<br>*/
    public AfDecimal getP56PcRipPatAsVita() {
        return readPackedAsDecimal(Pos.P56_PC_RIP_PAT_AS_VITA, Len.Int.P56_PC_RIP_PAT_AS_VITA, Len.Fract.P56_PC_RIP_PAT_AS_VITA);
    }

    public byte[] getP56PcRipPatAsVitaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_PC_RIP_PAT_AS_VITA, Pos.P56_PC_RIP_PAT_AS_VITA);
        return buffer;
    }

    public void setP56PcRipPatAsVitaNull(String p56PcRipPatAsVitaNull) {
        writeString(Pos.P56_PC_RIP_PAT_AS_VITA_NULL, p56PcRipPatAsVitaNull, Len.P56_PC_RIP_PAT_AS_VITA_NULL);
    }

    /**Original name: P56-PC-RIP-PAT-AS-VITA-NULL<br>*/
    public String getP56PcRipPatAsVitaNull() {
        return readString(Pos.P56_PC_RIP_PAT_AS_VITA_NULL, Len.P56_PC_RIP_PAT_AS_VITA_NULL);
    }

    public String getP56PcRipPatAsVitaNullFormatted() {
        return Functions.padBlanks(getP56PcRipPatAsVitaNull(), Len.P56_PC_RIP_PAT_AS_VITA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_PC_RIP_PAT_AS_VITA = 1;
        public static final int P56_PC_RIP_PAT_AS_VITA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_PC_RIP_PAT_AS_VITA = 4;
        public static final int P56_PC_RIP_PAT_AS_VITA_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_PC_RIP_PAT_AS_VITA = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_PC_RIP_PAT_AS_VITA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
