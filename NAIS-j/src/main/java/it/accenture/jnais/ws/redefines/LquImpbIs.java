package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-IS<br>
 * Variable: LQU-IMPB-IS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_IS;
    }

    public void setLquImpbIs(AfDecimal lquImpbIs) {
        writeDecimalAsPacked(Pos.LQU_IMPB_IS, lquImpbIs.copy());
    }

    public void setLquImpbIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_IS, Pos.LQU_IMPB_IS);
    }

    /**Original name: LQU-IMPB-IS<br>*/
    public AfDecimal getLquImpbIs() {
        return readPackedAsDecimal(Pos.LQU_IMPB_IS, Len.Int.LQU_IMPB_IS, Len.Fract.LQU_IMPB_IS);
    }

    public byte[] getLquImpbIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_IS, Pos.LQU_IMPB_IS);
        return buffer;
    }

    public void setLquImpbIsNull(String lquImpbIsNull) {
        writeString(Pos.LQU_IMPB_IS_NULL, lquImpbIsNull, Len.LQU_IMPB_IS_NULL);
    }

    /**Original name: LQU-IMPB-IS-NULL<br>*/
    public String getLquImpbIsNull() {
        return readString(Pos.LQU_IMPB_IS_NULL, Len.LQU_IMPB_IS_NULL);
    }

    public String getLquImpbIsNullFormatted() {
        return Functions.padBlanks(getLquImpbIsNull(), Len.LQU_IMPB_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_IS = 1;
        public static final int LQU_IMPB_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_IS = 8;
        public static final int LQU_IMPB_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
