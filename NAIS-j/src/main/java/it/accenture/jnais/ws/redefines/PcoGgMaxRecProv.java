package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-GG-MAX-REC-PROV<br>
 * Variable: PCO-GG-MAX-REC-PROV from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoGgMaxRecProv extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoGgMaxRecProv() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_GG_MAX_REC_PROV;
    }

    public void setPcoGgMaxRecProv(short pcoGgMaxRecProv) {
        writeShortAsPacked(Pos.PCO_GG_MAX_REC_PROV, pcoGgMaxRecProv, Len.Int.PCO_GG_MAX_REC_PROV);
    }

    public void setPcoGgMaxRecProvFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_GG_MAX_REC_PROV, Pos.PCO_GG_MAX_REC_PROV);
    }

    /**Original name: PCO-GG-MAX-REC-PROV<br>*/
    public short getPcoGgMaxRecProv() {
        return readPackedAsShort(Pos.PCO_GG_MAX_REC_PROV, Len.Int.PCO_GG_MAX_REC_PROV);
    }

    public byte[] getPcoGgMaxRecProvAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_GG_MAX_REC_PROV, Pos.PCO_GG_MAX_REC_PROV);
        return buffer;
    }

    public void initPcoGgMaxRecProvHighValues() {
        fill(Pos.PCO_GG_MAX_REC_PROV, Len.PCO_GG_MAX_REC_PROV, Types.HIGH_CHAR_VAL);
    }

    public void setPcoGgMaxRecProvNull(String pcoGgMaxRecProvNull) {
        writeString(Pos.PCO_GG_MAX_REC_PROV_NULL, pcoGgMaxRecProvNull, Len.PCO_GG_MAX_REC_PROV_NULL);
    }

    /**Original name: PCO-GG-MAX-REC-PROV-NULL<br>*/
    public String getPcoGgMaxRecProvNull() {
        return readString(Pos.PCO_GG_MAX_REC_PROV_NULL, Len.PCO_GG_MAX_REC_PROV_NULL);
    }

    public String getPcoGgMaxRecProvNullFormatted() {
        return Functions.padBlanks(getPcoGgMaxRecProvNull(), Len.PCO_GG_MAX_REC_PROV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_GG_MAX_REC_PROV = 1;
        public static final int PCO_GG_MAX_REC_PROV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_GG_MAX_REC_PROV = 3;
        public static final int PCO_GG_MAX_REC_PROV_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_GG_MAX_REC_PROV = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
