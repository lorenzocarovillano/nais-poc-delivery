package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRE-INVRIO-INI<br>
 * Variable: WPAG-PRE-INVRIO-INI from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPreInvrioIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPreInvrioIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRE_INVRIO_INI;
    }

    public void setWpagPreInvrioIni(AfDecimal wpagPreInvrioIni) {
        writeDecimalAsPacked(Pos.WPAG_PRE_INVRIO_INI, wpagPreInvrioIni.copy());
    }

    public void setWpagPreInvrioIniFormatted(String wpagPreInvrioIni) {
        setWpagPreInvrioIni(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRE_INVRIO_INI + Len.Fract.WPAG_PRE_INVRIO_INI, Len.Fract.WPAG_PRE_INVRIO_INI, wpagPreInvrioIni));
    }

    public void setWpagPreInvrioIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRE_INVRIO_INI, Pos.WPAG_PRE_INVRIO_INI);
    }

    /**Original name: WPAG-PRE-INVRIO-INI<br>*/
    public AfDecimal getWpagPreInvrioIni() {
        return readPackedAsDecimal(Pos.WPAG_PRE_INVRIO_INI, Len.Int.WPAG_PRE_INVRIO_INI, Len.Fract.WPAG_PRE_INVRIO_INI);
    }

    public byte[] getWpagPreInvrioIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRE_INVRIO_INI, Pos.WPAG_PRE_INVRIO_INI);
        return buffer;
    }

    public void initWpagPreInvrioIniSpaces() {
        fill(Pos.WPAG_PRE_INVRIO_INI, Len.WPAG_PRE_INVRIO_INI, Types.SPACE_CHAR);
    }

    public void setWpagPreInvrioIniNull(String wpagPreInvrioIniNull) {
        writeString(Pos.WPAG_PRE_INVRIO_INI_NULL, wpagPreInvrioIniNull, Len.WPAG_PRE_INVRIO_INI_NULL);
    }

    /**Original name: WPAG-PRE-INVRIO-INI-NULL<br>*/
    public String getWpagPreInvrioIniNull() {
        return readString(Pos.WPAG_PRE_INVRIO_INI_NULL, Len.WPAG_PRE_INVRIO_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_INVRIO_INI = 1;
        public static final int WPAG_PRE_INVRIO_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_INVRIO_INI = 8;
        public static final int WPAG_PRE_INVRIO_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_INVRIO_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_INVRIO_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
