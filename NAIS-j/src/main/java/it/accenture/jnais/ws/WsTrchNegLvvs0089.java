package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WS-TRCH-NEG<br>
 * Variable: WS-TRCH-NEG from program LVVS0089<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsTrchNegLvvs0089 {

    //==== PROPERTIES ====
    //Original name: WS-TP-TRCH-NEG-9
    private String neg9 = "9";
    //Original name: WS-TP-TRCH-NEG-10
    private String neg10 = "10";
    //Original name: WS-TP-TRCH-NEG-11
    private String neg11 = "11";
    //Original name: WS-TP-TRCH-NEG-13
    private String neg13 = "13";
    //Original name: WS-TP-TRCH-NEG-14
    private String neg14 = "14";
    //Original name: WS-TP-TRCH-NEG-15
    private String neg15 = "15";
    //Original name: FILLER-WS-TRCH-NEG
    private String flr1 = "";

    //==== METHODS ====
    public byte[] getWsTrchNegBytes() {
        byte[] buffer = new byte[Len.WS_TRCH_NEG];
        return getWsTrchNegBytes(buffer, 1);
    }

    public byte[] getWsTrchNegBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, neg9, Len.NEG9);
        position += Len.NEG9;
        MarshalByte.writeString(buffer, position, neg10, Len.NEG10);
        position += Len.NEG10;
        MarshalByte.writeString(buffer, position, neg11, Len.NEG11);
        position += Len.NEG11;
        MarshalByte.writeString(buffer, position, neg13, Len.NEG13);
        position += Len.NEG13;
        MarshalByte.writeString(buffer, position, neg14, Len.NEG14);
        position += Len.NEG14;
        MarshalByte.writeString(buffer, position, neg15, Len.NEG15);
        position += Len.NEG15;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        return buffer;
    }

    public String getNeg9() {
        return this.neg9;
    }

    public String getNeg10() {
        return this.neg10;
    }

    public String getNeg11() {
        return this.neg11;
    }

    public String getNeg13() {
        return this.neg13;
    }

    public String getNeg14() {
        return this.neg14;
    }

    public String getNeg15() {
        return this.neg15;
    }

    public String getFlr1() {
        return this.flr1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NEG9 = 2;
        public static final int NEG10 = 2;
        public static final int NEG11 = 2;
        public static final int NEG13 = 2;
        public static final int NEG14 = 2;
        public static final int NEG15 = 2;
        public static final int FLR1 = 8;
        public static final int WS_TRCH_NEG = NEG9 + NEG10 + NEG11 + NEG13 + NEG14 + NEG15 + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
