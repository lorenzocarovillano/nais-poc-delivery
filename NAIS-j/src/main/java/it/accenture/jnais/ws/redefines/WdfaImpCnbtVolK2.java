package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMP-CNBT-VOL-K2<br>
 * Variable: WDFA-IMP-CNBT-VOL-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpCnbtVolK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpCnbtVolK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMP_CNBT_VOL_K2;
    }

    public void setWdfaImpCnbtVolK2(AfDecimal wdfaImpCnbtVolK2) {
        writeDecimalAsPacked(Pos.WDFA_IMP_CNBT_VOL_K2, wdfaImpCnbtVolK2.copy());
    }

    public void setWdfaImpCnbtVolK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMP_CNBT_VOL_K2, Pos.WDFA_IMP_CNBT_VOL_K2);
    }

    /**Original name: WDFA-IMP-CNBT-VOL-K2<br>*/
    public AfDecimal getWdfaImpCnbtVolK2() {
        return readPackedAsDecimal(Pos.WDFA_IMP_CNBT_VOL_K2, Len.Int.WDFA_IMP_CNBT_VOL_K2, Len.Fract.WDFA_IMP_CNBT_VOL_K2);
    }

    public byte[] getWdfaImpCnbtVolK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMP_CNBT_VOL_K2, Pos.WDFA_IMP_CNBT_VOL_K2);
        return buffer;
    }

    public void setWdfaImpCnbtVolK2Null(String wdfaImpCnbtVolK2Null) {
        writeString(Pos.WDFA_IMP_CNBT_VOL_K2_NULL, wdfaImpCnbtVolK2Null, Len.WDFA_IMP_CNBT_VOL_K2_NULL);
    }

    /**Original name: WDFA-IMP-CNBT-VOL-K2-NULL<br>*/
    public String getWdfaImpCnbtVolK2Null() {
        return readString(Pos.WDFA_IMP_CNBT_VOL_K2_NULL, Len.WDFA_IMP_CNBT_VOL_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_VOL_K2 = 1;
        public static final int WDFA_IMP_CNBT_VOL_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_VOL_K2 = 8;
        public static final int WDFA_IMP_CNBT_VOL_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_VOL_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_VOL_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
