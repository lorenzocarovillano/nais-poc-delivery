package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-BNS<br>
 * Variable: TGA-IMP-BNS from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpBns extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpBns() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_BNS;
    }

    public void setTgaImpBns(AfDecimal tgaImpBns) {
        writeDecimalAsPacked(Pos.TGA_IMP_BNS, tgaImpBns.copy());
    }

    public void setTgaImpBnsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_BNS, Pos.TGA_IMP_BNS);
    }

    /**Original name: TGA-IMP-BNS<br>*/
    public AfDecimal getTgaImpBns() {
        return readPackedAsDecimal(Pos.TGA_IMP_BNS, Len.Int.TGA_IMP_BNS, Len.Fract.TGA_IMP_BNS);
    }

    public byte[] getTgaImpBnsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_BNS, Pos.TGA_IMP_BNS);
        return buffer;
    }

    public void setTgaImpBnsNull(String tgaImpBnsNull) {
        writeString(Pos.TGA_IMP_BNS_NULL, tgaImpBnsNull, Len.TGA_IMP_BNS_NULL);
    }

    /**Original name: TGA-IMP-BNS-NULL<br>*/
    public String getTgaImpBnsNull() {
        return readString(Pos.TGA_IMP_BNS_NULL, Len.TGA_IMP_BNS_NULL);
    }

    public String getTgaImpBnsNullFormatted() {
        return Functions.padBlanks(getTgaImpBnsNull(), Len.TGA_IMP_BNS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_BNS = 1;
        public static final int TGA_IMP_BNS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_BNS = 8;
        public static final int TGA_IMP_BNS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_BNS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_BNS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
