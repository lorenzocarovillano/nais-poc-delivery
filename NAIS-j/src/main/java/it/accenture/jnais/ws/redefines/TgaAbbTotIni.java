package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ABB-TOT-INI<br>
 * Variable: TGA-ABB-TOT-INI from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaAbbTotIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaAbbTotIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ABB_TOT_INI;
    }

    public void setTgaAbbTotIni(AfDecimal tgaAbbTotIni) {
        writeDecimalAsPacked(Pos.TGA_ABB_TOT_INI, tgaAbbTotIni.copy());
    }

    public void setTgaAbbTotIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ABB_TOT_INI, Pos.TGA_ABB_TOT_INI);
    }

    /**Original name: TGA-ABB-TOT-INI<br>*/
    public AfDecimal getTgaAbbTotIni() {
        return readPackedAsDecimal(Pos.TGA_ABB_TOT_INI, Len.Int.TGA_ABB_TOT_INI, Len.Fract.TGA_ABB_TOT_INI);
    }

    public byte[] getTgaAbbTotIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ABB_TOT_INI, Pos.TGA_ABB_TOT_INI);
        return buffer;
    }

    public void setTgaAbbTotIniNull(String tgaAbbTotIniNull) {
        writeString(Pos.TGA_ABB_TOT_INI_NULL, tgaAbbTotIniNull, Len.TGA_ABB_TOT_INI_NULL);
    }

    /**Original name: TGA-ABB-TOT-INI-NULL<br>*/
    public String getTgaAbbTotIniNull() {
        return readString(Pos.TGA_ABB_TOT_INI_NULL, Len.TGA_ABB_TOT_INI_NULL);
    }

    public String getTgaAbbTotIniNullFormatted() {
        return Functions.padBlanks(getTgaAbbTotIniNull(), Len.TGA_ABB_TOT_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ABB_TOT_INI = 1;
        public static final int TGA_ABB_TOT_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ABB_TOT_INI = 8;
        public static final int TGA_ABB_TOT_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ABB_TOT_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_ABB_TOT_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
