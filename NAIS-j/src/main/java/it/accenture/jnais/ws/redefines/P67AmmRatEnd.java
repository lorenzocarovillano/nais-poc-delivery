package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-AMM-RAT-END<br>
 * Variable: P67-AMM-RAT-END from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67AmmRatEnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67AmmRatEnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_AMM_RAT_END;
    }

    public void setP67AmmRatEnd(AfDecimal p67AmmRatEnd) {
        writeDecimalAsPacked(Pos.P67_AMM_RAT_END, p67AmmRatEnd.copy());
    }

    public void setP67AmmRatEndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_AMM_RAT_END, Pos.P67_AMM_RAT_END);
    }

    /**Original name: P67-AMM-RAT-END<br>*/
    public AfDecimal getP67AmmRatEnd() {
        return readPackedAsDecimal(Pos.P67_AMM_RAT_END, Len.Int.P67_AMM_RAT_END, Len.Fract.P67_AMM_RAT_END);
    }

    public byte[] getP67AmmRatEndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_AMM_RAT_END, Pos.P67_AMM_RAT_END);
        return buffer;
    }

    public void setP67AmmRatEndNull(String p67AmmRatEndNull) {
        writeString(Pos.P67_AMM_RAT_END_NULL, p67AmmRatEndNull, Len.P67_AMM_RAT_END_NULL);
    }

    /**Original name: P67-AMM-RAT-END-NULL<br>*/
    public String getP67AmmRatEndNull() {
        return readString(Pos.P67_AMM_RAT_END_NULL, Len.P67_AMM_RAT_END_NULL);
    }

    public String getP67AmmRatEndNullFormatted() {
        return Functions.padBlanks(getP67AmmRatEndNull(), Len.P67_AMM_RAT_END_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_AMM_RAT_END = 1;
        public static final int P67_AMM_RAT_END_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_AMM_RAT_END = 8;
        public static final int P67_AMM_RAT_END_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_AMM_RAT_END = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_AMM_RAT_END = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
