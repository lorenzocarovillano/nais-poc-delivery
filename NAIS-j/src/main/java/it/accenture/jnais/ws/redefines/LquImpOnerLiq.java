package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-ONER-LIQ<br>
 * Variable: LQU-IMP-ONER-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpOnerLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpOnerLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_ONER_LIQ;
    }

    public void setLquImpOnerLiq(AfDecimal lquImpOnerLiq) {
        writeDecimalAsPacked(Pos.LQU_IMP_ONER_LIQ, lquImpOnerLiq.copy());
    }

    public void setLquImpOnerLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_ONER_LIQ, Pos.LQU_IMP_ONER_LIQ);
    }

    /**Original name: LQU-IMP-ONER-LIQ<br>*/
    public AfDecimal getLquImpOnerLiq() {
        return readPackedAsDecimal(Pos.LQU_IMP_ONER_LIQ, Len.Int.LQU_IMP_ONER_LIQ, Len.Fract.LQU_IMP_ONER_LIQ);
    }

    public byte[] getLquImpOnerLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_ONER_LIQ, Pos.LQU_IMP_ONER_LIQ);
        return buffer;
    }

    public void setLquImpOnerLiqNull(String lquImpOnerLiqNull) {
        writeString(Pos.LQU_IMP_ONER_LIQ_NULL, lquImpOnerLiqNull, Len.LQU_IMP_ONER_LIQ_NULL);
    }

    /**Original name: LQU-IMP-ONER-LIQ-NULL<br>*/
    public String getLquImpOnerLiqNull() {
        return readString(Pos.LQU_IMP_ONER_LIQ_NULL, Len.LQU_IMP_ONER_LIQ_NULL);
    }

    public String getLquImpOnerLiqNullFormatted() {
        return Functions.padBlanks(getLquImpOnerLiqNull(), Len.LQU_IMP_ONER_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_ONER_LIQ = 1;
        public static final int LQU_IMP_ONER_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_ONER_LIQ = 8;
        public static final int LQU_IMP_ONER_LIQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_ONER_LIQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_ONER_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
