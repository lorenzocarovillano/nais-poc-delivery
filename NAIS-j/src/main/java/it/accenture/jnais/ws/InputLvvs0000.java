package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Lvvc0000DatiInput2;
import it.accenture.jnais.ws.enums.Lvvc0000FormatDate;

/**Original name: INPUT-LVVS0000<br>
 * Variable: INPUT-LVVS0000 from program LVVS0000<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class InputLvvs0000 extends SerializableParameter {

    //==== PROPERTIES ====
    /**Original name: LVVC0000-FORMAT-DATE<br>
	 * <pre>----------------------------------------------------------------*
	 *  AREA CHIAMATA MODULI CALCOLO DATA
	 * ----------------------------------------------------------------*</pre>*/
    private Lvvc0000FormatDate formatDate = new Lvvc0000FormatDate();
    //Original name: LVVC0000-DATA-INPUT
    private String dataInput = DefaultValues.stringVal(Len.DATA_INPUT);
    //Original name: LVVC0000-DATA-INPUT-1
    private String dataInput1 = DefaultValues.stringVal(Len.DATA_INPUT1);
    //Original name: LVVC0000-DATI-INPUT-2
    private Lvvc0000DatiInput2 datiInput2 = new Lvvc0000DatiInput2();
    //Original name: LVVC0000-ANNI-INPUT-3
    private String anniInput3 = DefaultValues.stringVal(Len.ANNI_INPUT3);
    //Original name: LVVC0000-MESI-INPUT-3
    private String mesiInput3 = DefaultValues.stringVal(Len.MESI_INPUT3);
    //Original name: LVVC0000-DATA-OUTPUT
    private AfDecimal dataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.INPUT_LVVS0000;
    }

    @Override
    public void deserialize(byte[] buf) {
        setInputLvvs0000Bytes(buf);
    }

    public String getInputLvvs0000Formatted() {
        return MarshalByteExt.bufferToStr(getInputLvvs0000Bytes());
    }

    public void setInputLvvs0000Bytes(byte[] buffer) {
        setInputLvvs0000Bytes(buffer, 1);
    }

    public byte[] getInputLvvs0000Bytes() {
        byte[] buffer = new byte[Len.INPUT_LVVS0000];
        return getInputLvvs0000Bytes(buffer, 1);
    }

    public void setInputLvvs0000Bytes(byte[] buffer, int offset) {
        int position = offset;
        formatDate.setFormatDate(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        setDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        setDatiInput1Bytes(buffer, position);
        position += Len.DATI_INPUT1;
        datiInput2.setDatiInput2Bytes(buffer, position);
        position += Lvvc0000DatiInput2.Len.DATI_INPUT2;
        setDatiInput3Bytes(buffer, position);
        position += Len.DATI_INPUT3;
        setDatiOutputBytes(buffer, position);
    }

    public byte[] getInputLvvs0000Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, formatDate.getFormatDate());
        position += Types.CHAR_SIZE;
        getDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        getDatiInput1Bytes(buffer, position);
        position += Len.DATI_INPUT1;
        datiInput2.getDatiInput2Bytes(buffer, position);
        position += Lvvc0000DatiInput2.Len.DATI_INPUT2;
        getDatiInput3Bytes(buffer, position);
        position += Len.DATI_INPUT3;
        getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        dataInput = MarshalByte.readFixedString(buffer, position, Len.DATA_INPUT);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, dataInput, Len.DATA_INPUT);
        return buffer;
    }

    public void setDataInputFormatted(String dataInput) {
        this.dataInput = Trunc.toUnsignedNumeric(dataInput, Len.DATA_INPUT);
    }

    public int getDataInput() {
        return NumericDisplay.asInt(this.dataInput);
    }

    public void setDatiInput1Bytes(byte[] buffer, int offset) {
        int position = offset;
        dataInput1 = MarshalByte.readFixedString(buffer, position, Len.DATA_INPUT1);
    }

    public byte[] getDatiInput1Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, dataInput1, Len.DATA_INPUT1);
        return buffer;
    }

    public void setDataInput1(int dataInput1) {
        this.dataInput1 = NumericDisplay.asString(dataInput1, Len.DATA_INPUT1);
    }

    public void setDataInput1Formatted(String dataInput1) {
        this.dataInput1 = Trunc.toUnsignedNumeric(dataInput1, Len.DATA_INPUT1);
    }

    public void setDataInput1FromBuffer(byte[] buffer) {
        dataInput1 = MarshalByte.readFixedString(buffer, 1, Len.DATA_INPUT1);
    }

    public int getDataInput1() {
        return NumericDisplay.asInt(this.dataInput1);
    }

    public String getDataInput1Formatted() {
        return this.dataInput1;
    }

    public void setDatiInput3Bytes(byte[] buffer, int offset) {
        int position = offset;
        anniInput3 = MarshalByte.readFixedString(buffer, position, Len.ANNI_INPUT3);
        position += Len.ANNI_INPUT3;
        mesiInput3 = MarshalByte.readFixedString(buffer, position, Len.MESI_INPUT3);
    }

    public byte[] getDatiInput3Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, anniInput3, Len.ANNI_INPUT3);
        position += Len.ANNI_INPUT3;
        MarshalByte.writeString(buffer, position, mesiInput3, Len.MESI_INPUT3);
        return buffer;
    }

    public void setAnniInput3Formatted(String anniInput3) {
        this.anniInput3 = Trunc.toUnsignedNumeric(anniInput3, Len.ANNI_INPUT3);
    }

    public int getAnniInput3() {
        return NumericDisplay.asInt(this.anniInput3);
    }

    public void setMesiInput3Formatted(String mesiInput3) {
        this.mesiInput3 = Trunc.toUnsignedNumeric(mesiInput3, Len.MESI_INPUT3);
    }

    public int getMesiInput3() {
        return NumericDisplay.asInt(this.mesiInput3);
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        dataOutput.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DATA_OUTPUT, Len.Fract.DATA_OUTPUT, SignType.NO_SIGN));
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimal(buffer, position, dataOutput.copy(), SignType.NO_SIGN);
        return buffer;
    }

    public void setDataOutput(AfDecimal dataOutput) {
        this.dataOutput.assign(dataOutput);
    }

    public AfDecimal getDataOutput() {
        return this.dataOutput.copy();
    }

    public Lvvc0000DatiInput2 getDatiInput2() {
        return datiInput2;
    }

    public Lvvc0000FormatDate getFormatDate() {
        return formatDate;
    }

    @Override
    public byte[] serialize() {
        return getInputLvvs0000Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DATA_INPUT = 8;
        public static final int DATI_INPUT = DATA_INPUT;
        public static final int DATA_INPUT1 = 8;
        public static final int DATI_INPUT1 = DATA_INPUT1;
        public static final int ANNI_INPUT3 = 5;
        public static final int MESI_INPUT3 = 5;
        public static final int DATI_INPUT3 = ANNI_INPUT3 + MESI_INPUT3;
        public static final int DATA_OUTPUT = 11;
        public static final int DATI_OUTPUT = DATA_OUTPUT;
        public static final int INPUT_LVVS0000 = Lvvc0000FormatDate.Len.FORMAT_DATE + DATI_INPUT + DATI_INPUT1 + Lvvc0000DatiInput2.Len.DATI_INPUT2 + DATI_INPUT3 + DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DATA_OUTPUT = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DATA_OUTPUT = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
