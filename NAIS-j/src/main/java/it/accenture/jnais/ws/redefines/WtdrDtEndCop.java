package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-DT-END-COP<br>
 * Variable: WTDR-DT-END-COP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrDtEndCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrDtEndCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_DT_END_COP;
    }

    public void setWtdrDtEndCop(int wtdrDtEndCop) {
        writeIntAsPacked(Pos.WTDR_DT_END_COP, wtdrDtEndCop, Len.Int.WTDR_DT_END_COP);
    }

    /**Original name: WTDR-DT-END-COP<br>*/
    public int getWtdrDtEndCop() {
        return readPackedAsInt(Pos.WTDR_DT_END_COP, Len.Int.WTDR_DT_END_COP);
    }

    public void setWtdrDtEndCopNull(String wtdrDtEndCopNull) {
        writeString(Pos.WTDR_DT_END_COP_NULL, wtdrDtEndCopNull, Len.WTDR_DT_END_COP_NULL);
    }

    /**Original name: WTDR-DT-END-COP-NULL<br>*/
    public String getWtdrDtEndCopNull() {
        return readString(Pos.WTDR_DT_END_COP_NULL, Len.WTDR_DT_END_COP_NULL);
    }

    public String getWtdrDtEndCopNullFormatted() {
        return Functions.padBlanks(getWtdrDtEndCopNull(), Len.WTDR_DT_END_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_DT_END_COP = 1;
        public static final int WTDR_DT_END_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_DT_END_COP = 5;
        public static final int WTDR_DT_END_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_DT_END_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
