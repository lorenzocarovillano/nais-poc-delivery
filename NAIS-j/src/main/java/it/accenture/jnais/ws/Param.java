package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: PARAM<br>
 * Variable: PARAM from program LCCS0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Param extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: PARAM
    private String param = "";

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PARAM;
    }

    @Override
    public void deserialize(byte[] buf) {
        setParamFromBuffer(buf);
    }

    public void setParam(short param) {
        this.param = NumericDisplay.asString(param, Len.PARAM);
    }

    public void setParamFormatted(String param) {
        this.param = Trunc.toUnsignedNumeric(param, Len.PARAM);
    }

    public void setParamFromBuffer(byte[] buffer, int offset) {
        setParamFormatted(MarshalByte.readString(buffer, offset, Len.PARAM));
    }

    public void setParamFromBuffer(byte[] buffer) {
        setParamFromBuffer(buffer, 1);
    }

    public short getParam() {
        return NumericDisplay.asShort(this.param);
    }

    @Override
    public byte[] serialize() {
        return MarshalByteExt.shortToBuffer(getParam(), Len.Int.PARAM, SignType.NO_SIGN);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PARAM = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PARAM = 1;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PARAM = 0;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
