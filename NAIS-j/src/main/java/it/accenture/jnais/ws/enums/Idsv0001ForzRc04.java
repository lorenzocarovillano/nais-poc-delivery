package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0001-FORZ-RC-04<br>
 * Variable: IDSV0001-FORZ-RC-04 from copybook IDSV0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0001ForzRc04 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char YES = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setForzRc04(char forzRc04) {
        this.value = forzRc04;
    }

    public char getForzRc04() {
        return this.value;
    }

    public boolean isIdsv0001ForzRc04Yes() {
        return value == YES;
    }

    public void setIdsv0001ForzRc04Yes() {
        value = YES;
    }

    public void setIdsv0001ForzRc04No() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FORZ_RC04 = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
