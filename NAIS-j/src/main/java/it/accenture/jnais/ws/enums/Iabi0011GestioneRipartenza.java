package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABI0011-GESTIONE-RIPARTENZA<br>
 * Variable: IABI0011-GESTIONE-RIPARTENZA from copybook IABI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabi0011GestioneRipartenza {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.GESTIONE_RIPARTENZA);
    public static final String SENZA_RIPARTENZA = "SR";
    public static final String RIP_CON_MONITORING = "CM";
    public static final String RIP_SENZA_MONITORING = "SM";
    public static final String RIP_CON_VERSIONAMENTO = "CV";

    //==== METHODS ====
    public void setGestioneRipartenza(String gestioneRipartenza) {
        this.value = Functions.subString(gestioneRipartenza, Len.GESTIONE_RIPARTENZA);
    }

    public String getGestioneRipartenza() {
        return this.value;
    }

    public String getGestioneRipartenzaFormatted() {
        return Functions.padBlanks(getGestioneRipartenza(), Len.GESTIONE_RIPARTENZA);
    }

    public boolean isSenzaRipartenza() {
        return value.equals(SENZA_RIPARTENZA);
    }

    public boolean isRipConMonitoring() {
        return value.equals(RIP_CON_MONITORING);
    }

    public boolean isRipSenzaMonitoring() {
        return value.equals(RIP_SENZA_MONITORING);
    }

    public boolean isRipConVersionamento() {
        return value.equals(RIP_CON_VERSIONAMENTO);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GESTIONE_RIPARTENZA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
