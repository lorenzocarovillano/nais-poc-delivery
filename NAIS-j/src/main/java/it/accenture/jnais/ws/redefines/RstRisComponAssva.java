package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-COMPON-ASSVA<br>
 * Variable: RST-RIS-COMPON-ASSVA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisComponAssva extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisComponAssva() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_COMPON_ASSVA;
    }

    public void setRstRisComponAssva(AfDecimal rstRisComponAssva) {
        writeDecimalAsPacked(Pos.RST_RIS_COMPON_ASSVA, rstRisComponAssva.copy());
    }

    public void setRstRisComponAssvaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_COMPON_ASSVA, Pos.RST_RIS_COMPON_ASSVA);
    }

    /**Original name: RST-RIS-COMPON-ASSVA<br>*/
    public AfDecimal getRstRisComponAssva() {
        return readPackedAsDecimal(Pos.RST_RIS_COMPON_ASSVA, Len.Int.RST_RIS_COMPON_ASSVA, Len.Fract.RST_RIS_COMPON_ASSVA);
    }

    public byte[] getRstRisComponAssvaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_COMPON_ASSVA, Pos.RST_RIS_COMPON_ASSVA);
        return buffer;
    }

    public void setRstRisComponAssvaNull(String rstRisComponAssvaNull) {
        writeString(Pos.RST_RIS_COMPON_ASSVA_NULL, rstRisComponAssvaNull, Len.RST_RIS_COMPON_ASSVA_NULL);
    }

    /**Original name: RST-RIS-COMPON-ASSVA-NULL<br>*/
    public String getRstRisComponAssvaNull() {
        return readString(Pos.RST_RIS_COMPON_ASSVA_NULL, Len.RST_RIS_COMPON_ASSVA_NULL);
    }

    public String getRstRisComponAssvaNullFormatted() {
        return Functions.padBlanks(getRstRisComponAssvaNull(), Len.RST_RIS_COMPON_ASSVA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_COMPON_ASSVA = 1;
        public static final int RST_RIS_COMPON_ASSVA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_COMPON_ASSVA = 8;
        public static final int RST_RIS_COMPON_ASSVA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_COMPON_ASSVA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_COMPON_ASSVA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
