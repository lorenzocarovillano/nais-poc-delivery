package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-RAT-LRD<br>
 * Variable: L3421-RAT-LRD from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421RatLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421RatLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_RAT_LRD;
    }

    public void setL3421RatLrdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_RAT_LRD, Pos.L3421_RAT_LRD);
    }

    /**Original name: L3421-RAT-LRD<br>*/
    public AfDecimal getL3421RatLrd() {
        return readPackedAsDecimal(Pos.L3421_RAT_LRD, Len.Int.L3421_RAT_LRD, Len.Fract.L3421_RAT_LRD);
    }

    public byte[] getL3421RatLrdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_RAT_LRD, Pos.L3421_RAT_LRD);
        return buffer;
    }

    /**Original name: L3421-RAT-LRD-NULL<br>*/
    public String getL3421RatLrdNull() {
        return readString(Pos.L3421_RAT_LRD_NULL, Len.L3421_RAT_LRD_NULL);
    }

    public String getL3421RatLrdNullFormatted() {
        return Functions.padBlanks(getL3421RatLrdNull(), Len.L3421_RAT_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_RAT_LRD = 1;
        public static final int L3421_RAT_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_RAT_LRD = 8;
        public static final int L3421_RAT_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_RAT_LRD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_RAT_LRD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
