package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.Ivvv0212CtrlAutOper;

/**Original name: AREA-IO-IVVS0212<br>
 * Variable: AREA-IO-IVVS0212 from program IVVS0212<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoIvvs0212 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int CTRL_AUT_OPER_MAXOCCURS = 20;
    /**Original name: IVVV0212-KEY-AUT-OPER1<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA INPUT VARIABILI
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA
	 *    LUNG.
	 * ----------------------------------------------------------------*
	 *  03 AREA-INPUT.</pre>*/
    private String keyAutOper1 = DefaultValues.stringVal(Len.KEY_AUT_OPER1);
    //Original name: IVVV0212-KEY-AUT-OPER2
    private String keyAutOper2 = DefaultValues.stringVal(Len.KEY_AUT_OPER2);
    //Original name: IVVV0212-KEY-AUT-OPER3
    private String keyAutOper3 = DefaultValues.stringVal(Len.KEY_AUT_OPER3);
    //Original name: IVVV0212-KEY-AUT-OPER4
    private String keyAutOper4 = DefaultValues.stringVal(Len.KEY_AUT_OPER4);
    //Original name: IVVV0212-KEY-AUT-OPER5
    private String keyAutOper5 = DefaultValues.stringVal(Len.KEY_AUT_OPER5);
    /**Original name: IVVV0212-ELE-CTRL-AUT-OPER-MAX<br>
	 * <pre>    AREA VARIABILI AUTONOMIA OPERATIVA
	 *  03 AREA-OUTPUT.</pre>*/
    private short eleCtrlAutOperMax = DefaultValues.SHORT_VAL;
    //Original name: IVVV0212-CTRL-AUT-OPER
    private Ivvv0212CtrlAutOper[] ctrlAutOper = new Ivvv0212CtrlAutOper[CTRL_AUT_OPER_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public AreaIoIvvs0212() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IO_IVVS0212;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIoIvvs0212Bytes(buf);
    }

    public void init() {
        for (int ctrlAutOperIdx = 1; ctrlAutOperIdx <= CTRL_AUT_OPER_MAXOCCURS; ctrlAutOperIdx++) {
            ctrlAutOper[ctrlAutOperIdx - 1] = new Ivvv0212CtrlAutOper();
        }
    }

    public String getAreaIvvv0212Formatted() {
        return MarshalByteExt.bufferToStr(getAreaIoIvvs0212Bytes());
    }

    public void setAreaIoIvvs0212Bytes(byte[] buffer) {
        setAreaIoIvvs0212Bytes(buffer, 1);
    }

    public byte[] getAreaIoIvvs0212Bytes() {
        byte[] buffer = new byte[Len.AREA_IO_IVVS0212];
        return getAreaIoIvvs0212Bytes(buffer, 1);
    }

    public void setAreaIoIvvs0212Bytes(byte[] buffer, int offset) {
        int position = offset;
        keyAutOper1 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER1);
        position += Len.KEY_AUT_OPER1;
        keyAutOper2 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER2);
        position += Len.KEY_AUT_OPER2;
        keyAutOper3 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER3);
        position += Len.KEY_AUT_OPER3;
        keyAutOper4 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER4);
        position += Len.KEY_AUT_OPER4;
        keyAutOper5 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER5);
        position += Len.KEY_AUT_OPER5;
        eleCtrlAutOperMax = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_CTRL_AUT_OPER_MAX, 0);
        position += Len.ELE_CTRL_AUT_OPER_MAX;
        for (int idx = 1; idx <= CTRL_AUT_OPER_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                ctrlAutOper[idx - 1].setCtrlAutOperBytes(buffer, position);
                position += Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
            }
            else {
                ctrlAutOper[idx - 1].initCtrlAutOperSpaces();
                position += Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
            }
        }
    }

    public byte[] getAreaIoIvvs0212Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, keyAutOper1, Len.KEY_AUT_OPER1);
        position += Len.KEY_AUT_OPER1;
        MarshalByte.writeString(buffer, position, keyAutOper2, Len.KEY_AUT_OPER2);
        position += Len.KEY_AUT_OPER2;
        MarshalByte.writeString(buffer, position, keyAutOper3, Len.KEY_AUT_OPER3);
        position += Len.KEY_AUT_OPER3;
        MarshalByte.writeString(buffer, position, keyAutOper4, Len.KEY_AUT_OPER4);
        position += Len.KEY_AUT_OPER4;
        MarshalByte.writeString(buffer, position, keyAutOper5, Len.KEY_AUT_OPER5);
        position += Len.KEY_AUT_OPER5;
        MarshalByte.writeShortAsPacked(buffer, position, eleCtrlAutOperMax, Len.Int.ELE_CTRL_AUT_OPER_MAX, 0);
        position += Len.ELE_CTRL_AUT_OPER_MAX;
        for (int idx = 1; idx <= CTRL_AUT_OPER_MAXOCCURS; idx++) {
            ctrlAutOper[idx - 1].getCtrlAutOperBytes(buffer, position);
            position += Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
        }
        return buffer;
    }

    public void setKeyAutOper1(String keyAutOper1) {
        this.keyAutOper1 = Functions.subString(keyAutOper1, Len.KEY_AUT_OPER1);
    }

    public String getKeyAutOper1() {
        return this.keyAutOper1;
    }

    public void setKeyAutOper2(String keyAutOper2) {
        this.keyAutOper2 = Functions.subString(keyAutOper2, Len.KEY_AUT_OPER2);
    }

    public String getKeyAutOper2() {
        return this.keyAutOper2;
    }

    public void setKeyAutOper3(String keyAutOper3) {
        this.keyAutOper3 = Functions.subString(keyAutOper3, Len.KEY_AUT_OPER3);
    }

    public String getKeyAutOper3() {
        return this.keyAutOper3;
    }

    public void setKeyAutOper4(String keyAutOper4) {
        this.keyAutOper4 = Functions.subString(keyAutOper4, Len.KEY_AUT_OPER4);
    }

    public String getKeyAutOper4() {
        return this.keyAutOper4;
    }

    public void setKeyAutOper5(String keyAutOper5) {
        this.keyAutOper5 = Functions.subString(keyAutOper5, Len.KEY_AUT_OPER5);
    }

    public String getKeyAutOper5() {
        return this.keyAutOper5;
    }

    public void setEleCtrlAutOperMax(short eleCtrlAutOperMax) {
        this.eleCtrlAutOperMax = eleCtrlAutOperMax;
    }

    public short getEleCtrlAutOperMax() {
        return this.eleCtrlAutOperMax;
    }

    public Ivvv0212CtrlAutOper getCtrlAutOper(int idx) {
        return ctrlAutOper[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getAreaIoIvvs0212Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int KEY_AUT_OPER1 = 20;
        public static final int KEY_AUT_OPER2 = 20;
        public static final int KEY_AUT_OPER3 = 20;
        public static final int KEY_AUT_OPER4 = 20;
        public static final int KEY_AUT_OPER5 = 20;
        public static final int ELE_CTRL_AUT_OPER_MAX = 3;
        public static final int AREA_IO_IVVS0212 = KEY_AUT_OPER1 + KEY_AUT_OPER2 + KEY_AUT_OPER3 + KEY_AUT_OPER4 + KEY_AUT_OPER5 + ELE_CTRL_AUT_OPER_MAX + AreaIoIvvs0212.CTRL_AUT_OPER_MAXOCCURS * Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ELE_CTRL_AUT_OPER_MAX = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
