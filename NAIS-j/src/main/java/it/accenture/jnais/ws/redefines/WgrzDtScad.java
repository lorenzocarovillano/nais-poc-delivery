package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-DT-SCAD<br>
 * Variable: WGRZ-DT-SCAD from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzDtScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzDtScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_DT_SCAD;
    }

    public void setWgrzDtScad(int wgrzDtScad) {
        writeIntAsPacked(Pos.WGRZ_DT_SCAD, wgrzDtScad, Len.Int.WGRZ_DT_SCAD);
    }

    public void setWgrzDtScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_DT_SCAD, Pos.WGRZ_DT_SCAD);
    }

    /**Original name: WGRZ-DT-SCAD<br>*/
    public int getWgrzDtScad() {
        return readPackedAsInt(Pos.WGRZ_DT_SCAD, Len.Int.WGRZ_DT_SCAD);
    }

    public byte[] getWgrzDtScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_DT_SCAD, Pos.WGRZ_DT_SCAD);
        return buffer;
    }

    public void initWgrzDtScadSpaces() {
        fill(Pos.WGRZ_DT_SCAD, Len.WGRZ_DT_SCAD, Types.SPACE_CHAR);
    }

    public void setWgrzDtScadNull(String wgrzDtScadNull) {
        writeString(Pos.WGRZ_DT_SCAD_NULL, wgrzDtScadNull, Len.WGRZ_DT_SCAD_NULL);
    }

    /**Original name: WGRZ-DT-SCAD-NULL<br>*/
    public String getWgrzDtScadNull() {
        return readString(Pos.WGRZ_DT_SCAD_NULL, Len.WGRZ_DT_SCAD_NULL);
    }

    public String getWgrzDtScadNullFormatted() {
        return Functions.padBlanks(getWgrzDtScadNull(), Len.WGRZ_DT_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_SCAD = 1;
        public static final int WGRZ_DT_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_SCAD = 5;
        public static final int WGRZ_DT_SCAD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_DT_SCAD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
