package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-QTZO-CL<br>
 * Variable: PCO-DT-ULT-QTZO-CL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltQtzoCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltQtzoCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_QTZO_CL;
    }

    public void setPcoDtUltQtzoCl(int pcoDtUltQtzoCl) {
        writeIntAsPacked(Pos.PCO_DT_ULT_QTZO_CL, pcoDtUltQtzoCl, Len.Int.PCO_DT_ULT_QTZO_CL);
    }

    public void setPcoDtUltQtzoClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_QTZO_CL, Pos.PCO_DT_ULT_QTZO_CL);
    }

    /**Original name: PCO-DT-ULT-QTZO-CL<br>*/
    public int getPcoDtUltQtzoCl() {
        return readPackedAsInt(Pos.PCO_DT_ULT_QTZO_CL, Len.Int.PCO_DT_ULT_QTZO_CL);
    }

    public byte[] getPcoDtUltQtzoClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_QTZO_CL, Pos.PCO_DT_ULT_QTZO_CL);
        return buffer;
    }

    public void initPcoDtUltQtzoClHighValues() {
        fill(Pos.PCO_DT_ULT_QTZO_CL, Len.PCO_DT_ULT_QTZO_CL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltQtzoClNull(String pcoDtUltQtzoClNull) {
        writeString(Pos.PCO_DT_ULT_QTZO_CL_NULL, pcoDtUltQtzoClNull, Len.PCO_DT_ULT_QTZO_CL_NULL);
    }

    /**Original name: PCO-DT-ULT-QTZO-CL-NULL<br>*/
    public String getPcoDtUltQtzoClNull() {
        return readString(Pos.PCO_DT_ULT_QTZO_CL_NULL, Len.PCO_DT_ULT_QTZO_CL_NULL);
    }

    public String getPcoDtUltQtzoClNullFormatted() {
        return Functions.padBlanks(getPcoDtUltQtzoClNull(), Len.PCO_DT_ULT_QTZO_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_QTZO_CL = 1;
        public static final int PCO_DT_ULT_QTZO_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_QTZO_CL = 5;
        public static final int PCO_DT_ULT_QTZO_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_QTZO_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
