package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-CUR-MOV<br>
 * Variable: FLAG-CUR-MOV from program LVVS0002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCurMov {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char INIT_CUR_MOV = 'S';
    public static final char FINE_CUR_MOV = 'N';

    //==== METHODS ====
    public void setFlagCurMov(char flagCurMov) {
        this.value = flagCurMov;
    }

    public char getFlagCurMov() {
        return this.value;
    }

    public void setInitCurMov() {
        value = INIT_CUR_MOV;
    }

    public boolean isFineCurMov() {
        return value == FINE_CUR_MOV;
    }

    public void setFineCurMov() {
        value = FINE_CUR_MOV;
    }
}
