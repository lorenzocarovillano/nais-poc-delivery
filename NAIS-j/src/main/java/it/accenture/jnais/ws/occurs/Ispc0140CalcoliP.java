package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ISPC0140-CALCOLI-P<br>
 * Variables: ISPC0140-CALCOLI-P from copybook ISPC0140<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0140CalcoliP {

    //==== PROPERTIES ====
    public static final int AREA_TP_PREMIO_P_MAXOCCURS = 3;
    //Original name: ISPC0140-O-TIPO-LIVELLO-P
    private char oTipoLivelloP = DefaultValues.CHAR_VAL;
    //Original name: ISPC0140-O-CODICE-LIVELLO-P
    private String oCodiceLivelloP = DefaultValues.stringVal(Len.O_CODICE_LIVELLO_P);
    //Original name: ISPC0140-PRE-NUM-MAX-ELE-P
    private String preNumMaxEleP = DefaultValues.stringVal(Len.PRE_NUM_MAX_ELE_P);
    //Original name: ISPC0140-AREA-TP-PREMIO-P
    private Ispc0140AreaTpPremioP[] areaTpPremioP = new Ispc0140AreaTpPremioP[AREA_TP_PREMIO_P_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ispc0140CalcoliP() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int areaTpPremioPIdx = 1; areaTpPremioPIdx <= AREA_TP_PREMIO_P_MAXOCCURS; areaTpPremioPIdx++) {
            areaTpPremioP[areaTpPremioPIdx - 1] = new Ispc0140AreaTpPremioP();
        }
    }

    public void setCalcoliPBytes(byte[] buffer, int offset) {
        int position = offset;
        oTipoLivelloP = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        oCodiceLivelloP = MarshalByte.readString(buffer, position, Len.O_CODICE_LIVELLO_P);
        position += Len.O_CODICE_LIVELLO_P;
        preNumMaxEleP = MarshalByte.readFixedString(buffer, position, Len.PRE_NUM_MAX_ELE_P);
        position += Len.PRE_NUM_MAX_ELE_P;
        for (int idx = 1; idx <= AREA_TP_PREMIO_P_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                areaTpPremioP[idx - 1].setAreaTpPremioPBytes(buffer, position);
                position += Ispc0140AreaTpPremioP.Len.AREA_TP_PREMIO_P;
            }
            else {
                areaTpPremioP[idx - 1].initAreaTpPremioPSpaces();
                position += Ispc0140AreaTpPremioP.Len.AREA_TP_PREMIO_P;
            }
        }
    }

    public byte[] getCalcoliPBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, oTipoLivelloP);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, oCodiceLivelloP, Len.O_CODICE_LIVELLO_P);
        position += Len.O_CODICE_LIVELLO_P;
        MarshalByte.writeString(buffer, position, preNumMaxEleP, Len.PRE_NUM_MAX_ELE_P);
        position += Len.PRE_NUM_MAX_ELE_P;
        for (int idx = 1; idx <= AREA_TP_PREMIO_P_MAXOCCURS; idx++) {
            areaTpPremioP[idx - 1].getAreaTpPremioPBytes(buffer, position);
            position += Ispc0140AreaTpPremioP.Len.AREA_TP_PREMIO_P;
        }
        return buffer;
    }

    public void initCalcoliPSpaces() {
        oTipoLivelloP = Types.SPACE_CHAR;
        oCodiceLivelloP = "";
        preNumMaxEleP = "";
        for (int idx = 1; idx <= AREA_TP_PREMIO_P_MAXOCCURS; idx++) {
            areaTpPremioP[idx - 1].initAreaTpPremioPSpaces();
        }
    }

    public void setoTipoLivelloP(char oTipoLivelloP) {
        this.oTipoLivelloP = oTipoLivelloP;
    }

    public char getoTipoLivelloP() {
        return this.oTipoLivelloP;
    }

    public void setoCodiceLivelloP(String oCodiceLivelloP) {
        this.oCodiceLivelloP = Functions.subString(oCodiceLivelloP, Len.O_CODICE_LIVELLO_P);
    }

    public String getoCodiceLivelloP() {
        return this.oCodiceLivelloP;
    }

    public short getIspc0140PreNumMaxEleP() {
        return NumericDisplay.asShort(this.preNumMaxEleP);
    }

    public Ispc0140AreaTpPremioP getAreaTpPremioP(int idx) {
        return areaTpPremioP[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int O_TIPO_LIVELLO_P = 1;
        public static final int O_CODICE_LIVELLO_P = 12;
        public static final int PRE_NUM_MAX_ELE_P = 3;
        public static final int CALCOLI_P = O_TIPO_LIVELLO_P + O_CODICE_LIVELLO_P + PRE_NUM_MAX_ELE_P + Ispc0140CalcoliP.AREA_TP_PREMIO_P_MAXOCCURS * Ispc0140AreaTpPremioP.Len.AREA_TP_PREMIO_P;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
