package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-TAX-SEP<br>
 * Variable: LQU-IMPB-TAX-SEP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbTaxSep extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbTaxSep() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_TAX_SEP;
    }

    public void setLquImpbTaxSep(AfDecimal lquImpbTaxSep) {
        writeDecimalAsPacked(Pos.LQU_IMPB_TAX_SEP, lquImpbTaxSep.copy());
    }

    public void setLquImpbTaxSepFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_TAX_SEP, Pos.LQU_IMPB_TAX_SEP);
    }

    /**Original name: LQU-IMPB-TAX-SEP<br>*/
    public AfDecimal getLquImpbTaxSep() {
        return readPackedAsDecimal(Pos.LQU_IMPB_TAX_SEP, Len.Int.LQU_IMPB_TAX_SEP, Len.Fract.LQU_IMPB_TAX_SEP);
    }

    public byte[] getLquImpbTaxSepAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_TAX_SEP, Pos.LQU_IMPB_TAX_SEP);
        return buffer;
    }

    public void setLquImpbTaxSepNull(String lquImpbTaxSepNull) {
        writeString(Pos.LQU_IMPB_TAX_SEP_NULL, lquImpbTaxSepNull, Len.LQU_IMPB_TAX_SEP_NULL);
    }

    /**Original name: LQU-IMPB-TAX-SEP-NULL<br>*/
    public String getLquImpbTaxSepNull() {
        return readString(Pos.LQU_IMPB_TAX_SEP_NULL, Len.LQU_IMPB_TAX_SEP_NULL);
    }

    public String getLquImpbTaxSepNullFormatted() {
        return Functions.padBlanks(getLquImpbTaxSepNull(), Len.LQU_IMPB_TAX_SEP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_TAX_SEP = 1;
        public static final int LQU_IMPB_TAX_SEP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_TAX_SEP = 8;
        public static final int LQU_IMPB_TAX_SEP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_TAX_SEP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_TAX_SEP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
