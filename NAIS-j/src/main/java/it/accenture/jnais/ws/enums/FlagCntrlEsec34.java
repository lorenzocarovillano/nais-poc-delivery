package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-CNTRL-ESEC-34<br>
 * Variable: FLAG-CNTRL-ESEC-34 from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCntrlEsec34 {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCntrlEsec34(char flagCntrlEsec34) {
        this.value = flagCntrlEsec34;
    }

    public char getFlagCntrlEsec34() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
