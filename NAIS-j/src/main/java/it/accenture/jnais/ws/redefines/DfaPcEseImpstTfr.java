package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-PC-ESE-IMPST-TFR<br>
 * Variable: DFA-PC-ESE-IMPST-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaPcEseImpstTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaPcEseImpstTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_PC_ESE_IMPST_TFR;
    }

    public void setDfaPcEseImpstTfr(AfDecimal dfaPcEseImpstTfr) {
        writeDecimalAsPacked(Pos.DFA_PC_ESE_IMPST_TFR, dfaPcEseImpstTfr.copy());
    }

    public void setDfaPcEseImpstTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_PC_ESE_IMPST_TFR, Pos.DFA_PC_ESE_IMPST_TFR);
    }

    /**Original name: DFA-PC-ESE-IMPST-TFR<br>*/
    public AfDecimal getDfaPcEseImpstTfr() {
        return readPackedAsDecimal(Pos.DFA_PC_ESE_IMPST_TFR, Len.Int.DFA_PC_ESE_IMPST_TFR, Len.Fract.DFA_PC_ESE_IMPST_TFR);
    }

    public byte[] getDfaPcEseImpstTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_PC_ESE_IMPST_TFR, Pos.DFA_PC_ESE_IMPST_TFR);
        return buffer;
    }

    public void setDfaPcEseImpstTfrNull(String dfaPcEseImpstTfrNull) {
        writeString(Pos.DFA_PC_ESE_IMPST_TFR_NULL, dfaPcEseImpstTfrNull, Len.DFA_PC_ESE_IMPST_TFR_NULL);
    }

    /**Original name: DFA-PC-ESE-IMPST-TFR-NULL<br>*/
    public String getDfaPcEseImpstTfrNull() {
        return readString(Pos.DFA_PC_ESE_IMPST_TFR_NULL, Len.DFA_PC_ESE_IMPST_TFR_NULL);
    }

    public String getDfaPcEseImpstTfrNullFormatted() {
        return Functions.padBlanks(getDfaPcEseImpstTfrNull(), Len.DFA_PC_ESE_IMPST_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_PC_ESE_IMPST_TFR = 1;
        public static final int DFA_PC_ESE_IMPST_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_PC_ESE_IMPST_TFR = 4;
        public static final int DFA_PC_ESE_IMPST_TFR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_PC_ESE_IMPST_TFR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_PC_ESE_IMPST_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
