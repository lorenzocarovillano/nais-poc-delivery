package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-INT-ERR<br>
 * Variable: FLAG-INT-ERR from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagIntErr {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagIntErr(char flagIntErr) {
        this.value = flagIntErr;
    }

    public char getFlagIntErr() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
