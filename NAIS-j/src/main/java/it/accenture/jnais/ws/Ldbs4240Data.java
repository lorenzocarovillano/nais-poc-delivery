package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idbvrre3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndRappRete;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS4240<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs4240Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-RAPP-RETE
    private IndRappRete indRappRete = new IndRappRete();
    //Original name: IDBVRRE3
    private Idbvrre3 idbvrre3 = new Idbvrre3();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idbvrre3 getIdbvrre3() {
        return idbvrre3;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndRappRete getIndRappRete() {
        return indRappRete;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
