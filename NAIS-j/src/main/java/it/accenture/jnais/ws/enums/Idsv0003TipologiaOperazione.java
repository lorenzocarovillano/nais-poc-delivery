package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0003-TIPOLOGIA-OPERAZIONE<br>
 * Variable: IDSV0003-TIPOLOGIA-OPERAZIONE from copybook IDSV0003<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0003TipologiaOperazione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TIPOLOGIA_OPERAZIONE);
    public static final String CONDITION01 = "WC1";
    public static final String CONDITION02 = "WC2";
    public static final String CONDITION03 = "WC3";
    public static final String CONDITION04 = "WC4";
    public static final String CONDITION05 = "WC5";
    public static final String CONDITION06 = "WC6";
    public static final String CONDITION07 = "WC7";
    public static final String CONDITION08 = "WC8";
    public static final String CONDITION09 = "WC9";
    public static final String CONDITION10 = "WC0";

    //==== METHODS ====
    public void setTipologiaOperazione(String tipologiaOperazione) {
        this.value = Functions.subString(tipologiaOperazione, Len.TIPOLOGIA_OPERAZIONE);
    }

    public String getTipologiaOperazione() {
        return this.value;
    }

    public boolean isIdsv0003WhereCondition01() {
        return value.equals(CONDITION01);
    }

    public void setIdsv0003WhereCondition01() {
        value = CONDITION01;
    }

    public boolean isIdsv0003WhereCondition02() {
        return value.equals(CONDITION02);
    }

    public void setIdsv0003WhereCondition02() {
        value = CONDITION02;
    }

    public boolean isIdsv0003WhereCondition03() {
        return value.equals(CONDITION03);
    }

    public void setIdsv0003WhereCondition03() {
        value = CONDITION03;
    }

    public boolean isIdsv0003WhereCondition04() {
        return value.equals(CONDITION04);
    }

    public void setIdsv0003WhereCondition04() {
        value = CONDITION04;
    }

    public boolean isIdsv0003WhereCondition05() {
        return value.equals(CONDITION05);
    }

    public void setIdsv0003WhereCondition05() {
        value = CONDITION05;
    }

    public boolean isIdsv0003WhereCondition06() {
        return value.equals(CONDITION06);
    }

    public void setIdsv0003WhereCondition06() {
        value = CONDITION06;
    }

    public boolean isIdsv0003WhereCondition07() {
        return value.equals(CONDITION07);
    }

    public void setIdsv0003WhereCondition07() {
        value = CONDITION07;
    }

    public boolean isIdsv0003WhereCondition08() {
        return value.equals(CONDITION08);
    }

    public void setIdsv0003WhereCondition09() {
        value = CONDITION09;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPOLOGIA_OPERAZIONE = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
