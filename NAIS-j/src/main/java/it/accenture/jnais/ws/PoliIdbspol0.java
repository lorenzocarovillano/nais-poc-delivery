package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.PolAaDiffProrDflt;
import it.accenture.jnais.ws.redefines.PolDir1oVers;
import it.accenture.jnais.ws.redefines.PolDirEmis;
import it.accenture.jnais.ws.redefines.PolDirQuiet;
import it.accenture.jnais.ws.redefines.PolDirVersAgg;
import it.accenture.jnais.ws.redefines.PolDtApplzConv;
import it.accenture.jnais.ws.redefines.PolDtIniVldtConv;
import it.accenture.jnais.ws.redefines.PolDtPresc;
import it.accenture.jnais.ws.redefines.PolDtProp;
import it.accenture.jnais.ws.redefines.PolDtScad;
import it.accenture.jnais.ws.redefines.PolDurAa;
import it.accenture.jnais.ws.redefines.PolDurGg;
import it.accenture.jnais.ws.redefines.PolDurMm;
import it.accenture.jnais.ws.redefines.PolIdAccComm;
import it.accenture.jnais.ws.redefines.PolIdMoviChiu;
import it.accenture.jnais.ws.redefines.PolSpeMed;

/**Original name: POLI<br>
 * Variable: POLI from copybook IDBVPOL1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class PoliIdbspol0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: POL-ID-POLI
    private int polIdPoli = DefaultValues.INT_VAL;
    //Original name: POL-ID-MOVI-CRZ
    private int polIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: POL-ID-MOVI-CHIU
    private PolIdMoviChiu polIdMoviChiu = new PolIdMoviChiu();
    //Original name: POL-IB-OGG
    private String polIbOgg = DefaultValues.stringVal(Len.POL_IB_OGG);
    //Original name: POL-IB-PROP
    private String polIbProp = DefaultValues.stringVal(Len.POL_IB_PROP);
    //Original name: POL-DT-PROP
    private PolDtProp polDtProp = new PolDtProp();
    //Original name: POL-DT-INI-EFF
    private int polDtIniEff = DefaultValues.INT_VAL;
    //Original name: POL-DT-END-EFF
    private int polDtEndEff = DefaultValues.INT_VAL;
    //Original name: POL-COD-COMP-ANIA
    private int polCodCompAnia = DefaultValues.INT_VAL;
    //Original name: POL-DT-DECOR
    private int polDtDecor = DefaultValues.INT_VAL;
    //Original name: POL-DT-EMIS
    private int polDtEmis = DefaultValues.INT_VAL;
    //Original name: POL-TP-POLI
    private String polTpPoli = DefaultValues.stringVal(Len.POL_TP_POLI);
    //Original name: POL-DUR-AA
    private PolDurAa polDurAa = new PolDurAa();
    //Original name: POL-DUR-MM
    private PolDurMm polDurMm = new PolDurMm();
    //Original name: POL-DT-SCAD
    private PolDtScad polDtScad = new PolDtScad();
    //Original name: POL-COD-PROD
    private String polCodProd = DefaultValues.stringVal(Len.POL_COD_PROD);
    //Original name: POL-DT-INI-VLDT-PROD
    private int polDtIniVldtProd = DefaultValues.INT_VAL;
    //Original name: POL-COD-CONV
    private String polCodConv = DefaultValues.stringVal(Len.POL_COD_CONV);
    //Original name: POL-COD-RAMO
    private String polCodRamo = DefaultValues.stringVal(Len.POL_COD_RAMO);
    //Original name: POL-DT-INI-VLDT-CONV
    private PolDtIniVldtConv polDtIniVldtConv = new PolDtIniVldtConv();
    //Original name: POL-DT-APPLZ-CONV
    private PolDtApplzConv polDtApplzConv = new PolDtApplzConv();
    //Original name: POL-TP-FRM-ASSVA
    private String polTpFrmAssva = DefaultValues.stringVal(Len.POL_TP_FRM_ASSVA);
    //Original name: POL-TP-RGM-FISC
    private String polTpRgmFisc = DefaultValues.stringVal(Len.POL_TP_RGM_FISC);
    //Original name: POL-FL-ESTAS
    private char polFlEstas = DefaultValues.CHAR_VAL;
    //Original name: POL-FL-RSH-COMUN
    private char polFlRshComun = DefaultValues.CHAR_VAL;
    //Original name: POL-FL-RSH-COMUN-COND
    private char polFlRshComunCond = DefaultValues.CHAR_VAL;
    //Original name: POL-TP-LIV-GENZ-TIT
    private String polTpLivGenzTit = DefaultValues.stringVal(Len.POL_TP_LIV_GENZ_TIT);
    //Original name: POL-FL-COP-FINANZ
    private char polFlCopFinanz = DefaultValues.CHAR_VAL;
    //Original name: POL-TP-APPLZ-DIR
    private String polTpApplzDir = DefaultValues.stringVal(Len.POL_TP_APPLZ_DIR);
    //Original name: POL-SPE-MED
    private PolSpeMed polSpeMed = new PolSpeMed();
    //Original name: POL-DIR-EMIS
    private PolDirEmis polDirEmis = new PolDirEmis();
    //Original name: POL-DIR-1O-VERS
    private PolDir1oVers polDir1oVers = new PolDir1oVers();
    //Original name: POL-DIR-VERS-AGG
    private PolDirVersAgg polDirVersAgg = new PolDirVersAgg();
    //Original name: POL-COD-DVS
    private String polCodDvs = DefaultValues.stringVal(Len.POL_COD_DVS);
    //Original name: POL-FL-FNT-AZ
    private char polFlFntAz = DefaultValues.CHAR_VAL;
    //Original name: POL-FL-FNT-ADER
    private char polFlFntAder = DefaultValues.CHAR_VAL;
    //Original name: POL-FL-FNT-TFR
    private char polFlFntTfr = DefaultValues.CHAR_VAL;
    //Original name: POL-FL-FNT-VOLO
    private char polFlFntVolo = DefaultValues.CHAR_VAL;
    //Original name: POL-TP-OPZ-A-SCAD
    private String polTpOpzAScad = DefaultValues.stringVal(Len.POL_TP_OPZ_A_SCAD);
    //Original name: POL-AA-DIFF-PROR-DFLT
    private PolAaDiffProrDflt polAaDiffProrDflt = new PolAaDiffProrDflt();
    //Original name: POL-FL-VER-PROD
    private String polFlVerProd = DefaultValues.stringVal(Len.POL_FL_VER_PROD);
    //Original name: POL-DUR-GG
    private PolDurGg polDurGg = new PolDurGg();
    //Original name: POL-DIR-QUIET
    private PolDirQuiet polDirQuiet = new PolDirQuiet();
    //Original name: POL-TP-PTF-ESTNO
    private String polTpPtfEstno = DefaultValues.stringVal(Len.POL_TP_PTF_ESTNO);
    //Original name: POL-FL-CUM-PRE-CNTR
    private char polFlCumPreCntr = DefaultValues.CHAR_VAL;
    //Original name: POL-FL-AMMB-MOVI
    private char polFlAmmbMovi = DefaultValues.CHAR_VAL;
    //Original name: POL-CONV-GECO
    private String polConvGeco = DefaultValues.stringVal(Len.POL_CONV_GECO);
    //Original name: POL-DS-RIGA
    private long polDsRiga = DefaultValues.LONG_VAL;
    //Original name: POL-DS-OPER-SQL
    private char polDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: POL-DS-VER
    private int polDsVer = DefaultValues.INT_VAL;
    //Original name: POL-DS-TS-INI-CPTZ
    private long polDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: POL-DS-TS-END-CPTZ
    private long polDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: POL-DS-UTENTE
    private String polDsUtente = DefaultValues.stringVal(Len.POL_DS_UTENTE);
    //Original name: POL-DS-STATO-ELAB
    private char polDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: POL-FL-SCUDO-FISC
    private char polFlScudoFisc = DefaultValues.CHAR_VAL;
    //Original name: POL-FL-TRASFE
    private char polFlTrasfe = DefaultValues.CHAR_VAL;
    //Original name: POL-FL-TFR-STRC
    private char polFlTfrStrc = DefaultValues.CHAR_VAL;
    //Original name: POL-DT-PRESC
    private PolDtPresc polDtPresc = new PolDtPresc();
    //Original name: POL-COD-CONV-AGG
    private String polCodConvAgg = DefaultValues.stringVal(Len.POL_COD_CONV_AGG);
    //Original name: POL-SUBCAT-PROD
    private String polSubcatProd = DefaultValues.stringVal(Len.POL_SUBCAT_PROD);
    //Original name: POL-FL-QUEST-ADEGZ-ASS
    private char polFlQuestAdegzAss = DefaultValues.CHAR_VAL;
    //Original name: POL-COD-TPA
    private String polCodTpa = DefaultValues.stringVal(Len.POL_COD_TPA);
    //Original name: POL-ID-ACC-COMM
    private PolIdAccComm polIdAccComm = new PolIdAccComm();
    //Original name: POL-FL-POLI-CPI-PR
    private char polFlPoliCpiPr = DefaultValues.CHAR_VAL;
    //Original name: POL-FL-POLI-BUNDLING
    private char polFlPoliBundling = DefaultValues.CHAR_VAL;
    //Original name: POL-IND-POLI-PRIN-COLL
    private char polIndPoliPrinColl = DefaultValues.CHAR_VAL;
    //Original name: POL-FL-VND-BUNDLE
    private char polFlVndBundle = DefaultValues.CHAR_VAL;
    //Original name: POL-IB-BS
    private String polIbBs = DefaultValues.stringVal(Len.POL_IB_BS);
    //Original name: POL-FL-POLI-IFP
    private char polFlPoliIfp = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POLI;
    }

    @Override
    public void deserialize(byte[] buf) {
        setPoliBytes(buf);
    }

    public void setPoliFormatted(String data) {
        byte[] buffer = new byte[Len.POLI];
        MarshalByte.writeString(buffer, 1, data, Len.POLI);
        setPoliBytes(buffer, 1);
    }

    public String getPoliFormatted() {
        return MarshalByteExt.bufferToStr(getPoliBytes());
    }

    public void setPoliBytes(byte[] buffer) {
        setPoliBytes(buffer, 1);
    }

    public byte[] getPoliBytes() {
        byte[] buffer = new byte[Len.POLI];
        return getPoliBytes(buffer, 1);
    }

    public void setPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        polIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_ID_POLI, 0);
        position += Len.POL_ID_POLI;
        polIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_ID_MOVI_CRZ, 0);
        position += Len.POL_ID_MOVI_CRZ;
        polIdMoviChiu.setPolIdMoviChiuFromBuffer(buffer, position);
        position += PolIdMoviChiu.Len.POL_ID_MOVI_CHIU;
        polIbOgg = MarshalByte.readString(buffer, position, Len.POL_IB_OGG);
        position += Len.POL_IB_OGG;
        polIbProp = MarshalByte.readString(buffer, position, Len.POL_IB_PROP);
        position += Len.POL_IB_PROP;
        polDtProp.setPolDtPropFromBuffer(buffer, position);
        position += PolDtProp.Len.POL_DT_PROP;
        polDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_INI_EFF, 0);
        position += Len.POL_DT_INI_EFF;
        polDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_END_EFF, 0);
        position += Len.POL_DT_END_EFF;
        polCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_COD_COMP_ANIA, 0);
        position += Len.POL_COD_COMP_ANIA;
        polDtDecor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_DECOR, 0);
        position += Len.POL_DT_DECOR;
        polDtEmis = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_EMIS, 0);
        position += Len.POL_DT_EMIS;
        polTpPoli = MarshalByte.readString(buffer, position, Len.POL_TP_POLI);
        position += Len.POL_TP_POLI;
        polDurAa.setPolDurAaFromBuffer(buffer, position);
        position += PolDurAa.Len.POL_DUR_AA;
        polDurMm.setPolDurMmFromBuffer(buffer, position);
        position += PolDurMm.Len.POL_DUR_MM;
        polDtScad.setPolDtScadFromBuffer(buffer, position);
        position += PolDtScad.Len.POL_DT_SCAD;
        polCodProd = MarshalByte.readString(buffer, position, Len.POL_COD_PROD);
        position += Len.POL_COD_PROD;
        polDtIniVldtProd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_INI_VLDT_PROD, 0);
        position += Len.POL_DT_INI_VLDT_PROD;
        polCodConv = MarshalByte.readString(buffer, position, Len.POL_COD_CONV);
        position += Len.POL_COD_CONV;
        polCodRamo = MarshalByte.readString(buffer, position, Len.POL_COD_RAMO);
        position += Len.POL_COD_RAMO;
        polDtIniVldtConv.setPolDtIniVldtConvFromBuffer(buffer, position);
        position += PolDtIniVldtConv.Len.POL_DT_INI_VLDT_CONV;
        polDtApplzConv.setPolDtApplzConvFromBuffer(buffer, position);
        position += PolDtApplzConv.Len.POL_DT_APPLZ_CONV;
        polTpFrmAssva = MarshalByte.readString(buffer, position, Len.POL_TP_FRM_ASSVA);
        position += Len.POL_TP_FRM_ASSVA;
        polTpRgmFisc = MarshalByte.readString(buffer, position, Len.POL_TP_RGM_FISC);
        position += Len.POL_TP_RGM_FISC;
        polFlEstas = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlRshComun = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlRshComunCond = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polTpLivGenzTit = MarshalByte.readString(buffer, position, Len.POL_TP_LIV_GENZ_TIT);
        position += Len.POL_TP_LIV_GENZ_TIT;
        polFlCopFinanz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polTpApplzDir = MarshalByte.readString(buffer, position, Len.POL_TP_APPLZ_DIR);
        position += Len.POL_TP_APPLZ_DIR;
        polSpeMed.setPolSpeMedFromBuffer(buffer, position);
        position += PolSpeMed.Len.POL_SPE_MED;
        polDirEmis.setPolDirEmisFromBuffer(buffer, position);
        position += PolDirEmis.Len.POL_DIR_EMIS;
        polDir1oVers.setPolDir1oVersFromBuffer(buffer, position);
        position += PolDir1oVers.Len.POL_DIR1O_VERS;
        polDirVersAgg.setPolDirVersAggFromBuffer(buffer, position);
        position += PolDirVersAgg.Len.POL_DIR_VERS_AGG;
        polCodDvs = MarshalByte.readString(buffer, position, Len.POL_COD_DVS);
        position += Len.POL_COD_DVS;
        polFlFntAz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlFntAder = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlFntTfr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlFntVolo = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polTpOpzAScad = MarshalByte.readString(buffer, position, Len.POL_TP_OPZ_A_SCAD);
        position += Len.POL_TP_OPZ_A_SCAD;
        polAaDiffProrDflt.setPolAaDiffProrDfltFromBuffer(buffer, position);
        position += PolAaDiffProrDflt.Len.POL_AA_DIFF_PROR_DFLT;
        polFlVerProd = MarshalByte.readString(buffer, position, Len.POL_FL_VER_PROD);
        position += Len.POL_FL_VER_PROD;
        polDurGg.setPolDurGgFromBuffer(buffer, position);
        position += PolDurGg.Len.POL_DUR_GG;
        polDirQuiet.setPolDirQuietFromBuffer(buffer, position);
        position += PolDirQuiet.Len.POL_DIR_QUIET;
        polTpPtfEstno = MarshalByte.readString(buffer, position, Len.POL_TP_PTF_ESTNO);
        position += Len.POL_TP_PTF_ESTNO;
        polFlCumPreCntr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlAmmbMovi = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polConvGeco = MarshalByte.readString(buffer, position, Len.POL_CONV_GECO);
        position += Len.POL_CONV_GECO;
        polDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.POL_DS_RIGA, 0);
        position += Len.POL_DS_RIGA;
        polDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DS_VER, 0);
        position += Len.POL_DS_VER;
        polDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.POL_DS_TS_INI_CPTZ, 0);
        position += Len.POL_DS_TS_INI_CPTZ;
        polDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.POL_DS_TS_END_CPTZ, 0);
        position += Len.POL_DS_TS_END_CPTZ;
        polDsUtente = MarshalByte.readString(buffer, position, Len.POL_DS_UTENTE);
        position += Len.POL_DS_UTENTE;
        polDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlScudoFisc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlTrasfe = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlTfrStrc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polDtPresc.setPolDtPrescFromBuffer(buffer, position);
        position += PolDtPresc.Len.POL_DT_PRESC;
        polCodConvAgg = MarshalByte.readString(buffer, position, Len.POL_COD_CONV_AGG);
        position += Len.POL_COD_CONV_AGG;
        polSubcatProd = MarshalByte.readString(buffer, position, Len.POL_SUBCAT_PROD);
        position += Len.POL_SUBCAT_PROD;
        polFlQuestAdegzAss = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polCodTpa = MarshalByte.readString(buffer, position, Len.POL_COD_TPA);
        position += Len.POL_COD_TPA;
        polIdAccComm.setPolIdAccCommFromBuffer(buffer, position);
        position += PolIdAccComm.Len.POL_ID_ACC_COMM;
        polFlPoliCpiPr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlPoliBundling = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polIndPoliPrinColl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlVndBundle = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polIbBs = MarshalByte.readString(buffer, position, Len.POL_IB_BS);
        position += Len.POL_IB_BS;
        polFlPoliIfp = MarshalByte.readChar(buffer, position);
    }

    public byte[] getPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, polIdPoli, Len.Int.POL_ID_POLI, 0);
        position += Len.POL_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, polIdMoviCrz, Len.Int.POL_ID_MOVI_CRZ, 0);
        position += Len.POL_ID_MOVI_CRZ;
        polIdMoviChiu.getPolIdMoviChiuAsBuffer(buffer, position);
        position += PolIdMoviChiu.Len.POL_ID_MOVI_CHIU;
        MarshalByte.writeString(buffer, position, polIbOgg, Len.POL_IB_OGG);
        position += Len.POL_IB_OGG;
        MarshalByte.writeString(buffer, position, polIbProp, Len.POL_IB_PROP);
        position += Len.POL_IB_PROP;
        polDtProp.getPolDtPropAsBuffer(buffer, position);
        position += PolDtProp.Len.POL_DT_PROP;
        MarshalByte.writeIntAsPacked(buffer, position, polDtIniEff, Len.Int.POL_DT_INI_EFF, 0);
        position += Len.POL_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, polDtEndEff, Len.Int.POL_DT_END_EFF, 0);
        position += Len.POL_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, polCodCompAnia, Len.Int.POL_COD_COMP_ANIA, 0);
        position += Len.POL_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, polDtDecor, Len.Int.POL_DT_DECOR, 0);
        position += Len.POL_DT_DECOR;
        MarshalByte.writeIntAsPacked(buffer, position, polDtEmis, Len.Int.POL_DT_EMIS, 0);
        position += Len.POL_DT_EMIS;
        MarshalByte.writeString(buffer, position, polTpPoli, Len.POL_TP_POLI);
        position += Len.POL_TP_POLI;
        polDurAa.getPolDurAaAsBuffer(buffer, position);
        position += PolDurAa.Len.POL_DUR_AA;
        polDurMm.getPolDurMmAsBuffer(buffer, position);
        position += PolDurMm.Len.POL_DUR_MM;
        polDtScad.getPolDtScadAsBuffer(buffer, position);
        position += PolDtScad.Len.POL_DT_SCAD;
        MarshalByte.writeString(buffer, position, polCodProd, Len.POL_COD_PROD);
        position += Len.POL_COD_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, polDtIniVldtProd, Len.Int.POL_DT_INI_VLDT_PROD, 0);
        position += Len.POL_DT_INI_VLDT_PROD;
        MarshalByte.writeString(buffer, position, polCodConv, Len.POL_COD_CONV);
        position += Len.POL_COD_CONV;
        MarshalByte.writeString(buffer, position, polCodRamo, Len.POL_COD_RAMO);
        position += Len.POL_COD_RAMO;
        polDtIniVldtConv.getPolDtIniVldtConvAsBuffer(buffer, position);
        position += PolDtIniVldtConv.Len.POL_DT_INI_VLDT_CONV;
        polDtApplzConv.getPolDtApplzConvAsBuffer(buffer, position);
        position += PolDtApplzConv.Len.POL_DT_APPLZ_CONV;
        MarshalByte.writeString(buffer, position, polTpFrmAssva, Len.POL_TP_FRM_ASSVA);
        position += Len.POL_TP_FRM_ASSVA;
        MarshalByte.writeString(buffer, position, polTpRgmFisc, Len.POL_TP_RGM_FISC);
        position += Len.POL_TP_RGM_FISC;
        MarshalByte.writeChar(buffer, position, polFlEstas);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlRshComun);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlRshComunCond);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polTpLivGenzTit, Len.POL_TP_LIV_GENZ_TIT);
        position += Len.POL_TP_LIV_GENZ_TIT;
        MarshalByte.writeChar(buffer, position, polFlCopFinanz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polTpApplzDir, Len.POL_TP_APPLZ_DIR);
        position += Len.POL_TP_APPLZ_DIR;
        polSpeMed.getPolSpeMedAsBuffer(buffer, position);
        position += PolSpeMed.Len.POL_SPE_MED;
        polDirEmis.getPolDirEmisAsBuffer(buffer, position);
        position += PolDirEmis.Len.POL_DIR_EMIS;
        polDir1oVers.getPolDir1oVersAsBuffer(buffer, position);
        position += PolDir1oVers.Len.POL_DIR1O_VERS;
        polDirVersAgg.getPolDirVersAggAsBuffer(buffer, position);
        position += PolDirVersAgg.Len.POL_DIR_VERS_AGG;
        MarshalByte.writeString(buffer, position, polCodDvs, Len.POL_COD_DVS);
        position += Len.POL_COD_DVS;
        MarshalByte.writeChar(buffer, position, polFlFntAz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlFntAder);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlFntTfr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlFntVolo);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polTpOpzAScad, Len.POL_TP_OPZ_A_SCAD);
        position += Len.POL_TP_OPZ_A_SCAD;
        polAaDiffProrDflt.getPolAaDiffProrDfltAsBuffer(buffer, position);
        position += PolAaDiffProrDflt.Len.POL_AA_DIFF_PROR_DFLT;
        MarshalByte.writeString(buffer, position, polFlVerProd, Len.POL_FL_VER_PROD);
        position += Len.POL_FL_VER_PROD;
        polDurGg.getPolDurGgAsBuffer(buffer, position);
        position += PolDurGg.Len.POL_DUR_GG;
        polDirQuiet.getPolDirQuietAsBuffer(buffer, position);
        position += PolDirQuiet.Len.POL_DIR_QUIET;
        MarshalByte.writeString(buffer, position, polTpPtfEstno, Len.POL_TP_PTF_ESTNO);
        position += Len.POL_TP_PTF_ESTNO;
        MarshalByte.writeChar(buffer, position, polFlCumPreCntr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlAmmbMovi);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polConvGeco, Len.POL_CONV_GECO);
        position += Len.POL_CONV_GECO;
        MarshalByte.writeLongAsPacked(buffer, position, polDsRiga, Len.Int.POL_DS_RIGA, 0);
        position += Len.POL_DS_RIGA;
        MarshalByte.writeChar(buffer, position, polDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, polDsVer, Len.Int.POL_DS_VER, 0);
        position += Len.POL_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, polDsTsIniCptz, Len.Int.POL_DS_TS_INI_CPTZ, 0);
        position += Len.POL_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, polDsTsEndCptz, Len.Int.POL_DS_TS_END_CPTZ, 0);
        position += Len.POL_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, polDsUtente, Len.POL_DS_UTENTE);
        position += Len.POL_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, polDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlScudoFisc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlTrasfe);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlTfrStrc);
        position += Types.CHAR_SIZE;
        polDtPresc.getPolDtPrescAsBuffer(buffer, position);
        position += PolDtPresc.Len.POL_DT_PRESC;
        MarshalByte.writeString(buffer, position, polCodConvAgg, Len.POL_COD_CONV_AGG);
        position += Len.POL_COD_CONV_AGG;
        MarshalByte.writeString(buffer, position, polSubcatProd, Len.POL_SUBCAT_PROD);
        position += Len.POL_SUBCAT_PROD;
        MarshalByte.writeChar(buffer, position, polFlQuestAdegzAss);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polCodTpa, Len.POL_COD_TPA);
        position += Len.POL_COD_TPA;
        polIdAccComm.getPolIdAccCommAsBuffer(buffer, position);
        position += PolIdAccComm.Len.POL_ID_ACC_COMM;
        MarshalByte.writeChar(buffer, position, polFlPoliCpiPr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlPoliBundling);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polIndPoliPrinColl);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlVndBundle);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polIbBs, Len.POL_IB_BS);
        position += Len.POL_IB_BS;
        MarshalByte.writeChar(buffer, position, polFlPoliIfp);
        return buffer;
    }

    public void setPolIdPoli(int polIdPoli) {
        this.polIdPoli = polIdPoli;
    }

    public int getPolIdPoli() {
        return this.polIdPoli;
    }

    public String getPolIdPoliFormatted() {
        return PicFormatter.display(new PicParams("S9(9)V").setUsage(PicUsage.PACKED)).format(getPolIdPoli()).toString();
    }

    public void setPolIdMoviCrz(int polIdMoviCrz) {
        this.polIdMoviCrz = polIdMoviCrz;
    }

    public int getPolIdMoviCrz() {
        return this.polIdMoviCrz;
    }

    public void setPolIbOgg(String polIbOgg) {
        this.polIbOgg = Functions.subString(polIbOgg, Len.POL_IB_OGG);
    }

    public String getPolIbOgg() {
        return this.polIbOgg;
    }

    public void setPolIbProp(String polIbProp) {
        this.polIbProp = Functions.subString(polIbProp, Len.POL_IB_PROP);
    }

    public String getPolIbProp() {
        return this.polIbProp;
    }

    public void setPolDtIniEff(int polDtIniEff) {
        this.polDtIniEff = polDtIniEff;
    }

    public int getPolDtIniEff() {
        return this.polDtIniEff;
    }

    public void setPolDtEndEff(int polDtEndEff) {
        this.polDtEndEff = polDtEndEff;
    }

    public int getPolDtEndEff() {
        return this.polDtEndEff;
    }

    public void setPolCodCompAnia(int polCodCompAnia) {
        this.polCodCompAnia = polCodCompAnia;
    }

    public int getPolCodCompAnia() {
        return this.polCodCompAnia;
    }

    public void setPolDtDecor(int polDtDecor) {
        this.polDtDecor = polDtDecor;
    }

    public int getPolDtDecor() {
        return this.polDtDecor;
    }

    public String getPolDtDecorFormatted() {
        return PicFormatter.display(new PicParams("S9(8)V").setUsage(PicUsage.PACKED)).format(getPolDtDecor()).toString();
    }

    public void setPolDtEmis(int polDtEmis) {
        this.polDtEmis = polDtEmis;
    }

    public int getPolDtEmis() {
        return this.polDtEmis;
    }

    public void setPolTpPoli(String polTpPoli) {
        this.polTpPoli = Functions.subString(polTpPoli, Len.POL_TP_POLI);
    }

    public String getPolTpPoli() {
        return this.polTpPoli;
    }

    public void setPolCodProd(String polCodProd) {
        this.polCodProd = Functions.subString(polCodProd, Len.POL_COD_PROD);
    }

    public String getPolCodProd() {
        return this.polCodProd;
    }

    public void setPolDtIniVldtProd(int polDtIniVldtProd) {
        this.polDtIniVldtProd = polDtIniVldtProd;
    }

    public int getPolDtIniVldtProd() {
        return this.polDtIniVldtProd;
    }

    public void setPolCodConv(String polCodConv) {
        this.polCodConv = Functions.subString(polCodConv, Len.POL_COD_CONV);
    }

    public String getPolCodConv() {
        return this.polCodConv;
    }

    public String getPolCodConvFormatted() {
        return Functions.padBlanks(getPolCodConv(), Len.POL_COD_CONV);
    }

    public void setPolCodRamo(String polCodRamo) {
        this.polCodRamo = Functions.subString(polCodRamo, Len.POL_COD_RAMO);
    }

    public String getPolCodRamo() {
        return this.polCodRamo;
    }

    public String getPolCodRamoFormatted() {
        return Functions.padBlanks(getPolCodRamo(), Len.POL_COD_RAMO);
    }

    public void setPolTpFrmAssva(String polTpFrmAssva) {
        this.polTpFrmAssva = Functions.subString(polTpFrmAssva, Len.POL_TP_FRM_ASSVA);
    }

    public String getPolTpFrmAssva() {
        return this.polTpFrmAssva;
    }

    public void setPolTpRgmFisc(String polTpRgmFisc) {
        this.polTpRgmFisc = Functions.subString(polTpRgmFisc, Len.POL_TP_RGM_FISC);
    }

    public String getPolTpRgmFisc() {
        return this.polTpRgmFisc;
    }

    public String getPolTpRgmFiscFormatted() {
        return Functions.padBlanks(getPolTpRgmFisc(), Len.POL_TP_RGM_FISC);
    }

    public void setPolFlEstas(char polFlEstas) {
        this.polFlEstas = polFlEstas;
    }

    public char getPolFlEstas() {
        return this.polFlEstas;
    }

    public void setPolFlRshComun(char polFlRshComun) {
        this.polFlRshComun = polFlRshComun;
    }

    public char getPolFlRshComun() {
        return this.polFlRshComun;
    }

    public void setPolFlRshComunCond(char polFlRshComunCond) {
        this.polFlRshComunCond = polFlRshComunCond;
    }

    public char getPolFlRshComunCond() {
        return this.polFlRshComunCond;
    }

    public void setPolTpLivGenzTit(String polTpLivGenzTit) {
        this.polTpLivGenzTit = Functions.subString(polTpLivGenzTit, Len.POL_TP_LIV_GENZ_TIT);
    }

    public String getPolTpLivGenzTit() {
        return this.polTpLivGenzTit;
    }

    public void setPolFlCopFinanz(char polFlCopFinanz) {
        this.polFlCopFinanz = polFlCopFinanz;
    }

    public char getPolFlCopFinanz() {
        return this.polFlCopFinanz;
    }

    public void setPolTpApplzDir(String polTpApplzDir) {
        this.polTpApplzDir = Functions.subString(polTpApplzDir, Len.POL_TP_APPLZ_DIR);
    }

    public String getPolTpApplzDir() {
        return this.polTpApplzDir;
    }

    public String getPolTpApplzDirFormatted() {
        return Functions.padBlanks(getPolTpApplzDir(), Len.POL_TP_APPLZ_DIR);
    }

    public void setPolCodDvs(String polCodDvs) {
        this.polCodDvs = Functions.subString(polCodDvs, Len.POL_COD_DVS);
    }

    public String getPolCodDvs() {
        return this.polCodDvs;
    }

    public String getPolCodDvsFormatted() {
        return Functions.padBlanks(getPolCodDvs(), Len.POL_COD_DVS);
    }

    public void setPolFlFntAz(char polFlFntAz) {
        this.polFlFntAz = polFlFntAz;
    }

    public char getPolFlFntAz() {
        return this.polFlFntAz;
    }

    public void setPolFlFntAder(char polFlFntAder) {
        this.polFlFntAder = polFlFntAder;
    }

    public char getPolFlFntAder() {
        return this.polFlFntAder;
    }

    public void setPolFlFntTfr(char polFlFntTfr) {
        this.polFlFntTfr = polFlFntTfr;
    }

    public char getPolFlFntTfr() {
        return this.polFlFntTfr;
    }

    public void setPolFlFntVolo(char polFlFntVolo) {
        this.polFlFntVolo = polFlFntVolo;
    }

    public char getPolFlFntVolo() {
        return this.polFlFntVolo;
    }

    public void setPolTpOpzAScad(String polTpOpzAScad) {
        this.polTpOpzAScad = Functions.subString(polTpOpzAScad, Len.POL_TP_OPZ_A_SCAD);
    }

    public String getPolTpOpzAScad() {
        return this.polTpOpzAScad;
    }

    public String getPolTpOpzAScadFormatted() {
        return Functions.padBlanks(getPolTpOpzAScad(), Len.POL_TP_OPZ_A_SCAD);
    }

    public void setPolFlVerProd(String polFlVerProd) {
        this.polFlVerProd = Functions.subString(polFlVerProd, Len.POL_FL_VER_PROD);
    }

    public String getPolFlVerProd() {
        return this.polFlVerProd;
    }

    public String getPolFlVerProdFormatted() {
        return Functions.padBlanks(getPolFlVerProd(), Len.POL_FL_VER_PROD);
    }

    public void setPolTpPtfEstno(String polTpPtfEstno) {
        this.polTpPtfEstno = Functions.subString(polTpPtfEstno, Len.POL_TP_PTF_ESTNO);
    }

    public String getPolTpPtfEstno() {
        return this.polTpPtfEstno;
    }

    public String getPolTpPtfEstnoFormatted() {
        return Functions.padBlanks(getPolTpPtfEstno(), Len.POL_TP_PTF_ESTNO);
    }

    public void setPolFlCumPreCntr(char polFlCumPreCntr) {
        this.polFlCumPreCntr = polFlCumPreCntr;
    }

    public char getPolFlCumPreCntr() {
        return this.polFlCumPreCntr;
    }

    public void setPolFlAmmbMovi(char polFlAmmbMovi) {
        this.polFlAmmbMovi = polFlAmmbMovi;
    }

    public char getPolFlAmmbMovi() {
        return this.polFlAmmbMovi;
    }

    public void setPolConvGeco(String polConvGeco) {
        this.polConvGeco = Functions.subString(polConvGeco, Len.POL_CONV_GECO);
    }

    public String getPolConvGeco() {
        return this.polConvGeco;
    }

    public String getPolConvGecoFormatted() {
        return Functions.padBlanks(getPolConvGeco(), Len.POL_CONV_GECO);
    }

    public void setPolDsRiga(long polDsRiga) {
        this.polDsRiga = polDsRiga;
    }

    public long getPolDsRiga() {
        return this.polDsRiga;
    }

    public void setPolDsOperSql(char polDsOperSql) {
        this.polDsOperSql = polDsOperSql;
    }

    public void setPolDsOperSqlFormatted(String polDsOperSql) {
        setPolDsOperSql(Functions.charAt(polDsOperSql, Types.CHAR_SIZE));
    }

    public char getPolDsOperSql() {
        return this.polDsOperSql;
    }

    public void setPolDsVer(int polDsVer) {
        this.polDsVer = polDsVer;
    }

    public void setPolDsVerFormatted(String polDsVer) {
        setPolDsVer(PicParser.display(new PicParams("S9(9)V").setUsage(PicUsage.PACKED)).parseInt(polDsVer));
    }

    public int getPolDsVer() {
        return this.polDsVer;
    }

    public void setPolDsTsIniCptz(long polDsTsIniCptz) {
        this.polDsTsIniCptz = polDsTsIniCptz;
    }

    public long getPolDsTsIniCptz() {
        return this.polDsTsIniCptz;
    }

    public void setPolDsTsEndCptz(long polDsTsEndCptz) {
        this.polDsTsEndCptz = polDsTsEndCptz;
    }

    public long getPolDsTsEndCptz() {
        return this.polDsTsEndCptz;
    }

    public void setPolDsUtente(String polDsUtente) {
        this.polDsUtente = Functions.subString(polDsUtente, Len.POL_DS_UTENTE);
    }

    public String getPolDsUtente() {
        return this.polDsUtente;
    }

    public void setPolDsStatoElab(char polDsStatoElab) {
        this.polDsStatoElab = polDsStatoElab;
    }

    public void setPolDsStatoElabFormatted(String polDsStatoElab) {
        setPolDsStatoElab(Functions.charAt(polDsStatoElab, Types.CHAR_SIZE));
    }

    public char getPolDsStatoElab() {
        return this.polDsStatoElab;
    }

    public void setPolFlScudoFisc(char polFlScudoFisc) {
        this.polFlScudoFisc = polFlScudoFisc;
    }

    public char getPolFlScudoFisc() {
        return this.polFlScudoFisc;
    }

    public void setPolFlTrasfe(char polFlTrasfe) {
        this.polFlTrasfe = polFlTrasfe;
    }

    public char getPolFlTrasfe() {
        return this.polFlTrasfe;
    }

    public void setPolFlTfrStrc(char polFlTfrStrc) {
        this.polFlTfrStrc = polFlTfrStrc;
    }

    public char getPolFlTfrStrc() {
        return this.polFlTfrStrc;
    }

    public void setPolCodConvAgg(String polCodConvAgg) {
        this.polCodConvAgg = Functions.subString(polCodConvAgg, Len.POL_COD_CONV_AGG);
    }

    public String getPolCodConvAgg() {
        return this.polCodConvAgg;
    }

    public String getPolCodConvAggFormatted() {
        return Functions.padBlanks(getPolCodConvAgg(), Len.POL_COD_CONV_AGG);
    }

    public void setPolSubcatProd(String polSubcatProd) {
        this.polSubcatProd = Functions.subString(polSubcatProd, Len.POL_SUBCAT_PROD);
    }

    public String getPolSubcatProd() {
        return this.polSubcatProd;
    }

    public String getPolSubcatProdFormatted() {
        return Functions.padBlanks(getPolSubcatProd(), Len.POL_SUBCAT_PROD);
    }

    public void setPolFlQuestAdegzAss(char polFlQuestAdegzAss) {
        this.polFlQuestAdegzAss = polFlQuestAdegzAss;
    }

    public char getPolFlQuestAdegzAss() {
        return this.polFlQuestAdegzAss;
    }

    public void setPolCodTpa(String polCodTpa) {
        this.polCodTpa = Functions.subString(polCodTpa, Len.POL_COD_TPA);
    }

    public String getPolCodTpa() {
        return this.polCodTpa;
    }

    public String getPolCodTpaFormatted() {
        return Functions.padBlanks(getPolCodTpa(), Len.POL_COD_TPA);
    }

    public void setPolFlPoliCpiPr(char polFlPoliCpiPr) {
        this.polFlPoliCpiPr = polFlPoliCpiPr;
    }

    public char getPolFlPoliCpiPr() {
        return this.polFlPoliCpiPr;
    }

    public void setPolFlPoliBundling(char polFlPoliBundling) {
        this.polFlPoliBundling = polFlPoliBundling;
    }

    public char getPolFlPoliBundling() {
        return this.polFlPoliBundling;
    }

    public void setPolIndPoliPrinColl(char polIndPoliPrinColl) {
        this.polIndPoliPrinColl = polIndPoliPrinColl;
    }

    public char getPolIndPoliPrinColl() {
        return this.polIndPoliPrinColl;
    }

    public void setPolFlVndBundle(char polFlVndBundle) {
        this.polFlVndBundle = polFlVndBundle;
    }

    public char getPolFlVndBundle() {
        return this.polFlVndBundle;
    }

    public void setPolIbBs(String polIbBs) {
        this.polIbBs = Functions.subString(polIbBs, Len.POL_IB_BS);
    }

    public String getPolIbBs() {
        return this.polIbBs;
    }

    public void setPolFlPoliIfp(char polFlPoliIfp) {
        this.polFlPoliIfp = polFlPoliIfp;
    }

    public char getPolFlPoliIfp() {
        return this.polFlPoliIfp;
    }

    public PolAaDiffProrDflt getPolAaDiffProrDflt() {
        return polAaDiffProrDflt;
    }

    public PolDir1oVers getPolDir1oVers() {
        return polDir1oVers;
    }

    public PolDirEmis getPolDirEmis() {
        return polDirEmis;
    }

    public PolDirQuiet getPolDirQuiet() {
        return polDirQuiet;
    }

    public PolDirVersAgg getPolDirVersAgg() {
        return polDirVersAgg;
    }

    public PolDtApplzConv getPolDtApplzConv() {
        return polDtApplzConv;
    }

    public PolDtIniVldtConv getPolDtIniVldtConv() {
        return polDtIniVldtConv;
    }

    public PolDtPresc getPolDtPresc() {
        return polDtPresc;
    }

    public PolDtProp getPolDtProp() {
        return polDtProp;
    }

    public PolDtScad getPolDtScad() {
        return polDtScad;
    }

    public PolDurAa getPolDurAa() {
        return polDurAa;
    }

    public PolDurGg getPolDurGg() {
        return polDurGg;
    }

    public PolDurMm getPolDurMm() {
        return polDurMm;
    }

    public PolIdAccComm getPolIdAccComm() {
        return polIdAccComm;
    }

    public PolIdMoviChiu getPolIdMoviChiu() {
        return polIdMoviChiu;
    }

    public PolSpeMed getPolSpeMed() {
        return polSpeMed;
    }

    @Override
    public byte[] serialize() {
        return getPoliBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_ID_POLI = 5;
        public static final int POL_ID_MOVI_CRZ = 5;
        public static final int POL_IB_OGG = 40;
        public static final int POL_IB_PROP = 40;
        public static final int POL_DT_INI_EFF = 5;
        public static final int POL_DT_END_EFF = 5;
        public static final int POL_COD_COMP_ANIA = 3;
        public static final int POL_DT_DECOR = 5;
        public static final int POL_DT_EMIS = 5;
        public static final int POL_TP_POLI = 2;
        public static final int POL_COD_PROD = 12;
        public static final int POL_DT_INI_VLDT_PROD = 5;
        public static final int POL_COD_CONV = 12;
        public static final int POL_COD_RAMO = 12;
        public static final int POL_TP_FRM_ASSVA = 2;
        public static final int POL_TP_RGM_FISC = 2;
        public static final int POL_FL_ESTAS = 1;
        public static final int POL_FL_RSH_COMUN = 1;
        public static final int POL_FL_RSH_COMUN_COND = 1;
        public static final int POL_TP_LIV_GENZ_TIT = 2;
        public static final int POL_FL_COP_FINANZ = 1;
        public static final int POL_TP_APPLZ_DIR = 2;
        public static final int POL_COD_DVS = 20;
        public static final int POL_FL_FNT_AZ = 1;
        public static final int POL_FL_FNT_ADER = 1;
        public static final int POL_FL_FNT_TFR = 1;
        public static final int POL_FL_FNT_VOLO = 1;
        public static final int POL_TP_OPZ_A_SCAD = 2;
        public static final int POL_FL_VER_PROD = 2;
        public static final int POL_TP_PTF_ESTNO = 2;
        public static final int POL_FL_CUM_PRE_CNTR = 1;
        public static final int POL_FL_AMMB_MOVI = 1;
        public static final int POL_CONV_GECO = 5;
        public static final int POL_DS_RIGA = 6;
        public static final int POL_DS_OPER_SQL = 1;
        public static final int POL_DS_VER = 5;
        public static final int POL_DS_TS_INI_CPTZ = 10;
        public static final int POL_DS_TS_END_CPTZ = 10;
        public static final int POL_DS_UTENTE = 20;
        public static final int POL_DS_STATO_ELAB = 1;
        public static final int POL_FL_SCUDO_FISC = 1;
        public static final int POL_FL_TRASFE = 1;
        public static final int POL_FL_TFR_STRC = 1;
        public static final int POL_COD_CONV_AGG = 12;
        public static final int POL_SUBCAT_PROD = 12;
        public static final int POL_FL_QUEST_ADEGZ_ASS = 1;
        public static final int POL_COD_TPA = 4;
        public static final int POL_FL_POLI_CPI_PR = 1;
        public static final int POL_FL_POLI_BUNDLING = 1;
        public static final int POL_IND_POLI_PRIN_COLL = 1;
        public static final int POL_FL_VND_BUNDLE = 1;
        public static final int POL_IB_BS = 40;
        public static final int POL_FL_POLI_IFP = 1;
        public static final int POLI = POL_ID_POLI + POL_ID_MOVI_CRZ + PolIdMoviChiu.Len.POL_ID_MOVI_CHIU + POL_IB_OGG + POL_IB_PROP + PolDtProp.Len.POL_DT_PROP + POL_DT_INI_EFF + POL_DT_END_EFF + POL_COD_COMP_ANIA + POL_DT_DECOR + POL_DT_EMIS + POL_TP_POLI + PolDurAa.Len.POL_DUR_AA + PolDurMm.Len.POL_DUR_MM + PolDtScad.Len.POL_DT_SCAD + POL_COD_PROD + POL_DT_INI_VLDT_PROD + POL_COD_CONV + POL_COD_RAMO + PolDtIniVldtConv.Len.POL_DT_INI_VLDT_CONV + PolDtApplzConv.Len.POL_DT_APPLZ_CONV + POL_TP_FRM_ASSVA + POL_TP_RGM_FISC + POL_FL_ESTAS + POL_FL_RSH_COMUN + POL_FL_RSH_COMUN_COND + POL_TP_LIV_GENZ_TIT + POL_FL_COP_FINANZ + POL_TP_APPLZ_DIR + PolSpeMed.Len.POL_SPE_MED + PolDirEmis.Len.POL_DIR_EMIS + PolDir1oVers.Len.POL_DIR1O_VERS + PolDirVersAgg.Len.POL_DIR_VERS_AGG + POL_COD_DVS + POL_FL_FNT_AZ + POL_FL_FNT_ADER + POL_FL_FNT_TFR + POL_FL_FNT_VOLO + POL_TP_OPZ_A_SCAD + PolAaDiffProrDflt.Len.POL_AA_DIFF_PROR_DFLT + POL_FL_VER_PROD + PolDurGg.Len.POL_DUR_GG + PolDirQuiet.Len.POL_DIR_QUIET + POL_TP_PTF_ESTNO + POL_FL_CUM_PRE_CNTR + POL_FL_AMMB_MOVI + POL_CONV_GECO + POL_DS_RIGA + POL_DS_OPER_SQL + POL_DS_VER + POL_DS_TS_INI_CPTZ + POL_DS_TS_END_CPTZ + POL_DS_UTENTE + POL_DS_STATO_ELAB + POL_FL_SCUDO_FISC + POL_FL_TRASFE + POL_FL_TFR_STRC + PolDtPresc.Len.POL_DT_PRESC + POL_COD_CONV_AGG + POL_SUBCAT_PROD + POL_FL_QUEST_ADEGZ_ASS + POL_COD_TPA + PolIdAccComm.Len.POL_ID_ACC_COMM + POL_FL_POLI_CPI_PR + POL_FL_POLI_BUNDLING + POL_IND_POLI_PRIN_COLL + POL_FL_VND_BUNDLE + POL_IB_BS + POL_FL_POLI_IFP;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_ID_POLI = 9;
            public static final int POL_ID_MOVI_CRZ = 9;
            public static final int POL_DT_INI_EFF = 8;
            public static final int POL_DT_END_EFF = 8;
            public static final int POL_COD_COMP_ANIA = 5;
            public static final int POL_DT_DECOR = 8;
            public static final int POL_DT_EMIS = 8;
            public static final int POL_DT_INI_VLDT_PROD = 8;
            public static final int POL_DS_RIGA = 10;
            public static final int POL_DS_VER = 9;
            public static final int POL_DS_TS_INI_CPTZ = 18;
            public static final int POL_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
