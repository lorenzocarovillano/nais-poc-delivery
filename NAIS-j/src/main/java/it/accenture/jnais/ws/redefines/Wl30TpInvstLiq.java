package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WL30-TP-INVST-LIQ<br>
 * Variable: WL30-TP-INVST-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl30TpInvstLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl30TpInvstLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL30_TP_INVST_LIQ;
    }

    public void setWl30TpInvstLiq(short wl30TpInvstLiq) {
        writeShortAsPacked(Pos.WL30_TP_INVST_LIQ, wl30TpInvstLiq, Len.Int.WL30_TP_INVST_LIQ);
    }

    public void setWl30TpInvstLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL30_TP_INVST_LIQ, Pos.WL30_TP_INVST_LIQ);
    }

    /**Original name: WL30-TP-INVST-LIQ<br>*/
    public short getWl30TpInvstLiq() {
        return readPackedAsShort(Pos.WL30_TP_INVST_LIQ, Len.Int.WL30_TP_INVST_LIQ);
    }

    public byte[] getWl30TpInvstLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL30_TP_INVST_LIQ, Pos.WL30_TP_INVST_LIQ);
        return buffer;
    }

    public void initWl30TpInvstLiqSpaces() {
        fill(Pos.WL30_TP_INVST_LIQ, Len.WL30_TP_INVST_LIQ, Types.SPACE_CHAR);
    }

    public void setWl30TpInvstLiqNull(String wl30TpInvstLiqNull) {
        writeString(Pos.WL30_TP_INVST_LIQ_NULL, wl30TpInvstLiqNull, Len.WL30_TP_INVST_LIQ_NULL);
    }

    /**Original name: WL30-TP-INVST-LIQ-NULL<br>*/
    public String getWl30TpInvstLiqNull() {
        return readString(Pos.WL30_TP_INVST_LIQ_NULL, Len.WL30_TP_INVST_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL30_TP_INVST_LIQ = 1;
        public static final int WL30_TP_INVST_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL30_TP_INVST_LIQ = 2;
        public static final int WL30_TP_INVST_LIQ_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL30_TP_INVST_LIQ = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
