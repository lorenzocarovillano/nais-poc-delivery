package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-CAR-ACQ-NON-SCON<br>
 * Variable: W-B03-CAR-ACQ-NON-SCON from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CarAcqNonSconLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CarAcqNonSconLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_CAR_ACQ_NON_SCON;
    }

    public void setwB03CarAcqNonScon(AfDecimal wB03CarAcqNonScon) {
        writeDecimalAsPacked(Pos.W_B03_CAR_ACQ_NON_SCON, wB03CarAcqNonScon.copy());
    }

    /**Original name: W-B03-CAR-ACQ-NON-SCON<br>*/
    public AfDecimal getwB03CarAcqNonScon() {
        return readPackedAsDecimal(Pos.W_B03_CAR_ACQ_NON_SCON, Len.Int.W_B03_CAR_ACQ_NON_SCON, Len.Fract.W_B03_CAR_ACQ_NON_SCON);
    }

    public byte[] getwB03CarAcqNonSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_CAR_ACQ_NON_SCON, Pos.W_B03_CAR_ACQ_NON_SCON);
        return buffer;
    }

    public void setwB03CarAcqNonSconNull(String wB03CarAcqNonSconNull) {
        writeString(Pos.W_B03_CAR_ACQ_NON_SCON_NULL, wB03CarAcqNonSconNull, Len.W_B03_CAR_ACQ_NON_SCON_NULL);
    }

    /**Original name: W-B03-CAR-ACQ-NON-SCON-NULL<br>*/
    public String getwB03CarAcqNonSconNull() {
        return readString(Pos.W_B03_CAR_ACQ_NON_SCON_NULL, Len.W_B03_CAR_ACQ_NON_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_CAR_ACQ_NON_SCON = 1;
        public static final int W_B03_CAR_ACQ_NON_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_CAR_ACQ_NON_SCON = 8;
        public static final int W_B03_CAR_ACQ_NON_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_CAR_ACQ_NON_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_CAR_ACQ_NON_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
