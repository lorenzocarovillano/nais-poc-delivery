package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-DUR-AA<br>
 * Variable: W-B03-DUR-AA from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03DurAaLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03DurAaLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DUR_AA;
    }

    public void setwB03DurAa(int wB03DurAa) {
        writeIntAsPacked(Pos.W_B03_DUR_AA, wB03DurAa, Len.Int.W_B03_DUR_AA);
    }

    /**Original name: W-B03-DUR-AA<br>*/
    public int getwB03DurAa() {
        return readPackedAsInt(Pos.W_B03_DUR_AA, Len.Int.W_B03_DUR_AA);
    }

    public byte[] getwB03DurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DUR_AA, Pos.W_B03_DUR_AA);
        return buffer;
    }

    public void setwB03DurAaNull(String wB03DurAaNull) {
        writeString(Pos.W_B03_DUR_AA_NULL, wB03DurAaNull, Len.W_B03_DUR_AA_NULL);
    }

    /**Original name: W-B03-DUR-AA-NULL<br>*/
    public String getwB03DurAaNull() {
        return readString(Pos.W_B03_DUR_AA_NULL, Len.W_B03_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_AA = 1;
        public static final int W_B03_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_AA = 3;
        public static final int W_B03_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
