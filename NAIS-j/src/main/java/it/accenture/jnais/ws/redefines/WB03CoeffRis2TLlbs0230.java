package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-COEFF-RIS-2-T<br>
 * Variable: W-B03-COEFF-RIS-2-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CoeffRis2TLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CoeffRis2TLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_COEFF_RIS2_T;
    }

    public void setwB03CoeffRis2T(AfDecimal wB03CoeffRis2T) {
        writeDecimalAsPacked(Pos.W_B03_COEFF_RIS2_T, wB03CoeffRis2T.copy());
    }

    /**Original name: W-B03-COEFF-RIS-2-T<br>*/
    public AfDecimal getwB03CoeffRis2T() {
        return readPackedAsDecimal(Pos.W_B03_COEFF_RIS2_T, Len.Int.W_B03_COEFF_RIS2_T, Len.Fract.W_B03_COEFF_RIS2_T);
    }

    public byte[] getwB03CoeffRis2TAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_COEFF_RIS2_T, Pos.W_B03_COEFF_RIS2_T);
        return buffer;
    }

    public void setwB03CoeffRis2TNull(String wB03CoeffRis2TNull) {
        writeString(Pos.W_B03_COEFF_RIS2_T_NULL, wB03CoeffRis2TNull, Len.W_B03_COEFF_RIS2_T_NULL);
    }

    /**Original name: W-B03-COEFF-RIS-2-T-NULL<br>*/
    public String getwB03CoeffRis2TNull() {
        return readString(Pos.W_B03_COEFF_RIS2_T_NULL, Len.W_B03_COEFF_RIS2_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_COEFF_RIS2_T = 1;
        public static final int W_B03_COEFF_RIS2_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_COEFF_RIS2_T = 8;
        public static final int W_B03_COEFF_RIS2_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_COEFF_RIS2_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_COEFF_RIS2_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
