package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ISPC0040-ADES-SENZA-ASSIC<br>
 * Variable: ISPC0040-ADES-SENZA-ASSIC from copybook ISPC0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0040AdesSenzaAssic {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = '1';
    public static final char NO = '0';

    //==== METHODS ====
    public void setAdesSenzaAssic(char adesSenzaAssic) {
        this.value = adesSenzaAssic;
    }

    public char getAdesSenzaAssic() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ADES_SENZA_ASSIC = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
