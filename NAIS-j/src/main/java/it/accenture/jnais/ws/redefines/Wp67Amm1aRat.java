package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-AMM-1A-RAT<br>
 * Variable: WP67-AMM-1A-RAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67Amm1aRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67Amm1aRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_AMM1A_RAT;
    }

    public void setWp67Amm1aRat(AfDecimal wp67Amm1aRat) {
        writeDecimalAsPacked(Pos.WP67_AMM1A_RAT, wp67Amm1aRat.copy());
    }

    public void setWp67Amm1aRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_AMM1A_RAT, Pos.WP67_AMM1A_RAT);
    }

    /**Original name: WP67-AMM-1A-RAT<br>*/
    public AfDecimal getWp67Amm1aRat() {
        return readPackedAsDecimal(Pos.WP67_AMM1A_RAT, Len.Int.WP67_AMM1A_RAT, Len.Fract.WP67_AMM1A_RAT);
    }

    public byte[] getWp67Amm1aRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_AMM1A_RAT, Pos.WP67_AMM1A_RAT);
        return buffer;
    }

    public void setWp67Amm1aRatNull(String wp67Amm1aRatNull) {
        writeString(Pos.WP67_AMM1A_RAT_NULL, wp67Amm1aRatNull, Len.WP67_AMM1A_RAT_NULL);
    }

    /**Original name: WP67-AMM-1A-RAT-NULL<br>*/
    public String getWp67Amm1aRatNull() {
        return readString(Pos.WP67_AMM1A_RAT_NULL, Len.WP67_AMM1A_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_AMM1A_RAT = 1;
        public static final int WP67_AMM1A_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_AMM1A_RAT = 8;
        public static final int WP67_AMM1A_RAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_AMM1A_RAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_AMM1A_RAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
