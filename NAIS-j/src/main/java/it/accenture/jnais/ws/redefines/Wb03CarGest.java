package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CAR-GEST<br>
 * Variable: WB03-CAR-GEST from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CAR_GEST;
    }

    public void setWb03CarGest(AfDecimal wb03CarGest) {
        writeDecimalAsPacked(Pos.WB03_CAR_GEST, wb03CarGest.copy());
    }

    public void setWb03CarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CAR_GEST, Pos.WB03_CAR_GEST);
    }

    /**Original name: WB03-CAR-GEST<br>*/
    public AfDecimal getWb03CarGest() {
        return readPackedAsDecimal(Pos.WB03_CAR_GEST, Len.Int.WB03_CAR_GEST, Len.Fract.WB03_CAR_GEST);
    }

    public byte[] getWb03CarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CAR_GEST, Pos.WB03_CAR_GEST);
        return buffer;
    }

    public void setWb03CarGestNull(String wb03CarGestNull) {
        writeString(Pos.WB03_CAR_GEST_NULL, wb03CarGestNull, Len.WB03_CAR_GEST_NULL);
    }

    /**Original name: WB03-CAR-GEST-NULL<br>*/
    public String getWb03CarGestNull() {
        return readString(Pos.WB03_CAR_GEST_NULL, Len.WB03_CAR_GEST_NULL);
    }

    public String getWb03CarGestNullFormatted() {
        return Functions.padBlanks(getWb03CarGestNull(), Len.WB03_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CAR_GEST = 1;
        public static final int WB03_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CAR_GEST = 8;
        public static final int WB03_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
