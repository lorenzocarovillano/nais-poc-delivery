package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-FRAZ-PAG-INTR<br>
 * Variable: PRE-FRAZ-PAG-INTR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreFrazPagIntr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreFrazPagIntr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_FRAZ_PAG_INTR;
    }

    public void setPreFrazPagIntr(int preFrazPagIntr) {
        writeIntAsPacked(Pos.PRE_FRAZ_PAG_INTR, preFrazPagIntr, Len.Int.PRE_FRAZ_PAG_INTR);
    }

    public void setPreFrazPagIntrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_FRAZ_PAG_INTR, Pos.PRE_FRAZ_PAG_INTR);
    }

    /**Original name: PRE-FRAZ-PAG-INTR<br>*/
    public int getPreFrazPagIntr() {
        return readPackedAsInt(Pos.PRE_FRAZ_PAG_INTR, Len.Int.PRE_FRAZ_PAG_INTR);
    }

    public byte[] getPreFrazPagIntrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_FRAZ_PAG_INTR, Pos.PRE_FRAZ_PAG_INTR);
        return buffer;
    }

    public void setPreFrazPagIntrNull(String preFrazPagIntrNull) {
        writeString(Pos.PRE_FRAZ_PAG_INTR_NULL, preFrazPagIntrNull, Len.PRE_FRAZ_PAG_INTR_NULL);
    }

    /**Original name: PRE-FRAZ-PAG-INTR-NULL<br>*/
    public String getPreFrazPagIntrNull() {
        return readString(Pos.PRE_FRAZ_PAG_INTR_NULL, Len.PRE_FRAZ_PAG_INTR_NULL);
    }

    public String getPreFrazPagIntrNullFormatted() {
        return Functions.padBlanks(getPreFrazPagIntrNull(), Len.PRE_FRAZ_PAG_INTR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_FRAZ_PAG_INTR = 1;
        public static final int PRE_FRAZ_PAG_INTR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_FRAZ_PAG_INTR = 3;
        public static final int PRE_FRAZ_PAG_INTR_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_FRAZ_PAG_INTR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
