package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CPT-RSH-MOR<br>
 * Variable: WPAG-CPT-RSH-MOR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagCptRshMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagCptRshMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CPT_RSH_MOR;
    }

    public void setWpagCptRshMor(AfDecimal wpagCptRshMor) {
        writeDecimalAsPacked(Pos.WPAG_CPT_RSH_MOR, wpagCptRshMor.copy());
    }

    public void setWpagCptRshMorFormatted(String wpagCptRshMor) {
        setWpagCptRshMor(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CPT_RSH_MOR + Len.Fract.WPAG_CPT_RSH_MOR, Len.Fract.WPAG_CPT_RSH_MOR, wpagCptRshMor));
    }

    public void setWpagCptRshMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CPT_RSH_MOR, Pos.WPAG_CPT_RSH_MOR);
    }

    /**Original name: WPAG-CPT-RSH-MOR<br>*/
    public AfDecimal getWpagCptRshMor() {
        return readPackedAsDecimal(Pos.WPAG_CPT_RSH_MOR, Len.Int.WPAG_CPT_RSH_MOR, Len.Fract.WPAG_CPT_RSH_MOR);
    }

    public byte[] getWpagCptRshMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CPT_RSH_MOR, Pos.WPAG_CPT_RSH_MOR);
        return buffer;
    }

    public void initWpagCptRshMorSpaces() {
        fill(Pos.WPAG_CPT_RSH_MOR, Len.WPAG_CPT_RSH_MOR, Types.SPACE_CHAR);
    }

    public void setWpagCptRshMorNull(String wpagCptRshMorNull) {
        writeString(Pos.WPAG_CPT_RSH_MOR_NULL, wpagCptRshMorNull, Len.WPAG_CPT_RSH_MOR_NULL);
    }

    /**Original name: WPAG-CPT-RSH-MOR-NULL<br>*/
    public String getWpagCptRshMorNull() {
        return readString(Pos.WPAG_CPT_RSH_MOR_NULL, Len.WPAG_CPT_RSH_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CPT_RSH_MOR = 1;
        public static final int WPAG_CPT_RSH_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CPT_RSH_MOR = 8;
        public static final int WPAG_CPT_RSH_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CPT_RSH_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CPT_RSH_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
