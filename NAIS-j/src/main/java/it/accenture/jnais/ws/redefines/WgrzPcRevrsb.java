package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WGRZ-PC-REVRSB<br>
 * Variable: WGRZ-PC-REVRSB from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzPcRevrsb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzPcRevrsb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_PC_REVRSB;
    }

    public void setWgrzPcRevrsb(AfDecimal wgrzPcRevrsb) {
        writeDecimalAsPacked(Pos.WGRZ_PC_REVRSB, wgrzPcRevrsb.copy());
    }

    public void setWgrzPcRevrsbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_PC_REVRSB, Pos.WGRZ_PC_REVRSB);
    }

    /**Original name: WGRZ-PC-REVRSB<br>*/
    public AfDecimal getWgrzPcRevrsb() {
        return readPackedAsDecimal(Pos.WGRZ_PC_REVRSB, Len.Int.WGRZ_PC_REVRSB, Len.Fract.WGRZ_PC_REVRSB);
    }

    public byte[] getWgrzPcRevrsbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_PC_REVRSB, Pos.WGRZ_PC_REVRSB);
        return buffer;
    }

    public void initWgrzPcRevrsbSpaces() {
        fill(Pos.WGRZ_PC_REVRSB, Len.WGRZ_PC_REVRSB, Types.SPACE_CHAR);
    }

    public void setWgrzPcRevrsbNull(String wgrzPcRevrsbNull) {
        writeString(Pos.WGRZ_PC_REVRSB_NULL, wgrzPcRevrsbNull, Len.WGRZ_PC_REVRSB_NULL);
    }

    /**Original name: WGRZ-PC-REVRSB-NULL<br>*/
    public String getWgrzPcRevrsbNull() {
        return readString(Pos.WGRZ_PC_REVRSB_NULL, Len.WGRZ_PC_REVRSB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_PC_REVRSB = 1;
        public static final int WGRZ_PC_REVRSB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_PC_REVRSB = 4;
        public static final int WGRZ_PC_REVRSB_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WGRZ_PC_REVRSB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_PC_REVRSB = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
