package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRAN-DT-DECES<br>
 * Variable: WRAN-DT-DECES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WranDtDeces extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WranDtDeces() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRAN_DT_DECES;
    }

    public void setWranDtDeces(int wranDtDeces) {
        writeIntAsPacked(Pos.WRAN_DT_DECES, wranDtDeces, Len.Int.WRAN_DT_DECES);
    }

    public void setWranDtDecesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRAN_DT_DECES, Pos.WRAN_DT_DECES);
    }

    /**Original name: WRAN-DT-DECES<br>*/
    public int getWranDtDeces() {
        return readPackedAsInt(Pos.WRAN_DT_DECES, Len.Int.WRAN_DT_DECES);
    }

    public byte[] getWranDtDecesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRAN_DT_DECES, Pos.WRAN_DT_DECES);
        return buffer;
    }

    public void initWranDtDecesSpaces() {
        fill(Pos.WRAN_DT_DECES, Len.WRAN_DT_DECES, Types.SPACE_CHAR);
    }

    public void setWranDtDecesNull(String wranDtDecesNull) {
        writeString(Pos.WRAN_DT_DECES_NULL, wranDtDecesNull, Len.WRAN_DT_DECES_NULL);
    }

    /**Original name: WRAN-DT-DECES-NULL<br>*/
    public String getWranDtDecesNull() {
        return readString(Pos.WRAN_DT_DECES_NULL, Len.WRAN_DT_DECES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRAN_DT_DECES = 1;
        public static final int WRAN_DT_DECES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRAN_DT_DECES = 5;
        public static final int WRAN_DT_DECES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRAN_DT_DECES = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
