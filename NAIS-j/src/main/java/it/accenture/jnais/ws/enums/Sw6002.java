package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-6002<br>
 * Variable: SW-6002 from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Sw6002 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO6002 = 'S';
    public static final char NO_TROVATO6002 = 'N';

    //==== METHODS ====
    public void setSw6002(char sw6002) {
        this.value = sw6002;
    }

    public char getSw6002() {
        return this.value;
    }

    public void setTrovato6002() {
        value = TROVATO6002;
    }

    public boolean isNoTrovato6002() {
        return value == NO_TROVATO6002;
    }

    public void setNoTrovato6002() {
        value = NO_TROVATO6002;
    }
}
