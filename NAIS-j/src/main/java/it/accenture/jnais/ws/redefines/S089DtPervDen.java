package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: S089-DT-PERV-DEN<br>
 * Variable: S089-DT-PERV-DEN from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089DtPervDen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089DtPervDen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_DT_PERV_DEN;
    }

    public void setWlquDtPervDen(int wlquDtPervDen) {
        writeIntAsPacked(Pos.S089_DT_PERV_DEN, wlquDtPervDen, Len.Int.WLQU_DT_PERV_DEN);
    }

    public void setWlquDtPervDenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_DT_PERV_DEN, Pos.S089_DT_PERV_DEN);
    }

    /**Original name: WLQU-DT-PERV-DEN<br>*/
    public int getWlquDtPervDen() {
        return readPackedAsInt(Pos.S089_DT_PERV_DEN, Len.Int.WLQU_DT_PERV_DEN);
    }

    public byte[] getWlquDtPervDenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_DT_PERV_DEN, Pos.S089_DT_PERV_DEN);
        return buffer;
    }

    public void initWlquDtPervDenSpaces() {
        fill(Pos.S089_DT_PERV_DEN, Len.S089_DT_PERV_DEN, Types.SPACE_CHAR);
    }

    public void setWlquDtPervDenNull(String wlquDtPervDenNull) {
        writeString(Pos.S089_DT_PERV_DEN_NULL, wlquDtPervDenNull, Len.WLQU_DT_PERV_DEN_NULL);
    }

    /**Original name: WLQU-DT-PERV-DEN-NULL<br>*/
    public String getWlquDtPervDenNull() {
        return readString(Pos.S089_DT_PERV_DEN_NULL, Len.WLQU_DT_PERV_DEN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_DT_PERV_DEN = 1;
        public static final int S089_DT_PERV_DEN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_DT_PERV_DEN = 5;
        public static final int WLQU_DT_PERV_DEN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_DT_PERV_DEN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
