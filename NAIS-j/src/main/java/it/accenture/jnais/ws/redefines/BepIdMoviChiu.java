package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEP-ID-MOVI-CHIU<br>
 * Variable: BEP-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BepIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BepIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEP_ID_MOVI_CHIU;
    }

    public void setBepIdMoviChiu(int bepIdMoviChiu) {
        writeIntAsPacked(Pos.BEP_ID_MOVI_CHIU, bepIdMoviChiu, Len.Int.BEP_ID_MOVI_CHIU);
    }

    public void setBepIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEP_ID_MOVI_CHIU, Pos.BEP_ID_MOVI_CHIU);
    }

    /**Original name: BEP-ID-MOVI-CHIU<br>*/
    public int getBepIdMoviChiu() {
        return readPackedAsInt(Pos.BEP_ID_MOVI_CHIU, Len.Int.BEP_ID_MOVI_CHIU);
    }

    public byte[] getBepIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEP_ID_MOVI_CHIU, Pos.BEP_ID_MOVI_CHIU);
        return buffer;
    }

    public void setBepIdMoviChiuNull(String bepIdMoviChiuNull) {
        writeString(Pos.BEP_ID_MOVI_CHIU_NULL, bepIdMoviChiuNull, Len.BEP_ID_MOVI_CHIU_NULL);
    }

    /**Original name: BEP-ID-MOVI-CHIU-NULL<br>*/
    public String getBepIdMoviChiuNull() {
        return readString(Pos.BEP_ID_MOVI_CHIU_NULL, Len.BEP_ID_MOVI_CHIU_NULL);
    }

    public String getBepIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getBepIdMoviChiuNull(), Len.BEP_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEP_ID_MOVI_CHIU = 1;
        public static final int BEP_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEP_ID_MOVI_CHIU = 5;
        public static final int BEP_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEP_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
