package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CPT-RSH-MOR<br>
 * Variable: WB03-CPT-RSH-MOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CptRshMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CptRshMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CPT_RSH_MOR;
    }

    public void setWb03CptRshMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CPT_RSH_MOR, Pos.WB03_CPT_RSH_MOR);
    }

    /**Original name: WB03-CPT-RSH-MOR<br>*/
    public AfDecimal getWb03CptRshMor() {
        return readPackedAsDecimal(Pos.WB03_CPT_RSH_MOR, Len.Int.WB03_CPT_RSH_MOR, Len.Fract.WB03_CPT_RSH_MOR);
    }

    public byte[] getWb03CptRshMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CPT_RSH_MOR, Pos.WB03_CPT_RSH_MOR);
        return buffer;
    }

    public void setWb03CptRshMorNull(String wb03CptRshMorNull) {
        writeString(Pos.WB03_CPT_RSH_MOR_NULL, wb03CptRshMorNull, Len.WB03_CPT_RSH_MOR_NULL);
    }

    /**Original name: WB03-CPT-RSH-MOR-NULL<br>*/
    public String getWb03CptRshMorNull() {
        return readString(Pos.WB03_CPT_RSH_MOR_NULL, Len.WB03_CPT_RSH_MOR_NULL);
    }

    public String getWb03CptRshMorNullFormatted() {
        return Functions.padBlanks(getWb03CptRshMorNull(), Len.WB03_CPT_RSH_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CPT_RSH_MOR = 1;
        public static final int WB03_CPT_RSH_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CPT_RSH_MOR = 8;
        public static final int WB03_CPT_RSH_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CPT_RSH_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CPT_RSH_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
