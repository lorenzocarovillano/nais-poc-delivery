package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-RIAT<br>
 * Variable: PCO-DT-ULT-BOLL-RIAT from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_RIAT;
    }

    public void setPcoDtUltBollRiat(int pcoDtUltBollRiat) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_RIAT, pcoDtUltBollRiat, Len.Int.PCO_DT_ULT_BOLL_RIAT);
    }

    public void setPcoDtUltBollRiatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RIAT, Pos.PCO_DT_ULT_BOLL_RIAT);
    }

    /**Original name: PCO-DT-ULT-BOLL-RIAT<br>*/
    public int getPcoDtUltBollRiat() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_RIAT, Len.Int.PCO_DT_ULT_BOLL_RIAT);
    }

    public byte[] getPcoDtUltBollRiatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RIAT, Pos.PCO_DT_ULT_BOLL_RIAT);
        return buffer;
    }

    public void initPcoDtUltBollRiatHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_RIAT, Len.PCO_DT_ULT_BOLL_RIAT, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollRiatNull(String pcoDtUltBollRiatNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_RIAT_NULL, pcoDtUltBollRiatNull, Len.PCO_DT_ULT_BOLL_RIAT_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-RIAT-NULL<br>*/
    public String getPcoDtUltBollRiatNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_RIAT_NULL, Len.PCO_DT_ULT_BOLL_RIAT_NULL);
    }

    public String getPcoDtUltBollRiatNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollRiatNull(), Len.PCO_DT_ULT_BOLL_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RIAT = 1;
        public static final int PCO_DT_ULT_BOLL_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RIAT = 5;
        public static final int PCO_DT_ULT_BOLL_RIAT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_RIAT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
