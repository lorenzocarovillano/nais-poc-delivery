package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-PRE-NET-IND<br>
 * Variable: ADE-PRE-NET-IND from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdePreNetInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdePreNetInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_PRE_NET_IND;
    }

    public void setAdePreNetInd(AfDecimal adePreNetInd) {
        writeDecimalAsPacked(Pos.ADE_PRE_NET_IND, adePreNetInd.copy());
    }

    public void setAdePreNetIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_PRE_NET_IND, Pos.ADE_PRE_NET_IND);
    }

    /**Original name: ADE-PRE-NET-IND<br>*/
    public AfDecimal getAdePreNetInd() {
        return readPackedAsDecimal(Pos.ADE_PRE_NET_IND, Len.Int.ADE_PRE_NET_IND, Len.Fract.ADE_PRE_NET_IND);
    }

    public byte[] getAdePreNetIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_PRE_NET_IND, Pos.ADE_PRE_NET_IND);
        return buffer;
    }

    public void setAdePreNetIndNull(String adePreNetIndNull) {
        writeString(Pos.ADE_PRE_NET_IND_NULL, adePreNetIndNull, Len.ADE_PRE_NET_IND_NULL);
    }

    /**Original name: ADE-PRE-NET-IND-NULL<br>*/
    public String getAdePreNetIndNull() {
        return readString(Pos.ADE_PRE_NET_IND_NULL, Len.ADE_PRE_NET_IND_NULL);
    }

    public String getAdePreNetIndNullFormatted() {
        return Functions.padBlanks(getAdePreNetIndNull(), Len.ADE_PRE_NET_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_PRE_NET_IND = 1;
        public static final int ADE_PRE_NET_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_PRE_NET_IND = 8;
        public static final int ADE_PRE_NET_IND_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_PRE_NET_IND = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_PRE_NET_IND = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
