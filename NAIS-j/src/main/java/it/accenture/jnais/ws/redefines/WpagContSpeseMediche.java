package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-SPESE-MEDICHE<br>
 * Variable: WPAG-CONT-SPESE-MEDICHE from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContSpeseMediche extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContSpeseMediche() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_SPESE_MEDICHE;
    }

    public void setWpagContSpeseMediche(AfDecimal wpagContSpeseMediche) {
        writeDecimalAsPacked(Pos.WPAG_CONT_SPESE_MEDICHE, wpagContSpeseMediche.copy());
    }

    public void setWpagContSpeseMedicheFormatted(String wpagContSpeseMediche) {
        setWpagContSpeseMediche(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_SPESE_MEDICHE + Len.Fract.WPAG_CONT_SPESE_MEDICHE, Len.Fract.WPAG_CONT_SPESE_MEDICHE, wpagContSpeseMediche));
    }

    public void setWpagContSpeseMedicheFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_SPESE_MEDICHE, Pos.WPAG_CONT_SPESE_MEDICHE);
    }

    /**Original name: WPAG-CONT-SPESE-MEDICHE<br>*/
    public AfDecimal getWpagContSpeseMediche() {
        return readPackedAsDecimal(Pos.WPAG_CONT_SPESE_MEDICHE, Len.Int.WPAG_CONT_SPESE_MEDICHE, Len.Fract.WPAG_CONT_SPESE_MEDICHE);
    }

    public byte[] getWpagContSpeseMedicheAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_SPESE_MEDICHE, Pos.WPAG_CONT_SPESE_MEDICHE);
        return buffer;
    }

    public void initWpagContSpeseMedicheSpaces() {
        fill(Pos.WPAG_CONT_SPESE_MEDICHE, Len.WPAG_CONT_SPESE_MEDICHE, Types.SPACE_CHAR);
    }

    public void setWpagContSpeseMedicheNull(String wpagContSpeseMedicheNull) {
        writeString(Pos.WPAG_CONT_SPESE_MEDICHE_NULL, wpagContSpeseMedicheNull, Len.WPAG_CONT_SPESE_MEDICHE_NULL);
    }

    /**Original name: WPAG-CONT-SPESE-MEDICHE-NULL<br>*/
    public String getWpagContSpeseMedicheNull() {
        return readString(Pos.WPAG_CONT_SPESE_MEDICHE_NULL, Len.WPAG_CONT_SPESE_MEDICHE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_SPESE_MEDICHE = 1;
        public static final int WPAG_CONT_SPESE_MEDICHE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_SPESE_MEDICHE = 8;
        public static final int WPAG_CONT_SPESE_MEDICHE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_SPESE_MEDICHE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_SPESE_MEDICHE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
