package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-ID-RAPP-RETE<br>
 * Variable: WTDR-ID-RAPP-RETE from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrIdRappRete extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrIdRappRete() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_ID_RAPP_RETE;
    }

    public void setWtdrIdRappRete(int wtdrIdRappRete) {
        writeIntAsPacked(Pos.WTDR_ID_RAPP_RETE, wtdrIdRappRete, Len.Int.WTDR_ID_RAPP_RETE);
    }

    /**Original name: WTDR-ID-RAPP-RETE<br>*/
    public int getWtdrIdRappRete() {
        return readPackedAsInt(Pos.WTDR_ID_RAPP_RETE, Len.Int.WTDR_ID_RAPP_RETE);
    }

    public void setWtdrIdRappReteNull(String wtdrIdRappReteNull) {
        writeString(Pos.WTDR_ID_RAPP_RETE_NULL, wtdrIdRappReteNull, Len.WTDR_ID_RAPP_RETE_NULL);
    }

    /**Original name: WTDR-ID-RAPP-RETE-NULL<br>*/
    public String getWtdrIdRappReteNull() {
        return readString(Pos.WTDR_ID_RAPP_RETE_NULL, Len.WTDR_ID_RAPP_RETE_NULL);
    }

    public String getWtdrIdRappReteNullFormatted() {
        return Functions.padBlanks(getWtdrIdRappReteNull(), Len.WTDR_ID_RAPP_RETE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_ID_RAPP_RETE = 1;
        public static final int WTDR_ID_RAPP_RETE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_ID_RAPP_RETE = 5;
        public static final int WTDR_ID_RAPP_RETE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_ID_RAPP_RETE = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
