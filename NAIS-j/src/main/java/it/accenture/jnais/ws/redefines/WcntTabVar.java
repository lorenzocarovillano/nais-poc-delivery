package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WCNT-TAB-VAR<br>
 * Variable: WCNT-TAB-VAR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WcntTabVar extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_VAR_CONT_MAXOCCURS = 500;

    //==== CONSTRUCTORS ====
    public WcntTabVar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WCNT_TAB_VAR;
    }

    public String getWcntTabVarFormatted() {
        return readFixedString(Pos.WCNT_TAB_VAR, Len.WCNT_TAB_VAR);
    }

    public void setWcntTabVarBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WCNT_TAB_VAR, Pos.WCNT_TAB_VAR);
    }

    public byte[] getWcntTabVarBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WCNT_TAB_VAR, Pos.WCNT_TAB_VAR);
        return buffer;
    }

    public void setWcntCodVarCont(int wcntCodVarContIdx, String wcntCodVarCont) {
        int position = Pos.wcntCodVarCont(wcntCodVarContIdx - 1);
        writeString(position, wcntCodVarCont, Len.COD_VAR_CONT);
    }

    /**Original name: WCNT-COD-VAR-CONT<br>*/
    public String getCodVarCont(int codVarContIdx) {
        int position = Pos.wcntCodVarCont(codVarContIdx - 1);
        return readString(position, Len.COD_VAR_CONT);
    }

    public void setWcntTpDatoCont(int wcntTpDatoContIdx, char wcntTpDatoCont) {
        int position = Pos.wcntTpDatoCont(wcntTpDatoContIdx - 1);
        writeChar(position, wcntTpDatoCont);
    }

    public void setWcntTpDatoContFormatted(int wcntTpDatoContIdx, String wcntTpDatoCont) {
        setWcntTpDatoCont(wcntTpDatoContIdx, Functions.charAt(wcntTpDatoCont, Types.CHAR_SIZE));
    }

    /**Original name: WCNT-TP-DATO-CONT<br>*/
    public char getWcntTpDatoCont(int wcntTpDatoContIdx) {
        int position = Pos.wcntTpDatoCont(wcntTpDatoContIdx - 1);
        return readChar(position);
    }

    public void setWcntValImpCont(int wcntValImpContIdx, AfDecimal wcntValImpCont) {
        int position = Pos.wcntValImpCont(wcntValImpContIdx - 1);
        writeDecimal(position, wcntValImpCont.copy());
    }

    /**Original name: WCNT-VAL-IMP-CONT<br>*/
    public AfDecimal getValImpCont(int valImpContIdx) {
        int position = Pos.wcntValImpCont(valImpContIdx - 1);
        return readDecimal(position, Len.Int.VAL_IMP_CONT, Len.Fract.VAL_IMP_CONT);
    }

    public void setWcntValPercCont(int wcntValPercContIdx, AfDecimal wcntValPercCont) {
        int position = Pos.wcntValPercCont(wcntValPercContIdx - 1);
        writeDecimal(position, wcntValPercCont.copy(), SignType.NO_SIGN);
    }

    /**Original name: WCNT-VAL-PERC-CONT<br>*/
    public AfDecimal getValPercCont(int valPercContIdx) {
        int position = Pos.wcntValPercCont(valPercContIdx - 1);
        return readDecimal(position, Len.Int.VAL_PERC_CONT, Len.Fract.VAL_PERC_CONT, SignType.NO_SIGN);
    }

    public void setWcntValStrCont(int wcntValStrContIdx, String wcntValStrCont) {
        int position = Pos.wcntValStrCont(wcntValStrContIdx - 1);
        writeString(position, wcntValStrCont, Len.VAL_STR_CONT);
    }

    /**Original name: WCNT-VAL-STR-CONT<br>*/
    public String getValStrCont(int valStrContIdx) {
        int position = Pos.wcntValStrCont(valStrContIdx - 1);
        return readString(position, Len.VAL_STR_CONT);
    }

    public void setWcntTpLivello(int wcntTpLivelloIdx, char wcntTpLivello) {
        int position = Pos.wcntTpLivello(wcntTpLivelloIdx - 1);
        writeChar(position, wcntTpLivello);
    }

    /**Original name: WCNT-TP-LIVELLO<br>*/
    public char getTpLivello(int tpLivelloIdx) {
        int position = Pos.wcntTpLivello(tpLivelloIdx - 1);
        return readChar(position);
    }

    public void setWcntCodLivello(int wcntCodLivelloIdx, String wcntCodLivello) {
        int position = Pos.wcntCodLivello(wcntCodLivelloIdx - 1);
        writeString(position, wcntCodLivello, Len.COD_LIVELLO);
    }

    /**Original name: WCNT-COD-LIVELLO<br>*/
    public String getWcntCodLivello(int wcntCodLivelloIdx) {
        int position = Pos.wcntCodLivello(wcntCodLivelloIdx - 1);
        return readString(position, Len.COD_LIVELLO);
    }

    public void setWcntIdLivello(int wcntIdLivelloIdx, int wcntIdLivello) {
        int position = Pos.wcntIdLivello(wcntIdLivelloIdx - 1);
        writeInt(position, wcntIdLivello, Len.Int.WCNT_ID_LIVELLO, SignType.NO_SIGN);
    }

    public void setWcntIdLivelloFormatted(int wcntIdLivelloIdx, String wcntIdLivello) {
        int position = Pos.wcntIdLivello(wcntIdLivelloIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(wcntIdLivello, Len.ID_LIVELLO), Len.ID_LIVELLO);
    }

    /**Original name: WCNT-ID-LIVELLO<br>*/
    public int getIdLivello(int idLivelloIdx) {
        int position = Pos.wcntIdLivello(idLivelloIdx - 1);
        return readNumDispUnsignedInt(position, Len.ID_LIVELLO);
    }

    public String getIdLivelloFormatted(int idLivelloIdx) {
        int position = Pos.wcntIdLivello(idLivelloIdx - 1);
        return readFixedString(position, Len.ID_LIVELLO);
    }

    public void setWcntRestoTabVar(String wcntRestoTabVar) {
        writeString(Pos.RESTO_TAB_VAR, wcntRestoTabVar, Len.WCNT_RESTO_TAB_VAR);
    }

    /**Original name: WCNT-RESTO-TAB-VAR<br>*/
    public String getWcntRestoTabVar() {
        return readString(Pos.RESTO_TAB_VAR, Len.WCNT_RESTO_TAB_VAR);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WCNT_TAB_VAR = 1;
        public static final int WCNT_TAB_VAR_R = 1;
        public static final int FLR1 = WCNT_TAB_VAR_R;
        public static final int RESTO_TAB_VAR = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wcntTabVarCont(int idx) {
            return WCNT_TAB_VAR + idx * Len.TAB_VAR_CONT;
        }

        public static int wcntAreaVarCont(int idx) {
            return wcntTabVarCont(idx);
        }

        public static int wcntCodVarCont(int idx) {
            return wcntAreaVarCont(idx);
        }

        public static int wcntTpDatoCont(int idx) {
            return wcntCodVarCont(idx) + Len.COD_VAR_CONT;
        }

        public static int wcntValImpCont(int idx) {
            return wcntTpDatoCont(idx) + Len.TP_DATO_CONT;
        }

        public static int wcntValPercCont(int idx) {
            return wcntValImpCont(idx) + Len.VAL_IMP_CONT;
        }

        public static int wcntValStrCont(int idx) {
            return wcntValPercCont(idx) + Len.VAL_PERC_CONT;
        }

        public static int wcntTpLivello(int idx) {
            return wcntValStrCont(idx) + Len.VAL_STR_CONT;
        }

        public static int wcntCodLivello(int idx) {
            return wcntTpLivello(idx) + Len.TP_LIVELLO;
        }

        public static int wcntIdLivello(int idx) {
            return wcntCodLivello(idx) + Len.COD_LIVELLO;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_VAR_CONT = 12;
        public static final int TP_DATO_CONT = 1;
        public static final int VAL_IMP_CONT = 18;
        public static final int VAL_PERC_CONT = 14;
        public static final int VAL_STR_CONT = 12;
        public static final int TP_LIVELLO = 1;
        public static final int COD_LIVELLO = 12;
        public static final int ID_LIVELLO = 9;
        public static final int AREA_VAR_CONT = COD_VAR_CONT + TP_DATO_CONT + VAL_IMP_CONT + VAL_PERC_CONT + VAL_STR_CONT + TP_LIVELLO + COD_LIVELLO + ID_LIVELLO;
        public static final int TAB_VAR_CONT = AREA_VAR_CONT;
        public static final int FLR1 = 79;
        public static final int WCNT_TAB_VAR = WcntTabVar.TAB_VAR_CONT_MAXOCCURS * TAB_VAR_CONT;
        public static final int WCNT_RESTO_TAB_VAR = 39421;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAL_IMP_CONT = 11;
            public static final int VAL_PERC_CONT = 5;
            public static final int WCNT_ID_LIVELLO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAL_IMP_CONT = 7;
            public static final int VAL_PERC_CONT = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
