package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-MONT-DAL2007-EFFLQ<br>
 * Variable: WDFL-MONT-DAL2007-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflMontDal2007Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflMontDal2007Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_MONT_DAL2007_EFFLQ;
    }

    public void setWdflMontDal2007Efflq(AfDecimal wdflMontDal2007Efflq) {
        writeDecimalAsPacked(Pos.WDFL_MONT_DAL2007_EFFLQ, wdflMontDal2007Efflq.copy());
    }

    public void setWdflMontDal2007EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_MONT_DAL2007_EFFLQ, Pos.WDFL_MONT_DAL2007_EFFLQ);
    }

    /**Original name: WDFL-MONT-DAL2007-EFFLQ<br>*/
    public AfDecimal getWdflMontDal2007Efflq() {
        return readPackedAsDecimal(Pos.WDFL_MONT_DAL2007_EFFLQ, Len.Int.WDFL_MONT_DAL2007_EFFLQ, Len.Fract.WDFL_MONT_DAL2007_EFFLQ);
    }

    public byte[] getWdflMontDal2007EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_MONT_DAL2007_EFFLQ, Pos.WDFL_MONT_DAL2007_EFFLQ);
        return buffer;
    }

    public void setWdflMontDal2007EfflqNull(String wdflMontDal2007EfflqNull) {
        writeString(Pos.WDFL_MONT_DAL2007_EFFLQ_NULL, wdflMontDal2007EfflqNull, Len.WDFL_MONT_DAL2007_EFFLQ_NULL);
    }

    /**Original name: WDFL-MONT-DAL2007-EFFLQ-NULL<br>*/
    public String getWdflMontDal2007EfflqNull() {
        return readString(Pos.WDFL_MONT_DAL2007_EFFLQ_NULL, Len.WDFL_MONT_DAL2007_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_DAL2007_EFFLQ = 1;
        public static final int WDFL_MONT_DAL2007_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_DAL2007_EFFLQ = 8;
        public static final int WDFL_MONT_DAL2007_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_DAL2007_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_DAL2007_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
