package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-ID-ADES<br>
 * Variable: WPMO-ID-ADES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoIdAdes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoIdAdes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_ID_ADES;
    }

    public void setWpmoIdAdes(int wpmoIdAdes) {
        writeIntAsPacked(Pos.WPMO_ID_ADES, wpmoIdAdes, Len.Int.WPMO_ID_ADES);
    }

    public void setWpmoIdAdesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_ID_ADES, Pos.WPMO_ID_ADES);
    }

    /**Original name: WPMO-ID-ADES<br>*/
    public int getWpmoIdAdes() {
        return readPackedAsInt(Pos.WPMO_ID_ADES, Len.Int.WPMO_ID_ADES);
    }

    public String getWpmoIdAdesFormatted() {
        return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.PACKED)).format(getWpmoIdAdes()).toString();
    }

    public byte[] getWpmoIdAdesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_ID_ADES, Pos.WPMO_ID_ADES);
        return buffer;
    }

    public void initWpmoIdAdesSpaces() {
        fill(Pos.WPMO_ID_ADES, Len.WPMO_ID_ADES, Types.SPACE_CHAR);
    }

    public void setWpmoIdAdesNull(String wpmoIdAdesNull) {
        writeString(Pos.WPMO_ID_ADES_NULL, wpmoIdAdesNull, Len.WPMO_ID_ADES_NULL);
    }

    /**Original name: WPMO-ID-ADES-NULL<br>*/
    public String getWpmoIdAdesNull() {
        return readString(Pos.WPMO_ID_ADES_NULL, Len.WPMO_ID_ADES_NULL);
    }

    public String getWpmoIdAdesNullFormatted() {
        return Functions.padBlanks(getWpmoIdAdesNull(), Len.WPMO_ID_ADES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_ID_ADES = 1;
        public static final int WPMO_ID_ADES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_ID_ADES = 5;
        public static final int WPMO_ID_ADES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
