package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-REMUN-ASS<br>
 * Variable: WB03-REMUN-ASS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03RemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03RemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_REMUN_ASS;
    }

    public void setWb03RemunAss(AfDecimal wb03RemunAss) {
        writeDecimalAsPacked(Pos.WB03_REMUN_ASS, wb03RemunAss.copy());
    }

    public void setWb03RemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_REMUN_ASS, Pos.WB03_REMUN_ASS);
    }

    /**Original name: WB03-REMUN-ASS<br>*/
    public AfDecimal getWb03RemunAss() {
        return readPackedAsDecimal(Pos.WB03_REMUN_ASS, Len.Int.WB03_REMUN_ASS, Len.Fract.WB03_REMUN_ASS);
    }

    public byte[] getWb03RemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_REMUN_ASS, Pos.WB03_REMUN_ASS);
        return buffer;
    }

    public void setWb03RemunAssNull(String wb03RemunAssNull) {
        writeString(Pos.WB03_REMUN_ASS_NULL, wb03RemunAssNull, Len.WB03_REMUN_ASS_NULL);
    }

    /**Original name: WB03-REMUN-ASS-NULL<br>*/
    public String getWb03RemunAssNull() {
        return readString(Pos.WB03_REMUN_ASS_NULL, Len.WB03_REMUN_ASS_NULL);
    }

    public String getWb03RemunAssNullFormatted() {
        return Functions.padBlanks(getWb03RemunAssNull(), Len.WB03_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_REMUN_ASS = 1;
        public static final int WB03_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_REMUN_ASS = 8;
        public static final int WB03_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
