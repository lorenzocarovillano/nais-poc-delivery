package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-SOPR-SPO<br>
 * Variable: TDR-TOT-SOPR-SPO from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_SOPR_SPO;
    }

    public void setTdrTotSoprSpo(AfDecimal tdrTotSoprSpo) {
        writeDecimalAsPacked(Pos.TDR_TOT_SOPR_SPO, tdrTotSoprSpo.copy());
    }

    public void setTdrTotSoprSpoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_SOPR_SPO, Pos.TDR_TOT_SOPR_SPO);
    }

    /**Original name: TDR-TOT-SOPR-SPO<br>*/
    public AfDecimal getTdrTotSoprSpo() {
        return readPackedAsDecimal(Pos.TDR_TOT_SOPR_SPO, Len.Int.TDR_TOT_SOPR_SPO, Len.Fract.TDR_TOT_SOPR_SPO);
    }

    public byte[] getTdrTotSoprSpoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_SOPR_SPO, Pos.TDR_TOT_SOPR_SPO);
        return buffer;
    }

    public void setTdrTotSoprSpoNull(String tdrTotSoprSpoNull) {
        writeString(Pos.TDR_TOT_SOPR_SPO_NULL, tdrTotSoprSpoNull, Len.TDR_TOT_SOPR_SPO_NULL);
    }

    /**Original name: TDR-TOT-SOPR-SPO-NULL<br>*/
    public String getTdrTotSoprSpoNull() {
        return readString(Pos.TDR_TOT_SOPR_SPO_NULL, Len.TDR_TOT_SOPR_SPO_NULL);
    }

    public String getTdrTotSoprSpoNullFormatted() {
        return Functions.padBlanks(getTdrTotSoprSpoNull(), Len.TDR_TOT_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SOPR_SPO = 1;
        public static final int TDR_TOT_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SOPR_SPO = 8;
        public static final int TDR_TOT_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
