package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.WsDataCompRstX;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0113<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0113Data {

    //==== PROPERTIES ====
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    //Original name: WS-DT-RST
    private WsDtRst wsDtRst = new WsDtRst();
    //Original name: WS-DT-RST-9
    private String wsDtRst9 = "00000000";
    //Original name: WS-DT-RST-AP
    private String wsDtRstAp = "00000000";
    //Original name: WS-DATA-COMPETENZA
    private long wsDataCompetenza = DefaultValues.LONG_VAL;
    //Original name: WS-DATA-COMP-RST-X
    private WsDataCompRstX wsDataCompRstX = new WsDataCompRstX();
    //Original name: WS-DT-9999MMGG
    private WsDt9999mmgg wsDt9999mmgg = new WsDt9999mmgg();
    //Original name: RIS-DI-TRCH
    private RisDiTrchIdbsrst0 risDiTrch = new RisDiTrchIdbsrst0();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ADE
    private short ixTabAde = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWsDtRst9(int wsDtRst9) {
        this.wsDtRst9 = NumericDisplay.asString(wsDtRst9, Len.WS_DT_RST9);
    }

    public void setWsDtRst9FromBuffer(byte[] buffer) {
        wsDtRst9 = MarshalByte.readFixedString(buffer, 1, Len.WS_DT_RST9);
    }

    public int getWsDtRst9() {
        return NumericDisplay.asInt(this.wsDtRst9);
    }

    public String getWsDtRst9Formatted() {
        return this.wsDtRst9;
    }

    public void setWsDtRstAp(int wsDtRstAp) {
        this.wsDtRstAp = NumericDisplay.asString(wsDtRstAp, Len.WS_DT_RST_AP);
    }

    public int getWsDtRstAp() {
        return NumericDisplay.asInt(this.wsDtRstAp);
    }

    public String getWsDtRstApFormatted() {
        return this.wsDtRstAp;
    }

    public void setWsDataCompetenza(long wsDataCompetenza) {
        this.wsDataCompetenza = wsDataCompetenza;
    }

    public long getWsDataCompetenza() {
        return this.wsDataCompetenza;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabAde(short ixTabAde) {
        this.ixTabAde = ixTabAde;
    }

    public short getIxTabAde() {
        return this.ixTabAde;
    }

    public RisDiTrchIdbsrst0 getRisDiTrch() {
        return risDiTrch;
    }

    public WsDataCompRstX getWsDataCompRstX() {
        return wsDataCompRstX;
    }

    public WsDt9999mmgg getWsDt9999mmgg() {
        return wsDt9999mmgg;
    }

    public WsDtRst getWsDtRst() {
        return wsDtRst;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_DT_RST9 = 8;
        public static final int WS_DT_RST_AP = 8;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
