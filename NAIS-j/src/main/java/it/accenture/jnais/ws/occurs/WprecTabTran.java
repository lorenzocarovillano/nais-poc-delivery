package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPREC-TAB-TRAN<br>
 * Variables: WPREC-TAB-TRAN from program LOAS0310<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WprecTabTran {

    //==== PROPERTIES ====
    //Original name: WPREC-ID-TRCH-DI-GAR
    private int idTrchDiGar = DefaultValues.INT_VAL;
    //Original name: WPREC-PRSTZ-ULT
    private AfDecimal prstzUlt = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    public void setIdTrchDiGar(int idTrchDiGar) {
        this.idTrchDiGar = idTrchDiGar;
    }

    public int getIdTrchDiGar() {
        return this.idTrchDiGar;
    }

    public void setPrstzUlt(AfDecimal prstzUlt) {
        this.prstzUlt.assign(prstzUlt);
    }

    public AfDecimal getPrstzUlt() {
        return this.prstzUlt.copy();
    }
}
