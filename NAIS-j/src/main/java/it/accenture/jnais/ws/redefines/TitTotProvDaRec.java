package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-PROV-DA-REC<br>
 * Variable: TIT-TOT-PROV-DA-REC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotProvDaRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotProvDaRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_PROV_DA_REC;
    }

    public void setTitTotProvDaRec(AfDecimal titTotProvDaRec) {
        writeDecimalAsPacked(Pos.TIT_TOT_PROV_DA_REC, titTotProvDaRec.copy());
    }

    public void setTitTotProvDaRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_PROV_DA_REC, Pos.TIT_TOT_PROV_DA_REC);
    }

    /**Original name: TIT-TOT-PROV-DA-REC<br>*/
    public AfDecimal getTitTotProvDaRec() {
        return readPackedAsDecimal(Pos.TIT_TOT_PROV_DA_REC, Len.Int.TIT_TOT_PROV_DA_REC, Len.Fract.TIT_TOT_PROV_DA_REC);
    }

    public byte[] getTitTotProvDaRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_PROV_DA_REC, Pos.TIT_TOT_PROV_DA_REC);
        return buffer;
    }

    public void setTitTotProvDaRecNull(String titTotProvDaRecNull) {
        writeString(Pos.TIT_TOT_PROV_DA_REC_NULL, titTotProvDaRecNull, Len.TIT_TOT_PROV_DA_REC_NULL);
    }

    /**Original name: TIT-TOT-PROV-DA-REC-NULL<br>*/
    public String getTitTotProvDaRecNull() {
        return readString(Pos.TIT_TOT_PROV_DA_REC_NULL, Len.TIT_TOT_PROV_DA_REC_NULL);
    }

    public String getTitTotProvDaRecNullFormatted() {
        return Functions.padBlanks(getTitTotProvDaRecNull(), Len.TIT_TOT_PROV_DA_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PROV_DA_REC = 1;
        public static final int TIT_TOT_PROV_DA_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PROV_DA_REC = 8;
        public static final int TIT_TOT_PROV_DA_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PROV_DA_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PROV_DA_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
