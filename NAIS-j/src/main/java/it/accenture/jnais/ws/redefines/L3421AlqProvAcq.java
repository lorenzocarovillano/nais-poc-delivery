package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-ALQ-PROV-ACQ<br>
 * Variable: L3421-ALQ-PROV-ACQ from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421AlqProvAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421AlqProvAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_ALQ_PROV_ACQ;
    }

    public void setL3421AlqProvAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_ALQ_PROV_ACQ, Pos.L3421_ALQ_PROV_ACQ);
    }

    /**Original name: L3421-ALQ-PROV-ACQ<br>*/
    public AfDecimal getL3421AlqProvAcq() {
        return readPackedAsDecimal(Pos.L3421_ALQ_PROV_ACQ, Len.Int.L3421_ALQ_PROV_ACQ, Len.Fract.L3421_ALQ_PROV_ACQ);
    }

    public byte[] getL3421AlqProvAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_ALQ_PROV_ACQ, Pos.L3421_ALQ_PROV_ACQ);
        return buffer;
    }

    /**Original name: L3421-ALQ-PROV-ACQ-NULL<br>*/
    public String getL3421AlqProvAcqNull() {
        return readString(Pos.L3421_ALQ_PROV_ACQ_NULL, Len.L3421_ALQ_PROV_ACQ_NULL);
    }

    public String getL3421AlqProvAcqNullFormatted() {
        return Functions.padBlanks(getL3421AlqProvAcqNull(), Len.L3421_ALQ_PROV_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_ALQ_PROV_ACQ = 1;
        public static final int L3421_ALQ_PROV_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_ALQ_PROV_ACQ = 4;
        public static final int L3421_ALQ_PROV_ACQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_ALQ_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_ALQ_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
