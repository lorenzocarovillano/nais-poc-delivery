package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-CPT-MIN-SCAD<br>
 * Variable: TGA-CPT-MIN-SCAD from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaCptMinScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaCptMinScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_CPT_MIN_SCAD;
    }

    public void setTgaCptMinScad(AfDecimal tgaCptMinScad) {
        writeDecimalAsPacked(Pos.TGA_CPT_MIN_SCAD, tgaCptMinScad.copy());
    }

    public void setTgaCptMinScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_CPT_MIN_SCAD, Pos.TGA_CPT_MIN_SCAD);
    }

    /**Original name: TGA-CPT-MIN-SCAD<br>*/
    public AfDecimal getTgaCptMinScad() {
        return readPackedAsDecimal(Pos.TGA_CPT_MIN_SCAD, Len.Int.TGA_CPT_MIN_SCAD, Len.Fract.TGA_CPT_MIN_SCAD);
    }

    public byte[] getTgaCptMinScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_CPT_MIN_SCAD, Pos.TGA_CPT_MIN_SCAD);
        return buffer;
    }

    public void setTgaCptMinScadNull(String tgaCptMinScadNull) {
        writeString(Pos.TGA_CPT_MIN_SCAD_NULL, tgaCptMinScadNull, Len.TGA_CPT_MIN_SCAD_NULL);
    }

    /**Original name: TGA-CPT-MIN-SCAD-NULL<br>*/
    public String getTgaCptMinScadNull() {
        return readString(Pos.TGA_CPT_MIN_SCAD_NULL, Len.TGA_CPT_MIN_SCAD_NULL);
    }

    public String getTgaCptMinScadNullFormatted() {
        return Functions.padBlanks(getTgaCptMinScadNull(), Len.TGA_CPT_MIN_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_CPT_MIN_SCAD = 1;
        public static final int TGA_CPT_MIN_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_CPT_MIN_SCAD = 8;
        public static final int TGA_CPT_MIN_SCAD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_CPT_MIN_SCAD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_CPT_MIN_SCAD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
