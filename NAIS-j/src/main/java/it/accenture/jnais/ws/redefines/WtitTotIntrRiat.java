package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-INTR-RIAT<br>
 * Variable: WTIT-TOT-INTR-RIAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotIntrRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotIntrRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_INTR_RIAT;
    }

    public void setWtitTotIntrRiat(AfDecimal wtitTotIntrRiat) {
        writeDecimalAsPacked(Pos.WTIT_TOT_INTR_RIAT, wtitTotIntrRiat.copy());
    }

    public void setWtitTotIntrRiatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_INTR_RIAT, Pos.WTIT_TOT_INTR_RIAT);
    }

    /**Original name: WTIT-TOT-INTR-RIAT<br>*/
    public AfDecimal getWtitTotIntrRiat() {
        return readPackedAsDecimal(Pos.WTIT_TOT_INTR_RIAT, Len.Int.WTIT_TOT_INTR_RIAT, Len.Fract.WTIT_TOT_INTR_RIAT);
    }

    public byte[] getWtitTotIntrRiatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_INTR_RIAT, Pos.WTIT_TOT_INTR_RIAT);
        return buffer;
    }

    public void initWtitTotIntrRiatSpaces() {
        fill(Pos.WTIT_TOT_INTR_RIAT, Len.WTIT_TOT_INTR_RIAT, Types.SPACE_CHAR);
    }

    public void setWtitTotIntrRiatNull(String wtitTotIntrRiatNull) {
        writeString(Pos.WTIT_TOT_INTR_RIAT_NULL, wtitTotIntrRiatNull, Len.WTIT_TOT_INTR_RIAT_NULL);
    }

    /**Original name: WTIT-TOT-INTR-RIAT-NULL<br>*/
    public String getWtitTotIntrRiatNull() {
        return readString(Pos.WTIT_TOT_INTR_RIAT_NULL, Len.WTIT_TOT_INTR_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_INTR_RIAT = 1;
        public static final int WTIT_TOT_INTR_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_INTR_RIAT = 8;
        public static final int WTIT_TOT_INTR_RIAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_INTR_RIAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
