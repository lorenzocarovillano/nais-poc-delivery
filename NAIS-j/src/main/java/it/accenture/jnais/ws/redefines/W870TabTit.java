package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: W870-TAB-TIT<br>
 * Variable: W870-TAB-TIT from program LOAS0870<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class W870TabTit extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int DATI_TIT_MAXOCCURS = 12;
    public static final int DATI_TGA_MAXOCCURS = 300;

    //==== CONSTRUCTORS ====
    public W870TabTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W870_TAB_TIT;
    }

    public String getW870TabTitFormatted() {
        return readFixedString(Pos.W870_TAB_TIT, Len.W870_TAB_TIT);
    }

    public void setW870TabTitBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.W870_TAB_TIT, Pos.W870_TAB_TIT);
    }

    public byte[] getW870TabTitBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W870_TAB_TIT, Pos.W870_TAB_TIT);
        return buffer;
    }

    public void setIdTrchDiGar(int idTrchDiGarIdx, int idTrchDiGar) {
        int position = Pos.w870IdTrchDiGar(idTrchDiGarIdx - 1);
        writeIntAsPacked(position, idTrchDiGar, Len.Int.ID_TRCH_DI_GAR);
    }

    /**Original name: W870-ID-TRCH-DI-GAR<br>*/
    public int getIdTrchDiGar(int idTrchDiGarIdx) {
        int position = Pos.w870IdTrchDiGar(idTrchDiGarIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_TRCH_DI_GAR);
    }

    public void setTitNumMaxEle(int titNumMaxEleIdx, short titNumMaxEle) {
        int position = Pos.w870TitNumMaxEle(titNumMaxEleIdx - 1);
        writeShort(position, titNumMaxEle, Len.Int.TIT_NUM_MAX_ELE);
    }

    /**Original name: W870-TIT-NUM-MAX-ELE<br>*/
    public short getTitNumMaxEle(int titNumMaxEleIdx) {
        int position = Pos.w870TitNumMaxEle(titNumMaxEleIdx - 1);
        return readNumDispShort(position, Len.TIT_NUM_MAX_ELE);
    }

    public void setIdTitCont(int idTitContIdx1, int idTitContIdx2, int idTitCont) {
        int position = Pos.w870IdTitCont(idTitContIdx1 - 1, idTitContIdx2 - 1);
        writeIntAsPacked(position, idTitCont, Len.Int.ID_TIT_CONT);
    }

    /**Original name: W870-ID-TIT-CONT<br>*/
    public int getIdTitCont(int idTitContIdx1, int idTitContIdx2) {
        int position = Pos.w870IdTitCont(idTitContIdx1 - 1, idTitContIdx2 - 1);
        return readPackedAsInt(position, Len.Int.ID_TIT_CONT);
    }

    public void setDtVlt(int dtVltIdx1, int dtVltIdx2, int dtVlt) {
        int position = Pos.w870DtVlt(dtVltIdx1 - 1, dtVltIdx2 - 1);
        writeIntAsPacked(position, dtVlt, Len.Int.DT_VLT);
    }

    /**Original name: W870-DT-VLT<br>*/
    public int getDtVlt(int dtVltIdx1, int dtVltIdx2) {
        int position = Pos.w870DtVlt(dtVltIdx1 - 1, dtVltIdx2 - 1);
        return readPackedAsInt(position, Len.Int.DT_VLT);
    }

    public void setDtVltNull(int dtVltNullIdx1, int dtVltNullIdx2, String dtVltNull) {
        int position = Pos.w870DtVltNull(dtVltNullIdx1 - 1, dtVltNullIdx2 - 1);
        writeString(position, dtVltNull, Len.DT_VLT_NULL);
    }

    /**Original name: W870-DT-VLT-NULL<br>*/
    public String getDtVltNull(int dtVltNullIdx1, int dtVltNullIdx2) {
        int position = Pos.w870DtVltNull(dtVltNullIdx1 - 1, dtVltNullIdx2 - 1);
        return readString(position, Len.DT_VLT_NULL);
    }

    public String getDtVltNullFormatted(int dtVltNullIdx1, int dtVltNullIdx2) {
        return Functions.padBlanks(getDtVltNull(dtVltNullIdx1, dtVltNullIdx2), Len.DT_VLT_NULL);
    }

    public void setDtIniEff(int dtIniEffIdx1, int dtIniEffIdx2, int dtIniEff) {
        int position = Pos.w870DtIniEff(dtIniEffIdx1 - 1, dtIniEffIdx2 - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: W870-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx1, int dtIniEffIdx2) {
        int position = Pos.w870DtIniEff(dtIniEffIdx1 - 1, dtIniEffIdx2 - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setTpStatTit(int tpStatTitIdx1, int tpStatTitIdx2, String tpStatTit) {
        int position = Pos.w870TpStatTit(tpStatTitIdx1 - 1, tpStatTitIdx2 - 1);
        writeString(position, tpStatTit, Len.TP_STAT_TIT);
    }

    /**Original name: W870-TP-STAT-TIT<br>*/
    public String getTpStatTit(int tpStatTitIdx1, int tpStatTitIdx2) {
        int position = Pos.w870TpStatTit(tpStatTitIdx1 - 1, tpStatTitIdx2 - 1);
        return readString(position, Len.TP_STAT_TIT);
    }

    public void setW870RestoTabTit(String w870RestoTabTit) {
        writeString(Pos.RESTO_TAB_TIT, w870RestoTabTit, Len.W870_RESTO_TAB_TIT);
    }

    /**Original name: W870-RESTO-TAB-TIT<br>*/
    public String getW870RestoTabTit() {
        return readString(Pos.RESTO_TAB_TIT, Len.W870_RESTO_TAB_TIT);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W870_TAB_TIT = 1;
        public static final int W870_TAB_TIT_R = 1;
        public static final int FLR1 = W870_TAB_TIT_R;
        public static final int RESTO_TAB_TIT = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int w870DatiTga(int idx) {
            return W870_TAB_TIT + idx * Len.DATI_TGA;
        }

        public static int w870IdTrchDiGar(int idx) {
            return w870DatiTga(idx);
        }

        public static int w870TitNumMaxEle(int idx) {
            return w870IdTrchDiGar(idx) + Len.ID_TRCH_DI_GAR;
        }

        public static int w870DatiTit(int idx1, int idx2) {
            return w870TitNumMaxEle(idx1) + Len.TIT_NUM_MAX_ELE + idx2 * Len.DATI_TIT;
        }

        public static int w870IdTitCont(int idx1, int idx2) {
            return w870DatiTit(idx1, idx2);
        }

        public static int w870DtVlt(int idx1, int idx2) {
            return w870IdTitCont(idx1, idx2) + Len.ID_TIT_CONT;
        }

        public static int w870DtVltNull(int idx1, int idx2) {
            return w870DtVlt(idx1, idx2);
        }

        public static int w870DtIniEff(int idx1, int idx2) {
            return w870DtVlt(idx1, idx2) + Len.DT_VLT;
        }

        public static int w870TpStatTit(int idx1, int idx2) {
            return w870DtIniEff(idx1, idx2) + Len.DT_INI_EFF;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_TRCH_DI_GAR = 5;
        public static final int TIT_NUM_MAX_ELE = 4;
        public static final int ID_TIT_CONT = 5;
        public static final int DT_VLT = 5;
        public static final int DT_INI_EFF = 5;
        public static final int TP_STAT_TIT = 2;
        public static final int DATI_TIT = ID_TIT_CONT + DT_VLT + DT_INI_EFF + TP_STAT_TIT;
        public static final int DATI_TGA = ID_TRCH_DI_GAR + TIT_NUM_MAX_ELE + W870TabTit.DATI_TIT_MAXOCCURS * DATI_TIT;
        public static final int FLR1 = 213;
        public static final int W870_TAB_TIT = W870TabTit.DATI_TGA_MAXOCCURS * DATI_TGA;
        public static final int DT_VLT_NULL = 5;
        public static final int W870_RESTO_TAB_TIT = 63687;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_TRCH_DI_GAR = 9;
            public static final int TIT_NUM_MAX_ELE = 4;
            public static final int ID_TIT_CONT = 9;
            public static final int DT_VLT = 8;
            public static final int DT_INI_EFF = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
