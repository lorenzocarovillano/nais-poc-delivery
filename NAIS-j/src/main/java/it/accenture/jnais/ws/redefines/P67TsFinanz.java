package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-TS-FINANZ<br>
 * Variable: P67-TS-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67TsFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67TsFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_TS_FINANZ;
    }

    public void setP67TsFinanz(AfDecimal p67TsFinanz) {
        writeDecimalAsPacked(Pos.P67_TS_FINANZ, p67TsFinanz.copy());
    }

    public void setP67TsFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_TS_FINANZ, Pos.P67_TS_FINANZ);
    }

    /**Original name: P67-TS-FINANZ<br>*/
    public AfDecimal getP67TsFinanz() {
        return readPackedAsDecimal(Pos.P67_TS_FINANZ, Len.Int.P67_TS_FINANZ, Len.Fract.P67_TS_FINANZ);
    }

    public byte[] getP67TsFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_TS_FINANZ, Pos.P67_TS_FINANZ);
        return buffer;
    }

    public void setP67TsFinanzNull(String p67TsFinanzNull) {
        writeString(Pos.P67_TS_FINANZ_NULL, p67TsFinanzNull, Len.P67_TS_FINANZ_NULL);
    }

    /**Original name: P67-TS-FINANZ-NULL<br>*/
    public String getP67TsFinanzNull() {
        return readString(Pos.P67_TS_FINANZ_NULL, Len.P67_TS_FINANZ_NULL);
    }

    public String getP67TsFinanzNullFormatted() {
        return Functions.padBlanks(getP67TsFinanzNull(), Len.P67_TS_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_TS_FINANZ = 1;
        public static final int P67_TS_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_TS_FINANZ = 8;
        public static final int P67_TS_FINANZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_TS_FINANZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_TS_FINANZ = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
