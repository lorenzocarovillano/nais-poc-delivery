package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-COD-SUBAGE<br>
 * Variable: B03-COD-SUBAGE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CodSubage extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CodSubage() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_COD_SUBAGE;
    }

    public void setB03CodSubage(int b03CodSubage) {
        writeIntAsPacked(Pos.B03_COD_SUBAGE, b03CodSubage, Len.Int.B03_COD_SUBAGE);
    }

    public void setB03CodSubageFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_COD_SUBAGE, Pos.B03_COD_SUBAGE);
    }

    /**Original name: B03-COD-SUBAGE<br>*/
    public int getB03CodSubage() {
        return readPackedAsInt(Pos.B03_COD_SUBAGE, Len.Int.B03_COD_SUBAGE);
    }

    public byte[] getB03CodSubageAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_COD_SUBAGE, Pos.B03_COD_SUBAGE);
        return buffer;
    }

    public void setB03CodSubageNull(String b03CodSubageNull) {
        writeString(Pos.B03_COD_SUBAGE_NULL, b03CodSubageNull, Len.B03_COD_SUBAGE_NULL);
    }

    /**Original name: B03-COD-SUBAGE-NULL<br>*/
    public String getB03CodSubageNull() {
        return readString(Pos.B03_COD_SUBAGE_NULL, Len.B03_COD_SUBAGE_NULL);
    }

    public String getB03CodSubageNullFormatted() {
        return Functions.padBlanks(getB03CodSubageNull(), Len.B03_COD_SUBAGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_COD_SUBAGE = 1;
        public static final int B03_COD_SUBAGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_COD_SUBAGE = 3;
        public static final int B03_COD_SUBAGE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_COD_SUBAGE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
