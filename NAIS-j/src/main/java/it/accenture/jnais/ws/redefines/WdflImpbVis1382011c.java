package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-VIS-1382011C<br>
 * Variable: WDFL-IMPB-VIS-1382011C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbVis1382011c extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbVis1382011c() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_VIS1382011C;
    }

    public void setWdflImpbVis1382011c(AfDecimal wdflImpbVis1382011c) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_VIS1382011C, wdflImpbVis1382011c.copy());
    }

    public void setWdflImpbVis1382011cFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_VIS1382011C, Pos.WDFL_IMPB_VIS1382011C);
    }

    /**Original name: WDFL-IMPB-VIS-1382011C<br>*/
    public AfDecimal getWdflImpbVis1382011c() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_VIS1382011C, Len.Int.WDFL_IMPB_VIS1382011C, Len.Fract.WDFL_IMPB_VIS1382011C);
    }

    public byte[] getWdflImpbVis1382011cAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_VIS1382011C, Pos.WDFL_IMPB_VIS1382011C);
        return buffer;
    }

    public void setWdflImpbVis1382011cNull(String wdflImpbVis1382011cNull) {
        writeString(Pos.WDFL_IMPB_VIS1382011C_NULL, wdflImpbVis1382011cNull, Len.WDFL_IMPB_VIS1382011C_NULL);
    }

    /**Original name: WDFL-IMPB-VIS-1382011C-NULL<br>*/
    public String getWdflImpbVis1382011cNull() {
        return readString(Pos.WDFL_IMPB_VIS1382011C_NULL, Len.WDFL_IMPB_VIS1382011C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_VIS1382011C = 1;
        public static final int WDFL_IMPB_VIS1382011C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_VIS1382011C = 8;
        public static final int WDFL_IMPB_VIS1382011C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_VIS1382011C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_VIS1382011C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
