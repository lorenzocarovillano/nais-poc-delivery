package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-IMP-ADER<br>
 * Variable: ADE-IMP-ADER from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_IMP_ADER;
    }

    public void setAdeImpAder(AfDecimal adeImpAder) {
        writeDecimalAsPacked(Pos.ADE_IMP_ADER, adeImpAder.copy());
    }

    public void setAdeImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_IMP_ADER, Pos.ADE_IMP_ADER);
    }

    /**Original name: ADE-IMP-ADER<br>*/
    public AfDecimal getAdeImpAder() {
        return readPackedAsDecimal(Pos.ADE_IMP_ADER, Len.Int.ADE_IMP_ADER, Len.Fract.ADE_IMP_ADER);
    }

    public byte[] getAdeImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_IMP_ADER, Pos.ADE_IMP_ADER);
        return buffer;
    }

    public void setAdeImpAderNull(String adeImpAderNull) {
        writeString(Pos.ADE_IMP_ADER_NULL, adeImpAderNull, Len.ADE_IMP_ADER_NULL);
    }

    /**Original name: ADE-IMP-ADER-NULL<br>*/
    public String getAdeImpAderNull() {
        return readString(Pos.ADE_IMP_ADER_NULL, Len.ADE_IMP_ADER_NULL);
    }

    public String getAdeImpAderNullFormatted() {
        return Functions.padBlanks(getAdeImpAderNull(), Len.ADE_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_IMP_ADER = 1;
        public static final int ADE_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_IMP_ADER = 8;
        public static final int ADE_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
