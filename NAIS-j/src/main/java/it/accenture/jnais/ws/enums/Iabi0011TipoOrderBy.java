package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABI0011-TIPO-ORDER-BY<br>
 * Variable: IABI0011-TIPO-ORDER-BY from copybook IABI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabi0011TipoOrderBy {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TIPO_ORDER_BY);
    public static final String IB_OGG = "IBO";
    public static final String ID_JOB = "JOB";

    //==== METHODS ====
    public void setTipoOrderBy(String tipoOrderBy) {
        this.value = Functions.subString(tipoOrderBy, Len.TIPO_ORDER_BY);
    }

    public String getTipoOrderBy() {
        return this.value;
    }

    public String getTipoOrderByFormatted() {
        return Functions.padBlanks(getTipoOrderBy(), Len.TIPO_ORDER_BY);
    }

    public boolean isIbOgg() {
        return value.equals(IB_OGG);
    }

    public boolean isIdJob() {
        return value.equals(ID_JOB);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_ORDER_BY = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
