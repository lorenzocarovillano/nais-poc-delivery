package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-ID-MOVI-CHIU<br>
 * Variable: VAS-ID-MOVI-CHIU from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_ID_MOVI_CHIU;
    }

    public void setVasIdMoviChiu(int vasIdMoviChiu) {
        writeIntAsPacked(Pos.VAS_ID_MOVI_CHIU, vasIdMoviChiu, Len.Int.VAS_ID_MOVI_CHIU);
    }

    public void setVasIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_ID_MOVI_CHIU, Pos.VAS_ID_MOVI_CHIU);
    }

    /**Original name: VAS-ID-MOVI-CHIU<br>*/
    public int getVasIdMoviChiu() {
        return readPackedAsInt(Pos.VAS_ID_MOVI_CHIU, Len.Int.VAS_ID_MOVI_CHIU);
    }

    public byte[] getVasIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_ID_MOVI_CHIU, Pos.VAS_ID_MOVI_CHIU);
        return buffer;
    }

    public void setVasIdMoviChiuNull(String vasIdMoviChiuNull) {
        writeString(Pos.VAS_ID_MOVI_CHIU_NULL, vasIdMoviChiuNull, Len.VAS_ID_MOVI_CHIU_NULL);
    }

    /**Original name: VAS-ID-MOVI-CHIU-NULL<br>*/
    public String getVasIdMoviChiuNull() {
        return readString(Pos.VAS_ID_MOVI_CHIU_NULL, Len.VAS_ID_MOVI_CHIU_NULL);
    }

    public String getVasIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getVasIdMoviChiuNull(), Len.VAS_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_ID_MOVI_CHIU = 1;
        public static final int VAS_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_ID_MOVI_CHIU = 5;
        public static final int VAS_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
