package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0040-COD-TP-TRASF<br>
 * Variable: ISPC0040-COD-TP-TRASF from copybook ISPC0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0040CodTpTrasf {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.COD_TP_TRASF);
    public static final String ABBUONO = "AB";
    public static final String PREM_UNICO = "PU";
    public static final String COMMERCIALE = "TC";
    public static final String NON_AMMESSA = "NA";

    //==== METHODS ====
    public void setCodTpTrasf(String codTpTrasf) {
        this.value = Functions.subString(codTpTrasf, Len.COD_TP_TRASF);
    }

    public String getCodTpTrasf() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_TP_TRASF = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
