package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-AREA-FILE<br>
 * Variable: WK-AREA-FILE from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkAreaFile {

    //==== PROPERTIES ====
    //Original name: WK-IB-OGG
    private String ibOgg = DefaultValues.stringVal(Len.IB_OGG);
    //Original name: WK-ID-POLI
    private String idPoli = DefaultValues.stringVal(Len.ID_POLI);
    //Original name: WK-ID-ADES
    private String idAdes = DefaultValues.stringVal(Len.ID_ADES);

    //==== METHODS ====
    public String getWkAreaFileFormatted() {
        return MarshalByteExt.bufferToStr(getWkAreaFileBytes());
    }

    public byte[] getWkAreaFileBytes() {
        byte[] buffer = new byte[Len.WK_AREA_FILE];
        return getWkAreaFileBytes(buffer, 1);
    }

    public byte[] getWkAreaFileBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ibOgg, Len.IB_OGG);
        position += Len.IB_OGG;
        MarshalByte.writeString(buffer, position, idPoli, Len.ID_POLI);
        position += Len.ID_POLI;
        MarshalByte.writeString(buffer, position, idAdes, Len.ID_ADES);
        return buffer;
    }

    public void setIbOgg(String ibOgg) {
        this.ibOgg = Functions.subString(ibOgg, Len.IB_OGG);
    }

    public String getIbOgg() {
        return this.ibOgg;
    }

    public void setIdPoliFormatted(String idPoli) {
        this.idPoli = Trunc.toUnsignedNumeric(idPoli, Len.ID_POLI);
    }

    public int getIdPoli() {
        return NumericDisplay.asInt(this.idPoli);
    }

    public void setIdAdesFormatted(String idAdes) {
        this.idAdes = Trunc.toUnsignedNumeric(idAdes, Len.ID_ADES);
    }

    public int getIdAdes() {
        return NumericDisplay.asInt(this.idAdes);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IB_OGG = 40;
        public static final int ID_POLI = 9;
        public static final int ID_ADES = 9;
        public static final int WK_AREA_FILE = IB_OGG + ID_POLI + ID_ADES;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
