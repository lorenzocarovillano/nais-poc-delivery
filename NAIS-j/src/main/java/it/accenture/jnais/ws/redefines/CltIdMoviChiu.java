package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: CLT-ID-MOVI-CHIU<br>
 * Variable: CLT-ID-MOVI-CHIU from program IDBSCLT0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CltIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public CltIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.CLT_ID_MOVI_CHIU;
    }

    public void setCltIdMoviChiu(int cltIdMoviChiu) {
        writeIntAsPacked(Pos.CLT_ID_MOVI_CHIU, cltIdMoviChiu, Len.Int.CLT_ID_MOVI_CHIU);
    }

    public void setCltIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.CLT_ID_MOVI_CHIU, Pos.CLT_ID_MOVI_CHIU);
    }

    /**Original name: CLT-ID-MOVI-CHIU<br>*/
    public int getCltIdMoviChiu() {
        return readPackedAsInt(Pos.CLT_ID_MOVI_CHIU, Len.Int.CLT_ID_MOVI_CHIU);
    }

    public byte[] getCltIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.CLT_ID_MOVI_CHIU, Pos.CLT_ID_MOVI_CHIU);
        return buffer;
    }

    public void setCltIdMoviChiuNull(String cltIdMoviChiuNull) {
        writeString(Pos.CLT_ID_MOVI_CHIU_NULL, cltIdMoviChiuNull, Len.CLT_ID_MOVI_CHIU_NULL);
    }

    /**Original name: CLT-ID-MOVI-CHIU-NULL<br>*/
    public String getCltIdMoviChiuNull() {
        return readString(Pos.CLT_ID_MOVI_CHIU_NULL, Len.CLT_ID_MOVI_CHIU_NULL);
    }

    public String getCltIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getCltIdMoviChiuNull(), Len.CLT_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int CLT_ID_MOVI_CHIU = 1;
        public static final int CLT_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int CLT_ID_MOVI_CHIU = 5;
        public static final int CLT_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CLT_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
