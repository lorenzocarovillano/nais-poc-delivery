package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-6003<br>
 * Variable: SW-6003 from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Sw6003 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO6003 = 'S';
    public static final char NO_TROVATO6003 = 'N';

    //==== METHODS ====
    public void setSw6003(char sw6003) {
        this.value = sw6003;
    }

    public char getSw6003() {
        return this.value;
    }

    public void setTrovato6003() {
        value = TROVATO6003;
    }

    public boolean isNoTrovato6003() {
        return value == NO_TROVATO6003;
    }

    public void setNoTrovato6003() {
        value = NO_TROVATO6003;
    }
}
