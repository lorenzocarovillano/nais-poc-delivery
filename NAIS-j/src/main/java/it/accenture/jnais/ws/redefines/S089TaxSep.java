package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TAX-SEP<br>
 * Variable: S089-TAX-SEP from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TaxSep extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TaxSep() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TAX_SEP;
    }

    public void setWlquTaxSep(AfDecimal wlquTaxSep) {
        writeDecimalAsPacked(Pos.S089_TAX_SEP, wlquTaxSep.copy());
    }

    public void setWlquTaxSepFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TAX_SEP, Pos.S089_TAX_SEP);
    }

    /**Original name: WLQU-TAX-SEP<br>*/
    public AfDecimal getWlquTaxSep() {
        return readPackedAsDecimal(Pos.S089_TAX_SEP, Len.Int.WLQU_TAX_SEP, Len.Fract.WLQU_TAX_SEP);
    }

    public byte[] getWlquTaxSepAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TAX_SEP, Pos.S089_TAX_SEP);
        return buffer;
    }

    public void initWlquTaxSepSpaces() {
        fill(Pos.S089_TAX_SEP, Len.S089_TAX_SEP, Types.SPACE_CHAR);
    }

    public void setWlquTaxSepNull(String wlquTaxSepNull) {
        writeString(Pos.S089_TAX_SEP_NULL, wlquTaxSepNull, Len.WLQU_TAX_SEP_NULL);
    }

    /**Original name: WLQU-TAX-SEP-NULL<br>*/
    public String getWlquTaxSepNull() {
        return readString(Pos.S089_TAX_SEP_NULL, Len.WLQU_TAX_SEP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TAX_SEP = 1;
        public static final int S089_TAX_SEP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TAX_SEP = 8;
        public static final int WLQU_TAX_SEP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TAX_SEP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TAX_SEP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
