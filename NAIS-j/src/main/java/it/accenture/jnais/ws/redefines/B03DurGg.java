package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DUR-GG<br>
 * Variable: B03-DUR-GG from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DUR_GG;
    }

    public void setB03DurGg(int b03DurGg) {
        writeIntAsPacked(Pos.B03_DUR_GG, b03DurGg, Len.Int.B03_DUR_GG);
    }

    public void setB03DurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DUR_GG, Pos.B03_DUR_GG);
    }

    /**Original name: B03-DUR-GG<br>*/
    public int getB03DurGg() {
        return readPackedAsInt(Pos.B03_DUR_GG, Len.Int.B03_DUR_GG);
    }

    public byte[] getB03DurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DUR_GG, Pos.B03_DUR_GG);
        return buffer;
    }

    public void setB03DurGgNull(String b03DurGgNull) {
        writeString(Pos.B03_DUR_GG_NULL, b03DurGgNull, Len.B03_DUR_GG_NULL);
    }

    /**Original name: B03-DUR-GG-NULL<br>*/
    public String getB03DurGgNull() {
        return readString(Pos.B03_DUR_GG_NULL, Len.B03_DUR_GG_NULL);
    }

    public String getB03DurGgNullFormatted() {
        return Functions.padBlanks(getB03DurGgNull(), Len.B03_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DUR_GG = 1;
        public static final int B03_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DUR_GG = 3;
        public static final int B03_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
