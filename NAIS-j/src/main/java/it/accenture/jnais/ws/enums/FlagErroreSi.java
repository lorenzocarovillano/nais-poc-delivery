package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-ERRORE-SI<br>
 * Variable: FLAG-ERRORE-SI from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagErroreSi {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagErroreSi(char flagErroreSi) {
        this.value = flagErroreSi;
    }

    public char getFlagErroreSi() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
