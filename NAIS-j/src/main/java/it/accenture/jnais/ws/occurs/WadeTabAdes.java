package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvade1;
import it.accenture.jnais.copy.WadeDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WADE-TAB-ADES<br>
 * Variables: WADE-TAB-ADES from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WadeTabAdes {

    //==== PROPERTIES ====
    //Original name: LCCVADE1
    private Lccvade1 lccvade1 = new Lccvade1();

    //==== METHODS ====
    public void setWadeTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvade1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvade1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvade1.Len.Int.ID_PTF, 0));
        position += Lccvade1.Len.ID_PTF;
        lccvade1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWadeTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvade1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvade1.getIdPtf(), Lccvade1.Len.Int.ID_PTF, 0);
        position += Lccvade1.Len.ID_PTF;
        lccvade1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWadeTabAdesSpaces() {
        lccvade1.initLccvade1Spaces();
    }

    public Lccvade1 getLccvade1() {
        return lccvade1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_TAB_ADES = WpolStatus.Len.STATUS + Lccvade1.Len.ID_PTF + WadeDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
