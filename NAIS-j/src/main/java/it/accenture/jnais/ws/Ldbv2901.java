package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LDBV2901<br>
 * Variable: LDBV2901 from copybook LDBV2901<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv2901 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV2901-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LDBV2901-IMPB-IS
    private AfDecimal impbIs = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LDBV2901-IIMPST-SOST
    private AfDecimal iimpstSost = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LDBV2901-DT-INI-PER
    private String dtIniPer = DefaultValues.stringVal(Len.DT_INI_PER);
    //Original name: LDBV2901-DT-FIN-PER
    private String dtFinPer = DefaultValues.stringVal(Len.DT_FIN_PER);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV2901;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv2901Bytes(buf);
    }

    public String getLdbv2901Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv2901Bytes());
    }

    public void setLdbv2901Bytes(byte[] buffer) {
        setLdbv2901Bytes(buffer, 1);
    }

    public byte[] getLdbv2901Bytes() {
        byte[] buffer = new byte[Len.LDBV2901];
        return getLdbv2901Bytes(buffer, 1);
    }

    public void setLdbv2901Bytes(byte[] buffer, int offset) {
        int position = offset;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        impbIs.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_IS, Len.Fract.IMPB_IS));
        position += Len.IMPB_IS;
        iimpstSost.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IIMPST_SOST, Len.Fract.IIMPST_SOST));
        position += Len.IIMPST_SOST;
        dtIniPer = MarshalByte.readFixedString(buffer, position, Len.DT_INI_PER);
        position += Len.DT_INI_PER;
        dtFinPer = MarshalByte.readFixedString(buffer, position, Len.DT_FIN_PER);
    }

    public byte[] getLdbv2901Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbIs.copy());
        position += Len.IMPB_IS;
        MarshalByte.writeDecimalAsPacked(buffer, position, iimpstSost.copy());
        position += Len.IIMPST_SOST;
        MarshalByte.writeString(buffer, position, dtIniPer, Len.DT_INI_PER);
        position += Len.DT_INI_PER;
        MarshalByte.writeString(buffer, position, dtFinPer, Len.DT_FIN_PER);
        return buffer;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setImpbIs(AfDecimal impbIs) {
        this.impbIs.assign(impbIs);
    }

    public AfDecimal getImpbIs() {
        return this.impbIs.copy();
    }

    public void setIimpstSost(AfDecimal iimpstSost) {
        this.iimpstSost.assign(iimpstSost);
    }

    public AfDecimal getIimpstSost() {
        return this.iimpstSost.copy();
    }

    public void setDtIniPer(int dtIniPer) {
        this.dtIniPer = NumericDisplay.asString(dtIniPer, Len.DT_INI_PER);
    }

    public int getDtIniPer() {
        return NumericDisplay.asInt(this.dtIniPer);
    }

    public String getDtIniPerFormatted() {
        return this.dtIniPer;
    }

    public void setDtFinPer(int dtFinPer) {
        this.dtFinPer = NumericDisplay.asString(dtFinPer, Len.DT_FIN_PER);
    }

    public int getDtFinPer() {
        return NumericDisplay.asInt(this.dtFinPer);
    }

    public String getDtFinPerFormatted() {
        return this.dtFinPer;
    }

    @Override
    public byte[] serialize() {
        return getLdbv2901Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_ADES = 5;
        public static final int IMPB_IS = 8;
        public static final int IIMPST_SOST = 8;
        public static final int DT_INI_PER = 8;
        public static final int DT_FIN_PER = 8;
        public static final int LDBV2901 = ID_ADES + IMPB_IS + IIMPST_SOST + DT_INI_PER + DT_FIN_PER;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_ADES = 9;
            public static final int IMPB_IS = 12;
            public static final int IIMPST_SOST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMPB_IS = 3;
            public static final int IIMPST_SOST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
