package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-ALQ-PROV-RICOR<br>
 * Variable: PVT-ALQ-PROV-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtAlqProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtAlqProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_ALQ_PROV_RICOR;
    }

    public void setPvtAlqProvRicor(AfDecimal pvtAlqProvRicor) {
        writeDecimalAsPacked(Pos.PVT_ALQ_PROV_RICOR, pvtAlqProvRicor.copy());
    }

    public void setPvtAlqProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_ALQ_PROV_RICOR, Pos.PVT_ALQ_PROV_RICOR);
    }

    /**Original name: PVT-ALQ-PROV-RICOR<br>*/
    public AfDecimal getPvtAlqProvRicor() {
        return readPackedAsDecimal(Pos.PVT_ALQ_PROV_RICOR, Len.Int.PVT_ALQ_PROV_RICOR, Len.Fract.PVT_ALQ_PROV_RICOR);
    }

    public byte[] getPvtAlqProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_ALQ_PROV_RICOR, Pos.PVT_ALQ_PROV_RICOR);
        return buffer;
    }

    public void setPvtAlqProvRicorNull(String pvtAlqProvRicorNull) {
        writeString(Pos.PVT_ALQ_PROV_RICOR_NULL, pvtAlqProvRicorNull, Len.PVT_ALQ_PROV_RICOR_NULL);
    }

    /**Original name: PVT-ALQ-PROV-RICOR-NULL<br>*/
    public String getPvtAlqProvRicorNull() {
        return readString(Pos.PVT_ALQ_PROV_RICOR_NULL, Len.PVT_ALQ_PROV_RICOR_NULL);
    }

    public String getPvtAlqProvRicorNullFormatted() {
        return Functions.padBlanks(getPvtAlqProvRicorNull(), Len.PVT_ALQ_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_ALQ_PROV_RICOR = 1;
        public static final int PVT_ALQ_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_ALQ_PROV_RICOR = 4;
        public static final int PVT_ALQ_PROV_RICOR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_ALQ_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PVT_ALQ_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
