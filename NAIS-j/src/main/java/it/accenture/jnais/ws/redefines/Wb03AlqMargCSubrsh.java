package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-ALQ-MARG-C-SUBRSH<br>
 * Variable: WB03-ALQ-MARG-C-SUBRSH from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03AlqMargCSubrsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03AlqMargCSubrsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_ALQ_MARG_C_SUBRSH;
    }

    public void setWb03AlqMargCSubrsh(AfDecimal wb03AlqMargCSubrsh) {
        writeDecimalAsPacked(Pos.WB03_ALQ_MARG_C_SUBRSH, wb03AlqMargCSubrsh.copy());
    }

    public void setWb03AlqMargCSubrshFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_ALQ_MARG_C_SUBRSH, Pos.WB03_ALQ_MARG_C_SUBRSH);
    }

    /**Original name: WB03-ALQ-MARG-C-SUBRSH<br>*/
    public AfDecimal getWb03AlqMargCSubrsh() {
        return readPackedAsDecimal(Pos.WB03_ALQ_MARG_C_SUBRSH, Len.Int.WB03_ALQ_MARG_C_SUBRSH, Len.Fract.WB03_ALQ_MARG_C_SUBRSH);
    }

    public byte[] getWb03AlqMargCSubrshAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_ALQ_MARG_C_SUBRSH, Pos.WB03_ALQ_MARG_C_SUBRSH);
        return buffer;
    }

    public void setWb03AlqMargCSubrshNull(String wb03AlqMargCSubrshNull) {
        writeString(Pos.WB03_ALQ_MARG_C_SUBRSH_NULL, wb03AlqMargCSubrshNull, Len.WB03_ALQ_MARG_C_SUBRSH_NULL);
    }

    /**Original name: WB03-ALQ-MARG-C-SUBRSH-NULL<br>*/
    public String getWb03AlqMargCSubrshNull() {
        return readString(Pos.WB03_ALQ_MARG_C_SUBRSH_NULL, Len.WB03_ALQ_MARG_C_SUBRSH_NULL);
    }

    public String getWb03AlqMargCSubrshNullFormatted() {
        return Functions.padBlanks(getWb03AlqMargCSubrshNull(), Len.WB03_ALQ_MARG_C_SUBRSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_ALQ_MARG_C_SUBRSH = 1;
        public static final int WB03_ALQ_MARG_C_SUBRSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ALQ_MARG_C_SUBRSH = 4;
        public static final int WB03_ALQ_MARG_C_SUBRSH_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ALQ_MARG_C_SUBRSH = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_ALQ_MARG_C_SUBRSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
