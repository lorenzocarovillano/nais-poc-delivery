package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-FRQ-MOVI<br>
 * Variable: WDTR-FRQ-MOVI from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrFrqMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrFrqMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_FRQ_MOVI;
    }

    public void setWdtrFrqMovi(int wdtrFrqMovi) {
        writeIntAsPacked(Pos.WDTR_FRQ_MOVI, wdtrFrqMovi, Len.Int.WDTR_FRQ_MOVI);
    }

    /**Original name: WDTR-FRQ-MOVI<br>*/
    public int getWdtrFrqMovi() {
        return readPackedAsInt(Pos.WDTR_FRQ_MOVI, Len.Int.WDTR_FRQ_MOVI);
    }

    public void setWdtrFrqMoviNull(String wdtrFrqMoviNull) {
        writeString(Pos.WDTR_FRQ_MOVI_NULL, wdtrFrqMoviNull, Len.WDTR_FRQ_MOVI_NULL);
    }

    /**Original name: WDTR-FRQ-MOVI-NULL<br>*/
    public String getWdtrFrqMoviNull() {
        return readString(Pos.WDTR_FRQ_MOVI_NULL, Len.WDTR_FRQ_MOVI_NULL);
    }

    public String getWdtrFrqMoviNullFormatted() {
        return Functions.padBlanks(getWdtrFrqMoviNull(), Len.WDTR_FRQ_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_FRQ_MOVI = 1;
        public static final int WDTR_FRQ_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_FRQ_MOVI = 3;
        public static final int WDTR_FRQ_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_FRQ_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
