package it.accenture.jnais.ws;

import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idbve153;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndEstRappAna;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSE150<br>
 * Generated as a class for rule WS.<br>*/
public class Idbse150Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WS-BUFFER-TABLE
    private String wsBufferTable = "";
    //Original name: WS-ID-MOVI-CRZ
    private int wsIdMoviCrz = 0;
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-EST-RAPP-ANA
    private IndEstRappAna indEstRappAna = new IndEstRappAna();
    //Original name: IDBVE153
    private Idbve153 idbve153 = new Idbve153();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWsBufferTable(String wsBufferTable) {
        this.wsBufferTable = Functions.subString(wsBufferTable, Len.WS_BUFFER_TABLE);
    }

    public String getWsBufferTable() {
        return this.wsBufferTable;
    }

    public String getWsBufferTableFormatted() {
        return Functions.padBlanks(getWsBufferTable(), Len.WS_BUFFER_TABLE);
    }

    public void setWsIdMoviCrz(int wsIdMoviCrz) {
        this.wsIdMoviCrz = wsIdMoviCrz;
    }

    public int getWsIdMoviCrz() {
        return this.wsIdMoviCrz;
    }

    public Idbve153 getIdbve153() {
        return idbve153;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndEstRappAna getIndEstRappAna() {
        return indEstRappAna;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int WS_BUFFER_TABLE = 2000;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
