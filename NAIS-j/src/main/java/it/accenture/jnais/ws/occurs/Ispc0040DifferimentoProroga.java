package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Ispc0040CodDiffProg;

/**Original name: ISPC0040-DIFFERIMENTO-PROROGA<br>
 * Variables: ISPC0040-DIFFERIMENTO-PROROGA from copybook ISPC0040<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0040DifferimentoProroga {

    //==== PROPERTIES ====
    //Original name: ISPC0040-COD-DIFF-PROG
    private Ispc0040CodDiffProg codDiffProg = new Ispc0040CodDiffProg();
    //Original name: ISPC0040-DESC-DIFF-PROG
    private String descDiffProg = DefaultValues.stringVal(Len.DESC_DIFF_PROG);

    //==== METHODS ====
    public void setDifferimentoProrogaBytes(byte[] buffer, int offset) {
        int position = offset;
        codDiffProg.setCodDiffProg(MarshalByte.readString(buffer, position, Ispc0040CodDiffProg.Len.COD_DIFF_PROG));
        position += Ispc0040CodDiffProg.Len.COD_DIFF_PROG;
        descDiffProg = MarshalByte.readString(buffer, position, Len.DESC_DIFF_PROG);
    }

    public byte[] getDifferimentoProrogaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codDiffProg.getCodDiffProg(), Ispc0040CodDiffProg.Len.COD_DIFF_PROG);
        position += Ispc0040CodDiffProg.Len.COD_DIFF_PROG;
        MarshalByte.writeString(buffer, position, descDiffProg, Len.DESC_DIFF_PROG);
        return buffer;
    }

    public void initDifferimentoProrogaSpaces() {
        codDiffProg.setCodDiffProg("");
        descDiffProg = "";
    }

    public void setDescDiffProg(String descDiffProg) {
        this.descDiffProg = Functions.subString(descDiffProg, Len.DESC_DIFF_PROG);
    }

    public String getDescDiffProg() {
        return this.descDiffProg;
    }

    public Ispc0040CodDiffProg getCodDiffProg() {
        return codDiffProg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESC_DIFF_PROG = 30;
        public static final int DIFFERIMENTO_PROROGA = Ispc0040CodDiffProg.Len.COD_DIFF_PROG + DESC_DIFF_PROG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
