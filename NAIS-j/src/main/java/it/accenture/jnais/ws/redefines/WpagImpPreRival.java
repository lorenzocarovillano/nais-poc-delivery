package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-PRE-RIVAL<br>
 * Variable: WPAG-IMP-PRE-RIVAL from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpPreRival extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpPreRival() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_PRE_RIVAL;
    }

    public void setWpagImpPreRival(AfDecimal wpagImpPreRival) {
        writeDecimalAsPacked(Pos.WPAG_IMP_PRE_RIVAL, wpagImpPreRival.copy());
    }

    public void setWpagImpPreRivalFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_PRE_RIVAL, Pos.WPAG_IMP_PRE_RIVAL);
    }

    /**Original name: WPAG-IMP-PRE-RIVAL<br>*/
    public AfDecimal getWpagImpPreRival() {
        return readPackedAsDecimal(Pos.WPAG_IMP_PRE_RIVAL, Len.Int.WPAG_IMP_PRE_RIVAL, Len.Fract.WPAG_IMP_PRE_RIVAL);
    }

    public byte[] getWpagImpPreRivalAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_PRE_RIVAL, Pos.WPAG_IMP_PRE_RIVAL);
        return buffer;
    }

    public void initWpagImpPreRivalSpaces() {
        fill(Pos.WPAG_IMP_PRE_RIVAL, Len.WPAG_IMP_PRE_RIVAL, Types.SPACE_CHAR);
    }

    public void setWpagImpPreRivalNull(String wpagImpPreRivalNull) {
        writeString(Pos.WPAG_IMP_PRE_RIVAL_NULL, wpagImpPreRivalNull, Len.WPAG_IMP_PRE_RIVAL_NULL);
    }

    /**Original name: WPAG-IMP-PRE-RIVAL-NULL<br>*/
    public String getWpagImpPreRivalNull() {
        return readString(Pos.WPAG_IMP_PRE_RIVAL_NULL, Len.WPAG_IMP_PRE_RIVAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_PRE_RIVAL = 1;
        public static final int WPAG_IMP_PRE_RIVAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_PRE_RIVAL = 8;
        public static final int WPAG_IMP_PRE_RIVAL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_PRE_RIVAL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_PRE_RIVAL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
