package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-CAR-GEST<br>
 * Variable: DTR-CAR-GEST from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_CAR_GEST;
    }

    public void setDtrCarGest(AfDecimal dtrCarGest) {
        writeDecimalAsPacked(Pos.DTR_CAR_GEST, dtrCarGest.copy());
    }

    public void setDtrCarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_CAR_GEST, Pos.DTR_CAR_GEST);
    }

    /**Original name: DTR-CAR-GEST<br>*/
    public AfDecimal getDtrCarGest() {
        return readPackedAsDecimal(Pos.DTR_CAR_GEST, Len.Int.DTR_CAR_GEST, Len.Fract.DTR_CAR_GEST);
    }

    public byte[] getDtrCarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_CAR_GEST, Pos.DTR_CAR_GEST);
        return buffer;
    }

    public void setDtrCarGestNull(String dtrCarGestNull) {
        writeString(Pos.DTR_CAR_GEST_NULL, dtrCarGestNull, Len.DTR_CAR_GEST_NULL);
    }

    /**Original name: DTR-CAR-GEST-NULL<br>*/
    public String getDtrCarGestNull() {
        return readString(Pos.DTR_CAR_GEST_NULL, Len.DTR_CAR_GEST_NULL);
    }

    public String getDtrCarGestNullFormatted() {
        return Functions.padBlanks(getDtrCarGestNull(), Len.DTR_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_CAR_GEST = 1;
        public static final int DTR_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_CAR_GEST = 8;
        public static final int DTR_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
