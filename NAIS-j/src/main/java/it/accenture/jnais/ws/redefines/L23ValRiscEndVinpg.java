package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L23-VAL-RISC-END-VINPG<br>
 * Variable: L23-VAL-RISC-END-VINPG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L23ValRiscEndVinpg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L23ValRiscEndVinpg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L23_VAL_RISC_END_VINPG;
    }

    public void setL23ValRiscEndVinpg(AfDecimal l23ValRiscEndVinpg) {
        writeDecimalAsPacked(Pos.L23_VAL_RISC_END_VINPG, l23ValRiscEndVinpg.copy());
    }

    public void setL23ValRiscEndVinpgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L23_VAL_RISC_END_VINPG, Pos.L23_VAL_RISC_END_VINPG);
    }

    /**Original name: L23-VAL-RISC-END-VINPG<br>*/
    public AfDecimal getL23ValRiscEndVinpg() {
        return readPackedAsDecimal(Pos.L23_VAL_RISC_END_VINPG, Len.Int.L23_VAL_RISC_END_VINPG, Len.Fract.L23_VAL_RISC_END_VINPG);
    }

    public byte[] getL23ValRiscEndVinpgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L23_VAL_RISC_END_VINPG, Pos.L23_VAL_RISC_END_VINPG);
        return buffer;
    }

    public void setL23ValRiscEndVinpgNull(String l23ValRiscEndVinpgNull) {
        writeString(Pos.L23_VAL_RISC_END_VINPG_NULL, l23ValRiscEndVinpgNull, Len.L23_VAL_RISC_END_VINPG_NULL);
    }

    /**Original name: L23-VAL-RISC-END-VINPG-NULL<br>*/
    public String getL23ValRiscEndVinpgNull() {
        return readString(Pos.L23_VAL_RISC_END_VINPG_NULL, Len.L23_VAL_RISC_END_VINPG_NULL);
    }

    public String getL23ValRiscEndVinpgNullFormatted() {
        return Functions.padBlanks(getL23ValRiscEndVinpgNull(), Len.L23_VAL_RISC_END_VINPG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L23_VAL_RISC_END_VINPG = 1;
        public static final int L23_VAL_RISC_END_VINPG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L23_VAL_RISC_END_VINPG = 8;
        public static final int L23_VAL_RISC_END_VINPG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L23_VAL_RISC_END_VINPG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L23_VAL_RISC_END_VINPG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
