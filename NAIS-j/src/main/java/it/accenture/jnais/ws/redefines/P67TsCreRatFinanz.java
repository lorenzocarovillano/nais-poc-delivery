package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-TS-CRE-RAT-FINANZ<br>
 * Variable: P67-TS-CRE-RAT-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67TsCreRatFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67TsCreRatFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_TS_CRE_RAT_FINANZ;
    }

    public void setP67TsCreRatFinanz(AfDecimal p67TsCreRatFinanz) {
        writeDecimalAsPacked(Pos.P67_TS_CRE_RAT_FINANZ, p67TsCreRatFinanz.copy());
    }

    public void setP67TsCreRatFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_TS_CRE_RAT_FINANZ, Pos.P67_TS_CRE_RAT_FINANZ);
    }

    /**Original name: P67-TS-CRE-RAT-FINANZ<br>*/
    public AfDecimal getP67TsCreRatFinanz() {
        return readPackedAsDecimal(Pos.P67_TS_CRE_RAT_FINANZ, Len.Int.P67_TS_CRE_RAT_FINANZ, Len.Fract.P67_TS_CRE_RAT_FINANZ);
    }

    public byte[] getP67TsCreRatFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_TS_CRE_RAT_FINANZ, Pos.P67_TS_CRE_RAT_FINANZ);
        return buffer;
    }

    public void setP67TsCreRatFinanzNull(String p67TsCreRatFinanzNull) {
        writeString(Pos.P67_TS_CRE_RAT_FINANZ_NULL, p67TsCreRatFinanzNull, Len.P67_TS_CRE_RAT_FINANZ_NULL);
    }

    /**Original name: P67-TS-CRE-RAT-FINANZ-NULL<br>*/
    public String getP67TsCreRatFinanzNull() {
        return readString(Pos.P67_TS_CRE_RAT_FINANZ_NULL, Len.P67_TS_CRE_RAT_FINANZ_NULL);
    }

    public String getP67TsCreRatFinanzNullFormatted() {
        return Functions.padBlanks(getP67TsCreRatFinanzNull(), Len.P67_TS_CRE_RAT_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_TS_CRE_RAT_FINANZ = 1;
        public static final int P67_TS_CRE_RAT_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_TS_CRE_RAT_FINANZ = 8;
        public static final int P67_TS_CRE_RAT_FINANZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_TS_CRE_RAT_FINANZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_TS_CRE_RAT_FINANZ = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
