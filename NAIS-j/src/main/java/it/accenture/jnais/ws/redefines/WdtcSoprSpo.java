package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-SOPR-SPO<br>
 * Variable: WDTC-SOPR-SPO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_SOPR_SPO;
    }

    public void setWdtcSoprSpo(AfDecimal wdtcSoprSpo) {
        writeDecimalAsPacked(Pos.WDTC_SOPR_SPO, wdtcSoprSpo.copy());
    }

    public void setWdtcSoprSpoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_SOPR_SPO, Pos.WDTC_SOPR_SPO);
    }

    /**Original name: WDTC-SOPR-SPO<br>*/
    public AfDecimal getWdtcSoprSpo() {
        return readPackedAsDecimal(Pos.WDTC_SOPR_SPO, Len.Int.WDTC_SOPR_SPO, Len.Fract.WDTC_SOPR_SPO);
    }

    public byte[] getWdtcSoprSpoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_SOPR_SPO, Pos.WDTC_SOPR_SPO);
        return buffer;
    }

    public void initWdtcSoprSpoSpaces() {
        fill(Pos.WDTC_SOPR_SPO, Len.WDTC_SOPR_SPO, Types.SPACE_CHAR);
    }

    public void setWdtcSoprSpoNull(String wdtcSoprSpoNull) {
        writeString(Pos.WDTC_SOPR_SPO_NULL, wdtcSoprSpoNull, Len.WDTC_SOPR_SPO_NULL);
    }

    /**Original name: WDTC-SOPR-SPO-NULL<br>*/
    public String getWdtcSoprSpoNull() {
        return readString(Pos.WDTC_SOPR_SPO_NULL, Len.WDTC_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_SOPR_SPO = 1;
        public static final int WDTC_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_SOPR_SPO = 8;
        public static final int WDTC_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
