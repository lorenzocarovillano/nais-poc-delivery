package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import org.apache.commons.lang3.ArrayUtils;

/**Original name: IDSV0001-LIVELLO-DEBUG<br>
 * Variable: IDSV0001-LIVELLO-DEBUG from copybook IDSV0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0001LivelloDebug {

    //==== PROPERTIES ====
    public String value = DefaultValues.stringVal(Len.LIVELLO_DEBUG);
    public static final String NO_DEBUG = "0";
    public static final String DEBUG_BASSO = "1";
    public static final String DEBUG_MEDIO = "2";
    public static final String DEBUG_ELEVATO = "3";
    public static final String DEBUG_ESASPERATO = "4";
    private static final String[] ANY_APPL_DBG = new String[] {"1", "2", "3", "4"};
    public static final String ARCH_BATCH_DBG = "5";
    public static final String COM_COB_JAV_DBG = "6";
    public static final String STRESS_TEST_DBG = "7";
    public static final String BUSINESS_DBG = "8";
    public static final String TOT_TUNING_DBG = "9";
    private static final String[] ANY_TUNING_DBG = new String[] {"5", "6", "7", "8", "9"};

    //==== METHODS ====
    public void setIdsv0001LivelloDebug(short idsv0001LivelloDebug) {
        this.value = NumericDisplay.asString(idsv0001LivelloDebug, Len.LIVELLO_DEBUG);
    }

    public void setIdsi0011LivelloDebugFormatted(String idsi0011LivelloDebug) {
        this.value = Trunc.toUnsignedNumeric(idsi0011LivelloDebug, Len.LIVELLO_DEBUG);
    }

    public short getIdsi0011LivelloDebug() {
        return NumericDisplay.asShort(this.value);
    }

    public String getIdsi0011LivelloDebugFormatted() {
        return this.value;
    }

    public void setIdsv0001NoDebug() {
        setIdsi0011LivelloDebugFormatted(NO_DEBUG);
    }

    public boolean isIdsv0001DebugBasso() {
        return getIdsi0011LivelloDebugFormatted().equals(DEBUG_BASSO);
    }

    public boolean isIdsv0001DebugMedio() {
        return getIdsi0011LivelloDebugFormatted().equals(DEBUG_MEDIO);
    }

    public boolean isIdsv0001DebugEsasperato() {
        return getIdsi0011LivelloDebugFormatted().equals(DEBUG_ESASPERATO);
    }

    public boolean isAnyApplDbg() {
        return ArrayUtils.contains(ANY_APPL_DBG, getIdsi0011LivelloDebugFormatted());
    }

    public boolean isIdsv0001ArchBatchDbg() {
        return getIdsi0011LivelloDebugFormatted().equals(ARCH_BATCH_DBG);
    }

    public boolean isTotTuningDbg() {
        return getIdsi0011LivelloDebugFormatted().equals(TOT_TUNING_DBG);
    }

    public boolean isAnyTuningDbg() {
        return ArrayUtils.contains(ANY_TUNING_DBG, getIdsi0011LivelloDebugFormatted());
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LIVELLO_DEBUG = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
