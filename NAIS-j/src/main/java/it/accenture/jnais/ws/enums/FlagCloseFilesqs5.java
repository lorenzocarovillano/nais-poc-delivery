package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-CLOSE-FILESQS5<br>
 * Variable: FLAG-CLOSE-FILESQS5 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCloseFilesqs5 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCloseFilesqs5(char flagCloseFilesqs5) {
        this.value = flagCloseFilesqs5;
    }

    public char getFlagCloseFilesqs5() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }
}
