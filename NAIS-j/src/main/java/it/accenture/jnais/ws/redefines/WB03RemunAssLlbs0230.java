package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-REMUN-ASS<br>
 * Variable: W-B03-REMUN-ASS from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03RemunAssLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03RemunAssLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_REMUN_ASS;
    }

    public void setwB03RemunAss(AfDecimal wB03RemunAss) {
        writeDecimalAsPacked(Pos.W_B03_REMUN_ASS, wB03RemunAss.copy());
    }

    /**Original name: W-B03-REMUN-ASS<br>*/
    public AfDecimal getwB03RemunAss() {
        return readPackedAsDecimal(Pos.W_B03_REMUN_ASS, Len.Int.W_B03_REMUN_ASS, Len.Fract.W_B03_REMUN_ASS);
    }

    public byte[] getwB03RemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_REMUN_ASS, Pos.W_B03_REMUN_ASS);
        return buffer;
    }

    public void setwB03RemunAssNull(String wB03RemunAssNull) {
        writeString(Pos.W_B03_REMUN_ASS_NULL, wB03RemunAssNull, Len.W_B03_REMUN_ASS_NULL);
    }

    /**Original name: W-B03-REMUN-ASS-NULL<br>*/
    public String getwB03RemunAssNull() {
        return readString(Pos.W_B03_REMUN_ASS_NULL, Len.W_B03_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_REMUN_ASS = 1;
        public static final int W_B03_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_REMUN_ASS = 8;
        public static final int W_B03_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
