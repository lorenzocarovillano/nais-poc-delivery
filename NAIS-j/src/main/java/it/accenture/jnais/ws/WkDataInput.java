package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-DATA-INPUT<br>
 * Variable: WK-DATA-INPUT from program LVVS0000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkDataInput {

    //==== PROPERTIES ====
    //Original name: WK-AAAA-INPUT
    private String aaaaInput = DefaultValues.stringVal(Len.AAAA_INPUT);
    //Original name: WK-MM-INPUT
    private String mmInput = DefaultValues.stringVal(Len.MM_INPUT);
    //Original name: WK-GG-INPUT
    private String ggInput = DefaultValues.stringVal(Len.GG_INPUT);

    //==== METHODS ====
    public void setWkDataInputFormatted(String data) {
        byte[] buffer = new byte[Len.WK_DATA_INPUT];
        MarshalByte.writeString(buffer, 1, data, Len.WK_DATA_INPUT);
        setWkDataInputBytes(buffer, 1);
    }

    public byte[] getWkDataInputBytes() {
        byte[] buffer = new byte[Len.WK_DATA_INPUT];
        return getWkDataInputBytes(buffer, 1);
    }

    public void setWkDataInputBytes(byte[] buffer, int offset) {
        int position = offset;
        aaaaInput = MarshalByte.readFixedString(buffer, position, Len.AAAA_INPUT);
        position += Len.AAAA_INPUT;
        mmInput = MarshalByte.readFixedString(buffer, position, Len.MM_INPUT);
        position += Len.MM_INPUT;
        ggInput = MarshalByte.readFixedString(buffer, position, Len.GG_INPUT);
    }

    public byte[] getWkDataInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aaaaInput, Len.AAAA_INPUT);
        position += Len.AAAA_INPUT;
        MarshalByte.writeString(buffer, position, mmInput, Len.MM_INPUT);
        position += Len.MM_INPUT;
        MarshalByte.writeString(buffer, position, ggInput, Len.GG_INPUT);
        return buffer;
    }

    public void setAaaaInput(short aaaaInput) {
        this.aaaaInput = NumericDisplay.asString(aaaaInput, Len.AAAA_INPUT);
    }

    public void setAaaaInputFormatted(String aaaaInput) {
        this.aaaaInput = Trunc.toUnsignedNumeric(aaaaInput, Len.AAAA_INPUT);
    }

    public short getAaaaInput() {
        return NumericDisplay.asShort(this.aaaaInput);
    }

    public String getAaaaInputFormatted() {
        return this.aaaaInput;
    }

    public void setMmInput(short mmInput) {
        this.mmInput = NumericDisplay.asString(mmInput, Len.MM_INPUT);
    }

    public void setMmInputFormatted(String mmInput) {
        this.mmInput = Trunc.toUnsignedNumeric(mmInput, Len.MM_INPUT);
    }

    public short getMmInput() {
        return NumericDisplay.asShort(this.mmInput);
    }

    public void setGgInput(short ggInput) {
        this.ggInput = NumericDisplay.asString(ggInput, Len.GG_INPUT);
    }

    public void setGgInputFormatted(String ggInput) {
        this.ggInput = Trunc.toUnsignedNumeric(ggInput, Len.GG_INPUT);
    }

    public short getGgInput() {
        return NumericDisplay.asShort(this.ggInput);
    }

    public String getGgInputFormatted() {
        return this.ggInput;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA_INPUT = 4;
        public static final int MM_INPUT = 2;
        public static final int GG_INPUT = 2;
        public static final int WK_DATA_INPUT = AAAA_INPUT + MM_INPUT + GG_INPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
