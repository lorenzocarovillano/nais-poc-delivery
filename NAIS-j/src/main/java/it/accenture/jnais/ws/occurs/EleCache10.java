package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ELE-CACHE-10<br>
 * Variables: ELE-CACHE-10 from program LRGS0660<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class EleCache10 {

    //==== PROPERTIES ====
    //Original name: REC10-ID-GAR
    private String idGar = DefaultValues.stringVal(Len.ID_GAR);
    //Original name: REC10-DESC-TP-MOVI
    private String descTpMovi = DefaultValues.stringVal(Len.DESC_TP_MOVI);
    //Original name: REC10-TP-MOVI
    private String tpMovi = DefaultValues.stringVal(Len.TP_MOVI);
    //Original name: REC10-IMP-OPER
    private AfDecimal impOper = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC10-IMP-VERS
    private AfDecimal impVers = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC10-NUMERO-QUO
    private AfDecimal numeroQuo = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: REC10-DT-EFF-MOVI
    private String dtEffMovi = DefaultValues.stringVal(Len.DT_EFF_MOVI);

    //==== METHODS ====
    public void setIdGarFormatted(String idGar) {
        this.idGar = Trunc.toUnsignedNumeric(idGar, Len.ID_GAR);
    }

    public int getIdGar() {
        return NumericDisplay.asInt(this.idGar);
    }

    public void setDescTpMovi(String descTpMovi) {
        this.descTpMovi = Functions.subString(descTpMovi, Len.DESC_TP_MOVI);
    }

    public String getDescTpMovi() {
        return this.descTpMovi;
    }

    public void setTpMoviFormatted(String tpMovi) {
        this.tpMovi = Trunc.toUnsignedNumeric(tpMovi, Len.TP_MOVI);
    }

    public int getTpMovi() {
        return NumericDisplay.asInt(this.tpMovi);
    }

    public void setImpOper(AfDecimal impOper) {
        this.impOper.assign(impOper);
    }

    public AfDecimal getImpOper() {
        return this.impOper.copy();
    }

    public void setImpVers(AfDecimal impVers) {
        this.impVers.assign(impVers);
    }

    public AfDecimal getImpVers() {
        return this.impVers.copy();
    }

    public void setNumeroQuo(AfDecimal numeroQuo) {
        this.numeroQuo.assign(numeroQuo);
    }

    public AfDecimal getNumeroQuo() {
        return this.numeroQuo.copy();
    }

    public void setDtEffMovi(String dtEffMovi) {
        this.dtEffMovi = Functions.subString(dtEffMovi, Len.DT_EFF_MOVI);
    }

    public String getDtEffMovi() {
        return this.dtEffMovi;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_GAR = 9;
        public static final int DESC_TP_MOVI = 50;
        public static final int TP_MOVI = 5;
        public static final int DT_EFF_MOVI = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
