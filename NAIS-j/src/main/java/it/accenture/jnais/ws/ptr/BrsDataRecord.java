package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: BRS-DATA-RECORD<br>
 * Variable: BRS-DATA-RECORD from copybook IDBVBRS1<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class BrsDataRecord extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BrsDataRecord() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DATA_RECORD;
    }

    public void setDataRecordFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DATA_RECORD, Pos.DATA_RECORD);
    }

    public String getDataRecord() {
        return readString(Pos.DATA_RECORD, Len.DATA_RECORD);
    }

    public byte[] getDataRecordAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DATA_RECORD, Pos.DATA_RECORD);
        return buffer;
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DATA_RECORD = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DATA_RECORD = 10000;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
