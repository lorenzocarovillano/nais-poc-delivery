package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-MANFEE-REC<br>
 * Variable: WDTC-MANFEE-REC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcManfeeRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcManfeeRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_MANFEE_REC;
    }

    public void setWdtcManfeeRec(AfDecimal wdtcManfeeRec) {
        writeDecimalAsPacked(Pos.WDTC_MANFEE_REC, wdtcManfeeRec.copy());
    }

    public void setWdtcManfeeRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_MANFEE_REC, Pos.WDTC_MANFEE_REC);
    }

    /**Original name: WDTC-MANFEE-REC<br>*/
    public AfDecimal getWdtcManfeeRec() {
        return readPackedAsDecimal(Pos.WDTC_MANFEE_REC, Len.Int.WDTC_MANFEE_REC, Len.Fract.WDTC_MANFEE_REC);
    }

    public byte[] getWdtcManfeeRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_MANFEE_REC, Pos.WDTC_MANFEE_REC);
        return buffer;
    }

    public void initWdtcManfeeRecSpaces() {
        fill(Pos.WDTC_MANFEE_REC, Len.WDTC_MANFEE_REC, Types.SPACE_CHAR);
    }

    public void setWdtcManfeeRecNull(String wdtcManfeeRecNull) {
        writeString(Pos.WDTC_MANFEE_REC_NULL, wdtcManfeeRecNull, Len.WDTC_MANFEE_REC_NULL);
    }

    /**Original name: WDTC-MANFEE-REC-NULL<br>*/
    public String getWdtcManfeeRecNull() {
        return readString(Pos.WDTC_MANFEE_REC_NULL, Len.WDTC_MANFEE_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_MANFEE_REC = 1;
        public static final int WDTC_MANFEE_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_MANFEE_REC = 8;
        public static final int WDTC_MANFEE_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_MANFEE_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_MANFEE_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
