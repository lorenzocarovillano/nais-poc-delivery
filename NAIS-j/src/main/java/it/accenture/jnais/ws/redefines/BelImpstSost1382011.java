package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-IMPST-SOST-1382011<br>
 * Variable: BEL-IMPST-SOST-1382011 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelImpstSost1382011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelImpstSost1382011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_IMPST_SOST1382011;
    }

    public void setBelImpstSost1382011(AfDecimal belImpstSost1382011) {
        writeDecimalAsPacked(Pos.BEL_IMPST_SOST1382011, belImpstSost1382011.copy());
    }

    public void setBelImpstSost1382011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_IMPST_SOST1382011, Pos.BEL_IMPST_SOST1382011);
    }

    /**Original name: BEL-IMPST-SOST-1382011<br>*/
    public AfDecimal getBelImpstSost1382011() {
        return readPackedAsDecimal(Pos.BEL_IMPST_SOST1382011, Len.Int.BEL_IMPST_SOST1382011, Len.Fract.BEL_IMPST_SOST1382011);
    }

    public byte[] getBelImpstSost1382011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_IMPST_SOST1382011, Pos.BEL_IMPST_SOST1382011);
        return buffer;
    }

    public void setBelImpstSost1382011Null(String belImpstSost1382011Null) {
        writeString(Pos.BEL_IMPST_SOST1382011_NULL, belImpstSost1382011Null, Len.BEL_IMPST_SOST1382011_NULL);
    }

    /**Original name: BEL-IMPST-SOST-1382011-NULL<br>*/
    public String getBelImpstSost1382011Null() {
        return readString(Pos.BEL_IMPST_SOST1382011_NULL, Len.BEL_IMPST_SOST1382011_NULL);
    }

    public String getBelImpstSost1382011NullFormatted() {
        return Functions.padBlanks(getBelImpstSost1382011Null(), Len.BEL_IMPST_SOST1382011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_SOST1382011 = 1;
        public static final int BEL_IMPST_SOST1382011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_SOST1382011 = 8;
        public static final int BEL_IMPST_SOST1382011_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_SOST1382011 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_SOST1382011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
