package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPST-VIS-1382011<br>
 * Variable: S089-IMPST-VIS-1382011 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpstVis1382011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpstVis1382011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPST_VIS1382011;
    }

    public void setWlquImpstVis1382011(AfDecimal wlquImpstVis1382011) {
        writeDecimalAsPacked(Pos.S089_IMPST_VIS1382011, wlquImpstVis1382011.copy());
    }

    public void setWlquImpstVis1382011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPST_VIS1382011, Pos.S089_IMPST_VIS1382011);
    }

    /**Original name: WLQU-IMPST-VIS-1382011<br>*/
    public AfDecimal getWlquImpstVis1382011() {
        return readPackedAsDecimal(Pos.S089_IMPST_VIS1382011, Len.Int.WLQU_IMPST_VIS1382011, Len.Fract.WLQU_IMPST_VIS1382011);
    }

    public byte[] getWlquImpstVis1382011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPST_VIS1382011, Pos.S089_IMPST_VIS1382011);
        return buffer;
    }

    public void initWlquImpstVis1382011Spaces() {
        fill(Pos.S089_IMPST_VIS1382011, Len.S089_IMPST_VIS1382011, Types.SPACE_CHAR);
    }

    public void setWlquImpstVis1382011Null(String wlquImpstVis1382011Null) {
        writeString(Pos.S089_IMPST_VIS1382011_NULL, wlquImpstVis1382011Null, Len.WLQU_IMPST_VIS1382011_NULL);
    }

    /**Original name: WLQU-IMPST-VIS-1382011-NULL<br>*/
    public String getWlquImpstVis1382011Null() {
        return readString(Pos.S089_IMPST_VIS1382011_NULL, Len.WLQU_IMPST_VIS1382011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPST_VIS1382011 = 1;
        public static final int S089_IMPST_VIS1382011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPST_VIS1382011 = 8;
        public static final int WLQU_IMPST_VIS1382011_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_VIS1382011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_VIS1382011 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
