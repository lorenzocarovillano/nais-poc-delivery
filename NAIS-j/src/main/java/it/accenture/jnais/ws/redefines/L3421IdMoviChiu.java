package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-ID-MOVI-CHIU<br>
 * Variable: L3421-ID-MOVI-CHIU from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_ID_MOVI_CHIU;
    }

    public void setL3421IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_ID_MOVI_CHIU, Pos.L3421_ID_MOVI_CHIU);
    }

    /**Original name: L3421-ID-MOVI-CHIU<br>*/
    public int getL3421IdMoviChiu() {
        return readPackedAsInt(Pos.L3421_ID_MOVI_CHIU, Len.Int.L3421_ID_MOVI_CHIU);
    }

    public byte[] getL3421IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_ID_MOVI_CHIU, Pos.L3421_ID_MOVI_CHIU);
        return buffer;
    }

    /**Original name: L3421-ID-MOVI-CHIU-NULL<br>*/
    public String getL3421IdMoviChiuNull() {
        return readString(Pos.L3421_ID_MOVI_CHIU_NULL, Len.L3421_ID_MOVI_CHIU_NULL);
    }

    public String getL3421IdMoviChiuNullFormatted() {
        return Functions.padBlanks(getL3421IdMoviChiuNull(), Len.L3421_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_ID_MOVI_CHIU = 1;
        public static final int L3421_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_ID_MOVI_CHIU = 5;
        public static final int L3421_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
