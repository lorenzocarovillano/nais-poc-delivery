package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPB-IS-K2-ANTIC<br>
 * Variable: WDFA-IMPB-IS-K2-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpbIsK2Antic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpbIsK2Antic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPB_IS_K2_ANTIC;
    }

    public void setWdfaImpbIsK2Antic(AfDecimal wdfaImpbIsK2Antic) {
        writeDecimalAsPacked(Pos.WDFA_IMPB_IS_K2_ANTIC, wdfaImpbIsK2Antic.copy());
    }

    public void setWdfaImpbIsK2AnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPB_IS_K2_ANTIC, Pos.WDFA_IMPB_IS_K2_ANTIC);
    }

    /**Original name: WDFA-IMPB-IS-K2-ANTIC<br>*/
    public AfDecimal getWdfaImpbIsK2Antic() {
        return readPackedAsDecimal(Pos.WDFA_IMPB_IS_K2_ANTIC, Len.Int.WDFA_IMPB_IS_K2_ANTIC, Len.Fract.WDFA_IMPB_IS_K2_ANTIC);
    }

    public byte[] getWdfaImpbIsK2AnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPB_IS_K2_ANTIC, Pos.WDFA_IMPB_IS_K2_ANTIC);
        return buffer;
    }

    public void setWdfaImpbIsK2AnticNull(String wdfaImpbIsK2AnticNull) {
        writeString(Pos.WDFA_IMPB_IS_K2_ANTIC_NULL, wdfaImpbIsK2AnticNull, Len.WDFA_IMPB_IS_K2_ANTIC_NULL);
    }

    /**Original name: WDFA-IMPB-IS-K2-ANTIC-NULL<br>*/
    public String getWdfaImpbIsK2AnticNull() {
        return readString(Pos.WDFA_IMPB_IS_K2_ANTIC_NULL, Len.WDFA_IMPB_IS_K2_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IS_K2_ANTIC = 1;
        public static final int WDFA_IMPB_IS_K2_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IS_K2_ANTIC = 8;
        public static final int WDFA_IMPB_IS_K2_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IS_K2_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IS_K2_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
