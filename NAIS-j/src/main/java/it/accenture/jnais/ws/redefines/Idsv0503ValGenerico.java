package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0503-VAL-GENERICO<br>
 * Variable: IDSV0503-VAL-GENERICO from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Idsv0503ValGenerico extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Idsv0503ValGenerico() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IDSV0503_VAL_GENERICO;
    }

    public void setIdsv0503ValGenericoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.IDSV0503_VAL_GENERICO, Pos.IDSV0503_VAL_GENERICO);
    }

    public void initIdsv0503ValGenericoSpaces() {
        fill(Pos.IDSV0503_VAL_GENERICO, Len.IDSV0503_VAL_GENERICO, Types.SPACE_CHAR);
    }

    /**Original name: IDSV0503-VAL-IMP<br>*/
    public AfDecimal getImp() {
        return readDecimal(Pos.IMP, Len.Int.IMP, Len.Fract.IMP);
    }

    /**Original name: IDSV0503-VAL-PERC<br>*/
    public AfDecimal getPerc() {
        return readDecimal(Pos.PERC, Len.Int.PERC, Len.Fract.PERC, SignType.NO_SIGN);
    }

    /**Original name: IDSV0503-VAL-STR<br>*/
    public String getStr() {
        return readString(Pos.STR, Len.STR);
    }

    public String getStrFormatted() {
        return Functions.padBlanks(getStr(), Len.STR);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int IDSV0503_VAL_GENERICO = 1;
        public static final int IDSV0503_VAL_IMP_GEN = 1;
        public static final int IMP = IDSV0503_VAL_IMP_GEN;
        public static final int IMP_FILLER = IMP + Len.IMP;
        public static final int IDSV0503_VAL_PERC_GEN = 1;
        public static final int PERC = IDSV0503_VAL_PERC_GEN;
        public static final int PERC_FILLER = PERC + Len.PERC;
        public static final int IDSV0503_VAL_STR_GEN = 1;
        public static final int STR = IDSV0503_VAL_STR_GEN;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int IMP = 18;
        public static final int PERC = 14;
        public static final int IDSV0503_VAL_GENERICO = 60;
        public static final int STR = 60;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IMP = 11;
            public static final int PERC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMP = 7;
            public static final int PERC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
