package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-COMMIS-INTER<br>
 * Variable: DTC-COMMIS-INTER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_COMMIS_INTER;
    }

    public void setDtcCommisInter(AfDecimal dtcCommisInter) {
        writeDecimalAsPacked(Pos.DTC_COMMIS_INTER, dtcCommisInter.copy());
    }

    public void setDtcCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_COMMIS_INTER, Pos.DTC_COMMIS_INTER);
    }

    /**Original name: DTC-COMMIS-INTER<br>*/
    public AfDecimal getDtcCommisInter() {
        return readPackedAsDecimal(Pos.DTC_COMMIS_INTER, Len.Int.DTC_COMMIS_INTER, Len.Fract.DTC_COMMIS_INTER);
    }

    public byte[] getDtcCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_COMMIS_INTER, Pos.DTC_COMMIS_INTER);
        return buffer;
    }

    public void setDtcCommisInterNull(String dtcCommisInterNull) {
        writeString(Pos.DTC_COMMIS_INTER_NULL, dtcCommisInterNull, Len.DTC_COMMIS_INTER_NULL);
    }

    /**Original name: DTC-COMMIS-INTER-NULL<br>*/
    public String getDtcCommisInterNull() {
        return readString(Pos.DTC_COMMIS_INTER_NULL, Len.DTC_COMMIS_INTER_NULL);
    }

    public String getDtcCommisInterNullFormatted() {
        return Functions.padBlanks(getDtcCommisInterNull(), Len.DTC_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_COMMIS_INTER = 1;
        public static final int DTC_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_COMMIS_INTER = 8;
        public static final int DTC_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
