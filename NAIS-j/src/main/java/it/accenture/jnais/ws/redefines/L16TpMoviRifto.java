package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L16-TP-MOVI-RIFTO<br>
 * Variable: L16-TP-MOVI-RIFTO from program IDBSL160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L16TpMoviRifto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L16TpMoviRifto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L16_TP_MOVI_RIFTO;
    }

    public void setL16TpMoviRifto(int l16TpMoviRifto) {
        writeIntAsPacked(Pos.L16_TP_MOVI_RIFTO, l16TpMoviRifto, Len.Int.L16_TP_MOVI_RIFTO);
    }

    public void setL16TpMoviRiftoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L16_TP_MOVI_RIFTO, Pos.L16_TP_MOVI_RIFTO);
    }

    /**Original name: L16-TP-MOVI-RIFTO<br>*/
    public int getL16TpMoviRifto() {
        return readPackedAsInt(Pos.L16_TP_MOVI_RIFTO, Len.Int.L16_TP_MOVI_RIFTO);
    }

    public byte[] getL16TpMoviRiftoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L16_TP_MOVI_RIFTO, Pos.L16_TP_MOVI_RIFTO);
        return buffer;
    }

    public void setL16TpMoviRiftoNull(String l16TpMoviRiftoNull) {
        writeString(Pos.L16_TP_MOVI_RIFTO_NULL, l16TpMoviRiftoNull, Len.L16_TP_MOVI_RIFTO_NULL);
    }

    /**Original name: L16-TP-MOVI-RIFTO-NULL<br>*/
    public String getL16TpMoviRiftoNull() {
        return readString(Pos.L16_TP_MOVI_RIFTO_NULL, Len.L16_TP_MOVI_RIFTO_NULL);
    }

    public String getL16TpMoviRiftoNullFormatted() {
        return Functions.padBlanks(getL16TpMoviRiftoNull(), Len.L16_TP_MOVI_RIFTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L16_TP_MOVI_RIFTO = 1;
        public static final int L16_TP_MOVI_RIFTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L16_TP_MOVI_RIFTO = 3;
        public static final int L16_TP_MOVI_RIFTO_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L16_TP_MOVI_RIFTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
