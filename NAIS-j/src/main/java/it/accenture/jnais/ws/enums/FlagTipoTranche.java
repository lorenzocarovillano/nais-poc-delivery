package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-TIPO-TRANCHE<br>
 * Variable: FLAG-TIPO-TRANCHE from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTipoTranche {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_TIPO_TRANCHE);
    public static final String INIT = "";
    public static final String CONTESTO = "TC";
    public static final String DB = "TD";

    //==== METHODS ====
    public void setFlagTipoTranche(String flagTipoTranche) {
        this.value = Functions.subString(flagTipoTranche, Len.FLAG_TIPO_TRANCHE);
    }

    public String getFlagTipoTranche() {
        return this.value;
    }

    public void setInit() {
        value = INIT;
    }

    public boolean isContesto() {
        return value.equals(CONTESTO);
    }

    public void setContesto() {
        value = CONTESTO;
    }

    public void setDb() {
        value = DB;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_TIPO_TRANCHE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
