package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-PRE-SOLO-RSH<br>
 * Variable: WDTR-PRE-SOLO-RSH from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrPreSoloRsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrPreSoloRsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_PRE_SOLO_RSH;
    }

    public void setWdtrPreSoloRsh(AfDecimal wdtrPreSoloRsh) {
        writeDecimalAsPacked(Pos.WDTR_PRE_SOLO_RSH, wdtrPreSoloRsh.copy());
    }

    /**Original name: WDTR-PRE-SOLO-RSH<br>*/
    public AfDecimal getWdtrPreSoloRsh() {
        return readPackedAsDecimal(Pos.WDTR_PRE_SOLO_RSH, Len.Int.WDTR_PRE_SOLO_RSH, Len.Fract.WDTR_PRE_SOLO_RSH);
    }

    public void setWdtrPreSoloRshNull(String wdtrPreSoloRshNull) {
        writeString(Pos.WDTR_PRE_SOLO_RSH_NULL, wdtrPreSoloRshNull, Len.WDTR_PRE_SOLO_RSH_NULL);
    }

    /**Original name: WDTR-PRE-SOLO-RSH-NULL<br>*/
    public String getWdtrPreSoloRshNull() {
        return readString(Pos.WDTR_PRE_SOLO_RSH_NULL, Len.WDTR_PRE_SOLO_RSH_NULL);
    }

    public String getWdtrPreSoloRshNullFormatted() {
        return Functions.padBlanks(getWdtrPreSoloRshNull(), Len.WDTR_PRE_SOLO_RSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_PRE_SOLO_RSH = 1;
        public static final int WDTR_PRE_SOLO_RSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_PRE_SOLO_RSH = 8;
        public static final int WDTR_PRE_SOLO_RSH_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_PRE_SOLO_RSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_PRE_SOLO_RSH = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
