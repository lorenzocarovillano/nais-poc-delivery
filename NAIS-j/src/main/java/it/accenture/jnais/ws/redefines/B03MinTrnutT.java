package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-MIN-TRNUT-T<br>
 * Variable: B03-MIN-TRNUT-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03MinTrnutT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03MinTrnutT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_MIN_TRNUT_T;
    }

    public void setB03MinTrnutT(AfDecimal b03MinTrnutT) {
        writeDecimalAsPacked(Pos.B03_MIN_TRNUT_T, b03MinTrnutT.copy());
    }

    public void setB03MinTrnutTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_MIN_TRNUT_T, Pos.B03_MIN_TRNUT_T);
    }

    /**Original name: B03-MIN-TRNUT-T<br>*/
    public AfDecimal getB03MinTrnutT() {
        return readPackedAsDecimal(Pos.B03_MIN_TRNUT_T, Len.Int.B03_MIN_TRNUT_T, Len.Fract.B03_MIN_TRNUT_T);
    }

    public byte[] getB03MinTrnutTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_MIN_TRNUT_T, Pos.B03_MIN_TRNUT_T);
        return buffer;
    }

    public void setB03MinTrnutTNull(String b03MinTrnutTNull) {
        writeString(Pos.B03_MIN_TRNUT_T_NULL, b03MinTrnutTNull, Len.B03_MIN_TRNUT_T_NULL);
    }

    /**Original name: B03-MIN-TRNUT-T-NULL<br>*/
    public String getB03MinTrnutTNull() {
        return readString(Pos.B03_MIN_TRNUT_T_NULL, Len.B03_MIN_TRNUT_T_NULL);
    }

    public String getB03MinTrnutTNullFormatted() {
        return Functions.padBlanks(getB03MinTrnutTNull(), Len.B03_MIN_TRNUT_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_MIN_TRNUT_T = 1;
        public static final int B03_MIN_TRNUT_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_MIN_TRNUT_T = 8;
        public static final int B03_MIN_TRNUT_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_MIN_TRNUT_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_MIN_TRNUT_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
