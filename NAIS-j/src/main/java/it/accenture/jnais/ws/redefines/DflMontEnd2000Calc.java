package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-MONT-END2000-CALC<br>
 * Variable: DFL-MONT-END2000-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflMontEnd2000Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflMontEnd2000Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_MONT_END2000_CALC;
    }

    public void setDflMontEnd2000Calc(AfDecimal dflMontEnd2000Calc) {
        writeDecimalAsPacked(Pos.DFL_MONT_END2000_CALC, dflMontEnd2000Calc.copy());
    }

    public void setDflMontEnd2000CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_MONT_END2000_CALC, Pos.DFL_MONT_END2000_CALC);
    }

    /**Original name: DFL-MONT-END2000-CALC<br>*/
    public AfDecimal getDflMontEnd2000Calc() {
        return readPackedAsDecimal(Pos.DFL_MONT_END2000_CALC, Len.Int.DFL_MONT_END2000_CALC, Len.Fract.DFL_MONT_END2000_CALC);
    }

    public byte[] getDflMontEnd2000CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_MONT_END2000_CALC, Pos.DFL_MONT_END2000_CALC);
        return buffer;
    }

    public void setDflMontEnd2000CalcNull(String dflMontEnd2000CalcNull) {
        writeString(Pos.DFL_MONT_END2000_CALC_NULL, dflMontEnd2000CalcNull, Len.DFL_MONT_END2000_CALC_NULL);
    }

    /**Original name: DFL-MONT-END2000-CALC-NULL<br>*/
    public String getDflMontEnd2000CalcNull() {
        return readString(Pos.DFL_MONT_END2000_CALC_NULL, Len.DFL_MONT_END2000_CALC_NULL);
    }

    public String getDflMontEnd2000CalcNullFormatted() {
        return Functions.padBlanks(getDflMontEnd2000CalcNull(), Len.DFL_MONT_END2000_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_MONT_END2000_CALC = 1;
        public static final int DFL_MONT_END2000_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_MONT_END2000_CALC = 8;
        public static final int DFL_MONT_END2000_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_MONT_END2000_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_MONT_END2000_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
