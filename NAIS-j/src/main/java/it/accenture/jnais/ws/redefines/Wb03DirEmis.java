package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DIR-EMIS<br>
 * Variable: WB03-DIR-EMIS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DirEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DirEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DIR_EMIS;
    }

    public void setWb03DirEmis(AfDecimal wb03DirEmis) {
        writeDecimalAsPacked(Pos.WB03_DIR_EMIS, wb03DirEmis.copy());
    }

    public void setWb03DirEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DIR_EMIS, Pos.WB03_DIR_EMIS);
    }

    /**Original name: WB03-DIR-EMIS<br>*/
    public AfDecimal getWb03DirEmis() {
        return readPackedAsDecimal(Pos.WB03_DIR_EMIS, Len.Int.WB03_DIR_EMIS, Len.Fract.WB03_DIR_EMIS);
    }

    public byte[] getWb03DirEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DIR_EMIS, Pos.WB03_DIR_EMIS);
        return buffer;
    }

    public void setWb03DirEmisNull(String wb03DirEmisNull) {
        writeString(Pos.WB03_DIR_EMIS_NULL, wb03DirEmisNull, Len.WB03_DIR_EMIS_NULL);
    }

    /**Original name: WB03-DIR-EMIS-NULL<br>*/
    public String getWb03DirEmisNull() {
        return readString(Pos.WB03_DIR_EMIS_NULL, Len.WB03_DIR_EMIS_NULL);
    }

    public String getWb03DirEmisNullFormatted() {
        return Functions.padBlanks(getWb03DirEmisNull(), Len.WB03_DIR_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DIR_EMIS = 1;
        public static final int WB03_DIR_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DIR_EMIS = 8;
        public static final int WB03_DIR_EMIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DIR_EMIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_DIR_EMIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
