package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-SOPR-SAN<br>
 * Variable: DTR-SOPR-SAN from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_SOPR_SAN;
    }

    public void setDtrSoprSan(AfDecimal dtrSoprSan) {
        writeDecimalAsPacked(Pos.DTR_SOPR_SAN, dtrSoprSan.copy());
    }

    public void setDtrSoprSanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_SOPR_SAN, Pos.DTR_SOPR_SAN);
    }

    /**Original name: DTR-SOPR-SAN<br>*/
    public AfDecimal getDtrSoprSan() {
        return readPackedAsDecimal(Pos.DTR_SOPR_SAN, Len.Int.DTR_SOPR_SAN, Len.Fract.DTR_SOPR_SAN);
    }

    public byte[] getDtrSoprSanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_SOPR_SAN, Pos.DTR_SOPR_SAN);
        return buffer;
    }

    public void setDtrSoprSanNull(String dtrSoprSanNull) {
        writeString(Pos.DTR_SOPR_SAN_NULL, dtrSoprSanNull, Len.DTR_SOPR_SAN_NULL);
    }

    /**Original name: DTR-SOPR-SAN-NULL<br>*/
    public String getDtrSoprSanNull() {
        return readString(Pos.DTR_SOPR_SAN_NULL, Len.DTR_SOPR_SAN_NULL);
    }

    public String getDtrSoprSanNullFormatted() {
        return Functions.padBlanks(getDtrSoprSanNull(), Len.DTR_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_SOPR_SAN = 1;
        public static final int DTR_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_SOPR_SAN = 8;
        public static final int DTR_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
