package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-PROV-INC<br>
 * Variable: WTIT-TOT-PROV-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_PROV_INC;
    }

    public void setWtitTotProvInc(AfDecimal wtitTotProvInc) {
        writeDecimalAsPacked(Pos.WTIT_TOT_PROV_INC, wtitTotProvInc.copy());
    }

    public void setWtitTotProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_PROV_INC, Pos.WTIT_TOT_PROV_INC);
    }

    /**Original name: WTIT-TOT-PROV-INC<br>*/
    public AfDecimal getWtitTotProvInc() {
        return readPackedAsDecimal(Pos.WTIT_TOT_PROV_INC, Len.Int.WTIT_TOT_PROV_INC, Len.Fract.WTIT_TOT_PROV_INC);
    }

    public byte[] getWtitTotProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_PROV_INC, Pos.WTIT_TOT_PROV_INC);
        return buffer;
    }

    public void initWtitTotProvIncSpaces() {
        fill(Pos.WTIT_TOT_PROV_INC, Len.WTIT_TOT_PROV_INC, Types.SPACE_CHAR);
    }

    public void setWtitTotProvIncNull(String wtitTotProvIncNull) {
        writeString(Pos.WTIT_TOT_PROV_INC_NULL, wtitTotProvIncNull, Len.WTIT_TOT_PROV_INC_NULL);
    }

    /**Original name: WTIT-TOT-PROV-INC-NULL<br>*/
    public String getWtitTotProvIncNull() {
        return readString(Pos.WTIT_TOT_PROV_INC_NULL, Len.WTIT_TOT_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PROV_INC = 1;
        public static final int WTIT_TOT_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PROV_INC = 8;
        public static final int WTIT_TOT_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
