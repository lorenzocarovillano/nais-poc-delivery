package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPB-IS-662014<br>
 * Variable: S089-IMPB-IS-662014 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpbIs662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpbIs662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPB_IS662014;
    }

    public void setWlquImpbIs662014(AfDecimal wlquImpbIs662014) {
        writeDecimalAsPacked(Pos.S089_IMPB_IS662014, wlquImpbIs662014.copy());
    }

    public void setWlquImpbIs662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPB_IS662014, Pos.S089_IMPB_IS662014);
    }

    /**Original name: WLQU-IMPB-IS-662014<br>*/
    public AfDecimal getWlquImpbIs662014() {
        return readPackedAsDecimal(Pos.S089_IMPB_IS662014, Len.Int.WLQU_IMPB_IS662014, Len.Fract.WLQU_IMPB_IS662014);
    }

    public byte[] getWlquImpbIs662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPB_IS662014, Pos.S089_IMPB_IS662014);
        return buffer;
    }

    public void initWlquImpbIs662014Spaces() {
        fill(Pos.S089_IMPB_IS662014, Len.S089_IMPB_IS662014, Types.SPACE_CHAR);
    }

    public void setWlquImpbIs662014Null(String wlquImpbIs662014Null) {
        writeString(Pos.S089_IMPB_IS662014_NULL, wlquImpbIs662014Null, Len.WLQU_IMPB_IS662014_NULL);
    }

    /**Original name: WLQU-IMPB-IS-662014-NULL<br>*/
    public String getWlquImpbIs662014Null() {
        return readString(Pos.S089_IMPB_IS662014_NULL, Len.WLQU_IMPB_IS662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPB_IS662014 = 1;
        public static final int S089_IMPB_IS662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPB_IS662014 = 8;
        public static final int WLQU_IMPB_IS662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_IS662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_IS662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
