package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.occurs.ComodoStatBusEle;
import it.accenture.jnais.ws.occurs.ComodoTpCausEle;

/**Original name: COMODO-STAT-CAUS<br>
 * Variable: COMODO-STAT-CAUS from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ComodoStatCaus {

    //==== PROPERTIES ====
    public static final int STAT_BUS_ELE_MAXOCCURS = 7;
    public static final int TP_CAUS_ELE_MAXOCCURS = 7;
    //Original name: COMODO-OPER-LOG-STAT-BUS
    private String operLogStatBus = DefaultValues.stringVal(Len.OPER_LOG_STAT_BUS);
    //Original name: COMODO-STAT-BUS-ELE
    private ComodoStatBusEle[] statBusEle = new ComodoStatBusEle[STAT_BUS_ELE_MAXOCCURS];
    //Original name: COMODO-OPER-LOG-CAUS
    private String operLogCaus = DefaultValues.stringVal(Len.OPER_LOG_CAUS);
    //Original name: COMODO-TP-CAUS-ELE
    private ComodoTpCausEle[] tpCausEle = new ComodoTpCausEle[TP_CAUS_ELE_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public ComodoStatCaus() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int statBusEleIdx = 1; statBusEleIdx <= STAT_BUS_ELE_MAXOCCURS; statBusEleIdx++) {
            statBusEle[statBusEleIdx - 1] = new ComodoStatBusEle();
        }
        for (int tpCausEleIdx = 1; tpCausEleIdx <= TP_CAUS_ELE_MAXOCCURS; tpCausEleIdx++) {
            tpCausEle[tpCausEleIdx - 1] = new ComodoTpCausEle();
        }
    }

    public void setOperLogStatBus(String operLogStatBus) {
        this.operLogStatBus = Functions.subString(operLogStatBus, Len.OPER_LOG_STAT_BUS);
    }

    public String getOperLogStatBus() {
        return this.operLogStatBus;
    }

    /**Original name: COMODO-STAT-BUS-TAB<br>*/
    public byte[] getStatBusTabBytes() {
        byte[] buffer = new byte[Len.STAT_BUS_TAB];
        return getStatBusTabBytes(buffer, 1);
    }

    public byte[] getStatBusTabBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= STAT_BUS_ELE_MAXOCCURS; idx++) {
            statBusEle[idx - 1].getStatBusEleBytes(buffer, position);
            position += ComodoStatBusEle.Len.STAT_BUS_ELE;
        }
        return buffer;
    }

    public void setOperLogCaus(String operLogCaus) {
        this.operLogCaus = Functions.subString(operLogCaus, Len.OPER_LOG_CAUS);
    }

    public String getOperLogCaus() {
        return this.operLogCaus;
    }

    /**Original name: COMODO-TP-CAUS-TAB<br>*/
    public byte[] getTpCausTabBytes() {
        byte[] buffer = new byte[Len.TP_CAUS_TAB];
        return getTpCausTabBytes(buffer, 1);
    }

    public byte[] getTpCausTabBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TP_CAUS_ELE_MAXOCCURS; idx++) {
            tpCausEle[idx - 1].getTpCausEleBytes(buffer, position);
            position += ComodoTpCausEle.Len.TP_CAUS_ELE;
        }
        return buffer;
    }

    public ComodoStatBusEle getStatBusEle(int idx) {
        return statBusEle[idx - 1];
    }

    public ComodoTpCausEle getTpCausEle(int idx) {
        return tpCausEle[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OPER_LOG_STAT_BUS = 3;
        public static final int OPER_LOG_CAUS = 3;
        public static final int STAT_BUS_TAB = ComodoStatCaus.STAT_BUS_ELE_MAXOCCURS * ComodoStatBusEle.Len.STAT_BUS_ELE;
        public static final int TP_CAUS_TAB = ComodoStatCaus.TP_CAUS_ELE_MAXOCCURS * ComodoTpCausEle.Len.TP_CAUS_ELE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
