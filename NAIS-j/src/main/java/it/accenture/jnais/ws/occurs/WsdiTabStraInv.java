package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvsdi1;
import it.accenture.jnais.copy.WsdiDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WSDI-TAB-STRA-INV<br>
 * Variables: WSDI-TAB-STRA-INV from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WsdiTabStraInv {

    //==== PROPERTIES ====
    //Original name: LCCVSDI1
    private Lccvsdi1 lccvsdi1 = new Lccvsdi1();

    //==== METHODS ====
    public void setVsdiTabStraInvBytes(byte[] buffer) {
        setWsdiTabStraInvBytes(buffer, 1);
    }

    public byte[] getTabStraInvBytes() {
        byte[] buffer = new byte[Len.WSDI_TAB_STRA_INV];
        return getWsdiTabStraInvBytes(buffer, 1);
    }

    public void setWsdiTabStraInvBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvsdi1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvsdi1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvsdi1.Len.Int.ID_PTF, 0));
        position += Lccvsdi1.Len.ID_PTF;
        lccvsdi1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWsdiTabStraInvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvsdi1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvsdi1.getIdPtf(), Lccvsdi1.Len.Int.ID_PTF, 0);
        position += Lccvsdi1.Len.ID_PTF;
        lccvsdi1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWsdiTabStraInvSpaces() {
        lccvsdi1.initLccvsdi1Spaces();
    }

    public Lccvsdi1 getLccvsdi1() {
        return lccvsdi1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WSDI_TAB_STRA_INV = WpolStatus.Len.STATUS + Lccvsdi1.Len.ID_PTF + WsdiDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
