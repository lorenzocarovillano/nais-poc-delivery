package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-ABB-ANNO-ULT<br>
 * Variable: WPAG-IMP-ABB-ANNO-ULT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpAbbAnnoUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpAbbAnnoUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_ABB_ANNO_ULT;
    }

    public void setWpagImpAbbAnnoUlt(AfDecimal wpagImpAbbAnnoUlt) {
        writeDecimalAsPacked(Pos.WPAG_IMP_ABB_ANNO_ULT, wpagImpAbbAnnoUlt.copy());
    }

    public void setWpagImpAbbAnnoUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_ABB_ANNO_ULT, Pos.WPAG_IMP_ABB_ANNO_ULT);
    }

    /**Original name: WPAG-IMP-ABB-ANNO-ULT<br>*/
    public AfDecimal getWpagImpAbbAnnoUlt() {
        return readPackedAsDecimal(Pos.WPAG_IMP_ABB_ANNO_ULT, Len.Int.WPAG_IMP_ABB_ANNO_ULT, Len.Fract.WPAG_IMP_ABB_ANNO_ULT);
    }

    public byte[] getWpagImpAbbAnnoUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_ABB_ANNO_ULT, Pos.WPAG_IMP_ABB_ANNO_ULT);
        return buffer;
    }

    public void initWpagImpAbbAnnoUltSpaces() {
        fill(Pos.WPAG_IMP_ABB_ANNO_ULT, Len.WPAG_IMP_ABB_ANNO_ULT, Types.SPACE_CHAR);
    }

    public void setWpagImpAbbAnnoUltNull(String wpagImpAbbAnnoUltNull) {
        writeString(Pos.WPAG_IMP_ABB_ANNO_ULT_NULL, wpagImpAbbAnnoUltNull, Len.WPAG_IMP_ABB_ANNO_ULT_NULL);
    }

    /**Original name: WPAG-IMP-ABB-ANNO-ULT-NULL<br>*/
    public String getWpagImpAbbAnnoUltNull() {
        return readString(Pos.WPAG_IMP_ABB_ANNO_ULT_NULL, Len.WPAG_IMP_ABB_ANNO_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ABB_ANNO_ULT = 1;
        public static final int WPAG_IMP_ABB_ANNO_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ABB_ANNO_ULT = 8;
        public static final int WPAG_IMP_ABB_ANNO_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ABB_ANNO_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ABB_ANNO_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
