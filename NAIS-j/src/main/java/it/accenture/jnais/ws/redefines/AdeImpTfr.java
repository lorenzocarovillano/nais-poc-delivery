package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-IMP-TFR<br>
 * Variable: ADE-IMP-TFR from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_IMP_TFR;
    }

    public void setAdeImpTfr(AfDecimal adeImpTfr) {
        writeDecimalAsPacked(Pos.ADE_IMP_TFR, adeImpTfr.copy());
    }

    public void setAdeImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_IMP_TFR, Pos.ADE_IMP_TFR);
    }

    /**Original name: ADE-IMP-TFR<br>*/
    public AfDecimal getAdeImpTfr() {
        return readPackedAsDecimal(Pos.ADE_IMP_TFR, Len.Int.ADE_IMP_TFR, Len.Fract.ADE_IMP_TFR);
    }

    public byte[] getAdeImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_IMP_TFR, Pos.ADE_IMP_TFR);
        return buffer;
    }

    public void setAdeImpTfrNull(String adeImpTfrNull) {
        writeString(Pos.ADE_IMP_TFR_NULL, adeImpTfrNull, Len.ADE_IMP_TFR_NULL);
    }

    /**Original name: ADE-IMP-TFR-NULL<br>*/
    public String getAdeImpTfrNull() {
        return readString(Pos.ADE_IMP_TFR_NULL, Len.ADE_IMP_TFR_NULL);
    }

    public String getAdeImpTfrNullFormatted() {
        return Functions.padBlanks(getAdeImpTfrNull(), Len.ADE_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_IMP_TFR = 1;
        public static final int ADE_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_IMP_TFR = 8;
        public static final int ADE_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
