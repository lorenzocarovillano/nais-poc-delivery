package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-COD-AGE<br>
 * Variable: B03-COD-AGE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CodAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CodAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_COD_AGE;
    }

    public void setB03CodAge(int b03CodAge) {
        writeIntAsPacked(Pos.B03_COD_AGE, b03CodAge, Len.Int.B03_COD_AGE);
    }

    public void setB03CodAgeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_COD_AGE, Pos.B03_COD_AGE);
    }

    /**Original name: B03-COD-AGE<br>*/
    public int getB03CodAge() {
        return readPackedAsInt(Pos.B03_COD_AGE, Len.Int.B03_COD_AGE);
    }

    public byte[] getB03CodAgeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_COD_AGE, Pos.B03_COD_AGE);
        return buffer;
    }

    public void setB03CodAgeNull(String b03CodAgeNull) {
        writeString(Pos.B03_COD_AGE_NULL, b03CodAgeNull, Len.B03_COD_AGE_NULL);
    }

    /**Original name: B03-COD-AGE-NULL<br>*/
    public String getB03CodAgeNull() {
        return readString(Pos.B03_COD_AGE_NULL, Len.B03_COD_AGE_NULL);
    }

    public String getB03CodAgeNullFormatted() {
        return Functions.padBlanks(getB03CodAgeNull(), Len.B03_COD_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_COD_AGE = 1;
        public static final int B03_COD_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_COD_AGE = 3;
        public static final int B03_COD_AGE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_COD_AGE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
