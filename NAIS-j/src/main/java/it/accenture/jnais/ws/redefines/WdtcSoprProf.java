package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-SOPR-PROF<br>
 * Variable: WDTC-SOPR-PROF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_SOPR_PROF;
    }

    public void setWdtcSoprProf(AfDecimal wdtcSoprProf) {
        writeDecimalAsPacked(Pos.WDTC_SOPR_PROF, wdtcSoprProf.copy());
    }

    public void setWdtcSoprProfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_SOPR_PROF, Pos.WDTC_SOPR_PROF);
    }

    /**Original name: WDTC-SOPR-PROF<br>*/
    public AfDecimal getWdtcSoprProf() {
        return readPackedAsDecimal(Pos.WDTC_SOPR_PROF, Len.Int.WDTC_SOPR_PROF, Len.Fract.WDTC_SOPR_PROF);
    }

    public byte[] getWdtcSoprProfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_SOPR_PROF, Pos.WDTC_SOPR_PROF);
        return buffer;
    }

    public void initWdtcSoprProfSpaces() {
        fill(Pos.WDTC_SOPR_PROF, Len.WDTC_SOPR_PROF, Types.SPACE_CHAR);
    }

    public void setWdtcSoprProfNull(String wdtcSoprProfNull) {
        writeString(Pos.WDTC_SOPR_PROF_NULL, wdtcSoprProfNull, Len.WDTC_SOPR_PROF_NULL);
    }

    /**Original name: WDTC-SOPR-PROF-NULL<br>*/
    public String getWdtcSoprProfNull() {
        return readString(Pos.WDTC_SOPR_PROF_NULL, Len.WDTC_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_SOPR_PROF = 1;
        public static final int WDTC_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_SOPR_PROF = 8;
        public static final int WDTC_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
