package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WALL-TABELLA<br>
 * Variable: WALL-TABELLA from copybook LCCVALL7<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class WallTabella extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_ASSET_ALL_MAXOCCURS = 220;
    public static final char ST_ADD = 'A';
    public static final char ST_MOD = 'M';
    public static final char ST_INV = 'I';
    public static final char ST_DEL = 'D';
    public static final char ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public WallTabella() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TABELLA;
    }

    public void setTabellaBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TABELLA, Pos.TABELLA);
    }

    public byte[] getTabellaBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TABELLA, Pos.TABELLA);
        return buffer;
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.wallStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: WALL-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA AST_ALLOC
	 *    ALIAS ALL
	 *    ULTIMO AGG. 31 MAR 2020
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.wallStatus(statusIdx - 1);
        return readChar(position);
    }

    public boolean isStDel(int stDelIdx) {
        return getStatus(stDelIdx) == ST_DEL;
    }

    public boolean isStCon(int stConIdx) {
        return getStatus(stConIdx) == ST_CON;
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.wallIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: WALL-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.wallIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdAstAlloc(int idAstAllocIdx, int idAstAlloc) {
        int position = Pos.wallIdAstAlloc(idAstAllocIdx - 1);
        writeIntAsPacked(position, idAstAlloc, Len.Int.ID_AST_ALLOC);
    }

    /**Original name: WALL-ID-AST-ALLOC<br>*/
    public int getIdAstAlloc(int idAstAllocIdx) {
        int position = Pos.wallIdAstAlloc(idAstAllocIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_AST_ALLOC);
    }

    public void setIdStraDiInvst(int idStraDiInvstIdx, int idStraDiInvst) {
        int position = Pos.wallIdStraDiInvst(idStraDiInvstIdx - 1);
        writeIntAsPacked(position, idStraDiInvst, Len.Int.ID_STRA_DI_INVST);
    }

    /**Original name: WALL-ID-STRA-DI-INVST<br>*/
    public int getIdStraDiInvst(int idStraDiInvstIdx) {
        int position = Pos.wallIdStraDiInvst(idStraDiInvstIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_STRA_DI_INVST);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.wallIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: WALL-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.wallIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.wallIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: WALL-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.wallIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setDtIniVldt(int dtIniVldtIdx, int dtIniVldt) {
        int position = Pos.wallDtIniVldt(dtIniVldtIdx - 1);
        writeIntAsPacked(position, dtIniVldt, Len.Int.DT_INI_VLDT);
    }

    /**Original name: WALL-DT-INI-VLDT<br>*/
    public int getDtIniVldt(int dtIniVldtIdx) {
        int position = Pos.wallDtIniVldt(dtIniVldtIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_VLDT);
    }

    public void setDtEndVldt(int dtEndVldtIdx, int dtEndVldt) {
        int position = Pos.wallDtEndVldt(dtEndVldtIdx - 1);
        writeIntAsPacked(position, dtEndVldt, Len.Int.DT_END_VLDT);
    }

    /**Original name: WALL-DT-END-VLDT<br>*/
    public int getDtEndVldt(int dtEndVldtIdx) {
        int position = Pos.wallDtEndVldt(dtEndVldtIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_VLDT);
    }

    /**Original name: WALL-DT-END-VLDT-NULL<br>*/
    public String getDtEndVldtNull(int dtEndVldtNullIdx) {
        int position = Pos.wallDtEndVldtNull(dtEndVldtNullIdx - 1);
        return readString(position, Len.DT_END_VLDT_NULL);
    }

    public String getDtEndVldtNullFormatted(int dtEndVldtNullIdx) {
        return Functions.padBlanks(getDtEndVldtNull(dtEndVldtNullIdx), Len.DT_END_VLDT_NULL);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.wallDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: WALL-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.wallDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.wallDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: WALL-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.wallDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.wallCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: WALL-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wallCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setCodFnd(int codFndIdx, String codFnd) {
        int position = Pos.wallCodFnd(codFndIdx - 1);
        writeString(position, codFnd, Len.COD_FND);
    }

    /**Original name: WALL-COD-FND<br>*/
    public String getCodFnd(int codFndIdx) {
        int position = Pos.wallCodFnd(codFndIdx - 1);
        return readString(position, Len.COD_FND);
    }

    public void setCodTari(int codTariIdx, String codTari) {
        int position = Pos.wallCodTari(codTariIdx - 1);
        writeString(position, codTari, Len.COD_TARI);
    }

    /**Original name: WALL-COD-TARI<br>*/
    public String getCodTari(int codTariIdx) {
        int position = Pos.wallCodTari(codTariIdx - 1);
        return readString(position, Len.COD_TARI);
    }

    /**Original name: WALL-COD-TARI-NULL<br>*/
    public String getCodTariNull(int codTariNullIdx) {
        int position = Pos.wallCodTariNull(codTariNullIdx - 1);
        return readString(position, Len.COD_TARI_NULL);
    }

    public String getCodTariNullFormatted(int codTariNullIdx) {
        return Functions.padBlanks(getCodTariNull(codTariNullIdx), Len.COD_TARI_NULL);
    }

    public void setTpApplzAst(int tpApplzAstIdx, String tpApplzAst) {
        int position = Pos.wallTpApplzAst(tpApplzAstIdx - 1);
        writeString(position, tpApplzAst, Len.TP_APPLZ_AST);
    }

    /**Original name: WALL-TP-APPLZ-AST<br>*/
    public String getTpApplzAst(int tpApplzAstIdx) {
        int position = Pos.wallTpApplzAst(tpApplzAstIdx - 1);
        return readString(position, Len.TP_APPLZ_AST);
    }

    public void setPcRipAst(int pcRipAstIdx, AfDecimal pcRipAst) {
        int position = Pos.wallPcRipAst(pcRipAstIdx - 1);
        writeDecimalAsPacked(position, pcRipAst.copy());
    }

    /**Original name: WALL-PC-RIP-AST<br>*/
    public AfDecimal getPcRipAst(int pcRipAstIdx) {
        int position = Pos.wallPcRipAst(pcRipAstIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_RIP_AST, Len.Fract.PC_RIP_AST);
    }

    public void setTpFnd(int tpFndIdx, char tpFnd) {
        int position = Pos.wallTpFnd(tpFndIdx - 1);
        writeChar(position, tpFnd);
    }

    /**Original name: WALL-TP-FND<br>*/
    public char getTpFnd(int tpFndIdx) {
        int position = Pos.wallTpFnd(tpFndIdx - 1);
        return readChar(position);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.wallDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: WALL-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.wallDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.wallDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: WALL-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.wallDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.wallDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: WALL-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.wallDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.wallDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: WALL-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.wallDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.wallDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: WALL-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.wallDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.wallDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: WALL-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.wallDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.wallDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: WALL-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.wallDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setPeriodo(int periodoIdx, short periodo) {
        int position = Pos.wallPeriodo(periodoIdx - 1);
        writeShortAsPacked(position, periodo, Len.Int.PERIODO);
    }

    /**Original name: WALL-PERIODO<br>*/
    public short getPeriodo(int periodoIdx) {
        int position = Pos.wallPeriodo(periodoIdx - 1);
        return readPackedAsShort(position, Len.Int.PERIODO);
    }

    public void setTpRibilFnd(int tpRibilFndIdx, String tpRibilFnd) {
        int position = Pos.wallTpRibilFnd(tpRibilFndIdx - 1);
        writeString(position, tpRibilFnd, Len.TP_RIBIL_FND);
    }

    /**Original name: WALL-TP-RIBIL-FND<br>*/
    public String getTpRibilFnd(int tpRibilFndIdx) {
        int position = Pos.wallTpRibilFnd(tpRibilFndIdx - 1);
        return readString(position, Len.TP_RIBIL_FND);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TABELLA = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wallTabAssetAll(int idx) {
            return TABELLA + idx * Len.TAB_ASSET_ALL;
        }

        public static int wallStatus(int idx) {
            return wallTabAssetAll(idx);
        }

        public static int wallIdPtf(int idx) {
            return wallStatus(idx) + Len.STATUS;
        }

        public static int wallDati(int idx) {
            return wallIdPtf(idx) + Len.ID_PTF;
        }

        public static int wallIdAstAlloc(int idx) {
            return wallDati(idx);
        }

        public static int wallIdStraDiInvst(int idx) {
            return wallIdAstAlloc(idx) + Len.ID_AST_ALLOC;
        }

        public static int wallIdMoviCrz(int idx) {
            return wallIdStraDiInvst(idx) + Len.ID_STRA_DI_INVST;
        }

        public static int wallIdMoviChiu(int idx) {
            return wallIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int wallIdMoviChiuNull(int idx) {
            return wallIdMoviChiu(idx);
        }

        public static int wallDtIniVldt(int idx) {
            return wallIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int wallDtEndVldt(int idx) {
            return wallDtIniVldt(idx) + Len.DT_INI_VLDT;
        }

        public static int wallDtEndVldtNull(int idx) {
            return wallDtEndVldt(idx);
        }

        public static int wallDtIniEff(int idx) {
            return wallDtEndVldt(idx) + Len.DT_END_VLDT;
        }

        public static int wallDtEndEff(int idx) {
            return wallDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int wallCodCompAnia(int idx) {
            return wallDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int wallCodFnd(int idx) {
            return wallCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wallCodFndNull(int idx) {
            return wallCodFnd(idx);
        }

        public static int wallCodTari(int idx) {
            return wallCodFnd(idx) + Len.COD_FND;
        }

        public static int wallCodTariNull(int idx) {
            return wallCodTari(idx);
        }

        public static int wallTpApplzAst(int idx) {
            return wallCodTari(idx) + Len.COD_TARI;
        }

        public static int wallTpApplzAstNull(int idx) {
            return wallTpApplzAst(idx);
        }

        public static int wallPcRipAst(int idx) {
            return wallTpApplzAst(idx) + Len.TP_APPLZ_AST;
        }

        public static int wallPcRipAstNull(int idx) {
            return wallPcRipAst(idx);
        }

        public static int wallTpFnd(int idx) {
            return wallPcRipAst(idx) + Len.PC_RIP_AST;
        }

        public static int wallDsRiga(int idx) {
            return wallTpFnd(idx) + Len.TP_FND;
        }

        public static int wallDsOperSql(int idx) {
            return wallDsRiga(idx) + Len.DS_RIGA;
        }

        public static int wallDsVer(int idx) {
            return wallDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int wallDsTsIniCptz(int idx) {
            return wallDsVer(idx) + Len.DS_VER;
        }

        public static int wallDsTsEndCptz(int idx) {
            return wallDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int wallDsUtente(int idx) {
            return wallDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int wallDsStatoElab(int idx) {
            return wallDsUtente(idx) + Len.DS_UTENTE;
        }

        public static int wallPeriodo(int idx) {
            return wallDsStatoElab(idx) + Len.DS_STATO_ELAB;
        }

        public static int wallPeriodoNull(int idx) {
            return wallPeriodo(idx);
        }

        public static int wallTpRibilFnd(int idx) {
            return wallPeriodo(idx) + Len.PERIODO;
        }

        public static int wallTpRibilFndNull(int idx) {
            return wallTpRibilFnd(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_AST_ALLOC = 5;
        public static final int ID_STRA_DI_INVST = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_VLDT = 5;
        public static final int DT_END_VLDT = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int COD_FND = 20;
        public static final int COD_TARI = 12;
        public static final int TP_APPLZ_AST = 2;
        public static final int PC_RIP_AST = 4;
        public static final int TP_FND = 1;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int PERIODO = 2;
        public static final int TP_RIBIL_FND = 2;
        public static final int DATI = ID_AST_ALLOC + ID_STRA_DI_INVST + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_VLDT + DT_END_VLDT + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + COD_FND + COD_TARI + TP_APPLZ_AST + PC_RIP_AST + TP_FND + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + PERIODO + TP_RIBIL_FND;
        public static final int TAB_ASSET_ALL = STATUS + ID_PTF + DATI;
        public static final int TABELLA = WallTabella.TAB_ASSET_ALL_MAXOCCURS * TAB_ASSET_ALL;
        public static final int COD_TARI_NULL = 12;
        public static final int DT_END_VLDT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_AST_ALLOC = 9;
            public static final int ID_STRA_DI_INVST = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_VLDT = 8;
            public static final int DT_END_VLDT = 8;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int PC_RIP_AST = 3;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int PERIODO = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PC_RIP_AST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
