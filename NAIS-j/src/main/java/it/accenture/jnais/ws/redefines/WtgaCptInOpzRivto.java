package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-CPT-IN-OPZ-RIVTO<br>
 * Variable: WTGA-CPT-IN-OPZ-RIVTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaCptInOpzRivto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaCptInOpzRivto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_CPT_IN_OPZ_RIVTO;
    }

    public void setWtgaCptInOpzRivto(AfDecimal wtgaCptInOpzRivto) {
        writeDecimalAsPacked(Pos.WTGA_CPT_IN_OPZ_RIVTO, wtgaCptInOpzRivto.copy());
    }

    public void setWtgaCptInOpzRivtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_CPT_IN_OPZ_RIVTO, Pos.WTGA_CPT_IN_OPZ_RIVTO);
    }

    /**Original name: WTGA-CPT-IN-OPZ-RIVTO<br>*/
    public AfDecimal getWtgaCptInOpzRivto() {
        return readPackedAsDecimal(Pos.WTGA_CPT_IN_OPZ_RIVTO, Len.Int.WTGA_CPT_IN_OPZ_RIVTO, Len.Fract.WTGA_CPT_IN_OPZ_RIVTO);
    }

    public byte[] getWtgaCptInOpzRivtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_CPT_IN_OPZ_RIVTO, Pos.WTGA_CPT_IN_OPZ_RIVTO);
        return buffer;
    }

    public void initWtgaCptInOpzRivtoSpaces() {
        fill(Pos.WTGA_CPT_IN_OPZ_RIVTO, Len.WTGA_CPT_IN_OPZ_RIVTO, Types.SPACE_CHAR);
    }

    public void setWtgaCptInOpzRivtoNull(String wtgaCptInOpzRivtoNull) {
        writeString(Pos.WTGA_CPT_IN_OPZ_RIVTO_NULL, wtgaCptInOpzRivtoNull, Len.WTGA_CPT_IN_OPZ_RIVTO_NULL);
    }

    /**Original name: WTGA-CPT-IN-OPZ-RIVTO-NULL<br>*/
    public String getWtgaCptInOpzRivtoNull() {
        return readString(Pos.WTGA_CPT_IN_OPZ_RIVTO_NULL, Len.WTGA_CPT_IN_OPZ_RIVTO_NULL);
    }

    public String getWtgaCptInOpzRivtoNullFormatted() {
        return Functions.padBlanks(getWtgaCptInOpzRivtoNull(), Len.WTGA_CPT_IN_OPZ_RIVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_CPT_IN_OPZ_RIVTO = 1;
        public static final int WTGA_CPT_IN_OPZ_RIVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_CPT_IN_OPZ_RIVTO = 8;
        public static final int WTGA_CPT_IN_OPZ_RIVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_CPT_IN_OPZ_RIVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_CPT_IN_OPZ_RIVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
