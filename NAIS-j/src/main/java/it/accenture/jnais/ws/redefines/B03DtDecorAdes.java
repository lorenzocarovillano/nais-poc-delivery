package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-DECOR-ADES<br>
 * Variable: B03-DT-DECOR-ADES from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtDecorAdes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtDecorAdes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_DECOR_ADES;
    }

    public void setB03DtDecorAdes(int b03DtDecorAdes) {
        writeIntAsPacked(Pos.B03_DT_DECOR_ADES, b03DtDecorAdes, Len.Int.B03_DT_DECOR_ADES);
    }

    public void setB03DtDecorAdesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_DECOR_ADES, Pos.B03_DT_DECOR_ADES);
    }

    /**Original name: B03-DT-DECOR-ADES<br>*/
    public int getB03DtDecorAdes() {
        return readPackedAsInt(Pos.B03_DT_DECOR_ADES, Len.Int.B03_DT_DECOR_ADES);
    }

    public byte[] getB03DtDecorAdesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_DECOR_ADES, Pos.B03_DT_DECOR_ADES);
        return buffer;
    }

    public void setB03DtDecorAdesNull(String b03DtDecorAdesNull) {
        writeString(Pos.B03_DT_DECOR_ADES_NULL, b03DtDecorAdesNull, Len.B03_DT_DECOR_ADES_NULL);
    }

    /**Original name: B03-DT-DECOR-ADES-NULL<br>*/
    public String getB03DtDecorAdesNull() {
        return readString(Pos.B03_DT_DECOR_ADES_NULL, Len.B03_DT_DECOR_ADES_NULL);
    }

    public String getB03DtDecorAdesNullFormatted() {
        return Functions.padBlanks(getB03DtDecorAdesNull(), Len.B03_DT_DECOR_ADES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_DECOR_ADES = 1;
        public static final int B03_DT_DECOR_ADES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_DECOR_ADES = 5;
        public static final int B03_DT_DECOR_ADES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_DECOR_ADES = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
