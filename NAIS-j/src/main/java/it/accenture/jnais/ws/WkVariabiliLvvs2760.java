package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.FlagFineGar;
import it.accenture.jnais.ws.enums.FlagFineRan;
import it.accenture.jnais.ws.enums.FlagPoli;

/**Original name: WK-VARIABILI<br>
 * Variable: WK-VARIABILI from program LVVS2760<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVariabiliLvvs2760 {

    //==== PROPERTIES ====
    //Original name: NUM-POLI
    private String numPoli = DefaultValues.stringVal(Len.NUM_POLI);
    //Original name: FLAG-POLI
    private FlagPoli flagPoli = new FlagPoli();
    //Original name: FLAG-FINE-RAN
    private FlagFineRan flagFineRan = new FlagFineRan();
    //Original name: FLAG-FINE-GAR
    private FlagFineGar flagFineGar = new FlagFineGar();

    //==== METHODS ====
    public void setNumPoli(int numPoli) {
        this.numPoli = NumericDisplay.asString(numPoli, Len.NUM_POLI);
    }

    public void setNumPoliFormatted(String numPoli) {
        this.numPoli = Trunc.toUnsignedNumeric(numPoli, Len.NUM_POLI);
    }

    public int getNumPoli() {
        return NumericDisplay.asInt(this.numPoli);
    }

    public FlagFineGar getFlagFineGar() {
        return flagFineGar;
    }

    public FlagFineRan getFlagFineRan() {
        return flagFineRan;
    }

    public FlagPoli getFlagPoli() {
        return flagPoli;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NUM_POLI = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
