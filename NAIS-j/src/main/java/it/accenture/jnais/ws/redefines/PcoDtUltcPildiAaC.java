package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-PILDI-AA-C<br>
 * Variable: PCO-DT-ULTC-PILDI-AA-C from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcPildiAaC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcPildiAaC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_PILDI_AA_C;
    }

    public void setPcoDtUltcPildiAaC(int pcoDtUltcPildiAaC) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_PILDI_AA_C, pcoDtUltcPildiAaC, Len.Int.PCO_DT_ULTC_PILDI_AA_C);
    }

    public void setPcoDtUltcPildiAaCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_PILDI_AA_C, Pos.PCO_DT_ULTC_PILDI_AA_C);
    }

    /**Original name: PCO-DT-ULTC-PILDI-AA-C<br>*/
    public int getPcoDtUltcPildiAaC() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_PILDI_AA_C, Len.Int.PCO_DT_ULTC_PILDI_AA_C);
    }

    public byte[] getPcoDtUltcPildiAaCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_PILDI_AA_C, Pos.PCO_DT_ULTC_PILDI_AA_C);
        return buffer;
    }

    public void initPcoDtUltcPildiAaCHighValues() {
        fill(Pos.PCO_DT_ULTC_PILDI_AA_C, Len.PCO_DT_ULTC_PILDI_AA_C, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcPildiAaCNull(String pcoDtUltcPildiAaCNull) {
        writeString(Pos.PCO_DT_ULTC_PILDI_AA_C_NULL, pcoDtUltcPildiAaCNull, Len.PCO_DT_ULTC_PILDI_AA_C_NULL);
    }

    /**Original name: PCO-DT-ULTC-PILDI-AA-C-NULL<br>*/
    public String getPcoDtUltcPildiAaCNull() {
        return readString(Pos.PCO_DT_ULTC_PILDI_AA_C_NULL, Len.PCO_DT_ULTC_PILDI_AA_C_NULL);
    }

    public String getPcoDtUltcPildiAaCNullFormatted() {
        return Functions.padBlanks(getPcoDtUltcPildiAaCNull(), Len.PCO_DT_ULTC_PILDI_AA_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_PILDI_AA_C = 1;
        public static final int PCO_DT_ULTC_PILDI_AA_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_PILDI_AA_C = 5;
        public static final int PCO_DT_ULTC_PILDI_AA_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_PILDI_AA_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
