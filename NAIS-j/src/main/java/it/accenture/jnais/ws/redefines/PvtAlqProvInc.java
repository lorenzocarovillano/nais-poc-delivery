package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-ALQ-PROV-INC<br>
 * Variable: PVT-ALQ-PROV-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtAlqProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtAlqProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_ALQ_PROV_INC;
    }

    public void setPvtAlqProvInc(AfDecimal pvtAlqProvInc) {
        writeDecimalAsPacked(Pos.PVT_ALQ_PROV_INC, pvtAlqProvInc.copy());
    }

    public void setPvtAlqProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_ALQ_PROV_INC, Pos.PVT_ALQ_PROV_INC);
    }

    /**Original name: PVT-ALQ-PROV-INC<br>*/
    public AfDecimal getPvtAlqProvInc() {
        return readPackedAsDecimal(Pos.PVT_ALQ_PROV_INC, Len.Int.PVT_ALQ_PROV_INC, Len.Fract.PVT_ALQ_PROV_INC);
    }

    public byte[] getPvtAlqProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_ALQ_PROV_INC, Pos.PVT_ALQ_PROV_INC);
        return buffer;
    }

    public void setPvtAlqProvIncNull(String pvtAlqProvIncNull) {
        writeString(Pos.PVT_ALQ_PROV_INC_NULL, pvtAlqProvIncNull, Len.PVT_ALQ_PROV_INC_NULL);
    }

    /**Original name: PVT-ALQ-PROV-INC-NULL<br>*/
    public String getPvtAlqProvIncNull() {
        return readString(Pos.PVT_ALQ_PROV_INC_NULL, Len.PVT_ALQ_PROV_INC_NULL);
    }

    public String getPvtAlqProvIncNullFormatted() {
        return Functions.padBlanks(getPvtAlqProvIncNull(), Len.PVT_ALQ_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_ALQ_PROV_INC = 1;
        public static final int PVT_ALQ_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_ALQ_PROV_INC = 4;
        public static final int PVT_ALQ_PROV_INC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_ALQ_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PVT_ALQ_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
