package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-IMP-UTI<br>
 * Variable: WTLI-IMP-UTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliImpUti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliImpUti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_IMP_UTI;
    }

    public void setWtliImpUti(AfDecimal wtliImpUti) {
        writeDecimalAsPacked(Pos.WTLI_IMP_UTI, wtliImpUti.copy());
    }

    public void setWtliImpUtiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_IMP_UTI, Pos.WTLI_IMP_UTI);
    }

    /**Original name: WTLI-IMP-UTI<br>*/
    public AfDecimal getWtliImpUti() {
        return readPackedAsDecimal(Pos.WTLI_IMP_UTI, Len.Int.WTLI_IMP_UTI, Len.Fract.WTLI_IMP_UTI);
    }

    public byte[] getWtliImpUtiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_IMP_UTI, Pos.WTLI_IMP_UTI);
        return buffer;
    }

    public void initWtliImpUtiSpaces() {
        fill(Pos.WTLI_IMP_UTI, Len.WTLI_IMP_UTI, Types.SPACE_CHAR);
    }

    public void setWtliImpUtiNull(String wtliImpUtiNull) {
        writeString(Pos.WTLI_IMP_UTI_NULL, wtliImpUtiNull, Len.WTLI_IMP_UTI_NULL);
    }

    /**Original name: WTLI-IMP-UTI-NULL<br>*/
    public String getWtliImpUtiNull() {
        return readString(Pos.WTLI_IMP_UTI_NULL, Len.WTLI_IMP_UTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_IMP_UTI = 1;
        public static final int WTLI_IMP_UTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_IMP_UTI = 8;
        public static final int WTLI_IMP_UTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_IMP_UTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_IMP_UTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
