package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-ANTIDUR-DT-CALC<br>
 * Variable: W-B03-ANTIDUR-DT-CALC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03AntidurDtCalcLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03AntidurDtCalcLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_ANTIDUR_DT_CALC;
    }

    public void setwB03AntidurDtCalc(AfDecimal wB03AntidurDtCalc) {
        writeDecimalAsPacked(Pos.W_B03_ANTIDUR_DT_CALC, wB03AntidurDtCalc.copy());
    }

    /**Original name: W-B03-ANTIDUR-DT-CALC<br>*/
    public AfDecimal getwB03AntidurDtCalc() {
        return readPackedAsDecimal(Pos.W_B03_ANTIDUR_DT_CALC, Len.Int.W_B03_ANTIDUR_DT_CALC, Len.Fract.W_B03_ANTIDUR_DT_CALC);
    }

    public byte[] getwB03AntidurDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_ANTIDUR_DT_CALC, Pos.W_B03_ANTIDUR_DT_CALC);
        return buffer;
    }

    public void setwB03AntidurDtCalcNull(String wB03AntidurDtCalcNull) {
        writeString(Pos.W_B03_ANTIDUR_DT_CALC_NULL, wB03AntidurDtCalcNull, Len.W_B03_ANTIDUR_DT_CALC_NULL);
    }

    /**Original name: W-B03-ANTIDUR-DT-CALC-NULL<br>*/
    public String getwB03AntidurDtCalcNull() {
        return readString(Pos.W_B03_ANTIDUR_DT_CALC_NULL, Len.W_B03_ANTIDUR_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_ANTIDUR_DT_CALC = 1;
        public static final int W_B03_ANTIDUR_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_ANTIDUR_DT_CALC = 6;
        public static final int W_B03_ANTIDUR_DT_CALC_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_ANTIDUR_DT_CALC = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_ANTIDUR_DT_CALC = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
