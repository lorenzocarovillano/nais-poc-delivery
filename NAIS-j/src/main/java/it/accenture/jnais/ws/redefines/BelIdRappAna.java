package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-ID-RAPP-ANA<br>
 * Variable: BEL-ID-RAPP-ANA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelIdRappAna extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelIdRappAna() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_ID_RAPP_ANA;
    }

    public void setBelIdRappAna(int belIdRappAna) {
        writeIntAsPacked(Pos.BEL_ID_RAPP_ANA, belIdRappAna, Len.Int.BEL_ID_RAPP_ANA);
    }

    public void setBelIdRappAnaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_ID_RAPP_ANA, Pos.BEL_ID_RAPP_ANA);
    }

    /**Original name: BEL-ID-RAPP-ANA<br>*/
    public int getBelIdRappAna() {
        return readPackedAsInt(Pos.BEL_ID_RAPP_ANA, Len.Int.BEL_ID_RAPP_ANA);
    }

    public byte[] getBelIdRappAnaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_ID_RAPP_ANA, Pos.BEL_ID_RAPP_ANA);
        return buffer;
    }

    public void setBelIdRappAnaNull(String belIdRappAnaNull) {
        writeString(Pos.BEL_ID_RAPP_ANA_NULL, belIdRappAnaNull, Len.BEL_ID_RAPP_ANA_NULL);
    }

    /**Original name: BEL-ID-RAPP-ANA-NULL<br>*/
    public String getBelIdRappAnaNull() {
        return readString(Pos.BEL_ID_RAPP_ANA_NULL, Len.BEL_ID_RAPP_ANA_NULL);
    }

    public String getBelIdRappAnaNullFormatted() {
        return Functions.padBlanks(getBelIdRappAnaNull(), Len.BEL_ID_RAPP_ANA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_ID_RAPP_ANA = 1;
        public static final int BEL_ID_RAPP_ANA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_ID_RAPP_ANA = 5;
        public static final int BEL_ID_RAPP_ANA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_ID_RAPP_ANA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
