package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRL-DT-SIN-3O-ASSTO<br>
 * Variable: GRL-DT-SIN-3O-ASSTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrlDtSin3oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrlDtSin3oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRL_DT_SIN3O_ASSTO;
    }

    public void setGrlDtSin3oAssto(int grlDtSin3oAssto) {
        writeIntAsPacked(Pos.GRL_DT_SIN3O_ASSTO, grlDtSin3oAssto, Len.Int.GRL_DT_SIN3O_ASSTO);
    }

    public void setGrlDtSin3oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRL_DT_SIN3O_ASSTO, Pos.GRL_DT_SIN3O_ASSTO);
    }

    /**Original name: GRL-DT-SIN-3O-ASSTO<br>*/
    public int getGrlDtSin3oAssto() {
        return readPackedAsInt(Pos.GRL_DT_SIN3O_ASSTO, Len.Int.GRL_DT_SIN3O_ASSTO);
    }

    public byte[] getGrlDtSin3oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRL_DT_SIN3O_ASSTO, Pos.GRL_DT_SIN3O_ASSTO);
        return buffer;
    }

    public void setGrlDtSin3oAsstoNull(String grlDtSin3oAsstoNull) {
        writeString(Pos.GRL_DT_SIN3O_ASSTO_NULL, grlDtSin3oAsstoNull, Len.GRL_DT_SIN3O_ASSTO_NULL);
    }

    /**Original name: GRL-DT-SIN-3O-ASSTO-NULL<br>*/
    public String getGrlDtSin3oAsstoNull() {
        return readString(Pos.GRL_DT_SIN3O_ASSTO_NULL, Len.GRL_DT_SIN3O_ASSTO_NULL);
    }

    public String getGrlDtSin3oAsstoNullFormatted() {
        return Functions.padBlanks(getGrlDtSin3oAsstoNull(), Len.GRL_DT_SIN3O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRL_DT_SIN3O_ASSTO = 1;
        public static final int GRL_DT_SIN3O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRL_DT_SIN3O_ASSTO = 5;
        public static final int GRL_DT_SIN3O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRL_DT_SIN3O_ASSTO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
