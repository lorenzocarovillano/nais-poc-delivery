package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WpagTabIas;

/**Original name: WPAG-AREA-IAS<br>
 * Variable: WPAG-AREA-IAS from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WpagAreaIas extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_IAS_MAXOCCURS = 10;
    /**Original name: WPAG-ELE-MAX-IAS<br>
	 * <pre>-----------------------------------------------------------------*
	 *    AREA INCLUSIONE ADESIONE - CALCOLI
	 *                               AREA DI PAGINA IAS
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA
	 *    LUNG.
	 * -----------------------------------------------------------------*
	 *  03 AREA-IAS.</pre>*/
    private short eleMaxIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPAG-TAB-IAS
    private WpagTabIas[] tabIas = new WpagTabIas[TAB_IAS_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WpagAreaIas() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_AREA_IAS;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWpagAreaIasBytes(buf);
    }

    public void init() {
        for (int tabIasIdx = 1; tabIasIdx <= TAB_IAS_MAXOCCURS; tabIasIdx++) {
            tabIas[tabIasIdx - 1] = new WpagTabIas();
        }
    }

    public String getWpagAreaIasFormatted() {
        return MarshalByteExt.bufferToStr(getWpagAreaIasBytes());
    }

    public void setWpagAreaIasBytes(byte[] buffer) {
        setWpagAreaIasBytes(buffer, 1);
    }

    public byte[] getWpagAreaIasBytes() {
        byte[] buffer = new byte[Len.WPAG_AREA_IAS];
        return getWpagAreaIasBytes(buffer, 1);
    }

    public void setWpagAreaIasBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxIas = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_IAS_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabIas[idx - 1].setTabIasBytes(buffer, position);
                position += WpagTabIas.Len.TAB_IAS;
            }
            else {
                tabIas[idx - 1].initTabIasSpaces();
                position += WpagTabIas.Len.TAB_IAS;
            }
        }
    }

    public byte[] getWpagAreaIasBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleMaxIas);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_IAS_MAXOCCURS; idx++) {
            tabIas[idx - 1].getTabIasBytes(buffer, position);
            position += WpagTabIas.Len.TAB_IAS;
        }
        return buffer;
    }

    public void setEleMaxIas(short eleMaxIas) {
        this.eleMaxIas = eleMaxIas;
    }

    public short getEleMaxIas() {
        return this.eleMaxIas;
    }

    public WpagTabIas getTabIas(int idx) {
        return tabIas[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWpagAreaIasBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_MAX_IAS = 2;
        public static final int WPAG_AREA_IAS = ELE_MAX_IAS + WpagAreaIas.TAB_IAS_MAXOCCURS * WpagTabIas.Len.TAB_IAS;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
