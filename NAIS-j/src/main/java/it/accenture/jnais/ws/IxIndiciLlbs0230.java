package it.accenture.jnais.ws;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LLBS0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLlbs0230 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-POL
    private short tabPol = ((short)0);
    //Original name: IX-TAB-ADE
    private short tabAde = ((short)0);
    //Original name: IX-TAB-GRZ
    private short tabGrz = ((short)0);
    //Original name: IX-TAB-TGA
    private short tabTga = ((short)0);
    //Original name: IX-TAB-STB
    private short tabStb = ((short)0);
    //Original name: IX-TAB-PMO
    private short tabPmo = ((short)0);
    //Original name: IX-TAB-POG
    private short tabPog = ((short)0);
    //Original name: IX-TAB-RST
    private short tabRst = ((short)0);
    //Original name: IX-AREA-SCHEDA
    private short areaScheda = ((short)0);
    //Original name: IX-TAB-VAR
    private short tabVar = ((short)0);
    //Original name: IX-TAB-B03
    private short tabB03 = ((short)0);
    //Original name: IX-TAB-B04
    private short tabB04 = ((short)0);
    //Original name: IX-CONTA
    private short conta = ((short)0);
    //Original name: IX-TAB-STR
    private short tabStr = ((short)0);
    //Original name: IX-TAB-TGA-STR
    private short tabTgaStr = ((short)0);
    //Original name: IX-TAB-TGA1
    private short tabTga1 = ((short)0);
    //Original name: IX-TAB-501
    private short tab501 = ((short)0);
    //Original name: IX-TAB-450
    private short tab450 = ((short)0);
    //Original name: IX-TAB-LISTA
    private short tabLista = ((short)0);
    //Original name: IX-TAB-CAUS
    private short tabCaus = ((short)0);
    //Original name: IX-CNT
    private short cnt = ((short)0);
    //Original name: IX-MAX-VAR
    private short maxVar = ((short)0);

    //==== METHODS ====
    public void setTabPol(short tabPol) {
        this.tabPol = tabPol;
    }

    public short getTabPol() {
        return this.tabPol;
    }

    public void setTabAde(short tabAde) {
        this.tabAde = tabAde;
    }

    public short getTabAde() {
        return this.tabAde;
    }

    public void setTabGrz(short tabGrz) {
        this.tabGrz = tabGrz;
    }

    public short getTabGrz() {
        return this.tabGrz;
    }

    public void setTabTga(short tabTga) {
        this.tabTga = tabTga;
    }

    public short getTabTga() {
        return this.tabTga;
    }

    public void setTabStb(short tabStb) {
        this.tabStb = tabStb;
    }

    public short getTabStb() {
        return this.tabStb;
    }

    public void setTabPmo(short tabPmo) {
        this.tabPmo = tabPmo;
    }

    public short getTabPmo() {
        return this.tabPmo;
    }

    public void setTabPog(short tabPog) {
        this.tabPog = tabPog;
    }

    public short getTabPog() {
        return this.tabPog;
    }

    public void setTabRst(short tabRst) {
        this.tabRst = tabRst;
    }

    public short getTabRst() {
        return this.tabRst;
    }

    public void setAreaScheda(short areaScheda) {
        this.areaScheda = areaScheda;
    }

    public short getAreaScheda() {
        return this.areaScheda;
    }

    public void setTabVar(short tabVar) {
        this.tabVar = tabVar;
    }

    public short getTabVar() {
        return this.tabVar;
    }

    public void setTabB03(short tabB03) {
        this.tabB03 = tabB03;
    }

    public short getTabB03() {
        return this.tabB03;
    }

    public void setTabB04(short tabB04) {
        this.tabB04 = tabB04;
    }

    public short getTabB04() {
        return this.tabB04;
    }

    public void setConta(short conta) {
        this.conta = conta;
    }

    public short getConta() {
        return this.conta;
    }

    public void setTabStr(short tabStr) {
        this.tabStr = tabStr;
    }

    public short getTabStr() {
        return this.tabStr;
    }

    public void setTabTgaStr(short tabTgaStr) {
        this.tabTgaStr = tabTgaStr;
    }

    public short getTabTgaStr() {
        return this.tabTgaStr;
    }

    public void setTabTga1(short tabTga1) {
        this.tabTga1 = tabTga1;
    }

    public short getTabTga1() {
        return this.tabTga1;
    }

    public void setTab501(short tab501) {
        this.tab501 = tab501;
    }

    public short getTab501() {
        return this.tab501;
    }

    public void setTab450(short tab450) {
        this.tab450 = tab450;
    }

    public short getTab450() {
        return this.tab450;
    }

    public void setTabLista(short tabLista) {
        this.tabLista = tabLista;
    }

    public short getTabLista() {
        return this.tabLista;
    }

    public void setTabCaus(short tabCaus) {
        this.tabCaus = tabCaus;
    }

    public short getTabCaus() {
        return this.tabCaus;
    }

    public void setCnt(short cnt) {
        this.cnt = cnt;
    }

    public short getCnt() {
        return this.cnt;
    }

    public void setMaxVar(short maxVar) {
        this.maxVar = maxVar;
    }

    public short getMaxVar() {
        return this.maxVar;
    }
}
