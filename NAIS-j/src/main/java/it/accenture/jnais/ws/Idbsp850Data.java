package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndLogErrore;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSP850<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsp850Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-ESTR-CNT-DIAGN-RIV
    private IndLogErrore indEstrCntDiagnRiv = new IndLogErrore();
    //Original name: P85-DT-RIVAL-DB
    private String p85DtRivalDb = DefaultValues.stringVal(Len.P85_DT_RIVAL_DB);

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setP85DtRivalDb(String p85DtRivalDb) {
        this.p85DtRivalDb = Functions.subString(p85DtRivalDb, Len.P85_DT_RIVAL_DB);
    }

    public String getP85DtRivalDb() {
        return this.p85DtRivalDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndLogErrore getIndEstrCntDiagnRiv() {
        return indEstrCntDiagnRiv;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P85_DT_RIVAL_DB = 10;
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
