package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PC-RIP-PRE<br>
 * Variable: TGA-PC-RIP-PRE from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPcRipPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPcRipPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PC_RIP_PRE;
    }

    public void setTgaPcRipPre(AfDecimal tgaPcRipPre) {
        writeDecimalAsPacked(Pos.TGA_PC_RIP_PRE, tgaPcRipPre.copy());
    }

    public void setTgaPcRipPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PC_RIP_PRE, Pos.TGA_PC_RIP_PRE);
    }

    /**Original name: TGA-PC-RIP-PRE<br>*/
    public AfDecimal getTgaPcRipPre() {
        return readPackedAsDecimal(Pos.TGA_PC_RIP_PRE, Len.Int.TGA_PC_RIP_PRE, Len.Fract.TGA_PC_RIP_PRE);
    }

    public byte[] getTgaPcRipPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PC_RIP_PRE, Pos.TGA_PC_RIP_PRE);
        return buffer;
    }

    public void setTgaPcRipPreNull(String tgaPcRipPreNull) {
        writeString(Pos.TGA_PC_RIP_PRE_NULL, tgaPcRipPreNull, Len.TGA_PC_RIP_PRE_NULL);
    }

    /**Original name: TGA-PC-RIP-PRE-NULL<br>*/
    public String getTgaPcRipPreNull() {
        return readString(Pos.TGA_PC_RIP_PRE_NULL, Len.TGA_PC_RIP_PRE_NULL);
    }

    public String getTgaPcRipPreNullFormatted() {
        return Functions.padBlanks(getTgaPcRipPreNull(), Len.TGA_PC_RIP_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PC_RIP_PRE = 1;
        public static final int TGA_PC_RIP_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PC_RIP_PRE = 4;
        public static final int TGA_PC_RIP_PRE_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PC_RIP_PRE = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PC_RIP_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
