package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WKS-TAB-STR-DATO<br>
 * Variable: WKS-TAB-STR-DATO from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WksTabStrDato extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELEMENTS_STR_DATO_MAXOCCURS = 1000;
    public static final int AREA_TABB_MAXOCCURS = 100;

    //==== CONSTRUCTORS ====
    public WksTabStrDato() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WKS_TAB_STR_DATO;
    }

    public String getWksTabStrDatoFormatted() {
        return readFixedString(Pos.WKS_TAB_STR_DATO, Len.WKS_TAB_STR_DATO);
    }

    public void setNomeTabb(int nomeTabbIdx, String nomeTabb) {
        int position = Pos.wksNomeTabb(nomeTabbIdx - 1);
        writeString(position, nomeTabb, Len.NOME_TABB);
    }

    /**Original name: WKS-NOME-TABB<br>*/
    public String getNomeTabb(int nomeTabbIdx) {
        int position = Pos.wksNomeTabb(nomeTabbIdx - 1);
        return readString(position, Len.NOME_TABB);
    }

    public void setEleMaxDato(int eleMaxDatoIdx, short eleMaxDato) {
        int position = Pos.wksEleMaxDato(eleMaxDatoIdx - 1);
        writeShortAsPacked(position, eleMaxDato, Len.Int.ELE_MAX_DATO);
    }

    /**Original name: WKS-ELE-MAX-DATO<br>*/
    public short getEleMaxDato(int eleMaxDatoIdx) {
        int position = Pos.wksEleMaxDato(eleMaxDatoIdx - 1);
        return readPackedAsShort(position, Len.Int.ELE_MAX_DATO);
    }

    public void setCodiceDato(int codiceDatoIdx1, int codiceDatoIdx2, String codiceDato) {
        int position = Pos.wksCodiceDato(codiceDatoIdx1 - 1, codiceDatoIdx2 - 1);
        writeString(position, codiceDato, Len.CODICE_DATO);
    }

    /**Original name: WKS-CODICE-DATO<br>*/
    public String getCodiceDato(int codiceDatoIdx1, int codiceDatoIdx2) {
        int position = Pos.wksCodiceDato(codiceDatoIdx1 - 1, codiceDatoIdx2 - 1);
        return readString(position, Len.CODICE_DATO);
    }

    public void setTipoDato(int tipoDatoIdx1, int tipoDatoIdx2, String tipoDato) {
        int position = Pos.wksTipoDato(tipoDatoIdx1 - 1, tipoDatoIdx2 - 1);
        writeString(position, tipoDato, Len.TIPO_DATO);
    }

    /**Original name: WKS-TIPO-DATO<br>*/
    public String getTipoDato(int tipoDatoIdx1, int tipoDatoIdx2) {
        int position = Pos.wksTipoDato(tipoDatoIdx1 - 1, tipoDatoIdx2 - 1);
        return readString(position, Len.TIPO_DATO);
    }

    public void setIniPosi(int iniPosiIdx1, int iniPosiIdx2, long iniPosi) {
        int position = Pos.wksIniPosi(iniPosiIdx1 - 1, iniPosiIdx2 - 1);
        writeBinaryUnsignedInt(position, iniPosi);
    }

    /**Original name: WKS-INI-POSI<br>*/
    public long getIniPosi(int iniPosiIdx1, int iniPosiIdx2) {
        int position = Pos.wksIniPosi(iniPosiIdx1 - 1, iniPosiIdx2 - 1);
        return readBinaryUnsignedInt(position);
    }

    public void setLunDato(int lunDatoIdx1, int lunDatoIdx2, long lunDato) {
        int position = Pos.wksLunDato(lunDatoIdx1 - 1, lunDatoIdx2 - 1);
        writeBinaryUnsignedInt(position, lunDato);
    }

    /**Original name: WKS-LUN-DATO<br>*/
    public long getLunDato(int lunDatoIdx1, int lunDatoIdx2) {
        int position = Pos.wksLunDato(lunDatoIdx1 - 1, lunDatoIdx2 - 1);
        return readBinaryUnsignedInt(position);
    }

    public void setLunghezzaDatoNom(int lunghezzaDatoNomIdx1, int lunghezzaDatoNomIdx2, long lunghezzaDatoNom) {
        int position = Pos.wksLunghezzaDatoNom(lunghezzaDatoNomIdx1 - 1, lunghezzaDatoNomIdx2 - 1);
        writeBinaryUnsignedInt(position, lunghezzaDatoNom);
    }

    /**Original name: WKS-LUNGHEZZA-DATO-NOM<br>*/
    public long getLunghezzaDatoNom(int lunghezzaDatoNomIdx1, int lunghezzaDatoNomIdx2) {
        int position = Pos.wksLunghezzaDatoNom(lunghezzaDatoNomIdx1 - 1, lunghezzaDatoNomIdx2 - 1);
        return readBinaryUnsignedInt(position);
    }

    public void setPrecisioneDato(int precisioneDatoIdx1, int precisioneDatoIdx2, int precisioneDato) {
        int position = Pos.wksPrecisioneDato(precisioneDatoIdx1 - 1, precisioneDatoIdx2 - 1);
        writeBinaryUnsignedShort(position, precisioneDato);
    }

    /**Original name: WKS-PRECISIONE-DATO<br>*/
    public int getPrecisioneDato(int precisioneDatoIdx1, int precisioneDatoIdx2) {
        int position = Pos.wksPrecisioneDato(precisioneDatoIdx1 - 1, precisioneDatoIdx2 - 1);
        return readBinaryUnsignedShort(position);
    }

    public void setRestoTabStrDato(String restoTabStrDato) {
        writeString(Pos.RESTO_TAB_STR_DATO, restoTabStrDato, Len.RESTO_TAB_STR_DATO);
    }

    /**Original name: WKS-RESTO-TAB-STR-DATO<br>*/
    public String getRestoTabStrDato() {
        return readString(Pos.RESTO_TAB_STR_DATO, Len.RESTO_TAB_STR_DATO);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WKS_TAB_STR_DATO = 1;
        public static final int WKS_TAB_STR_DATO_R = 1;
        public static final int FLR1 = WKS_TAB_STR_DATO_R;
        public static final int RESTO_TAB_STR_DATO = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wksAreaTabb(int idx) {
            return WKS_TAB_STR_DATO + idx * Len.AREA_TABB;
        }

        public static int wksNomeTabb(int idx) {
            return wksAreaTabb(idx);
        }

        public static int wksEleMaxDato(int idx) {
            return wksNomeTabb(idx) + Len.NOME_TABB;
        }

        public static int wksElementsStrDato(int idx1, int idx2) {
            return wksEleMaxDato(idx1) + Len.ELE_MAX_DATO + idx2 * Len.ELEMENTS_STR_DATO;
        }

        public static int wksCodiceDato(int idx1, int idx2) {
            return wksElementsStrDato(idx1, idx2);
        }

        public static int wksTipoDato(int idx1, int idx2) {
            return wksCodiceDato(idx1, idx2) + Len.CODICE_DATO;
        }

        public static int wksIniPosi(int idx1, int idx2) {
            return wksTipoDato(idx1, idx2) + Len.TIPO_DATO;
        }

        public static int wksLunDato(int idx1, int idx2) {
            return wksIniPosi(idx1, idx2) + Len.INI_POSI;
        }

        public static int wksLunghezzaDatoNom(int idx1, int idx2) {
            return wksLunDato(idx1, idx2) + Len.LUN_DATO;
        }

        public static int wksPrecisioneDato(int idx1, int idx2) {
            return wksLunghezzaDatoNom(idx1, idx2) + Len.LUNGHEZZA_DATO_NOM;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int NOME_TABB = 30;
        public static final int ELE_MAX_DATO = 3;
        public static final int CODICE_DATO = 30;
        public static final int TIPO_DATO = 2;
        public static final int INI_POSI = 4;
        public static final int LUN_DATO = 4;
        public static final int LUNGHEZZA_DATO_NOM = 4;
        public static final int PRECISIONE_DATO = 2;
        public static final int ELEMENTS_STR_DATO = CODICE_DATO + TIPO_DATO + INI_POSI + LUN_DATO + LUNGHEZZA_DATO_NOM + PRECISIONE_DATO;
        public static final int AREA_TABB = NOME_TABB + ELE_MAX_DATO + WksTabStrDato.ELEMENTS_STR_DATO_MAXOCCURS * ELEMENTS_STR_DATO;
        public static final int FLR1 = 46033;
        public static final int WKS_TAB_STR_DATO = WksTabStrDato.AREA_TABB_MAXOCCURS * AREA_TABB;
        public static final int RESTO_TAB_STR_DATO = 4557267;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ELE_MAX_DATO = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
