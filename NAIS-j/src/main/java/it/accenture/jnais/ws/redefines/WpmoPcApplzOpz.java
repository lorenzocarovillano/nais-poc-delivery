package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-PC-APPLZ-OPZ<br>
 * Variable: WPMO-PC-APPLZ-OPZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoPcApplzOpz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoPcApplzOpz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_PC_APPLZ_OPZ;
    }

    public void setWpmoPcApplzOpz(AfDecimal wpmoPcApplzOpz) {
        writeDecimalAsPacked(Pos.WPMO_PC_APPLZ_OPZ, wpmoPcApplzOpz.copy());
    }

    public void setWpmoPcApplzOpzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_PC_APPLZ_OPZ, Pos.WPMO_PC_APPLZ_OPZ);
    }

    /**Original name: WPMO-PC-APPLZ-OPZ<br>*/
    public AfDecimal getWpmoPcApplzOpz() {
        return readPackedAsDecimal(Pos.WPMO_PC_APPLZ_OPZ, Len.Int.WPMO_PC_APPLZ_OPZ, Len.Fract.WPMO_PC_APPLZ_OPZ);
    }

    public byte[] getWpmoPcApplzOpzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_PC_APPLZ_OPZ, Pos.WPMO_PC_APPLZ_OPZ);
        return buffer;
    }

    public void initWpmoPcApplzOpzSpaces() {
        fill(Pos.WPMO_PC_APPLZ_OPZ, Len.WPMO_PC_APPLZ_OPZ, Types.SPACE_CHAR);
    }

    public void setWpmoPcApplzOpzNull(String wpmoPcApplzOpzNull) {
        writeString(Pos.WPMO_PC_APPLZ_OPZ_NULL, wpmoPcApplzOpzNull, Len.WPMO_PC_APPLZ_OPZ_NULL);
    }

    /**Original name: WPMO-PC-APPLZ-OPZ-NULL<br>*/
    public String getWpmoPcApplzOpzNull() {
        return readString(Pos.WPMO_PC_APPLZ_OPZ_NULL, Len.WPMO_PC_APPLZ_OPZ_NULL);
    }

    public String getWpmoPcApplzOpzNullFormatted() {
        return Functions.padBlanks(getWpmoPcApplzOpzNull(), Len.WPMO_PC_APPLZ_OPZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_PC_APPLZ_OPZ = 1;
        public static final int WPMO_PC_APPLZ_OPZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_PC_APPLZ_OPZ = 4;
        public static final int WPMO_PC_APPLZ_OPZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_PC_APPLZ_OPZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_PC_APPLZ_OPZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
