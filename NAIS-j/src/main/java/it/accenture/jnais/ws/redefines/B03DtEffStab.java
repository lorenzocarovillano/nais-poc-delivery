package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-EFF-STAB<br>
 * Variable: B03-DT-EFF-STAB from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtEffStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtEffStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_EFF_STAB;
    }

    public void setB03DtEffStab(int b03DtEffStab) {
        writeIntAsPacked(Pos.B03_DT_EFF_STAB, b03DtEffStab, Len.Int.B03_DT_EFF_STAB);
    }

    public void setB03DtEffStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_EFF_STAB, Pos.B03_DT_EFF_STAB);
    }

    /**Original name: B03-DT-EFF-STAB<br>*/
    public int getB03DtEffStab() {
        return readPackedAsInt(Pos.B03_DT_EFF_STAB, Len.Int.B03_DT_EFF_STAB);
    }

    public byte[] getB03DtEffStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_EFF_STAB, Pos.B03_DT_EFF_STAB);
        return buffer;
    }

    public void setB03DtEffStabNull(String b03DtEffStabNull) {
        writeString(Pos.B03_DT_EFF_STAB_NULL, b03DtEffStabNull, Len.B03_DT_EFF_STAB_NULL);
    }

    /**Original name: B03-DT-EFF-STAB-NULL<br>*/
    public String getB03DtEffStabNull() {
        return readString(Pos.B03_DT_EFF_STAB_NULL, Len.B03_DT_EFF_STAB_NULL);
    }

    public String getB03DtEffStabNullFormatted() {
        return Functions.padBlanks(getB03DtEffStabNull(), Len.B03_DT_EFF_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_EFF_STAB = 1;
        public static final int B03_DT_EFF_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_EFF_STAB = 5;
        public static final int B03_DT_EFF_STAB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_EFF_STAB = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
