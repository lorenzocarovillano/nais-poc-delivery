package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ULT-RM<br>
 * Variable: B03-ULT-RM from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03UltRm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03UltRm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ULT_RM;
    }

    public void setB03UltRm(AfDecimal b03UltRm) {
        writeDecimalAsPacked(Pos.B03_ULT_RM, b03UltRm.copy());
    }

    public void setB03UltRmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ULT_RM, Pos.B03_ULT_RM);
    }

    /**Original name: B03-ULT-RM<br>*/
    public AfDecimal getB03UltRm() {
        return readPackedAsDecimal(Pos.B03_ULT_RM, Len.Int.B03_ULT_RM, Len.Fract.B03_ULT_RM);
    }

    public byte[] getB03UltRmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ULT_RM, Pos.B03_ULT_RM);
        return buffer;
    }

    public void setB03UltRmNull(String b03UltRmNull) {
        writeString(Pos.B03_ULT_RM_NULL, b03UltRmNull, Len.B03_ULT_RM_NULL);
    }

    /**Original name: B03-ULT-RM-NULL<br>*/
    public String getB03UltRmNull() {
        return readString(Pos.B03_ULT_RM_NULL, Len.B03_ULT_RM_NULL);
    }

    public String getB03UltRmNullFormatted() {
        return Functions.padBlanks(getB03UltRmNull(), Len.B03_ULT_RM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ULT_RM = 1;
        public static final int B03_ULT_RM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ULT_RM = 8;
        public static final int B03_ULT_RM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_ULT_RM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ULT_RM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
