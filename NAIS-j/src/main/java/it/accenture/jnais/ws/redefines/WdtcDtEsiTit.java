package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WDTC-DT-ESI-TIT<br>
 * Variable: WDTC-DT-ESI-TIT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcDtEsiTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcDtEsiTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_DT_ESI_TIT;
    }

    public void setWdtcDtEsiTit(int wdtcDtEsiTit) {
        writeIntAsPacked(Pos.WDTC_DT_ESI_TIT, wdtcDtEsiTit, Len.Int.WDTC_DT_ESI_TIT);
    }

    public void setWdtcDtEsiTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_DT_ESI_TIT, Pos.WDTC_DT_ESI_TIT);
    }

    /**Original name: WDTC-DT-ESI-TIT<br>*/
    public int getWdtcDtEsiTit() {
        return readPackedAsInt(Pos.WDTC_DT_ESI_TIT, Len.Int.WDTC_DT_ESI_TIT);
    }

    public byte[] getWdtcDtEsiTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_DT_ESI_TIT, Pos.WDTC_DT_ESI_TIT);
        return buffer;
    }

    public void initWdtcDtEsiTitSpaces() {
        fill(Pos.WDTC_DT_ESI_TIT, Len.WDTC_DT_ESI_TIT, Types.SPACE_CHAR);
    }

    public void setWdtcDtEsiTitNull(String wdtcDtEsiTitNull) {
        writeString(Pos.WDTC_DT_ESI_TIT_NULL, wdtcDtEsiTitNull, Len.WDTC_DT_ESI_TIT_NULL);
    }

    /**Original name: WDTC-DT-ESI-TIT-NULL<br>*/
    public String getWdtcDtEsiTitNull() {
        return readString(Pos.WDTC_DT_ESI_TIT_NULL, Len.WDTC_DT_ESI_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_DT_ESI_TIT = 1;
        public static final int WDTC_DT_ESI_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_DT_ESI_TIT = 5;
        public static final int WDTC_DT_ESI_TIT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_DT_ESI_TIT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
