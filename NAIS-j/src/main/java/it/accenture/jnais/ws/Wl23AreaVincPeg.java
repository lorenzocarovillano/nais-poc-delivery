package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.Wl23TabVincPeg;

/**Original name: WL23-AREA-VINC-PEG<br>
 * Variable: WL23-AREA-VINC-PEG from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Wl23AreaVincPeg extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_VINC_PEG_MAXOCCURS = 10;
    //Original name: WL23-ELE-VINC-PEG-MAX
    private short eleVincPegMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WL23-TAB-VINC-PEG
    private Wl23TabVincPeg[] tabVincPeg = new Wl23TabVincPeg[TAB_VINC_PEG_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Wl23AreaVincPeg() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL23_AREA_VINC_PEG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWl23AreaVincPegBytes(buf);
    }

    public void init() {
        for (int tabVincPegIdx = 1; tabVincPegIdx <= TAB_VINC_PEG_MAXOCCURS; tabVincPegIdx++) {
            tabVincPeg[tabVincPegIdx - 1] = new Wl23TabVincPeg();
        }
    }

    public String getWl23AreaVincPegFormatted() {
        return MarshalByteExt.bufferToStr(getWl23AreaVincPegBytes());
    }

    public void setWl23AreaVincPegBytes(byte[] buffer) {
        setWl23AreaVincPegBytes(buffer, 1);
    }

    public byte[] getWl23AreaVincPegBytes() {
        byte[] buffer = new byte[Len.WL23_AREA_VINC_PEG];
        return getWl23AreaVincPegBytes(buffer, 1);
    }

    public void setWl23AreaVincPegBytes(byte[] buffer, int offset) {
        int position = offset;
        eleVincPegMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_VINC_PEG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabVincPeg[idx - 1].setWl23TabVincPegBytes(buffer, position);
                position += Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;
            }
            else {
                tabVincPeg[idx - 1].initWl23TabVincPegSpaces();
                position += Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;
            }
        }
    }

    public byte[] getWl23AreaVincPegBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleVincPegMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_VINC_PEG_MAXOCCURS; idx++) {
            tabVincPeg[idx - 1].getWl23TabVincPegBytes(buffer, position);
            position += Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;
        }
        return buffer;
    }

    public void setEleVincPegMax(short eleVincPegMax) {
        this.eleVincPegMax = eleVincPegMax;
    }

    public short getEleVincPegMax() {
        return this.eleVincPegMax;
    }

    public Wl23TabVincPeg getTabVincPeg(int idx) {
        return tabVincPeg[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWl23AreaVincPegBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_VINC_PEG_MAX = 2;
        public static final int WL23_AREA_VINC_PEG = ELE_VINC_PEG_MAX + Wl23AreaVincPeg.TAB_VINC_PEG_MAXOCCURS * Wl23TabVincPeg.Len.WL23_TAB_VINC_PEG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
