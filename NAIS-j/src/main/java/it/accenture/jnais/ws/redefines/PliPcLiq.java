package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PLI-PC-LIQ<br>
 * Variable: PLI-PC-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PliPcLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PliPcLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PLI_PC_LIQ;
    }

    public void setPliPcLiq(AfDecimal pliPcLiq) {
        writeDecimalAsPacked(Pos.PLI_PC_LIQ, pliPcLiq.copy());
    }

    public void setPliPcLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PLI_PC_LIQ, Pos.PLI_PC_LIQ);
    }

    /**Original name: PLI-PC-LIQ<br>*/
    public AfDecimal getPliPcLiq() {
        return readPackedAsDecimal(Pos.PLI_PC_LIQ, Len.Int.PLI_PC_LIQ, Len.Fract.PLI_PC_LIQ);
    }

    public byte[] getPliPcLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PLI_PC_LIQ, Pos.PLI_PC_LIQ);
        return buffer;
    }

    public void setPliPcLiqNull(String pliPcLiqNull) {
        writeString(Pos.PLI_PC_LIQ_NULL, pliPcLiqNull, Len.PLI_PC_LIQ_NULL);
    }

    /**Original name: PLI-PC-LIQ-NULL<br>*/
    public String getPliPcLiqNull() {
        return readString(Pos.PLI_PC_LIQ_NULL, Len.PLI_PC_LIQ_NULL);
    }

    public String getPliPcLiqNullFormatted() {
        return Functions.padBlanks(getPliPcLiqNull(), Len.PLI_PC_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PLI_PC_LIQ = 1;
        public static final int PLI_PC_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PLI_PC_LIQ = 4;
        public static final int PLI_PC_LIQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PLI_PC_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PLI_PC_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
