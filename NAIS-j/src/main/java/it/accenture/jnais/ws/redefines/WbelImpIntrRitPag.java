package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMP-INTR-RIT-PAG<br>
 * Variable: WBEL-IMP-INTR-RIT-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpIntrRitPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpIntrRitPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMP_INTR_RIT_PAG;
    }

    public void setWbelImpIntrRitPag(AfDecimal wbelImpIntrRitPag) {
        writeDecimalAsPacked(Pos.WBEL_IMP_INTR_RIT_PAG, wbelImpIntrRitPag.copy());
    }

    public void setWbelImpIntrRitPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMP_INTR_RIT_PAG, Pos.WBEL_IMP_INTR_RIT_PAG);
    }

    /**Original name: WBEL-IMP-INTR-RIT-PAG<br>*/
    public AfDecimal getWbelImpIntrRitPag() {
        return readPackedAsDecimal(Pos.WBEL_IMP_INTR_RIT_PAG, Len.Int.WBEL_IMP_INTR_RIT_PAG, Len.Fract.WBEL_IMP_INTR_RIT_PAG);
    }

    public byte[] getWbelImpIntrRitPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMP_INTR_RIT_PAG, Pos.WBEL_IMP_INTR_RIT_PAG);
        return buffer;
    }

    public void initWbelImpIntrRitPagSpaces() {
        fill(Pos.WBEL_IMP_INTR_RIT_PAG, Len.WBEL_IMP_INTR_RIT_PAG, Types.SPACE_CHAR);
    }

    public void setWbelImpIntrRitPagNull(String wbelImpIntrRitPagNull) {
        writeString(Pos.WBEL_IMP_INTR_RIT_PAG_NULL, wbelImpIntrRitPagNull, Len.WBEL_IMP_INTR_RIT_PAG_NULL);
    }

    /**Original name: WBEL-IMP-INTR-RIT-PAG-NULL<br>*/
    public String getWbelImpIntrRitPagNull() {
        return readString(Pos.WBEL_IMP_INTR_RIT_PAG_NULL, Len.WBEL_IMP_INTR_RIT_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMP_INTR_RIT_PAG = 1;
        public static final int WBEL_IMP_INTR_RIT_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMP_INTR_RIT_PAG = 8;
        public static final int WBEL_IMP_INTR_RIT_PAG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMP_INTR_RIT_PAG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMP_INTR_RIT_PAG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
