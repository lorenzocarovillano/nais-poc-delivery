package it.accenture.jnais.ws.occurs;

import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.copy.Lccvvas1;

/**Original name: WVAS-TAB-VAL-AST<br>
 * Variables: WVAS-TAB-VAL-AST from copybook LCCVVASA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WvasTabValAst implements ICopyable<WvasTabValAst> {

    //==== PROPERTIES ====
    //Original name: LCCVVAS1
    private Lccvvas1 lccvvas1 = new Lccvvas1();

    //==== CONSTRUCTORS ====
    public WvasTabValAst() {
    }

    public WvasTabValAst(WvasTabValAst wvasTabValAst) {
        this();
        this.lccvvas1 = ((Lccvvas1)wvasTabValAst.lccvvas1.copy());
    }

    //==== METHODS ====
    public Lccvvas1 getLccvvas1() {
        return lccvvas1;
    }

    public WvasTabValAst copy() {
        return new WvasTabValAst(this);
    }
}
