package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: MBS-ID-JOB<br>
 * Variable: MBS-ID-JOB from program LCCS0024<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MbsIdJob extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MbsIdJob() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MBS_ID_JOB;
    }

    public void setMbsIdJob(int mbsIdJob) {
        writeIntAsPacked(Pos.MBS_ID_JOB, mbsIdJob, Len.Int.MBS_ID_JOB);
    }

    public void setMbsIdJobFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MBS_ID_JOB, Pos.MBS_ID_JOB);
    }

    /**Original name: MBS-ID-JOB<br>*/
    public int getMbsIdJob() {
        return readPackedAsInt(Pos.MBS_ID_JOB, Len.Int.MBS_ID_JOB);
    }

    public byte[] getMbsIdJobAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MBS_ID_JOB, Pos.MBS_ID_JOB);
        return buffer;
    }

    public void setMbsIdJobNull(String mbsIdJobNull) {
        writeString(Pos.MBS_ID_JOB_NULL, mbsIdJobNull, Len.MBS_ID_JOB_NULL);
    }

    /**Original name: MBS-ID-JOB-NULL<br>*/
    public String getMbsIdJobNull() {
        return readString(Pos.MBS_ID_JOB_NULL, Len.MBS_ID_JOB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MBS_ID_JOB = 1;
        public static final int MBS_ID_JOB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MBS_ID_JOB = 5;
        public static final int MBS_ID_JOB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MBS_ID_JOB = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
