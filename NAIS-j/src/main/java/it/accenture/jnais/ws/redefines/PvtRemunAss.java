package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-REMUN-ASS<br>
 * Variable: PVT-REMUN-ASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_REMUN_ASS;
    }

    public void setPvtRemunAss(AfDecimal pvtRemunAss) {
        writeDecimalAsPacked(Pos.PVT_REMUN_ASS, pvtRemunAss.copy());
    }

    public void setPvtRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_REMUN_ASS, Pos.PVT_REMUN_ASS);
    }

    /**Original name: PVT-REMUN-ASS<br>*/
    public AfDecimal getPvtRemunAss() {
        return readPackedAsDecimal(Pos.PVT_REMUN_ASS, Len.Int.PVT_REMUN_ASS, Len.Fract.PVT_REMUN_ASS);
    }

    public byte[] getPvtRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_REMUN_ASS, Pos.PVT_REMUN_ASS);
        return buffer;
    }

    public void setPvtRemunAssNull(String pvtRemunAssNull) {
        writeString(Pos.PVT_REMUN_ASS_NULL, pvtRemunAssNull, Len.PVT_REMUN_ASS_NULL);
    }

    /**Original name: PVT-REMUN-ASS-NULL<br>*/
    public String getPvtRemunAssNull() {
        return readString(Pos.PVT_REMUN_ASS_NULL, Len.PVT_REMUN_ASS_NULL);
    }

    public String getPvtRemunAssNullFormatted() {
        return Functions.padBlanks(getPvtRemunAssNull(), Len.PVT_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_REMUN_ASS = 1;
        public static final int PVT_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_REMUN_ASS = 8;
        public static final int PVT_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PVT_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
