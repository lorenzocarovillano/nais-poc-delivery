package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: IEAI9801-AREA<br>
 * Variable: IEAI9801-AREA from copybook IEAI9801<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ieai9801Area extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IEAI9801-DESC-ERRORE-ESTESA
    private String descErroreEstesa = DefaultValues.stringVal(Len.DESC_ERRORE_ESTESA);
    //Original name: IEAI9801-PARAMETRI-ERR
    private String parametriErr = DefaultValues.stringVal(Len.PARAMETRI_ERR);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IEAI9801_AREA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIeai9801AreaBytes(buf);
    }

    public String getIeai9801AreaFormatted() {
        return MarshalByteExt.bufferToStr(getIeai9801AreaBytes());
    }

    public void setIeai9801AreaBytes(byte[] buffer) {
        setIeai9801AreaBytes(buffer, 1);
    }

    public byte[] getIeai9801AreaBytes() {
        byte[] buffer = new byte[Len.IEAI9801_AREA];
        return getIeai9801AreaBytes(buffer, 1);
    }

    public void setIeai9801AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        descErroreEstesa = MarshalByte.readString(buffer, position, Len.DESC_ERRORE_ESTESA);
        position += Len.DESC_ERRORE_ESTESA;
        parametriErr = MarshalByte.readString(buffer, position, Len.PARAMETRI_ERR);
    }

    public byte[] getIeai9801AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, descErroreEstesa, Len.DESC_ERRORE_ESTESA);
        position += Len.DESC_ERRORE_ESTESA;
        MarshalByte.writeString(buffer, position, parametriErr, Len.PARAMETRI_ERR);
        return buffer;
    }

    public void setDescErroreEstesa(String descErroreEstesa) {
        this.descErroreEstesa = Functions.subString(descErroreEstesa, Len.DESC_ERRORE_ESTESA);
    }

    public String getDescErroreEstesa() {
        return this.descErroreEstesa;
    }

    public String getDescErroreEstesaFormatted() {
        return Functions.padBlanks(getDescErroreEstesa(), Len.DESC_ERRORE_ESTESA);
    }

    public void setParametriErr(String parametriErr) {
        this.parametriErr = Functions.subString(parametriErr, Len.PARAMETRI_ERR);
    }

    public String getParametriErr() {
        return this.parametriErr;
    }

    public String getParametriErrFormatted() {
        return Functions.padBlanks(getParametriErr(), Len.PARAMETRI_ERR);
    }

    @Override
    public byte[] serialize() {
        return getIeai9801AreaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESC_ERRORE_ESTESA = 200;
        public static final int PARAMETRI_ERR = 100;
        public static final int IEAI9801_AREA = DESC_ERRORE_ESTESA + PARAMETRI_ERR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
