package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-IMP-TFR<br>
 * Variable: DAD-IMP-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_IMP_TFR;
    }

    public void setDadImpTfr(AfDecimal dadImpTfr) {
        writeDecimalAsPacked(Pos.DAD_IMP_TFR, dadImpTfr.copy());
    }

    public void setDadImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_IMP_TFR, Pos.DAD_IMP_TFR);
    }

    /**Original name: DAD-IMP-TFR<br>*/
    public AfDecimal getDadImpTfr() {
        return readPackedAsDecimal(Pos.DAD_IMP_TFR, Len.Int.DAD_IMP_TFR, Len.Fract.DAD_IMP_TFR);
    }

    public byte[] getDadImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_IMP_TFR, Pos.DAD_IMP_TFR);
        return buffer;
    }

    public void setDadImpTfrNull(String dadImpTfrNull) {
        writeString(Pos.DAD_IMP_TFR_NULL, dadImpTfrNull, Len.DAD_IMP_TFR_NULL);
    }

    /**Original name: DAD-IMP-TFR-NULL<br>*/
    public String getDadImpTfrNull() {
        return readString(Pos.DAD_IMP_TFR_NULL, Len.DAD_IMP_TFR_NULL);
    }

    public String getDadImpTfrNullFormatted() {
        return Functions.padBlanks(getDadImpTfrNull(), Len.DAD_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_IMP_TFR = 1;
        public static final int DAD_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_IMP_TFR = 8;
        public static final int DAD_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DAD_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
