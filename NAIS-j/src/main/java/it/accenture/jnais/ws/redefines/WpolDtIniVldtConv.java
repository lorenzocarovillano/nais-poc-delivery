package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPOL-DT-INI-VLDT-CONV<br>
 * Variable: WPOL-DT-INI-VLDT-CONV from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDtIniVldtConv extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDtIniVldtConv() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DT_INI_VLDT_CONV;
    }

    public void setWpolDtIniVldtConv(int wpolDtIniVldtConv) {
        writeIntAsPacked(Pos.WPOL_DT_INI_VLDT_CONV, wpolDtIniVldtConv, Len.Int.WPOL_DT_INI_VLDT_CONV);
    }

    public void setWpolDtIniVldtConvFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DT_INI_VLDT_CONV, Pos.WPOL_DT_INI_VLDT_CONV);
    }

    /**Original name: WPOL-DT-INI-VLDT-CONV<br>*/
    public int getWpolDtIniVldtConv() {
        return readPackedAsInt(Pos.WPOL_DT_INI_VLDT_CONV, Len.Int.WPOL_DT_INI_VLDT_CONV);
    }

    public String getWpolDtIniVldtConvFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getWpolDtIniVldtConv()).toString();
    }

    public byte[] getWpolDtIniVldtConvAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DT_INI_VLDT_CONV, Pos.WPOL_DT_INI_VLDT_CONV);
        return buffer;
    }

    public void setWpolDtIniVldtConvNull(String wpolDtIniVldtConvNull) {
        writeString(Pos.WPOL_DT_INI_VLDT_CONV_NULL, wpolDtIniVldtConvNull, Len.WPOL_DT_INI_VLDT_CONV_NULL);
    }

    /**Original name: WPOL-DT-INI-VLDT-CONV-NULL<br>*/
    public String getWpolDtIniVldtConvNull() {
        return readString(Pos.WPOL_DT_INI_VLDT_CONV_NULL, Len.WPOL_DT_INI_VLDT_CONV_NULL);
    }

    public String getWpolDtIniVldtConvNullFormatted() {
        return Functions.padBlanks(getWpolDtIniVldtConvNull(), Len.WPOL_DT_INI_VLDT_CONV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DT_INI_VLDT_CONV = 1;
        public static final int WPOL_DT_INI_VLDT_CONV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DT_INI_VLDT_CONV = 5;
        public static final int WPOL_DT_INI_VLDT_CONV_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DT_INI_VLDT_CONV = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
