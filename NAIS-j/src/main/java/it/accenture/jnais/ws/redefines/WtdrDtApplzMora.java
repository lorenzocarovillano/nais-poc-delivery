package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-DT-APPLZ-MORA<br>
 * Variable: WTDR-DT-APPLZ-MORA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrDtApplzMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrDtApplzMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_DT_APPLZ_MORA;
    }

    public void setWtdrDtApplzMora(int wtdrDtApplzMora) {
        writeIntAsPacked(Pos.WTDR_DT_APPLZ_MORA, wtdrDtApplzMora, Len.Int.WTDR_DT_APPLZ_MORA);
    }

    /**Original name: WTDR-DT-APPLZ-MORA<br>*/
    public int getWtdrDtApplzMora() {
        return readPackedAsInt(Pos.WTDR_DT_APPLZ_MORA, Len.Int.WTDR_DT_APPLZ_MORA);
    }

    public void setWtdrDtApplzMoraNull(String wtdrDtApplzMoraNull) {
        writeString(Pos.WTDR_DT_APPLZ_MORA_NULL, wtdrDtApplzMoraNull, Len.WTDR_DT_APPLZ_MORA_NULL);
    }

    /**Original name: WTDR-DT-APPLZ-MORA-NULL<br>*/
    public String getWtdrDtApplzMoraNull() {
        return readString(Pos.WTDR_DT_APPLZ_MORA_NULL, Len.WTDR_DT_APPLZ_MORA_NULL);
    }

    public String getWtdrDtApplzMoraNullFormatted() {
        return Functions.padBlanks(getWtdrDtApplzMoraNull(), Len.WTDR_DT_APPLZ_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_DT_APPLZ_MORA = 1;
        public static final int WTDR_DT_APPLZ_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_DT_APPLZ_MORA = 5;
        public static final int WTDR_DT_APPLZ_MORA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_DT_APPLZ_MORA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
