package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-SPE-MED<br>
 * Variable: WTIT-TOT-SPE-MED from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotSpeMed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotSpeMed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_SPE_MED;
    }

    public void setWtitTotSpeMed(AfDecimal wtitTotSpeMed) {
        writeDecimalAsPacked(Pos.WTIT_TOT_SPE_MED, wtitTotSpeMed.copy());
    }

    public void setWtitTotSpeMedFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_SPE_MED, Pos.WTIT_TOT_SPE_MED);
    }

    /**Original name: WTIT-TOT-SPE-MED<br>*/
    public AfDecimal getWtitTotSpeMed() {
        return readPackedAsDecimal(Pos.WTIT_TOT_SPE_MED, Len.Int.WTIT_TOT_SPE_MED, Len.Fract.WTIT_TOT_SPE_MED);
    }

    public byte[] getWtitTotSpeMedAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_SPE_MED, Pos.WTIT_TOT_SPE_MED);
        return buffer;
    }

    public void initWtitTotSpeMedSpaces() {
        fill(Pos.WTIT_TOT_SPE_MED, Len.WTIT_TOT_SPE_MED, Types.SPACE_CHAR);
    }

    public void setWtitTotSpeMedNull(String wtitTotSpeMedNull) {
        writeString(Pos.WTIT_TOT_SPE_MED_NULL, wtitTotSpeMedNull, Len.WTIT_TOT_SPE_MED_NULL);
    }

    /**Original name: WTIT-TOT-SPE-MED-NULL<br>*/
    public String getWtitTotSpeMedNull() {
        return readString(Pos.WTIT_TOT_SPE_MED_NULL, Len.WTIT_TOT_SPE_MED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SPE_MED = 1;
        public static final int WTIT_TOT_SPE_MED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SPE_MED = 8;
        public static final int WTIT_TOT_SPE_MED_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SPE_MED = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SPE_MED = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
