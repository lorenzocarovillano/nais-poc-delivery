package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-COD-CAN<br>
 * Variable: W-B03-COD-CAN from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CodCanLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CodCanLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_COD_CAN;
    }

    public void setwB03CodCan(int wB03CodCan) {
        writeIntAsPacked(Pos.W_B03_COD_CAN, wB03CodCan, Len.Int.W_B03_COD_CAN);
    }

    /**Original name: W-B03-COD-CAN<br>*/
    public int getwB03CodCan() {
        return readPackedAsInt(Pos.W_B03_COD_CAN, Len.Int.W_B03_COD_CAN);
    }

    public byte[] getwB03CodCanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_COD_CAN, Pos.W_B03_COD_CAN);
        return buffer;
    }

    public void setwB03CodCanNull(String wB03CodCanNull) {
        writeString(Pos.W_B03_COD_CAN_NULL, wB03CodCanNull, Len.W_B03_COD_CAN_NULL);
    }

    /**Original name: W-B03-COD-CAN-NULL<br>*/
    public String getwB03CodCanNull() {
        return readString(Pos.W_B03_COD_CAN_NULL, Len.W_B03_COD_CAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_COD_CAN = 1;
        public static final int W_B03_COD_CAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_COD_CAN = 3;
        public static final int W_B03_COD_CAN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_COD_CAN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
