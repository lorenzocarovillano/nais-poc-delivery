package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L23-SOM-PRE-VINPG<br>
 * Variable: L23-SOM-PRE-VINPG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L23SomPreVinpg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L23SomPreVinpg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L23_SOM_PRE_VINPG;
    }

    public void setL23SomPreVinpg(AfDecimal l23SomPreVinpg) {
        writeDecimalAsPacked(Pos.L23_SOM_PRE_VINPG, l23SomPreVinpg.copy());
    }

    public void setL23SomPreVinpgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L23_SOM_PRE_VINPG, Pos.L23_SOM_PRE_VINPG);
    }

    /**Original name: L23-SOM-PRE-VINPG<br>*/
    public AfDecimal getL23SomPreVinpg() {
        return readPackedAsDecimal(Pos.L23_SOM_PRE_VINPG, Len.Int.L23_SOM_PRE_VINPG, Len.Fract.L23_SOM_PRE_VINPG);
    }

    public byte[] getL23SomPreVinpgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L23_SOM_PRE_VINPG, Pos.L23_SOM_PRE_VINPG);
        return buffer;
    }

    public void setL23SomPreVinpgNull(String l23SomPreVinpgNull) {
        writeString(Pos.L23_SOM_PRE_VINPG_NULL, l23SomPreVinpgNull, Len.L23_SOM_PRE_VINPG_NULL);
    }

    /**Original name: L23-SOM-PRE-VINPG-NULL<br>*/
    public String getL23SomPreVinpgNull() {
        return readString(Pos.L23_SOM_PRE_VINPG_NULL, Len.L23_SOM_PRE_VINPG_NULL);
    }

    public String getL23SomPreVinpgNullFormatted() {
        return Functions.padBlanks(getL23SomPreVinpgNull(), Len.L23_SOM_PRE_VINPG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L23_SOM_PRE_VINPG = 1;
        public static final int L23_SOM_PRE_VINPG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L23_SOM_PRE_VINPG = 8;
        public static final int L23_SOM_PRE_VINPG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L23_SOM_PRE_VINPG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L23_SOM_PRE_VINPG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
