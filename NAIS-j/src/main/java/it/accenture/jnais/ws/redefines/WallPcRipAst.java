package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WALL-PC-RIP-AST<br>
 * Variable: WALL-PC-RIP-AST from program LCCS1900<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WallPcRipAst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WallPcRipAst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WALL_PC_RIP_AST;
    }

    public void setWallPcRipAst(AfDecimal wallPcRipAst) {
        writeDecimalAsPacked(Pos.WALL_PC_RIP_AST, wallPcRipAst.copy());
    }

    public void setWallPcRipAstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WALL_PC_RIP_AST, Pos.WALL_PC_RIP_AST);
    }

    /**Original name: WALL-PC-RIP-AST<br>*/
    public AfDecimal getWallPcRipAst() {
        return readPackedAsDecimal(Pos.WALL_PC_RIP_AST, Len.Int.WALL_PC_RIP_AST, Len.Fract.WALL_PC_RIP_AST);
    }

    public byte[] getWallPcRipAstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WALL_PC_RIP_AST, Pos.WALL_PC_RIP_AST);
        return buffer;
    }

    public void initWallPcRipAstSpaces() {
        fill(Pos.WALL_PC_RIP_AST, Len.WALL_PC_RIP_AST, Types.SPACE_CHAR);
    }

    public void setWallPcRipAstNull(String wallPcRipAstNull) {
        writeString(Pos.WALL_PC_RIP_AST_NULL, wallPcRipAstNull, Len.WALL_PC_RIP_AST_NULL);
    }

    /**Original name: WALL-PC-RIP-AST-NULL<br>*/
    public String getWallPcRipAstNull() {
        return readString(Pos.WALL_PC_RIP_AST_NULL, Len.WALL_PC_RIP_AST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WALL_PC_RIP_AST = 1;
        public static final int WALL_PC_RIP_AST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WALL_PC_RIP_AST = 4;
        public static final int WALL_PC_RIP_AST_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WALL_PC_RIP_AST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WALL_PC_RIP_AST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
