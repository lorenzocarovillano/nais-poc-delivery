package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-PC-REN-K3<br>
 * Variable: LQU-PC-REN-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquPcRenK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquPcRenK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_PC_REN_K3;
    }

    public void setLquPcRenK3(AfDecimal lquPcRenK3) {
        writeDecimalAsPacked(Pos.LQU_PC_REN_K3, lquPcRenK3.copy());
    }

    public void setLquPcRenK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_PC_REN_K3, Pos.LQU_PC_REN_K3);
    }

    /**Original name: LQU-PC-REN-K3<br>*/
    public AfDecimal getLquPcRenK3() {
        return readPackedAsDecimal(Pos.LQU_PC_REN_K3, Len.Int.LQU_PC_REN_K3, Len.Fract.LQU_PC_REN_K3);
    }

    public byte[] getLquPcRenK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_PC_REN_K3, Pos.LQU_PC_REN_K3);
        return buffer;
    }

    public void setLquPcRenK3Null(String lquPcRenK3Null) {
        writeString(Pos.LQU_PC_REN_K3_NULL, lquPcRenK3Null, Len.LQU_PC_REN_K3_NULL);
    }

    /**Original name: LQU-PC-REN-K3-NULL<br>*/
    public String getLquPcRenK3Null() {
        return readString(Pos.LQU_PC_REN_K3_NULL, Len.LQU_PC_REN_K3_NULL);
    }

    public String getLquPcRenK3NullFormatted() {
        return Functions.padBlanks(getLquPcRenK3Null(), Len.LQU_PC_REN_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_PC_REN_K3 = 1;
        public static final int LQU_PC_REN_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_PC_REN_K3 = 4;
        public static final int LQU_PC_REN_K3_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_PC_REN_K3 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_PC_REN_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
