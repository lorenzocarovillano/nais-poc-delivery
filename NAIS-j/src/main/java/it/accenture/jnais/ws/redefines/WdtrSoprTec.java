package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-SOPR-TEC<br>
 * Variable: WDTR-SOPR-TEC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_SOPR_TEC;
    }

    public void setWdtrSoprTec(AfDecimal wdtrSoprTec) {
        writeDecimalAsPacked(Pos.WDTR_SOPR_TEC, wdtrSoprTec.copy());
    }

    /**Original name: WDTR-SOPR-TEC<br>*/
    public AfDecimal getWdtrSoprTec() {
        return readPackedAsDecimal(Pos.WDTR_SOPR_TEC, Len.Int.WDTR_SOPR_TEC, Len.Fract.WDTR_SOPR_TEC);
    }

    public void setWdtrSoprTecNull(String wdtrSoprTecNull) {
        writeString(Pos.WDTR_SOPR_TEC_NULL, wdtrSoprTecNull, Len.WDTR_SOPR_TEC_NULL);
    }

    /**Original name: WDTR-SOPR-TEC-NULL<br>*/
    public String getWdtrSoprTecNull() {
        return readString(Pos.WDTR_SOPR_TEC_NULL, Len.WDTR_SOPR_TEC_NULL);
    }

    public String getWdtrSoprTecNullFormatted() {
        return Functions.padBlanks(getWdtrSoprTecNull(), Len.WDTR_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_SOPR_TEC = 1;
        public static final int WDTR_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_SOPR_TEC = 8;
        public static final int WDTR_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
