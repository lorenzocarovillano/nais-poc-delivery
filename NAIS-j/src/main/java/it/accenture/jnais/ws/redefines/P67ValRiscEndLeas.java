package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-VAL-RISC-END-LEAS<br>
 * Variable: P67-VAL-RISC-END-LEAS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67ValRiscEndLeas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67ValRiscEndLeas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_VAL_RISC_END_LEAS;
    }

    public void setP67ValRiscEndLeas(AfDecimal p67ValRiscEndLeas) {
        writeDecimalAsPacked(Pos.P67_VAL_RISC_END_LEAS, p67ValRiscEndLeas.copy());
    }

    public void setP67ValRiscEndLeasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_VAL_RISC_END_LEAS, Pos.P67_VAL_RISC_END_LEAS);
    }

    /**Original name: P67-VAL-RISC-END-LEAS<br>*/
    public AfDecimal getP67ValRiscEndLeas() {
        return readPackedAsDecimal(Pos.P67_VAL_RISC_END_LEAS, Len.Int.P67_VAL_RISC_END_LEAS, Len.Fract.P67_VAL_RISC_END_LEAS);
    }

    public byte[] getP67ValRiscEndLeasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_VAL_RISC_END_LEAS, Pos.P67_VAL_RISC_END_LEAS);
        return buffer;
    }

    public void setP67ValRiscEndLeasNull(String p67ValRiscEndLeasNull) {
        writeString(Pos.P67_VAL_RISC_END_LEAS_NULL, p67ValRiscEndLeasNull, Len.P67_VAL_RISC_END_LEAS_NULL);
    }

    /**Original name: P67-VAL-RISC-END-LEAS-NULL<br>*/
    public String getP67ValRiscEndLeasNull() {
        return readString(Pos.P67_VAL_RISC_END_LEAS_NULL, Len.P67_VAL_RISC_END_LEAS_NULL);
    }

    public String getP67ValRiscEndLeasNullFormatted() {
        return Functions.padBlanks(getP67ValRiscEndLeasNull(), Len.P67_VAL_RISC_END_LEAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_VAL_RISC_END_LEAS = 1;
        public static final int P67_VAL_RISC_END_LEAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_VAL_RISC_END_LEAS = 8;
        public static final int P67_VAL_RISC_END_LEAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_VAL_RISC_END_LEAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_VAL_RISC_END_LEAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
