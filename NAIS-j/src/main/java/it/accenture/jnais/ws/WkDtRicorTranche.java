package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-DT-RICOR-TRANCHE<br>
 * Variable: WK-DT-RICOR-TRANCHE from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WkDtRicorTranche {

    //==== PROPERTIES ====
    //Original name: WK-DT-RICOR-TRANCHE-AA
    private String aa = DefaultValues.stringVal(Len.AA);
    //Original name: WK-DT-RICOR-TRANCHE-MM
    private String mm = DefaultValues.stringVal(Len.MM);
    //Original name: WK-DT-RICOR-TRANCHE-GG
    private String gg = DefaultValues.stringVal(Len.GG);

    //==== METHODS ====
    public void setWkDtRicorTrancheNum(int wkDtRicorTrancheNum) {
        int position = 1;
        byte[] buffer = getWkDtRicorTrancheBytes();
        MarshalByte.writeInt(buffer, position, wkDtRicorTrancheNum, Len.Int.WK_DT_RICOR_TRANCHE_NUM, SignType.NO_SIGN);
        setWkDtRicorTrancheBytes(buffer);
    }

    public void setWkDtRicorTrancheNumFormatted(String wkDtRicorTrancheNum) {
        int position = 1;
        byte[] buffer = getWkDtRicorTrancheBytes();
        MarshalByte.writeString(buffer, position, Trunc.toNumeric(wkDtRicorTrancheNum, Len.WK_DT_RICOR_TRANCHE_NUM), Len.WK_DT_RICOR_TRANCHE_NUM);
        setWkDtRicorTrancheBytes(buffer);
    }

    /**Original name: WK-DT-RICOR-TRANCHE-NUM<br>*/
    public int getWkDtRicorTrancheNum() {
        int position = 1;
        return MarshalByte.readInt(getWkDtRicorTrancheBytes(), position, Len.Int.WK_DT_RICOR_TRANCHE_NUM, SignType.NO_SIGN);
    }

    public void setWkDtRicorTrancheBytes(byte[] buffer) {
        setWkDtRicorTrancheBytes(buffer, 1);
    }

    /**Original name: WK-DT-RICOR-TRANCHE<br>*/
    public byte[] getWkDtRicorTrancheBytes() {
        byte[] buffer = new byte[Len.WK_DT_RICOR_TRANCHE];
        return getWkDtRicorTrancheBytes(buffer, 1);
    }

    public void setWkDtRicorTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        aa = MarshalByte.readFixedString(buffer, position, Len.AA);
        position += Len.AA;
        mm = MarshalByte.readFixedString(buffer, position, Len.MM);
        position += Len.MM;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
    }

    public byte[] getWkDtRicorTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aa, Len.AA);
        position += Len.AA;
        MarshalByte.writeString(buffer, position, mm, Len.MM);
        position += Len.MM;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        return buffer;
    }

    public void setAaFormatted(String aa) {
        this.aa = Trunc.toUnsignedNumeric(aa, Len.AA);
    }

    public short getAa() {
        return NumericDisplay.asShort(this.aa);
    }

    public String getAaFormatted() {
        return this.aa;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WK_DT_RICOR_TRANCHE_NUM = 8;
        public static final int WK_DT_RICOR_TRANCHE = AA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_DT_RICOR_TRANCHE_NUM = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
