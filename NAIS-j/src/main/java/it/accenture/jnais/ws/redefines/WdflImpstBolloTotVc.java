package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-BOLLO-TOT-VC<br>
 * Variable: WDFL-IMPST-BOLLO-TOT-VC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstBolloTotVc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstBolloTotVc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_BOLLO_TOT_VC;
    }

    public void setWdflImpstBolloTotVc(AfDecimal wdflImpstBolloTotVc) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_BOLLO_TOT_VC, wdflImpstBolloTotVc.copy());
    }

    public void setWdflImpstBolloTotVcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_BOLLO_TOT_VC, Pos.WDFL_IMPST_BOLLO_TOT_VC);
    }

    /**Original name: WDFL-IMPST-BOLLO-TOT-VC<br>*/
    public AfDecimal getWdflImpstBolloTotVc() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_BOLLO_TOT_VC, Len.Int.WDFL_IMPST_BOLLO_TOT_VC, Len.Fract.WDFL_IMPST_BOLLO_TOT_VC);
    }

    public byte[] getWdflImpstBolloTotVcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_BOLLO_TOT_VC, Pos.WDFL_IMPST_BOLLO_TOT_VC);
        return buffer;
    }

    public void setWdflImpstBolloTotVcNull(String wdflImpstBolloTotVcNull) {
        writeString(Pos.WDFL_IMPST_BOLLO_TOT_VC_NULL, wdflImpstBolloTotVcNull, Len.WDFL_IMPST_BOLLO_TOT_VC_NULL);
    }

    /**Original name: WDFL-IMPST-BOLLO-TOT-VC-NULL<br>*/
    public String getWdflImpstBolloTotVcNull() {
        return readString(Pos.WDFL_IMPST_BOLLO_TOT_VC_NULL, Len.WDFL_IMPST_BOLLO_TOT_VC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_BOLLO_TOT_VC = 1;
        public static final int WDFL_IMPST_BOLLO_TOT_VC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_BOLLO_TOT_VC = 8;
        public static final int WDFL_IMPST_BOLLO_TOT_VC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_BOLLO_TOT_VC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_BOLLO_TOT_VC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
