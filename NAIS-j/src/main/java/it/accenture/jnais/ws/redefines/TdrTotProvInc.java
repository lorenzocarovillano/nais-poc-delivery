package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-PROV-INC<br>
 * Variable: TDR-TOT-PROV-INC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_PROV_INC;
    }

    public void setTdrTotProvInc(AfDecimal tdrTotProvInc) {
        writeDecimalAsPacked(Pos.TDR_TOT_PROV_INC, tdrTotProvInc.copy());
    }

    public void setTdrTotProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_PROV_INC, Pos.TDR_TOT_PROV_INC);
    }

    /**Original name: TDR-TOT-PROV-INC<br>*/
    public AfDecimal getTdrTotProvInc() {
        return readPackedAsDecimal(Pos.TDR_TOT_PROV_INC, Len.Int.TDR_TOT_PROV_INC, Len.Fract.TDR_TOT_PROV_INC);
    }

    public byte[] getTdrTotProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_PROV_INC, Pos.TDR_TOT_PROV_INC);
        return buffer;
    }

    public void setTdrTotProvIncNull(String tdrTotProvIncNull) {
        writeString(Pos.TDR_TOT_PROV_INC_NULL, tdrTotProvIncNull, Len.TDR_TOT_PROV_INC_NULL);
    }

    /**Original name: TDR-TOT-PROV-INC-NULL<br>*/
    public String getTdrTotProvIncNull() {
        return readString(Pos.TDR_TOT_PROV_INC_NULL, Len.TDR_TOT_PROV_INC_NULL);
    }

    public String getTdrTotProvIncNullFormatted() {
        return Functions.padBlanks(getTdrTotProvIncNull(), Len.TDR_TOT_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PROV_INC = 1;
        public static final int TDR_TOT_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PROV_INC = 8;
        public static final int TDR_TOT_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
