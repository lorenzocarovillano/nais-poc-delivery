package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-DUR-MM<br>
 * Variable: L3421-DUR-MM from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421DurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421DurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_DUR_MM;
    }

    public void setL3421DurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_DUR_MM, Pos.L3421_DUR_MM);
    }

    /**Original name: L3421-DUR-MM<br>*/
    public int getL3421DurMm() {
        return readPackedAsInt(Pos.L3421_DUR_MM, Len.Int.L3421_DUR_MM);
    }

    public byte[] getL3421DurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_DUR_MM, Pos.L3421_DUR_MM);
        return buffer;
    }

    /**Original name: L3421-DUR-MM-NULL<br>*/
    public String getL3421DurMmNull() {
        return readString(Pos.L3421_DUR_MM_NULL, Len.L3421_DUR_MM_NULL);
    }

    public String getL3421DurMmNullFormatted() {
        return Functions.padBlanks(getL3421DurMmNull(), Len.L3421_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_DUR_MM = 1;
        public static final int L3421_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_DUR_MM = 3;
        public static final int L3421_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
