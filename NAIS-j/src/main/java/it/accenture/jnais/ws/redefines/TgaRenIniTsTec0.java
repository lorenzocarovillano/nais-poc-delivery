package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-REN-INI-TS-TEC-0<br>
 * Variable: TGA-REN-INI-TS-TEC-0 from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaRenIniTsTec0 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaRenIniTsTec0() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_REN_INI_TS_TEC0;
    }

    public void setTgaRenIniTsTec0(AfDecimal tgaRenIniTsTec0) {
        writeDecimalAsPacked(Pos.TGA_REN_INI_TS_TEC0, tgaRenIniTsTec0.copy());
    }

    public void setTgaRenIniTsTec0FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_REN_INI_TS_TEC0, Pos.TGA_REN_INI_TS_TEC0);
    }

    /**Original name: TGA-REN-INI-TS-TEC-0<br>*/
    public AfDecimal getTgaRenIniTsTec0() {
        return readPackedAsDecimal(Pos.TGA_REN_INI_TS_TEC0, Len.Int.TGA_REN_INI_TS_TEC0, Len.Fract.TGA_REN_INI_TS_TEC0);
    }

    public byte[] getTgaRenIniTsTec0AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_REN_INI_TS_TEC0, Pos.TGA_REN_INI_TS_TEC0);
        return buffer;
    }

    public void setTgaRenIniTsTec0Null(String tgaRenIniTsTec0Null) {
        writeString(Pos.TGA_REN_INI_TS_TEC0_NULL, tgaRenIniTsTec0Null, Len.TGA_REN_INI_TS_TEC0_NULL);
    }

    /**Original name: TGA-REN-INI-TS-TEC-0-NULL<br>*/
    public String getTgaRenIniTsTec0Null() {
        return readString(Pos.TGA_REN_INI_TS_TEC0_NULL, Len.TGA_REN_INI_TS_TEC0_NULL);
    }

    public String getTgaRenIniTsTec0NullFormatted() {
        return Functions.padBlanks(getTgaRenIniTsTec0Null(), Len.TGA_REN_INI_TS_TEC0_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_REN_INI_TS_TEC0 = 1;
        public static final int TGA_REN_INI_TS_TEC0_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_REN_INI_TS_TEC0 = 8;
        public static final int TGA_REN_INI_TS_TEC0_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_REN_INI_TS_TEC0 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_REN_INI_TS_TEC0 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
