package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.ws.redefines.W670DtVlt;

/**Original name: W670-DATI-TIT<br>
 * Variables: W670-DATI-TIT from copybook LOAC0670<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class W670DatiTit implements ICopyable<W670DatiTit> {

    //==== PROPERTIES ====
    //Original name: W670-ID-TRCH-DI-GAR
    private int w670IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: W670-ID-TIT-CONT
    private int w670IdTitCont = DefaultValues.INT_VAL;
    //Original name: W670-DT-VLT
    private W670DtVlt w670DtVlt = new W670DtVlt();
    //Original name: W670-DT-INI-EFF
    private int w670DtIniEff = DefaultValues.INT_VAL;
    //Original name: W670-TP-STAT-TIT
    private String w670TpStatTit = DefaultValues.stringVal(Len.W670_TP_STAT_TIT);

    //==== CONSTRUCTORS ====
    public W670DatiTit() {
    }

    public W670DatiTit(W670DatiTit datiTit) {
        this();
        this.w670IdTrchDiGar = datiTit.w670IdTrchDiGar;
        this.w670IdTitCont = datiTit.w670IdTitCont;
        this.w670DtVlt = ((W670DtVlt)datiTit.w670DtVlt.copy());
        this.w670DtIniEff = datiTit.w670DtIniEff;
        this.w670TpStatTit = datiTit.w670TpStatTit;
    }

    //==== METHODS ====
    public void setDatiTitBytes(byte[] buffer, int offset) {
        int position = offset;
        w670IdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.W670_ID_TRCH_DI_GAR, 0);
        position += Len.W670_ID_TRCH_DI_GAR;
        w670IdTitCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.W670_ID_TIT_CONT, 0);
        position += Len.W670_ID_TIT_CONT;
        w670DtVlt.setW670DtVltFromBuffer(buffer, position);
        position += W670DtVlt.Len.W670_DT_VLT;
        w670DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.W670_DT_INI_EFF, 0);
        position += Len.W670_DT_INI_EFF;
        w670TpStatTit = MarshalByte.readString(buffer, position, Len.W670_TP_STAT_TIT);
    }

    public byte[] getDatiTitBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, w670IdTrchDiGar, Len.Int.W670_ID_TRCH_DI_GAR, 0);
        position += Len.W670_ID_TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, w670IdTitCont, Len.Int.W670_ID_TIT_CONT, 0);
        position += Len.W670_ID_TIT_CONT;
        w670DtVlt.getW670DtVltAsBuffer(buffer, position);
        position += W670DtVlt.Len.W670_DT_VLT;
        MarshalByte.writeIntAsPacked(buffer, position, w670DtIniEff, Len.Int.W670_DT_INI_EFF, 0);
        position += Len.W670_DT_INI_EFF;
        MarshalByte.writeString(buffer, position, w670TpStatTit, Len.W670_TP_STAT_TIT);
        return buffer;
    }

    public W670DatiTit initDatiTitSpaces() {
        w670IdTrchDiGar = Types.INVALID_INT_VAL;
        w670IdTitCont = Types.INVALID_INT_VAL;
        w670DtVlt.initW670DtVltSpaces();
        w670DtIniEff = Types.INVALID_INT_VAL;
        w670TpStatTit = "";
        return this;
    }

    public void setW670IdTrchDiGar(int w670IdTrchDiGar) {
        this.w670IdTrchDiGar = w670IdTrchDiGar;
    }

    public int getW670IdTrchDiGar() {
        return this.w670IdTrchDiGar;
    }

    public void setW670IdTitCont(int w670IdTitCont) {
        this.w670IdTitCont = w670IdTitCont;
    }

    public int getW670IdTitCont() {
        return this.w670IdTitCont;
    }

    public void setW670DtIniEff(int w670DtIniEff) {
        this.w670DtIniEff = w670DtIniEff;
    }

    public int getW670DtIniEff() {
        return this.w670DtIniEff;
    }

    public void setW670TpStatTit(String w670TpStatTit) {
        this.w670TpStatTit = Functions.subString(w670TpStatTit, Len.W670_TP_STAT_TIT);
    }

    public String getW670TpStatTit() {
        return this.w670TpStatTit;
    }

    public W670DtVlt getW670DtVlt() {
        return w670DtVlt;
    }

    public W670DatiTit copy() {
        return new W670DatiTit(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int W670_ID_TRCH_DI_GAR = 5;
        public static final int W670_ID_TIT_CONT = 5;
        public static final int W670_DT_INI_EFF = 5;
        public static final int W670_TP_STAT_TIT = 2;
        public static final int DATI_TIT = W670_ID_TRCH_DI_GAR + W670_ID_TIT_CONT + W670DtVlt.Len.W670_DT_VLT + W670_DT_INI_EFF + W670_TP_STAT_TIT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W670_ID_TRCH_DI_GAR = 9;
            public static final int W670_ID_TIT_CONT = 9;
            public static final int W670_DT_INI_EFF = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
