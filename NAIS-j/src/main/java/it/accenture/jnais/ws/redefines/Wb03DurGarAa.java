package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-GAR-AA<br>
 * Variable: WB03-DUR-GAR-AA from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DurGarAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DurGarAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR_GAR_AA;
    }

    public void setWb03DurGarAa(int wb03DurGarAa) {
        writeIntAsPacked(Pos.WB03_DUR_GAR_AA, wb03DurGarAa, Len.Int.WB03_DUR_GAR_AA);
    }

    public void setWb03DurGarAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR_GAR_AA, Pos.WB03_DUR_GAR_AA);
    }

    /**Original name: WB03-DUR-GAR-AA<br>*/
    public int getWb03DurGarAa() {
        return readPackedAsInt(Pos.WB03_DUR_GAR_AA, Len.Int.WB03_DUR_GAR_AA);
    }

    public byte[] getWb03DurGarAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR_GAR_AA, Pos.WB03_DUR_GAR_AA);
        return buffer;
    }

    public void setWb03DurGarAaNull(String wb03DurGarAaNull) {
        writeString(Pos.WB03_DUR_GAR_AA_NULL, wb03DurGarAaNull, Len.WB03_DUR_GAR_AA_NULL);
    }

    /**Original name: WB03-DUR-GAR-AA-NULL<br>*/
    public String getWb03DurGarAaNull() {
        return readString(Pos.WB03_DUR_GAR_AA_NULL, Len.WB03_DUR_GAR_AA_NULL);
    }

    public String getWb03DurGarAaNullFormatted() {
        return Functions.padBlanks(getWb03DurGarAaNull(), Len.WB03_DUR_GAR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR_GAR_AA = 1;
        public static final int WB03_DUR_GAR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR_GAR_AA = 3;
        public static final int WB03_DUR_GAR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR_GAR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
