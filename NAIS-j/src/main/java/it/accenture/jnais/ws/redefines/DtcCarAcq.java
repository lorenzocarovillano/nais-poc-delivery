package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-CAR-ACQ<br>
 * Variable: DTC-CAR-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_CAR_ACQ;
    }

    public void setDtcCarAcq(AfDecimal dtcCarAcq) {
        writeDecimalAsPacked(Pos.DTC_CAR_ACQ, dtcCarAcq.copy());
    }

    public void setDtcCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_CAR_ACQ, Pos.DTC_CAR_ACQ);
    }

    /**Original name: DTC-CAR-ACQ<br>*/
    public AfDecimal getDtcCarAcq() {
        return readPackedAsDecimal(Pos.DTC_CAR_ACQ, Len.Int.DTC_CAR_ACQ, Len.Fract.DTC_CAR_ACQ);
    }

    public byte[] getDtcCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_CAR_ACQ, Pos.DTC_CAR_ACQ);
        return buffer;
    }

    public void setDtcCarAcqNull(String dtcCarAcqNull) {
        writeString(Pos.DTC_CAR_ACQ_NULL, dtcCarAcqNull, Len.DTC_CAR_ACQ_NULL);
    }

    /**Original name: DTC-CAR-ACQ-NULL<br>*/
    public String getDtcCarAcqNull() {
        return readString(Pos.DTC_CAR_ACQ_NULL, Len.DTC_CAR_ACQ_NULL);
    }

    public String getDtcCarAcqNullFormatted() {
        return Functions.padBlanks(getDtcCarAcqNull(), Len.DTC_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_CAR_ACQ = 1;
        public static final int DTC_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_CAR_ACQ = 8;
        public static final int DTC_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
