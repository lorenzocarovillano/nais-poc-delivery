package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-PC-AZ<br>
 * Variable: DAD-PC-AZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadPcAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadPcAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_PC_AZ;
    }

    public void setDadPcAz(AfDecimal dadPcAz) {
        writeDecimalAsPacked(Pos.DAD_PC_AZ, dadPcAz.copy());
    }

    public void setDadPcAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_PC_AZ, Pos.DAD_PC_AZ);
    }

    /**Original name: DAD-PC-AZ<br>*/
    public AfDecimal getDadPcAz() {
        return readPackedAsDecimal(Pos.DAD_PC_AZ, Len.Int.DAD_PC_AZ, Len.Fract.DAD_PC_AZ);
    }

    public byte[] getDadPcAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_PC_AZ, Pos.DAD_PC_AZ);
        return buffer;
    }

    public void setDadPcAzNull(String dadPcAzNull) {
        writeString(Pos.DAD_PC_AZ_NULL, dadPcAzNull, Len.DAD_PC_AZ_NULL);
    }

    /**Original name: DAD-PC-AZ-NULL<br>*/
    public String getDadPcAzNull() {
        return readString(Pos.DAD_PC_AZ_NULL, Len.DAD_PC_AZ_NULL);
    }

    public String getDadPcAzNullFormatted() {
        return Functions.padBlanks(getDadPcAzNull(), Len.DAD_PC_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_PC_AZ = 1;
        public static final int DAD_PC_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_PC_AZ = 4;
        public static final int DAD_PC_AZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_PC_AZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DAD_PC_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
