package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccc00011;
import it.accenture.jnais.copy.WcomDatiDeroghe;
import it.accenture.jnais.copy.WcomStati;
import it.accenture.jnais.copy.WcomTastiDaAbilitare;
import it.accenture.jnais.ws.enums.WcomModificaDtdecor;
import it.accenture.jnais.ws.enums.WcomNavigabilita;
import it.accenture.jnais.ws.enums.WcomTipoOperazione;
import it.accenture.jnais.ws.enums.WcomTpVisualizPag;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WORK-COMMAREA<br>
 * Variable: WORK-COMMAREA from program LVES0268<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WorkCommareaLves0268 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LCCC0001
    private Lccc00011 lccc0001 = new Lccc00011();
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza = new WpolAreaPolizzaLccs0005();
    //Original name: WDCO-AREA-DT-COLL
    private WdcoAreaDtCollettiva wdcoAreaDtColl = new WdcoAreaDtCollettiva();
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLccs0005 wadeAreaAdesione = new WadeAreaAdesioneLccs0005();
    //Original name: WDAD-AREA-DEF-ADES
    private WdadAreaDefAdes wdadAreaDefAdes = new WdadAreaDefAdes();
    //Original name: WRAN-AREA-RAPP-ANAG
    private WranAreaRappAnag wranAreaRappAnag = new WranAreaRappAnag();
    //Original name: WDFA-AREA-DT-FISC-ADES
    private WdfaAreaDtFiscAdes wdfaAreaDtFiscAdes = new WdfaAreaDtFiscAdes();
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia = new WgrzAreaGaranziaLccs0005();
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTranche wtgaAreaTranche = new WtgaAreaTranche();
    //Original name: WSPG-AREA-SOPRAP-GAR
    private WspgAreaSoprGar wspgAreaSoprapGar = new WspgAreaSoprGar();
    //Original name: WSDI-AREA-STRA-INV
    private WsdiAreaStraInv wsdiAreaStraInv = new WsdiAreaStraInv();
    //Original name: WALL-AREA-ASSET
    private WallAreaAssetLves0268 wallAreaAsset = new WallAreaAssetLves0268();
    //Original name: WPMO-AREA-PARAM-MOV
    private WpmoAreaParamMovi wpmoAreaParamMov = new WpmoAreaParamMovi();
    //Original name: WPOG-AREA-PARAM-OGG
    private WpogAreaParamOggLves0245 wpogAreaParamOgg = new WpogAreaParamOggLves0245();
    //Original name: WBEP-AREA-BENEFICIARI
    private WbepAreaBeneficiari wbepAreaBeneficiari = new WbepAreaBeneficiari();
    //Original name: WQUE-AREA-QUEST
    private WqueAreaQuest wqueAreaQuest = new WqueAreaQuest();
    //Original name: WDEQ-AREA-DETT-QUEST
    private WdeqAreaDettQuestLves0269 wdeqAreaDettQuest = new WdeqAreaDettQuestLves0269();
    //Original name: WCLT-AREA-CLAUSOLE
    private WcltAreaClausole wcltAreaClausole = new WcltAreaClausole();
    //Original name: WOCO-AREA-OGG-COLLG
    private WocoAreaOggCollg wocoAreaOggCollg = new WocoAreaOggCollg();
    //Original name: WL23-AREA-VINC-PEG
    private Wl23AreaVincPeg wl23AreaVincPeg = new Wl23AreaVincPeg();
    //Original name: WRRE-AREA-RAP-RETE
    private WrreAreaRappRete wrreAreaRapRete = new WrreAreaRappRete();
    //Original name: WP67-AREA-EST-POLI-CPI-PR
    private Wp67AreaEstPoliCpiPr wp67AreaEstPoliCpiPr = new Wp67AreaEstPoliCpiPr();
    //Original name: WPAG-AREA-PAGINA
    private WpagAreaPagina wpagAreaPagina = new WpagAreaPagina();
    //Original name: WPAG-AREA-IAS
    private WpagAreaIas wpagAreaIas = new WpagAreaIas();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WORK_COMMAREA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWorkCommareaBytes(buf);
    }

    public String getWorkCommareaFormatted() {
        return MarshalByteExt.bufferToStr(getWorkCommareaBytes());
    }

    public void setWorkCommareaBytes(byte[] buffer) {
        setWorkCommareaBytes(buffer, 1);
    }

    public byte[] getWorkCommareaBytes() {
        byte[] buffer = new byte[Len.WORK_COMMAREA];
        return getWorkCommareaBytes(buffer, 1);
    }

    public void setWorkCommareaBytes(byte[] buffer, int offset) {
        int position = offset;
        setWcomAreaStatiBytes(buffer, position);
        position += Len.WCOM_AREA_STATI;
        wpolAreaPolizza.setWpolAreaPolizzaBytes(buffer, position);
        position += WpolAreaPolizzaLccs0005.Len.WPOL_AREA_POLIZZA;
        wdcoAreaDtColl.setWdcoAreaDtCollettivaBytes(buffer, position);
        position += WdcoAreaDtCollettiva.Len.WDCO_AREA_DT_COLLETTIVA;
        wadeAreaAdesione.setWadeAreaAdesioneBytes(buffer, position);
        position += WadeAreaAdesioneLccs0005.Len.WADE_AREA_ADESIONE;
        wdadAreaDefAdes.setWdadAreaDefAdesBytes(buffer, position);
        position += WdadAreaDefAdes.Len.WDAD_AREA_DEF_ADES;
        wranAreaRappAnag.setWranAreaRappAnagBytes(buffer, position);
        position += WranAreaRappAnag.Len.WRAN_AREA_RAPP_ANAG;
        wdfaAreaDtFiscAdes.setWdfaAreaDtFiscAdesBytes(buffer, position);
        position += WdfaAreaDtFiscAdes.Len.WDFA_AREA_DT_FISC_ADES;
        wgrzAreaGaranzia.setWgrzAreaGaranziaBytes(buffer, position);
        position += WgrzAreaGaranziaLccs0005.Len.WGRZ_AREA_GARANZIA;
        wtgaAreaTranche.setWtgaAreaTrancheBytes(buffer, position);
        position += WtgaAreaTranche.Len.WTGA_AREA_TRANCHE;
        wspgAreaSoprapGar.setWspgAreaSoprGarBytes(buffer, position);
        position += WspgAreaSoprGar.Len.WSPG_AREA_SOPR_GAR;
        wsdiAreaStraInv.setWsdiAreaStraInvBytes(buffer, position);
        position += WsdiAreaStraInv.Len.WSDI_AREA_STRA_INV;
        wallAreaAsset.setWallAreaAssetBytes(buffer, position);
        position += WallAreaAssetLves0268.Len.WALL_AREA_ASSET;
        wpmoAreaParamMov.setWpmoAreaParamMoviBytes(buffer, position);
        position += WpmoAreaParamMovi.Len.WPMO_AREA_PARAM_MOVI;
        wpogAreaParamOgg.setWpogAreaParamOggBytes(buffer, position);
        position += WpogAreaParamOggLves0245.Len.WPOG_AREA_PARAM_OGG;
        wbepAreaBeneficiari.setWbepAreaBeneficiariBytes(buffer, position);
        position += WbepAreaBeneficiari.Len.WBEP_AREA_BENEFICIARI;
        wqueAreaQuest.setWqueAreaQuestBytes(buffer, position);
        position += WqueAreaQuest.Len.WQUE_AREA_QUEST;
        wdeqAreaDettQuest.setWdeqAreaDettQuestBytes(buffer, position);
        position += WdeqAreaDettQuestLves0269.Len.WDEQ_AREA_DETT_QUEST;
        wcltAreaClausole.setWcltAreaClausoleBytes(buffer, position);
        position += WcltAreaClausole.Len.WCLT_AREA_CLAUSOLE;
        wocoAreaOggCollg.setWocoAreaOggCollgBytes(buffer, position);
        position += WocoAreaOggCollg.Len.WOCO_AREA_OGG_COLLG;
        wl23AreaVincPeg.setWl23AreaVincPegBytes(buffer, position);
        position += Wl23AreaVincPeg.Len.WL23_AREA_VINC_PEG;
        wrreAreaRapRete.setWrreAreaRappReteBytes(buffer, position);
        position += WrreAreaRappRete.Len.WRRE_AREA_RAPP_RETE;
        wp67AreaEstPoliCpiPr.setWp67AreaEstPoliCpiPrBytes(buffer, position);
        position += Wp67AreaEstPoliCpiPr.Len.WP67_AREA_EST_POLI_CPI_PR;
        wpagAreaPagina.setWpagAreaPaginaBytes(buffer, position);
        position += WpagAreaPagina.Len.WPAG_AREA_PAGINA;
        wpagAreaIas.setWpagAreaIasBytes(buffer, position);
    }

    public byte[] getWorkCommareaBytes(byte[] buffer, int offset) {
        int position = offset;
        getWcomAreaStatiBytes(buffer, position);
        position += Len.WCOM_AREA_STATI;
        wpolAreaPolizza.getWpolAreaPolizzaBytes(buffer, position);
        position += WpolAreaPolizzaLccs0005.Len.WPOL_AREA_POLIZZA;
        wdcoAreaDtColl.getWdcoAreaDtCollettivaBytes(buffer, position);
        position += WdcoAreaDtCollettiva.Len.WDCO_AREA_DT_COLLETTIVA;
        wadeAreaAdesione.getWadeAreaAdesioneBytes(buffer, position);
        position += WadeAreaAdesioneLccs0005.Len.WADE_AREA_ADESIONE;
        wdadAreaDefAdes.getWdadAreaDefAdesBytes(buffer, position);
        position += WdadAreaDefAdes.Len.WDAD_AREA_DEF_ADES;
        wranAreaRappAnag.getWranAreaRappAnagBytes(buffer, position);
        position += WranAreaRappAnag.Len.WRAN_AREA_RAPP_ANAG;
        wdfaAreaDtFiscAdes.getWdfaAreaDtFiscAdesBytes(buffer, position);
        position += WdfaAreaDtFiscAdes.Len.WDFA_AREA_DT_FISC_ADES;
        wgrzAreaGaranzia.getWgrzAreaGaranziaBytes(buffer, position);
        position += WgrzAreaGaranziaLccs0005.Len.WGRZ_AREA_GARANZIA;
        wtgaAreaTranche.getWtgaAreaTrancheBytes(buffer, position);
        position += WtgaAreaTranche.Len.WTGA_AREA_TRANCHE;
        wspgAreaSoprapGar.getWspgAreaSoprGarBytes(buffer, position);
        position += WspgAreaSoprGar.Len.WSPG_AREA_SOPR_GAR;
        wsdiAreaStraInv.getWsdiAreaStraInvBytes(buffer, position);
        position += WsdiAreaStraInv.Len.WSDI_AREA_STRA_INV;
        wallAreaAsset.getWallAreaAssetBytes(buffer, position);
        position += WallAreaAssetLves0268.Len.WALL_AREA_ASSET;
        wpmoAreaParamMov.getWpmoAreaParamMoviBytes(buffer, position);
        position += WpmoAreaParamMovi.Len.WPMO_AREA_PARAM_MOVI;
        wpogAreaParamOgg.getWpogAreaParamOggBytes(buffer, position);
        position += WpogAreaParamOggLves0245.Len.WPOG_AREA_PARAM_OGG;
        wbepAreaBeneficiari.getWbepAreaBeneficiariBytes(buffer, position);
        position += WbepAreaBeneficiari.Len.WBEP_AREA_BENEFICIARI;
        wqueAreaQuest.getWqueAreaQuestBytes(buffer, position);
        position += WqueAreaQuest.Len.WQUE_AREA_QUEST;
        wdeqAreaDettQuest.getWdeqAreaDettQuestBytes(buffer, position);
        position += WdeqAreaDettQuestLves0269.Len.WDEQ_AREA_DETT_QUEST;
        wcltAreaClausole.getWcltAreaClausoleBytes(buffer, position);
        position += WcltAreaClausole.Len.WCLT_AREA_CLAUSOLE;
        wocoAreaOggCollg.getWocoAreaOggCollgBytes(buffer, position);
        position += WocoAreaOggCollg.Len.WOCO_AREA_OGG_COLLG;
        wl23AreaVincPeg.getWl23AreaVincPegBytes(buffer, position);
        position += Wl23AreaVincPeg.Len.WL23_AREA_VINC_PEG;
        wrreAreaRapRete.getWrreAreaRappReteBytes(buffer, position);
        position += WrreAreaRappRete.Len.WRRE_AREA_RAPP_RETE;
        wp67AreaEstPoliCpiPr.getWp67AreaEstPoliCpiPrBytes(buffer, position);
        position += Wp67AreaEstPoliCpiPr.Len.WP67_AREA_EST_POLI_CPI_PR;
        wpagAreaPagina.getWpagAreaPaginaBytes(buffer, position);
        position += WpagAreaPagina.Len.WPAG_AREA_PAGINA;
        wpagAreaIas.getWpagAreaIasBytes(buffer, position);
        return buffer;
    }

    public void setWcomAreaStatiBytes(byte[] buffer, int offset) {
        int position = offset;
        lccc0001.getTipoOperazione().setTipoOperazione(MarshalByte.readString(buffer, position, WcomTipoOperazione.Len.TIPO_OPERAZIONE));
        position += WcomTipoOperazione.Len.TIPO_OPERAZIONE;
        lccc0001.getNavigabilita().setNavigabilita(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.getTastiDaAbilitare().setTastiDaAbilitareBytes(buffer, position);
        position += WcomTastiDaAbilitare.Len.TASTI_DA_ABILITARE;
        lccc0001.setDatiActuatorBytes(buffer, position);
        position += Lccc00011.Len.DATI_ACTUATOR;
        lccc0001.getStati().setStatiBytes(buffer, position);
        position += WcomStati.Len.STATI;
        lccc0001.getDatiDeroghe().setDatiDerogheBytes(buffer, position);
        position += WcomDatiDeroghe.Len.DATI_DEROGHE;
        lccc0001.setMovimentiAnnullBytes(buffer, position);
        position += Lccc00011.Len.MOVIMENTI_ANNULL;
        lccc0001.setIdMoviCrz(MarshalByte.readPackedAsInt(buffer, position, Lccc00011.Len.Int.ID_MOVI_CRZ, 0));
        position += Lccc00011.Len.ID_MOVI_CRZ;
        lccc0001.setFlagTariffaRischio(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.setAreaPlatfondBytes(buffer, position);
        position += Lccc00011.Len.AREA_PLATFOND;
        lccc0001.getModificaDtdecor().setModificaDtdecor(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.getStatusDer().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.setIdRichEst(MarshalByte.readPackedAsInt(buffer, position, Lccc00011.Len.Int.ID_RICH_EST, 0));
        position += Lccc00011.Len.ID_RICH_EST;
        lccc0001.setCodiceIniziativa(MarshalByte.readString(buffer, position, Lccc00011.Len.CODICE_INIZIATIVA));
        position += Lccc00011.Len.CODICE_INIZIATIVA;
        lccc0001.getTpVisualizPag().setTpVisualizPag(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccc0001.setFlr1(MarshalByte.readString(buffer, position, Lccc00011.Len.FLR1));
    }

    public byte[] getWcomAreaStatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, lccc0001.getTipoOperazione().getTipoOperazione(), WcomTipoOperazione.Len.TIPO_OPERAZIONE);
        position += WcomTipoOperazione.Len.TIPO_OPERAZIONE;
        MarshalByte.writeChar(buffer, position, lccc0001.getNavigabilita().getNavigabilita());
        position += Types.CHAR_SIZE;
        lccc0001.getTastiDaAbilitare().getTastiDaAbilitareBytes(buffer, position);
        position += WcomTastiDaAbilitare.Len.TASTI_DA_ABILITARE;
        lccc0001.getDatiActuatorBytes(buffer, position);
        position += Lccc00011.Len.DATI_ACTUATOR;
        lccc0001.getStati().getStatiBytes(buffer, position);
        position += WcomStati.Len.STATI;
        lccc0001.getDatiDeroghe().getDatiDerogheBytes(buffer, position);
        position += WcomDatiDeroghe.Len.DATI_DEROGHE;
        lccc0001.getMovimentiAnnullBytes(buffer, position);
        position += Lccc00011.Len.MOVIMENTI_ANNULL;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0001.getIdMoviCrz(), Lccc00011.Len.Int.ID_MOVI_CRZ, 0);
        position += Lccc00011.Len.ID_MOVI_CRZ;
        MarshalByte.writeChar(buffer, position, lccc0001.getFlagTariffaRischio());
        position += Types.CHAR_SIZE;
        lccc0001.getAreaPlatfondBytes(buffer, position);
        position += Lccc00011.Len.AREA_PLATFOND;
        MarshalByte.writeChar(buffer, position, lccc0001.getModificaDtdecor().getModificaDtdecor());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, lccc0001.getStatusDer().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0001.getIdRichEst(), Lccc00011.Len.Int.ID_RICH_EST, 0);
        position += Lccc00011.Len.ID_RICH_EST;
        MarshalByte.writeString(buffer, position, lccc0001.getCodiceIniziativa(), Lccc00011.Len.CODICE_INIZIATIVA);
        position += Lccc00011.Len.CODICE_INIZIATIVA;
        MarshalByte.writeChar(buffer, position, lccc0001.getTpVisualizPag().getTpVisualizPag());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, lccc0001.getFlr1(), Lccc00011.Len.FLR1);
        return buffer;
    }

    public Lccc00011 getLccc0001() {
        return lccc0001;
    }

    public WadeAreaAdesioneLccs0005 getWadeAreaAdesione() {
        return wadeAreaAdesione;
    }

    public WallAreaAssetLves0268 getWallAreaAsset() {
        return wallAreaAsset;
    }

    public WbepAreaBeneficiari getWbepAreaBeneficiari() {
        return wbepAreaBeneficiari;
    }

    public WcltAreaClausole getWcltAreaClausole() {
        return wcltAreaClausole;
    }

    public WdadAreaDefAdes getWdadAreaDefAdes() {
        return wdadAreaDefAdes;
    }

    public WdcoAreaDtCollettiva getWdcoAreaDtColl() {
        return wdcoAreaDtColl;
    }

    public WdeqAreaDettQuestLves0269 getWdeqAreaDettQuest() {
        return wdeqAreaDettQuest;
    }

    public WdfaAreaDtFiscAdes getWdfaAreaDtFiscAdes() {
        return wdfaAreaDtFiscAdes;
    }

    public WgrzAreaGaranziaLccs0005 getWgrzAreaGaranzia() {
        return wgrzAreaGaranzia;
    }

    public Wl23AreaVincPeg getWl23AreaVincPeg() {
        return wl23AreaVincPeg;
    }

    public WocoAreaOggCollg getWocoAreaOggCollg() {
        return wocoAreaOggCollg;
    }

    public Wp67AreaEstPoliCpiPr getWp67AreaEstPoliCpiPr() {
        return wp67AreaEstPoliCpiPr;
    }

    public WpagAreaIas getWpagAreaIas() {
        return wpagAreaIas;
    }

    public WpagAreaPagina getWpagAreaPagina() {
        return wpagAreaPagina;
    }

    public WpmoAreaParamMovi getWpmoAreaParamMov() {
        return wpmoAreaParamMov;
    }

    public WpogAreaParamOggLves0245 getWpogAreaParamOgg() {
        return wpogAreaParamOgg;
    }

    public WpolAreaPolizzaLccs0005 getWpolAreaPolizza() {
        return wpolAreaPolizza;
    }

    public WqueAreaQuest getWqueAreaQuest() {
        return wqueAreaQuest;
    }

    public WranAreaRappAnag getWranAreaRappAnag() {
        return wranAreaRappAnag;
    }

    public WrreAreaRappRete getWrreAreaRapRete() {
        return wrreAreaRapRete;
    }

    public WsdiAreaStraInv getWsdiAreaStraInv() {
        return wsdiAreaStraInv;
    }

    public WspgAreaSoprGar getWspgAreaSoprapGar() {
        return wspgAreaSoprapGar;
    }

    public WtgaAreaTranche getWtgaAreaTranche() {
        return wtgaAreaTranche;
    }

    @Override
    public byte[] serialize() {
        return getWorkCommareaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WCOM_AREA_STATI = WcomTipoOperazione.Len.TIPO_OPERAZIONE + WcomNavigabilita.Len.NAVIGABILITA + WcomTastiDaAbilitare.Len.TASTI_DA_ABILITARE + Lccc00011.Len.DATI_ACTUATOR + WcomStati.Len.STATI + WcomDatiDeroghe.Len.DATI_DEROGHE + Lccc00011.Len.MOVIMENTI_ANNULL + Lccc00011.Len.ID_MOVI_CRZ + Lccc00011.Len.FLAG_TARIFFA_RISCHIO + Lccc00011.Len.AREA_PLATFOND + WcomModificaDtdecor.Len.MODIFICA_DTDECOR + WpolStatus.Len.STATUS + Lccc00011.Len.ID_RICH_EST + Lccc00011.Len.CODICE_INIZIATIVA + WcomTpVisualizPag.Len.TP_VISUALIZ_PAG + Lccc00011.Len.FLR1;
        public static final int WORK_COMMAREA = WCOM_AREA_STATI + WpolAreaPolizzaLccs0005.Len.WPOL_AREA_POLIZZA + WdcoAreaDtCollettiva.Len.WDCO_AREA_DT_COLLETTIVA + WadeAreaAdesioneLccs0005.Len.WADE_AREA_ADESIONE + WdadAreaDefAdes.Len.WDAD_AREA_DEF_ADES + WranAreaRappAnag.Len.WRAN_AREA_RAPP_ANAG + WdfaAreaDtFiscAdes.Len.WDFA_AREA_DT_FISC_ADES + WgrzAreaGaranziaLccs0005.Len.WGRZ_AREA_GARANZIA + WtgaAreaTranche.Len.WTGA_AREA_TRANCHE + WspgAreaSoprGar.Len.WSPG_AREA_SOPR_GAR + WsdiAreaStraInv.Len.WSDI_AREA_STRA_INV + WallAreaAssetLves0268.Len.WALL_AREA_ASSET + WpmoAreaParamMovi.Len.WPMO_AREA_PARAM_MOVI + WpogAreaParamOggLves0245.Len.WPOG_AREA_PARAM_OGG + WbepAreaBeneficiari.Len.WBEP_AREA_BENEFICIARI + WqueAreaQuest.Len.WQUE_AREA_QUEST + WdeqAreaDettQuestLves0269.Len.WDEQ_AREA_DETT_QUEST + WcltAreaClausole.Len.WCLT_AREA_CLAUSOLE + WocoAreaOggCollg.Len.WOCO_AREA_OGG_COLLG + Wl23AreaVincPeg.Len.WL23_AREA_VINC_PEG + WrreAreaRappRete.Len.WRRE_AREA_RAPP_RETE + Wp67AreaEstPoliCpiPr.Len.WP67_AREA_EST_POLI_CPI_PR + WpagAreaPagina.Len.WPAG_AREA_PAGINA + WpagAreaIas.Len.WPAG_AREA_IAS;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
