package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-MANFEE-RICOR<br>
 * Variable: DTC-MANFEE-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcManfeeRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcManfeeRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_MANFEE_RICOR;
    }

    public void setDtcManfeeRicor(AfDecimal dtcManfeeRicor) {
        writeDecimalAsPacked(Pos.DTC_MANFEE_RICOR, dtcManfeeRicor.copy());
    }

    public void setDtcManfeeRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_MANFEE_RICOR, Pos.DTC_MANFEE_RICOR);
    }

    /**Original name: DTC-MANFEE-RICOR<br>*/
    public AfDecimal getDtcManfeeRicor() {
        return readPackedAsDecimal(Pos.DTC_MANFEE_RICOR, Len.Int.DTC_MANFEE_RICOR, Len.Fract.DTC_MANFEE_RICOR);
    }

    public byte[] getDtcManfeeRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_MANFEE_RICOR, Pos.DTC_MANFEE_RICOR);
        return buffer;
    }

    public void setDtcManfeeRicorNull(String dtcManfeeRicorNull) {
        writeString(Pos.DTC_MANFEE_RICOR_NULL, dtcManfeeRicorNull, Len.DTC_MANFEE_RICOR_NULL);
    }

    /**Original name: DTC-MANFEE-RICOR-NULL<br>*/
    public String getDtcManfeeRicorNull() {
        return readString(Pos.DTC_MANFEE_RICOR_NULL, Len.DTC_MANFEE_RICOR_NULL);
    }

    public String getDtcManfeeRicorNullFormatted() {
        return Functions.padBlanks(getDtcManfeeRicorNull(), Len.DTC_MANFEE_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_MANFEE_RICOR = 1;
        public static final int DTC_MANFEE_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_MANFEE_RICOR = 8;
        public static final int DTC_MANFEE_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_MANFEE_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_MANFEE_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
