package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: W660-DATI-MANFEE-CONTESTO<br>
 * Variable: W660-DATI-MANFEE-CONTESTO from copybook LOAC0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class W660DatiManfeeContesto {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.DATI_MANFEE_CONTESTO);
    public static final String SI = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setDatiManfeeContesto(String datiManfeeContesto) {
        this.value = Functions.subString(datiManfeeContesto, Len.DATI_MANFEE_CONTESTO);
    }

    public String getDatiManfeeContesto() {
        return this.value;
    }

    public boolean isSi() {
        return value.equals(SI);
    }

    public void setW660ManfeeContNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DATI_MANFEE_CONTESTO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
