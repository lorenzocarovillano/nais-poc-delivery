package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-VARIABILI<br>
 * Variable: WS-VARIABILI from program IVVS0212<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsVariabiliIvvs0212 {

    //==== PROPERTIES ====
    //Original name: WS-COMPAGNIA
    private int compagnia = DefaultValues.INT_VAL;
    //Original name: WS-MOVIMENTO
    private String movimento = DefaultValues.stringVal(Len.MOVIMENTO);
    //Original name: WS-NUM-ACCESSO
    private String numAccesso = DefaultValues.stringVal(Len.NUM_ACCESSO);

    //==== METHODS ====
    public void setCompagnia(int compagnia) {
        this.compagnia = compagnia;
    }

    public int getCompagnia() {
        return this.compagnia;
    }

    public void setMovimentoFormatted(String movimento) {
        this.movimento = Trunc.toUnsignedNumeric(movimento, Len.MOVIMENTO);
    }

    public int getMovimento() {
        return NumericDisplay.asInt(this.movimento);
    }

    public void setNumAccesso(short numAccesso) {
        this.numAccesso = NumericDisplay.asString(numAccesso, Len.NUM_ACCESSO);
    }

    public short getNumAccesso() {
        return NumericDisplay.asShort(this.numAccesso);
    }

    public String getNumAccessoFormatted() {
        return this.numAccesso;
    }

    public String getNumAccessoAsString() {
        return getNumAccessoFormatted();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COMPAGNIA = 5;
        public static final int MOVIMENTO = 5;
        public static final int NUM_ACCESSO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
