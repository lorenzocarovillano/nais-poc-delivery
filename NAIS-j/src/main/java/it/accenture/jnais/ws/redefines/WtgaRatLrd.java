package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-RAT-LRD<br>
 * Variable: WTGA-RAT-LRD from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaRatLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaRatLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_RAT_LRD;
    }

    public void setWtgaRatLrd(AfDecimal wtgaRatLrd) {
        writeDecimalAsPacked(Pos.WTGA_RAT_LRD, wtgaRatLrd.copy());
    }

    public void setWtgaRatLrdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_RAT_LRD, Pos.WTGA_RAT_LRD);
    }

    /**Original name: WTGA-RAT-LRD<br>*/
    public AfDecimal getWtgaRatLrd() {
        return readPackedAsDecimal(Pos.WTGA_RAT_LRD, Len.Int.WTGA_RAT_LRD, Len.Fract.WTGA_RAT_LRD);
    }

    public byte[] getWtgaRatLrdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_RAT_LRD, Pos.WTGA_RAT_LRD);
        return buffer;
    }

    public void initWtgaRatLrdSpaces() {
        fill(Pos.WTGA_RAT_LRD, Len.WTGA_RAT_LRD, Types.SPACE_CHAR);
    }

    public void setWtgaRatLrdNull(String wtgaRatLrdNull) {
        writeString(Pos.WTGA_RAT_LRD_NULL, wtgaRatLrdNull, Len.WTGA_RAT_LRD_NULL);
    }

    /**Original name: WTGA-RAT-LRD-NULL<br>*/
    public String getWtgaRatLrdNull() {
        return readString(Pos.WTGA_RAT_LRD_NULL, Len.WTGA_RAT_LRD_NULL);
    }

    public String getWtgaRatLrdNullFormatted() {
        return Functions.padBlanks(getWtgaRatLrdNull(), Len.WTGA_RAT_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_RAT_LRD = 1;
        public static final int WTGA_RAT_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_RAT_LRD = 8;
        public static final int WTGA_RAT_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_RAT_LRD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_RAT_LRD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
