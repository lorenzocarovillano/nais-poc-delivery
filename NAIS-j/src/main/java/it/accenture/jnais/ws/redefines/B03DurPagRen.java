package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DUR-PAG-REN<br>
 * Variable: B03-DUR-PAG-REN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DurPagRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DurPagRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DUR_PAG_REN;
    }

    public void setB03DurPagRen(int b03DurPagRen) {
        writeIntAsPacked(Pos.B03_DUR_PAG_REN, b03DurPagRen, Len.Int.B03_DUR_PAG_REN);
    }

    public void setB03DurPagRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DUR_PAG_REN, Pos.B03_DUR_PAG_REN);
    }

    /**Original name: B03-DUR-PAG-REN<br>*/
    public int getB03DurPagRen() {
        return readPackedAsInt(Pos.B03_DUR_PAG_REN, Len.Int.B03_DUR_PAG_REN);
    }

    public byte[] getB03DurPagRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DUR_PAG_REN, Pos.B03_DUR_PAG_REN);
        return buffer;
    }

    public void setB03DurPagRenNull(String b03DurPagRenNull) {
        writeString(Pos.B03_DUR_PAG_REN_NULL, b03DurPagRenNull, Len.B03_DUR_PAG_REN_NULL);
    }

    /**Original name: B03-DUR-PAG-REN-NULL<br>*/
    public String getB03DurPagRenNull() {
        return readString(Pos.B03_DUR_PAG_REN_NULL, Len.B03_DUR_PAG_REN_NULL);
    }

    public String getB03DurPagRenNullFormatted() {
        return Functions.padBlanks(getB03DurPagRenNull(), Len.B03_DUR_PAG_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DUR_PAG_REN = 1;
        public static final int B03_DUR_PAG_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DUR_PAG_REN = 3;
        public static final int B03_DUR_PAG_REN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DUR_PAG_REN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
