package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-PROV-ACQ-2AA<br>
 * Variable: DTC-PROV-ACQ-2AA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcProvAcq2aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcProvAcq2aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_PROV_ACQ2AA;
    }

    public void setDtcProvAcq2aa(AfDecimal dtcProvAcq2aa) {
        writeDecimalAsPacked(Pos.DTC_PROV_ACQ2AA, dtcProvAcq2aa.copy());
    }

    public void setDtcProvAcq2aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_PROV_ACQ2AA, Pos.DTC_PROV_ACQ2AA);
    }

    /**Original name: DTC-PROV-ACQ-2AA<br>*/
    public AfDecimal getDtcProvAcq2aa() {
        return readPackedAsDecimal(Pos.DTC_PROV_ACQ2AA, Len.Int.DTC_PROV_ACQ2AA, Len.Fract.DTC_PROV_ACQ2AA);
    }

    public byte[] getDtcProvAcq2aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_PROV_ACQ2AA, Pos.DTC_PROV_ACQ2AA);
        return buffer;
    }

    public void setDtcProvAcq2aaNull(String dtcProvAcq2aaNull) {
        writeString(Pos.DTC_PROV_ACQ2AA_NULL, dtcProvAcq2aaNull, Len.DTC_PROV_ACQ2AA_NULL);
    }

    /**Original name: DTC-PROV-ACQ-2AA-NULL<br>*/
    public String getDtcProvAcq2aaNull() {
        return readString(Pos.DTC_PROV_ACQ2AA_NULL, Len.DTC_PROV_ACQ2AA_NULL);
    }

    public String getDtcProvAcq2aaNullFormatted() {
        return Functions.padBlanks(getDtcProvAcq2aaNull(), Len.DTC_PROV_ACQ2AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_PROV_ACQ2AA = 1;
        public static final int DTC_PROV_ACQ2AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_PROV_ACQ2AA = 8;
        public static final int DTC_PROV_ACQ2AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_PROV_ACQ2AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_PROV_ACQ2AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
