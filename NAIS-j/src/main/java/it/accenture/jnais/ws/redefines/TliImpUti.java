package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-IMP-UTI<br>
 * Variable: TLI-IMP-UTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliImpUti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliImpUti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_IMP_UTI;
    }

    public void setTliImpUti(AfDecimal tliImpUti) {
        writeDecimalAsPacked(Pos.TLI_IMP_UTI, tliImpUti.copy());
    }

    public void setTliImpUtiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_IMP_UTI, Pos.TLI_IMP_UTI);
    }

    /**Original name: TLI-IMP-UTI<br>*/
    public AfDecimal getTliImpUti() {
        return readPackedAsDecimal(Pos.TLI_IMP_UTI, Len.Int.TLI_IMP_UTI, Len.Fract.TLI_IMP_UTI);
    }

    public byte[] getTliImpUtiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_IMP_UTI, Pos.TLI_IMP_UTI);
        return buffer;
    }

    public void setTliImpUtiNull(String tliImpUtiNull) {
        writeString(Pos.TLI_IMP_UTI_NULL, tliImpUtiNull, Len.TLI_IMP_UTI_NULL);
    }

    /**Original name: TLI-IMP-UTI-NULL<br>*/
    public String getTliImpUtiNull() {
        return readString(Pos.TLI_IMP_UTI_NULL, Len.TLI_IMP_UTI_NULL);
    }

    public String getTliImpUtiNullFormatted() {
        return Functions.padBlanks(getTliImpUtiNull(), Len.TLI_IMP_UTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_IMP_UTI = 1;
        public static final int TLI_IMP_UTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_IMP_UTI = 8;
        public static final int TLI_IMP_UTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_IMP_UTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_IMP_UTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
