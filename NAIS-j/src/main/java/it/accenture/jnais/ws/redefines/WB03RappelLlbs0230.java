package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-RAPPEL<br>
 * Variable: W-B03-RAPPEL from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03RappelLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03RappelLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_RAPPEL;
    }

    public void setwB03Rappel(AfDecimal wB03Rappel) {
        writeDecimalAsPacked(Pos.W_B03_RAPPEL, wB03Rappel.copy());
    }

    /**Original name: W-B03-RAPPEL<br>*/
    public AfDecimal getwB03Rappel() {
        return readPackedAsDecimal(Pos.W_B03_RAPPEL, Len.Int.W_B03_RAPPEL, Len.Fract.W_B03_RAPPEL);
    }

    public byte[] getwB03RappelAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_RAPPEL, Pos.W_B03_RAPPEL);
        return buffer;
    }

    public void setwB03RappelNull(String wB03RappelNull) {
        writeString(Pos.W_B03_RAPPEL_NULL, wB03RappelNull, Len.W_B03_RAPPEL_NULL);
    }

    /**Original name: W-B03-RAPPEL-NULL<br>*/
    public String getwB03RappelNull() {
        return readString(Pos.W_B03_RAPPEL_NULL, Len.W_B03_RAPPEL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_RAPPEL = 1;
        public static final int W_B03_RAPPEL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_RAPPEL = 8;
        public static final int W_B03_RAPPEL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_RAPPEL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_RAPPEL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
