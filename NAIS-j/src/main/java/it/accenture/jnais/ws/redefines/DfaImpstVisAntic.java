package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPST-VIS-ANTIC<br>
 * Variable: DFA-IMPST-VIS-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpstVisAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpstVisAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPST_VIS_ANTIC;
    }

    public void setDfaImpstVisAntic(AfDecimal dfaImpstVisAntic) {
        writeDecimalAsPacked(Pos.DFA_IMPST_VIS_ANTIC, dfaImpstVisAntic.copy());
    }

    public void setDfaImpstVisAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPST_VIS_ANTIC, Pos.DFA_IMPST_VIS_ANTIC);
    }

    /**Original name: DFA-IMPST-VIS-ANTIC<br>*/
    public AfDecimal getDfaImpstVisAntic() {
        return readPackedAsDecimal(Pos.DFA_IMPST_VIS_ANTIC, Len.Int.DFA_IMPST_VIS_ANTIC, Len.Fract.DFA_IMPST_VIS_ANTIC);
    }

    public byte[] getDfaImpstVisAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPST_VIS_ANTIC, Pos.DFA_IMPST_VIS_ANTIC);
        return buffer;
    }

    public void setDfaImpstVisAnticNull(String dfaImpstVisAnticNull) {
        writeString(Pos.DFA_IMPST_VIS_ANTIC_NULL, dfaImpstVisAnticNull, Len.DFA_IMPST_VIS_ANTIC_NULL);
    }

    /**Original name: DFA-IMPST-VIS-ANTIC-NULL<br>*/
    public String getDfaImpstVisAnticNull() {
        return readString(Pos.DFA_IMPST_VIS_ANTIC_NULL, Len.DFA_IMPST_VIS_ANTIC_NULL);
    }

    public String getDfaImpstVisAnticNullFormatted() {
        return Functions.padBlanks(getDfaImpstVisAnticNull(), Len.DFA_IMPST_VIS_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_VIS_ANTIC = 1;
        public static final int DFA_IMPST_VIS_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_VIS_ANTIC = 8;
        public static final int DFA_IMPST_VIS_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_VIS_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_VIS_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
