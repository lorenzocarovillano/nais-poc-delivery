package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0140-AREA-VARIABILI-P<br>
 * Variables: ISPC0140-AREA-VARIABILI-P from copybook ISPC0140<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0140AreaVariabiliP {

    //==== PROPERTIES ====
    //Original name: ISPC0140-CODICE-VARIABILE-P
    private String codiceVariabileP = DefaultValues.stringVal(Len.CODICE_VARIABILE_P);
    //Original name: ISPC0140-TIPO-DATO-P
    private char tipoDatoP = DefaultValues.CHAR_VAL;
    //Original name: ISPC0140-VAL-GENERICO-P
    private String valGenericoP = DefaultValues.stringVal(Len.VAL_GENERICO_P);

    //==== METHODS ====
    public void setAreaVariabiliPBytes(byte[] buffer, int offset) {
        int position = offset;
        codiceVariabileP = MarshalByte.readString(buffer, position, Len.CODICE_VARIABILE_P);
        position += Len.CODICE_VARIABILE_P;
        tipoDatoP = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        valGenericoP = MarshalByte.readString(buffer, position, Len.VAL_GENERICO_P);
    }

    public byte[] getAreaVariabiliPBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codiceVariabileP, Len.CODICE_VARIABILE_P);
        position += Len.CODICE_VARIABILE_P;
        MarshalByte.writeChar(buffer, position, tipoDatoP);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, valGenericoP, Len.VAL_GENERICO_P);
        return buffer;
    }

    public void initAreaVariabiliPSpaces() {
        codiceVariabileP = "";
        tipoDatoP = Types.SPACE_CHAR;
        valGenericoP = "";
    }

    public void setCodiceVariabileP(String codiceVariabileP) {
        this.codiceVariabileP = Functions.subString(codiceVariabileP, Len.CODICE_VARIABILE_P);
    }

    public String getCodiceVariabileP() {
        return this.codiceVariabileP;
    }

    public void setTipoDatoP(char tipoDatoP) {
        this.tipoDatoP = tipoDatoP;
    }

    public char getTipoDatoP() {
        return this.tipoDatoP;
    }

    public void setValGenericoP(String valGenericoP) {
        this.valGenericoP = Functions.subString(valGenericoP, Len.VAL_GENERICO_P);
    }

    public String getValGenericoP() {
        return this.valGenericoP;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_VARIABILE_P = 12;
        public static final int TIPO_DATO_P = 1;
        public static final int VAL_GENERICO_P = 60;
        public static final int AREA_VARIABILI_P = CODICE_VARIABILE_P + TIPO_DATO_P + VAL_GENERICO_P;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
