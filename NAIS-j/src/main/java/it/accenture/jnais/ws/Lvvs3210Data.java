package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvpol1;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3210<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3210Data {

    //==== PROPERTIES ====
    //Original name: WK-PGM-CALLED
    private String wkPgmCalled = "LDBSF110";
    //Original name: LDBVF111
    private Ldbvf111 ldbvf111 = new Ldbvf111();
    //Original name: WPOL-ELE-POLI-MAX
    private short wpolElePoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1 lccvpol1 = new Lccvpol1();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgmCalled() {
        return this.wkPgmCalled;
    }

    public void setWpolAreaPolizzaFormatted(String data) {
        byte[] buffer = new byte[Len.WPOL_AREA_POLIZZA];
        MarshalByte.writeString(buffer, 1, data, Len.WPOL_AREA_POLIZZA);
        setWpolAreaPolizzaBytes(buffer, 1);
    }

    public void setWpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        wpolElePoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWpolTabPoliBytes(buffer, position);
    }

    public void setWpolElePoliMax(short wpolElePoliMax) {
        this.wpolElePoliMax = wpolElePoliMax;
    }

    public short getWpolElePoliMax() {
        return this.wpolElePoliMax;
    }

    public void setWpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1.Len.Int.ID_PTF, 0));
        position += Lccvpol1.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvpol1 getLccvpol1() {
        return lccvpol1;
    }

    public Ldbvf111 getLdbvf111() {
        return ldbvf111;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_ELE_POLI_MAX = 2;
        public static final int WPOL_TAB_POLI = WpolStatus.Len.STATUS + Lccvpol1.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int WPOL_AREA_POLIZZA = WPOL_ELE_POLI_MAX + WPOL_TAB_POLI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
