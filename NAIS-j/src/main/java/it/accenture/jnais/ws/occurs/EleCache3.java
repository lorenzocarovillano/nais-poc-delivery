package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ELE-CACHE-3<br>
 * Variables: ELE-CACHE-3 from program LRGS0660<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class EleCache3 {

    //==== PROPERTIES ====
    //Original name: REC3-ID-GAR
    private String idGar = DefaultValues.stringVal(Len.ID_GAR);
    //Original name: REC3-COD-GAR
    private String codGar = DefaultValues.stringVal(Len.COD_GAR);
    //Original name: REC3-TP-GAR
    private String tpGar = DefaultValues.stringVal(Len.TP_GAR);
    //Original name: REC3-TP-INVST
    private String tpInvst = DefaultValues.stringVal(Len.TP_INVST);
    //Original name: REC3-CUM-PREM-INVST-AA-RIF
    private AfDecimal cumPremInvstAaRif = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC3-CUM-PREM-VERS-AA-RIF
    private AfDecimal cumPremVersAaRif = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC3-PREST-MATUR-AA-RIF
    private AfDecimal prestMaturAaRif = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC3-CAP-LIQ-GA
    private AfDecimal capLiqGa = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC3-RISC-TOT-GA
    private AfDecimal riscTotGa = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC3-CUM-PREM-VERS-EC-PREC
    private AfDecimal cumPremVersEcPrec = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC3-PREST-MATUR-EC-PREC
    private AfDecimal prestMaturEcPrec = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC3-IMP-LRD-RISC-PARZ-AP-GAR
    private AfDecimal impLrdRiscParzApGar = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC3-IMP-LRD-RISC-PARZ-AC-GAR
    private AfDecimal impLrdRiscParzAcGar = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC3-TASSO-ANN-RIVAL-PREST-GAR
    private AfDecimal tassoAnnRivalPrestGar = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: REC3-TASSO-TECNICO
    private AfDecimal tassoTecnico = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: REC3-REND-LOR-GEST-SEP-GAR
    private AfDecimal rendLorGestSepGar = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: REC3-ALIQ-RETROC-RICON-GAR
    private AfDecimal aliqRetrocRiconGar = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: REC3-TASSO-ANN-REND-RETROC-GAR
    private AfDecimal tassoAnnRendRetrocGar = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: REC3-PC-COMM-GEST-GAR
    private AfDecimal pcCommGestGar = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: REC3-REN-MIN-TRNUT
    private AfDecimal renMinTrnut = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: REC3-REN-MIN-GARTO
    private AfDecimal renMinGarto = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: REC3-FL-STORNATE-ANN
    private char flStornateAnn = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setIdGarFormatted(String idGar) {
        this.idGar = Trunc.toUnsignedNumeric(idGar, Len.ID_GAR);
    }

    public int getIdGar() {
        return NumericDisplay.asInt(this.idGar);
    }

    public void setCodGar(String codGar) {
        this.codGar = Functions.subString(codGar, Len.COD_GAR);
    }

    public String getCodGar() {
        return this.codGar;
    }

    public void setTpGarFormatted(String tpGar) {
        this.tpGar = Trunc.toUnsignedNumeric(tpGar, Len.TP_GAR);
    }

    public short getTpGar() {
        return NumericDisplay.asShort(this.tpGar);
    }

    public void setTpInvstFormatted(String tpInvst) {
        this.tpInvst = Trunc.toUnsignedNumeric(tpInvst, Len.TP_INVST);
    }

    public short getTpInvst() {
        return NumericDisplay.asShort(this.tpInvst);
    }

    public void setCumPremInvstAaRif(AfDecimal cumPremInvstAaRif) {
        this.cumPremInvstAaRif.assign(cumPremInvstAaRif);
    }

    public AfDecimal getCumPremInvstAaRif() {
        return this.cumPremInvstAaRif.copy();
    }

    public void setCumPremVersAaRif(AfDecimal cumPremVersAaRif) {
        this.cumPremVersAaRif.assign(cumPremVersAaRif);
    }

    public AfDecimal getCumPremVersAaRif() {
        return this.cumPremVersAaRif.copy();
    }

    public void setPrestMaturAaRif(AfDecimal prestMaturAaRif) {
        this.prestMaturAaRif.assign(prestMaturAaRif);
    }

    public AfDecimal getPrestMaturAaRif() {
        return this.prestMaturAaRif.copy();
    }

    public void setCapLiqGa(AfDecimal capLiqGa) {
        this.capLiqGa.assign(capLiqGa);
    }

    public AfDecimal getCapLiqGa() {
        return this.capLiqGa.copy();
    }

    public void setRiscTotGa(AfDecimal riscTotGa) {
        this.riscTotGa.assign(riscTotGa);
    }

    public AfDecimal getRiscTotGa() {
        return this.riscTotGa.copy();
    }

    public void setCumPremVersEcPrec(AfDecimal cumPremVersEcPrec) {
        this.cumPremVersEcPrec.assign(cumPremVersEcPrec);
    }

    public AfDecimal getCumPremVersEcPrec() {
        return this.cumPremVersEcPrec.copy();
    }

    public void setPrestMaturEcPrec(AfDecimal prestMaturEcPrec) {
        this.prestMaturEcPrec.assign(prestMaturEcPrec);
    }

    public AfDecimal getPrestMaturEcPrec() {
        return this.prestMaturEcPrec.copy();
    }

    public void setImpLrdRiscParzApGar(AfDecimal impLrdRiscParzApGar) {
        this.impLrdRiscParzApGar.assign(impLrdRiscParzApGar);
    }

    public AfDecimal getImpLrdRiscParzApGar() {
        return this.impLrdRiscParzApGar.copy();
    }

    public void setImpLrdRiscParzAcGar(AfDecimal impLrdRiscParzAcGar) {
        this.impLrdRiscParzAcGar.assign(impLrdRiscParzAcGar);
    }

    public AfDecimal getImpLrdRiscParzAcGar() {
        return this.impLrdRiscParzAcGar.copy();
    }

    public void setTassoAnnRivalPrestGar(AfDecimal tassoAnnRivalPrestGar) {
        this.tassoAnnRivalPrestGar.assign(tassoAnnRivalPrestGar);
    }

    public AfDecimal getTassoAnnRivalPrestGar() {
        return this.tassoAnnRivalPrestGar.copy();
    }

    public void setTassoTecnico(AfDecimal tassoTecnico) {
        this.tassoTecnico.assign(tassoTecnico);
    }

    public AfDecimal getTassoTecnico() {
        return this.tassoTecnico.copy();
    }

    public void setRendLorGestSepGar(AfDecimal rendLorGestSepGar) {
        this.rendLorGestSepGar.assign(rendLorGestSepGar);
    }

    public AfDecimal getRendLorGestSepGar() {
        return this.rendLorGestSepGar.copy();
    }

    public void setAliqRetrocRiconGar(AfDecimal aliqRetrocRiconGar) {
        this.aliqRetrocRiconGar.assign(aliqRetrocRiconGar);
    }

    public AfDecimal getAliqRetrocRiconGar() {
        return this.aliqRetrocRiconGar.copy();
    }

    public void setTassoAnnRendRetrocGar(AfDecimal tassoAnnRendRetrocGar) {
        this.tassoAnnRendRetrocGar.assign(tassoAnnRendRetrocGar);
    }

    public AfDecimal getTassoAnnRendRetrocGar() {
        return this.tassoAnnRendRetrocGar.copy();
    }

    public void setPcCommGestGar(AfDecimal pcCommGestGar) {
        this.pcCommGestGar.assign(pcCommGestGar);
    }

    public AfDecimal getPcCommGestGar() {
        return this.pcCommGestGar.copy();
    }

    public void setRenMinTrnut(AfDecimal renMinTrnut) {
        this.renMinTrnut.assign(renMinTrnut);
    }

    public AfDecimal getRenMinTrnut() {
        return this.renMinTrnut.copy();
    }

    public void setRenMinGarto(AfDecimal renMinGarto) {
        this.renMinGarto.assign(renMinGarto);
    }

    public AfDecimal getRenMinGarto() {
        return this.renMinGarto.copy();
    }

    public void setFlStornateAnn(char flStornateAnn) {
        this.flStornateAnn = flStornateAnn;
    }

    public char getFlStornateAnn() {
        return this.flStornateAnn;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_GAR = 9;
        public static final int COD_GAR = 12;
        public static final int TP_GAR = 2;
        public static final int TP_INVST = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
