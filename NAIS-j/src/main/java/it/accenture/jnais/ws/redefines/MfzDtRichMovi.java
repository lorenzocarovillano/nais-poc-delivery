package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MFZ-DT-RICH-MOVI<br>
 * Variable: MFZ-DT-RICH-MOVI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MfzDtRichMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MfzDtRichMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MFZ_DT_RICH_MOVI;
    }

    public void setMfzDtRichMovi(int mfzDtRichMovi) {
        writeIntAsPacked(Pos.MFZ_DT_RICH_MOVI, mfzDtRichMovi, Len.Int.MFZ_DT_RICH_MOVI);
    }

    public void setMfzDtRichMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MFZ_DT_RICH_MOVI, Pos.MFZ_DT_RICH_MOVI);
    }

    /**Original name: MFZ-DT-RICH-MOVI<br>*/
    public int getMfzDtRichMovi() {
        return readPackedAsInt(Pos.MFZ_DT_RICH_MOVI, Len.Int.MFZ_DT_RICH_MOVI);
    }

    public byte[] getMfzDtRichMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MFZ_DT_RICH_MOVI, Pos.MFZ_DT_RICH_MOVI);
        return buffer;
    }

    public void setMfzDtRichMoviNull(String mfzDtRichMoviNull) {
        writeString(Pos.MFZ_DT_RICH_MOVI_NULL, mfzDtRichMoviNull, Len.MFZ_DT_RICH_MOVI_NULL);
    }

    /**Original name: MFZ-DT-RICH-MOVI-NULL<br>*/
    public String getMfzDtRichMoviNull() {
        return readString(Pos.MFZ_DT_RICH_MOVI_NULL, Len.MFZ_DT_RICH_MOVI_NULL);
    }

    public String getMfzDtRichMoviNullFormatted() {
        return Functions.padBlanks(getMfzDtRichMoviNull(), Len.MFZ_DT_RICH_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MFZ_DT_RICH_MOVI = 1;
        public static final int MFZ_DT_RICH_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MFZ_DT_RICH_MOVI = 5;
        public static final int MFZ_DT_RICH_MOVI_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MFZ_DT_RICH_MOVI = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
