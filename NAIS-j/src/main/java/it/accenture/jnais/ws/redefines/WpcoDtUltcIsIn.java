package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-IS-IN<br>
 * Variable: WPCO-DT-ULTC-IS-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcIsIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcIsIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_IS_IN;
    }

    public void setWpcoDtUltcIsIn(int wpcoDtUltcIsIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_IS_IN, wpcoDtUltcIsIn, Len.Int.WPCO_DT_ULTC_IS_IN);
    }

    public void setDpcoDtUltcIsInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_IS_IN, Pos.WPCO_DT_ULTC_IS_IN);
    }

    /**Original name: WPCO-DT-ULTC-IS-IN<br>*/
    public int getWpcoDtUltcIsIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_IS_IN, Len.Int.WPCO_DT_ULTC_IS_IN);
    }

    public byte[] getWpcoDtUltcIsInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_IS_IN, Pos.WPCO_DT_ULTC_IS_IN);
        return buffer;
    }

    public void setWpcoDtUltcIsInNull(String wpcoDtUltcIsInNull) {
        writeString(Pos.WPCO_DT_ULTC_IS_IN_NULL, wpcoDtUltcIsInNull, Len.WPCO_DT_ULTC_IS_IN_NULL);
    }

    /**Original name: WPCO-DT-ULTC-IS-IN-NULL<br>*/
    public String getWpcoDtUltcIsInNull() {
        return readString(Pos.WPCO_DT_ULTC_IS_IN_NULL, Len.WPCO_DT_ULTC_IS_IN_NULL);
    }

    public String getWpcoDtUltcIsInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcIsInNull(), Len.WPCO_DT_ULTC_IS_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_IS_IN = 1;
        public static final int WPCO_DT_ULTC_IS_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_IS_IN = 5;
        public static final int WPCO_DT_ULTC_IS_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_IS_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
