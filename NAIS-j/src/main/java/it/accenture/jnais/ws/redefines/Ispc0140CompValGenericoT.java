package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0140-COMP-VAL-GENERICO-T<br>
 * Variable: ISPC0140-COMP-VAL-GENERICO-T from program ISPS0140<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ispc0140CompValGenericoT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Ispc0140CompValGenericoT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISPC0140_COMP_VAL_GENERICO_T;
    }

    public void setIspc0140CompValGenericoTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISPC0140_COMP_VAL_GENERICO_T, Pos.ISPC0140_COMP_VAL_GENERICO_T);
    }

    public byte[] getIspc0140CompValGenericoTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISPC0140_COMP_VAL_GENERICO_T, Pos.ISPC0140_COMP_VAL_GENERICO_T);
        return buffer;
    }

    public void initIspc0140CompValGenericoTSpaces() {
        fill(Pos.ISPC0140_COMP_VAL_GENERICO_T, Len.ISPC0140_COMP_VAL_GENERICO_T, Types.SPACE_CHAR);
    }

    /**Original name: ISPC0140-COMP-VALORE-IMP-T<br>*/
    public AfDecimal getIspc0140CompValoreImpT() {
        return readDecimal(Pos.COMP_VALORE_IMP_T, Len.Int.ISPC0140_COMP_VALORE_IMP_T, Len.Fract.ISPC0140_COMP_VALORE_IMP_T);
    }

    public String getIspc0140CompValoreImpTFormatted() {
        return readFixedString(Pos.COMP_VALORE_IMP_T, Len.COMP_VALORE_IMP_T);
    }

    /**Original name: ISPC0140-COMP-VALORE-PERC-T<br>*/
    public AfDecimal getIspc0140CompValorePercT() {
        return readDecimal(Pos.COMP_VALORE_PERC_T, Len.Int.ISPC0140_COMP_VALORE_PERC_T, Len.Fract.ISPC0140_COMP_VALORE_PERC_T, SignType.NO_SIGN);
    }

    /**Original name: ISPC0140-COMP-VALORE-STR-T<br>*/
    public String getIspc0140CompValoreStrT() {
        return readString(Pos.COMP_VALORE_STR_T, Len.ISPC0140_COMP_VALORE_STR_T);
    }

    public String getIspc0140CompValoreStrTFormatted() {
        return Functions.padBlanks(getIspc0140CompValoreStrT(), Len.ISPC0140_COMP_VALORE_STR_T);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISPC0140_COMP_VAL_GENERICO_T = 1;
        public static final int ISPC0140_COMP_VAL_IMP_GEN_T = 1;
        public static final int COMP_VALORE_IMP_T = ISPC0140_COMP_VAL_IMP_GEN_T;
        public static final int VAL_IMP_FILLER_T = COMP_VALORE_IMP_T + Len.COMP_VALORE_IMP_T;
        public static final int ISPC0140_COMP_VAL_PERC_GEN_T = 1;
        public static final int COMP_VALORE_PERC_T = ISPC0140_COMP_VAL_PERC_GEN_T;
        public static final int VAL_PERC_FILLER_T = COMP_VALORE_PERC_T + Len.COMP_VALORE_PERC_T;
        public static final int ISPC0140_COMP_VAL_STR_GEN_T = 1;
        public static final int COMP_VALORE_STR_T = ISPC0140_COMP_VAL_STR_GEN_T;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int COMP_VALORE_IMP_T = 18;
        public static final int COMP_VALORE_PERC_T = 14;
        public static final int ISPC0140_COMP_VAL_GENERICO_T = 60;
        public static final int ISPC0140_COMP_VALORE_STR_T = 60;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISPC0140_COMP_VALORE_IMP_T = 11;
            public static final int ISPC0140_COMP_VALORE_PERC_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISPC0140_COMP_VALORE_IMP_T = 7;
            public static final int ISPC0140_COMP_VALORE_PERC_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
