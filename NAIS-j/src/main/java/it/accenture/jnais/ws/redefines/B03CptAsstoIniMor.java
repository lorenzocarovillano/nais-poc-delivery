package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CPT-ASSTO-INI-MOR<br>
 * Variable: B03-CPT-ASSTO-INI-MOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CptAsstoIniMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CptAsstoIniMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CPT_ASSTO_INI_MOR;
    }

    public void setB03CptAsstoIniMor(AfDecimal b03CptAsstoIniMor) {
        writeDecimalAsPacked(Pos.B03_CPT_ASSTO_INI_MOR, b03CptAsstoIniMor.copy());
    }

    public void setB03CptAsstoIniMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CPT_ASSTO_INI_MOR, Pos.B03_CPT_ASSTO_INI_MOR);
    }

    /**Original name: B03-CPT-ASSTO-INI-MOR<br>*/
    public AfDecimal getB03CptAsstoIniMor() {
        return readPackedAsDecimal(Pos.B03_CPT_ASSTO_INI_MOR, Len.Int.B03_CPT_ASSTO_INI_MOR, Len.Fract.B03_CPT_ASSTO_INI_MOR);
    }

    public byte[] getB03CptAsstoIniMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CPT_ASSTO_INI_MOR, Pos.B03_CPT_ASSTO_INI_MOR);
        return buffer;
    }

    public void setB03CptAsstoIniMorNull(String b03CptAsstoIniMorNull) {
        writeString(Pos.B03_CPT_ASSTO_INI_MOR_NULL, b03CptAsstoIniMorNull, Len.B03_CPT_ASSTO_INI_MOR_NULL);
    }

    /**Original name: B03-CPT-ASSTO-INI-MOR-NULL<br>*/
    public String getB03CptAsstoIniMorNull() {
        return readString(Pos.B03_CPT_ASSTO_INI_MOR_NULL, Len.B03_CPT_ASSTO_INI_MOR_NULL);
    }

    public String getB03CptAsstoIniMorNullFormatted() {
        return Functions.padBlanks(getB03CptAsstoIniMorNull(), Len.B03_CPT_ASSTO_INI_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CPT_ASSTO_INI_MOR = 1;
        public static final int B03_CPT_ASSTO_INI_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CPT_ASSTO_INI_MOR = 8;
        public static final int B03_CPT_ASSTO_INI_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CPT_ASSTO_INI_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CPT_ASSTO_INI_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
