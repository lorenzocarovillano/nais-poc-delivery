package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMPB-PROV-INC<br>
 * Variable: WTGA-IMPB-PROV-INC from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpbProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpbProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMPB_PROV_INC;
    }

    public void setWtgaImpbProvInc(AfDecimal wtgaImpbProvInc) {
        writeDecimalAsPacked(Pos.WTGA_IMPB_PROV_INC, wtgaImpbProvInc.copy());
    }

    public void setWtgaImpbProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMPB_PROV_INC, Pos.WTGA_IMPB_PROV_INC);
    }

    /**Original name: WTGA-IMPB-PROV-INC<br>*/
    public AfDecimal getWtgaImpbProvInc() {
        return readPackedAsDecimal(Pos.WTGA_IMPB_PROV_INC, Len.Int.WTGA_IMPB_PROV_INC, Len.Fract.WTGA_IMPB_PROV_INC);
    }

    public byte[] getWtgaImpbProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMPB_PROV_INC, Pos.WTGA_IMPB_PROV_INC);
        return buffer;
    }

    public void initWtgaImpbProvIncSpaces() {
        fill(Pos.WTGA_IMPB_PROV_INC, Len.WTGA_IMPB_PROV_INC, Types.SPACE_CHAR);
    }

    public void setWtgaImpbProvIncNull(String wtgaImpbProvIncNull) {
        writeString(Pos.WTGA_IMPB_PROV_INC_NULL, wtgaImpbProvIncNull, Len.WTGA_IMPB_PROV_INC_NULL);
    }

    /**Original name: WTGA-IMPB-PROV-INC-NULL<br>*/
    public String getWtgaImpbProvIncNull() {
        return readString(Pos.WTGA_IMPB_PROV_INC_NULL, Len.WTGA_IMPB_PROV_INC_NULL);
    }

    public String getWtgaImpbProvIncNullFormatted() {
        return Functions.padBlanks(getWtgaImpbProvIncNull(), Len.WTGA_IMPB_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMPB_PROV_INC = 1;
        public static final int WTGA_IMPB_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMPB_PROV_INC = 8;
        public static final int WTGA_IMPB_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMPB_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMPB_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
