package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WP58-ID-MOVI-CHIU<br>
 * Variable: WP58-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp58IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp58IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP58_ID_MOVI_CHIU;
    }

    public void setWp58IdMoviChiu(int wp58IdMoviChiu) {
        writeIntAsPacked(Pos.WP58_ID_MOVI_CHIU, wp58IdMoviChiu, Len.Int.WP58_ID_MOVI_CHIU);
    }

    public void setWp58IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP58_ID_MOVI_CHIU, Pos.WP58_ID_MOVI_CHIU);
    }

    /**Original name: WP58-ID-MOVI-CHIU<br>*/
    public int getWp58IdMoviChiu() {
        return readPackedAsInt(Pos.WP58_ID_MOVI_CHIU, Len.Int.WP58_ID_MOVI_CHIU);
    }

    public byte[] getWp58IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP58_ID_MOVI_CHIU, Pos.WP58_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWp58IdMoviChiuSpaces() {
        fill(Pos.WP58_ID_MOVI_CHIU, Len.WP58_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setDp58IdMoviChiuNull(String dp58IdMoviChiuNull) {
        writeString(Pos.WP58_ID_MOVI_CHIU_NULL, dp58IdMoviChiuNull, Len.DP58_ID_MOVI_CHIU_NULL);
    }

    /**Original name: DP58-ID-MOVI-CHIU-NULL<br>*/
    public String getDp58IdMoviChiuNull() {
        return readString(Pos.WP58_ID_MOVI_CHIU_NULL, Len.DP58_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP58_ID_MOVI_CHIU = 1;
        public static final int WP58_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP58_ID_MOVI_CHIU = 5;
        public static final int DP58_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP58_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
