package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WE12-CUM-PRSTZ<br>
 * Variable: WE12-CUM-PRSTZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class We12CumPrstz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public We12CumPrstz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WE12_CUM_PRSTZ;
    }

    public void setWe12CumPrstz(AfDecimal we12CumPrstz) {
        writeDecimalAsPacked(Pos.WE12_CUM_PRSTZ, we12CumPrstz.copy());
    }

    /**Original name: WE12-CUM-PRSTZ<br>*/
    public AfDecimal getWe12CumPrstz() {
        return readPackedAsDecimal(Pos.WE12_CUM_PRSTZ, Len.Int.WE12_CUM_PRSTZ, Len.Fract.WE12_CUM_PRSTZ);
    }

    public byte[] getWe12CumPrstzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WE12_CUM_PRSTZ, Pos.WE12_CUM_PRSTZ);
        return buffer;
    }

    public void setWe12CumPrstzNull(String we12CumPrstzNull) {
        writeString(Pos.WE12_CUM_PRSTZ_NULL, we12CumPrstzNull, Len.WE12_CUM_PRSTZ_NULL);
    }

    /**Original name: WE12-CUM-PRSTZ-NULL<br>*/
    public String getWe12CumPrstzNull() {
        return readString(Pos.WE12_CUM_PRSTZ_NULL, Len.WE12_CUM_PRSTZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WE12_CUM_PRSTZ = 1;
        public static final int WE12_CUM_PRSTZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WE12_CUM_PRSTZ = 8;
        public static final int WE12_CUM_PRSTZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WE12_CUM_PRSTZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WE12_CUM_PRSTZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
