package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.QueIdMoviChiu;

/**Original name: QUEST<br>
 * Variable: QUEST from copybook IDBVQUE1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class QuestIdbsque0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: QUE-ID-QUEST
    private int queIdQuest = DefaultValues.INT_VAL;
    //Original name: QUE-ID-RAPP-ANA
    private int queIdRappAna = DefaultValues.INT_VAL;
    //Original name: QUE-ID-MOVI-CRZ
    private int queIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: QUE-ID-MOVI-CHIU
    private QueIdMoviChiu queIdMoviChiu = new QueIdMoviChiu();
    //Original name: QUE-DT-INI-EFF
    private int queDtIniEff = DefaultValues.INT_VAL;
    //Original name: QUE-DT-END-EFF
    private int queDtEndEff = DefaultValues.INT_VAL;
    //Original name: QUE-COD-COMP-ANIA
    private int queCodCompAnia = DefaultValues.INT_VAL;
    //Original name: QUE-COD-QUEST
    private String queCodQuest = DefaultValues.stringVal(Len.QUE_COD_QUEST);
    //Original name: QUE-TP-QUEST
    private String queTpQuest = DefaultValues.stringVal(Len.QUE_TP_QUEST);
    //Original name: QUE-FL-VST-MED
    private char queFlVstMed = DefaultValues.CHAR_VAL;
    //Original name: QUE-FL-STAT-BUON-SAL
    private char queFlStatBuonSal = DefaultValues.CHAR_VAL;
    //Original name: QUE-DS-RIGA
    private long queDsRiga = DefaultValues.LONG_VAL;
    //Original name: QUE-DS-OPER-SQL
    private char queDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: QUE-DS-VER
    private int queDsVer = DefaultValues.INT_VAL;
    //Original name: QUE-DS-TS-INI-CPTZ
    private long queDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: QUE-DS-TS-END-CPTZ
    private long queDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: QUE-DS-UTENTE
    private String queDsUtente = DefaultValues.stringVal(Len.QUE_DS_UTENTE);
    //Original name: QUE-DS-STATO-ELAB
    private char queDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: QUE-TP-ADEGZ
    private String queTpAdegz = DefaultValues.stringVal(Len.QUE_TP_ADEGZ);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.QUEST;
    }

    @Override
    public void deserialize(byte[] buf) {
        setQuestBytes(buf);
    }

    public void setQuestFormatted(String data) {
        byte[] buffer = new byte[Len.QUEST];
        MarshalByte.writeString(buffer, 1, data, Len.QUEST);
        setQuestBytes(buffer, 1);
    }

    public String getQuestFormatted() {
        return MarshalByteExt.bufferToStr(getQuestBytes());
    }

    public void setQuestBytes(byte[] buffer) {
        setQuestBytes(buffer, 1);
    }

    public byte[] getQuestBytes() {
        byte[] buffer = new byte[Len.QUEST];
        return getQuestBytes(buffer, 1);
    }

    public void setQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        queIdQuest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.QUE_ID_QUEST, 0);
        position += Len.QUE_ID_QUEST;
        queIdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.QUE_ID_RAPP_ANA, 0);
        position += Len.QUE_ID_RAPP_ANA;
        queIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.QUE_ID_MOVI_CRZ, 0);
        position += Len.QUE_ID_MOVI_CRZ;
        queIdMoviChiu.setQueIdMoviChiuFromBuffer(buffer, position);
        position += QueIdMoviChiu.Len.QUE_ID_MOVI_CHIU;
        queDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.QUE_DT_INI_EFF, 0);
        position += Len.QUE_DT_INI_EFF;
        queDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.QUE_DT_END_EFF, 0);
        position += Len.QUE_DT_END_EFF;
        queCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.QUE_COD_COMP_ANIA, 0);
        position += Len.QUE_COD_COMP_ANIA;
        queCodQuest = MarshalByte.readString(buffer, position, Len.QUE_COD_QUEST);
        position += Len.QUE_COD_QUEST;
        queTpQuest = MarshalByte.readString(buffer, position, Len.QUE_TP_QUEST);
        position += Len.QUE_TP_QUEST;
        queFlVstMed = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        queFlStatBuonSal = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        queDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.QUE_DS_RIGA, 0);
        position += Len.QUE_DS_RIGA;
        queDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        queDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.QUE_DS_VER, 0);
        position += Len.QUE_DS_VER;
        queDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.QUE_DS_TS_INI_CPTZ, 0);
        position += Len.QUE_DS_TS_INI_CPTZ;
        queDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.QUE_DS_TS_END_CPTZ, 0);
        position += Len.QUE_DS_TS_END_CPTZ;
        queDsUtente = MarshalByte.readString(buffer, position, Len.QUE_DS_UTENTE);
        position += Len.QUE_DS_UTENTE;
        queDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        queTpAdegz = MarshalByte.readString(buffer, position, Len.QUE_TP_ADEGZ);
    }

    public byte[] getQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, queIdQuest, Len.Int.QUE_ID_QUEST, 0);
        position += Len.QUE_ID_QUEST;
        MarshalByte.writeIntAsPacked(buffer, position, queIdRappAna, Len.Int.QUE_ID_RAPP_ANA, 0);
        position += Len.QUE_ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, queIdMoviCrz, Len.Int.QUE_ID_MOVI_CRZ, 0);
        position += Len.QUE_ID_MOVI_CRZ;
        queIdMoviChiu.getQueIdMoviChiuAsBuffer(buffer, position);
        position += QueIdMoviChiu.Len.QUE_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, queDtIniEff, Len.Int.QUE_DT_INI_EFF, 0);
        position += Len.QUE_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, queDtEndEff, Len.Int.QUE_DT_END_EFF, 0);
        position += Len.QUE_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, queCodCompAnia, Len.Int.QUE_COD_COMP_ANIA, 0);
        position += Len.QUE_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, queCodQuest, Len.QUE_COD_QUEST);
        position += Len.QUE_COD_QUEST;
        MarshalByte.writeString(buffer, position, queTpQuest, Len.QUE_TP_QUEST);
        position += Len.QUE_TP_QUEST;
        MarshalByte.writeChar(buffer, position, queFlVstMed);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, queFlStatBuonSal);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, queDsRiga, Len.Int.QUE_DS_RIGA, 0);
        position += Len.QUE_DS_RIGA;
        MarshalByte.writeChar(buffer, position, queDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, queDsVer, Len.Int.QUE_DS_VER, 0);
        position += Len.QUE_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, queDsTsIniCptz, Len.Int.QUE_DS_TS_INI_CPTZ, 0);
        position += Len.QUE_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, queDsTsEndCptz, Len.Int.QUE_DS_TS_END_CPTZ, 0);
        position += Len.QUE_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, queDsUtente, Len.QUE_DS_UTENTE);
        position += Len.QUE_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, queDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, queTpAdegz, Len.QUE_TP_ADEGZ);
        return buffer;
    }

    public void setQueIdQuest(int queIdQuest) {
        this.queIdQuest = queIdQuest;
    }

    public int getQueIdQuest() {
        return this.queIdQuest;
    }

    public void setQueIdRappAna(int queIdRappAna) {
        this.queIdRappAna = queIdRappAna;
    }

    public int getQueIdRappAna() {
        return this.queIdRappAna;
    }

    public void setQueIdMoviCrz(int queIdMoviCrz) {
        this.queIdMoviCrz = queIdMoviCrz;
    }

    public int getQueIdMoviCrz() {
        return this.queIdMoviCrz;
    }

    public void setQueDtIniEff(int queDtIniEff) {
        this.queDtIniEff = queDtIniEff;
    }

    public int getQueDtIniEff() {
        return this.queDtIniEff;
    }

    public void setQueDtEndEff(int queDtEndEff) {
        this.queDtEndEff = queDtEndEff;
    }

    public int getQueDtEndEff() {
        return this.queDtEndEff;
    }

    public void setQueCodCompAnia(int queCodCompAnia) {
        this.queCodCompAnia = queCodCompAnia;
    }

    public int getQueCodCompAnia() {
        return this.queCodCompAnia;
    }

    public void setQueCodQuest(String queCodQuest) {
        this.queCodQuest = Functions.subString(queCodQuest, Len.QUE_COD_QUEST);
    }

    public String getQueCodQuest() {
        return this.queCodQuest;
    }

    public void setQueTpQuest(String queTpQuest) {
        this.queTpQuest = Functions.subString(queTpQuest, Len.QUE_TP_QUEST);
    }

    public String getQueTpQuest() {
        return this.queTpQuest;
    }

    public void setQueFlVstMed(char queFlVstMed) {
        this.queFlVstMed = queFlVstMed;
    }

    public char getQueFlVstMed() {
        return this.queFlVstMed;
    }

    public void setQueFlStatBuonSal(char queFlStatBuonSal) {
        this.queFlStatBuonSal = queFlStatBuonSal;
    }

    public char getQueFlStatBuonSal() {
        return this.queFlStatBuonSal;
    }

    public void setQueDsRiga(long queDsRiga) {
        this.queDsRiga = queDsRiga;
    }

    public long getQueDsRiga() {
        return this.queDsRiga;
    }

    public void setQueDsOperSql(char queDsOperSql) {
        this.queDsOperSql = queDsOperSql;
    }

    public void setQueDsOperSqlFormatted(String queDsOperSql) {
        setQueDsOperSql(Functions.charAt(queDsOperSql, Types.CHAR_SIZE));
    }

    public char getQueDsOperSql() {
        return this.queDsOperSql;
    }

    public void setQueDsVer(int queDsVer) {
        this.queDsVer = queDsVer;
    }

    public int getQueDsVer() {
        return this.queDsVer;
    }

    public void setQueDsTsIniCptz(long queDsTsIniCptz) {
        this.queDsTsIniCptz = queDsTsIniCptz;
    }

    public long getQueDsTsIniCptz() {
        return this.queDsTsIniCptz;
    }

    public void setQueDsTsEndCptz(long queDsTsEndCptz) {
        this.queDsTsEndCptz = queDsTsEndCptz;
    }

    public long getQueDsTsEndCptz() {
        return this.queDsTsEndCptz;
    }

    public void setQueDsUtente(String queDsUtente) {
        this.queDsUtente = Functions.subString(queDsUtente, Len.QUE_DS_UTENTE);
    }

    public String getQueDsUtente() {
        return this.queDsUtente;
    }

    public void setQueDsStatoElab(char queDsStatoElab) {
        this.queDsStatoElab = queDsStatoElab;
    }

    public void setQueDsStatoElabFormatted(String queDsStatoElab) {
        setQueDsStatoElab(Functions.charAt(queDsStatoElab, Types.CHAR_SIZE));
    }

    public char getQueDsStatoElab() {
        return this.queDsStatoElab;
    }

    public void setQueTpAdegz(String queTpAdegz) {
        this.queTpAdegz = Functions.subString(queTpAdegz, Len.QUE_TP_ADEGZ);
    }

    public String getQueTpAdegz() {
        return this.queTpAdegz;
    }

    public String getQueTpAdegzFormatted() {
        return Functions.padBlanks(getQueTpAdegz(), Len.QUE_TP_ADEGZ);
    }

    public QueIdMoviChiu getQueIdMoviChiu() {
        return queIdMoviChiu;
    }

    @Override
    public byte[] serialize() {
        return getQuestBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int QUE_ID_QUEST = 5;
        public static final int QUE_ID_RAPP_ANA = 5;
        public static final int QUE_ID_MOVI_CRZ = 5;
        public static final int QUE_DT_INI_EFF = 5;
        public static final int QUE_DT_END_EFF = 5;
        public static final int QUE_COD_COMP_ANIA = 3;
        public static final int QUE_COD_QUEST = 20;
        public static final int QUE_TP_QUEST = 20;
        public static final int QUE_FL_VST_MED = 1;
        public static final int QUE_FL_STAT_BUON_SAL = 1;
        public static final int QUE_DS_RIGA = 6;
        public static final int QUE_DS_OPER_SQL = 1;
        public static final int QUE_DS_VER = 5;
        public static final int QUE_DS_TS_INI_CPTZ = 10;
        public static final int QUE_DS_TS_END_CPTZ = 10;
        public static final int QUE_DS_UTENTE = 20;
        public static final int QUE_DS_STATO_ELAB = 1;
        public static final int QUE_TP_ADEGZ = 2;
        public static final int QUEST = QUE_ID_QUEST + QUE_ID_RAPP_ANA + QUE_ID_MOVI_CRZ + QueIdMoviChiu.Len.QUE_ID_MOVI_CHIU + QUE_DT_INI_EFF + QUE_DT_END_EFF + QUE_COD_COMP_ANIA + QUE_COD_QUEST + QUE_TP_QUEST + QUE_FL_VST_MED + QUE_FL_STAT_BUON_SAL + QUE_DS_RIGA + QUE_DS_OPER_SQL + QUE_DS_VER + QUE_DS_TS_INI_CPTZ + QUE_DS_TS_END_CPTZ + QUE_DS_UTENTE + QUE_DS_STATO_ELAB + QUE_TP_ADEGZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int QUE_ID_QUEST = 9;
            public static final int QUE_ID_RAPP_ANA = 9;
            public static final int QUE_ID_MOVI_CRZ = 9;
            public static final int QUE_DT_INI_EFF = 8;
            public static final int QUE_DT_END_EFF = 8;
            public static final int QUE_COD_COMP_ANIA = 5;
            public static final int QUE_DS_RIGA = 10;
            public static final int QUE_DS_VER = 9;
            public static final int QUE_DS_TS_INI_CPTZ = 18;
            public static final int QUE_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
