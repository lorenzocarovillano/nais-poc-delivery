package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: ISPC0040-TAB-FONDI<br>
 * Variable: ISPC0040-TAB-FONDI from program ISPS0040<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ispc0040TabFondi extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int FONDI_MAXOCCURS = 200;

    //==== CONSTRUCTORS ====
    public Ispc0040TabFondi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISPC0040_TAB_FONDI;
    }

    public String getIspc0040TabFondiFormatted() {
        return readFixedString(Pos.ISPC0040_TAB_FONDI, Len.ISPC0040_TAB_FONDI);
    }

    public void setIspc0040TabFondiBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISPC0040_TAB_FONDI, Pos.ISPC0040_TAB_FONDI);
    }

    public byte[] getIspc0040TabFondiBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISPC0040_TAB_FONDI, Pos.ISPC0040_TAB_FONDI);
        return buffer;
    }

    public void setIspc0040CodFondi(int ispc0040CodFondiIdx, String ispc0040CodFondi) {
        int position = Pos.ispc0040CodFondi(ispc0040CodFondiIdx - 1);
        writeString(position, ispc0040CodFondi, Len.COD_FONDI);
    }

    /**Original name: ISPC0040-COD-FONDI<br>*/
    public String getIspc0040CodFondi(int ispc0040CodFondiIdx) {
        int position = Pos.ispc0040CodFondi(ispc0040CodFondiIdx - 1);
        return readString(position, Len.COD_FONDI);
    }

    public void setIspc0040DescFond(int ispc0040DescFondIdx, String ispc0040DescFond) {
        int position = Pos.ispc0040DescFond(ispc0040DescFondIdx - 1);
        writeString(position, ispc0040DescFond, Len.DESC_FOND);
    }

    /**Original name: ISPC0040-DESC-FOND<br>*/
    public String getIspc0040DescFond(int ispc0040DescFondIdx) {
        int position = Pos.ispc0040DescFond(ispc0040DescFondIdx - 1);
        return readString(position, Len.DESC_FOND);
    }

    public void setIspc0040RestoTabFondi(String ispc0040RestoTabFondi) {
        writeString(Pos.RESTO_TAB_FONDI, ispc0040RestoTabFondi, Len.ISPC0040_RESTO_TAB_FONDI);
    }

    /**Original name: ISPC0040-RESTO-TAB-FONDI<br>*/
    public String getIspc0040RestoTabFondi() {
        return readString(Pos.RESTO_TAB_FONDI, Len.ISPC0040_RESTO_TAB_FONDI);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISPC0040_TAB_FONDI = 1;
        public static final int ISPC0040_TAB_FONDI_R = 1;
        public static final int FLR1 = ISPC0040_TAB_FONDI_R;
        public static final int RESTO_TAB_FONDI = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int ispc0040Fondi(int idx) {
            return ISPC0040_TAB_FONDI + idx * Len.FONDI;
        }

        public static int ispc0040CodFondi(int idx) {
            return ispc0040Fondi(idx);
        }

        public static int ispc0040DescFond(int idx) {
            return ispc0040CodFondi(idx) + Len.COD_FONDI;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_FONDI = 12;
        public static final int DESC_FOND = 80;
        public static final int FONDI = COD_FONDI + DESC_FOND;
        public static final int FLR1 = 92;
        public static final int ISPC0040_TAB_FONDI = Ispc0040TabFondi.FONDI_MAXOCCURS * FONDI;
        public static final int ISPC0040_RESTO_TAB_FONDI = 18308;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
