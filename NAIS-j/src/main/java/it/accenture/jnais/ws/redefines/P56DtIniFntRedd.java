package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-DT-INI-FNT-REDD<br>
 * Variable: P56-DT-INI-FNT-REDD from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56DtIniFntRedd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56DtIniFntRedd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_DT_INI_FNT_REDD;
    }

    public void setP56DtIniFntRedd(int p56DtIniFntRedd) {
        writeIntAsPacked(Pos.P56_DT_INI_FNT_REDD, p56DtIniFntRedd, Len.Int.P56_DT_INI_FNT_REDD);
    }

    public void setP56DtIniFntReddFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_DT_INI_FNT_REDD, Pos.P56_DT_INI_FNT_REDD);
    }

    /**Original name: P56-DT-INI-FNT-REDD<br>*/
    public int getP56DtIniFntRedd() {
        return readPackedAsInt(Pos.P56_DT_INI_FNT_REDD, Len.Int.P56_DT_INI_FNT_REDD);
    }

    public byte[] getP56DtIniFntReddAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_DT_INI_FNT_REDD, Pos.P56_DT_INI_FNT_REDD);
        return buffer;
    }

    public void setP56DtIniFntReddNull(String p56DtIniFntReddNull) {
        writeString(Pos.P56_DT_INI_FNT_REDD_NULL, p56DtIniFntReddNull, Len.P56_DT_INI_FNT_REDD_NULL);
    }

    /**Original name: P56-DT-INI-FNT-REDD-NULL<br>*/
    public String getP56DtIniFntReddNull() {
        return readString(Pos.P56_DT_INI_FNT_REDD_NULL, Len.P56_DT_INI_FNT_REDD_NULL);
    }

    public String getP56DtIniFntReddNullFormatted() {
        return Functions.padBlanks(getP56DtIniFntReddNull(), Len.P56_DT_INI_FNT_REDD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_DT_INI_FNT_REDD = 1;
        public static final int P56_DT_INI_FNT_REDD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_DT_INI_FNT_REDD = 5;
        public static final int P56_DT_INI_FNT_REDD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_DT_INI_FNT_REDD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
