package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMP-REN-K2<br>
 * Variable: S089-IMP-REN-K2 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpRenK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpRenK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMP_REN_K2;
    }

    public void setWlquImpRenK2(AfDecimal wlquImpRenK2) {
        writeDecimalAsPacked(Pos.S089_IMP_REN_K2, wlquImpRenK2.copy());
    }

    public void setWlquImpRenK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMP_REN_K2, Pos.S089_IMP_REN_K2);
    }

    /**Original name: WLQU-IMP-REN-K2<br>*/
    public AfDecimal getWlquImpRenK2() {
        return readPackedAsDecimal(Pos.S089_IMP_REN_K2, Len.Int.WLQU_IMP_REN_K2, Len.Fract.WLQU_IMP_REN_K2);
    }

    public byte[] getWlquImpRenK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMP_REN_K2, Pos.S089_IMP_REN_K2);
        return buffer;
    }

    public void initWlquImpRenK2Spaces() {
        fill(Pos.S089_IMP_REN_K2, Len.S089_IMP_REN_K2, Types.SPACE_CHAR);
    }

    public void setWlquImpRenK2Null(String wlquImpRenK2Null) {
        writeString(Pos.S089_IMP_REN_K2_NULL, wlquImpRenK2Null, Len.WLQU_IMP_REN_K2_NULL);
    }

    /**Original name: WLQU-IMP-REN-K2-NULL<br>*/
    public String getWlquImpRenK2Null() {
        return readString(Pos.S089_IMP_REN_K2_NULL, Len.WLQU_IMP_REN_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMP_REN_K2 = 1;
        public static final int S089_IMP_REN_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMP_REN_K2 = 8;
        public static final int WLQU_IMP_REN_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_REN_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_REN_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
