package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.commons.data.to.ILogErrore;
import it.accenture.jnais.copy.IndLogErrore;
import it.accenture.jnais.copy.LogErrore;
import it.accenture.jnais.ws.enums.WsEsito;
import it.accenture.jnais.ws.redefines.WsTimestampDclFrm;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IEAS9700<br>
 * Generated as a class for rule WS.<br>*/
public class Ieas9700Data implements ILogErrore {

    //==== PROPERTIES ====
    //Original name: WS-VARIABILI
    private WsVariabili wsVariabili = new WsVariabili();
    /**Original name: WS-ESITO<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *                    FLAG PER ESITO ELABORAZIONE                 *
	 *                                                                *
	 * ****************************************************************</pre>*/
    private WsEsito wsEsito = new WsEsito();
    /**Original name: WS-TIMESTAMP<br>
	 * <pre>---> dichiaro variabile di appoggio per set timestamp</pre>*/
    private String wsTimestamp = DefaultValues.stringVal(Len.WS_TIMESTAMP);
    /**Original name: WN-TIMESTAMP-DCL<br>
	 * <pre>---> dichiaro variabile di appoggio per valorizzazione campo di
	 * ---> tabella log_errore</pre>*/
    private long wnTimestampDcl = DefaultValues.LONG_VAL;
    /**Original name: WS-TIMESTAMP-DCL-FRM<br>
	 * <pre>---appoggio per formattazione timestamp------------------------*</pre>*/
    private WsTimestampDclFrm wsTimestampDclFrm = new WsTimestampDclFrm();
    //Original name: LOG-ERRORE
    private LogErrore logErrore = new LogErrore();
    //Original name: IND-LOG-ERRORE
    private IndLogErrore indLogErrore = new IndLogErrore();

    //==== METHODS ====
    public void setWsTimestamp(String wsTimestamp) {
        this.wsTimestamp = Functions.subString(wsTimestamp, Len.WS_TIMESTAMP);
    }

    public String getWsTimestamp() {
        return this.wsTimestamp;
    }

    public String getWsTimestampFormatted() {
        return Functions.padBlanks(getWsTimestamp(), Len.WS_TIMESTAMP);
    }

    public void setWnTimestampDcl(long wnTimestampDcl) {
        this.wnTimestampDcl = wnTimestampDcl;
    }

    public long getWnTimestampDcl() {
        return this.wnTimestampDcl;
    }

    @Override
    public String getCodMainBatch() {
        return logErrore.getLorCodMainBatch();
    }

    @Override
    public void setCodMainBatch(String codMainBatch) {
        this.logErrore.setLorCodMainBatch(codMainBatch);
    }

    @Override
    public String getCodServizioBe() {
        return logErrore.getLorCodServizioBe();
    }

    @Override
    public void setCodServizioBe(String codServizioBe) {
        this.logErrore.setLorCodServizioBe(codServizioBe);
    }

    @Override
    public String getDescErroreEstesaVchar() {
        return logErrore.getLorDescErroreEstesaVcharFormatted();
    }

    @Override
    public void setDescErroreEstesaVchar(String descErroreEstesaVchar) {
        this.logErrore.setLorDescErroreEstesaVcharFormatted(descErroreEstesaVchar);
    }

    @Override
    public String getIbOggetto() {
        return logErrore.getLorIbOggetto();
    }

    @Override
    public void setIbOggetto(String ibOggetto) {
        this.logErrore.setLorIbOggetto(ibOggetto);
    }

    @Override
    public String getIbOggettoObj() {
        return getIbOggetto();
    }

    @Override
    public void setIbOggettoObj(String ibOggettoObj) {
        setIbOggetto(ibOggettoObj);
    }

    @Override
    public int getIdGravitaErrore() {
        return logErrore.getLorIdGravitaErrore();
    }

    @Override
    public void setIdGravitaErrore(int idGravitaErrore) {
        this.logErrore.setLorIdGravitaErrore(idGravitaErrore);
    }

    @Override
    public int getIdLogErrore() {
        return logErrore.getLorIdLogErrore();
    }

    @Override
    public void setIdLogErrore(int idLogErrore) {
        this.logErrore.setLorIdLogErrore(idLogErrore);
    }

    public IndLogErrore getIndLogErrore() {
        return indLogErrore;
    }

    @Override
    public String getKeyTabellaVchar() {
        return logErrore.getLorKeyTabellaVcharFormatted();
    }

    @Override
    public void setKeyTabellaVchar(String keyTabellaVchar) {
        this.logErrore.setLorKeyTabellaVcharFormatted(keyTabellaVchar);
    }

    @Override
    public String getKeyTabellaVcharObj() {
        return getKeyTabellaVchar();
    }

    @Override
    public void setKeyTabellaVcharObj(String keyTabellaVcharObj) {
        setKeyTabellaVchar(keyTabellaVcharObj);
    }

    @Override
    public String getLabelErr() {
        return logErrore.getLorLabelErr();
    }

    @Override
    public void setLabelErr(String labelErr) {
        this.logErrore.setLorLabelErr(labelErr);
    }

    @Override
    public String getLabelErrObj() {
        return getLabelErr();
    }

    @Override
    public void setLabelErrObj(String labelErrObj) {
        setLabelErr(labelErrObj);
    }

    public LogErrore getLogErrore() {
        return logErrore;
    }

    @Override
    public String getNomeTabella() {
        return logErrore.getLorNomeTabella();
    }

    @Override
    public void setNomeTabella(String nomeTabella) {
        this.logErrore.setLorNomeTabella(nomeTabella);
    }

    @Override
    public String getNomeTabellaObj() {
        return getNomeTabella();
    }

    @Override
    public void setNomeTabellaObj(String nomeTabellaObj) {
        setNomeTabella(nomeTabellaObj);
    }

    @Override
    public String getOperTabella() {
        return logErrore.getLorOperTabella();
    }

    @Override
    public void setOperTabella(String operTabella) {
        this.logErrore.setLorOperTabella(operTabella);
    }

    @Override
    public String getOperTabellaObj() {
        return getOperTabella();
    }

    @Override
    public void setOperTabellaObj(String operTabellaObj) {
        setOperTabella(operTabellaObj);
    }

    @Override
    public int getProgLogErrore() {
        return logErrore.getLorProgLogErrore();
    }

    @Override
    public void setProgLogErrore(int progLogErrore) {
        this.logErrore.setLorProgLogErrore(progLogErrore);
    }

    @Override
    public String getStatusTabella() {
        return logErrore.getLorStatusTabella();
    }

    @Override
    public void setStatusTabella(String statusTabella) {
        this.logErrore.setLorStatusTabella(statusTabella);
    }

    @Override
    public String getStatusTabellaObj() {
        return getStatusTabella();
    }

    @Override
    public void setStatusTabellaObj(String statusTabellaObj) {
        setStatusTabella(statusTabellaObj);
    }

    @Override
    public long getTimestampReg() {
        return logErrore.getLorTimestampReg();
    }

    @Override
    public void setTimestampReg(long timestampReg) {
        this.logErrore.setLorTimestampReg(timestampReg);
    }

    @Override
    public short getTipoOggetto() {
        return logErrore.getLorTipoOggetto().getLorTipoOggetto();
    }

    @Override
    public void setTipoOggetto(short tipoOggetto) {
        this.logErrore.getLorTipoOggetto().setLorTipoOggetto(tipoOggetto);
    }

    @Override
    public Short getTipoOggettoObj() {
        return ((Short)getTipoOggetto());
    }

    @Override
    public void setTipoOggettoObj(Short tipoOggettoObj) {
        setTipoOggetto(((short)tipoOggettoObj));
    }

    public WsEsito getWsEsito() {
        return wsEsito;
    }

    public WsTimestampDclFrm getWsTimestampDclFrm() {
        return wsTimestampDclFrm;
    }

    public WsVariabili getWsVariabili() {
        return wsVariabili;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TIMESTAMP = 26;
        public static final int CONTA_VALORE = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
