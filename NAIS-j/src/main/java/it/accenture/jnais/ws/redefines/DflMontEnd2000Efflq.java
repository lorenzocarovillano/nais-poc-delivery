package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-MONT-END2000-EFFLQ<br>
 * Variable: DFL-MONT-END2000-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflMontEnd2000Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflMontEnd2000Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_MONT_END2000_EFFLQ;
    }

    public void setDflMontEnd2000Efflq(AfDecimal dflMontEnd2000Efflq) {
        writeDecimalAsPacked(Pos.DFL_MONT_END2000_EFFLQ, dflMontEnd2000Efflq.copy());
    }

    public void setDflMontEnd2000EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_MONT_END2000_EFFLQ, Pos.DFL_MONT_END2000_EFFLQ);
    }

    /**Original name: DFL-MONT-END2000-EFFLQ<br>*/
    public AfDecimal getDflMontEnd2000Efflq() {
        return readPackedAsDecimal(Pos.DFL_MONT_END2000_EFFLQ, Len.Int.DFL_MONT_END2000_EFFLQ, Len.Fract.DFL_MONT_END2000_EFFLQ);
    }

    public byte[] getDflMontEnd2000EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_MONT_END2000_EFFLQ, Pos.DFL_MONT_END2000_EFFLQ);
        return buffer;
    }

    public void setDflMontEnd2000EfflqNull(String dflMontEnd2000EfflqNull) {
        writeString(Pos.DFL_MONT_END2000_EFFLQ_NULL, dflMontEnd2000EfflqNull, Len.DFL_MONT_END2000_EFFLQ_NULL);
    }

    /**Original name: DFL-MONT-END2000-EFFLQ-NULL<br>*/
    public String getDflMontEnd2000EfflqNull() {
        return readString(Pos.DFL_MONT_END2000_EFFLQ_NULL, Len.DFL_MONT_END2000_EFFLQ_NULL);
    }

    public String getDflMontEnd2000EfflqNullFormatted() {
        return Functions.padBlanks(getDflMontEnd2000EfflqNull(), Len.DFL_MONT_END2000_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_MONT_END2000_EFFLQ = 1;
        public static final int DFL_MONT_END2000_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_MONT_END2000_EFFLQ = 8;
        public static final int DFL_MONT_END2000_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_MONT_END2000_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_MONT_END2000_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
