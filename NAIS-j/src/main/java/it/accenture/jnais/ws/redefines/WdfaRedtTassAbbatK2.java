package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-REDT-TASS-ABBAT-K2<br>
 * Variable: WDFA-REDT-TASS-ABBAT-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaRedtTassAbbatK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaRedtTassAbbatK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_REDT_TASS_ABBAT_K2;
    }

    public void setWdfaRedtTassAbbatK2(AfDecimal wdfaRedtTassAbbatK2) {
        writeDecimalAsPacked(Pos.WDFA_REDT_TASS_ABBAT_K2, wdfaRedtTassAbbatK2.copy());
    }

    public void setWdfaRedtTassAbbatK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_REDT_TASS_ABBAT_K2, Pos.WDFA_REDT_TASS_ABBAT_K2);
    }

    /**Original name: WDFA-REDT-TASS-ABBAT-K2<br>*/
    public AfDecimal getWdfaRedtTassAbbatK2() {
        return readPackedAsDecimal(Pos.WDFA_REDT_TASS_ABBAT_K2, Len.Int.WDFA_REDT_TASS_ABBAT_K2, Len.Fract.WDFA_REDT_TASS_ABBAT_K2);
    }

    public byte[] getWdfaRedtTassAbbatK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_REDT_TASS_ABBAT_K2, Pos.WDFA_REDT_TASS_ABBAT_K2);
        return buffer;
    }

    public void setWdfaRedtTassAbbatK2Null(String wdfaRedtTassAbbatK2Null) {
        writeString(Pos.WDFA_REDT_TASS_ABBAT_K2_NULL, wdfaRedtTassAbbatK2Null, Len.WDFA_REDT_TASS_ABBAT_K2_NULL);
    }

    /**Original name: WDFA-REDT-TASS-ABBAT-K2-NULL<br>*/
    public String getWdfaRedtTassAbbatK2Null() {
        return readString(Pos.WDFA_REDT_TASS_ABBAT_K2_NULL, Len.WDFA_REDT_TASS_ABBAT_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_REDT_TASS_ABBAT_K2 = 1;
        public static final int WDFA_REDT_TASS_ABBAT_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_REDT_TASS_ABBAT_K2 = 8;
        public static final int WDFA_REDT_TASS_ABBAT_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_REDT_TASS_ABBAT_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_REDT_TASS_ABBAT_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
