package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMP-INTR-PREST<br>
 * Variable: S089-TOT-IMP-INTR-PREST from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMP_INTR_PREST;
    }

    public void setWlquTotImpIntrPrest(AfDecimal wlquTotImpIntrPrest) {
        writeDecimalAsPacked(Pos.S089_TOT_IMP_INTR_PREST, wlquTotImpIntrPrest.copy());
    }

    public void setWlquTotImpIntrPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMP_INTR_PREST, Pos.S089_TOT_IMP_INTR_PREST);
    }

    /**Original name: WLQU-TOT-IMP-INTR-PREST<br>*/
    public AfDecimal getWlquTotImpIntrPrest() {
        return readPackedAsDecimal(Pos.S089_TOT_IMP_INTR_PREST, Len.Int.WLQU_TOT_IMP_INTR_PREST, Len.Fract.WLQU_TOT_IMP_INTR_PREST);
    }

    public byte[] getWlquTotImpIntrPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMP_INTR_PREST, Pos.S089_TOT_IMP_INTR_PREST);
        return buffer;
    }

    public void initWlquTotImpIntrPrestSpaces() {
        fill(Pos.S089_TOT_IMP_INTR_PREST, Len.S089_TOT_IMP_INTR_PREST, Types.SPACE_CHAR);
    }

    public void setWlquTotImpIntrPrestNull(String wlquTotImpIntrPrestNull) {
        writeString(Pos.S089_TOT_IMP_INTR_PREST_NULL, wlquTotImpIntrPrestNull, Len.WLQU_TOT_IMP_INTR_PREST_NULL);
    }

    /**Original name: WLQU-TOT-IMP-INTR-PREST-NULL<br>*/
    public String getWlquTotImpIntrPrestNull() {
        return readString(Pos.S089_TOT_IMP_INTR_PREST_NULL, Len.WLQU_TOT_IMP_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_INTR_PREST = 1;
        public static final int S089_TOT_IMP_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_INTR_PREST = 8;
        public static final int WLQU_TOT_IMP_INTR_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_INTR_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
