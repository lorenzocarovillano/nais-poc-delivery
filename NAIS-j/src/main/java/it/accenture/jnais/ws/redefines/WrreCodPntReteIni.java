package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRRE-COD-PNT-RETE-INI<br>
 * Variable: WRRE-COD-PNT-RETE-INI from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrreCodPntReteIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrreCodPntReteIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRRE_COD_PNT_RETE_INI;
    }

    public void setWrreCodPntReteIni(long wrreCodPntReteIni) {
        writeLongAsPacked(Pos.WRRE_COD_PNT_RETE_INI, wrreCodPntReteIni, Len.Int.WRRE_COD_PNT_RETE_INI);
    }

    public void setWrreCodPntReteIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRRE_COD_PNT_RETE_INI, Pos.WRRE_COD_PNT_RETE_INI);
    }

    /**Original name: WRRE-COD-PNT-RETE-INI<br>*/
    public long getWrreCodPntReteIni() {
        return readPackedAsLong(Pos.WRRE_COD_PNT_RETE_INI, Len.Int.WRRE_COD_PNT_RETE_INI);
    }

    public byte[] getWrreCodPntReteIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRRE_COD_PNT_RETE_INI, Pos.WRRE_COD_PNT_RETE_INI);
        return buffer;
    }

    public void initWrreCodPntReteIniSpaces() {
        fill(Pos.WRRE_COD_PNT_RETE_INI, Len.WRRE_COD_PNT_RETE_INI, Types.SPACE_CHAR);
    }

    public void setWrreCodPntReteIniNull(String wrreCodPntReteIniNull) {
        writeString(Pos.WRRE_COD_PNT_RETE_INI_NULL, wrreCodPntReteIniNull, Len.WRRE_COD_PNT_RETE_INI_NULL);
    }

    /**Original name: WRRE-COD-PNT-RETE-INI-NULL<br>*/
    public String getWrreCodPntReteIniNull() {
        return readString(Pos.WRRE_COD_PNT_RETE_INI_NULL, Len.WRRE_COD_PNT_RETE_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRRE_COD_PNT_RETE_INI = 1;
        public static final int WRRE_COD_PNT_RETE_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRRE_COD_PNT_RETE_INI = 6;
        public static final int WRRE_COD_PNT_RETE_INI_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRRE_COD_PNT_RETE_INI = 10;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
