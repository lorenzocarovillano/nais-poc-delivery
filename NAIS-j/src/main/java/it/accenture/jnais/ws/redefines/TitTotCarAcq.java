package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-CAR-ACQ<br>
 * Variable: TIT-TOT-CAR-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_CAR_ACQ;
    }

    public void setTitTotCarAcq(AfDecimal titTotCarAcq) {
        writeDecimalAsPacked(Pos.TIT_TOT_CAR_ACQ, titTotCarAcq.copy());
    }

    public void setTitTotCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_CAR_ACQ, Pos.TIT_TOT_CAR_ACQ);
    }

    /**Original name: TIT-TOT-CAR-ACQ<br>*/
    public AfDecimal getTitTotCarAcq() {
        return readPackedAsDecimal(Pos.TIT_TOT_CAR_ACQ, Len.Int.TIT_TOT_CAR_ACQ, Len.Fract.TIT_TOT_CAR_ACQ);
    }

    public byte[] getTitTotCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_CAR_ACQ, Pos.TIT_TOT_CAR_ACQ);
        return buffer;
    }

    public void setTitTotCarAcqNull(String titTotCarAcqNull) {
        writeString(Pos.TIT_TOT_CAR_ACQ_NULL, titTotCarAcqNull, Len.TIT_TOT_CAR_ACQ_NULL);
    }

    /**Original name: TIT-TOT-CAR-ACQ-NULL<br>*/
    public String getTitTotCarAcqNull() {
        return readString(Pos.TIT_TOT_CAR_ACQ_NULL, Len.TIT_TOT_CAR_ACQ_NULL);
    }

    public String getTitTotCarAcqNullFormatted() {
        return Functions.padBlanks(getTitTotCarAcqNull(), Len.TIT_TOT_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_CAR_ACQ = 1;
        public static final int TIT_TOT_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_CAR_ACQ = 8;
        public static final int TIT_TOT_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
