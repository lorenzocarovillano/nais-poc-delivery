package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: FL-RICERCA<br>
 * Variable: FL-RICERCA from program LVES0245<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlRicercaLves0245 {

    //==== PROPERTIES ====
    private String value = "";
    public static final String TROVATO = "SI";
    public static final String NON_TROVATO = "NO";

    //==== METHODS ====
    public void setFlRicerca(String flRicerca) {
        this.value = Functions.subString(flRicerca, Len.FL_RICERCA);
    }

    public String getFlRicerca() {
        return this.value;
    }

    public boolean isTrovato() {
        return value.equals(TROVATO);
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public void setNonTrovato() {
        value = NON_TROVATO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_RICERCA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
