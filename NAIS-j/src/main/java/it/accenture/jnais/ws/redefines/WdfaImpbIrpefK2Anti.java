package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPB-IRPEF-K2-ANTI<br>
 * Variable: WDFA-IMPB-IRPEF-K2-ANTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpbIrpefK2Anti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpbIrpefK2Anti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPB_IRPEF_K2_ANTI;
    }

    public void setWdfaImpbIrpefK2Anti(AfDecimal wdfaImpbIrpefK2Anti) {
        writeDecimalAsPacked(Pos.WDFA_IMPB_IRPEF_K2_ANTI, wdfaImpbIrpefK2Anti.copy());
    }

    public void setWdfaImpbIrpefK2AntiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPB_IRPEF_K2_ANTI, Pos.WDFA_IMPB_IRPEF_K2_ANTI);
    }

    /**Original name: WDFA-IMPB-IRPEF-K2-ANTI<br>*/
    public AfDecimal getWdfaImpbIrpefK2Anti() {
        return readPackedAsDecimal(Pos.WDFA_IMPB_IRPEF_K2_ANTI, Len.Int.WDFA_IMPB_IRPEF_K2_ANTI, Len.Fract.WDFA_IMPB_IRPEF_K2_ANTI);
    }

    public byte[] getWdfaImpbIrpefK2AntiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPB_IRPEF_K2_ANTI, Pos.WDFA_IMPB_IRPEF_K2_ANTI);
        return buffer;
    }

    public void setWdfaImpbIrpefK2AntiNull(String wdfaImpbIrpefK2AntiNull) {
        writeString(Pos.WDFA_IMPB_IRPEF_K2_ANTI_NULL, wdfaImpbIrpefK2AntiNull, Len.WDFA_IMPB_IRPEF_K2_ANTI_NULL);
    }

    /**Original name: WDFA-IMPB-IRPEF-K2-ANTI-NULL<br>*/
    public String getWdfaImpbIrpefK2AntiNull() {
        return readString(Pos.WDFA_IMPB_IRPEF_K2_ANTI_NULL, Len.WDFA_IMPB_IRPEF_K2_ANTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IRPEF_K2_ANTI = 1;
        public static final int WDFA_IMPB_IRPEF_K2_ANTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IRPEF_K2_ANTI = 8;
        public static final int WDFA_IMPB_IRPEF_K2_ANTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IRPEF_K2_ANTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IRPEF_K2_ANTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
