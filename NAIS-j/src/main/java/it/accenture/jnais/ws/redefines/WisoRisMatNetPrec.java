package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WISO-RIS-MAT-NET-PREC<br>
 * Variable: WISO-RIS-MAT-NET-PREC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoRisMatNetPrec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoRisMatNetPrec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_RIS_MAT_NET_PREC;
    }

    public void setWisoRisMatNetPrec(AfDecimal wisoRisMatNetPrec) {
        writeDecimalAsPacked(Pos.WISO_RIS_MAT_NET_PREC, wisoRisMatNetPrec.copy());
    }

    public void setWisoRisMatNetPrecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_RIS_MAT_NET_PREC, Pos.WISO_RIS_MAT_NET_PREC);
    }

    /**Original name: WISO-RIS-MAT-NET-PREC<br>*/
    public AfDecimal getWisoRisMatNetPrec() {
        return readPackedAsDecimal(Pos.WISO_RIS_MAT_NET_PREC, Len.Int.WISO_RIS_MAT_NET_PREC, Len.Fract.WISO_RIS_MAT_NET_PREC);
    }

    public byte[] getWisoRisMatNetPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_RIS_MAT_NET_PREC, Pos.WISO_RIS_MAT_NET_PREC);
        return buffer;
    }

    public void initWisoRisMatNetPrecSpaces() {
        fill(Pos.WISO_RIS_MAT_NET_PREC, Len.WISO_RIS_MAT_NET_PREC, Types.SPACE_CHAR);
    }

    public void setWisoRisMatNetPrecNull(String wisoRisMatNetPrecNull) {
        writeString(Pos.WISO_RIS_MAT_NET_PREC_NULL, wisoRisMatNetPrecNull, Len.WISO_RIS_MAT_NET_PREC_NULL);
    }

    /**Original name: WISO-RIS-MAT-NET-PREC-NULL<br>*/
    public String getWisoRisMatNetPrecNull() {
        return readString(Pos.WISO_RIS_MAT_NET_PREC_NULL, Len.WISO_RIS_MAT_NET_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_RIS_MAT_NET_PREC = 1;
        public static final int WISO_RIS_MAT_NET_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_RIS_MAT_NET_PREC = 8;
        public static final int WISO_RIS_MAT_NET_PREC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WISO_RIS_MAT_NET_PREC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_RIS_MAT_NET_PREC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
