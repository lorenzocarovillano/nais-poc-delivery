package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RIT-IRPEF-EFFLQ<br>
 * Variable: DFL-RIT-IRPEF-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflRitIrpefEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflRitIrpefEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RIT_IRPEF_EFFLQ;
    }

    public void setDflRitIrpefEfflq(AfDecimal dflRitIrpefEfflq) {
        writeDecimalAsPacked(Pos.DFL_RIT_IRPEF_EFFLQ, dflRitIrpefEfflq.copy());
    }

    public void setDflRitIrpefEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RIT_IRPEF_EFFLQ, Pos.DFL_RIT_IRPEF_EFFLQ);
    }

    /**Original name: DFL-RIT-IRPEF-EFFLQ<br>*/
    public AfDecimal getDflRitIrpefEfflq() {
        return readPackedAsDecimal(Pos.DFL_RIT_IRPEF_EFFLQ, Len.Int.DFL_RIT_IRPEF_EFFLQ, Len.Fract.DFL_RIT_IRPEF_EFFLQ);
    }

    public byte[] getDflRitIrpefEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RIT_IRPEF_EFFLQ, Pos.DFL_RIT_IRPEF_EFFLQ);
        return buffer;
    }

    public void setDflRitIrpefEfflqNull(String dflRitIrpefEfflqNull) {
        writeString(Pos.DFL_RIT_IRPEF_EFFLQ_NULL, dflRitIrpefEfflqNull, Len.DFL_RIT_IRPEF_EFFLQ_NULL);
    }

    /**Original name: DFL-RIT-IRPEF-EFFLQ-NULL<br>*/
    public String getDflRitIrpefEfflqNull() {
        return readString(Pos.DFL_RIT_IRPEF_EFFLQ_NULL, Len.DFL_RIT_IRPEF_EFFLQ_NULL);
    }

    public String getDflRitIrpefEfflqNullFormatted() {
        return Functions.padBlanks(getDflRitIrpefEfflqNull(), Len.DFL_RIT_IRPEF_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RIT_IRPEF_EFFLQ = 1;
        public static final int DFL_RIT_IRPEF_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RIT_IRPEF_EFFLQ = 8;
        public static final int DFL_RIT_IRPEF_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RIT_IRPEF_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RIT_IRPEF_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
