package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-GG-DEL-MM-SCAD-RAT<br>
 * Variable: P67-GG-DEL-MM-SCAD-RAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67GgDelMmScadRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67GgDelMmScadRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_GG_DEL_MM_SCAD_RAT;
    }

    public void setP67GgDelMmScadRat(short p67GgDelMmScadRat) {
        writeShortAsPacked(Pos.P67_GG_DEL_MM_SCAD_RAT, p67GgDelMmScadRat, Len.Int.P67_GG_DEL_MM_SCAD_RAT);
    }

    public void setP67GgDelMmScadRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_GG_DEL_MM_SCAD_RAT, Pos.P67_GG_DEL_MM_SCAD_RAT);
    }

    /**Original name: P67-GG-DEL-MM-SCAD-RAT<br>*/
    public short getP67GgDelMmScadRat() {
        return readPackedAsShort(Pos.P67_GG_DEL_MM_SCAD_RAT, Len.Int.P67_GG_DEL_MM_SCAD_RAT);
    }

    public byte[] getP67GgDelMmScadRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_GG_DEL_MM_SCAD_RAT, Pos.P67_GG_DEL_MM_SCAD_RAT);
        return buffer;
    }

    public void setP67GgDelMmScadRatNull(String p67GgDelMmScadRatNull) {
        writeString(Pos.P67_GG_DEL_MM_SCAD_RAT_NULL, p67GgDelMmScadRatNull, Len.P67_GG_DEL_MM_SCAD_RAT_NULL);
    }

    /**Original name: P67-GG-DEL-MM-SCAD-RAT-NULL<br>*/
    public String getP67GgDelMmScadRatNull() {
        return readString(Pos.P67_GG_DEL_MM_SCAD_RAT_NULL, Len.P67_GG_DEL_MM_SCAD_RAT_NULL);
    }

    public String getP67GgDelMmScadRatNullFormatted() {
        return Functions.padBlanks(getP67GgDelMmScadRatNull(), Len.P67_GG_DEL_MM_SCAD_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_GG_DEL_MM_SCAD_RAT = 1;
        public static final int P67_GG_DEL_MM_SCAD_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_GG_DEL_MM_SCAD_RAT = 2;
        public static final int P67_GG_DEL_MM_SCAD_RAT_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_GG_DEL_MM_SCAD_RAT = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
