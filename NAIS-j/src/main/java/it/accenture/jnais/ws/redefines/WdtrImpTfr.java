package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-IMP-TFR<br>
 * Variable: WDTR-IMP-TFR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_IMP_TFR;
    }

    public void setWdtrImpTfr(AfDecimal wdtrImpTfr) {
        writeDecimalAsPacked(Pos.WDTR_IMP_TFR, wdtrImpTfr.copy());
    }

    /**Original name: WDTR-IMP-TFR<br>*/
    public AfDecimal getWdtrImpTfr() {
        return readPackedAsDecimal(Pos.WDTR_IMP_TFR, Len.Int.WDTR_IMP_TFR, Len.Fract.WDTR_IMP_TFR);
    }

    public void setWdtrImpTfrNull(String wdtrImpTfrNull) {
        writeString(Pos.WDTR_IMP_TFR_NULL, wdtrImpTfrNull, Len.WDTR_IMP_TFR_NULL);
    }

    /**Original name: WDTR-IMP-TFR-NULL<br>*/
    public String getWdtrImpTfrNull() {
        return readString(Pos.WDTR_IMP_TFR_NULL, Len.WDTR_IMP_TFR_NULL);
    }

    public String getWdtrImpTfrNullFormatted() {
        return Functions.padBlanks(getWdtrImpTfrNull(), Len.WDTR_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_IMP_TFR = 1;
        public static final int WDTR_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_IMP_TFR = 8;
        public static final int WDTR_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
