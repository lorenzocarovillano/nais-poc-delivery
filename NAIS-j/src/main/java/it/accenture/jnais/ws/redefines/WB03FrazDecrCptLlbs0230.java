package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-FRAZ-DECR-CPT<br>
 * Variable: W-B03-FRAZ-DECR-CPT from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03FrazDecrCptLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03FrazDecrCptLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_FRAZ_DECR_CPT;
    }

    public void setwB03FrazDecrCpt(int wB03FrazDecrCpt) {
        writeIntAsPacked(Pos.W_B03_FRAZ_DECR_CPT, wB03FrazDecrCpt, Len.Int.W_B03_FRAZ_DECR_CPT);
    }

    /**Original name: W-B03-FRAZ-DECR-CPT<br>*/
    public int getwB03FrazDecrCpt() {
        return readPackedAsInt(Pos.W_B03_FRAZ_DECR_CPT, Len.Int.W_B03_FRAZ_DECR_CPT);
    }

    public byte[] getwB03FrazDecrCptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_FRAZ_DECR_CPT, Pos.W_B03_FRAZ_DECR_CPT);
        return buffer;
    }

    public void setwB03FrazDecrCptNull(String wB03FrazDecrCptNull) {
        writeString(Pos.W_B03_FRAZ_DECR_CPT_NULL, wB03FrazDecrCptNull, Len.W_B03_FRAZ_DECR_CPT_NULL);
    }

    /**Original name: W-B03-FRAZ-DECR-CPT-NULL<br>*/
    public String getwB03FrazDecrCptNull() {
        return readString(Pos.W_B03_FRAZ_DECR_CPT_NULL, Len.W_B03_FRAZ_DECR_CPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_FRAZ_DECR_CPT = 1;
        public static final int W_B03_FRAZ_DECR_CPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_FRAZ_DECR_CPT = 3;
        public static final int W_B03_FRAZ_DECR_CPT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_FRAZ_DECR_CPT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
