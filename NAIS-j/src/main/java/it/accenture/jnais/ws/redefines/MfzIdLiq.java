package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MFZ-ID-LIQ<br>
 * Variable: MFZ-ID-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MfzIdLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MfzIdLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MFZ_ID_LIQ;
    }

    public void setMfzIdLiq(int mfzIdLiq) {
        writeIntAsPacked(Pos.MFZ_ID_LIQ, mfzIdLiq, Len.Int.MFZ_ID_LIQ);
    }

    public void setMfzIdLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MFZ_ID_LIQ, Pos.MFZ_ID_LIQ);
    }

    /**Original name: MFZ-ID-LIQ<br>*/
    public int getMfzIdLiq() {
        return readPackedAsInt(Pos.MFZ_ID_LIQ, Len.Int.MFZ_ID_LIQ);
    }

    public byte[] getMfzIdLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MFZ_ID_LIQ, Pos.MFZ_ID_LIQ);
        return buffer;
    }

    public void setMfzIdLiqNull(String mfzIdLiqNull) {
        writeString(Pos.MFZ_ID_LIQ_NULL, mfzIdLiqNull, Len.MFZ_ID_LIQ_NULL);
    }

    /**Original name: MFZ-ID-LIQ-NULL<br>*/
    public String getMfzIdLiqNull() {
        return readString(Pos.MFZ_ID_LIQ_NULL, Len.MFZ_ID_LIQ_NULL);
    }

    public String getMfzIdLiqNullFormatted() {
        return Functions.padBlanks(getMfzIdLiqNull(), Len.MFZ_ID_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MFZ_ID_LIQ = 1;
        public static final int MFZ_ID_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MFZ_ID_LIQ = 5;
        public static final int MFZ_ID_LIQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MFZ_ID_LIQ = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
