package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-IMP-INTR-RIT-PAG<br>
 * Variable: BEL-IMP-INTR-RIT-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelImpIntrRitPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelImpIntrRitPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_IMP_INTR_RIT_PAG;
    }

    public void setBelImpIntrRitPag(AfDecimal belImpIntrRitPag) {
        writeDecimalAsPacked(Pos.BEL_IMP_INTR_RIT_PAG, belImpIntrRitPag.copy());
    }

    public void setBelImpIntrRitPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_IMP_INTR_RIT_PAG, Pos.BEL_IMP_INTR_RIT_PAG);
    }

    /**Original name: BEL-IMP-INTR-RIT-PAG<br>*/
    public AfDecimal getBelImpIntrRitPag() {
        return readPackedAsDecimal(Pos.BEL_IMP_INTR_RIT_PAG, Len.Int.BEL_IMP_INTR_RIT_PAG, Len.Fract.BEL_IMP_INTR_RIT_PAG);
    }

    public byte[] getBelImpIntrRitPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_IMP_INTR_RIT_PAG, Pos.BEL_IMP_INTR_RIT_PAG);
        return buffer;
    }

    public void setBelImpIntrRitPagNull(String belImpIntrRitPagNull) {
        writeString(Pos.BEL_IMP_INTR_RIT_PAG_NULL, belImpIntrRitPagNull, Len.BEL_IMP_INTR_RIT_PAG_NULL);
    }

    /**Original name: BEL-IMP-INTR-RIT-PAG-NULL<br>*/
    public String getBelImpIntrRitPagNull() {
        return readString(Pos.BEL_IMP_INTR_RIT_PAG_NULL, Len.BEL_IMP_INTR_RIT_PAG_NULL);
    }

    public String getBelImpIntrRitPagNullFormatted() {
        return Functions.padBlanks(getBelImpIntrRitPagNull(), Len.BEL_IMP_INTR_RIT_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_IMP_INTR_RIT_PAG = 1;
        public static final int BEL_IMP_INTR_RIT_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_IMP_INTR_RIT_PAG = 8;
        public static final int BEL_IMP_INTR_RIT_PAG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_IMP_INTR_RIT_PAG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_IMP_INTR_RIT_PAG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
