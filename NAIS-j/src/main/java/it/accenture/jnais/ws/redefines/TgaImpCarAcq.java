package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-CAR-ACQ<br>
 * Variable: TGA-IMP-CAR-ACQ from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_CAR_ACQ;
    }

    public void setTgaImpCarAcq(AfDecimal tgaImpCarAcq) {
        writeDecimalAsPacked(Pos.TGA_IMP_CAR_ACQ, tgaImpCarAcq.copy());
    }

    public void setTgaImpCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_CAR_ACQ, Pos.TGA_IMP_CAR_ACQ);
    }

    /**Original name: TGA-IMP-CAR-ACQ<br>*/
    public AfDecimal getTgaImpCarAcq() {
        return readPackedAsDecimal(Pos.TGA_IMP_CAR_ACQ, Len.Int.TGA_IMP_CAR_ACQ, Len.Fract.TGA_IMP_CAR_ACQ);
    }

    public byte[] getTgaImpCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_CAR_ACQ, Pos.TGA_IMP_CAR_ACQ);
        return buffer;
    }

    public void setTgaImpCarAcqNull(String tgaImpCarAcqNull) {
        writeString(Pos.TGA_IMP_CAR_ACQ_NULL, tgaImpCarAcqNull, Len.TGA_IMP_CAR_ACQ_NULL);
    }

    /**Original name: TGA-IMP-CAR-ACQ-NULL<br>*/
    public String getTgaImpCarAcqNull() {
        return readString(Pos.TGA_IMP_CAR_ACQ_NULL, Len.TGA_IMP_CAR_ACQ_NULL);
    }

    public String getTgaImpCarAcqNullFormatted() {
        return Functions.padBlanks(getTgaImpCarAcqNull(), Len.TGA_IMP_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_CAR_ACQ = 1;
        public static final int TGA_IMP_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_CAR_ACQ = 8;
        public static final int TGA_IMP_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
