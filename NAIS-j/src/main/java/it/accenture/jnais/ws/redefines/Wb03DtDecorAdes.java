package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-DECOR-ADES<br>
 * Variable: WB03-DT-DECOR-ADES from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtDecorAdes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtDecorAdes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_DECOR_ADES;
    }

    public void setWb03DtDecorAdes(int wb03DtDecorAdes) {
        writeIntAsPacked(Pos.WB03_DT_DECOR_ADES, wb03DtDecorAdes, Len.Int.WB03_DT_DECOR_ADES);
    }

    public void setWb03DtDecorAdesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_DECOR_ADES, Pos.WB03_DT_DECOR_ADES);
    }

    /**Original name: WB03-DT-DECOR-ADES<br>*/
    public int getWb03DtDecorAdes() {
        return readPackedAsInt(Pos.WB03_DT_DECOR_ADES, Len.Int.WB03_DT_DECOR_ADES);
    }

    public byte[] getWb03DtDecorAdesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_DECOR_ADES, Pos.WB03_DT_DECOR_ADES);
        return buffer;
    }

    public void setWb03DtDecorAdesNull(String wb03DtDecorAdesNull) {
        writeString(Pos.WB03_DT_DECOR_ADES_NULL, wb03DtDecorAdesNull, Len.WB03_DT_DECOR_ADES_NULL);
    }

    /**Original name: WB03-DT-DECOR-ADES-NULL<br>*/
    public String getWb03DtDecorAdesNull() {
        return readString(Pos.WB03_DT_DECOR_ADES_NULL, Len.WB03_DT_DECOR_ADES_NULL);
    }

    public String getWb03DtDecorAdesNullFormatted() {
        return Functions.padBlanks(getWb03DtDecorAdesNull(), Len.WB03_DT_DECOR_ADES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_DECOR_ADES = 1;
        public static final int WB03_DT_DECOR_ADES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_DECOR_ADES = 5;
        public static final int WB03_DT_DECOR_ADES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_DECOR_ADES = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
