package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WPOL-ID-MOVI-CHIU<br>
 * Variable: WPOL-ID-MOVI-CHIU from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_ID_MOVI_CHIU;
    }

    public void setWpolIdMoviChiu(int wpolIdMoviChiu) {
        writeIntAsPacked(Pos.WPOL_ID_MOVI_CHIU, wpolIdMoviChiu, Len.Int.WPOL_ID_MOVI_CHIU);
    }

    public void setWpolIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_ID_MOVI_CHIU, Pos.WPOL_ID_MOVI_CHIU);
    }

    /**Original name: WPOL-ID-MOVI-CHIU<br>*/
    public int getWpolIdMoviChiu() {
        return readPackedAsInt(Pos.WPOL_ID_MOVI_CHIU, Len.Int.WPOL_ID_MOVI_CHIU);
    }

    public byte[] getWpolIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_ID_MOVI_CHIU, Pos.WPOL_ID_MOVI_CHIU);
        return buffer;
    }

    public void setWpolIdMoviChiuNull(String wpolIdMoviChiuNull) {
        writeString(Pos.WPOL_ID_MOVI_CHIU_NULL, wpolIdMoviChiuNull, Len.WPOL_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WPOL-ID-MOVI-CHIU-NULL<br>*/
    public String getWpolIdMoviChiuNull() {
        return readString(Pos.WPOL_ID_MOVI_CHIU_NULL, Len.WPOL_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_ID_MOVI_CHIU = 1;
        public static final int WPOL_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_ID_MOVI_CHIU = 5;
        public static final int WPOL_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
