package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-PC-LIQ<br>
 * Variable: BEL-PC-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelPcLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelPcLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_PC_LIQ;
    }

    public void setBelPcLiq(AfDecimal belPcLiq) {
        writeDecimalAsPacked(Pos.BEL_PC_LIQ, belPcLiq.copy());
    }

    public void setBelPcLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_PC_LIQ, Pos.BEL_PC_LIQ);
    }

    /**Original name: BEL-PC-LIQ<br>*/
    public AfDecimal getBelPcLiq() {
        return readPackedAsDecimal(Pos.BEL_PC_LIQ, Len.Int.BEL_PC_LIQ, Len.Fract.BEL_PC_LIQ);
    }

    public byte[] getBelPcLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_PC_LIQ, Pos.BEL_PC_LIQ);
        return buffer;
    }

    public void setBelPcLiqNull(String belPcLiqNull) {
        writeString(Pos.BEL_PC_LIQ_NULL, belPcLiqNull, Len.BEL_PC_LIQ_NULL);
    }

    /**Original name: BEL-PC-LIQ-NULL<br>*/
    public String getBelPcLiqNull() {
        return readString(Pos.BEL_PC_LIQ_NULL, Len.BEL_PC_LIQ_NULL);
    }

    public String getBelPcLiqNullFormatted() {
        return Functions.padBlanks(getBelPcLiqNull(), Len.BEL_PC_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_PC_LIQ = 1;
        public static final int BEL_PC_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_PC_LIQ = 4;
        public static final int BEL_PC_LIQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_PC_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_PC_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
