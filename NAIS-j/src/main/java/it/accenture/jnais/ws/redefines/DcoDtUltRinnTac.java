package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCO-DT-ULT-RINN-TAC<br>
 * Variable: DCO-DT-ULT-RINN-TAC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DcoDtUltRinnTac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DcoDtUltRinnTac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DCO_DT_ULT_RINN_TAC;
    }

    public void setDcoDtUltRinnTac(int dcoDtUltRinnTac) {
        writeIntAsPacked(Pos.DCO_DT_ULT_RINN_TAC, dcoDtUltRinnTac, Len.Int.DCO_DT_ULT_RINN_TAC);
    }

    public void setDcoDtUltRinnTacFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DCO_DT_ULT_RINN_TAC, Pos.DCO_DT_ULT_RINN_TAC);
    }

    /**Original name: DCO-DT-ULT-RINN-TAC<br>*/
    public int getDcoDtUltRinnTac() {
        return readPackedAsInt(Pos.DCO_DT_ULT_RINN_TAC, Len.Int.DCO_DT_ULT_RINN_TAC);
    }

    public byte[] getDcoDtUltRinnTacAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DCO_DT_ULT_RINN_TAC, Pos.DCO_DT_ULT_RINN_TAC);
        return buffer;
    }

    public void setDcoDtUltRinnTacNull(String dcoDtUltRinnTacNull) {
        writeString(Pos.DCO_DT_ULT_RINN_TAC_NULL, dcoDtUltRinnTacNull, Len.DCO_DT_ULT_RINN_TAC_NULL);
    }

    /**Original name: DCO-DT-ULT-RINN-TAC-NULL<br>*/
    public String getDcoDtUltRinnTacNull() {
        return readString(Pos.DCO_DT_ULT_RINN_TAC_NULL, Len.DCO_DT_ULT_RINN_TAC_NULL);
    }

    public String getDcoDtUltRinnTacNullFormatted() {
        return Functions.padBlanks(getDcoDtUltRinnTacNull(), Len.DCO_DT_ULT_RINN_TAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DCO_DT_ULT_RINN_TAC = 1;
        public static final int DCO_DT_ULT_RINN_TAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_DT_ULT_RINN_TAC = 5;
        public static final int DCO_DT_ULT_RINN_TAC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_DT_ULT_RINN_TAC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
