package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-ALT-SOPR<br>
 * Variable: WTGA-IMP-ALT-SOPR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpAltSopr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpAltSopr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_ALT_SOPR;
    }

    public void setWtgaImpAltSopr(AfDecimal wtgaImpAltSopr) {
        writeDecimalAsPacked(Pos.WTGA_IMP_ALT_SOPR, wtgaImpAltSopr.copy());
    }

    public void setWtgaImpAltSoprFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_ALT_SOPR, Pos.WTGA_IMP_ALT_SOPR);
    }

    /**Original name: WTGA-IMP-ALT-SOPR<br>*/
    public AfDecimal getWtgaImpAltSopr() {
        return readPackedAsDecimal(Pos.WTGA_IMP_ALT_SOPR, Len.Int.WTGA_IMP_ALT_SOPR, Len.Fract.WTGA_IMP_ALT_SOPR);
    }

    public byte[] getWtgaImpAltSoprAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_ALT_SOPR, Pos.WTGA_IMP_ALT_SOPR);
        return buffer;
    }

    public void initWtgaImpAltSoprSpaces() {
        fill(Pos.WTGA_IMP_ALT_SOPR, Len.WTGA_IMP_ALT_SOPR, Types.SPACE_CHAR);
    }

    public void setWtgaImpAltSoprNull(String wtgaImpAltSoprNull) {
        writeString(Pos.WTGA_IMP_ALT_SOPR_NULL, wtgaImpAltSoprNull, Len.WTGA_IMP_ALT_SOPR_NULL);
    }

    /**Original name: WTGA-IMP-ALT-SOPR-NULL<br>*/
    public String getWtgaImpAltSoprNull() {
        return readString(Pos.WTGA_IMP_ALT_SOPR_NULL, Len.WTGA_IMP_ALT_SOPR_NULL);
    }

    public String getWtgaImpAltSoprNullFormatted() {
        return Functions.padBlanks(getWtgaImpAltSoprNull(), Len.WTGA_IMP_ALT_SOPR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_ALT_SOPR = 1;
        public static final int WTGA_IMP_ALT_SOPR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_ALT_SOPR = 8;
        public static final int WTGA_IMP_ALT_SOPR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_ALT_SOPR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_ALT_SOPR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
