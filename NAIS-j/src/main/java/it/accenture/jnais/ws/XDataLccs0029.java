package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: X-DATA<br>
 * Variable: X-DATA from program LCCS0029<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class XDataLccs0029 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: X-GG
    private String gg = DefaultValues.stringVal(Len.GG);
    //Original name: X-MM
    private String mm = DefaultValues.stringVal(Len.MM);
    //Original name: X-AAAA
    private String aaaa = DefaultValues.stringVal(Len.AAAA);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.X_DATA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setxDataBytes(buf);
    }

    public String getxDataFormatted() {
        return MarshalByteExt.bufferToStr(getxDataBytes());
    }

    public void setxDataBytes(byte[] buffer) {
        setxDataBytes(buffer, 1);
    }

    public byte[] getxDataBytes() {
        byte[] buffer = new byte[Len.X_DATA];
        return getxDataBytes(buffer, 1);
    }

    public void setxDataBytes(byte[] buffer, int offset) {
        int position = offset;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
        position += Len.GG;
        mm = MarshalByte.readFixedString(buffer, position, Len.MM);
        position += Len.MM;
        aaaa = MarshalByte.readFixedString(buffer, position, Len.AAAA);
    }

    public byte[] getxDataBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        position += Len.GG;
        MarshalByte.writeString(buffer, position, mm, Len.MM);
        position += Len.MM;
        MarshalByte.writeString(buffer, position, aaaa, Len.AAAA);
        return buffer;
    }

    public void setGgFormatted(String gg) {
        this.gg = Trunc.toUnsignedNumeric(gg, Len.GG);
    }

    public short getGg() {
        return NumericDisplay.asShort(this.gg);
    }

    public void setMmFormatted(String mm) {
        this.mm = Trunc.toUnsignedNumeric(mm, Len.MM);
    }

    public short getMm() {
        return NumericDisplay.asShort(this.mm);
    }

    public void setAaaaFormatted(String aaaa) {
        this.aaaa = Trunc.toUnsignedNumeric(aaaa, Len.AAAA);
    }

    public short getAaaa() {
        return NumericDisplay.asShort(this.aaaa);
    }

    @Override
    public byte[] serialize() {
        return getxDataBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GG = 2;
        public static final int MM = 2;
        public static final int AAAA = 4;
        public static final int X_DATA = GG + MM + AAAA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
