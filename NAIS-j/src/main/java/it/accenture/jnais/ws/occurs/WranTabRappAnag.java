package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvran1;
import it.accenture.jnais.copy.WranDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WRAN-TAB-RAPP-ANAG<br>
 * Variables: WRAN-TAB-RAPP-ANAG from copybook LCCVRANA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WranTabRappAnag {

    //==== PROPERTIES ====
    //Original name: LCCVRAN1
    private Lccvran1 lccvran1 = new Lccvran1();

    //==== METHODS ====
    public void setVranTabRappAnagBytes(byte[] buffer) {
        setWranTabRappAnagBytes(buffer, 1);
    }

    public byte[] getTabRappAnagBytes() {
        byte[] buffer = new byte[Len.WRAN_TAB_RAPP_ANAG];
        return getWranTabRappAnagBytes(buffer, 1);
    }

    public void setWranTabRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvran1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvran1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvran1.Len.Int.ID_PTF, 0));
        position += Lccvran1.Len.ID_PTF;
        lccvran1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWranTabRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvran1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvran1.getIdPtf(), Lccvran1.Len.Int.ID_PTF, 0);
        position += Lccvran1.Len.ID_PTF;
        lccvran1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWranTabRappAnagSpaces() {
        lccvran1.initLccvran1Spaces();
    }

    public Lccvran1 getLccvran1() {
        return lccvran1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRAN_TAB_RAPP_ANAG = WpolStatus.Len.STATUS + Lccvran1.Len.ID_PTF + WranDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
