package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-ELEMENTI-PADRE<br>
 * Variables: WK-ELEMENTI-PADRE from program IDSS0020<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WkElementiPadre {

    //==== PROPERTIES ====
    //Original name: WK-INDICE-PADRE
    private String indicePadre = DefaultValues.stringVal(Len.INDICE_PADRE);
    //Original name: WK-LUNG-PADRE
    private int lungPadre = DefaultValues.INT_VAL;
    //Original name: WK-NUMERO-RICORRENZE
    private int numeroRicorrenze = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setIndicePadre(short indicePadre) {
        this.indicePadre = NumericDisplay.asString(indicePadre, Len.INDICE_PADRE);
    }

    public void setIndicePadreFormatted(String indicePadre) {
        this.indicePadre = Trunc.toUnsignedNumeric(indicePadre, Len.INDICE_PADRE);
    }

    public short getIndicePadre() {
        return NumericDisplay.asShort(this.indicePadre);
    }

    public void setLungPadre(int lungPadre) {
        this.lungPadre = lungPadre;
    }

    public int getLungPadre() {
        return this.lungPadre;
    }

    public void setNumeroRicorrenze(int numeroRicorrenze) {
        this.numeroRicorrenze = numeroRicorrenze;
    }

    public int getNumeroRicorrenze() {
        return this.numeroRicorrenze;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INDICE_PADRE = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
