package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPAG-CONT-DIRITTI<br>
 * Variable: WPAG-CONT-DIRITTI from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContDiritti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContDiritti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_DIRITTI;
    }

    public void setWpagContDiritti(AfDecimal wpagContDiritti) {
        writeDecimalAsPacked(Pos.WPAG_CONT_DIRITTI, wpagContDiritti.copy());
    }

    public void setWpagContDirittiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_DIRITTI, Pos.WPAG_CONT_DIRITTI);
    }

    /**Original name: WPAG-CONT-DIRITTI<br>*/
    public AfDecimal getWpagContDiritti() {
        return readPackedAsDecimal(Pos.WPAG_CONT_DIRITTI, Len.Int.WPAG_CONT_DIRITTI, Len.Fract.WPAG_CONT_DIRITTI);
    }

    public byte[] getWpagContDirittiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_DIRITTI, Pos.WPAG_CONT_DIRITTI);
        return buffer;
    }

    public void initWpagContDirittiSpaces() {
        fill(Pos.WPAG_CONT_DIRITTI, Len.WPAG_CONT_DIRITTI, Types.SPACE_CHAR);
    }

    public void setWpagContDirittiNull(String wpagContDirittiNull) {
        writeString(Pos.WPAG_CONT_DIRITTI_NULL, wpagContDirittiNull, Len.WPAG_CONT_DIRITTI_NULL);
    }

    /**Original name: WPAG-CONT-DIRITTI-NULL<br>*/
    public String getWpagContDirittiNull() {
        return readString(Pos.WPAG_CONT_DIRITTI_NULL, Len.WPAG_CONT_DIRITTI_NULL);
    }

    public String getWpagContDirittiNullFormatted() {
        return Functions.padBlanks(getWpagContDirittiNull(), Len.WPAG_CONT_DIRITTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_DIRITTI = 1;
        public static final int WPAG_CONT_DIRITTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_DIRITTI = 8;
        public static final int WPAG_CONT_DIRITTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_DIRITTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_DIRITTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
