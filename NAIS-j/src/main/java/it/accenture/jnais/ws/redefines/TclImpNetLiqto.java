package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-NET-LIQTO<br>
 * Variable: TCL-IMP-NET-LIQTO from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpNetLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpNetLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_NET_LIQTO;
    }

    public void setTclImpNetLiqto(AfDecimal tclImpNetLiqto) {
        writeDecimalAsPacked(Pos.TCL_IMP_NET_LIQTO, tclImpNetLiqto.copy());
    }

    public void setTclImpNetLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_NET_LIQTO, Pos.TCL_IMP_NET_LIQTO);
    }

    /**Original name: TCL-IMP-NET-LIQTO<br>*/
    public AfDecimal getTclImpNetLiqto() {
        return readPackedAsDecimal(Pos.TCL_IMP_NET_LIQTO, Len.Int.TCL_IMP_NET_LIQTO, Len.Fract.TCL_IMP_NET_LIQTO);
    }

    public byte[] getTclImpNetLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_NET_LIQTO, Pos.TCL_IMP_NET_LIQTO);
        return buffer;
    }

    public void setTclImpNetLiqtoNull(String tclImpNetLiqtoNull) {
        writeString(Pos.TCL_IMP_NET_LIQTO_NULL, tclImpNetLiqtoNull, Len.TCL_IMP_NET_LIQTO_NULL);
    }

    /**Original name: TCL-IMP-NET-LIQTO-NULL<br>*/
    public String getTclImpNetLiqtoNull() {
        return readString(Pos.TCL_IMP_NET_LIQTO_NULL, Len.TCL_IMP_NET_LIQTO_NULL);
    }

    public String getTclImpNetLiqtoNullFormatted() {
        return Functions.padBlanks(getTclImpNetLiqtoNull(), Len.TCL_IMP_NET_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_NET_LIQTO = 1;
        public static final int TCL_IMP_NET_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_NET_LIQTO = 8;
        public static final int TCL_IMP_NET_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_NET_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_NET_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
