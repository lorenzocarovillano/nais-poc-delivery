package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PLI-ID-MOVI-CHIU<br>
 * Variable: PLI-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PliIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PliIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PLI_ID_MOVI_CHIU;
    }

    public void setPliIdMoviChiu(int pliIdMoviChiu) {
        writeIntAsPacked(Pos.PLI_ID_MOVI_CHIU, pliIdMoviChiu, Len.Int.PLI_ID_MOVI_CHIU);
    }

    public void setPliIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PLI_ID_MOVI_CHIU, Pos.PLI_ID_MOVI_CHIU);
    }

    /**Original name: PLI-ID-MOVI-CHIU<br>*/
    public int getPliIdMoviChiu() {
        return readPackedAsInt(Pos.PLI_ID_MOVI_CHIU, Len.Int.PLI_ID_MOVI_CHIU);
    }

    public byte[] getPliIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PLI_ID_MOVI_CHIU, Pos.PLI_ID_MOVI_CHIU);
        return buffer;
    }

    public void setPliIdMoviChiuNull(String pliIdMoviChiuNull) {
        writeString(Pos.PLI_ID_MOVI_CHIU_NULL, pliIdMoviChiuNull, Len.PLI_ID_MOVI_CHIU_NULL);
    }

    /**Original name: PLI-ID-MOVI-CHIU-NULL<br>*/
    public String getPliIdMoviChiuNull() {
        return readString(Pos.PLI_ID_MOVI_CHIU_NULL, Len.PLI_ID_MOVI_CHIU_NULL);
    }

    public String getPliIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getPliIdMoviChiuNull(), Len.PLI_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PLI_ID_MOVI_CHIU = 1;
        public static final int PLI_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PLI_ID_MOVI_CHIU = 5;
        public static final int PLI_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PLI_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
