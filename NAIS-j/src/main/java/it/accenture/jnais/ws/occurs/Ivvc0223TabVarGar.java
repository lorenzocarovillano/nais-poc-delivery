package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0223-TAB-VAR-GAR<br>
 * Variables: IVVC0223-TAB-VAR-GAR from copybook IVVC0223<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0223TabVarGar {

    //==== PROPERTIES ====
    //Original name: IVVC0223-COD-VARIABILE
    private String codVariabile = DefaultValues.stringVal(Len.COD_VARIABILE);
    //Original name: IVVC0223-TP-DATO
    private char tpDato = DefaultValues.CHAR_VAL;
    //Original name: IVVC0223-VAL-IMP
    private AfDecimal valImp = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);
    //Original name: IVVC0223-VAL-PERC
    private AfDecimal valPerc = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: IVVC0223-VAL-STR
    private String valStr = DefaultValues.stringVal(Len.VAL_STR);

    //==== METHODS ====
    public void setTabVarGarBytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaVarGarBytes(buffer, position);
    }

    public byte[] getTabVarGarBytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaVarGarBytes(buffer, position);
        return buffer;
    }

    public void initTabVarGarSpaces() {
        initAreaVarGarSpaces();
    }

    public void setAreaVarGarBytes(byte[] buffer, int offset) {
        int position = offset;
        codVariabile = MarshalByte.readString(buffer, position, Len.COD_VARIABILE);
        position += Len.COD_VARIABILE;
        tpDato = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        valImp.assign(MarshalByte.readDecimal(buffer, position, Len.Int.VAL_IMP, Len.Fract.VAL_IMP));
        position += Len.VAL_IMP;
        valPerc.assign(MarshalByte.readDecimal(buffer, position, Len.Int.VAL_PERC, Len.Fract.VAL_PERC, SignType.NO_SIGN));
        position += Len.VAL_PERC;
        valStr = MarshalByte.readString(buffer, position, Len.VAL_STR);
    }

    public byte[] getAreaVarGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codVariabile, Len.COD_VARIABILE);
        position += Len.COD_VARIABILE;
        MarshalByte.writeChar(buffer, position, tpDato);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimal(buffer, position, valImp.copy());
        position += Len.VAL_IMP;
        MarshalByte.writeDecimal(buffer, position, valPerc.copy(), SignType.NO_SIGN);
        position += Len.VAL_PERC;
        MarshalByte.writeString(buffer, position, valStr, Len.VAL_STR);
        return buffer;
    }

    public void initAreaVarGarSpaces() {
        codVariabile = "";
        tpDato = Types.SPACE_CHAR;
        valImp.setNaN();
        valPerc.setNaN();
        valStr = "";
    }

    public void setCodVariabile(String codVariabile) {
        this.codVariabile = Functions.subString(codVariabile, Len.COD_VARIABILE);
    }

    public String getCodVariabile() {
        return this.codVariabile;
    }

    public void setTpDato(char tpDato) {
        this.tpDato = tpDato;
    }

    public char getTpDato() {
        return this.tpDato;
    }

    public void setValImp(AfDecimal valImp) {
        this.valImp.assign(valImp);
    }

    public AfDecimal getValImp() {
        return this.valImp.copy();
    }

    public void setValPerc(AfDecimal valPerc) {
        this.valPerc.assign(valPerc);
    }

    public AfDecimal getValPerc() {
        return this.valPerc.copy();
    }

    public void setValStr(String valStr) {
        this.valStr = Functions.subString(valStr, Len.VAL_STR);
    }

    public String getValStr() {
        return this.valStr;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_VARIABILE = 12;
        public static final int TP_DATO = 1;
        public static final int VAL_IMP = 18;
        public static final int VAL_PERC = 14;
        public static final int VAL_STR = 60;
        public static final int AREA_VAR_GAR = COD_VARIABILE + TP_DATO + VAL_IMP + VAL_PERC + VAL_STR;
        public static final int TAB_VAR_GAR = AREA_VAR_GAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAL_IMP = 11;
            public static final int VAL_PERC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAL_IMP = 7;
            public static final int VAL_PERC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
