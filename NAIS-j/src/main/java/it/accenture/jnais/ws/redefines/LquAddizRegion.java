package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-ADDIZ-REGION<br>
 * Variable: LQU-ADDIZ-REGION from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquAddizRegion extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquAddizRegion() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_ADDIZ_REGION;
    }

    public void setLquAddizRegion(AfDecimal lquAddizRegion) {
        writeDecimalAsPacked(Pos.LQU_ADDIZ_REGION, lquAddizRegion.copy());
    }

    public void setLquAddizRegionFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_ADDIZ_REGION, Pos.LQU_ADDIZ_REGION);
    }

    /**Original name: LQU-ADDIZ-REGION<br>*/
    public AfDecimal getLquAddizRegion() {
        return readPackedAsDecimal(Pos.LQU_ADDIZ_REGION, Len.Int.LQU_ADDIZ_REGION, Len.Fract.LQU_ADDIZ_REGION);
    }

    public byte[] getLquAddizRegionAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_ADDIZ_REGION, Pos.LQU_ADDIZ_REGION);
        return buffer;
    }

    public void setLquAddizRegionNull(String lquAddizRegionNull) {
        writeString(Pos.LQU_ADDIZ_REGION_NULL, lquAddizRegionNull, Len.LQU_ADDIZ_REGION_NULL);
    }

    /**Original name: LQU-ADDIZ-REGION-NULL<br>*/
    public String getLquAddizRegionNull() {
        return readString(Pos.LQU_ADDIZ_REGION_NULL, Len.LQU_ADDIZ_REGION_NULL);
    }

    public String getLquAddizRegionNullFormatted() {
        return Functions.padBlanks(getLquAddizRegionNull(), Len.LQU_ADDIZ_REGION_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_ADDIZ_REGION = 1;
        public static final int LQU_ADDIZ_REGION_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_ADDIZ_REGION = 8;
        public static final int LQU_ADDIZ_REGION_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_ADDIZ_REGION = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_ADDIZ_REGION = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
