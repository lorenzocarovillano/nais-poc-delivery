package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.redefines.VtgaTab;

/**Original name: VTGA-AREA-TRANCHE<br>
 * Variable: VTGA-AREA-TRANCHE from program LOAS0310<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class VtgaAreaTranche {

    //==== PROPERTIES ====
    //Original name: VTGA-ELE-TRAN-MAX
    private short vtgaEleTranMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: VTGA-TAB
    private VtgaTab vtgaTab = new VtgaTab();

    //==== METHODS ====
    public String getVtgaAreaTrancheFormatted() {
        return MarshalByteExt.bufferToStr(getVtgaAreaTrancheBytes());
    }

    public void setVtgaAreaTrancheBytes(byte[] buffer) {
        setVtgaAreaTrancheBytes(buffer, 1);
    }

    public byte[] getVtgaAreaTrancheBytes() {
        byte[] buffer = new byte[Len.VTGA_AREA_TRANCHE];
        return getVtgaAreaTrancheBytes(buffer, 1);
    }

    public void setVtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        vtgaEleTranMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        vtgaTab.setVtgaTabBytes(buffer, position);
    }

    public byte[] getVtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vtgaEleTranMax);
        position += Types.SHORT_SIZE;
        vtgaTab.getVtgaTabBytes(buffer, position);
        return buffer;
    }

    public void setVtgaEleTranMax(short vtgaEleTranMax) {
        this.vtgaEleTranMax = vtgaEleTranMax;
    }

    public short getVtgaEleTranMax() {
        return this.vtgaEleTranMax;
    }

    public VtgaTab getVtgaTab() {
        return vtgaTab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int VTGA_ELE_TRAN_MAX = 2;
        public static final int VTGA_AREA_TRANCHE = VTGA_ELE_TRAN_MAX + VtgaTab.Len.VTGA_TAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
