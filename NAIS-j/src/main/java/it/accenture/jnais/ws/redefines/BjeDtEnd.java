package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BJE-DT-END<br>
 * Variable: BJE-DT-END from program IABS0080<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BjeDtEnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BjeDtEnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BJE_DT_END;
    }

    public void setBjeDtEnd(long bjeDtEnd) {
        writeLongAsPacked(Pos.BJE_DT_END, bjeDtEnd, Len.Int.BJE_DT_END);
    }

    public void setBjeDtEndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BJE_DT_END, Pos.BJE_DT_END);
    }

    /**Original name: BJE-DT-END<br>*/
    public long getBjeDtEnd() {
        return readPackedAsLong(Pos.BJE_DT_END, Len.Int.BJE_DT_END);
    }

    public byte[] getBjeDtEndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BJE_DT_END, Pos.BJE_DT_END);
        return buffer;
    }

    public void setBjeDtEndNull(String bjeDtEndNull) {
        writeString(Pos.BJE_DT_END_NULL, bjeDtEndNull, Len.BJE_DT_END_NULL);
    }

    /**Original name: BJE-DT-END-NULL<br>*/
    public String getBjeDtEndNull() {
        return readString(Pos.BJE_DT_END_NULL, Len.BJE_DT_END_NULL);
    }

    public String getBjeDtEndNullFormatted() {
        return Functions.padBlanks(getBjeDtEndNull(), Len.BJE_DT_END_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BJE_DT_END = 1;
        public static final int BJE_DT_END_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BJE_DT_END = 10;
        public static final int BJE_DT_END_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BJE_DT_END = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
