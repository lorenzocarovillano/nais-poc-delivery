package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WMFZ-COS-OPRZ<br>
 * Variable: WMFZ-COS-OPRZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmfzCosOprz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmfzCosOprz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMFZ_COS_OPRZ;
    }

    public void setWmfzCosOprz(AfDecimal wmfzCosOprz) {
        writeDecimalAsPacked(Pos.WMFZ_COS_OPRZ, wmfzCosOprz.copy());
    }

    public void setWmfzCosOprzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMFZ_COS_OPRZ, Pos.WMFZ_COS_OPRZ);
    }

    /**Original name: WMFZ-COS-OPRZ<br>*/
    public AfDecimal getWmfzCosOprz() {
        return readPackedAsDecimal(Pos.WMFZ_COS_OPRZ, Len.Int.WMFZ_COS_OPRZ, Len.Fract.WMFZ_COS_OPRZ);
    }

    public byte[] getWmfzCosOprzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMFZ_COS_OPRZ, Pos.WMFZ_COS_OPRZ);
        return buffer;
    }

    public void initWmfzCosOprzSpaces() {
        fill(Pos.WMFZ_COS_OPRZ, Len.WMFZ_COS_OPRZ, Types.SPACE_CHAR);
    }

    public void setWmfzCosOprzNull(String wmfzCosOprzNull) {
        writeString(Pos.WMFZ_COS_OPRZ_NULL, wmfzCosOprzNull, Len.WMFZ_COS_OPRZ_NULL);
    }

    /**Original name: WMFZ-COS-OPRZ-NULL<br>*/
    public String getWmfzCosOprzNull() {
        return readString(Pos.WMFZ_COS_OPRZ_NULL, Len.WMFZ_COS_OPRZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMFZ_COS_OPRZ = 1;
        public static final int WMFZ_COS_OPRZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMFZ_COS_OPRZ = 8;
        public static final int WMFZ_COS_OPRZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WMFZ_COS_OPRZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WMFZ_COS_OPRZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
