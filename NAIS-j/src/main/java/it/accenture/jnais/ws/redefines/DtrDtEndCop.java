package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-DT-END-COP<br>
 * Variable: DTR-DT-END-COP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrDtEndCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrDtEndCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_DT_END_COP;
    }

    public void setDtrDtEndCop(int dtrDtEndCop) {
        writeIntAsPacked(Pos.DTR_DT_END_COP, dtrDtEndCop, Len.Int.DTR_DT_END_COP);
    }

    public void setDtrDtEndCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_DT_END_COP, Pos.DTR_DT_END_COP);
    }

    /**Original name: DTR-DT-END-COP<br>*/
    public int getDtrDtEndCop() {
        return readPackedAsInt(Pos.DTR_DT_END_COP, Len.Int.DTR_DT_END_COP);
    }

    public byte[] getDtrDtEndCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_DT_END_COP, Pos.DTR_DT_END_COP);
        return buffer;
    }

    public void setDtrDtEndCopNull(String dtrDtEndCopNull) {
        writeString(Pos.DTR_DT_END_COP_NULL, dtrDtEndCopNull, Len.DTR_DT_END_COP_NULL);
    }

    /**Original name: DTR-DT-END-COP-NULL<br>*/
    public String getDtrDtEndCopNull() {
        return readString(Pos.DTR_DT_END_COP_NULL, Len.DTR_DT_END_COP_NULL);
    }

    public String getDtrDtEndCopNullFormatted() {
        return Functions.padBlanks(getDtrDtEndCopNull(), Len.DTR_DT_END_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_DT_END_COP = 1;
        public static final int DTR_DT_END_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_DT_END_COP = 5;
        public static final int DTR_DT_END_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_DT_END_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
