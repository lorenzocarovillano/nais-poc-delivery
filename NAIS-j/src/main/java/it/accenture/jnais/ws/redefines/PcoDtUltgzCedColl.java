package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTGZ-CED-COLL<br>
 * Variable: PCO-DT-ULTGZ-CED-COLL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltgzCedColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltgzCedColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTGZ_CED_COLL;
    }

    public void setPcoDtUltgzCedColl(int pcoDtUltgzCedColl) {
        writeIntAsPacked(Pos.PCO_DT_ULTGZ_CED_COLL, pcoDtUltgzCedColl, Len.Int.PCO_DT_ULTGZ_CED_COLL);
    }

    public void setPcoDtUltgzCedCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTGZ_CED_COLL, Pos.PCO_DT_ULTGZ_CED_COLL);
    }

    /**Original name: PCO-DT-ULTGZ-CED-COLL<br>*/
    public int getPcoDtUltgzCedColl() {
        return readPackedAsInt(Pos.PCO_DT_ULTGZ_CED_COLL, Len.Int.PCO_DT_ULTGZ_CED_COLL);
    }

    public byte[] getPcoDtUltgzCedCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTGZ_CED_COLL, Pos.PCO_DT_ULTGZ_CED_COLL);
        return buffer;
    }

    public void initPcoDtUltgzCedCollHighValues() {
        fill(Pos.PCO_DT_ULTGZ_CED_COLL, Len.PCO_DT_ULTGZ_CED_COLL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltgzCedCollNull(String pcoDtUltgzCedCollNull) {
        writeString(Pos.PCO_DT_ULTGZ_CED_COLL_NULL, pcoDtUltgzCedCollNull, Len.PCO_DT_ULTGZ_CED_COLL_NULL);
    }

    /**Original name: PCO-DT-ULTGZ-CED-COLL-NULL<br>*/
    public String getPcoDtUltgzCedCollNull() {
        return readString(Pos.PCO_DT_ULTGZ_CED_COLL_NULL, Len.PCO_DT_ULTGZ_CED_COLL_NULL);
    }

    public String getPcoDtUltgzCedCollNullFormatted() {
        return Functions.padBlanks(getPcoDtUltgzCedCollNull(), Len.PCO_DT_ULTGZ_CED_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTGZ_CED_COLL = 1;
        public static final int PCO_DT_ULTGZ_CED_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTGZ_CED_COLL = 5;
        public static final int PCO_DT_ULTGZ_CED_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTGZ_CED_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
