package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-VARIABILI<br>
 * Variable: WK-VARIABILI from program LCCS1900<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVariabiliLccs1900 {

    //==== PROPERTIES ====
    //Original name: WK-DT-RICOR-SUCC
    private String dtRicorSucc = DefaultValues.stringVal(Len.DT_RICOR_SUCC);
    //Original name: WK-COD-CAN
    private int codCan = DefaultValues.INT_VAL;
    //Original name: WK-LABEL
    private String label = DefaultValues.stringVal(Len.LABEL);

    //==== METHODS ====
    public void setDtRicorSucc(int dtRicorSucc) {
        this.dtRicorSucc = NumericDisplay.asString(dtRicorSucc, Len.DT_RICOR_SUCC);
    }

    public void setDtRicorSuccFormatted(String dtRicorSucc) {
        this.dtRicorSucc = Trunc.toUnsignedNumeric(dtRicorSucc, Len.DT_RICOR_SUCC);
    }

    public int getDtRicorSucc() {
        return NumericDisplay.asInt(this.dtRicorSucc);
    }

    public String getDtRicorSuccFormatted() {
        return this.dtRicorSucc;
    }

    public void setCodCan(int codCan) {
        this.codCan = codCan;
    }

    public int getCodCan() {
        return this.codCan;
    }

    public void setLabel(String label) {
        this.label = Functions.subString(label, Len.LABEL);
    }

    public String getLabel() {
        return this.label;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_RICOR_SUCC = 8;
        public static final int LABEL = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
