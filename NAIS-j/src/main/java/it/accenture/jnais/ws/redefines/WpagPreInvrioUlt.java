package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRE-INVRIO-ULT<br>
 * Variable: WPAG-PRE-INVRIO-ULT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPreInvrioUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPreInvrioUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRE_INVRIO_ULT;
    }

    public void setWpagPreInvrioUlt(AfDecimal wpagPreInvrioUlt) {
        writeDecimalAsPacked(Pos.WPAG_PRE_INVRIO_ULT, wpagPreInvrioUlt.copy());
    }

    public void setWpagPreInvrioUltFormatted(String wpagPreInvrioUlt) {
        setWpagPreInvrioUlt(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRE_INVRIO_ULT + Len.Fract.WPAG_PRE_INVRIO_ULT, Len.Fract.WPAG_PRE_INVRIO_ULT, wpagPreInvrioUlt));
    }

    public void setWpagPreInvrioUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRE_INVRIO_ULT, Pos.WPAG_PRE_INVRIO_ULT);
    }

    /**Original name: WPAG-PRE-INVRIO-ULT<br>*/
    public AfDecimal getWpagPreInvrioUlt() {
        return readPackedAsDecimal(Pos.WPAG_PRE_INVRIO_ULT, Len.Int.WPAG_PRE_INVRIO_ULT, Len.Fract.WPAG_PRE_INVRIO_ULT);
    }

    public byte[] getWpagPreInvrioUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRE_INVRIO_ULT, Pos.WPAG_PRE_INVRIO_ULT);
        return buffer;
    }

    public void initWpagPreInvrioUltSpaces() {
        fill(Pos.WPAG_PRE_INVRIO_ULT, Len.WPAG_PRE_INVRIO_ULT, Types.SPACE_CHAR);
    }

    public void setWpagPreInvrioUltNull(String wpagPreInvrioUltNull) {
        writeString(Pos.WPAG_PRE_INVRIO_ULT_NULL, wpagPreInvrioUltNull, Len.WPAG_PRE_INVRIO_ULT_NULL);
    }

    /**Original name: WPAG-PRE-INVRIO-ULT-NULL<br>*/
    public String getWpagPreInvrioUltNull() {
        return readString(Pos.WPAG_PRE_INVRIO_ULT_NULL, Len.WPAG_PRE_INVRIO_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_INVRIO_ULT = 1;
        public static final int WPAG_PRE_INVRIO_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_INVRIO_ULT = 8;
        public static final int WPAG_PRE_INVRIO_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_INVRIO_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_INVRIO_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
