package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-DUR-AA-ADES-DFLT<br>
 * Variable: DAD-DUR-AA-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadDurAaAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadDurAaAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_DUR_AA_ADES_DFLT;
    }

    public void setDadDurAaAdesDflt(int dadDurAaAdesDflt) {
        writeIntAsPacked(Pos.DAD_DUR_AA_ADES_DFLT, dadDurAaAdesDflt, Len.Int.DAD_DUR_AA_ADES_DFLT);
    }

    public void setDadDurAaAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_DUR_AA_ADES_DFLT, Pos.DAD_DUR_AA_ADES_DFLT);
    }

    /**Original name: DAD-DUR-AA-ADES-DFLT<br>*/
    public int getDadDurAaAdesDflt() {
        return readPackedAsInt(Pos.DAD_DUR_AA_ADES_DFLT, Len.Int.DAD_DUR_AA_ADES_DFLT);
    }

    public byte[] getDadDurAaAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_DUR_AA_ADES_DFLT, Pos.DAD_DUR_AA_ADES_DFLT);
        return buffer;
    }

    public void setDadDurAaAdesDfltNull(String dadDurAaAdesDfltNull) {
        writeString(Pos.DAD_DUR_AA_ADES_DFLT_NULL, dadDurAaAdesDfltNull, Len.DAD_DUR_AA_ADES_DFLT_NULL);
    }

    /**Original name: DAD-DUR-AA-ADES-DFLT-NULL<br>*/
    public String getDadDurAaAdesDfltNull() {
        return readString(Pos.DAD_DUR_AA_ADES_DFLT_NULL, Len.DAD_DUR_AA_ADES_DFLT_NULL);
    }

    public String getDadDurAaAdesDfltNullFormatted() {
        return Functions.padBlanks(getDadDurAaAdesDfltNull(), Len.DAD_DUR_AA_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_DUR_AA_ADES_DFLT = 1;
        public static final int DAD_DUR_AA_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_DUR_AA_ADES_DFLT = 3;
        public static final int DAD_DUR_AA_ADES_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_DUR_AA_ADES_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
