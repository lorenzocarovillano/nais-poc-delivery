package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-TAX-SEP-DFZ<br>
 * Variable: WDFL-TAX-SEP-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflTaxSepDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflTaxSepDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_TAX_SEP_DFZ;
    }

    public void setWdflTaxSepDfz(AfDecimal wdflTaxSepDfz) {
        writeDecimalAsPacked(Pos.WDFL_TAX_SEP_DFZ, wdflTaxSepDfz.copy());
    }

    public void setWdflTaxSepDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_TAX_SEP_DFZ, Pos.WDFL_TAX_SEP_DFZ);
    }

    /**Original name: WDFL-TAX-SEP-DFZ<br>*/
    public AfDecimal getWdflTaxSepDfz() {
        return readPackedAsDecimal(Pos.WDFL_TAX_SEP_DFZ, Len.Int.WDFL_TAX_SEP_DFZ, Len.Fract.WDFL_TAX_SEP_DFZ);
    }

    public byte[] getWdflTaxSepDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_TAX_SEP_DFZ, Pos.WDFL_TAX_SEP_DFZ);
        return buffer;
    }

    public void setWdflTaxSepDfzNull(String wdflTaxSepDfzNull) {
        writeString(Pos.WDFL_TAX_SEP_DFZ_NULL, wdflTaxSepDfzNull, Len.WDFL_TAX_SEP_DFZ_NULL);
    }

    /**Original name: WDFL-TAX-SEP-DFZ-NULL<br>*/
    public String getWdflTaxSepDfzNull() {
        return readString(Pos.WDFL_TAX_SEP_DFZ_NULL, Len.WDFL_TAX_SEP_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_TAX_SEP_DFZ = 1;
        public static final int WDFL_TAX_SEP_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_TAX_SEP_DFZ = 8;
        public static final int WDFL_TAX_SEP_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_TAX_SEP_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_TAX_SEP_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
