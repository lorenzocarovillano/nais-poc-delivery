package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-REWRITE-FILESQS-ALL<br>
 * Variable: FLAG-REWRITE-FILESQS-ALL from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagRewriteFilesqsAll {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagRewriteFilesqsAll(char flagRewriteFilesqsAll) {
        this.value = flagRewriteFilesqsAll;
    }

    public char getFlagRewriteFilesqsAll() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setNo() {
        value = NO;
    }
}
