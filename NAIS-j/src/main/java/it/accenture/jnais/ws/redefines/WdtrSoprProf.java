package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-SOPR-PROF<br>
 * Variable: WDTR-SOPR-PROF from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_SOPR_PROF;
    }

    public void setWdtrSoprProf(AfDecimal wdtrSoprProf) {
        writeDecimalAsPacked(Pos.WDTR_SOPR_PROF, wdtrSoprProf.copy());
    }

    /**Original name: WDTR-SOPR-PROF<br>*/
    public AfDecimal getWdtrSoprProf() {
        return readPackedAsDecimal(Pos.WDTR_SOPR_PROF, Len.Int.WDTR_SOPR_PROF, Len.Fract.WDTR_SOPR_PROF);
    }

    public void setWdtrSoprProfNull(String wdtrSoprProfNull) {
        writeString(Pos.WDTR_SOPR_PROF_NULL, wdtrSoprProfNull, Len.WDTR_SOPR_PROF_NULL);
    }

    /**Original name: WDTR-SOPR-PROF-NULL<br>*/
    public String getWdtrSoprProfNull() {
        return readString(Pos.WDTR_SOPR_PROF_NULL, Len.WDTR_SOPR_PROF_NULL);
    }

    public String getWdtrSoprProfNullFormatted() {
        return Functions.padBlanks(getWdtrSoprProfNull(), Len.WDTR_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_SOPR_PROF = 1;
        public static final int WDTR_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_SOPR_PROF = 8;
        public static final int WDTR_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
