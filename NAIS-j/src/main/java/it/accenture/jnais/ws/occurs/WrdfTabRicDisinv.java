package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.copy.Lccvrdf1;
import it.accenture.jnais.copy.WrdfDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WRDF-TAB-RIC-DISINV<br>
 * Variables: WRDF-TAB-RIC-DISINV from copybook LCCVRDFA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WrdfTabRicDisinv implements ICopyable<WrdfTabRicDisinv> {

    //==== PROPERTIES ====
    //Original name: LCCVRDF1
    private Lccvrdf1 lccvrdf1 = new Lccvrdf1();

    //==== CONSTRUCTORS ====
    public WrdfTabRicDisinv() {
    }

    public WrdfTabRicDisinv(WrdfTabRicDisinv wrdfTabRicDisinv) {
        this();
        this.lccvrdf1 = ((Lccvrdf1)wrdfTabRicDisinv.lccvrdf1.copy());
    }

    //==== METHODS ====
    public void setWrdfTabRicDisinvBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvrdf1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvrdf1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvrdf1.Len.Int.ID_PTF, 0));
        position += Lccvrdf1.Len.ID_PTF;
        lccvrdf1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWrdfTabRicDisinvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvrdf1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvrdf1.getIdPtf(), Lccvrdf1.Len.Int.ID_PTF, 0);
        position += Lccvrdf1.Len.ID_PTF;
        lccvrdf1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public WrdfTabRicDisinv initWrdfTabRicDisinvSpaces() {
        lccvrdf1.initLccvrdf1Spaces();
        return this;
    }

    public WrdfTabRicDisinv copy() {
        return new WrdfTabRicDisinv(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRDF_TAB_RIC_DISINV = WpolStatus.Len.STATUS + Lccvrdf1.Len.ID_PTF + WrdfDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
