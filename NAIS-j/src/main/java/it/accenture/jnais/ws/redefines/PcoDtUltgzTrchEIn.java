package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTGZ-TRCH-E-IN<br>
 * Variable: PCO-DT-ULTGZ-TRCH-E-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltgzTrchEIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltgzTrchEIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTGZ_TRCH_E_IN;
    }

    public void setPcoDtUltgzTrchEIn(int pcoDtUltgzTrchEIn) {
        writeIntAsPacked(Pos.PCO_DT_ULTGZ_TRCH_E_IN, pcoDtUltgzTrchEIn, Len.Int.PCO_DT_ULTGZ_TRCH_E_IN);
    }

    public void setPcoDtUltgzTrchEInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTGZ_TRCH_E_IN, Pos.PCO_DT_ULTGZ_TRCH_E_IN);
    }

    /**Original name: PCO-DT-ULTGZ-TRCH-E-IN<br>*/
    public int getPcoDtUltgzTrchEIn() {
        return readPackedAsInt(Pos.PCO_DT_ULTGZ_TRCH_E_IN, Len.Int.PCO_DT_ULTGZ_TRCH_E_IN);
    }

    public byte[] getPcoDtUltgzTrchEInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTGZ_TRCH_E_IN, Pos.PCO_DT_ULTGZ_TRCH_E_IN);
        return buffer;
    }

    public void initPcoDtUltgzTrchEInHighValues() {
        fill(Pos.PCO_DT_ULTGZ_TRCH_E_IN, Len.PCO_DT_ULTGZ_TRCH_E_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltgzTrchEInNull(String pcoDtUltgzTrchEInNull) {
        writeString(Pos.PCO_DT_ULTGZ_TRCH_E_IN_NULL, pcoDtUltgzTrchEInNull, Len.PCO_DT_ULTGZ_TRCH_E_IN_NULL);
    }

    /**Original name: PCO-DT-ULTGZ-TRCH-E-IN-NULL<br>*/
    public String getPcoDtUltgzTrchEInNull() {
        return readString(Pos.PCO_DT_ULTGZ_TRCH_E_IN_NULL, Len.PCO_DT_ULTGZ_TRCH_E_IN_NULL);
    }

    public String getPcoDtUltgzTrchEInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltgzTrchEInNull(), Len.PCO_DT_ULTGZ_TRCH_E_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTGZ_TRCH_E_IN = 1;
        public static final int PCO_DT_ULTGZ_TRCH_E_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTGZ_TRCH_E_IN = 5;
        public static final int PCO_DT_ULTGZ_TRCH_E_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTGZ_TRCH_E_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
