package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-ULT-COEFF-AGG-RIS<br>
 * Variable: RST-ULT-COEFF-AGG-RIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstUltCoeffAggRis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstUltCoeffAggRis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_ULT_COEFF_AGG_RIS;
    }

    public void setRstUltCoeffAggRis(AfDecimal rstUltCoeffAggRis) {
        writeDecimalAsPacked(Pos.RST_ULT_COEFF_AGG_RIS, rstUltCoeffAggRis.copy());
    }

    public void setRstUltCoeffAggRisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_ULT_COEFF_AGG_RIS, Pos.RST_ULT_COEFF_AGG_RIS);
    }

    /**Original name: RST-ULT-COEFF-AGG-RIS<br>*/
    public AfDecimal getRstUltCoeffAggRis() {
        return readPackedAsDecimal(Pos.RST_ULT_COEFF_AGG_RIS, Len.Int.RST_ULT_COEFF_AGG_RIS, Len.Fract.RST_ULT_COEFF_AGG_RIS);
    }

    public byte[] getRstUltCoeffAggRisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_ULT_COEFF_AGG_RIS, Pos.RST_ULT_COEFF_AGG_RIS);
        return buffer;
    }

    public void setRstUltCoeffAggRisNull(String rstUltCoeffAggRisNull) {
        writeString(Pos.RST_ULT_COEFF_AGG_RIS_NULL, rstUltCoeffAggRisNull, Len.RST_ULT_COEFF_AGG_RIS_NULL);
    }

    /**Original name: RST-ULT-COEFF-AGG-RIS-NULL<br>*/
    public String getRstUltCoeffAggRisNull() {
        return readString(Pos.RST_ULT_COEFF_AGG_RIS_NULL, Len.RST_ULT_COEFF_AGG_RIS_NULL);
    }

    public String getRstUltCoeffAggRisNullFormatted() {
        return Functions.padBlanks(getRstUltCoeffAggRisNull(), Len.RST_ULT_COEFF_AGG_RIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_ULT_COEFF_AGG_RIS = 1;
        public static final int RST_ULT_COEFF_AGG_RIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_ULT_COEFF_AGG_RIS = 8;
        public static final int RST_ULT_COEFF_AGG_RIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_ULT_COEFF_AGG_RIS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_ULT_COEFF_AGG_RIS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
