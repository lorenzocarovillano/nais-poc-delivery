package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-VIS-662014C<br>
 * Variable: DFL-IMPB-VIS-662014C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbVis662014c extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbVis662014c() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_VIS662014C;
    }

    public void setDflImpbVis662014c(AfDecimal dflImpbVis662014c) {
        writeDecimalAsPacked(Pos.DFL_IMPB_VIS662014C, dflImpbVis662014c.copy());
    }

    public void setDflImpbVis662014cFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_VIS662014C, Pos.DFL_IMPB_VIS662014C);
    }

    /**Original name: DFL-IMPB-VIS-662014C<br>*/
    public AfDecimal getDflImpbVis662014c() {
        return readPackedAsDecimal(Pos.DFL_IMPB_VIS662014C, Len.Int.DFL_IMPB_VIS662014C, Len.Fract.DFL_IMPB_VIS662014C);
    }

    public byte[] getDflImpbVis662014cAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_VIS662014C, Pos.DFL_IMPB_VIS662014C);
        return buffer;
    }

    public void setDflImpbVis662014cNull(String dflImpbVis662014cNull) {
        writeString(Pos.DFL_IMPB_VIS662014C_NULL, dflImpbVis662014cNull, Len.DFL_IMPB_VIS662014C_NULL);
    }

    /**Original name: DFL-IMPB-VIS-662014C-NULL<br>*/
    public String getDflImpbVis662014cNull() {
        return readString(Pos.DFL_IMPB_VIS662014C_NULL, Len.DFL_IMPB_VIS662014C_NULL);
    }

    public String getDflImpbVis662014cNullFormatted() {
        return Functions.padBlanks(getDflImpbVis662014cNull(), Len.DFL_IMPB_VIS662014C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS662014C = 1;
        public static final int DFL_IMPB_VIS662014C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS662014C = 8;
        public static final int DFL_IMPB_VIS662014C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS662014C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS662014C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
