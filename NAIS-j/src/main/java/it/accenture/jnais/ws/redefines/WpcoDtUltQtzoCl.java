package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-QTZO-CL<br>
 * Variable: WPCO-DT-ULT-QTZO-CL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltQtzoCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltQtzoCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_QTZO_CL;
    }

    public void setWpcoDtUltQtzoCl(int wpcoDtUltQtzoCl) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_QTZO_CL, wpcoDtUltQtzoCl, Len.Int.WPCO_DT_ULT_QTZO_CL);
    }

    public void setDpcoDtUltQtzoClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_QTZO_CL, Pos.WPCO_DT_ULT_QTZO_CL);
    }

    /**Original name: WPCO-DT-ULT-QTZO-CL<br>*/
    public int getWpcoDtUltQtzoCl() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_QTZO_CL, Len.Int.WPCO_DT_ULT_QTZO_CL);
    }

    public byte[] getWpcoDtUltQtzoClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_QTZO_CL, Pos.WPCO_DT_ULT_QTZO_CL);
        return buffer;
    }

    public void setWpcoDtUltQtzoClNull(String wpcoDtUltQtzoClNull) {
        writeString(Pos.WPCO_DT_ULT_QTZO_CL_NULL, wpcoDtUltQtzoClNull, Len.WPCO_DT_ULT_QTZO_CL_NULL);
    }

    /**Original name: WPCO-DT-ULT-QTZO-CL-NULL<br>*/
    public String getWpcoDtUltQtzoClNull() {
        return readString(Pos.WPCO_DT_ULT_QTZO_CL_NULL, Len.WPCO_DT_ULT_QTZO_CL_NULL);
    }

    public String getWpcoDtUltQtzoClNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltQtzoClNull(), Len.WPCO_DT_ULT_QTZO_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_QTZO_CL = 1;
        public static final int WPCO_DT_ULT_QTZO_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_QTZO_CL = 5;
        public static final int WPCO_DT_ULT_QTZO_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_QTZO_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
