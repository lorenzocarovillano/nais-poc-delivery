package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-FLAG-GUIDE-TYPE<br>
 * Variable: WK-FLAG-GUIDE-TYPE from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFlagGuideType {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_FLAG_GUIDE_TYPE);
    public static final String SEQUENTIAL_GUIDE = "SQ";
    public static final String DB_GUIDE = "DB";

    //==== METHODS ====
    public void setWkFlagGuideType(String wkFlagGuideType) {
        this.value = Functions.subString(wkFlagGuideType, Len.WK_FLAG_GUIDE_TYPE);
    }

    public String getWkFlagGuideType() {
        return this.value;
    }

    public boolean isSequentialGuide() {
        return value.equals(SEQUENTIAL_GUIDE);
    }

    public void setSequentialGuide() {
        value = SEQUENTIAL_GUIDE;
    }

    public void setDbGuide() {
        value = DB_GUIDE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_FLAG_GUIDE_TYPE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
