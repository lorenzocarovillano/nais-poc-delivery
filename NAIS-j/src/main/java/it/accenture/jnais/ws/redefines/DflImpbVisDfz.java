package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-VIS-DFZ<br>
 * Variable: DFL-IMPB-VIS-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbVisDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbVisDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_VIS_DFZ;
    }

    public void setDflImpbVisDfz(AfDecimal dflImpbVisDfz) {
        writeDecimalAsPacked(Pos.DFL_IMPB_VIS_DFZ, dflImpbVisDfz.copy());
    }

    public void setDflImpbVisDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_VIS_DFZ, Pos.DFL_IMPB_VIS_DFZ);
    }

    /**Original name: DFL-IMPB-VIS-DFZ<br>*/
    public AfDecimal getDflImpbVisDfz() {
        return readPackedAsDecimal(Pos.DFL_IMPB_VIS_DFZ, Len.Int.DFL_IMPB_VIS_DFZ, Len.Fract.DFL_IMPB_VIS_DFZ);
    }

    public byte[] getDflImpbVisDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_VIS_DFZ, Pos.DFL_IMPB_VIS_DFZ);
        return buffer;
    }

    public void setDflImpbVisDfzNull(String dflImpbVisDfzNull) {
        writeString(Pos.DFL_IMPB_VIS_DFZ_NULL, dflImpbVisDfzNull, Len.DFL_IMPB_VIS_DFZ_NULL);
    }

    /**Original name: DFL-IMPB-VIS-DFZ-NULL<br>*/
    public String getDflImpbVisDfzNull() {
        return readString(Pos.DFL_IMPB_VIS_DFZ_NULL, Len.DFL_IMPB_VIS_DFZ_NULL);
    }

    public String getDflImpbVisDfzNullFormatted() {
        return Functions.padBlanks(getDflImpbVisDfzNull(), Len.DFL_IMPB_VIS_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS_DFZ = 1;
        public static final int DFL_IMPB_VIS_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS_DFZ = 8;
        public static final int DFL_IMPB_VIS_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
