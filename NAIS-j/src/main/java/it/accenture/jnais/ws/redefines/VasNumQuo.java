package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-NUM-QUO<br>
 * Variable: VAS-NUM-QUO from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasNumQuo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasNumQuo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_NUM_QUO;
    }

    public void setVasNumQuo(AfDecimal vasNumQuo) {
        writeDecimalAsPacked(Pos.VAS_NUM_QUO, vasNumQuo.copy());
    }

    public void setVasNumQuoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_NUM_QUO, Pos.VAS_NUM_QUO);
    }

    /**Original name: VAS-NUM-QUO<br>*/
    public AfDecimal getVasNumQuo() {
        return readPackedAsDecimal(Pos.VAS_NUM_QUO, Len.Int.VAS_NUM_QUO, Len.Fract.VAS_NUM_QUO);
    }

    public byte[] getVasNumQuoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_NUM_QUO, Pos.VAS_NUM_QUO);
        return buffer;
    }

    public void setVasNumQuoNull(String vasNumQuoNull) {
        writeString(Pos.VAS_NUM_QUO_NULL, vasNumQuoNull, Len.VAS_NUM_QUO_NULL);
    }

    /**Original name: VAS-NUM-QUO-NULL<br>*/
    public String getVasNumQuoNull() {
        return readString(Pos.VAS_NUM_QUO_NULL, Len.VAS_NUM_QUO_NULL);
    }

    public String getVasNumQuoNullFormatted() {
        return Functions.padBlanks(getVasNumQuoNull(), Len.VAS_NUM_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_NUM_QUO = 1;
        public static final int VAS_NUM_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_NUM_QUO = 7;
        public static final int VAS_NUM_QUO_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAS_NUM_QUO = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_NUM_QUO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
