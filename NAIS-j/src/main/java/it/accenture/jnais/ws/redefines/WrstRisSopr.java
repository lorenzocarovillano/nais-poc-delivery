package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-SOPR<br>
 * Variable: WRST-RIS-SOPR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisSopr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisSopr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_SOPR;
    }

    public void setWrstRisSopr(AfDecimal wrstRisSopr) {
        writeDecimalAsPacked(Pos.WRST_RIS_SOPR, wrstRisSopr.copy());
    }

    public void setWrstRisSoprFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_SOPR, Pos.WRST_RIS_SOPR);
    }

    /**Original name: WRST-RIS-SOPR<br>*/
    public AfDecimal getWrstRisSopr() {
        return readPackedAsDecimal(Pos.WRST_RIS_SOPR, Len.Int.WRST_RIS_SOPR, Len.Fract.WRST_RIS_SOPR);
    }

    public byte[] getWrstRisSoprAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_SOPR, Pos.WRST_RIS_SOPR);
        return buffer;
    }

    public void initWrstRisSoprSpaces() {
        fill(Pos.WRST_RIS_SOPR, Len.WRST_RIS_SOPR, Types.SPACE_CHAR);
    }

    public void setWrstRisSoprNull(String wrstRisSoprNull) {
        writeString(Pos.WRST_RIS_SOPR_NULL, wrstRisSoprNull, Len.WRST_RIS_SOPR_NULL);
    }

    /**Original name: WRST-RIS-SOPR-NULL<br>*/
    public String getWrstRisSoprNull() {
        return readString(Pos.WRST_RIS_SOPR_NULL, Len.WRST_RIS_SOPR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_SOPR = 1;
        public static final int WRST_RIS_SOPR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_SOPR = 8;
        public static final int WRST_RIS_SOPR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_SOPR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_SOPR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
