package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WK-TAB-VAR<br>
 * Variable: WK-TAB-VAR from program LOAS0310<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkTabVar extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_VARIABILI_MAXOCCURS = 70;
    public static final int TAB_LIVELLO_MAXOCCURS = 400;

    //==== CONSTRUCTORS ====
    public WkTabVar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_TAB_VAR;
    }

    public String getWkTabVarFormatted() {
        return readFixedString(Pos.WK_TAB_VAR, Len.WK_TAB_VAR);
    }

    public void setCodCompAniaFormatted(int codCompAniaIdx, String codCompAnia) {
        int position = Pos.wkCodCompAnia(codCompAniaIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(codCompAnia, Len.COD_COMP_ANIA), Len.COD_COMP_ANIA);
    }

    /**Original name: WK-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wkCodCompAnia(codCompAniaIdx - 1);
        return readNumDispUnsignedInt(position, Len.COD_COMP_ANIA);
    }

    public void setTpLivello(int tpLivelloIdx, char tpLivello) {
        int position = Pos.wkTpLivello(tpLivelloIdx - 1);
        writeChar(position, tpLivello);
    }

    /**Original name: WK-TP-LIVELLO<br>*/
    public char getTpLivello(int tpLivelloIdx) {
        int position = Pos.wkTpLivello(tpLivelloIdx - 1);
        return readChar(position);
    }

    public void setCodLivello(int codLivelloIdx, String codLivello) {
        int position = Pos.wkCodLivello(codLivelloIdx - 1);
        writeString(position, codLivello, Len.COD_LIVELLO);
    }

    /**Original name: WK-COD-LIVELLO<br>*/
    public String getCodLivello(int codLivelloIdx) {
        int position = Pos.wkCodLivello(codLivelloIdx - 1);
        return readString(position, Len.COD_LIVELLO);
    }

    public void setIdLivelloFormatted(int idLivelloIdx, String idLivello) {
        int position = Pos.wkIdLivello(idLivelloIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(idLivello, Len.ID_LIVELLO), Len.ID_LIVELLO);
    }

    /**Original name: WK-ID-LIVELLO<br>*/
    public int getIdLivello(int idLivelloIdx) {
        int position = Pos.wkIdLivello(idLivelloIdx - 1);
        return readNumDispUnsignedInt(position, Len.ID_LIVELLO);
    }

    public void setEleVariabiliMax(int eleVariabiliMaxIdx, short eleVariabiliMax) {
        int position = Pos.wkEleVariabiliMax(eleVariabiliMaxIdx - 1);
        writeShortAsPacked(position, eleVariabiliMax, Len.Int.ELE_VARIABILI_MAX);
    }

    /**Original name: WK-ELE-VARIABILI-MAX<br>*/
    public short getEleVariabiliMax(int eleVariabiliMaxIdx) {
        int position = Pos.wkEleVariabiliMax(eleVariabiliMaxIdx - 1);
        return readPackedAsShort(position, Len.Int.ELE_VARIABILI_MAX);
    }

    public void setCodVariabile(int codVariabileIdx1, int codVariabileIdx2, String codVariabile) {
        int position = Pos.wkCodVariabile(codVariabileIdx1 - 1, codVariabileIdx2 - 1);
        writeString(position, codVariabile, Len.COD_VARIABILE);
    }

    /**Original name: WK-COD-VARIABILE<br>*/
    public String getCodVariabile(int codVariabileIdx1, int codVariabileIdx2) {
        int position = Pos.wkCodVariabile(codVariabileIdx1 - 1, codVariabileIdx2 - 1);
        return readString(position, Len.COD_VARIABILE);
    }

    public void setTpDato(int tpDatoIdx1, int tpDatoIdx2, char tpDato) {
        int position = Pos.wkTpDato(tpDatoIdx1 - 1, tpDatoIdx2 - 1);
        writeChar(position, tpDato);
    }

    /**Original name: WK-TP-DATO<br>*/
    public char getTpDato(int tpDatoIdx1, int tpDatoIdx2) {
        int position = Pos.wkTpDato(tpDatoIdx1 - 1, tpDatoIdx2 - 1);
        return readChar(position);
    }

    public void setValImp(int valImpIdx1, int valImpIdx2, AfDecimal valImp) {
        int position = Pos.wkValImp(valImpIdx1 - 1, valImpIdx2 - 1);
        writeDecimal(position, valImp.copy(), SignType.NO_SIGN);
    }

    /**Original name: WK-VAL-IMP<br>*/
    public AfDecimal getValImp(int valImpIdx1, int valImpIdx2) {
        int position = Pos.wkValImp(valImpIdx1 - 1, valImpIdx2 - 1);
        return readDecimal(position, Len.Int.VAL_IMP, Len.Fract.VAL_IMP, SignType.NO_SIGN);
    }

    public void setValPerc(int valPercIdx1, int valPercIdx2, AfDecimal valPerc) {
        int position = Pos.wkValPerc(valPercIdx1 - 1, valPercIdx2 - 1);
        writeDecimal(position, valPerc.copy(), SignType.NO_SIGN);
    }

    /**Original name: WK-VAL-PERC<br>*/
    public AfDecimal getValPerc(int valPercIdx1, int valPercIdx2) {
        int position = Pos.wkValPerc(valPercIdx1 - 1, valPercIdx2 - 1);
        return readDecimal(position, Len.Int.VAL_PERC, Len.Fract.VAL_PERC, SignType.NO_SIGN);
    }

    public void setValStr(int valStrIdx1, int valStrIdx2, String valStr) {
        int position = Pos.wkValStr(valStrIdx1 - 1, valStrIdx2 - 1);
        writeString(position, valStr, Len.VAL_STR);
    }

    /**Original name: WK-VAL-STR<br>*/
    public String getValStr(int valStrIdx1, int valStrIdx2) {
        int position = Pos.wkValStr(valStrIdx1 - 1, valStrIdx2 - 1);
        return readString(position, Len.VAL_STR);
    }

    public void setNomeServizio(int nomeServizioIdx, String nomeServizio) {
        int position = Pos.wkNomeServizio(nomeServizioIdx - 1);
        writeString(position, nomeServizio, Len.NOME_SERVIZIO);
    }

    /**Original name: WK-NOME-SERVIZIO<br>*/
    public String getNomeServizio(int nomeServizioIdx) {
        int position = Pos.wkNomeServizio(nomeServizioIdx - 1);
        return readString(position, Len.NOME_SERVIZIO);
    }

    public void setRestoTabVar(String restoTabVar) {
        writeString(Pos.RESTO_TAB_VAR, restoTabVar, Len.RESTO_TAB_VAR);
    }

    /**Original name: WK-RESTO-TAB-VAR<br>*/
    public String getRestoTabVar() {
        return readString(Pos.RESTO_TAB_VAR, Len.RESTO_TAB_VAR);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_TAB_VAR = 1;
        public static final int WK_TAB_VAR_R = 1;
        public static final int FLR1 = WK_TAB_VAR_R;
        public static final int RESTO_TAB_VAR = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wkTabLivello(int idx) {
            return WK_TAB_VAR + idx * Len.TAB_LIVELLO;
        }

        public static int wkCodCompAnia(int idx) {
            return wkTabLivello(idx);
        }

        public static int wkTpLivello(int idx) {
            return wkCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wkCodLivello(int idx) {
            return wkTpLivello(idx) + Len.TP_LIVELLO;
        }

        public static int wkIdLivello(int idx) {
            return wkCodLivello(idx) + Len.COD_LIVELLO;
        }

        public static int wkAreaVariabili(int idx) {
            return wkIdLivello(idx) + Len.ID_LIVELLO;
        }

        public static int wkEleVariabiliMax(int idx) {
            return wkAreaVariabili(idx);
        }

        public static int wkTabVariabili(int idx1, int idx2) {
            return wkEleVariabiliMax(idx1) + Len.ELE_VARIABILI_MAX + idx2 * Len.TAB_VARIABILI;
        }

        public static int wkCodVariabile(int idx1, int idx2) {
            return wkTabVariabili(idx1, idx2);
        }

        public static int wkTpDato(int idx1, int idx2) {
            return wkCodVariabile(idx1, idx2) + Len.COD_VARIABILE;
        }

        public static int wkValImp(int idx1, int idx2) {
            return wkTpDato(idx1, idx2) + Len.TP_DATO;
        }

        public static int wkValPerc(int idx1, int idx2) {
            return wkValImp(idx1, idx2) + Len.VAL_IMP;
        }

        public static int wkValStr(int idx1, int idx2) {
            return wkValPerc(idx1, idx2) + Len.VAL_PERC;
        }

        public static int wkNomeServizio(int idx) {
            return wkValStr(idx, TAB_VARIABILI_MAXOCCURS - 1) + Len.VAL_STR;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_COMP_ANIA = 5;
        public static final int TP_LIVELLO = 1;
        public static final int COD_LIVELLO = 12;
        public static final int ID_LIVELLO = 9;
        public static final int ELE_VARIABILI_MAX = 3;
        public static final int COD_VARIABILE = 12;
        public static final int TP_DATO = 1;
        public static final int VAL_IMP = 18;
        public static final int VAL_PERC = 14;
        public static final int VAL_STR = 12;
        public static final int TAB_VARIABILI = COD_VARIABILE + TP_DATO + VAL_IMP + VAL_PERC + VAL_STR;
        public static final int AREA_VARIABILI = ELE_VARIABILI_MAX + WkTabVar.TAB_VARIABILI_MAXOCCURS * TAB_VARIABILI;
        public static final int NOME_SERVIZIO = 8;
        public static final int TAB_LIVELLO = COD_COMP_ANIA + TP_LIVELLO + COD_LIVELLO + ID_LIVELLO + AREA_VARIABILI + NOME_SERVIZIO;
        public static final int FLR1 = 4028;
        public static final int WK_TAB_VAR = WkTabVar.TAB_LIVELLO_MAXOCCURS * TAB_LIVELLO;
        public static final int RESTO_TAB_VAR = 1607172;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ELE_VARIABILI_MAX = 4;
            public static final int VAL_IMP = 11;
            public static final int VAL_PERC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAL_IMP = 7;
            public static final int VAL_PERC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
