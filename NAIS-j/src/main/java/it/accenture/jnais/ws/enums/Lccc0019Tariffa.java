package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0019-TARIFFA<br>
 * Variable: LCCC0019-TARIFFA from copybook LCCC0019<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0019Tariffa {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TARIFFA);
    public static final String OK = "OK";
    public static final String KO = "KO";

    //==== METHODS ====
    public void setTariffa(String tariffa) {
        this.value = Functions.subString(tariffa, Len.TARIFFA);
    }

    public String getTariffa() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TARIFFA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
