package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-COEFF-RIS-1-T<br>
 * Variable: W-B03-COEFF-RIS-1-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CoeffRis1TLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CoeffRis1TLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_COEFF_RIS1_T;
    }

    public void setwB03CoeffRis1T(AfDecimal wB03CoeffRis1T) {
        writeDecimalAsPacked(Pos.W_B03_COEFF_RIS1_T, wB03CoeffRis1T.copy());
    }

    /**Original name: W-B03-COEFF-RIS-1-T<br>*/
    public AfDecimal getwB03CoeffRis1T() {
        return readPackedAsDecimal(Pos.W_B03_COEFF_RIS1_T, Len.Int.W_B03_COEFF_RIS1_T, Len.Fract.W_B03_COEFF_RIS1_T);
    }

    public byte[] getwB03CoeffRis1TAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_COEFF_RIS1_T, Pos.W_B03_COEFF_RIS1_T);
        return buffer;
    }

    public void setwB03CoeffRis1TNull(String wB03CoeffRis1TNull) {
        writeString(Pos.W_B03_COEFF_RIS1_T_NULL, wB03CoeffRis1TNull, Len.W_B03_COEFF_RIS1_T_NULL);
    }

    /**Original name: W-B03-COEFF-RIS-1-T-NULL<br>*/
    public String getwB03CoeffRis1TNull() {
        return readString(Pos.W_B03_COEFF_RIS1_T_NULL, Len.W_B03_COEFF_RIS1_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_COEFF_RIS1_T = 1;
        public static final int W_B03_COEFF_RIS1_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_COEFF_RIS1_T = 8;
        public static final int W_B03_COEFF_RIS1_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_COEFF_RIS1_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_COEFF_RIS1_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
