package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMPB-VIS<br>
 * Variable: S089-TOT-IMPB-VIS from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpbVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpbVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMPB_VIS;
    }

    public void setWlquTotImpbVis(AfDecimal wlquTotImpbVis) {
        writeDecimalAsPacked(Pos.S089_TOT_IMPB_VIS, wlquTotImpbVis.copy());
    }

    public void setWlquTotImpbVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMPB_VIS, Pos.S089_TOT_IMPB_VIS);
    }

    /**Original name: WLQU-TOT-IMPB-VIS<br>*/
    public AfDecimal getWlquTotImpbVis() {
        return readPackedAsDecimal(Pos.S089_TOT_IMPB_VIS, Len.Int.WLQU_TOT_IMPB_VIS, Len.Fract.WLQU_TOT_IMPB_VIS);
    }

    public byte[] getWlquTotImpbVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMPB_VIS, Pos.S089_TOT_IMPB_VIS);
        return buffer;
    }

    public void initWlquTotImpbVisSpaces() {
        fill(Pos.S089_TOT_IMPB_VIS, Len.S089_TOT_IMPB_VIS, Types.SPACE_CHAR);
    }

    public void setWlquTotImpbVisNull(String wlquTotImpbVisNull) {
        writeString(Pos.S089_TOT_IMPB_VIS_NULL, wlquTotImpbVisNull, Len.WLQU_TOT_IMPB_VIS_NULL);
    }

    /**Original name: WLQU-TOT-IMPB-VIS-NULL<br>*/
    public String getWlquTotImpbVisNull() {
        return readString(Pos.S089_TOT_IMPB_VIS_NULL, Len.WLQU_TOT_IMPB_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMPB_VIS = 1;
        public static final int S089_TOT_IMPB_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMPB_VIS = 8;
        public static final int WLQU_TOT_IMPB_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMPB_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMPB_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
