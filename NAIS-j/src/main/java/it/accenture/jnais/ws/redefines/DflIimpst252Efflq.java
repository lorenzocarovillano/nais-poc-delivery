package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IIMPST-252-EFFLQ<br>
 * Variable: DFL-IIMPST-252-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIimpst252Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIimpst252Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IIMPST252_EFFLQ;
    }

    public void setDflIimpst252Efflq(AfDecimal dflIimpst252Efflq) {
        writeDecimalAsPacked(Pos.DFL_IIMPST252_EFFLQ, dflIimpst252Efflq.copy());
    }

    public void setDflIimpst252EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IIMPST252_EFFLQ, Pos.DFL_IIMPST252_EFFLQ);
    }

    /**Original name: DFL-IIMPST-252-EFFLQ<br>*/
    public AfDecimal getDflIimpst252Efflq() {
        return readPackedAsDecimal(Pos.DFL_IIMPST252_EFFLQ, Len.Int.DFL_IIMPST252_EFFLQ, Len.Fract.DFL_IIMPST252_EFFLQ);
    }

    public byte[] getDflIimpst252EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IIMPST252_EFFLQ, Pos.DFL_IIMPST252_EFFLQ);
        return buffer;
    }

    public void setDflIimpst252EfflqNull(String dflIimpst252EfflqNull) {
        writeString(Pos.DFL_IIMPST252_EFFLQ_NULL, dflIimpst252EfflqNull, Len.DFL_IIMPST252_EFFLQ_NULL);
    }

    /**Original name: DFL-IIMPST-252-EFFLQ-NULL<br>*/
    public String getDflIimpst252EfflqNull() {
        return readString(Pos.DFL_IIMPST252_EFFLQ_NULL, Len.DFL_IIMPST252_EFFLQ_NULL);
    }

    public String getDflIimpst252EfflqNullFormatted() {
        return Functions.padBlanks(getDflIimpst252EfflqNull(), Len.DFL_IIMPST252_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST252_EFFLQ = 1;
        public static final int DFL_IIMPST252_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST252_EFFLQ = 8;
        public static final int DFL_IIMPST252_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST252_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST252_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
