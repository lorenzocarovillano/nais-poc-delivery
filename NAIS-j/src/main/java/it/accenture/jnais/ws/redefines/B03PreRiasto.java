package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRE-RIASTO<br>
 * Variable: B03-PRE-RIASTO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PreRiasto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PreRiasto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRE_RIASTO;
    }

    public void setB03PreRiasto(AfDecimal b03PreRiasto) {
        writeDecimalAsPacked(Pos.B03_PRE_RIASTO, b03PreRiasto.copy());
    }

    public void setB03PreRiastoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRE_RIASTO, Pos.B03_PRE_RIASTO);
    }

    /**Original name: B03-PRE-RIASTO<br>*/
    public AfDecimal getB03PreRiasto() {
        return readPackedAsDecimal(Pos.B03_PRE_RIASTO, Len.Int.B03_PRE_RIASTO, Len.Fract.B03_PRE_RIASTO);
    }

    public byte[] getB03PreRiastoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRE_RIASTO, Pos.B03_PRE_RIASTO);
        return buffer;
    }

    public void setB03PreRiastoNull(String b03PreRiastoNull) {
        writeString(Pos.B03_PRE_RIASTO_NULL, b03PreRiastoNull, Len.B03_PRE_RIASTO_NULL);
    }

    /**Original name: B03-PRE-RIASTO-NULL<br>*/
    public String getB03PreRiastoNull() {
        return readString(Pos.B03_PRE_RIASTO_NULL, Len.B03_PRE_RIASTO_NULL);
    }

    public String getB03PreRiastoNullFormatted() {
        return Functions.padBlanks(getB03PreRiastoNull(), Len.B03_PRE_RIASTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRE_RIASTO = 1;
        public static final int B03_PRE_RIASTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRE_RIASTO = 8;
        public static final int B03_PRE_RIASTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRE_RIASTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRE_RIASTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
