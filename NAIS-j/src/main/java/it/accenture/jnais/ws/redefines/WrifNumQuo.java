package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRIF-NUM-QUO<br>
 * Variable: WRIF-NUM-QUO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrifNumQuo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrifNumQuo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIF_NUM_QUO;
    }

    public void setWrifNumQuo(AfDecimal wrifNumQuo) {
        writeDecimalAsPacked(Pos.WRIF_NUM_QUO, wrifNumQuo.copy());
    }

    public void setWrifNumQuoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIF_NUM_QUO, Pos.WRIF_NUM_QUO);
    }

    /**Original name: WRIF-NUM-QUO<br>*/
    public AfDecimal getWrifNumQuo() {
        return readPackedAsDecimal(Pos.WRIF_NUM_QUO, Len.Int.WRIF_NUM_QUO, Len.Fract.WRIF_NUM_QUO);
    }

    public byte[] getWrifNumQuoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIF_NUM_QUO, Pos.WRIF_NUM_QUO);
        return buffer;
    }

    public void initWrifNumQuoSpaces() {
        fill(Pos.WRIF_NUM_QUO, Len.WRIF_NUM_QUO, Types.SPACE_CHAR);
    }

    public void setWrifNumQuoNull(String wrifNumQuoNull) {
        writeString(Pos.WRIF_NUM_QUO_NULL, wrifNumQuoNull, Len.WRIF_NUM_QUO_NULL);
    }

    /**Original name: WRIF-NUM-QUO-NULL<br>*/
    public String getWrifNumQuoNull() {
        return readString(Pos.WRIF_NUM_QUO_NULL, Len.WRIF_NUM_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIF_NUM_QUO = 1;
        public static final int WRIF_NUM_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIF_NUM_QUO = 7;
        public static final int WRIF_NUM_QUO_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRIF_NUM_QUO = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIF_NUM_QUO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
