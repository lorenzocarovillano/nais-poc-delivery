package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RAT-LRD<br>
 * Variable: WPAG-RAT-LRD from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRatLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRatLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RAT_LRD;
    }

    public void setWpagRatLrd(AfDecimal wpagRatLrd) {
        writeDecimalAsPacked(Pos.WPAG_RAT_LRD, wpagRatLrd.copy());
    }

    public void setWpagRatLrdFormatted(String wpagRatLrd) {
        setWpagRatLrd(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RAT_LRD + Len.Fract.WPAG_RAT_LRD, Len.Fract.WPAG_RAT_LRD, wpagRatLrd));
    }

    public void setWpagRatLrdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RAT_LRD, Pos.WPAG_RAT_LRD);
    }

    /**Original name: WPAG-RAT-LRD<br>*/
    public AfDecimal getWpagRatLrd() {
        return readPackedAsDecimal(Pos.WPAG_RAT_LRD, Len.Int.WPAG_RAT_LRD, Len.Fract.WPAG_RAT_LRD);
    }

    public byte[] getWpagRatLrdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RAT_LRD, Pos.WPAG_RAT_LRD);
        return buffer;
    }

    public void initWpagRatLrdSpaces() {
        fill(Pos.WPAG_RAT_LRD, Len.WPAG_RAT_LRD, Types.SPACE_CHAR);
    }

    public void setWpagRatLrdNull(String wpagRatLrdNull) {
        writeString(Pos.WPAG_RAT_LRD_NULL, wpagRatLrdNull, Len.WPAG_RAT_LRD_NULL);
    }

    /**Original name: WPAG-RAT-LRD-NULL<br>*/
    public String getWpagRatLrdNull() {
        return readString(Pos.WPAG_RAT_LRD_NULL, Len.WPAG_RAT_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RAT_LRD = 1;
        public static final int WPAG_RAT_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RAT_LRD = 8;
        public static final int WPAG_RAT_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RAT_LRD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RAT_LRD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
