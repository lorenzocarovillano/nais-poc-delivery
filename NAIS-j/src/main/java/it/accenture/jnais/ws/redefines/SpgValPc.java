package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: SPG-VAL-PC<br>
 * Variable: SPG-VAL-PC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SpgValPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public SpgValPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SPG_VAL_PC;
    }

    public void setSpgValPc(AfDecimal spgValPc) {
        writeDecimalAsPacked(Pos.SPG_VAL_PC, spgValPc.copy());
    }

    public void setSpgValPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SPG_VAL_PC, Pos.SPG_VAL_PC);
    }

    /**Original name: SPG-VAL-PC<br>*/
    public AfDecimal getSpgValPc() {
        return readPackedAsDecimal(Pos.SPG_VAL_PC, Len.Int.SPG_VAL_PC, Len.Fract.SPG_VAL_PC);
    }

    public byte[] getSpgValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.SPG_VAL_PC, Pos.SPG_VAL_PC);
        return buffer;
    }

    public void setSpgValPcNull(String spgValPcNull) {
        writeString(Pos.SPG_VAL_PC_NULL, spgValPcNull, Len.SPG_VAL_PC_NULL);
    }

    /**Original name: SPG-VAL-PC-NULL<br>*/
    public String getSpgValPcNull() {
        return readString(Pos.SPG_VAL_PC_NULL, Len.SPG_VAL_PC_NULL);
    }

    public String getSpgValPcNullFormatted() {
        return Functions.padBlanks(getSpgValPcNull(), Len.SPG_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SPG_VAL_PC = 1;
        public static final int SPG_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SPG_VAL_PC = 8;
        public static final int SPG_VAL_PC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SPG_VAL_PC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int SPG_VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
