package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-CAR-ACQ<br>
 * Variable: WOCO-CAR-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_CAR_ACQ;
    }

    public void setWocoCarAcq(AfDecimal wocoCarAcq) {
        writeDecimalAsPacked(Pos.WOCO_CAR_ACQ, wocoCarAcq.copy());
    }

    public void setWocoCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_CAR_ACQ, Pos.WOCO_CAR_ACQ);
    }

    /**Original name: WOCO-CAR-ACQ<br>*/
    public AfDecimal getWocoCarAcq() {
        return readPackedAsDecimal(Pos.WOCO_CAR_ACQ, Len.Int.WOCO_CAR_ACQ, Len.Fract.WOCO_CAR_ACQ);
    }

    public byte[] getWocoCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_CAR_ACQ, Pos.WOCO_CAR_ACQ);
        return buffer;
    }

    public void initWocoCarAcqSpaces() {
        fill(Pos.WOCO_CAR_ACQ, Len.WOCO_CAR_ACQ, Types.SPACE_CHAR);
    }

    public void setWocoCarAcqNull(String wocoCarAcqNull) {
        writeString(Pos.WOCO_CAR_ACQ_NULL, wocoCarAcqNull, Len.WOCO_CAR_ACQ_NULL);
    }

    /**Original name: WOCO-CAR-ACQ-NULL<br>*/
    public String getWocoCarAcqNull() {
        return readString(Pos.WOCO_CAR_ACQ_NULL, Len.WOCO_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_CAR_ACQ = 1;
        public static final int WOCO_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_CAR_ACQ = 8;
        public static final int WOCO_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
