package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-INCAS<br>
 * Variable: WPAG-RATA-INCAS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataIncas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataIncas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_INCAS;
    }

    public void setWpagRataIncas(AfDecimal wpagRataIncas) {
        writeDecimalAsPacked(Pos.WPAG_RATA_INCAS, wpagRataIncas.copy());
    }

    public void setWpagRataIncasFormatted(String wpagRataIncas) {
        setWpagRataIncas(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_INCAS + Len.Fract.WPAG_RATA_INCAS, Len.Fract.WPAG_RATA_INCAS, wpagRataIncas));
    }

    public void setWpagRataIncasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_INCAS, Pos.WPAG_RATA_INCAS);
    }

    /**Original name: WPAG-RATA-INCAS<br>*/
    public AfDecimal getWpagRataIncas() {
        return readPackedAsDecimal(Pos.WPAG_RATA_INCAS, Len.Int.WPAG_RATA_INCAS, Len.Fract.WPAG_RATA_INCAS);
    }

    public byte[] getWpagRataIncasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_INCAS, Pos.WPAG_RATA_INCAS);
        return buffer;
    }

    public void initWpagRataIncasSpaces() {
        fill(Pos.WPAG_RATA_INCAS, Len.WPAG_RATA_INCAS, Types.SPACE_CHAR);
    }

    public void setWpagRataIncasNull(String wpagRataIncasNull) {
        writeString(Pos.WPAG_RATA_INCAS_NULL, wpagRataIncasNull, Len.WPAG_RATA_INCAS_NULL);
    }

    /**Original name: WPAG-RATA-INCAS-NULL<br>*/
    public String getWpagRataIncasNull() {
        return readString(Pos.WPAG_RATA_INCAS_NULL, Len.WPAG_RATA_INCAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_INCAS = 1;
        public static final int WPAG_RATA_INCAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_INCAS = 8;
        public static final int WPAG_RATA_INCAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_INCAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_INCAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
