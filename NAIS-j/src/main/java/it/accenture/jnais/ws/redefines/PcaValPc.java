package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCA-VAL-PC<br>
 * Variable: PCA-VAL-PC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcaValPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcaValPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCA_VAL_PC;
    }

    public void setPcaValPc(AfDecimal pcaValPc) {
        writeDecimalAsPacked(Pos.PCA_VAL_PC, pcaValPc.copy());
    }

    public void setPcaValPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCA_VAL_PC, Pos.PCA_VAL_PC);
    }

    /**Original name: PCA-VAL-PC<br>*/
    public AfDecimal getPcaValPc() {
        return readPackedAsDecimal(Pos.PCA_VAL_PC, Len.Int.PCA_VAL_PC, Len.Fract.PCA_VAL_PC);
    }

    public byte[] getPcaValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCA_VAL_PC, Pos.PCA_VAL_PC);
        return buffer;
    }

    public void setPcaValPcNull(String pcaValPcNull) {
        writeString(Pos.PCA_VAL_PC_NULL, pcaValPcNull, Len.PCA_VAL_PC_NULL);
    }

    /**Original name: PCA-VAL-PC-NULL<br>*/
    public String getPcaValPcNull() {
        return readString(Pos.PCA_VAL_PC_NULL, Len.PCA_VAL_PC_NULL);
    }

    public String getPcaValPcNullFormatted() {
        return Functions.padBlanks(getPcaValPcNull(), Len.PCA_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCA_VAL_PC = 1;
        public static final int PCA_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCA_VAL_PC = 8;
        public static final int PCA_VAL_PC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCA_VAL_PC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCA_VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
