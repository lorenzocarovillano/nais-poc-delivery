package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-TS-TARI-SCON<br>
 * Variable: B03-TS-TARI-SCON from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03TsTariScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03TsTariScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_TS_TARI_SCON;
    }

    public void setB03TsTariScon(AfDecimal b03TsTariScon) {
        writeDecimalAsPacked(Pos.B03_TS_TARI_SCON, b03TsTariScon.copy());
    }

    public void setB03TsTariSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_TS_TARI_SCON, Pos.B03_TS_TARI_SCON);
    }

    /**Original name: B03-TS-TARI-SCON<br>*/
    public AfDecimal getB03TsTariScon() {
        return readPackedAsDecimal(Pos.B03_TS_TARI_SCON, Len.Int.B03_TS_TARI_SCON, Len.Fract.B03_TS_TARI_SCON);
    }

    public byte[] getB03TsTariSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_TS_TARI_SCON, Pos.B03_TS_TARI_SCON);
        return buffer;
    }

    public void setB03TsTariSconNull(String b03TsTariSconNull) {
        writeString(Pos.B03_TS_TARI_SCON_NULL, b03TsTariSconNull, Len.B03_TS_TARI_SCON_NULL);
    }

    /**Original name: B03-TS-TARI-SCON-NULL<br>*/
    public String getB03TsTariSconNull() {
        return readString(Pos.B03_TS_TARI_SCON_NULL, Len.B03_TS_TARI_SCON_NULL);
    }

    public String getB03TsTariSconNullFormatted() {
        return Functions.padBlanks(getB03TsTariSconNull(), Len.B03_TS_TARI_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_TS_TARI_SCON = 1;
        public static final int B03_TS_TARI_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_TS_TARI_SCON = 8;
        public static final int B03_TS_TARI_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_TS_TARI_SCON = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_TS_TARI_SCON = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
