package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-FRAZ-DFLT<br>
 * Variable: DAD-FRAZ-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadFrazDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadFrazDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_FRAZ_DFLT;
    }

    public void setDadFrazDflt(int dadFrazDflt) {
        writeIntAsPacked(Pos.DAD_FRAZ_DFLT, dadFrazDflt, Len.Int.DAD_FRAZ_DFLT);
    }

    public void setDadFrazDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_FRAZ_DFLT, Pos.DAD_FRAZ_DFLT);
    }

    /**Original name: DAD-FRAZ-DFLT<br>*/
    public int getDadFrazDflt() {
        return readPackedAsInt(Pos.DAD_FRAZ_DFLT, Len.Int.DAD_FRAZ_DFLT);
    }

    public byte[] getDadFrazDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_FRAZ_DFLT, Pos.DAD_FRAZ_DFLT);
        return buffer;
    }

    public void setDadFrazDfltNull(String dadFrazDfltNull) {
        writeString(Pos.DAD_FRAZ_DFLT_NULL, dadFrazDfltNull, Len.DAD_FRAZ_DFLT_NULL);
    }

    /**Original name: DAD-FRAZ-DFLT-NULL<br>*/
    public String getDadFrazDfltNull() {
        return readString(Pos.DAD_FRAZ_DFLT_NULL, Len.DAD_FRAZ_DFLT_NULL);
    }

    public String getDadFrazDfltNullFormatted() {
        return Functions.padBlanks(getDadFrazDfltNull(), Len.DAD_FRAZ_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_FRAZ_DFLT = 1;
        public static final int DAD_FRAZ_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_FRAZ_DFLT = 3;
        public static final int DAD_FRAZ_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_FRAZ_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
