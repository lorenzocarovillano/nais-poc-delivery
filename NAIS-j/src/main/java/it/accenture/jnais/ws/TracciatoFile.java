package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: TRACCIATO-FILE<br>
 * Variable: TRACCIATO-FILE from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TracciatoFile {

    //==== PROPERTIES ====
    //Original name: OUT-IB-OGG
    private String outIbOgg = DefaultValues.stringVal(Len.OUT_IB_OGG);
    //Original name: FILLER-1
    private char filler1 = DefaultValues.CHAR_VAL;
    //Original name: OUT-ERR
    private String outErr = DefaultValues.stringVal(Len.OUT_ERR);
    //Original name: FILLER-2
    private char filler2 = DefaultValues.CHAR_VAL;
    //Original name: WS-CAMPO-1
    private String wsCampo1 = DefaultValues.stringVal(Len.WS_CAMPO1);
    //Original name: FILLER-3
    private char filler3 = DefaultValues.CHAR_VAL;
    //Original name: WS-CAMPO-2
    private String wsCampo2 = DefaultValues.stringVal(Len.WS_CAMPO2);
    //Original name: FILLER-4
    private char filler4 = DefaultValues.CHAR_VAL;
    //Original name: WS-CAMPO-3
    private String wsCampo3 = DefaultValues.stringVal(Len.WS_CAMPO3);
    //Original name: FILLER-7
    private char filler7 = DefaultValues.CHAR_VAL;
    //Original name: WS-DATA-1
    private String wsData1 = DefaultValues.stringVal(Len.WS_DATA1);
    //Original name: FILLER-5
    private char filler5 = DefaultValues.CHAR_VAL;
    //Original name: WS-DATA-2
    private String wsData2 = DefaultValues.stringVal(Len.WS_DATA2);
    //Original name: FILLER-6
    private char filler6 = DefaultValues.CHAR_VAL;
    //Original name: WS-DATA-3
    private String wsData3 = DefaultValues.stringVal(Len.WS_DATA3);
    //Original name: FILLER-TRACCIATO-FILE
    private String flr1 = DefaultValues.stringVal(Len.FLR1);

    //==== METHODS ====
    public String getTracciatoFileFormatted() {
        return MarshalByteExt.bufferToStr(getTracciatoFileBytes());
    }

    public byte[] getTracciatoFileBytes() {
        byte[] buffer = new byte[Len.TRACCIATO_FILE];
        return getTracciatoFileBytes(buffer, 1);
    }

    public byte[] getTracciatoFileBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, outIbOgg, Len.OUT_IB_OGG);
        position += Len.OUT_IB_OGG;
        MarshalByte.writeChar(buffer, position, filler1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, outErr, Len.OUT_ERR);
        position += Len.OUT_ERR;
        MarshalByte.writeChar(buffer, position, filler2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wsCampo1, Len.WS_CAMPO1);
        position += Len.WS_CAMPO1;
        MarshalByte.writeChar(buffer, position, filler3);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wsCampo2, Len.WS_CAMPO2);
        position += Len.WS_CAMPO2;
        MarshalByte.writeChar(buffer, position, filler4);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wsCampo3, Len.WS_CAMPO3);
        position += Len.WS_CAMPO3;
        MarshalByte.writeChar(buffer, position, filler7);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wsData1, Len.WS_DATA1);
        position += Len.WS_DATA1;
        MarshalByte.writeChar(buffer, position, filler5);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wsData2, Len.WS_DATA2);
        position += Len.WS_DATA2;
        MarshalByte.writeChar(buffer, position, filler6);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wsData3, Len.WS_DATA3);
        position += Len.WS_DATA3;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        return buffer;
    }

    public void setOutIbOgg(String outIbOgg) {
        this.outIbOgg = Functions.subString(outIbOgg, Len.OUT_IB_OGG);
    }

    public String getOutIbOgg() {
        return this.outIbOgg;
    }

    public void setFiller1(char filler1) {
        this.filler1 = filler1;
    }

    public void setFiller1Formatted(String filler1) {
        setFiller1(Functions.charAt(filler1, Types.CHAR_SIZE));
    }

    public char getFiller1() {
        return this.filler1;
    }

    public void setOutErr(String outErr) {
        this.outErr = Functions.subString(outErr, Len.OUT_ERR);
    }

    public String getOutErr() {
        return this.outErr;
    }

    public void setFiller2(char filler2) {
        this.filler2 = filler2;
    }

    public void setFiller2Formatted(String filler2) {
        setFiller2(Functions.charAt(filler2, Types.CHAR_SIZE));
    }

    public char getFiller2() {
        return this.filler2;
    }

    public void setWsCampo1(AfDecimal wsCampo1) {
        this.wsCampo1 = PicFormatter.display("9(12).9(3)").format(wsCampo1.copy()).toString();
    }

    public AfDecimal getWsCampo1() {
        return PicParser.display("9(12).9(3)").parseDecimal(Len.Int.WS_CAMPO1 + Len.Fract.WS_CAMPO1, Len.Fract.WS_CAMPO1, this.wsCampo1);
    }

    public void setFiller3(char filler3) {
        this.filler3 = filler3;
    }

    public void setFiller3Formatted(String filler3) {
        setFiller3(Functions.charAt(filler3, Types.CHAR_SIZE));
    }

    public char getFiller3() {
        return this.filler3;
    }

    public void setWsCampo2(AfDecimal wsCampo2) {
        this.wsCampo2 = PicFormatter.display("9(12).9(3)").format(wsCampo2.copy()).toString();
    }

    public AfDecimal getWsCampo2() {
        return PicParser.display("9(12).9(3)").parseDecimal(Len.Int.WS_CAMPO2 + Len.Fract.WS_CAMPO2, Len.Fract.WS_CAMPO2, this.wsCampo2);
    }

    public void setFiller4(char filler4) {
        this.filler4 = filler4;
    }

    public void setFiller4Formatted(String filler4) {
        setFiller4(Functions.charAt(filler4, Types.CHAR_SIZE));
    }

    public char getFiller4() {
        return this.filler4;
    }

    public void setWsCampo3(AfDecimal wsCampo3) {
        this.wsCampo3 = PicFormatter.display("9(12).9(3)").format(wsCampo3.copy()).toString();
    }

    public AfDecimal getWsCampo3() {
        return PicParser.display("9(12).9(3)").parseDecimal(Len.Int.WS_CAMPO3 + Len.Fract.WS_CAMPO3, Len.Fract.WS_CAMPO3, this.wsCampo3);
    }

    public void setFiller7(char filler7) {
        this.filler7 = filler7;
    }

    public void setFiller7Formatted(String filler7) {
        setFiller7(Functions.charAt(filler7, Types.CHAR_SIZE));
    }

    public char getFiller7() {
        return this.filler7;
    }

    public void setWsData1(String wsData1) {
        this.wsData1 = Functions.subString(wsData1, Len.WS_DATA1);
    }

    public String getWsData1() {
        return this.wsData1;
    }

    public void setFiller5(char filler5) {
        this.filler5 = filler5;
    }

    public void setFiller5Formatted(String filler5) {
        setFiller5(Functions.charAt(filler5, Types.CHAR_SIZE));
    }

    public char getFiller5() {
        return this.filler5;
    }

    public void setWsData2(String wsData2) {
        this.wsData2 = Functions.subString(wsData2, Len.WS_DATA2);
    }

    public String getWsData2() {
        return this.wsData2;
    }

    public void setFiller6(char filler6) {
        this.filler6 = filler6;
    }

    public void setFiller6Formatted(String filler6) {
        setFiller6(Functions.charAt(filler6, Types.CHAR_SIZE));
    }

    public char getFiller6() {
        return this.filler6;
    }

    public void setWsData3(String wsData3) {
        this.wsData3 = Functions.subString(wsData3, Len.WS_DATA3);
    }

    public String getWsData3() {
        return this.wsData3;
    }

    public String getFlr1() {
        return this.flr1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OUT_IB_OGG = 20;
        public static final int OUT_ERR = 185;
        public static final int WS_CAMPO1 = 16;
        public static final int WS_CAMPO2 = 16;
        public static final int WS_CAMPO3 = 16;
        public static final int WS_DATA1 = 8;
        public static final int WS_DATA2 = 8;
        public static final int WS_DATA3 = 8;
        public static final int FLR1 = 2;
        public static final int FILLER1 = 1;
        public static final int FILLER2 = 1;
        public static final int FILLER3 = 1;
        public static final int FILLER4 = 1;
        public static final int FILLER7 = 1;
        public static final int FILLER5 = 1;
        public static final int FILLER6 = 1;
        public static final int TRACCIATO_FILE = OUT_IB_OGG + FILLER1 + OUT_ERR + FILLER2 + WS_CAMPO1 + FILLER3 + WS_CAMPO2 + FILLER4 + WS_CAMPO3 + FILLER7 + WS_DATA1 + FILLER5 + WS_DATA2 + FILLER6 + WS_DATA3 + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WS_CAMPO1 = 12;
            public static final int WS_CAMPO2 = 12;
            public static final int WS_CAMPO3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WS_CAMPO1 = 3;
            public static final int WS_CAMPO2 = 3;
            public static final int WS_CAMPO3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
