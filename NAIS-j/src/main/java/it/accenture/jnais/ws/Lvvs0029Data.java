package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.WkTgaMax;
import it.accenture.jnais.ws.enums.FlagAreaTrovata;
import it.accenture.jnais.ws.enums.FlagStrutturaTrovata;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0029<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0029Data {

    //==== PROPERTIES ====
    public static final int WTGA_TAB_TRAN_MAXOCCURS = 1250;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     PROGRAMMI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0029";
    //Original name: PGM-LVVS0000
    private String pgmLvvs0000 = "LVVS0000";
    //Original name: WK-DATA-INPUT
    private WkDataInput wkDataInput = new WkDataInput();
    //Original name: IND-STR
    private short indStr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA
    private short indTga = DefaultValues.BIN_SHORT_VAL;
    //Original name: ALIAS-TRANCHE
    private String aliasTranche = "TGA";
    //Original name: FLAG-AREA-TROVATA
    private FlagAreaTrovata flagAreaTrovata = new FlagAreaTrovata();
    //Original name: FLAG-STRUTTURA-TROVATA
    private FlagStrutturaTrovata flagStrutturaTrovata = new FlagStrutturaTrovata();
    //Original name: WTGA-ELE-TRAN-MAX
    private short wtgaEleTranMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTGA-TAB-TRAN
    private LazyArrayCopy<W1tgaTabTran> wtgaTabTran = new LazyArrayCopy<W1tgaTabTran>(new W1tgaTabTran(), 1, WTGA_TAB_TRAN_MAXOCCURS);
    //Original name: WK-TGA-MAX
    private WkTgaMax wkTgaMax = new WkTgaMax();
    //Original name: INPUT-LVVS0000
    private InputLvvs0000 inputLvvs0000 = new InputLvvs0000();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getPgmLvvs0000() {
        return this.pgmLvvs0000;
    }

    public void setIndStr(short indStr) {
        this.indStr = indStr;
    }

    public short getIndStr() {
        return this.indStr;
    }

    public void setIndTga(short indTga) {
        this.indTga = indTga;
    }

    public short getIndTga() {
        return this.indTga;
    }

    public String getAliasTranche() {
        return this.aliasTranche;
    }

    public String getAliasTrancheFormatted() {
        return Functions.padBlanks(getAliasTranche(), Len.ALIAS_TRANCHE);
    }

    public void setWtgaAreaTrancheFormatted(String data) {
        byte[] buffer = new byte[Len.WTGA_AREA_TRANCHE];
        MarshalByte.writeString(buffer, 1, data, Len.WTGA_AREA_TRANCHE);
        setWtgaAreaTrancheBytes(buffer, 1);
    }

    public void setWtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        wtgaEleTranMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTGA_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wtgaTabTran.get(idx - 1).setW1tgaTabTranBytes(buffer, position);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN;
            }
            else {
                W1tgaTabTran temp_wtgaTabTran = new W1tgaTabTran();
                temp_wtgaTabTran.initW1tgaTabTranSpaces();
                getWtgaTabTranObj().fill(temp_wtgaTabTran);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN * (WTGA_TAB_TRAN_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public void setWtgaEleTranMax(short wtgaEleTranMax) {
        this.wtgaEleTranMax = wtgaEleTranMax;
    }

    public short getWtgaEleTranMax() {
        return this.wtgaEleTranMax;
    }

    public FlagAreaTrovata getFlagAreaTrovata() {
        return flagAreaTrovata;
    }

    public FlagStrutturaTrovata getFlagStrutturaTrovata() {
        return flagStrutturaTrovata;
    }

    public InputLvvs0000 getInputLvvs0000() {
        return inputLvvs0000;
    }

    public WkDataInput getWkDataInput() {
        return wkDataInput;
    }

    public WkTgaMax getWkTgaMax() {
        return wkTgaMax;
    }

    public W1tgaTabTran getWtgaTabTran(int idx) {
        return wtgaTabTran.get(idx - 1);
    }

    public LazyArrayCopy<W1tgaTabTran> getWtgaTabTranObj() {
        return wtgaTabTran;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ALIAS_TRANCHE = 3;
        public static final int WTGA_ELE_TRAN_MAX = 2;
        public static final int WTGA_AREA_TRANCHE = WTGA_ELE_TRAN_MAX + Lvvs0029Data.WTGA_TAB_TRAN_MAXOCCURS * W1tgaTabTran.Len.W1TGA_TAB_TRAN;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
