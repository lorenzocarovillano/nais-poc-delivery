package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Ivvc0211DatiInput;
import it.accenture.jnais.copy.Ivvc0211RestoDati;
import it.accenture.jnais.ws.redefines.Ivvc0211TabInfo1;

/**Original name: INPUT-IVVS0211<br>
 * Variable: INPUT-IVVS0211 from program IVVS0211<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class InputIvvs0211 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IVVC0211-DATI-INPUT
    private Ivvc0211DatiInput ivvc0211DatiInput = new Ivvc0211DatiInput();
    //Original name: IVVC0211-TAB-INFO1
    private Ivvc0211TabInfo1 ivvc0211TabInfo1 = new Ivvc0211TabInfo1();
    //Original name: IVVC0211-RESTO-DATI
    private Ivvc0211RestoDati ivvc0211RestoDati = new Ivvc0211RestoDati();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.INPUT_IVVS0211;
    }

    @Override
    public void deserialize(byte[] buf) {
        setInputIvvs0211Bytes(buf);
    }

    public String getAreaIoIvvs0211Formatted() {
        return MarshalByteExt.bufferToStr(getInputIvvs0211Bytes());
    }

    public void setInputIvvs0211Bytes(byte[] buffer) {
        setInputIvvs0211Bytes(buffer, 1);
    }

    public byte[] getInputIvvs0211Bytes() {
        byte[] buffer = new byte[Len.INPUT_IVVS0211];
        return getInputIvvs0211Bytes(buffer, 1);
    }

    public void setInputIvvs0211Bytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0211DatiInput.setIvvc0211DatiInputBytes(buffer, position);
        position += Ivvc0211DatiInput.Len.IVVC0211_DATI_INPUT;
        ivvc0211TabInfo1.setIvvc0211TabInfo1Bytes(buffer, position);
        position += Ivvc0211TabInfo1.Len.IVVC0211_TAB_INFO1;
        ivvc0211RestoDati.setIvvc0211RestoDatiBytes(buffer, position);
    }

    public byte[] getInputIvvs0211Bytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0211DatiInput.getIvvc0211DatiInputBytes(buffer, position);
        position += Ivvc0211DatiInput.Len.IVVC0211_DATI_INPUT;
        ivvc0211TabInfo1.getIvvc0211TabInfo1Bytes(buffer, position);
        position += Ivvc0211TabInfo1.Len.IVVC0211_TAB_INFO1;
        ivvc0211RestoDati.getIvvc0211RestoDatiBytes(buffer, position);
        return buffer;
    }

    public Ivvc0211DatiInput getIvvc0211DatiInput() {
        return ivvc0211DatiInput;
    }

    public Ivvc0211RestoDati getIvvc0211RestoDati() {
        return ivvc0211RestoDati;
    }

    public Ivvc0211TabInfo1 getIvvc0211TabInfo1() {
        return ivvc0211TabInfo1;
    }

    @Override
    public byte[] serialize() {
        return getInputIvvs0211Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INPUT_IVVS0211 = Ivvc0211DatiInput.Len.IVVC0211_DATI_INPUT + Ivvc0211TabInfo1.Len.IVVC0211_TAB_INFO1 + Ivvc0211RestoDati.Len.IVVC0211_RESTO_DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
