package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Ispv0000;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvpol1Lvvs0002;
import it.accenture.jnais.copy.Ldbv1421;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.FineCiclo;
import it.accenture.jnais.ws.enums.FineGar;
import it.accenture.jnais.ws.enums.FlagComunTrov;
import it.accenture.jnais.ws.enums.FlagCurMov;
import it.accenture.jnais.ws.enums.FlagRamoI;
import it.accenture.jnais.ws.enums.FlagRamoIiiV;
import it.accenture.jnais.ws.enums.FlagUltimaLettura;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0037;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS2780<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs2780Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS2780";
    //Original name: LDBS1420
    private String ldbs1420 = "LDBS1420";
    //Original name: IDBSP580
    private String idbsp580 = "IDBSP580";
    //Original name: LDBS6040
    private String ldbs6040 = "LDBS6040";
    //Original name: FINE-CICLO
    private FineCiclo fineCiclo = new FineCiclo();
    //Original name: FINE-GAR
    private FineGar fineGar = new FineGar();
    //Original name: FLAG-RAMO-III-V
    private FlagRamoIiiV flagRamoIiiV = new FlagRamoIiiV();
    //Original name: FLAG-RAMO-I
    private FlagRamoI flagRamoI = new FlagRamoI();
    //Original name: FLAG-COMUN-TROV
    private FlagComunTrov flagComunTrov = new FlagComunTrov();
    //Original name: FLAG-CUR-MOV
    private FlagCurMov flagCurMov = new FlagCurMov();
    //Original name: FLAG-ULTIMA-LETTURA
    private FlagUltimaLettura flagUltimaLettura = new FlagUltimaLettura();
    //Original name: WK-VAR-MOVI-COMUN
    private WkVarMoviComun wkVarMoviComun = new WkVarMoviComun();
    //Original name: GAR
    private Gar gar = new Gar();
    //Original name: IMPST-BOLLO
    private ImpstBollo impstBollo = new ImpstBollo();
    //Original name: MOVI
    private MoviLdbs1530 movi = new MoviLdbs1530();
    //Original name: LDBV1421
    private Ldbv1421 ldbv1421 = new Ldbv1421();
    //Original name: ISPV0000
    private Ispv0000 ispv0000 = new Ispv0000();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0037 wsMovimento = new WsMovimentoLvvs0037();
    //Original name: DPOL-ELE-POLI-MAX
    private short dpolElePoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1Lvvs0002 lccvpol1 = new Lccvpol1Lvvs0002();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ISO
    private short ixTabIso = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getLdbs1420() {
        return this.ldbs1420;
    }

    public String getIdbsp580() {
        return this.idbsp580;
    }

    public String getLdbs6040() {
        return this.ldbs6040;
    }

    public void setDpolAreaPoliFormatted(String data) {
        byte[] buffer = new byte[Len.DPOL_AREA_POLI];
        MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POLI);
        setDpolAreaPoliBytes(buffer, 1);
    }

    public void setDpolAreaPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        dpolElePoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDpolTabPoliBytes(buffer, position);
    }

    public void setDpolElePoliMax(short dpolElePoliMax) {
        this.dpolElePoliMax = dpolElePoliMax;
    }

    public short getDpolElePoliMax() {
        return this.dpolElePoliMax;
    }

    public void setDpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
        position += Lccvpol1Lvvs0002.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabIso(short ixTabIso) {
        this.ixTabIso = ixTabIso;
    }

    public short getIxTabIso() {
        return this.ixTabIso;
    }

    public FineCiclo getFineCiclo() {
        return fineCiclo;
    }

    public FineGar getFineGar() {
        return fineGar;
    }

    public FlagComunTrov getFlagComunTrov() {
        return flagComunTrov;
    }

    public FlagCurMov getFlagCurMov() {
        return flagCurMov;
    }

    public FlagRamoI getFlagRamoI() {
        return flagRamoI;
    }

    public FlagRamoIiiV getFlagRamoIiiV() {
        return flagRamoIiiV;
    }

    public FlagUltimaLettura getFlagUltimaLettura() {
        return flagUltimaLettura;
    }

    public Gar getGar() {
        return gar;
    }

    public ImpstBollo getImpstBollo() {
        return impstBollo;
    }

    public Ispv0000 getIspv0000() {
        return ispv0000;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvpol1Lvvs0002 getLccvpol1() {
        return lccvpol1;
    }

    public Ldbv1421 getLdbv1421() {
        return ldbv1421;
    }

    public MoviLdbs1530 getMovi() {
        return movi;
    }

    public WkVarMoviComun getWkVarMoviComun() {
        return wkVarMoviComun;
    }

    public WsMovimentoLvvs0037 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_DATA = 8;
        public static final int DPOL_ELE_POLI_MAX = 2;
        public static final int DPOL_TAB_POLI = WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int DPOL_AREA_POLI = DPOL_ELE_POLI_MAX + DPOL_TAB_POLI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
