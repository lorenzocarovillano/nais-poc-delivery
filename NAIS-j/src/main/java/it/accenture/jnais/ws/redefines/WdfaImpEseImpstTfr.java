package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMP-ESE-IMPST-TFR<br>
 * Variable: WDFA-IMP-ESE-IMPST-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpEseImpstTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpEseImpstTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMP_ESE_IMPST_TFR;
    }

    public void setWdfaImpEseImpstTfr(AfDecimal wdfaImpEseImpstTfr) {
        writeDecimalAsPacked(Pos.WDFA_IMP_ESE_IMPST_TFR, wdfaImpEseImpstTfr.copy());
    }

    public void setWdfaImpEseImpstTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMP_ESE_IMPST_TFR, Pos.WDFA_IMP_ESE_IMPST_TFR);
    }

    /**Original name: WDFA-IMP-ESE-IMPST-TFR<br>*/
    public AfDecimal getWdfaImpEseImpstTfr() {
        return readPackedAsDecimal(Pos.WDFA_IMP_ESE_IMPST_TFR, Len.Int.WDFA_IMP_ESE_IMPST_TFR, Len.Fract.WDFA_IMP_ESE_IMPST_TFR);
    }

    public byte[] getWdfaImpEseImpstTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMP_ESE_IMPST_TFR, Pos.WDFA_IMP_ESE_IMPST_TFR);
        return buffer;
    }

    public void setWdfaImpEseImpstTfrNull(String wdfaImpEseImpstTfrNull) {
        writeString(Pos.WDFA_IMP_ESE_IMPST_TFR_NULL, wdfaImpEseImpstTfrNull, Len.WDFA_IMP_ESE_IMPST_TFR_NULL);
    }

    /**Original name: WDFA-IMP-ESE-IMPST-TFR-NULL<br>*/
    public String getWdfaImpEseImpstTfrNull() {
        return readString(Pos.WDFA_IMP_ESE_IMPST_TFR_NULL, Len.WDFA_IMP_ESE_IMPST_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_ESE_IMPST_TFR = 1;
        public static final int WDFA_IMP_ESE_IMPST_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_ESE_IMPST_TFR = 8;
        public static final int WDFA_IMP_ESE_IMPST_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_ESE_IMPST_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_ESE_IMPST_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
