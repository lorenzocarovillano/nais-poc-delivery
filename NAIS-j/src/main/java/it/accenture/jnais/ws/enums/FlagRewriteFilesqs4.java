package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-REWRITE-FILESQS4<br>
 * Variable: FLAG-REWRITE-FILESQS4 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagRewriteFilesqs4 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagRewriteFilesqs4(char flagRewriteFilesqs4) {
        this.value = flagRewriteFilesqs4;
    }

    public char getFlagRewriteFilesqs4() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setNo() {
        value = NO;
    }
}
