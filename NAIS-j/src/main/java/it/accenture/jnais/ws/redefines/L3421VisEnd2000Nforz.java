package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-VIS-END2000-NFORZ<br>
 * Variable: L3421-VIS-END2000-NFORZ from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421VisEnd2000Nforz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421VisEnd2000Nforz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_VIS_END2000_NFORZ;
    }

    public void setL3421VisEnd2000NforzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_VIS_END2000_NFORZ, Pos.L3421_VIS_END2000_NFORZ);
    }

    /**Original name: L3421-VIS-END2000-NFORZ<br>*/
    public AfDecimal getL3421VisEnd2000Nforz() {
        return readPackedAsDecimal(Pos.L3421_VIS_END2000_NFORZ, Len.Int.L3421_VIS_END2000_NFORZ, Len.Fract.L3421_VIS_END2000_NFORZ);
    }

    public byte[] getL3421VisEnd2000NforzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_VIS_END2000_NFORZ, Pos.L3421_VIS_END2000_NFORZ);
        return buffer;
    }

    /**Original name: L3421-VIS-END2000-NFORZ-NULL<br>*/
    public String getL3421VisEnd2000NforzNull() {
        return readString(Pos.L3421_VIS_END2000_NFORZ_NULL, Len.L3421_VIS_END2000_NFORZ_NULL);
    }

    public String getL3421VisEnd2000NforzNullFormatted() {
        return Functions.padBlanks(getL3421VisEnd2000NforzNull(), Len.L3421_VIS_END2000_NFORZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_VIS_END2000_NFORZ = 1;
        public static final int L3421_VIS_END2000_NFORZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_VIS_END2000_NFORZ = 8;
        public static final int L3421_VIS_END2000_NFORZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_VIS_END2000_NFORZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_VIS_END2000_NFORZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
