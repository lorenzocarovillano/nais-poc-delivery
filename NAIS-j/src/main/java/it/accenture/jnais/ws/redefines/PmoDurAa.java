package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-DUR-AA<br>
 * Variable: PMO-DUR-AA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoDurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoDurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_DUR_AA;
    }

    public void setPmoDurAa(int pmoDurAa) {
        writeIntAsPacked(Pos.PMO_DUR_AA, pmoDurAa, Len.Int.PMO_DUR_AA);
    }

    public void setPmoDurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_DUR_AA, Pos.PMO_DUR_AA);
    }

    /**Original name: PMO-DUR-AA<br>*/
    public int getPmoDurAa() {
        return readPackedAsInt(Pos.PMO_DUR_AA, Len.Int.PMO_DUR_AA);
    }

    public byte[] getPmoDurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_DUR_AA, Pos.PMO_DUR_AA);
        return buffer;
    }

    public void initPmoDurAaHighValues() {
        fill(Pos.PMO_DUR_AA, Len.PMO_DUR_AA, Types.HIGH_CHAR_VAL);
    }

    public void setPmoDurAaNull(String pmoDurAaNull) {
        writeString(Pos.PMO_DUR_AA_NULL, pmoDurAaNull, Len.PMO_DUR_AA_NULL);
    }

    /**Original name: PMO-DUR-AA-NULL<br>*/
    public String getPmoDurAaNull() {
        return readString(Pos.PMO_DUR_AA_NULL, Len.PMO_DUR_AA_NULL);
    }

    public String getPmoDurAaNullFormatted() {
        return Functions.padBlanks(getPmoDurAaNull(), Len.PMO_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_DUR_AA = 1;
        public static final int PMO_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_DUR_AA = 3;
        public static final int PMO_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
