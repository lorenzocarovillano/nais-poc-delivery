package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IAS-PNL<br>
 * Variable: S089-TOT-IAS-PNL from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotIasPnl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotIasPnl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IAS_PNL;
    }

    public void setWlquTotIasPnl(AfDecimal wlquTotIasPnl) {
        writeDecimalAsPacked(Pos.S089_TOT_IAS_PNL, wlquTotIasPnl.copy());
    }

    public void setWlquTotIasPnlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IAS_PNL, Pos.S089_TOT_IAS_PNL);
    }

    /**Original name: WLQU-TOT-IAS-PNL<br>*/
    public AfDecimal getWlquTotIasPnl() {
        return readPackedAsDecimal(Pos.S089_TOT_IAS_PNL, Len.Int.WLQU_TOT_IAS_PNL, Len.Fract.WLQU_TOT_IAS_PNL);
    }

    public byte[] getWlquTotIasPnlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IAS_PNL, Pos.S089_TOT_IAS_PNL);
        return buffer;
    }

    public void initWlquTotIasPnlSpaces() {
        fill(Pos.S089_TOT_IAS_PNL, Len.S089_TOT_IAS_PNL, Types.SPACE_CHAR);
    }

    public void setWlquTotIasPnlNull(String wlquTotIasPnlNull) {
        writeString(Pos.S089_TOT_IAS_PNL_NULL, wlquTotIasPnlNull, Len.WLQU_TOT_IAS_PNL_NULL);
    }

    /**Original name: WLQU-TOT-IAS-PNL-NULL<br>*/
    public String getWlquTotIasPnlNull() {
        return readString(Pos.S089_TOT_IAS_PNL_NULL, Len.WLQU_TOT_IAS_PNL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IAS_PNL = 1;
        public static final int S089_TOT_IAS_PNL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IAS_PNL = 8;
        public static final int WLQU_TOT_IAS_PNL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IAS_PNL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IAS_PNL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
