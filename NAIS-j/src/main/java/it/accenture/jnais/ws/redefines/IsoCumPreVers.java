package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-CUM-PRE-VERS<br>
 * Variable: ISO-CUM-PRE-VERS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoCumPreVers extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoCumPreVers() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_CUM_PRE_VERS;
    }

    public void setIsoCumPreVers(AfDecimal isoCumPreVers) {
        writeDecimalAsPacked(Pos.ISO_CUM_PRE_VERS, isoCumPreVers.copy());
    }

    public void setIsoCumPreVersFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_CUM_PRE_VERS, Pos.ISO_CUM_PRE_VERS);
    }

    /**Original name: ISO-CUM-PRE-VERS<br>*/
    public AfDecimal getIsoCumPreVers() {
        return readPackedAsDecimal(Pos.ISO_CUM_PRE_VERS, Len.Int.ISO_CUM_PRE_VERS, Len.Fract.ISO_CUM_PRE_VERS);
    }

    public byte[] getIsoCumPreVersAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_CUM_PRE_VERS, Pos.ISO_CUM_PRE_VERS);
        return buffer;
    }

    public void setIsoCumPreVersNull(String isoCumPreVersNull) {
        writeString(Pos.ISO_CUM_PRE_VERS_NULL, isoCumPreVersNull, Len.ISO_CUM_PRE_VERS_NULL);
    }

    /**Original name: ISO-CUM-PRE-VERS-NULL<br>*/
    public String getIsoCumPreVersNull() {
        return readString(Pos.ISO_CUM_PRE_VERS_NULL, Len.ISO_CUM_PRE_VERS_NULL);
    }

    public String getIsoCumPreVersNullFormatted() {
        return Functions.padBlanks(getIsoCumPreVersNull(), Len.ISO_CUM_PRE_VERS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_CUM_PRE_VERS = 1;
        public static final int ISO_CUM_PRE_VERS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_CUM_PRE_VERS = 8;
        public static final int ISO_CUM_PRE_VERS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_CUM_PRE_VERS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISO_CUM_PRE_VERS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
