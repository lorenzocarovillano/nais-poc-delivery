package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-COMMIS-INTER<br>
 * Variable: WTGA-COMMIS-INTER from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_COMMIS_INTER;
    }

    public void setWtgaCommisInter(AfDecimal wtgaCommisInter) {
        writeDecimalAsPacked(Pos.WTGA_COMMIS_INTER, wtgaCommisInter.copy());
    }

    public void setWtgaCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_COMMIS_INTER, Pos.WTGA_COMMIS_INTER);
    }

    /**Original name: WTGA-COMMIS-INTER<br>*/
    public AfDecimal getWtgaCommisInter() {
        return readPackedAsDecimal(Pos.WTGA_COMMIS_INTER, Len.Int.WTGA_COMMIS_INTER, Len.Fract.WTGA_COMMIS_INTER);
    }

    public byte[] getWtgaCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_COMMIS_INTER, Pos.WTGA_COMMIS_INTER);
        return buffer;
    }

    public void initWtgaCommisInterSpaces() {
        fill(Pos.WTGA_COMMIS_INTER, Len.WTGA_COMMIS_INTER, Types.SPACE_CHAR);
    }

    public void setWtgaCommisInterNull(String wtgaCommisInterNull) {
        writeString(Pos.WTGA_COMMIS_INTER_NULL, wtgaCommisInterNull, Len.WTGA_COMMIS_INTER_NULL);
    }

    /**Original name: WTGA-COMMIS-INTER-NULL<br>*/
    public String getWtgaCommisInterNull() {
        return readString(Pos.WTGA_COMMIS_INTER_NULL, Len.WTGA_COMMIS_INTER_NULL);
    }

    public String getWtgaCommisInterNullFormatted() {
        return Functions.padBlanks(getWtgaCommisInterNull(), Len.WTGA_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_COMMIS_INTER = 1;
        public static final int WTGA_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_COMMIS_INTER = 8;
        public static final int WTGA_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
