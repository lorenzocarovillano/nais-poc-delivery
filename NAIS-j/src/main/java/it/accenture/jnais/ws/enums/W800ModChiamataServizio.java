package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: W800-MOD-CHIAMATA-SERVIZIO<br>
 * Variable: W800-MOD-CHIAMATA-SERVIZIO from copybook LOAC0800<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class W800ModChiamataServizio {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.MOD_CHIAMATA_SERVIZIO);
    public static final String CON_CALCOLO = "SI";
    public static final String SENZA_CALCOLO = "NO";

    //==== METHODS ====
    public void setModChiamataServizio(String modChiamataServizio) {
        this.value = Functions.subString(modChiamataServizio, Len.MOD_CHIAMATA_SERVIZIO);
    }

    public String getModChiamataServizio() {
        return this.value;
    }

    public String getModChiamataServizioFormatted() {
        return Functions.padBlanks(getModChiamataServizio(), Len.MOD_CHIAMATA_SERVIZIO);
    }

    public boolean isConCalcolo() {
        return value.equals(CON_CALCOLO);
    }

    public boolean isSenzaCalcolo() {
        return value.equals(SENZA_CALCOLO);
    }

    public void setW800SenzaCalcolo() {
        value = SENZA_CALCOLO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MOD_CHIAMATA_SERVIZIO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
