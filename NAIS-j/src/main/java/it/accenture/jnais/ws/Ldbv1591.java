package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LDBV1591<br>
 * Variable: LDBV1591 from copybook LDBV1591<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv1591 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV1591-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LDBV1591-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV1591-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV1591-DT-DECOR
    private int dtDecor = DefaultValues.INT_VAL;
    //Original name: LDBV1591-TP-STAT-TIT
    private String tpStatTit = DefaultValues.stringVal(Len.TP_STAT_TIT);
    //Original name: LDBV1591-IMP-TOT
    private AfDecimal impTot = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV1591;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv1591Bytes(buf);
    }

    public String getLdbv1591Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv1591Bytes());
    }

    public void setLdbv1591Bytes(byte[] buffer) {
        setLdbv1591Bytes(buffer, 1);
    }

    public byte[] getLdbv1591Bytes() {
        byte[] buffer = new byte[Len.LDBV1591];
        return getLdbv1591Bytes(buffer, 1);
    }

    public void setLdbv1591Bytes(byte[] buffer, int offset) {
        int position = offset;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        dtDecor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DECOR, 0);
        position += Len.DT_DECOR;
        tpStatTit = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT);
        position += Len.TP_STAT_TIT;
        impTot.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_TOT, Len.Fract.IMP_TOT));
    }

    public byte[] getLdbv1591Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, dtDecor, Len.Int.DT_DECOR, 0);
        position += Len.DT_DECOR;
        MarshalByte.writeString(buffer, position, tpStatTit, Len.TP_STAT_TIT);
        position += Len.TP_STAT_TIT;
        MarshalByte.writeDecimalAsPacked(buffer, position, impTot.copy());
        return buffer;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setDtDecor(int dtDecor) {
        this.dtDecor = dtDecor;
    }

    public int getDtDecor() {
        return this.dtDecor;
    }

    public void setTpStatTit(String tpStatTit) {
        this.tpStatTit = Functions.subString(tpStatTit, Len.TP_STAT_TIT);
    }

    public String getTpStatTit() {
        return this.tpStatTit;
    }

    public void setImpTot(AfDecimal impTot) {
        this.impTot.assign(impTot);
    }

    public AfDecimal getImpTot() {
        return this.impTot.copy();
    }

    @Override
    public byte[] serialize() {
        return getLdbv1591Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_ADES = 5;
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int DT_DECOR = 5;
        public static final int TP_STAT_TIT = 2;
        public static final int IMP_TOT = 8;
        public static final int LDBV1591 = ID_ADES + ID_OGG + TP_OGG + DT_DECOR + TP_STAT_TIT + IMP_TOT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_ADES = 9;
            public static final int ID_OGG = 9;
            public static final int DT_DECOR = 8;
            public static final int IMP_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMP_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
