package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCA-VAL-IMP<br>
 * Variable: PCA-VAL-IMP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcaValImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcaValImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCA_VAL_IMP;
    }

    public void setPcaValImp(AfDecimal pcaValImp) {
        writeDecimalAsPacked(Pos.PCA_VAL_IMP, pcaValImp.copy());
    }

    public void setPcaValImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCA_VAL_IMP, Pos.PCA_VAL_IMP);
    }

    /**Original name: PCA-VAL-IMP<br>*/
    public AfDecimal getPcaValImp() {
        return readPackedAsDecimal(Pos.PCA_VAL_IMP, Len.Int.PCA_VAL_IMP, Len.Fract.PCA_VAL_IMP);
    }

    public byte[] getPcaValImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCA_VAL_IMP, Pos.PCA_VAL_IMP);
        return buffer;
    }

    public void setPcaValImpNull(String pcaValImpNull) {
        writeString(Pos.PCA_VAL_IMP_NULL, pcaValImpNull, Len.PCA_VAL_IMP_NULL);
    }

    /**Original name: PCA-VAL-IMP-NULL<br>*/
    public String getPcaValImpNull() {
        return readString(Pos.PCA_VAL_IMP_NULL, Len.PCA_VAL_IMP_NULL);
    }

    public String getPcaValImpNullFormatted() {
        return Functions.padBlanks(getPcaValImpNull(), Len.PCA_VAL_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCA_VAL_IMP = 1;
        public static final int PCA_VAL_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCA_VAL_IMP = 8;
        public static final int PCA_VAL_IMP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCA_VAL_IMP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCA_VAL_IMP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
