package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-IMP-TFR<br>
 * Variable: TDR-IMP-TFR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_IMP_TFR;
    }

    public void setTdrImpTfr(AfDecimal tdrImpTfr) {
        writeDecimalAsPacked(Pos.TDR_IMP_TFR, tdrImpTfr.copy());
    }

    public void setTdrImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_IMP_TFR, Pos.TDR_IMP_TFR);
    }

    /**Original name: TDR-IMP-TFR<br>*/
    public AfDecimal getTdrImpTfr() {
        return readPackedAsDecimal(Pos.TDR_IMP_TFR, Len.Int.TDR_IMP_TFR, Len.Fract.TDR_IMP_TFR);
    }

    public byte[] getTdrImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_IMP_TFR, Pos.TDR_IMP_TFR);
        return buffer;
    }

    public void setTdrImpTfrNull(String tdrImpTfrNull) {
        writeString(Pos.TDR_IMP_TFR_NULL, tdrImpTfrNull, Len.TDR_IMP_TFR_NULL);
    }

    /**Original name: TDR-IMP-TFR-NULL<br>*/
    public String getTdrImpTfrNull() {
        return readString(Pos.TDR_IMP_TFR_NULL, Len.TDR_IMP_TFR_NULL);
    }

    public String getTdrImpTfrNullFormatted() {
        return Functions.padBlanks(getTdrImpTfrNull(), Len.TDR_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_IMP_TFR = 1;
        public static final int TDR_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_IMP_TFR = 8;
        public static final int TDR_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
