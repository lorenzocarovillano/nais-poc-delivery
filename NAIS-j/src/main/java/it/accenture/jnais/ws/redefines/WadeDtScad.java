package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WADE-DT-SCAD<br>
 * Variable: WADE-DT-SCAD from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeDtScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeDtScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_DT_SCAD;
    }

    public void setWadeDtScad(int wadeDtScad) {
        writeIntAsPacked(Pos.WADE_DT_SCAD, wadeDtScad, Len.Int.WADE_DT_SCAD);
    }

    public void setWadeDtScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_DT_SCAD, Pos.WADE_DT_SCAD);
    }

    /**Original name: WADE-DT-SCAD<br>*/
    public int getWadeDtScad() {
        return readPackedAsInt(Pos.WADE_DT_SCAD, Len.Int.WADE_DT_SCAD);
    }

    public byte[] getWadeDtScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_DT_SCAD, Pos.WADE_DT_SCAD);
        return buffer;
    }

    public void initWadeDtScadSpaces() {
        fill(Pos.WADE_DT_SCAD, Len.WADE_DT_SCAD, Types.SPACE_CHAR);
    }

    public void setWadeDtScadNull(String wadeDtScadNull) {
        writeString(Pos.WADE_DT_SCAD_NULL, wadeDtScadNull, Len.WADE_DT_SCAD_NULL);
    }

    /**Original name: WADE-DT-SCAD-NULL<br>*/
    public String getWadeDtScadNull() {
        return readString(Pos.WADE_DT_SCAD_NULL, Len.WADE_DT_SCAD_NULL);
    }

    public String getWadeDtScadNullFormatted() {
        return Functions.padBlanks(getWadeDtScadNull(), Len.WADE_DT_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_DT_SCAD = 1;
        public static final int WADE_DT_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_DT_SCAD = 5;
        public static final int WADE_DT_SCAD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_DT_SCAD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
