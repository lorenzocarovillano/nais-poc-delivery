package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CARZ<br>
 * Variable: B03-CARZ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03Carz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03Carz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CARZ;
    }

    public void setB03Carz(int b03Carz) {
        writeIntAsPacked(Pos.B03_CARZ, b03Carz, Len.Int.B03_CARZ);
    }

    public void setB03CarzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CARZ, Pos.B03_CARZ);
    }

    /**Original name: B03-CARZ<br>*/
    public int getB03Carz() {
        return readPackedAsInt(Pos.B03_CARZ, Len.Int.B03_CARZ);
    }

    public byte[] getB03CarzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CARZ, Pos.B03_CARZ);
        return buffer;
    }

    public void setB03CarzNull(String b03CarzNull) {
        writeString(Pos.B03_CARZ_NULL, b03CarzNull, Len.B03_CARZ_NULL);
    }

    /**Original name: B03-CARZ-NULL<br>*/
    public String getB03CarzNull() {
        return readString(Pos.B03_CARZ_NULL, Len.B03_CARZ_NULL);
    }

    public String getB03CarzNullFormatted() {
        return Functions.padBlanks(getB03CarzNull(), Len.B03_CARZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CARZ = 1;
        public static final int B03_CARZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CARZ = 3;
        public static final int B03_CARZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CARZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
