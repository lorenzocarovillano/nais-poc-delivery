package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ISPC0140-RICALC-QUIET<br>
 * Variable: ISPC0140-RICALC-QUIET from copybook ISPC0140<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0140RicalcQuiet {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setRicalcQuiet(char ricalcQuiet) {
        this.value = ricalcQuiet;
    }

    public char getRicalcQuiet() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RICALC_QUIET = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
