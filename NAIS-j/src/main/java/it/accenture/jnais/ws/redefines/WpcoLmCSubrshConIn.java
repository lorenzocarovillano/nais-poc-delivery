package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-LM-C-SUBRSH-CON-IN<br>
 * Variable: WPCO-LM-C-SUBRSH-CON-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoLmCSubrshConIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoLmCSubrshConIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_LM_C_SUBRSH_CON_IN;
    }

    public void setWpcoLmCSubrshConIn(AfDecimal wpcoLmCSubrshConIn) {
        writeDecimalAsPacked(Pos.WPCO_LM_C_SUBRSH_CON_IN, wpcoLmCSubrshConIn.copy());
    }

    public void setDpcoLmCSubrshConInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_LM_C_SUBRSH_CON_IN, Pos.WPCO_LM_C_SUBRSH_CON_IN);
    }

    /**Original name: WPCO-LM-C-SUBRSH-CON-IN<br>*/
    public AfDecimal getWpcoLmCSubrshConIn() {
        return readPackedAsDecimal(Pos.WPCO_LM_C_SUBRSH_CON_IN, Len.Int.WPCO_LM_C_SUBRSH_CON_IN, Len.Fract.WPCO_LM_C_SUBRSH_CON_IN);
    }

    public byte[] getWpcoLmCSubrshConInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_LM_C_SUBRSH_CON_IN, Pos.WPCO_LM_C_SUBRSH_CON_IN);
        return buffer;
    }

    public void setWpcoLmCSubrshConInNull(String wpcoLmCSubrshConInNull) {
        writeString(Pos.WPCO_LM_C_SUBRSH_CON_IN_NULL, wpcoLmCSubrshConInNull, Len.WPCO_LM_C_SUBRSH_CON_IN_NULL);
    }

    /**Original name: WPCO-LM-C-SUBRSH-CON-IN-NULL<br>*/
    public String getWpcoLmCSubrshConInNull() {
        return readString(Pos.WPCO_LM_C_SUBRSH_CON_IN_NULL, Len.WPCO_LM_C_SUBRSH_CON_IN_NULL);
    }

    public String getWpcoLmCSubrshConInNullFormatted() {
        return Functions.padBlanks(getWpcoLmCSubrshConInNull(), Len.WPCO_LM_C_SUBRSH_CON_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_LM_C_SUBRSH_CON_IN = 1;
        public static final int WPCO_LM_C_SUBRSH_CON_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_LM_C_SUBRSH_CON_IN = 4;
        public static final int WPCO_LM_C_SUBRSH_CON_IN_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_LM_C_SUBRSH_CON_IN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_LM_C_SUBRSH_CON_IN = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
