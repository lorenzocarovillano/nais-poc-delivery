package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: A25-DT-1A-ATVT<br>
 * Variable: A25-DT-1A-ATVT from program LDBS1300<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A25Dt1aAtvt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A25Dt1aAtvt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A25_DT1A_ATVT;
    }

    public void setA25Dt1aAtvt(int a25Dt1aAtvt) {
        writeIntAsPacked(Pos.A25_DT1A_ATVT, a25Dt1aAtvt, Len.Int.A25_DT1A_ATVT);
    }

    public void setA25Dt1aAtvtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A25_DT1A_ATVT, Pos.A25_DT1A_ATVT);
    }

    /**Original name: A25-DT-1A-ATVT<br>*/
    public int getA25Dt1aAtvt() {
        return readPackedAsInt(Pos.A25_DT1A_ATVT, Len.Int.A25_DT1A_ATVT);
    }

    public byte[] getA25Dt1aAtvtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A25_DT1A_ATVT, Pos.A25_DT1A_ATVT);
        return buffer;
    }

    public void setA25Dt1aAtvtNull(String a25Dt1aAtvtNull) {
        writeString(Pos.A25_DT1A_ATVT_NULL, a25Dt1aAtvtNull, Len.A25_DT1A_ATVT_NULL);
    }

    /**Original name: A25-DT-1A-ATVT-NULL<br>*/
    public String getA25Dt1aAtvtNull() {
        return readString(Pos.A25_DT1A_ATVT_NULL, Len.A25_DT1A_ATVT_NULL);
    }

    public String getA25Dt1aAtvtNullFormatted() {
        return Functions.padBlanks(getA25Dt1aAtvtNull(), Len.A25_DT1A_ATVT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A25_DT1A_ATVT = 1;
        public static final int A25_DT1A_ATVT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_DT1A_ATVT = 5;
        public static final int A25_DT1A_ATVT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_DT1A_ATVT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
