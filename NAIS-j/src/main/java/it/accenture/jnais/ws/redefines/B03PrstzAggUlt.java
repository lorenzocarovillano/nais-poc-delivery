package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRSTZ-AGG-ULT<br>
 * Variable: B03-PRSTZ-AGG-ULT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PrstzAggUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PrstzAggUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRSTZ_AGG_ULT;
    }

    public void setB03PrstzAggUlt(AfDecimal b03PrstzAggUlt) {
        writeDecimalAsPacked(Pos.B03_PRSTZ_AGG_ULT, b03PrstzAggUlt.copy());
    }

    public void setB03PrstzAggUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRSTZ_AGG_ULT, Pos.B03_PRSTZ_AGG_ULT);
    }

    /**Original name: B03-PRSTZ-AGG-ULT<br>*/
    public AfDecimal getB03PrstzAggUlt() {
        return readPackedAsDecimal(Pos.B03_PRSTZ_AGG_ULT, Len.Int.B03_PRSTZ_AGG_ULT, Len.Fract.B03_PRSTZ_AGG_ULT);
    }

    public byte[] getB03PrstzAggUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRSTZ_AGG_ULT, Pos.B03_PRSTZ_AGG_ULT);
        return buffer;
    }

    public void setB03PrstzAggUltNull(String b03PrstzAggUltNull) {
        writeString(Pos.B03_PRSTZ_AGG_ULT_NULL, b03PrstzAggUltNull, Len.B03_PRSTZ_AGG_ULT_NULL);
    }

    /**Original name: B03-PRSTZ-AGG-ULT-NULL<br>*/
    public String getB03PrstzAggUltNull() {
        return readString(Pos.B03_PRSTZ_AGG_ULT_NULL, Len.B03_PRSTZ_AGG_ULT_NULL);
    }

    public String getB03PrstzAggUltNullFormatted() {
        return Functions.padBlanks(getB03PrstzAggUltNull(), Len.B03_PRSTZ_AGG_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRSTZ_AGG_ULT = 1;
        public static final int B03_PRSTZ_AGG_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRSTZ_AGG_ULT = 8;
        public static final int B03_PRSTZ_AGG_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRSTZ_AGG_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRSTZ_AGG_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
