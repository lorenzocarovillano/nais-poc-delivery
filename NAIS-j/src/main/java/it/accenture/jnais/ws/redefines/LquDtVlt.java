package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-DT-VLT<br>
 * Variable: LQU-DT-VLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquDtVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquDtVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_DT_VLT;
    }

    public void setLquDtVlt(int lquDtVlt) {
        writeIntAsPacked(Pos.LQU_DT_VLT, lquDtVlt, Len.Int.LQU_DT_VLT);
    }

    public void setLquDtVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_DT_VLT, Pos.LQU_DT_VLT);
    }

    /**Original name: LQU-DT-VLT<br>*/
    public int getLquDtVlt() {
        return readPackedAsInt(Pos.LQU_DT_VLT, Len.Int.LQU_DT_VLT);
    }

    public byte[] getLquDtVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_DT_VLT, Pos.LQU_DT_VLT);
        return buffer;
    }

    public void setLquDtVltNull(String lquDtVltNull) {
        writeString(Pos.LQU_DT_VLT_NULL, lquDtVltNull, Len.LQU_DT_VLT_NULL);
    }

    /**Original name: LQU-DT-VLT-NULL<br>*/
    public String getLquDtVltNull() {
        return readString(Pos.LQU_DT_VLT_NULL, Len.LQU_DT_VLT_NULL);
    }

    public String getLquDtVltNullFormatted() {
        return Functions.padBlanks(getLquDtVltNull(), Len.LQU_DT_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_DT_VLT = 1;
        public static final int LQU_DT_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_DT_VLT = 5;
        public static final int LQU_DT_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_DT_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
