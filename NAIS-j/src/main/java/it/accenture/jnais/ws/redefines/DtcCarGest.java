package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-CAR-GEST<br>
 * Variable: DTC-CAR-GEST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_CAR_GEST;
    }

    public void setDtcCarGest(AfDecimal dtcCarGest) {
        writeDecimalAsPacked(Pos.DTC_CAR_GEST, dtcCarGest.copy());
    }

    public void setDtcCarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_CAR_GEST, Pos.DTC_CAR_GEST);
    }

    /**Original name: DTC-CAR-GEST<br>*/
    public AfDecimal getDtcCarGest() {
        return readPackedAsDecimal(Pos.DTC_CAR_GEST, Len.Int.DTC_CAR_GEST, Len.Fract.DTC_CAR_GEST);
    }

    public byte[] getDtcCarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_CAR_GEST, Pos.DTC_CAR_GEST);
        return buffer;
    }

    public void setDtcCarGestNull(String dtcCarGestNull) {
        writeString(Pos.DTC_CAR_GEST_NULL, dtcCarGestNull, Len.DTC_CAR_GEST_NULL);
    }

    /**Original name: DTC-CAR-GEST-NULL<br>*/
    public String getDtcCarGestNull() {
        return readString(Pos.DTC_CAR_GEST_NULL, Len.DTC_CAR_GEST_NULL);
    }

    public String getDtcCarGestNullFormatted() {
        return Functions.padBlanks(getDtcCarGestNull(), Len.DTC_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_CAR_GEST = 1;
        public static final int DTC_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_CAR_GEST = 8;
        public static final int DTC_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
