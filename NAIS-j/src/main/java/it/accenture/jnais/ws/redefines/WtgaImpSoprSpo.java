package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-SOPR-SPO<br>
 * Variable: WTGA-IMP-SOPR-SPO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_SOPR_SPO;
    }

    public void setWtgaImpSoprSpo(AfDecimal wtgaImpSoprSpo) {
        writeDecimalAsPacked(Pos.WTGA_IMP_SOPR_SPO, wtgaImpSoprSpo.copy());
    }

    public void setWtgaImpSoprSpoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_SOPR_SPO, Pos.WTGA_IMP_SOPR_SPO);
    }

    /**Original name: WTGA-IMP-SOPR-SPO<br>*/
    public AfDecimal getWtgaImpSoprSpo() {
        return readPackedAsDecimal(Pos.WTGA_IMP_SOPR_SPO, Len.Int.WTGA_IMP_SOPR_SPO, Len.Fract.WTGA_IMP_SOPR_SPO);
    }

    public byte[] getWtgaImpSoprSpoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_SOPR_SPO, Pos.WTGA_IMP_SOPR_SPO);
        return buffer;
    }

    public void initWtgaImpSoprSpoSpaces() {
        fill(Pos.WTGA_IMP_SOPR_SPO, Len.WTGA_IMP_SOPR_SPO, Types.SPACE_CHAR);
    }

    public void setWtgaImpSoprSpoNull(String wtgaImpSoprSpoNull) {
        writeString(Pos.WTGA_IMP_SOPR_SPO_NULL, wtgaImpSoprSpoNull, Len.WTGA_IMP_SOPR_SPO_NULL);
    }

    /**Original name: WTGA-IMP-SOPR-SPO-NULL<br>*/
    public String getWtgaImpSoprSpoNull() {
        return readString(Pos.WTGA_IMP_SOPR_SPO_NULL, Len.WTGA_IMP_SOPR_SPO_NULL);
    }

    public String getWtgaImpSoprSpoNullFormatted() {
        return Functions.padBlanks(getWtgaImpSoprSpoNull(), Len.WTGA_IMP_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_SOPR_SPO = 1;
        public static final int WTGA_IMP_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_SOPR_SPO = 8;
        public static final int WTGA_IMP_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
