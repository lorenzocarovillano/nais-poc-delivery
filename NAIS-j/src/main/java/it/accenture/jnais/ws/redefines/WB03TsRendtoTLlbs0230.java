package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-TS-RENDTO-T<br>
 * Variable: W-B03-TS-RENDTO-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03TsRendtoTLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03TsRendtoTLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_TS_RENDTO_T;
    }

    public void setwB03TsRendtoT(AfDecimal wB03TsRendtoT) {
        writeDecimalAsPacked(Pos.W_B03_TS_RENDTO_T, wB03TsRendtoT.copy());
    }

    /**Original name: W-B03-TS-RENDTO-T<br>*/
    public AfDecimal getwB03TsRendtoT() {
        return readPackedAsDecimal(Pos.W_B03_TS_RENDTO_T, Len.Int.W_B03_TS_RENDTO_T, Len.Fract.W_B03_TS_RENDTO_T);
    }

    public byte[] getwB03TsRendtoTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_TS_RENDTO_T, Pos.W_B03_TS_RENDTO_T);
        return buffer;
    }

    public void setwB03TsRendtoTNull(String wB03TsRendtoTNull) {
        writeString(Pos.W_B03_TS_RENDTO_T_NULL, wB03TsRendtoTNull, Len.W_B03_TS_RENDTO_T_NULL);
    }

    /**Original name: W-B03-TS-RENDTO-T-NULL<br>*/
    public String getwB03TsRendtoTNull() {
        return readString(Pos.W_B03_TS_RENDTO_T_NULL, Len.W_B03_TS_RENDTO_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_TS_RENDTO_T = 1;
        public static final int W_B03_TS_RENDTO_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_TS_RENDTO_T = 8;
        public static final int W_B03_TS_RENDTO_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_TS_RENDTO_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_TS_RENDTO_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
