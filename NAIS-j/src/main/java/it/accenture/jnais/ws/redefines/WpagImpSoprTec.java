package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-SOPR-TEC<br>
 * Variable: WPAG-IMP-SOPR-TEC from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_SOPR_TEC;
    }

    public void setWpagImpSoprTec(AfDecimal wpagImpSoprTec) {
        writeDecimalAsPacked(Pos.WPAG_IMP_SOPR_TEC, wpagImpSoprTec.copy());
    }

    public void setWpagImpSoprTecFormatted(String wpagImpSoprTec) {
        setWpagImpSoprTec(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_SOPR_TEC + Len.Fract.WPAG_IMP_SOPR_TEC, Len.Fract.WPAG_IMP_SOPR_TEC, wpagImpSoprTec));
    }

    public void setWpagImpSoprTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_SOPR_TEC, Pos.WPAG_IMP_SOPR_TEC);
    }

    /**Original name: WPAG-IMP-SOPR-TEC<br>*/
    public AfDecimal getWpagImpSoprTec() {
        return readPackedAsDecimal(Pos.WPAG_IMP_SOPR_TEC, Len.Int.WPAG_IMP_SOPR_TEC, Len.Fract.WPAG_IMP_SOPR_TEC);
    }

    public byte[] getWpagImpSoprTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_SOPR_TEC, Pos.WPAG_IMP_SOPR_TEC);
        return buffer;
    }

    public void initWpagImpSoprTecSpaces() {
        fill(Pos.WPAG_IMP_SOPR_TEC, Len.WPAG_IMP_SOPR_TEC, Types.SPACE_CHAR);
    }

    public void setWpagImpSoprTecNull(String wpagImpSoprTecNull) {
        writeString(Pos.WPAG_IMP_SOPR_TEC_NULL, wpagImpSoprTecNull, Len.WPAG_IMP_SOPR_TEC_NULL);
    }

    /**Original name: WPAG-IMP-SOPR-TEC-NULL<br>*/
    public String getWpagImpSoprTecNull() {
        return readString(Pos.WPAG_IMP_SOPR_TEC_NULL, Len.WPAG_IMP_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SOPR_TEC = 1;
        public static final int WPAG_IMP_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SOPR_TEC = 8;
        public static final int WPAG_IMP_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
