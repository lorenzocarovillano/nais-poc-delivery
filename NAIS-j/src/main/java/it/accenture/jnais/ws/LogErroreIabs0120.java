package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.ILogErrore;
import it.accenture.jnais.ws.redefines.LorTipoOggetto;

/**Original name: LOG-ERRORE<br>
 * Variable: LOG-ERRORE from copybook IDBVLOR1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LogErroreIabs0120 extends SerializableParameter implements ILogErrore {

    //==== PROPERTIES ====
    //Original name: LOR-ID-LOG-ERRORE
    private int lorIdLogErrore = DefaultValues.INT_VAL;
    //Original name: LOR-PROG-LOG-ERRORE
    private int lorProgLogErrore = DefaultValues.INT_VAL;
    //Original name: LOR-ID-GRAVITA-ERRORE
    private int lorIdGravitaErrore = DefaultValues.INT_VAL;
    //Original name: LOR-DESC-ERRORE-ESTESA-LEN
    private short lorDescErroreEstesaLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: LOR-DESC-ERRORE-ESTESA
    private String lorDescErroreEstesa = DefaultValues.stringVal(Len.LOR_DESC_ERRORE_ESTESA);
    //Original name: LOR-COD-MAIN-BATCH
    private String lorCodMainBatch = DefaultValues.stringVal(Len.LOR_COD_MAIN_BATCH);
    //Original name: LOR-COD-SERVIZIO-BE
    private String lorCodServizioBe = DefaultValues.stringVal(Len.LOR_COD_SERVIZIO_BE);
    //Original name: LOR-LABEL-ERR
    private String lorLabelErr = DefaultValues.stringVal(Len.LOR_LABEL_ERR);
    //Original name: LOR-OPER-TABELLA
    private String lorOperTabella = DefaultValues.stringVal(Len.LOR_OPER_TABELLA);
    //Original name: LOR-NOME-TABELLA
    private String lorNomeTabella = DefaultValues.stringVal(Len.LOR_NOME_TABELLA);
    //Original name: LOR-STATUS-TABELLA
    private String lorStatusTabella = DefaultValues.stringVal(Len.LOR_STATUS_TABELLA);
    //Original name: LOR-KEY-TABELLA-LEN
    private short lorKeyTabellaLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: LOR-KEY-TABELLA
    private String lorKeyTabella = DefaultValues.stringVal(Len.LOR_KEY_TABELLA);
    //Original name: LOR-TIMESTAMP-REG
    private long lorTimestampReg = DefaultValues.LONG_VAL;
    //Original name: LOR-TIPO-OGGETTO
    private LorTipoOggetto lorTipoOggetto = new LorTipoOggetto();
    //Original name: LOR-IB-OGGETTO
    private String lorIbOggetto = DefaultValues.stringVal(Len.LOR_IB_OGGETTO);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LOG_ERRORE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLogErroreBytes(buf);
    }

    public String getLogErroreFormatted() {
        return MarshalByteExt.bufferToStr(getLogErroreBytes());
    }

    public void setLogErroreBytes(byte[] buffer) {
        setLogErroreBytes(buffer, 1);
    }

    public byte[] getLogErroreBytes() {
        byte[] buffer = new byte[Len.LOG_ERRORE];
        return getLogErroreBytes(buffer, 1);
    }

    public void setLogErroreBytes(byte[] buffer, int offset) {
        int position = offset;
        lorIdLogErrore = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LOR_ID_LOG_ERRORE, 0);
        position += Len.LOR_ID_LOG_ERRORE;
        lorProgLogErrore = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LOR_PROG_LOG_ERRORE, 0);
        position += Len.LOR_PROG_LOG_ERRORE;
        lorIdGravitaErrore = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LOR_ID_GRAVITA_ERRORE, 0);
        position += Len.LOR_ID_GRAVITA_ERRORE;
        setLorDescErroreEstesaVcharBytes(buffer, position);
        position += Len.LOR_DESC_ERRORE_ESTESA_VCHAR;
        lorCodMainBatch = MarshalByte.readString(buffer, position, Len.LOR_COD_MAIN_BATCH);
        position += Len.LOR_COD_MAIN_BATCH;
        lorCodServizioBe = MarshalByte.readString(buffer, position, Len.LOR_COD_SERVIZIO_BE);
        position += Len.LOR_COD_SERVIZIO_BE;
        lorLabelErr = MarshalByte.readString(buffer, position, Len.LOR_LABEL_ERR);
        position += Len.LOR_LABEL_ERR;
        lorOperTabella = MarshalByte.readString(buffer, position, Len.LOR_OPER_TABELLA);
        position += Len.LOR_OPER_TABELLA;
        lorNomeTabella = MarshalByte.readString(buffer, position, Len.LOR_NOME_TABELLA);
        position += Len.LOR_NOME_TABELLA;
        lorStatusTabella = MarshalByte.readString(buffer, position, Len.LOR_STATUS_TABELLA);
        position += Len.LOR_STATUS_TABELLA;
        setLorKeyTabellaVcharBytes(buffer, position);
        position += Len.LOR_KEY_TABELLA_VCHAR;
        lorTimestampReg = MarshalByte.readPackedAsLong(buffer, position, Len.Int.LOR_TIMESTAMP_REG, 0);
        position += Len.LOR_TIMESTAMP_REG;
        lorTipoOggetto.setLorTipoOggettoFromBuffer(buffer, position);
        position += LorTipoOggetto.Len.LOR_TIPO_OGGETTO;
        lorIbOggetto = MarshalByte.readString(buffer, position, Len.LOR_IB_OGGETTO);
    }

    public byte[] getLogErroreBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, lorIdLogErrore, Len.Int.LOR_ID_LOG_ERRORE, 0);
        position += Len.LOR_ID_LOG_ERRORE;
        MarshalByte.writeIntAsPacked(buffer, position, lorProgLogErrore, Len.Int.LOR_PROG_LOG_ERRORE, 0);
        position += Len.LOR_PROG_LOG_ERRORE;
        MarshalByte.writeIntAsPacked(buffer, position, lorIdGravitaErrore, Len.Int.LOR_ID_GRAVITA_ERRORE, 0);
        position += Len.LOR_ID_GRAVITA_ERRORE;
        getLorDescErroreEstesaVcharBytes(buffer, position);
        position += Len.LOR_DESC_ERRORE_ESTESA_VCHAR;
        MarshalByte.writeString(buffer, position, lorCodMainBatch, Len.LOR_COD_MAIN_BATCH);
        position += Len.LOR_COD_MAIN_BATCH;
        MarshalByte.writeString(buffer, position, lorCodServizioBe, Len.LOR_COD_SERVIZIO_BE);
        position += Len.LOR_COD_SERVIZIO_BE;
        MarshalByte.writeString(buffer, position, lorLabelErr, Len.LOR_LABEL_ERR);
        position += Len.LOR_LABEL_ERR;
        MarshalByte.writeString(buffer, position, lorOperTabella, Len.LOR_OPER_TABELLA);
        position += Len.LOR_OPER_TABELLA;
        MarshalByte.writeString(buffer, position, lorNomeTabella, Len.LOR_NOME_TABELLA);
        position += Len.LOR_NOME_TABELLA;
        MarshalByte.writeString(buffer, position, lorStatusTabella, Len.LOR_STATUS_TABELLA);
        position += Len.LOR_STATUS_TABELLA;
        getLorKeyTabellaVcharBytes(buffer, position);
        position += Len.LOR_KEY_TABELLA_VCHAR;
        MarshalByte.writeLongAsPacked(buffer, position, lorTimestampReg, Len.Int.LOR_TIMESTAMP_REG, 0);
        position += Len.LOR_TIMESTAMP_REG;
        lorTipoOggetto.getLorTipoOggettoAsBuffer(buffer, position);
        position += LorTipoOggetto.Len.LOR_TIPO_OGGETTO;
        MarshalByte.writeString(buffer, position, lorIbOggetto, Len.LOR_IB_OGGETTO);
        return buffer;
    }

    public void initLogErroreHighValues() {
        lorIdLogErrore = Types.HIGH_INT_VAL;
        lorProgLogErrore = Types.HIGH_INT_VAL;
        lorIdGravitaErrore = Types.HIGH_INT_VAL;
        initLorDescErroreEstesaVcharHighValues();
        lorCodMainBatch = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LOR_COD_MAIN_BATCH);
        lorCodServizioBe = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LOR_COD_SERVIZIO_BE);
        lorLabelErr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LOR_LABEL_ERR);
        lorOperTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LOR_OPER_TABELLA);
        lorNomeTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LOR_NOME_TABELLA);
        lorStatusTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LOR_STATUS_TABELLA);
        initLorKeyTabellaVcharHighValues();
        lorTimestampReg = Types.HIGH_LONG_VAL;
        lorTipoOggetto.initLorTipoOggettoHighValues();
        lorIbOggetto = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LOR_IB_OGGETTO);
    }

    public void setLorIdLogErrore(int lorIdLogErrore) {
        this.lorIdLogErrore = lorIdLogErrore;
    }

    public int getLorIdLogErrore() {
        return this.lorIdLogErrore;
    }

    public void setLorProgLogErrore(int lorProgLogErrore) {
        this.lorProgLogErrore = lorProgLogErrore;
    }

    public int getLorProgLogErrore() {
        return this.lorProgLogErrore;
    }

    public void setLorIdGravitaErrore(int lorIdGravitaErrore) {
        this.lorIdGravitaErrore = lorIdGravitaErrore;
    }

    public int getLorIdGravitaErrore() {
        return this.lorIdGravitaErrore;
    }

    public void setLorDescErroreEstesaVcharFormatted(String data) {
        byte[] buffer = new byte[Len.LOR_DESC_ERRORE_ESTESA_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.LOR_DESC_ERRORE_ESTESA_VCHAR);
        setLorDescErroreEstesaVcharBytes(buffer, 1);
    }

    public String getLorDescErroreEstesaVcharFormatted() {
        return MarshalByteExt.bufferToStr(getLorDescErroreEstesaVcharBytes());
    }

    /**Original name: LOR-DESC-ERRORE-ESTESA-VCHAR<br>*/
    public byte[] getLorDescErroreEstesaVcharBytes() {
        byte[] buffer = new byte[Len.LOR_DESC_ERRORE_ESTESA_VCHAR];
        return getLorDescErroreEstesaVcharBytes(buffer, 1);
    }

    public void setLorDescErroreEstesaVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        lorDescErroreEstesaLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        lorDescErroreEstesa = MarshalByte.readString(buffer, position, Len.LOR_DESC_ERRORE_ESTESA);
    }

    public byte[] getLorDescErroreEstesaVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, lorDescErroreEstesaLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, lorDescErroreEstesa, Len.LOR_DESC_ERRORE_ESTESA);
        return buffer;
    }

    public void initLorDescErroreEstesaVcharHighValues() {
        lorDescErroreEstesaLen = Types.HIGH_SHORT_VAL;
        lorDescErroreEstesa = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LOR_DESC_ERRORE_ESTESA);
    }

    public void setLorDescErroreEstesaLen(short lorDescErroreEstesaLen) {
        this.lorDescErroreEstesaLen = lorDescErroreEstesaLen;
    }

    public short getLorDescErroreEstesaLen() {
        return this.lorDescErroreEstesaLen;
    }

    public void setLorDescErroreEstesa(String lorDescErroreEstesa) {
        this.lorDescErroreEstesa = Functions.subString(lorDescErroreEstesa, Len.LOR_DESC_ERRORE_ESTESA);
    }

    public String getLorDescErroreEstesa() {
        return this.lorDescErroreEstesa;
    }

    public void setLorCodMainBatch(String lorCodMainBatch) {
        this.lorCodMainBatch = Functions.subString(lorCodMainBatch, Len.LOR_COD_MAIN_BATCH);
    }

    public String getLorCodMainBatch() {
        return this.lorCodMainBatch;
    }

    public void setLorCodServizioBe(String lorCodServizioBe) {
        this.lorCodServizioBe = Functions.subString(lorCodServizioBe, Len.LOR_COD_SERVIZIO_BE);
    }

    public String getLorCodServizioBe() {
        return this.lorCodServizioBe;
    }

    public void setLorLabelErr(String lorLabelErr) {
        this.lorLabelErr = Functions.subString(lorLabelErr, Len.LOR_LABEL_ERR);
    }

    public String getLorLabelErr() {
        return this.lorLabelErr;
    }

    public void setLorOperTabella(String lorOperTabella) {
        this.lorOperTabella = Functions.subString(lorOperTabella, Len.LOR_OPER_TABELLA);
    }

    public String getLorOperTabella() {
        return this.lorOperTabella;
    }

    public String getLorOperTabellaFormatted() {
        return Functions.padBlanks(getLorOperTabella(), Len.LOR_OPER_TABELLA);
    }

    public void setLorNomeTabella(String lorNomeTabella) {
        this.lorNomeTabella = Functions.subString(lorNomeTabella, Len.LOR_NOME_TABELLA);
    }

    public String getLorNomeTabella() {
        return this.lorNomeTabella;
    }

    public String getLorNomeTabellaFormatted() {
        return Functions.padBlanks(getLorNomeTabella(), Len.LOR_NOME_TABELLA);
    }

    public void setLorStatusTabella(String lorStatusTabella) {
        this.lorStatusTabella = Functions.subString(lorStatusTabella, Len.LOR_STATUS_TABELLA);
    }

    public String getLorStatusTabella() {
        return this.lorStatusTabella;
    }

    public String getLorStatusTabellaFormatted() {
        return Functions.padBlanks(getLorStatusTabella(), Len.LOR_STATUS_TABELLA);
    }

    public void setLorKeyTabellaVcharFormatted(String data) {
        byte[] buffer = new byte[Len.LOR_KEY_TABELLA_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.LOR_KEY_TABELLA_VCHAR);
        setLorKeyTabellaVcharBytes(buffer, 1);
    }

    public String getLorKeyTabellaVcharFormatted() {
        return MarshalByteExt.bufferToStr(getLorKeyTabellaVcharBytes());
    }

    /**Original name: LOR-KEY-TABELLA-VCHAR<br>*/
    public byte[] getLorKeyTabellaVcharBytes() {
        byte[] buffer = new byte[Len.LOR_KEY_TABELLA_VCHAR];
        return getLorKeyTabellaVcharBytes(buffer, 1);
    }

    public void setLorKeyTabellaVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        lorKeyTabellaLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        lorKeyTabella = MarshalByte.readString(buffer, position, Len.LOR_KEY_TABELLA);
    }

    public byte[] getLorKeyTabellaVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, lorKeyTabellaLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, lorKeyTabella, Len.LOR_KEY_TABELLA);
        return buffer;
    }

    public void initLorKeyTabellaVcharHighValues() {
        lorKeyTabellaLen = Types.HIGH_SHORT_VAL;
        lorKeyTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LOR_KEY_TABELLA);
    }

    public void setLorKeyTabellaLen(short lorKeyTabellaLen) {
        this.lorKeyTabellaLen = lorKeyTabellaLen;
    }

    public short getLorKeyTabellaLen() {
        return this.lorKeyTabellaLen;
    }

    public void setLorKeyTabella(String lorKeyTabella) {
        this.lorKeyTabella = Functions.subString(lorKeyTabella, Len.LOR_KEY_TABELLA);
    }

    public String getLorKeyTabella() {
        return this.lorKeyTabella;
    }

    public void setLorTimestampReg(long lorTimestampReg) {
        this.lorTimestampReg = lorTimestampReg;
    }

    public long getLorTimestampReg() {
        return this.lorTimestampReg;
    }

    public void setLorIbOggetto(String lorIbOggetto) {
        this.lorIbOggetto = Functions.subString(lorIbOggetto, Len.LOR_IB_OGGETTO);
    }

    public String getLorIbOggetto() {
        return this.lorIbOggetto;
    }

    public String getLorIbOggettoFormatted() {
        return Functions.padBlanks(getLorIbOggetto(), Len.LOR_IB_OGGETTO);
    }

    @Override
    public String getCodMainBatch() {
        throw new FieldNotMappedException("codMainBatch");
    }

    @Override
    public void setCodMainBatch(String codMainBatch) {
        throw new FieldNotMappedException("codMainBatch");
    }

    @Override
    public String getCodServizioBe() {
        throw new FieldNotMappedException("codServizioBe");
    }

    @Override
    public void setCodServizioBe(String codServizioBe) {
        throw new FieldNotMappedException("codServizioBe");
    }

    @Override
    public String getDescErroreEstesaVchar() {
        throw new FieldNotMappedException("descErroreEstesaVchar");
    }

    @Override
    public void setDescErroreEstesaVchar(String descErroreEstesaVchar) {
        throw new FieldNotMappedException("descErroreEstesaVchar");
    }

    @Override
    public String getIbOggetto() {
        throw new FieldNotMappedException("ibOggetto");
    }

    @Override
    public void setIbOggetto(String ibOggetto) {
        throw new FieldNotMappedException("ibOggetto");
    }

    @Override
    public String getIbOggettoObj() {
        return getIbOggetto();
    }

    @Override
    public void setIbOggettoObj(String ibOggettoObj) {
        setIbOggetto(ibOggettoObj);
    }

    @Override
    public int getIdGravitaErrore() {
        throw new FieldNotMappedException("idGravitaErrore");
    }

    @Override
    public void setIdGravitaErrore(int idGravitaErrore) {
        throw new FieldNotMappedException("idGravitaErrore");
    }

    @Override
    public int getIdLogErrore() {
        throw new FieldNotMappedException("idLogErrore");
    }

    @Override
    public void setIdLogErrore(int idLogErrore) {
        throw new FieldNotMappedException("idLogErrore");
    }

    @Override
    public String getKeyTabellaVchar() {
        throw new FieldNotMappedException("keyTabellaVchar");
    }

    @Override
    public void setKeyTabellaVchar(String keyTabellaVchar) {
        throw new FieldNotMappedException("keyTabellaVchar");
    }

    @Override
    public String getKeyTabellaVcharObj() {
        return getKeyTabellaVchar();
    }

    @Override
    public void setKeyTabellaVcharObj(String keyTabellaVcharObj) {
        setKeyTabellaVchar(keyTabellaVcharObj);
    }

    @Override
    public String getLabelErr() {
        throw new FieldNotMappedException("labelErr");
    }

    @Override
    public void setLabelErr(String labelErr) {
        throw new FieldNotMappedException("labelErr");
    }

    @Override
    public String getLabelErrObj() {
        return getLabelErr();
    }

    @Override
    public void setLabelErrObj(String labelErrObj) {
        setLabelErr(labelErrObj);
    }

    public LorTipoOggetto getLorTipoOggetto() {
        return lorTipoOggetto;
    }

    @Override
    public String getNomeTabella() {
        throw new FieldNotMappedException("nomeTabella");
    }

    @Override
    public void setNomeTabella(String nomeTabella) {
        throw new FieldNotMappedException("nomeTabella");
    }

    @Override
    public String getNomeTabellaObj() {
        return getNomeTabella();
    }

    @Override
    public void setNomeTabellaObj(String nomeTabellaObj) {
        setNomeTabella(nomeTabellaObj);
    }

    @Override
    public String getOperTabella() {
        throw new FieldNotMappedException("operTabella");
    }

    @Override
    public void setOperTabella(String operTabella) {
        throw new FieldNotMappedException("operTabella");
    }

    @Override
    public String getOperTabellaObj() {
        return getOperTabella();
    }

    @Override
    public void setOperTabellaObj(String operTabellaObj) {
        setOperTabella(operTabellaObj);
    }

    @Override
    public int getProgLogErrore() {
        throw new FieldNotMappedException("progLogErrore");
    }

    @Override
    public void setProgLogErrore(int progLogErrore) {
        throw new FieldNotMappedException("progLogErrore");
    }

    @Override
    public String getStatusTabella() {
        throw new FieldNotMappedException("statusTabella");
    }

    @Override
    public void setStatusTabella(String statusTabella) {
        throw new FieldNotMappedException("statusTabella");
    }

    @Override
    public String getStatusTabellaObj() {
        return getStatusTabella();
    }

    @Override
    public void setStatusTabellaObj(String statusTabellaObj) {
        setStatusTabella(statusTabellaObj);
    }

    @Override
    public long getTimestampReg() {
        throw new FieldNotMappedException("timestampReg");
    }

    @Override
    public void setTimestampReg(long timestampReg) {
        throw new FieldNotMappedException("timestampReg");
    }

    @Override
    public short getTipoOggetto() {
        throw new FieldNotMappedException("tipoOggetto");
    }

    @Override
    public void setTipoOggetto(short tipoOggetto) {
        throw new FieldNotMappedException("tipoOggetto");
    }

    @Override
    public Short getTipoOggettoObj() {
        return ((Short)getTipoOggetto());
    }

    @Override
    public void setTipoOggettoObj(Short tipoOggettoObj) {
        setTipoOggetto(((short)tipoOggettoObj));
    }

    @Override
    public byte[] serialize() {
        return getLogErroreBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LOR_ID_LOG_ERRORE = 5;
        public static final int LOR_PROG_LOG_ERRORE = 3;
        public static final int LOR_ID_GRAVITA_ERRORE = 5;
        public static final int LOR_DESC_ERRORE_ESTESA_LEN = 2;
        public static final int LOR_DESC_ERRORE_ESTESA = 200;
        public static final int LOR_DESC_ERRORE_ESTESA_VCHAR = LOR_DESC_ERRORE_ESTESA_LEN + LOR_DESC_ERRORE_ESTESA;
        public static final int LOR_COD_MAIN_BATCH = 8;
        public static final int LOR_COD_SERVIZIO_BE = 8;
        public static final int LOR_LABEL_ERR = 30;
        public static final int LOR_OPER_TABELLA = 2;
        public static final int LOR_NOME_TABELLA = 18;
        public static final int LOR_STATUS_TABELLA = 10;
        public static final int LOR_KEY_TABELLA_LEN = 2;
        public static final int LOR_KEY_TABELLA = 100;
        public static final int LOR_KEY_TABELLA_VCHAR = LOR_KEY_TABELLA_LEN + LOR_KEY_TABELLA;
        public static final int LOR_TIMESTAMP_REG = 10;
        public static final int LOR_IB_OGGETTO = 20;
        public static final int LOG_ERRORE = LOR_ID_LOG_ERRORE + LOR_PROG_LOG_ERRORE + LOR_ID_GRAVITA_ERRORE + LOR_DESC_ERRORE_ESTESA_VCHAR + LOR_COD_MAIN_BATCH + LOR_COD_SERVIZIO_BE + LOR_LABEL_ERR + LOR_OPER_TABELLA + LOR_NOME_TABELLA + LOR_STATUS_TABELLA + LOR_KEY_TABELLA_VCHAR + LOR_TIMESTAMP_REG + LorTipoOggetto.Len.LOR_TIPO_OGGETTO + LOR_IB_OGGETTO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LOR_ID_LOG_ERRORE = 9;
            public static final int LOR_PROG_LOG_ERRORE = 5;
            public static final int LOR_ID_GRAVITA_ERRORE = 9;
            public static final int LOR_TIMESTAMP_REG = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
