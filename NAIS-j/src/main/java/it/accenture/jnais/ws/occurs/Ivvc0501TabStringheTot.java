package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0501-TAB-STRINGHE-TOT<br>
 * Variables: IVVC0501-TAB-STRINGHE-TOT from copybook IVVC0501<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0501TabStringheTot {

    //==== PROPERTIES ====
    //Original name: IVVC0501-STRINGA-TOT
    private String ivvc0501StringaTot = DefaultValues.stringVal(Len.IVVC0501_STRINGA_TOT);

    //==== METHODS ====
    public void setTabStringheTotBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0501StringaTot = MarshalByte.readString(buffer, position, Len.IVVC0501_STRINGA_TOT);
    }

    public byte[] getTabStringheTotBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ivvc0501StringaTot, Len.IVVC0501_STRINGA_TOT);
        return buffer;
    }

    public void initTabStringheTotSpaces() {
        ivvc0501StringaTot = "";
    }

    public void setIvvc0501StringaTot(String ivvc0501StringaTot) {
        this.ivvc0501StringaTot = Functions.subString(ivvc0501StringaTot, Len.IVVC0501_STRINGA_TOT);
    }

    public String getIvvc0501StringaTot() {
        return this.ivvc0501StringaTot;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IVVC0501_STRINGA_TOT = 60;
        public static final int TAB_STRINGHE_TOT = IVVC0501_STRINGA_TOT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
