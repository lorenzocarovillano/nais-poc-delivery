package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-BILA<br>
 * Variable: WRST-RIS-BILA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisBila extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisBila() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_BILA;
    }

    public void setWrstRisBila(AfDecimal wrstRisBila) {
        writeDecimalAsPacked(Pos.WRST_RIS_BILA, wrstRisBila.copy());
    }

    public void setWrstRisBilaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_BILA, Pos.WRST_RIS_BILA);
    }

    /**Original name: WRST-RIS-BILA<br>*/
    public AfDecimal getWrstRisBila() {
        return readPackedAsDecimal(Pos.WRST_RIS_BILA, Len.Int.WRST_RIS_BILA, Len.Fract.WRST_RIS_BILA);
    }

    public byte[] getWrstRisBilaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_BILA, Pos.WRST_RIS_BILA);
        return buffer;
    }

    public void initWrstRisBilaSpaces() {
        fill(Pos.WRST_RIS_BILA, Len.WRST_RIS_BILA, Types.SPACE_CHAR);
    }

    public void setWrstRisBilaNull(String wrstRisBilaNull) {
        writeString(Pos.WRST_RIS_BILA_NULL, wrstRisBilaNull, Len.WRST_RIS_BILA_NULL);
    }

    /**Original name: WRST-RIS-BILA-NULL<br>*/
    public String getWrstRisBilaNull() {
        return readString(Pos.WRST_RIS_BILA_NULL, Len.WRST_RIS_BILA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_BILA = 1;
        public static final int WRST_RIS_BILA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_BILA = 8;
        public static final int WRST_RIS_BILA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_BILA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_BILA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
