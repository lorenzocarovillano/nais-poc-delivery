package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: LCCC0062-TAB-RATE<br>
 * Variables: LCCC0062-TAB-RATE from copybook LCCC0062<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Lccc0062TabRate {

    //==== PROPERTIES ====
    //Original name: LCCC0062-DT-INF
    private int dtInf = DefaultValues.INT_VAL;
    //Original name: LCCC0062-DT-SUP
    private int dtSup = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setTabRateBytes(byte[] buffer, int offset) {
        int position = offset;
        setRataBytes(buffer, position);
    }

    public byte[] getTabRateBytes(byte[] buffer, int offset) {
        int position = offset;
        getRataBytes(buffer, position);
        return buffer;
    }

    public void initTabRateSpaces() {
        initRataSpaces();
    }

    public void setRataBytes(byte[] buffer, int offset) {
        int position = offset;
        dtInf = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INF, 0, SignType.NO_SIGN);
        position += Len.DT_INF;
        dtSup = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_SUP, 0, SignType.NO_SIGN);
    }

    public byte[] getRataBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, dtInf, Len.Int.DT_INF, 0, SignType.NO_SIGN);
        position += Len.DT_INF;
        MarshalByte.writeIntAsPacked(buffer, position, dtSup, Len.Int.DT_SUP, 0, SignType.NO_SIGN);
        return buffer;
    }

    public void initRataSpaces() {
        dtInf = Types.INVALID_INT_VAL;
        dtSup = Types.INVALID_INT_VAL;
    }

    public void setDtInf(int dtInf) {
        this.dtInf = dtInf;
    }

    public int getDtInf() {
        return this.dtInf;
    }

    public void setDtSup(int dtSup) {
        this.dtSup = dtSup;
    }

    public int getDtSup() {
        return this.dtSup;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_INF = 5;
        public static final int DT_SUP = 5;
        public static final int RATA = DT_INF + DT_SUP;
        public static final int TAB_RATE = RATA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DT_INF = 8;
            public static final int DT_SUP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
