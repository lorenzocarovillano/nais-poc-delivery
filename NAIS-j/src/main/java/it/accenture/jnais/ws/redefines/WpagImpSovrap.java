package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-SOVRAP<br>
 * Variable: WPAG-IMP-SOVRAP from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpSovrap extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpSovrap() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_SOVRAP;
    }

    public void setWpagImpSovrap(AfDecimal wpagImpSovrap) {
        writeDecimalAsPacked(Pos.WPAG_IMP_SOVRAP, wpagImpSovrap.copy());
    }

    public void setWpagImpSovrapFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_SOVRAP, Pos.WPAG_IMP_SOVRAP);
    }

    /**Original name: WPAG-IMP-SOVRAP<br>*/
    public AfDecimal getWpagImpSovrap() {
        return readPackedAsDecimal(Pos.WPAG_IMP_SOVRAP, Len.Int.WPAG_IMP_SOVRAP, Len.Fract.WPAG_IMP_SOVRAP);
    }

    public byte[] getWpagImpSovrapAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_SOVRAP, Pos.WPAG_IMP_SOVRAP);
        return buffer;
    }

    public void initWpagImpSovrapSpaces() {
        fill(Pos.WPAG_IMP_SOVRAP, Len.WPAG_IMP_SOVRAP, Types.SPACE_CHAR);
    }

    public void setWpagImpSovrapNull(String wpagImpSovrapNull) {
        writeString(Pos.WPAG_IMP_SOVRAP_NULL, wpagImpSovrapNull, Len.WPAG_IMP_SOVRAP_NULL);
    }

    /**Original name: WPAG-IMP-SOVRAP-NULL<br>*/
    public String getWpagImpSovrapNull() {
        return readString(Pos.WPAG_IMP_SOVRAP_NULL, Len.WPAG_IMP_SOVRAP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SOVRAP = 1;
        public static final int WPAG_IMP_SOVRAP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SOVRAP = 8;
        public static final int WPAG_IMP_SOVRAP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SOVRAP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SOVRAP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
