package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-SCAD-PAG-PRE<br>
 * Variable: B03-DT-SCAD-PAG-PRE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtScadPagPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtScadPagPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_SCAD_PAG_PRE;
    }

    public void setB03DtScadPagPre(int b03DtScadPagPre) {
        writeIntAsPacked(Pos.B03_DT_SCAD_PAG_PRE, b03DtScadPagPre, Len.Int.B03_DT_SCAD_PAG_PRE);
    }

    public void setB03DtScadPagPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_SCAD_PAG_PRE, Pos.B03_DT_SCAD_PAG_PRE);
    }

    /**Original name: B03-DT-SCAD-PAG-PRE<br>*/
    public int getB03DtScadPagPre() {
        return readPackedAsInt(Pos.B03_DT_SCAD_PAG_PRE, Len.Int.B03_DT_SCAD_PAG_PRE);
    }

    public byte[] getB03DtScadPagPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_SCAD_PAG_PRE, Pos.B03_DT_SCAD_PAG_PRE);
        return buffer;
    }

    public void setB03DtScadPagPreNull(String b03DtScadPagPreNull) {
        writeString(Pos.B03_DT_SCAD_PAG_PRE_NULL, b03DtScadPagPreNull, Len.B03_DT_SCAD_PAG_PRE_NULL);
    }

    /**Original name: B03-DT-SCAD-PAG-PRE-NULL<br>*/
    public String getB03DtScadPagPreNull() {
        return readString(Pos.B03_DT_SCAD_PAG_PRE_NULL, Len.B03_DT_SCAD_PAG_PRE_NULL);
    }

    public String getB03DtScadPagPreNullFormatted() {
        return Functions.padBlanks(getB03DtScadPagPreNull(), Len.B03_DT_SCAD_PAG_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_SCAD_PAG_PRE = 1;
        public static final int B03_DT_SCAD_PAG_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_SCAD_PAG_PRE = 5;
        public static final int B03_DT_SCAD_PAG_PRE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_SCAD_PAG_PRE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
