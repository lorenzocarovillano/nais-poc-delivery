package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-MANFEE-ANTIC<br>
 * Variable: WTIT-TOT-MANFEE-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_MANFEE_ANTIC;
    }

    public void setWtitTotManfeeAntic(AfDecimal wtitTotManfeeAntic) {
        writeDecimalAsPacked(Pos.WTIT_TOT_MANFEE_ANTIC, wtitTotManfeeAntic.copy());
    }

    public void setWtitTotManfeeAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_MANFEE_ANTIC, Pos.WTIT_TOT_MANFEE_ANTIC);
    }

    /**Original name: WTIT-TOT-MANFEE-ANTIC<br>*/
    public AfDecimal getWtitTotManfeeAntic() {
        return readPackedAsDecimal(Pos.WTIT_TOT_MANFEE_ANTIC, Len.Int.WTIT_TOT_MANFEE_ANTIC, Len.Fract.WTIT_TOT_MANFEE_ANTIC);
    }

    public byte[] getWtitTotManfeeAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_MANFEE_ANTIC, Pos.WTIT_TOT_MANFEE_ANTIC);
        return buffer;
    }

    public void initWtitTotManfeeAnticSpaces() {
        fill(Pos.WTIT_TOT_MANFEE_ANTIC, Len.WTIT_TOT_MANFEE_ANTIC, Types.SPACE_CHAR);
    }

    public void setWtitTotManfeeAnticNull(String wtitTotManfeeAnticNull) {
        writeString(Pos.WTIT_TOT_MANFEE_ANTIC_NULL, wtitTotManfeeAnticNull, Len.WTIT_TOT_MANFEE_ANTIC_NULL);
    }

    /**Original name: WTIT-TOT-MANFEE-ANTIC-NULL<br>*/
    public String getWtitTotManfeeAnticNull() {
        return readString(Pos.WTIT_TOT_MANFEE_ANTIC_NULL, Len.WTIT_TOT_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_MANFEE_ANTIC = 1;
        public static final int WTIT_TOT_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_MANFEE_ANTIC = 8;
        public static final int WTIT_TOT_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
