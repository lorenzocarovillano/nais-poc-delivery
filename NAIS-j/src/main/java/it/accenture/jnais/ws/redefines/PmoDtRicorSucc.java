package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-DT-RICOR-SUCC<br>
 * Variable: PMO-DT-RICOR-SUCC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoDtRicorSucc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoDtRicorSucc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_DT_RICOR_SUCC;
    }

    public void setPmoDtRicorSucc(int pmoDtRicorSucc) {
        writeIntAsPacked(Pos.PMO_DT_RICOR_SUCC, pmoDtRicorSucc, Len.Int.PMO_DT_RICOR_SUCC);
    }

    public void setPmoDtRicorSuccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_DT_RICOR_SUCC, Pos.PMO_DT_RICOR_SUCC);
    }

    /**Original name: PMO-DT-RICOR-SUCC<br>*/
    public int getPmoDtRicorSucc() {
        return readPackedAsInt(Pos.PMO_DT_RICOR_SUCC, Len.Int.PMO_DT_RICOR_SUCC);
    }

    public byte[] getPmoDtRicorSuccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_DT_RICOR_SUCC, Pos.PMO_DT_RICOR_SUCC);
        return buffer;
    }

    public void initPmoDtRicorSuccHighValues() {
        fill(Pos.PMO_DT_RICOR_SUCC, Len.PMO_DT_RICOR_SUCC, Types.HIGH_CHAR_VAL);
    }

    public void setPmoDtRicorSuccNull(String pmoDtRicorSuccNull) {
        writeString(Pos.PMO_DT_RICOR_SUCC_NULL, pmoDtRicorSuccNull, Len.PMO_DT_RICOR_SUCC_NULL);
    }

    /**Original name: PMO-DT-RICOR-SUCC-NULL<br>*/
    public String getPmoDtRicorSuccNull() {
        return readString(Pos.PMO_DT_RICOR_SUCC_NULL, Len.PMO_DT_RICOR_SUCC_NULL);
    }

    public String getPmoDtRicorSuccNullFormatted() {
        return Functions.padBlanks(getPmoDtRicorSuccNull(), Len.PMO_DT_RICOR_SUCC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_DT_RICOR_SUCC = 1;
        public static final int PMO_DT_RICOR_SUCC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_DT_RICOR_SUCC = 5;
        public static final int PMO_DT_RICOR_SUCC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_DT_RICOR_SUCC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
