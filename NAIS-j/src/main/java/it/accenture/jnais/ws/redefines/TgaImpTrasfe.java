package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-TRASFE<br>
 * Variable: TGA-IMP-TRASFE from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_TRASFE;
    }

    public void setTgaImpTrasfe(AfDecimal tgaImpTrasfe) {
        writeDecimalAsPacked(Pos.TGA_IMP_TRASFE, tgaImpTrasfe.copy());
    }

    public void setTgaImpTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_TRASFE, Pos.TGA_IMP_TRASFE);
    }

    /**Original name: TGA-IMP-TRASFE<br>*/
    public AfDecimal getTgaImpTrasfe() {
        return readPackedAsDecimal(Pos.TGA_IMP_TRASFE, Len.Int.TGA_IMP_TRASFE, Len.Fract.TGA_IMP_TRASFE);
    }

    public byte[] getTgaImpTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_TRASFE, Pos.TGA_IMP_TRASFE);
        return buffer;
    }

    public void setTgaImpTrasfeNull(String tgaImpTrasfeNull) {
        writeString(Pos.TGA_IMP_TRASFE_NULL, tgaImpTrasfeNull, Len.TGA_IMP_TRASFE_NULL);
    }

    /**Original name: TGA-IMP-TRASFE-NULL<br>*/
    public String getTgaImpTrasfeNull() {
        return readString(Pos.TGA_IMP_TRASFE_NULL, Len.TGA_IMP_TRASFE_NULL);
    }

    public String getTgaImpTrasfeNullFormatted() {
        return Functions.padBlanks(getTgaImpTrasfeNull(), Len.TGA_IMP_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_TRASFE = 1;
        public static final int TGA_IMP_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_TRASFE = 8;
        public static final int TGA_IMP_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
