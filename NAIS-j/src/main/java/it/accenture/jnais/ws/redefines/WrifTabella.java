package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRIF-TABELLA<br>
 * Variable: WRIF-TABELLA from program IVVS0211<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrifTabella extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_RIC_INV_MAXOCCURS = 1250;
    public static final char WRIF_ST_ADD = 'A';
    public static final char WRIF_ST_MOD = 'M';
    public static final char WRIF_ST_INV = 'I';
    public static final char WRIF_ST_DEL = 'D';
    public static final char WRIF_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public WrifTabella() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIF_TABELLA;
    }

    public String getWrifTabellaFormatted() {
        return readFixedString(Pos.WRIF_TABELLA, Len.WRIF_TABELLA);
    }

    public void setWrifTabellaBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIF_TABELLA, Pos.WRIF_TABELLA);
    }

    public byte[] getWrifTabellaBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIF_TABELLA, Pos.WRIF_TABELLA);
        return buffer;
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.wrifStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: WRIF-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA RICH_INVST_FND
	 *    ALIAS RIF
	 *    ULTIMO AGG. 25 NOV 2019
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.wrifStatus(statusIdx - 1);
        return readChar(position);
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.wrifIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: WRIF-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.wrifIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdRichInvstFnd(int idRichInvstFndIdx, int idRichInvstFnd) {
        int position = Pos.wrifIdRichInvstFnd(idRichInvstFndIdx - 1);
        writeIntAsPacked(position, idRichInvstFnd, Len.Int.ID_RICH_INVST_FND);
    }

    /**Original name: WRIF-ID-RICH-INVST-FND<br>*/
    public int getIdRichInvstFnd(int idRichInvstFndIdx) {
        int position = Pos.wrifIdRichInvstFnd(idRichInvstFndIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_RICH_INVST_FND);
    }

    public void setIdMoviFinrio(int idMoviFinrioIdx, int idMoviFinrio) {
        int position = Pos.wrifIdMoviFinrio(idMoviFinrioIdx - 1);
        writeIntAsPacked(position, idMoviFinrio, Len.Int.ID_MOVI_FINRIO);
    }

    /**Original name: WRIF-ID-MOVI-FINRIO<br>*/
    public int getIdMoviFinrio(int idMoviFinrioIdx) {
        int position = Pos.wrifIdMoviFinrio(idMoviFinrioIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_FINRIO);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.wrifIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: WRIF-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.wrifIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.wrifIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: WRIF-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.wrifIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.wrifDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: WRIF-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.wrifDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.wrifDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: WRIF-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.wrifDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.wrifCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: WRIF-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wrifCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setCodFnd(int codFndIdx, String codFnd) {
        int position = Pos.wrifCodFnd(codFndIdx - 1);
        writeString(position, codFnd, Len.COD_FND);
    }

    /**Original name: WRIF-COD-FND<br>*/
    public String getCodFnd(int codFndIdx) {
        int position = Pos.wrifCodFnd(codFndIdx - 1);
        return readString(position, Len.COD_FND);
    }

    public void setNumQuo(int numQuoIdx, AfDecimal numQuo) {
        int position = Pos.wrifNumQuo(numQuoIdx - 1);
        writeDecimalAsPacked(position, numQuo.copy());
    }

    /**Original name: WRIF-NUM-QUO<br>*/
    public AfDecimal getNumQuo(int numQuoIdx) {
        int position = Pos.wrifNumQuo(numQuoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.NUM_QUO, Len.Fract.NUM_QUO);
    }

    public void setPc(int pcIdx, AfDecimal pc) {
        int position = Pos.wrifPc(pcIdx - 1);
        writeDecimalAsPacked(position, pc.copy());
    }

    /**Original name: WRIF-PC<br>*/
    public AfDecimal getPc(int pcIdx) {
        int position = Pos.wrifPc(pcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC, Len.Fract.PC);
    }

    public void setImpMovto(int impMovtoIdx, AfDecimal impMovto) {
        int position = Pos.wrifImpMovto(impMovtoIdx - 1);
        writeDecimalAsPacked(position, impMovto.copy());
    }

    /**Original name: WRIF-IMP-MOVTO<br>*/
    public AfDecimal getImpMovto(int impMovtoIdx) {
        int position = Pos.wrifImpMovto(impMovtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_MOVTO, Len.Fract.IMP_MOVTO);
    }

    public void setDtInvst(int dtInvstIdx, int dtInvst) {
        int position = Pos.wrifDtInvst(dtInvstIdx - 1);
        writeIntAsPacked(position, dtInvst, Len.Int.DT_INVST);
    }

    /**Original name: WRIF-DT-INVST<br>*/
    public int getDtInvst(int dtInvstIdx) {
        int position = Pos.wrifDtInvst(dtInvstIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INVST);
    }

    public void setCodTari(int codTariIdx, String codTari) {
        int position = Pos.wrifCodTari(codTariIdx - 1);
        writeString(position, codTari, Len.COD_TARI);
    }

    /**Original name: WRIF-COD-TARI<br>*/
    public String getCodTari(int codTariIdx) {
        int position = Pos.wrifCodTari(codTariIdx - 1);
        return readString(position, Len.COD_TARI);
    }

    public void setTpStat(int tpStatIdx, String tpStat) {
        int position = Pos.wrifTpStat(tpStatIdx - 1);
        writeString(position, tpStat, Len.TP_STAT);
    }

    /**Original name: WRIF-TP-STAT<br>*/
    public String getTpStat(int tpStatIdx) {
        int position = Pos.wrifTpStat(tpStatIdx - 1);
        return readString(position, Len.TP_STAT);
    }

    public void setTpModInvst(int tpModInvstIdx, String tpModInvst) {
        int position = Pos.wrifTpModInvst(tpModInvstIdx - 1);
        writeString(position, tpModInvst, Len.TP_MOD_INVST);
    }

    /**Original name: WRIF-TP-MOD-INVST<br>*/
    public String getTpModInvst(int tpModInvstIdx) {
        int position = Pos.wrifTpModInvst(tpModInvstIdx - 1);
        return readString(position, Len.TP_MOD_INVST);
    }

    public void setCodDiv(int codDivIdx, String codDiv) {
        int position = Pos.wrifCodDiv(codDivIdx - 1);
        writeString(position, codDiv, Len.COD_DIV);
    }

    /**Original name: WRIF-COD-DIV<br>*/
    public String getCodDiv(int codDivIdx) {
        int position = Pos.wrifCodDiv(codDivIdx - 1);
        return readString(position, Len.COD_DIV);
    }

    public void setDtCambioVlt(int dtCambioVltIdx, int dtCambioVlt) {
        int position = Pos.wrifDtCambioVlt(dtCambioVltIdx - 1);
        writeIntAsPacked(position, dtCambioVlt, Len.Int.DT_CAMBIO_VLT);
    }

    /**Original name: WRIF-DT-CAMBIO-VLT<br>*/
    public int getDtCambioVlt(int dtCambioVltIdx) {
        int position = Pos.wrifDtCambioVlt(dtCambioVltIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_CAMBIO_VLT);
    }

    public void setTpFnd(int tpFndIdx, char tpFnd) {
        int position = Pos.wrifTpFnd(tpFndIdx - 1);
        writeChar(position, tpFnd);
    }

    /**Original name: WRIF-TP-FND<br>*/
    public char getTpFnd(int tpFndIdx) {
        int position = Pos.wrifTpFnd(tpFndIdx - 1);
        return readChar(position);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.wrifDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: WRIF-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.wrifDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.wrifDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: WRIF-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.wrifDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.wrifDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: WRIF-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.wrifDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.wrifDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: WRIF-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.wrifDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.wrifDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: WRIF-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.wrifDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.wrifDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: WRIF-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.wrifDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.wrifDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: WRIF-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.wrifDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setDtInvstCalc(int dtInvstCalcIdx, int dtInvstCalc) {
        int position = Pos.wrifDtInvstCalc(dtInvstCalcIdx - 1);
        writeIntAsPacked(position, dtInvstCalc, Len.Int.DT_INVST_CALC);
    }

    /**Original name: WRIF-DT-INVST-CALC<br>*/
    public int getDtInvstCalc(int dtInvstCalcIdx) {
        int position = Pos.wrifDtInvstCalc(dtInvstCalcIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INVST_CALC);
    }

    public void setFlCalcInvto(int flCalcInvtoIdx, char flCalcInvto) {
        int position = Pos.wrifFlCalcInvto(flCalcInvtoIdx - 1);
        writeChar(position, flCalcInvto);
    }

    /**Original name: WRIF-FL-CALC-INVTO<br>*/
    public char getFlCalcInvto(int flCalcInvtoIdx) {
        int position = Pos.wrifFlCalcInvto(flCalcInvtoIdx - 1);
        return readChar(position);
    }

    public void setImpGapEvent(int impGapEventIdx, AfDecimal impGapEvent) {
        int position = Pos.wrifImpGapEvent(impGapEventIdx - 1);
        writeDecimalAsPacked(position, impGapEvent.copy());
    }

    /**Original name: WRIF-IMP-GAP-EVENT<br>*/
    public AfDecimal getImpGapEvent(int impGapEventIdx) {
        int position = Pos.wrifImpGapEvent(impGapEventIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_GAP_EVENT, Len.Fract.IMP_GAP_EVENT);
    }

    public void setFlSwmBp2s(int flSwmBp2sIdx, char flSwmBp2s) {
        int position = Pos.wrifFlSwmBp2s(flSwmBp2sIdx - 1);
        writeChar(position, flSwmBp2s);
    }

    /**Original name: WRIF-FL-SWM-BP2S<br>*/
    public char getFlSwmBp2s(int flSwmBp2sIdx) {
        int position = Pos.wrifFlSwmBp2s(flSwmBp2sIdx - 1);
        return readChar(position);
    }

    public void setRestoTabella(String restoTabella) {
        writeString(Pos.RESTO_TABELLA, restoTabella, Len.RESTO_TABELLA);
    }

    /**Original name: WRIF-RESTO-TABELLA<br>
	 * <pre>          04 WRIF-RESTO-TABELLA      PIC X(239808).</pre>*/
    public String getRestoTabella() {
        return readString(Pos.RESTO_TABELLA, Len.RESTO_TABELLA);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIF_TABELLA = 1;
        public static final int WRIF_TABELLA_R = 1;
        public static final int FLR1 = WRIF_TABELLA_R;
        public static final int RESTO_TABELLA = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wrifTabRicInv(int idx) {
            return WRIF_TABELLA + idx * Len.TAB_RIC_INV;
        }

        public static int wrifStatus(int idx) {
            return wrifTabRicInv(idx);
        }

        public static int wrifIdPtf(int idx) {
            return wrifStatus(idx) + Len.STATUS;
        }

        public static int wrifDati(int idx) {
            return wrifIdPtf(idx) + Len.ID_PTF;
        }

        public static int wrifIdRichInvstFnd(int idx) {
            return wrifDati(idx);
        }

        public static int wrifIdMoviFinrio(int idx) {
            return wrifIdRichInvstFnd(idx) + Len.ID_RICH_INVST_FND;
        }

        public static int wrifIdMoviCrz(int idx) {
            return wrifIdMoviFinrio(idx) + Len.ID_MOVI_FINRIO;
        }

        public static int wrifIdMoviChiu(int idx) {
            return wrifIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int wrifIdMoviChiuNull(int idx) {
            return wrifIdMoviChiu(idx);
        }

        public static int wrifDtIniEff(int idx) {
            return wrifIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int wrifDtEndEff(int idx) {
            return wrifDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int wrifCodCompAnia(int idx) {
            return wrifDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int wrifCodFnd(int idx) {
            return wrifCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wrifCodFndNull(int idx) {
            return wrifCodFnd(idx);
        }

        public static int wrifNumQuo(int idx) {
            return wrifCodFnd(idx) + Len.COD_FND;
        }

        public static int wrifNumQuoNull(int idx) {
            return wrifNumQuo(idx);
        }

        public static int wrifPc(int idx) {
            return wrifNumQuo(idx) + Len.NUM_QUO;
        }

        public static int wrifPcNull(int idx) {
            return wrifPc(idx);
        }

        public static int wrifImpMovto(int idx) {
            return wrifPc(idx) + Len.PC;
        }

        public static int wrifImpMovtoNull(int idx) {
            return wrifImpMovto(idx);
        }

        public static int wrifDtInvst(int idx) {
            return wrifImpMovto(idx) + Len.IMP_MOVTO;
        }

        public static int wrifDtInvstNull(int idx) {
            return wrifDtInvst(idx);
        }

        public static int wrifCodTari(int idx) {
            return wrifDtInvst(idx) + Len.DT_INVST;
        }

        public static int wrifCodTariNull(int idx) {
            return wrifCodTari(idx);
        }

        public static int wrifTpStat(int idx) {
            return wrifCodTari(idx) + Len.COD_TARI;
        }

        public static int wrifTpStatNull(int idx) {
            return wrifTpStat(idx);
        }

        public static int wrifTpModInvst(int idx) {
            return wrifTpStat(idx) + Len.TP_STAT;
        }

        public static int wrifTpModInvstNull(int idx) {
            return wrifTpModInvst(idx);
        }

        public static int wrifCodDiv(int idx) {
            return wrifTpModInvst(idx) + Len.TP_MOD_INVST;
        }

        public static int wrifDtCambioVlt(int idx) {
            return wrifCodDiv(idx) + Len.COD_DIV;
        }

        public static int wrifDtCambioVltNull(int idx) {
            return wrifDtCambioVlt(idx);
        }

        public static int wrifTpFnd(int idx) {
            return wrifDtCambioVlt(idx) + Len.DT_CAMBIO_VLT;
        }

        public static int wrifDsRiga(int idx) {
            return wrifTpFnd(idx) + Len.TP_FND;
        }

        public static int wrifDsOperSql(int idx) {
            return wrifDsRiga(idx) + Len.DS_RIGA;
        }

        public static int wrifDsVer(int idx) {
            return wrifDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int wrifDsTsIniCptz(int idx) {
            return wrifDsVer(idx) + Len.DS_VER;
        }

        public static int wrifDsTsEndCptz(int idx) {
            return wrifDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int wrifDsUtente(int idx) {
            return wrifDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int wrifDsStatoElab(int idx) {
            return wrifDsUtente(idx) + Len.DS_UTENTE;
        }

        public static int wrifDtInvstCalc(int idx) {
            return wrifDsStatoElab(idx) + Len.DS_STATO_ELAB;
        }

        public static int wrifDtInvstCalcNull(int idx) {
            return wrifDtInvstCalc(idx);
        }

        public static int wrifFlCalcInvto(int idx) {
            return wrifDtInvstCalc(idx) + Len.DT_INVST_CALC;
        }

        public static int wrifFlCalcInvtoNull(int idx) {
            return wrifFlCalcInvto(idx);
        }

        public static int wrifImpGapEvent(int idx) {
            return wrifFlCalcInvto(idx) + Len.FL_CALC_INVTO;
        }

        public static int wrifImpGapEventNull(int idx) {
            return wrifImpGapEvent(idx);
        }

        public static int wrifFlSwmBp2s(int idx) {
            return wrifImpGapEvent(idx) + Len.IMP_GAP_EVENT;
        }

        public static int wrifFlSwmBp2sNull(int idx) {
            return wrifFlSwmBp2s(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_RICH_INVST_FND = 5;
        public static final int ID_MOVI_FINRIO = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int COD_FND = 20;
        public static final int NUM_QUO = 7;
        public static final int PC = 4;
        public static final int IMP_MOVTO = 8;
        public static final int DT_INVST = 5;
        public static final int COD_TARI = 12;
        public static final int TP_STAT = 2;
        public static final int TP_MOD_INVST = 2;
        public static final int COD_DIV = 20;
        public static final int DT_CAMBIO_VLT = 5;
        public static final int TP_FND = 1;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int DT_INVST_CALC = 5;
        public static final int FL_CALC_INVTO = 1;
        public static final int IMP_GAP_EVENT = 8;
        public static final int FL_SWM_BP2S = 1;
        public static final int DATI = ID_RICH_INVST_FND + ID_MOVI_FINRIO + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + COD_FND + NUM_QUO + PC + IMP_MOVTO + DT_INVST + COD_TARI + TP_STAT + TP_MOD_INVST + COD_DIV + DT_CAMBIO_VLT + TP_FND + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + DT_INVST_CALC + FL_CALC_INVTO + IMP_GAP_EVENT + FL_SWM_BP2S;
        public static final int TAB_RIC_INV = STATUS + ID_PTF + DATI;
        public static final int FLR1 = 193;
        public static final int WRIF_TABELLA = WrifTabella.TAB_RIC_INV_MAXOCCURS * TAB_RIC_INV;
        public static final int RESTO_TABELLA = 241057;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_RICH_INVST_FND = 9;
            public static final int ID_MOVI_FINRIO = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int NUM_QUO = 7;
            public static final int PC = 3;
            public static final int IMP_MOVTO = 12;
            public static final int DT_INVST = 8;
            public static final int DT_CAMBIO_VLT = 8;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int DT_INVST_CALC = 8;
            public static final int IMP_GAP_EVENT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int NUM_QUO = 5;
            public static final int PC = 3;
            public static final int IMP_MOVTO = 3;
            public static final int IMP_GAP_EVENT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
