package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvpol1Lvvs0002;
import it.accenture.jnais.copy.Ldbv3021;
import it.accenture.jnais.copy.WkTgaMax;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsTpCaus;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.enums.WsTpStatBus;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0007<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0007Data {

    //==== PROPERTIES ====
    public static final int DTGA_TAB_TRAN_MAXOCCURS = 1250;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0007";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: WS-PRE-SOLO-RSH
    private AfDecimal wsPreSoloRsh = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-SOMMA-DIS
    private long wsSommaDis = DefaultValues.LONG_VAL;
    //Original name: WS-IX-TGA
    private int wsIxTga = DefaultValues.INT_VAL;
    //Original name: WS-IX-TGA-MAX
    private int wsIxTgaMax = DefaultValues.INT_VAL;
    //Original name: LDBV1591
    private Ldbv1591 ldbv1591 = new Ldbv1591();
    //Original name: LDBV3021
    private Ldbv3021 ldbv3021 = new Ldbv3021();
    //Original name: DPOL-ELE-POLI-MAX
    private short dpolElePoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1Lvvs0002 lccvpol1 = new Lccvpol1Lvvs0002();
    //Original name: DTGA-ELE-TRAN-MAX
    private short dtgaEleTranMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTGA-TAB-TRAN
    private LazyArrayCopy<W1tgaTabTran> dtgaTabTran = new LazyArrayCopy<W1tgaTabTran>(new W1tgaTabTran(), 1, DTGA_TAB_TRAN_MAXOCCURS);
    /**Original name: WS-TP-CAUS<br>
	 * <pre>----------------------------------------------------------------*
	 * --> ALTRE COPY
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_CAUS (Tipo Causale)
	 * *****************************************************************</pre>*/
    private WsTpCaus wsTpCaus = new WsTpCaus();
    /**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    /**Original name: WS-TP-STAT-BUS<br>
	 * <pre>*****************************************************************
	 *     TP_STAT_BUS (Stato Oggetto di Business)
	 * *****************************************************************</pre>*/
    private WsTpStatBus wsTpStatBus = new WsTpStatBus();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: TRCH-DI-GAR
    private TrchDiGar trchDiGar = new TrchDiGar();
    //Original name: DETT-TIT-CONT
    private DettTitContIdbsdtc0 dettTitCont = new DettTitContIdbsdtc0();
    //Original name: WK-TGA-MAX
    private WkTgaMax wkTgaMax = new WkTgaMax();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-TGA
    private short ixTabTga = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setWsPreSoloRsh(AfDecimal wsPreSoloRsh) {
        this.wsPreSoloRsh.assign(wsPreSoloRsh);
    }

    public AfDecimal getWsPreSoloRsh() {
        return this.wsPreSoloRsh.copy();
    }

    public void setWsSommaDis(long wsSommaDis) {
        this.wsSommaDis = wsSommaDis;
    }

    public long getWsSommaDis() {
        return this.wsSommaDis;
    }

    public void setWsIxTga(int wsIxTga) {
        this.wsIxTga = wsIxTga;
    }

    public int getWsIxTga() {
        return this.wsIxTga;
    }

    public void setWsIxTgaMax(int wsIxTgaMax) {
        this.wsIxTgaMax = wsIxTgaMax;
    }

    public int getWsIxTgaMax() {
        return this.wsIxTgaMax;
    }

    public void setDpolAreaPolizzaFormatted(String data) {
        byte[] buffer = new byte[Len.DPOL_AREA_POLIZZA];
        MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POLIZZA);
        setDpolAreaPolizzaBytes(buffer, 1);
    }

    public void setDpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        dpolElePoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDpolTabPoliBytes(buffer, position);
    }

    public void setDpolElePoliMax(short dpolElePoliMax) {
        this.dpolElePoliMax = dpolElePoliMax;
    }

    public short getDpolElePoliMax() {
        return this.dpolElePoliMax;
    }

    public void setDpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
        position += Lccvpol1Lvvs0002.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setDtgaEleTranMax(short dtgaEleTranMax) {
        this.dtgaEleTranMax = dtgaEleTranMax;
    }

    public short getDtgaEleTranMax() {
        return this.dtgaEleTranMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabTga(short ixTabTga) {
        this.ixTabTga = ixTabTga;
    }

    public short getIxTabTga() {
        return this.ixTabTga;
    }

    public DettTitContIdbsdtc0 getDettTitCont() {
        return dettTitCont;
    }

    public W1tgaTabTran getDtgaTabTran(int idx) {
        return dtgaTabTran.get(idx - 1);
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvpol1Lvvs0002 getLccvpol1() {
        return lccvpol1;
    }

    public Ldbv1591 getLdbv1591() {
        return ldbv1591;
    }

    public Ldbv3021 getLdbv3021() {
        return ldbv3021;
    }

    public TrchDiGar getTrchDiGar() {
        return trchDiGar;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    public WkTgaMax getWkTgaMax() {
        return wkTgaMax;
    }

    public WsTpCaus getWsTpCaus() {
        return wsTpCaus;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    public WsTpStatBus getWsTpStatBus() {
        return wsTpStatBus;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_SOMMA_DIS = 15;
        public static final int WS_IX_TGA = 9;
        public static final int WS_IX_TGA_MAX = 9;
        public static final int DPOL_ELE_POLI_MAX = 2;
        public static final int DPOL_TAB_POLI = WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int DPOL_AREA_POLIZZA = DPOL_ELE_POLI_MAX + DPOL_TAB_POLI;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
