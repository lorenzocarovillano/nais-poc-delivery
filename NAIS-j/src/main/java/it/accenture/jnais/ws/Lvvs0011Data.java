package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvmov1Lvvs0011;
import it.accenture.jnais.copy.WmovDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0011<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0011Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0011";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: INPUT-LVVS0000
    private InputLvvs0000 inputLvvs0000 = new InputLvvs0000();
    //Original name: DMOV-ELE-MOV-MAX
    private short dmovEleMovMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVMOV1
    private Lccvmov1Lvvs0011 lccvmov1 = new Lccvmov1Lvvs0011();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setDmovAreaMovimentoFormatted(String data) {
        byte[] buffer = new byte[Len.DMOV_AREA_MOVIMENTO];
        MarshalByte.writeString(buffer, 1, data, Len.DMOV_AREA_MOVIMENTO);
        setDmovAreaMovimentoBytes(buffer, 1);
    }

    public void setDmovAreaMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        dmovEleMovMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDmovTabMovBytes(buffer, position);
    }

    public void setDmovEleMovMax(short dmovEleMovMax) {
        this.dmovEleMovMax = dmovEleMovMax;
    }

    public short getDmovEleMovMax() {
        return this.dmovEleMovMax;
    }

    public void setDmovTabMovBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvmov1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvmov1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvmov1Lvvs0011.Len.Int.ID_PTF, 0));
        position += Lccvmov1Lvvs0011.Len.ID_PTF;
        lccvmov1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public InputLvvs0000 getInputLvvs0000() {
        return inputLvvs0000;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvmov1Lvvs0011 getLccvmov1() {
        return lccvmov1;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_DATA_APPO1 = 8;
        public static final int DMOV_ELE_MOV_MAX = 2;
        public static final int DMOV_TAB_MOV = WpolStatus.Len.STATUS + Lccvmov1Lvvs0011.Len.ID_PTF + WmovDati.Len.DATI;
        public static final int DMOV_AREA_MOVIMENTO = DMOV_ELE_MOV_MAX + DMOV_TAB_MOV;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
