package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-PROG-TIT<br>
 * Variable: WTDR-PROG-TIT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrProgTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrProgTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_PROG_TIT;
    }

    public void setWtdrProgTit(int wtdrProgTit) {
        writeIntAsPacked(Pos.WTDR_PROG_TIT, wtdrProgTit, Len.Int.WTDR_PROG_TIT);
    }

    /**Original name: WTDR-PROG-TIT<br>*/
    public int getWtdrProgTit() {
        return readPackedAsInt(Pos.WTDR_PROG_TIT, Len.Int.WTDR_PROG_TIT);
    }

    public void setWtdrProgTitNull(String wtdrProgTitNull) {
        writeString(Pos.WTDR_PROG_TIT_NULL, wtdrProgTitNull, Len.WTDR_PROG_TIT_NULL);
    }

    /**Original name: WTDR-PROG-TIT-NULL<br>*/
    public String getWtdrProgTitNull() {
        return readString(Pos.WTDR_PROG_TIT_NULL, Len.WTDR_PROG_TIT_NULL);
    }

    public String getWtdrProgTitNullFormatted() {
        return Functions.padBlanks(getWtdrProgTitNull(), Len.WTDR_PROG_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_PROG_TIT = 1;
        public static final int WTDR_PROG_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_PROG_TIT = 3;
        public static final int WTDR_PROG_TIT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_PROG_TIT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
