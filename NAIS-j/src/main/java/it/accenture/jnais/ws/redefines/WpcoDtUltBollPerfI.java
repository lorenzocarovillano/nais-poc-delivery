package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-PERF-I<br>
 * Variable: WPCO-DT-ULT-BOLL-PERF-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollPerfI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollPerfI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_PERF_I;
    }

    public void setWpcoDtUltBollPerfI(int wpcoDtUltBollPerfI) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_PERF_I, wpcoDtUltBollPerfI, Len.Int.WPCO_DT_ULT_BOLL_PERF_I);
    }

    public void setDpcoDtUltBollPerfIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_PERF_I, Pos.WPCO_DT_ULT_BOLL_PERF_I);
    }

    /**Original name: WPCO-DT-ULT-BOLL-PERF-I<br>*/
    public int getWpcoDtUltBollPerfI() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_PERF_I, Len.Int.WPCO_DT_ULT_BOLL_PERF_I);
    }

    public byte[] getWpcoDtUltBollPerfIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_PERF_I, Pos.WPCO_DT_ULT_BOLL_PERF_I);
        return buffer;
    }

    public void setWpcoDtUltBollPerfINull(String wpcoDtUltBollPerfINull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_PERF_I_NULL, wpcoDtUltBollPerfINull, Len.WPCO_DT_ULT_BOLL_PERF_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-PERF-I-NULL<br>*/
    public String getWpcoDtUltBollPerfINull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_PERF_I_NULL, Len.WPCO_DT_ULT_BOLL_PERF_I_NULL);
    }

    public String getWpcoDtUltBollPerfINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollPerfINull(), Len.WPCO_DT_ULT_BOLL_PERF_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_PERF_I = 1;
        public static final int WPCO_DT_ULT_BOLL_PERF_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_PERF_I = 5;
        public static final int WPCO_DT_ULT_BOLL_PERF_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_PERF_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
