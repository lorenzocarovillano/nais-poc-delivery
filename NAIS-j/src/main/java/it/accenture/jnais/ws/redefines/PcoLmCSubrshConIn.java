package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-LM-C-SUBRSH-CON-IN<br>
 * Variable: PCO-LM-C-SUBRSH-CON-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoLmCSubrshConIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoLmCSubrshConIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_LM_C_SUBRSH_CON_IN;
    }

    public void setPcoLmCSubrshConIn(AfDecimal pcoLmCSubrshConIn) {
        writeDecimalAsPacked(Pos.PCO_LM_C_SUBRSH_CON_IN, pcoLmCSubrshConIn.copy());
    }

    public void setPcoLmCSubrshConInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_LM_C_SUBRSH_CON_IN, Pos.PCO_LM_C_SUBRSH_CON_IN);
    }

    /**Original name: PCO-LM-C-SUBRSH-CON-IN<br>*/
    public AfDecimal getPcoLmCSubrshConIn() {
        return readPackedAsDecimal(Pos.PCO_LM_C_SUBRSH_CON_IN, Len.Int.PCO_LM_C_SUBRSH_CON_IN, Len.Fract.PCO_LM_C_SUBRSH_CON_IN);
    }

    public byte[] getPcoLmCSubrshConInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_LM_C_SUBRSH_CON_IN, Pos.PCO_LM_C_SUBRSH_CON_IN);
        return buffer;
    }

    public void initPcoLmCSubrshConInHighValues() {
        fill(Pos.PCO_LM_C_SUBRSH_CON_IN, Len.PCO_LM_C_SUBRSH_CON_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoLmCSubrshConInNull(String pcoLmCSubrshConInNull) {
        writeString(Pos.PCO_LM_C_SUBRSH_CON_IN_NULL, pcoLmCSubrshConInNull, Len.PCO_LM_C_SUBRSH_CON_IN_NULL);
    }

    /**Original name: PCO-LM-C-SUBRSH-CON-IN-NULL<br>*/
    public String getPcoLmCSubrshConInNull() {
        return readString(Pos.PCO_LM_C_SUBRSH_CON_IN_NULL, Len.PCO_LM_C_SUBRSH_CON_IN_NULL);
    }

    public String getPcoLmCSubrshConInNullFormatted() {
        return Functions.padBlanks(getPcoLmCSubrshConInNull(), Len.PCO_LM_C_SUBRSH_CON_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_LM_C_SUBRSH_CON_IN = 1;
        public static final int PCO_LM_C_SUBRSH_CON_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_LM_C_SUBRSH_CON_IN = 4;
        public static final int PCO_LM_C_SUBRSH_CON_IN_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_LM_C_SUBRSH_CON_IN = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_LM_C_SUBRSH_CON_IN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
