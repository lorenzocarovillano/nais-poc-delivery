package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PRE-ANNUALIZ-RICOR<br>
 * Variable: W-B03-PRE-ANNUALIZ-RICOR from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03PreAnnualizRicorLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03PreAnnualizRicorLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PRE_ANNUALIZ_RICOR;
    }

    public void setwB03PreAnnualizRicor(AfDecimal wB03PreAnnualizRicor) {
        writeDecimalAsPacked(Pos.W_B03_PRE_ANNUALIZ_RICOR, wB03PreAnnualizRicor.copy());
    }

    /**Original name: W-B03-PRE-ANNUALIZ-RICOR<br>*/
    public AfDecimal getwB03PreAnnualizRicor() {
        return readPackedAsDecimal(Pos.W_B03_PRE_ANNUALIZ_RICOR, Len.Int.W_B03_PRE_ANNUALIZ_RICOR, Len.Fract.W_B03_PRE_ANNUALIZ_RICOR);
    }

    public byte[] getwB03PreAnnualizRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PRE_ANNUALIZ_RICOR, Pos.W_B03_PRE_ANNUALIZ_RICOR);
        return buffer;
    }

    public void setwB03PreAnnualizRicorNull(String wB03PreAnnualizRicorNull) {
        writeString(Pos.W_B03_PRE_ANNUALIZ_RICOR_NULL, wB03PreAnnualizRicorNull, Len.W_B03_PRE_ANNUALIZ_RICOR_NULL);
    }

    /**Original name: W-B03-PRE-ANNUALIZ-RICOR-NULL<br>*/
    public String getwB03PreAnnualizRicorNull() {
        return readString(Pos.W_B03_PRE_ANNUALIZ_RICOR_NULL, Len.W_B03_PRE_ANNUALIZ_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_ANNUALIZ_RICOR = 1;
        public static final int W_B03_PRE_ANNUALIZ_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_ANNUALIZ_RICOR = 8;
        public static final int W_B03_PRE_ANNUALIZ_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_ANNUALIZ_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_ANNUALIZ_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
