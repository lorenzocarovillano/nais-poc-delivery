package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-INT-RETRODT<br>
 * Variable: WPAG-RATA-INT-RETRODT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataIntRetrodt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataIntRetrodt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_INT_RETRODT;
    }

    public void setWpagRataIntRetrodt(AfDecimal wpagRataIntRetrodt) {
        writeDecimalAsPacked(Pos.WPAG_RATA_INT_RETRODT, wpagRataIntRetrodt.copy());
    }

    public void setWpagRataIntRetrodtFormatted(String wpagRataIntRetrodt) {
        setWpagRataIntRetrodt(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_INT_RETRODT + Len.Fract.WPAG_RATA_INT_RETRODT, Len.Fract.WPAG_RATA_INT_RETRODT, wpagRataIntRetrodt));
    }

    public void setWpagRataIntRetrodtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_INT_RETRODT, Pos.WPAG_RATA_INT_RETRODT);
    }

    /**Original name: WPAG-RATA-INT-RETRODT<br>*/
    public AfDecimal getWpagRataIntRetrodt() {
        return readPackedAsDecimal(Pos.WPAG_RATA_INT_RETRODT, Len.Int.WPAG_RATA_INT_RETRODT, Len.Fract.WPAG_RATA_INT_RETRODT);
    }

    public byte[] getWpagRataIntRetrodtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_INT_RETRODT, Pos.WPAG_RATA_INT_RETRODT);
        return buffer;
    }

    public void initWpagRataIntRetrodtSpaces() {
        fill(Pos.WPAG_RATA_INT_RETRODT, Len.WPAG_RATA_INT_RETRODT, Types.SPACE_CHAR);
    }

    public void setWpagRataIntRetrodtNull(String wpagRataIntRetrodtNull) {
        writeString(Pos.WPAG_RATA_INT_RETRODT_NULL, wpagRataIntRetrodtNull, Len.WPAG_RATA_INT_RETRODT_NULL);
    }

    /**Original name: WPAG-RATA-INT-RETRODT-NULL<br>*/
    public String getWpagRataIntRetrodtNull() {
        return readString(Pos.WPAG_RATA_INT_RETRODT_NULL, Len.WPAG_RATA_INT_RETRODT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_INT_RETRODT = 1;
        public static final int WPAG_RATA_INT_RETRODT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_INT_RETRODT = 8;
        public static final int WPAG_RATA_INT_RETRODT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_INT_RETRODT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_INT_RETRODT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
