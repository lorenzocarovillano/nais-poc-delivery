package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRE-RSH-T<br>
 * Variable: B03-PRE-RSH-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PreRshT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PreRshT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRE_RSH_T;
    }

    public void setB03PreRshT(AfDecimal b03PreRshT) {
        writeDecimalAsPacked(Pos.B03_PRE_RSH_T, b03PreRshT.copy());
    }

    public void setB03PreRshTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRE_RSH_T, Pos.B03_PRE_RSH_T);
    }

    /**Original name: B03-PRE-RSH-T<br>*/
    public AfDecimal getB03PreRshT() {
        return readPackedAsDecimal(Pos.B03_PRE_RSH_T, Len.Int.B03_PRE_RSH_T, Len.Fract.B03_PRE_RSH_T);
    }

    public byte[] getB03PreRshTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRE_RSH_T, Pos.B03_PRE_RSH_T);
        return buffer;
    }

    public void setB03PreRshTNull(String b03PreRshTNull) {
        writeString(Pos.B03_PRE_RSH_T_NULL, b03PreRshTNull, Len.B03_PRE_RSH_T_NULL);
    }

    /**Original name: B03-PRE-RSH-T-NULL<br>*/
    public String getB03PreRshTNull() {
        return readString(Pos.B03_PRE_RSH_T_NULL, Len.B03_PRE_RSH_T_NULL);
    }

    public String getB03PreRshTNullFormatted() {
        return Functions.padBlanks(getB03PreRshTNull(), Len.B03_PRE_RSH_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRE_RSH_T = 1;
        public static final int B03_PRE_RSH_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRE_RSH_T = 8;
        public static final int B03_PRE_RSH_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRE_RSH_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRE_RSH_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
