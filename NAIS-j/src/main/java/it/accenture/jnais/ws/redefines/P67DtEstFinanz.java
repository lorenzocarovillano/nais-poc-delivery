package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-DT-EST-FINANZ<br>
 * Variable: P67-DT-EST-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67DtEstFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67DtEstFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_DT_EST_FINANZ;
    }

    public void setP67DtEstFinanz(int p67DtEstFinanz) {
        writeIntAsPacked(Pos.P67_DT_EST_FINANZ, p67DtEstFinanz, Len.Int.P67_DT_EST_FINANZ);
    }

    public void setP67DtEstFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_DT_EST_FINANZ, Pos.P67_DT_EST_FINANZ);
    }

    /**Original name: P67-DT-EST-FINANZ<br>*/
    public int getP67DtEstFinanz() {
        return readPackedAsInt(Pos.P67_DT_EST_FINANZ, Len.Int.P67_DT_EST_FINANZ);
    }

    public byte[] getP67DtEstFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_DT_EST_FINANZ, Pos.P67_DT_EST_FINANZ);
        return buffer;
    }

    public void setP67DtEstFinanzNull(String p67DtEstFinanzNull) {
        writeString(Pos.P67_DT_EST_FINANZ_NULL, p67DtEstFinanzNull, Len.P67_DT_EST_FINANZ_NULL);
    }

    /**Original name: P67-DT-EST-FINANZ-NULL<br>*/
    public String getP67DtEstFinanzNull() {
        return readString(Pos.P67_DT_EST_FINANZ_NULL, Len.P67_DT_EST_FINANZ_NULL);
    }

    public String getP67DtEstFinanzNullFormatted() {
        return Functions.padBlanks(getP67DtEstFinanzNull(), Len.P67_DT_EST_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_DT_EST_FINANZ = 1;
        public static final int P67_DT_EST_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_DT_EST_FINANZ = 5;
        public static final int P67_DT_EST_FINANZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_DT_EST_FINANZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
