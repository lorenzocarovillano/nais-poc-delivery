package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.WpagModIas;

/**Original name: WPAG-TAB-IAS<br>
 * Variables: WPAG-TAB-IAS from copybook LVEC0069<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpagTabIas {

    //==== PROPERTIES ====
    //Original name: WPAG-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: WPAG-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: WPAG-TP-IAS-OLD
    private String tpIasOld = DefaultValues.stringVal(Len.TP_IAS_OLD);
    //Original name: WPAG-TP-IAS-NEW
    private String tpIasNew = DefaultValues.stringVal(Len.TP_IAS_NEW);
    //Original name: WPAG-MOD-IAS
    private WpagModIas modIas = new WpagModIas();

    //==== METHODS ====
    public void setTabIasBytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpIasOld = MarshalByte.readString(buffer, position, Len.TP_IAS_OLD);
        position += Len.TP_IAS_OLD;
        tpIasNew = MarshalByte.readString(buffer, position, Len.TP_IAS_NEW);
        position += Len.TP_IAS_NEW;
        modIas.setModIas(MarshalByte.readChar(buffer, position));
    }

    public byte[] getTabIasBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tpIasOld, Len.TP_IAS_OLD);
        position += Len.TP_IAS_OLD;
        MarshalByte.writeString(buffer, position, tpIasNew, Len.TP_IAS_NEW);
        position += Len.TP_IAS_NEW;
        MarshalByte.writeChar(buffer, position, modIas.getModIas());
        return buffer;
    }

    public void initTabIasSpaces() {
        idOgg = Types.INVALID_INT_VAL;
        tpOgg = "";
        tpIasOld = "";
        tpIasNew = "";
        modIas.setModIas(Types.SPACE_CHAR);
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpIasOld(String tpIasOld) {
        this.tpIasOld = Functions.subString(tpIasOld, Len.TP_IAS_OLD);
    }

    public String getTpIasOld() {
        return this.tpIasOld;
    }

    public void setTpIasNew(String tpIasNew) {
        this.tpIasNew = Functions.subString(tpIasNew, Len.TP_IAS_NEW);
    }

    public String getTpIasNew() {
        return this.tpIasNew;
    }

    public WpagModIas getModIas() {
        return modIas;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int TP_IAS_OLD = 2;
        public static final int TP_IAS_NEW = 2;
        public static final int TAB_IAS = ID_OGG + TP_OGG + TP_IAS_OLD + TP_IAS_NEW + WpagModIas.Len.MOD_IAS;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
