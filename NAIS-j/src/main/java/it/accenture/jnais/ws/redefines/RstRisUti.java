package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-UTI<br>
 * Variable: RST-RIS-UTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisUti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisUti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_UTI;
    }

    public void setRstRisUti(AfDecimal rstRisUti) {
        writeDecimalAsPacked(Pos.RST_RIS_UTI, rstRisUti.copy());
    }

    public void setRstRisUtiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_UTI, Pos.RST_RIS_UTI);
    }

    /**Original name: RST-RIS-UTI<br>*/
    public AfDecimal getRstRisUti() {
        return readPackedAsDecimal(Pos.RST_RIS_UTI, Len.Int.RST_RIS_UTI, Len.Fract.RST_RIS_UTI);
    }

    public byte[] getRstRisUtiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_UTI, Pos.RST_RIS_UTI);
        return buffer;
    }

    public void setRstRisUtiNull(String rstRisUtiNull) {
        writeString(Pos.RST_RIS_UTI_NULL, rstRisUtiNull, Len.RST_RIS_UTI_NULL);
    }

    /**Original name: RST-RIS-UTI-NULL<br>*/
    public String getRstRisUtiNull() {
        return readString(Pos.RST_RIS_UTI_NULL, Len.RST_RIS_UTI_NULL);
    }

    public String getRstRisUtiNullFormatted() {
        return Functions.padBlanks(getRstRisUtiNull(), Len.RST_RIS_UTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_UTI = 1;
        public static final int RST_RIS_UTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_UTI = 8;
        public static final int RST_RIS_UTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_UTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_UTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
