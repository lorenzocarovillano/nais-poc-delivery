package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LDBV2971<br>
 * Variable: LDBV2971 from copybook LDBV2971<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv2971 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV2971-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV2971-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV2971-DT-INI-COP
    private String dtIniCop = DefaultValues.stringVal(Len.DT_INI_COP);
    //Original name: LDBV2971-FRAZ
    private int fraz = DefaultValues.INT_VAL;
    //Original name: LDBV2971-NUM-RAT-ACCORPATE
    private int numRatAccorpate = DefaultValues.INT_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV2971;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv2971Bytes(buf);
    }

    public String getLdbv2971Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv2971Bytes());
    }

    public void setLdbv2971Bytes(byte[] buffer) {
        setLdbv2971Bytes(buffer, 1);
    }

    public byte[] getLdbv2971Bytes() {
        byte[] buffer = new byte[Len.LDBV2971];
        return getLdbv2971Bytes(buffer, 1);
    }

    public void setLdbv2971Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        dtIniCop = MarshalByte.readFixedString(buffer, position, Len.DT_INI_COP);
        position += Len.DT_INI_COP;
        fraz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.FRAZ, 0);
        position += Len.FRAZ;
        numRatAccorpate = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_RAT_ACCORPATE, 0);
    }

    public byte[] getLdbv2971Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, dtIniCop, Len.DT_INI_COP);
        position += Len.DT_INI_COP;
        MarshalByte.writeIntAsPacked(buffer, position, fraz, Len.Int.FRAZ, 0);
        position += Len.FRAZ;
        MarshalByte.writeIntAsPacked(buffer, position, numRatAccorpate, Len.Int.NUM_RAT_ACCORPATE, 0);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setDtIniCopFormatted(String dtIniCop) {
        this.dtIniCop = Trunc.toUnsignedNumeric(dtIniCop, Len.DT_INI_COP);
    }

    public int getDtIniCop() {
        return NumericDisplay.asInt(this.dtIniCop);
    }

    public String getDtIniCopFormatted() {
        return this.dtIniCop;
    }

    public void setFraz(int fraz) {
        this.fraz = fraz;
    }

    public int getFraz() {
        return this.fraz;
    }

    public void setNumRatAccorpate(int numRatAccorpate) {
        this.numRatAccorpate = numRatAccorpate;
    }

    public int getNumRatAccorpate() {
        return this.numRatAccorpate;
    }

    @Override
    public byte[] serialize() {
        return getLdbv2971Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int DT_INI_COP = 8;
        public static final int FRAZ = 3;
        public static final int NUM_RAT_ACCORPATE = 3;
        public static final int LDBV2971 = ID_OGG + TP_OGG + DT_INI_COP + FRAZ + NUM_RAT_ACCORPATE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int FRAZ = 5;
            public static final int NUM_RAT_ACCORPATE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
