package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LDBV4021<br>
 * Variable: LDBV4021 from copybook LDBV4021<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv4021 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV4021-ID-TGA
    private int idTga = DefaultValues.INT_VAL;
    //Original name: LDBV4021-PRE-LRD
    private AfDecimal preLrd = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LDBV4021-DATA-INIZIO
    private String dataInizio = DefaultValues.stringVal(Len.DATA_INIZIO);
    //Original name: LDBV4021-DATA-FINE
    private String dataFine = DefaultValues.stringVal(Len.DATA_FINE);
    //Original name: LDBV4021-DATA-INIZIO-DB
    private String dataInizioDb = DefaultValues.stringVal(Len.DATA_INIZIO_DB);
    //Original name: LDBV4021-DATA-FINE-DB
    private String dataFineDb = DefaultValues.stringVal(Len.DATA_FINE_DB);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV4021;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv4021Bytes(buf);
    }

    public String getLdbv4021Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv4021Bytes());
    }

    public void setLdbv4021Bytes(byte[] buffer) {
        setLdbv4021Bytes(buffer, 1);
    }

    public byte[] getLdbv4021Bytes() {
        byte[] buffer = new byte[Len.LDBV4021];
        return getLdbv4021Bytes(buffer, 1);
    }

    public void setLdbv4021Bytes(byte[] buffer, int offset) {
        int position = offset;
        idTga = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_TGA, 0);
        position += Len.ID_TGA;
        preLrd.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_LRD, Len.Fract.PRE_LRD));
        position += Len.PRE_LRD;
        dataInizio = MarshalByte.readFixedString(buffer, position, Len.DATA_INIZIO);
        position += Len.DATA_INIZIO;
        dataFine = MarshalByte.readFixedString(buffer, position, Len.DATA_FINE);
        position += Len.DATA_FINE;
        dataInizioDb = MarshalByte.readString(buffer, position, Len.DATA_INIZIO_DB);
        position += Len.DATA_INIZIO_DB;
        dataFineDb = MarshalByte.readString(buffer, position, Len.DATA_FINE_DB);
    }

    public byte[] getLdbv4021Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idTga, Len.Int.ID_TGA, 0);
        position += Len.ID_TGA;
        MarshalByte.writeDecimalAsPacked(buffer, position, preLrd.copy());
        position += Len.PRE_LRD;
        MarshalByte.writeString(buffer, position, dataInizio, Len.DATA_INIZIO);
        position += Len.DATA_INIZIO;
        MarshalByte.writeString(buffer, position, dataFine, Len.DATA_FINE);
        position += Len.DATA_FINE;
        MarshalByte.writeString(buffer, position, dataInizioDb, Len.DATA_INIZIO_DB);
        position += Len.DATA_INIZIO_DB;
        MarshalByte.writeString(buffer, position, dataFineDb, Len.DATA_FINE_DB);
        return buffer;
    }

    public void setIdTga(int idTga) {
        this.idTga = idTga;
    }

    public int getIdTga() {
        return this.idTga;
    }

    public void setPreLrd(AfDecimal preLrd) {
        this.preLrd.assign(preLrd);
    }

    public AfDecimal getPreLrd() {
        return this.preLrd.copy();
    }

    public void setDataInizioFormatted(String dataInizio) {
        this.dataInizio = Trunc.toUnsignedNumeric(dataInizio, Len.DATA_INIZIO);
    }

    public int getDataInizio() {
        return NumericDisplay.asInt(this.dataInizio);
    }

    public String getDataInizioFormatted() {
        return this.dataInizio;
    }

    public void setDataFineFormatted(String dataFine) {
        this.dataFine = Trunc.toUnsignedNumeric(dataFine, Len.DATA_FINE);
    }

    public int getDataFine() {
        return NumericDisplay.asInt(this.dataFine);
    }

    public String getDataFineFormatted() {
        return this.dataFine;
    }

    public void setDataInizioDb(String dataInizioDb) {
        this.dataInizioDb = Functions.subString(dataInizioDb, Len.DATA_INIZIO_DB);
    }

    public String getDataInizioDb() {
        return this.dataInizioDb;
    }

    public void setDataFineDb(String dataFineDb) {
        this.dataFineDb = Functions.subString(dataFineDb, Len.DATA_FINE_DB);
    }

    public String getDataFineDb() {
        return this.dataFineDb;
    }

    @Override
    public byte[] serialize() {
        return getLdbv4021Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_TGA = 5;
        public static final int PRE_LRD = 8;
        public static final int DATA_INIZIO = 8;
        public static final int DATA_FINE = 8;
        public static final int DATA_INIZIO_DB = 10;
        public static final int DATA_FINE_DB = 10;
        public static final int LDBV4021 = ID_TGA + PRE_LRD + DATA_INIZIO + DATA_FINE + DATA_INIZIO_DB + DATA_FINE_DB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_TGA = 9;
            public static final int PRE_LRD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_LRD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
