package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-SNDEN<br>
 * Variable: WPCO-DT-ULT-BOLL-SNDEN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollSnden extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollSnden() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_SNDEN;
    }

    public void setWpcoDtUltBollSnden(int wpcoDtUltBollSnden) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_SNDEN, wpcoDtUltBollSnden, Len.Int.WPCO_DT_ULT_BOLL_SNDEN);
    }

    public void setDpcoDtUltBollSndenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_SNDEN, Pos.WPCO_DT_ULT_BOLL_SNDEN);
    }

    /**Original name: WPCO-DT-ULT-BOLL-SNDEN<br>*/
    public int getWpcoDtUltBollSnden() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_SNDEN, Len.Int.WPCO_DT_ULT_BOLL_SNDEN);
    }

    public byte[] getWpcoDtUltBollSndenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_SNDEN, Pos.WPCO_DT_ULT_BOLL_SNDEN);
        return buffer;
    }

    public void setWpcoDtUltBollSndenNull(String wpcoDtUltBollSndenNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_SNDEN_NULL, wpcoDtUltBollSndenNull, Len.WPCO_DT_ULT_BOLL_SNDEN_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-SNDEN-NULL<br>*/
    public String getWpcoDtUltBollSndenNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_SNDEN_NULL, Len.WPCO_DT_ULT_BOLL_SNDEN_NULL);
    }

    public String getWpcoDtUltBollSndenNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollSndenNull(), Len.WPCO_DT_ULT_BOLL_SNDEN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_SNDEN = 1;
        public static final int WPCO_DT_ULT_BOLL_SNDEN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_SNDEN = 5;
        public static final int WPCO_DT_ULT_BOLL_SNDEN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_SNDEN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
