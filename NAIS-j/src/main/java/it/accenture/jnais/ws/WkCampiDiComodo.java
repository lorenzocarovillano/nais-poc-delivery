package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.WkReturnCode;
import it.accenture.jnais.ws.redefines.WkDataInferiore;
import it.accenture.jnais.ws.redefines.WkDataSuperiore;

/**Original name: WK-CAMPI-DI-COMODO<br>
 * Variable: WK-CAMPI-DI-COMODO from program LCCS1750<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WkCampiDiComodo extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WK-DATA-INFERIORE
    private WkDataInferiore wkDataInferiore = new WkDataInferiore();
    //Original name: WK-DATA-SUPERIORE
    private WkDataSuperiore wkDataSuperiore = new WkDataSuperiore();
    /**Original name: WK-CALL-PGM<br>
	 * <pre>-- AREA ROUTINE PER IL CALCOLO</pre>*/
    private String wkCallPgm = "LCCS0010";
    //Original name: WK-COD-SERVIZIO-BE
    private String wkCodServizioBe = DefaultValues.stringVal(Len.WK_COD_SERVIZIO_BE);
    //Original name: WK-AA-DIFF
    private String wkAaDiff = DefaultValues.stringVal(Len.WK_AA_DIFF);
    //Original name: WK-RESTO
    private String wkResto = "00";
    //Original name: WK-INTERO
    private String wkIntero = "00000";
    //Original name: WK-FORMATO
    private char wkFormato = 'M';
    //Original name: WK-CODICE-RITORNO
    private char wkCodiceRitorno = DefaultValues.CHAR_VAL;
    //Original name: WK-NUM-GIORNI
    private int wkNumGiorni = DefaultValues.INT_VAL;
    //Original name: WK-MM-DIFF
    private String wkMmDiff = DefaultValues.stringVal(Len.WK_MM_DIFF);
    //Original name: WK-RETURN-CODE
    private WkReturnCode wkReturnCode = new WkReturnCode();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_CAMPI_DI_COMODO;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWkCampiDiComodoBytes(buf);
    }

    public String getWkCampiDiComodoFormatted() {
        return MarshalByteExt.bufferToStr(getWkCampiDiComodoBytes());
    }

    public void setWkCampiDiComodoBytes(byte[] buffer) {
        setWkCampiDiComodoBytes(buffer, 1);
    }

    public byte[] getWkCampiDiComodoBytes() {
        byte[] buffer = new byte[Len.WK_CAMPI_DI_COMODO];
        return getWkCampiDiComodoBytes(buffer, 1);
    }

    public void setWkCampiDiComodoBytes(byte[] buffer, int offset) {
        int position = offset;
        wkDataInferiore.setWkDataInferioreBytes(buffer, position);
        position += WkDataInferiore.Len.WK_DATA_INFERIORE;
        wkDataSuperiore.setWkDataSuperioreBytes(buffer, position);
        position += WkDataSuperiore.Len.WK_DATA_SUPERIORE;
        wkCallPgm = MarshalByte.readString(buffer, position, Len.WK_CALL_PGM);
        position += Len.WK_CALL_PGM;
        wkCodServizioBe = MarshalByte.readString(buffer, position, Len.WK_COD_SERVIZIO_BE);
        position += Len.WK_COD_SERVIZIO_BE;
        wkAaDiff = MarshalByte.readFixedString(buffer, position, Len.WK_AA_DIFF);
        position += Len.WK_AA_DIFF;
        wkResto = MarshalByte.readFixedString(buffer, position, Len.WK_RESTO);
        position += Len.WK_RESTO;
        wkIntero = MarshalByte.readFixedString(buffer, position, Len.WK_INTERO);
        position += Len.WK_INTERO;
        wkFormato = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wkCodiceRitorno = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wkNumGiorni = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WK_NUM_GIORNI, 0);
        position += Len.WK_NUM_GIORNI;
        wkMmDiff = MarshalByte.readFixedString(buffer, position, Len.WK_MM_DIFF);
        position += Len.WK_MM_DIFF;
        wkReturnCode.setWkReturnCode(MarshalByte.readString(buffer, position, WkReturnCode.Len.WK_RETURN_CODE));
    }

    public byte[] getWkCampiDiComodoBytes(byte[] buffer, int offset) {
        int position = offset;
        wkDataInferiore.getWkDataInferioreBytes(buffer, position);
        position += WkDataInferiore.Len.WK_DATA_INFERIORE;
        wkDataSuperiore.getWkDataSuperioreBytes(buffer, position);
        position += WkDataSuperiore.Len.WK_DATA_SUPERIORE;
        MarshalByte.writeString(buffer, position, wkCallPgm, Len.WK_CALL_PGM);
        position += Len.WK_CALL_PGM;
        MarshalByte.writeString(buffer, position, wkCodServizioBe, Len.WK_COD_SERVIZIO_BE);
        position += Len.WK_COD_SERVIZIO_BE;
        MarshalByte.writeString(buffer, position, wkAaDiff, Len.WK_AA_DIFF);
        position += Len.WK_AA_DIFF;
        MarshalByte.writeString(buffer, position, wkResto, Len.WK_RESTO);
        position += Len.WK_RESTO;
        MarshalByte.writeString(buffer, position, wkIntero, Len.WK_INTERO);
        position += Len.WK_INTERO;
        MarshalByte.writeChar(buffer, position, wkFormato);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wkCodiceRitorno);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wkNumGiorni, Len.Int.WK_NUM_GIORNI, 0);
        position += Len.WK_NUM_GIORNI;
        MarshalByte.writeString(buffer, position, wkMmDiff, Len.WK_MM_DIFF);
        position += Len.WK_MM_DIFF;
        MarshalByte.writeString(buffer, position, wkReturnCode.getWkReturnCode(), WkReturnCode.Len.WK_RETURN_CODE);
        return buffer;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public String getWkCallPgmFormatted() {
        return Functions.padBlanks(getWkCallPgm(), Len.WK_CALL_PGM);
    }

    public void setWkCodServizioBe(String wkCodServizioBe) {
        this.wkCodServizioBe = Functions.subString(wkCodServizioBe, Len.WK_COD_SERVIZIO_BE);
    }

    public String getWkCodServizioBe() {
        return this.wkCodServizioBe;
    }

    public void setWkResto(short wkResto) {
        this.wkResto = NumericDisplay.asString(wkResto, Len.WK_RESTO);
    }

    public short getWkResto() {
        return NumericDisplay.asShort(this.wkResto);
    }

    public String getWkRestoFormatted() {
        return this.wkResto;
    }

    public void setWkIntero(int wkIntero) {
        this.wkIntero = NumericDisplay.asString(wkIntero, Len.WK_INTERO);
    }

    public int getWkIntero() {
        return NumericDisplay.asInt(this.wkIntero);
    }

    public String getWkInteroFormatted() {
        return this.wkIntero;
    }

    public void setWkFormato(char wkFormato) {
        this.wkFormato = wkFormato;
    }

    public void setWkFormatoFromBuffer(byte[] buffer) {
        wkFormato = MarshalByte.readChar(buffer, 1);
    }

    public char getWkFormato() {
        return this.wkFormato;
    }

    public void setWkCodiceRitorno(char wkCodiceRitorno) {
        this.wkCodiceRitorno = wkCodiceRitorno;
    }

    public void setWkCodiceRitornoFromBuffer(byte[] buffer) {
        wkCodiceRitorno = MarshalByte.readChar(buffer, 1);
    }

    public char getWkCodiceRitorno() {
        return this.wkCodiceRitorno;
    }

    public void setWkNumGiorni(int wkNumGiorni) {
        this.wkNumGiorni = wkNumGiorni;
    }

    public int getWkNumGiorni() {
        return this.wkNumGiorni;
    }

    public void setWkMmDiff(int wkMmDiff) {
        this.wkMmDiff = NumericDisplay.asString(wkMmDiff, Len.WK_MM_DIFF);
    }

    public void setWkMmDiffFromBuffer(byte[] buffer) {
        wkMmDiff = MarshalByte.readFixedString(buffer, 1, Len.WK_MM_DIFF);
    }

    public int getWkMmDiff() {
        return NumericDisplay.asInt(this.wkMmDiff);
    }

    public String getWkMmDiffFormatted() {
        return this.wkMmDiff;
    }

    public WkDataInferiore getWkDataInferiore() {
        return wkDataInferiore;
    }

    public WkDataSuperiore getWkDataSuperiore() {
        return wkDataSuperiore;
    }

    public WkReturnCode getWkReturnCode() {
        return wkReturnCode;
    }

    @Override
    public byte[] serialize() {
        return getWkCampiDiComodoBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_CALL_PGM = 8;
        public static final int WK_COD_SERVIZIO_BE = 8;
        public static final int WK_AA_DIFF = 5;
        public static final int WK_RESTO = 2;
        public static final int WK_INTERO = 5;
        public static final int WK_FORMATO = 1;
        public static final int WK_CODICE_RITORNO = 1;
        public static final int WK_NUM_GIORNI = 5;
        public static final int WK_MM_DIFF = 5;
        public static final int WK_CAMPI_DI_COMODO = WkDataInferiore.Len.WK_DATA_INFERIORE + WkDataSuperiore.Len.WK_DATA_SUPERIORE + WK_CALL_PGM + WK_COD_SERVIZIO_BE + WK_AA_DIFF + WK_RESTO + WK_INTERO + WK_FORMATO + WK_CODICE_RITORNO + WK_NUM_GIORNI + WK_MM_DIFF + WkReturnCode.Len.WK_RETURN_CODE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_NUM_GIORNI = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
