package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndQuotzFndUnit;
import it.accenture.jnais.copy.RichDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBSE590<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbse590Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-IMPST-BOLLO
    private IndQuotzFndUnit indImpstBollo = new IndQuotzFndUnit();
    //Original name: IMPST-BOLLO-DB
    private RichDb impstBolloDb = new RichDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public RichDb getImpstBolloDb() {
        return impstBolloDb;
    }

    public IndQuotzFndUnit getIndImpstBollo() {
        return indImpstBollo;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
