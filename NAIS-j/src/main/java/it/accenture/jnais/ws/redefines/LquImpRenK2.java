package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-REN-K2<br>
 * Variable: LQU-IMP-REN-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpRenK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpRenK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_REN_K2;
    }

    public void setLquImpRenK2(AfDecimal lquImpRenK2) {
        writeDecimalAsPacked(Pos.LQU_IMP_REN_K2, lquImpRenK2.copy());
    }

    public void setLquImpRenK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_REN_K2, Pos.LQU_IMP_REN_K2);
    }

    /**Original name: LQU-IMP-REN-K2<br>*/
    public AfDecimal getLquImpRenK2() {
        return readPackedAsDecimal(Pos.LQU_IMP_REN_K2, Len.Int.LQU_IMP_REN_K2, Len.Fract.LQU_IMP_REN_K2);
    }

    public byte[] getLquImpRenK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_REN_K2, Pos.LQU_IMP_REN_K2);
        return buffer;
    }

    public void setLquImpRenK2Null(String lquImpRenK2Null) {
        writeString(Pos.LQU_IMP_REN_K2_NULL, lquImpRenK2Null, Len.LQU_IMP_REN_K2_NULL);
    }

    /**Original name: LQU-IMP-REN-K2-NULL<br>*/
    public String getLquImpRenK2Null() {
        return readString(Pos.LQU_IMP_REN_K2_NULL, Len.LQU_IMP_REN_K2_NULL);
    }

    public String getLquImpRenK2NullFormatted() {
        return Functions.padBlanks(getLquImpRenK2Null(), Len.LQU_IMP_REN_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_REN_K2 = 1;
        public static final int LQU_IMP_REN_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_REN_K2 = 8;
        public static final int LQU_IMP_REN_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_REN_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_REN_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
