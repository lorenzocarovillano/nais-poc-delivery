package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WCOM-DATI-OUTPUT<br>
 * Variable: WCOM-DATI-OUTPUT from program LCCS0234<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WcomDatiOutput extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WCOM-ID-OGG-PTF-EOC
    private int idOggPtfEoc = DefaultValues.INT_VAL;
    //Original name: WCOM-IB-OGG-PTF-EOC
    private String ibOggPtfEoc = DefaultValues.stringVal(Len.IB_OGG_PTF_EOC);
    //Original name: WCOM-ID-POLI-PTF
    private int idPoliPtf = DefaultValues.INT_VAL;
    //Original name: WCOM-ID-ADES-PTF
    private int idAdesPtf = DefaultValues.INT_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WCOM_DATI_OUTPUT;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWcomDatiOutputBytes(buf);
    }

    public String getS234DatiOutputFormatted() {
        return MarshalByteExt.bufferToStr(getWcomDatiOutputBytes());
    }

    public void setWcomDatiOutputBytes(byte[] buffer) {
        setWcomDatiOutputBytes(buffer, 1);
    }

    public byte[] getWcomDatiOutputBytes() {
        byte[] buffer = new byte[Len.WCOM_DATI_OUTPUT];
        return getWcomDatiOutputBytes(buffer, 1);
    }

    public void setWcomDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        idOggPtfEoc = MarshalByte.readInt(buffer, position, Len.ID_OGG_PTF_EOC);
        position += Len.ID_OGG_PTF_EOC;
        ibOggPtfEoc = MarshalByte.readString(buffer, position, Len.IB_OGG_PTF_EOC);
        position += Len.IB_OGG_PTF_EOC;
        idPoliPtf = MarshalByte.readInt(buffer, position, Len.ID_POLI_PTF);
        position += Len.ID_POLI_PTF;
        idAdesPtf = MarshalByte.readInt(buffer, position, Len.ID_ADES_PTF);
    }

    public byte[] getWcomDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeInt(buffer, position, idOggPtfEoc, Len.ID_OGG_PTF_EOC);
        position += Len.ID_OGG_PTF_EOC;
        MarshalByte.writeString(buffer, position, ibOggPtfEoc, Len.IB_OGG_PTF_EOC);
        position += Len.IB_OGG_PTF_EOC;
        MarshalByte.writeInt(buffer, position, idPoliPtf, Len.ID_POLI_PTF);
        position += Len.ID_POLI_PTF;
        MarshalByte.writeInt(buffer, position, idAdesPtf, Len.ID_ADES_PTF);
        return buffer;
    }

    public void setIdOggPtfEoc(int idOggPtfEoc) {
        this.idOggPtfEoc = idOggPtfEoc;
    }

    public int getIdOggPtfEoc() {
        return this.idOggPtfEoc;
    }

    public void setIbOggPtfEoc(String ibOggPtfEoc) {
        this.ibOggPtfEoc = Functions.subString(ibOggPtfEoc, Len.IB_OGG_PTF_EOC);
    }

    public String getIbOggPtfEoc() {
        return this.ibOggPtfEoc;
    }

    public void setIdPoliPtf(int idPoliPtf) {
        this.idPoliPtf = idPoliPtf;
    }

    public int getIdPoliPtf() {
        return this.idPoliPtf;
    }

    public void setIdAdesPtf(int idAdesPtf) {
        this.idAdesPtf = idAdesPtf;
    }

    public int getIdAdesPtf() {
        return this.idAdesPtf;
    }

    @Override
    public byte[] serialize() {
        return getWcomDatiOutputBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG_PTF_EOC = 9;
        public static final int IB_OGG_PTF_EOC = 40;
        public static final int ID_POLI_PTF = 9;
        public static final int ID_ADES_PTF = 9;
        public static final int WCOM_DATI_OUTPUT = ID_OGG_PTF_EOC + IB_OGG_PTF_EOC + ID_POLI_PTF + ID_ADES_PTF;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
