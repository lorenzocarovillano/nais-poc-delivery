package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: INDICI<br>
 * Variable: INDICI from program IVVS0212<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Indici {

    //==== PROPERTIES ====
    //Original name: IND-UNZIP
    private short indUnzip = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-START-VAR
    private short indStartVar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-END-VAR
    private short indEndVar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CHAR
    private short indChar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-CAO
    private short ixTabCao = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-AUT-OPER
    private short ixAutOper = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-PARAM
    private short ixParam = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIndUnzip(short indUnzip) {
        this.indUnzip = indUnzip;
    }

    public short getIndUnzip() {
        return this.indUnzip;
    }

    public void setIndStartVar(short indStartVar) {
        this.indStartVar = indStartVar;
    }

    public short getIndStartVar() {
        return this.indStartVar;
    }

    public void setIndEndVar(short indEndVar) {
        this.indEndVar = indEndVar;
    }

    public short getIndEndVar() {
        return this.indEndVar;
    }

    public void setIndChar(short indChar) {
        this.indChar = indChar;
    }

    public short getIndChar() {
        return this.indChar;
    }

    public void setIxTabCao(short ixTabCao) {
        this.ixTabCao = ixTabCao;
    }

    public short getIxTabCao() {
        return this.ixTabCao;
    }

    public void setIxAutOper(short ixAutOper) {
        this.ixAutOper = ixAutOper;
    }

    public short getIxAutOper() {
        return this.ixAutOper;
    }

    public void setIxParam(short ixParam) {
        this.ixParam = ixParam;
    }

    public short getIxParam() {
        return this.ixParam;
    }
}
