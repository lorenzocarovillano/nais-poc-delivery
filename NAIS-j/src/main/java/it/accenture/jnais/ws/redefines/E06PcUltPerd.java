package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: E06-PC-ULT-PERD<br>
 * Variable: E06-PC-ULT-PERD from program IDBSE060<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E06PcUltPerd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E06PcUltPerd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E06_PC_ULT_PERD;
    }

    public void setE06PcUltPerd(AfDecimal e06PcUltPerd) {
        writeDecimalAsPacked(Pos.E06_PC_ULT_PERD, e06PcUltPerd.copy());
    }

    public void setE06PcUltPerdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E06_PC_ULT_PERD, Pos.E06_PC_ULT_PERD);
    }

    /**Original name: E06-PC-ULT-PERD<br>*/
    public AfDecimal getE06PcUltPerd() {
        return readPackedAsDecimal(Pos.E06_PC_ULT_PERD, Len.Int.E06_PC_ULT_PERD, Len.Fract.E06_PC_ULT_PERD);
    }

    public byte[] getE06PcUltPerdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E06_PC_ULT_PERD, Pos.E06_PC_ULT_PERD);
        return buffer;
    }

    public void setE06PcUltPerdNull(String e06PcUltPerdNull) {
        writeString(Pos.E06_PC_ULT_PERD_NULL, e06PcUltPerdNull, Len.E06_PC_ULT_PERD_NULL);
    }

    /**Original name: E06-PC-ULT-PERD-NULL<br>*/
    public String getE06PcUltPerdNull() {
        return readString(Pos.E06_PC_ULT_PERD_NULL, Len.E06_PC_ULT_PERD_NULL);
    }

    public String getE06PcUltPerdNullFormatted() {
        return Functions.padBlanks(getE06PcUltPerdNull(), Len.E06_PC_ULT_PERD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E06_PC_ULT_PERD = 1;
        public static final int E06_PC_ULT_PERD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E06_PC_ULT_PERD = 4;
        public static final int E06_PC_ULT_PERD_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E06_PC_ULT_PERD = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int E06_PC_ULT_PERD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
