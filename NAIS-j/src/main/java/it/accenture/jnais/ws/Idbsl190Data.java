package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idbvl193;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndQuotzFndUnit;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSL190<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsl190Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-QUOTZ-FND-UNIT
    private IndQuotzFndUnit indQuotzFndUnit = new IndQuotzFndUnit();
    //Original name: IDBVL193
    private Idbvl193 idbvl193 = new Idbvl193();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idbvl193 getIdbvl193() {
        return idbvl193;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndQuotzFndUnit getIndQuotzFndUnit() {
        return indQuotzFndUnit;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
