package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-COD-PRDT<br>
 * Variable: W-B03-COD-PRDT from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CodPrdtLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CodPrdtLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_COD_PRDT;
    }

    public void setwB03CodPrdt(int wB03CodPrdt) {
        writeIntAsPacked(Pos.W_B03_COD_PRDT, wB03CodPrdt, Len.Int.W_B03_COD_PRDT);
    }

    /**Original name: W-B03-COD-PRDT<br>*/
    public int getwB03CodPrdt() {
        return readPackedAsInt(Pos.W_B03_COD_PRDT, Len.Int.W_B03_COD_PRDT);
    }

    public byte[] getwB03CodPrdtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_COD_PRDT, Pos.W_B03_COD_PRDT);
        return buffer;
    }

    public void setwB03CodPrdtNull(String wB03CodPrdtNull) {
        writeString(Pos.W_B03_COD_PRDT_NULL, wB03CodPrdtNull, Len.W_B03_COD_PRDT_NULL);
    }

    /**Original name: W-B03-COD-PRDT-NULL<br>*/
    public String getwB03CodPrdtNull() {
        return readString(Pos.W_B03_COD_PRDT_NULL, Len.W_B03_COD_PRDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_COD_PRDT = 1;
        public static final int W_B03_COD_PRDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_COD_PRDT = 3;
        public static final int W_B03_COD_PRDT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_COD_PRDT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
