package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMP-RIT-VIS<br>
 * Variable: LQU-TOT-IMP-RIT-VIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpRitVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpRitVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMP_RIT_VIS;
    }

    public void setLquTotImpRitVis(AfDecimal lquTotImpRitVis) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMP_RIT_VIS, lquTotImpRitVis.copy());
    }

    public void setLquTotImpRitVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMP_RIT_VIS, Pos.LQU_TOT_IMP_RIT_VIS);
    }

    /**Original name: LQU-TOT-IMP-RIT-VIS<br>*/
    public AfDecimal getLquTotImpRitVis() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMP_RIT_VIS, Len.Int.LQU_TOT_IMP_RIT_VIS, Len.Fract.LQU_TOT_IMP_RIT_VIS);
    }

    public byte[] getLquTotImpRitVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMP_RIT_VIS, Pos.LQU_TOT_IMP_RIT_VIS);
        return buffer;
    }

    public void setLquTotImpRitVisNull(String lquTotImpRitVisNull) {
        writeString(Pos.LQU_TOT_IMP_RIT_VIS_NULL, lquTotImpRitVisNull, Len.LQU_TOT_IMP_RIT_VIS_NULL);
    }

    /**Original name: LQU-TOT-IMP-RIT-VIS-NULL<br>*/
    public String getLquTotImpRitVisNull() {
        return readString(Pos.LQU_TOT_IMP_RIT_VIS_NULL, Len.LQU_TOT_IMP_RIT_VIS_NULL);
    }

    public String getLquTotImpRitVisNullFormatted() {
        return Functions.padBlanks(getLquTotImpRitVisNull(), Len.LQU_TOT_IMP_RIT_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_RIT_VIS = 1;
        public static final int LQU_TOT_IMP_RIT_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_RIT_VIS = 8;
        public static final int LQU_TOT_IMP_RIT_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_RIT_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_RIT_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
