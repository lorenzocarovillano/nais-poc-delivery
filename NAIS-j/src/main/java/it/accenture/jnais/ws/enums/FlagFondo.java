package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-FONDO<br>
 * Variable: FLAG-FONDO from program LCCS0450<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagFondo {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO = 'S';
    public static final char NON_TROVATO = 'N';

    //==== METHODS ====
    public void setFlagFondo(char flagFondo) {
        this.value = flagFondo;
    }

    public char getFlagFondo() {
        return this.value;
    }

    public boolean isTrovato() {
        return value == TROVATO;
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public boolean isNonTrovato() {
        return value == NON_TROVATO;
    }

    public void setNonTrovato() {
        value = NON_TROVATO;
    }
}
