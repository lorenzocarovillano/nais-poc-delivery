package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV0371-DTC-DIR<br>
 * Variable: LDBV0371-DTC-DIR from program LDBS0370<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ldbv0371DtcDir extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Ldbv0371DtcDir() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV0371_DTC_DIR;
    }

    public void setLdbv0371DtcDir(AfDecimal ldbv0371DtcDir) {
        writeDecimalAsPacked(Pos.LDBV0371_DTC_DIR, ldbv0371DtcDir.copy());
    }

    public void setLdbv0371DtcDirFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LDBV0371_DTC_DIR, Pos.LDBV0371_DTC_DIR);
    }

    /**Original name: LDBV0371-DTC-DIR<br>*/
    public AfDecimal getLdbv0371DtcDir() {
        return readPackedAsDecimal(Pos.LDBV0371_DTC_DIR, Len.Int.LDBV0371_DTC_DIR, Len.Fract.LDBV0371_DTC_DIR);
    }

    public byte[] getLdbv0371DtcDirAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LDBV0371_DTC_DIR, Pos.LDBV0371_DTC_DIR);
        return buffer;
    }

    public void setLdbv0371DtcDirNull(String ldbv0371DtcDirNull) {
        writeString(Pos.LDBV0371_DTC_DIR_NULL, ldbv0371DtcDirNull, Len.LDBV0371_DTC_DIR_NULL);
    }

    /**Original name: LDBV0371-DTC-DIR-NULL<br>*/
    public String getLdbv0371DtcDirNull() {
        return readString(Pos.LDBV0371_DTC_DIR_NULL, Len.LDBV0371_DTC_DIR_NULL);
    }

    public String getLdbv0371DtcDirNullFormatted() {
        return Functions.padBlanks(getLdbv0371DtcDirNull(), Len.LDBV0371_DTC_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LDBV0371_DTC_DIR = 1;
        public static final int LDBV0371_DTC_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV0371_DTC_DIR = 8;
        public static final int LDBV0371_DTC_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int LDBV0371_DTC_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV0371_DTC_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
