package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-PRE-LRD-IND<br>
 * Variable: ADE-PRE-LRD-IND from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdePreLrdInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdePreLrdInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_PRE_LRD_IND;
    }

    public void setAdePreLrdInd(AfDecimal adePreLrdInd) {
        writeDecimalAsPacked(Pos.ADE_PRE_LRD_IND, adePreLrdInd.copy());
    }

    public void setAdePreLrdIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_PRE_LRD_IND, Pos.ADE_PRE_LRD_IND);
    }

    /**Original name: ADE-PRE-LRD-IND<br>*/
    public AfDecimal getAdePreLrdInd() {
        return readPackedAsDecimal(Pos.ADE_PRE_LRD_IND, Len.Int.ADE_PRE_LRD_IND, Len.Fract.ADE_PRE_LRD_IND);
    }

    public byte[] getAdePreLrdIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_PRE_LRD_IND, Pos.ADE_PRE_LRD_IND);
        return buffer;
    }

    public void setAdePreLrdIndNull(String adePreLrdIndNull) {
        writeString(Pos.ADE_PRE_LRD_IND_NULL, adePreLrdIndNull, Len.ADE_PRE_LRD_IND_NULL);
    }

    /**Original name: ADE-PRE-LRD-IND-NULL<br>*/
    public String getAdePreLrdIndNull() {
        return readString(Pos.ADE_PRE_LRD_IND_NULL, Len.ADE_PRE_LRD_IND_NULL);
    }

    public String getAdePreLrdIndNullFormatted() {
        return Functions.padBlanks(getAdePreLrdIndNull(), Len.ADE_PRE_LRD_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_PRE_LRD_IND = 1;
        public static final int ADE_PRE_LRD_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_PRE_LRD_IND = 8;
        public static final int ADE_PRE_LRD_IND_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_PRE_LRD_IND = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_PRE_LRD_IND = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
