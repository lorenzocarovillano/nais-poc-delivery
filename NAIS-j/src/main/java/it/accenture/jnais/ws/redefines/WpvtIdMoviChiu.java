package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPVT-ID-MOVI-CHIU<br>
 * Variable: WPVT-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_ID_MOVI_CHIU;
    }

    public void setWpvtIdMoviChiu(int wpvtIdMoviChiu) {
        writeIntAsPacked(Pos.WPVT_ID_MOVI_CHIU, wpvtIdMoviChiu, Len.Int.WPVT_ID_MOVI_CHIU);
    }

    public void setWpvtIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_ID_MOVI_CHIU, Pos.WPVT_ID_MOVI_CHIU);
    }

    /**Original name: WPVT-ID-MOVI-CHIU<br>*/
    public int getWpvtIdMoviChiu() {
        return readPackedAsInt(Pos.WPVT_ID_MOVI_CHIU, Len.Int.WPVT_ID_MOVI_CHIU);
    }

    public byte[] getWpvtIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_ID_MOVI_CHIU, Pos.WPVT_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWpvtIdMoviChiuSpaces() {
        fill(Pos.WPVT_ID_MOVI_CHIU, Len.WPVT_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWpvtIdMoviChiuNull(String wpvtIdMoviChiuNull) {
        writeString(Pos.WPVT_ID_MOVI_CHIU_NULL, wpvtIdMoviChiuNull, Len.WPVT_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WPVT-ID-MOVI-CHIU-NULL<br>*/
    public String getWpvtIdMoviChiuNull() {
        return readString(Pos.WPVT_ID_MOVI_CHIU_NULL, Len.WPVT_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_ID_MOVI_CHIU = 1;
        public static final int WPVT_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_ID_MOVI_CHIU = 5;
        public static final int WPVT_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
