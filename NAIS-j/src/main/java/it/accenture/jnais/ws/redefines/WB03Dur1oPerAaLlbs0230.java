package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-DUR-1O-PER-AA<br>
 * Variable: W-B03-DUR-1O-PER-AA from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03Dur1oPerAaLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03Dur1oPerAaLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DUR1O_PER_AA;
    }

    public void setwB03Dur1oPerAa(int wB03Dur1oPerAa) {
        writeIntAsPacked(Pos.W_B03_DUR1O_PER_AA, wB03Dur1oPerAa, Len.Int.W_B03_DUR1O_PER_AA);
    }

    /**Original name: W-B03-DUR-1O-PER-AA<br>*/
    public int getwB03Dur1oPerAa() {
        return readPackedAsInt(Pos.W_B03_DUR1O_PER_AA, Len.Int.W_B03_DUR1O_PER_AA);
    }

    public byte[] getwB03Dur1oPerAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DUR1O_PER_AA, Pos.W_B03_DUR1O_PER_AA);
        return buffer;
    }

    public void setwB03Dur1oPerAaNull(String wB03Dur1oPerAaNull) {
        writeString(Pos.W_B03_DUR1O_PER_AA_NULL, wB03Dur1oPerAaNull, Len.W_B03_DUR1O_PER_AA_NULL);
    }

    /**Original name: W-B03-DUR-1O-PER-AA-NULL<br>*/
    public String getwB03Dur1oPerAaNull() {
        return readString(Pos.W_B03_DUR1O_PER_AA_NULL, Len.W_B03_DUR1O_PER_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DUR1O_PER_AA = 1;
        public static final int W_B03_DUR1O_PER_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DUR1O_PER_AA = 3;
        public static final int W_B03_DUR1O_PER_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DUR1O_PER_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
