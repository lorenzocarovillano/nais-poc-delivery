package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P61-IMPB-IS-RP-P62014<br>
 * Variable: P61-IMPB-IS-RP-P62014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P61ImpbIsRpP62014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P61ImpbIsRpP62014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P61_IMPB_IS_RP_P62014;
    }

    public void setP61ImpbIsRpP62014(AfDecimal p61ImpbIsRpP62014) {
        writeDecimalAsPacked(Pos.P61_IMPB_IS_RP_P62014, p61ImpbIsRpP62014.copy());
    }

    public void setP61ImpbIsRpP62014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P61_IMPB_IS_RP_P62014, Pos.P61_IMPB_IS_RP_P62014);
    }

    /**Original name: P61-IMPB-IS-RP-P62014<br>*/
    public AfDecimal getP61ImpbIsRpP62014() {
        return readPackedAsDecimal(Pos.P61_IMPB_IS_RP_P62014, Len.Int.P61_IMPB_IS_RP_P62014, Len.Fract.P61_IMPB_IS_RP_P62014);
    }

    public byte[] getP61ImpbIsRpP62014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P61_IMPB_IS_RP_P62014, Pos.P61_IMPB_IS_RP_P62014);
        return buffer;
    }

    public void setP61ImpbIsRpP62014Null(String p61ImpbIsRpP62014Null) {
        writeString(Pos.P61_IMPB_IS_RP_P62014_NULL, p61ImpbIsRpP62014Null, Len.P61_IMPB_IS_RP_P62014_NULL);
    }

    /**Original name: P61-IMPB-IS-RP-P62014-NULL<br>*/
    public String getP61ImpbIsRpP62014Null() {
        return readString(Pos.P61_IMPB_IS_RP_P62014_NULL, Len.P61_IMPB_IS_RP_P62014_NULL);
    }

    public String getP61ImpbIsRpP62014NullFormatted() {
        return Functions.padBlanks(getP61ImpbIsRpP62014Null(), Len.P61_IMPB_IS_RP_P62014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P61_IMPB_IS_RP_P62014 = 1;
        public static final int P61_IMPB_IS_RP_P62014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_IMPB_IS_RP_P62014 = 8;
        public static final int P61_IMPB_IS_RP_P62014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_IMPB_IS_RP_P62014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P61_IMPB_IS_RP_P62014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
