package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-TS-RIVAL-FIS<br>
 * Variable: TGA-TS-RIVAL-FIS from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaTsRivalFis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaTsRivalFis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_TS_RIVAL_FIS;
    }

    public void setTgaTsRivalFis(AfDecimal tgaTsRivalFis) {
        writeDecimalAsPacked(Pos.TGA_TS_RIVAL_FIS, tgaTsRivalFis.copy());
    }

    public void setTgaTsRivalFisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_TS_RIVAL_FIS, Pos.TGA_TS_RIVAL_FIS);
    }

    /**Original name: TGA-TS-RIVAL-FIS<br>*/
    public AfDecimal getTgaTsRivalFis() {
        return readPackedAsDecimal(Pos.TGA_TS_RIVAL_FIS, Len.Int.TGA_TS_RIVAL_FIS, Len.Fract.TGA_TS_RIVAL_FIS);
    }

    public byte[] getTgaTsRivalFisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_TS_RIVAL_FIS, Pos.TGA_TS_RIVAL_FIS);
        return buffer;
    }

    public void setTgaTsRivalFisNull(String tgaTsRivalFisNull) {
        writeString(Pos.TGA_TS_RIVAL_FIS_NULL, tgaTsRivalFisNull, Len.TGA_TS_RIVAL_FIS_NULL);
    }

    /**Original name: TGA-TS-RIVAL-FIS-NULL<br>*/
    public String getTgaTsRivalFisNull() {
        return readString(Pos.TGA_TS_RIVAL_FIS_NULL, Len.TGA_TS_RIVAL_FIS_NULL);
    }

    public String getTgaTsRivalFisNullFormatted() {
        return Functions.padBlanks(getTgaTsRivalFisNull(), Len.TGA_TS_RIVAL_FIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_TS_RIVAL_FIS = 1;
        public static final int TGA_TS_RIVAL_FIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_TS_RIVAL_FIS = 8;
        public static final int TGA_TS_RIVAL_FIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_TS_RIVAL_FIS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_TS_RIVAL_FIS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
