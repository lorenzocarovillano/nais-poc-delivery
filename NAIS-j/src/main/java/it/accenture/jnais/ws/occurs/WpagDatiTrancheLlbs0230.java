package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: WPAG-DATI-TRANCHE<br>
 * Variables: WPAG-DATI-TRANCHE from copybook LSTC0072<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpagDatiTrancheLlbs0230 implements ICopyable<WpagDatiTrancheLlbs0230> {

    //==== PROPERTIES ====
    //Original name: WPAG-COD-FND
    private String codFnd = DefaultValues.stringVal(Len.COD_FND);
    //Original name: WPAG-TP-FND
    private char tpFnd = DefaultValues.CHAR_VAL;
    //Original name: WPAG-NUM-QUO
    private AfDecimal numQuo = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: WPAG-VAL-QUO
    private AfDecimal valQuo = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WPAG-IMP-DISINV
    private AfDecimal impDisinv = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WPAG-ID-TRCH-DI-GAR
    private int idTrchDiGar = DefaultValues.INT_VAL;

    //==== CONSTRUCTORS ====
    public WpagDatiTrancheLlbs0230() {
    }

    public WpagDatiTrancheLlbs0230(WpagDatiTrancheLlbs0230 datiTranche) {
        this();
        this.codFnd = datiTranche.codFnd;
        this.tpFnd = datiTranche.tpFnd;
        this.numQuo.assign(datiTranche.numQuo);
        this.valQuo.assign(datiTranche.valQuo);
        this.impDisinv.assign(datiTranche.impDisinv);
        this.idTrchDiGar = datiTranche.idTrchDiGar;
    }

    //==== METHODS ====
    public void setCodFnd(String codFnd) {
        this.codFnd = Functions.subString(codFnd, Len.COD_FND);
    }

    public String getCodFnd() {
        return this.codFnd;
    }

    public void setTpFnd(char tpFnd) {
        this.tpFnd = tpFnd;
    }

    public char getTpFnd() {
        return this.tpFnd;
    }

    public void setNumQuo(AfDecimal numQuo) {
        this.numQuo.assign(numQuo);
    }

    public AfDecimal getNumQuo() {
        return this.numQuo.copy();
    }

    public void setValQuo(AfDecimal valQuo) {
        this.valQuo.assign(valQuo);
    }

    public AfDecimal getValQuo() {
        return this.valQuo.copy();
    }

    public void setImpDisinv(AfDecimal impDisinv) {
        this.impDisinv.assign(impDisinv);
    }

    public AfDecimal getImpDisinv() {
        return this.impDisinv.copy();
    }

    public void setIdTrchDiGar(int idTrchDiGar) {
        this.idTrchDiGar = idTrchDiGar;
    }

    public int getIdTrchDiGar() {
        return this.idTrchDiGar;
    }

    public WpagDatiTrancheLlbs0230 copy() {
        return new WpagDatiTrancheLlbs0230(this);
    }

    public WpagDatiTrancheLlbs0230 initWpagDatiTrancheLlbs0230() {
        codFnd = "";
        tpFnd = Types.SPACE_CHAR;
        numQuo.assign(0);
        valQuo.assign(0);
        impDisinv.assign(0);
        idTrchDiGar = 0;
        return this;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_FND = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
