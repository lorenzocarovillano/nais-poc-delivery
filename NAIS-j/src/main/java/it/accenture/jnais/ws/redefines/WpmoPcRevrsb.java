package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-PC-REVRSB<br>
 * Variable: WPMO-PC-REVRSB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoPcRevrsb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoPcRevrsb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_PC_REVRSB;
    }

    public void setWpmoPcRevrsb(AfDecimal wpmoPcRevrsb) {
        writeDecimalAsPacked(Pos.WPMO_PC_REVRSB, wpmoPcRevrsb.copy());
    }

    public void setWpmoPcRevrsbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_PC_REVRSB, Pos.WPMO_PC_REVRSB);
    }

    /**Original name: WPMO-PC-REVRSB<br>*/
    public AfDecimal getWpmoPcRevrsb() {
        return readPackedAsDecimal(Pos.WPMO_PC_REVRSB, Len.Int.WPMO_PC_REVRSB, Len.Fract.WPMO_PC_REVRSB);
    }

    public byte[] getWpmoPcRevrsbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_PC_REVRSB, Pos.WPMO_PC_REVRSB);
        return buffer;
    }

    public void initWpmoPcRevrsbSpaces() {
        fill(Pos.WPMO_PC_REVRSB, Len.WPMO_PC_REVRSB, Types.SPACE_CHAR);
    }

    public void setWpmoPcRevrsbNull(String wpmoPcRevrsbNull) {
        writeString(Pos.WPMO_PC_REVRSB_NULL, wpmoPcRevrsbNull, Len.WPMO_PC_REVRSB_NULL);
    }

    /**Original name: WPMO-PC-REVRSB-NULL<br>*/
    public String getWpmoPcRevrsbNull() {
        return readString(Pos.WPMO_PC_REVRSB_NULL, Len.WPMO_PC_REVRSB_NULL);
    }

    public String getWpmoPcRevrsbNullFormatted() {
        return Functions.padBlanks(getWpmoPcRevrsbNull(), Len.WPMO_PC_REVRSB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_PC_REVRSB = 1;
        public static final int WPMO_PC_REVRSB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_PC_REVRSB = 4;
        public static final int WPMO_PC_REVRSB_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_PC_REVRSB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_PC_REVRSB = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
