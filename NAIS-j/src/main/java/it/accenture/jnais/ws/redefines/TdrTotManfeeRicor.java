package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-MANFEE-RICOR<br>
 * Variable: TDR-TOT-MANFEE-RICOR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotManfeeRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotManfeeRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_MANFEE_RICOR;
    }

    public void setTdrTotManfeeRicor(AfDecimal tdrTotManfeeRicor) {
        writeDecimalAsPacked(Pos.TDR_TOT_MANFEE_RICOR, tdrTotManfeeRicor.copy());
    }

    public void setTdrTotManfeeRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_MANFEE_RICOR, Pos.TDR_TOT_MANFEE_RICOR);
    }

    /**Original name: TDR-TOT-MANFEE-RICOR<br>*/
    public AfDecimal getTdrTotManfeeRicor() {
        return readPackedAsDecimal(Pos.TDR_TOT_MANFEE_RICOR, Len.Int.TDR_TOT_MANFEE_RICOR, Len.Fract.TDR_TOT_MANFEE_RICOR);
    }

    public byte[] getTdrTotManfeeRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_MANFEE_RICOR, Pos.TDR_TOT_MANFEE_RICOR);
        return buffer;
    }

    public void setTdrTotManfeeRicorNull(String tdrTotManfeeRicorNull) {
        writeString(Pos.TDR_TOT_MANFEE_RICOR_NULL, tdrTotManfeeRicorNull, Len.TDR_TOT_MANFEE_RICOR_NULL);
    }

    /**Original name: TDR-TOT-MANFEE-RICOR-NULL<br>*/
    public String getTdrTotManfeeRicorNull() {
        return readString(Pos.TDR_TOT_MANFEE_RICOR_NULL, Len.TDR_TOT_MANFEE_RICOR_NULL);
    }

    public String getTdrTotManfeeRicorNullFormatted() {
        return Functions.padBlanks(getTdrTotManfeeRicorNull(), Len.TDR_TOT_MANFEE_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_MANFEE_RICOR = 1;
        public static final int TDR_TOT_MANFEE_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_MANFEE_RICOR = 8;
        public static final int TDR_TOT_MANFEE_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_MANFEE_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_MANFEE_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
