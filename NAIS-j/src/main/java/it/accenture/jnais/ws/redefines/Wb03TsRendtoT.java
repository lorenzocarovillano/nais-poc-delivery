package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-TS-RENDTO-T<br>
 * Variable: WB03-TS-RENDTO-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03TsRendtoT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03TsRendtoT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_TS_RENDTO_T;
    }

    public void setWb03TsRendtoTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_TS_RENDTO_T, Pos.WB03_TS_RENDTO_T);
    }

    /**Original name: WB03-TS-RENDTO-T<br>*/
    public AfDecimal getWb03TsRendtoT() {
        return readPackedAsDecimal(Pos.WB03_TS_RENDTO_T, Len.Int.WB03_TS_RENDTO_T, Len.Fract.WB03_TS_RENDTO_T);
    }

    public byte[] getWb03TsRendtoTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_TS_RENDTO_T, Pos.WB03_TS_RENDTO_T);
        return buffer;
    }

    public void setWb03TsRendtoTNull(String wb03TsRendtoTNull) {
        writeString(Pos.WB03_TS_RENDTO_T_NULL, wb03TsRendtoTNull, Len.WB03_TS_RENDTO_T_NULL);
    }

    /**Original name: WB03-TS-RENDTO-T-NULL<br>*/
    public String getWb03TsRendtoTNull() {
        return readString(Pos.WB03_TS_RENDTO_T_NULL, Len.WB03_TS_RENDTO_T_NULL);
    }

    public String getWb03TsRendtoTNullFormatted() {
        return Functions.padBlanks(getWb03TsRendtoTNull(), Len.WB03_TS_RENDTO_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_TS_RENDTO_T = 1;
        public static final int WB03_TS_RENDTO_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_TS_RENDTO_T = 8;
        public static final int WB03_TS_RENDTO_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_TS_RENDTO_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_TS_RENDTO_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
