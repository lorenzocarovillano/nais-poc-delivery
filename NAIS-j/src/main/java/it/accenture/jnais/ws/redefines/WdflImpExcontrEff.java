package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMP-EXCONTR-EFF<br>
 * Variable: WDFL-IMP-EXCONTR-EFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpExcontrEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpExcontrEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMP_EXCONTR_EFF;
    }

    public void setWdflImpExcontrEff(AfDecimal wdflImpExcontrEff) {
        writeDecimalAsPacked(Pos.WDFL_IMP_EXCONTR_EFF, wdflImpExcontrEff.copy());
    }

    public void setWdflImpExcontrEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMP_EXCONTR_EFF, Pos.WDFL_IMP_EXCONTR_EFF);
    }

    /**Original name: WDFL-IMP-EXCONTR-EFF<br>*/
    public AfDecimal getWdflImpExcontrEff() {
        return readPackedAsDecimal(Pos.WDFL_IMP_EXCONTR_EFF, Len.Int.WDFL_IMP_EXCONTR_EFF, Len.Fract.WDFL_IMP_EXCONTR_EFF);
    }

    public byte[] getWdflImpExcontrEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMP_EXCONTR_EFF, Pos.WDFL_IMP_EXCONTR_EFF);
        return buffer;
    }

    public void setWdflImpExcontrEffNull(String wdflImpExcontrEffNull) {
        writeString(Pos.WDFL_IMP_EXCONTR_EFF_NULL, wdflImpExcontrEffNull, Len.WDFL_IMP_EXCONTR_EFF_NULL);
    }

    /**Original name: WDFL-IMP-EXCONTR-EFF-NULL<br>*/
    public String getWdflImpExcontrEffNull() {
        return readString(Pos.WDFL_IMP_EXCONTR_EFF_NULL, Len.WDFL_IMP_EXCONTR_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_EXCONTR_EFF = 1;
        public static final int WDFL_IMP_EXCONTR_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_EXCONTR_EFF = 8;
        public static final int WDFL_IMP_EXCONTR_EFF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_EXCONTR_EFF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_EXCONTR_EFF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
