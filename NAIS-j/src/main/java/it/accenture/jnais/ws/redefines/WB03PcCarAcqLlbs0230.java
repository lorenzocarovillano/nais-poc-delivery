package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PC-CAR-ACQ<br>
 * Variable: W-B03-PC-CAR-ACQ from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03PcCarAcqLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03PcCarAcqLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PC_CAR_ACQ;
    }

    public void setwB03PcCarAcq(AfDecimal wB03PcCarAcq) {
        writeDecimalAsPacked(Pos.W_B03_PC_CAR_ACQ, wB03PcCarAcq.copy());
    }

    /**Original name: W-B03-PC-CAR-ACQ<br>*/
    public AfDecimal getwB03PcCarAcq() {
        return readPackedAsDecimal(Pos.W_B03_PC_CAR_ACQ, Len.Int.W_B03_PC_CAR_ACQ, Len.Fract.W_B03_PC_CAR_ACQ);
    }

    public byte[] getwB03PcCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PC_CAR_ACQ, Pos.W_B03_PC_CAR_ACQ);
        return buffer;
    }

    public void setwB03PcCarAcqNull(String wB03PcCarAcqNull) {
        writeString(Pos.W_B03_PC_CAR_ACQ_NULL, wB03PcCarAcqNull, Len.W_B03_PC_CAR_ACQ_NULL);
    }

    /**Original name: W-B03-PC-CAR-ACQ-NULL<br>*/
    public String getwB03PcCarAcqNull() {
        return readString(Pos.W_B03_PC_CAR_ACQ_NULL, Len.W_B03_PC_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PC_CAR_ACQ = 1;
        public static final int W_B03_PC_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PC_CAR_ACQ = 4;
        public static final int W_B03_PC_CAR_ACQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PC_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PC_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
