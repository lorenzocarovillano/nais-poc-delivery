package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;

/**Original name: MOV-TP-MOVI<br>
 * Variable: MOV-TP-MOVI from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MovTpMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MovTpMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MOV_TP_MOVI;
    }

    public void setMovTpMovi(int movTpMovi) {
        writeIntAsPacked(Pos.MOV_TP_MOVI, movTpMovi, Len.Int.MOV_TP_MOVI);
    }

    public void setMovTpMoviFormatted(String movTpMovi) {
        setMovTpMovi(PicParser.display(new PicParams("S9(5)V").setUsage(PicUsage.PACKED)).parseInt(movTpMovi));
    }

    public void setMovTpMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MOV_TP_MOVI, Pos.MOV_TP_MOVI);
    }

    /**Original name: MOV-TP-MOVI<br>*/
    public int getMovTpMovi() {
        return readPackedAsInt(Pos.MOV_TP_MOVI, Len.Int.MOV_TP_MOVI);
    }

    public String getMovTpMoviFormatted() {
        return PicFormatter.display(new PicParams("S9(5)V").setUsage(PicUsage.PACKED)).format(getMovTpMovi()).toString();
    }

    public byte[] getMovTpMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MOV_TP_MOVI, Pos.MOV_TP_MOVI);
        return buffer;
    }

    public void setMovTpMoviNull(String movTpMoviNull) {
        writeString(Pos.MOV_TP_MOVI_NULL, movTpMoviNull, Len.MOV_TP_MOVI_NULL);
    }

    /**Original name: MOV-TP-MOVI-NULL<br>*/
    public String getMovTpMoviNull() {
        return readString(Pos.MOV_TP_MOVI_NULL, Len.MOV_TP_MOVI_NULL);
    }

    public String getMovTpMoviNullFormatted() {
        return Functions.padBlanks(getMovTpMoviNull(), Len.MOV_TP_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MOV_TP_MOVI = 1;
        public static final int MOV_TP_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MOV_TP_MOVI = 3;
        public static final int MOV_TP_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MOV_TP_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
