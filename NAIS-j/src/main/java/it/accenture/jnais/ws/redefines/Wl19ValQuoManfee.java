package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WL19-VAL-QUO-MANFEE<br>
 * Variable: WL19-VAL-QUO-MANFEE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl19ValQuoManfee extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl19ValQuoManfee() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL19_VAL_QUO_MANFEE;
    }

    public void setDl19ValQuoManfee(AfDecimal dl19ValQuoManfee) {
        writeDecimalAsPacked(Pos.WL19_VAL_QUO_MANFEE, dl19ValQuoManfee.copy());
    }

    public void setWl19ValQuoManfeeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL19_VAL_QUO_MANFEE, Pos.WL19_VAL_QUO_MANFEE);
    }

    /**Original name: DL19-VAL-QUO-MANFEE<br>*/
    public AfDecimal getDl19ValQuoManfee() {
        return readPackedAsDecimal(Pos.WL19_VAL_QUO_MANFEE, Len.Int.DL19_VAL_QUO_MANFEE, Len.Fract.DL19_VAL_QUO_MANFEE);
    }

    public byte[] getWl19ValQuoManfeeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL19_VAL_QUO_MANFEE, Pos.WL19_VAL_QUO_MANFEE);
        return buffer;
    }

    public void initWl19ValQuoManfeeSpaces() {
        fill(Pos.WL19_VAL_QUO_MANFEE, Len.WL19_VAL_QUO_MANFEE, Types.SPACE_CHAR);
    }

    public void setWl19ValQuoManfeeNull(String wl19ValQuoManfeeNull) {
        writeString(Pos.WL19_VAL_QUO_MANFEE_NULL, wl19ValQuoManfeeNull, Len.WL19_VAL_QUO_MANFEE_NULL);
    }

    /**Original name: WL19-VAL-QUO-MANFEE-NULL<br>*/
    public String getWl19ValQuoManfeeNull() {
        return readString(Pos.WL19_VAL_QUO_MANFEE_NULL, Len.WL19_VAL_QUO_MANFEE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL19_VAL_QUO_MANFEE = 1;
        public static final int WL19_VAL_QUO_MANFEE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL19_VAL_QUO_MANFEE = 7;
        public static final int WL19_VAL_QUO_MANFEE_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DL19_VAL_QUO_MANFEE = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DL19_VAL_QUO_MANFEE = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
