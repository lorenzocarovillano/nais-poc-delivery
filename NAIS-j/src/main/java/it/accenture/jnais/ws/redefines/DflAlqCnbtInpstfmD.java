package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ALQ-CNBT-INPSTFM-D<br>
 * Variable: DFL-ALQ-CNBT-INPSTFM-D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAlqCnbtInpstfmD extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAlqCnbtInpstfmD() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ALQ_CNBT_INPSTFM_D;
    }

    public void setDflAlqCnbtInpstfmD(AfDecimal dflAlqCnbtInpstfmD) {
        writeDecimalAsPacked(Pos.DFL_ALQ_CNBT_INPSTFM_D, dflAlqCnbtInpstfmD.copy());
    }

    public void setDflAlqCnbtInpstfmDFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ALQ_CNBT_INPSTFM_D, Pos.DFL_ALQ_CNBT_INPSTFM_D);
    }

    /**Original name: DFL-ALQ-CNBT-INPSTFM-D<br>*/
    public AfDecimal getDflAlqCnbtInpstfmD() {
        return readPackedAsDecimal(Pos.DFL_ALQ_CNBT_INPSTFM_D, Len.Int.DFL_ALQ_CNBT_INPSTFM_D, Len.Fract.DFL_ALQ_CNBT_INPSTFM_D);
    }

    public byte[] getDflAlqCnbtInpstfmDAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ALQ_CNBT_INPSTFM_D, Pos.DFL_ALQ_CNBT_INPSTFM_D);
        return buffer;
    }

    public void setDflAlqCnbtInpstfmDNull(String dflAlqCnbtInpstfmDNull) {
        writeString(Pos.DFL_ALQ_CNBT_INPSTFM_D_NULL, dflAlqCnbtInpstfmDNull, Len.DFL_ALQ_CNBT_INPSTFM_D_NULL);
    }

    /**Original name: DFL-ALQ-CNBT-INPSTFM-D-NULL<br>*/
    public String getDflAlqCnbtInpstfmDNull() {
        return readString(Pos.DFL_ALQ_CNBT_INPSTFM_D_NULL, Len.DFL_ALQ_CNBT_INPSTFM_D_NULL);
    }

    public String getDflAlqCnbtInpstfmDNullFormatted() {
        return Functions.padBlanks(getDflAlqCnbtInpstfmDNull(), Len.DFL_ALQ_CNBT_INPSTFM_D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_CNBT_INPSTFM_D = 1;
        public static final int DFL_ALQ_CNBT_INPSTFM_D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_CNBT_INPSTFM_D = 4;
        public static final int DFL_ALQ_CNBT_INPSTFM_D_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_CNBT_INPSTFM_D = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_CNBT_INPSTFM_D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
