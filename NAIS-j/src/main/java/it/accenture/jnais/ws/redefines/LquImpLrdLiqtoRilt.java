package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-LRD-LIQTO-RILT<br>
 * Variable: LQU-IMP-LRD-LIQTO-RILT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpLrdLiqtoRilt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpLrdLiqtoRilt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_LRD_LIQTO_RILT;
    }

    public void setLquImpLrdLiqtoRilt(AfDecimal lquImpLrdLiqtoRilt) {
        writeDecimalAsPacked(Pos.LQU_IMP_LRD_LIQTO_RILT, lquImpLrdLiqtoRilt.copy());
    }

    public void setLquImpLrdLiqtoRiltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_LRD_LIQTO_RILT, Pos.LQU_IMP_LRD_LIQTO_RILT);
    }

    /**Original name: LQU-IMP-LRD-LIQTO-RILT<br>*/
    public AfDecimal getLquImpLrdLiqtoRilt() {
        return readPackedAsDecimal(Pos.LQU_IMP_LRD_LIQTO_RILT, Len.Int.LQU_IMP_LRD_LIQTO_RILT, Len.Fract.LQU_IMP_LRD_LIQTO_RILT);
    }

    public byte[] getLquImpLrdLiqtoRiltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_LRD_LIQTO_RILT, Pos.LQU_IMP_LRD_LIQTO_RILT);
        return buffer;
    }

    public void setLquImpLrdLiqtoRiltNull(String lquImpLrdLiqtoRiltNull) {
        writeString(Pos.LQU_IMP_LRD_LIQTO_RILT_NULL, lquImpLrdLiqtoRiltNull, Len.LQU_IMP_LRD_LIQTO_RILT_NULL);
    }

    /**Original name: LQU-IMP-LRD-LIQTO-RILT-NULL<br>*/
    public String getLquImpLrdLiqtoRiltNull() {
        return readString(Pos.LQU_IMP_LRD_LIQTO_RILT_NULL, Len.LQU_IMP_LRD_LIQTO_RILT_NULL);
    }

    public String getLquImpLrdLiqtoRiltNullFormatted() {
        return Functions.padBlanks(getLquImpLrdLiqtoRiltNull(), Len.LQU_IMP_LRD_LIQTO_RILT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_LRD_LIQTO_RILT = 1;
        public static final int LQU_IMP_LRD_LIQTO_RILT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_LRD_LIQTO_RILT = 8;
        public static final int LQU_IMP_LRD_LIQTO_RILT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_LRD_LIQTO_RILT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_LRD_LIQTO_RILT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
