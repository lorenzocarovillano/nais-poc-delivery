package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-ID-ASSICURATI<br>
 * Variable: P56-ID-ASSICURATI from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56IdAssicurati extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56IdAssicurati() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_ID_ASSICURATI;
    }

    public void setP56IdAssicurati(int p56IdAssicurati) {
        writeIntAsPacked(Pos.P56_ID_ASSICURATI, p56IdAssicurati, Len.Int.P56_ID_ASSICURATI);
    }

    public void setP56IdAssicuratiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_ID_ASSICURATI, Pos.P56_ID_ASSICURATI);
    }

    /**Original name: P56-ID-ASSICURATI<br>*/
    public int getP56IdAssicurati() {
        return readPackedAsInt(Pos.P56_ID_ASSICURATI, Len.Int.P56_ID_ASSICURATI);
    }

    public byte[] getP56IdAssicuratiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_ID_ASSICURATI, Pos.P56_ID_ASSICURATI);
        return buffer;
    }

    public void setP56IdAssicuratiNull(String p56IdAssicuratiNull) {
        writeString(Pos.P56_ID_ASSICURATI_NULL, p56IdAssicuratiNull, Len.P56_ID_ASSICURATI_NULL);
    }

    /**Original name: P56-ID-ASSICURATI-NULL<br>*/
    public String getP56IdAssicuratiNull() {
        return readString(Pos.P56_ID_ASSICURATI_NULL, Len.P56_ID_ASSICURATI_NULL);
    }

    public String getP56IdAssicuratiNullFormatted() {
        return Functions.padBlanks(getP56IdAssicuratiNull(), Len.P56_ID_ASSICURATI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_ID_ASSICURATI = 1;
        public static final int P56_ID_ASSICURATI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_ID_ASSICURATI = 5;
        public static final int P56_ID_ASSICURATI_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_ID_ASSICURATI = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
