package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP85-PC-RETR<br>
 * Variable: WP85-PC-RETR from program LRGS0660<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp85PcRetr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp85PcRetr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP85_PC_RETR;
    }

    public void setWp85PcRetr(AfDecimal wp85PcRetr) {
        writeDecimalAsPacked(Pos.WP85_PC_RETR, wp85PcRetr.copy());
    }

    /**Original name: WP85-PC-RETR<br>*/
    public AfDecimal getWp85PcRetr() {
        return readPackedAsDecimal(Pos.WP85_PC_RETR, Len.Int.WP85_PC_RETR, Len.Fract.WP85_PC_RETR);
    }

    public void setWp85PcRetrNull(String wp85PcRetrNull) {
        writeString(Pos.WP85_PC_RETR_NULL, wp85PcRetrNull, Len.WP85_PC_RETR_NULL);
    }

    /**Original name: WP85-PC-RETR-NULL<br>*/
    public String getWp85PcRetrNull() {
        return readString(Pos.WP85_PC_RETR_NULL, Len.WP85_PC_RETR_NULL);
    }

    public String getWp85PcRetrNullFormatted() {
        return Functions.padBlanks(getWp85PcRetrNull(), Len.WP85_PC_RETR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP85_PC_RETR = 1;
        public static final int WP85_PC_RETR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP85_PC_RETR = 4;
        public static final int WP85_PC_RETR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP85_PC_RETR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP85_PC_RETR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
