package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-SOM-ASSTA-GARAC<br>
 * Variable: WPMO-SOM-ASSTA-GARAC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoSomAsstaGarac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoSomAsstaGarac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_SOM_ASSTA_GARAC;
    }

    public void setWpmoSomAsstaGarac(AfDecimal wpmoSomAsstaGarac) {
        writeDecimalAsPacked(Pos.WPMO_SOM_ASSTA_GARAC, wpmoSomAsstaGarac.copy());
    }

    public void setWpmoSomAsstaGaracFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_SOM_ASSTA_GARAC, Pos.WPMO_SOM_ASSTA_GARAC);
    }

    /**Original name: WPMO-SOM-ASSTA-GARAC<br>*/
    public AfDecimal getWpmoSomAsstaGarac() {
        return readPackedAsDecimal(Pos.WPMO_SOM_ASSTA_GARAC, Len.Int.WPMO_SOM_ASSTA_GARAC, Len.Fract.WPMO_SOM_ASSTA_GARAC);
    }

    public byte[] getWpmoSomAsstaGaracAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_SOM_ASSTA_GARAC, Pos.WPMO_SOM_ASSTA_GARAC);
        return buffer;
    }

    public void initWpmoSomAsstaGaracSpaces() {
        fill(Pos.WPMO_SOM_ASSTA_GARAC, Len.WPMO_SOM_ASSTA_GARAC, Types.SPACE_CHAR);
    }

    public void setWpmoSomAsstaGaracNull(String wpmoSomAsstaGaracNull) {
        writeString(Pos.WPMO_SOM_ASSTA_GARAC_NULL, wpmoSomAsstaGaracNull, Len.WPMO_SOM_ASSTA_GARAC_NULL);
    }

    /**Original name: WPMO-SOM-ASSTA-GARAC-NULL<br>*/
    public String getWpmoSomAsstaGaracNull() {
        return readString(Pos.WPMO_SOM_ASSTA_GARAC_NULL, Len.WPMO_SOM_ASSTA_GARAC_NULL);
    }

    public String getWpmoSomAsstaGaracNullFormatted() {
        return Functions.padBlanks(getWpmoSomAsstaGaracNull(), Len.WPMO_SOM_ASSTA_GARAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_SOM_ASSTA_GARAC = 1;
        public static final int WPMO_SOM_ASSTA_GARAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_SOM_ASSTA_GARAC = 8;
        public static final int WPMO_SOM_ASSTA_GARAC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_SOM_ASSTA_GARAC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_SOM_ASSTA_GARAC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
