package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TP-MET-RISC<br>
 * Variable: LQU-TP-MET-RISC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTpMetRisc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTpMetRisc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TP_MET_RISC;
    }

    public void setLquTpMetRisc(short lquTpMetRisc) {
        writeShortAsPacked(Pos.LQU_TP_MET_RISC, lquTpMetRisc, Len.Int.LQU_TP_MET_RISC);
    }

    public void setLquTpMetRiscFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TP_MET_RISC, Pos.LQU_TP_MET_RISC);
    }

    /**Original name: LQU-TP-MET-RISC<br>*/
    public short getLquTpMetRisc() {
        return readPackedAsShort(Pos.LQU_TP_MET_RISC, Len.Int.LQU_TP_MET_RISC);
    }

    public byte[] getLquTpMetRiscAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TP_MET_RISC, Pos.LQU_TP_MET_RISC);
        return buffer;
    }

    public void setLquTpMetRiscNull(String lquTpMetRiscNull) {
        writeString(Pos.LQU_TP_MET_RISC_NULL, lquTpMetRiscNull, Len.LQU_TP_MET_RISC_NULL);
    }

    /**Original name: LQU-TP-MET-RISC-NULL<br>*/
    public String getLquTpMetRiscNull() {
        return readString(Pos.LQU_TP_MET_RISC_NULL, Len.LQU_TP_MET_RISC_NULL);
    }

    public String getLquTpMetRiscNullFormatted() {
        return Functions.padBlanks(getLquTpMetRiscNull(), Len.LQU_TP_MET_RISC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TP_MET_RISC = 1;
        public static final int LQU_TP_MET_RISC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TP_MET_RISC = 2;
        public static final int LQU_TP_MET_RISC_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TP_MET_RISC = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
