package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: A25-ID-SEGMENTAZ-CLI<br>
 * Variable: A25-ID-SEGMENTAZ-CLI from program LDBS1300<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A25IdSegmentazCli extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A25IdSegmentazCli() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A25_ID_SEGMENTAZ_CLI;
    }

    public void setA25IdSegmentazCli(int a25IdSegmentazCli) {
        writeIntAsPacked(Pos.A25_ID_SEGMENTAZ_CLI, a25IdSegmentazCli, Len.Int.A25_ID_SEGMENTAZ_CLI);
    }

    public void setA25IdSegmentazCliFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A25_ID_SEGMENTAZ_CLI, Pos.A25_ID_SEGMENTAZ_CLI);
    }

    /**Original name: A25-ID-SEGMENTAZ-CLI<br>*/
    public int getA25IdSegmentazCli() {
        return readPackedAsInt(Pos.A25_ID_SEGMENTAZ_CLI, Len.Int.A25_ID_SEGMENTAZ_CLI);
    }

    public byte[] getA25IdSegmentazCliAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A25_ID_SEGMENTAZ_CLI, Pos.A25_ID_SEGMENTAZ_CLI);
        return buffer;
    }

    public void setA25IdSegmentazCliNull(String a25IdSegmentazCliNull) {
        writeString(Pos.A25_ID_SEGMENTAZ_CLI_NULL, a25IdSegmentazCliNull, Len.A25_ID_SEGMENTAZ_CLI_NULL);
    }

    /**Original name: A25-ID-SEGMENTAZ-CLI-NULL<br>*/
    public String getA25IdSegmentazCliNull() {
        return readString(Pos.A25_ID_SEGMENTAZ_CLI_NULL, Len.A25_ID_SEGMENTAZ_CLI_NULL);
    }

    public String getA25IdSegmentazCliNullFormatted() {
        return Functions.padBlanks(getA25IdSegmentazCliNull(), Len.A25_ID_SEGMENTAZ_CLI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A25_ID_SEGMENTAZ_CLI = 1;
        public static final int A25_ID_SEGMENTAZ_CLI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_ID_SEGMENTAZ_CLI = 5;
        public static final int A25_ID_SEGMENTAZ_CLI_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_ID_SEGMENTAZ_CLI = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
