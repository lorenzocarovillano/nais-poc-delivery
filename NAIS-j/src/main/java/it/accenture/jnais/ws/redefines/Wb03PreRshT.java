package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRE-RSH-T<br>
 * Variable: WB03-PRE-RSH-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PreRshT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PreRshT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRE_RSH_T;
    }

    public void setWb03PreRshTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRE_RSH_T, Pos.WB03_PRE_RSH_T);
    }

    /**Original name: WB03-PRE-RSH-T<br>*/
    public AfDecimal getWb03PreRshT() {
        return readPackedAsDecimal(Pos.WB03_PRE_RSH_T, Len.Int.WB03_PRE_RSH_T, Len.Fract.WB03_PRE_RSH_T);
    }

    public byte[] getWb03PreRshTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRE_RSH_T, Pos.WB03_PRE_RSH_T);
        return buffer;
    }

    public void setWb03PreRshTNull(String wb03PreRshTNull) {
        writeString(Pos.WB03_PRE_RSH_T_NULL, wb03PreRshTNull, Len.WB03_PRE_RSH_T_NULL);
    }

    /**Original name: WB03-PRE-RSH-T-NULL<br>*/
    public String getWb03PreRshTNull() {
        return readString(Pos.WB03_PRE_RSH_T_NULL, Len.WB03_PRE_RSH_T_NULL);
    }

    public String getWb03PreRshTNullFormatted() {
        return Functions.padBlanks(getWb03PreRshTNull(), Len.WB03_PRE_RSH_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRE_RSH_T = 1;
        public static final int WB03_PRE_RSH_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRE_RSH_T = 8;
        public static final int WB03_PRE_RSH_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRE_RSH_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRE_RSH_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
