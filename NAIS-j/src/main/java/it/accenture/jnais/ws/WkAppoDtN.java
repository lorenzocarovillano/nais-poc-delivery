package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-APPO-DT-N<br>
 * Variable: WK-APPO-DT-N from program LOAS0820<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkAppoDtN {

    //==== PROPERTIES ====
    //Original name: WK-DT-N-AAAA
    private String aaaa = DefaultValues.stringVal(Len.AAAA);
    //Original name: WK-DT-N-MM
    private String mm = DefaultValues.stringVal(Len.MM);
    //Original name: WK-DT-N-GG
    private String gg = DefaultValues.stringVal(Len.GG);

    //==== METHODS ====
    public void setWkAppoDtNFormatted(String data) {
        byte[] buffer = new byte[Len.WK_APPO_DT_N];
        MarshalByte.writeString(buffer, 1, data, Len.WK_APPO_DT_N);
        setWkAppoDtNBytes(buffer, 1);
    }

    public byte[] getWkAppoDtNBytes() {
        byte[] buffer = new byte[Len.WK_APPO_DT_N];
        return getWkAppoDtNBytes(buffer, 1);
    }

    public void setWkAppoDtNBytes(byte[] buffer, int offset) {
        int position = offset;
        aaaa = MarshalByte.readFixedString(buffer, position, Len.AAAA);
        position += Len.AAAA;
        mm = MarshalByte.readFixedString(buffer, position, Len.MM);
        position += Len.MM;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
    }

    public byte[] getWkAppoDtNBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aaaa, Len.AAAA);
        position += Len.AAAA;
        MarshalByte.writeString(buffer, position, mm, Len.MM);
        position += Len.MM;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        return buffer;
    }

    public String getAaaaFormatted() {
        return this.aaaa;
    }

    public void setMm(short mm) {
        this.mm = NumericDisplay.asString(mm, Len.MM);
    }

    public short getMm() {
        return NumericDisplay.asShort(this.mm);
    }

    public String getMmFormatted() {
        return this.mm;
    }

    public void setGg(short gg) {
        this.gg = NumericDisplay.asString(gg, Len.GG);
    }

    public short getGg() {
        return NumericDisplay.asShort(this.gg);
    }

    public String getGgFormatted() {
        return this.gg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WK_APPO_DT_N = AAAA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
