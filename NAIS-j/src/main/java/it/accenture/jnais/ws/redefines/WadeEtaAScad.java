package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WADE-ETA-A-SCAD<br>
 * Variable: WADE-ETA-A-SCAD from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeEtaAScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeEtaAScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_ETA_A_SCAD;
    }

    public void setWadeEtaAScad(int wadeEtaAScad) {
        writeIntAsPacked(Pos.WADE_ETA_A_SCAD, wadeEtaAScad, Len.Int.WADE_ETA_A_SCAD);
    }

    public void setWadeEtaAScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_ETA_A_SCAD, Pos.WADE_ETA_A_SCAD);
    }

    /**Original name: WADE-ETA-A-SCAD<br>*/
    public int getWadeEtaAScad() {
        return readPackedAsInt(Pos.WADE_ETA_A_SCAD, Len.Int.WADE_ETA_A_SCAD);
    }

    public byte[] getWadeEtaAScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_ETA_A_SCAD, Pos.WADE_ETA_A_SCAD);
        return buffer;
    }

    public void initWadeEtaAScadSpaces() {
        fill(Pos.WADE_ETA_A_SCAD, Len.WADE_ETA_A_SCAD, Types.SPACE_CHAR);
    }

    public void setWadeEtaAScadNull(String wadeEtaAScadNull) {
        writeString(Pos.WADE_ETA_A_SCAD_NULL, wadeEtaAScadNull, Len.WADE_ETA_A_SCAD_NULL);
    }

    /**Original name: WADE-ETA-A-SCAD-NULL<br>*/
    public String getWadeEtaAScadNull() {
        return readString(Pos.WADE_ETA_A_SCAD_NULL, Len.WADE_ETA_A_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_ETA_A_SCAD = 1;
        public static final int WADE_ETA_A_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_ETA_A_SCAD = 3;
        public static final int WADE_ETA_A_SCAD_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_ETA_A_SCAD = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
