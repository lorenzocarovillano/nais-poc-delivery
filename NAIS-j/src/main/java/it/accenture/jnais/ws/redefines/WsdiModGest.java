package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WSDI-MOD-GEST<br>
 * Variable: WSDI-MOD-GEST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsdiModGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsdiModGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSDI_MOD_GEST;
    }

    public void setWsdiModGest(short wsdiModGest) {
        writeShortAsPacked(Pos.WSDI_MOD_GEST, wsdiModGest, Len.Int.WSDI_MOD_GEST);
    }

    public void setWsdiModGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSDI_MOD_GEST, Pos.WSDI_MOD_GEST);
    }

    /**Original name: WSDI-MOD-GEST<br>*/
    public short getWsdiModGest() {
        return readPackedAsShort(Pos.WSDI_MOD_GEST, Len.Int.WSDI_MOD_GEST);
    }

    public byte[] getWsdiModGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSDI_MOD_GEST, Pos.WSDI_MOD_GEST);
        return buffer;
    }

    public void initWsdiModGestSpaces() {
        fill(Pos.WSDI_MOD_GEST, Len.WSDI_MOD_GEST, Types.SPACE_CHAR);
    }

    public void setWsdiModGestNull(String wsdiModGestNull) {
        writeString(Pos.WSDI_MOD_GEST_NULL, wsdiModGestNull, Len.WSDI_MOD_GEST_NULL);
    }

    /**Original name: WSDI-MOD-GEST-NULL<br>*/
    public String getWsdiModGestNull() {
        return readString(Pos.WSDI_MOD_GEST_NULL, Len.WSDI_MOD_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSDI_MOD_GEST = 1;
        public static final int WSDI_MOD_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WSDI_MOD_GEST = 2;
        public static final int WSDI_MOD_GEST_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WSDI_MOD_GEST = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
