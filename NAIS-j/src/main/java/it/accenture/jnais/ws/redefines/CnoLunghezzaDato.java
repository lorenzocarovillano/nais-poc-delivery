package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: CNO-LUNGHEZZA-DATO<br>
 * Variable: CNO-LUNGHEZZA-DATO from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CnoLunghezzaDato extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public CnoLunghezzaDato() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.CNO_LUNGHEZZA_DATO;
    }

    public void setCnoLunghezzaDato(int cnoLunghezzaDato) {
        writeIntAsPacked(Pos.CNO_LUNGHEZZA_DATO, cnoLunghezzaDato, Len.Int.CNO_LUNGHEZZA_DATO);
    }

    public void setCnoLunghezzaDatoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.CNO_LUNGHEZZA_DATO, Pos.CNO_LUNGHEZZA_DATO);
    }

    /**Original name: CNO-LUNGHEZZA-DATO<br>*/
    public int getCnoLunghezzaDato() {
        return readPackedAsInt(Pos.CNO_LUNGHEZZA_DATO, Len.Int.CNO_LUNGHEZZA_DATO);
    }

    public byte[] getCnoLunghezzaDatoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.CNO_LUNGHEZZA_DATO, Pos.CNO_LUNGHEZZA_DATO);
        return buffer;
    }

    public void setCnoLunghezzaDatoNull(String cnoLunghezzaDatoNull) {
        writeString(Pos.CNO_LUNGHEZZA_DATO_NULL, cnoLunghezzaDatoNull, Len.CNO_LUNGHEZZA_DATO_NULL);
    }

    /**Original name: CNO-LUNGHEZZA-DATO-NULL<br>*/
    public String getCnoLunghezzaDatoNull() {
        return readString(Pos.CNO_LUNGHEZZA_DATO_NULL, Len.CNO_LUNGHEZZA_DATO_NULL);
    }

    public String getCnoLunghezzaDatoNullFormatted() {
        return Functions.padBlanks(getCnoLunghezzaDatoNull(), Len.CNO_LUNGHEZZA_DATO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int CNO_LUNGHEZZA_DATO = 1;
        public static final int CNO_LUNGHEZZA_DATO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int CNO_LUNGHEZZA_DATO = 3;
        public static final int CNO_LUNGHEZZA_DATO_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CNO_LUNGHEZZA_DATO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
