package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-DT-SCAD<br>
 * Variable: ADE-DT-SCAD from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeDtScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeDtScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_DT_SCAD;
    }

    public void setAdeDtScad(int adeDtScad) {
        writeIntAsPacked(Pos.ADE_DT_SCAD, adeDtScad, Len.Int.ADE_DT_SCAD);
    }

    public void setAdeDtScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_DT_SCAD, Pos.ADE_DT_SCAD);
    }

    /**Original name: ADE-DT-SCAD<br>*/
    public int getAdeDtScad() {
        return readPackedAsInt(Pos.ADE_DT_SCAD, Len.Int.ADE_DT_SCAD);
    }

    public byte[] getAdeDtScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_DT_SCAD, Pos.ADE_DT_SCAD);
        return buffer;
    }

    public void setAdeDtScadNull(String adeDtScadNull) {
        writeString(Pos.ADE_DT_SCAD_NULL, adeDtScadNull, Len.ADE_DT_SCAD_NULL);
    }

    /**Original name: ADE-DT-SCAD-NULL<br>*/
    public String getAdeDtScadNull() {
        return readString(Pos.ADE_DT_SCAD_NULL, Len.ADE_DT_SCAD_NULL);
    }

    public String getAdeDtScadNullFormatted() {
        return Functions.padBlanks(getAdeDtScadNull(), Len.ADE_DT_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_DT_SCAD = 1;
        public static final int ADE_DT_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_DT_SCAD = 5;
        public static final int ADE_DT_SCAD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_DT_SCAD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
