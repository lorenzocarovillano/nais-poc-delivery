package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-RIS-RIASTA-ECC<br>
 * Variable: WB03-RIS-RIASTA-ECC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03RisRiastaEcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03RisRiastaEcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_RIS_RIASTA_ECC;
    }

    public void setWb03RisRiastaEccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_RIS_RIASTA_ECC, Pos.WB03_RIS_RIASTA_ECC);
    }

    /**Original name: WB03-RIS-RIASTA-ECC<br>*/
    public AfDecimal getWb03RisRiastaEcc() {
        return readPackedAsDecimal(Pos.WB03_RIS_RIASTA_ECC, Len.Int.WB03_RIS_RIASTA_ECC, Len.Fract.WB03_RIS_RIASTA_ECC);
    }

    public byte[] getWb03RisRiastaEccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_RIS_RIASTA_ECC, Pos.WB03_RIS_RIASTA_ECC);
        return buffer;
    }

    public void setWb03RisRiastaEccNull(String wb03RisRiastaEccNull) {
        writeString(Pos.WB03_RIS_RIASTA_ECC_NULL, wb03RisRiastaEccNull, Len.WB03_RIS_RIASTA_ECC_NULL);
    }

    /**Original name: WB03-RIS-RIASTA-ECC-NULL<br>*/
    public String getWb03RisRiastaEccNull() {
        return readString(Pos.WB03_RIS_RIASTA_ECC_NULL, Len.WB03_RIS_RIASTA_ECC_NULL);
    }

    public String getWb03RisRiastaEccNullFormatted() {
        return Functions.padBlanks(getWb03RisRiastaEccNull(), Len.WB03_RIS_RIASTA_ECC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_RIS_RIASTA_ECC = 1;
        public static final int WB03_RIS_RIASTA_ECC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_RIS_RIASTA_ECC = 8;
        public static final int WB03_RIS_RIASTA_ECC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_RIS_RIASTA_ECC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_RIS_RIASTA_ECC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
