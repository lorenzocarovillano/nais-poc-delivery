package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-PC-RISC-PARZ<br>
 * Variable: LQU-PC-RISC-PARZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquPcRiscParz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquPcRiscParz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_PC_RISC_PARZ;
    }

    public void setLquPcRiscParz(AfDecimal lquPcRiscParz) {
        writeDecimalAsPacked(Pos.LQU_PC_RISC_PARZ, lquPcRiscParz.copy());
    }

    public void setLquPcRiscParzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_PC_RISC_PARZ, Pos.LQU_PC_RISC_PARZ);
    }

    /**Original name: LQU-PC-RISC-PARZ<br>*/
    public AfDecimal getLquPcRiscParz() {
        return readPackedAsDecimal(Pos.LQU_PC_RISC_PARZ, Len.Int.LQU_PC_RISC_PARZ, Len.Fract.LQU_PC_RISC_PARZ);
    }

    public byte[] getLquPcRiscParzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_PC_RISC_PARZ, Pos.LQU_PC_RISC_PARZ);
        return buffer;
    }

    public void setLquPcRiscParzNull(String lquPcRiscParzNull) {
        writeString(Pos.LQU_PC_RISC_PARZ_NULL, lquPcRiscParzNull, Len.LQU_PC_RISC_PARZ_NULL);
    }

    /**Original name: LQU-PC-RISC-PARZ-NULL<br>*/
    public String getLquPcRiscParzNull() {
        return readString(Pos.LQU_PC_RISC_PARZ_NULL, Len.LQU_PC_RISC_PARZ_NULL);
    }

    public String getLquPcRiscParzNullFormatted() {
        return Functions.padBlanks(getLquPcRiscParzNull(), Len.LQU_PC_RISC_PARZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_PC_RISC_PARZ = 1;
        public static final int LQU_PC_RISC_PARZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_PC_RISC_PARZ = 7;
        public static final int LQU_PC_RISC_PARZ_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_PC_RISC_PARZ = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_PC_RISC_PARZ = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
