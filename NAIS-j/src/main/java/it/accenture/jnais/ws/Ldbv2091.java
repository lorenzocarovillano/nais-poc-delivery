package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LDBV2091<br>
 * Variable: LDBV2091 from copybook LDBV2091<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv2091 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV2091-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV2091-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV2091-TP-STAT-TIT-01
    private String tpStatTit01 = DefaultValues.stringVal(Len.TP_STAT_TIT01);
    //Original name: LDBV2091-TP-STAT-TIT-02
    private String tpStatTit02 = DefaultValues.stringVal(Len.TP_STAT_TIT02);
    //Original name: LDBV2091-TP-STAT-TIT-03
    private String tpStatTit03 = DefaultValues.stringVal(Len.TP_STAT_TIT03);
    //Original name: LDBV2091-TP-STAT-TIT-04
    private String tpStatTit04 = DefaultValues.stringVal(Len.TP_STAT_TIT04);
    //Original name: LDBV2091-TP-STAT-TIT-05
    private String tpStatTit05 = DefaultValues.stringVal(Len.TP_STAT_TIT05);
    //Original name: LDBV2091-TOT-PREMI
    private AfDecimal totPremi = new AfDecimal(DefaultValues.DEC_VAL, 18, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV2091;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv2091Bytes(buf);
    }

    public String getLdbv2131Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv2091Bytes());
    }

    public void setLdbv2091Bytes(byte[] buffer) {
        setLdbv2091Bytes(buffer, 1);
    }

    public byte[] getLdbv2091Bytes() {
        byte[] buffer = new byte[Len.LDBV2091];
        return getLdbv2091Bytes(buffer, 1);
    }

    public void setLdbv2091Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpStatTit01 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT01);
        position += Len.TP_STAT_TIT01;
        tpStatTit02 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT02);
        position += Len.TP_STAT_TIT02;
        tpStatTit03 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT03);
        position += Len.TP_STAT_TIT03;
        tpStatTit04 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT04);
        position += Len.TP_STAT_TIT04;
        tpStatTit05 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT05);
        position += Len.TP_STAT_TIT05;
        totPremi.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.TOT_PREMI, Len.Fract.TOT_PREMI));
    }

    public byte[] getLdbv2091Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tpStatTit01, Len.TP_STAT_TIT01);
        position += Len.TP_STAT_TIT01;
        MarshalByte.writeString(buffer, position, tpStatTit02, Len.TP_STAT_TIT02);
        position += Len.TP_STAT_TIT02;
        MarshalByte.writeString(buffer, position, tpStatTit03, Len.TP_STAT_TIT03);
        position += Len.TP_STAT_TIT03;
        MarshalByte.writeString(buffer, position, tpStatTit04, Len.TP_STAT_TIT04);
        position += Len.TP_STAT_TIT04;
        MarshalByte.writeString(buffer, position, tpStatTit05, Len.TP_STAT_TIT05);
        position += Len.TP_STAT_TIT05;
        MarshalByte.writeDecimalAsPacked(buffer, position, totPremi.copy());
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpStatTit01(String tpStatTit01) {
        this.tpStatTit01 = Functions.subString(tpStatTit01, Len.TP_STAT_TIT01);
    }

    public String getTpStatTit01() {
        return this.tpStatTit01;
    }

    public void setTpStatTit02(String tpStatTit02) {
        this.tpStatTit02 = Functions.subString(tpStatTit02, Len.TP_STAT_TIT02);
    }

    public String getTpStatTit02() {
        return this.tpStatTit02;
    }

    public void setTpStatTit03(String tpStatTit03) {
        this.tpStatTit03 = Functions.subString(tpStatTit03, Len.TP_STAT_TIT03);
    }

    public String getTpStatTit03() {
        return this.tpStatTit03;
    }

    public void setTpStatTit04(String tpStatTit04) {
        this.tpStatTit04 = Functions.subString(tpStatTit04, Len.TP_STAT_TIT04);
    }

    public String getTpStatTit04() {
        return this.tpStatTit04;
    }

    public void setTpStatTit05(String tpStatTit05) {
        this.tpStatTit05 = Functions.subString(tpStatTit05, Len.TP_STAT_TIT05);
    }

    public String getTpStatTit05() {
        return this.tpStatTit05;
    }

    public void setTotPremi(AfDecimal totPremi) {
        this.totPremi.assign(totPremi);
    }

    public AfDecimal getTotPremi() {
        return this.totPremi.copy();
    }

    @Override
    public byte[] serialize() {
        return getLdbv2091Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int TP_STAT_TIT01 = 2;
        public static final int TP_STAT_TIT02 = 2;
        public static final int TP_STAT_TIT03 = 2;
        public static final int TP_STAT_TIT04 = 2;
        public static final int TP_STAT_TIT05 = 2;
        public static final int TOT_PREMI = 10;
        public static final int LDBV2091 = ID_OGG + TP_OGG + TP_STAT_TIT01 + TP_STAT_TIT02 + TP_STAT_TIT03 + TP_STAT_TIT04 + TP_STAT_TIT05 + TOT_PREMI;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int TOT_PREMI = 15;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TOT_PREMI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
