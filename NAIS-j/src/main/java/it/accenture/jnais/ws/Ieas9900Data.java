package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import it.accenture.jnais.copy.GravitaErrore;
import it.accenture.jnais.copy.LinguaErrore;
import it.accenture.jnais.ws.occurs.WkTabellaErrori;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IEAS9900<br>
 * Generated as a class for rule WS.<br>*/
public class Ieas9900Data {

    //==== PROPERTIES ====
    public static final int WK_TABELLA_ERRORI_MAXOCCURS = 20;
    //Original name: GRAVITA-ERRORE
    private GravitaErrore gravitaErrore = new GravitaErrore();
    //Original name: LINGUA-ERRORE
    private LinguaErrore linguaErrore = new LinguaErrore();
    //Original name: SW-SWITCH
    private SwSwitch swSwitch = new SwSwitch();
    //Original name: COSTANTI
    private Costanti costanti = new Costanti();
    //Original name: IX-IND
    private short ixInd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ERR
    private short ixTabErr = DefaultValues.BIN_SHORT_VAL;
    //Original name: WS-VARIABILI
    private WsVariabiliIeas9900 wsVariabili = new WsVariabiliIeas9900();
    //Original name: WS-SQLCODE
    private String wsSqlcode = DefaultValues.stringVal(Len.WS_SQLCODE);
    //Original name: WK-ERRORI-NUM-MAX-ELE
    private short wkErroriNumMaxEle = ((short)0);
    //Original name: WK-TABELLA-ERRORI
    private WkTabellaErrori[] wkTabellaErrori = new WkTabellaErrori[WK_TABELLA_ERRORI_MAXOCCURS];
    //Original name: IEAI9701-AREA
    private Ieai9701Area ieai9701Area = new Ieai9701Area();
    //Original name: IEAO9701-AREA
    private Ieao9701Area ieao9701Area = new Ieao9701Area();
    //Original name: IEAI9801-AREA
    private Ieai9801Area ieai9801Area = new Ieai9801Area();
    //Original name: IEAO9801-AREA
    private Ieao9801Area ieao9801Area = new Ieao9801Area();

    //==== CONSTRUCTORS ====
    public Ieas9900Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wkTabellaErroriIdx = 1; wkTabellaErroriIdx <= WK_TABELLA_ERRORI_MAXOCCURS; wkTabellaErroriIdx++) {
            wkTabellaErrori[wkTabellaErroriIdx - 1] = new WkTabellaErrori();
        }
    }

    public void setIxInd(short ixInd) {
        this.ixInd = ixInd;
    }

    public short getIxInd() {
        return this.ixInd;
    }

    public void setIxTabErr(short ixTabErr) {
        this.ixTabErr = ixTabErr;
    }

    public short getIxTabErr() {
        return this.ixTabErr;
    }

    public void setWsSqlcode(long wsSqlcode) {
        this.wsSqlcode = PicFormatter.display("-9(4)").format(wsSqlcode).toString();
    }

    public long getWsSqlcode() {
        return PicParser.display("-9(4)").parseLong(this.wsSqlcode);
    }

    public String getWsSqlcodeFormatted() {
        return this.wsSqlcode;
    }

    public String getWsSqlcodeAsString() {
        return getWsSqlcodeFormatted();
    }

    public void setWkErroriNumMaxEle(short wkErroriNumMaxEle) {
        this.wkErroriNumMaxEle = wkErroriNumMaxEle;
    }

    public short getWkErroriNumMaxEle() {
        return this.wkErroriNumMaxEle;
    }

    public Costanti getCostanti() {
        return costanti;
    }

    public GravitaErrore getGravitaErrore() {
        return gravitaErrore;
    }

    public Ieai9701Area getIeai9701Area() {
        return ieai9701Area;
    }

    public Ieai9801Area getIeai9801Area() {
        return ieai9801Area;
    }

    public Ieao9701Area getIeao9701Area() {
        return ieao9701Area;
    }

    public Ieao9801Area getIeao9801Area() {
        return ieao9801Area;
    }

    public LinguaErrore getLinguaErrore() {
        return linguaErrore;
    }

    public SwSwitch getSwSwitch() {
        return swSwitch;
    }

    public WkTabellaErrori getWkTabellaErrori(int idx) {
        return wkTabellaErrori[idx - 1];
    }

    public WsVariabiliIeas9900 getWsVariabili() {
        return wsVariabili;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_SQLCODE = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
