package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ALQ-TAX-SEP-EFFLQ<br>
 * Variable: WDFL-ALQ-TAX-SEP-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAlqTaxSepEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAlqTaxSepEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ALQ_TAX_SEP_EFFLQ;
    }

    public void setWdflAlqTaxSepEfflq(AfDecimal wdflAlqTaxSepEfflq) {
        writeDecimalAsPacked(Pos.WDFL_ALQ_TAX_SEP_EFFLQ, wdflAlqTaxSepEfflq.copy());
    }

    public void setWdflAlqTaxSepEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ALQ_TAX_SEP_EFFLQ, Pos.WDFL_ALQ_TAX_SEP_EFFLQ);
    }

    /**Original name: WDFL-ALQ-TAX-SEP-EFFLQ<br>*/
    public AfDecimal getWdflAlqTaxSepEfflq() {
        return readPackedAsDecimal(Pos.WDFL_ALQ_TAX_SEP_EFFLQ, Len.Int.WDFL_ALQ_TAX_SEP_EFFLQ, Len.Fract.WDFL_ALQ_TAX_SEP_EFFLQ);
    }

    public byte[] getWdflAlqTaxSepEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ALQ_TAX_SEP_EFFLQ, Pos.WDFL_ALQ_TAX_SEP_EFFLQ);
        return buffer;
    }

    public void setWdflAlqTaxSepEfflqNull(String wdflAlqTaxSepEfflqNull) {
        writeString(Pos.WDFL_ALQ_TAX_SEP_EFFLQ_NULL, wdflAlqTaxSepEfflqNull, Len.WDFL_ALQ_TAX_SEP_EFFLQ_NULL);
    }

    /**Original name: WDFL-ALQ-TAX-SEP-EFFLQ-NULL<br>*/
    public String getWdflAlqTaxSepEfflqNull() {
        return readString(Pos.WDFL_ALQ_TAX_SEP_EFFLQ_NULL, Len.WDFL_ALQ_TAX_SEP_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_TAX_SEP_EFFLQ = 1;
        public static final int WDFL_ALQ_TAX_SEP_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_TAX_SEP_EFFLQ = 4;
        public static final int WDFL_ALQ_TAX_SEP_EFFLQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_TAX_SEP_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_TAX_SEP_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
