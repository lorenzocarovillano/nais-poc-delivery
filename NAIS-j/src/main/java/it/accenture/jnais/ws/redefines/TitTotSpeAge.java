package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-SPE-AGE<br>
 * Variable: TIT-TOT-SPE-AGE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotSpeAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotSpeAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_SPE_AGE;
    }

    public void setTitTotSpeAge(AfDecimal titTotSpeAge) {
        writeDecimalAsPacked(Pos.TIT_TOT_SPE_AGE, titTotSpeAge.copy());
    }

    public void setTitTotSpeAgeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_SPE_AGE, Pos.TIT_TOT_SPE_AGE);
    }

    /**Original name: TIT-TOT-SPE-AGE<br>*/
    public AfDecimal getTitTotSpeAge() {
        return readPackedAsDecimal(Pos.TIT_TOT_SPE_AGE, Len.Int.TIT_TOT_SPE_AGE, Len.Fract.TIT_TOT_SPE_AGE);
    }

    public byte[] getTitTotSpeAgeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_SPE_AGE, Pos.TIT_TOT_SPE_AGE);
        return buffer;
    }

    public void setTitTotSpeAgeNull(String titTotSpeAgeNull) {
        writeString(Pos.TIT_TOT_SPE_AGE_NULL, titTotSpeAgeNull, Len.TIT_TOT_SPE_AGE_NULL);
    }

    /**Original name: TIT-TOT-SPE-AGE-NULL<br>*/
    public String getTitTotSpeAgeNull() {
        return readString(Pos.TIT_TOT_SPE_AGE_NULL, Len.TIT_TOT_SPE_AGE_NULL);
    }

    public String getTitTotSpeAgeNullFormatted() {
        return Functions.padBlanks(getTitTotSpeAgeNull(), Len.TIT_TOT_SPE_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SPE_AGE = 1;
        public static final int TIT_TOT_SPE_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SPE_AGE = 8;
        public static final int TIT_TOT_SPE_AGE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SPE_AGE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SPE_AGE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
