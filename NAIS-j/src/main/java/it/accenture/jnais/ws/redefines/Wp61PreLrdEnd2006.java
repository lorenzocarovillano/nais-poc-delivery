package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP61-PRE-LRD-END2006<br>
 * Variable: WP61-PRE-LRD-END2006 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp61PreLrdEnd2006 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp61PreLrdEnd2006() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP61_PRE_LRD_END2006;
    }

    public void setWp61PreLrdEnd2006(AfDecimal wp61PreLrdEnd2006) {
        writeDecimalAsPacked(Pos.WP61_PRE_LRD_END2006, wp61PreLrdEnd2006.copy());
    }

    public void setWp61PreLrdEnd2006FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP61_PRE_LRD_END2006, Pos.WP61_PRE_LRD_END2006);
    }

    /**Original name: WP61-PRE-LRD-END2006<br>*/
    public AfDecimal getWp61PreLrdEnd2006() {
        return readPackedAsDecimal(Pos.WP61_PRE_LRD_END2006, Len.Int.WP61_PRE_LRD_END2006, Len.Fract.WP61_PRE_LRD_END2006);
    }

    public byte[] getWp61PreLrdEnd2006AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP61_PRE_LRD_END2006, Pos.WP61_PRE_LRD_END2006);
        return buffer;
    }

    public void setWp61PreLrdEnd2006Null(String wp61PreLrdEnd2006Null) {
        writeString(Pos.WP61_PRE_LRD_END2006_NULL, wp61PreLrdEnd2006Null, Len.WP61_PRE_LRD_END2006_NULL);
    }

    /**Original name: WP61-PRE-LRD-END2006-NULL<br>*/
    public String getWp61PreLrdEnd2006Null() {
        return readString(Pos.WP61_PRE_LRD_END2006_NULL, Len.WP61_PRE_LRD_END2006_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP61_PRE_LRD_END2006 = 1;
        public static final int WP61_PRE_LRD_END2006_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_PRE_LRD_END2006 = 8;
        public static final int WP61_PRE_LRD_END2006_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP61_PRE_LRD_END2006 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_PRE_LRD_END2006 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
