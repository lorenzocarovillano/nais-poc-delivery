package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WSPG-ID-MOVI-CHIU<br>
 * Variable: WSPG-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WspgIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WspgIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSPG_ID_MOVI_CHIU;
    }

    public void setWspgIdMoviChiu(int wspgIdMoviChiu) {
        writeIntAsPacked(Pos.WSPG_ID_MOVI_CHIU, wspgIdMoviChiu, Len.Int.WSPG_ID_MOVI_CHIU);
    }

    public void setWspgIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSPG_ID_MOVI_CHIU, Pos.WSPG_ID_MOVI_CHIU);
    }

    /**Original name: WSPG-ID-MOVI-CHIU<br>*/
    public int getWspgIdMoviChiu() {
        return readPackedAsInt(Pos.WSPG_ID_MOVI_CHIU, Len.Int.WSPG_ID_MOVI_CHIU);
    }

    public byte[] getWspgIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSPG_ID_MOVI_CHIU, Pos.WSPG_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWspgIdMoviChiuSpaces() {
        fill(Pos.WSPG_ID_MOVI_CHIU, Len.WSPG_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWspgIdMoviChiuNull(String wspgIdMoviChiuNull) {
        writeString(Pos.WSPG_ID_MOVI_CHIU_NULL, wspgIdMoviChiuNull, Len.WSPG_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WSPG-ID-MOVI-CHIU-NULL<br>*/
    public String getWspgIdMoviChiuNull() {
        return readString(Pos.WSPG_ID_MOVI_CHIU_NULL, Len.WSPG_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSPG_ID_MOVI_CHIU = 1;
        public static final int WSPG_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WSPG_ID_MOVI_CHIU = 5;
        public static final int WSPG_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WSPG_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
