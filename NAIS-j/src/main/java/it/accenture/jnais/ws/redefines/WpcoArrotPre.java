package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-ARROT-PRE<br>
 * Variable: WPCO-ARROT-PRE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoArrotPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoArrotPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_ARROT_PRE;
    }

    public void setWpcoArrotPre(AfDecimal wpcoArrotPre) {
        writeDecimalAsPacked(Pos.WPCO_ARROT_PRE, wpcoArrotPre.copy());
    }

    public void setDpcoArrotPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_ARROT_PRE, Pos.WPCO_ARROT_PRE);
    }

    /**Original name: WPCO-ARROT-PRE<br>*/
    public AfDecimal getWpcoArrotPre() {
        return readPackedAsDecimal(Pos.WPCO_ARROT_PRE, Len.Int.WPCO_ARROT_PRE, Len.Fract.WPCO_ARROT_PRE);
    }

    public byte[] getWpcoArrotPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_ARROT_PRE, Pos.WPCO_ARROT_PRE);
        return buffer;
    }

    public void setWpcoArrotPreNull(String wpcoArrotPreNull) {
        writeString(Pos.WPCO_ARROT_PRE_NULL, wpcoArrotPreNull, Len.WPCO_ARROT_PRE_NULL);
    }

    /**Original name: WPCO-ARROT-PRE-NULL<br>*/
    public String getWpcoArrotPreNull() {
        return readString(Pos.WPCO_ARROT_PRE_NULL, Len.WPCO_ARROT_PRE_NULL);
    }

    public String getWpcoArrotPreNullFormatted() {
        return Functions.padBlanks(getWpcoArrotPreNull(), Len.WPCO_ARROT_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_ARROT_PRE = 1;
        public static final int WPCO_ARROT_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_ARROT_PRE = 8;
        public static final int WPCO_ARROT_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_ARROT_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_ARROT_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
