package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IINT-PREST-DFZ<br>
 * Variable: WDFL-IINT-PREST-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIintPrestDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIintPrestDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IINT_PREST_DFZ;
    }

    public void setWdflIintPrestDfz(AfDecimal wdflIintPrestDfz) {
        writeDecimalAsPacked(Pos.WDFL_IINT_PREST_DFZ, wdflIintPrestDfz.copy());
    }

    public void setWdflIintPrestDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IINT_PREST_DFZ, Pos.WDFL_IINT_PREST_DFZ);
    }

    /**Original name: WDFL-IINT-PREST-DFZ<br>*/
    public AfDecimal getWdflIintPrestDfz() {
        return readPackedAsDecimal(Pos.WDFL_IINT_PREST_DFZ, Len.Int.WDFL_IINT_PREST_DFZ, Len.Fract.WDFL_IINT_PREST_DFZ);
    }

    public byte[] getWdflIintPrestDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IINT_PREST_DFZ, Pos.WDFL_IINT_PREST_DFZ);
        return buffer;
    }

    public void setWdflIintPrestDfzNull(String wdflIintPrestDfzNull) {
        writeString(Pos.WDFL_IINT_PREST_DFZ_NULL, wdflIintPrestDfzNull, Len.WDFL_IINT_PREST_DFZ_NULL);
    }

    /**Original name: WDFL-IINT-PREST-DFZ-NULL<br>*/
    public String getWdflIintPrestDfzNull() {
        return readString(Pos.WDFL_IINT_PREST_DFZ_NULL, Len.WDFL_IINT_PREST_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IINT_PREST_DFZ = 1;
        public static final int WDFL_IINT_PREST_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IINT_PREST_DFZ = 8;
        public static final int WDFL_IINT_PREST_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IINT_PREST_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IINT_PREST_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
