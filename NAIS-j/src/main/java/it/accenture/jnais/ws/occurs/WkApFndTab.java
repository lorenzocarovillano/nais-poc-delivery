package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-AP-FND-TAB<br>
 * Variables: WK-AP-FND-TAB from copybook LRGC0041<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WkApFndTab {

    //==== PROPERTIES ====
    //Original name: WK-AP-COD-FND
    private String codFnd = DefaultValues.stringVal(Len.COD_FND);
    //Original name: WK-AP-CNTRVAL
    private AfDecimal cntrval = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    public void setCodFnd(String codFnd) {
        this.codFnd = Functions.subString(codFnd, Len.COD_FND);
    }

    public String getCodFnd() {
        return this.codFnd;
    }

    public void setCntrval(AfDecimal cntrval) {
        this.cntrval.assign(cntrval);
    }

    public AfDecimal getCntrval() {
        return this.cntrval.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_FND = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
