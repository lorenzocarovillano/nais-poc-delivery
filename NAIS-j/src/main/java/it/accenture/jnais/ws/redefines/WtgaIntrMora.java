package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-INTR-MORA<br>
 * Variable: WTGA-INTR-MORA from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaIntrMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaIntrMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_INTR_MORA;
    }

    public void setWtgaIntrMora(AfDecimal wtgaIntrMora) {
        writeDecimalAsPacked(Pos.WTGA_INTR_MORA, wtgaIntrMora.copy());
    }

    public void setWtgaIntrMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_INTR_MORA, Pos.WTGA_INTR_MORA);
    }

    /**Original name: WTGA-INTR-MORA<br>*/
    public AfDecimal getWtgaIntrMora() {
        return readPackedAsDecimal(Pos.WTGA_INTR_MORA, Len.Int.WTGA_INTR_MORA, Len.Fract.WTGA_INTR_MORA);
    }

    public byte[] getWtgaIntrMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_INTR_MORA, Pos.WTGA_INTR_MORA);
        return buffer;
    }

    public void initWtgaIntrMoraSpaces() {
        fill(Pos.WTGA_INTR_MORA, Len.WTGA_INTR_MORA, Types.SPACE_CHAR);
    }

    public void setWtgaIntrMoraNull(String wtgaIntrMoraNull) {
        writeString(Pos.WTGA_INTR_MORA_NULL, wtgaIntrMoraNull, Len.WTGA_INTR_MORA_NULL);
    }

    /**Original name: WTGA-INTR-MORA-NULL<br>*/
    public String getWtgaIntrMoraNull() {
        return readString(Pos.WTGA_INTR_MORA_NULL, Len.WTGA_INTR_MORA_NULL);
    }

    public String getWtgaIntrMoraNullFormatted() {
        return Functions.padBlanks(getWtgaIntrMoraNull(), Len.WTGA_INTR_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_INTR_MORA = 1;
        public static final int WTGA_INTR_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_INTR_MORA = 8;
        public static final int WTGA_INTR_MORA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_INTR_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_INTR_MORA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
