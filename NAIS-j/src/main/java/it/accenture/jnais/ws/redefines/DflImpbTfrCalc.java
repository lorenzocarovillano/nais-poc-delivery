package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-TFR-CALC<br>
 * Variable: DFL-IMPB-TFR-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbTfrCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbTfrCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_TFR_CALC;
    }

    public void setDflImpbTfrCalc(AfDecimal dflImpbTfrCalc) {
        writeDecimalAsPacked(Pos.DFL_IMPB_TFR_CALC, dflImpbTfrCalc.copy());
    }

    public void setDflImpbTfrCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_TFR_CALC, Pos.DFL_IMPB_TFR_CALC);
    }

    /**Original name: DFL-IMPB-TFR-CALC<br>*/
    public AfDecimal getDflImpbTfrCalc() {
        return readPackedAsDecimal(Pos.DFL_IMPB_TFR_CALC, Len.Int.DFL_IMPB_TFR_CALC, Len.Fract.DFL_IMPB_TFR_CALC);
    }

    public byte[] getDflImpbTfrCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_TFR_CALC, Pos.DFL_IMPB_TFR_CALC);
        return buffer;
    }

    public void setDflImpbTfrCalcNull(String dflImpbTfrCalcNull) {
        writeString(Pos.DFL_IMPB_TFR_CALC_NULL, dflImpbTfrCalcNull, Len.DFL_IMPB_TFR_CALC_NULL);
    }

    /**Original name: DFL-IMPB-TFR-CALC-NULL<br>*/
    public String getDflImpbTfrCalcNull() {
        return readString(Pos.DFL_IMPB_TFR_CALC_NULL, Len.DFL_IMPB_TFR_CALC_NULL);
    }

    public String getDflImpbTfrCalcNullFormatted() {
        return Functions.padBlanks(getDflImpbTfrCalcNull(), Len.DFL_IMPB_TFR_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TFR_CALC = 1;
        public static final int DFL_IMPB_TFR_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TFR_CALC = 8;
        public static final int DFL_IMPB_TFR_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TFR_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TFR_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
