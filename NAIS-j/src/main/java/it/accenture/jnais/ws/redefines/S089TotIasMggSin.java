package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IAS-MGG-SIN<br>
 * Variable: S089-TOT-IAS-MGG-SIN from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotIasMggSin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotIasMggSin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IAS_MGG_SIN;
    }

    public void setWlquTotIasMggSin(AfDecimal wlquTotIasMggSin) {
        writeDecimalAsPacked(Pos.S089_TOT_IAS_MGG_SIN, wlquTotIasMggSin.copy());
    }

    public void setWlquTotIasMggSinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IAS_MGG_SIN, Pos.S089_TOT_IAS_MGG_SIN);
    }

    /**Original name: WLQU-TOT-IAS-MGG-SIN<br>*/
    public AfDecimal getWlquTotIasMggSin() {
        return readPackedAsDecimal(Pos.S089_TOT_IAS_MGG_SIN, Len.Int.WLQU_TOT_IAS_MGG_SIN, Len.Fract.WLQU_TOT_IAS_MGG_SIN);
    }

    public byte[] getWlquTotIasMggSinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IAS_MGG_SIN, Pos.S089_TOT_IAS_MGG_SIN);
        return buffer;
    }

    public void initWlquTotIasMggSinSpaces() {
        fill(Pos.S089_TOT_IAS_MGG_SIN, Len.S089_TOT_IAS_MGG_SIN, Types.SPACE_CHAR);
    }

    public void setWlquTotIasMggSinNull(String wlquTotIasMggSinNull) {
        writeString(Pos.S089_TOT_IAS_MGG_SIN_NULL, wlquTotIasMggSinNull, Len.WLQU_TOT_IAS_MGG_SIN_NULL);
    }

    /**Original name: WLQU-TOT-IAS-MGG-SIN-NULL<br>*/
    public String getWlquTotIasMggSinNull() {
        return readString(Pos.S089_TOT_IAS_MGG_SIN_NULL, Len.WLQU_TOT_IAS_MGG_SIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IAS_MGG_SIN = 1;
        public static final int S089_TOT_IAS_MGG_SIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IAS_MGG_SIN = 8;
        public static final int WLQU_TOT_IAS_MGG_SIN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IAS_MGG_SIN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IAS_MGG_SIN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
