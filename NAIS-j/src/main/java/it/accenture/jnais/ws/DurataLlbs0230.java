package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DURATA<br>
 * Variable: DURATA from program LLBS0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DurataLlbs0230 {

    //==== PROPERTIES ====
    //Original name: DUR-AAAA
    private String aaaa = DefaultValues.stringVal(Len.AAAA);
    //Original name: DUR-MM
    private short mm = DefaultValues.SHORT_VAL;

    //==== METHODS ====
    public void setAaaa(short aaaa) {
        this.aaaa = NumericDisplay.asString(aaaa, Len.AAAA);
    }

    public short getAaaa() {
        return NumericDisplay.asShort(this.aaaa);
    }

    public String getAaaaFormatted() {
        return this.aaaa;
    }

    public void setMm(short mm) {
        this.mm = mm;
    }

    public short getMm() {
        return this.mm;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA = 4;
        public static final int MM = 2;
        public static final int GG = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
