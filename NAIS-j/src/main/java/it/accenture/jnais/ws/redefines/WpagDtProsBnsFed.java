package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-DT-PROS-BNS-FED<br>
 * Variable: WPAG-DT-PROS-BNS-FED from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagDtProsBnsFed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagDtProsBnsFed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_DT_PROS_BNS_FED;
    }

    public void setWpagDtProsBnsFed(int wpagDtProsBnsFed) {
        writeIntAsPacked(Pos.WPAG_DT_PROS_BNS_FED, wpagDtProsBnsFed, Len.Int.WPAG_DT_PROS_BNS_FED);
    }

    public void setWpagDtProsBnsFedFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_DT_PROS_BNS_FED, Pos.WPAG_DT_PROS_BNS_FED);
    }

    /**Original name: WPAG-DT-PROS-BNS-FED<br>*/
    public int getWpagDtProsBnsFed() {
        return readPackedAsInt(Pos.WPAG_DT_PROS_BNS_FED, Len.Int.WPAG_DT_PROS_BNS_FED);
    }

    public byte[] getWpagDtProsBnsFedAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_DT_PROS_BNS_FED, Pos.WPAG_DT_PROS_BNS_FED);
        return buffer;
    }

    public void initWpagDtProsBnsFedSpaces() {
        fill(Pos.WPAG_DT_PROS_BNS_FED, Len.WPAG_DT_PROS_BNS_FED, Types.SPACE_CHAR);
    }

    public void setWpagDtProsBnsFedNull(String wpagDtProsBnsFedNull) {
        writeString(Pos.WPAG_DT_PROS_BNS_FED_NULL, wpagDtProsBnsFedNull, Len.WPAG_DT_PROS_BNS_FED_NULL);
    }

    /**Original name: WPAG-DT-PROS-BNS-FED-NULL<br>*/
    public String getWpagDtProsBnsFedNull() {
        return readString(Pos.WPAG_DT_PROS_BNS_FED_NULL, Len.WPAG_DT_PROS_BNS_FED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_DT_PROS_BNS_FED = 1;
        public static final int WPAG_DT_PROS_BNS_FED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_DT_PROS_BNS_FED = 5;
        public static final int WPAG_DT_PROS_BNS_FED_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_DT_PROS_BNS_FED = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
