package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-IMP-REC-RIT-ACC<br>
 * Variable: ADE-IMP-REC-RIT-ACC from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeImpRecRitAcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeImpRecRitAcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_IMP_REC_RIT_ACC;
    }

    public void setAdeImpRecRitAcc(AfDecimal adeImpRecRitAcc) {
        writeDecimalAsPacked(Pos.ADE_IMP_REC_RIT_ACC, adeImpRecRitAcc.copy());
    }

    public void setAdeImpRecRitAccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_IMP_REC_RIT_ACC, Pos.ADE_IMP_REC_RIT_ACC);
    }

    /**Original name: ADE-IMP-REC-RIT-ACC<br>*/
    public AfDecimal getAdeImpRecRitAcc() {
        return readPackedAsDecimal(Pos.ADE_IMP_REC_RIT_ACC, Len.Int.ADE_IMP_REC_RIT_ACC, Len.Fract.ADE_IMP_REC_RIT_ACC);
    }

    public byte[] getAdeImpRecRitAccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_IMP_REC_RIT_ACC, Pos.ADE_IMP_REC_RIT_ACC);
        return buffer;
    }

    public void setAdeImpRecRitAccNull(String adeImpRecRitAccNull) {
        writeString(Pos.ADE_IMP_REC_RIT_ACC_NULL, adeImpRecRitAccNull, Len.ADE_IMP_REC_RIT_ACC_NULL);
    }

    /**Original name: ADE-IMP-REC-RIT-ACC-NULL<br>*/
    public String getAdeImpRecRitAccNull() {
        return readString(Pos.ADE_IMP_REC_RIT_ACC_NULL, Len.ADE_IMP_REC_RIT_ACC_NULL);
    }

    public String getAdeImpRecRitAccNullFormatted() {
        return Functions.padBlanks(getAdeImpRecRitAccNull(), Len.ADE_IMP_REC_RIT_ACC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_IMP_REC_RIT_ACC = 1;
        public static final int ADE_IMP_REC_RIT_ACC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_IMP_REC_RIT_ACC = 8;
        public static final int ADE_IMP_REC_RIT_ACC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_IMP_REC_RIT_ACC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_IMP_REC_RIT_ACC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
