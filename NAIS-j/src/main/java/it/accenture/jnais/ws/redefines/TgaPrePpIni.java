package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-PP-INI<br>
 * Variable: TGA-PRE-PP-INI from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPrePpIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPrePpIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_PP_INI;
    }

    public void setTgaPrePpIni(AfDecimal tgaPrePpIni) {
        writeDecimalAsPacked(Pos.TGA_PRE_PP_INI, tgaPrePpIni.copy());
    }

    public void setTgaPrePpIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_PP_INI, Pos.TGA_PRE_PP_INI);
    }

    /**Original name: TGA-PRE-PP-INI<br>*/
    public AfDecimal getTgaPrePpIni() {
        return readPackedAsDecimal(Pos.TGA_PRE_PP_INI, Len.Int.TGA_PRE_PP_INI, Len.Fract.TGA_PRE_PP_INI);
    }

    public byte[] getTgaPrePpIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_PP_INI, Pos.TGA_PRE_PP_INI);
        return buffer;
    }

    public void setTgaPrePpIniNull(String tgaPrePpIniNull) {
        writeString(Pos.TGA_PRE_PP_INI_NULL, tgaPrePpIniNull, Len.TGA_PRE_PP_INI_NULL);
    }

    /**Original name: TGA-PRE-PP-INI-NULL<br>*/
    public String getTgaPrePpIniNull() {
        return readString(Pos.TGA_PRE_PP_INI_NULL, Len.TGA_PRE_PP_INI_NULL);
    }

    public String getTgaPrePpIniNullFormatted() {
        return Functions.padBlanks(getTgaPrePpIniNull(), Len.TGA_PRE_PP_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_PP_INI = 1;
        public static final int TGA_PRE_PP_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_PP_INI = 8;
        public static final int TGA_PRE_PP_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_PP_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_PP_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
