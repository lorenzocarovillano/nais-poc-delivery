package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-TAX-SEP<br>
 * Variable: WBEL-TAX-SEP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelTaxSep extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelTaxSep() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_TAX_SEP;
    }

    public void setWbelTaxSep(AfDecimal wbelTaxSep) {
        writeDecimalAsPacked(Pos.WBEL_TAX_SEP, wbelTaxSep.copy());
    }

    public void setWbelTaxSepFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_TAX_SEP, Pos.WBEL_TAX_SEP);
    }

    /**Original name: WBEL-TAX-SEP<br>*/
    public AfDecimal getWbelTaxSep() {
        return readPackedAsDecimal(Pos.WBEL_TAX_SEP, Len.Int.WBEL_TAX_SEP, Len.Fract.WBEL_TAX_SEP);
    }

    public byte[] getWbelTaxSepAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_TAX_SEP, Pos.WBEL_TAX_SEP);
        return buffer;
    }

    public void initWbelTaxSepSpaces() {
        fill(Pos.WBEL_TAX_SEP, Len.WBEL_TAX_SEP, Types.SPACE_CHAR);
    }

    public void setWbelTaxSepNull(String wbelTaxSepNull) {
        writeString(Pos.WBEL_TAX_SEP_NULL, wbelTaxSepNull, Len.WBEL_TAX_SEP_NULL);
    }

    /**Original name: WBEL-TAX-SEP-NULL<br>*/
    public String getWbelTaxSepNull() {
        return readString(Pos.WBEL_TAX_SEP_NULL, Len.WBEL_TAX_SEP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_TAX_SEP = 1;
        public static final int WBEL_TAX_SEP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_TAX_SEP = 8;
        public static final int WBEL_TAX_SEP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_TAX_SEP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_TAX_SEP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
