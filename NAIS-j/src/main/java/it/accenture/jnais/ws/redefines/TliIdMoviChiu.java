package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-ID-MOVI-CHIU<br>
 * Variable: TLI-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_ID_MOVI_CHIU;
    }

    public void setTliIdMoviChiu(int tliIdMoviChiu) {
        writeIntAsPacked(Pos.TLI_ID_MOVI_CHIU, tliIdMoviChiu, Len.Int.TLI_ID_MOVI_CHIU);
    }

    public void setTliIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_ID_MOVI_CHIU, Pos.TLI_ID_MOVI_CHIU);
    }

    /**Original name: TLI-ID-MOVI-CHIU<br>*/
    public int getTliIdMoviChiu() {
        return readPackedAsInt(Pos.TLI_ID_MOVI_CHIU, Len.Int.TLI_ID_MOVI_CHIU);
    }

    public byte[] getTliIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_ID_MOVI_CHIU, Pos.TLI_ID_MOVI_CHIU);
        return buffer;
    }

    public void setTliIdMoviChiuNull(String tliIdMoviChiuNull) {
        writeString(Pos.TLI_ID_MOVI_CHIU_NULL, tliIdMoviChiuNull, Len.TLI_ID_MOVI_CHIU_NULL);
    }

    /**Original name: TLI-ID-MOVI-CHIU-NULL<br>*/
    public String getTliIdMoviChiuNull() {
        return readString(Pos.TLI_ID_MOVI_CHIU_NULL, Len.TLI_ID_MOVI_CHIU_NULL);
    }

    public String getTliIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getTliIdMoviChiuNull(), Len.TLI_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_ID_MOVI_CHIU = 1;
        public static final int TLI_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_ID_MOVI_CHIU = 5;
        public static final int TLI_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
