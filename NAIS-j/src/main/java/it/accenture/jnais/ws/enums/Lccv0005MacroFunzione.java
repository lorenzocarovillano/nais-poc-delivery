package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCV0005-MACRO-FUNZIONE<br>
 * Variable: LCCV0005-MACRO-FUNZIONE from copybook LCCV0005<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccv0005MacroFunzione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.MACRO_FUNZIONE);
    public static final String VENDITA = "VE";
    public static final String VARIAZIONE = "VG";
    public static final String STORNI = "ST";
    public static final String OPER_AUTOM = "OA";
    public static final String LEGGE_BILANCIO = "LB";
    public static final String CONTABILITA = "CT";
    public static final String OPZIONI = "OP";
    public static final String PRESTITI = "PR";
    public static final String RID_RIATT = "RR";
    public static final String COASSICURAZIONE = "CS";
    public static final String PAGAMENTI = "PA";
    public static final String PREVENTIVAZIONE = "PV";
    public static final String ANNULLI = "AN";
    public static final String RENDITE = "RE";

    //==== METHODS ====
    public void setMacroFunzione(String macroFunzione) {
        this.value = Functions.subString(macroFunzione, Len.MACRO_FUNZIONE);
    }

    public String getMacroFunzione() {
        return this.value;
    }

    public boolean isVendita() {
        return value.equals(VENDITA);
    }

    public void setLccv0005OperAutom() {
        value = OPER_AUTOM;
    }

    public boolean isPreventivazione() {
        return value.equals(PREVENTIVAZIONE);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MACRO_FUNZIONE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
