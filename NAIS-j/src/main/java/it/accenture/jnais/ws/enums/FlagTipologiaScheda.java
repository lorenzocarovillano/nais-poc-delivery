package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-TIPOLOGIA-SCHEDA<br>
 * Variable: FLAG-TIPOLOGIA-SCHEDA from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTipologiaScheda {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_TIPOLOGIA_SCHEDA);
    public static final String STANDARD = "ST";
    public static final String OPZIONI = "OP";

    //==== METHODS ====
    public void setFlagTipologiaScheda(String flagTipologiaScheda) {
        this.value = Functions.subString(flagTipologiaScheda, Len.FLAG_TIPOLOGIA_SCHEDA);
    }

    public String getFlagTipologiaScheda() {
        return this.value;
    }

    public String getFlagTipologiaSchedaFormatted() {
        return Functions.padBlanks(getFlagTipologiaScheda(), Len.FLAG_TIPOLOGIA_SCHEDA);
    }

    public void setStandard() {
        value = STANDARD;
    }

    public boolean isOpzioni() {
        return value.equals(OPZIONI);
    }

    public void setOpzioni() {
        value = OPZIONI;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_TIPOLOGIA_SCHEDA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
