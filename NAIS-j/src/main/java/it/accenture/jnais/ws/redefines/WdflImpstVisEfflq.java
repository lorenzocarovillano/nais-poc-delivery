package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-VIS-EFFLQ<br>
 * Variable: WDFL-IMPST-VIS-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstVisEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstVisEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_VIS_EFFLQ;
    }

    public void setWdflImpstVisEfflq(AfDecimal wdflImpstVisEfflq) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_VIS_EFFLQ, wdflImpstVisEfflq.copy());
    }

    public void setWdflImpstVisEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_VIS_EFFLQ, Pos.WDFL_IMPST_VIS_EFFLQ);
    }

    /**Original name: WDFL-IMPST-VIS-EFFLQ<br>*/
    public AfDecimal getWdflImpstVisEfflq() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_VIS_EFFLQ, Len.Int.WDFL_IMPST_VIS_EFFLQ, Len.Fract.WDFL_IMPST_VIS_EFFLQ);
    }

    public byte[] getWdflImpstVisEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_VIS_EFFLQ, Pos.WDFL_IMPST_VIS_EFFLQ);
        return buffer;
    }

    public void setWdflImpstVisEfflqNull(String wdflImpstVisEfflqNull) {
        writeString(Pos.WDFL_IMPST_VIS_EFFLQ_NULL, wdflImpstVisEfflqNull, Len.WDFL_IMPST_VIS_EFFLQ_NULL);
    }

    /**Original name: WDFL-IMPST-VIS-EFFLQ-NULL<br>*/
    public String getWdflImpstVisEfflqNull() {
        return readString(Pos.WDFL_IMPST_VIS_EFFLQ_NULL, Len.WDFL_IMPST_VIS_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS_EFFLQ = 1;
        public static final int WDFL_IMPST_VIS_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS_EFFLQ = 8;
        public static final int WDFL_IMPST_VIS_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
