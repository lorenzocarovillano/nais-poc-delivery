package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-ID-3O-ASSTO<br>
 * Variable: GRZ-ID-3O-ASSTO from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzId3oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzId3oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_ID3O_ASSTO;
    }

    public void setGrzId3oAssto(int grzId3oAssto) {
        writeIntAsPacked(Pos.GRZ_ID3O_ASSTO, grzId3oAssto, Len.Int.GRZ_ID3O_ASSTO);
    }

    public void setGrzId3oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_ID3O_ASSTO, Pos.GRZ_ID3O_ASSTO);
    }

    /**Original name: GRZ-ID-3O-ASSTO<br>*/
    public int getGrzId3oAssto() {
        return readPackedAsInt(Pos.GRZ_ID3O_ASSTO, Len.Int.GRZ_ID3O_ASSTO);
    }

    public byte[] getGrzId3oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_ID3O_ASSTO, Pos.GRZ_ID3O_ASSTO);
        return buffer;
    }

    public void setGrzId3oAsstoNull(String grzId3oAsstoNull) {
        writeString(Pos.GRZ_ID3O_ASSTO_NULL, grzId3oAsstoNull, Len.GRZ_ID3O_ASSTO_NULL);
    }

    /**Original name: GRZ-ID-3O-ASSTO-NULL<br>*/
    public String getGrzId3oAsstoNull() {
        return readString(Pos.GRZ_ID3O_ASSTO_NULL, Len.GRZ_ID3O_ASSTO_NULL);
    }

    public String getGrzId3oAsstoNullFormatted() {
        return Functions.padBlanks(getGrzId3oAsstoNull(), Len.GRZ_ID3O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_ID3O_ASSTO = 1;
        public static final int GRZ_ID3O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_ID3O_ASSTO = 5;
        public static final int GRZ_ID3O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_ID3O_ASSTO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
