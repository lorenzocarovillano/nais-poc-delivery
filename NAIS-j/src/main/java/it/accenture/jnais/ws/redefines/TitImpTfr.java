package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-IMP-TFR<br>
 * Variable: TIT-IMP-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_IMP_TFR;
    }

    public void setTitImpTfr(AfDecimal titImpTfr) {
        writeDecimalAsPacked(Pos.TIT_IMP_TFR, titImpTfr.copy());
    }

    public void setTitImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_IMP_TFR, Pos.TIT_IMP_TFR);
    }

    /**Original name: TIT-IMP-TFR<br>*/
    public AfDecimal getTitImpTfr() {
        return readPackedAsDecimal(Pos.TIT_IMP_TFR, Len.Int.TIT_IMP_TFR, Len.Fract.TIT_IMP_TFR);
    }

    public byte[] getTitImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_IMP_TFR, Pos.TIT_IMP_TFR);
        return buffer;
    }

    public void setTitImpTfrNull(String titImpTfrNull) {
        writeString(Pos.TIT_IMP_TFR_NULL, titImpTfrNull, Len.TIT_IMP_TFR_NULL);
    }

    /**Original name: TIT-IMP-TFR-NULL<br>*/
    public String getTitImpTfrNull() {
        return readString(Pos.TIT_IMP_TFR_NULL, Len.TIT_IMP_TFR_NULL);
    }

    public String getTitImpTfrNullFormatted() {
        return Functions.padBlanks(getTitImpTfrNull(), Len.TIT_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_IMP_TFR = 1;
        public static final int TIT_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_IMP_TFR = 8;
        public static final int TIT_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
