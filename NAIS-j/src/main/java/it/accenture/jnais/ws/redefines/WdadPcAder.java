package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDAD-PC-ADER<br>
 * Variable: WDAD-PC-ADER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadPcAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadPcAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_PC_ADER;
    }

    public void setWdadPcAder(AfDecimal wdadPcAder) {
        writeDecimalAsPacked(Pos.WDAD_PC_ADER, wdadPcAder.copy());
    }

    public void setWdadPcAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_PC_ADER, Pos.WDAD_PC_ADER);
    }

    /**Original name: WDAD-PC-ADER<br>*/
    public AfDecimal getWdadPcAder() {
        return readPackedAsDecimal(Pos.WDAD_PC_ADER, Len.Int.WDAD_PC_ADER, Len.Fract.WDAD_PC_ADER);
    }

    public byte[] getWdadPcAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_PC_ADER, Pos.WDAD_PC_ADER);
        return buffer;
    }

    public void setWdadPcAderNull(String wdadPcAderNull) {
        writeString(Pos.WDAD_PC_ADER_NULL, wdadPcAderNull, Len.WDAD_PC_ADER_NULL);
    }

    /**Original name: WDAD-PC-ADER-NULL<br>*/
    public String getWdadPcAderNull() {
        return readString(Pos.WDAD_PC_ADER_NULL, Len.WDAD_PC_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_PC_ADER = 1;
        public static final int WDAD_PC_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_PC_ADER = 4;
        public static final int WDAD_PC_ADER_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDAD_PC_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_PC_ADER = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
