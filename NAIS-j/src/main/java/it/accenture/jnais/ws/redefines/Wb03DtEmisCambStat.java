package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-EMIS-CAMB-STAT<br>
 * Variable: WB03-DT-EMIS-CAMB-STAT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtEmisCambStat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtEmisCambStat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_EMIS_CAMB_STAT;
    }

    public void setWb03DtEmisCambStatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_EMIS_CAMB_STAT, Pos.WB03_DT_EMIS_CAMB_STAT);
    }

    /**Original name: WB03-DT-EMIS-CAMB-STAT<br>*/
    public int getWb03DtEmisCambStat() {
        return readPackedAsInt(Pos.WB03_DT_EMIS_CAMB_STAT, Len.Int.WB03_DT_EMIS_CAMB_STAT);
    }

    public byte[] getWb03DtEmisCambStatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_EMIS_CAMB_STAT, Pos.WB03_DT_EMIS_CAMB_STAT);
        return buffer;
    }

    public void setWb03DtEmisCambStatNull(String wb03DtEmisCambStatNull) {
        writeString(Pos.WB03_DT_EMIS_CAMB_STAT_NULL, wb03DtEmisCambStatNull, Len.WB03_DT_EMIS_CAMB_STAT_NULL);
    }

    /**Original name: WB03-DT-EMIS-CAMB-STAT-NULL<br>*/
    public String getWb03DtEmisCambStatNull() {
        return readString(Pos.WB03_DT_EMIS_CAMB_STAT_NULL, Len.WB03_DT_EMIS_CAMB_STAT_NULL);
    }

    public String getWb03DtEmisCambStatNullFormatted() {
        return Functions.padBlanks(getWb03DtEmisCambStatNull(), Len.WB03_DT_EMIS_CAMB_STAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_EMIS_CAMB_STAT = 1;
        public static final int WB03_DT_EMIS_CAMB_STAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_EMIS_CAMB_STAT = 5;
        public static final int WB03_DT_EMIS_CAMB_STAT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_EMIS_CAMB_STAT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
