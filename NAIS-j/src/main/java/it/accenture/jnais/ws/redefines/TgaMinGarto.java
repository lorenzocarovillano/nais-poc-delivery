package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-MIN-GARTO<br>
 * Variable: TGA-MIN-GARTO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaMinGarto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaMinGarto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_MIN_GARTO;
    }

    public void setTgaMinGarto(AfDecimal tgaMinGarto) {
        writeDecimalAsPacked(Pos.TGA_MIN_GARTO, tgaMinGarto.copy());
    }

    public void setTgaMinGartoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_MIN_GARTO, Pos.TGA_MIN_GARTO);
    }

    /**Original name: TGA-MIN-GARTO<br>*/
    public AfDecimal getTgaMinGarto() {
        return readPackedAsDecimal(Pos.TGA_MIN_GARTO, Len.Int.TGA_MIN_GARTO, Len.Fract.TGA_MIN_GARTO);
    }

    public byte[] getTgaMinGartoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_MIN_GARTO, Pos.TGA_MIN_GARTO);
        return buffer;
    }

    public void setTgaMinGartoNull(String tgaMinGartoNull) {
        writeString(Pos.TGA_MIN_GARTO_NULL, tgaMinGartoNull, Len.TGA_MIN_GARTO_NULL);
    }

    /**Original name: TGA-MIN-GARTO-NULL<br>*/
    public String getTgaMinGartoNull() {
        return readString(Pos.TGA_MIN_GARTO_NULL, Len.TGA_MIN_GARTO_NULL);
    }

    public String getTgaMinGartoNullFormatted() {
        return Functions.padBlanks(getTgaMinGartoNull(), Len.TGA_MIN_GARTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_MIN_GARTO = 1;
        public static final int TGA_MIN_GARTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_MIN_GARTO = 8;
        public static final int TGA_MIN_GARTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_MIN_GARTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_MIN_GARTO = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
