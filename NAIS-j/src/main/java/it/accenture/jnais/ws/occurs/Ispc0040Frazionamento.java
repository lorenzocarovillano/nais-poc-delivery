package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0040-FRAZIONAMENTO<br>
 * Variables: ISPC0040-FRAZIONAMENTO from copybook ISPC0040<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0040Frazionamento {

    //==== PROPERTIES ====
    //Original name: ISPC0040-COD-FRAZ
    private String codFraz = DefaultValues.stringVal(Len.COD_FRAZ);
    //Original name: ISPC0040-DESC-FRAZ
    private String descFraz = DefaultValues.stringVal(Len.DESC_FRAZ);

    //==== METHODS ====
    public void setFrazionamentoBytes(byte[] buffer, int offset) {
        int position = offset;
        codFraz = MarshalByte.readString(buffer, position, Len.COD_FRAZ);
        position += Len.COD_FRAZ;
        descFraz = MarshalByte.readString(buffer, position, Len.DESC_FRAZ);
    }

    public byte[] getFrazionamentoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codFraz, Len.COD_FRAZ);
        position += Len.COD_FRAZ;
        MarshalByte.writeString(buffer, position, descFraz, Len.DESC_FRAZ);
        return buffer;
    }

    public void initFrazionamentoSpaces() {
        codFraz = "";
        descFraz = "";
    }

    public void setCodFraz(String codFraz) {
        this.codFraz = Functions.subString(codFraz, Len.COD_FRAZ);
    }

    public String getCodFraz() {
        return this.codFraz;
    }

    public void setDescFraz(String descFraz) {
        this.descFraz = Functions.subString(descFraz, Len.DESC_FRAZ);
    }

    public String getDescFraz() {
        return this.descFraz;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_FRAZ = 2;
        public static final int DESC_FRAZ = 30;
        public static final int FRAZIONAMENTO = COD_FRAZ + DESC_FRAZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
