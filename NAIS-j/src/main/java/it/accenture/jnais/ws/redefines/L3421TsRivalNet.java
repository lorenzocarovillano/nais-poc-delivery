package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-TS-RIVAL-NET<br>
 * Variable: L3421-TS-RIVAL-NET from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421TsRivalNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421TsRivalNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_TS_RIVAL_NET;
    }

    public void setL3421TsRivalNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_TS_RIVAL_NET, Pos.L3421_TS_RIVAL_NET);
    }

    /**Original name: L3421-TS-RIVAL-NET<br>*/
    public AfDecimal getL3421TsRivalNet() {
        return readPackedAsDecimal(Pos.L3421_TS_RIVAL_NET, Len.Int.L3421_TS_RIVAL_NET, Len.Fract.L3421_TS_RIVAL_NET);
    }

    public byte[] getL3421TsRivalNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_TS_RIVAL_NET, Pos.L3421_TS_RIVAL_NET);
        return buffer;
    }

    /**Original name: L3421-TS-RIVAL-NET-NULL<br>*/
    public String getL3421TsRivalNetNull() {
        return readString(Pos.L3421_TS_RIVAL_NET_NULL, Len.L3421_TS_RIVAL_NET_NULL);
    }

    public String getL3421TsRivalNetNullFormatted() {
        return Functions.padBlanks(getL3421TsRivalNetNull(), Len.L3421_TS_RIVAL_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_TS_RIVAL_NET = 1;
        public static final int L3421_TS_RIVAL_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_TS_RIVAL_NET = 8;
        public static final int L3421_TS_RIVAL_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_TS_RIVAL_NET = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_TS_RIVAL_NET = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
