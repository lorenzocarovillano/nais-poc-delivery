package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRAN-PC-NEL-RAPP<br>
 * Variable: WRAN-PC-NEL-RAPP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WranPcNelRapp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WranPcNelRapp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRAN_PC_NEL_RAPP;
    }

    public void setWranPcNelRapp(AfDecimal wranPcNelRapp) {
        writeDecimalAsPacked(Pos.WRAN_PC_NEL_RAPP, wranPcNelRapp.copy());
    }

    public void setWranPcNelRappFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRAN_PC_NEL_RAPP, Pos.WRAN_PC_NEL_RAPP);
    }

    /**Original name: WRAN-PC-NEL-RAPP<br>*/
    public AfDecimal getWranPcNelRapp() {
        return readPackedAsDecimal(Pos.WRAN_PC_NEL_RAPP, Len.Int.WRAN_PC_NEL_RAPP, Len.Fract.WRAN_PC_NEL_RAPP);
    }

    public byte[] getWranPcNelRappAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRAN_PC_NEL_RAPP, Pos.WRAN_PC_NEL_RAPP);
        return buffer;
    }

    public void initWranPcNelRappSpaces() {
        fill(Pos.WRAN_PC_NEL_RAPP, Len.WRAN_PC_NEL_RAPP, Types.SPACE_CHAR);
    }

    public void setWranPcNelRappNull(String wranPcNelRappNull) {
        writeString(Pos.WRAN_PC_NEL_RAPP_NULL, wranPcNelRappNull, Len.WRAN_PC_NEL_RAPP_NULL);
    }

    /**Original name: WRAN-PC-NEL-RAPP-NULL<br>*/
    public String getWranPcNelRappNull() {
        return readString(Pos.WRAN_PC_NEL_RAPP_NULL, Len.WRAN_PC_NEL_RAPP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRAN_PC_NEL_RAPP = 1;
        public static final int WRAN_PC_NEL_RAPP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRAN_PC_NEL_RAPP = 4;
        public static final int WRAN_PC_NEL_RAPP_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRAN_PC_NEL_RAPP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRAN_PC_NEL_RAPP = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
