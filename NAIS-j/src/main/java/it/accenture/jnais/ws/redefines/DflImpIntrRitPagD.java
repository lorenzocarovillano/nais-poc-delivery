package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMP-INTR-RIT-PAG-D<br>
 * Variable: DFL-IMP-INTR-RIT-PAG-D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpIntrRitPagD extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpIntrRitPagD() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMP_INTR_RIT_PAG_D;
    }

    public void setDflImpIntrRitPagD(AfDecimal dflImpIntrRitPagD) {
        writeDecimalAsPacked(Pos.DFL_IMP_INTR_RIT_PAG_D, dflImpIntrRitPagD.copy());
    }

    public void setDflImpIntrRitPagDFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMP_INTR_RIT_PAG_D, Pos.DFL_IMP_INTR_RIT_PAG_D);
    }

    /**Original name: DFL-IMP-INTR-RIT-PAG-D<br>*/
    public AfDecimal getDflImpIntrRitPagD() {
        return readPackedAsDecimal(Pos.DFL_IMP_INTR_RIT_PAG_D, Len.Int.DFL_IMP_INTR_RIT_PAG_D, Len.Fract.DFL_IMP_INTR_RIT_PAG_D);
    }

    public byte[] getDflImpIntrRitPagDAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMP_INTR_RIT_PAG_D, Pos.DFL_IMP_INTR_RIT_PAG_D);
        return buffer;
    }

    public void setDflImpIntrRitPagDNull(String dflImpIntrRitPagDNull) {
        writeString(Pos.DFL_IMP_INTR_RIT_PAG_D_NULL, dflImpIntrRitPagDNull, Len.DFL_IMP_INTR_RIT_PAG_D_NULL);
    }

    /**Original name: DFL-IMP-INTR-RIT-PAG-D-NULL<br>*/
    public String getDflImpIntrRitPagDNull() {
        return readString(Pos.DFL_IMP_INTR_RIT_PAG_D_NULL, Len.DFL_IMP_INTR_RIT_PAG_D_NULL);
    }

    public String getDflImpIntrRitPagDNullFormatted() {
        return Functions.padBlanks(getDflImpIntrRitPagDNull(), Len.DFL_IMP_INTR_RIT_PAG_D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMP_INTR_RIT_PAG_D = 1;
        public static final int DFL_IMP_INTR_RIT_PAG_D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMP_INTR_RIT_PAG_D = 8;
        public static final int DFL_IMP_INTR_RIT_PAG_D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMP_INTR_RIT_PAG_D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMP_INTR_RIT_PAG_D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
