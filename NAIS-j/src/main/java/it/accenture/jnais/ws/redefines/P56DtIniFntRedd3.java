package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-DT-INI-FNT-REDD-3<br>
 * Variable: P56-DT-INI-FNT-REDD-3 from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56DtIniFntRedd3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56DtIniFntRedd3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_DT_INI_FNT_REDD3;
    }

    public void setP56DtIniFntRedd3(int p56DtIniFntRedd3) {
        writeIntAsPacked(Pos.P56_DT_INI_FNT_REDD3, p56DtIniFntRedd3, Len.Int.P56_DT_INI_FNT_REDD3);
    }

    public void setP56DtIniFntRedd3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_DT_INI_FNT_REDD3, Pos.P56_DT_INI_FNT_REDD3);
    }

    /**Original name: P56-DT-INI-FNT-REDD-3<br>*/
    public int getP56DtIniFntRedd3() {
        return readPackedAsInt(Pos.P56_DT_INI_FNT_REDD3, Len.Int.P56_DT_INI_FNT_REDD3);
    }

    public byte[] getP56DtIniFntRedd3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_DT_INI_FNT_REDD3, Pos.P56_DT_INI_FNT_REDD3);
        return buffer;
    }

    public void setP56DtIniFntRedd3Null(String p56DtIniFntRedd3Null) {
        writeString(Pos.P56_DT_INI_FNT_REDD3_NULL, p56DtIniFntRedd3Null, Len.P56_DT_INI_FNT_REDD3_NULL);
    }

    /**Original name: P56-DT-INI-FNT-REDD-3-NULL<br>*/
    public String getP56DtIniFntRedd3Null() {
        return readString(Pos.P56_DT_INI_FNT_REDD3_NULL, Len.P56_DT_INI_FNT_REDD3_NULL);
    }

    public String getP56DtIniFntRedd3NullFormatted() {
        return Functions.padBlanks(getP56DtIniFntRedd3Null(), Len.P56_DT_INI_FNT_REDD3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_DT_INI_FNT_REDD3 = 1;
        public static final int P56_DT_INI_FNT_REDD3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_DT_INI_FNT_REDD3 = 5;
        public static final int P56_DT_INI_FNT_REDD3_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_DT_INI_FNT_REDD3 = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
