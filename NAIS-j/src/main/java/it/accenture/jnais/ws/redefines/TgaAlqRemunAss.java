package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ALQ-REMUN-ASS<br>
 * Variable: TGA-ALQ-REMUN-ASS from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaAlqRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaAlqRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ALQ_REMUN_ASS;
    }

    public void setTgaAlqRemunAss(AfDecimal tgaAlqRemunAss) {
        writeDecimalAsPacked(Pos.TGA_ALQ_REMUN_ASS, tgaAlqRemunAss.copy());
    }

    public void setTgaAlqRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ALQ_REMUN_ASS, Pos.TGA_ALQ_REMUN_ASS);
    }

    /**Original name: TGA-ALQ-REMUN-ASS<br>*/
    public AfDecimal getTgaAlqRemunAss() {
        return readPackedAsDecimal(Pos.TGA_ALQ_REMUN_ASS, Len.Int.TGA_ALQ_REMUN_ASS, Len.Fract.TGA_ALQ_REMUN_ASS);
    }

    public byte[] getTgaAlqRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ALQ_REMUN_ASS, Pos.TGA_ALQ_REMUN_ASS);
        return buffer;
    }

    public void setTgaAlqRemunAssNull(String tgaAlqRemunAssNull) {
        writeString(Pos.TGA_ALQ_REMUN_ASS_NULL, tgaAlqRemunAssNull, Len.TGA_ALQ_REMUN_ASS_NULL);
    }

    /**Original name: TGA-ALQ-REMUN-ASS-NULL<br>*/
    public String getTgaAlqRemunAssNull() {
        return readString(Pos.TGA_ALQ_REMUN_ASS_NULL, Len.TGA_ALQ_REMUN_ASS_NULL);
    }

    public String getTgaAlqRemunAssNullFormatted() {
        return Functions.padBlanks(getTgaAlqRemunAssNull(), Len.TGA_ALQ_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_REMUN_ASS = 1;
        public static final int TGA_ALQ_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_REMUN_ASS = 4;
        public static final int TGA_ALQ_REMUN_ASS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
