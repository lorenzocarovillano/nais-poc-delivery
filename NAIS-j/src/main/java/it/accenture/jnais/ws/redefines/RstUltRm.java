package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-ULT-RM<br>
 * Variable: RST-ULT-RM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstUltRm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstUltRm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_ULT_RM;
    }

    public void setRstUltRm(AfDecimal rstUltRm) {
        writeDecimalAsPacked(Pos.RST_ULT_RM, rstUltRm.copy());
    }

    public void setRstUltRmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_ULT_RM, Pos.RST_ULT_RM);
    }

    /**Original name: RST-ULT-RM<br>*/
    public AfDecimal getRstUltRm() {
        return readPackedAsDecimal(Pos.RST_ULT_RM, Len.Int.RST_ULT_RM, Len.Fract.RST_ULT_RM);
    }

    public byte[] getRstUltRmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_ULT_RM, Pos.RST_ULT_RM);
        return buffer;
    }

    public void setRstUltRmNull(String rstUltRmNull) {
        writeString(Pos.RST_ULT_RM_NULL, rstUltRmNull, Len.RST_ULT_RM_NULL);
    }

    /**Original name: RST-ULT-RM-NULL<br>*/
    public String getRstUltRmNull() {
        return readString(Pos.RST_ULT_RM_NULL, Len.RST_ULT_RM_NULL);
    }

    public String getRstUltRmNullFormatted() {
        return Functions.padBlanks(getRstUltRmNull(), Len.RST_ULT_RM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_ULT_RM = 1;
        public static final int RST_ULT_RM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_ULT_RM = 8;
        public static final int RST_ULT_RM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_ULT_RM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_ULT_RM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
