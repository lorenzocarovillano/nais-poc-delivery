package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0140-COMP-VAL-GENERICO-P<br>
 * Variable: ISPC0140-COMP-VAL-GENERICO-P from program ISPS0140<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ispc0140CompValGenericoP extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Ispc0140CompValGenericoP() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISPC0140_COMP_VAL_GENERICO_P;
    }

    public void setIspc0140CompValGenericoPFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISPC0140_COMP_VAL_GENERICO_P, Pos.ISPC0140_COMP_VAL_GENERICO_P);
    }

    public byte[] getIspc0140CompValGenericoPAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISPC0140_COMP_VAL_GENERICO_P, Pos.ISPC0140_COMP_VAL_GENERICO_P);
        return buffer;
    }

    public void initIspc0140CompValGenericoPSpaces() {
        fill(Pos.ISPC0140_COMP_VAL_GENERICO_P, Len.ISPC0140_COMP_VAL_GENERICO_P, Types.SPACE_CHAR);
    }

    /**Original name: ISPC0140-COMP-VALORE-IMP-P<br>*/
    public AfDecimal getIspc0140CompValoreImpP() {
        return readDecimal(Pos.COMP_VALORE_IMP_P, Len.Int.ISPC0140_COMP_VALORE_IMP_P, Len.Fract.ISPC0140_COMP_VALORE_IMP_P);
    }

    /**Original name: ISPC0140-COMP-VALORE-STR-P<br>*/
    public String getIspc0140CompValoreStrP() {
        return readString(Pos.COMP_VALORE_STR_P, Len.ISPC0140_COMP_VALORE_STR_P);
    }

    public String getIspc0140CompValoreStrPFormatted() {
        return Functions.padBlanks(getIspc0140CompValoreStrP(), Len.ISPC0140_COMP_VALORE_STR_P);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISPC0140_COMP_VAL_GENERICO_P = 1;
        public static final int ISPC0140_COMP_VAL_IMP_GEN_P = 1;
        public static final int COMP_VALORE_IMP_P = ISPC0140_COMP_VAL_IMP_GEN_P;
        public static final int VAL_IMP_FILLER_P = COMP_VALORE_IMP_P + Len.COMP_VALORE_IMP_P;
        public static final int ISPC0140_COMP_VAL_STR_GEN_P = 1;
        public static final int COMP_VALORE_STR_P = ISPC0140_COMP_VAL_STR_GEN_P;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int COMP_VALORE_IMP_P = 18;
        public static final int ISPC0140_COMP_VAL_GENERICO_P = 60;
        public static final int ISPC0140_COMP_VALORE_STR_P = 60;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISPC0140_COMP_VALORE_IMP_P = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISPC0140_COMP_VALORE_IMP_P = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
