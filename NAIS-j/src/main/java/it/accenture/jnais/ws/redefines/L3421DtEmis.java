package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-DT-EMIS<br>
 * Variable: L3421-DT-EMIS from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421DtEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421DtEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_DT_EMIS;
    }

    public void setL3421DtEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_DT_EMIS, Pos.L3421_DT_EMIS);
    }

    /**Original name: L3421-DT-EMIS<br>*/
    public int getL3421DtEmis() {
        return readPackedAsInt(Pos.L3421_DT_EMIS, Len.Int.L3421_DT_EMIS);
    }

    public byte[] getL3421DtEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_DT_EMIS, Pos.L3421_DT_EMIS);
        return buffer;
    }

    /**Original name: L3421-DT-EMIS-NULL<br>*/
    public String getL3421DtEmisNull() {
        return readString(Pos.L3421_DT_EMIS_NULL, Len.L3421_DT_EMIS_NULL);
    }

    public String getL3421DtEmisNullFormatted() {
        return Functions.padBlanks(getL3421DtEmisNull(), Len.L3421_DT_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_DT_EMIS = 1;
        public static final int L3421_DT_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_DT_EMIS = 5;
        public static final int L3421_DT_EMIS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_DT_EMIS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
