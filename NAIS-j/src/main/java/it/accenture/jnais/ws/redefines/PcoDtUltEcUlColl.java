package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-EC-UL-COLL<br>
 * Variable: PCO-DT-ULT-EC-UL-COLL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEcUlColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEcUlColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_EC_UL_COLL;
    }

    public void setPcoDtUltEcUlColl(int pcoDtUltEcUlColl) {
        writeIntAsPacked(Pos.PCO_DT_ULT_EC_UL_COLL, pcoDtUltEcUlColl, Len.Int.PCO_DT_ULT_EC_UL_COLL);
    }

    public void setPcoDtUltEcUlCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_EC_UL_COLL, Pos.PCO_DT_ULT_EC_UL_COLL);
    }

    /**Original name: PCO-DT-ULT-EC-UL-COLL<br>*/
    public int getPcoDtUltEcUlColl() {
        return readPackedAsInt(Pos.PCO_DT_ULT_EC_UL_COLL, Len.Int.PCO_DT_ULT_EC_UL_COLL);
    }

    public byte[] getPcoDtUltEcUlCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_EC_UL_COLL, Pos.PCO_DT_ULT_EC_UL_COLL);
        return buffer;
    }

    public void initPcoDtUltEcUlCollHighValues() {
        fill(Pos.PCO_DT_ULT_EC_UL_COLL, Len.PCO_DT_ULT_EC_UL_COLL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEcUlCollNull(String pcoDtUltEcUlCollNull) {
        writeString(Pos.PCO_DT_ULT_EC_UL_COLL_NULL, pcoDtUltEcUlCollNull, Len.PCO_DT_ULT_EC_UL_COLL_NULL);
    }

    /**Original name: PCO-DT-ULT-EC-UL-COLL-NULL<br>*/
    public String getPcoDtUltEcUlCollNull() {
        return readString(Pos.PCO_DT_ULT_EC_UL_COLL_NULL, Len.PCO_DT_ULT_EC_UL_COLL_NULL);
    }

    public String getPcoDtUltEcUlCollNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEcUlCollNull(), Len.PCO_DT_ULT_EC_UL_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_UL_COLL = 1;
        public static final int PCO_DT_ULT_EC_UL_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_UL_COLL = 5;
        public static final int PCO_DT_ULT_EC_UL_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_EC_UL_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
