package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-DIR<br>
 * Variable: DTC-DIR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcDir extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcDir() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_DIR;
    }

    public void setDtcDir(AfDecimal dtcDir) {
        writeDecimalAsPacked(Pos.DTC_DIR, dtcDir.copy());
    }

    public void setDtcDirFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_DIR, Pos.DTC_DIR);
    }

    /**Original name: DTC-DIR<br>*/
    public AfDecimal getDtcDir() {
        return readPackedAsDecimal(Pos.DTC_DIR, Len.Int.DTC_DIR, Len.Fract.DTC_DIR);
    }

    public byte[] getDtcDirAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_DIR, Pos.DTC_DIR);
        return buffer;
    }

    public void setDtcDirNull(String dtcDirNull) {
        writeString(Pos.DTC_DIR_NULL, dtcDirNull, Len.DTC_DIR_NULL);
    }

    /**Original name: DTC-DIR-NULL<br>*/
    public String getDtcDirNull() {
        return readString(Pos.DTC_DIR_NULL, Len.DTC_DIR_NULL);
    }

    public String getDtcDirNullFormatted() {
        return Functions.padBlanks(getDtcDirNull(), Len.DTC_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_DIR = 1;
        public static final int DTC_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_DIR = 8;
        public static final int DTC_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
