package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-MM-CNBZ-K2<br>
 * Variable: DFA-MM-CNBZ-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaMmCnbzK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaMmCnbzK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_MM_CNBZ_K2;
    }

    public void setDfaMmCnbzK2(short dfaMmCnbzK2) {
        writeShortAsPacked(Pos.DFA_MM_CNBZ_K2, dfaMmCnbzK2, Len.Int.DFA_MM_CNBZ_K2);
    }

    public void setDfaMmCnbzK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_MM_CNBZ_K2, Pos.DFA_MM_CNBZ_K2);
    }

    /**Original name: DFA-MM-CNBZ-K2<br>*/
    public short getDfaMmCnbzK2() {
        return readPackedAsShort(Pos.DFA_MM_CNBZ_K2, Len.Int.DFA_MM_CNBZ_K2);
    }

    public byte[] getDfaMmCnbzK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_MM_CNBZ_K2, Pos.DFA_MM_CNBZ_K2);
        return buffer;
    }

    public void setDfaMmCnbzK2Null(String dfaMmCnbzK2Null) {
        writeString(Pos.DFA_MM_CNBZ_K2_NULL, dfaMmCnbzK2Null, Len.DFA_MM_CNBZ_K2_NULL);
    }

    /**Original name: DFA-MM-CNBZ-K2-NULL<br>*/
    public String getDfaMmCnbzK2Null() {
        return readString(Pos.DFA_MM_CNBZ_K2_NULL, Len.DFA_MM_CNBZ_K2_NULL);
    }

    public String getDfaMmCnbzK2NullFormatted() {
        return Functions.padBlanks(getDfaMmCnbzK2Null(), Len.DFA_MM_CNBZ_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_MM_CNBZ_K2 = 1;
        public static final int DFA_MM_CNBZ_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_MM_CNBZ_K2 = 3;
        public static final int DFA_MM_CNBZ_K2_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_MM_CNBZ_K2 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
