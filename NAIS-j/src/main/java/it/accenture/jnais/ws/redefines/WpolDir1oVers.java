package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPOL-DIR-1O-VERS<br>
 * Variable: WPOL-DIR-1O-VERS from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDir1oVers extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDir1oVers() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DIR1O_VERS;
    }

    public void setWpolDir1oVers(AfDecimal wpolDir1oVers) {
        writeDecimalAsPacked(Pos.WPOL_DIR1O_VERS, wpolDir1oVers.copy());
    }

    public void setWpolDir1oVersFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DIR1O_VERS, Pos.WPOL_DIR1O_VERS);
    }

    /**Original name: WPOL-DIR-1O-VERS<br>*/
    public AfDecimal getWpolDir1oVers() {
        return readPackedAsDecimal(Pos.WPOL_DIR1O_VERS, Len.Int.WPOL_DIR1O_VERS, Len.Fract.WPOL_DIR1O_VERS);
    }

    public byte[] getWpolDir1oVersAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DIR1O_VERS, Pos.WPOL_DIR1O_VERS);
        return buffer;
    }

    public void setWpolDir1oVersNull(String wpolDir1oVersNull) {
        writeString(Pos.WPOL_DIR1O_VERS_NULL, wpolDir1oVersNull, Len.WPOL_DIR1O_VERS_NULL);
    }

    /**Original name: WPOL-DIR-1O-VERS-NULL<br>*/
    public String getWpolDir1oVersNull() {
        return readString(Pos.WPOL_DIR1O_VERS_NULL, Len.WPOL_DIR1O_VERS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DIR1O_VERS = 1;
        public static final int WPOL_DIR1O_VERS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DIR1O_VERS = 8;
        public static final int WPOL_DIR1O_VERS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPOL_DIR1O_VERS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DIR1O_VERS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
