package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-REDPRO<br>
 * Variable: WPCO-DT-ULT-ELAB-REDPRO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabRedpro extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabRedpro() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_REDPRO;
    }

    public void setWpcoDtUltElabRedpro(int wpcoDtUltElabRedpro) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_REDPRO, wpcoDtUltElabRedpro, Len.Int.WPCO_DT_ULT_ELAB_REDPRO);
    }

    public void setDpcoDtUltElabRedproFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_REDPRO, Pos.WPCO_DT_ULT_ELAB_REDPRO);
    }

    /**Original name: WPCO-DT-ULT-ELAB-REDPRO<br>*/
    public int getWpcoDtUltElabRedpro() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_REDPRO, Len.Int.WPCO_DT_ULT_ELAB_REDPRO);
    }

    public byte[] getWpcoDtUltElabRedproAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_REDPRO, Pos.WPCO_DT_ULT_ELAB_REDPRO);
        return buffer;
    }

    public void setWpcoDtUltElabRedproNull(String wpcoDtUltElabRedproNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_REDPRO_NULL, wpcoDtUltElabRedproNull, Len.WPCO_DT_ULT_ELAB_REDPRO_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-REDPRO-NULL<br>*/
    public String getWpcoDtUltElabRedproNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_REDPRO_NULL, Len.WPCO_DT_ULT_ELAB_REDPRO_NULL);
    }

    public String getWpcoDtUltElabRedproNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabRedproNull(), Len.WPCO_DT_ULT_ELAB_REDPRO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_REDPRO = 1;
        public static final int WPCO_DT_ULT_ELAB_REDPRO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_REDPRO = 5;
        public static final int WPCO_DT_ULT_ELAB_REDPRO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_REDPRO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
