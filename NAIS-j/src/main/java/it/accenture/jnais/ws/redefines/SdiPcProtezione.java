package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: SDI-PC-PROTEZIONE<br>
 * Variable: SDI-PC-PROTEZIONE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SdiPcProtezione extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public SdiPcProtezione() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SDI_PC_PROTEZIONE;
    }

    public void setSdiPcProtezione(AfDecimal sdiPcProtezione) {
        writeDecimalAsPacked(Pos.SDI_PC_PROTEZIONE, sdiPcProtezione.copy());
    }

    public void setSdiPcProtezioneFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SDI_PC_PROTEZIONE, Pos.SDI_PC_PROTEZIONE);
    }

    /**Original name: SDI-PC-PROTEZIONE<br>*/
    public AfDecimal getSdiPcProtezione() {
        return readPackedAsDecimal(Pos.SDI_PC_PROTEZIONE, Len.Int.SDI_PC_PROTEZIONE, Len.Fract.SDI_PC_PROTEZIONE);
    }

    public byte[] getSdiPcProtezioneAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.SDI_PC_PROTEZIONE, Pos.SDI_PC_PROTEZIONE);
        return buffer;
    }

    public void setSdiPcProtezioneNull(String sdiPcProtezioneNull) {
        writeString(Pos.SDI_PC_PROTEZIONE_NULL, sdiPcProtezioneNull, Len.SDI_PC_PROTEZIONE_NULL);
    }

    /**Original name: SDI-PC-PROTEZIONE-NULL<br>*/
    public String getSdiPcProtezioneNull() {
        return readString(Pos.SDI_PC_PROTEZIONE_NULL, Len.SDI_PC_PROTEZIONE_NULL);
    }

    public String getSdiPcProtezioneNullFormatted() {
        return Functions.padBlanks(getSdiPcProtezioneNull(), Len.SDI_PC_PROTEZIONE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SDI_PC_PROTEZIONE = 1;
        public static final int SDI_PC_PROTEZIONE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SDI_PC_PROTEZIONE = 4;
        public static final int SDI_PC_PROTEZIONE_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SDI_PC_PROTEZIONE = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int SDI_PC_PROTEZIONE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
