package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import org.apache.commons.lang3.ArrayUtils;
import static com.bphx.ctu.af.util.StringUtil.*;

/**Original name: IDSV0301-FL-CONTIGUOUS<br>
 * Variable: IDSV0301-FL-CONTIGUOUS from copybook IDSV0301<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0301FlContiguous {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    private static final char[] NO = new char[] {'N', parseHexChar("FF"), parseHexChar("00"), ' '};

    //==== METHODS ====
    public void setFlContiguous(char flContiguous) {
        this.value = flContiguous;
    }

    public char getFlContiguous() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public boolean isNo() {
        return ArrayUtils.contains(NO, value);
    }

    public void setIdsv0301ContiguousNo() {
        value = NO[0];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_CONTIGUOUS = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
