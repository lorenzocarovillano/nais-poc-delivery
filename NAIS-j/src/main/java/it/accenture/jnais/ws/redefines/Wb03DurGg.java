package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-GG<br>
 * Variable: WB03-DUR-GG from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR_GG;
    }

    public void setWb03DurGg(int wb03DurGg) {
        writeIntAsPacked(Pos.WB03_DUR_GG, wb03DurGg, Len.Int.WB03_DUR_GG);
    }

    public void setWb03DurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR_GG, Pos.WB03_DUR_GG);
    }

    /**Original name: WB03-DUR-GG<br>*/
    public int getWb03DurGg() {
        return readPackedAsInt(Pos.WB03_DUR_GG, Len.Int.WB03_DUR_GG);
    }

    public byte[] getWb03DurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR_GG, Pos.WB03_DUR_GG);
        return buffer;
    }

    public void setWb03DurGgNull(String wb03DurGgNull) {
        writeString(Pos.WB03_DUR_GG_NULL, wb03DurGgNull, Len.WB03_DUR_GG_NULL);
    }

    /**Original name: WB03-DUR-GG-NULL<br>*/
    public String getWb03DurGgNull() {
        return readString(Pos.WB03_DUR_GG_NULL, Len.WB03_DUR_GG_NULL);
    }

    public String getWb03DurGgNullFormatted() {
        return Functions.padBlanks(getWb03DurGgNull(), Len.WB03_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR_GG = 1;
        public static final int WB03_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR_GG = 3;
        public static final int WB03_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
