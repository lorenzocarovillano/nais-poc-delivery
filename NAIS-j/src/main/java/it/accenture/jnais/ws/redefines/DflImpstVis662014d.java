package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-VIS-662014D<br>
 * Variable: DFL-IMPST-VIS-662014D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstVis662014d extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstVis662014d() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_VIS662014D;
    }

    public void setDflImpstVis662014d(AfDecimal dflImpstVis662014d) {
        writeDecimalAsPacked(Pos.DFL_IMPST_VIS662014D, dflImpstVis662014d.copy());
    }

    public void setDflImpstVis662014dFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_VIS662014D, Pos.DFL_IMPST_VIS662014D);
    }

    /**Original name: DFL-IMPST-VIS-662014D<br>*/
    public AfDecimal getDflImpstVis662014d() {
        return readPackedAsDecimal(Pos.DFL_IMPST_VIS662014D, Len.Int.DFL_IMPST_VIS662014D, Len.Fract.DFL_IMPST_VIS662014D);
    }

    public byte[] getDflImpstVis662014dAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_VIS662014D, Pos.DFL_IMPST_VIS662014D);
        return buffer;
    }

    public void setDflImpstVis662014dNull(String dflImpstVis662014dNull) {
        writeString(Pos.DFL_IMPST_VIS662014D_NULL, dflImpstVis662014dNull, Len.DFL_IMPST_VIS662014D_NULL);
    }

    /**Original name: DFL-IMPST-VIS-662014D-NULL<br>*/
    public String getDflImpstVis662014dNull() {
        return readString(Pos.DFL_IMPST_VIS662014D_NULL, Len.DFL_IMPST_VIS662014D_NULL);
    }

    public String getDflImpstVis662014dNullFormatted() {
        return Functions.padBlanks(getDflImpstVis662014dNull(), Len.DFL_IMPST_VIS662014D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS662014D = 1;
        public static final int DFL_IMPST_VIS662014D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS662014D = 8;
        public static final int DFL_IMPST_VIS662014D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS662014D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS662014D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
