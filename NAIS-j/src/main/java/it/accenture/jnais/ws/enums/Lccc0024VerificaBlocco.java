package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LCCC0024-VERIFICA-BLOCCO<br>
 * Variable: LCCC0024-VERIFICA-BLOCCO from copybook LCCC0024<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0024VerificaBlocco {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char PRESENTE_PTF = '0';
    public static final char CENSITO_PTF = '1';

    //==== METHODS ====
    public void setVerificaBlocco(char verificaBlocco) {
        this.value = verificaBlocco;
    }

    public char getVerificaBlocco() {
        return this.value;
    }

    public boolean isLccc0024BlcCensitoPtf() {
        return value == CENSITO_PTF;
    }

    public void setCensitoPtf() {
        value = CENSITO_PTF;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int VERIFICA_BLOCCO = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
