package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPRE-TP-ELA-EST-CONTO-SEL<br>
 * Variable: WPRE-TP-ELA-EST-CONTO-SEL from copybook LOAC0560<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpreTpElaEstContoSel {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TP_ELA_EST_CONTO);
    public static final String RIVALUTAB_SEL = "RV";
    public static final String INDEX_LINKED_SEL = "IL";
    public static final String UNIT_LINKED_SEL = "UL";
    public static final String MULTIRAMO_SEL = "MU";
    public static final String TCM_SEL = "TC";

    //==== METHODS ====
    public void setTpElaEstConto(String tpElaEstConto) {
        this.value = Functions.subString(tpElaEstConto, Len.TP_ELA_EST_CONTO);
    }

    public String getTpElaEstConto() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_ELA_EST_CONTO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
