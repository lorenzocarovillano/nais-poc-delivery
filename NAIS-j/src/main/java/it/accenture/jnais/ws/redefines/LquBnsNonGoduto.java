package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-BNS-NON-GODUTO<br>
 * Variable: LQU-BNS-NON-GODUTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquBnsNonGoduto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquBnsNonGoduto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_BNS_NON_GODUTO;
    }

    public void setLquBnsNonGoduto(AfDecimal lquBnsNonGoduto) {
        writeDecimalAsPacked(Pos.LQU_BNS_NON_GODUTO, lquBnsNonGoduto.copy());
    }

    public void setLquBnsNonGodutoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_BNS_NON_GODUTO, Pos.LQU_BNS_NON_GODUTO);
    }

    /**Original name: LQU-BNS-NON-GODUTO<br>*/
    public AfDecimal getLquBnsNonGoduto() {
        return readPackedAsDecimal(Pos.LQU_BNS_NON_GODUTO, Len.Int.LQU_BNS_NON_GODUTO, Len.Fract.LQU_BNS_NON_GODUTO);
    }

    public byte[] getLquBnsNonGodutoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_BNS_NON_GODUTO, Pos.LQU_BNS_NON_GODUTO);
        return buffer;
    }

    public void setLquBnsNonGodutoNull(String lquBnsNonGodutoNull) {
        writeString(Pos.LQU_BNS_NON_GODUTO_NULL, lquBnsNonGodutoNull, Len.LQU_BNS_NON_GODUTO_NULL);
    }

    /**Original name: LQU-BNS-NON-GODUTO-NULL<br>*/
    public String getLquBnsNonGodutoNull() {
        return readString(Pos.LQU_BNS_NON_GODUTO_NULL, Len.LQU_BNS_NON_GODUTO_NULL);
    }

    public String getLquBnsNonGodutoNullFormatted() {
        return Functions.padBlanks(getLquBnsNonGodutoNull(), Len.LQU_BNS_NON_GODUTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_BNS_NON_GODUTO = 1;
        public static final int LQU_BNS_NON_GODUTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_BNS_NON_GODUTO = 8;
        public static final int LQU_BNS_NON_GODUTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_BNS_NON_GODUTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_BNS_NON_GODUTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
