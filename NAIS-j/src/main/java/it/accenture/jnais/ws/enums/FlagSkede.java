package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-SKEDE<br>
 * Variable: FLAG-SKEDE from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagSkede {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_SKEDE);
    public static final String PRODOTTI = "PR";
    public static final String GARANZIE = "GA";
    public static final String TOTALI = "**";

    //==== METHODS ====
    public void setFlagSkede(String flagSkede) {
        this.value = Functions.subString(flagSkede, Len.FLAG_SKEDE);
    }

    public String getFlagSkede() {
        return this.value;
    }

    public boolean isProdotti() {
        return value.equals(PRODOTTI);
    }

    public boolean isGaranzie() {
        return value.equals(GARANZIE);
    }

    public boolean isTotali() {
        return value.equals(TOTALI);
    }

    public void setTotali() {
        value = TOTALI;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_SKEDE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
