package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-CAR-IAS<br>
 * Variable: WDTC-CAR-IAS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcCarIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcCarIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_CAR_IAS;
    }

    public void setWdtcCarIas(AfDecimal wdtcCarIas) {
        writeDecimalAsPacked(Pos.WDTC_CAR_IAS, wdtcCarIas.copy());
    }

    public void setWdtcCarIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_CAR_IAS, Pos.WDTC_CAR_IAS);
    }

    /**Original name: WDTC-CAR-IAS<br>*/
    public AfDecimal getWdtcCarIas() {
        return readPackedAsDecimal(Pos.WDTC_CAR_IAS, Len.Int.WDTC_CAR_IAS, Len.Fract.WDTC_CAR_IAS);
    }

    public byte[] getWdtcCarIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_CAR_IAS, Pos.WDTC_CAR_IAS);
        return buffer;
    }

    public void initWdtcCarIasSpaces() {
        fill(Pos.WDTC_CAR_IAS, Len.WDTC_CAR_IAS, Types.SPACE_CHAR);
    }

    public void setWdtcCarIasNull(String wdtcCarIasNull) {
        writeString(Pos.WDTC_CAR_IAS_NULL, wdtcCarIasNull, Len.WDTC_CAR_IAS_NULL);
    }

    /**Original name: WDTC-CAR-IAS-NULL<br>*/
    public String getWdtcCarIasNull() {
        return readString(Pos.WDTC_CAR_IAS_NULL, Len.WDTC_CAR_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_CAR_IAS = 1;
        public static final int WDTC_CAR_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_CAR_IAS = 8;
        public static final int WDTC_CAR_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_CAR_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_CAR_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
