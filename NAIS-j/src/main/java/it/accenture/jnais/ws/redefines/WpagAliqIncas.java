package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-ALIQ-INCAS<br>
 * Variable: WPAG-ALIQ-INCAS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagAliqIncas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagAliqIncas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_ALIQ_INCAS;
    }

    public void setWpagAliqIncas(AfDecimal wpagAliqIncas) {
        writeDecimalAsPacked(Pos.WPAG_ALIQ_INCAS, wpagAliqIncas.copy());
    }

    public void setWpagAliqIncasFormatted(String wpagAliqIncas) {
        setWpagAliqIncas(PicParser.display(new PicParams("S9(5)V9(9)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_ALIQ_INCAS + Len.Fract.WPAG_ALIQ_INCAS, Len.Fract.WPAG_ALIQ_INCAS, wpagAliqIncas));
    }

    public void setWpagAliqIncasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_ALIQ_INCAS, Pos.WPAG_ALIQ_INCAS);
    }

    /**Original name: WPAG-ALIQ-INCAS<br>*/
    public AfDecimal getWpagAliqIncas() {
        return readPackedAsDecimal(Pos.WPAG_ALIQ_INCAS, Len.Int.WPAG_ALIQ_INCAS, Len.Fract.WPAG_ALIQ_INCAS);
    }

    public byte[] getWpagAliqIncasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_ALIQ_INCAS, Pos.WPAG_ALIQ_INCAS);
        return buffer;
    }

    public void initWpagAliqIncasSpaces() {
        fill(Pos.WPAG_ALIQ_INCAS, Len.WPAG_ALIQ_INCAS, Types.SPACE_CHAR);
    }

    public void setWpagAliqIncasNull(String wpagAliqIncasNull) {
        writeString(Pos.WPAG_ALIQ_INCAS_NULL, wpagAliqIncasNull, Len.WPAG_ALIQ_INCAS_NULL);
    }

    /**Original name: WPAG-ALIQ-INCAS-NULL<br>*/
    public String getWpagAliqIncasNull() {
        return readString(Pos.WPAG_ALIQ_INCAS_NULL, Len.WPAG_ALIQ_INCAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_ALIQ_INCAS = 1;
        public static final int WPAG_ALIQ_INCAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_ALIQ_INCAS = 8;
        public static final int WPAG_ALIQ_INCAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_ALIQ_INCAS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_ALIQ_INCAS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
