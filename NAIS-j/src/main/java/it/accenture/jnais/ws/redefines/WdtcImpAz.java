package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-IMP-AZ<br>
 * Variable: WDTC-IMP-AZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_IMP_AZ;
    }

    public void setWdtcImpAz(AfDecimal wdtcImpAz) {
        writeDecimalAsPacked(Pos.WDTC_IMP_AZ, wdtcImpAz.copy());
    }

    public void setWdtcImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_IMP_AZ, Pos.WDTC_IMP_AZ);
    }

    /**Original name: WDTC-IMP-AZ<br>*/
    public AfDecimal getWdtcImpAz() {
        return readPackedAsDecimal(Pos.WDTC_IMP_AZ, Len.Int.WDTC_IMP_AZ, Len.Fract.WDTC_IMP_AZ);
    }

    public byte[] getWdtcImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_IMP_AZ, Pos.WDTC_IMP_AZ);
        return buffer;
    }

    public void initWdtcImpAzSpaces() {
        fill(Pos.WDTC_IMP_AZ, Len.WDTC_IMP_AZ, Types.SPACE_CHAR);
    }

    public void setWdtcImpAzNull(String wdtcImpAzNull) {
        writeString(Pos.WDTC_IMP_AZ_NULL, wdtcImpAzNull, Len.WDTC_IMP_AZ_NULL);
    }

    /**Original name: WDTC-IMP-AZ-NULL<br>*/
    public String getWdtcImpAzNull() {
        return readString(Pos.WDTC_IMP_AZ_NULL, Len.WDTC_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_AZ = 1;
        public static final int WDTC_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_AZ = 8;
        public static final int WDTC_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
