package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: V1391-LUNGHEZZA-DATO<br>
 * Variable: V1391-LUNGHEZZA-DATO from program LDBS1390<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class V1391LunghezzaDato extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public V1391LunghezzaDato() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.V1391_LUNGHEZZA_DATO;
    }

    public void setV1391LunghezzaDato(int v1391LunghezzaDato) {
        writeIntAsPacked(Pos.V1391_LUNGHEZZA_DATO, v1391LunghezzaDato, Len.Int.V1391_LUNGHEZZA_DATO);
    }

    public void setV1391LunghezzaDatoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.V1391_LUNGHEZZA_DATO, Pos.V1391_LUNGHEZZA_DATO);
    }

    /**Original name: V1391-LUNGHEZZA-DATO<br>*/
    public int getV1391LunghezzaDato() {
        return readPackedAsInt(Pos.V1391_LUNGHEZZA_DATO, Len.Int.V1391_LUNGHEZZA_DATO);
    }

    public byte[] getV1391LunghezzaDatoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.V1391_LUNGHEZZA_DATO, Pos.V1391_LUNGHEZZA_DATO);
        return buffer;
    }

    public void initV1391LunghezzaDatoSpaces() {
        fill(Pos.V1391_LUNGHEZZA_DATO, Len.V1391_LUNGHEZZA_DATO, Types.SPACE_CHAR);
    }

    public void setV1391LunghezzaDatoNull(String v1391LunghezzaDatoNull) {
        writeString(Pos.V1391_LUNGHEZZA_DATO_NULL, v1391LunghezzaDatoNull, Len.V1391_LUNGHEZZA_DATO_NULL);
    }

    /**Original name: V1391-LUNGHEZZA-DATO-NULL<br>*/
    public String getV1391LunghezzaDatoNull() {
        return readString(Pos.V1391_LUNGHEZZA_DATO_NULL, Len.V1391_LUNGHEZZA_DATO_NULL);
    }

    public String getV1391LunghezzaDatoNullFormatted() {
        return Functions.padBlanks(getV1391LunghezzaDatoNull(), Len.V1391_LUNGHEZZA_DATO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int V1391_LUNGHEZZA_DATO = 1;
        public static final int V1391_LUNGHEZZA_DATO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int V1391_LUNGHEZZA_DATO = 3;
        public static final int V1391_LUNGHEZZA_DATO_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int V1391_LUNGHEZZA_DATO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
