package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: COMODO-STAT-BUS-ELE<br>
 * Variables: COMODO-STAT-BUS-ELE from program IVVS0211<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class ComodoStatBusEle {

    //==== PROPERTIES ====
    //Original name: COMODO-TP-STAT-BUS
    private String comodoTpStatBus = DefaultValues.stringVal(Len.COMODO_TP_STAT_BUS);

    //==== METHODS ====
    public byte[] getStatBusEleBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, comodoTpStatBus, Len.COMODO_TP_STAT_BUS);
        return buffer;
    }

    public void setComodoTpStatBus(String comodoTpStatBus) {
        this.comodoTpStatBus = Functions.subString(comodoTpStatBus, Len.COMODO_TP_STAT_BUS);
    }

    public String getComodoTpStatBus() {
        return this.comodoTpStatBus;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COMODO_TP_STAT_BUS = 2;
        public static final int STAT_BUS_ELE = COMODO_TP_STAT_BUS;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
