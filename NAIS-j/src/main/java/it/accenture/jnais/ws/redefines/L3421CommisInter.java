package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-COMMIS-INTER<br>
 * Variable: L3421-COMMIS-INTER from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421CommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421CommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_COMMIS_INTER;
    }

    public void setL3421CommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_COMMIS_INTER, Pos.L3421_COMMIS_INTER);
    }

    /**Original name: L3421-COMMIS-INTER<br>*/
    public AfDecimal getL3421CommisInter() {
        return readPackedAsDecimal(Pos.L3421_COMMIS_INTER, Len.Int.L3421_COMMIS_INTER, Len.Fract.L3421_COMMIS_INTER);
    }

    public byte[] getL3421CommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_COMMIS_INTER, Pos.L3421_COMMIS_INTER);
        return buffer;
    }

    /**Original name: L3421-COMMIS-INTER-NULL<br>*/
    public String getL3421CommisInterNull() {
        return readString(Pos.L3421_COMMIS_INTER_NULL, Len.L3421_COMMIS_INTER_NULL);
    }

    public String getL3421CommisInterNullFormatted() {
        return Functions.padBlanks(getL3421CommisInterNull(), Len.L3421_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_COMMIS_INTER = 1;
        public static final int L3421_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_COMMIS_INTER = 8;
        public static final int L3421_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
