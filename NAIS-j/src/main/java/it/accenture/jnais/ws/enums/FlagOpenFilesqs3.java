package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-OPEN-FILESQS3<br>
 * Variable: FLAG-OPEN-FILESQS3 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagOpenFilesqs3 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagOpenFilesqs3(char flagOpenFilesqs3) {
        this.value = flagOpenFilesqs3;
    }

    public char getFlagOpenFilesqs3() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setOpenFilesqs3Si() {
        value = SI;
    }

    public void setOpenFilesqs3No() {
        value = NO;
    }
}
