package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DIR-EMIS<br>
 * Variable: B03-DIR-EMIS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DirEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DirEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DIR_EMIS;
    }

    public void setB03DirEmis(AfDecimal b03DirEmis) {
        writeDecimalAsPacked(Pos.B03_DIR_EMIS, b03DirEmis.copy());
    }

    public void setB03DirEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DIR_EMIS, Pos.B03_DIR_EMIS);
    }

    /**Original name: B03-DIR-EMIS<br>*/
    public AfDecimal getB03DirEmis() {
        return readPackedAsDecimal(Pos.B03_DIR_EMIS, Len.Int.B03_DIR_EMIS, Len.Fract.B03_DIR_EMIS);
    }

    public byte[] getB03DirEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DIR_EMIS, Pos.B03_DIR_EMIS);
        return buffer;
    }

    public void setB03DirEmisNull(String b03DirEmisNull) {
        writeString(Pos.B03_DIR_EMIS_NULL, b03DirEmisNull, Len.B03_DIR_EMIS_NULL);
    }

    /**Original name: B03-DIR-EMIS-NULL<br>*/
    public String getB03DirEmisNull() {
        return readString(Pos.B03_DIR_EMIS_NULL, Len.B03_DIR_EMIS_NULL);
    }

    public String getB03DirEmisNullFormatted() {
        return Functions.padBlanks(getB03DirEmisNull(), Len.B03_DIR_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DIR_EMIS = 1;
        public static final int B03_DIR_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DIR_EMIS = 8;
        public static final int B03_DIR_EMIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_DIR_EMIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DIR_EMIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
