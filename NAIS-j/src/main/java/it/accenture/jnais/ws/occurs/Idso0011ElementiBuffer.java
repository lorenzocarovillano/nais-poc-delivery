package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSO0011-ELEMENTI-BUFFER<br>
 * Variables: IDSO0011-ELEMENTI-BUFFER from copybook IDSO0011<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idso0011ElementiBuffer {

    //==== PROPERTIES ====
    //Original name: IDSO0011-CODICE-STR-DATO
    private String codiceStrDato = DefaultValues.stringVal(Len.CODICE_STR_DATO);
    //Original name: IDSO0011-INIZIO-POSIZIONE
    private int inizioPosizione = DefaultValues.INT_VAL;
    //Original name: IDSO0011-LUNGHEZZA-DATO
    private int lunghezzaDato = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setElementiBufferBytes(byte[] buffer, int offset) {
        int position = offset;
        codiceStrDato = MarshalByte.readString(buffer, position, Len.CODICE_STR_DATO);
        position += Len.CODICE_STR_DATO;
        inizioPosizione = MarshalByte.readPackedAsInt(buffer, position, Len.Int.INIZIO_POSIZIONE, 0);
        position += Len.INIZIO_POSIZIONE;
        lunghezzaDato = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LUNGHEZZA_DATO, 0);
    }

    public byte[] getElementiBufferBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codiceStrDato, Len.CODICE_STR_DATO);
        position += Len.CODICE_STR_DATO;
        MarshalByte.writeIntAsPacked(buffer, position, inizioPosizione, Len.Int.INIZIO_POSIZIONE, 0);
        position += Len.INIZIO_POSIZIONE;
        MarshalByte.writeIntAsPacked(buffer, position, lunghezzaDato, Len.Int.LUNGHEZZA_DATO, 0);
        return buffer;
    }

    public void initElementiBufferSpaces() {
        codiceStrDato = "";
        inizioPosizione = Types.INVALID_INT_VAL;
        lunghezzaDato = Types.INVALID_INT_VAL;
    }

    public void setCodiceStrDato(String codiceStrDato) {
        this.codiceStrDato = Functions.subString(codiceStrDato, Len.CODICE_STR_DATO);
    }

    public String getCodiceStrDato() {
        return this.codiceStrDato;
    }

    public void setInizioPosizione(int inizioPosizione) {
        this.inizioPosizione = inizioPosizione;
    }

    public int getInizioPosizione() {
        return this.inizioPosizione;
    }

    public void setLunghezzaDato(int lunghezzaDato) {
        this.lunghezzaDato = lunghezzaDato;
    }

    public int getLunghezzaDato() {
        return this.lunghezzaDato;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_STR_DATO = 30;
        public static final int INIZIO_POSIZIONE = 3;
        public static final int LUNGHEZZA_DATO = 3;
        public static final int ELEMENTI_BUFFER = CODICE_STR_DATO + INIZIO_POSIZIONE + LUNGHEZZA_DATO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int INIZIO_POSIZIONE = 5;
            public static final int LUNGHEZZA_DATO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
