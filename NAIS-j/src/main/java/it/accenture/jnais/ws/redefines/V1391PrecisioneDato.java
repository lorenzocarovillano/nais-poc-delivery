package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: V1391-PRECISIONE-DATO<br>
 * Variable: V1391-PRECISIONE-DATO from program LDBS1390<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class V1391PrecisioneDato extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public V1391PrecisioneDato() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.V1391_PRECISIONE_DATO;
    }

    public void setV1391PrecisioneDato(short v1391PrecisioneDato) {
        writeShortAsPacked(Pos.V1391_PRECISIONE_DATO, v1391PrecisioneDato, Len.Int.V1391_PRECISIONE_DATO);
    }

    public void setV1391PrecisioneDatoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.V1391_PRECISIONE_DATO, Pos.V1391_PRECISIONE_DATO);
    }

    /**Original name: V1391-PRECISIONE-DATO<br>*/
    public short getV1391PrecisioneDato() {
        return readPackedAsShort(Pos.V1391_PRECISIONE_DATO, Len.Int.V1391_PRECISIONE_DATO);
    }

    public byte[] getV1391PrecisioneDatoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.V1391_PRECISIONE_DATO, Pos.V1391_PRECISIONE_DATO);
        return buffer;
    }

    public void initV1391PrecisioneDatoSpaces() {
        fill(Pos.V1391_PRECISIONE_DATO, Len.V1391_PRECISIONE_DATO, Types.SPACE_CHAR);
    }

    public void setV1391PrecisioneDatoNull(String v1391PrecisioneDatoNull) {
        writeString(Pos.V1391_PRECISIONE_DATO_NULL, v1391PrecisioneDatoNull, Len.V1391_PRECISIONE_DATO_NULL);
    }

    /**Original name: V1391-PRECISIONE-DATO-NULL<br>*/
    public String getV1391PrecisioneDatoNull() {
        return readString(Pos.V1391_PRECISIONE_DATO_NULL, Len.V1391_PRECISIONE_DATO_NULL);
    }

    public String getV1391PrecisioneDatoNullFormatted() {
        return Functions.padBlanks(getV1391PrecisioneDatoNull(), Len.V1391_PRECISIONE_DATO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int V1391_PRECISIONE_DATO = 1;
        public static final int V1391_PRECISIONE_DATO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int V1391_PRECISIONE_DATO = 2;
        public static final int V1391_PRECISIONE_DATO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int V1391_PRECISIONE_DATO = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
