package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvbep1;
import it.accenture.jnais.copy.WbepDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WBEP-TAB-BENEFICIARI<br>
 * Variables: WBEP-TAB-BENEFICIARI from copybook LCCVBEPA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WbepTabBeneficiari {

    //==== PROPERTIES ====
    //Original name: LCCVBEP1
    private Lccvbep1 lccvbep1 = new Lccvbep1();

    //==== METHODS ====
    public void setVbepTabBeneficiariBytes(byte[] buffer) {
        setWbepTabBeneficiariBytes(buffer, 1);
    }

    public byte[] getTabBeneficiariBytes() {
        byte[] buffer = new byte[Len.WBEP_TAB_BENEFICIARI];
        return getWbepTabBeneficiariBytes(buffer, 1);
    }

    public void setWbepTabBeneficiariBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvbep1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvbep1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvbep1.Len.Int.ID_PTF, 0));
        position += Lccvbep1.Len.ID_PTF;
        lccvbep1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWbepTabBeneficiariBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvbep1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvbep1.getIdPtf(), Lccvbep1.Len.Int.ID_PTF, 0);
        position += Lccvbep1.Len.ID_PTF;
        lccvbep1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWbepTabBeneficiariSpaces() {
        lccvbep1.initLccvbep1Spaces();
    }

    public Lccvbep1 getLccvbep1() {
        return lccvbep1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEP_TAB_BENEFICIARI = WpolStatus.Len.STATUS + Lccvbep1.Len.ID_PTF + WbepDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
