package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRIF-ID-MOVI-CHIU<br>
 * Variable: WRIF-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrifIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrifIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIF_ID_MOVI_CHIU;
    }

    public void setWrifIdMoviChiu(int wrifIdMoviChiu) {
        writeIntAsPacked(Pos.WRIF_ID_MOVI_CHIU, wrifIdMoviChiu, Len.Int.WRIF_ID_MOVI_CHIU);
    }

    public void setWrifIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIF_ID_MOVI_CHIU, Pos.WRIF_ID_MOVI_CHIU);
    }

    /**Original name: WRIF-ID-MOVI-CHIU<br>*/
    public int getWrifIdMoviChiu() {
        return readPackedAsInt(Pos.WRIF_ID_MOVI_CHIU, Len.Int.WRIF_ID_MOVI_CHIU);
    }

    public byte[] getWrifIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIF_ID_MOVI_CHIU, Pos.WRIF_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWrifIdMoviChiuSpaces() {
        fill(Pos.WRIF_ID_MOVI_CHIU, Len.WRIF_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWrifIdMoviChiuNull(String wrifIdMoviChiuNull) {
        writeString(Pos.WRIF_ID_MOVI_CHIU_NULL, wrifIdMoviChiuNull, Len.WRIF_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WRIF-ID-MOVI-CHIU-NULL<br>*/
    public String getWrifIdMoviChiuNull() {
        return readString(Pos.WRIF_ID_MOVI_CHIU_NULL, Len.WRIF_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIF_ID_MOVI_CHIU = 1;
        public static final int WRIF_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIF_ID_MOVI_CHIU = 5;
        public static final int WRIF_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIF_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
