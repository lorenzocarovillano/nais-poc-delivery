package it.accenture.jnais.ws;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LOAS0320<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLoas0320 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-GRZ
    private short grz = ((short)0);
    //Original name: IX-TAB-TGA
    private short tga = ((short)0);
    //Original name: IX-TAB-TDR
    private short tdr = ((short)0);
    //Original name: IX-TAB-DTR
    private short dtr = ((short)0);

    //==== METHODS ====
    public void setGrz(short grz) {
        this.grz = grz;
    }

    public short getGrz() {
        return this.grz;
    }

    public void setTga(short tga) {
        this.tga = tga;
    }

    public short getTga() {
        return this.tga;
    }

    public void setTdr(short tdr) {
        this.tdr = tdr;
    }

    public short getTdr() {
        return this.tdr;
    }

    public void setDtr(short dtr) {
        this.dtr = dtr;
    }

    public short getDtr() {
        return this.dtr;
    }
}
