package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import it.accenture.jnais.ws.enums.FlagBatchComplete;
import it.accenture.jnais.ws.enums.FlagBatchState;
import it.accenture.jnais.ws.enums.WsFlagFase;

/**Original name: WS-BUFFER-WHERE-COND-IABS0130<br>
 * Variable: WS-BUFFER-WHERE-COND-IABS0130 from program IABS0130<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsBufferWhereCondIabs0130 {

    //==== PROPERTIES ====
    //Original name: WS-FLAG-FASE
    private WsFlagFase wsFlagFase = new WsFlagFase();
    //Original name: FLAG-BATCH-COMPLETE
    private FlagBatchComplete flagBatchComplete = new FlagBatchComplete();
    //Original name: FLAG-BATCH-STATE
    private FlagBatchState flagBatchState = new FlagBatchState();

    //==== METHODS ====
    public void setWsBufferWhereCondIabs0130Formatted(String data) {
        byte[] buffer = new byte[Len.WS_BUFFER_WHERE_COND_IABS0130];
        MarshalByte.writeString(buffer, 1, data, Len.WS_BUFFER_WHERE_COND_IABS0130);
        setWsBufferWhereCondIabs0130Bytes(buffer, 1);
    }

    public String getWsBufferWhereCondIabs0130Formatted() {
        return MarshalByteExt.bufferToStr(getWsBufferWhereCondIabs0130Bytes());
    }

    public byte[] getWsBufferWhereCondIabs0130Bytes() {
        byte[] buffer = new byte[Len.WS_BUFFER_WHERE_COND_IABS0130];
        return getWsBufferWhereCondIabs0130Bytes(buffer, 1);
    }

    public void setWsBufferWhereCondIabs0130Bytes(byte[] buffer, int offset) {
        int position = offset;
        wsFlagFase.setWsFlagFase(MarshalByte.readString(buffer, position, WsFlagFase.Len.WS_FLAG_FASE));
        position += WsFlagFase.Len.WS_FLAG_FASE;
        flagBatchComplete.setFlagBatchComplete(MarshalByte.readString(buffer, position, FlagBatchComplete.Len.FLAG_BATCH_COMPLETE));
        position += FlagBatchComplete.Len.FLAG_BATCH_COMPLETE;
        flagBatchState.setFlagBatchState(MarshalByte.readString(buffer, position, FlagBatchState.Len.FLAG_BATCH_STATE));
    }

    public byte[] getWsBufferWhereCondIabs0130Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wsFlagFase.getWsFlagFase(), WsFlagFase.Len.WS_FLAG_FASE);
        position += WsFlagFase.Len.WS_FLAG_FASE;
        MarshalByte.writeString(buffer, position, flagBatchComplete.getFlagBatchComplete(), FlagBatchComplete.Len.FLAG_BATCH_COMPLETE);
        position += FlagBatchComplete.Len.FLAG_BATCH_COMPLETE;
        MarshalByte.writeString(buffer, position, flagBatchState.getFlagBatchState(), FlagBatchState.Len.FLAG_BATCH_STATE);
        return buffer;
    }

    public FlagBatchComplete getFlagBatchComplete() {
        return flagBatchComplete;
    }

    public FlagBatchState getFlagBatchState() {
        return flagBatchState;
    }

    public WsFlagFase getWsFlagFase() {
        return wsFlagFase;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_BUFFER_WHERE_COND_IABS0130 = WsFlagFase.Len.WS_FLAG_FASE + FlagBatchComplete.Len.FLAG_BATCH_COMPLETE + FlagBatchState.Len.FLAG_BATCH_STATE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
