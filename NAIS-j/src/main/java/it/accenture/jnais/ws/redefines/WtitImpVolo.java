package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-IMP-VOLO<br>
 * Variable: WTIT-IMP-VOLO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_IMP_VOLO;
    }

    public void setWtitImpVolo(AfDecimal wtitImpVolo) {
        writeDecimalAsPacked(Pos.WTIT_IMP_VOLO, wtitImpVolo.copy());
    }

    public void setWtitImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_IMP_VOLO, Pos.WTIT_IMP_VOLO);
    }

    /**Original name: WTIT-IMP-VOLO<br>*/
    public AfDecimal getWtitImpVolo() {
        return readPackedAsDecimal(Pos.WTIT_IMP_VOLO, Len.Int.WTIT_IMP_VOLO, Len.Fract.WTIT_IMP_VOLO);
    }

    public byte[] getWtitImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_IMP_VOLO, Pos.WTIT_IMP_VOLO);
        return buffer;
    }

    public void initWtitImpVoloSpaces() {
        fill(Pos.WTIT_IMP_VOLO, Len.WTIT_IMP_VOLO, Types.SPACE_CHAR);
    }

    public void setWtitImpVoloNull(String wtitImpVoloNull) {
        writeString(Pos.WTIT_IMP_VOLO_NULL, wtitImpVoloNull, Len.WTIT_IMP_VOLO_NULL);
    }

    /**Original name: WTIT-IMP-VOLO-NULL<br>*/
    public String getWtitImpVoloNull() {
        return readString(Pos.WTIT_IMP_VOLO_NULL, Len.WTIT_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_VOLO = 1;
        public static final int WTIT_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_VOLO = 8;
        public static final int WTIT_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
