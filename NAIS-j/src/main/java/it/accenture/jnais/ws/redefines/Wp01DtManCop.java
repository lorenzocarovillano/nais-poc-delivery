package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP01-DT-MAN-COP<br>
 * Variable: WP01-DT-MAN-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp01DtManCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp01DtManCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP01_DT_MAN_COP;
    }

    public void setWp01DtManCop(int wp01DtManCop) {
        writeIntAsPacked(Pos.WP01_DT_MAN_COP, wp01DtManCop, Len.Int.WP01_DT_MAN_COP);
    }

    public void setWp01DtManCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP01_DT_MAN_COP, Pos.WP01_DT_MAN_COP);
    }

    /**Original name: WP01-DT-MAN-COP<br>*/
    public int getWp01DtManCop() {
        return readPackedAsInt(Pos.WP01_DT_MAN_COP, Len.Int.WP01_DT_MAN_COP);
    }

    public byte[] getWp01DtManCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP01_DT_MAN_COP, Pos.WP01_DT_MAN_COP);
        return buffer;
    }

    public void setWp01DtManCopNull(String wp01DtManCopNull) {
        writeString(Pos.WP01_DT_MAN_COP_NULL, wp01DtManCopNull, Len.WP01_DT_MAN_COP_NULL);
    }

    /**Original name: WP01-DT-MAN-COP-NULL<br>*/
    public String getWp01DtManCopNull() {
        return readString(Pos.WP01_DT_MAN_COP_NULL, Len.WP01_DT_MAN_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP01_DT_MAN_COP = 1;
        public static final int WP01_DT_MAN_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP01_DT_MAN_COP = 5;
        public static final int WP01_DT_MAN_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP01_DT_MAN_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
