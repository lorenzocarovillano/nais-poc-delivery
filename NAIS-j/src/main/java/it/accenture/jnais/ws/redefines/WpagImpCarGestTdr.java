package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPAG-IMP-CAR-GEST-TDR<br>
 * Variable: WPAG-IMP-CAR-GEST-TDR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpCarGestTdr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpCarGestTdr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_CAR_GEST_TDR;
    }

    public void setWpagImpCarGestTdr(AfDecimal wpagImpCarGestTdr) {
        writeDecimalAsPacked(Pos.WPAG_IMP_CAR_GEST_TDR, wpagImpCarGestTdr.copy());
    }

    public void setWpagImpCarGestTdrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_CAR_GEST_TDR, Pos.WPAG_IMP_CAR_GEST_TDR);
    }

    /**Original name: WPAG-IMP-CAR-GEST-TDR<br>*/
    public AfDecimal getWpagImpCarGestTdr() {
        return readPackedAsDecimal(Pos.WPAG_IMP_CAR_GEST_TDR, Len.Int.WPAG_IMP_CAR_GEST_TDR, Len.Fract.WPAG_IMP_CAR_GEST_TDR);
    }

    public byte[] getWpagImpCarGestTdrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_CAR_GEST_TDR, Pos.WPAG_IMP_CAR_GEST_TDR);
        return buffer;
    }

    public void initWpagImpCarGestTdrSpaces() {
        fill(Pos.WPAG_IMP_CAR_GEST_TDR, Len.WPAG_IMP_CAR_GEST_TDR, Types.SPACE_CHAR);
    }

    public void setWpagImpCarGestTdrNull(String wpagImpCarGestTdrNull) {
        writeString(Pos.WPAG_IMP_CAR_GEST_TDR_NULL, wpagImpCarGestTdrNull, Len.WPAG_IMP_CAR_GEST_TDR_NULL);
    }

    /**Original name: WPAG-IMP-CAR-GEST-TDR-NULL<br>*/
    public String getWpagImpCarGestTdrNull() {
        return readString(Pos.WPAG_IMP_CAR_GEST_TDR_NULL, Len.WPAG_IMP_CAR_GEST_TDR_NULL);
    }

    public String getWpagImpCarGestTdrNullFormatted() {
        return Functions.padBlanks(getWpagImpCarGestTdrNull(), Len.WPAG_IMP_CAR_GEST_TDR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_GEST_TDR = 1;
        public static final int WPAG_IMP_CAR_GEST_TDR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_GEST_TDR = 8;
        public static final int WPAG_IMP_CAR_GEST_TDR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_GEST_TDR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_GEST_TDR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
