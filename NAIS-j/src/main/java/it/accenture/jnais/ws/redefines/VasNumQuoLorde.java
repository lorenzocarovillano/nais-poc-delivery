package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-NUM-QUO-LORDE<br>
 * Variable: VAS-NUM-QUO-LORDE from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasNumQuoLorde extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasNumQuoLorde() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_NUM_QUO_LORDE;
    }

    public void setVasNumQuoLorde(AfDecimal vasNumQuoLorde) {
        writeDecimalAsPacked(Pos.VAS_NUM_QUO_LORDE, vasNumQuoLorde.copy());
    }

    public void setVasNumQuoLordeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_NUM_QUO_LORDE, Pos.VAS_NUM_QUO_LORDE);
    }

    /**Original name: VAS-NUM-QUO-LORDE<br>*/
    public AfDecimal getVasNumQuoLorde() {
        return readPackedAsDecimal(Pos.VAS_NUM_QUO_LORDE, Len.Int.VAS_NUM_QUO_LORDE, Len.Fract.VAS_NUM_QUO_LORDE);
    }

    public byte[] getVasNumQuoLordeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_NUM_QUO_LORDE, Pos.VAS_NUM_QUO_LORDE);
        return buffer;
    }

    public void setVasNumQuoLordeNull(String vasNumQuoLordeNull) {
        writeString(Pos.VAS_NUM_QUO_LORDE_NULL, vasNumQuoLordeNull, Len.VAS_NUM_QUO_LORDE_NULL);
    }

    /**Original name: VAS-NUM-QUO-LORDE-NULL<br>*/
    public String getVasNumQuoLordeNull() {
        return readString(Pos.VAS_NUM_QUO_LORDE_NULL, Len.VAS_NUM_QUO_LORDE_NULL);
    }

    public String getVasNumQuoLordeNullFormatted() {
        return Functions.padBlanks(getVasNumQuoLordeNull(), Len.VAS_NUM_QUO_LORDE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_NUM_QUO_LORDE = 1;
        public static final int VAS_NUM_QUO_LORDE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_NUM_QUO_LORDE = 7;
        public static final int VAS_NUM_QUO_LORDE_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAS_NUM_QUO_LORDE = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_NUM_QUO_LORDE = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
