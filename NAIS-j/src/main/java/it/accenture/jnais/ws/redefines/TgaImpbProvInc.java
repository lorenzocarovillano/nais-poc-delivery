package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMPB-PROV-INC<br>
 * Variable: TGA-IMPB-PROV-INC from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpbProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpbProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMPB_PROV_INC;
    }

    public void setTgaImpbProvInc(AfDecimal tgaImpbProvInc) {
        writeDecimalAsPacked(Pos.TGA_IMPB_PROV_INC, tgaImpbProvInc.copy());
    }

    public void setTgaImpbProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMPB_PROV_INC, Pos.TGA_IMPB_PROV_INC);
    }

    /**Original name: TGA-IMPB-PROV-INC<br>*/
    public AfDecimal getTgaImpbProvInc() {
        return readPackedAsDecimal(Pos.TGA_IMPB_PROV_INC, Len.Int.TGA_IMPB_PROV_INC, Len.Fract.TGA_IMPB_PROV_INC);
    }

    public byte[] getTgaImpbProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMPB_PROV_INC, Pos.TGA_IMPB_PROV_INC);
        return buffer;
    }

    public void setTgaImpbProvIncNull(String tgaImpbProvIncNull) {
        writeString(Pos.TGA_IMPB_PROV_INC_NULL, tgaImpbProvIncNull, Len.TGA_IMPB_PROV_INC_NULL);
    }

    /**Original name: TGA-IMPB-PROV-INC-NULL<br>*/
    public String getTgaImpbProvIncNull() {
        return readString(Pos.TGA_IMPB_PROV_INC_NULL, Len.TGA_IMPB_PROV_INC_NULL);
    }

    public String getTgaImpbProvIncNullFormatted() {
        return Functions.padBlanks(getTgaImpbProvIncNull(), Len.TGA_IMPB_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_PROV_INC = 1;
        public static final int TGA_IMPB_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_PROV_INC = 8;
        public static final int TGA_IMPB_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
