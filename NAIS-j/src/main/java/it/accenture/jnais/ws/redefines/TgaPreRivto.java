package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-RIVTO<br>
 * Variable: TGA-PRE-RIVTO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPreRivto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPreRivto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_RIVTO;
    }

    public void setTgaPreRivto(AfDecimal tgaPreRivto) {
        writeDecimalAsPacked(Pos.TGA_PRE_RIVTO, tgaPreRivto.copy());
    }

    public void setTgaPreRivtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_RIVTO, Pos.TGA_PRE_RIVTO);
    }

    /**Original name: TGA-PRE-RIVTO<br>*/
    public AfDecimal getTgaPreRivto() {
        return readPackedAsDecimal(Pos.TGA_PRE_RIVTO, Len.Int.TGA_PRE_RIVTO, Len.Fract.TGA_PRE_RIVTO);
    }

    public byte[] getTgaPreRivtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_RIVTO, Pos.TGA_PRE_RIVTO);
        return buffer;
    }

    public void setTgaPreRivtoNull(String tgaPreRivtoNull) {
        writeString(Pos.TGA_PRE_RIVTO_NULL, tgaPreRivtoNull, Len.TGA_PRE_RIVTO_NULL);
    }

    /**Original name: TGA-PRE-RIVTO-NULL<br>*/
    public String getTgaPreRivtoNull() {
        return readString(Pos.TGA_PRE_RIVTO_NULL, Len.TGA_PRE_RIVTO_NULL);
    }

    public String getTgaPreRivtoNullFormatted() {
        return Functions.padBlanks(getTgaPreRivtoNull(), Len.TGA_PRE_RIVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_RIVTO = 1;
        public static final int TGA_PRE_RIVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_RIVTO = 8;
        public static final int TGA_PRE_RIVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_RIVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_RIVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
