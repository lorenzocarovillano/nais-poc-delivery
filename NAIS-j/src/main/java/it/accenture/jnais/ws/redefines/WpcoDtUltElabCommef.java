package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-COMMEF<br>
 * Variable: WPCO-DT-ULT-ELAB-COMMEF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabCommef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabCommef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_COMMEF;
    }

    public void setWpcoDtUltElabCommef(int wpcoDtUltElabCommef) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_COMMEF, wpcoDtUltElabCommef, Len.Int.WPCO_DT_ULT_ELAB_COMMEF);
    }

    public void setDpcoDtUltElabCommefFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_COMMEF, Pos.WPCO_DT_ULT_ELAB_COMMEF);
    }

    /**Original name: WPCO-DT-ULT-ELAB-COMMEF<br>*/
    public int getWpcoDtUltElabCommef() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_COMMEF, Len.Int.WPCO_DT_ULT_ELAB_COMMEF);
    }

    public byte[] getWpcoDtUltElabCommefAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_COMMEF, Pos.WPCO_DT_ULT_ELAB_COMMEF);
        return buffer;
    }

    public void setWpcoDtUltElabCommefNull(String wpcoDtUltElabCommefNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_COMMEF_NULL, wpcoDtUltElabCommefNull, Len.WPCO_DT_ULT_ELAB_COMMEF_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-COMMEF-NULL<br>*/
    public String getWpcoDtUltElabCommefNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_COMMEF_NULL, Len.WPCO_DT_ULT_ELAB_COMMEF_NULL);
    }

    public String getWpcoDtUltElabCommefNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabCommefNull(), Len.WPCO_DT_ULT_ELAB_COMMEF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_COMMEF = 1;
        public static final int WPCO_DT_ULT_ELAB_COMMEF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_COMMEF = 5;
        public static final int WPCO_DT_ULT_ELAB_COMMEF_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_COMMEF = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
