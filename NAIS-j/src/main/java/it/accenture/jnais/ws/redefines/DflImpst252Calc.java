package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-252-CALC<br>
 * Variable: DFL-IMPST-252-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpst252Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpst252Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST252_CALC;
    }

    public void setDflImpst252Calc(AfDecimal dflImpst252Calc) {
        writeDecimalAsPacked(Pos.DFL_IMPST252_CALC, dflImpst252Calc.copy());
    }

    public void setDflImpst252CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST252_CALC, Pos.DFL_IMPST252_CALC);
    }

    /**Original name: DFL-IMPST-252-CALC<br>*/
    public AfDecimal getDflImpst252Calc() {
        return readPackedAsDecimal(Pos.DFL_IMPST252_CALC, Len.Int.DFL_IMPST252_CALC, Len.Fract.DFL_IMPST252_CALC);
    }

    public byte[] getDflImpst252CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST252_CALC, Pos.DFL_IMPST252_CALC);
        return buffer;
    }

    public void setDflImpst252CalcNull(String dflImpst252CalcNull) {
        writeString(Pos.DFL_IMPST252_CALC_NULL, dflImpst252CalcNull, Len.DFL_IMPST252_CALC_NULL);
    }

    /**Original name: DFL-IMPST-252-CALC-NULL<br>*/
    public String getDflImpst252CalcNull() {
        return readString(Pos.DFL_IMPST252_CALC_NULL, Len.DFL_IMPST252_CALC_NULL);
    }

    public String getDflImpst252CalcNullFormatted() {
        return Functions.padBlanks(getDflImpst252CalcNull(), Len.DFL_IMPST252_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST252_CALC = 1;
        public static final int DFL_IMPST252_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST252_CALC = 8;
        public static final int DFL_IMPST252_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST252_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST252_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
