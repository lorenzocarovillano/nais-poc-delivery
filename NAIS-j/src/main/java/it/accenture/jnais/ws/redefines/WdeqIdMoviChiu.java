package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WDEQ-ID-MOVI-CHIU<br>
 * Variable: WDEQ-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdeqIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdeqIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDEQ_ID_MOVI_CHIU;
    }

    public void setWdeqIdMoviChiu(int wdeqIdMoviChiu) {
        writeIntAsPacked(Pos.WDEQ_ID_MOVI_CHIU, wdeqIdMoviChiu, Len.Int.WDEQ_ID_MOVI_CHIU);
    }

    public void setWdeqIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDEQ_ID_MOVI_CHIU, Pos.WDEQ_ID_MOVI_CHIU);
    }

    /**Original name: WDEQ-ID-MOVI-CHIU<br>*/
    public int getWdeqIdMoviChiu() {
        return readPackedAsInt(Pos.WDEQ_ID_MOVI_CHIU, Len.Int.WDEQ_ID_MOVI_CHIU);
    }

    public byte[] getWdeqIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDEQ_ID_MOVI_CHIU, Pos.WDEQ_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWdeqIdMoviChiuSpaces() {
        fill(Pos.WDEQ_ID_MOVI_CHIU, Len.WDEQ_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWdeqIdMoviChiuNull(String wdeqIdMoviChiuNull) {
        writeString(Pos.WDEQ_ID_MOVI_CHIU_NULL, wdeqIdMoviChiuNull, Len.WDEQ_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WDEQ-ID-MOVI-CHIU-NULL<br>*/
    public String getWdeqIdMoviChiuNull() {
        return readString(Pos.WDEQ_ID_MOVI_CHIU_NULL, Len.WDEQ_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDEQ_ID_MOVI_CHIU = 1;
        public static final int WDEQ_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDEQ_ID_MOVI_CHIU = 5;
        public static final int WDEQ_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDEQ_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
