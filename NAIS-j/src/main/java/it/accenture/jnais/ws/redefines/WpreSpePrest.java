package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPRE-SPE-PREST<br>
 * Variable: WPRE-SPE-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreSpePrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpreSpePrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_SPE_PREST;
    }

    public void setWpreSpePrest(AfDecimal wpreSpePrest) {
        writeDecimalAsPacked(Pos.WPRE_SPE_PREST, wpreSpePrest.copy());
    }

    public void setWpreSpePrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_SPE_PREST, Pos.WPRE_SPE_PREST);
    }

    /**Original name: WPRE-SPE-PREST<br>*/
    public AfDecimal getWpreSpePrest() {
        return readPackedAsDecimal(Pos.WPRE_SPE_PREST, Len.Int.WPRE_SPE_PREST, Len.Fract.WPRE_SPE_PREST);
    }

    public byte[] getWpreSpePrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_SPE_PREST, Pos.WPRE_SPE_PREST);
        return buffer;
    }

    public void initWpreSpePrestSpaces() {
        fill(Pos.WPRE_SPE_PREST, Len.WPRE_SPE_PREST, Types.SPACE_CHAR);
    }

    public void setWpreSpePrestNull(String wpreSpePrestNull) {
        writeString(Pos.WPRE_SPE_PREST_NULL, wpreSpePrestNull, Len.WPRE_SPE_PREST_NULL);
    }

    /**Original name: WPRE-SPE-PREST-NULL<br>*/
    public String getWpreSpePrestNull() {
        return readString(Pos.WPRE_SPE_PREST_NULL, Len.WPRE_SPE_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_SPE_PREST = 1;
        public static final int WPRE_SPE_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_SPE_PREST = 8;
        public static final int WPRE_SPE_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPRE_SPE_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_SPE_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
