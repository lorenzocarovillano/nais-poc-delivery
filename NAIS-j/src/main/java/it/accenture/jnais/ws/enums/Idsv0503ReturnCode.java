package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0503-RETURN-CODE<br>
 * Variable: IDSV0503-RETURN-CODE from copybook IDSV0503<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0503ReturnCode {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.IDSV0503_RETURN_CODE);
    public static final String SUCCESSFUL_RC = "00";
    public static final String GENERIC_ERROR = "KO";

    //==== METHODS ====
    public void setIdsv0503ReturnCode(String idsv0503ReturnCode) {
        this.value = Functions.subString(idsv0503ReturnCode, Len.IDSV0503_RETURN_CODE);
    }

    public String getIdsv0503ReturnCode() {
        return this.value;
    }

    public boolean isSuccessfulRc() {
        return value.equals(SUCCESSFUL_RC);
    }

    public void setSuccessfulRc() {
        value = SUCCESSFUL_RC;
    }

    public void setGenericError() {
        value = GENERIC_ERROR;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSV0503_RETURN_CODE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
