package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndAmmbFunzFunz;
import it.accenture.jnais.copy.Ldbv2441;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS2440<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs2440Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-AMMB-FUNZ-FUNZ
    private IndAmmbFunzFunz indAmmbFunzFunz = new IndAmmbFunzFunz();
    //Original name: LDBV2441
    private Ldbv2441 ldbv2441 = new Ldbv2441();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndAmmbFunzFunz getIndAmmbFunzFunz() {
        return indAmmbFunzFunz;
    }

    public Ldbv2441 getLdbv2441() {
        return ldbv2441;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
