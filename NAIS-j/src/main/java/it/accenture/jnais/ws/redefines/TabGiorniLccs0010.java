package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: TAB-GIORNI<br>
 * Variable: TAB-GIORNI from program LCCS0010<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TabGiorniLccs0010 extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int T_GG_MAXOCCURS = 12;

    //==== CONSTRUCTORS ====
    public TabGiorniLccs0010() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TAB_GIORNI;
    }

    @Override
    public void init() {
        int position = 1;
        writeString(position, "303030303030303030303030", Len.TAB_GIORNI);
    }

    /**Original name: T-GG<br>*/
    public short gettGg(int tGgIdx) {
        int position = Pos.tGg(tGgIdx - 1);
        return readNumDispUnsignedShort(position, Len.T_GG);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TAB_GIORNI = 1;
        public static final int FILLER_WS1 = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int tGg(int idx) {
            return FILLER_WS1 + idx * Len.T_GG;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int T_GG = 2;
        public static final int TAB_GIORNI = 24;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
