package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WOPZ-GAR-OPZIONE<br>
 * Variables: WOPZ-GAR-OPZIONE from copybook IVVC0217<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WopzGarOpzione {

    //==== PROPERTIES ====
    //Original name: WOPZ-GAR-OPZ-CODICE
    private String codice = DefaultValues.stringVal(Len.CODICE);
    //Original name: WOPZ-GAR-OPZ-DECORRENZA
    private String decorrenza = DefaultValues.stringVal(Len.DECORRENZA);
    //Original name: WOPZ-GAR-OPZ-VERSIONE
    private char versione = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setGarOpzioneBytes(byte[] buffer, int offset) {
        int position = offset;
        codice = MarshalByte.readString(buffer, position, Len.CODICE);
        position += Len.CODICE;
        decorrenza = MarshalByte.readString(buffer, position, Len.DECORRENZA);
        position += Len.DECORRENZA;
        versione = MarshalByte.readChar(buffer, position);
    }

    public byte[] getGarOpzioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codice, Len.CODICE);
        position += Len.CODICE;
        MarshalByte.writeString(buffer, position, decorrenza, Len.DECORRENZA);
        position += Len.DECORRENZA;
        MarshalByte.writeChar(buffer, position, versione);
        return buffer;
    }

    public void initGarOpzioneSpaces() {
        codice = "";
        decorrenza = "";
        versione = Types.SPACE_CHAR;
    }

    public void setCodice(String codice) {
        this.codice = Functions.subString(codice, Len.CODICE);
    }

    public String getCodice() {
        return this.codice;
    }

    public String getWopzGarOpzCodiceFormatted() {
        return Functions.padBlanks(getCodice(), Len.CODICE);
    }

    public void setDecorrenza(String decorrenza) {
        this.decorrenza = Functions.subString(decorrenza, Len.DECORRENZA);
    }

    public String getDecorrenza() {
        return this.decorrenza;
    }

    public String getWopzGarOpzDecorrenzaFormatted() {
        return Functions.padBlanks(getDecorrenza(), Len.DECORRENZA);
    }

    public void setVersione(char versione) {
        this.versione = versione;
    }

    public char getVersione() {
        return this.versione;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE = 12;
        public static final int DECORRENZA = 8;
        public static final int VERSIONE = 1;
        public static final int GAR_OPZIONE = CODICE + DECORRENZA + VERSIONE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
