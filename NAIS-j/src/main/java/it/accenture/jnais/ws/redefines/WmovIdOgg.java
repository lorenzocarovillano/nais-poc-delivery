package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WMOV-ID-OGG<br>
 * Variable: WMOV-ID-OGG from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmovIdOgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmovIdOgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMOV_ID_OGG;
    }

    public void setWmovIdOgg(int wmovIdOgg) {
        writeIntAsPacked(Pos.WMOV_ID_OGG, wmovIdOgg, Len.Int.WMOV_ID_OGG);
    }

    public void setWmovIdOggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMOV_ID_OGG, Pos.WMOV_ID_OGG);
    }

    /**Original name: WMOV-ID-OGG<br>*/
    public int getWmovIdOgg() {
        return readPackedAsInt(Pos.WMOV_ID_OGG, Len.Int.WMOV_ID_OGG);
    }

    public byte[] getWmovIdOggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMOV_ID_OGG, Pos.WMOV_ID_OGG);
        return buffer;
    }

    public void setWmovIdOggNull(String wmovIdOggNull) {
        writeString(Pos.WMOV_ID_OGG_NULL, wmovIdOggNull, Len.WMOV_ID_OGG_NULL);
    }

    /**Original name: WMOV-ID-OGG-NULL<br>*/
    public String getWmovIdOggNull() {
        return readString(Pos.WMOV_ID_OGG_NULL, Len.WMOV_ID_OGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMOV_ID_OGG = 1;
        public static final int WMOV_ID_OGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMOV_ID_OGG = 5;
        public static final int WMOV_ID_OGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMOV_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
