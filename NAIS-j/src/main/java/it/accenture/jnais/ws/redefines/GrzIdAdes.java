package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-ID-ADES<br>
 * Variable: GRZ-ID-ADES from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzIdAdes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzIdAdes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_ID_ADES;
    }

    public void setGrzIdAdes(int grzIdAdes) {
        writeIntAsPacked(Pos.GRZ_ID_ADES, grzIdAdes, Len.Int.GRZ_ID_ADES);
    }

    public void setGrzIdAdesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_ID_ADES, Pos.GRZ_ID_ADES);
    }

    /**Original name: GRZ-ID-ADES<br>*/
    public int getGrzIdAdes() {
        return readPackedAsInt(Pos.GRZ_ID_ADES, Len.Int.GRZ_ID_ADES);
    }

    public byte[] getGrzIdAdesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_ID_ADES, Pos.GRZ_ID_ADES);
        return buffer;
    }

    public void setGrzIdAdesNull(String grzIdAdesNull) {
        writeString(Pos.GRZ_ID_ADES_NULL, grzIdAdesNull, Len.GRZ_ID_ADES_NULL);
    }

    /**Original name: GRZ-ID-ADES-NULL<br>*/
    public String getGrzIdAdesNull() {
        return readString(Pos.GRZ_ID_ADES_NULL, Len.GRZ_ID_ADES_NULL);
    }

    public String getGrzIdAdesNullFormatted() {
        return Functions.padBlanks(getGrzIdAdesNull(), Len.GRZ_ID_ADES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_ID_ADES = 1;
        public static final int GRZ_ID_ADES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_ID_ADES = 5;
        public static final int GRZ_ID_ADES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
