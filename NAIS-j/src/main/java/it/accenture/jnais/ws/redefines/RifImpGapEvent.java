package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIF-IMP-GAP-EVENT<br>
 * Variable: RIF-IMP-GAP-EVENT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RifImpGapEvent extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RifImpGapEvent() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIF_IMP_GAP_EVENT;
    }

    public void setRifImpGapEvent(AfDecimal rifImpGapEvent) {
        writeDecimalAsPacked(Pos.RIF_IMP_GAP_EVENT, rifImpGapEvent.copy());
    }

    public void setRifImpGapEventFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIF_IMP_GAP_EVENT, Pos.RIF_IMP_GAP_EVENT);
    }

    /**Original name: RIF-IMP-GAP-EVENT<br>*/
    public AfDecimal getRifImpGapEvent() {
        return readPackedAsDecimal(Pos.RIF_IMP_GAP_EVENT, Len.Int.RIF_IMP_GAP_EVENT, Len.Fract.RIF_IMP_GAP_EVENT);
    }

    public byte[] getRifImpGapEventAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIF_IMP_GAP_EVENT, Pos.RIF_IMP_GAP_EVENT);
        return buffer;
    }

    public void setRifImpGapEventNull(String rifImpGapEventNull) {
        writeString(Pos.RIF_IMP_GAP_EVENT_NULL, rifImpGapEventNull, Len.RIF_IMP_GAP_EVENT_NULL);
    }

    /**Original name: RIF-IMP-GAP-EVENT-NULL<br>*/
    public String getRifImpGapEventNull() {
        return readString(Pos.RIF_IMP_GAP_EVENT_NULL, Len.RIF_IMP_GAP_EVENT_NULL);
    }

    public String getRifImpGapEventNullFormatted() {
        return Functions.padBlanks(getRifImpGapEventNull(), Len.RIF_IMP_GAP_EVENT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIF_IMP_GAP_EVENT = 1;
        public static final int RIF_IMP_GAP_EVENT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIF_IMP_GAP_EVENT = 8;
        public static final int RIF_IMP_GAP_EVENT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIF_IMP_GAP_EVENT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RIF_IMP_GAP_EVENT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
