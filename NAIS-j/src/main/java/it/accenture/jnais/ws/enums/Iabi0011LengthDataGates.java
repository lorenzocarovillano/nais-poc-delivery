package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABI0011-LENGTH-DATA-GATES<br>
 * Variable: IABI0011-LENGTH-DATA-GATES from copybook IABI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabi0011LengthDataGates {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.LENGTH_DATA_GATES);
    public static final String DATA1K = "1K";
    public static final String DATA2K = "2K";
    public static final String DATA3K = "3K";
    public static final String DATA10K = "10K";

    //==== METHODS ====
    public void setLengthDataGates(String lengthDataGates) {
        this.value = Functions.subString(lengthDataGates, Len.LENGTH_DATA_GATES);
    }

    public String getLengthDataGates() {
        return this.value;
    }

    public String getLengthDataGatesFormatted() {
        return Functions.padBlanks(getLengthDataGates(), Len.LENGTH_DATA_GATES);
    }

    public boolean isData1k() {
        return value.equals(DATA1K);
    }

    public boolean isData2k() {
        return value.equals(DATA2K);
    }

    public boolean isData3k() {
        return value.equals(DATA3K);
    }

    public boolean isData10k() {
        return value.equals(DATA10K);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LENGTH_DATA_GATES = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
