package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-EMES-I<br>
 * Variable: PCO-DT-ULT-BOLL-EMES-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollEmesI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollEmesI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_EMES_I;
    }

    public void setPcoDtUltBollEmesI(int pcoDtUltBollEmesI) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_EMES_I, pcoDtUltBollEmesI, Len.Int.PCO_DT_ULT_BOLL_EMES_I);
    }

    public void setPcoDtUltBollEmesIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_EMES_I, Pos.PCO_DT_ULT_BOLL_EMES_I);
    }

    /**Original name: PCO-DT-ULT-BOLL-EMES-I<br>*/
    public int getPcoDtUltBollEmesI() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_EMES_I, Len.Int.PCO_DT_ULT_BOLL_EMES_I);
    }

    public byte[] getPcoDtUltBollEmesIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_EMES_I, Pos.PCO_DT_ULT_BOLL_EMES_I);
        return buffer;
    }

    public void initPcoDtUltBollEmesIHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_EMES_I, Len.PCO_DT_ULT_BOLL_EMES_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollEmesINull(String pcoDtUltBollEmesINull) {
        writeString(Pos.PCO_DT_ULT_BOLL_EMES_I_NULL, pcoDtUltBollEmesINull, Len.PCO_DT_ULT_BOLL_EMES_I_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-EMES-I-NULL<br>*/
    public String getPcoDtUltBollEmesINull() {
        return readString(Pos.PCO_DT_ULT_BOLL_EMES_I_NULL, Len.PCO_DT_ULT_BOLL_EMES_I_NULL);
    }

    public String getPcoDtUltBollEmesINullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollEmesINull(), Len.PCO_DT_ULT_BOLL_EMES_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_EMES_I = 1;
        public static final int PCO_DT_ULT_BOLL_EMES_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_EMES_I = 5;
        public static final int PCO_DT_ULT_BOLL_EMES_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_EMES_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
