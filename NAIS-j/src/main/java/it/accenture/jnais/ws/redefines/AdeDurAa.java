package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-DUR-AA<br>
 * Variable: ADE-DUR-AA from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeDurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeDurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_DUR_AA;
    }

    public void setAdeDurAa(int adeDurAa) {
        writeIntAsPacked(Pos.ADE_DUR_AA, adeDurAa, Len.Int.ADE_DUR_AA);
    }

    public void setAdeDurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_DUR_AA, Pos.ADE_DUR_AA);
    }

    /**Original name: ADE-DUR-AA<br>*/
    public int getAdeDurAa() {
        return readPackedAsInt(Pos.ADE_DUR_AA, Len.Int.ADE_DUR_AA);
    }

    public byte[] getAdeDurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_DUR_AA, Pos.ADE_DUR_AA);
        return buffer;
    }

    public void setAdeDurAaNull(String adeDurAaNull) {
        writeString(Pos.ADE_DUR_AA_NULL, adeDurAaNull, Len.ADE_DUR_AA_NULL);
    }

    /**Original name: ADE-DUR-AA-NULL<br>*/
    public String getAdeDurAaNull() {
        return readString(Pos.ADE_DUR_AA_NULL, Len.ADE_DUR_AA_NULL);
    }

    public String getAdeDurAaNullFormatted() {
        return Functions.padBlanks(getAdeDurAaNull(), Len.ADE_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_DUR_AA = 1;
        public static final int ADE_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_DUR_AA = 3;
        public static final int ADE_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
