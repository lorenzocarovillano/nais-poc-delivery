package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Idsv0503ValGenerico;

/**Original name: IDSV0503-TAB-VARIABILI<br>
 * Variables: IDSV0503-TAB-VARIABILI from copybook IDSV0503<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idsv0503TabVariabili {

    //==== PROPERTIES ====
    //Original name: IDSV0503-COD-VARIABILE
    private String idsv0503CodVariabile = DefaultValues.stringVal(Len.IDSV0503_COD_VARIABILE);
    //Original name: IDSV0503-TP-DATO
    private char idsv0503TpDato = DefaultValues.CHAR_VAL;
    //Original name: IDSV0503-VAL-GENERICO
    private Idsv0503ValGenerico idsv0503ValGenerico = new Idsv0503ValGenerico();

    //==== METHODS ====
    public void setIdsv0503TabVariabiliBytes(byte[] buffer, int offset) {
        int position = offset;
        setIdsv0503AreaVariabileBytes(buffer, position);
    }

    public void initIdsv0503TabVariabiliSpaces() {
        initIdsv0503AreaVariabileSpaces();
    }

    public void setIdsv0503AreaVariabileBytes(byte[] buffer, int offset) {
        int position = offset;
        idsv0503CodVariabile = MarshalByte.readString(buffer, position, Len.IDSV0503_COD_VARIABILE);
        position += Len.IDSV0503_COD_VARIABILE;
        idsv0503TpDato = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        idsv0503ValGenerico.setIdsv0503ValGenericoFromBuffer(buffer, position);
    }

    public void initIdsv0503AreaVariabileSpaces() {
        idsv0503CodVariabile = "";
        idsv0503TpDato = Types.SPACE_CHAR;
        idsv0503ValGenerico.initIdsv0503ValGenericoSpaces();
    }

    public void setIdsv0503CodVariabile(String idsv0503CodVariabile) {
        this.idsv0503CodVariabile = Functions.subString(idsv0503CodVariabile, Len.IDSV0503_COD_VARIABILE);
    }

    public String getIdsv0503CodVariabile() {
        return this.idsv0503CodVariabile;
    }

    public String getIdsv0503CodVariabileFormatted() {
        return Functions.padBlanks(getIdsv0503CodVariabile(), Len.IDSV0503_COD_VARIABILE);
    }

    public void setIdsv0503TpDato(char idsv0503TpDato) {
        this.idsv0503TpDato = idsv0503TpDato;
    }

    public char getIdsv0503TpDato() {
        return this.idsv0503TpDato;
    }

    public Idsv0503ValGenerico getIdsv0503ValGenerico() {
        return idsv0503ValGenerico;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSV0503_COD_VARIABILE = 12;
        public static final int IDSV0503_TP_DATO = 1;
        public static final int IDSV0503_AREA_VARIABILE = IDSV0503_COD_VARIABILE + IDSV0503_TP_DATO + Idsv0503ValGenerico.Len.IDSV0503_VAL_GENERICO;
        public static final int IDSV0503_TAB_VARIABILI = IDSV0503_AREA_VARIABILE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
