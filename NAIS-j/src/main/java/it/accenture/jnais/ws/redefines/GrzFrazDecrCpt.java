package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-FRAZ-DECR-CPT<br>
 * Variable: GRZ-FRAZ-DECR-CPT from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzFrazDecrCpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzFrazDecrCpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_FRAZ_DECR_CPT;
    }

    public void setGrzFrazDecrCpt(int grzFrazDecrCpt) {
        writeIntAsPacked(Pos.GRZ_FRAZ_DECR_CPT, grzFrazDecrCpt, Len.Int.GRZ_FRAZ_DECR_CPT);
    }

    public void setGrzFrazDecrCptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_FRAZ_DECR_CPT, Pos.GRZ_FRAZ_DECR_CPT);
    }

    /**Original name: GRZ-FRAZ-DECR-CPT<br>*/
    public int getGrzFrazDecrCpt() {
        return readPackedAsInt(Pos.GRZ_FRAZ_DECR_CPT, Len.Int.GRZ_FRAZ_DECR_CPT);
    }

    public byte[] getGrzFrazDecrCptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_FRAZ_DECR_CPT, Pos.GRZ_FRAZ_DECR_CPT);
        return buffer;
    }

    public void setGrzFrazDecrCptNull(String grzFrazDecrCptNull) {
        writeString(Pos.GRZ_FRAZ_DECR_CPT_NULL, grzFrazDecrCptNull, Len.GRZ_FRAZ_DECR_CPT_NULL);
    }

    /**Original name: GRZ-FRAZ-DECR-CPT-NULL<br>*/
    public String getGrzFrazDecrCptNull() {
        return readString(Pos.GRZ_FRAZ_DECR_CPT_NULL, Len.GRZ_FRAZ_DECR_CPT_NULL);
    }

    public String getGrzFrazDecrCptNullFormatted() {
        return Functions.padBlanks(getGrzFrazDecrCptNull(), Len.GRZ_FRAZ_DECR_CPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_FRAZ_DECR_CPT = 1;
        public static final int GRZ_FRAZ_DECR_CPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_FRAZ_DECR_CPT = 3;
        public static final int GRZ_FRAZ_DECR_CPT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_FRAZ_DECR_CPT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
