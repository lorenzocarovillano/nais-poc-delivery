package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-IMP-REC-RIT-ACC<br>
 * Variable: WADE-IMP-REC-RIT-ACC from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeImpRecRitAcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeImpRecRitAcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_IMP_REC_RIT_ACC;
    }

    public void setWadeImpRecRitAcc(AfDecimal wadeImpRecRitAcc) {
        writeDecimalAsPacked(Pos.WADE_IMP_REC_RIT_ACC, wadeImpRecRitAcc.copy());
    }

    public void setWadeImpRecRitAccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_IMP_REC_RIT_ACC, Pos.WADE_IMP_REC_RIT_ACC);
    }

    /**Original name: WADE-IMP-REC-RIT-ACC<br>*/
    public AfDecimal getWadeImpRecRitAcc() {
        return readPackedAsDecimal(Pos.WADE_IMP_REC_RIT_ACC, Len.Int.WADE_IMP_REC_RIT_ACC, Len.Fract.WADE_IMP_REC_RIT_ACC);
    }

    public byte[] getWadeImpRecRitAccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_IMP_REC_RIT_ACC, Pos.WADE_IMP_REC_RIT_ACC);
        return buffer;
    }

    public void initWadeImpRecRitAccSpaces() {
        fill(Pos.WADE_IMP_REC_RIT_ACC, Len.WADE_IMP_REC_RIT_ACC, Types.SPACE_CHAR);
    }

    public void setWadeImpRecRitAccNull(String wadeImpRecRitAccNull) {
        writeString(Pos.WADE_IMP_REC_RIT_ACC_NULL, wadeImpRecRitAccNull, Len.WADE_IMP_REC_RIT_ACC_NULL);
    }

    /**Original name: WADE-IMP-REC-RIT-ACC-NULL<br>*/
    public String getWadeImpRecRitAccNull() {
        return readString(Pos.WADE_IMP_REC_RIT_ACC_NULL, Len.WADE_IMP_REC_RIT_ACC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_IMP_REC_RIT_ACC = 1;
        public static final int WADE_IMP_REC_RIT_ACC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_IMP_REC_RIT_ACC = 8;
        public static final int WADE_IMP_REC_RIT_ACC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_IMP_REC_RIT_ACC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_IMP_REC_RIT_ACC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
