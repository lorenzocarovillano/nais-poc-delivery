package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: C214-MODALITA-ID-TGA<br>
 * Variable: C214-MODALITA-ID-TGA from copybook IVVC0213<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class C214ModalitaIdTga {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char FITTZIO = 'F';
    public static final char AUTENTICO = 'A';

    //==== METHODS ====
    public void setModalitaIdTga(char modalitaIdTga) {
        this.value = modalitaIdTga;
    }

    public char getModalitaIdTga() {
        return this.value;
    }

    public void setFittzio() {
        value = FITTZIO;
    }

    public boolean isIvvc0213IdTgaAutentico() {
        return value == AUTENTICO;
    }

    public void setAutentico() {
        value = AUTENTICO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MODALITA_ID_TGA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
