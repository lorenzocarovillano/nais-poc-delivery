package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-ALIQ-ACQ<br>
 * Variable: WPAG-ALIQ-ACQ from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagAliqAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagAliqAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_ALIQ_ACQ;
    }

    public void setWpagAliqAcq(AfDecimal wpagAliqAcq) {
        writeDecimalAsPacked(Pos.WPAG_ALIQ_ACQ, wpagAliqAcq.copy());
    }

    public void setWpagAliqAcqFormatted(String wpagAliqAcq) {
        setWpagAliqAcq(PicParser.display(new PicParams("S9(5)V9(9)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_ALIQ_ACQ + Len.Fract.WPAG_ALIQ_ACQ, Len.Fract.WPAG_ALIQ_ACQ, wpagAliqAcq));
    }

    public void setWpagAliqAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_ALIQ_ACQ, Pos.WPAG_ALIQ_ACQ);
    }

    /**Original name: WPAG-ALIQ-ACQ<br>*/
    public AfDecimal getWpagAliqAcq() {
        return readPackedAsDecimal(Pos.WPAG_ALIQ_ACQ, Len.Int.WPAG_ALIQ_ACQ, Len.Fract.WPAG_ALIQ_ACQ);
    }

    public byte[] getWpagAliqAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_ALIQ_ACQ, Pos.WPAG_ALIQ_ACQ);
        return buffer;
    }

    public void initWpagAliqAcqSpaces() {
        fill(Pos.WPAG_ALIQ_ACQ, Len.WPAG_ALIQ_ACQ, Types.SPACE_CHAR);
    }

    public void setWpagAliqAcqNull(String wpagAliqAcqNull) {
        writeString(Pos.WPAG_ALIQ_ACQ_NULL, wpagAliqAcqNull, Len.WPAG_ALIQ_ACQ_NULL);
    }

    /**Original name: WPAG-ALIQ-ACQ-NULL<br>*/
    public String getWpagAliqAcqNull() {
        return readString(Pos.WPAG_ALIQ_ACQ_NULL, Len.WPAG_ALIQ_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_ALIQ_ACQ = 1;
        public static final int WPAG_ALIQ_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_ALIQ_ACQ = 8;
        public static final int WPAG_ALIQ_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_ALIQ_ACQ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_ALIQ_ACQ = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
