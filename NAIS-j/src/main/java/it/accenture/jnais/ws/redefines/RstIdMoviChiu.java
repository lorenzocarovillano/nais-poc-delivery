package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-ID-MOVI-CHIU<br>
 * Variable: RST-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_ID_MOVI_CHIU;
    }

    public void setRstIdMoviChiu(int rstIdMoviChiu) {
        writeIntAsPacked(Pos.RST_ID_MOVI_CHIU, rstIdMoviChiu, Len.Int.RST_ID_MOVI_CHIU);
    }

    public void setRstIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_ID_MOVI_CHIU, Pos.RST_ID_MOVI_CHIU);
    }

    /**Original name: RST-ID-MOVI-CHIU<br>*/
    public int getRstIdMoviChiu() {
        return readPackedAsInt(Pos.RST_ID_MOVI_CHIU, Len.Int.RST_ID_MOVI_CHIU);
    }

    public byte[] getRstIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_ID_MOVI_CHIU, Pos.RST_ID_MOVI_CHIU);
        return buffer;
    }

    public void setRstIdMoviChiuNull(String rstIdMoviChiuNull) {
        writeString(Pos.RST_ID_MOVI_CHIU_NULL, rstIdMoviChiuNull, Len.RST_ID_MOVI_CHIU_NULL);
    }

    /**Original name: RST-ID-MOVI-CHIU-NULL<br>*/
    public String getRstIdMoviChiuNull() {
        return readString(Pos.RST_ID_MOVI_CHIU_NULL, Len.RST_ID_MOVI_CHIU_NULL);
    }

    public String getRstIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getRstIdMoviChiuNull(), Len.RST_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_ID_MOVI_CHIU = 1;
        public static final int RST_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_ID_MOVI_CHIU = 5;
        public static final int RST_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
