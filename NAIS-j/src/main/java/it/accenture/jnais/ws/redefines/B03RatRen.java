package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-RAT-REN<br>
 * Variable: B03-RAT-REN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03RatRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03RatRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_RAT_REN;
    }

    public void setB03RatRen(AfDecimal b03RatRen) {
        writeDecimalAsPacked(Pos.B03_RAT_REN, b03RatRen.copy());
    }

    public void setB03RatRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_RAT_REN, Pos.B03_RAT_REN);
    }

    /**Original name: B03-RAT-REN<br>*/
    public AfDecimal getB03RatRen() {
        return readPackedAsDecimal(Pos.B03_RAT_REN, Len.Int.B03_RAT_REN, Len.Fract.B03_RAT_REN);
    }

    public byte[] getB03RatRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_RAT_REN, Pos.B03_RAT_REN);
        return buffer;
    }

    public void setB03RatRenNull(String b03RatRenNull) {
        writeString(Pos.B03_RAT_REN_NULL, b03RatRenNull, Len.B03_RAT_REN_NULL);
    }

    /**Original name: B03-RAT-REN-NULL<br>*/
    public String getB03RatRenNull() {
        return readString(Pos.B03_RAT_REN_NULL, Len.B03_RAT_REN_NULL);
    }

    public String getB03RatRenNullFormatted() {
        return Functions.padBlanks(getB03RatRenNull(), Len.B03_RAT_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_RAT_REN = 1;
        public static final int B03_RAT_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_RAT_REN = 8;
        public static final int B03_RAT_REN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_RAT_REN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_RAT_REN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
