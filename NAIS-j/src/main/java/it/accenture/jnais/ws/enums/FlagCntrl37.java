package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-CNTRL-37<br>
 * Variable: FLAG-CNTRL-37 from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCntrl37 {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCntrl37(char flagCntrl37) {
        this.value = flagCntrl37;
    }

    public char getFlagCntrl37() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
