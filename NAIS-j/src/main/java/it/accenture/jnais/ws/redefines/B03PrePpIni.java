package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRE-PP-INI<br>
 * Variable: B03-PRE-PP-INI from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PrePpIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PrePpIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRE_PP_INI;
    }

    public void setB03PrePpIni(AfDecimal b03PrePpIni) {
        writeDecimalAsPacked(Pos.B03_PRE_PP_INI, b03PrePpIni.copy());
    }

    public void setB03PrePpIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRE_PP_INI, Pos.B03_PRE_PP_INI);
    }

    /**Original name: B03-PRE-PP-INI<br>*/
    public AfDecimal getB03PrePpIni() {
        return readPackedAsDecimal(Pos.B03_PRE_PP_INI, Len.Int.B03_PRE_PP_INI, Len.Fract.B03_PRE_PP_INI);
    }

    public byte[] getB03PrePpIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRE_PP_INI, Pos.B03_PRE_PP_INI);
        return buffer;
    }

    public void setB03PrePpIniNull(String b03PrePpIniNull) {
        writeString(Pos.B03_PRE_PP_INI_NULL, b03PrePpIniNull, Len.B03_PRE_PP_INI_NULL);
    }

    /**Original name: B03-PRE-PP-INI-NULL<br>*/
    public String getB03PrePpIniNull() {
        return readString(Pos.B03_PRE_PP_INI_NULL, Len.B03_PRE_PP_INI_NULL);
    }

    public String getB03PrePpIniNullFormatted() {
        return Functions.padBlanks(getB03PrePpIniNull(), Len.B03_PRE_PP_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRE_PP_INI = 1;
        public static final int B03_PRE_PP_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRE_PP_INI = 8;
        public static final int B03_PRE_PP_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRE_PP_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRE_PP_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
