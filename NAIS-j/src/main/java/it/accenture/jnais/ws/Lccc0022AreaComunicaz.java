package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.Lccc0022TpStatoBlocco;
import it.accenture.jnais.ws.occurs.Lccc0022TabOggBlocco;

/**Original name: LCCC0022-AREA-COMUNICAZ<br>
 * Variable: LCCC0022-AREA-COMUNICAZ from program LCCS0022<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Lccc0022AreaComunicaz extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_OGG_BLOCCO_MAXOCCURS = 30;
    //Original name: LCCC0022-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LCCC0022-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LCCC0022-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LCCC0022-TP-STATO-BLOCCO
    private Lccc0022TpStatoBlocco tpStatoBlocco = new Lccc0022TpStatoBlocco();
    //Original name: LCCC0022-FL-PRES-GRAV-BLOC
    private char flPresGravBloc = DefaultValues.CHAR_VAL;
    //Original name: LCCC0022-ELE-MAX-OGG-BLOC
    private short eleMaxOggBloc = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCC0022-TAB-OGG-BLOCCO
    private Lccc0022TabOggBlocco[] tabOggBlocco = new Lccc0022TabOggBlocco[TAB_OGG_BLOCCO_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Lccc0022AreaComunicaz() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0022_AREA_COMUNICAZ;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLccc0022AreaComunicazBytes(buf);
    }

    public void init() {
        for (int tabOggBloccoIdx = 1; tabOggBloccoIdx <= TAB_OGG_BLOCCO_MAXOCCURS; tabOggBloccoIdx++) {
            tabOggBlocco[tabOggBloccoIdx - 1] = new Lccc0022TabOggBlocco();
        }
    }

    public String getAreaIoLccs0022Formatted() {
        return MarshalByteExt.bufferToStr(getLccc0022AreaComunicazBytes());
    }

    public void setLccc0022AreaComunicazBytes(byte[] buffer) {
        setLccc0022AreaComunicazBytes(buffer, 1);
    }

    public byte[] getLccc0022AreaComunicazBytes() {
        byte[] buffer = new byte[Len.LCCC0022_AREA_COMUNICAZ];
        return getLccc0022AreaComunicazBytes(buffer, 1);
    }

    public void setLccc0022AreaComunicazBytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
    }

    public byte[] getLccc0022AreaComunicazBytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpStatoBlocco.setTpStatoBlocco(MarshalByte.readString(buffer, position, Lccc0022TpStatoBlocco.Len.TP_STATO_BLOCCO));
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tpStatoBlocco.getTpStatoBlocco(), Lccc0022TpStatoBlocco.Len.TP_STATO_BLOCCO);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public String getTpOggFormatted() {
        return Functions.padBlanks(getTpOgg(), Len.TP_OGG);
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        flPresGravBloc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        eleMaxOggBloc = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_OGG_BLOCCO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabOggBlocco[idx - 1].setTabOggBloccoBytes(buffer, position);
                position += Lccc0022TabOggBlocco.Len.TAB_OGG_BLOCCO;
            }
            else {
                tabOggBlocco[idx - 1].initTabOggBloccoSpaces();
                position += Lccc0022TabOggBlocco.Len.TAB_OGG_BLOCCO;
            }
        }
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, flPresGravBloc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeBinaryShort(buffer, position, eleMaxOggBloc);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_OGG_BLOCCO_MAXOCCURS; idx++) {
            tabOggBlocco[idx - 1].getTabOggBloccoBytes(buffer, position);
            position += Lccc0022TabOggBlocco.Len.TAB_OGG_BLOCCO;
        }
        return buffer;
    }

    public void setFlPresGravBloc(char flPresGravBloc) {
        this.flPresGravBloc = flPresGravBloc;
    }

    public void setFlPresGravBlocFormatted(String flPresGravBloc) {
        setFlPresGravBloc(Functions.charAt(flPresGravBloc, Types.CHAR_SIZE));
    }

    public char getFlPresGravBloc() {
        return this.flPresGravBloc;
    }

    public void setEleMaxOggBloc(short eleMaxOggBloc) {
        this.eleMaxOggBloc = eleMaxOggBloc;
    }

    public short getEleMaxOggBloc() {
        return this.eleMaxOggBloc;
    }

    public Lccc0022TabOggBlocco getTabOggBlocco(int idx) {
        return tabOggBlocco[idx - 1];
    }

    public Lccc0022TpStatoBlocco getTpStatoBlocco() {
        return tpStatoBlocco;
    }

    @Override
    public byte[] serialize() {
        return getLccc0022AreaComunicazBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POLI = 5;
        public static final int ID_ADES = 5;
        public static final int TP_OGG = 2;
        public static final int DATI_INPUT = ID_POLI + ID_ADES + TP_OGG + Lccc0022TpStatoBlocco.Len.TP_STATO_BLOCCO;
        public static final int FL_PRES_GRAV_BLOC = 1;
        public static final int ELE_MAX_OGG_BLOC = 2;
        public static final int DATI_OUTPUT = FL_PRES_GRAV_BLOC + ELE_MAX_OGG_BLOC + Lccc0022AreaComunicaz.TAB_OGG_BLOCCO_MAXOCCURS * Lccc0022TabOggBlocco.Len.TAB_OGG_BLOCCO;
        public static final int LCCC0022_AREA_COMUNICAZ = DATI_INPUT + DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
