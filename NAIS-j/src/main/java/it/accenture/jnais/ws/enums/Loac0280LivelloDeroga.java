package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LOAC0280-LIVELLO-DEROGA<br>
 * Variable: LOAC0280-LIVELLO-DEROGA from copybook LOAC0280<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Loac0280LivelloDeroga {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.LIVELLO_DEROGA);
    public static final String NULL_FLD = "";
    public static final String LIV_POLI = "POLIZZA";
    public static final String LIV_ADES = "ADESIONE";

    //==== METHODS ====
    public void setLivelloDeroga(String livelloDeroga) {
        this.value = Functions.subString(livelloDeroga, Len.LIVELLO_DEROGA);
    }

    public String getLivelloDeroga() {
        return this.value;
    }

    public void setNullFld() {
        value = NULL_FLD;
    }

    public boolean isLoac0280LivPoli() {
        return value.equals(LIV_POLI);
    }

    public void setLivPoli() {
        value = LIV_POLI;
    }

    public void setLivAdes() {
        value = LIV_ADES;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LIVELLO_DEROGA = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
