package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRRE-ID-OGG<br>
 * Variable: WRRE-ID-OGG from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrreIdOgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrreIdOgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRRE_ID_OGG;
    }

    public void setWrreIdOgg(int wrreIdOgg) {
        writeIntAsPacked(Pos.WRRE_ID_OGG, wrreIdOgg, Len.Int.WRRE_ID_OGG);
    }

    public void setWrreIdOggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRRE_ID_OGG, Pos.WRRE_ID_OGG);
    }

    /**Original name: WRRE-ID-OGG<br>*/
    public int getWrreIdOgg() {
        return readPackedAsInt(Pos.WRRE_ID_OGG, Len.Int.WRRE_ID_OGG);
    }

    public byte[] getWrreIdOggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRRE_ID_OGG, Pos.WRRE_ID_OGG);
        return buffer;
    }

    public void initWrreIdOggSpaces() {
        fill(Pos.WRRE_ID_OGG, Len.WRRE_ID_OGG, Types.SPACE_CHAR);
    }

    public void setWrreIdOggNull(String wrreIdOggNull) {
        writeString(Pos.WRRE_ID_OGG_NULL, wrreIdOggNull, Len.WRRE_ID_OGG_NULL);
    }

    /**Original name: WRRE-ID-OGG-NULL<br>*/
    public String getWrreIdOggNull() {
        return readString(Pos.WRRE_ID_OGG_NULL, Len.WRRE_ID_OGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRRE_ID_OGG = 1;
        public static final int WRRE_ID_OGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRRE_ID_OGG = 5;
        public static final int WRRE_ID_OGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRRE_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
