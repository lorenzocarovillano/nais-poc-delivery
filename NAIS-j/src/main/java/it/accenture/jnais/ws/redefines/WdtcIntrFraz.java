package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-INTR-FRAZ<br>
 * Variable: WDTC-INTR-FRAZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcIntrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcIntrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_INTR_FRAZ;
    }

    public void setWdtcIntrFraz(AfDecimal wdtcIntrFraz) {
        writeDecimalAsPacked(Pos.WDTC_INTR_FRAZ, wdtcIntrFraz.copy());
    }

    public void setWdtcIntrFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_INTR_FRAZ, Pos.WDTC_INTR_FRAZ);
    }

    /**Original name: WDTC-INTR-FRAZ<br>*/
    public AfDecimal getWdtcIntrFraz() {
        return readPackedAsDecimal(Pos.WDTC_INTR_FRAZ, Len.Int.WDTC_INTR_FRAZ, Len.Fract.WDTC_INTR_FRAZ);
    }

    public byte[] getWdtcIntrFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_INTR_FRAZ, Pos.WDTC_INTR_FRAZ);
        return buffer;
    }

    public void initWdtcIntrFrazSpaces() {
        fill(Pos.WDTC_INTR_FRAZ, Len.WDTC_INTR_FRAZ, Types.SPACE_CHAR);
    }

    public void setWdtcIntrFrazNull(String wdtcIntrFrazNull) {
        writeString(Pos.WDTC_INTR_FRAZ_NULL, wdtcIntrFrazNull, Len.WDTC_INTR_FRAZ_NULL);
    }

    /**Original name: WDTC-INTR-FRAZ-NULL<br>*/
    public String getWdtcIntrFrazNull() {
        return readString(Pos.WDTC_INTR_FRAZ_NULL, Len.WDTC_INTR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_INTR_FRAZ = 1;
        public static final int WDTC_INTR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_INTR_FRAZ = 8;
        public static final int WDTC_INTR_FRAZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_INTR_FRAZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
