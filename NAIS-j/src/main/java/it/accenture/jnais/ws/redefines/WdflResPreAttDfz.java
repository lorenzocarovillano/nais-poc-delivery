package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RES-PRE-ATT-DFZ<br>
 * Variable: WDFL-RES-PRE-ATT-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflResPreAttDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflResPreAttDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RES_PRE_ATT_DFZ;
    }

    public void setWdflResPreAttDfz(AfDecimal wdflResPreAttDfz) {
        writeDecimalAsPacked(Pos.WDFL_RES_PRE_ATT_DFZ, wdflResPreAttDfz.copy());
    }

    public void setWdflResPreAttDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RES_PRE_ATT_DFZ, Pos.WDFL_RES_PRE_ATT_DFZ);
    }

    /**Original name: WDFL-RES-PRE-ATT-DFZ<br>*/
    public AfDecimal getWdflResPreAttDfz() {
        return readPackedAsDecimal(Pos.WDFL_RES_PRE_ATT_DFZ, Len.Int.WDFL_RES_PRE_ATT_DFZ, Len.Fract.WDFL_RES_PRE_ATT_DFZ);
    }

    public byte[] getWdflResPreAttDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RES_PRE_ATT_DFZ, Pos.WDFL_RES_PRE_ATT_DFZ);
        return buffer;
    }

    public void setWdflResPreAttDfzNull(String wdflResPreAttDfzNull) {
        writeString(Pos.WDFL_RES_PRE_ATT_DFZ_NULL, wdflResPreAttDfzNull, Len.WDFL_RES_PRE_ATT_DFZ_NULL);
    }

    /**Original name: WDFL-RES-PRE-ATT-DFZ-NULL<br>*/
    public String getWdflResPreAttDfzNull() {
        return readString(Pos.WDFL_RES_PRE_ATT_DFZ_NULL, Len.WDFL_RES_PRE_ATT_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRE_ATT_DFZ = 1;
        public static final int WDFL_RES_PRE_ATT_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRE_ATT_DFZ = 8;
        public static final int WDFL_RES_PRE_ATT_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRE_ATT_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRE_ATT_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
