package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-MAT<br>
 * Variable: WRST-RIS-MAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisMat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisMat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_MAT;
    }

    public void setWrstRisMat(AfDecimal wrstRisMat) {
        writeDecimalAsPacked(Pos.WRST_RIS_MAT, wrstRisMat.copy());
    }

    public void setWrstRisMatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_MAT, Pos.WRST_RIS_MAT);
    }

    /**Original name: WRST-RIS-MAT<br>*/
    public AfDecimal getWrstRisMat() {
        return readPackedAsDecimal(Pos.WRST_RIS_MAT, Len.Int.WRST_RIS_MAT, Len.Fract.WRST_RIS_MAT);
    }

    public byte[] getWrstRisMatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_MAT, Pos.WRST_RIS_MAT);
        return buffer;
    }

    public void initWrstRisMatSpaces() {
        fill(Pos.WRST_RIS_MAT, Len.WRST_RIS_MAT, Types.SPACE_CHAR);
    }

    public void setWrstRisMatNull(String wrstRisMatNull) {
        writeString(Pos.WRST_RIS_MAT_NULL, wrstRisMatNull, Len.WRST_RIS_MAT_NULL);
    }

    /**Original name: WRST-RIS-MAT-NULL<br>*/
    public String getWrstRisMatNull() {
        return readString(Pos.WRST_RIS_MAT_NULL, Len.WRST_RIS_MAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_MAT = 1;
        public static final int WRST_RIS_MAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_MAT = 8;
        public static final int WRST_RIS_MAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_MAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_MAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
