package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LVES0245<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLves0245 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-PMO
    private short tabPmo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-POG
    private short tabPog = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-RIC-PMO
    private short ricPmo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-RIC-POG
    private short ricPog = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-RIC-RAN
    private short ricRan = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setTabPmo(short tabPmo) {
        this.tabPmo = tabPmo;
    }

    public short getTabPmo() {
        return this.tabPmo;
    }

    public void setTabPog(short tabPog) {
        this.tabPog = tabPog;
    }

    public short getTabPog() {
        return this.tabPog;
    }

    public void setRicPmo(short ricPmo) {
        this.ricPmo = ricPmo;
    }

    public short getRicPmo() {
        return this.ricPmo;
    }

    public void setRicPog(short ricPog) {
        this.ricPog = ricPog;
    }

    public short getRicPog() {
        return this.ricPog;
    }

    public void setRicRan(short ricRan) {
        this.ricRan = ricRan;
    }

    public short getRicRan() {
        return this.ricRan;
    }
}
