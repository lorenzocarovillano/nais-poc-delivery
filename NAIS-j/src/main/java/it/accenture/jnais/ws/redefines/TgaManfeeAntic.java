package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-MANFEE-ANTIC<br>
 * Variable: TGA-MANFEE-ANTIC from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_MANFEE_ANTIC;
    }

    public void setTgaManfeeAntic(AfDecimal tgaManfeeAntic) {
        writeDecimalAsPacked(Pos.TGA_MANFEE_ANTIC, tgaManfeeAntic.copy());
    }

    public void setTgaManfeeAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_MANFEE_ANTIC, Pos.TGA_MANFEE_ANTIC);
    }

    /**Original name: TGA-MANFEE-ANTIC<br>*/
    public AfDecimal getTgaManfeeAntic() {
        return readPackedAsDecimal(Pos.TGA_MANFEE_ANTIC, Len.Int.TGA_MANFEE_ANTIC, Len.Fract.TGA_MANFEE_ANTIC);
    }

    public byte[] getTgaManfeeAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_MANFEE_ANTIC, Pos.TGA_MANFEE_ANTIC);
        return buffer;
    }

    public void setTgaManfeeAnticNull(String tgaManfeeAnticNull) {
        writeString(Pos.TGA_MANFEE_ANTIC_NULL, tgaManfeeAnticNull, Len.TGA_MANFEE_ANTIC_NULL);
    }

    /**Original name: TGA-MANFEE-ANTIC-NULL<br>*/
    public String getTgaManfeeAnticNull() {
        return readString(Pos.TGA_MANFEE_ANTIC_NULL, Len.TGA_MANFEE_ANTIC_NULL);
    }

    public String getTgaManfeeAnticNullFormatted() {
        return Functions.padBlanks(getTgaManfeeAnticNull(), Len.TGA_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_MANFEE_ANTIC = 1;
        public static final int TGA_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_MANFEE_ANTIC = 8;
        public static final int TGA_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
