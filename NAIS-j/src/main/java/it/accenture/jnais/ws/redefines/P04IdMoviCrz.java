package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P04-ID-MOVI-CRZ<br>
 * Variable: P04-ID-MOVI-CRZ from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P04IdMoviCrz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P04IdMoviCrz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P04_ID_MOVI_CRZ;
    }

    public void setP04IdMoviCrz(int p04IdMoviCrz) {
        writeIntAsPacked(Pos.P04_ID_MOVI_CRZ, p04IdMoviCrz, Len.Int.P04_ID_MOVI_CRZ);
    }

    public void setP04IdMoviCrzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P04_ID_MOVI_CRZ, Pos.P04_ID_MOVI_CRZ);
    }

    /**Original name: P04-ID-MOVI-CRZ<br>*/
    public int getP04IdMoviCrz() {
        return readPackedAsInt(Pos.P04_ID_MOVI_CRZ, Len.Int.P04_ID_MOVI_CRZ);
    }

    public byte[] getP04IdMoviCrzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P04_ID_MOVI_CRZ, Pos.P04_ID_MOVI_CRZ);
        return buffer;
    }

    public void setP04IdMoviCrzNull(String p04IdMoviCrzNull) {
        writeString(Pos.P04_ID_MOVI_CRZ_NULL, p04IdMoviCrzNull, Len.P04_ID_MOVI_CRZ_NULL);
    }

    /**Original name: P04-ID-MOVI-CRZ-NULL<br>*/
    public String getP04IdMoviCrzNull() {
        return readString(Pos.P04_ID_MOVI_CRZ_NULL, Len.P04_ID_MOVI_CRZ_NULL);
    }

    public String getP04IdMoviCrzNullFormatted() {
        return Functions.padBlanks(getP04IdMoviCrzNull(), Len.P04_ID_MOVI_CRZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P04_ID_MOVI_CRZ = 1;
        public static final int P04_ID_MOVI_CRZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P04_ID_MOVI_CRZ = 5;
        public static final int P04_ID_MOVI_CRZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P04_ID_MOVI_CRZ = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
