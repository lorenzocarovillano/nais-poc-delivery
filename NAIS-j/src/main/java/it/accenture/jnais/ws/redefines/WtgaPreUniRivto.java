package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-UNI-RIVTO<br>
 * Variable: WTGA-PRE-UNI-RIVTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPreUniRivto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPreUniRivto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_UNI_RIVTO;
    }

    public void setWtgaPreUniRivto(AfDecimal wtgaPreUniRivto) {
        writeDecimalAsPacked(Pos.WTGA_PRE_UNI_RIVTO, wtgaPreUniRivto.copy());
    }

    public void setWtgaPreUniRivtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_UNI_RIVTO, Pos.WTGA_PRE_UNI_RIVTO);
    }

    /**Original name: WTGA-PRE-UNI-RIVTO<br>*/
    public AfDecimal getWtgaPreUniRivto() {
        return readPackedAsDecimal(Pos.WTGA_PRE_UNI_RIVTO, Len.Int.WTGA_PRE_UNI_RIVTO, Len.Fract.WTGA_PRE_UNI_RIVTO);
    }

    public byte[] getWtgaPreUniRivtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_UNI_RIVTO, Pos.WTGA_PRE_UNI_RIVTO);
        return buffer;
    }

    public void initWtgaPreUniRivtoSpaces() {
        fill(Pos.WTGA_PRE_UNI_RIVTO, Len.WTGA_PRE_UNI_RIVTO, Types.SPACE_CHAR);
    }

    public void setWtgaPreUniRivtoNull(String wtgaPreUniRivtoNull) {
        writeString(Pos.WTGA_PRE_UNI_RIVTO_NULL, wtgaPreUniRivtoNull, Len.WTGA_PRE_UNI_RIVTO_NULL);
    }

    /**Original name: WTGA-PRE-UNI-RIVTO-NULL<br>*/
    public String getWtgaPreUniRivtoNull() {
        return readString(Pos.WTGA_PRE_UNI_RIVTO_NULL, Len.WTGA_PRE_UNI_RIVTO_NULL);
    }

    public String getWtgaPreUniRivtoNullFormatted() {
        return Functions.padBlanks(getWtgaPreUniRivtoNull(), Len.WTGA_PRE_UNI_RIVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_UNI_RIVTO = 1;
        public static final int WTGA_PRE_UNI_RIVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_UNI_RIVTO = 8;
        public static final int WTGA_PRE_UNI_RIVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_UNI_RIVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_UNI_RIVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
