package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-EMIS-TRCH<br>
 * Variable: WB03-DT-EMIS-TRCH from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtEmisTrch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtEmisTrch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_EMIS_TRCH;
    }

    public void setWb03DtEmisTrch(int wb03DtEmisTrch) {
        writeIntAsPacked(Pos.WB03_DT_EMIS_TRCH, wb03DtEmisTrch, Len.Int.WB03_DT_EMIS_TRCH);
    }

    public void setWb03DtEmisTrchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_EMIS_TRCH, Pos.WB03_DT_EMIS_TRCH);
    }

    /**Original name: WB03-DT-EMIS-TRCH<br>*/
    public int getWb03DtEmisTrch() {
        return readPackedAsInt(Pos.WB03_DT_EMIS_TRCH, Len.Int.WB03_DT_EMIS_TRCH);
    }

    public byte[] getWb03DtEmisTrchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_EMIS_TRCH, Pos.WB03_DT_EMIS_TRCH);
        return buffer;
    }

    public void setWb03DtEmisTrchNull(String wb03DtEmisTrchNull) {
        writeString(Pos.WB03_DT_EMIS_TRCH_NULL, wb03DtEmisTrchNull, Len.WB03_DT_EMIS_TRCH_NULL);
    }

    /**Original name: WB03-DT-EMIS-TRCH-NULL<br>*/
    public String getWb03DtEmisTrchNull() {
        return readString(Pos.WB03_DT_EMIS_TRCH_NULL, Len.WB03_DT_EMIS_TRCH_NULL);
    }

    public String getWb03DtEmisTrchNullFormatted() {
        return Functions.padBlanks(getWb03DtEmisTrchNull(), Len.WB03_DT_EMIS_TRCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_EMIS_TRCH = 1;
        public static final int WB03_DT_EMIS_TRCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_EMIS_TRCH = 5;
        public static final int WB03_DT_EMIS_TRCH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_EMIS_TRCH = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
