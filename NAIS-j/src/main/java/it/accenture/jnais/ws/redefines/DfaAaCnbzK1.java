package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-AA-CNBZ-K1<br>
 * Variable: DFA-AA-CNBZ-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaAaCnbzK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaAaCnbzK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_AA_CNBZ_K1;
    }

    public void setDfaAaCnbzK1(short dfaAaCnbzK1) {
        writeShortAsPacked(Pos.DFA_AA_CNBZ_K1, dfaAaCnbzK1, Len.Int.DFA_AA_CNBZ_K1);
    }

    public void setDfaAaCnbzK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_AA_CNBZ_K1, Pos.DFA_AA_CNBZ_K1);
    }

    /**Original name: DFA-AA-CNBZ-K1<br>*/
    public short getDfaAaCnbzK1() {
        return readPackedAsShort(Pos.DFA_AA_CNBZ_K1, Len.Int.DFA_AA_CNBZ_K1);
    }

    public byte[] getDfaAaCnbzK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_AA_CNBZ_K1, Pos.DFA_AA_CNBZ_K1);
        return buffer;
    }

    public void setDfaAaCnbzK1Null(String dfaAaCnbzK1Null) {
        writeString(Pos.DFA_AA_CNBZ_K1_NULL, dfaAaCnbzK1Null, Len.DFA_AA_CNBZ_K1_NULL);
    }

    /**Original name: DFA-AA-CNBZ-K1-NULL<br>*/
    public String getDfaAaCnbzK1Null() {
        return readString(Pos.DFA_AA_CNBZ_K1_NULL, Len.DFA_AA_CNBZ_K1_NULL);
    }

    public String getDfaAaCnbzK1NullFormatted() {
        return Functions.padBlanks(getDfaAaCnbzK1Null(), Len.DFA_AA_CNBZ_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_AA_CNBZ_K1 = 1;
        public static final int DFA_AA_CNBZ_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_AA_CNBZ_K1 = 3;
        public static final int DFA_AA_CNBZ_K1_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_AA_CNBZ_K1 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
