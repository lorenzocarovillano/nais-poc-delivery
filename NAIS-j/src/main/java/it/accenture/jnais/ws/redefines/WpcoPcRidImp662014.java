package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-PC-RID-IMP-662014<br>
 * Variable: WPCO-PC-RID-IMP-662014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoPcRidImp662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoPcRidImp662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_PC_RID_IMP662014;
    }

    public void setWpcoPcRidImp662014(AfDecimal wpcoPcRidImp662014) {
        writeDecimalAsPacked(Pos.WPCO_PC_RID_IMP662014, wpcoPcRidImp662014.copy());
    }

    public void setDpcoPcRidImp662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_PC_RID_IMP662014, Pos.WPCO_PC_RID_IMP662014);
    }

    /**Original name: WPCO-PC-RID-IMP-662014<br>*/
    public AfDecimal getWpcoPcRidImp662014() {
        return readPackedAsDecimal(Pos.WPCO_PC_RID_IMP662014, Len.Int.WPCO_PC_RID_IMP662014, Len.Fract.WPCO_PC_RID_IMP662014);
    }

    public byte[] getWpcoPcRidImp662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_PC_RID_IMP662014, Pos.WPCO_PC_RID_IMP662014);
        return buffer;
    }

    public void setWpcoPcRidImp662014Null(String wpcoPcRidImp662014Null) {
        writeString(Pos.WPCO_PC_RID_IMP662014_NULL, wpcoPcRidImp662014Null, Len.WPCO_PC_RID_IMP662014_NULL);
    }

    /**Original name: WPCO-PC-RID-IMP-662014-NULL<br>*/
    public String getWpcoPcRidImp662014Null() {
        return readString(Pos.WPCO_PC_RID_IMP662014_NULL, Len.WPCO_PC_RID_IMP662014_NULL);
    }

    public String getWpcoPcRidImp662014NullFormatted() {
        return Functions.padBlanks(getWpcoPcRidImp662014Null(), Len.WPCO_PC_RID_IMP662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_PC_RID_IMP662014 = 1;
        public static final int WPCO_PC_RID_IMP662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_PC_RID_IMP662014 = 4;
        public static final int WPCO_PC_RID_IMP662014_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_PC_RID_IMP662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_PC_RID_IMP662014 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
