package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-ACQ-1O-ANNO<br>
 * Variable: WPAG-CONT-ACQ-1O-ANNO from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContAcq1oAnno extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContAcq1oAnno() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_ACQ1O_ANNO;
    }

    public void setWpagContAcq1oAnno(AfDecimal wpagContAcq1oAnno) {
        writeDecimalAsPacked(Pos.WPAG_CONT_ACQ1O_ANNO, wpagContAcq1oAnno.copy());
    }

    public void setWpagContAcq1oAnnoFormatted(String wpagContAcq1oAnno) {
        setWpagContAcq1oAnno(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_ACQ1O_ANNO + Len.Fract.WPAG_CONT_ACQ1O_ANNO, Len.Fract.WPAG_CONT_ACQ1O_ANNO, wpagContAcq1oAnno));
    }

    public void setWpagContAcq1oAnnoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_ACQ1O_ANNO, Pos.WPAG_CONT_ACQ1O_ANNO);
    }

    /**Original name: WPAG-CONT-ACQ-1O-ANNO<br>*/
    public AfDecimal getWpagContAcq1oAnno() {
        return readPackedAsDecimal(Pos.WPAG_CONT_ACQ1O_ANNO, Len.Int.WPAG_CONT_ACQ1O_ANNO, Len.Fract.WPAG_CONT_ACQ1O_ANNO);
    }

    public byte[] getWpagContAcq1oAnnoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_ACQ1O_ANNO, Pos.WPAG_CONT_ACQ1O_ANNO);
        return buffer;
    }

    public void initWpagContAcq1oAnnoSpaces() {
        fill(Pos.WPAG_CONT_ACQ1O_ANNO, Len.WPAG_CONT_ACQ1O_ANNO, Types.SPACE_CHAR);
    }

    public void setWpagContAcq1oAnnoNull(String wpagContAcq1oAnnoNull) {
        writeString(Pos.WPAG_CONT_ACQ1O_ANNO_NULL, wpagContAcq1oAnnoNull, Len.WPAG_CONT_ACQ1O_ANNO_NULL);
    }

    /**Original name: WPAG-CONT-ACQ-1O-ANNO-NULL<br>*/
    public String getWpagContAcq1oAnnoNull() {
        return readString(Pos.WPAG_CONT_ACQ1O_ANNO_NULL, Len.WPAG_CONT_ACQ1O_ANNO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_ACQ1O_ANNO = 1;
        public static final int WPAG_CONT_ACQ1O_ANNO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_ACQ1O_ANNO = 8;
        public static final int WPAG_CONT_ACQ1O_ANNO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_ACQ1O_ANNO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_ACQ1O_ANNO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
