package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IBtcJobExecution;
import it.accenture.jnais.ws.redefines.BjeDtEnd;

/**Original name: BTC-JOB-EXECUTION<br>
 * Variable: BTC-JOB-EXECUTION from copybook IDBVBJE1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BtcJobExecution extends SerializableParameter implements IBtcJobExecution {

    //==== PROPERTIES ====
    //Original name: BJE-ID-BATCH
    private int bjeIdBatch = DefaultValues.BIN_INT_VAL;
    //Original name: BJE-ID-JOB
    private int bjeIdJob = DefaultValues.BIN_INT_VAL;
    //Original name: BJE-ID-EXECUTION
    private int bjeIdExecution = DefaultValues.BIN_INT_VAL;
    //Original name: BJE-COD-ELAB-STATE
    private char bjeCodElabState = DefaultValues.CHAR_VAL;
    //Original name: BJE-DATA-LEN
    private short bjeDataLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: BJE-DATA
    private String bjeData = DefaultValues.stringVal(Len.BJE_DATA);
    //Original name: BJE-FLAG-WARNINGS
    private char bjeFlagWarnings = DefaultValues.CHAR_VAL;
    //Original name: BJE-DT-START
    private long bjeDtStart = DefaultValues.LONG_VAL;
    //Original name: BJE-DT-END
    private BjeDtEnd bjeDtEnd = new BjeDtEnd();
    //Original name: BJE-USER-START
    private String bjeUserStart = DefaultValues.stringVal(Len.BJE_USER_START);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_JOB_EXECUTION;
    }

    @Override
    public void deserialize(byte[] buf) {
        setBtcJobExecutionBytes(buf);
    }

    public String getBtcJobExecutionFormatted() {
        return MarshalByteExt.bufferToStr(getBtcJobExecutionBytes());
    }

    public void setBtcJobExecutionBytes(byte[] buffer) {
        setBtcJobExecutionBytes(buffer, 1);
    }

    public byte[] getBtcJobExecutionBytes() {
        byte[] buffer = new byte[Len.BTC_JOB_EXECUTION];
        return getBtcJobExecutionBytes(buffer, 1);
    }

    public void setBtcJobExecutionBytes(byte[] buffer, int offset) {
        int position = offset;
        bjeIdBatch = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        bjeIdJob = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        bjeIdExecution = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        bjeCodElabState = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setBjeDataVcharBytes(buffer, position);
        position += Len.BJE_DATA_VCHAR;
        bjeFlagWarnings = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bjeDtStart = MarshalByte.readPackedAsLong(buffer, position, Len.Int.BJE_DT_START, 0);
        position += Len.BJE_DT_START;
        bjeDtEnd.setBjeDtEndFromBuffer(buffer, position);
        position += BjeDtEnd.Len.BJE_DT_END;
        bjeUserStart = MarshalByte.readString(buffer, position, Len.BJE_USER_START);
    }

    public byte[] getBtcJobExecutionBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, bjeIdBatch);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, bjeIdJob);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, bjeIdExecution);
        position += Types.INT_SIZE;
        MarshalByte.writeChar(buffer, position, bjeCodElabState);
        position += Types.CHAR_SIZE;
        getBjeDataVcharBytes(buffer, position);
        position += Len.BJE_DATA_VCHAR;
        MarshalByte.writeChar(buffer, position, bjeFlagWarnings);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, bjeDtStart, Len.Int.BJE_DT_START, 0);
        position += Len.BJE_DT_START;
        bjeDtEnd.getBjeDtEndAsBuffer(buffer, position);
        position += BjeDtEnd.Len.BJE_DT_END;
        MarshalByte.writeString(buffer, position, bjeUserStart, Len.BJE_USER_START);
        return buffer;
    }

    public void setBjeIdBatch(int bjeIdBatch) {
        this.bjeIdBatch = bjeIdBatch;
    }

    public int getBjeIdBatch() {
        return this.bjeIdBatch;
    }

    public void setBjeIdJob(int bjeIdJob) {
        this.bjeIdJob = bjeIdJob;
    }

    public int getBjeIdJob() {
        return this.bjeIdJob;
    }

    public void setBjeIdExecution(int bjeIdExecution) {
        this.bjeIdExecution = bjeIdExecution;
    }

    public int getBjeIdExecution() {
        return this.bjeIdExecution;
    }

    public void setBjeCodElabState(char bjeCodElabState) {
        this.bjeCodElabState = bjeCodElabState;
    }

    public char getBjeCodElabState() {
        return this.bjeCodElabState;
    }

    public void setBjeDataVcharFormatted(String data) {
        byte[] buffer = new byte[Len.BJE_DATA_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.BJE_DATA_VCHAR);
        setBjeDataVcharBytes(buffer, 1);
    }

    public String getBjeDataVcharFormatted() {
        return MarshalByteExt.bufferToStr(getBjeDataVcharBytes());
    }

    /**Original name: BJE-DATA-VCHAR<br>*/
    public byte[] getBjeDataVcharBytes() {
        byte[] buffer = new byte[Len.BJE_DATA_VCHAR];
        return getBjeDataVcharBytes(buffer, 1);
    }

    public void setBjeDataVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        bjeDataLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        bjeData = MarshalByte.readString(buffer, position, Len.BJE_DATA);
    }

    public byte[] getBjeDataVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, bjeDataLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, bjeData, Len.BJE_DATA);
        return buffer;
    }

    public void setBjeDataLen(short bjeDataLen) {
        this.bjeDataLen = bjeDataLen;
    }

    public short getBjeDataLen() {
        return this.bjeDataLen;
    }

    public void setBjeData(String bjeData) {
        this.bjeData = Functions.subString(bjeData, Len.BJE_DATA);
    }

    public String getBjeData() {
        return this.bjeData;
    }

    public void setBjeFlagWarnings(char bjeFlagWarnings) {
        this.bjeFlagWarnings = bjeFlagWarnings;
    }

    public char getBjeFlagWarnings() {
        return this.bjeFlagWarnings;
    }

    public void setBjeDtStart(long bjeDtStart) {
        this.bjeDtStart = bjeDtStart;
    }

    public long getBjeDtStart() {
        return this.bjeDtStart;
    }

    public void setBjeUserStart(String bjeUserStart) {
        this.bjeUserStart = Functions.subString(bjeUserStart, Len.BJE_USER_START);
    }

    public String getBjeUserStart() {
        return this.bjeUserStart;
    }

    public BjeDtEnd getBjeDtEnd() {
        return bjeDtEnd;
    }

    @Override
    public char getCodElabState() {
        throw new FieldNotMappedException("codElabState");
    }

    @Override
    public void setCodElabState(char codElabState) {
        throw new FieldNotMappedException("codElabState");
    }

    @Override
    public String getDataVchar() {
        throw new FieldNotMappedException("dataVchar");
    }

    @Override
    public void setDataVchar(String dataVchar) {
        throw new FieldNotMappedException("dataVchar");
    }

    @Override
    public String getDataVcharObj() {
        return getDataVchar();
    }

    @Override
    public void setDataVcharObj(String dataVcharObj) {
        setDataVchar(dataVcharObj);
    }

    @Override
    public String getDtEndDb() {
        throw new FieldNotMappedException("dtEndDb");
    }

    @Override
    public void setDtEndDb(String dtEndDb) {
        throw new FieldNotMappedException("dtEndDb");
    }

    @Override
    public String getDtEndDbObj() {
        return getDtEndDb();
    }

    @Override
    public void setDtEndDbObj(String dtEndDbObj) {
        setDtEndDb(dtEndDbObj);
    }

    @Override
    public String getDtStartDb() {
        throw new FieldNotMappedException("dtStartDb");
    }

    @Override
    public void setDtStartDb(String dtStartDb) {
        throw new FieldNotMappedException("dtStartDb");
    }

    @Override
    public char getFlagWarnings() {
        throw new FieldNotMappedException("flagWarnings");
    }

    @Override
    public void setFlagWarnings(char flagWarnings) {
        throw new FieldNotMappedException("flagWarnings");
    }

    @Override
    public Character getFlagWarningsObj() {
        return ((Character)getFlagWarnings());
    }

    @Override
    public void setFlagWarningsObj(Character flagWarningsObj) {
        setFlagWarnings(((char)flagWarningsObj));
    }

    @Override
    public int getIdBatch() {
        throw new FieldNotMappedException("idBatch");
    }

    @Override
    public void setIdBatch(int idBatch) {
        throw new FieldNotMappedException("idBatch");
    }

    @Override
    public int getIdExecution() {
        throw new FieldNotMappedException("idExecution");
    }

    @Override
    public void setIdExecution(int idExecution) {
        throw new FieldNotMappedException("idExecution");
    }

    @Override
    public int getIdJob() {
        throw new FieldNotMappedException("idJob");
    }

    @Override
    public void setIdJob(int idJob) {
        throw new FieldNotMappedException("idJob");
    }

    @Override
    public String getUserStart() {
        throw new FieldNotMappedException("userStart");
    }

    @Override
    public void setUserStart(String userStart) {
        throw new FieldNotMappedException("userStart");
    }

    @Override
    public byte[] serialize() {
        return getBtcJobExecutionBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BJE_ID_BATCH = 4;
        public static final int BJE_ID_JOB = 4;
        public static final int BJE_ID_EXECUTION = 4;
        public static final int BJE_COD_ELAB_STATE = 1;
        public static final int BJE_DATA_LEN = 2;
        public static final int BJE_DATA = 32000;
        public static final int BJE_DATA_VCHAR = BJE_DATA_LEN + BJE_DATA;
        public static final int BJE_FLAG_WARNINGS = 1;
        public static final int BJE_DT_START = 10;
        public static final int BJE_USER_START = 30;
        public static final int BTC_JOB_EXECUTION = BJE_ID_BATCH + BJE_ID_JOB + BJE_ID_EXECUTION + BJE_COD_ELAB_STATE + BJE_DATA_VCHAR + BJE_FLAG_WARNINGS + BJE_DT_START + BjeDtEnd.Len.BJE_DT_END + BJE_USER_START;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BJE_DT_START = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
