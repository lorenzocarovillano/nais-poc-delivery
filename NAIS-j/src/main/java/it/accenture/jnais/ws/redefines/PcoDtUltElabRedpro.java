package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-REDPRO<br>
 * Variable: PCO-DT-ULT-ELAB-REDPRO from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabRedpro extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabRedpro() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_REDPRO;
    }

    public void setPcoDtUltElabRedpro(int pcoDtUltElabRedpro) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_REDPRO, pcoDtUltElabRedpro, Len.Int.PCO_DT_ULT_ELAB_REDPRO);
    }

    public void setPcoDtUltElabRedproFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_REDPRO, Pos.PCO_DT_ULT_ELAB_REDPRO);
    }

    /**Original name: PCO-DT-ULT-ELAB-REDPRO<br>*/
    public int getPcoDtUltElabRedpro() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_REDPRO, Len.Int.PCO_DT_ULT_ELAB_REDPRO);
    }

    public byte[] getPcoDtUltElabRedproAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_REDPRO, Pos.PCO_DT_ULT_ELAB_REDPRO);
        return buffer;
    }

    public void initPcoDtUltElabRedproHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_REDPRO, Len.PCO_DT_ULT_ELAB_REDPRO, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabRedproNull(String pcoDtUltElabRedproNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_REDPRO_NULL, pcoDtUltElabRedproNull, Len.PCO_DT_ULT_ELAB_REDPRO_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-REDPRO-NULL<br>*/
    public String getPcoDtUltElabRedproNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_REDPRO_NULL, Len.PCO_DT_ULT_ELAB_REDPRO_NULL);
    }

    public String getPcoDtUltElabRedproNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabRedproNull(), Len.PCO_DT_ULT_ELAB_REDPRO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_REDPRO = 1;
        public static final int PCO_DT_ULT_ELAB_REDPRO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_REDPRO = 5;
        public static final int PCO_DT_ULT_ELAB_REDPRO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_REDPRO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
