package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0503-DATO-INPUT-TROVATO<br>
 * Variable: IDSV0503-DATO-INPUT-TROVATO from copybook IDSV0503<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0503DatoInputTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setIdsv0503DatoInputTrovato(char idsv0503DatoInputTrovato) {
        this.value = idsv0503DatoInputTrovato;
    }

    public char getIdsv0503DatoInputTrovato() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
