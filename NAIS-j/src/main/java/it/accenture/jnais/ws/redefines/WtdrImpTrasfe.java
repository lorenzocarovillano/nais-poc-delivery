package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-IMP-TRASFE<br>
 * Variable: WTDR-IMP-TRASFE from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrImpTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrImpTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_IMP_TRASFE;
    }

    public void setWtdrImpTrasfe(AfDecimal wtdrImpTrasfe) {
        writeDecimalAsPacked(Pos.WTDR_IMP_TRASFE, wtdrImpTrasfe.copy());
    }

    /**Original name: WTDR-IMP-TRASFE<br>*/
    public AfDecimal getWtdrImpTrasfe() {
        return readPackedAsDecimal(Pos.WTDR_IMP_TRASFE, Len.Int.WTDR_IMP_TRASFE, Len.Fract.WTDR_IMP_TRASFE);
    }

    public void setWtdrImpTrasfeNull(String wtdrImpTrasfeNull) {
        writeString(Pos.WTDR_IMP_TRASFE_NULL, wtdrImpTrasfeNull, Len.WTDR_IMP_TRASFE_NULL);
    }

    /**Original name: WTDR-IMP-TRASFE-NULL<br>*/
    public String getWtdrImpTrasfeNull() {
        return readString(Pos.WTDR_IMP_TRASFE_NULL, Len.WTDR_IMP_TRASFE_NULL);
    }

    public String getWtdrImpTrasfeNullFormatted() {
        return Functions.padBlanks(getWtdrImpTrasfeNull(), Len.WTDR_IMP_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_TRASFE = 1;
        public static final int WTDR_IMP_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_TRASFE = 8;
        public static final int WTDR_IMP_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
