package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-DT-INI-VAL-TAR<br>
 * Variable: L3401-DT-INI-VAL-TAR from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401DtIniValTar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401DtIniValTar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_DT_INI_VAL_TAR;
    }

    public void setL3401DtIniValTar(int l3401DtIniValTar) {
        writeIntAsPacked(Pos.L3401_DT_INI_VAL_TAR, l3401DtIniValTar, Len.Int.L3401_DT_INI_VAL_TAR);
    }

    public void setL3401DtIniValTarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_DT_INI_VAL_TAR, Pos.L3401_DT_INI_VAL_TAR);
    }

    /**Original name: L3401-DT-INI-VAL-TAR<br>*/
    public int getL3401DtIniValTar() {
        return readPackedAsInt(Pos.L3401_DT_INI_VAL_TAR, Len.Int.L3401_DT_INI_VAL_TAR);
    }

    public byte[] getL3401DtIniValTarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_DT_INI_VAL_TAR, Pos.L3401_DT_INI_VAL_TAR);
        return buffer;
    }

    /**Original name: L3401-DT-INI-VAL-TAR-NULL<br>*/
    public String getL3401DtIniValTarNull() {
        return readString(Pos.L3401_DT_INI_VAL_TAR_NULL, Len.L3401_DT_INI_VAL_TAR_NULL);
    }

    public String getL3401DtIniValTarNullFormatted() {
        return Functions.padBlanks(getL3401DtIniValTarNull(), Len.L3401_DT_INI_VAL_TAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_DT_INI_VAL_TAR = 1;
        public static final int L3401_DT_INI_VAL_TAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_DT_INI_VAL_TAR = 5;
        public static final int L3401_DT_INI_VAL_TAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_DT_INI_VAL_TAR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
