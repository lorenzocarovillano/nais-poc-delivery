package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPST-APPL-RILT<br>
 * Variable: S089-IMPST-APPL-RILT from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpstApplRilt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpstApplRilt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPST_APPL_RILT;
    }

    public void setWlquImpstApplRilt(AfDecimal wlquImpstApplRilt) {
        writeDecimalAsPacked(Pos.S089_IMPST_APPL_RILT, wlquImpstApplRilt.copy());
    }

    public void setWlquImpstApplRiltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPST_APPL_RILT, Pos.S089_IMPST_APPL_RILT);
    }

    /**Original name: WLQU-IMPST-APPL-RILT<br>*/
    public AfDecimal getWlquImpstApplRilt() {
        return readPackedAsDecimal(Pos.S089_IMPST_APPL_RILT, Len.Int.WLQU_IMPST_APPL_RILT, Len.Fract.WLQU_IMPST_APPL_RILT);
    }

    public byte[] getWlquImpstApplRiltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPST_APPL_RILT, Pos.S089_IMPST_APPL_RILT);
        return buffer;
    }

    public void initWlquImpstApplRiltSpaces() {
        fill(Pos.S089_IMPST_APPL_RILT, Len.S089_IMPST_APPL_RILT, Types.SPACE_CHAR);
    }

    public void setWlquImpstApplRiltNull(String wlquImpstApplRiltNull) {
        writeString(Pos.S089_IMPST_APPL_RILT_NULL, wlquImpstApplRiltNull, Len.WLQU_IMPST_APPL_RILT_NULL);
    }

    /**Original name: WLQU-IMPST-APPL-RILT-NULL<br>*/
    public String getWlquImpstApplRiltNull() {
        return readString(Pos.S089_IMPST_APPL_RILT_NULL, Len.WLQU_IMPST_APPL_RILT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPST_APPL_RILT = 1;
        public static final int S089_IMPST_APPL_RILT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPST_APPL_RILT = 8;
        public static final int WLQU_IMPST_APPL_RILT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_APPL_RILT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_APPL_RILT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
