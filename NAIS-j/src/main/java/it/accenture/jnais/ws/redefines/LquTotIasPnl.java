package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IAS-PNL<br>
 * Variable: LQU-TOT-IAS-PNL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotIasPnl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotIasPnl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IAS_PNL;
    }

    public void setLquTotIasPnl(AfDecimal lquTotIasPnl) {
        writeDecimalAsPacked(Pos.LQU_TOT_IAS_PNL, lquTotIasPnl.copy());
    }

    public void setLquTotIasPnlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IAS_PNL, Pos.LQU_TOT_IAS_PNL);
    }

    /**Original name: LQU-TOT-IAS-PNL<br>*/
    public AfDecimal getLquTotIasPnl() {
        return readPackedAsDecimal(Pos.LQU_TOT_IAS_PNL, Len.Int.LQU_TOT_IAS_PNL, Len.Fract.LQU_TOT_IAS_PNL);
    }

    public byte[] getLquTotIasPnlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IAS_PNL, Pos.LQU_TOT_IAS_PNL);
        return buffer;
    }

    public void setLquTotIasPnlNull(String lquTotIasPnlNull) {
        writeString(Pos.LQU_TOT_IAS_PNL_NULL, lquTotIasPnlNull, Len.LQU_TOT_IAS_PNL_NULL);
    }

    /**Original name: LQU-TOT-IAS-PNL-NULL<br>*/
    public String getLquTotIasPnlNull() {
        return readString(Pos.LQU_TOT_IAS_PNL_NULL, Len.LQU_TOT_IAS_PNL_NULL);
    }

    public String getLquTotIasPnlNullFormatted() {
        return Functions.padBlanks(getLquTotIasPnlNull(), Len.LQU_TOT_IAS_PNL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IAS_PNL = 1;
        public static final int LQU_TOT_IAS_PNL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IAS_PNL = 8;
        public static final int LQU_TOT_IAS_PNL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IAS_PNL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IAS_PNL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
