package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-SAVE-TRANCHE<br>
 * Variable: SW-SAVE-TRANCHE from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwSaveTranche {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_SAVE_TRANCHE = 'S';
    public static final char NO_SAVE_TRANCHE = 'N';

    //==== METHODS ====
    public void setSwSaveTranche(char swSaveTranche) {
        this.value = swSaveTranche;
    }

    public char getSwSaveTranche() {
        return this.value;
    }

    public boolean isSiSaveTranche() {
        return value == SI_SAVE_TRANCHE;
    }

    public void setSiSaveTranche() {
        value = SI_SAVE_TRANCHE;
    }

    public void setNoSaveTranche() {
        value = NO_SAVE_TRANCHE;
    }
}
