package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Idsv0001AreaComune;
import it.accenture.jnais.copy.Idsv0001LogErrore;
import it.accenture.jnais.ws.enums.Idsv0001Esito;
import it.accenture.jnais.ws.occurs.Idsv0001EleErrori;

/**Original name: AREA-IDSV0001<br>
 * Variable: AREA-IDSV0001 from program IEAS9700<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIdsv0001 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int ELE_ERRORI_MAXOCCURS = 10;
    //Original name: IDSV0001-AREA-COMUNE
    private Idsv0001AreaComune areaComune = new Idsv0001AreaComune();
    //Original name: IDSV0001-ESITO
    private Idsv0001Esito esito = new Idsv0001Esito();
    //Original name: IDSV0001-LOG-ERRORE
    private Idsv0001LogErrore logErrore = new Idsv0001LogErrore();
    //Original name: IDSV0001-MAX-ELE-ERRORI
    private short maxEleErrori = DefaultValues.BIN_SHORT_VAL;
    //Original name: IDSV0001-ELE-ERRORI
    private Idsv0001EleErrori[] eleErrori = new Idsv0001EleErrori[ELE_ERRORI_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public AreaIdsv0001() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IDSV0001;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIdsv0001Bytes(buf);
    }

    public void init() {
        for (int eleErroriIdx = 1; eleErroriIdx <= ELE_ERRORI_MAXOCCURS; eleErroriIdx++) {
            eleErrori[eleErroriIdx - 1] = new Idsv0001EleErrori();
        }
    }

    public String getAreaIdsv0001Formatted() {
        return getAreaContestoFormatted();
    }

    public void setAreaIdsv0001Bytes(byte[] buffer) {
        setAreaIdsv0001Bytes(buffer, 1);
    }

    public byte[] getAreaIdsv0001Bytes() {
        byte[] buffer = new byte[Len.AREA_IDSV0001];
        return getAreaIdsv0001Bytes(buffer, 1);
    }

    public void setAreaIdsv0001Bytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaContestoBytes(buffer, position);
    }

    public byte[] getAreaIdsv0001Bytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaContestoBytes(buffer, position);
        return buffer;
    }

    public String getAreaContestoFormatted() {
        return MarshalByteExt.bufferToStr(getAreaContestoBytes());
    }

    /**Original name: IDSV0001-AREA-CONTESTO<br>*/
    public byte[] getAreaContestoBytes() {
        byte[] buffer = new byte[Len.AREA_CONTESTO];
        return getAreaContestoBytes(buffer, 1);
    }

    public void setAreaContestoBytes(byte[] buffer, int offset) {
        int position = offset;
        areaComune.setAreaComuneBytes(buffer, position);
        position += Idsv0001AreaComune.Len.AREA_COMUNE;
        setAreaErroriBytes(buffer, position);
    }

    public byte[] getAreaContestoBytes(byte[] buffer, int offset) {
        int position = offset;
        areaComune.getAreaComuneBytes(buffer, position);
        position += Idsv0001AreaComune.Len.AREA_COMUNE;
        getAreaErroriBytes(buffer, position);
        return buffer;
    }

    public void setAreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        esito.setEsito(MarshalByte.readString(buffer, position, Idsv0001Esito.Len.ESITO));
        position += Idsv0001Esito.Len.ESITO;
        logErrore.setLogErroreBytes(buffer, position);
        position += Idsv0001LogErrore.Len.LOG_ERRORE;
        maxEleErrori = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setTabErroriFrontEndBytes(buffer, position);
    }

    public byte[] getAreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, esito.getEsito(), Idsv0001Esito.Len.ESITO);
        position += Idsv0001Esito.Len.ESITO;
        logErrore.getLogErroreBytes(buffer, position);
        position += Idsv0001LogErrore.Len.LOG_ERRORE;
        MarshalByte.writeBinaryShort(buffer, position, maxEleErrori);
        position += Types.SHORT_SIZE;
        getTabErroriFrontEndBytes(buffer, position);
        return buffer;
    }

    public void setMaxEleErrori(short maxEleErrori) {
        this.maxEleErrori = maxEleErrori;
    }

    public short getMaxEleErrori() {
        return this.maxEleErrori;
    }

    public void setTabErroriFrontEndBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ELE_ERRORI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                eleErrori[idx - 1].setEleErroriBytes(buffer, position);
                position += Idsv0001EleErrori.Len.ELE_ERRORI;
            }
            else {
                eleErrori[idx - 1].initEleErroriSpaces();
                position += Idsv0001EleErrori.Len.ELE_ERRORI;
            }
        }
    }

    public byte[] getTabErroriFrontEndBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ELE_ERRORI_MAXOCCURS; idx++) {
            eleErrori[idx - 1].getEleErroriBytes(buffer, position);
            position += Idsv0001EleErrori.Len.ELE_ERRORI;
        }
        return buffer;
    }

    public Idsv0001AreaComune getAreaComune() {
        return areaComune;
    }

    public Idsv0001EleErrori getEleErrori(int idx) {
        return eleErrori[idx - 1];
    }

    public Idsv0001Esito getEsito() {
        return esito;
    }

    public Idsv0001LogErrore getLogErrore() {
        return logErrore;
    }

    @Override
    public byte[] serialize() {
        return getAreaIdsv0001Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MAX_ELE_ERRORI = 2;
        public static final int TAB_ERRORI_FRONT_END = AreaIdsv0001.ELE_ERRORI_MAXOCCURS * Idsv0001EleErrori.Len.ELE_ERRORI;
        public static final int AREA_ERRORI = Idsv0001Esito.Len.ESITO + Idsv0001LogErrore.Len.LOG_ERRORE + MAX_ELE_ERRORI + TAB_ERRORI_FRONT_END;
        public static final int AREA_CONTESTO = Idsv0001AreaComune.Len.AREA_COMUNE + AREA_ERRORI;
        public static final int AREA_IDSV0001 = AREA_CONTESTO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
