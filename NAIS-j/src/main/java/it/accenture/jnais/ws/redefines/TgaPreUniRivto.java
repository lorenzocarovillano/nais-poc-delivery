package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-UNI-RIVTO<br>
 * Variable: TGA-PRE-UNI-RIVTO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPreUniRivto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPreUniRivto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_UNI_RIVTO;
    }

    public void setTgaPreUniRivto(AfDecimal tgaPreUniRivto) {
        writeDecimalAsPacked(Pos.TGA_PRE_UNI_RIVTO, tgaPreUniRivto.copy());
    }

    public void setTgaPreUniRivtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_UNI_RIVTO, Pos.TGA_PRE_UNI_RIVTO);
    }

    /**Original name: TGA-PRE-UNI-RIVTO<br>*/
    public AfDecimal getTgaPreUniRivto() {
        return readPackedAsDecimal(Pos.TGA_PRE_UNI_RIVTO, Len.Int.TGA_PRE_UNI_RIVTO, Len.Fract.TGA_PRE_UNI_RIVTO);
    }

    public byte[] getTgaPreUniRivtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_UNI_RIVTO, Pos.TGA_PRE_UNI_RIVTO);
        return buffer;
    }

    public void setTgaPreUniRivtoNull(String tgaPreUniRivtoNull) {
        writeString(Pos.TGA_PRE_UNI_RIVTO_NULL, tgaPreUniRivtoNull, Len.TGA_PRE_UNI_RIVTO_NULL);
    }

    /**Original name: TGA-PRE-UNI-RIVTO-NULL<br>*/
    public String getTgaPreUniRivtoNull() {
        return readString(Pos.TGA_PRE_UNI_RIVTO_NULL, Len.TGA_PRE_UNI_RIVTO_NULL);
    }

    public String getTgaPreUniRivtoNullFormatted() {
        return Functions.padBlanks(getTgaPreUniRivtoNull(), Len.TGA_PRE_UNI_RIVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_UNI_RIVTO = 1;
        public static final int TGA_PRE_UNI_RIVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_UNI_RIVTO = 8;
        public static final int TGA_PRE_UNI_RIVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_UNI_RIVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_UNI_RIVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
