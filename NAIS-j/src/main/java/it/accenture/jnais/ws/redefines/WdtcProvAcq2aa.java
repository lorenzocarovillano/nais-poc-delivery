package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-PROV-ACQ-2AA<br>
 * Variable: WDTC-PROV-ACQ-2AA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcProvAcq2aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcProvAcq2aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_PROV_ACQ2AA;
    }

    public void setWdtcProvAcq2aa(AfDecimal wdtcProvAcq2aa) {
        writeDecimalAsPacked(Pos.WDTC_PROV_ACQ2AA, wdtcProvAcq2aa.copy());
    }

    public void setWdtcProvAcq2aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_PROV_ACQ2AA, Pos.WDTC_PROV_ACQ2AA);
    }

    /**Original name: WDTC-PROV-ACQ-2AA<br>*/
    public AfDecimal getWdtcProvAcq2aa() {
        return readPackedAsDecimal(Pos.WDTC_PROV_ACQ2AA, Len.Int.WDTC_PROV_ACQ2AA, Len.Fract.WDTC_PROV_ACQ2AA);
    }

    public byte[] getWdtcProvAcq2aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_PROV_ACQ2AA, Pos.WDTC_PROV_ACQ2AA);
        return buffer;
    }

    public void initWdtcProvAcq2aaSpaces() {
        fill(Pos.WDTC_PROV_ACQ2AA, Len.WDTC_PROV_ACQ2AA, Types.SPACE_CHAR);
    }

    public void setWdtcProvAcq2aaNull(String wdtcProvAcq2aaNull) {
        writeString(Pos.WDTC_PROV_ACQ2AA_NULL, wdtcProvAcq2aaNull, Len.WDTC_PROV_ACQ2AA_NULL);
    }

    /**Original name: WDTC-PROV-ACQ-2AA-NULL<br>*/
    public String getWdtcProvAcq2aaNull() {
        return readString(Pos.WDTC_PROV_ACQ2AA_NULL, Len.WDTC_PROV_ACQ2AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_PROV_ACQ2AA = 1;
        public static final int WDTC_PROV_ACQ2AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_PROV_ACQ2AA = 8;
        public static final int WDTC_PROV_ACQ2AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_PROV_ACQ2AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_PROV_ACQ2AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
