package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.enums.WkFindLetto;
import it.accenture.jnais.ws.occurs.WpreTabPrestiti;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0017<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0017Data {

    //==== PROPERTIES ====
    public static final int DPRE_TAB_PRE_MAXOCCURS = 10;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0017";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: WK-FIND-LETTO
    private WkFindLetto wkFindLetto = new WkFindLetto();
    //Original name: INPUT-LVVS0000
    private InputLvvs0000 inputLvvs0000 = new InputLvvs0000();
    //Original name: DPRE-ELE-PREST-MAX
    private short dpreElePrestMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DPRE-TAB-PRE
    private WpreTabPrestiti[] dpreTabPre = new WpreTabPrestiti[DPRE_TAB_PRE_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-PRE
    private short ixTabPre = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0017Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dpreTabPreIdx = 1; dpreTabPreIdx <= DPRE_TAB_PRE_MAXOCCURS; dpreTabPreIdx++) {
            dpreTabPre[dpreTabPreIdx - 1] = new WpreTabPrestiti();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setDpreAreaPrestitiFormatted(String data) {
        byte[] buffer = new byte[Len.DPRE_AREA_PRESTITI];
        MarshalByte.writeString(buffer, 1, data, Len.DPRE_AREA_PRESTITI);
        setDpreAreaPrestitiBytes(buffer, 1);
    }

    public void setDpreAreaPrestitiBytes(byte[] buffer, int offset) {
        int position = offset;
        dpreElePrestMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DPRE_TAB_PRE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dpreTabPre[idx - 1].setWpreTabPrestitiBytes(buffer, position);
                position += WpreTabPrestiti.Len.WPRE_TAB_PRESTITI;
            }
            else {
                dpreTabPre[idx - 1].initWpreTabPrestitiSpaces();
                position += WpreTabPrestiti.Len.WPRE_TAB_PRESTITI;
            }
        }
    }

    public void setDpreElePrestMax(short dpreElePrestMax) {
        this.dpreElePrestMax = dpreElePrestMax;
    }

    public short getDpreElePrestMax() {
        return this.dpreElePrestMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabPre(short ixTabPre) {
        this.ixTabPre = ixTabPre;
    }

    public short getIxTabPre() {
        return this.ixTabPre;
    }

    public WpreTabPrestiti getDpreTabPre(int idx) {
        return dpreTabPre[idx - 1];
    }

    public InputLvvs0000 getInputLvvs0000() {
        return inputLvvs0000;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    public WkFindLetto getWkFindLetto() {
        return wkFindLetto;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DPRE_ELE_PREST_MAX = 2;
        public static final int DPRE_AREA_PRESTITI = DPRE_ELE_PREST_MAX + Lvvs0017Data.DPRE_TAB_PRE_MAXOCCURS * WpreTabPrestiti.Len.WPRE_TAB_PRESTITI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
