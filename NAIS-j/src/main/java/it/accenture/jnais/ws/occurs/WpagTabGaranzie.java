package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.WpagFlGarInclusaI;

/**Original name: WPAG-TAB-GARANZIE<br>
 * Variables: WPAG-TAB-GARANZIE from copybook LVEC0268<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpagTabGaranzie {

    //==== PROPERTIES ====
    //Original name: WPAG-COD-GARANZIA-I
    private String codGaranziaI = DefaultValues.stringVal(Len.COD_GARANZIA_I);
    //Original name: WPAG-FL-GAR-INCLUSA-I
    private WpagFlGarInclusaI flGarInclusaI = new WpagFlGarInclusaI();

    //==== METHODS ====
    public void setTabGaranzieBytes(byte[] buffer, int offset) {
        int position = offset;
        codGaranziaI = MarshalByte.readString(buffer, position, Len.COD_GARANZIA_I);
        position += Len.COD_GARANZIA_I;
        flGarInclusaI.setFlGarInclusaI(MarshalByte.readChar(buffer, position));
    }

    public byte[] getTabGaranzieBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codGaranziaI, Len.COD_GARANZIA_I);
        position += Len.COD_GARANZIA_I;
        MarshalByte.writeChar(buffer, position, flGarInclusaI.getFlGarInclusaI());
        return buffer;
    }

    public void initTabGaranzieSpaces() {
        codGaranziaI = "";
        flGarInclusaI.setFlGarInclusaI(Types.SPACE_CHAR);
    }

    public void setCodGaranziaI(String codGaranziaI) {
        this.codGaranziaI = Functions.subString(codGaranziaI, Len.COD_GARANZIA_I);
    }

    public String getCodGaranziaI() {
        return this.codGaranziaI;
    }

    public WpagFlGarInclusaI getFlGarInclusaI() {
        return flGarInclusaI;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_GARANZIA_I = 12;
        public static final int TAB_GARANZIE = COD_GARANZIA_I + WpagFlGarInclusaI.Len.FL_GAR_INCLUSA_I;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
