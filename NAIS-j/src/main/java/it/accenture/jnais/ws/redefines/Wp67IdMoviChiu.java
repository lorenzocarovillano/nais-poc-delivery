package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-ID-MOVI-CHIU<br>
 * Variable: WP67-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_ID_MOVI_CHIU;
    }

    public void setWp67IdMoviChiu(int wp67IdMoviChiu) {
        writeIntAsPacked(Pos.WP67_ID_MOVI_CHIU, wp67IdMoviChiu, Len.Int.WP67_ID_MOVI_CHIU);
    }

    public void setWp67IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_ID_MOVI_CHIU, Pos.WP67_ID_MOVI_CHIU);
    }

    /**Original name: WP67-ID-MOVI-CHIU<br>*/
    public int getWp67IdMoviChiu() {
        return readPackedAsInt(Pos.WP67_ID_MOVI_CHIU, Len.Int.WP67_ID_MOVI_CHIU);
    }

    public byte[] getWp67IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_ID_MOVI_CHIU, Pos.WP67_ID_MOVI_CHIU);
        return buffer;
    }

    public void setWp67IdMoviChiuNull(String wp67IdMoviChiuNull) {
        writeString(Pos.WP67_ID_MOVI_CHIU_NULL, wp67IdMoviChiuNull, Len.WP67_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WP67-ID-MOVI-CHIU-NULL<br>*/
    public String getWp67IdMoviChiuNull() {
        return readString(Pos.WP67_ID_MOVI_CHIU_NULL, Len.WP67_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_ID_MOVI_CHIU = 1;
        public static final int WP67_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_ID_MOVI_CHIU = 5;
        public static final int WP67_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
