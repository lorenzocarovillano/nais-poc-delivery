package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-PROV-RICOR<br>
 * Variable: WTDR-TOT-PROV-RICOR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_PROV_RICOR;
    }

    public void setWtdrTotProvRicor(AfDecimal wtdrTotProvRicor) {
        writeDecimalAsPacked(Pos.WTDR_TOT_PROV_RICOR, wtdrTotProvRicor.copy());
    }

    /**Original name: WTDR-TOT-PROV-RICOR<br>*/
    public AfDecimal getWtdrTotProvRicor() {
        return readPackedAsDecimal(Pos.WTDR_TOT_PROV_RICOR, Len.Int.WTDR_TOT_PROV_RICOR, Len.Fract.WTDR_TOT_PROV_RICOR);
    }

    public void setWtdrTotProvRicorNull(String wtdrTotProvRicorNull) {
        writeString(Pos.WTDR_TOT_PROV_RICOR_NULL, wtdrTotProvRicorNull, Len.WTDR_TOT_PROV_RICOR_NULL);
    }

    /**Original name: WTDR-TOT-PROV-RICOR-NULL<br>*/
    public String getWtdrTotProvRicorNull() {
        return readString(Pos.WTDR_TOT_PROV_RICOR_NULL, Len.WTDR_TOT_PROV_RICOR_NULL);
    }

    public String getWtdrTotProvRicorNullFormatted() {
        return Functions.padBlanks(getWtdrTotProvRicorNull(), Len.WTDR_TOT_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PROV_RICOR = 1;
        public static final int WTDR_TOT_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PROV_RICOR = 8;
        public static final int WTDR_TOT_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
