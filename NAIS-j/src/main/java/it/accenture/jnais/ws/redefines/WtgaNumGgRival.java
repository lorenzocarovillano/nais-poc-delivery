package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-NUM-GG-RIVAL<br>
 * Variable: WTGA-NUM-GG-RIVAL from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaNumGgRival extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaNumGgRival() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_NUM_GG_RIVAL;
    }

    public void setWtgaNumGgRival(int wtgaNumGgRival) {
        writeIntAsPacked(Pos.WTGA_NUM_GG_RIVAL, wtgaNumGgRival, Len.Int.WTGA_NUM_GG_RIVAL);
    }

    public void setWtgaNumGgRivalFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_NUM_GG_RIVAL, Pos.WTGA_NUM_GG_RIVAL);
    }

    /**Original name: WTGA-NUM-GG-RIVAL<br>*/
    public int getWtgaNumGgRival() {
        return readPackedAsInt(Pos.WTGA_NUM_GG_RIVAL, Len.Int.WTGA_NUM_GG_RIVAL);
    }

    public byte[] getWtgaNumGgRivalAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_NUM_GG_RIVAL, Pos.WTGA_NUM_GG_RIVAL);
        return buffer;
    }

    public void initWtgaNumGgRivalSpaces() {
        fill(Pos.WTGA_NUM_GG_RIVAL, Len.WTGA_NUM_GG_RIVAL, Types.SPACE_CHAR);
    }

    public void setWtgaNumGgRivalNull(String wtgaNumGgRivalNull) {
        writeString(Pos.WTGA_NUM_GG_RIVAL_NULL, wtgaNumGgRivalNull, Len.WTGA_NUM_GG_RIVAL_NULL);
    }

    /**Original name: WTGA-NUM-GG-RIVAL-NULL<br>*/
    public String getWtgaNumGgRivalNull() {
        return readString(Pos.WTGA_NUM_GG_RIVAL_NULL, Len.WTGA_NUM_GG_RIVAL_NULL);
    }

    public String getWtgaNumGgRivalNullFormatted() {
        return Functions.padBlanks(getWtgaNumGgRivalNull(), Len.WTGA_NUM_GG_RIVAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_NUM_GG_RIVAL = 1;
        public static final int WTGA_NUM_GG_RIVAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_NUM_GG_RIVAL = 3;
        public static final int WTGA_NUM_GG_RIVAL_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_NUM_GG_RIVAL = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
