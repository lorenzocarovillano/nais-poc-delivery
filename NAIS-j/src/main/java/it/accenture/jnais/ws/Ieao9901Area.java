package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IEAO9901-AREA<br>
 * Variable: IEAO9901-AREA from copybook IEAO9901<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ieao9901Area extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IEAO9901-ID-GRAVITA-ERRORE
    private String idGravitaErrore = DefaultValues.stringVal(Len.ID_GRAVITA_ERRORE);
    //Original name: IEAO9901-LIV-GRAVITA
    private int livGravita = DefaultValues.INT_VAL;
    //Original name: IEAO9901-TIPO-TRATT-FE
    private char tipoTrattFe = DefaultValues.CHAR_VAL;
    //Original name: IEAO9901-DESC-ERRORE-BREVE
    private String descErroreBreve = DefaultValues.stringVal(Len.DESC_ERRORE_BREVE);
    //Original name: IEAO9901-DESC-ERRORE-ESTESA
    private String descErroreEstesa = DefaultValues.stringVal(Len.DESC_ERRORE_ESTESA);
    /**Original name: IEAO9901-COD-ERRORE-990<br>
	 * <pre> IL CODICE ERRORE E LA LABEL DI SEGUITO INDICATI SONO INERENTI
	 *  ALL'ESITO DEL SERVIZIO DI RICERCA GRAVITA' ERRORE.</pre>*/
    private String codErrore990 = DefaultValues.stringVal(Len.COD_ERRORE990);
    //Original name: IEAO9901-LABEL-ERR-990
    private String labelErr990 = DefaultValues.stringVal(Len.LABEL_ERR990);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IEAO9901_AREA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIeao9901AreaBytes(buf);
    }

    public String getIeao9901AreaFormatted() {
        return MarshalByteExt.bufferToStr(getIeao9901AreaBytes());
    }

    public void setIeao9901AreaBytes(byte[] buffer) {
        setIeao9901AreaBytes(buffer, 1);
    }

    public byte[] getIeao9901AreaBytes() {
        byte[] buffer = new byte[Len.IEAO9901_AREA];
        return getIeao9901AreaBytes(buffer, 1);
    }

    public void setIeao9901AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        idGravitaErrore = MarshalByte.readFixedString(buffer, position, Len.ID_GRAVITA_ERRORE);
        position += Len.ID_GRAVITA_ERRORE;
        livGravita = MarshalByte.readInt(buffer, position, Len.LIV_GRAVITA);
        position += Len.LIV_GRAVITA;
        tipoTrattFe = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        descErroreBreve = MarshalByte.readString(buffer, position, Len.DESC_ERRORE_BREVE);
        position += Len.DESC_ERRORE_BREVE;
        descErroreEstesa = MarshalByte.readString(buffer, position, Len.DESC_ERRORE_ESTESA);
        position += Len.DESC_ERRORE_ESTESA;
        codErrore990 = MarshalByte.readFixedString(buffer, position, Len.COD_ERRORE990);
        position += Len.COD_ERRORE990;
        labelErr990 = MarshalByte.readString(buffer, position, Len.LABEL_ERR990);
    }

    public byte[] getIeao9901AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, idGravitaErrore, Len.ID_GRAVITA_ERRORE);
        position += Len.ID_GRAVITA_ERRORE;
        MarshalByte.writeInt(buffer, position, livGravita, Len.LIV_GRAVITA);
        position += Len.LIV_GRAVITA;
        MarshalByte.writeChar(buffer, position, tipoTrattFe);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, descErroreBreve, Len.DESC_ERRORE_BREVE);
        position += Len.DESC_ERRORE_BREVE;
        MarshalByte.writeString(buffer, position, descErroreEstesa, Len.DESC_ERRORE_ESTESA);
        position += Len.DESC_ERRORE_ESTESA;
        MarshalByte.writeString(buffer, position, codErrore990, Len.COD_ERRORE990);
        position += Len.COD_ERRORE990;
        MarshalByte.writeString(buffer, position, labelErr990, Len.LABEL_ERR990);
        return buffer;
    }

    public void setIdGravitaErrore(int idGravitaErrore) {
        this.idGravitaErrore = NumericDisplay.asString(idGravitaErrore, Len.ID_GRAVITA_ERRORE);
    }

    public void setIdGravitaErroreFormatted(String idGravitaErrore) {
        this.idGravitaErrore = Trunc.toUnsignedNumeric(idGravitaErrore, Len.ID_GRAVITA_ERRORE);
    }

    public int getIdGravitaErrore() {
        return NumericDisplay.asInt(this.idGravitaErrore);
    }

    public String getIdGravitaErroreFormatted() {
        return this.idGravitaErrore;
    }

    public void setLivGravita(int livGravita) {
        this.livGravita = livGravita;
    }

    public int getLivGravita() {
        return this.livGravita;
    }

    public void setTipoTrattFe(char tipoTrattFe) {
        this.tipoTrattFe = tipoTrattFe;
    }

    public char getTipoTrattFe() {
        return this.tipoTrattFe;
    }

    public void setDescErroreBreve(String descErroreBreve) {
        this.descErroreBreve = Functions.subString(descErroreBreve, Len.DESC_ERRORE_BREVE);
    }

    public String getDescErroreBreve() {
        return this.descErroreBreve;
    }

    public void setDescErroreEstesa(String descErroreEstesa) {
        this.descErroreEstesa = Functions.subString(descErroreEstesa, Len.DESC_ERRORE_ESTESA);
    }

    public String getDescErroreEstesa() {
        return this.descErroreEstesa;
    }

    public String getDescErroreEstesaFormatted() {
        return Functions.padBlanks(getDescErroreEstesa(), Len.DESC_ERRORE_ESTESA);
    }

    public void setCodErrore990(int codErrore990) {
        this.codErrore990 = NumericDisplay.asString(codErrore990, Len.COD_ERRORE990);
    }

    public void setCodErrore990Formatted(String codErrore990) {
        this.codErrore990 = Trunc.toUnsignedNumeric(codErrore990, Len.COD_ERRORE990);
    }

    public int getCodErrore990() {
        return NumericDisplay.asInt(this.codErrore990);
    }

    public String getCodErrore990Formatted() {
        return this.codErrore990;
    }

    public void setLabelErr990(String labelErr990) {
        this.labelErr990 = Functions.subString(labelErr990, Len.LABEL_ERR990);
    }

    public String getLabelErr990() {
        return this.labelErr990;
    }

    @Override
    public byte[] serialize() {
        return getIeao9901AreaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_GRAVITA_ERRORE = 9;
        public static final int LIV_GRAVITA = 5;
        public static final int TIPO_TRATT_FE = 1;
        public static final int DESC_ERRORE_BREVE = 100;
        public static final int DESC_ERRORE_ESTESA = 200;
        public static final int COD_ERRORE990 = 6;
        public static final int LABEL_ERR990 = 30;
        public static final int IEAO9901_AREA = ID_GRAVITA_ERRORE + LIV_GRAVITA + TIPO_TRATT_FE + DESC_ERRORE_BREVE + DESC_ERRORE_ESTESA + COD_ERRORE990 + LABEL_ERR990;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
