package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-RIDZ-TFR-SU-ANTIC<br>
 * Variable: DFA-RIDZ-TFR-SU-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaRidzTfrSuAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaRidzTfrSuAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_RIDZ_TFR_SU_ANTIC;
    }

    public void setDfaRidzTfrSuAntic(AfDecimal dfaRidzTfrSuAntic) {
        writeDecimalAsPacked(Pos.DFA_RIDZ_TFR_SU_ANTIC, dfaRidzTfrSuAntic.copy());
    }

    public void setDfaRidzTfrSuAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_RIDZ_TFR_SU_ANTIC, Pos.DFA_RIDZ_TFR_SU_ANTIC);
    }

    /**Original name: DFA-RIDZ-TFR-SU-ANTIC<br>*/
    public AfDecimal getDfaRidzTfrSuAntic() {
        return readPackedAsDecimal(Pos.DFA_RIDZ_TFR_SU_ANTIC, Len.Int.DFA_RIDZ_TFR_SU_ANTIC, Len.Fract.DFA_RIDZ_TFR_SU_ANTIC);
    }

    public byte[] getDfaRidzTfrSuAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_RIDZ_TFR_SU_ANTIC, Pos.DFA_RIDZ_TFR_SU_ANTIC);
        return buffer;
    }

    public void setDfaRidzTfrSuAnticNull(String dfaRidzTfrSuAnticNull) {
        writeString(Pos.DFA_RIDZ_TFR_SU_ANTIC_NULL, dfaRidzTfrSuAnticNull, Len.DFA_RIDZ_TFR_SU_ANTIC_NULL);
    }

    /**Original name: DFA-RIDZ-TFR-SU-ANTIC-NULL<br>*/
    public String getDfaRidzTfrSuAnticNull() {
        return readString(Pos.DFA_RIDZ_TFR_SU_ANTIC_NULL, Len.DFA_RIDZ_TFR_SU_ANTIC_NULL);
    }

    public String getDfaRidzTfrSuAnticNullFormatted() {
        return Functions.padBlanks(getDfaRidzTfrSuAnticNull(), Len.DFA_RIDZ_TFR_SU_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_RIDZ_TFR_SU_ANTIC = 1;
        public static final int DFA_RIDZ_TFR_SU_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_RIDZ_TFR_SU_ANTIC = 8;
        public static final int DFA_RIDZ_TFR_SU_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_RIDZ_TFR_SU_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_RIDZ_TFR_SU_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
