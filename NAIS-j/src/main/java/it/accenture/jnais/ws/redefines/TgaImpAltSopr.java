package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-ALT-SOPR<br>
 * Variable: TGA-IMP-ALT-SOPR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpAltSopr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpAltSopr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_ALT_SOPR;
    }

    public void setTgaImpAltSopr(AfDecimal tgaImpAltSopr) {
        writeDecimalAsPacked(Pos.TGA_IMP_ALT_SOPR, tgaImpAltSopr.copy());
    }

    public void setTgaImpAltSoprFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_ALT_SOPR, Pos.TGA_IMP_ALT_SOPR);
    }

    /**Original name: TGA-IMP-ALT-SOPR<br>*/
    public AfDecimal getTgaImpAltSopr() {
        return readPackedAsDecimal(Pos.TGA_IMP_ALT_SOPR, Len.Int.TGA_IMP_ALT_SOPR, Len.Fract.TGA_IMP_ALT_SOPR);
    }

    public byte[] getTgaImpAltSoprAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_ALT_SOPR, Pos.TGA_IMP_ALT_SOPR);
        return buffer;
    }

    public void setTgaImpAltSoprNull(String tgaImpAltSoprNull) {
        writeString(Pos.TGA_IMP_ALT_SOPR_NULL, tgaImpAltSoprNull, Len.TGA_IMP_ALT_SOPR_NULL);
    }

    /**Original name: TGA-IMP-ALT-SOPR-NULL<br>*/
    public String getTgaImpAltSoprNull() {
        return readString(Pos.TGA_IMP_ALT_SOPR_NULL, Len.TGA_IMP_ALT_SOPR_NULL);
    }

    public String getTgaImpAltSoprNullFormatted() {
        return Functions.padBlanks(getTgaImpAltSoprNull(), Len.TGA_IMP_ALT_SOPR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_ALT_SOPR = 1;
        public static final int TGA_IMP_ALT_SOPR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_ALT_SOPR = 8;
        public static final int TGA_IMP_ALT_SOPR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_ALT_SOPR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_ALT_SOPR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
