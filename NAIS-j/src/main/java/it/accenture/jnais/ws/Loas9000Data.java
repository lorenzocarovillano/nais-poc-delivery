package it.accenture.jnais.ws;

import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Iabv0004;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv0901;
import it.accenture.jnais.ws.enums.FlagFineGuide;
import it.accenture.jnais.ws.enums.FlagTpFrmAssva;
import it.accenture.jnais.ws.enums.FlagTpObl;
import it.accenture.jnais.ws.enums.FlagTpOggMbs;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS9000<br>
 * Generated as a class for rule WS.<br>*/
public class Loas9000Data {

    //==== PROPERTIES ====
    //Original name: WK-PGM
    private String wkPgm = "LOAS9000";
    //Original name: WK-PGM-GUIDA
    private String wkPgmGuida = "LDBS3540";
    //Original name: WK-PGM-BUSINESS
    private String wkPgmBusiness = "LOAS0310";
    //Original name: WK-LABEL
    private String wkLabel = "";
    /**Original name: GARANZIA<br>
	 * <pre>*****************************************************************
	 *  COSTANTI
	 * *****************************************************************</pre>*/
    private String garanzia = "GA";
    /**Original name: FLAG-TP-OBL<br>
	 * <pre>*****************************************************************
	 *  FLAGS
	 * *****************************************************************</pre>*/
    private FlagTpObl flagTpObl = new FlagTpObl();
    //Original name: FLAG-TP-OGG-MBS
    private FlagTpOggMbs flagTpOggMbs = new FlagTpOggMbs();
    //Original name: FLAG-TP-FRM-ASSVA
    private FlagTpFrmAssva flagTpFrmAssva = new FlagTpFrmAssva();
    //Original name: FLAG-FINE-GUIDE
    private FlagFineGuide flagFineGuide = new FlagFineGuide();
    /**Original name: IX-TAB-PMO<br>
	 * <pre>*****************************************************************
	 *  INDICI
	 * *****************************************************************</pre>*/
    private short ixTabPmo = ((short)0);
    /**Original name: WK-LIMITE-MAX-PMO<br>
	 * <pre>*****************************************************************
	 *  LIMITI
	 * *****************************************************************
	 * 01 WK-LIMITE-MAX-PMO               PIC S9(4) VALUE 100.</pre>*/
    private short wkLimiteMaxPmo = ((short)10);
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: PARAM-MOVI
    private ParamMoviLdbs1470 paramMovi = new ParamMoviLdbs1470();
    //Original name: IDSV0003
    private Idsv0003 idsv0003 = new Idsv0003();
    //Original name: IABV0004
    private Iabv0004 iabv0004 = new Iabv0004();
    //Original name: AREA-MAIN
    private AreaMainLoam0170 areaMain = new AreaMainLoam0170();
    //Original name: WCOM-INTERVALLO-ELAB
    private WcomIntervalloElab wcomIntervalloElab = new WcomIntervalloElab();
    //Original name: WCOM-IO-STATI
    private AreaPassaggio wcomIoStati = new AreaPassaggio();
    //Original name: IDSV0901
    private Idsv0901 idsv0901 = new Idsv0901();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getWkPgmGuida() {
        return this.wkPgmGuida;
    }

    public String getWkPgmGuidaFormatted() {
        return Functions.padBlanks(getWkPgmGuida(), Len.WK_PGM_GUIDA);
    }

    public String getWkPgmBusiness() {
        return this.wkPgmBusiness;
    }

    public String getWkPgmBusinessFormatted() {
        return Functions.padBlanks(getWkPgmBusiness(), Len.WK_PGM_BUSINESS);
    }

    public void setWkLabel(String wkLabel) {
        this.wkLabel = Functions.subString(wkLabel, Len.WK_LABEL);
    }

    public String getWkLabel() {
        return this.wkLabel;
    }

    public String getGaranzia() {
        return this.garanzia;
    }

    public void setIxTabPmo(short ixTabPmo) {
        this.ixTabPmo = ixTabPmo;
    }

    public short getIxTabPmo() {
        return this.ixTabPmo;
    }

    public short getWkLimiteMaxPmo() {
        return this.wkLimiteMaxPmo;
    }

    public AreaMainLoam0170 getAreaMain() {
        return areaMain;
    }

    public FlagFineGuide getFlagFineGuide() {
        return flagFineGuide;
    }

    public FlagTpFrmAssva getFlagTpFrmAssva() {
        return flagTpFrmAssva;
    }

    public FlagTpObl getFlagTpObl() {
        return flagTpObl;
    }

    public FlagTpOggMbs getFlagTpOggMbs() {
        return flagTpOggMbs;
    }

    public Iabv0004 getIabv0004() {
        return iabv0004;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv0003 getIdsv0003() {
        return idsv0003;
    }

    public Idsv0901 getIdsv0901() {
        return idsv0901;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public ParamMoviLdbs1470 getParamMovi() {
        return paramMovi;
    }

    public WcomIntervalloElab getWcomIntervalloElab() {
        return wcomIntervalloElab;
    }

    public AreaPassaggio getWcomIoStati() {
        return wcomIoStati;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_LABEL = 30;
        public static final int WK_PGM_GUIDA = 8;
        public static final int WK_PGM_BUSINESS = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
