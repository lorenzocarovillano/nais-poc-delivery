package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WspgTabSpg;

/**Original name: WSPG-AREA-SOPR-GAR<br>
 * Variable: WSPG-AREA-SOPR-GAR from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WspgAreaSoprGar extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_SPG_MAXOCCURS = 20;
    //Original name: WSPG-ELE-SOPR-GAR-MAX
    private short eleSoprGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WSPG-TAB-SPG
    private WspgTabSpg[] tabSpg = new WspgTabSpg[TAB_SPG_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WspgAreaSoprGar() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSPG_AREA_SOPR_GAR;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWspgAreaSoprGarBytes(buf);
    }

    public void init() {
        for (int tabSpgIdx = 1; tabSpgIdx <= TAB_SPG_MAXOCCURS; tabSpgIdx++) {
            tabSpg[tabSpgIdx - 1] = new WspgTabSpg();
        }
    }

    public String getWspgAreaSoprapGarFormatted() {
        return MarshalByteExt.bufferToStr(getWspgAreaSoprGarBytes());
    }

    public void setWspgAreaSoprGarBytes(byte[] buffer) {
        setWspgAreaSoprGarBytes(buffer, 1);
    }

    public byte[] getWspgAreaSoprGarBytes() {
        byte[] buffer = new byte[Len.WSPG_AREA_SOPR_GAR];
        return getWspgAreaSoprGarBytes(buffer, 1);
    }

    public void setWspgAreaSoprGarBytes(byte[] buffer, int offset) {
        int position = offset;
        eleSoprGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_SPG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabSpg[idx - 1].setWspgTabSpgBytes(buffer, position);
                position += WspgTabSpg.Len.WSPG_TAB_SPG;
            }
            else {
                tabSpg[idx - 1].initWspgTabSpgSpaces();
                position += WspgTabSpg.Len.WSPG_TAB_SPG;
            }
        }
    }

    public byte[] getWspgAreaSoprGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleSoprGarMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_SPG_MAXOCCURS; idx++) {
            tabSpg[idx - 1].getWspgTabSpgBytes(buffer, position);
            position += WspgTabSpg.Len.WSPG_TAB_SPG;
        }
        return buffer;
    }

    public void setEleSoprGarMax(short eleSoprGarMax) {
        this.eleSoprGarMax = eleSoprGarMax;
    }

    public short getEleSoprGarMax() {
        return this.eleSoprGarMax;
    }

    public WspgTabSpg getTabSpg(int idx) {
        return tabSpg[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWspgAreaSoprGarBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_SOPR_GAR_MAX = 2;
        public static final int WSPG_AREA_SOPR_GAR = ELE_SOPR_GAR_MAX + WspgAreaSoprGar.TAB_SPG_MAXOCCURS * WspgTabSpg.Len.WSPG_TAB_SPG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
