package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WADE-DT-VARZ-TP-IAS<br>
 * Variable: WADE-DT-VARZ-TP-IAS from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeDtVarzTpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeDtVarzTpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_DT_VARZ_TP_IAS;
    }

    public void setWadeDtVarzTpIas(int wadeDtVarzTpIas) {
        writeIntAsPacked(Pos.WADE_DT_VARZ_TP_IAS, wadeDtVarzTpIas, Len.Int.WADE_DT_VARZ_TP_IAS);
    }

    public void setWadeDtVarzTpIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_DT_VARZ_TP_IAS, Pos.WADE_DT_VARZ_TP_IAS);
    }

    /**Original name: WADE-DT-VARZ-TP-IAS<br>*/
    public int getWadeDtVarzTpIas() {
        return readPackedAsInt(Pos.WADE_DT_VARZ_TP_IAS, Len.Int.WADE_DT_VARZ_TP_IAS);
    }

    public byte[] getWadeDtVarzTpIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_DT_VARZ_TP_IAS, Pos.WADE_DT_VARZ_TP_IAS);
        return buffer;
    }

    public void initWadeDtVarzTpIasSpaces() {
        fill(Pos.WADE_DT_VARZ_TP_IAS, Len.WADE_DT_VARZ_TP_IAS, Types.SPACE_CHAR);
    }

    public void setWadeDtVarzTpIasNull(String wadeDtVarzTpIasNull) {
        writeString(Pos.WADE_DT_VARZ_TP_IAS_NULL, wadeDtVarzTpIasNull, Len.WADE_DT_VARZ_TP_IAS_NULL);
    }

    /**Original name: WADE-DT-VARZ-TP-IAS-NULL<br>*/
    public String getWadeDtVarzTpIasNull() {
        return readString(Pos.WADE_DT_VARZ_TP_IAS_NULL, Len.WADE_DT_VARZ_TP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_DT_VARZ_TP_IAS = 1;
        public static final int WADE_DT_VARZ_TP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_DT_VARZ_TP_IAS = 5;
        public static final int WADE_DT_VARZ_TP_IAS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_DT_VARZ_TP_IAS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
