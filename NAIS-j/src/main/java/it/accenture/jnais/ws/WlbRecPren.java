package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WLB-REC-PREN<br>
 * Variable: WLB-REC-PREN from copybook LLBO0261<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WlbRecPren extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WLB-ID-RICH
    private String idRich = DefaultValues.stringVal(Len.ID_RICH);
    //Original name: WLB-ID-RICH-COLL
    private String idRichColl = DefaultValues.stringVal(Len.ID_RICH_COLL);
    //Original name: WLB-ID-RICH-CONSOLID
    private String idRichConsolid = DefaultValues.stringVal(Len.ID_RICH_CONSOLID);
    //Original name: WLB-DT-RISERVA
    private String dtRiserva = DefaultValues.stringVal(Len.DT_RISERVA);
    //Original name: WLB-DT-PRODUZIONE
    private String dtProduzione = DefaultValues.stringVal(Len.DT_PRODUZIONE);
    //Original name: WLB-DT-ESECUZIONE
    private String dtEsecuzione = DefaultValues.stringVal(Len.DT_ESECUZIONE);
    //Original name: WLB-DT-ANNULLO
    private String dtAnnullo = DefaultValues.stringVal(Len.DT_ANNULLO);
    //Original name: WLB-DT-CERTIFICAZ
    private String dtCertificaz = DefaultValues.stringVal(Len.DT_CERTIFICAZ);
    //Original name: WLB-DT-CONSOLID
    private String dtConsolid = DefaultValues.stringVal(Len.DT_CONSOLID);
    //Original name: WLB-TP-FRM-ASSVA
    private String tpFrmAssva = DefaultValues.stringVal(Len.TP_FRM_ASSVA);
    //Original name: WLB-RAMO-BILA
    private String ramoBila = DefaultValues.stringVal(Len.RAMO_BILA);
    //Original name: WLB-COD-PROD
    private String codProd = DefaultValues.stringVal(Len.COD_PROD);
    //Original name: WLB-IB-POLI-FIRST
    private String ibPoliFirst = DefaultValues.stringVal(Len.IB_POLI_FIRST);
    //Original name: WLB-IB-POLI-LAST
    private String ibPoliLast = DefaultValues.stringVal(Len.IB_POLI_LAST);
    //Original name: WLB-IB-ADE-FIRST
    private String ibAdeFirst = DefaultValues.stringVal(Len.IB_ADE_FIRST);
    //Original name: WLB-IB-ADE-LAST
    private String ibAdeLast = DefaultValues.stringVal(Len.IB_ADE_LAST);
    //Original name: WLB-TP-CALC-RIS
    private String tpCalcRis = DefaultValues.stringVal(Len.TP_CALC_RIS);
    //Original name: WLB-TP-RICH
    private String tpRich = DefaultValues.stringVal(Len.TP_RICH);
    //Original name: WLB-TS-COMPETENZA
    private String tsCompetenza = DefaultValues.stringVal(Len.TS_COMPETENZA);
    //Original name: WLB-FL-SIMULAZIONE
    private char flSimulazione = DefaultValues.CHAR_VAL;
    //Original name: WLB-TP-INVST
    private String tpInvst = DefaultValues.stringVal(Len.TP_INVST);
    //Original name: WLB-FL-CALCOLO-A-DATA-FUTURA
    private char flCalcoloADataFutura = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WLB_REC_PREN;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWlbRecPrenBytes(buf);
    }

    public void setWlbRecPrenFormatted(String data) {
        byte[] buffer = new byte[Len.WLB_REC_PREN];
        MarshalByte.writeString(buffer, 1, data, Len.WLB_REC_PREN);
        setWlbRecPrenBytes(buffer, 1);
    }

    public String getWlbRecPrenFormatted() {
        return MarshalByteExt.bufferToStr(getWlbRecPrenBytes());
    }

    public void setWlbRecPrenBytes(byte[] buffer) {
        setWlbRecPrenBytes(buffer, 1);
    }

    public byte[] getWlbRecPrenBytes() {
        byte[] buffer = new byte[Len.WLB_REC_PREN];
        return getWlbRecPrenBytes(buffer, 1);
    }

    public void setWlbRecPrenBytes(byte[] buffer, int offset) {
        int position = offset;
        idRich = MarshalByte.readFixedString(buffer, position, Len.ID_RICH);
        position += Len.ID_RICH;
        idRichColl = MarshalByte.readFixedString(buffer, position, Len.ID_RICH_COLL);
        position += Len.ID_RICH_COLL;
        idRichConsolid = MarshalByte.readFixedString(buffer, position, Len.ID_RICH_CONSOLID);
        position += Len.ID_RICH_CONSOLID;
        dtRiserva = MarshalByte.readFixedString(buffer, position, Len.DT_RISERVA);
        position += Len.DT_RISERVA;
        dtProduzione = MarshalByte.readFixedString(buffer, position, Len.DT_PRODUZIONE);
        position += Len.DT_PRODUZIONE;
        dtEsecuzione = MarshalByte.readFixedString(buffer, position, Len.DT_ESECUZIONE);
        position += Len.DT_ESECUZIONE;
        dtAnnullo = MarshalByte.readFixedString(buffer, position, Len.DT_ANNULLO);
        position += Len.DT_ANNULLO;
        dtCertificaz = MarshalByte.readFixedString(buffer, position, Len.DT_CERTIFICAZ);
        position += Len.DT_CERTIFICAZ;
        dtConsolid = MarshalByte.readFixedString(buffer, position, Len.DT_CONSOLID);
        position += Len.DT_CONSOLID;
        tpFrmAssva = MarshalByte.readString(buffer, position, Len.TP_FRM_ASSVA);
        position += Len.TP_FRM_ASSVA;
        ramoBila = MarshalByte.readString(buffer, position, Len.RAMO_BILA);
        position += Len.RAMO_BILA;
        codProd = MarshalByte.readString(buffer, position, Len.COD_PROD);
        position += Len.COD_PROD;
        ibPoliFirst = MarshalByte.readString(buffer, position, Len.IB_POLI_FIRST);
        position += Len.IB_POLI_FIRST;
        ibPoliLast = MarshalByte.readString(buffer, position, Len.IB_POLI_LAST);
        position += Len.IB_POLI_LAST;
        ibAdeFirst = MarshalByte.readString(buffer, position, Len.IB_ADE_FIRST);
        position += Len.IB_ADE_FIRST;
        ibAdeLast = MarshalByte.readString(buffer, position, Len.IB_ADE_LAST);
        position += Len.IB_ADE_LAST;
        tpCalcRis = MarshalByte.readString(buffer, position, Len.TP_CALC_RIS);
        position += Len.TP_CALC_RIS;
        tpRich = MarshalByte.readString(buffer, position, Len.TP_RICH);
        position += Len.TP_RICH;
        tsCompetenza = MarshalByte.readFixedString(buffer, position, Len.TS_COMPETENZA);
        position += Len.TS_COMPETENZA;
        flSimulazione = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpInvst = MarshalByte.readFixedString(buffer, position, Len.TP_INVST);
        position += Len.TP_INVST;
        flCalcoloADataFutura = MarshalByte.readChar(buffer, position);
    }

    public byte[] getWlbRecPrenBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, idRich, Len.ID_RICH);
        position += Len.ID_RICH;
        MarshalByte.writeString(buffer, position, idRichColl, Len.ID_RICH_COLL);
        position += Len.ID_RICH_COLL;
        MarshalByte.writeString(buffer, position, idRichConsolid, Len.ID_RICH_CONSOLID);
        position += Len.ID_RICH_CONSOLID;
        MarshalByte.writeString(buffer, position, dtRiserva, Len.DT_RISERVA);
        position += Len.DT_RISERVA;
        MarshalByte.writeString(buffer, position, dtProduzione, Len.DT_PRODUZIONE);
        position += Len.DT_PRODUZIONE;
        MarshalByte.writeString(buffer, position, dtEsecuzione, Len.DT_ESECUZIONE);
        position += Len.DT_ESECUZIONE;
        MarshalByte.writeString(buffer, position, dtAnnullo, Len.DT_ANNULLO);
        position += Len.DT_ANNULLO;
        MarshalByte.writeString(buffer, position, dtCertificaz, Len.DT_CERTIFICAZ);
        position += Len.DT_CERTIFICAZ;
        MarshalByte.writeString(buffer, position, dtConsolid, Len.DT_CONSOLID);
        position += Len.DT_CONSOLID;
        MarshalByte.writeString(buffer, position, tpFrmAssva, Len.TP_FRM_ASSVA);
        position += Len.TP_FRM_ASSVA;
        MarshalByte.writeString(buffer, position, ramoBila, Len.RAMO_BILA);
        position += Len.RAMO_BILA;
        MarshalByte.writeString(buffer, position, codProd, Len.COD_PROD);
        position += Len.COD_PROD;
        MarshalByte.writeString(buffer, position, ibPoliFirst, Len.IB_POLI_FIRST);
        position += Len.IB_POLI_FIRST;
        MarshalByte.writeString(buffer, position, ibPoliLast, Len.IB_POLI_LAST);
        position += Len.IB_POLI_LAST;
        MarshalByte.writeString(buffer, position, ibAdeFirst, Len.IB_ADE_FIRST);
        position += Len.IB_ADE_FIRST;
        MarshalByte.writeString(buffer, position, ibAdeLast, Len.IB_ADE_LAST);
        position += Len.IB_ADE_LAST;
        MarshalByte.writeString(buffer, position, tpCalcRis, Len.TP_CALC_RIS);
        position += Len.TP_CALC_RIS;
        MarshalByte.writeString(buffer, position, tpRich, Len.TP_RICH);
        position += Len.TP_RICH;
        MarshalByte.writeString(buffer, position, tsCompetenza, Len.TS_COMPETENZA);
        position += Len.TS_COMPETENZA;
        MarshalByte.writeChar(buffer, position, flSimulazione);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpInvst, Len.TP_INVST);
        position += Len.TP_INVST;
        MarshalByte.writeChar(buffer, position, flCalcoloADataFutura);
        return buffer;
    }

    public void initWlbRecPrenSpaces() {
        idRich = "";
        idRichColl = "";
        idRichConsolid = "";
        dtRiserva = "";
        dtProduzione = "";
        dtEsecuzione = "";
        dtAnnullo = "";
        dtCertificaz = "";
        dtConsolid = "";
        tpFrmAssva = "";
        ramoBila = "";
        codProd = "";
        ibPoliFirst = "";
        ibPoliLast = "";
        ibAdeFirst = "";
        ibAdeLast = "";
        tpCalcRis = "";
        tpRich = "";
        tsCompetenza = "";
        flSimulazione = Types.SPACE_CHAR;
        tpInvst = "";
        flCalcoloADataFutura = Types.SPACE_CHAR;
    }

    public void setIdRich(int idRich) {
        this.idRich = NumericDisplay.asString(idRich, Len.ID_RICH);
    }

    public void setIdRichFormatted(String idRich) {
        this.idRich = Trunc.toUnsignedNumeric(idRich, Len.ID_RICH);
    }

    public int getIdRich() {
        return NumericDisplay.asInt(this.idRich);
    }

    public void setIdRichCollFormatted(String idRichColl) {
        this.idRichColl = Trunc.toUnsignedNumeric(idRichColl, Len.ID_RICH_COLL);
    }

    public int getIdRichColl() {
        return NumericDisplay.asInt(this.idRichColl);
    }

    public void setIdRichConsolidFormatted(String idRichConsolid) {
        this.idRichConsolid = Trunc.toUnsignedNumeric(idRichConsolid, Len.ID_RICH_CONSOLID);
    }

    public int getIdRichConsolid() {
        return NumericDisplay.asInt(this.idRichConsolid);
    }

    public void setDtRiservaFormatted(String dtRiserva) {
        this.dtRiserva = Trunc.toUnsignedNumeric(dtRiserva, Len.DT_RISERVA);
    }

    public int getDtRiserva() {
        return NumericDisplay.asInt(this.dtRiserva);
    }

    public String getDtRiservaFormatted() {
        return this.dtRiserva;
    }

    public void setDtProduzioneFormatted(String dtProduzione) {
        this.dtProduzione = Trunc.toUnsignedNumeric(dtProduzione, Len.DT_PRODUZIONE);
    }

    public int getDtProduzione() {
        return NumericDisplay.asInt(this.dtProduzione);
    }

    public String getDtProduzioneFormatted() {
        return this.dtProduzione;
    }

    public void setDtEsecuzioneFormatted(String dtEsecuzione) {
        this.dtEsecuzione = Trunc.toUnsignedNumeric(dtEsecuzione, Len.DT_ESECUZIONE);
    }

    public int getDtEsecuzione() {
        return NumericDisplay.asInt(this.dtEsecuzione);
    }

    public void setDtAnnulloFormatted(String dtAnnullo) {
        this.dtAnnullo = Trunc.toUnsignedNumeric(dtAnnullo, Len.DT_ANNULLO);
    }

    public int getDtAnnullo() {
        return NumericDisplay.asInt(this.dtAnnullo);
    }

    public void setDtCertificazFormatted(String dtCertificaz) {
        this.dtCertificaz = Trunc.toUnsignedNumeric(dtCertificaz, Len.DT_CERTIFICAZ);
    }

    public int getDtCertificaz() {
        return NumericDisplay.asInt(this.dtCertificaz);
    }

    public void setDtConsolidFormatted(String dtConsolid) {
        this.dtConsolid = Trunc.toUnsignedNumeric(dtConsolid, Len.DT_CONSOLID);
    }

    public int getDtConsolid() {
        return NumericDisplay.asInt(this.dtConsolid);
    }

    public void setTpFrmAssva(String tpFrmAssva) {
        this.tpFrmAssva = Functions.subString(tpFrmAssva, Len.TP_FRM_ASSVA);
    }

    public String getTpFrmAssva() {
        return this.tpFrmAssva;
    }

    public void setRamoBila(String ramoBila) {
        this.ramoBila = Functions.subString(ramoBila, Len.RAMO_BILA);
    }

    public String getRamoBila() {
        return this.ramoBila;
    }

    public void setCodProd(String codProd) {
        this.codProd = Functions.subString(codProd, Len.COD_PROD);
    }

    public String getCodProd() {
        return this.codProd;
    }

    public String getCodProdFormatted() {
        return Functions.padBlanks(getCodProd(), Len.COD_PROD);
    }

    public void setIbPoliFirst(String ibPoliFirst) {
        this.ibPoliFirst = Functions.subString(ibPoliFirst, Len.IB_POLI_FIRST);
    }

    public String getIbPoliFirst() {
        return this.ibPoliFirst;
    }

    public void setIbPoliLast(String ibPoliLast) {
        this.ibPoliLast = Functions.subString(ibPoliLast, Len.IB_POLI_LAST);
    }

    public String getIbPoliLast() {
        return this.ibPoliLast;
    }

    public void setIbAdeFirst(String ibAdeFirst) {
        this.ibAdeFirst = Functions.subString(ibAdeFirst, Len.IB_ADE_FIRST);
    }

    public String getIbAdeFirst() {
        return this.ibAdeFirst;
    }

    public void setIbAdeLast(String ibAdeLast) {
        this.ibAdeLast = Functions.subString(ibAdeLast, Len.IB_ADE_LAST);
    }

    public String getIbAdeLast() {
        return this.ibAdeLast;
    }

    public void setTpCalcRis(String tpCalcRis) {
        this.tpCalcRis = Functions.subString(tpCalcRis, Len.TP_CALC_RIS);
    }

    public String getTpCalcRis() {
        return this.tpCalcRis;
    }

    public void setTpRich(String tpRich) {
        this.tpRich = Functions.subString(tpRich, Len.TP_RICH);
    }

    public String getTpRich() {
        return this.tpRich;
    }

    public void setTsCompetenza(long tsCompetenza) {
        this.tsCompetenza = NumericDisplay.asString(tsCompetenza, Len.TS_COMPETENZA);
    }

    public long getTsCompetenza() {
        return NumericDisplay.asLong(this.tsCompetenza);
    }

    public String getTsCompetenzaFormatted() {
        return this.tsCompetenza;
    }

    public void setFlSimulazione(char flSimulazione) {
        this.flSimulazione = flSimulazione;
    }

    public char getFlSimulazione() {
        return this.flSimulazione;
    }

    public void setTpInvstFormatted(String tpInvst) {
        this.tpInvst = Trunc.toUnsignedNumeric(tpInvst, Len.TP_INVST);
    }

    public short getTpInvst() {
        return NumericDisplay.asShort(this.tpInvst);
    }

    public void setFlCalcoloADataFutura(char flCalcoloADataFutura) {
        this.flCalcoloADataFutura = flCalcoloADataFutura;
    }

    public char getFlCalcoloADataFutura() {
        return this.flCalcoloADataFutura;
    }

    @Override
    public byte[] serialize() {
        return getWlbRecPrenBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_RICH = 9;
        public static final int ID_RICH_COLL = 9;
        public static final int ID_RICH_CONSOLID = 9;
        public static final int DT_RISERVA = 8;
        public static final int DT_PRODUZIONE = 8;
        public static final int DT_ESECUZIONE = 8;
        public static final int DT_ANNULLO = 8;
        public static final int DT_CERTIFICAZ = 8;
        public static final int DT_CONSOLID = 8;
        public static final int TP_FRM_ASSVA = 2;
        public static final int RAMO_BILA = 12;
        public static final int COD_PROD = 12;
        public static final int IB_POLI_FIRST = 40;
        public static final int IB_POLI_LAST = 40;
        public static final int IB_ADE_FIRST = 40;
        public static final int IB_ADE_LAST = 40;
        public static final int TP_CALC_RIS = 2;
        public static final int TP_RICH = 2;
        public static final int TS_COMPETENZA = 18;
        public static final int FL_SIMULAZIONE = 1;
        public static final int TP_INVST = 2;
        public static final int FL_CALCOLO_A_DATA_FUTURA = 1;
        public static final int WLB_REC_PREN = ID_RICH + ID_RICH_COLL + ID_RICH_CONSOLID + DT_RISERVA + DT_PRODUZIONE + DT_ESECUZIONE + DT_ANNULLO + DT_CERTIFICAZ + DT_CONSOLID + TP_FRM_ASSVA + RAMO_BILA + COD_PROD + IB_POLI_FIRST + IB_POLI_LAST + IB_ADE_FIRST + IB_ADE_LAST + TP_CALC_RIS + TP_RICH + TS_COMPETENZA + FL_SIMULAZIONE + TP_INVST + FL_CALCOLO_A_DATA_FUTURA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
