package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPB-VIS-1382011<br>
 * Variable: TCL-IMPB-VIS-1382011 from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpbVis1382011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpbVis1382011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPB_VIS1382011;
    }

    public void setTclImpbVis1382011(AfDecimal tclImpbVis1382011) {
        writeDecimalAsPacked(Pos.TCL_IMPB_VIS1382011, tclImpbVis1382011.copy());
    }

    public void setTclImpbVis1382011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPB_VIS1382011, Pos.TCL_IMPB_VIS1382011);
    }

    /**Original name: TCL-IMPB-VIS-1382011<br>*/
    public AfDecimal getTclImpbVis1382011() {
        return readPackedAsDecimal(Pos.TCL_IMPB_VIS1382011, Len.Int.TCL_IMPB_VIS1382011, Len.Fract.TCL_IMPB_VIS1382011);
    }

    public byte[] getTclImpbVis1382011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPB_VIS1382011, Pos.TCL_IMPB_VIS1382011);
        return buffer;
    }

    public void setTclImpbVis1382011Null(String tclImpbVis1382011Null) {
        writeString(Pos.TCL_IMPB_VIS1382011_NULL, tclImpbVis1382011Null, Len.TCL_IMPB_VIS1382011_NULL);
    }

    /**Original name: TCL-IMPB-VIS-1382011-NULL<br>*/
    public String getTclImpbVis1382011Null() {
        return readString(Pos.TCL_IMPB_VIS1382011_NULL, Len.TCL_IMPB_VIS1382011_NULL);
    }

    public String getTclImpbVis1382011NullFormatted() {
        return Functions.padBlanks(getTclImpbVis1382011Null(), Len.TCL_IMPB_VIS1382011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_VIS1382011 = 1;
        public static final int TCL_IMPB_VIS1382011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_VIS1382011 = 8;
        public static final int TCL_IMPB_VIS1382011_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_VIS1382011 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_VIS1382011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
