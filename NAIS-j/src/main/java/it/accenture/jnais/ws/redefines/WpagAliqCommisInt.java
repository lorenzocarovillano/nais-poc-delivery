package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-ALIQ-COMMIS-INT<br>
 * Variable: WPAG-ALIQ-COMMIS-INT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagAliqCommisInt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagAliqCommisInt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_ALIQ_COMMIS_INT;
    }

    public void setWpagAliqCommisInt(AfDecimal wpagAliqCommisInt) {
        writeDecimalAsPacked(Pos.WPAG_ALIQ_COMMIS_INT, wpagAliqCommisInt.copy());
    }

    public void setWpagAliqCommisIntFormatted(String wpagAliqCommisInt) {
        setWpagAliqCommisInt(PicParser.display(new PicParams("S9(5)V9(9)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_ALIQ_COMMIS_INT + Len.Fract.WPAG_ALIQ_COMMIS_INT, Len.Fract.WPAG_ALIQ_COMMIS_INT, wpagAliqCommisInt));
    }

    public void setWpagAliqCommisIntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_ALIQ_COMMIS_INT, Pos.WPAG_ALIQ_COMMIS_INT);
    }

    /**Original name: WPAG-ALIQ-COMMIS-INT<br>*/
    public AfDecimal getWpagAliqCommisInt() {
        return readPackedAsDecimal(Pos.WPAG_ALIQ_COMMIS_INT, Len.Int.WPAG_ALIQ_COMMIS_INT, Len.Fract.WPAG_ALIQ_COMMIS_INT);
    }

    public byte[] getWpagAliqCommisIntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_ALIQ_COMMIS_INT, Pos.WPAG_ALIQ_COMMIS_INT);
        return buffer;
    }

    public void initWpagAliqCommisIntSpaces() {
        fill(Pos.WPAG_ALIQ_COMMIS_INT, Len.WPAG_ALIQ_COMMIS_INT, Types.SPACE_CHAR);
    }

    public void setWpagAliqCommisIntNull(String wpagAliqCommisIntNull) {
        writeString(Pos.WPAG_ALIQ_COMMIS_INT_NULL, wpagAliqCommisIntNull, Len.WPAG_ALIQ_COMMIS_INT_NULL);
    }

    /**Original name: WPAG-ALIQ-COMMIS-INT-NULL<br>*/
    public String getWpagAliqCommisIntNull() {
        return readString(Pos.WPAG_ALIQ_COMMIS_INT_NULL, Len.WPAG_ALIQ_COMMIS_INT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_ALIQ_COMMIS_INT = 1;
        public static final int WPAG_ALIQ_COMMIS_INT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_ALIQ_COMMIS_INT = 8;
        public static final int WPAG_ALIQ_COMMIS_INT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_ALIQ_COMMIS_INT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_ALIQ_COMMIS_INT = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
