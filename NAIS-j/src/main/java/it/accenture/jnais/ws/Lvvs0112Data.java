package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvpco1;
import it.accenture.jnais.copy.WpcoDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0112<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0112Data {

    //==== PROPERTIES ====
    //Original name: DPCO-ELE-PCO-MAX
    private short dpcoElePcoMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPCO1
    private Lccvpco1 lccvpco1 = new Lccvpco1();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-RAN
    private short ixTabRan = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setDpcoAreaPcoFormatted(String data) {
        byte[] buffer = new byte[Len.DPCO_AREA_PCO];
        MarshalByte.writeString(buffer, 1, data, Len.DPCO_AREA_PCO);
        setDpcoAreaPcoBytes(buffer, 1);
    }

    public void setDpcoAreaPcoBytes(byte[] buffer, int offset) {
        int position = offset;
        dpcoElePcoMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDpcoTabPcoBytes(buffer, position);
    }

    public void setDpcoElePcoMax(short dpcoElePcoMax) {
        this.dpcoElePcoMax = dpcoElePcoMax;
    }

    public short getDpcoElePcoMax() {
        return this.dpcoElePcoMax;
    }

    public void setDpcoTabPcoBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpco1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpco1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabRan(short ixTabRan) {
        this.ixTabRan = ixTabRan;
    }

    public short getIxTabRan() {
        return this.ixTabRan;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvpco1 getLccvpco1() {
        return lccvpco1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DPCO_ELE_PCO_MAX = 2;
        public static final int DPCO_TAB_PCO = WpolStatus.Len.STATUS + WpcoDati.Len.DATI;
        public static final int DPCO_AREA_PCO = DPCO_ELE_PCO_MAX + DPCO_TAB_PCO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
