package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccve121;
import it.accenture.jnais.copy.We12Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WE12-TAB-EST-TRCH<br>
 * Variables: WE12-TAB-EST-TRCH from copybook LCCVE12B<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class We12TabEstTrch {

    //==== PROPERTIES ====
    //Original name: LCCVE121
    private Lccve121 lccve121 = new Lccve121();

    //==== METHODS ====
    public byte[] getWe12TabEstTrchBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccve121.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccve121.getIdPtf(), Lccve121.Len.Int.ID_PTF, 0);
        position += Lccve121.Len.ID_PTF;
        lccve121.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public Lccve121 getLccve121() {
        return lccve121;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WE12_TAB_EST_TRCH = WpolStatus.Len.STATUS + Lccve121.Len.ID_PTF + We12Dati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
