package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-TS-PP<br>
 * Variable: B03-TS-PP from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03TsPp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03TsPp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_TS_PP;
    }

    public void setB03TsPp(AfDecimal b03TsPp) {
        writeDecimalAsPacked(Pos.B03_TS_PP, b03TsPp.copy());
    }

    public void setB03TsPpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_TS_PP, Pos.B03_TS_PP);
    }

    /**Original name: B03-TS-PP<br>*/
    public AfDecimal getB03TsPp() {
        return readPackedAsDecimal(Pos.B03_TS_PP, Len.Int.B03_TS_PP, Len.Fract.B03_TS_PP);
    }

    public byte[] getB03TsPpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_TS_PP, Pos.B03_TS_PP);
        return buffer;
    }

    public void setB03TsPpNull(String b03TsPpNull) {
        writeString(Pos.B03_TS_PP_NULL, b03TsPpNull, Len.B03_TS_PP_NULL);
    }

    /**Original name: B03-TS-PP-NULL<br>*/
    public String getB03TsPpNull() {
        return readString(Pos.B03_TS_PP_NULL, Len.B03_TS_PP_NULL);
    }

    public String getB03TsPpNullFormatted() {
        return Functions.padBlanks(getB03TsPpNull(), Len.B03_TS_PP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_TS_PP = 1;
        public static final int B03_TS_PP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_TS_PP = 8;
        public static final int B03_TS_PP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_TS_PP = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_TS_PP = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
