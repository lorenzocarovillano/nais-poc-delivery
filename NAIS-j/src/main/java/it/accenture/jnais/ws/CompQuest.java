package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.Q04CodCan;

/**Original name: COMP-QUEST<br>
 * Variable: COMP-QUEST from copybook IDBVQ041<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class CompQuest extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: Q04-ID-COMP-QUEST
    private int q04IdCompQuest = DefaultValues.INT_VAL;
    //Original name: Q04-DT-INI-EFF
    private int q04DtIniEff = DefaultValues.INT_VAL;
    //Original name: Q04-COD-COMP-ANIA
    private int q04CodCompAnia = DefaultValues.INT_VAL;
    //Original name: Q04-COD-CAN
    private Q04CodCan q04CodCan = new Q04CodCan();
    //Original name: Q04-DT-END-EFF
    private int q04DtEndEff = DefaultValues.INT_VAL;
    //Original name: Q04-COD-QUEST
    private String q04CodQuest = DefaultValues.stringVal(Len.Q04_COD_QUEST);
    //Original name: Q04-COD-DOMANDA
    private String q04CodDomanda = DefaultValues.stringVal(Len.Q04_COD_DOMANDA);
    //Original name: Q04-ORDINE-DOMANDA
    private int q04OrdineDomanda = DefaultValues.INT_VAL;
    //Original name: Q04-COD-RISP
    private String q04CodRisp = DefaultValues.stringVal(Len.Q04_COD_RISP);
    //Original name: Q04-OBBL-RISP
    private char q04ObblRisp = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.COMP_QUEST;
    }

    @Override
    public void deserialize(byte[] buf) {
        setCompQuestBytes(buf);
    }

    public String getCompQuestFormatted() {
        return MarshalByteExt.bufferToStr(getCompQuestBytes());
    }

    public void setCompQuestBytes(byte[] buffer) {
        setCompQuestBytes(buffer, 1);
    }

    public byte[] getCompQuestBytes() {
        byte[] buffer = new byte[Len.COMP_QUEST];
        return getCompQuestBytes(buffer, 1);
    }

    public void setCompQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        q04IdCompQuest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.Q04_ID_COMP_QUEST, 0);
        position += Len.Q04_ID_COMP_QUEST;
        q04DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.Q04_DT_INI_EFF, 0);
        position += Len.Q04_DT_INI_EFF;
        q04CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.Q04_COD_COMP_ANIA, 0);
        position += Len.Q04_COD_COMP_ANIA;
        q04CodCan.setQ04CodCanFromBuffer(buffer, position);
        position += Q04CodCan.Len.Q04_COD_CAN;
        q04DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.Q04_DT_END_EFF, 0);
        position += Len.Q04_DT_END_EFF;
        q04CodQuest = MarshalByte.readString(buffer, position, Len.Q04_COD_QUEST);
        position += Len.Q04_COD_QUEST;
        q04CodDomanda = MarshalByte.readString(buffer, position, Len.Q04_COD_DOMANDA);
        position += Len.Q04_COD_DOMANDA;
        q04OrdineDomanda = MarshalByte.readPackedAsInt(buffer, position, Len.Int.Q04_ORDINE_DOMANDA, 0);
        position += Len.Q04_ORDINE_DOMANDA;
        q04CodRisp = MarshalByte.readString(buffer, position, Len.Q04_COD_RISP);
        position += Len.Q04_COD_RISP;
        q04ObblRisp = MarshalByte.readChar(buffer, position);
    }

    public byte[] getCompQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, q04IdCompQuest, Len.Int.Q04_ID_COMP_QUEST, 0);
        position += Len.Q04_ID_COMP_QUEST;
        MarshalByte.writeIntAsPacked(buffer, position, q04DtIniEff, Len.Int.Q04_DT_INI_EFF, 0);
        position += Len.Q04_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, q04CodCompAnia, Len.Int.Q04_COD_COMP_ANIA, 0);
        position += Len.Q04_COD_COMP_ANIA;
        q04CodCan.getQ04CodCanAsBuffer(buffer, position);
        position += Q04CodCan.Len.Q04_COD_CAN;
        MarshalByte.writeIntAsPacked(buffer, position, q04DtEndEff, Len.Int.Q04_DT_END_EFF, 0);
        position += Len.Q04_DT_END_EFF;
        MarshalByte.writeString(buffer, position, q04CodQuest, Len.Q04_COD_QUEST);
        position += Len.Q04_COD_QUEST;
        MarshalByte.writeString(buffer, position, q04CodDomanda, Len.Q04_COD_DOMANDA);
        position += Len.Q04_COD_DOMANDA;
        MarshalByte.writeIntAsPacked(buffer, position, q04OrdineDomanda, Len.Int.Q04_ORDINE_DOMANDA, 0);
        position += Len.Q04_ORDINE_DOMANDA;
        MarshalByte.writeString(buffer, position, q04CodRisp, Len.Q04_COD_RISP);
        position += Len.Q04_COD_RISP;
        MarshalByte.writeChar(buffer, position, q04ObblRisp);
        return buffer;
    }

    public void setQ04IdCompQuest(int q04IdCompQuest) {
        this.q04IdCompQuest = q04IdCompQuest;
    }

    public int getQ04IdCompQuest() {
        return this.q04IdCompQuest;
    }

    public void setQ04DtIniEff(int q04DtIniEff) {
        this.q04DtIniEff = q04DtIniEff;
    }

    public int getQ04DtIniEff() {
        return this.q04DtIniEff;
    }

    public void setQ04CodCompAnia(int q04CodCompAnia) {
        this.q04CodCompAnia = q04CodCompAnia;
    }

    public int getQ04CodCompAnia() {
        return this.q04CodCompAnia;
    }

    public void setQ04DtEndEff(int q04DtEndEff) {
        this.q04DtEndEff = q04DtEndEff;
    }

    public int getQ04DtEndEff() {
        return this.q04DtEndEff;
    }

    public void setQ04CodQuest(String q04CodQuest) {
        this.q04CodQuest = Functions.subString(q04CodQuest, Len.Q04_COD_QUEST);
    }

    public String getQ04CodQuest() {
        return this.q04CodQuest;
    }

    public void setQ04CodDomanda(String q04CodDomanda) {
        this.q04CodDomanda = Functions.subString(q04CodDomanda, Len.Q04_COD_DOMANDA);
    }

    public String getQ04CodDomanda() {
        return this.q04CodDomanda;
    }

    public void setQ04OrdineDomanda(int q04OrdineDomanda) {
        this.q04OrdineDomanda = q04OrdineDomanda;
    }

    public int getQ04OrdineDomanda() {
        return this.q04OrdineDomanda;
    }

    public void setQ04CodRisp(String q04CodRisp) {
        this.q04CodRisp = Functions.subString(q04CodRisp, Len.Q04_COD_RISP);
    }

    public String getQ04CodRisp() {
        return this.q04CodRisp;
    }

    public void setQ04ObblRisp(char q04ObblRisp) {
        this.q04ObblRisp = q04ObblRisp;
    }

    public char getQ04ObblRisp() {
        return this.q04ObblRisp;
    }

    public Q04CodCan getQ04CodCan() {
        return q04CodCan;
    }

    @Override
    public byte[] serialize() {
        return getCompQuestBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int Q04_ID_COMP_QUEST = 5;
        public static final int Q04_DT_INI_EFF = 5;
        public static final int Q04_COD_COMP_ANIA = 3;
        public static final int Q04_DT_END_EFF = 5;
        public static final int Q04_COD_QUEST = 20;
        public static final int Q04_COD_DOMANDA = 20;
        public static final int Q04_ORDINE_DOMANDA = 3;
        public static final int Q04_COD_RISP = 20;
        public static final int Q04_OBBL_RISP = 1;
        public static final int COMP_QUEST = Q04_ID_COMP_QUEST + Q04_DT_INI_EFF + Q04_COD_COMP_ANIA + Q04CodCan.Len.Q04_COD_CAN + Q04_DT_END_EFF + Q04_COD_QUEST + Q04_COD_DOMANDA + Q04_ORDINE_DOMANDA + Q04_COD_RISP + Q04_OBBL_RISP;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int Q04_ID_COMP_QUEST = 9;
            public static final int Q04_DT_INI_EFF = 8;
            public static final int Q04_COD_COMP_ANIA = 5;
            public static final int Q04_DT_END_EFF = 8;
            public static final int Q04_ORDINE_DOMANDA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
