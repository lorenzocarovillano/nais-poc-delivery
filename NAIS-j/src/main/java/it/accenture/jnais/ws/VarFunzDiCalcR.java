package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IVarFunzDiCalcR;
import it.accenture.jnais.VarFunzDiCalcIdbsvfc0;
import it.accenture.jnais.ws.redefines.Ac5Isprecalc;

/**Original name: VAR-FUNZ-DI-CALC-R<br>
 * Variable: VAR-FUNZ-DI-CALC-R from copybook IDBVAC51<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class VarFunzDiCalcR extends SerializableParameter implements IVarFunzDiCalcR {

    //==== PROPERTIES ====
    private VarFunzDiCalcIdbsvfc0 varFunzDiCalcIdbsvfc0 = new VarFunzDiCalcIdbsvfc0(this);
    //Original name: AC5-IDCOMP
    private short ac5Idcomp = DefaultValues.BIN_SHORT_VAL;
    //Original name: AC5-CODPROD-LEN
    private short ac5CodprodLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: AC5-CODPROD
    private String ac5Codprod = DefaultValues.stringVal(Len.AC5_CODPROD);
    //Original name: AC5-CODTARI-LEN
    private short ac5CodtariLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: AC5-CODTARI
    private String ac5Codtari = DefaultValues.stringVal(Len.AC5_CODTARI);
    //Original name: AC5-DINIZ
    private int ac5Diniz = DefaultValues.BIN_INT_VAL;
    //Original name: AC5-NOMEFUNZ-LEN
    private short ac5NomefunzLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: AC5-NOMEFUNZ
    private String ac5Nomefunz = DefaultValues.stringVal(Len.AC5_NOMEFUNZ);
    //Original name: AC5-DEND
    private int ac5Dend = DefaultValues.BIN_INT_VAL;
    //Original name: AC5-ISPRECALC
    private Ac5Isprecalc ac5Isprecalc = new Ac5Isprecalc();
    //Original name: AC5-NOMEPROGR-LEN
    private short ac5NomeprogrLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: AC5-NOMEPROGR
    private String ac5Nomeprogr = DefaultValues.stringVal(Len.AC5_NOMEPROGR);
    //Original name: AC5-MODCALC-LEN
    private short ac5ModcalcLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: AC5-MODCALC
    private String ac5Modcalc = DefaultValues.stringVal(Len.AC5_MODCALC);
    //Original name: AC5-GLOVARLIST-LEN
    private short ac5GlovarlistLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: AC5-GLOVARLIST
    private String ac5Glovarlist = DefaultValues.stringVal(Len.AC5_GLOVARLIST);
    //Original name: AC5-STEP-ELAB-LEN
    private short ac5StepElabLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: AC5-STEP-ELAB
    private String ac5StepElab = DefaultValues.stringVal(Len.AC5_STEP_ELAB);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAR_FUNZ_DI_CALC_R;
    }

    @Override
    public void deserialize(byte[] buf) {
        setVarFunzDiCalcRBytes(buf);
    }

    public String getVarFunzDiCalcRFormatted() {
        return MarshalByteExt.bufferToStr(getVarFunzDiCalcRBytes());
    }

    public void setVarFunzDiCalcRBytes(byte[] buffer) {
        setVarFunzDiCalcRBytes(buffer, 1);
    }

    public byte[] getVarFunzDiCalcRBytes() {
        byte[] buffer = new byte[Len.VAR_FUNZ_DI_CALC_R];
        return getVarFunzDiCalcRBytes(buffer, 1);
    }

    public void setVarFunzDiCalcRBytes(byte[] buffer, int offset) {
        int position = offset;
        ac5Idcomp = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setAc5CodprodVcharBytes(buffer, position);
        position += Len.AC5_CODPROD_VCHAR;
        setAc5CodtariVcharBytes(buffer, position);
        position += Len.AC5_CODTARI_VCHAR;
        ac5Diniz = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        setAc5NomefunzVcharBytes(buffer, position);
        position += Len.AC5_NOMEFUNZ_VCHAR;
        ac5Dend = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        ac5Isprecalc.setAc5IsprecalcFromBuffer(buffer, position);
        position += Types.SHORT_SIZE;
        setAc5NomeprogrVcharBytes(buffer, position);
        position += Len.AC5_NOMEPROGR_VCHAR;
        setAc5ModcalcVcharBytes(buffer, position);
        position += Len.AC5_MODCALC_VCHAR;
        setAc5GlovarlistVcharBytes(buffer, position);
        position += Len.AC5_GLOVARLIST_VCHAR;
        setAc5StepElabVcharBytes(buffer, position);
    }

    public byte[] getVarFunzDiCalcRBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ac5Idcomp);
        position += Types.SHORT_SIZE;
        getAc5CodprodVcharBytes(buffer, position);
        position += Len.AC5_CODPROD_VCHAR;
        getAc5CodtariVcharBytes(buffer, position);
        position += Len.AC5_CODTARI_VCHAR;
        MarshalByte.writeBinaryInt(buffer, position, ac5Diniz);
        position += Types.INT_SIZE;
        getAc5NomefunzVcharBytes(buffer, position);
        position += Len.AC5_NOMEFUNZ_VCHAR;
        MarshalByte.writeBinaryInt(buffer, position, ac5Dend);
        position += Types.INT_SIZE;
        ac5Isprecalc.getAc5IsprecalcAsBuffer(buffer, position);
        position += Types.SHORT_SIZE;
        getAc5NomeprogrVcharBytes(buffer, position);
        position += Len.AC5_NOMEPROGR_VCHAR;
        getAc5ModcalcVcharBytes(buffer, position);
        position += Len.AC5_MODCALC_VCHAR;
        getAc5GlovarlistVcharBytes(buffer, position);
        position += Len.AC5_GLOVARLIST_VCHAR;
        getAc5StepElabVcharBytes(buffer, position);
        return buffer;
    }

    public void setAc5Idcomp(short ac5Idcomp) {
        this.ac5Idcomp = ac5Idcomp;
    }

    public short getAc5Idcomp() {
        return this.ac5Idcomp;
    }

    public void setAc5CodprodVcharFormatted(String data) {
        byte[] buffer = new byte[Len.AC5_CODPROD_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.AC5_CODPROD_VCHAR);
        setAc5CodprodVcharBytes(buffer, 1);
    }

    public String getAc5CodprodVcharFormatted() {
        return MarshalByteExt.bufferToStr(getAc5CodprodVcharBytes());
    }

    /**Original name: AC5-CODPROD-VCHAR<br>*/
    public byte[] getAc5CodprodVcharBytes() {
        byte[] buffer = new byte[Len.AC5_CODPROD_VCHAR];
        return getAc5CodprodVcharBytes(buffer, 1);
    }

    public void setAc5CodprodVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ac5CodprodLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ac5Codprod = MarshalByte.readString(buffer, position, Len.AC5_CODPROD);
    }

    public byte[] getAc5CodprodVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ac5CodprodLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ac5Codprod, Len.AC5_CODPROD);
        return buffer;
    }

    public void setAc5CodprodLen(short ac5CodprodLen) {
        this.ac5CodprodLen = ac5CodprodLen;
    }

    public short getAc5CodprodLen() {
        return this.ac5CodprodLen;
    }

    public void setAc5Codprod(String ac5Codprod) {
        this.ac5Codprod = Functions.subString(ac5Codprod, Len.AC5_CODPROD);
    }

    public String getAc5Codprod() {
        return this.ac5Codprod;
    }

    public void setAc5CodtariVcharFormatted(String data) {
        byte[] buffer = new byte[Len.AC5_CODTARI_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.AC5_CODTARI_VCHAR);
        setAc5CodtariVcharBytes(buffer, 1);
    }

    public String getAc5CodtariVcharFormatted() {
        return MarshalByteExt.bufferToStr(getAc5CodtariVcharBytes());
    }

    /**Original name: AC5-CODTARI-VCHAR<br>*/
    public byte[] getAc5CodtariVcharBytes() {
        byte[] buffer = new byte[Len.AC5_CODTARI_VCHAR];
        return getAc5CodtariVcharBytes(buffer, 1);
    }

    public void setAc5CodtariVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ac5CodtariLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ac5Codtari = MarshalByte.readString(buffer, position, Len.AC5_CODTARI);
    }

    public byte[] getAc5CodtariVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ac5CodtariLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ac5Codtari, Len.AC5_CODTARI);
        return buffer;
    }

    public void setAc5CodtariLen(short ac5CodtariLen) {
        this.ac5CodtariLen = ac5CodtariLen;
    }

    public short getAc5CodtariLen() {
        return this.ac5CodtariLen;
    }

    public void setAc5Codtari(String ac5Codtari) {
        this.ac5Codtari = Functions.subString(ac5Codtari, Len.AC5_CODTARI);
    }

    public String getAc5Codtari() {
        return this.ac5Codtari;
    }

    public void setAc5Diniz(int ac5Diniz) {
        this.ac5Diniz = ac5Diniz;
    }

    public void setVfcDinizFormatted(String vfcDiniz) {
        setAc5Diniz(PicParser.display(new PicParams("S9(9)").setUsage(PicUsage.BINARY)).parseInt(vfcDiniz));
    }

    public int getAc5Diniz() {
        return this.ac5Diniz;
    }

    public void setAc5NomefunzVcharFormatted(String data) {
        byte[] buffer = new byte[Len.AC5_NOMEFUNZ_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.AC5_NOMEFUNZ_VCHAR);
        setAc5NomefunzVcharBytes(buffer, 1);
    }

    public String getAc5NomefunzVcharFormatted() {
        return MarshalByteExt.bufferToStr(getAc5NomefunzVcharBytes());
    }

    /**Original name: AC5-NOMEFUNZ-VCHAR<br>*/
    public byte[] getAc5NomefunzVcharBytes() {
        byte[] buffer = new byte[Len.AC5_NOMEFUNZ_VCHAR];
        return getAc5NomefunzVcharBytes(buffer, 1);
    }

    public void setAc5NomefunzVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ac5NomefunzLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ac5Nomefunz = MarshalByte.readString(buffer, position, Len.AC5_NOMEFUNZ);
    }

    public byte[] getAc5NomefunzVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ac5NomefunzLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ac5Nomefunz, Len.AC5_NOMEFUNZ);
        return buffer;
    }

    public void setAc5NomefunzLen(short ac5NomefunzLen) {
        this.ac5NomefunzLen = ac5NomefunzLen;
    }

    public short getAc5NomefunzLen() {
        return this.ac5NomefunzLen;
    }

    public void setAc5Nomefunz(String ac5Nomefunz) {
        this.ac5Nomefunz = Functions.subString(ac5Nomefunz, Len.AC5_NOMEFUNZ);
    }

    public String getAc5Nomefunz() {
        return this.ac5Nomefunz;
    }

    public void setAc5Dend(int ac5Dend) {
        this.ac5Dend = ac5Dend;
    }

    public int getAc5Dend() {
        return this.ac5Dend;
    }

    public void setAc5NomeprogrVcharFormatted(String data) {
        byte[] buffer = new byte[Len.AC5_NOMEPROGR_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.AC5_NOMEPROGR_VCHAR);
        setAc5NomeprogrVcharBytes(buffer, 1);
    }

    public String getAc5NomeprogrVcharFormatted() {
        return MarshalByteExt.bufferToStr(getAc5NomeprogrVcharBytes());
    }

    /**Original name: AC5-NOMEPROGR-VCHAR<br>*/
    public byte[] getAc5NomeprogrVcharBytes() {
        byte[] buffer = new byte[Len.AC5_NOMEPROGR_VCHAR];
        return getAc5NomeprogrVcharBytes(buffer, 1);
    }

    public void setAc5NomeprogrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ac5NomeprogrLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ac5Nomeprogr = MarshalByte.readString(buffer, position, Len.AC5_NOMEPROGR);
    }

    public byte[] getAc5NomeprogrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ac5NomeprogrLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ac5Nomeprogr, Len.AC5_NOMEPROGR);
        return buffer;
    }

    public void setAc5NomeprogrLen(short ac5NomeprogrLen) {
        this.ac5NomeprogrLen = ac5NomeprogrLen;
    }

    public short getAc5NomeprogrLen() {
        return this.ac5NomeprogrLen;
    }

    public void setAc5Nomeprogr(String ac5Nomeprogr) {
        this.ac5Nomeprogr = Functions.subString(ac5Nomeprogr, Len.AC5_NOMEPROGR);
    }

    public String getAc5Nomeprogr() {
        return this.ac5Nomeprogr;
    }

    public String getAc5NomeprogrFormatted() {
        return Functions.padBlanks(getAc5Nomeprogr(), Len.AC5_NOMEPROGR);
    }

    public void setAc5ModcalcVcharFormatted(String data) {
        byte[] buffer = new byte[Len.AC5_MODCALC_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.AC5_MODCALC_VCHAR);
        setAc5ModcalcVcharBytes(buffer, 1);
    }

    public String getAc5ModcalcVcharFormatted() {
        return MarshalByteExt.bufferToStr(getAc5ModcalcVcharBytes());
    }

    /**Original name: AC5-MODCALC-VCHAR<br>*/
    public byte[] getAc5ModcalcVcharBytes() {
        byte[] buffer = new byte[Len.AC5_MODCALC_VCHAR];
        return getAc5ModcalcVcharBytes(buffer, 1);
    }

    public void setAc5ModcalcVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ac5ModcalcLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ac5Modcalc = MarshalByte.readString(buffer, position, Len.AC5_MODCALC);
    }

    public byte[] getAc5ModcalcVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ac5ModcalcLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ac5Modcalc, Len.AC5_MODCALC);
        return buffer;
    }

    public void setAc5ModcalcLen(short ac5ModcalcLen) {
        this.ac5ModcalcLen = ac5ModcalcLen;
    }

    public short getAc5ModcalcLen() {
        return this.ac5ModcalcLen;
    }

    public void setAc5Modcalc(String ac5Modcalc) {
        this.ac5Modcalc = Functions.subString(ac5Modcalc, Len.AC5_MODCALC);
    }

    public String getAc5Modcalc() {
        return this.ac5Modcalc;
    }

    public String getAc5ModcalcFormatted() {
        return Functions.padBlanks(getAc5Modcalc(), Len.AC5_MODCALC);
    }

    public void setAc5GlovarlistVcharFormatted(String data) {
        byte[] buffer = new byte[Len.AC5_GLOVARLIST_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.AC5_GLOVARLIST_VCHAR);
        setAc5GlovarlistVcharBytes(buffer, 1);
    }

    public String getAc5GlovarlistVcharFormatted() {
        return MarshalByteExt.bufferToStr(getAc5GlovarlistVcharBytes());
    }

    /**Original name: AC5-GLOVARLIST-VCHAR<br>*/
    public byte[] getAc5GlovarlistVcharBytes() {
        byte[] buffer = new byte[Len.AC5_GLOVARLIST_VCHAR];
        return getAc5GlovarlistVcharBytes(buffer, 1);
    }

    public void setAc5GlovarlistVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ac5GlovarlistLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ac5Glovarlist = MarshalByte.readString(buffer, position, Len.AC5_GLOVARLIST);
    }

    public byte[] getAc5GlovarlistVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ac5GlovarlistLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ac5Glovarlist, Len.AC5_GLOVARLIST);
        return buffer;
    }

    public void setAc5GlovarlistLen(short ac5GlovarlistLen) {
        this.ac5GlovarlistLen = ac5GlovarlistLen;
    }

    public short getAc5GlovarlistLen() {
        return this.ac5GlovarlistLen;
    }

    public void setAc5Glovarlist(String ac5Glovarlist) {
        this.ac5Glovarlist = Functions.subString(ac5Glovarlist, Len.AC5_GLOVARLIST);
    }

    public String getAc5Glovarlist() {
        return this.ac5Glovarlist;
    }

    public void setAc5StepElabVcharFormatted(String data) {
        byte[] buffer = new byte[Len.AC5_STEP_ELAB_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.AC5_STEP_ELAB_VCHAR);
        setAc5StepElabVcharBytes(buffer, 1);
    }

    public String getAc5StepElabVcharFormatted() {
        return MarshalByteExt.bufferToStr(getAc5StepElabVcharBytes());
    }

    /**Original name: AC5-STEP-ELAB-VCHAR<br>*/
    public byte[] getAc5StepElabVcharBytes() {
        byte[] buffer = new byte[Len.AC5_STEP_ELAB_VCHAR];
        return getAc5StepElabVcharBytes(buffer, 1);
    }

    public void setAc5StepElabVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ac5StepElabLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ac5StepElab = MarshalByte.readString(buffer, position, Len.AC5_STEP_ELAB);
    }

    public byte[] getAc5StepElabVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ac5StepElabLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ac5StepElab, Len.AC5_STEP_ELAB);
        return buffer;
    }

    public void setAc5StepElabLen(short ac5StepElabLen) {
        this.ac5StepElabLen = ac5StepElabLen;
    }

    public short getAc5StepElabLen() {
        return this.ac5StepElabLen;
    }

    public void setAc5StepElab(String ac5StepElab) {
        this.ac5StepElab = Functions.subString(ac5StepElab, Len.AC5_STEP_ELAB);
    }

    public String getAc5StepElab() {
        return this.ac5StepElab;
    }

    public Ac5Isprecalc getAc5Isprecalc() {
        return ac5Isprecalc;
    }

    @Override
    public String getCodprodVchar() {
        return getAc5CodprodVcharFormatted();
    }

    @Override
    public void setCodprodVchar(String codprodVchar) {
        this.setAc5CodprodVcharFormatted(codprodVchar);
    }

    @Override
    public String getCodtariVchar() {
        return getAc5CodtariVcharFormatted();
    }

    @Override
    public void setCodtariVchar(String codtariVchar) {
        this.setAc5CodtariVcharFormatted(codtariVchar);
    }

    @Override
    public int getDend() {
        throw new FieldNotMappedException("dend");
    }

    @Override
    public void setDend(int dend) {
        throw new FieldNotMappedException("dend");
    }

    @Override
    public int getDiniz() {
        return getAc5Diniz();
    }

    @Override
    public void setDiniz(int diniz) {
        this.setAc5Diniz(diniz);
    }

    @Override
    public String getGlovarlistVchar() {
        throw new FieldNotMappedException("glovarlistVchar");
    }

    @Override
    public void setGlovarlistVchar(String glovarlistVchar) {
        throw new FieldNotMappedException("glovarlistVchar");
    }

    @Override
    public String getGlovarlistVcharObj() {
        return getGlovarlistVchar();
    }

    @Override
    public void setGlovarlistVcharObj(String glovarlistVcharObj) {
        setGlovarlistVchar(glovarlistVcharObj);
    }

    @Override
    public short getIdcomp() {
        return getAc5Idcomp();
    }

    @Override
    public void setIdcomp(short idcomp) {
        this.setAc5Idcomp(idcomp);
    }

    @Override
    public short getIsprecalc() {
        throw new FieldNotMappedException("isprecalc");
    }

    @Override
    public void setIsprecalc(short isprecalc) {
        throw new FieldNotMappedException("isprecalc");
    }

    @Override
    public Short getIsprecalcObj() {
        return ((Short)getIsprecalc());
    }

    @Override
    public void setIsprecalcObj(Short isprecalcObj) {
        setIsprecalc(((short)isprecalcObj));
    }

    @Override
    public String getModcalcVchar() {
        throw new FieldNotMappedException("modcalcVchar");
    }

    @Override
    public void setModcalcVchar(String modcalcVchar) {
        throw new FieldNotMappedException("modcalcVchar");
    }

    @Override
    public String getModcalcVcharObj() {
        return getModcalcVchar();
    }

    @Override
    public void setModcalcVcharObj(String modcalcVcharObj) {
        setModcalcVchar(modcalcVcharObj);
    }

    @Override
    public String getNomefunzVchar() {
        return getAc5NomefunzVcharFormatted();
    }

    @Override
    public void setNomefunzVchar(String nomefunzVchar) {
        this.setAc5NomefunzVcharFormatted(nomefunzVchar);
    }

    @Override
    public String getNomeprogrVchar() {
        throw new FieldNotMappedException("nomeprogrVchar");
    }

    @Override
    public void setNomeprogrVchar(String nomeprogrVchar) {
        throw new FieldNotMappedException("nomeprogrVchar");
    }

    @Override
    public String getNomeprogrVcharObj() {
        return getNomeprogrVchar();
    }

    @Override
    public void setNomeprogrVcharObj(String nomeprogrVcharObj) {
        setNomeprogrVchar(nomeprogrVcharObj);
    }

    @Override
    public String getStepElabVchar() {
        return getAc5StepElabVcharFormatted();
    }

    @Override
    public void setStepElabVchar(String stepElabVchar) {
        this.setAc5StepElabVcharFormatted(stepElabVchar);
    }

    public VarFunzDiCalcIdbsvfc0 getVarFunzDiCalcIdbsvfc0() {
        return varFunzDiCalcIdbsvfc0;
    }

    @Override
    public byte[] serialize() {
        return getVarFunzDiCalcRBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AC5_IDCOMP = 2;
        public static final int AC5_CODPROD_LEN = 2;
        public static final int AC5_CODPROD = 12;
        public static final int AC5_CODPROD_VCHAR = AC5_CODPROD_LEN + AC5_CODPROD;
        public static final int AC5_CODTARI_LEN = 2;
        public static final int AC5_CODTARI = 12;
        public static final int AC5_CODTARI_VCHAR = AC5_CODTARI_LEN + AC5_CODTARI;
        public static final int AC5_DINIZ = 4;
        public static final int AC5_NOMEFUNZ_LEN = 2;
        public static final int AC5_NOMEFUNZ = 12;
        public static final int AC5_NOMEFUNZ_VCHAR = AC5_NOMEFUNZ_LEN + AC5_NOMEFUNZ;
        public static final int AC5_DEND = 4;
        public static final int AC5_NOMEPROGR_LEN = 2;
        public static final int AC5_NOMEPROGR = 12;
        public static final int AC5_NOMEPROGR_VCHAR = AC5_NOMEPROGR_LEN + AC5_NOMEPROGR;
        public static final int AC5_MODCALC_LEN = 2;
        public static final int AC5_MODCALC = 12;
        public static final int AC5_MODCALC_VCHAR = AC5_MODCALC_LEN + AC5_MODCALC;
        public static final int AC5_GLOVARLIST_LEN = 2;
        public static final int AC5_GLOVARLIST = 4000;
        public static final int AC5_GLOVARLIST_VCHAR = AC5_GLOVARLIST_LEN + AC5_GLOVARLIST;
        public static final int AC5_STEP_ELAB_LEN = 2;
        public static final int AC5_STEP_ELAB = 20;
        public static final int AC5_STEP_ELAB_VCHAR = AC5_STEP_ELAB_LEN + AC5_STEP_ELAB;
        public static final int VAR_FUNZ_DI_CALC_R = AC5_IDCOMP + AC5_CODPROD_VCHAR + AC5_CODTARI_VCHAR + AC5_DINIZ + AC5_NOMEFUNZ_VCHAR + AC5_DEND + Ac5Isprecalc.Len.AC5_ISPRECALC + AC5_NOMEPROGR_VCHAR + AC5_MODCALC_VCHAR + AC5_GLOVARLIST_VCHAR + AC5_STEP_ELAB_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
