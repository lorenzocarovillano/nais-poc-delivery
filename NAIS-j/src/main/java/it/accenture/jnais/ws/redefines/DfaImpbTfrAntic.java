package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPB-TFR-ANTIC<br>
 * Variable: DFA-IMPB-TFR-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpbTfrAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpbTfrAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPB_TFR_ANTIC;
    }

    public void setDfaImpbTfrAntic(AfDecimal dfaImpbTfrAntic) {
        writeDecimalAsPacked(Pos.DFA_IMPB_TFR_ANTIC, dfaImpbTfrAntic.copy());
    }

    public void setDfaImpbTfrAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPB_TFR_ANTIC, Pos.DFA_IMPB_TFR_ANTIC);
    }

    /**Original name: DFA-IMPB-TFR-ANTIC<br>*/
    public AfDecimal getDfaImpbTfrAntic() {
        return readPackedAsDecimal(Pos.DFA_IMPB_TFR_ANTIC, Len.Int.DFA_IMPB_TFR_ANTIC, Len.Fract.DFA_IMPB_TFR_ANTIC);
    }

    public byte[] getDfaImpbTfrAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPB_TFR_ANTIC, Pos.DFA_IMPB_TFR_ANTIC);
        return buffer;
    }

    public void setDfaImpbTfrAnticNull(String dfaImpbTfrAnticNull) {
        writeString(Pos.DFA_IMPB_TFR_ANTIC_NULL, dfaImpbTfrAnticNull, Len.DFA_IMPB_TFR_ANTIC_NULL);
    }

    /**Original name: DFA-IMPB-TFR-ANTIC-NULL<br>*/
    public String getDfaImpbTfrAnticNull() {
        return readString(Pos.DFA_IMPB_TFR_ANTIC_NULL, Len.DFA_IMPB_TFR_ANTIC_NULL);
    }

    public String getDfaImpbTfrAnticNullFormatted() {
        return Functions.padBlanks(getDfaImpbTfrAnticNull(), Len.DFA_IMPB_TFR_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_TFR_ANTIC = 1;
        public static final int DFA_IMPB_TFR_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_TFR_ANTIC = 8;
        public static final int DFA_IMPB_TFR_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_TFR_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_TFR_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
