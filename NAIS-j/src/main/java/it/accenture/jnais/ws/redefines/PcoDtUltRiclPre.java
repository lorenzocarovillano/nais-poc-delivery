package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-RICL-PRE<br>
 * Variable: PCO-DT-ULT-RICL-PRE from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltRiclPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltRiclPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_RICL_PRE;
    }

    public void setPcoDtUltRiclPre(int pcoDtUltRiclPre) {
        writeIntAsPacked(Pos.PCO_DT_ULT_RICL_PRE, pcoDtUltRiclPre, Len.Int.PCO_DT_ULT_RICL_PRE);
    }

    public void setPcoDtUltRiclPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_RICL_PRE, Pos.PCO_DT_ULT_RICL_PRE);
    }

    /**Original name: PCO-DT-ULT-RICL-PRE<br>*/
    public int getPcoDtUltRiclPre() {
        return readPackedAsInt(Pos.PCO_DT_ULT_RICL_PRE, Len.Int.PCO_DT_ULT_RICL_PRE);
    }

    public byte[] getPcoDtUltRiclPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_RICL_PRE, Pos.PCO_DT_ULT_RICL_PRE);
        return buffer;
    }

    public void initPcoDtUltRiclPreHighValues() {
        fill(Pos.PCO_DT_ULT_RICL_PRE, Len.PCO_DT_ULT_RICL_PRE, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltRiclPreNull(String pcoDtUltRiclPreNull) {
        writeString(Pos.PCO_DT_ULT_RICL_PRE_NULL, pcoDtUltRiclPreNull, Len.PCO_DT_ULT_RICL_PRE_NULL);
    }

    /**Original name: PCO-DT-ULT-RICL-PRE-NULL<br>*/
    public String getPcoDtUltRiclPreNull() {
        return readString(Pos.PCO_DT_ULT_RICL_PRE_NULL, Len.PCO_DT_ULT_RICL_PRE_NULL);
    }

    public String getPcoDtUltRiclPreNullFormatted() {
        return Functions.padBlanks(getPcoDtUltRiclPreNull(), Len.PCO_DT_ULT_RICL_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RICL_PRE = 1;
        public static final int PCO_DT_ULT_RICL_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RICL_PRE = 5;
        public static final int PCO_DT_ULT_RICL_PRE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_RICL_PRE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
