package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-VIS-662014L<br>
 * Variable: DFL-IMPB-VIS-662014L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbVis662014l extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbVis662014l() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_VIS662014L;
    }

    public void setDflImpbVis662014l(AfDecimal dflImpbVis662014l) {
        writeDecimalAsPacked(Pos.DFL_IMPB_VIS662014L, dflImpbVis662014l.copy());
    }

    public void setDflImpbVis662014lFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_VIS662014L, Pos.DFL_IMPB_VIS662014L);
    }

    /**Original name: DFL-IMPB-VIS-662014L<br>*/
    public AfDecimal getDflImpbVis662014l() {
        return readPackedAsDecimal(Pos.DFL_IMPB_VIS662014L, Len.Int.DFL_IMPB_VIS662014L, Len.Fract.DFL_IMPB_VIS662014L);
    }

    public byte[] getDflImpbVis662014lAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_VIS662014L, Pos.DFL_IMPB_VIS662014L);
        return buffer;
    }

    public void setDflImpbVis662014lNull(String dflImpbVis662014lNull) {
        writeString(Pos.DFL_IMPB_VIS662014L_NULL, dflImpbVis662014lNull, Len.DFL_IMPB_VIS662014L_NULL);
    }

    /**Original name: DFL-IMPB-VIS-662014L-NULL<br>*/
    public String getDflImpbVis662014lNull() {
        return readString(Pos.DFL_IMPB_VIS662014L_NULL, Len.DFL_IMPB_VIS662014L_NULL);
    }

    public String getDflImpbVis662014lNullFormatted() {
        return Functions.padBlanks(getDflImpbVis662014lNull(), Len.DFL_IMPB_VIS662014L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS662014L = 1;
        public static final int DFL_IMPB_VIS662014L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS662014L = 8;
        public static final int DFL_IMPB_VIS662014L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS662014L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS662014L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
