package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WADE-ID-MOVI-CHIU<br>
 * Variable: WADE-ID-MOVI-CHIU from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_ID_MOVI_CHIU;
    }

    public void setWadeIdMoviChiu(int wadeIdMoviChiu) {
        writeIntAsPacked(Pos.WADE_ID_MOVI_CHIU, wadeIdMoviChiu, Len.Int.WADE_ID_MOVI_CHIU);
    }

    public void setWadeIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_ID_MOVI_CHIU, Pos.WADE_ID_MOVI_CHIU);
    }

    /**Original name: WADE-ID-MOVI-CHIU<br>*/
    public int getWadeIdMoviChiu() {
        return readPackedAsInt(Pos.WADE_ID_MOVI_CHIU, Len.Int.WADE_ID_MOVI_CHIU);
    }

    public byte[] getWadeIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_ID_MOVI_CHIU, Pos.WADE_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWadeIdMoviChiuSpaces() {
        fill(Pos.WADE_ID_MOVI_CHIU, Len.WADE_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWadeIdMoviChiuNull(String wadeIdMoviChiuNull) {
        writeString(Pos.WADE_ID_MOVI_CHIU_NULL, wadeIdMoviChiuNull, Len.WADE_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WADE-ID-MOVI-CHIU-NULL<br>*/
    public String getWadeIdMoviChiuNull() {
        return readString(Pos.WADE_ID_MOVI_CHIU_NULL, Len.WADE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_ID_MOVI_CHIU = 1;
        public static final int WADE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_ID_MOVI_CHIU = 5;
        public static final int WADE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
