package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-DT-INI-COP<br>
 * Variable: DTR-DT-INI-COP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrDtIniCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrDtIniCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_DT_INI_COP;
    }

    public void setDtrDtIniCop(int dtrDtIniCop) {
        writeIntAsPacked(Pos.DTR_DT_INI_COP, dtrDtIniCop, Len.Int.DTR_DT_INI_COP);
    }

    public void setDtrDtIniCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_DT_INI_COP, Pos.DTR_DT_INI_COP);
    }

    /**Original name: DTR-DT-INI-COP<br>*/
    public int getDtrDtIniCop() {
        return readPackedAsInt(Pos.DTR_DT_INI_COP, Len.Int.DTR_DT_INI_COP);
    }

    public byte[] getDtrDtIniCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_DT_INI_COP, Pos.DTR_DT_INI_COP);
        return buffer;
    }

    public void setDtrDtIniCopNull(String dtrDtIniCopNull) {
        writeString(Pos.DTR_DT_INI_COP_NULL, dtrDtIniCopNull, Len.DTR_DT_INI_COP_NULL);
    }

    /**Original name: DTR-DT-INI-COP-NULL<br>*/
    public String getDtrDtIniCopNull() {
        return readString(Pos.DTR_DT_INI_COP_NULL, Len.DTR_DT_INI_COP_NULL);
    }

    public String getDtrDtIniCopNullFormatted() {
        return Functions.padBlanks(getDtrDtIniCopNull(), Len.DTR_DT_INI_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_DT_INI_COP = 1;
        public static final int DTR_DT_INI_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_DT_INI_COP = 5;
        public static final int DTR_DT_INI_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_DT_INI_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
