package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-VIS-END2000-NFORZ<br>
 * Variable: TGA-VIS-END2000-NFORZ from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaVisEnd2000Nforz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaVisEnd2000Nforz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_VIS_END2000_NFORZ;
    }

    public void setTgaVisEnd2000Nforz(AfDecimal tgaVisEnd2000Nforz) {
        writeDecimalAsPacked(Pos.TGA_VIS_END2000_NFORZ, tgaVisEnd2000Nforz.copy());
    }

    public void setTgaVisEnd2000NforzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_VIS_END2000_NFORZ, Pos.TGA_VIS_END2000_NFORZ);
    }

    /**Original name: TGA-VIS-END2000-NFORZ<br>*/
    public AfDecimal getTgaVisEnd2000Nforz() {
        return readPackedAsDecimal(Pos.TGA_VIS_END2000_NFORZ, Len.Int.TGA_VIS_END2000_NFORZ, Len.Fract.TGA_VIS_END2000_NFORZ);
    }

    public byte[] getTgaVisEnd2000NforzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_VIS_END2000_NFORZ, Pos.TGA_VIS_END2000_NFORZ);
        return buffer;
    }

    public void setTgaVisEnd2000NforzNull(String tgaVisEnd2000NforzNull) {
        writeString(Pos.TGA_VIS_END2000_NFORZ_NULL, tgaVisEnd2000NforzNull, Len.TGA_VIS_END2000_NFORZ_NULL);
    }

    /**Original name: TGA-VIS-END2000-NFORZ-NULL<br>*/
    public String getTgaVisEnd2000NforzNull() {
        return readString(Pos.TGA_VIS_END2000_NFORZ_NULL, Len.TGA_VIS_END2000_NFORZ_NULL);
    }

    public String getTgaVisEnd2000NforzNullFormatted() {
        return Functions.padBlanks(getTgaVisEnd2000NforzNull(), Len.TGA_VIS_END2000_NFORZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_VIS_END2000_NFORZ = 1;
        public static final int TGA_VIS_END2000_NFORZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_VIS_END2000_NFORZ = 8;
        public static final int TGA_VIS_END2000_NFORZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_VIS_END2000_NFORZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_VIS_END2000_NFORZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
