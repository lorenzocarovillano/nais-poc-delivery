package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-REDD-FATT-ANNU<br>
 * Variable: P56-REDD-FATT-ANNU from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56ReddFattAnnu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56ReddFattAnnu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_REDD_FATT_ANNU;
    }

    public void setP56ReddFattAnnu(AfDecimal p56ReddFattAnnu) {
        writeDecimalAsPacked(Pos.P56_REDD_FATT_ANNU, p56ReddFattAnnu.copy());
    }

    public void setP56ReddFattAnnuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_REDD_FATT_ANNU, Pos.P56_REDD_FATT_ANNU);
    }

    /**Original name: P56-REDD-FATT-ANNU<br>*/
    public AfDecimal getP56ReddFattAnnu() {
        return readPackedAsDecimal(Pos.P56_REDD_FATT_ANNU, Len.Int.P56_REDD_FATT_ANNU, Len.Fract.P56_REDD_FATT_ANNU);
    }

    public byte[] getP56ReddFattAnnuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_REDD_FATT_ANNU, Pos.P56_REDD_FATT_ANNU);
        return buffer;
    }

    public void setP56ReddFattAnnuNull(String p56ReddFattAnnuNull) {
        writeString(Pos.P56_REDD_FATT_ANNU_NULL, p56ReddFattAnnuNull, Len.P56_REDD_FATT_ANNU_NULL);
    }

    /**Original name: P56-REDD-FATT-ANNU-NULL<br>*/
    public String getP56ReddFattAnnuNull() {
        return readString(Pos.P56_REDD_FATT_ANNU_NULL, Len.P56_REDD_FATT_ANNU_NULL);
    }

    public String getP56ReddFattAnnuNullFormatted() {
        return Functions.padBlanks(getP56ReddFattAnnuNull(), Len.P56_REDD_FATT_ANNU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_REDD_FATT_ANNU = 1;
        public static final int P56_REDD_FATT_ANNU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_REDD_FATT_ANNU = 8;
        public static final int P56_REDD_FATT_ANNU_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_REDD_FATT_ANNU = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_REDD_FATT_ANNU = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
