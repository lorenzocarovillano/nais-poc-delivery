package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-TAB-PLATFOND<br>
 * Variables: WCOM-TAB-PLATFOND from copybook LCCC0001<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WcomTabPlatfond {

    //==== PROPERTIES ====
    //Original name: WCOM-IMP-PLATFOND
    private AfDecimal impPlatfond = new AfDecimal(DefaultValues.DEC_VAL, 18, 3);
    //Original name: WCOM-COD-FONDO
    private String codFondo = DefaultValues.stringVal(Len.COD_FONDO);
    //Original name: WCOM-PERC-FOND-OLD
    private AfDecimal percFondOld = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: WCOM-IMPORTO-OLD
    private AfDecimal importoOld = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    public void setTabPlatfondBytes(byte[] buffer, int offset) {
        int position = offset;
        impPlatfond.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_PLATFOND, Len.Fract.IMP_PLATFOND));
        position += Len.IMP_PLATFOND;
        codFondo = MarshalByte.readString(buffer, position, Len.COD_FONDO);
        position += Len.COD_FONDO;
        percFondOld.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PERC_FOND_OLD, Len.Fract.PERC_FOND_OLD));
        position += Len.PERC_FOND_OLD;
        importoOld.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPORTO_OLD, Len.Fract.IMPORTO_OLD));
    }

    public byte[] getTabPlatfondBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimalAsPacked(buffer, position, impPlatfond.copy());
        position += Len.IMP_PLATFOND;
        MarshalByte.writeString(buffer, position, codFondo, Len.COD_FONDO);
        position += Len.COD_FONDO;
        MarshalByte.writeDecimalAsPacked(buffer, position, percFondOld.copy());
        position += Len.PERC_FOND_OLD;
        MarshalByte.writeDecimalAsPacked(buffer, position, importoOld.copy());
        return buffer;
    }

    public void initTabPlatfondSpaces() {
        impPlatfond.setNaN();
        codFondo = "";
        percFondOld.setNaN();
        importoOld.setNaN();
    }

    public void setImpPlatfond(AfDecimal impPlatfond) {
        this.impPlatfond.assign(impPlatfond);
    }

    public AfDecimal getImpPlatfond() {
        return this.impPlatfond.copy();
    }

    public void setCodFondo(String codFondo) {
        this.codFondo = Functions.subString(codFondo, Len.COD_FONDO);
    }

    public String getCodFondo() {
        return this.codFondo;
    }

    public void setPercFondOld(AfDecimal percFondOld) {
        this.percFondOld.assign(percFondOld);
    }

    public AfDecimal getPercFondOld() {
        return this.percFondOld.copy();
    }

    public void setImportoOld(AfDecimal importoOld) {
        this.importoOld.assign(importoOld);
    }

    public AfDecimal getImportoOld() {
        return this.importoOld.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IMP_PLATFOND = 10;
        public static final int COD_FONDO = 12;
        public static final int PERC_FOND_OLD = 4;
        public static final int IMPORTO_OLD = 8;
        public static final int TAB_PLATFOND = IMP_PLATFOND + COD_FONDO + PERC_FOND_OLD + IMPORTO_OLD;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IMP_PLATFOND = 15;
            public static final int PERC_FOND_OLD = 3;
            public static final int IMPORTO_OLD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMP_PLATFOND = 3;
            public static final int PERC_FOND_OLD = 3;
            public static final int IMPORTO_OLD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
