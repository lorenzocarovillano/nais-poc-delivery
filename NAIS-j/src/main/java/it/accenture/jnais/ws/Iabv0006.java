package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Iabv0006CustomCounters;
import it.accenture.jnais.copy.Iabv0006Report;
import it.accenture.jnais.ws.enums.Iabv0006FlagCommit;
import it.accenture.jnais.ws.enums.Iabv0006FlagProcessType;
import it.accenture.jnais.ws.enums.Iabv0006FlagSimulazione;
import it.accenture.jnais.ws.enums.Iabv0006GestRotturaIbo;
import it.accenture.jnais.ws.enums.Iabv0006TipoLancioBus;
import it.accenture.jnais.ws.occurs.Iabv0006VariabiliCondWf;

/**Original name: IABV0006<br>
 * Variable: IABV0006 from program LOAS0110<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Iabv0006 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int VARIABILI_COND_WF_MAXOCCURS = 20;
    /**Original name: IABV0006-TIPO-LANCIO-BUS<br>
	 * <pre>---------------------------------------------------------------*
	 *  AREA DATI EXTRA IN AMBITO DI BATCH
	 * ---------------------------------------------------------------*</pre>*/
    private Iabv0006TipoLancioBus tipoLancioBus = new Iabv0006TipoLancioBus();
    //Original name: IABV0006-DATA-BATCH
    private String dataBatch = DefaultValues.stringVal(Len.DATA_BATCH);
    //Original name: IABV0006-IBO-OLD
    private String iboOld = DefaultValues.stringVal(Len.IBO_OLD);
    //Original name: IABV0006-IBO-NEW
    private String iboNew = DefaultValues.stringVal(Len.IBO_NEW);
    //Original name: IABV0006-FLAG-SIMULAZIONE
    private Iabv0006FlagSimulazione flagSimulazione = new Iabv0006FlagSimulazione();
    //Original name: IABV0006-FLAG-COMMIT
    private Iabv0006FlagCommit flagCommit = new Iabv0006FlagCommit();
    //Original name: IABV0006-FLAG-PROCESS-TYPE
    private Iabv0006FlagProcessType flagProcessType = new Iabv0006FlagProcessType();
    //Original name: IABV0006-COD-LIV-AUT-PROFIL
    private int codLivAutProfil = DefaultValues.INT_VAL;
    //Original name: IABV0006-COD-GRU-AUT-PROFIL
    private long codGruAutProfil = DefaultValues.LONG_VAL;
    //Original name: IABV0006-CANALE-VENDITA
    private int canaleVendita = DefaultValues.INT_VAL;
    //Original name: IABV0006-PUNTO-VENDITA
    private String puntoVendita = DefaultValues.stringVal(Len.PUNTO_VENDITA);
    //Original name: IABV0006-ID-BATCH
    private int idBatch = DefaultValues.INT_VAL;
    //Original name: IABV0006-ID-RICH
    private int idRich = DefaultValues.INT_VAL;
    //Original name: IABV0006-ID-RICH-EST
    private int idRichEst = DefaultValues.INT_VAL;
    //Original name: IABV0006-REPORT
    private Iabv0006Report report = new Iabv0006Report();
    //Original name: IABV0006-CUSTOM-COUNTERS
    private Iabv0006CustomCounters customCounters = new Iabv0006CustomCounters();
    //Original name: IABV0006-GEST-ROTTURA-IBO
    private Iabv0006GestRotturaIbo gestRotturaIbo = new Iabv0006GestRotturaIbo();
    //Original name: IABV0006-NUM-ELE-VAR-COND
    private String numEleVarCond = DefaultValues.stringVal(Len.NUM_ELE_VAR_COND);
    //Original name: IABV0006-VARIABILI-COND-WF
    private Iabv0006VariabiliCondWf[] variabiliCondWf = new Iabv0006VariabiliCondWf[VARIABILI_COND_WF_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Iabv0006() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IABV0006;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIabv0006Bytes(buf);
    }

    public void init() {
        for (int variabiliCondWfIdx = 1; variabiliCondWfIdx <= VARIABILI_COND_WF_MAXOCCURS; variabiliCondWfIdx++) {
            variabiliCondWf[variabiliCondWfIdx - 1] = new Iabv0006VariabiliCondWf();
        }
    }

    public String getIabv0006Formatted() {
        return MarshalByteExt.bufferToStr(getIabv0006Bytes());
    }

    public void setIabv0006Bytes(byte[] buffer) {
        setIabv0006Bytes(buffer, 1);
    }

    public byte[] getIabv0006Bytes() {
        byte[] buffer = new byte[Len.IABV0006];
        return getIabv0006Bytes(buffer, 1);
    }

    public void setIabv0006Bytes(byte[] buffer, int offset) {
        int position = offset;
        tipoLancioBus.setTipoLancioBus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        dataBatch = MarshalByte.readString(buffer, position, Len.DATA_BATCH);
        position += Len.DATA_BATCH;
        setIboBytes(buffer, position);
        position += Len.IBO;
        flagSimulazione.setFlagSimulazione(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        flagCommit.setFlagCommit(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        flagProcessType.setFlagProcessType(MarshalByte.readString(buffer, position, Iabv0006FlagProcessType.Len.FLAG_PROCESS_TYPE));
        position += Iabv0006FlagProcessType.Len.FLAG_PROCESS_TYPE;
        codLivAutProfil = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_LIV_AUT_PROFIL, 0);
        position += Len.COD_LIV_AUT_PROFIL;
        codGruAutProfil = MarshalByte.readPackedAsLong(buffer, position, Len.Int.COD_GRU_AUT_PROFIL, 0);
        position += Len.COD_GRU_AUT_PROFIL;
        canaleVendita = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CANALE_VENDITA, 0);
        position += Len.CANALE_VENDITA;
        puntoVendita = MarshalByte.readString(buffer, position, Len.PUNTO_VENDITA);
        position += Len.PUNTO_VENDITA;
        idBatch = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_BATCH, 0);
        position += Len.ID_BATCH;
        idRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_RICH, 0);
        position += Len.ID_RICH;
        idRichEst = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_RICH_EST, 0);
        position += Len.ID_RICH_EST;
        report.setReportBytes(buffer, position);
        position += Iabv0006Report.Len.REPORT;
        customCounters.setCustomCountersBytes(buffer, position);
        position += Iabv0006CustomCounters.Len.CUSTOM_COUNTERS;
        gestRotturaIbo.setGestRotturaIbo(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        numEleVarCond = MarshalByte.readFixedString(buffer, position, Len.NUM_ELE_VAR_COND);
        position += Len.NUM_ELE_VAR_COND;
        for (int idx = 1; idx <= VARIABILI_COND_WF_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                variabiliCondWf[idx - 1].setVariabiliCondWfBytes(buffer, position);
                position += Iabv0006VariabiliCondWf.Len.VARIABILI_COND_WF;
            }
            else {
                variabiliCondWf[idx - 1].initVariabiliCondWfSpaces();
                position += Iabv0006VariabiliCondWf.Len.VARIABILI_COND_WF;
            }
        }
    }

    public byte[] getIabv0006Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, tipoLancioBus.getTipoLancioBus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dataBatch, Len.DATA_BATCH);
        position += Len.DATA_BATCH;
        getIboBytes(buffer, position);
        position += Len.IBO;
        MarshalByte.writeChar(buffer, position, flagSimulazione.getFlagSimulazione());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flagCommit.getFlagCommit());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flagProcessType.getFlagProcessType(), Iabv0006FlagProcessType.Len.FLAG_PROCESS_TYPE);
        position += Iabv0006FlagProcessType.Len.FLAG_PROCESS_TYPE;
        MarshalByte.writeIntAsPacked(buffer, position, codLivAutProfil, Len.Int.COD_LIV_AUT_PROFIL, 0);
        position += Len.COD_LIV_AUT_PROFIL;
        MarshalByte.writeLongAsPacked(buffer, position, codGruAutProfil, Len.Int.COD_GRU_AUT_PROFIL, 0);
        position += Len.COD_GRU_AUT_PROFIL;
        MarshalByte.writeIntAsPacked(buffer, position, canaleVendita, Len.Int.CANALE_VENDITA, 0);
        position += Len.CANALE_VENDITA;
        MarshalByte.writeString(buffer, position, puntoVendita, Len.PUNTO_VENDITA);
        position += Len.PUNTO_VENDITA;
        MarshalByte.writeIntAsPacked(buffer, position, idBatch, Len.Int.ID_BATCH, 0);
        position += Len.ID_BATCH;
        MarshalByte.writeIntAsPacked(buffer, position, idRich, Len.Int.ID_RICH, 0);
        position += Len.ID_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, idRichEst, Len.Int.ID_RICH_EST, 0);
        position += Len.ID_RICH_EST;
        report.getReportBytes(buffer, position);
        position += Iabv0006Report.Len.REPORT;
        customCounters.getCustomCountersBytes(buffer, position);
        position += Iabv0006CustomCounters.Len.CUSTOM_COUNTERS;
        MarshalByte.writeChar(buffer, position, gestRotturaIbo.getGestRotturaIbo());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, numEleVarCond, Len.NUM_ELE_VAR_COND);
        position += Len.NUM_ELE_VAR_COND;
        for (int idx = 1; idx <= VARIABILI_COND_WF_MAXOCCURS; idx++) {
            variabiliCondWf[idx - 1].getVariabiliCondWfBytes(buffer, position);
            position += Iabv0006VariabiliCondWf.Len.VARIABILI_COND_WF;
        }
        return buffer;
    }

    public void setDataBatch(String dataBatch) {
        this.dataBatch = Functions.subString(dataBatch, Len.DATA_BATCH);
    }

    public String getDataBatch() {
        return this.dataBatch;
    }

    public void setIboBytes(byte[] buffer, int offset) {
        int position = offset;
        iboOld = MarshalByte.readString(buffer, position, Len.IBO_OLD);
        position += Len.IBO_OLD;
        iboNew = MarshalByte.readString(buffer, position, Len.IBO_NEW);
    }

    public byte[] getIboBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, iboOld, Len.IBO_OLD);
        position += Len.IBO_OLD;
        MarshalByte.writeString(buffer, position, iboNew, Len.IBO_NEW);
        return buffer;
    }

    public void setIboOld(String iboOld) {
        this.iboOld = Functions.subString(iboOld, Len.IBO_OLD);
    }

    public String getIboOld() {
        return this.iboOld;
    }

    public void setIboNew(String iboNew) {
        this.iboNew = Functions.subString(iboNew, Len.IBO_NEW);
    }

    public String getIboNew() {
        return this.iboNew;
    }

    public void setCodLivAutProfil(int codLivAutProfil) {
        this.codLivAutProfil = codLivAutProfil;
    }

    public int getCodLivAutProfil() {
        return this.codLivAutProfil;
    }

    public void setCodGruAutProfil(long codGruAutProfil) {
        this.codGruAutProfil = codGruAutProfil;
    }

    public long getCodGruAutProfil() {
        return this.codGruAutProfil;
    }

    public void setCanaleVendita(int canaleVendita) {
        this.canaleVendita = canaleVendita;
    }

    public int getCanaleVendita() {
        return this.canaleVendita;
    }

    public void setPuntoVendita(String puntoVendita) {
        this.puntoVendita = Functions.subString(puntoVendita, Len.PUNTO_VENDITA);
    }

    public String getPuntoVendita() {
        return this.puntoVendita;
    }

    public void setIdBatch(int idBatch) {
        this.idBatch = idBatch;
    }

    public int getIdBatch() {
        return this.idBatch;
    }

    public void setIdRich(int idRich) {
        this.idRich = idRich;
    }

    public int getIdRich() {
        return this.idRich;
    }

    public void setIdRichEst(int idRichEst) {
        this.idRichEst = idRichEst;
    }

    public int getIdRichEst() {
        return this.idRichEst;
    }

    public Iabv0006CustomCounters getCustomCounters() {
        return customCounters;
    }

    public Iabv0006FlagCommit getFlagCommit() {
        return flagCommit;
    }

    public Iabv0006FlagProcessType getFlagProcessType() {
        return flagProcessType;
    }

    public Iabv0006FlagSimulazione getFlagSimulazione() {
        return flagSimulazione;
    }

    public Iabv0006GestRotturaIbo getGestRotturaIbo() {
        return gestRotturaIbo;
    }

    public Iabv0006Report getReport() {
        return report;
    }

    public Iabv0006TipoLancioBus getTipoLancioBus() {
        return tipoLancioBus;
    }

    @Override
    public byte[] serialize() {
        return getIabv0006Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DATA_BATCH = 32000;
        public static final int IBO_OLD = 100;
        public static final int IBO_NEW = 100;
        public static final int IBO = IBO_OLD + IBO_NEW;
        public static final int COD_LIV_AUT_PROFIL = 3;
        public static final int COD_GRU_AUT_PROFIL = 6;
        public static final int CANALE_VENDITA = 3;
        public static final int PUNTO_VENDITA = 6;
        public static final int ID_BATCH = 5;
        public static final int ID_RICH = 5;
        public static final int ID_RICH_EST = 5;
        public static final int NUM_ELE_VAR_COND = 3;
        public static final int IABV0006 = Iabv0006TipoLancioBus.Len.TIPO_LANCIO_BUS + DATA_BATCH + IBO + Iabv0006FlagSimulazione.Len.FLAG_SIMULAZIONE + Iabv0006FlagCommit.Len.FLAG_COMMIT + Iabv0006FlagProcessType.Len.FLAG_PROCESS_TYPE + COD_LIV_AUT_PROFIL + COD_GRU_AUT_PROFIL + CANALE_VENDITA + PUNTO_VENDITA + ID_BATCH + ID_RICH + ID_RICH_EST + Iabv0006Report.Len.REPORT + Iabv0006CustomCounters.Len.CUSTOM_COUNTERS + Iabv0006GestRotturaIbo.Len.GEST_ROTTURA_IBO + NUM_ELE_VAR_COND + Iabv0006.VARIABILI_COND_WF_MAXOCCURS * Iabv0006VariabiliCondWf.Len.VARIABILI_COND_WF;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_LIV_AUT_PROFIL = 5;
            public static final int COD_GRU_AUT_PROFIL = 10;
            public static final int CANALE_VENDITA = 5;
            public static final int ID_BATCH = 9;
            public static final int ID_RICH = 9;
            public static final int ID_RICH_EST = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
