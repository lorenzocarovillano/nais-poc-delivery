package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.DeqIdCompQuest;
import it.accenture.jnais.ws.redefines.DeqIdMoviChiu;
import it.accenture.jnais.ws.redefines.DeqRispDt;
import it.accenture.jnais.ws.redefines.DeqRispImp;
import it.accenture.jnais.ws.redefines.DeqRispNum;
import it.accenture.jnais.ws.redefines.DeqRispPc;
import it.accenture.jnais.ws.redefines.DeqRispTs;

/**Original name: DETT-QUEST<br>
 * Variable: DETT-QUEST from copybook IDBVDEQ1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DettQuestIdbsdeq0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: DEQ-ID-DETT-QUEST
    private int deqIdDettQuest = DefaultValues.INT_VAL;
    //Original name: DEQ-ID-QUEST
    private int deqIdQuest = DefaultValues.INT_VAL;
    //Original name: DEQ-ID-MOVI-CRZ
    private int deqIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: DEQ-ID-MOVI-CHIU
    private DeqIdMoviChiu deqIdMoviChiu = new DeqIdMoviChiu();
    //Original name: DEQ-DT-INI-EFF
    private int deqDtIniEff = DefaultValues.INT_VAL;
    //Original name: DEQ-DT-END-EFF
    private int deqDtEndEff = DefaultValues.INT_VAL;
    //Original name: DEQ-COD-COMP-ANIA
    private int deqCodCompAnia = DefaultValues.INT_VAL;
    //Original name: DEQ-COD-QUEST
    private String deqCodQuest = DefaultValues.stringVal(Len.DEQ_COD_QUEST);
    //Original name: DEQ-COD-DOM
    private String deqCodDom = DefaultValues.stringVal(Len.DEQ_COD_DOM);
    //Original name: DEQ-COD-DOM-COLLG
    private String deqCodDomCollg = DefaultValues.stringVal(Len.DEQ_COD_DOM_COLLG);
    //Original name: DEQ-RISP-NUM
    private DeqRispNum deqRispNum = new DeqRispNum();
    //Original name: DEQ-RISP-FL
    private char deqRispFl = DefaultValues.CHAR_VAL;
    //Original name: DEQ-RISP-TXT-LEN
    private short deqRispTxtLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: DEQ-RISP-TXT
    private String deqRispTxt = DefaultValues.stringVal(Len.DEQ_RISP_TXT);
    //Original name: DEQ-RISP-TS
    private DeqRispTs deqRispTs = new DeqRispTs();
    //Original name: DEQ-RISP-IMP
    private DeqRispImp deqRispImp = new DeqRispImp();
    //Original name: DEQ-RISP-PC
    private DeqRispPc deqRispPc = new DeqRispPc();
    //Original name: DEQ-RISP-DT
    private DeqRispDt deqRispDt = new DeqRispDt();
    //Original name: DEQ-RISP-KEY
    private String deqRispKey = DefaultValues.stringVal(Len.DEQ_RISP_KEY);
    //Original name: DEQ-TP-RISP
    private String deqTpRisp = DefaultValues.stringVal(Len.DEQ_TP_RISP);
    //Original name: DEQ-ID-COMP-QUEST
    private DeqIdCompQuest deqIdCompQuest = new DeqIdCompQuest();
    //Original name: DEQ-VAL-RISP-LEN
    private short deqValRispLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: DEQ-VAL-RISP
    private String deqValRisp = DefaultValues.stringVal(Len.DEQ_VAL_RISP);
    //Original name: DEQ-DS-RIGA
    private long deqDsRiga = DefaultValues.LONG_VAL;
    //Original name: DEQ-DS-OPER-SQL
    private char deqDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: DEQ-DS-VER
    private int deqDsVer = DefaultValues.INT_VAL;
    //Original name: DEQ-DS-TS-INI-CPTZ
    private long deqDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: DEQ-DS-TS-END-CPTZ
    private long deqDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: DEQ-DS-UTENTE
    private String deqDsUtente = DefaultValues.stringVal(Len.DEQ_DS_UTENTE);
    //Original name: DEQ-DS-STATO-ELAB
    private char deqDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DETT_QUEST;
    }

    @Override
    public void deserialize(byte[] buf) {
        setDettQuestBytes(buf);
    }

    public void setDettQuestFormatted(String data) {
        byte[] buffer = new byte[Len.DETT_QUEST];
        MarshalByte.writeString(buffer, 1, data, Len.DETT_QUEST);
        setDettQuestBytes(buffer, 1);
    }

    public String getDettQuestFormatted() {
        return MarshalByteExt.bufferToStr(getDettQuestBytes());
    }

    public void setDettQuestBytes(byte[] buffer) {
        setDettQuestBytes(buffer, 1);
    }

    public byte[] getDettQuestBytes() {
        byte[] buffer = new byte[Len.DETT_QUEST];
        return getDettQuestBytes(buffer, 1);
    }

    public void setDettQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        deqIdDettQuest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DEQ_ID_DETT_QUEST, 0);
        position += Len.DEQ_ID_DETT_QUEST;
        deqIdQuest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DEQ_ID_QUEST, 0);
        position += Len.DEQ_ID_QUEST;
        deqIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DEQ_ID_MOVI_CRZ, 0);
        position += Len.DEQ_ID_MOVI_CRZ;
        deqIdMoviChiu.setDeqIdMoviChiuFromBuffer(buffer, position);
        position += DeqIdMoviChiu.Len.DEQ_ID_MOVI_CHIU;
        deqDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DEQ_DT_INI_EFF, 0);
        position += Len.DEQ_DT_INI_EFF;
        deqDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DEQ_DT_END_EFF, 0);
        position += Len.DEQ_DT_END_EFF;
        deqCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DEQ_COD_COMP_ANIA, 0);
        position += Len.DEQ_COD_COMP_ANIA;
        deqCodQuest = MarshalByte.readString(buffer, position, Len.DEQ_COD_QUEST);
        position += Len.DEQ_COD_QUEST;
        deqCodDom = MarshalByte.readString(buffer, position, Len.DEQ_COD_DOM);
        position += Len.DEQ_COD_DOM;
        deqCodDomCollg = MarshalByte.readString(buffer, position, Len.DEQ_COD_DOM_COLLG);
        position += Len.DEQ_COD_DOM_COLLG;
        deqRispNum.setDeqRispNumFromBuffer(buffer, position);
        position += DeqRispNum.Len.DEQ_RISP_NUM;
        deqRispFl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setDeqRispTxtVcharBytes(buffer, position);
        position += Len.DEQ_RISP_TXT_VCHAR;
        deqRispTs.setDeqRispTsFromBuffer(buffer, position);
        position += DeqRispTs.Len.DEQ_RISP_TS;
        deqRispImp.setDeqRispImpFromBuffer(buffer, position);
        position += DeqRispImp.Len.DEQ_RISP_IMP;
        deqRispPc.setDeqRispPcFromBuffer(buffer, position);
        position += DeqRispPc.Len.DEQ_RISP_PC;
        deqRispDt.setDeqRispDtFromBuffer(buffer, position);
        position += DeqRispDt.Len.DEQ_RISP_DT;
        deqRispKey = MarshalByte.readString(buffer, position, Len.DEQ_RISP_KEY);
        position += Len.DEQ_RISP_KEY;
        deqTpRisp = MarshalByte.readString(buffer, position, Len.DEQ_TP_RISP);
        position += Len.DEQ_TP_RISP;
        deqIdCompQuest.setDeqIdCompQuestFromBuffer(buffer, position);
        position += DeqIdCompQuest.Len.DEQ_ID_COMP_QUEST;
        setDeqValRispVcharBytes(buffer, position);
        position += Len.DEQ_VAL_RISP_VCHAR;
        deqDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DEQ_DS_RIGA, 0);
        position += Len.DEQ_DS_RIGA;
        deqDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        deqDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DEQ_DS_VER, 0);
        position += Len.DEQ_DS_VER;
        deqDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DEQ_DS_TS_INI_CPTZ, 0);
        position += Len.DEQ_DS_TS_INI_CPTZ;
        deqDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DEQ_DS_TS_END_CPTZ, 0);
        position += Len.DEQ_DS_TS_END_CPTZ;
        deqDsUtente = MarshalByte.readString(buffer, position, Len.DEQ_DS_UTENTE);
        position += Len.DEQ_DS_UTENTE;
        deqDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDettQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, deqIdDettQuest, Len.Int.DEQ_ID_DETT_QUEST, 0);
        position += Len.DEQ_ID_DETT_QUEST;
        MarshalByte.writeIntAsPacked(buffer, position, deqIdQuest, Len.Int.DEQ_ID_QUEST, 0);
        position += Len.DEQ_ID_QUEST;
        MarshalByte.writeIntAsPacked(buffer, position, deqIdMoviCrz, Len.Int.DEQ_ID_MOVI_CRZ, 0);
        position += Len.DEQ_ID_MOVI_CRZ;
        deqIdMoviChiu.getDeqIdMoviChiuAsBuffer(buffer, position);
        position += DeqIdMoviChiu.Len.DEQ_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, deqDtIniEff, Len.Int.DEQ_DT_INI_EFF, 0);
        position += Len.DEQ_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, deqDtEndEff, Len.Int.DEQ_DT_END_EFF, 0);
        position += Len.DEQ_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, deqCodCompAnia, Len.Int.DEQ_COD_COMP_ANIA, 0);
        position += Len.DEQ_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, deqCodQuest, Len.DEQ_COD_QUEST);
        position += Len.DEQ_COD_QUEST;
        MarshalByte.writeString(buffer, position, deqCodDom, Len.DEQ_COD_DOM);
        position += Len.DEQ_COD_DOM;
        MarshalByte.writeString(buffer, position, deqCodDomCollg, Len.DEQ_COD_DOM_COLLG);
        position += Len.DEQ_COD_DOM_COLLG;
        deqRispNum.getDeqRispNumAsBuffer(buffer, position);
        position += DeqRispNum.Len.DEQ_RISP_NUM;
        MarshalByte.writeChar(buffer, position, deqRispFl);
        position += Types.CHAR_SIZE;
        getDeqRispTxtVcharBytes(buffer, position);
        position += Len.DEQ_RISP_TXT_VCHAR;
        deqRispTs.getDeqRispTsAsBuffer(buffer, position);
        position += DeqRispTs.Len.DEQ_RISP_TS;
        deqRispImp.getDeqRispImpAsBuffer(buffer, position);
        position += DeqRispImp.Len.DEQ_RISP_IMP;
        deqRispPc.getDeqRispPcAsBuffer(buffer, position);
        position += DeqRispPc.Len.DEQ_RISP_PC;
        deqRispDt.getDeqRispDtAsBuffer(buffer, position);
        position += DeqRispDt.Len.DEQ_RISP_DT;
        MarshalByte.writeString(buffer, position, deqRispKey, Len.DEQ_RISP_KEY);
        position += Len.DEQ_RISP_KEY;
        MarshalByte.writeString(buffer, position, deqTpRisp, Len.DEQ_TP_RISP);
        position += Len.DEQ_TP_RISP;
        deqIdCompQuest.getDeqIdCompQuestAsBuffer(buffer, position);
        position += DeqIdCompQuest.Len.DEQ_ID_COMP_QUEST;
        getDeqValRispVcharBytes(buffer, position);
        position += Len.DEQ_VAL_RISP_VCHAR;
        MarshalByte.writeLongAsPacked(buffer, position, deqDsRiga, Len.Int.DEQ_DS_RIGA, 0);
        position += Len.DEQ_DS_RIGA;
        MarshalByte.writeChar(buffer, position, deqDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, deqDsVer, Len.Int.DEQ_DS_VER, 0);
        position += Len.DEQ_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, deqDsTsIniCptz, Len.Int.DEQ_DS_TS_INI_CPTZ, 0);
        position += Len.DEQ_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, deqDsTsEndCptz, Len.Int.DEQ_DS_TS_END_CPTZ, 0);
        position += Len.DEQ_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, deqDsUtente, Len.DEQ_DS_UTENTE);
        position += Len.DEQ_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, deqDsStatoElab);
        return buffer;
    }

    public void setDeqIdDettQuest(int deqIdDettQuest) {
        this.deqIdDettQuest = deqIdDettQuest;
    }

    public int getDeqIdDettQuest() {
        return this.deqIdDettQuest;
    }

    public void setDeqIdQuest(int deqIdQuest) {
        this.deqIdQuest = deqIdQuest;
    }

    public int getDeqIdQuest() {
        return this.deqIdQuest;
    }

    public void setDeqIdMoviCrz(int deqIdMoviCrz) {
        this.deqIdMoviCrz = deqIdMoviCrz;
    }

    public int getDeqIdMoviCrz() {
        return this.deqIdMoviCrz;
    }

    public void setDeqDtIniEff(int deqDtIniEff) {
        this.deqDtIniEff = deqDtIniEff;
    }

    public int getDeqDtIniEff() {
        return this.deqDtIniEff;
    }

    public void setDeqDtEndEff(int deqDtEndEff) {
        this.deqDtEndEff = deqDtEndEff;
    }

    public int getDeqDtEndEff() {
        return this.deqDtEndEff;
    }

    public void setDeqCodCompAnia(int deqCodCompAnia) {
        this.deqCodCompAnia = deqCodCompAnia;
    }

    public int getDeqCodCompAnia() {
        return this.deqCodCompAnia;
    }

    public void setDeqCodQuest(String deqCodQuest) {
        this.deqCodQuest = Functions.subString(deqCodQuest, Len.DEQ_COD_QUEST);
    }

    public String getDeqCodQuest() {
        return this.deqCodQuest;
    }

    public String getDeqCodQuestFormatted() {
        return Functions.padBlanks(getDeqCodQuest(), Len.DEQ_COD_QUEST);
    }

    public void setDeqCodDom(String deqCodDom) {
        this.deqCodDom = Functions.subString(deqCodDom, Len.DEQ_COD_DOM);
    }

    public String getDeqCodDom() {
        return this.deqCodDom;
    }

    public String getDeqCodDomFormatted() {
        return Functions.padBlanks(getDeqCodDom(), Len.DEQ_COD_DOM);
    }

    public void setDeqCodDomCollg(String deqCodDomCollg) {
        this.deqCodDomCollg = Functions.subString(deqCodDomCollg, Len.DEQ_COD_DOM_COLLG);
    }

    public String getDeqCodDomCollg() {
        return this.deqCodDomCollg;
    }

    public String getDeqCodDomCollgFormatted() {
        return Functions.padBlanks(getDeqCodDomCollg(), Len.DEQ_COD_DOM_COLLG);
    }

    public void setDeqRispFl(char deqRispFl) {
        this.deqRispFl = deqRispFl;
    }

    public char getDeqRispFl() {
        return this.deqRispFl;
    }

    public void setDeqRispTxtVcharFormatted(String data) {
        byte[] buffer = new byte[Len.DEQ_RISP_TXT_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.DEQ_RISP_TXT_VCHAR);
        setDeqRispTxtVcharBytes(buffer, 1);
    }

    public String getDeqRispTxtVcharFormatted() {
        return MarshalByteExt.bufferToStr(getDeqRispTxtVcharBytes());
    }

    /**Original name: DEQ-RISP-TXT-VCHAR<br>*/
    public byte[] getDeqRispTxtVcharBytes() {
        byte[] buffer = new byte[Len.DEQ_RISP_TXT_VCHAR];
        return getDeqRispTxtVcharBytes(buffer, 1);
    }

    public void setDeqRispTxtVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        deqRispTxtLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        deqRispTxt = MarshalByte.readString(buffer, position, Len.DEQ_RISP_TXT);
    }

    public byte[] getDeqRispTxtVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, deqRispTxtLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, deqRispTxt, Len.DEQ_RISP_TXT);
        return buffer;
    }

    public void setDeqRispTxtLen(short deqRispTxtLen) {
        this.deqRispTxtLen = deqRispTxtLen;
    }

    public short getDeqRispTxtLen() {
        return this.deqRispTxtLen;
    }

    public void setDeqRispTxt(String deqRispTxt) {
        this.deqRispTxt = Functions.subString(deqRispTxt, Len.DEQ_RISP_TXT);
    }

    public String getDeqRispTxt() {
        return this.deqRispTxt;
    }

    public void setDeqRispKey(String deqRispKey) {
        this.deqRispKey = Functions.subString(deqRispKey, Len.DEQ_RISP_KEY);
    }

    public String getDeqRispKey() {
        return this.deqRispKey;
    }

    public String getDeqRispKeyFormatted() {
        return Functions.padBlanks(getDeqRispKey(), Len.DEQ_RISP_KEY);
    }

    public void setDeqTpRisp(String deqTpRisp) {
        this.deqTpRisp = Functions.subString(deqTpRisp, Len.DEQ_TP_RISP);
    }

    public String getDeqTpRisp() {
        return this.deqTpRisp;
    }

    public String getDeqTpRispFormatted() {
        return Functions.padBlanks(getDeqTpRisp(), Len.DEQ_TP_RISP);
    }

    public void setDeqValRispVcharFormatted(String data) {
        byte[] buffer = new byte[Len.DEQ_VAL_RISP_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.DEQ_VAL_RISP_VCHAR);
        setDeqValRispVcharBytes(buffer, 1);
    }

    public String getDeqValRispVcharFormatted() {
        return MarshalByteExt.bufferToStr(getDeqValRispVcharBytes());
    }

    /**Original name: DEQ-VAL-RISP-VCHAR<br>*/
    public byte[] getDeqValRispVcharBytes() {
        byte[] buffer = new byte[Len.DEQ_VAL_RISP_VCHAR];
        return getDeqValRispVcharBytes(buffer, 1);
    }

    public void setDeqValRispVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        deqValRispLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        deqValRisp = MarshalByte.readString(buffer, position, Len.DEQ_VAL_RISP);
    }

    public byte[] getDeqValRispVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, deqValRispLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, deqValRisp, Len.DEQ_VAL_RISP);
        return buffer;
    }

    public void setDeqValRispLen(short deqValRispLen) {
        this.deqValRispLen = deqValRispLen;
    }

    public short getDeqValRispLen() {
        return this.deqValRispLen;
    }

    public void setDeqValRisp(String deqValRisp) {
        this.deqValRisp = Functions.subString(deqValRisp, Len.DEQ_VAL_RISP);
    }

    public String getDeqValRisp() {
        return this.deqValRisp;
    }

    public void setDeqDsRiga(long deqDsRiga) {
        this.deqDsRiga = deqDsRiga;
    }

    public long getDeqDsRiga() {
        return this.deqDsRiga;
    }

    public void setDeqDsOperSql(char deqDsOperSql) {
        this.deqDsOperSql = deqDsOperSql;
    }

    public void setDeqDsOperSqlFormatted(String deqDsOperSql) {
        setDeqDsOperSql(Functions.charAt(deqDsOperSql, Types.CHAR_SIZE));
    }

    public char getDeqDsOperSql() {
        return this.deqDsOperSql;
    }

    public void setDeqDsVer(int deqDsVer) {
        this.deqDsVer = deqDsVer;
    }

    public int getDeqDsVer() {
        return this.deqDsVer;
    }

    public void setDeqDsTsIniCptz(long deqDsTsIniCptz) {
        this.deqDsTsIniCptz = deqDsTsIniCptz;
    }

    public long getDeqDsTsIniCptz() {
        return this.deqDsTsIniCptz;
    }

    public void setDeqDsTsEndCptz(long deqDsTsEndCptz) {
        this.deqDsTsEndCptz = deqDsTsEndCptz;
    }

    public long getDeqDsTsEndCptz() {
        return this.deqDsTsEndCptz;
    }

    public void setDeqDsUtente(String deqDsUtente) {
        this.deqDsUtente = Functions.subString(deqDsUtente, Len.DEQ_DS_UTENTE);
    }

    public String getDeqDsUtente() {
        return this.deqDsUtente;
    }

    public void setDeqDsStatoElab(char deqDsStatoElab) {
        this.deqDsStatoElab = deqDsStatoElab;
    }

    public void setDeqDsStatoElabFormatted(String deqDsStatoElab) {
        setDeqDsStatoElab(Functions.charAt(deqDsStatoElab, Types.CHAR_SIZE));
    }

    public char getDeqDsStatoElab() {
        return this.deqDsStatoElab;
    }

    public DeqIdCompQuest getDeqIdCompQuest() {
        return deqIdCompQuest;
    }

    public DeqIdMoviChiu getDeqIdMoviChiu() {
        return deqIdMoviChiu;
    }

    public DeqRispDt getDeqRispDt() {
        return deqRispDt;
    }

    public DeqRispImp getDeqRispImp() {
        return deqRispImp;
    }

    public DeqRispNum getDeqRispNum() {
        return deqRispNum;
    }

    public DeqRispPc getDeqRispPc() {
        return deqRispPc;
    }

    public DeqRispTs getDeqRispTs() {
        return deqRispTs;
    }

    @Override
    public byte[] serialize() {
        return getDettQuestBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DEQ_ID_DETT_QUEST = 5;
        public static final int DEQ_ID_QUEST = 5;
        public static final int DEQ_ID_MOVI_CRZ = 5;
        public static final int DEQ_DT_INI_EFF = 5;
        public static final int DEQ_DT_END_EFF = 5;
        public static final int DEQ_COD_COMP_ANIA = 3;
        public static final int DEQ_COD_QUEST = 20;
        public static final int DEQ_COD_DOM = 20;
        public static final int DEQ_COD_DOM_COLLG = 20;
        public static final int DEQ_RISP_FL = 1;
        public static final int DEQ_RISP_TXT_LEN = 2;
        public static final int DEQ_RISP_TXT = 100;
        public static final int DEQ_RISP_TXT_VCHAR = DEQ_RISP_TXT_LEN + DEQ_RISP_TXT;
        public static final int DEQ_RISP_KEY = 20;
        public static final int DEQ_TP_RISP = 2;
        public static final int DEQ_VAL_RISP_LEN = 2;
        public static final int DEQ_VAL_RISP = 250;
        public static final int DEQ_VAL_RISP_VCHAR = DEQ_VAL_RISP_LEN + DEQ_VAL_RISP;
        public static final int DEQ_DS_RIGA = 6;
        public static final int DEQ_DS_OPER_SQL = 1;
        public static final int DEQ_DS_VER = 5;
        public static final int DEQ_DS_TS_INI_CPTZ = 10;
        public static final int DEQ_DS_TS_END_CPTZ = 10;
        public static final int DEQ_DS_UTENTE = 20;
        public static final int DEQ_DS_STATO_ELAB = 1;
        public static final int DETT_QUEST = DEQ_ID_DETT_QUEST + DEQ_ID_QUEST + DEQ_ID_MOVI_CRZ + DeqIdMoviChiu.Len.DEQ_ID_MOVI_CHIU + DEQ_DT_INI_EFF + DEQ_DT_END_EFF + DEQ_COD_COMP_ANIA + DEQ_COD_QUEST + DEQ_COD_DOM + DEQ_COD_DOM_COLLG + DeqRispNum.Len.DEQ_RISP_NUM + DEQ_RISP_FL + DEQ_RISP_TXT_VCHAR + DeqRispTs.Len.DEQ_RISP_TS + DeqRispImp.Len.DEQ_RISP_IMP + DeqRispPc.Len.DEQ_RISP_PC + DeqRispDt.Len.DEQ_RISP_DT + DEQ_RISP_KEY + DEQ_TP_RISP + DeqIdCompQuest.Len.DEQ_ID_COMP_QUEST + DEQ_VAL_RISP_VCHAR + DEQ_DS_RIGA + DEQ_DS_OPER_SQL + DEQ_DS_VER + DEQ_DS_TS_INI_CPTZ + DEQ_DS_TS_END_CPTZ + DEQ_DS_UTENTE + DEQ_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DEQ_ID_DETT_QUEST = 9;
            public static final int DEQ_ID_QUEST = 9;
            public static final int DEQ_ID_MOVI_CRZ = 9;
            public static final int DEQ_DT_INI_EFF = 8;
            public static final int DEQ_DT_END_EFF = 8;
            public static final int DEQ_COD_COMP_ANIA = 5;
            public static final int DEQ_DS_RIGA = 10;
            public static final int DEQ_DS_VER = 9;
            public static final int DEQ_DS_TS_INI_CPTZ = 18;
            public static final int DEQ_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
