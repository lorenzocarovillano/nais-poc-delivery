package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WgrzTabGar;

/**Original name: WGRZ-AREA-GARANZIA<br>
 * Variable: WGRZ-AREA-GARANZIA from program LCCS0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WgrzAreaGaranziaLccs0005 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_GAR_MAXOCCURS = 20;
    //Original name: WGRZ-ELE-GAR-MAX
    private short eleGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WGRZ-TAB-GAR
    private WgrzTabGar[] tabGar = new WgrzTabGar[TAB_GAR_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WgrzAreaGaranziaLccs0005() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_AREA_GARANZIA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWgrzAreaGaranziaBytes(buf);
    }

    public void init() {
        for (int tabGarIdx = 1; tabGarIdx <= TAB_GAR_MAXOCCURS; tabGarIdx++) {
            tabGar[tabGarIdx - 1] = new WgrzTabGar();
        }
    }

    public String getWgrzAreaGaranziaFormatted() {
        return MarshalByteExt.bufferToStr(getWgrzAreaGaranziaBytes());
    }

    public void setWgrzAreaGaranziaBytes(byte[] buffer) {
        setWgrzAreaGaranziaBytes(buffer, 1);
    }

    public byte[] getWgrzAreaGaranziaBytes() {
        byte[] buffer = new byte[Len.WGRZ_AREA_GARANZIA];
        return getWgrzAreaGaranziaBytes(buffer, 1);
    }

    public void setWgrzAreaGaranziaBytes(byte[] buffer, int offset) {
        int position = offset;
        eleGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                tabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public byte[] getWgrzAreaGaranziaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleGarMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_GAR_MAXOCCURS; idx++) {
            tabGar[idx - 1].getTabGarBytes(buffer, position);
            position += WgrzTabGar.Len.TAB_GAR;
        }
        return buffer;
    }

    public void setEleGarMax(short eleGarMax) {
        this.eleGarMax = eleGarMax;
    }

    public short getEleGarMax() {
        return this.eleGarMax;
    }

    public WgrzTabGar getTabGar(int idx) {
        return tabGar[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWgrzAreaGaranziaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_GAR_MAX = 2;
        public static final int WGRZ_AREA_GARANZIA = ELE_GAR_MAX + WgrzAreaGaranziaLccs0005.TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
