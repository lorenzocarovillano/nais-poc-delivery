package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-SOGL-AML-PRE-UNI<br>
 * Variable: WPCO-SOGL-AML-PRE-UNI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoSoglAmlPreUni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoSoglAmlPreUni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_SOGL_AML_PRE_UNI;
    }

    public void setWpcoSoglAmlPreUni(AfDecimal wpcoSoglAmlPreUni) {
        writeDecimalAsPacked(Pos.WPCO_SOGL_AML_PRE_UNI, wpcoSoglAmlPreUni.copy());
    }

    public void setDpcoSoglAmlPreUniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_SOGL_AML_PRE_UNI, Pos.WPCO_SOGL_AML_PRE_UNI);
    }

    /**Original name: WPCO-SOGL-AML-PRE-UNI<br>*/
    public AfDecimal getWpcoSoglAmlPreUni() {
        return readPackedAsDecimal(Pos.WPCO_SOGL_AML_PRE_UNI, Len.Int.WPCO_SOGL_AML_PRE_UNI, Len.Fract.WPCO_SOGL_AML_PRE_UNI);
    }

    public byte[] getWpcoSoglAmlPreUniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_SOGL_AML_PRE_UNI, Pos.WPCO_SOGL_AML_PRE_UNI);
        return buffer;
    }

    public void setWpcoSoglAmlPreUniNull(String wpcoSoglAmlPreUniNull) {
        writeString(Pos.WPCO_SOGL_AML_PRE_UNI_NULL, wpcoSoglAmlPreUniNull, Len.WPCO_SOGL_AML_PRE_UNI_NULL);
    }

    /**Original name: WPCO-SOGL-AML-PRE-UNI-NULL<br>*/
    public String getWpcoSoglAmlPreUniNull() {
        return readString(Pos.WPCO_SOGL_AML_PRE_UNI_NULL, Len.WPCO_SOGL_AML_PRE_UNI_NULL);
    }

    public String getWpcoSoglAmlPreUniNullFormatted() {
        return Functions.padBlanks(getWpcoSoglAmlPreUniNull(), Len.WPCO_SOGL_AML_PRE_UNI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_SOGL_AML_PRE_UNI = 1;
        public static final int WPCO_SOGL_AML_PRE_UNI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_SOGL_AML_PRE_UNI = 8;
        public static final int WPCO_SOGL_AML_PRE_UNI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_SOGL_AML_PRE_UNI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_SOGL_AML_PRE_UNI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
