package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-DT-DECOR-PREST-BAN<br>
 * Variable: ADE-DT-DECOR-PREST-BAN from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeDtDecorPrestBan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeDtDecorPrestBan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_DT_DECOR_PREST_BAN;
    }

    public void setAdeDtDecorPrestBan(int adeDtDecorPrestBan) {
        writeIntAsPacked(Pos.ADE_DT_DECOR_PREST_BAN, adeDtDecorPrestBan, Len.Int.ADE_DT_DECOR_PREST_BAN);
    }

    public void setAdeDtDecorPrestBanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_DT_DECOR_PREST_BAN, Pos.ADE_DT_DECOR_PREST_BAN);
    }

    /**Original name: ADE-DT-DECOR-PREST-BAN<br>*/
    public int getAdeDtDecorPrestBan() {
        return readPackedAsInt(Pos.ADE_DT_DECOR_PREST_BAN, Len.Int.ADE_DT_DECOR_PREST_BAN);
    }

    public byte[] getAdeDtDecorPrestBanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_DT_DECOR_PREST_BAN, Pos.ADE_DT_DECOR_PREST_BAN);
        return buffer;
    }

    public void setAdeDtDecorPrestBanNull(String adeDtDecorPrestBanNull) {
        writeString(Pos.ADE_DT_DECOR_PREST_BAN_NULL, adeDtDecorPrestBanNull, Len.ADE_DT_DECOR_PREST_BAN_NULL);
    }

    /**Original name: ADE-DT-DECOR-PREST-BAN-NULL<br>*/
    public String getAdeDtDecorPrestBanNull() {
        return readString(Pos.ADE_DT_DECOR_PREST_BAN_NULL, Len.ADE_DT_DECOR_PREST_BAN_NULL);
    }

    public String getAdeDtDecorPrestBanNullFormatted() {
        return Functions.padBlanks(getAdeDtDecorPrestBanNull(), Len.ADE_DT_DECOR_PREST_BAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_DT_DECOR_PREST_BAN = 1;
        public static final int ADE_DT_DECOR_PREST_BAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_DT_DECOR_PREST_BAN = 5;
        public static final int ADE_DT_DECOR_PREST_BAN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_DT_DECOR_PREST_BAN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
