package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-QUIE-C<br>
 * Variable: WPCO-DT-ULT-BOLL-QUIE-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollQuieC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollQuieC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_QUIE_C;
    }

    public void setWpcoDtUltBollQuieC(int wpcoDtUltBollQuieC) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_QUIE_C, wpcoDtUltBollQuieC, Len.Int.WPCO_DT_ULT_BOLL_QUIE_C);
    }

    public void setDpcoDtUltBollQuieCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_QUIE_C, Pos.WPCO_DT_ULT_BOLL_QUIE_C);
    }

    /**Original name: WPCO-DT-ULT-BOLL-QUIE-C<br>*/
    public int getWpcoDtUltBollQuieC() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_QUIE_C, Len.Int.WPCO_DT_ULT_BOLL_QUIE_C);
    }

    public byte[] getWpcoDtUltBollQuieCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_QUIE_C, Pos.WPCO_DT_ULT_BOLL_QUIE_C);
        return buffer;
    }

    public void setWpcoDtUltBollQuieCNull(String wpcoDtUltBollQuieCNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_QUIE_C_NULL, wpcoDtUltBollQuieCNull, Len.WPCO_DT_ULT_BOLL_QUIE_C_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-QUIE-C-NULL<br>*/
    public String getWpcoDtUltBollQuieCNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_QUIE_C_NULL, Len.WPCO_DT_ULT_BOLL_QUIE_C_NULL);
    }

    public String getWpcoDtUltBollQuieCNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollQuieCNull(), Len.WPCO_DT_ULT_BOLL_QUIE_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_QUIE_C = 1;
        public static final int WPCO_DT_ULT_BOLL_QUIE_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_QUIE_C = 5;
        public static final int WPCO_DT_ULT_BOLL_QUIE_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_QUIE_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
