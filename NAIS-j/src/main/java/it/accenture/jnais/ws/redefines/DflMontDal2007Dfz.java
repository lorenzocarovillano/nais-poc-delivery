package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-MONT-DAL2007-DFZ<br>
 * Variable: DFL-MONT-DAL2007-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflMontDal2007Dfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflMontDal2007Dfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_MONT_DAL2007_DFZ;
    }

    public void setDflMontDal2007Dfz(AfDecimal dflMontDal2007Dfz) {
        writeDecimalAsPacked(Pos.DFL_MONT_DAL2007_DFZ, dflMontDal2007Dfz.copy());
    }

    public void setDflMontDal2007DfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_MONT_DAL2007_DFZ, Pos.DFL_MONT_DAL2007_DFZ);
    }

    /**Original name: DFL-MONT-DAL2007-DFZ<br>*/
    public AfDecimal getDflMontDal2007Dfz() {
        return readPackedAsDecimal(Pos.DFL_MONT_DAL2007_DFZ, Len.Int.DFL_MONT_DAL2007_DFZ, Len.Fract.DFL_MONT_DAL2007_DFZ);
    }

    public byte[] getDflMontDal2007DfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_MONT_DAL2007_DFZ, Pos.DFL_MONT_DAL2007_DFZ);
        return buffer;
    }

    public void setDflMontDal2007DfzNull(String dflMontDal2007DfzNull) {
        writeString(Pos.DFL_MONT_DAL2007_DFZ_NULL, dflMontDal2007DfzNull, Len.DFL_MONT_DAL2007_DFZ_NULL);
    }

    /**Original name: DFL-MONT-DAL2007-DFZ-NULL<br>*/
    public String getDflMontDal2007DfzNull() {
        return readString(Pos.DFL_MONT_DAL2007_DFZ_NULL, Len.DFL_MONT_DAL2007_DFZ_NULL);
    }

    public String getDflMontDal2007DfzNullFormatted() {
        return Functions.padBlanks(getDflMontDal2007DfzNull(), Len.DFL_MONT_DAL2007_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_MONT_DAL2007_DFZ = 1;
        public static final int DFL_MONT_DAL2007_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_MONT_DAL2007_DFZ = 8;
        public static final int DFL_MONT_DAL2007_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_MONT_DAL2007_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_MONT_DAL2007_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
