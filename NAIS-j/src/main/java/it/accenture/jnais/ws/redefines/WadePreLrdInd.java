package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-PRE-LRD-IND<br>
 * Variable: WADE-PRE-LRD-IND from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadePreLrdInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadePreLrdInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_PRE_LRD_IND;
    }

    public void setWadePreLrdInd(AfDecimal wadePreLrdInd) {
        writeDecimalAsPacked(Pos.WADE_PRE_LRD_IND, wadePreLrdInd.copy());
    }

    public void setWadePreLrdIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_PRE_LRD_IND, Pos.WADE_PRE_LRD_IND);
    }

    /**Original name: WADE-PRE-LRD-IND<br>*/
    public AfDecimal getWadePreLrdInd() {
        return readPackedAsDecimal(Pos.WADE_PRE_LRD_IND, Len.Int.WADE_PRE_LRD_IND, Len.Fract.WADE_PRE_LRD_IND);
    }

    public byte[] getWadePreLrdIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_PRE_LRD_IND, Pos.WADE_PRE_LRD_IND);
        return buffer;
    }

    public void initWadePreLrdIndSpaces() {
        fill(Pos.WADE_PRE_LRD_IND, Len.WADE_PRE_LRD_IND, Types.SPACE_CHAR);
    }

    public void setWadePreLrdIndNull(String wadePreLrdIndNull) {
        writeString(Pos.WADE_PRE_LRD_IND_NULL, wadePreLrdIndNull, Len.WADE_PRE_LRD_IND_NULL);
    }

    /**Original name: WADE-PRE-LRD-IND-NULL<br>*/
    public String getWadePreLrdIndNull() {
        return readString(Pos.WADE_PRE_LRD_IND_NULL, Len.WADE_PRE_LRD_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_PRE_LRD_IND = 1;
        public static final int WADE_PRE_LRD_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_PRE_LRD_IND = 8;
        public static final int WADE_PRE_LRD_IND_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_PRE_LRD_IND = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_PRE_LRD_IND = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
