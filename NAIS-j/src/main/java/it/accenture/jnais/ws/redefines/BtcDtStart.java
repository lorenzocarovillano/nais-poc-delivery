package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BTC-DT-START<br>
 * Variable: BTC-DT-START from program IABS0040<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BtcDtStart extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BtcDtStart() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_DT_START;
    }

    public void setBtcDtStart(long btcDtStart) {
        writeLongAsPacked(Pos.BTC_DT_START, btcDtStart, Len.Int.BTC_DT_START);
    }

    public void setBtcDtStartFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BTC_DT_START, Pos.BTC_DT_START);
    }

    /**Original name: BTC-DT-START<br>*/
    public long getBtcDtStart() {
        return readPackedAsLong(Pos.BTC_DT_START, Len.Int.BTC_DT_START);
    }

    public byte[] getBtcDtStartAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BTC_DT_START, Pos.BTC_DT_START);
        return buffer;
    }

    public void setBtcDtStartNull(String btcDtStartNull) {
        writeString(Pos.BTC_DT_START_NULL, btcDtStartNull, Len.BTC_DT_START_NULL);
    }

    /**Original name: BTC-DT-START-NULL<br>*/
    public String getBtcDtStartNull() {
        return readString(Pos.BTC_DT_START_NULL, Len.BTC_DT_START_NULL);
    }

    public String getBtcDtStartNullFormatted() {
        return Functions.padBlanks(getBtcDtStartNull(), Len.BTC_DT_START_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BTC_DT_START = 1;
        public static final int BTC_DT_START_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BTC_DT_START = 10;
        public static final int BTC_DT_START_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BTC_DT_START = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
