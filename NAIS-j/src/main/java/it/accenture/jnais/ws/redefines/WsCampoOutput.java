package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WS-CAMPO-OUTPUT<br>
 * Variable: WS-CAMPO-OUTPUT from program IWFS0050<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsCampoOutput extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELE_STRINGA_OUTPUT_MAXOCCURS = 18;

    //==== CONSTRUCTORS ====
    public WsCampoOutput() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_CAMPO_OUTPUT;
    }

    public void setWsCampoOutput(AfDecimal wsCampoOutput) {
        writeDecimal(Pos.WS_CAMPO_OUTPUT, wsCampoOutput.copy());
    }

    public void setWsCampoOutputFormatted(String wsCampoOutput) {
        setWsCampoOutput(PicParser.display("S9(13)V9(5)").parseDecimal(Len.Int.WS_CAMPO_OUTPUT + Len.Fract.WS_CAMPO_OUTPUT, Len.Fract.WS_CAMPO_OUTPUT, wsCampoOutput));
    }

    /**Original name: WS-CAMPO-OUTPUT<br>*/
    public AfDecimal getWsCampoOutput() {
        return readDecimal(Pos.WS_CAMPO_OUTPUT, Len.Int.WS_CAMPO_OUTPUT, Len.Fract.WS_CAMPO_OUTPUT);
    }

    public String getWsCampoOutputFormatted() {
        return readFixedString(Pos.WS_CAMPO_OUTPUT, Len.WS_CAMPO_OUTPUT);
    }

    public void setEleStringaOutput(int eleStringaOutputIdx, char eleStringaOutput) {
        int position = Pos.eleStringaOutput(eleStringaOutputIdx - 1);
        writeChar(position, eleStringaOutput);
    }

    /**Original name: ELE-STRINGA-OUTPUT<br>*/
    public char getEleStringaOutput(int eleStringaOutputIdx) {
        int position = Pos.eleStringaOutput(eleStringaOutputIdx - 1);
        return readChar(position);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_CAMPO_OUTPUT = 1;
        public static final int ARRAY_STRINGA_OUTPUT = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int eleStringaOutput(int idx) {
            return ARRAY_STRINGA_OUTPUT + idx * Len.ELE_STRINGA_OUTPUT;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_STRINGA_OUTPUT = 1;
        public static final int WS_CAMPO_OUTPUT = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WS_CAMPO_OUTPUT = 13;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WS_CAMPO_OUTPUT = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
