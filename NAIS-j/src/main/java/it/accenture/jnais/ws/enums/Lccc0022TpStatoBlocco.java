package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0022-TP-STATO-BLOCCO<br>
 * Variable: LCCC0022-TP-STATO-BLOCCO from copybook LCCC0022<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0022TpStatoBlocco {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TP_STATO_BLOCCO);
    public static final String ATTIVO = "AT";
    public static final String NON_ATTIVO = "NA";

    //==== METHODS ====
    public void setTpStatoBlocco(String tpStatoBlocco) {
        this.value = Functions.subString(tpStatoBlocco, Len.TP_STATO_BLOCCO);
    }

    public String getTpStatoBlocco() {
        return this.value;
    }

    public String getTpStatoBloccoFormatted() {
        return Functions.padBlanks(getTpStatoBlocco(), Len.TP_STATO_BLOCCO);
    }

    public void setLccc0022BloccoAttivo() {
        value = ATTIVO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_STATO_BLOCCO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
