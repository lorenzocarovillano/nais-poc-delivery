package it.accenture.jnais.ws;

import com.bphx.ctu.af.lang.types.AfDecimal;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.ws.enums.BeneficiarioCedola;
import it.accenture.jnais.ws.enums.MovimentoCedola;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVES0270<br>
 * Generated as a class for rule WS.<br>*/
public class Lves0270Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI DI WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVES0270";
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: IX-INDICI
    private Indici ixIndici = new Indici();
    //Original name: MOVIMENTO-CEDOLA
    private MovimentoCedola movimentoCedola = new MovimentoCedola();
    //Original name: BENEFICIARIO-CEDOLA
    private BeneficiarioCedola beneficiarioCedola = new BeneficiarioCedola();
    /**Original name: WK-PREST-INI-TOT<br>
	 * <pre>MOD20110512
	 * ----------------------------------------------------------------*
	 *  COSTANI                                                *
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkPrestIniTot = new AfDecimal("0", 15, 3);
    //Original name: AREA-LCCC0019
    private AreaLccc0019 areaLccc0019 = new AreaLccc0019();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkPrestIniTot(AfDecimal wkPrestIniTot) {
        this.wkPrestIniTot.assign(wkPrestIniTot);
    }

    public AfDecimal getWkPrestIniTot() {
        return this.wkPrestIniTot.copy();
    }

    public AreaLccc0019 getAreaLccc0019() {
        return areaLccc0019;
    }

    public BeneficiarioCedola getBeneficiarioCedola() {
        return beneficiarioCedola;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Indici getIxIndici() {
        return ixIndici;
    }

    public MovimentoCedola getMovimentoCedola() {
        return movimentoCedola;
    }
}
