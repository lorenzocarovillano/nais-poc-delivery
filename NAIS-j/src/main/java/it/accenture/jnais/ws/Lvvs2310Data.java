package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.enums.WcomFlagTrovato;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS2310<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs2310Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS2310";
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariablesLvvs2310 dispatcherVariables = new DispatcherVariablesLvvs2310();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-INDICI
    private IxIndiciLvvs0001 ixIndici = new IxIndiciLvvs0001();
    //Original name: WCOM-FLAG-TROVATO
    private WcomFlagTrovato wcomFlagTrovato = new WcomFlagTrovato();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public DispatcherVariablesLvvs2310 getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndiciLvvs0001 getIxIndici() {
        return ixIndici;
    }

    public WcomFlagTrovato getWcomFlagTrovato() {
        return wcomFlagTrovato;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WKS_NOME_TABELLA = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
