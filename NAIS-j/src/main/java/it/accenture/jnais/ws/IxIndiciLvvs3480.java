package it.accenture.jnais.ws;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LVVS3480<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLvvs3480 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-CDG
    private short tabCdg = ((short)0);
    //Original name: IX-DCLGEN
    private short dclgen = ((short)0);
    //Original name: IX-TAB-0501
    private short tab0501 = ((short)0);

    //==== METHODS ====
    public void setTabCdg(short tabCdg) {
        this.tabCdg = tabCdg;
    }

    public short getTabCdg() {
        return this.tabCdg;
    }

    public void setDclgen(short dclgen) {
        this.dclgen = dclgen;
    }

    public short getDclgen() {
        return this.dclgen;
    }

    public void setTab0501(short tab0501) {
        this.tab0501 = tab0501;
    }

    public short getTab0501() {
        return this.tab0501;
    }
}
