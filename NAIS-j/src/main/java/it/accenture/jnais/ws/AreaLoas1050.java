package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: AREA-LOAS1050<br>
 * Variable: AREA-LOAS1050 from program LOAS1050<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaLoas1050 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: S1050-TP-MOVI
    private String tpMovi = DefaultValues.stringVal(Len.TP_MOVI);
    //Original name: S1050-FORMA-ASS
    private String formaAss = DefaultValues.stringVal(Len.FORMA_ASS);
    //Original name: S1050-COD-RAMO
    private String codRamo = DefaultValues.stringVal(Len.COD_RAMO);
    //Original name: S1050-DT-ULT-ELABORAZIONE
    private String dtUltElaborazione = DefaultValues.stringVal(Len.DT_ULT_ELABORAZIONE);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_LOAS1050;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaLoas1050Bytes(buf);
    }

    public String getAreaLoas1050Formatted() {
        return getAreaDatiFormatted();
    }

    public void setAreaLoas1050Bytes(byte[] buffer) {
        setAreaLoas1050Bytes(buffer, 1);
    }

    public byte[] getAreaLoas1050Bytes() {
        byte[] buffer = new byte[Len.AREA_LOAS1050];
        return getAreaLoas1050Bytes(buffer, 1);
    }

    public void setAreaLoas1050Bytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaDatiBytes(buffer, position);
    }

    public byte[] getAreaLoas1050Bytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaDatiBytes(buffer, position);
        return buffer;
    }

    public String getAreaDatiFormatted() {
        return MarshalByteExt.bufferToStr(getAreaDatiBytes());
    }

    /**Original name: S1050-AREA-DATI<br>
	 * <pre> allineamento 10/09/2008</pre>*/
    public byte[] getAreaDatiBytes() {
        byte[] buffer = new byte[Len.AREA_DATI];
        return getAreaDatiBytes(buffer, 1);
    }

    public void setAreaDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        tpMovi = MarshalByte.readFixedString(buffer, position, Len.TP_MOVI);
        position += Len.TP_MOVI;
        formaAss = MarshalByte.readString(buffer, position, Len.FORMA_ASS);
        position += Len.FORMA_ASS;
        codRamo = MarshalByte.readString(buffer, position, Len.COD_RAMO);
        position += Len.COD_RAMO;
        dtUltElaborazione = MarshalByte.readFixedString(buffer, position, Len.DT_ULT_ELABORAZIONE);
    }

    public byte[] getAreaDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tpMovi, Len.TP_MOVI);
        position += Len.TP_MOVI;
        MarshalByte.writeString(buffer, position, formaAss, Len.FORMA_ASS);
        position += Len.FORMA_ASS;
        MarshalByte.writeString(buffer, position, codRamo, Len.COD_RAMO);
        position += Len.COD_RAMO;
        MarshalByte.writeString(buffer, position, dtUltElaborazione, Len.DT_ULT_ELABORAZIONE);
        return buffer;
    }

    public void setTpMovi(int tpMovi) {
        this.tpMovi = NumericDisplay.asString(tpMovi, Len.TP_MOVI);
    }

    public void setTpMoviFormatted(String tpMovi) {
        this.tpMovi = Trunc.toUnsignedNumeric(tpMovi, Len.TP_MOVI);
    }

    public int getTpMovi() {
        return NumericDisplay.asInt(this.tpMovi);
    }

    public void setFormaAss(String formaAss) {
        this.formaAss = Functions.subString(formaAss, Len.FORMA_ASS);
    }

    public String getFormaAss() {
        return this.formaAss;
    }

    public void setCodRamo(String codRamo) {
        this.codRamo = Functions.subString(codRamo, Len.COD_RAMO);
    }

    public String getCodRamo() {
        return this.codRamo;
    }

    public void setDtUltElaborazioneFormatted(String dtUltElaborazione) {
        this.dtUltElaborazione = Trunc.toUnsignedNumeric(dtUltElaborazione, Len.DT_ULT_ELABORAZIONE);
    }

    public int getDtUltElaborazione() {
        return NumericDisplay.asInt(this.dtUltElaborazione);
    }

    @Override
    public byte[] serialize() {
        return getAreaLoas1050Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_MOVI = 5;
        public static final int FORMA_ASS = 2;
        public static final int COD_RAMO = 12;
        public static final int DT_ULT_ELABORAZIONE = 8;
        public static final int AREA_DATI = TP_MOVI + FORMA_ASS + COD_RAMO + DT_ULT_ELABORAZIONE;
        public static final int AREA_LOAS1050 = AREA_DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
