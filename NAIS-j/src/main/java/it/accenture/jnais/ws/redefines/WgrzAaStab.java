package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-AA-STAB<br>
 * Variable: WGRZ-AA-STAB from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzAaStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzAaStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_AA_STAB;
    }

    public void setWgrzAaStab(int wgrzAaStab) {
        writeIntAsPacked(Pos.WGRZ_AA_STAB, wgrzAaStab, Len.Int.WGRZ_AA_STAB);
    }

    public void setWgrzAaStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_AA_STAB, Pos.WGRZ_AA_STAB);
    }

    /**Original name: WGRZ-AA-STAB<br>*/
    public int getWgrzAaStab() {
        return readPackedAsInt(Pos.WGRZ_AA_STAB, Len.Int.WGRZ_AA_STAB);
    }

    public byte[] getWgrzAaStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_AA_STAB, Pos.WGRZ_AA_STAB);
        return buffer;
    }

    public void initWgrzAaStabSpaces() {
        fill(Pos.WGRZ_AA_STAB, Len.WGRZ_AA_STAB, Types.SPACE_CHAR);
    }

    public void setWgrzAaStabNull(String wgrzAaStabNull) {
        writeString(Pos.WGRZ_AA_STAB_NULL, wgrzAaStabNull, Len.WGRZ_AA_STAB_NULL);
    }

    /**Original name: WGRZ-AA-STAB-NULL<br>*/
    public String getWgrzAaStabNull() {
        return readString(Pos.WGRZ_AA_STAB_NULL, Len.WGRZ_AA_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_AA_STAB = 1;
        public static final int WGRZ_AA_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_AA_STAB = 3;
        public static final int WGRZ_AA_STAB_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_AA_STAB = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
