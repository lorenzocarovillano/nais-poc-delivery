package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-MIN-GARTO<br>
 * Variable: RST-RIS-MIN-GARTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisMinGarto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisMinGarto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_MIN_GARTO;
    }

    public void setRstRisMinGarto(AfDecimal rstRisMinGarto) {
        writeDecimalAsPacked(Pos.RST_RIS_MIN_GARTO, rstRisMinGarto.copy());
    }

    public void setRstRisMinGartoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_MIN_GARTO, Pos.RST_RIS_MIN_GARTO);
    }

    /**Original name: RST-RIS-MIN-GARTO<br>*/
    public AfDecimal getRstRisMinGarto() {
        return readPackedAsDecimal(Pos.RST_RIS_MIN_GARTO, Len.Int.RST_RIS_MIN_GARTO, Len.Fract.RST_RIS_MIN_GARTO);
    }

    public byte[] getRstRisMinGartoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_MIN_GARTO, Pos.RST_RIS_MIN_GARTO);
        return buffer;
    }

    public void setRstRisMinGartoNull(String rstRisMinGartoNull) {
        writeString(Pos.RST_RIS_MIN_GARTO_NULL, rstRisMinGartoNull, Len.RST_RIS_MIN_GARTO_NULL);
    }

    /**Original name: RST-RIS-MIN-GARTO-NULL<br>*/
    public String getRstRisMinGartoNull() {
        return readString(Pos.RST_RIS_MIN_GARTO_NULL, Len.RST_RIS_MIN_GARTO_NULL);
    }

    public String getRstRisMinGartoNullFormatted() {
        return Functions.padBlanks(getRstRisMinGartoNull(), Len.RST_RIS_MIN_GARTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_MIN_GARTO = 1;
        public static final int RST_RIS_MIN_GARTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_MIN_GARTO = 8;
        public static final int RST_RIS_MIN_GARTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_MIN_GARTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_MIN_GARTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
