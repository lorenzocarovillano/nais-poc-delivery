package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WK-ADDRESS<br>
 * Variable: WK-ADDRESS from program IDSS8880<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkAddress extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WK-ADDRESS
    private int wkAddress;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_ADDRESS;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWkAddressFromBuffer(buf);
    }

    public void setWkAddress(int wkAddress) {
        this.wkAddress = wkAddress;
    }

    public void setWkAddressFromBuffer(byte[] buffer, int offset) {
        setWkAddress(MarshalByte.readBinaryInt(buffer, offset));
    }

    public void setWkAddressFromBuffer(byte[] buffer) {
        setWkAddressFromBuffer(buffer, 1);
    }

    public int getWkAddress() {
        return this.wkAddress;
    }

    @Override
    public byte[] serialize() {
        return MarshalByteExt.binIntToBuffer(getWkAddress());
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_ADDRESS = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_ADDRESS = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WK_ADDRESS = 0;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
