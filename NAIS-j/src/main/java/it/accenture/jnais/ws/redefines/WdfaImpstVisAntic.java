package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPST-VIS-ANTIC<br>
 * Variable: WDFA-IMPST-VIS-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpstVisAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpstVisAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPST_VIS_ANTIC;
    }

    public void setWdfaImpstVisAntic(AfDecimal wdfaImpstVisAntic) {
        writeDecimalAsPacked(Pos.WDFA_IMPST_VIS_ANTIC, wdfaImpstVisAntic.copy());
    }

    public void setWdfaImpstVisAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPST_VIS_ANTIC, Pos.WDFA_IMPST_VIS_ANTIC);
    }

    /**Original name: WDFA-IMPST-VIS-ANTIC<br>*/
    public AfDecimal getWdfaImpstVisAntic() {
        return readPackedAsDecimal(Pos.WDFA_IMPST_VIS_ANTIC, Len.Int.WDFA_IMPST_VIS_ANTIC, Len.Fract.WDFA_IMPST_VIS_ANTIC);
    }

    public byte[] getWdfaImpstVisAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPST_VIS_ANTIC, Pos.WDFA_IMPST_VIS_ANTIC);
        return buffer;
    }

    public void setWdfaImpstVisAnticNull(String wdfaImpstVisAnticNull) {
        writeString(Pos.WDFA_IMPST_VIS_ANTIC_NULL, wdfaImpstVisAnticNull, Len.WDFA_IMPST_VIS_ANTIC_NULL);
    }

    /**Original name: WDFA-IMPST-VIS-ANTIC-NULL<br>*/
    public String getWdfaImpstVisAnticNull() {
        return readString(Pos.WDFA_IMPST_VIS_ANTIC_NULL, Len.WDFA_IMPST_VIS_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_VIS_ANTIC = 1;
        public static final int WDFA_IMPST_VIS_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_VIS_ANTIC = 8;
        public static final int WDFA_IMPST_VIS_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_VIS_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_VIS_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
