package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-DIR-EMIS<br>
 * Variable: POL-DIR-EMIS from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDirEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolDirEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_DIR_EMIS;
    }

    public void setPolDirEmis(AfDecimal polDirEmis) {
        writeDecimalAsPacked(Pos.POL_DIR_EMIS, polDirEmis.copy());
    }

    public void setPolDirEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_DIR_EMIS, Pos.POL_DIR_EMIS);
    }

    /**Original name: POL-DIR-EMIS<br>*/
    public AfDecimal getPolDirEmis() {
        return readPackedAsDecimal(Pos.POL_DIR_EMIS, Len.Int.POL_DIR_EMIS, Len.Fract.POL_DIR_EMIS);
    }

    public byte[] getPolDirEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_DIR_EMIS, Pos.POL_DIR_EMIS);
        return buffer;
    }

    public void setPolDirEmisNull(String polDirEmisNull) {
        writeString(Pos.POL_DIR_EMIS_NULL, polDirEmisNull, Len.POL_DIR_EMIS_NULL);
    }

    /**Original name: POL-DIR-EMIS-NULL<br>*/
    public String getPolDirEmisNull() {
        return readString(Pos.POL_DIR_EMIS_NULL, Len.POL_DIR_EMIS_NULL);
    }

    public String getPolDirEmisNullFormatted() {
        return Functions.padBlanks(getPolDirEmisNull(), Len.POL_DIR_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_DIR_EMIS = 1;
        public static final int POL_DIR_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_DIR_EMIS = 8;
        public static final int POL_DIR_EMIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_DIR_EMIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int POL_DIR_EMIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
