package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-DT-EMIS-TIT<br>
 * Variable: TIT-DT-EMIS-TIT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitDtEmisTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitDtEmisTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_DT_EMIS_TIT;
    }

    public void setTitDtEmisTit(int titDtEmisTit) {
        writeIntAsPacked(Pos.TIT_DT_EMIS_TIT, titDtEmisTit, Len.Int.TIT_DT_EMIS_TIT);
    }

    public void setTitDtEmisTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_DT_EMIS_TIT, Pos.TIT_DT_EMIS_TIT);
    }

    /**Original name: TIT-DT-EMIS-TIT<br>*/
    public int getTitDtEmisTit() {
        return readPackedAsInt(Pos.TIT_DT_EMIS_TIT, Len.Int.TIT_DT_EMIS_TIT);
    }

    public byte[] getTitDtEmisTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_DT_EMIS_TIT, Pos.TIT_DT_EMIS_TIT);
        return buffer;
    }

    public void setTitDtEmisTitNull(String titDtEmisTitNull) {
        writeString(Pos.TIT_DT_EMIS_TIT_NULL, titDtEmisTitNull, Len.TIT_DT_EMIS_TIT_NULL);
    }

    /**Original name: TIT-DT-EMIS-TIT-NULL<br>*/
    public String getTitDtEmisTitNull() {
        return readString(Pos.TIT_DT_EMIS_TIT_NULL, Len.TIT_DT_EMIS_TIT_NULL);
    }

    public String getTitDtEmisTitNullFormatted() {
        return Functions.padBlanks(getTitDtEmisTitNull(), Len.TIT_DT_EMIS_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_DT_EMIS_TIT = 1;
        public static final int TIT_DT_EMIS_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_DT_EMIS_TIT = 5;
        public static final int TIT_DT_EMIS_TIT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_DT_EMIS_TIT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
