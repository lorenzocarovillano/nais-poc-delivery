package it.accenture.jnais.ws;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-LIVELLO-GRAVITA<br>
 * Variable: WK-LIVELLO-GRAVITA from program IDSS0150<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkLivelloGravita {

    //==== PROPERTIES ====
    //Original name: WARNING
    private short warning = ((short)2);
    //Original name: ERRORE-BLOCCANTE
    private short erroreBloccante = ((short)3);
    //Original name: ERRORE-FATALE
    private String erroreFatale = "4";

    //==== METHODS ====
    public short getWarning() {
        return this.warning;
    }

    public short getErroreBloccante() {
        return this.erroreBloccante;
    }

    public short getErroreFatale() {
        return NumericDisplay.asShort(this.erroreFatale);
    }

    public String getErroreFataleFormatted() {
        return this.erroreFatale;
    }
}
