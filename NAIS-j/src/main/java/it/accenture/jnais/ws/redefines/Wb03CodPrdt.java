package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-COD-PRDT<br>
 * Variable: WB03-COD-PRDT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CodPrdt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CodPrdt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_COD_PRDT;
    }

    public void setWb03CodPrdt(int wb03CodPrdt) {
        writeIntAsPacked(Pos.WB03_COD_PRDT, wb03CodPrdt, Len.Int.WB03_COD_PRDT);
    }

    public void setWb03CodPrdtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_COD_PRDT, Pos.WB03_COD_PRDT);
    }

    /**Original name: WB03-COD-PRDT<br>*/
    public int getWb03CodPrdt() {
        return readPackedAsInt(Pos.WB03_COD_PRDT, Len.Int.WB03_COD_PRDT);
    }

    public byte[] getWb03CodPrdtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_COD_PRDT, Pos.WB03_COD_PRDT);
        return buffer;
    }

    public void setWb03CodPrdtNull(String wb03CodPrdtNull) {
        writeString(Pos.WB03_COD_PRDT_NULL, wb03CodPrdtNull, Len.WB03_COD_PRDT_NULL);
    }

    /**Original name: WB03-COD-PRDT-NULL<br>*/
    public String getWb03CodPrdtNull() {
        return readString(Pos.WB03_COD_PRDT_NULL, Len.WB03_COD_PRDT_NULL);
    }

    public String getWb03CodPrdtNullFormatted() {
        return Functions.padBlanks(getWb03CodPrdtNull(), Len.WB03_COD_PRDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_COD_PRDT = 1;
        public static final int WB03_COD_PRDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_COD_PRDT = 3;
        public static final int WB03_COD_PRDT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_COD_PRDT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
