package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IAS-ONER-PRVNT<br>
 * Variable: LQU-TOT-IAS-ONER-PRVNT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotIasOnerPrvnt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotIasOnerPrvnt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IAS_ONER_PRVNT;
    }

    public void setLquTotIasOnerPrvnt(AfDecimal lquTotIasOnerPrvnt) {
        writeDecimalAsPacked(Pos.LQU_TOT_IAS_ONER_PRVNT, lquTotIasOnerPrvnt.copy());
    }

    public void setLquTotIasOnerPrvntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IAS_ONER_PRVNT, Pos.LQU_TOT_IAS_ONER_PRVNT);
    }

    /**Original name: LQU-TOT-IAS-ONER-PRVNT<br>*/
    public AfDecimal getLquTotIasOnerPrvnt() {
        return readPackedAsDecimal(Pos.LQU_TOT_IAS_ONER_PRVNT, Len.Int.LQU_TOT_IAS_ONER_PRVNT, Len.Fract.LQU_TOT_IAS_ONER_PRVNT);
    }

    public byte[] getLquTotIasOnerPrvntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IAS_ONER_PRVNT, Pos.LQU_TOT_IAS_ONER_PRVNT);
        return buffer;
    }

    public void setLquTotIasOnerPrvntNull(String lquTotIasOnerPrvntNull) {
        writeString(Pos.LQU_TOT_IAS_ONER_PRVNT_NULL, lquTotIasOnerPrvntNull, Len.LQU_TOT_IAS_ONER_PRVNT_NULL);
    }

    /**Original name: LQU-TOT-IAS-ONER-PRVNT-NULL<br>*/
    public String getLquTotIasOnerPrvntNull() {
        return readString(Pos.LQU_TOT_IAS_ONER_PRVNT_NULL, Len.LQU_TOT_IAS_ONER_PRVNT_NULL);
    }

    public String getLquTotIasOnerPrvntNullFormatted() {
        return Functions.padBlanks(getLquTotIasOnerPrvntNull(), Len.LQU_TOT_IAS_ONER_PRVNT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IAS_ONER_PRVNT = 1;
        public static final int LQU_TOT_IAS_ONER_PRVNT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IAS_ONER_PRVNT = 8;
        public static final int LQU_TOT_IAS_ONER_PRVNT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IAS_ONER_PRVNT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IAS_ONER_PRVNT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
