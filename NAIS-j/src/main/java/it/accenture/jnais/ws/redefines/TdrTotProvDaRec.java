package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-PROV-DA-REC<br>
 * Variable: TDR-TOT-PROV-DA-REC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotProvDaRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotProvDaRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_PROV_DA_REC;
    }

    public void setTdrTotProvDaRec(AfDecimal tdrTotProvDaRec) {
        writeDecimalAsPacked(Pos.TDR_TOT_PROV_DA_REC, tdrTotProvDaRec.copy());
    }

    public void setTdrTotProvDaRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_PROV_DA_REC, Pos.TDR_TOT_PROV_DA_REC);
    }

    /**Original name: TDR-TOT-PROV-DA-REC<br>*/
    public AfDecimal getTdrTotProvDaRec() {
        return readPackedAsDecimal(Pos.TDR_TOT_PROV_DA_REC, Len.Int.TDR_TOT_PROV_DA_REC, Len.Fract.TDR_TOT_PROV_DA_REC);
    }

    public byte[] getTdrTotProvDaRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_PROV_DA_REC, Pos.TDR_TOT_PROV_DA_REC);
        return buffer;
    }

    public void setTdrTotProvDaRecNull(String tdrTotProvDaRecNull) {
        writeString(Pos.TDR_TOT_PROV_DA_REC_NULL, tdrTotProvDaRecNull, Len.TDR_TOT_PROV_DA_REC_NULL);
    }

    /**Original name: TDR-TOT-PROV-DA-REC-NULL<br>*/
    public String getTdrTotProvDaRecNull() {
        return readString(Pos.TDR_TOT_PROV_DA_REC_NULL, Len.TDR_TOT_PROV_DA_REC_NULL);
    }

    public String getTdrTotProvDaRecNullFormatted() {
        return Functions.padBlanks(getTdrTotProvDaRecNull(), Len.TDR_TOT_PROV_DA_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PROV_DA_REC = 1;
        public static final int TDR_TOT_PROV_DA_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PROV_DA_REC = 8;
        public static final int TDR_TOT_PROV_DA_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PROV_DA_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PROV_DA_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
