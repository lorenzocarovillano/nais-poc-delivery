package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCO-IMP-SCON<br>
 * Variable: DCO-IMP-SCON from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DcoImpScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DcoImpScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DCO_IMP_SCON;
    }

    public void setDcoImpScon(AfDecimal dcoImpScon) {
        writeDecimalAsPacked(Pos.DCO_IMP_SCON, dcoImpScon.copy());
    }

    public void setDcoImpSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DCO_IMP_SCON, Pos.DCO_IMP_SCON);
    }

    /**Original name: DCO-IMP-SCON<br>*/
    public AfDecimal getDcoImpScon() {
        return readPackedAsDecimal(Pos.DCO_IMP_SCON, Len.Int.DCO_IMP_SCON, Len.Fract.DCO_IMP_SCON);
    }

    public byte[] getDcoImpSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DCO_IMP_SCON, Pos.DCO_IMP_SCON);
        return buffer;
    }

    public void setDcoImpSconNull(String dcoImpSconNull) {
        writeString(Pos.DCO_IMP_SCON_NULL, dcoImpSconNull, Len.DCO_IMP_SCON_NULL);
    }

    /**Original name: DCO-IMP-SCON-NULL<br>*/
    public String getDcoImpSconNull() {
        return readString(Pos.DCO_IMP_SCON_NULL, Len.DCO_IMP_SCON_NULL);
    }

    public String getDcoImpSconNullFormatted() {
        return Functions.padBlanks(getDcoImpSconNull(), Len.DCO_IMP_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DCO_IMP_SCON = 1;
        public static final int DCO_IMP_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_IMP_SCON = 8;
        public static final int DCO_IMP_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_IMP_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DCO_IMP_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
