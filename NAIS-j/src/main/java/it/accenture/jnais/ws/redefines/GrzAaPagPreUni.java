package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-AA-PAG-PRE-UNI<br>
 * Variable: GRZ-AA-PAG-PRE-UNI from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzAaPagPreUni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzAaPagPreUni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_AA_PAG_PRE_UNI;
    }

    public void setGrzAaPagPreUni(int grzAaPagPreUni) {
        writeIntAsPacked(Pos.GRZ_AA_PAG_PRE_UNI, grzAaPagPreUni, Len.Int.GRZ_AA_PAG_PRE_UNI);
    }

    public void setGrzAaPagPreUniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_AA_PAG_PRE_UNI, Pos.GRZ_AA_PAG_PRE_UNI);
    }

    /**Original name: GRZ-AA-PAG-PRE-UNI<br>*/
    public int getGrzAaPagPreUni() {
        return readPackedAsInt(Pos.GRZ_AA_PAG_PRE_UNI, Len.Int.GRZ_AA_PAG_PRE_UNI);
    }

    public byte[] getGrzAaPagPreUniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_AA_PAG_PRE_UNI, Pos.GRZ_AA_PAG_PRE_UNI);
        return buffer;
    }

    public void setGrzAaPagPreUniNull(String grzAaPagPreUniNull) {
        writeString(Pos.GRZ_AA_PAG_PRE_UNI_NULL, grzAaPagPreUniNull, Len.GRZ_AA_PAG_PRE_UNI_NULL);
    }

    /**Original name: GRZ-AA-PAG-PRE-UNI-NULL<br>*/
    public String getGrzAaPagPreUniNull() {
        return readString(Pos.GRZ_AA_PAG_PRE_UNI_NULL, Len.GRZ_AA_PAG_PRE_UNI_NULL);
    }

    public String getGrzAaPagPreUniNullFormatted() {
        return Functions.padBlanks(getGrzAaPagPreUniNull(), Len.GRZ_AA_PAG_PRE_UNI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_AA_PAG_PRE_UNI = 1;
        public static final int GRZ_AA_PAG_PRE_UNI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_AA_PAG_PRE_UNI = 3;
        public static final int GRZ_AA_PAG_PRE_UNI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_AA_PAG_PRE_UNI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
