package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WK-ANNO-RIF-X<br>
 * Variable: WK-ANNO-RIF-X from program LRGS0660<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkAnnoRifX extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkAnnoRifX() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_ANNO_RIF_X;
    }

    public void setWkAnnoRifX(String wkAnnoRifX) {
        writeString(Pos.WK_ANNO_RIF_X, wkAnnoRifX, Len.WK_ANNO_RIF_X);
    }

    /**Original name: WK-ANNO-RIF-X<br>*/
    public String getWkAnnoRifX() {
        return readString(Pos.WK_ANNO_RIF_X, Len.WK_ANNO_RIF_X);
    }

    /**Original name: WK-ANNO-RIF<br>*/
    public short getWkAnnoRif() {
        return readNumDispUnsignedShort(Pos.WK_ANNO_RIF, Len.WK_ANNO_RIF);
    }

    public String getWkAnnoRifFormatted() {
        return readFixedString(Pos.WK_ANNO_RIF, Len.WK_ANNO_RIF);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_ANNO_RIF_X = 1;
        public static final int WK_ANNO_RIF = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_ANNO_RIF_X = 4;
        public static final int WK_ANNO_RIF = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
