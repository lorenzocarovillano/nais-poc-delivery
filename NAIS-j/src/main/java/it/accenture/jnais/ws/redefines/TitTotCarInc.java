package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-CAR-INC<br>
 * Variable: TIT-TOT-CAR-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotCarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotCarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_CAR_INC;
    }

    public void setTitTotCarInc(AfDecimal titTotCarInc) {
        writeDecimalAsPacked(Pos.TIT_TOT_CAR_INC, titTotCarInc.copy());
    }

    public void setTitTotCarIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_CAR_INC, Pos.TIT_TOT_CAR_INC);
    }

    /**Original name: TIT-TOT-CAR-INC<br>*/
    public AfDecimal getTitTotCarInc() {
        return readPackedAsDecimal(Pos.TIT_TOT_CAR_INC, Len.Int.TIT_TOT_CAR_INC, Len.Fract.TIT_TOT_CAR_INC);
    }

    public byte[] getTitTotCarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_CAR_INC, Pos.TIT_TOT_CAR_INC);
        return buffer;
    }

    public void setTitTotCarIncNull(String titTotCarIncNull) {
        writeString(Pos.TIT_TOT_CAR_INC_NULL, titTotCarIncNull, Len.TIT_TOT_CAR_INC_NULL);
    }

    /**Original name: TIT-TOT-CAR-INC-NULL<br>*/
    public String getTitTotCarIncNull() {
        return readString(Pos.TIT_TOT_CAR_INC_NULL, Len.TIT_TOT_CAR_INC_NULL);
    }

    public String getTitTotCarIncNullFormatted() {
        return Functions.padBlanks(getTitTotCarIncNull(), Len.TIT_TOT_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_CAR_INC = 1;
        public static final int TIT_TOT_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_CAR_INC = 8;
        public static final int TIT_TOT_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
