package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: STRUCT-NUM<br>
 * Variable: STRUCT-NUM from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class StructNum extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELE_NUM_IMP_MAXOCCURS = 18;

    //==== CONSTRUCTORS ====
    public StructNum() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.STRUCT_NUM;
    }

    public void setStructNum(long structNum) {
        writeLong(Pos.STRUCT_NUM, structNum, Len.Int.STRUCT_NUM, SignType.NO_SIGN);
    }

    /**Original name: STRUCT-NUM<br>*/
    public long getStructNum() {
        return readNumDispUnsignedLong(Pos.STRUCT_NUM, Len.STRUCT_NUM);
    }

    public void setStructImp(AfDecimal structImp) {
        writeDecimal(Pos.STRUCT_IMP, structImp.copy(), SignType.NO_SIGN);
    }

    /**Original name: STRUCT-IMP<br>*/
    public AfDecimal getStructImp() {
        return readDecimal(Pos.STRUCT_IMP, Len.Int.STRUCT_IMP, Len.Fract.STRUCT_IMP, SignType.NO_SIGN);
    }

    public void setStructPerc(AfDecimal structPerc) {
        writeDecimal(Pos.STRUCT_PERC, structPerc.copy(), SignType.NO_SIGN);
    }

    /**Original name: STRUCT-PERC<br>*/
    public AfDecimal getStructPerc() {
        return readDecimal(Pos.STRUCT_PERC, Len.Int.STRUCT_PERC, Len.Fract.STRUCT_PERC, SignType.NO_SIGN);
    }

    /**Original name: ELE-NUM-IMP<br>*/
    public char getEleNumImp(int eleNumImpIdx) {
        int position = Pos.eleNumImp(eleNumImpIdx - 1);
        return readChar(position);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int STRUCT_NUM = 1;
        public static final int STRUCT_IMP = 1;
        public static final int STRUCT_PERC = 1;
        public static final int ELEMENT_NUM_IMP = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int eleNumImp(int idx) {
            return ELEMENT_NUM_IMP + idx * Len.ELE_NUM_IMP;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_NUM_IMP = 1;
        public static final int STRUCT_NUM = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int STRUCT_NUM = 18;
            public static final int STRUCT_IMP = 11;
            public static final int STRUCT_PERC = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int STRUCT_IMP = 7;
            public static final int STRUCT_PERC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
