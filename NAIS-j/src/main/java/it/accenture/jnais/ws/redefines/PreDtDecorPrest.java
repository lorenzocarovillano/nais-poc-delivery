package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-DT-DECOR-PREST<br>
 * Variable: PRE-DT-DECOR-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreDtDecorPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreDtDecorPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_DT_DECOR_PREST;
    }

    public void setPreDtDecorPrest(int preDtDecorPrest) {
        writeIntAsPacked(Pos.PRE_DT_DECOR_PREST, preDtDecorPrest, Len.Int.PRE_DT_DECOR_PREST);
    }

    public void setPreDtDecorPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_DT_DECOR_PREST, Pos.PRE_DT_DECOR_PREST);
    }

    /**Original name: PRE-DT-DECOR-PREST<br>*/
    public int getPreDtDecorPrest() {
        return readPackedAsInt(Pos.PRE_DT_DECOR_PREST, Len.Int.PRE_DT_DECOR_PREST);
    }

    public byte[] getPreDtDecorPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_DT_DECOR_PREST, Pos.PRE_DT_DECOR_PREST);
        return buffer;
    }

    public void setPreDtDecorPrestNull(String preDtDecorPrestNull) {
        writeString(Pos.PRE_DT_DECOR_PREST_NULL, preDtDecorPrestNull, Len.PRE_DT_DECOR_PREST_NULL);
    }

    /**Original name: PRE-DT-DECOR-PREST-NULL<br>*/
    public String getPreDtDecorPrestNull() {
        return readString(Pos.PRE_DT_DECOR_PREST_NULL, Len.PRE_DT_DECOR_PREST_NULL);
    }

    public String getPreDtDecorPrestNullFormatted() {
        return Functions.padBlanks(getPreDtDecorPrestNull(), Len.PRE_DT_DECOR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_DT_DECOR_PREST = 1;
        public static final int PRE_DT_DECOR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_DT_DECOR_PREST = 5;
        public static final int PRE_DT_DECOR_PREST_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_DT_DECOR_PREST = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
