package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-PRLCOS<br>
 * Variable: PCO-DT-ULT-ELAB-PRLCOS from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabPrlcos extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabPrlcos() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_PRLCOS;
    }

    public void setPcoDtUltElabPrlcos(int pcoDtUltElabPrlcos) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_PRLCOS, pcoDtUltElabPrlcos, Len.Int.PCO_DT_ULT_ELAB_PRLCOS);
    }

    public void setPcoDtUltElabPrlcosFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_PRLCOS, Pos.PCO_DT_ULT_ELAB_PRLCOS);
    }

    /**Original name: PCO-DT-ULT-ELAB-PRLCOS<br>*/
    public int getPcoDtUltElabPrlcos() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_PRLCOS, Len.Int.PCO_DT_ULT_ELAB_PRLCOS);
    }

    public byte[] getPcoDtUltElabPrlcosAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_PRLCOS, Pos.PCO_DT_ULT_ELAB_PRLCOS);
        return buffer;
    }

    public void initPcoDtUltElabPrlcosHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_PRLCOS, Len.PCO_DT_ULT_ELAB_PRLCOS, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabPrlcosNull(String pcoDtUltElabPrlcosNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_PRLCOS_NULL, pcoDtUltElabPrlcosNull, Len.PCO_DT_ULT_ELAB_PRLCOS_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-PRLCOS-NULL<br>*/
    public String getPcoDtUltElabPrlcosNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_PRLCOS_NULL, Len.PCO_DT_ULT_ELAB_PRLCOS_NULL);
    }

    public String getPcoDtUltElabPrlcosNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabPrlcosNull(), Len.PCO_DT_ULT_ELAB_PRLCOS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_PRLCOS = 1;
        public static final int PCO_DT_ULT_ELAB_PRLCOS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_PRLCOS = 5;
        public static final int PCO_DT_ULT_ELAB_PRLCOS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_PRLCOS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
