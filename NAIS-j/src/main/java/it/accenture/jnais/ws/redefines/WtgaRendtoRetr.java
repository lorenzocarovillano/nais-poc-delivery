package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-RENDTO-RETR<br>
 * Variable: WTGA-RENDTO-RETR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaRendtoRetr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaRendtoRetr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_RENDTO_RETR;
    }

    public void setWtgaRendtoRetr(AfDecimal wtgaRendtoRetr) {
        writeDecimalAsPacked(Pos.WTGA_RENDTO_RETR, wtgaRendtoRetr.copy());
    }

    public void setWtgaRendtoRetrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_RENDTO_RETR, Pos.WTGA_RENDTO_RETR);
    }

    /**Original name: WTGA-RENDTO-RETR<br>*/
    public AfDecimal getWtgaRendtoRetr() {
        return readPackedAsDecimal(Pos.WTGA_RENDTO_RETR, Len.Int.WTGA_RENDTO_RETR, Len.Fract.WTGA_RENDTO_RETR);
    }

    public byte[] getWtgaRendtoRetrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_RENDTO_RETR, Pos.WTGA_RENDTO_RETR);
        return buffer;
    }

    public void initWtgaRendtoRetrSpaces() {
        fill(Pos.WTGA_RENDTO_RETR, Len.WTGA_RENDTO_RETR, Types.SPACE_CHAR);
    }

    public void setWtgaRendtoRetrNull(String wtgaRendtoRetrNull) {
        writeString(Pos.WTGA_RENDTO_RETR_NULL, wtgaRendtoRetrNull, Len.WTGA_RENDTO_RETR_NULL);
    }

    /**Original name: WTGA-RENDTO-RETR-NULL<br>*/
    public String getWtgaRendtoRetrNull() {
        return readString(Pos.WTGA_RENDTO_RETR_NULL, Len.WTGA_RENDTO_RETR_NULL);
    }

    public String getWtgaRendtoRetrNullFormatted() {
        return Functions.padBlanks(getWtgaRendtoRetrNull(), Len.WTGA_RENDTO_RETR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_RENDTO_RETR = 1;
        public static final int WTGA_RENDTO_RETR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_RENDTO_RETR = 8;
        public static final int WTGA_RENDTO_RETR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_RENDTO_RETR = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_RENDTO_RETR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
