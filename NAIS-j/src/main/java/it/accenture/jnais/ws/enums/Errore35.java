package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: ERRORE-35<br>
 * Variable: ERRORE-35 from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Errore35 {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setErrore35(char errore35) {
        this.value = errore35;
    }

    public char getErrore35() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
