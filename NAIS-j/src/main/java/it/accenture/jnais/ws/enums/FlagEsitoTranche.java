package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-ESITO-TRANCHE<br>
 * Variable: FLAG-ESITO-TRANCHE from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagEsitoTranche {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagEsitoTranche(char flagEsitoTranche) {
        this.value = flagEsitoTranche;
    }

    public char getFlagEsitoTranche() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
