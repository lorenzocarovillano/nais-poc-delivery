package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-SPE-RCS<br>
 * Variable: LQU-SPE-RCS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquSpeRcs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquSpeRcs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_SPE_RCS;
    }

    public void setLquSpeRcs(AfDecimal lquSpeRcs) {
        writeDecimalAsPacked(Pos.LQU_SPE_RCS, lquSpeRcs.copy());
    }

    public void setLquSpeRcsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_SPE_RCS, Pos.LQU_SPE_RCS);
    }

    /**Original name: LQU-SPE-RCS<br>*/
    public AfDecimal getLquSpeRcs() {
        return readPackedAsDecimal(Pos.LQU_SPE_RCS, Len.Int.LQU_SPE_RCS, Len.Fract.LQU_SPE_RCS);
    }

    public byte[] getLquSpeRcsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_SPE_RCS, Pos.LQU_SPE_RCS);
        return buffer;
    }

    public void setLquSpeRcsNull(String lquSpeRcsNull) {
        writeString(Pos.LQU_SPE_RCS_NULL, lquSpeRcsNull, Len.LQU_SPE_RCS_NULL);
    }

    /**Original name: LQU-SPE-RCS-NULL<br>*/
    public String getLquSpeRcsNull() {
        return readString(Pos.LQU_SPE_RCS_NULL, Len.LQU_SPE_RCS_NULL);
    }

    public String getLquSpeRcsNullFormatted() {
        return Functions.padBlanks(getLquSpeRcsNull(), Len.LQU_SPE_RCS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_SPE_RCS = 1;
        public static final int LQU_SPE_RCS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_SPE_RCS = 8;
        public static final int LQU_SPE_RCS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_SPE_RCS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_SPE_RCS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
