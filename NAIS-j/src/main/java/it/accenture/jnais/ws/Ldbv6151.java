package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LDBV6151<br>
 * Variable: LDBV6151 from copybook LDBV6151<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv6151 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV6151-TP-TIT-01
    private String tpTit01 = DefaultValues.stringVal(Len.TP_TIT01);
    //Original name: LDBV6151-TP-TIT-02
    private String tpTit02 = DefaultValues.stringVal(Len.TP_TIT02);
    //Original name: LDBV6151-TP-STAT-TIT
    private String tpStatTit = DefaultValues.stringVal(Len.TP_STAT_TIT);
    //Original name: LDBV6151-DT-MAX
    private String dtMax = DefaultValues.stringVal(Len.DT_MAX);
    //Original name: LDBV6151-DT-MAX-DB
    private String dtMaxDb = DefaultValues.stringVal(Len.DT_MAX_DB);
    //Original name: LDBV6151-DT-MAX-IND
    private short dtMaxInd = DefaultValues.BIN_SHORT_VAL;
    //Original name: LDBV6151-DT-DECOR-PREST
    private int dtDecorPrest = DefaultValues.INT_VAL;
    //Original name: LDBV6151-DT-DECOR-PREST-DB
    private String dtDecorPrestDb = DefaultValues.stringVal(Len.DT_DECOR_PREST_DB);
    //Original name: LDBV6151-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV6151-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV6151;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv6151Bytes(buf);
    }

    public String getLdbv6151Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv6151Bytes());
    }

    public void setLdbv6151Bytes(byte[] buffer) {
        setLdbv6151Bytes(buffer, 1);
    }

    public byte[] getLdbv6151Bytes() {
        byte[] buffer = new byte[Len.LDBV6151];
        return getLdbv6151Bytes(buffer, 1);
    }

    public void setLdbv6151Bytes(byte[] buffer, int offset) {
        int position = offset;
        tpTit01 = MarshalByte.readString(buffer, position, Len.TP_TIT01);
        position += Len.TP_TIT01;
        tpTit02 = MarshalByte.readString(buffer, position, Len.TP_TIT02);
        position += Len.TP_TIT02;
        tpStatTit = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT);
        position += Len.TP_STAT_TIT;
        dtMax = MarshalByte.readFixedString(buffer, position, Len.DT_MAX);
        position += Len.DT_MAX;
        dtMaxDb = MarshalByte.readString(buffer, position, Len.DT_MAX_DB);
        position += Len.DT_MAX_DB;
        dtMaxInd = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        dtDecorPrest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DECOR_PREST, 0);
        position += Len.DT_DECOR_PREST;
        dtDecorPrestDb = MarshalByte.readString(buffer, position, Len.DT_DECOR_PREST_DB);
        position += Len.DT_DECOR_PREST_DB;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
    }

    public byte[] getLdbv6151Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tpTit01, Len.TP_TIT01);
        position += Len.TP_TIT01;
        MarshalByte.writeString(buffer, position, tpTit02, Len.TP_TIT02);
        position += Len.TP_TIT02;
        MarshalByte.writeString(buffer, position, tpStatTit, Len.TP_STAT_TIT);
        position += Len.TP_STAT_TIT;
        MarshalByte.writeString(buffer, position, dtMax, Len.DT_MAX);
        position += Len.DT_MAX;
        MarshalByte.writeString(buffer, position, dtMaxDb, Len.DT_MAX_DB);
        position += Len.DT_MAX_DB;
        MarshalByte.writeBinaryShort(buffer, position, dtMaxInd);
        position += Types.SHORT_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dtDecorPrest, Len.Int.DT_DECOR_PREST, 0);
        position += Len.DT_DECOR_PREST;
        MarshalByte.writeString(buffer, position, dtDecorPrestDb, Len.DT_DECOR_PREST_DB);
        position += Len.DT_DECOR_PREST_DB;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        return buffer;
    }

    public void setTpTit01(String tpTit01) {
        this.tpTit01 = Functions.subString(tpTit01, Len.TP_TIT01);
    }

    public String getTpTit01() {
        return this.tpTit01;
    }

    public void setTpTit02(String tpTit02) {
        this.tpTit02 = Functions.subString(tpTit02, Len.TP_TIT02);
    }

    public String getTpTit02() {
        return this.tpTit02;
    }

    public void setTpStatTit(String tpStatTit) {
        this.tpStatTit = Functions.subString(tpStatTit, Len.TP_STAT_TIT);
    }

    public String getTpStatTit() {
        return this.tpStatTit;
    }

    public void setDtMaxFormatted(String dtMax) {
        this.dtMax = Trunc.toUnsignedNumeric(dtMax, Len.DT_MAX);
    }

    public int getDtMax() {
        return NumericDisplay.asInt(this.dtMax);
    }

    public String getDtMaxFormatted() {
        return this.dtMax;
    }

    public void setDtMaxDb(String dtMaxDb) {
        this.dtMaxDb = Functions.subString(dtMaxDb, Len.DT_MAX_DB);
    }

    public String getDtMaxDb() {
        return this.dtMaxDb;
    }

    public void setDtMaxInd(short dtMaxInd) {
        this.dtMaxInd = dtMaxInd;
    }

    public short getDtMaxInd() {
        return this.dtMaxInd;
    }

    public void setDtDecorPrest(int dtDecorPrest) {
        this.dtDecorPrest = dtDecorPrest;
    }

    public int getDtDecorPrest() {
        return this.dtDecorPrest;
    }

    public void setDtDecorPrestDb(String dtDecorPrestDb) {
        this.dtDecorPrestDb = Functions.subString(dtDecorPrestDb, Len.DT_DECOR_PREST_DB);
    }

    public String getDtDecorPrestDb() {
        return this.dtDecorPrestDb;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    @Override
    public byte[] serialize() {
        return getLdbv6151Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_TIT01 = 2;
        public static final int TP_TIT02 = 2;
        public static final int TP_STAT_TIT = 2;
        public static final int DT_MAX = 8;
        public static final int DT_MAX_DB = 10;
        public static final int DT_MAX_IND = 2;
        public static final int DT_DECOR_PREST = 5;
        public static final int DT_DECOR_PREST_DB = 10;
        public static final int TP_OGG = 2;
        public static final int ID_OGG = 5;
        public static final int LDBV6151 = TP_TIT01 + TP_TIT02 + TP_STAT_TIT + DT_MAX + DT_MAX_DB + DT_MAX_IND + DT_DECOR_PREST + DT_DECOR_PREST_DB + TP_OGG + ID_OGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DT_DECOR_PREST = 8;
            public static final int ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
