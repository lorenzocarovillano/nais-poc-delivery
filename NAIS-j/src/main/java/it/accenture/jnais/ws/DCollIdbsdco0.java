package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.DcoDtScadAdesDflt;
import it.accenture.jnais.ws.redefines.DcoDtUltRinnTac;
import it.accenture.jnais.ws.redefines.DcoDurAaAdesDflt;
import it.accenture.jnais.ws.redefines.DcoDurGgAdesDflt;
import it.accenture.jnais.ws.redefines.DcoDurMmAdesDflt;
import it.accenture.jnais.ws.redefines.DcoEtaScadFemmDflt;
import it.accenture.jnais.ws.redefines.DcoEtaScadMascDflt;
import it.accenture.jnais.ws.redefines.DcoFrazDflt;
import it.accenture.jnais.ws.redefines.DcoIdMoviChiu;
import it.accenture.jnais.ws.redefines.DcoImpArrotPre;
import it.accenture.jnais.ws.redefines.DcoImpScon;
import it.accenture.jnais.ws.redefines.DcoPcScon;

/**Original name: D-COLL<br>
 * Variable: D-COLL from copybook IDBVDCO1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DCollIdbsdco0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: DCO-ID-D-COLL
    private int dcoIdDColl = DefaultValues.INT_VAL;
    //Original name: DCO-ID-POLI
    private int dcoIdPoli = DefaultValues.INT_VAL;
    //Original name: DCO-ID-MOVI-CRZ
    private int dcoIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: DCO-ID-MOVI-CHIU
    private DcoIdMoviChiu dcoIdMoviChiu = new DcoIdMoviChiu();
    //Original name: DCO-DT-INI-EFF
    private int dcoDtIniEff = DefaultValues.INT_VAL;
    //Original name: DCO-DT-END-EFF
    private int dcoDtEndEff = DefaultValues.INT_VAL;
    //Original name: DCO-COD-COMP-ANIA
    private int dcoCodCompAnia = DefaultValues.INT_VAL;
    //Original name: DCO-IMP-ARROT-PRE
    private DcoImpArrotPre dcoImpArrotPre = new DcoImpArrotPre();
    //Original name: DCO-PC-SCON
    private DcoPcScon dcoPcScon = new DcoPcScon();
    //Original name: DCO-FL-ADES-SING
    private char dcoFlAdesSing = DefaultValues.CHAR_VAL;
    //Original name: DCO-TP-IMP
    private String dcoTpImp = DefaultValues.stringVal(Len.DCO_TP_IMP);
    //Original name: DCO-FL-RICL-PRE-DA-CPT
    private char dcoFlRiclPreDaCpt = DefaultValues.CHAR_VAL;
    //Original name: DCO-TP-ADES
    private String dcoTpAdes = DefaultValues.stringVal(Len.DCO_TP_ADES);
    //Original name: DCO-DT-ULT-RINN-TAC
    private DcoDtUltRinnTac dcoDtUltRinnTac = new DcoDtUltRinnTac();
    //Original name: DCO-IMP-SCON
    private DcoImpScon dcoImpScon = new DcoImpScon();
    //Original name: DCO-FRAZ-DFLT
    private DcoFrazDflt dcoFrazDflt = new DcoFrazDflt();
    //Original name: DCO-ETA-SCAD-MASC-DFLT
    private DcoEtaScadMascDflt dcoEtaScadMascDflt = new DcoEtaScadMascDflt();
    //Original name: DCO-ETA-SCAD-FEMM-DFLT
    private DcoEtaScadFemmDflt dcoEtaScadFemmDflt = new DcoEtaScadFemmDflt();
    //Original name: DCO-TP-DFLT-DUR
    private String dcoTpDfltDur = DefaultValues.stringVal(Len.DCO_TP_DFLT_DUR);
    //Original name: DCO-DUR-AA-ADES-DFLT
    private DcoDurAaAdesDflt dcoDurAaAdesDflt = new DcoDurAaAdesDflt();
    //Original name: DCO-DUR-MM-ADES-DFLT
    private DcoDurMmAdesDflt dcoDurMmAdesDflt = new DcoDurMmAdesDflt();
    //Original name: DCO-DUR-GG-ADES-DFLT
    private DcoDurGgAdesDflt dcoDurGgAdesDflt = new DcoDurGgAdesDflt();
    //Original name: DCO-DT-SCAD-ADES-DFLT
    private DcoDtScadAdesDflt dcoDtScadAdesDflt = new DcoDtScadAdesDflt();
    //Original name: DCO-COD-FND-DFLT
    private String dcoCodFndDflt = DefaultValues.stringVal(Len.DCO_COD_FND_DFLT);
    //Original name: DCO-TP-DUR
    private String dcoTpDur = DefaultValues.stringVal(Len.DCO_TP_DUR);
    //Original name: DCO-TP-CALC-DUR
    private String dcoTpCalcDur = DefaultValues.stringVal(Len.DCO_TP_CALC_DUR);
    //Original name: DCO-FL-NO-ADERENTI
    private char dcoFlNoAderenti = DefaultValues.CHAR_VAL;
    //Original name: DCO-FL-DISTINTA-CNBTVA
    private char dcoFlDistintaCnbtva = DefaultValues.CHAR_VAL;
    //Original name: DCO-FL-CNBT-AUTES
    private char dcoFlCnbtAutes = DefaultValues.CHAR_VAL;
    //Original name: DCO-FL-QTZ-POST-EMIS
    private char dcoFlQtzPostEmis = DefaultValues.CHAR_VAL;
    //Original name: DCO-FL-COMNZ-FND-IS
    private char dcoFlComnzFndIs = DefaultValues.CHAR_VAL;
    //Original name: DCO-DS-RIGA
    private long dcoDsRiga = DefaultValues.LONG_VAL;
    //Original name: DCO-DS-OPER-SQL
    private char dcoDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: DCO-DS-VER
    private int dcoDsVer = DefaultValues.INT_VAL;
    //Original name: DCO-DS-TS-INI-CPTZ
    private long dcoDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: DCO-DS-TS-END-CPTZ
    private long dcoDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: DCO-DS-UTENTE
    private String dcoDsUtente = DefaultValues.stringVal(Len.DCO_DS_UTENTE);
    //Original name: DCO-DS-STATO-ELAB
    private char dcoDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.D_COLL;
    }

    @Override
    public void deserialize(byte[] buf) {
        setdCollBytes(buf);
    }

    public void setdCollFormatted(String data) {
        byte[] buffer = new byte[Len.D_COLL];
        MarshalByte.writeString(buffer, 1, data, Len.D_COLL);
        setdCollBytes(buffer, 1);
    }

    public String getdCollFormatted() {
        return MarshalByteExt.bufferToStr(getdCollBytes());
    }

    public void setdCollBytes(byte[] buffer) {
        setdCollBytes(buffer, 1);
    }

    public byte[] getdCollBytes() {
        byte[] buffer = new byte[Len.D_COLL];
        return getdCollBytes(buffer, 1);
    }

    public void setdCollBytes(byte[] buffer, int offset) {
        int position = offset;
        dcoIdDColl = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DCO_ID_D_COLL, 0);
        position += Len.DCO_ID_D_COLL;
        dcoIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DCO_ID_POLI, 0);
        position += Len.DCO_ID_POLI;
        dcoIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DCO_ID_MOVI_CRZ, 0);
        position += Len.DCO_ID_MOVI_CRZ;
        dcoIdMoviChiu.setDcoIdMoviChiuFromBuffer(buffer, position);
        position += DcoIdMoviChiu.Len.DCO_ID_MOVI_CHIU;
        dcoDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DCO_DT_INI_EFF, 0);
        position += Len.DCO_DT_INI_EFF;
        dcoDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DCO_DT_END_EFF, 0);
        position += Len.DCO_DT_END_EFF;
        dcoCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DCO_COD_COMP_ANIA, 0);
        position += Len.DCO_COD_COMP_ANIA;
        dcoImpArrotPre.setDcoImpArrotPreFromBuffer(buffer, position);
        position += DcoImpArrotPre.Len.DCO_IMP_ARROT_PRE;
        dcoPcScon.setDcoPcSconFromBuffer(buffer, position);
        position += DcoPcScon.Len.DCO_PC_SCON;
        dcoFlAdesSing = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dcoTpImp = MarshalByte.readString(buffer, position, Len.DCO_TP_IMP);
        position += Len.DCO_TP_IMP;
        dcoFlRiclPreDaCpt = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dcoTpAdes = MarshalByte.readString(buffer, position, Len.DCO_TP_ADES);
        position += Len.DCO_TP_ADES;
        dcoDtUltRinnTac.setDcoDtUltRinnTacFromBuffer(buffer, position);
        position += DcoDtUltRinnTac.Len.DCO_DT_ULT_RINN_TAC;
        dcoImpScon.setDcoImpSconFromBuffer(buffer, position);
        position += DcoImpScon.Len.DCO_IMP_SCON;
        dcoFrazDflt.setDcoFrazDfltFromBuffer(buffer, position);
        position += DcoFrazDflt.Len.DCO_FRAZ_DFLT;
        dcoEtaScadMascDflt.setDcoEtaScadMascDfltFromBuffer(buffer, position);
        position += DcoEtaScadMascDflt.Len.DCO_ETA_SCAD_MASC_DFLT;
        dcoEtaScadFemmDflt.setDcoEtaScadFemmDfltFromBuffer(buffer, position);
        position += DcoEtaScadFemmDflt.Len.DCO_ETA_SCAD_FEMM_DFLT;
        dcoTpDfltDur = MarshalByte.readString(buffer, position, Len.DCO_TP_DFLT_DUR);
        position += Len.DCO_TP_DFLT_DUR;
        dcoDurAaAdesDflt.setDcoDurAaAdesDfltFromBuffer(buffer, position);
        position += DcoDurAaAdesDflt.Len.DCO_DUR_AA_ADES_DFLT;
        dcoDurMmAdesDflt.setDcoDurMmAdesDfltFromBuffer(buffer, position);
        position += DcoDurMmAdesDflt.Len.DCO_DUR_MM_ADES_DFLT;
        dcoDurGgAdesDflt.setDcoDurGgAdesDfltFromBuffer(buffer, position);
        position += DcoDurGgAdesDflt.Len.DCO_DUR_GG_ADES_DFLT;
        dcoDtScadAdesDflt.setDcoDtScadAdesDfltFromBuffer(buffer, position);
        position += DcoDtScadAdesDflt.Len.DCO_DT_SCAD_ADES_DFLT;
        dcoCodFndDflt = MarshalByte.readString(buffer, position, Len.DCO_COD_FND_DFLT);
        position += Len.DCO_COD_FND_DFLT;
        dcoTpDur = MarshalByte.readString(buffer, position, Len.DCO_TP_DUR);
        position += Len.DCO_TP_DUR;
        dcoTpCalcDur = MarshalByte.readString(buffer, position, Len.DCO_TP_CALC_DUR);
        position += Len.DCO_TP_CALC_DUR;
        dcoFlNoAderenti = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dcoFlDistintaCnbtva = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dcoFlCnbtAutes = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dcoFlQtzPostEmis = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dcoFlComnzFndIs = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dcoDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DCO_DS_RIGA, 0);
        position += Len.DCO_DS_RIGA;
        dcoDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dcoDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DCO_DS_VER, 0);
        position += Len.DCO_DS_VER;
        dcoDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DCO_DS_TS_INI_CPTZ, 0);
        position += Len.DCO_DS_TS_INI_CPTZ;
        dcoDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DCO_DS_TS_END_CPTZ, 0);
        position += Len.DCO_DS_TS_END_CPTZ;
        dcoDsUtente = MarshalByte.readString(buffer, position, Len.DCO_DS_UTENTE);
        position += Len.DCO_DS_UTENTE;
        dcoDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getdCollBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, dcoIdDColl, Len.Int.DCO_ID_D_COLL, 0);
        position += Len.DCO_ID_D_COLL;
        MarshalByte.writeIntAsPacked(buffer, position, dcoIdPoli, Len.Int.DCO_ID_POLI, 0);
        position += Len.DCO_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, dcoIdMoviCrz, Len.Int.DCO_ID_MOVI_CRZ, 0);
        position += Len.DCO_ID_MOVI_CRZ;
        dcoIdMoviChiu.getDcoIdMoviChiuAsBuffer(buffer, position);
        position += DcoIdMoviChiu.Len.DCO_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dcoDtIniEff, Len.Int.DCO_DT_INI_EFF, 0);
        position += Len.DCO_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dcoDtEndEff, Len.Int.DCO_DT_END_EFF, 0);
        position += Len.DCO_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dcoCodCompAnia, Len.Int.DCO_COD_COMP_ANIA, 0);
        position += Len.DCO_COD_COMP_ANIA;
        dcoImpArrotPre.getDcoImpArrotPreAsBuffer(buffer, position);
        position += DcoImpArrotPre.Len.DCO_IMP_ARROT_PRE;
        dcoPcScon.getDcoPcSconAsBuffer(buffer, position);
        position += DcoPcScon.Len.DCO_PC_SCON;
        MarshalByte.writeChar(buffer, position, dcoFlAdesSing);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dcoTpImp, Len.DCO_TP_IMP);
        position += Len.DCO_TP_IMP;
        MarshalByte.writeChar(buffer, position, dcoFlRiclPreDaCpt);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dcoTpAdes, Len.DCO_TP_ADES);
        position += Len.DCO_TP_ADES;
        dcoDtUltRinnTac.getDcoDtUltRinnTacAsBuffer(buffer, position);
        position += DcoDtUltRinnTac.Len.DCO_DT_ULT_RINN_TAC;
        dcoImpScon.getDcoImpSconAsBuffer(buffer, position);
        position += DcoImpScon.Len.DCO_IMP_SCON;
        dcoFrazDflt.getDcoFrazDfltAsBuffer(buffer, position);
        position += DcoFrazDflt.Len.DCO_FRAZ_DFLT;
        dcoEtaScadMascDflt.getDcoEtaScadMascDfltAsBuffer(buffer, position);
        position += DcoEtaScadMascDflt.Len.DCO_ETA_SCAD_MASC_DFLT;
        dcoEtaScadFemmDflt.getDcoEtaScadFemmDfltAsBuffer(buffer, position);
        position += DcoEtaScadFemmDflt.Len.DCO_ETA_SCAD_FEMM_DFLT;
        MarshalByte.writeString(buffer, position, dcoTpDfltDur, Len.DCO_TP_DFLT_DUR);
        position += Len.DCO_TP_DFLT_DUR;
        dcoDurAaAdesDflt.getDcoDurAaAdesDfltAsBuffer(buffer, position);
        position += DcoDurAaAdesDflt.Len.DCO_DUR_AA_ADES_DFLT;
        dcoDurMmAdesDflt.getDcoDurMmAdesDfltAsBuffer(buffer, position);
        position += DcoDurMmAdesDflt.Len.DCO_DUR_MM_ADES_DFLT;
        dcoDurGgAdesDflt.getDcoDurGgAdesDfltAsBuffer(buffer, position);
        position += DcoDurGgAdesDflt.Len.DCO_DUR_GG_ADES_DFLT;
        dcoDtScadAdesDflt.getDcoDtScadAdesDfltAsBuffer(buffer, position);
        position += DcoDtScadAdesDflt.Len.DCO_DT_SCAD_ADES_DFLT;
        MarshalByte.writeString(buffer, position, dcoCodFndDflt, Len.DCO_COD_FND_DFLT);
        position += Len.DCO_COD_FND_DFLT;
        MarshalByte.writeString(buffer, position, dcoTpDur, Len.DCO_TP_DUR);
        position += Len.DCO_TP_DUR;
        MarshalByte.writeString(buffer, position, dcoTpCalcDur, Len.DCO_TP_CALC_DUR);
        position += Len.DCO_TP_CALC_DUR;
        MarshalByte.writeChar(buffer, position, dcoFlNoAderenti);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, dcoFlDistintaCnbtva);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, dcoFlCnbtAutes);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, dcoFlQtzPostEmis);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, dcoFlComnzFndIs);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, dcoDsRiga, Len.Int.DCO_DS_RIGA, 0);
        position += Len.DCO_DS_RIGA;
        MarshalByte.writeChar(buffer, position, dcoDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dcoDsVer, Len.Int.DCO_DS_VER, 0);
        position += Len.DCO_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dcoDsTsIniCptz, Len.Int.DCO_DS_TS_INI_CPTZ, 0);
        position += Len.DCO_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dcoDsTsEndCptz, Len.Int.DCO_DS_TS_END_CPTZ, 0);
        position += Len.DCO_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dcoDsUtente, Len.DCO_DS_UTENTE);
        position += Len.DCO_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dcoDsStatoElab);
        return buffer;
    }

    public void setDcoIdDColl(int dcoIdDColl) {
        this.dcoIdDColl = dcoIdDColl;
    }

    public int getDcoIdDColl() {
        return this.dcoIdDColl;
    }

    public void setDcoIdPoli(int dcoIdPoli) {
        this.dcoIdPoli = dcoIdPoli;
    }

    public int getDcoIdPoli() {
        return this.dcoIdPoli;
    }

    public void setDcoIdMoviCrz(int dcoIdMoviCrz) {
        this.dcoIdMoviCrz = dcoIdMoviCrz;
    }

    public int getDcoIdMoviCrz() {
        return this.dcoIdMoviCrz;
    }

    public void setDcoDtIniEff(int dcoDtIniEff) {
        this.dcoDtIniEff = dcoDtIniEff;
    }

    public int getDcoDtIniEff() {
        return this.dcoDtIniEff;
    }

    public void setDcoDtEndEff(int dcoDtEndEff) {
        this.dcoDtEndEff = dcoDtEndEff;
    }

    public int getDcoDtEndEff() {
        return this.dcoDtEndEff;
    }

    public void setDcoCodCompAnia(int dcoCodCompAnia) {
        this.dcoCodCompAnia = dcoCodCompAnia;
    }

    public int getDcoCodCompAnia() {
        return this.dcoCodCompAnia;
    }

    public void setDcoFlAdesSing(char dcoFlAdesSing) {
        this.dcoFlAdesSing = dcoFlAdesSing;
    }

    public char getDcoFlAdesSing() {
        return this.dcoFlAdesSing;
    }

    public void setDcoTpImp(String dcoTpImp) {
        this.dcoTpImp = Functions.subString(dcoTpImp, Len.DCO_TP_IMP);
    }

    public String getDcoTpImp() {
        return this.dcoTpImp;
    }

    public String getDcoTpImpFormatted() {
        return Functions.padBlanks(getDcoTpImp(), Len.DCO_TP_IMP);
    }

    public void setDcoFlRiclPreDaCpt(char dcoFlRiclPreDaCpt) {
        this.dcoFlRiclPreDaCpt = dcoFlRiclPreDaCpt;
    }

    public char getDcoFlRiclPreDaCpt() {
        return this.dcoFlRiclPreDaCpt;
    }

    public void setDcoTpAdes(String dcoTpAdes) {
        this.dcoTpAdes = Functions.subString(dcoTpAdes, Len.DCO_TP_ADES);
    }

    public String getDcoTpAdes() {
        return this.dcoTpAdes;
    }

    public String getDcoTpAdesFormatted() {
        return Functions.padBlanks(getDcoTpAdes(), Len.DCO_TP_ADES);
    }

    public void setDcoTpDfltDur(String dcoTpDfltDur) {
        this.dcoTpDfltDur = Functions.subString(dcoTpDfltDur, Len.DCO_TP_DFLT_DUR);
    }

    public String getDcoTpDfltDur() {
        return this.dcoTpDfltDur;
    }

    public String getDcoTpDfltDurFormatted() {
        return Functions.padBlanks(getDcoTpDfltDur(), Len.DCO_TP_DFLT_DUR);
    }

    public void setDcoCodFndDflt(String dcoCodFndDflt) {
        this.dcoCodFndDflt = Functions.subString(dcoCodFndDflt, Len.DCO_COD_FND_DFLT);
    }

    public String getDcoCodFndDflt() {
        return this.dcoCodFndDflt;
    }

    public String getDcoCodFndDfltFormatted() {
        return Functions.padBlanks(getDcoCodFndDflt(), Len.DCO_COD_FND_DFLT);
    }

    public void setDcoTpDur(String dcoTpDur) {
        this.dcoTpDur = Functions.subString(dcoTpDur, Len.DCO_TP_DUR);
    }

    public String getDcoTpDur() {
        return this.dcoTpDur;
    }

    public String getDcoTpDurFormatted() {
        return Functions.padBlanks(getDcoTpDur(), Len.DCO_TP_DUR);
    }

    public void setDcoTpCalcDur(String dcoTpCalcDur) {
        this.dcoTpCalcDur = Functions.subString(dcoTpCalcDur, Len.DCO_TP_CALC_DUR);
    }

    public String getDcoTpCalcDur() {
        return this.dcoTpCalcDur;
    }

    public String getDcoTpCalcDurFormatted() {
        return Functions.padBlanks(getDcoTpCalcDur(), Len.DCO_TP_CALC_DUR);
    }

    public void setDcoFlNoAderenti(char dcoFlNoAderenti) {
        this.dcoFlNoAderenti = dcoFlNoAderenti;
    }

    public char getDcoFlNoAderenti() {
        return this.dcoFlNoAderenti;
    }

    public void setDcoFlDistintaCnbtva(char dcoFlDistintaCnbtva) {
        this.dcoFlDistintaCnbtva = dcoFlDistintaCnbtva;
    }

    public char getDcoFlDistintaCnbtva() {
        return this.dcoFlDistintaCnbtva;
    }

    public void setDcoFlCnbtAutes(char dcoFlCnbtAutes) {
        this.dcoFlCnbtAutes = dcoFlCnbtAutes;
    }

    public char getDcoFlCnbtAutes() {
        return this.dcoFlCnbtAutes;
    }

    public void setDcoFlQtzPostEmis(char dcoFlQtzPostEmis) {
        this.dcoFlQtzPostEmis = dcoFlQtzPostEmis;
    }

    public char getDcoFlQtzPostEmis() {
        return this.dcoFlQtzPostEmis;
    }

    public void setDcoFlComnzFndIs(char dcoFlComnzFndIs) {
        this.dcoFlComnzFndIs = dcoFlComnzFndIs;
    }

    public char getDcoFlComnzFndIs() {
        return this.dcoFlComnzFndIs;
    }

    public void setDcoDsRiga(long dcoDsRiga) {
        this.dcoDsRiga = dcoDsRiga;
    }

    public long getDcoDsRiga() {
        return this.dcoDsRiga;
    }

    public void setDcoDsOperSql(char dcoDsOperSql) {
        this.dcoDsOperSql = dcoDsOperSql;
    }

    public void setDcoDsOperSqlFormatted(String dcoDsOperSql) {
        setDcoDsOperSql(Functions.charAt(dcoDsOperSql, Types.CHAR_SIZE));
    }

    public char getDcoDsOperSql() {
        return this.dcoDsOperSql;
    }

    public void setDcoDsVer(int dcoDsVer) {
        this.dcoDsVer = dcoDsVer;
    }

    public int getDcoDsVer() {
        return this.dcoDsVer;
    }

    public void setDcoDsTsIniCptz(long dcoDsTsIniCptz) {
        this.dcoDsTsIniCptz = dcoDsTsIniCptz;
    }

    public long getDcoDsTsIniCptz() {
        return this.dcoDsTsIniCptz;
    }

    public void setDcoDsTsEndCptz(long dcoDsTsEndCptz) {
        this.dcoDsTsEndCptz = dcoDsTsEndCptz;
    }

    public long getDcoDsTsEndCptz() {
        return this.dcoDsTsEndCptz;
    }

    public void setDcoDsUtente(String dcoDsUtente) {
        this.dcoDsUtente = Functions.subString(dcoDsUtente, Len.DCO_DS_UTENTE);
    }

    public String getDcoDsUtente() {
        return this.dcoDsUtente;
    }

    public void setDcoDsStatoElab(char dcoDsStatoElab) {
        this.dcoDsStatoElab = dcoDsStatoElab;
    }

    public void setDcoDsStatoElabFormatted(String dcoDsStatoElab) {
        setDcoDsStatoElab(Functions.charAt(dcoDsStatoElab, Types.CHAR_SIZE));
    }

    public char getDcoDsStatoElab() {
        return this.dcoDsStatoElab;
    }

    public DcoDtScadAdesDflt getDcoDtScadAdesDflt() {
        return dcoDtScadAdesDflt;
    }

    public DcoDtUltRinnTac getDcoDtUltRinnTac() {
        return dcoDtUltRinnTac;
    }

    public DcoDurAaAdesDflt getDcoDurAaAdesDflt() {
        return dcoDurAaAdesDflt;
    }

    public DcoDurGgAdesDflt getDcoDurGgAdesDflt() {
        return dcoDurGgAdesDflt;
    }

    public DcoDurMmAdesDflt getDcoDurMmAdesDflt() {
        return dcoDurMmAdesDflt;
    }

    public DcoEtaScadFemmDflt getDcoEtaScadFemmDflt() {
        return dcoEtaScadFemmDflt;
    }

    public DcoEtaScadMascDflt getDcoEtaScadMascDflt() {
        return dcoEtaScadMascDflt;
    }

    public DcoFrazDflt getDcoFrazDflt() {
        return dcoFrazDflt;
    }

    public DcoIdMoviChiu getDcoIdMoviChiu() {
        return dcoIdMoviChiu;
    }

    public DcoImpArrotPre getDcoImpArrotPre() {
        return dcoImpArrotPre;
    }

    public DcoImpScon getDcoImpScon() {
        return dcoImpScon;
    }

    public DcoPcScon getDcoPcScon() {
        return dcoPcScon;
    }

    @Override
    public byte[] serialize() {
        return getdCollBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_ID_D_COLL = 5;
        public static final int DCO_ID_POLI = 5;
        public static final int DCO_ID_MOVI_CRZ = 5;
        public static final int DCO_DT_INI_EFF = 5;
        public static final int DCO_DT_END_EFF = 5;
        public static final int DCO_COD_COMP_ANIA = 3;
        public static final int DCO_FL_ADES_SING = 1;
        public static final int DCO_TP_IMP = 2;
        public static final int DCO_FL_RICL_PRE_DA_CPT = 1;
        public static final int DCO_TP_ADES = 2;
        public static final int DCO_TP_DFLT_DUR = 2;
        public static final int DCO_COD_FND_DFLT = 12;
        public static final int DCO_TP_DUR = 2;
        public static final int DCO_TP_CALC_DUR = 12;
        public static final int DCO_FL_NO_ADERENTI = 1;
        public static final int DCO_FL_DISTINTA_CNBTVA = 1;
        public static final int DCO_FL_CNBT_AUTES = 1;
        public static final int DCO_FL_QTZ_POST_EMIS = 1;
        public static final int DCO_FL_COMNZ_FND_IS = 1;
        public static final int DCO_DS_RIGA = 6;
        public static final int DCO_DS_OPER_SQL = 1;
        public static final int DCO_DS_VER = 5;
        public static final int DCO_DS_TS_INI_CPTZ = 10;
        public static final int DCO_DS_TS_END_CPTZ = 10;
        public static final int DCO_DS_UTENTE = 20;
        public static final int DCO_DS_STATO_ELAB = 1;
        public static final int D_COLL = DCO_ID_D_COLL + DCO_ID_POLI + DCO_ID_MOVI_CRZ + DcoIdMoviChiu.Len.DCO_ID_MOVI_CHIU + DCO_DT_INI_EFF + DCO_DT_END_EFF + DCO_COD_COMP_ANIA + DcoImpArrotPre.Len.DCO_IMP_ARROT_PRE + DcoPcScon.Len.DCO_PC_SCON + DCO_FL_ADES_SING + DCO_TP_IMP + DCO_FL_RICL_PRE_DA_CPT + DCO_TP_ADES + DcoDtUltRinnTac.Len.DCO_DT_ULT_RINN_TAC + DcoImpScon.Len.DCO_IMP_SCON + DcoFrazDflt.Len.DCO_FRAZ_DFLT + DcoEtaScadMascDflt.Len.DCO_ETA_SCAD_MASC_DFLT + DcoEtaScadFemmDflt.Len.DCO_ETA_SCAD_FEMM_DFLT + DCO_TP_DFLT_DUR + DcoDurAaAdesDflt.Len.DCO_DUR_AA_ADES_DFLT + DcoDurMmAdesDflt.Len.DCO_DUR_MM_ADES_DFLT + DcoDurGgAdesDflt.Len.DCO_DUR_GG_ADES_DFLT + DcoDtScadAdesDflt.Len.DCO_DT_SCAD_ADES_DFLT + DCO_COD_FND_DFLT + DCO_TP_DUR + DCO_TP_CALC_DUR + DCO_FL_NO_ADERENTI + DCO_FL_DISTINTA_CNBTVA + DCO_FL_CNBT_AUTES + DCO_FL_QTZ_POST_EMIS + DCO_FL_COMNZ_FND_IS + DCO_DS_RIGA + DCO_DS_OPER_SQL + DCO_DS_VER + DCO_DS_TS_INI_CPTZ + DCO_DS_TS_END_CPTZ + DCO_DS_UTENTE + DCO_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_ID_D_COLL = 9;
            public static final int DCO_ID_POLI = 9;
            public static final int DCO_ID_MOVI_CRZ = 9;
            public static final int DCO_DT_INI_EFF = 8;
            public static final int DCO_DT_END_EFF = 8;
            public static final int DCO_COD_COMP_ANIA = 5;
            public static final int DCO_DS_RIGA = 10;
            public static final int DCO_DS_VER = 9;
            public static final int DCO_DS_TS_INI_CPTZ = 18;
            public static final int DCO_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
