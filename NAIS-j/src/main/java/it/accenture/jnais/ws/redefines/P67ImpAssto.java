package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-IMP-ASSTO<br>
 * Variable: P67-IMP-ASSTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67ImpAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67ImpAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_IMP_ASSTO;
    }

    public void setP67ImpAssto(AfDecimal p67ImpAssto) {
        writeDecimalAsPacked(Pos.P67_IMP_ASSTO, p67ImpAssto.copy());
    }

    public void setP67ImpAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_IMP_ASSTO, Pos.P67_IMP_ASSTO);
    }

    /**Original name: P67-IMP-ASSTO<br>*/
    public AfDecimal getP67ImpAssto() {
        return readPackedAsDecimal(Pos.P67_IMP_ASSTO, Len.Int.P67_IMP_ASSTO, Len.Fract.P67_IMP_ASSTO);
    }

    public byte[] getP67ImpAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_IMP_ASSTO, Pos.P67_IMP_ASSTO);
        return buffer;
    }

    public void setP67ImpAsstoNull(String p67ImpAsstoNull) {
        writeString(Pos.P67_IMP_ASSTO_NULL, p67ImpAsstoNull, Len.P67_IMP_ASSTO_NULL);
    }

    /**Original name: P67-IMP-ASSTO-NULL<br>*/
    public String getP67ImpAsstoNull() {
        return readString(Pos.P67_IMP_ASSTO_NULL, Len.P67_IMP_ASSTO_NULL);
    }

    public String getP67ImpAsstoNullFormatted() {
        return Functions.padBlanks(getP67ImpAsstoNull(), Len.P67_IMP_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_IMP_ASSTO = 1;
        public static final int P67_IMP_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_IMP_ASSTO = 8;
        public static final int P67_IMP_ASSTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_IMP_ASSTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_IMP_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
