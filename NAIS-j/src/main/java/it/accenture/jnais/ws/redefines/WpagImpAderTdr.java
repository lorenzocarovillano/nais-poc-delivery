package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-ADER-TDR<br>
 * Variable: WPAG-IMP-ADER-TDR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpAderTdr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpAderTdr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_ADER_TDR;
    }

    public void setWpagImpAderTdr(AfDecimal wpagImpAderTdr) {
        writeDecimalAsPacked(Pos.WPAG_IMP_ADER_TDR, wpagImpAderTdr.copy());
    }

    public void setWpagImpAderTdrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_ADER_TDR, Pos.WPAG_IMP_ADER_TDR);
    }

    /**Original name: WPAG-IMP-ADER-TDR<br>*/
    public AfDecimal getWpagImpAderTdr() {
        return readPackedAsDecimal(Pos.WPAG_IMP_ADER_TDR, Len.Int.WPAG_IMP_ADER_TDR, Len.Fract.WPAG_IMP_ADER_TDR);
    }

    public byte[] getWpagImpAderTdrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_ADER_TDR, Pos.WPAG_IMP_ADER_TDR);
        return buffer;
    }

    public void initWpagImpAderTdrSpaces() {
        fill(Pos.WPAG_IMP_ADER_TDR, Len.WPAG_IMP_ADER_TDR, Types.SPACE_CHAR);
    }

    public void setWpagImpAderTdrNull(String wpagImpAderTdrNull) {
        writeString(Pos.WPAG_IMP_ADER_TDR_NULL, wpagImpAderTdrNull, Len.WPAG_IMP_ADER_TDR_NULL);
    }

    /**Original name: WPAG-IMP-ADER-TDR-NULL<br>*/
    public String getWpagImpAderTdrNull() {
        return readString(Pos.WPAG_IMP_ADER_TDR_NULL, Len.WPAG_IMP_ADER_TDR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ADER_TDR = 1;
        public static final int WPAG_IMP_ADER_TDR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ADER_TDR = 8;
        public static final int WPAG_IMP_ADER_TDR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ADER_TDR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ADER_TDR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
