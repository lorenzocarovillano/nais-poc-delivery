package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDCO-IMP-SCON<br>
 * Variable: WDCO-IMP-SCON from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdcoImpScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdcoImpScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_IMP_SCON;
    }

    public void setWdcoImpScon(AfDecimal wdcoImpScon) {
        writeDecimalAsPacked(Pos.WDCO_IMP_SCON, wdcoImpScon.copy());
    }

    public void setWdcoImpSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDCO_IMP_SCON, Pos.WDCO_IMP_SCON);
    }

    /**Original name: WDCO-IMP-SCON<br>*/
    public AfDecimal getWdcoImpScon() {
        return readPackedAsDecimal(Pos.WDCO_IMP_SCON, Len.Int.WDCO_IMP_SCON, Len.Fract.WDCO_IMP_SCON);
    }

    public byte[] getWdcoImpSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDCO_IMP_SCON, Pos.WDCO_IMP_SCON);
        return buffer;
    }

    public void setWdcoImpSconNull(String wdcoImpSconNull) {
        writeString(Pos.WDCO_IMP_SCON_NULL, wdcoImpSconNull, Len.WDCO_IMP_SCON_NULL);
    }

    /**Original name: WDCO-IMP-SCON-NULL<br>*/
    public String getWdcoImpSconNull() {
        return readString(Pos.WDCO_IMP_SCON_NULL, Len.WDCO_IMP_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDCO_IMP_SCON = 1;
        public static final int WDCO_IMP_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_IMP_SCON = 8;
        public static final int WDCO_IMP_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDCO_IMP_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_IMP_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
