package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-PRVR-DFZ<br>
 * Variable: WDFL-IMPST-PRVR-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstPrvrDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstPrvrDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_PRVR_DFZ;
    }

    public void setWdflImpstPrvrDfz(AfDecimal wdflImpstPrvrDfz) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_PRVR_DFZ, wdflImpstPrvrDfz.copy());
    }

    public void setWdflImpstPrvrDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_PRVR_DFZ, Pos.WDFL_IMPST_PRVR_DFZ);
    }

    /**Original name: WDFL-IMPST-PRVR-DFZ<br>*/
    public AfDecimal getWdflImpstPrvrDfz() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_PRVR_DFZ, Len.Int.WDFL_IMPST_PRVR_DFZ, Len.Fract.WDFL_IMPST_PRVR_DFZ);
    }

    public byte[] getWdflImpstPrvrDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_PRVR_DFZ, Pos.WDFL_IMPST_PRVR_DFZ);
        return buffer;
    }

    public void setWdflImpstPrvrDfzNull(String wdflImpstPrvrDfzNull) {
        writeString(Pos.WDFL_IMPST_PRVR_DFZ_NULL, wdflImpstPrvrDfzNull, Len.WDFL_IMPST_PRVR_DFZ_NULL);
    }

    /**Original name: WDFL-IMPST-PRVR-DFZ-NULL<br>*/
    public String getWdflImpstPrvrDfzNull() {
        return readString(Pos.WDFL_IMPST_PRVR_DFZ_NULL, Len.WDFL_IMPST_PRVR_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_PRVR_DFZ = 1;
        public static final int WDFL_IMPST_PRVR_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_PRVR_DFZ = 8;
        public static final int WDFL_IMPST_PRVR_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_PRVR_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_PRVR_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
