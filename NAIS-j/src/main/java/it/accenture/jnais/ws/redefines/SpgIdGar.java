package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: SPG-ID-GAR<br>
 * Variable: SPG-ID-GAR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SpgIdGar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public SpgIdGar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SPG_ID_GAR;
    }

    public void setSpgIdGar(int spgIdGar) {
        writeIntAsPacked(Pos.SPG_ID_GAR, spgIdGar, Len.Int.SPG_ID_GAR);
    }

    public void setSpgIdGarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SPG_ID_GAR, Pos.SPG_ID_GAR);
    }

    /**Original name: SPG-ID-GAR<br>*/
    public int getSpgIdGar() {
        return readPackedAsInt(Pos.SPG_ID_GAR, Len.Int.SPG_ID_GAR);
    }

    public byte[] getSpgIdGarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.SPG_ID_GAR, Pos.SPG_ID_GAR);
        return buffer;
    }

    public void setSpgIdGarNull(String spgIdGarNull) {
        writeString(Pos.SPG_ID_GAR_NULL, spgIdGarNull, Len.SPG_ID_GAR_NULL);
    }

    /**Original name: SPG-ID-GAR-NULL<br>*/
    public String getSpgIdGarNull() {
        return readString(Pos.SPG_ID_GAR_NULL, Len.SPG_ID_GAR_NULL);
    }

    public String getSpgIdGarNullFormatted() {
        return Functions.padBlanks(getSpgIdGarNull(), Len.SPG_ID_GAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SPG_ID_GAR = 1;
        public static final int SPG_ID_GAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SPG_ID_GAR = 5;
        public static final int SPG_ID_GAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SPG_ID_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
