package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-TP-INVST<br>
 * Variable: L3401-TP-INVST from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401TpInvst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401TpInvst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_TP_INVST;
    }

    public void setL3401TpInvst(short l3401TpInvst) {
        writeShortAsPacked(Pos.L3401_TP_INVST, l3401TpInvst, Len.Int.L3401_TP_INVST);
    }

    public void setL3401TpInvstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_TP_INVST, Pos.L3401_TP_INVST);
    }

    /**Original name: L3401-TP-INVST<br>*/
    public short getL3401TpInvst() {
        return readPackedAsShort(Pos.L3401_TP_INVST, Len.Int.L3401_TP_INVST);
    }

    public byte[] getL3401TpInvstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_TP_INVST, Pos.L3401_TP_INVST);
        return buffer;
    }

    /**Original name: L3401-TP-INVST-NULL<br>*/
    public String getL3401TpInvstNull() {
        return readString(Pos.L3401_TP_INVST_NULL, Len.L3401_TP_INVST_NULL);
    }

    public String getL3401TpInvstNullFormatted() {
        return Functions.padBlanks(getL3401TpInvstNull(), Len.L3401_TP_INVST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_TP_INVST = 1;
        public static final int L3401_TP_INVST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_TP_INVST = 2;
        public static final int L3401_TP_INVST_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_TP_INVST = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
