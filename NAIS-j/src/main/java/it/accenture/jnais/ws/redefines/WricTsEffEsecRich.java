package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WRIC-TS-EFF-ESEC-RICH<br>
 * Variable: WRIC-TS-EFF-ESEC-RICH from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WricTsEffEsecRich extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WricTsEffEsecRich() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIC_TS_EFF_ESEC_RICH;
    }

    public void setWricTsEffEsecRich(long wricTsEffEsecRich) {
        writeLongAsPacked(Pos.WRIC_TS_EFF_ESEC_RICH, wricTsEffEsecRich, Len.Int.WRIC_TS_EFF_ESEC_RICH);
    }

    public void setWricTsEffEsecRichFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIC_TS_EFF_ESEC_RICH, Pos.WRIC_TS_EFF_ESEC_RICH);
    }

    /**Original name: WRIC-TS-EFF-ESEC-RICH<br>*/
    public long getWricTsEffEsecRich() {
        return readPackedAsLong(Pos.WRIC_TS_EFF_ESEC_RICH, Len.Int.WRIC_TS_EFF_ESEC_RICH);
    }

    public byte[] getWricTsEffEsecRichAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIC_TS_EFF_ESEC_RICH, Pos.WRIC_TS_EFF_ESEC_RICH);
        return buffer;
    }

    public void setWricTsEffEsecRichNull(String wricTsEffEsecRichNull) {
        writeString(Pos.WRIC_TS_EFF_ESEC_RICH_NULL, wricTsEffEsecRichNull, Len.WRIC_TS_EFF_ESEC_RICH_NULL);
    }

    /**Original name: WRIC-TS-EFF-ESEC-RICH-NULL<br>*/
    public String getWricTsEffEsecRichNull() {
        return readString(Pos.WRIC_TS_EFF_ESEC_RICH_NULL, Len.WRIC_TS_EFF_ESEC_RICH_NULL);
    }

    public String getWricTsEffEsecRichNullFormatted() {
        return Functions.padBlanks(getWricTsEffEsecRichNull(), Len.WRIC_TS_EFF_ESEC_RICH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIC_TS_EFF_ESEC_RICH = 1;
        public static final int WRIC_TS_EFF_ESEC_RICH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIC_TS_EFF_ESEC_RICH = 10;
        public static final int WRIC_TS_EFF_ESEC_RICH_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIC_TS_EFF_ESEC_RICH = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
