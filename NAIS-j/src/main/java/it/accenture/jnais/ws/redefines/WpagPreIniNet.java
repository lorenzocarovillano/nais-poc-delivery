package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRE-INI-NET<br>
 * Variable: WPAG-PRE-INI-NET from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPreIniNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPreIniNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRE_INI_NET;
    }

    public void setWpagPreIniNet(AfDecimal wpagPreIniNet) {
        writeDecimalAsPacked(Pos.WPAG_PRE_INI_NET, wpagPreIniNet.copy());
    }

    public void setWpagPreIniNetFormatted(String wpagPreIniNet) {
        setWpagPreIniNet(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRE_INI_NET + Len.Fract.WPAG_PRE_INI_NET, Len.Fract.WPAG_PRE_INI_NET, wpagPreIniNet));
    }

    public void setWpagPreIniNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRE_INI_NET, Pos.WPAG_PRE_INI_NET);
    }

    /**Original name: WPAG-PRE-INI-NET<br>*/
    public AfDecimal getWpagPreIniNet() {
        return readPackedAsDecimal(Pos.WPAG_PRE_INI_NET, Len.Int.WPAG_PRE_INI_NET, Len.Fract.WPAG_PRE_INI_NET);
    }

    public byte[] getWpagPreIniNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRE_INI_NET, Pos.WPAG_PRE_INI_NET);
        return buffer;
    }

    public void initWpagPreIniNetSpaces() {
        fill(Pos.WPAG_PRE_INI_NET, Len.WPAG_PRE_INI_NET, Types.SPACE_CHAR);
    }

    public void setWpagPreIniNetNull(String wpagPreIniNetNull) {
        writeString(Pos.WPAG_PRE_INI_NET_NULL, wpagPreIniNetNull, Len.WPAG_PRE_INI_NET_NULL);
    }

    /**Original name: WPAG-PRE-INI-NET-NULL<br>*/
    public String getWpagPreIniNetNull() {
        return readString(Pos.WPAG_PRE_INI_NET_NULL, Len.WPAG_PRE_INI_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_INI_NET = 1;
        public static final int WPAG_PRE_INI_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_INI_NET = 8;
        public static final int WPAG_PRE_INI_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_INI_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_INI_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
