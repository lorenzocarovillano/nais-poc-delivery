package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-LIMITE-ARRAY-BLOB<br>
 * Variable: WK-LIMITE-ARRAY-BLOB from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkLimiteArrayBlob {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_LIMITE_ARRAY_BLOB);
    public static final String BLOB1K = "650";
    public static final String BLOB2K = "500";
    public static final String BLOB3K = "350";
    public static final String BLOB10K = "100";

    //==== METHODS ====
    public void setWkLimiteArrayBlob(short wkLimiteArrayBlob) {
        this.value = NumericDisplay.asString(wkLimiteArrayBlob, Len.WK_LIMITE_ARRAY_BLOB);
    }

    public void setWkLimiteArrayBlobFormatted(String wkLimiteArrayBlob) {
        this.value = Trunc.toUnsignedNumeric(wkLimiteArrayBlob, Len.WK_LIMITE_ARRAY_BLOB);
    }

    public short getWkLimiteArrayBlob() {
        return NumericDisplay.asShort(this.value);
    }

    public void setBlob1k() {
        setWkLimiteArrayBlobFormatted(BLOB1K);
    }

    public void setBlob2k() {
        setWkLimiteArrayBlobFormatted(BLOB2K);
    }

    public void setBlob3k() {
        setWkLimiteArrayBlobFormatted(BLOB3K);
    }

    public void setBlob10k() {
        setWkLimiteArrayBlobFormatted(BLOB10K);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_LIMITE_ARRAY_BLOB = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
