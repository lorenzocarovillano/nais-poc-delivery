package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RIT-ACC-EFFLQ<br>
 * Variable: WDFL-RIT-ACC-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflRitAccEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflRitAccEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RIT_ACC_EFFLQ;
    }

    public void setWdflRitAccEfflq(AfDecimal wdflRitAccEfflq) {
        writeDecimalAsPacked(Pos.WDFL_RIT_ACC_EFFLQ, wdflRitAccEfflq.copy());
    }

    public void setWdflRitAccEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RIT_ACC_EFFLQ, Pos.WDFL_RIT_ACC_EFFLQ);
    }

    /**Original name: WDFL-RIT-ACC-EFFLQ<br>*/
    public AfDecimal getWdflRitAccEfflq() {
        return readPackedAsDecimal(Pos.WDFL_RIT_ACC_EFFLQ, Len.Int.WDFL_RIT_ACC_EFFLQ, Len.Fract.WDFL_RIT_ACC_EFFLQ);
    }

    public byte[] getWdflRitAccEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RIT_ACC_EFFLQ, Pos.WDFL_RIT_ACC_EFFLQ);
        return buffer;
    }

    public void setWdflRitAccEfflqNull(String wdflRitAccEfflqNull) {
        writeString(Pos.WDFL_RIT_ACC_EFFLQ_NULL, wdflRitAccEfflqNull, Len.WDFL_RIT_ACC_EFFLQ_NULL);
    }

    /**Original name: WDFL-RIT-ACC-EFFLQ-NULL<br>*/
    public String getWdflRitAccEfflqNull() {
        return readString(Pos.WDFL_RIT_ACC_EFFLQ_NULL, Len.WDFL_RIT_ACC_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_ACC_EFFLQ = 1;
        public static final int WDFL_RIT_ACC_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_ACC_EFFLQ = 8;
        public static final int WDFL_RIT_ACC_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_ACC_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_ACC_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
