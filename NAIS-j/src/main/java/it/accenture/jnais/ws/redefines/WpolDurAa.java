package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPOL-DUR-AA<br>
 * Variable: WPOL-DUR-AA from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DUR_AA;
    }

    public void setWpolDurAa(int wpolDurAa) {
        writeIntAsPacked(Pos.WPOL_DUR_AA, wpolDurAa, Len.Int.WPOL_DUR_AA);
    }

    public void setWpolDurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DUR_AA, Pos.WPOL_DUR_AA);
    }

    /**Original name: WPOL-DUR-AA<br>*/
    public int getWpolDurAa() {
        return readPackedAsInt(Pos.WPOL_DUR_AA, Len.Int.WPOL_DUR_AA);
    }

    public byte[] getWpolDurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DUR_AA, Pos.WPOL_DUR_AA);
        return buffer;
    }

    public void setWpolDurAaNull(String wpolDurAaNull) {
        writeString(Pos.WPOL_DUR_AA_NULL, wpolDurAaNull, Len.WPOL_DUR_AA_NULL);
    }

    /**Original name: WPOL-DUR-AA-NULL<br>*/
    public String getWpolDurAaNull() {
        return readString(Pos.WPOL_DUR_AA_NULL, Len.WPOL_DUR_AA_NULL);
    }

    public String getWpolDurAaNullFormatted() {
        return Functions.padBlanks(getWpolDurAaNull(), Len.WPOL_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DUR_AA = 1;
        public static final int WPOL_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DUR_AA = 3;
        public static final int WPOL_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
