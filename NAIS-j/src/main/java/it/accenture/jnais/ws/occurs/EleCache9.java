package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ELE-CACHE-9<br>
 * Variables: ELE-CACHE-9 from program LRGS0660<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class EleCache9 {

    //==== PROPERTIES ====
    //Original name: REC9-IMP-LRD-RISC-PARZ
    private AfDecimal impLrdRiscParz = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC9-RP-TP-MOVI
    private String rpTpMovi = DefaultValues.stringVal(Len.RP_TP_MOVI);
    //Original name: REC9-DT-EFFETTO-LIQ
    private String dtEffettoLiq = DefaultValues.stringVal(Len.DT_EFFETTO_LIQ);

    //==== METHODS ====
    public void setImpLrdRiscParz(AfDecimal impLrdRiscParz) {
        this.impLrdRiscParz.assign(impLrdRiscParz);
    }

    public AfDecimal getImpLrdRiscParz() {
        return this.impLrdRiscParz.copy();
    }

    public void setRpTpMoviFormatted(String rpTpMovi) {
        this.rpTpMovi = Trunc.toUnsignedNumeric(rpTpMovi, Len.RP_TP_MOVI);
    }

    public int getRpTpMovi() {
        return NumericDisplay.asInt(this.rpTpMovi);
    }

    public void setDtEffettoLiq(String dtEffettoLiq) {
        this.dtEffettoLiq = Functions.subString(dtEffettoLiq, Len.DT_EFFETTO_LIQ);
    }

    public String getDtEffettoLiq() {
        return this.dtEffettoLiq;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RP_TP_MOVI = 5;
        public static final int DT_EFFETTO_LIQ = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
