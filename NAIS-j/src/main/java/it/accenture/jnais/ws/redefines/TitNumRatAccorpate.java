package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-NUM-RAT-ACCORPATE<br>
 * Variable: TIT-NUM-RAT-ACCORPATE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitNumRatAccorpate extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitNumRatAccorpate() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_NUM_RAT_ACCORPATE;
    }

    public void setTitNumRatAccorpate(int titNumRatAccorpate) {
        writeIntAsPacked(Pos.TIT_NUM_RAT_ACCORPATE, titNumRatAccorpate, Len.Int.TIT_NUM_RAT_ACCORPATE);
    }

    public void setTitNumRatAccorpateFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_NUM_RAT_ACCORPATE, Pos.TIT_NUM_RAT_ACCORPATE);
    }

    /**Original name: TIT-NUM-RAT-ACCORPATE<br>*/
    public int getTitNumRatAccorpate() {
        return readPackedAsInt(Pos.TIT_NUM_RAT_ACCORPATE, Len.Int.TIT_NUM_RAT_ACCORPATE);
    }

    public byte[] getTitNumRatAccorpateAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_NUM_RAT_ACCORPATE, Pos.TIT_NUM_RAT_ACCORPATE);
        return buffer;
    }

    public void setTitNumRatAccorpateNull(String titNumRatAccorpateNull) {
        writeString(Pos.TIT_NUM_RAT_ACCORPATE_NULL, titNumRatAccorpateNull, Len.TIT_NUM_RAT_ACCORPATE_NULL);
    }

    /**Original name: TIT-NUM-RAT-ACCORPATE-NULL<br>*/
    public String getTitNumRatAccorpateNull() {
        return readString(Pos.TIT_NUM_RAT_ACCORPATE_NULL, Len.TIT_NUM_RAT_ACCORPATE_NULL);
    }

    public String getTitNumRatAccorpateNullFormatted() {
        return Functions.padBlanks(getTitNumRatAccorpateNull(), Len.TIT_NUM_RAT_ACCORPATE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_NUM_RAT_ACCORPATE = 1;
        public static final int TIT_NUM_RAT_ACCORPATE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_NUM_RAT_ACCORPATE = 3;
        public static final int TIT_NUM_RAT_ACCORPATE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_NUM_RAT_ACCORPATE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
