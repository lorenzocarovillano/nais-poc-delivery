package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMP-RIT-VIS<br>
 * Variable: S089-TOT-IMP-RIT-VIS from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpRitVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpRitVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMP_RIT_VIS;
    }

    public void setWlquTotImpRitVis(AfDecimal wlquTotImpRitVis) {
        writeDecimalAsPacked(Pos.S089_TOT_IMP_RIT_VIS, wlquTotImpRitVis.copy());
    }

    public void setWlquTotImpRitVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMP_RIT_VIS, Pos.S089_TOT_IMP_RIT_VIS);
    }

    /**Original name: WLQU-TOT-IMP-RIT-VIS<br>*/
    public AfDecimal getWlquTotImpRitVis() {
        return readPackedAsDecimal(Pos.S089_TOT_IMP_RIT_VIS, Len.Int.WLQU_TOT_IMP_RIT_VIS, Len.Fract.WLQU_TOT_IMP_RIT_VIS);
    }

    public byte[] getWlquTotImpRitVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMP_RIT_VIS, Pos.S089_TOT_IMP_RIT_VIS);
        return buffer;
    }

    public void initWlquTotImpRitVisSpaces() {
        fill(Pos.S089_TOT_IMP_RIT_VIS, Len.S089_TOT_IMP_RIT_VIS, Types.SPACE_CHAR);
    }

    public void setWlquTotImpRitVisNull(String wlquTotImpRitVisNull) {
        writeString(Pos.S089_TOT_IMP_RIT_VIS_NULL, wlquTotImpRitVisNull, Len.WLQU_TOT_IMP_RIT_VIS_NULL);
    }

    /**Original name: WLQU-TOT-IMP-RIT-VIS-NULL<br>*/
    public String getWlquTotImpRitVisNull() {
        return readString(Pos.S089_TOT_IMP_RIT_VIS_NULL, Len.WLQU_TOT_IMP_RIT_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_RIT_VIS = 1;
        public static final int S089_TOT_IMP_RIT_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_RIT_VIS = 8;
        public static final int WLQU_TOT_IMP_RIT_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_RIT_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_RIT_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
