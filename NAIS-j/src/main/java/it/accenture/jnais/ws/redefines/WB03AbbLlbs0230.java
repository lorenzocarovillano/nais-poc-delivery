package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-ABB<br>
 * Variable: W-B03-ABB from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03AbbLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03AbbLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_ABB;
    }

    public void setwB03Abb(AfDecimal wB03Abb) {
        writeDecimalAsPacked(Pos.W_B03_ABB, wB03Abb.copy());
    }

    /**Original name: W-B03-ABB<br>*/
    public AfDecimal getwB03Abb() {
        return readPackedAsDecimal(Pos.W_B03_ABB, Len.Int.W_B03_ABB, Len.Fract.W_B03_ABB);
    }

    public byte[] getwB03AbbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_ABB, Pos.W_B03_ABB);
        return buffer;
    }

    public void setwB03AbbNull(String wB03AbbNull) {
        writeString(Pos.W_B03_ABB_NULL, wB03AbbNull, Len.W_B03_ABB_NULL);
    }

    /**Original name: W-B03-ABB-NULL<br>*/
    public String getwB03AbbNull() {
        return readString(Pos.W_B03_ABB_NULL, Len.W_B03_ABB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_ABB = 1;
        public static final int W_B03_ABB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_ABB = 8;
        public static final int W_B03_ABB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_ABB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_ABB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
