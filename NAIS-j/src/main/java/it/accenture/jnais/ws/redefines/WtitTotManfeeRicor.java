package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-MANFEE-RICOR<br>
 * Variable: WTIT-TOT-MANFEE-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotManfeeRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotManfeeRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_MANFEE_RICOR;
    }

    public void setWtitTotManfeeRicor(AfDecimal wtitTotManfeeRicor) {
        writeDecimalAsPacked(Pos.WTIT_TOT_MANFEE_RICOR, wtitTotManfeeRicor.copy());
    }

    public void setWtitTotManfeeRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_MANFEE_RICOR, Pos.WTIT_TOT_MANFEE_RICOR);
    }

    /**Original name: WTIT-TOT-MANFEE-RICOR<br>*/
    public AfDecimal getWtitTotManfeeRicor() {
        return readPackedAsDecimal(Pos.WTIT_TOT_MANFEE_RICOR, Len.Int.WTIT_TOT_MANFEE_RICOR, Len.Fract.WTIT_TOT_MANFEE_RICOR);
    }

    public byte[] getWtitTotManfeeRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_MANFEE_RICOR, Pos.WTIT_TOT_MANFEE_RICOR);
        return buffer;
    }

    public void initWtitTotManfeeRicorSpaces() {
        fill(Pos.WTIT_TOT_MANFEE_RICOR, Len.WTIT_TOT_MANFEE_RICOR, Types.SPACE_CHAR);
    }

    public void setWtitTotManfeeRicorNull(String wtitTotManfeeRicorNull) {
        writeString(Pos.WTIT_TOT_MANFEE_RICOR_NULL, wtitTotManfeeRicorNull, Len.WTIT_TOT_MANFEE_RICOR_NULL);
    }

    /**Original name: WTIT-TOT-MANFEE-RICOR-NULL<br>*/
    public String getWtitTotManfeeRicorNull() {
        return readString(Pos.WTIT_TOT_MANFEE_RICOR_NULL, Len.WTIT_TOT_MANFEE_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_MANFEE_RICOR = 1;
        public static final int WTIT_TOT_MANFEE_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_MANFEE_RICOR = 8;
        public static final int WTIT_TOT_MANFEE_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_MANFEE_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_MANFEE_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
