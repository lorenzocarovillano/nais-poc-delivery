package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-EMIS-RIDZ<br>
 * Variable: WB03-DT-EMIS-RIDZ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtEmisRidz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtEmisRidz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_EMIS_RIDZ;
    }

    public void setWb03DtEmisRidz(int wb03DtEmisRidz) {
        writeIntAsPacked(Pos.WB03_DT_EMIS_RIDZ, wb03DtEmisRidz, Len.Int.WB03_DT_EMIS_RIDZ);
    }

    public void setWb03DtEmisRidzFormatted(String wb03DtEmisRidz) {
        setWb03DtEmisRidz(PicParser.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).parseInt(wb03DtEmisRidz));
    }

    public void setWb03DtEmisRidzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_EMIS_RIDZ, Pos.WB03_DT_EMIS_RIDZ);
    }

    /**Original name: WB03-DT-EMIS-RIDZ<br>*/
    public int getWb03DtEmisRidz() {
        return readPackedAsInt(Pos.WB03_DT_EMIS_RIDZ, Len.Int.WB03_DT_EMIS_RIDZ);
    }

    public byte[] getWb03DtEmisRidzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_EMIS_RIDZ, Pos.WB03_DT_EMIS_RIDZ);
        return buffer;
    }

    public void setWb03DtEmisRidzNull(String wb03DtEmisRidzNull) {
        writeString(Pos.WB03_DT_EMIS_RIDZ_NULL, wb03DtEmisRidzNull, Len.WB03_DT_EMIS_RIDZ_NULL);
    }

    /**Original name: WB03-DT-EMIS-RIDZ-NULL<br>*/
    public String getWb03DtEmisRidzNull() {
        return readString(Pos.WB03_DT_EMIS_RIDZ_NULL, Len.WB03_DT_EMIS_RIDZ_NULL);
    }

    public String getWb03DtEmisRidzNullFormatted() {
        return Functions.padBlanks(getWb03DtEmisRidzNull(), Len.WB03_DT_EMIS_RIDZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_EMIS_RIDZ = 1;
        public static final int WB03_DT_EMIS_RIDZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_EMIS_RIDZ = 5;
        public static final int WB03_DT_EMIS_RIDZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_EMIS_RIDZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
