package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ACCPRE-SOST-CALC<br>
 * Variable: WDFL-ACCPRE-SOST-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAccpreSostCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAccpreSostCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ACCPRE_SOST_CALC;
    }

    public void setWdflAccpreSostCalc(AfDecimal wdflAccpreSostCalc) {
        writeDecimalAsPacked(Pos.WDFL_ACCPRE_SOST_CALC, wdflAccpreSostCalc.copy());
    }

    public void setWdflAccpreSostCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ACCPRE_SOST_CALC, Pos.WDFL_ACCPRE_SOST_CALC);
    }

    /**Original name: WDFL-ACCPRE-SOST-CALC<br>*/
    public AfDecimal getWdflAccpreSostCalc() {
        return readPackedAsDecimal(Pos.WDFL_ACCPRE_SOST_CALC, Len.Int.WDFL_ACCPRE_SOST_CALC, Len.Fract.WDFL_ACCPRE_SOST_CALC);
    }

    public byte[] getWdflAccpreSostCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ACCPRE_SOST_CALC, Pos.WDFL_ACCPRE_SOST_CALC);
        return buffer;
    }

    public void setWdflAccpreSostCalcNull(String wdflAccpreSostCalcNull) {
        writeString(Pos.WDFL_ACCPRE_SOST_CALC_NULL, wdflAccpreSostCalcNull, Len.WDFL_ACCPRE_SOST_CALC_NULL);
    }

    /**Original name: WDFL-ACCPRE-SOST-CALC-NULL<br>*/
    public String getWdflAccpreSostCalcNull() {
        return readString(Pos.WDFL_ACCPRE_SOST_CALC_NULL, Len.WDFL_ACCPRE_SOST_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_SOST_CALC = 1;
        public static final int WDFL_ACCPRE_SOST_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_SOST_CALC = 8;
        public static final int WDFL_ACCPRE_SOST_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_SOST_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_SOST_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
