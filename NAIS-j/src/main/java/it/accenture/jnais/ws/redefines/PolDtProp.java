package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-DT-PROP<br>
 * Variable: POL-DT-PROP from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDtProp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolDtProp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_DT_PROP;
    }

    public void setPolDtProp(int polDtProp) {
        writeIntAsPacked(Pos.POL_DT_PROP, polDtProp, Len.Int.POL_DT_PROP);
    }

    public void setPolDtPropFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_DT_PROP, Pos.POL_DT_PROP);
    }

    /**Original name: POL-DT-PROP<br>*/
    public int getPolDtProp() {
        return readPackedAsInt(Pos.POL_DT_PROP, Len.Int.POL_DT_PROP);
    }

    public byte[] getPolDtPropAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_DT_PROP, Pos.POL_DT_PROP);
        return buffer;
    }

    public void setPolDtPropNull(String polDtPropNull) {
        writeString(Pos.POL_DT_PROP_NULL, polDtPropNull, Len.POL_DT_PROP_NULL);
    }

    /**Original name: POL-DT-PROP-NULL<br>*/
    public String getPolDtPropNull() {
        return readString(Pos.POL_DT_PROP_NULL, Len.POL_DT_PROP_NULL);
    }

    public String getPolDtPropNullFormatted() {
        return Functions.padBlanks(getPolDtPropNull(), Len.POL_DT_PROP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_DT_PROP = 1;
        public static final int POL_DT_PROP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_DT_PROP = 5;
        public static final int POL_DT_PROP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_DT_PROP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
