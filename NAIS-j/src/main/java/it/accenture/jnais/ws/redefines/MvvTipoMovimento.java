package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MVV-TIPO-MOVIMENTO<br>
 * Variable: MVV-TIPO-MOVIMENTO from program LDBS1390<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MvvTipoMovimento extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MvvTipoMovimento() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MVV_TIPO_MOVIMENTO;
    }

    public void setMvvTipoMovimento(int mvvTipoMovimento) {
        writeIntAsPacked(Pos.MVV_TIPO_MOVIMENTO, mvvTipoMovimento, Len.Int.MVV_TIPO_MOVIMENTO);
    }

    public void setMvvTipoMovimentoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MVV_TIPO_MOVIMENTO, Pos.MVV_TIPO_MOVIMENTO);
    }

    /**Original name: MVV-TIPO-MOVIMENTO<br>*/
    public int getMvvTipoMovimento() {
        return readPackedAsInt(Pos.MVV_TIPO_MOVIMENTO, Len.Int.MVV_TIPO_MOVIMENTO);
    }

    public byte[] getMvvTipoMovimentoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MVV_TIPO_MOVIMENTO, Pos.MVV_TIPO_MOVIMENTO);
        return buffer;
    }

    public void setMvvTipoMovimentoNull(String mvvTipoMovimentoNull) {
        writeString(Pos.MVV_TIPO_MOVIMENTO_NULL, mvvTipoMovimentoNull, Len.MVV_TIPO_MOVIMENTO_NULL);
    }

    /**Original name: MVV-TIPO-MOVIMENTO-NULL<br>*/
    public String getMvvTipoMovimentoNull() {
        return readString(Pos.MVV_TIPO_MOVIMENTO_NULL, Len.MVV_TIPO_MOVIMENTO_NULL);
    }

    public String getMvvTipoMovimentoNullFormatted() {
        return Functions.padBlanks(getMvvTipoMovimentoNull(), Len.MVV_TIPO_MOVIMENTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MVV_TIPO_MOVIMENTO = 1;
        public static final int MVV_TIPO_MOVIMENTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MVV_TIPO_MOVIMENTO = 3;
        public static final int MVV_TIPO_MOVIMENTO_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MVV_TIPO_MOVIMENTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
