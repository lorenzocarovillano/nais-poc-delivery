package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-EC-UL-COLL<br>
 * Variable: WPCO-DT-ULT-EC-UL-COLL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEcUlColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEcUlColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_EC_UL_COLL;
    }

    public void setWpcoDtUltEcUlColl(int wpcoDtUltEcUlColl) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_EC_UL_COLL, wpcoDtUltEcUlColl, Len.Int.WPCO_DT_ULT_EC_UL_COLL);
    }

    public void setDpcoDtUltEcUlCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_EC_UL_COLL, Pos.WPCO_DT_ULT_EC_UL_COLL);
    }

    /**Original name: WPCO-DT-ULT-EC-UL-COLL<br>*/
    public int getWpcoDtUltEcUlColl() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_EC_UL_COLL, Len.Int.WPCO_DT_ULT_EC_UL_COLL);
    }

    public byte[] getWpcoDtUltEcUlCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_EC_UL_COLL, Pos.WPCO_DT_ULT_EC_UL_COLL);
        return buffer;
    }

    public void setWpcoDtUltEcUlCollNull(String wpcoDtUltEcUlCollNull) {
        writeString(Pos.WPCO_DT_ULT_EC_UL_COLL_NULL, wpcoDtUltEcUlCollNull, Len.WPCO_DT_ULT_EC_UL_COLL_NULL);
    }

    /**Original name: WPCO-DT-ULT-EC-UL-COLL-NULL<br>*/
    public String getWpcoDtUltEcUlCollNull() {
        return readString(Pos.WPCO_DT_ULT_EC_UL_COLL_NULL, Len.WPCO_DT_ULT_EC_UL_COLL_NULL);
    }

    public String getWpcoDtUltEcUlCollNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEcUlCollNull(), Len.WPCO_DT_ULT_EC_UL_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_UL_COLL = 1;
        public static final int WPCO_DT_ULT_EC_UL_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_UL_COLL = 5;
        public static final int WPCO_DT_ULT_EC_UL_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_EC_UL_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
