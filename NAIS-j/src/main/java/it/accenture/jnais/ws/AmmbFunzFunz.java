package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IAmmbFunz;

/**Original name: AMMB-FUNZ-FUNZ<br>
 * Variable: AMMB-FUNZ-FUNZ from copybook IDBVL051<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AmmbFunzFunz extends SerializableParameter implements IAmmbFunz {

    //==== PROPERTIES ====
    //Original name: L05-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: L05-TP-MOVI-ESEC
    private int tpMoviEsec = DefaultValues.INT_VAL;
    //Original name: L05-TP-MOVI-RIFTO
    private int tpMoviRifto = DefaultValues.INT_VAL;
    //Original name: L05-GRAV-FUNZ-FUNZ
    private String gravFunzFunz = DefaultValues.stringVal(Len.GRAV_FUNZ_FUNZ);
    //Original name: L05-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: L05-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: L05-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: L05-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: L05-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: L05-COD-BLOCCO
    private String codBlocco = DefaultValues.stringVal(Len.COD_BLOCCO);
    //Original name: L05-SRVZ-VER-ANN
    private String srvzVerAnn = DefaultValues.stringVal(Len.SRVZ_VER_ANN);
    //Original name: L05-WHERE-CONDITION-LEN
    private short whereConditionLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: L05-WHERE-CONDITION
    private String whereCondition = DefaultValues.stringVal(Len.WHERE_CONDITION);
    //Original name: L05-FL-POLI-IFP
    private char flPoliIfp = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AMMB_FUNZ_FUNZ;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAmmbFunzFunzBytes(buf);
    }

    public void setAmmbFunzFunzBytes(byte[] buffer) {
        setAmmbFunzFunzBytes(buffer, 1);
    }

    public byte[] getAmmbFunzFunzBytes() {
        byte[] buffer = new byte[Len.AMMB_FUNZ_FUNZ];
        return getAmmbFunzFunzBytes(buffer, 1);
    }

    public void setAmmbFunzFunzBytes(byte[] buffer, int offset) {
        int position = offset;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        tpMoviEsec = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI_ESEC, 0);
        position += Len.TP_MOVI_ESEC;
        tpMoviRifto = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI_RIFTO, 0);
        position += Len.TP_MOVI_RIFTO;
        gravFunzFunz = MarshalByte.readString(buffer, position, Len.GRAV_FUNZ_FUNZ);
        position += Len.GRAV_FUNZ_FUNZ;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        codBlocco = MarshalByte.readString(buffer, position, Len.COD_BLOCCO);
        position += Len.COD_BLOCCO;
        srvzVerAnn = MarshalByte.readString(buffer, position, Len.SRVZ_VER_ANN);
        position += Len.SRVZ_VER_ANN;
        setWhereConditionVcharBytes(buffer, position);
        position += Len.WHERE_CONDITION_VCHAR;
        flPoliIfp = MarshalByte.readChar(buffer, position);
    }

    public byte[] getAmmbFunzFunzBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, tpMoviEsec, Len.Int.TP_MOVI_ESEC, 0);
        position += Len.TP_MOVI_ESEC;
        MarshalByte.writeIntAsPacked(buffer, position, tpMoviRifto, Len.Int.TP_MOVI_RIFTO, 0);
        position += Len.TP_MOVI_RIFTO;
        MarshalByte.writeString(buffer, position, gravFunzFunz, Len.GRAV_FUNZ_FUNZ);
        position += Len.GRAV_FUNZ_FUNZ;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsCptz, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codBlocco, Len.COD_BLOCCO);
        position += Len.COD_BLOCCO;
        MarshalByte.writeString(buffer, position, srvzVerAnn, Len.SRVZ_VER_ANN);
        position += Len.SRVZ_VER_ANN;
        getWhereConditionVcharBytes(buffer, position);
        position += Len.WHERE_CONDITION_VCHAR;
        MarshalByte.writeChar(buffer, position, flPoliIfp);
        return buffer;
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    @Override
    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setTpMoviEsec(int tpMoviEsec) {
        this.tpMoviEsec = tpMoviEsec;
    }

    public int getTpMoviEsec() {
        return this.tpMoviEsec;
    }

    public void setTpMoviRifto(int tpMoviRifto) {
        this.tpMoviRifto = tpMoviRifto;
    }

    public int getTpMoviRifto() {
        return this.tpMoviRifto;
    }

    @Override
    public void setGravFunzFunz(String gravFunzFunz) {
        this.gravFunzFunz = Functions.subString(gravFunzFunz, Len.GRAV_FUNZ_FUNZ);
    }

    @Override
    public String getGravFunzFunz() {
        return this.gravFunzFunz;
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public void setDsOperSqlFormatted(String dsOperSql) {
        setDsOperSql(Functions.charAt(dsOperSql, Types.CHAR_SIZE));
    }

    @Override
    public char getDsOperSql() {
        return this.dsOperSql;
    }

    @Override
    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    @Override
    public int getDsVer() {
        return this.dsVer;
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    @Override
    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    @Override
    public String getDsUtente() {
        return this.dsUtente;
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public void setDsStatoElabFormatted(String dsStatoElab) {
        setDsStatoElab(Functions.charAt(dsStatoElab, Types.CHAR_SIZE));
    }

    @Override
    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    @Override
    public void setCodBlocco(String codBlocco) {
        this.codBlocco = Functions.subString(codBlocco, Len.COD_BLOCCO);
    }

    @Override
    public String getCodBlocco() {
        return this.codBlocco;
    }

    public String getCodBloccoFormatted() {
        return Functions.padBlanks(getCodBlocco(), Len.COD_BLOCCO);
    }

    @Override
    public void setSrvzVerAnn(String srvzVerAnn) {
        this.srvzVerAnn = Functions.subString(srvzVerAnn, Len.SRVZ_VER_ANN);
    }

    @Override
    public String getSrvzVerAnn() {
        return this.srvzVerAnn;
    }

    public String getSrvzVerAnnFormatted() {
        return Functions.padBlanks(getSrvzVerAnn(), Len.SRVZ_VER_ANN);
    }

    public void setWhereConditionVcharFormatted(String data) {
        byte[] buffer = new byte[Len.WHERE_CONDITION_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.WHERE_CONDITION_VCHAR);
        setWhereConditionVcharBytes(buffer, 1);
    }

    public String getWhereConditionVcharFormatted() {
        return MarshalByteExt.bufferToStr(getWhereConditionVcharBytes());
    }

    /**Original name: L05-WHERE-CONDITION-VCHAR<br>*/
    public byte[] getWhereConditionVcharBytes() {
        byte[] buffer = new byte[Len.WHERE_CONDITION_VCHAR];
        return getWhereConditionVcharBytes(buffer, 1);
    }

    public void setWhereConditionVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        whereConditionLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        whereCondition = MarshalByte.readString(buffer, position, Len.WHERE_CONDITION);
    }

    public byte[] getWhereConditionVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, whereConditionLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, whereCondition, Len.WHERE_CONDITION);
        return buffer;
    }

    public void setWhereConditionLen(short whereConditionLen) {
        this.whereConditionLen = whereConditionLen;
    }

    public short getWhereConditionLen() {
        return this.whereConditionLen;
    }

    public void setWhereCondition(String whereCondition) {
        this.whereCondition = Functions.subString(whereCondition, Len.WHERE_CONDITION);
    }

    public String getWhereCondition() {
        return this.whereCondition;
    }

    @Override
    public void setFlPoliIfp(char flPoliIfp) {
        this.flPoliIfp = flPoliIfp;
    }

    @Override
    public char getFlPoliIfp() {
        return this.flPoliIfp;
    }

    @Override
    public String getCodBloccoObj() {
        return getCodBlocco();
    }

    @Override
    public void setCodBloccoObj(String codBloccoObj) {
        setCodBlocco(codBloccoObj);
    }

    @Override
    public Character getFlPoliIfpObj() {
        return ((Character)getFlPoliIfp());
    }

    @Override
    public void setFlPoliIfpObj(Character flPoliIfpObj) {
        setFlPoliIfp(((char)flPoliIfpObj));
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public int getL05TpMoviEsec() {
        throw new FieldNotMappedException("l05TpMoviEsec");
    }

    @Override
    public void setL05TpMoviEsec(int l05TpMoviEsec) {
        throw new FieldNotMappedException("l05TpMoviEsec");
    }

    @Override
    public int getL05TpMoviRifto() {
        throw new FieldNotMappedException("l05TpMoviRifto");
    }

    @Override
    public void setL05TpMoviRifto(int l05TpMoviRifto) {
        throw new FieldNotMappedException("l05TpMoviRifto");
    }

    @Override
    public String getLdbv2441GravFunzFunz1() {
        throw new FieldNotMappedException("ldbv2441GravFunzFunz1");
    }

    @Override
    public void setLdbv2441GravFunzFunz1(String ldbv2441GravFunzFunz1) {
        throw new FieldNotMappedException("ldbv2441GravFunzFunz1");
    }

    @Override
    public String getLdbv2441GravFunzFunz2() {
        throw new FieldNotMappedException("ldbv2441GravFunzFunz2");
    }

    @Override
    public void setLdbv2441GravFunzFunz2(String ldbv2441GravFunzFunz2) {
        throw new FieldNotMappedException("ldbv2441GravFunzFunz2");
    }

    @Override
    public String getLdbv2441GravFunzFunz3() {
        throw new FieldNotMappedException("ldbv2441GravFunzFunz3");
    }

    @Override
    public void setLdbv2441GravFunzFunz3(String ldbv2441GravFunzFunz3) {
        throw new FieldNotMappedException("ldbv2441GravFunzFunz3");
    }

    @Override
    public String getLdbv2441GravFunzFunz4() {
        throw new FieldNotMappedException("ldbv2441GravFunzFunz4");
    }

    @Override
    public void setLdbv2441GravFunzFunz4(String ldbv2441GravFunzFunz4) {
        throw new FieldNotMappedException("ldbv2441GravFunzFunz4");
    }

    @Override
    public String getLdbv2441GravFunzFunz5() {
        throw new FieldNotMappedException("ldbv2441GravFunzFunz5");
    }

    @Override
    public void setLdbv2441GravFunzFunz5(String ldbv2441GravFunzFunz5) {
        throw new FieldNotMappedException("ldbv2441GravFunzFunz5");
    }

    @Override
    public char getLdbvg781FlPoliIfp() {
        throw new FieldNotMappedException("ldbvg781FlPoliIfp");
    }

    @Override
    public void setLdbvg781FlPoliIfp(char ldbvg781FlPoliIfp) {
        throw new FieldNotMappedException("ldbvg781FlPoliIfp");
    }

    @Override
    public String getLdbvg781GravFunzFunz1() {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz1");
    }

    @Override
    public void setLdbvg781GravFunzFunz1(String ldbvg781GravFunzFunz1) {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz1");
    }

    @Override
    public String getLdbvg781GravFunzFunz2() {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz2");
    }

    @Override
    public void setLdbvg781GravFunzFunz2(String ldbvg781GravFunzFunz2) {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz2");
    }

    @Override
    public String getLdbvg781GravFunzFunz3() {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz3");
    }

    @Override
    public void setLdbvg781GravFunzFunz3(String ldbvg781GravFunzFunz3) {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz3");
    }

    @Override
    public String getLdbvg781GravFunzFunz4() {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz4");
    }

    @Override
    public void setLdbvg781GravFunzFunz4(String ldbvg781GravFunzFunz4) {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz4");
    }

    @Override
    public String getLdbvg781GravFunzFunz5() {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz5");
    }

    @Override
    public void setLdbvg781GravFunzFunz5(String ldbvg781GravFunzFunz5) {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz5");
    }

    @Override
    public String getSrvzVerAnnObj() {
        return getSrvzVerAnn();
    }

    @Override
    public void setSrvzVerAnnObj(String srvzVerAnnObj) {
        setSrvzVerAnn(srvzVerAnnObj);
    }

    @Override
    public String getWhereConditionVchar() {
        throw new FieldNotMappedException("whereConditionVchar");
    }

    @Override
    public void setWhereConditionVchar(String whereConditionVchar) {
        throw new FieldNotMappedException("whereConditionVchar");
    }

    @Override
    public String getWhereConditionVcharObj() {
        return getWhereConditionVchar();
    }

    @Override
    public void setWhereConditionVcharObj(String whereConditionVcharObj) {
        setWhereConditionVchar(whereConditionVcharObj);
    }

    @Override
    public byte[] serialize() {
        return getAmmbFunzFunzBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_COMP_ANIA = 3;
        public static final int TP_MOVI_ESEC = 3;
        public static final int TP_MOVI_RIFTO = 3;
        public static final int GRAV_FUNZ_FUNZ = 2;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int COD_BLOCCO = 5;
        public static final int SRVZ_VER_ANN = 8;
        public static final int WHERE_CONDITION_LEN = 2;
        public static final int WHERE_CONDITION = 300;
        public static final int WHERE_CONDITION_VCHAR = WHERE_CONDITION_LEN + WHERE_CONDITION;
        public static final int FL_POLI_IFP = 1;
        public static final int AMMB_FUNZ_FUNZ = COD_COMP_ANIA + TP_MOVI_ESEC + TP_MOVI_RIFTO + GRAV_FUNZ_FUNZ + DS_OPER_SQL + DS_VER + DS_TS_CPTZ + DS_UTENTE + DS_STATO_ELAB + COD_BLOCCO + SRVZ_VER_ANN + WHERE_CONDITION_VCHAR + FL_POLI_IFP;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_COMP_ANIA = 5;
            public static final int TP_MOVI_ESEC = 5;
            public static final int TP_MOVI_RIFTO = 5;
            public static final int DS_VER = 9;
            public static final int DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
