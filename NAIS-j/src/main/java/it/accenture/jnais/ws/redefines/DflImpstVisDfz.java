package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-VIS-DFZ<br>
 * Variable: DFL-IMPST-VIS-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstVisDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstVisDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_VIS_DFZ;
    }

    public void setDflImpstVisDfz(AfDecimal dflImpstVisDfz) {
        writeDecimalAsPacked(Pos.DFL_IMPST_VIS_DFZ, dflImpstVisDfz.copy());
    }

    public void setDflImpstVisDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_VIS_DFZ, Pos.DFL_IMPST_VIS_DFZ);
    }

    /**Original name: DFL-IMPST-VIS-DFZ<br>*/
    public AfDecimal getDflImpstVisDfz() {
        return readPackedAsDecimal(Pos.DFL_IMPST_VIS_DFZ, Len.Int.DFL_IMPST_VIS_DFZ, Len.Fract.DFL_IMPST_VIS_DFZ);
    }

    public byte[] getDflImpstVisDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_VIS_DFZ, Pos.DFL_IMPST_VIS_DFZ);
        return buffer;
    }

    public void setDflImpstVisDfzNull(String dflImpstVisDfzNull) {
        writeString(Pos.DFL_IMPST_VIS_DFZ_NULL, dflImpstVisDfzNull, Len.DFL_IMPST_VIS_DFZ_NULL);
    }

    /**Original name: DFL-IMPST-VIS-DFZ-NULL<br>*/
    public String getDflImpstVisDfzNull() {
        return readString(Pos.DFL_IMPST_VIS_DFZ_NULL, Len.DFL_IMPST_VIS_DFZ_NULL);
    }

    public String getDflImpstVisDfzNullFormatted() {
        return Functions.padBlanks(getDflImpstVisDfzNull(), Len.DFL_IMPST_VIS_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS_DFZ = 1;
        public static final int DFL_IMPST_VIS_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS_DFZ = 8;
        public static final int DFL_IMPST_VIS_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
