package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-INI-VAL-TAR<br>
 * Variable: B03-DT-INI-VAL-TAR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtIniValTar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtIniValTar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_INI_VAL_TAR;
    }

    public void setB03DtIniValTar(int b03DtIniValTar) {
        writeIntAsPacked(Pos.B03_DT_INI_VAL_TAR, b03DtIniValTar, Len.Int.B03_DT_INI_VAL_TAR);
    }

    public void setB03DtIniValTarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_INI_VAL_TAR, Pos.B03_DT_INI_VAL_TAR);
    }

    /**Original name: B03-DT-INI-VAL-TAR<br>*/
    public int getB03DtIniValTar() {
        return readPackedAsInt(Pos.B03_DT_INI_VAL_TAR, Len.Int.B03_DT_INI_VAL_TAR);
    }

    public byte[] getB03DtIniValTarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_INI_VAL_TAR, Pos.B03_DT_INI_VAL_TAR);
        return buffer;
    }

    public void setB03DtIniValTarNull(String b03DtIniValTarNull) {
        writeString(Pos.B03_DT_INI_VAL_TAR_NULL, b03DtIniValTarNull, Len.B03_DT_INI_VAL_TAR_NULL);
    }

    /**Original name: B03-DT-INI-VAL-TAR-NULL<br>*/
    public String getB03DtIniValTarNull() {
        return readString(Pos.B03_DT_INI_VAL_TAR_NULL, Len.B03_DT_INI_VAL_TAR_NULL);
    }

    public String getB03DtIniValTarNullFormatted() {
        return Functions.padBlanks(getB03DtIniValTarNull(), Len.B03_DT_INI_VAL_TAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_INI_VAL_TAR = 1;
        public static final int B03_DT_INI_VAL_TAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_INI_VAL_TAR = 5;
        public static final int B03_DT_INI_VAL_TAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_INI_VAL_TAR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
