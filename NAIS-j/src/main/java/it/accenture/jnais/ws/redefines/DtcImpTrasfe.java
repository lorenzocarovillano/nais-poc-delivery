package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-IMP-TRASFE<br>
 * Variable: DTC-IMP-TRASFE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcImpTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcImpTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_IMP_TRASFE;
    }

    public void setDtcImpTrasfe(AfDecimal dtcImpTrasfe) {
        writeDecimalAsPacked(Pos.DTC_IMP_TRASFE, dtcImpTrasfe.copy());
    }

    public void setDtcImpTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_IMP_TRASFE, Pos.DTC_IMP_TRASFE);
    }

    /**Original name: DTC-IMP-TRASFE<br>*/
    public AfDecimal getDtcImpTrasfe() {
        return readPackedAsDecimal(Pos.DTC_IMP_TRASFE, Len.Int.DTC_IMP_TRASFE, Len.Fract.DTC_IMP_TRASFE);
    }

    public byte[] getDtcImpTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_IMP_TRASFE, Pos.DTC_IMP_TRASFE);
        return buffer;
    }

    public void setDtcImpTrasfeNull(String dtcImpTrasfeNull) {
        writeString(Pos.DTC_IMP_TRASFE_NULL, dtcImpTrasfeNull, Len.DTC_IMP_TRASFE_NULL);
    }

    /**Original name: DTC-IMP-TRASFE-NULL<br>*/
    public String getDtcImpTrasfeNull() {
        return readString(Pos.DTC_IMP_TRASFE_NULL, Len.DTC_IMP_TRASFE_NULL);
    }

    public String getDtcImpTrasfeNullFormatted() {
        return Functions.padBlanks(getDtcImpTrasfeNull(), Len.DTC_IMP_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_IMP_TRASFE = 1;
        public static final int DTC_IMP_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_IMP_TRASFE = 8;
        public static final int DTC_IMP_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_IMP_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_IMP_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
