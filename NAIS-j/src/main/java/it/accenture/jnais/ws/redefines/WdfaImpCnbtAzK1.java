package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMP-CNBT-AZ-K1<br>
 * Variable: WDFA-IMP-CNBT-AZ-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpCnbtAzK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpCnbtAzK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMP_CNBT_AZ_K1;
    }

    public void setWdfaImpCnbtAzK1(AfDecimal wdfaImpCnbtAzK1) {
        writeDecimalAsPacked(Pos.WDFA_IMP_CNBT_AZ_K1, wdfaImpCnbtAzK1.copy());
    }

    public void setWdfaImpCnbtAzK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMP_CNBT_AZ_K1, Pos.WDFA_IMP_CNBT_AZ_K1);
    }

    /**Original name: WDFA-IMP-CNBT-AZ-K1<br>*/
    public AfDecimal getWdfaImpCnbtAzK1() {
        return readPackedAsDecimal(Pos.WDFA_IMP_CNBT_AZ_K1, Len.Int.WDFA_IMP_CNBT_AZ_K1, Len.Fract.WDFA_IMP_CNBT_AZ_K1);
    }

    public byte[] getWdfaImpCnbtAzK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMP_CNBT_AZ_K1, Pos.WDFA_IMP_CNBT_AZ_K1);
        return buffer;
    }

    public void setWdfaImpCnbtAzK1Null(String wdfaImpCnbtAzK1Null) {
        writeString(Pos.WDFA_IMP_CNBT_AZ_K1_NULL, wdfaImpCnbtAzK1Null, Len.WDFA_IMP_CNBT_AZ_K1_NULL);
    }

    /**Original name: WDFA-IMP-CNBT-AZ-K1-NULL<br>*/
    public String getWdfaImpCnbtAzK1Null() {
        return readString(Pos.WDFA_IMP_CNBT_AZ_K1_NULL, Len.WDFA_IMP_CNBT_AZ_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_AZ_K1 = 1;
        public static final int WDFA_IMP_CNBT_AZ_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_AZ_K1 = 8;
        public static final int WDFA_IMP_CNBT_AZ_K1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_AZ_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_AZ_K1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
