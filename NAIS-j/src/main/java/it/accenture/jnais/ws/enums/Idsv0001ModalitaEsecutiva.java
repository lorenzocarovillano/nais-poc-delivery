package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0001-MODALITA-ESECUTIVA<br>
 * Variable: IDSV0001-MODALITA-ESECUTIVA from copybook IDSV0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0001ModalitaEsecutiva {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char ON_LINE = 'O';
    public static final char BATCH = 'B';
    public static final char BATCH_INFR = 'I';

    //==== METHODS ====
    public void setModalitaEsecutiva(char modalitaEsecutiva) {
        this.value = modalitaEsecutiva;
    }

    public char getModalitaEsecutiva() {
        return this.value;
    }

    public boolean isIdsv0001OnLine() {
        return value == ON_LINE;
    }

    public boolean isIdsv0001Batch() {
        return value == BATCH;
    }

    public boolean isIdsv0001BatchInfr() {
        return value == BATCH_INFR;
    }

    public void setIdsv0001BatchInfr() {
        value = BATCH_INFR;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MODALITA_ESECUTIVA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
