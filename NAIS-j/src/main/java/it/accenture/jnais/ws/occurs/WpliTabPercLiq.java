package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvpli1;
import it.accenture.jnais.copy.WpliDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WPLI-TAB-PERC-LIQ<br>
 * Variables: WPLI-TAB-PERC-LIQ from copybook LCCVPLIA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpliTabPercLiq {

    //==== PROPERTIES ====
    //Original name: LCCVPLI1
    private Lccvpli1 lccvpli1 = new Lccvpli1();

    //==== METHODS ====
    public void setWpliTabPercLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpli1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpli1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpli1.Len.Int.ID_PTF, 0));
        position += Lccvpli1.Len.ID_PTF;
        lccvpli1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWpliTabPercLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvpli1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvpli1.getIdPtf(), Lccvpli1.Len.Int.ID_PTF, 0);
        position += Lccvpli1.Len.ID_PTF;
        lccvpli1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWpliTabPercLiqSpaces() {
        lccvpli1.initLccvpli1Spaces();
    }

    public Lccvpli1 getLccvpli1() {
        return lccvpli1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPLI_TAB_PERC_LIQ = WpolStatus.Len.STATUS + Lccvpli1.Len.ID_PTF + WpliDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
