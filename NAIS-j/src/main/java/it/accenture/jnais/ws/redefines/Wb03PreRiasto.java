package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRE-RIASTO<br>
 * Variable: WB03-PRE-RIASTO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PreRiasto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PreRiasto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRE_RIASTO;
    }

    public void setWb03PreRiastoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRE_RIASTO, Pos.WB03_PRE_RIASTO);
    }

    /**Original name: WB03-PRE-RIASTO<br>*/
    public AfDecimal getWb03PreRiasto() {
        return readPackedAsDecimal(Pos.WB03_PRE_RIASTO, Len.Int.WB03_PRE_RIASTO, Len.Fract.WB03_PRE_RIASTO);
    }

    public byte[] getWb03PreRiastoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRE_RIASTO, Pos.WB03_PRE_RIASTO);
        return buffer;
    }

    public void setWb03PreRiastoNull(String wb03PreRiastoNull) {
        writeString(Pos.WB03_PRE_RIASTO_NULL, wb03PreRiastoNull, Len.WB03_PRE_RIASTO_NULL);
    }

    /**Original name: WB03-PRE-RIASTO-NULL<br>*/
    public String getWb03PreRiastoNull() {
        return readString(Pos.WB03_PRE_RIASTO_NULL, Len.WB03_PRE_RIASTO_NULL);
    }

    public String getWb03PreRiastoNullFormatted() {
        return Functions.padBlanks(getWb03PreRiastoNull(), Len.WB03_PRE_RIASTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRE_RIASTO = 1;
        public static final int WB03_PRE_RIASTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRE_RIASTO = 8;
        public static final int WB03_PRE_RIASTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRE_RIASTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRE_RIASTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
