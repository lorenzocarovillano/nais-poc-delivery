package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-IMP-ADER<br>
 * Variable: DAD-IMP-ADER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_IMP_ADER;
    }

    public void setDadImpAder(AfDecimal dadImpAder) {
        writeDecimalAsPacked(Pos.DAD_IMP_ADER, dadImpAder.copy());
    }

    public void setDadImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_IMP_ADER, Pos.DAD_IMP_ADER);
    }

    /**Original name: DAD-IMP-ADER<br>*/
    public AfDecimal getDadImpAder() {
        return readPackedAsDecimal(Pos.DAD_IMP_ADER, Len.Int.DAD_IMP_ADER, Len.Fract.DAD_IMP_ADER);
    }

    public byte[] getDadImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_IMP_ADER, Pos.DAD_IMP_ADER);
        return buffer;
    }

    public void setDadImpAderNull(String dadImpAderNull) {
        writeString(Pos.DAD_IMP_ADER_NULL, dadImpAderNull, Len.DAD_IMP_ADER_NULL);
    }

    /**Original name: DAD-IMP-ADER-NULL<br>*/
    public String getDadImpAderNull() {
        return readString(Pos.DAD_IMP_ADER_NULL, Len.DAD_IMP_ADER_NULL);
    }

    public String getDadImpAderNullFormatted() {
        return Functions.padBlanks(getDadImpAderNull(), Len.DAD_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_IMP_ADER = 1;
        public static final int DAD_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_IMP_ADER = 8;
        public static final int DAD_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DAD_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
