package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-INTR-SU-PREST<br>
 * Variable: LQU-IMPB-INTR-SU-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbIntrSuPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbIntrSuPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_INTR_SU_PREST;
    }

    public void setLquImpbIntrSuPrest(AfDecimal lquImpbIntrSuPrest) {
        writeDecimalAsPacked(Pos.LQU_IMPB_INTR_SU_PREST, lquImpbIntrSuPrest.copy());
    }

    public void setLquImpbIntrSuPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_INTR_SU_PREST, Pos.LQU_IMPB_INTR_SU_PREST);
    }

    /**Original name: LQU-IMPB-INTR-SU-PREST<br>*/
    public AfDecimal getLquImpbIntrSuPrest() {
        return readPackedAsDecimal(Pos.LQU_IMPB_INTR_SU_PREST, Len.Int.LQU_IMPB_INTR_SU_PREST, Len.Fract.LQU_IMPB_INTR_SU_PREST);
    }

    public byte[] getLquImpbIntrSuPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_INTR_SU_PREST, Pos.LQU_IMPB_INTR_SU_PREST);
        return buffer;
    }

    public void setLquImpbIntrSuPrestNull(String lquImpbIntrSuPrestNull) {
        writeString(Pos.LQU_IMPB_INTR_SU_PREST_NULL, lquImpbIntrSuPrestNull, Len.LQU_IMPB_INTR_SU_PREST_NULL);
    }

    /**Original name: LQU-IMPB-INTR-SU-PREST-NULL<br>*/
    public String getLquImpbIntrSuPrestNull() {
        return readString(Pos.LQU_IMPB_INTR_SU_PREST_NULL, Len.LQU_IMPB_INTR_SU_PREST_NULL);
    }

    public String getLquImpbIntrSuPrestNullFormatted() {
        return Functions.padBlanks(getLquImpbIntrSuPrestNull(), Len.LQU_IMPB_INTR_SU_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_INTR_SU_PREST = 1;
        public static final int LQU_IMPB_INTR_SU_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_INTR_SU_PREST = 8;
        public static final int LQU_IMPB_INTR_SU_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_INTR_SU_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_INTR_SU_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
