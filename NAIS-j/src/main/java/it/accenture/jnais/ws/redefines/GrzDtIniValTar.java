package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-DT-INI-VAL-TAR<br>
 * Variable: GRZ-DT-INI-VAL-TAR from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzDtIniValTar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzDtIniValTar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_DT_INI_VAL_TAR;
    }

    public void setGrzDtIniValTar(int grzDtIniValTar) {
        writeIntAsPacked(Pos.GRZ_DT_INI_VAL_TAR, grzDtIniValTar, Len.Int.GRZ_DT_INI_VAL_TAR);
    }

    public void setGrzDtIniValTarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_DT_INI_VAL_TAR, Pos.GRZ_DT_INI_VAL_TAR);
    }

    /**Original name: GRZ-DT-INI-VAL-TAR<br>*/
    public int getGrzDtIniValTar() {
        return readPackedAsInt(Pos.GRZ_DT_INI_VAL_TAR, Len.Int.GRZ_DT_INI_VAL_TAR);
    }

    public byte[] getGrzDtIniValTarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_DT_INI_VAL_TAR, Pos.GRZ_DT_INI_VAL_TAR);
        return buffer;
    }

    public void setGrzDtIniValTarNull(String grzDtIniValTarNull) {
        writeString(Pos.GRZ_DT_INI_VAL_TAR_NULL, grzDtIniValTarNull, Len.GRZ_DT_INI_VAL_TAR_NULL);
    }

    /**Original name: GRZ-DT-INI-VAL-TAR-NULL<br>*/
    public String getGrzDtIniValTarNull() {
        return readString(Pos.GRZ_DT_INI_VAL_TAR_NULL, Len.GRZ_DT_INI_VAL_TAR_NULL);
    }

    public String getGrzDtIniValTarNullFormatted() {
        return Functions.padBlanks(getGrzDtIniValTarNull(), Len.GRZ_DT_INI_VAL_TAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_DT_INI_VAL_TAR = 1;
        public static final int GRZ_DT_INI_VAL_TAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_DT_INI_VAL_TAR = 5;
        public static final int GRZ_DT_INI_VAL_TAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_DT_INI_VAL_TAR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
