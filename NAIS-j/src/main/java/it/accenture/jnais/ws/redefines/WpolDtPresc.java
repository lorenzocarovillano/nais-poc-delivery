package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WPOL-DT-PRESC<br>
 * Variable: WPOL-DT-PRESC from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDtPresc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDtPresc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DT_PRESC;
    }

    public void setWpolDtPresc(int wpolDtPresc) {
        writeIntAsPacked(Pos.WPOL_DT_PRESC, wpolDtPresc, Len.Int.WPOL_DT_PRESC);
    }

    public void setWpolDtPrescFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DT_PRESC, Pos.WPOL_DT_PRESC);
    }

    /**Original name: WPOL-DT-PRESC<br>*/
    public int getWpolDtPresc() {
        return readPackedAsInt(Pos.WPOL_DT_PRESC, Len.Int.WPOL_DT_PRESC);
    }

    public byte[] getWpolDtPrescAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DT_PRESC, Pos.WPOL_DT_PRESC);
        return buffer;
    }

    public void setWpolDtPrescNull(String wpolDtPrescNull) {
        writeString(Pos.WPOL_DT_PRESC_NULL, wpolDtPrescNull, Len.WPOL_DT_PRESC_NULL);
    }

    /**Original name: WPOL-DT-PRESC-NULL<br>*/
    public String getWpolDtPrescNull() {
        return readString(Pos.WPOL_DT_PRESC_NULL, Len.WPOL_DT_PRESC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DT_PRESC = 1;
        public static final int WPOL_DT_PRESC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DT_PRESC = 5;
        public static final int WPOL_DT_PRESC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DT_PRESC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
