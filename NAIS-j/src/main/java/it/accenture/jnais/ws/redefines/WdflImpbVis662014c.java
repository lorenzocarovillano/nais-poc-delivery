package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-VIS-662014C<br>
 * Variable: WDFL-IMPB-VIS-662014C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbVis662014c extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbVis662014c() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_VIS662014C;
    }

    public void setWdflImpbVis662014c(AfDecimal wdflImpbVis662014c) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_VIS662014C, wdflImpbVis662014c.copy());
    }

    public void setWdflImpbVis662014cFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_VIS662014C, Pos.WDFL_IMPB_VIS662014C);
    }

    /**Original name: WDFL-IMPB-VIS-662014C<br>*/
    public AfDecimal getWdflImpbVis662014c() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_VIS662014C, Len.Int.WDFL_IMPB_VIS662014C, Len.Fract.WDFL_IMPB_VIS662014C);
    }

    public byte[] getWdflImpbVis662014cAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_VIS662014C, Pos.WDFL_IMPB_VIS662014C);
        return buffer;
    }

    public void setWdflImpbVis662014cNull(String wdflImpbVis662014cNull) {
        writeString(Pos.WDFL_IMPB_VIS662014C_NULL, wdflImpbVis662014cNull, Len.WDFL_IMPB_VIS662014C_NULL);
    }

    /**Original name: WDFL-IMPB-VIS-662014C-NULL<br>*/
    public String getWdflImpbVis662014cNull() {
        return readString(Pos.WDFL_IMPB_VIS662014C_NULL, Len.WDFL_IMPB_VIS662014C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_VIS662014C = 1;
        public static final int WDFL_IMPB_VIS662014C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_VIS662014C = 8;
        public static final int WDFL_IMPB_VIS662014C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_VIS662014C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_VIS662014C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
