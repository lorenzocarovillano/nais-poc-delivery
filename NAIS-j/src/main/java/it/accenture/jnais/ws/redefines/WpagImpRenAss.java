package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-REN-ASS<br>
 * Variable: WPAG-IMP-REN-ASS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpRenAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpRenAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_REN_ASS;
    }

    public void setWpagImpRenAss(AfDecimal wpagImpRenAss) {
        writeDecimalAsPacked(Pos.WPAG_IMP_REN_ASS, wpagImpRenAss.copy());
    }

    public void setWpagImpRenAssFormatted(String wpagImpRenAss) {
        setWpagImpRenAss(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_REN_ASS + Len.Fract.WPAG_IMP_REN_ASS, Len.Fract.WPAG_IMP_REN_ASS, wpagImpRenAss));
    }

    public void setWpagImpRenAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_REN_ASS, Pos.WPAG_IMP_REN_ASS);
    }

    /**Original name: WPAG-IMP-REN-ASS<br>*/
    public AfDecimal getWpagImpRenAss() {
        return readPackedAsDecimal(Pos.WPAG_IMP_REN_ASS, Len.Int.WPAG_IMP_REN_ASS, Len.Fract.WPAG_IMP_REN_ASS);
    }

    public byte[] getWpagImpRenAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_REN_ASS, Pos.WPAG_IMP_REN_ASS);
        return buffer;
    }

    public void initWpagImpRenAssSpaces() {
        fill(Pos.WPAG_IMP_REN_ASS, Len.WPAG_IMP_REN_ASS, Types.SPACE_CHAR);
    }

    public void setWpagImpRenAssNull(String wpagImpRenAssNull) {
        writeString(Pos.WPAG_IMP_REN_ASS_NULL, wpagImpRenAssNull, Len.WPAG_IMP_REN_ASS_NULL);
    }

    /**Original name: WPAG-IMP-REN-ASS-NULL<br>*/
    public String getWpagImpRenAssNull() {
        return readString(Pos.WPAG_IMP_REN_ASS_NULL, Len.WPAG_IMP_REN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_REN_ASS = 1;
        public static final int WPAG_IMP_REN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_REN_ASS = 8;
        public static final int WPAG_IMP_REN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_REN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_REN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
