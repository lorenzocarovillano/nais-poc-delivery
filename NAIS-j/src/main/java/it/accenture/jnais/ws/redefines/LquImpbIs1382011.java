package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-IS-1382011<br>
 * Variable: LQU-IMPB-IS-1382011 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbIs1382011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbIs1382011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_IS1382011;
    }

    public void setLquImpbIs1382011(AfDecimal lquImpbIs1382011) {
        writeDecimalAsPacked(Pos.LQU_IMPB_IS1382011, lquImpbIs1382011.copy());
    }

    public void setLquImpbIs1382011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_IS1382011, Pos.LQU_IMPB_IS1382011);
    }

    /**Original name: LQU-IMPB-IS-1382011<br>*/
    public AfDecimal getLquImpbIs1382011() {
        return readPackedAsDecimal(Pos.LQU_IMPB_IS1382011, Len.Int.LQU_IMPB_IS1382011, Len.Fract.LQU_IMPB_IS1382011);
    }

    public byte[] getLquImpbIs1382011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_IS1382011, Pos.LQU_IMPB_IS1382011);
        return buffer;
    }

    public void setLquImpbIs1382011Null(String lquImpbIs1382011Null) {
        writeString(Pos.LQU_IMPB_IS1382011_NULL, lquImpbIs1382011Null, Len.LQU_IMPB_IS1382011_NULL);
    }

    /**Original name: LQU-IMPB-IS-1382011-NULL<br>*/
    public String getLquImpbIs1382011Null() {
        return readString(Pos.LQU_IMPB_IS1382011_NULL, Len.LQU_IMPB_IS1382011_NULL);
    }

    public String getLquImpbIs1382011NullFormatted() {
        return Functions.padBlanks(getLquImpbIs1382011Null(), Len.LQU_IMPB_IS1382011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_IS1382011 = 1;
        public static final int LQU_IMPB_IS1382011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_IS1382011 = 8;
        public static final int LQU_IMPB_IS1382011_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_IS1382011 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_IS1382011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
