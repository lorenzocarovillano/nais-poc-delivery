package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: LDBV1351-TP-CAUS-TAB<br>
 * Variable: LDBV1351-TP-CAUS-TAB from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ldbv1351TpCausTab extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELE_MAXOCCURS = 7;

    //==== CONSTRUCTORS ====
    public Ldbv1351TpCausTab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV1351_TP_CAUS_TAB;
    }

    public void setLdbv1351TpCausTabBytes(byte[] buffer) {
        setLdbv1351TpCausTabBytes(buffer, 1);
    }

    public void setLdbv1351TpCausTabBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LDBV1351_TP_CAUS_TAB, Pos.LDBV1351_TP_CAUS_TAB);
    }

    public byte[] getLdbv1351TpCausTabBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LDBV1351_TP_CAUS_TAB, Pos.LDBV1351_TP_CAUS_TAB);
        return buffer;
    }

    public void setLdbv1361TpCaus(int ldbv1361TpCausIdx, String ldbv1361TpCaus) {
        int position = Pos.ldbv1351TpCaus(ldbv1361TpCausIdx - 1);
        writeString(position, ldbv1361TpCaus, Len.S);
    }

    /**Original name: LDBV1361-TP-CAUS<br>*/
    public String getLdbv1361TpCaus(int ldbv1361TpCausIdx) {
        int position = Pos.ldbv1351TpCaus(ldbv1361TpCausIdx - 1);
        return readString(position, Len.S);
    }

    public void setCaus01(String caus01) {
        writeString(Pos.CAUS01, caus01, Len.CAUS01);
    }

    /**Original name: LDBV1351-TP-CAUS-01<br>*/
    public String getCaus01() {
        return readString(Pos.CAUS01, Len.CAUS01);
    }

    public void setCaus02(String caus02) {
        writeString(Pos.CAUS02, caus02, Len.CAUS02);
    }

    /**Original name: LDBV1351-TP-CAUS-02<br>*/
    public String getCaus02() {
        return readString(Pos.CAUS02, Len.CAUS02);
    }

    public void setCaus03(String caus03) {
        writeString(Pos.CAUS03, caus03, Len.CAUS03);
    }

    /**Original name: LDBV1351-TP-CAUS-03<br>*/
    public String getCaus03() {
        return readString(Pos.CAUS03, Len.CAUS03);
    }

    public void setCaus04(String caus04) {
        writeString(Pos.CAUS04, caus04, Len.CAUS04);
    }

    /**Original name: LDBV1351-TP-CAUS-04<br>*/
    public String getCaus04() {
        return readString(Pos.CAUS04, Len.CAUS04);
    }

    public void setCaus05(String caus05) {
        writeString(Pos.CAUS05, caus05, Len.CAUS05);
    }

    /**Original name: LDBV1351-TP-CAUS-05<br>*/
    public String getCaus05() {
        return readString(Pos.CAUS05, Len.CAUS05);
    }

    public void setCaus06(String caus06) {
        writeString(Pos.CAUS06, caus06, Len.CAUS06);
    }

    /**Original name: LDBV1351-TP-CAUS-06<br>*/
    public String getCaus06() {
        return readString(Pos.CAUS06, Len.CAUS06);
    }

    public void setCaus07(String caus07) {
        writeString(Pos.CAUS07, caus07, Len.CAUS07);
    }

    /**Original name: LDBV1351-TP-CAUS-07<br>*/
    public String getCaus07() {
        return readString(Pos.CAUS07, Len.CAUS07);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LDBV1351_TP_CAUS_TAB = 1;
        public static final int LDBV1351_TP_CAUS_ELE_EST = 1;
        public static final int CAUS01 = LDBV1351_TP_CAUS_ELE_EST;
        public static final int CAUS02 = CAUS01 + Len.CAUS01;
        public static final int CAUS03 = CAUS02 + Len.CAUS02;
        public static final int CAUS04 = CAUS03 + Len.CAUS03;
        public static final int CAUS05 = CAUS04 + Len.CAUS04;
        public static final int CAUS06 = CAUS05 + Len.CAUS05;
        public static final int CAUS07 = CAUS06 + Len.CAUS06;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int ldbv1351TpCausEle(int idx) {
            return LDBV1351_TP_CAUS_TAB + idx * Len.ELE;
        }

        public static int ldbv1351TpCaus(int idx) {
            return ldbv1351TpCausEle(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S = 2;
        public static final int ELE = S;
        public static final int CAUS01 = 2;
        public static final int CAUS02 = 2;
        public static final int CAUS03 = 2;
        public static final int CAUS04 = 2;
        public static final int CAUS05 = 2;
        public static final int CAUS06 = 2;
        public static final int LDBV1351_TP_CAUS_TAB = Ldbv1351TpCausTab.ELE_MAXOCCURS * ELE;
        public static final int CAUS07 = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
