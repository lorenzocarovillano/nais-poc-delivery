package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-PARTITA-IVA<br>
 * Variable: FLAG-PARTITA-IVA from program LVVS2720<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagPartitaIva {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagPartitaIva(char flagPartitaIva) {
        this.value = flagPartitaIva;
    }

    public char getFlagPartitaIva() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
