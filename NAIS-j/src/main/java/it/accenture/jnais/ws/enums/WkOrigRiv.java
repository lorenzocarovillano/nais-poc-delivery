package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-ORIG-RIV<br>
 * Variable: WK-ORIG-RIV from program LOAS0310<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkOrigRiv {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_ORIG_RIV);
    public static final String IN_FLD = "IN";
    public static final String EM = "EM";

    //==== METHODS ====
    public void setWkOrigRiv(String wkOrigRiv) {
        this.value = Functions.subString(wkOrigRiv, Len.WK_ORIG_RIV);
    }

    public String getWkOrigRiv() {
        return this.value;
    }

    public boolean isInFld() {
        return value.equals(IN_FLD);
    }

    public void setInFld() {
        value = IN_FLD;
    }

    public void setEm() {
        value = EM;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_ORIG_RIV = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
