package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-DT-LIQ<br>
 * Variable: LQU-DT-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquDtLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquDtLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_DT_LIQ;
    }

    public void setLquDtLiq(int lquDtLiq) {
        writeIntAsPacked(Pos.LQU_DT_LIQ, lquDtLiq, Len.Int.LQU_DT_LIQ);
    }

    public void setLquDtLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_DT_LIQ, Pos.LQU_DT_LIQ);
    }

    /**Original name: LQU-DT-LIQ<br>*/
    public int getLquDtLiq() {
        return readPackedAsInt(Pos.LQU_DT_LIQ, Len.Int.LQU_DT_LIQ);
    }

    public byte[] getLquDtLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_DT_LIQ, Pos.LQU_DT_LIQ);
        return buffer;
    }

    public void setLquDtLiqNull(String lquDtLiqNull) {
        writeString(Pos.LQU_DT_LIQ_NULL, lquDtLiqNull, Len.LQU_DT_LIQ_NULL);
    }

    /**Original name: LQU-DT-LIQ-NULL<br>*/
    public String getLquDtLiqNull() {
        return readString(Pos.LQU_DT_LIQ_NULL, Len.LQU_DT_LIQ_NULL);
    }

    public String getLquDtLiqNullFormatted() {
        return Functions.padBlanks(getLquDtLiqNull(), Len.LQU_DT_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_DT_LIQ = 1;
        public static final int LQU_DT_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_DT_LIQ = 5;
        public static final int LQU_DT_LIQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_DT_LIQ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
