package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-INTR-PREST-CALC<br>
 * Variable: DFL-INTR-PREST-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIntrPrestCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIntrPrestCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_INTR_PREST_CALC;
    }

    public void setDflIntrPrestCalc(AfDecimal dflIntrPrestCalc) {
        writeDecimalAsPacked(Pos.DFL_INTR_PREST_CALC, dflIntrPrestCalc.copy());
    }

    public void setDflIntrPrestCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_INTR_PREST_CALC, Pos.DFL_INTR_PREST_CALC);
    }

    /**Original name: DFL-INTR-PREST-CALC<br>*/
    public AfDecimal getDflIntrPrestCalc() {
        return readPackedAsDecimal(Pos.DFL_INTR_PREST_CALC, Len.Int.DFL_INTR_PREST_CALC, Len.Fract.DFL_INTR_PREST_CALC);
    }

    public byte[] getDflIntrPrestCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_INTR_PREST_CALC, Pos.DFL_INTR_PREST_CALC);
        return buffer;
    }

    public void setDflIntrPrestCalcNull(String dflIntrPrestCalcNull) {
        writeString(Pos.DFL_INTR_PREST_CALC_NULL, dflIntrPrestCalcNull, Len.DFL_INTR_PREST_CALC_NULL);
    }

    /**Original name: DFL-INTR-PREST-CALC-NULL<br>*/
    public String getDflIntrPrestCalcNull() {
        return readString(Pos.DFL_INTR_PREST_CALC_NULL, Len.DFL_INTR_PREST_CALC_NULL);
    }

    public String getDflIntrPrestCalcNullFormatted() {
        return Functions.padBlanks(getDflIntrPrestCalcNull(), Len.DFL_INTR_PREST_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_INTR_PREST_CALC = 1;
        public static final int DFL_INTR_PREST_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_INTR_PREST_CALC = 8;
        public static final int DFL_INTR_PREST_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_INTR_PREST_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_INTR_PREST_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
