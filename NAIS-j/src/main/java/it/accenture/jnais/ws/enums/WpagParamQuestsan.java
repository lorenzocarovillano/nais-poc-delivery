package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WPAG-PARAM-QUESTSAN<br>
 * Variable: WPAG-PARAM-QUESTSAN from copybook LVEC0268<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpagParamQuestsan {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char PARAM_QUESTSAN_SI = 'S';
    public static final char PARAM_QUESTSAN_NO = 'N';
    public static final char VISITA_MEDICA_OBB = 'P';

    //==== METHODS ====
    public void setWpagParamQuestsan(char wpagParamQuestsan) {
        this.value = wpagParamQuestsan;
    }

    public char getWpagParamQuestsan() {
        return this.value;
    }

    public void setParamQuestsanSi() {
        value = PARAM_QUESTSAN_SI;
    }

    public void setParamQuestsanNo() {
        value = PARAM_QUESTSAN_NO;
    }

    public void setVisitaMedicaObb() {
        value = VISITA_MEDICA_OBB;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PARAM_QUESTSAN = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
