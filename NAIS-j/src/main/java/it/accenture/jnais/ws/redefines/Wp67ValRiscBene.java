package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-VAL-RISC-BENE<br>
 * Variable: WP67-VAL-RISC-BENE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67ValRiscBene extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67ValRiscBene() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_VAL_RISC_BENE;
    }

    public void setWp67ValRiscBene(AfDecimal wp67ValRiscBene) {
        writeDecimalAsPacked(Pos.WP67_VAL_RISC_BENE, wp67ValRiscBene.copy());
    }

    public void setWp67ValRiscBeneFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_VAL_RISC_BENE, Pos.WP67_VAL_RISC_BENE);
    }

    /**Original name: WP67-VAL-RISC-BENE<br>*/
    public AfDecimal getWp67ValRiscBene() {
        return readPackedAsDecimal(Pos.WP67_VAL_RISC_BENE, Len.Int.WP67_VAL_RISC_BENE, Len.Fract.WP67_VAL_RISC_BENE);
    }

    public byte[] getWp67ValRiscBeneAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_VAL_RISC_BENE, Pos.WP67_VAL_RISC_BENE);
        return buffer;
    }

    public void setWp67ValRiscBeneNull(String wp67ValRiscBeneNull) {
        writeString(Pos.WP67_VAL_RISC_BENE_NULL, wp67ValRiscBeneNull, Len.WP67_VAL_RISC_BENE_NULL);
    }

    /**Original name: WP67-VAL-RISC-BENE-NULL<br>*/
    public String getWp67ValRiscBeneNull() {
        return readString(Pos.WP67_VAL_RISC_BENE_NULL, Len.WP67_VAL_RISC_BENE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_VAL_RISC_BENE = 1;
        public static final int WP67_VAL_RISC_BENE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_VAL_RISC_BENE = 8;
        public static final int WP67_VAL_RISC_BENE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_VAL_RISC_BENE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_VAL_RISC_BENE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
