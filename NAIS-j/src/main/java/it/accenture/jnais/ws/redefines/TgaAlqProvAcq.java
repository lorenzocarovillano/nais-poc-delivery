package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ALQ-PROV-ACQ<br>
 * Variable: TGA-ALQ-PROV-ACQ from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaAlqProvAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaAlqProvAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ALQ_PROV_ACQ;
    }

    public void setTgaAlqProvAcq(AfDecimal tgaAlqProvAcq) {
        writeDecimalAsPacked(Pos.TGA_ALQ_PROV_ACQ, tgaAlqProvAcq.copy());
    }

    public void setTgaAlqProvAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ALQ_PROV_ACQ, Pos.TGA_ALQ_PROV_ACQ);
    }

    /**Original name: TGA-ALQ-PROV-ACQ<br>*/
    public AfDecimal getTgaAlqProvAcq() {
        return readPackedAsDecimal(Pos.TGA_ALQ_PROV_ACQ, Len.Int.TGA_ALQ_PROV_ACQ, Len.Fract.TGA_ALQ_PROV_ACQ);
    }

    public byte[] getTgaAlqProvAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ALQ_PROV_ACQ, Pos.TGA_ALQ_PROV_ACQ);
        return buffer;
    }

    public void setTgaAlqProvAcqNull(String tgaAlqProvAcqNull) {
        writeString(Pos.TGA_ALQ_PROV_ACQ_NULL, tgaAlqProvAcqNull, Len.TGA_ALQ_PROV_ACQ_NULL);
    }

    /**Original name: TGA-ALQ-PROV-ACQ-NULL<br>*/
    public String getTgaAlqProvAcqNull() {
        return readString(Pos.TGA_ALQ_PROV_ACQ_NULL, Len.TGA_ALQ_PROV_ACQ_NULL);
    }

    public String getTgaAlqProvAcqNullFormatted() {
        return Functions.padBlanks(getTgaAlqProvAcqNull(), Len.TGA_ALQ_PROV_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_PROV_ACQ = 1;
        public static final int TGA_ALQ_PROV_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_PROV_ACQ = 4;
        public static final int TGA_ALQ_PROV_ACQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
