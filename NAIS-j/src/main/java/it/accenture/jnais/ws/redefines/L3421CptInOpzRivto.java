package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-CPT-IN-OPZ-RIVTO<br>
 * Variable: L3421-CPT-IN-OPZ-RIVTO from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421CptInOpzRivto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421CptInOpzRivto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_CPT_IN_OPZ_RIVTO;
    }

    public void setL3421CptInOpzRivtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_CPT_IN_OPZ_RIVTO, Pos.L3421_CPT_IN_OPZ_RIVTO);
    }

    /**Original name: L3421-CPT-IN-OPZ-RIVTO<br>*/
    public AfDecimal getL3421CptInOpzRivto() {
        return readPackedAsDecimal(Pos.L3421_CPT_IN_OPZ_RIVTO, Len.Int.L3421_CPT_IN_OPZ_RIVTO, Len.Fract.L3421_CPT_IN_OPZ_RIVTO);
    }

    public byte[] getL3421CptInOpzRivtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_CPT_IN_OPZ_RIVTO, Pos.L3421_CPT_IN_OPZ_RIVTO);
        return buffer;
    }

    /**Original name: L3421-CPT-IN-OPZ-RIVTO-NULL<br>*/
    public String getL3421CptInOpzRivtoNull() {
        return readString(Pos.L3421_CPT_IN_OPZ_RIVTO_NULL, Len.L3421_CPT_IN_OPZ_RIVTO_NULL);
    }

    public String getL3421CptInOpzRivtoNullFormatted() {
        return Functions.padBlanks(getL3421CptInOpzRivtoNull(), Len.L3421_CPT_IN_OPZ_RIVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_CPT_IN_OPZ_RIVTO = 1;
        public static final int L3421_CPT_IN_OPZ_RIVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_CPT_IN_OPZ_RIVTO = 8;
        public static final int L3421_CPT_IN_OPZ_RIVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_CPT_IN_OPZ_RIVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_CPT_IN_OPZ_RIVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
