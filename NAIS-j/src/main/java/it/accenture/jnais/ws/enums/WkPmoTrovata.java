package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-PMO-TROVATA<br>
 * Variable: WK-PMO-TROVATA from program LOAS0870<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkPmoTrovata {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkPmoTrovata(char wkPmoTrovata) {
        this.value = wkPmoTrovata;
    }

    public char getWkPmoTrovata() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
