package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: MBS-ID-BATCH<br>
 * Variable: MBS-ID-BATCH from program LCCS0024<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MbsIdBatch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MbsIdBatch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MBS_ID_BATCH;
    }

    public void setMbsIdBatch(int mbsIdBatch) {
        writeIntAsPacked(Pos.MBS_ID_BATCH, mbsIdBatch, Len.Int.MBS_ID_BATCH);
    }

    public void setMbsIdBatchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MBS_ID_BATCH, Pos.MBS_ID_BATCH);
    }

    /**Original name: MBS-ID-BATCH<br>*/
    public int getMbsIdBatch() {
        return readPackedAsInt(Pos.MBS_ID_BATCH, Len.Int.MBS_ID_BATCH);
    }

    public byte[] getMbsIdBatchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MBS_ID_BATCH, Pos.MBS_ID_BATCH);
        return buffer;
    }

    public void setMbsIdBatchNull(String mbsIdBatchNull) {
        writeString(Pos.MBS_ID_BATCH_NULL, mbsIdBatchNull, Len.MBS_ID_BATCH_NULL);
    }

    /**Original name: MBS-ID-BATCH-NULL<br>*/
    public String getMbsIdBatchNull() {
        return readString(Pos.MBS_ID_BATCH_NULL, Len.MBS_ID_BATCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MBS_ID_BATCH = 1;
        public static final int MBS_ID_BATCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MBS_ID_BATCH = 5;
        public static final int MBS_ID_BATCH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MBS_ID_BATCH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
