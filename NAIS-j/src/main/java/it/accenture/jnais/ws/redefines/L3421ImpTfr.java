package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-TFR<br>
 * Variable: L3421-IMP-TFR from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_TFR;
    }

    public void setL3421ImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_TFR, Pos.L3421_IMP_TFR);
    }

    /**Original name: L3421-IMP-TFR<br>*/
    public AfDecimal getL3421ImpTfr() {
        return readPackedAsDecimal(Pos.L3421_IMP_TFR, Len.Int.L3421_IMP_TFR, Len.Fract.L3421_IMP_TFR);
    }

    public byte[] getL3421ImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_TFR, Pos.L3421_IMP_TFR);
        return buffer;
    }

    /**Original name: L3421-IMP-TFR-NULL<br>*/
    public String getL3421ImpTfrNull() {
        return readString(Pos.L3421_IMP_TFR_NULL, Len.L3421_IMP_TFR_NULL);
    }

    public String getL3421ImpTfrNullFormatted() {
        return Functions.padBlanks(getL3421ImpTfrNull(), Len.L3421_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_TFR = 1;
        public static final int L3421_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_TFR = 8;
        public static final int L3421_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
