package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-FONDO-TROVATO<br>
 * Variable: FLAG-FONDO-TROVATO from program LVVS0560<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagFondoTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char NO = 'N';
    public static final char SI = 'S';

    //==== METHODS ====
    public void setFlagFondoTrovato(char flagFondoTrovato) {
        this.value = flagFondoTrovato;
    }

    public char getFlagFondoTrovato() {
        return this.value;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }
}
