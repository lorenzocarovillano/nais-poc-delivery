package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPB-IMPST-PRVR<br>
 * Variable: TCL-IMPB-IMPST-PRVR from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpbImpstPrvr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpbImpstPrvr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPB_IMPST_PRVR;
    }

    public void setTclImpbImpstPrvr(AfDecimal tclImpbImpstPrvr) {
        writeDecimalAsPacked(Pos.TCL_IMPB_IMPST_PRVR, tclImpbImpstPrvr.copy());
    }

    public void setTclImpbImpstPrvrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPB_IMPST_PRVR, Pos.TCL_IMPB_IMPST_PRVR);
    }

    /**Original name: TCL-IMPB-IMPST-PRVR<br>*/
    public AfDecimal getTclImpbImpstPrvr() {
        return readPackedAsDecimal(Pos.TCL_IMPB_IMPST_PRVR, Len.Int.TCL_IMPB_IMPST_PRVR, Len.Fract.TCL_IMPB_IMPST_PRVR);
    }

    public byte[] getTclImpbImpstPrvrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPB_IMPST_PRVR, Pos.TCL_IMPB_IMPST_PRVR);
        return buffer;
    }

    public void setTclImpbImpstPrvrNull(String tclImpbImpstPrvrNull) {
        writeString(Pos.TCL_IMPB_IMPST_PRVR_NULL, tclImpbImpstPrvrNull, Len.TCL_IMPB_IMPST_PRVR_NULL);
    }

    /**Original name: TCL-IMPB-IMPST-PRVR-NULL<br>*/
    public String getTclImpbImpstPrvrNull() {
        return readString(Pos.TCL_IMPB_IMPST_PRVR_NULL, Len.TCL_IMPB_IMPST_PRVR_NULL);
    }

    public String getTclImpbImpstPrvrNullFormatted() {
        return Functions.padBlanks(getTclImpbImpstPrvrNull(), Len.TCL_IMPB_IMPST_PRVR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_IMPST_PRVR = 1;
        public static final int TCL_IMPB_IMPST_PRVR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_IMPST_PRVR = 8;
        public static final int TCL_IMPB_IMPST_PRVR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_IMPST_PRVR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_IMPST_PRVR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
