package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: DISTINCT-TAB-INFO<br>
 * Variables: DISTINCT-TAB-INFO from program IVVS0211<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class DistinctTabInfo {

    //==== PROPERTIES ====
    //Original name: DISTINCT-DT-INI-VAL-TAR
    private int distinctDtIniValTar = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setDistinctDtIniValTar(int distinctDtIniValTar) {
        this.distinctDtIniValTar = distinctDtIniValTar;
    }

    public int getDistinctDtIniValTar() {
        return this.distinctDtIniValTar;
    }
}
