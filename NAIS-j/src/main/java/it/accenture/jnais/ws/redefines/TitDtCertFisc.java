package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-DT-CERT-FISC<br>
 * Variable: TIT-DT-CERT-FISC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitDtCertFisc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitDtCertFisc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_DT_CERT_FISC;
    }

    public void setTitDtCertFisc(int titDtCertFisc) {
        writeIntAsPacked(Pos.TIT_DT_CERT_FISC, titDtCertFisc, Len.Int.TIT_DT_CERT_FISC);
    }

    public void setTitDtCertFiscFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_DT_CERT_FISC, Pos.TIT_DT_CERT_FISC);
    }

    /**Original name: TIT-DT-CERT-FISC<br>*/
    public int getTitDtCertFisc() {
        return readPackedAsInt(Pos.TIT_DT_CERT_FISC, Len.Int.TIT_DT_CERT_FISC);
    }

    public byte[] getTitDtCertFiscAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_DT_CERT_FISC, Pos.TIT_DT_CERT_FISC);
        return buffer;
    }

    public void setTitDtCertFiscNull(String titDtCertFiscNull) {
        writeString(Pos.TIT_DT_CERT_FISC_NULL, titDtCertFiscNull, Len.TIT_DT_CERT_FISC_NULL);
    }

    /**Original name: TIT-DT-CERT-FISC-NULL<br>*/
    public String getTitDtCertFiscNull() {
        return readString(Pos.TIT_DT_CERT_FISC_NULL, Len.TIT_DT_CERT_FISC_NULL);
    }

    public String getTitDtCertFiscNullFormatted() {
        return Functions.padBlanks(getTitDtCertFiscNull(), Len.TIT_DT_CERT_FISC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_DT_CERT_FISC = 1;
        public static final int TIT_DT_CERT_FISC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_DT_CERT_FISC = 5;
        public static final int TIT_DT_CERT_FISC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_DT_CERT_FISC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
