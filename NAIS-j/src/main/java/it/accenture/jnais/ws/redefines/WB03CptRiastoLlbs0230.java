package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-CPT-RIASTO<br>
 * Variable: W-B03-CPT-RIASTO from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CptRiastoLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CptRiastoLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_CPT_RIASTO;
    }

    public void setwB03CptRiasto(AfDecimal wB03CptRiasto) {
        writeDecimalAsPacked(Pos.W_B03_CPT_RIASTO, wB03CptRiasto.copy());
    }

    /**Original name: W-B03-CPT-RIASTO<br>*/
    public AfDecimal getwB03CptRiasto() {
        return readPackedAsDecimal(Pos.W_B03_CPT_RIASTO, Len.Int.W_B03_CPT_RIASTO, Len.Fract.W_B03_CPT_RIASTO);
    }

    public byte[] getwB03CptRiastoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_CPT_RIASTO, Pos.W_B03_CPT_RIASTO);
        return buffer;
    }

    public void setwB03CptRiastoNull(String wB03CptRiastoNull) {
        writeString(Pos.W_B03_CPT_RIASTO_NULL, wB03CptRiastoNull, Len.W_B03_CPT_RIASTO_NULL);
    }

    /**Original name: W-B03-CPT-RIASTO-NULL<br>*/
    public String getwB03CptRiastoNull() {
        return readString(Pos.W_B03_CPT_RIASTO_NULL, Len.W_B03_CPT_RIASTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_CPT_RIASTO = 1;
        public static final int W_B03_CPT_RIASTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_CPT_RIASTO = 8;
        public static final int W_B03_CPT_RIASTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_CPT_RIASTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_CPT_RIASTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
