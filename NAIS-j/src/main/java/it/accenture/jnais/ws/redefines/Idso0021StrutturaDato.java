package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: IDSO0021-STRUTTURA-DATO<br>
 * Variable: IDSO0021-STRUTTURA-DATO from program IDSS0020<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Idso0021StrutturaDato extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELEMENTS_STR_DATO_MAXOCCURS = 250;

    //==== CONSTRUCTORS ====
    public Idso0021StrutturaDato() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IDSO0021_STRUTTURA_DATO;
    }

    public String getIdso0021StrutturaDatoFormatted() {
        return readFixedString(Pos.IDSO0021_STRUTTURA_DATO, Len.IDSO0021_STRUTTURA_DATO);
    }

    /**Original name: IDSO0021-STRUTTURA-DATO<br>*/
    public byte[] getIdso0021StrutturaDatoBytes() {
        byte[] buffer = new byte[Len.IDSO0021_STRUTTURA_DATO];
        return getIdso0021StrutturaDatoBytes(buffer, 1);
    }

    public void setIdso0021StrutturaDatoBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.IDSO0021_STRUTTURA_DATO, Pos.IDSO0021_STRUTTURA_DATO);
    }

    public byte[] getIdso0021StrutturaDatoBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.IDSO0021_STRUTTURA_DATO, Pos.IDSO0021_STRUTTURA_DATO);
        return buffer;
    }

    public void initIdso0021StrutturaDatoHighValues() {
        fill(Pos.IDSO0021_STRUTTURA_DATO, Len.IDSO0021_STRUTTURA_DATO, Types.HIGH_CHAR_VAL);
    }

    public void initIdso0021StrutturaDatoSpaces() {
        fill(Pos.IDSO0021_STRUTTURA_DATO, Len.IDSO0021_STRUTTURA_DATO, Types.SPACE_CHAR);
    }

    public void setElementsStrDatoBytes(int elementsStrDatoIdx, byte[] buffer) {
        setElementsStrDatoBytes(elementsStrDatoIdx, buffer, 1);
    }

    public void setElementsStrDatoBytes(int elementsStrDatoIdx, byte[] buffer, int offset) {
        int position = Pos.idso0021ElementsStrDato(elementsStrDatoIdx - 1);
        setBytes(buffer, offset, Len.ELEMENTS_STR_DATO, position);
    }

    public void setCodiceDato(int codiceDatoIdx, String codiceDato) {
        int position = Pos.idso0021CodiceDato(codiceDatoIdx - 1);
        writeString(position, codiceDato, Len.CODICE_DATO);
    }

    /**Original name: IDSO0021-CODICE-DATO<br>*/
    public String getCodiceDato(int codiceDatoIdx) {
        int position = Pos.idso0021CodiceDato(codiceDatoIdx - 1);
        return readString(position, Len.CODICE_DATO);
    }

    public void setFlagKey(int flagKeyIdx, char flagKey) {
        int position = Pos.idso0021FlagKey(flagKeyIdx - 1);
        writeChar(position, flagKey);
    }

    /**Original name: IDSO0021-FLAG-KEY<br>*/
    public char getFlagKey(int flagKeyIdx) {
        int position = Pos.idso0021FlagKey(flagKeyIdx - 1);
        return readChar(position);
    }

    public void setFlagReturnCode(int flagReturnCodeIdx, char flagReturnCode) {
        int position = Pos.idso0021FlagReturnCode(flagReturnCodeIdx - 1);
        writeChar(position, flagReturnCode);
    }

    /**Original name: IDSO0021-FLAG-RETURN-CODE<br>*/
    public char getFlagReturnCode(int flagReturnCodeIdx) {
        int position = Pos.idso0021FlagReturnCode(flagReturnCodeIdx - 1);
        return readChar(position);
    }

    public void setFlagCallUsing(int flagCallUsingIdx, char flagCallUsing) {
        int position = Pos.idso0021FlagCallUsing(flagCallUsingIdx - 1);
        writeChar(position, flagCallUsing);
    }

    /**Original name: IDSO0021-FLAG-CALL-USING<br>*/
    public char getFlagCallUsing(int flagCallUsingIdx) {
        int position = Pos.idso0021FlagCallUsing(flagCallUsingIdx - 1);
        return readChar(position);
    }

    public void setFlagWhereCond(int flagWhereCondIdx, char flagWhereCond) {
        int position = Pos.idso0021FlagWhereCond(flagWhereCondIdx - 1);
        writeChar(position, flagWhereCond);
    }

    /**Original name: IDSO0021-FLAG-WHERE-COND<br>*/
    public char getFlagWhereCond(int flagWhereCondIdx) {
        int position = Pos.idso0021FlagWhereCond(flagWhereCondIdx - 1);
        return readChar(position);
    }

    public void setFlagRedefines(int flagRedefinesIdx, char flagRedefines) {
        int position = Pos.idso0021FlagRedefines(flagRedefinesIdx - 1);
        writeChar(position, flagRedefines);
    }

    /**Original name: IDSO0021-FLAG-REDEFINES<br>*/
    public char getFlagRedefines(int flagRedefinesIdx) {
        int position = Pos.idso0021FlagRedefines(flagRedefinesIdx - 1);
        return readChar(position);
    }

    public void setTipoDato(int tipoDatoIdx, String tipoDato) {
        int position = Pos.idso0021TipoDato(tipoDatoIdx - 1);
        writeString(position, tipoDato, Len.TIPO_DATO);
    }

    /**Original name: IDSO0021-TIPO-DATO<br>*/
    public String getTipoDato(int tipoDatoIdx) {
        int position = Pos.idso0021TipoDato(tipoDatoIdx - 1);
        return readString(position, Len.TIPO_DATO);
    }

    public void setInizioPosizione(int inizioPosizioneIdx, int inizioPosizione) {
        int position = Pos.idso0021InizioPosizione(inizioPosizioneIdx - 1);
        writeIntAsPacked(position, inizioPosizione, Len.Int.INIZIO_POSIZIONE);
    }

    /**Original name: IDSO0021-INIZIO-POSIZIONE<br>*/
    public int getInizioPosizione(int inizioPosizioneIdx) {
        int position = Pos.idso0021InizioPosizione(inizioPosizioneIdx - 1);
        return readPackedAsInt(position, Len.Int.INIZIO_POSIZIONE);
    }

    public void setLunghezzaDato(int lunghezzaDatoIdx, int lunghezzaDato) {
        int position = Pos.idso0021LunghezzaDato(lunghezzaDatoIdx - 1);
        writeIntAsPacked(position, lunghezzaDato, Len.Int.LUNGHEZZA_DATO);
    }

    /**Original name: IDSO0021-LUNGHEZZA-DATO<br>*/
    public int getLunghezzaDato(int lunghezzaDatoIdx) {
        int position = Pos.idso0021LunghezzaDato(lunghezzaDatoIdx - 1);
        return readPackedAsInt(position, Len.Int.LUNGHEZZA_DATO);
    }

    public void setLunghezzaDatoNom(int lunghezzaDatoNomIdx, int lunghezzaDatoNom) {
        int position = Pos.idso0021LunghezzaDatoNom(lunghezzaDatoNomIdx - 1);
        writeIntAsPacked(position, lunghezzaDatoNom, Len.Int.LUNGHEZZA_DATO_NOM);
    }

    /**Original name: IDSO0021-LUNGHEZZA-DATO-NOM<br>*/
    public int getLunghezzaDatoNom(int lunghezzaDatoNomIdx) {
        int position = Pos.idso0021LunghezzaDatoNom(lunghezzaDatoNomIdx - 1);
        return readPackedAsInt(position, Len.Int.LUNGHEZZA_DATO_NOM);
    }

    public void setPrecisioneDato(int precisioneDatoIdx, short precisioneDato) {
        int position = Pos.idso0021PrecisioneDato(precisioneDatoIdx - 1);
        writeShortAsPacked(position, precisioneDato, Len.Int.PRECISIONE_DATO);
    }

    /**Original name: IDSO0021-PRECISIONE-DATO<br>*/
    public short getPrecisioneDato(int precisioneDatoIdx) {
        int position = Pos.idso0021PrecisioneDato(precisioneDatoIdx - 1);
        return readPackedAsShort(position, Len.Int.PRECISIONE_DATO);
    }

    public void setFormattazioneDato(int formattazioneDatoIdx, String formattazioneDato) {
        int position = Pos.idso0021FormattazioneDato(formattazioneDatoIdx - 1);
        writeString(position, formattazioneDato, Len.FORMATTAZIONE_DATO);
    }

    /**Original name: IDSO0021-FORMATTAZIONE-DATO<br>*/
    public String getFormattazioneDato(int formattazioneDatoIdx) {
        int position = Pos.idso0021FormattazioneDato(formattazioneDatoIdx - 1);
        return readString(position, Len.FORMATTAZIONE_DATO);
    }

    public void setServConversione(int servConversioneIdx, String servConversione) {
        int position = Pos.idso0021ServConversione(servConversioneIdx - 1);
        writeString(position, servConversione, Len.SERV_CONVERSIONE);
    }

    /**Original name: IDSO0021-SERV-CONVERSIONE<br>*/
    public String getServConversione(int servConversioneIdx) {
        int position = Pos.idso0021ServConversione(servConversioneIdx - 1);
        return readString(position, Len.SERV_CONVERSIONE);
    }

    public void setAreaConvStandard(int areaConvStandardIdx, char areaConvStandard) {
        int position = Pos.idso0021AreaConvStandard(areaConvStandardIdx - 1);
        writeChar(position, areaConvStandard);
    }

    /**Original name: IDSO0021-AREA-CONV-STANDARD<br>*/
    public char getAreaConvStandard(int areaConvStandardIdx) {
        int position = Pos.idso0021AreaConvStandard(areaConvStandardIdx - 1);
        return readChar(position);
    }

    public void setCodiceDominio(int codiceDominioIdx, String codiceDominio) {
        int position = Pos.idso0021CodiceDominio(codiceDominioIdx - 1);
        writeString(position, codiceDominio, Len.CODICE_DOMINIO);
    }

    /**Original name: IDSO0021-CODICE-DOMINIO<br>*/
    public String getCodiceDominio(int codiceDominioIdx) {
        int position = Pos.idso0021CodiceDominio(codiceDominioIdx - 1);
        return readString(position, Len.CODICE_DOMINIO);
    }

    public void setIdso0021ServizioValidazione(int idso0021ServizioValidazioneIdx, String idso0021ServizioValidazione) {
        int position = Pos.idso0021ServizioValidazione(idso0021ServizioValidazioneIdx - 1);
        writeString(position, idso0021ServizioValidazione, Len.SERVIZIO_VALIDAZIONE);
    }

    /**Original name: IDSO0021-SERVIZIO-VALIDAZIONE<br>*/
    public String getIdso0021ServizioValidazione(int idso0021ServizioValidazioneIdx) {
        int position = Pos.idso0021ServizioValidazione(idso0021ServizioValidazioneIdx - 1);
        return readString(position, Len.SERVIZIO_VALIDAZIONE);
    }

    public void setRicorrenza(int ricorrenzaIdx, int ricorrenza) {
        int position = Pos.idso0021Ricorrenza(ricorrenzaIdx - 1);
        writeIntAsPacked(position, ricorrenza, Len.Int.RICORRENZA);
    }

    /**Original name: IDSO0021-RICORRENZA<br>*/
    public int getRicorrenza(int ricorrenzaIdx) {
        int position = Pos.idso0021Ricorrenza(ricorrenzaIdx - 1);
        return readPackedAsInt(position, Len.Int.RICORRENZA);
    }

    public void setClone(int cloneIdx, char clone) {
        int position = Pos.idso0021Clone(cloneIdx - 1);
        writeChar(position, clone);
    }

    /**Original name: IDSO0021-CLONE<br>*/
    public char getClone(int cloneIdx) {
        int position = Pos.idso0021Clone(cloneIdx - 1);
        return readChar(position);
    }

    public void setObbligatorieta(int obbligatorietaIdx, char obbligatorieta) {
        int position = Pos.idso0021Obbligatorieta(obbligatorietaIdx - 1);
        writeChar(position, obbligatorieta);
    }

    /**Original name: IDSO0021-OBBLIGATORIETA<br>*/
    public char getObbligatorieta(int obbligatorietaIdx) {
        int position = Pos.idso0021Obbligatorieta(obbligatorietaIdx - 1);
        return readChar(position);
    }

    public void setValoreDefault(int valoreDefaultIdx, String valoreDefault) {
        int position = Pos.idso0021ValoreDefault(valoreDefaultIdx - 1);
        writeString(position, valoreDefault, Len.VALORE_DEFAULT);
    }

    /**Original name: IDSO0021-VALORE-DEFAULT<br>*/
    public String getValoreDefault(int valoreDefaultIdx) {
        int position = Pos.idso0021ValoreDefault(valoreDefaultIdx - 1);
        return readString(position, Len.VALORE_DEFAULT);
    }

    public void setIdso0021RestoTab(String idso0021RestoTab) {
        writeString(Pos.RESTO_TAB, idso0021RestoTab, Len.IDSO0021_RESTO_TAB);
    }

    /**Original name: IDSO0021-RESTO-TAB<br>*/
    public String getIdso0021RestoTab() {
        return readString(Pos.RESTO_TAB, Len.IDSO0021_RESTO_TAB);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int IDSO0021_STRUTTURA_DATO = 1;
        public static final int IDSO0021_STRUTTURA_DATO_R = 1;
        public static final int FLR1 = IDSO0021_STRUTTURA_DATO_R;
        public static final int RESTO_TAB = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int idso0021ElementsStrDato(int idx) {
            return IDSO0021_STRUTTURA_DATO + idx * Len.ELEMENTS_STR_DATO;
        }

        public static int idso0021CodiceDato(int idx) {
            return idso0021ElementsStrDato(idx);
        }

        public static int idso0021FlagKey(int idx) {
            return idso0021CodiceDato(idx) + Len.CODICE_DATO;
        }

        public static int idso0021FlagReturnCode(int idx) {
            return idso0021FlagKey(idx) + Len.FLAG_KEY;
        }

        public static int idso0021FlagCallUsing(int idx) {
            return idso0021FlagReturnCode(idx) + Len.FLAG_RETURN_CODE;
        }

        public static int idso0021FlagWhereCond(int idx) {
            return idso0021FlagCallUsing(idx) + Len.FLAG_CALL_USING;
        }

        public static int idso0021FlagRedefines(int idx) {
            return idso0021FlagWhereCond(idx) + Len.FLAG_WHERE_COND;
        }

        public static int idso0021TipoDato(int idx) {
            return idso0021FlagRedefines(idx) + Len.FLAG_REDEFINES;
        }

        public static int idso0021InizioPosizione(int idx) {
            return idso0021TipoDato(idx) + Len.TIPO_DATO;
        }

        public static int idso0021LunghezzaDato(int idx) {
            return idso0021InizioPosizione(idx) + Len.INIZIO_POSIZIONE;
        }

        public static int idso0021LunghezzaDatoNom(int idx) {
            return idso0021LunghezzaDato(idx) + Len.LUNGHEZZA_DATO;
        }

        public static int idso0021PrecisioneDato(int idx) {
            return idso0021LunghezzaDatoNom(idx) + Len.LUNGHEZZA_DATO_NOM;
        }

        public static int idso0021FormattazioneDato(int idx) {
            return idso0021PrecisioneDato(idx) + Len.PRECISIONE_DATO;
        }

        public static int idso0021ServConversione(int idx) {
            return idso0021FormattazioneDato(idx) + Len.FORMATTAZIONE_DATO;
        }

        public static int idso0021AreaConvStandard(int idx) {
            return idso0021ServConversione(idx) + Len.SERV_CONVERSIONE;
        }

        public static int idso0021CodiceDominio(int idx) {
            return idso0021AreaConvStandard(idx) + Len.AREA_CONV_STANDARD;
        }

        public static int idso0021ServizioValidazione(int idx) {
            return idso0021CodiceDominio(idx) + Len.CODICE_DOMINIO;
        }

        public static int idso0021Ricorrenza(int idx) {
            return idso0021ServizioValidazione(idx) + Len.SERVIZIO_VALIDAZIONE;
        }

        public static int idso0021Clone(int idx) {
            return idso0021Ricorrenza(idx) + Len.RICORRENZA;
        }

        public static int idso0021Obbligatorieta(int idx) {
            return idso0021Clone(idx) + Len.CLONE;
        }

        public static int idso0021ValoreDefault(int idx) {
            return idso0021Obbligatorieta(idx) + Len.OBBLIGATORIETA;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_DATO = 30;
        public static final int FLAG_KEY = 1;
        public static final int FLAG_RETURN_CODE = 1;
        public static final int FLAG_CALL_USING = 1;
        public static final int FLAG_WHERE_COND = 1;
        public static final int FLAG_REDEFINES = 1;
        public static final int TIPO_DATO = 2;
        public static final int INIZIO_POSIZIONE = 3;
        public static final int LUNGHEZZA_DATO = 3;
        public static final int LUNGHEZZA_DATO_NOM = 3;
        public static final int PRECISIONE_DATO = 2;
        public static final int FORMATTAZIONE_DATO = 20;
        public static final int SERV_CONVERSIONE = 8;
        public static final int AREA_CONV_STANDARD = 1;
        public static final int CODICE_DOMINIO = 30;
        public static final int SERVIZIO_VALIDAZIONE = 8;
        public static final int RICORRENZA = 3;
        public static final int CLONE = 1;
        public static final int OBBLIGATORIETA = 1;
        public static final int VALORE_DEFAULT = 10;
        public static final int ELEMENTS_STR_DATO = CODICE_DATO + FLAG_KEY + FLAG_RETURN_CODE + FLAG_CALL_USING + FLAG_WHERE_COND + FLAG_REDEFINES + TIPO_DATO + INIZIO_POSIZIONE + LUNGHEZZA_DATO + LUNGHEZZA_DATO_NOM + PRECISIONE_DATO + FORMATTAZIONE_DATO + SERV_CONVERSIONE + AREA_CONV_STANDARD + CODICE_DOMINIO + SERVIZIO_VALIDAZIONE + RICORRENZA + CLONE + OBBLIGATORIETA + VALORE_DEFAULT;
        public static final int FLR1 = 130;
        public static final int IDSO0021_STRUTTURA_DATO = Idso0021StrutturaDato.ELEMENTS_STR_DATO_MAXOCCURS * ELEMENTS_STR_DATO;
        public static final int IDSO0021_RESTO_TAB = 32370;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int INIZIO_POSIZIONE = 5;
            public static final int LUNGHEZZA_DATO = 5;
            public static final int LUNGHEZZA_DATO_NOM = 5;
            public static final int PRECISIONE_DATO = 2;
            public static final int RICORRENZA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
