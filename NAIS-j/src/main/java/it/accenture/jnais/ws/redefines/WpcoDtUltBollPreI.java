package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-PRE-I<br>
 * Variable: WPCO-DT-ULT-BOLL-PRE-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollPreI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollPreI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_PRE_I;
    }

    public void setWpcoDtUltBollPreI(int wpcoDtUltBollPreI) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_PRE_I, wpcoDtUltBollPreI, Len.Int.WPCO_DT_ULT_BOLL_PRE_I);
    }

    public void setDpcoDtUltBollPreIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_PRE_I, Pos.WPCO_DT_ULT_BOLL_PRE_I);
    }

    /**Original name: WPCO-DT-ULT-BOLL-PRE-I<br>*/
    public int getWpcoDtUltBollPreI() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_PRE_I, Len.Int.WPCO_DT_ULT_BOLL_PRE_I);
    }

    public byte[] getWpcoDtUltBollPreIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_PRE_I, Pos.WPCO_DT_ULT_BOLL_PRE_I);
        return buffer;
    }

    public void setWpcoDtUltBollPreINull(String wpcoDtUltBollPreINull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_PRE_I_NULL, wpcoDtUltBollPreINull, Len.WPCO_DT_ULT_BOLL_PRE_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-PRE-I-NULL<br>*/
    public String getWpcoDtUltBollPreINull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_PRE_I_NULL, Len.WPCO_DT_ULT_BOLL_PRE_I_NULL);
    }

    public String getWpcoDtUltBollPreINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollPreINull(), Len.WPCO_DT_ULT_BOLL_PRE_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_PRE_I = 1;
        public static final int WPCO_DT_ULT_BOLL_PRE_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_PRE_I = 5;
        public static final int WPCO_DT_ULT_BOLL_PRE_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_PRE_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
