package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IN-RCODE<br>
 * Variable: IN-RCODE from program LCCS0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class InRcode extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IN-RCODE
    private String inRcode = "";

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IN_RCODE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setInRcodeFromBuffer(buf);
    }

    public void setInRcode(short inRcode) {
        this.inRcode = NumericDisplay.asString(inRcode, Len.IN_RCODE);
    }

    public void setInRcodeFormatted(String inRcode) {
        this.inRcode = Trunc.toUnsignedNumeric(inRcode, Len.IN_RCODE);
    }

    public void setInRcodeFromBuffer(byte[] buffer, int offset) {
        setInRcodeFormatted(MarshalByte.readString(buffer, offset, Len.IN_RCODE));
    }

    public void setInRcodeFromBuffer(byte[] buffer) {
        setInRcodeFromBuffer(buffer, 1);
    }

    public short getInRcode() {
        return NumericDisplay.asShort(this.inRcode);
    }

    @Override
    public byte[] serialize() {
        return MarshalByteExt.shortToBuffer(getInRcode(), Len.Int.IN_RCODE, SignType.NO_SIGN);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IN_RCODE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IN_RCODE = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IN_RCODE = 0;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
