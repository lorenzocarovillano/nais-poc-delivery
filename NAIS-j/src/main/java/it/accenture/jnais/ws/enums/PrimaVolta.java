package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: PRIMA-VOLTA<br>
 * Variable: PRIMA-VOLTA from copybook IDSV0501<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class PrimaVolta {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setPrimaVolta(char primaVolta) {
        this.value = primaVolta;
    }

    public char getPrimaVolta() {
        return this.value;
    }

    public boolean isPrimaVoltaSi() {
        return value == SI;
    }

    public void setPrimaVoltaSi() {
        value = SI;
    }

    public void setPrimaVoltaNo() {
        value = NO;
    }
}
