package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-CNDE-DAL2007-CALC<br>
 * Variable: DFL-CNDE-DAL2007-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflCndeDal2007Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflCndeDal2007Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_CNDE_DAL2007_CALC;
    }

    public void setDflCndeDal2007Calc(AfDecimal dflCndeDal2007Calc) {
        writeDecimalAsPacked(Pos.DFL_CNDE_DAL2007_CALC, dflCndeDal2007Calc.copy());
    }

    public void setDflCndeDal2007CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_CNDE_DAL2007_CALC, Pos.DFL_CNDE_DAL2007_CALC);
    }

    /**Original name: DFL-CNDE-DAL2007-CALC<br>*/
    public AfDecimal getDflCndeDal2007Calc() {
        return readPackedAsDecimal(Pos.DFL_CNDE_DAL2007_CALC, Len.Int.DFL_CNDE_DAL2007_CALC, Len.Fract.DFL_CNDE_DAL2007_CALC);
    }

    public byte[] getDflCndeDal2007CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_CNDE_DAL2007_CALC, Pos.DFL_CNDE_DAL2007_CALC);
        return buffer;
    }

    public void setDflCndeDal2007CalcNull(String dflCndeDal2007CalcNull) {
        writeString(Pos.DFL_CNDE_DAL2007_CALC_NULL, dflCndeDal2007CalcNull, Len.DFL_CNDE_DAL2007_CALC_NULL);
    }

    /**Original name: DFL-CNDE-DAL2007-CALC-NULL<br>*/
    public String getDflCndeDal2007CalcNull() {
        return readString(Pos.DFL_CNDE_DAL2007_CALC_NULL, Len.DFL_CNDE_DAL2007_CALC_NULL);
    }

    public String getDflCndeDal2007CalcNullFormatted() {
        return Functions.padBlanks(getDflCndeDal2007CalcNull(), Len.DFL_CNDE_DAL2007_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_CNDE_DAL2007_CALC = 1;
        public static final int DFL_CNDE_DAL2007_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_CNDE_DAL2007_CALC = 8;
        public static final int DFL_CNDE_DAL2007_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_CNDE_DAL2007_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_CNDE_DAL2007_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
