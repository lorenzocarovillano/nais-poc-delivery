package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-DT-PRESC<br>
 * Variable: ADE-DT-PRESC from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeDtPresc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeDtPresc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_DT_PRESC;
    }

    public void setAdeDtPresc(int adeDtPresc) {
        writeIntAsPacked(Pos.ADE_DT_PRESC, adeDtPresc, Len.Int.ADE_DT_PRESC);
    }

    public void setAdeDtPrescFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_DT_PRESC, Pos.ADE_DT_PRESC);
    }

    /**Original name: ADE-DT-PRESC<br>*/
    public int getAdeDtPresc() {
        return readPackedAsInt(Pos.ADE_DT_PRESC, Len.Int.ADE_DT_PRESC);
    }

    public byte[] getAdeDtPrescAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_DT_PRESC, Pos.ADE_DT_PRESC);
        return buffer;
    }

    public void setAdeDtPrescNull(String adeDtPrescNull) {
        writeString(Pos.ADE_DT_PRESC_NULL, adeDtPrescNull, Len.ADE_DT_PRESC_NULL);
    }

    /**Original name: ADE-DT-PRESC-NULL<br>*/
    public String getAdeDtPrescNull() {
        return readString(Pos.ADE_DT_PRESC_NULL, Len.ADE_DT_PRESC_NULL);
    }

    public String getAdeDtPrescNullFormatted() {
        return Functions.padBlanks(getAdeDtPrescNull(), Len.ADE_DT_PRESC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_DT_PRESC = 1;
        public static final int ADE_DT_PRESC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_DT_PRESC = 5;
        public static final int ADE_DT_PRESC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_DT_PRESC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
