package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B05-PROG-SCHEDA-VALOR<br>
 * Variable: W-B05-PROG-SCHEDA-VALOR from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB05ProgSchedaValorLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB05ProgSchedaValorLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B05_PROG_SCHEDA_VALOR;
    }

    public void setwB05ProgSchedaValor(int wB05ProgSchedaValor) {
        writeIntAsPacked(Pos.W_B05_PROG_SCHEDA_VALOR, wB05ProgSchedaValor, Len.Int.W_B05_PROG_SCHEDA_VALOR);
    }

    /**Original name: W-B05-PROG-SCHEDA-VALOR<br>*/
    public int getwB05ProgSchedaValor() {
        return readPackedAsInt(Pos.W_B05_PROG_SCHEDA_VALOR, Len.Int.W_B05_PROG_SCHEDA_VALOR);
    }

    public byte[] getwB05ProgSchedaValorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B05_PROG_SCHEDA_VALOR, Pos.W_B05_PROG_SCHEDA_VALOR);
        return buffer;
    }

    public void setwB05ProgSchedaValorNull(String wB05ProgSchedaValorNull) {
        writeString(Pos.W_B05_PROG_SCHEDA_VALOR_NULL, wB05ProgSchedaValorNull, Len.W_B05_PROG_SCHEDA_VALOR_NULL);
    }

    /**Original name: W-B05-PROG-SCHEDA-VALOR-NULL<br>*/
    public String getwB05ProgSchedaValorNull() {
        return readString(Pos.W_B05_PROG_SCHEDA_VALOR_NULL, Len.W_B05_PROG_SCHEDA_VALOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B05_PROG_SCHEDA_VALOR = 1;
        public static final int W_B05_PROG_SCHEDA_VALOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B05_PROG_SCHEDA_VALOR = 5;
        public static final int W_B05_PROG_SCHEDA_VALOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B05_PROG_SCHEDA_VALOR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
