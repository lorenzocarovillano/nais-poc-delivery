package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMPST-VIS-1382011<br>
 * Variable: WBEL-IMPST-VIS-1382011 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpstVis1382011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpstVis1382011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMPST_VIS1382011;
    }

    public void setWbelImpstVis1382011(AfDecimal wbelImpstVis1382011) {
        writeDecimalAsPacked(Pos.WBEL_IMPST_VIS1382011, wbelImpstVis1382011.copy());
    }

    public void setWbelImpstVis1382011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMPST_VIS1382011, Pos.WBEL_IMPST_VIS1382011);
    }

    /**Original name: WBEL-IMPST-VIS-1382011<br>*/
    public AfDecimal getWbelImpstVis1382011() {
        return readPackedAsDecimal(Pos.WBEL_IMPST_VIS1382011, Len.Int.WBEL_IMPST_VIS1382011, Len.Fract.WBEL_IMPST_VIS1382011);
    }

    public byte[] getWbelImpstVis1382011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMPST_VIS1382011, Pos.WBEL_IMPST_VIS1382011);
        return buffer;
    }

    public void initWbelImpstVis1382011Spaces() {
        fill(Pos.WBEL_IMPST_VIS1382011, Len.WBEL_IMPST_VIS1382011, Types.SPACE_CHAR);
    }

    public void setWbelImpstVis1382011Null(String wbelImpstVis1382011Null) {
        writeString(Pos.WBEL_IMPST_VIS1382011_NULL, wbelImpstVis1382011Null, Len.WBEL_IMPST_VIS1382011_NULL);
    }

    /**Original name: WBEL-IMPST-VIS-1382011-NULL<br>*/
    public String getWbelImpstVis1382011Null() {
        return readString(Pos.WBEL_IMPST_VIS1382011_NULL, Len.WBEL_IMPST_VIS1382011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_VIS1382011 = 1;
        public static final int WBEL_IMPST_VIS1382011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_VIS1382011 = 8;
        public static final int WBEL_IMPST_VIS1382011_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_VIS1382011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_VIS1382011 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
