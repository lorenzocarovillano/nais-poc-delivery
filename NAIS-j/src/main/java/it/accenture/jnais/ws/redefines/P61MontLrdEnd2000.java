package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P61-MONT-LRD-END2000<br>
 * Variable: P61-MONT-LRD-END2000 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P61MontLrdEnd2000 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P61MontLrdEnd2000() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P61_MONT_LRD_END2000;
    }

    public void setP61MontLrdEnd2000(AfDecimal p61MontLrdEnd2000) {
        writeDecimalAsPacked(Pos.P61_MONT_LRD_END2000, p61MontLrdEnd2000.copy());
    }

    public void setP61MontLrdEnd2000FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P61_MONT_LRD_END2000, Pos.P61_MONT_LRD_END2000);
    }

    /**Original name: P61-MONT-LRD-END2000<br>*/
    public AfDecimal getP61MontLrdEnd2000() {
        return readPackedAsDecimal(Pos.P61_MONT_LRD_END2000, Len.Int.P61_MONT_LRD_END2000, Len.Fract.P61_MONT_LRD_END2000);
    }

    public byte[] getP61MontLrdEnd2000AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P61_MONT_LRD_END2000, Pos.P61_MONT_LRD_END2000);
        return buffer;
    }

    public void setP61MontLrdEnd2000Null(String p61MontLrdEnd2000Null) {
        writeString(Pos.P61_MONT_LRD_END2000_NULL, p61MontLrdEnd2000Null, Len.P61_MONT_LRD_END2000_NULL);
    }

    /**Original name: P61-MONT-LRD-END2000-NULL<br>*/
    public String getP61MontLrdEnd2000Null() {
        return readString(Pos.P61_MONT_LRD_END2000_NULL, Len.P61_MONT_LRD_END2000_NULL);
    }

    public String getP61MontLrdEnd2000NullFormatted() {
        return Functions.padBlanks(getP61MontLrdEnd2000Null(), Len.P61_MONT_LRD_END2000_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P61_MONT_LRD_END2000 = 1;
        public static final int P61_MONT_LRD_END2000_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_MONT_LRD_END2000 = 8;
        public static final int P61_MONT_LRD_END2000_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_MONT_LRD_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P61_MONT_LRD_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
