package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-SOPR-SPORT<br>
 * Variable: WPAG-RATA-SOPR-SPORT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataSoprSport extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataSoprSport() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_SOPR_SPORT;
    }

    public void setWpagRataSoprSport(AfDecimal wpagRataSoprSport) {
        writeDecimalAsPacked(Pos.WPAG_RATA_SOPR_SPORT, wpagRataSoprSport.copy());
    }

    public void setWpagRataSoprSportFormatted(String wpagRataSoprSport) {
        setWpagRataSoprSport(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_SOPR_SPORT + Len.Fract.WPAG_RATA_SOPR_SPORT, Len.Fract.WPAG_RATA_SOPR_SPORT, wpagRataSoprSport));
    }

    public void setWpagRataSoprSportFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_SOPR_SPORT, Pos.WPAG_RATA_SOPR_SPORT);
    }

    /**Original name: WPAG-RATA-SOPR-SPORT<br>*/
    public AfDecimal getWpagRataSoprSport() {
        return readPackedAsDecimal(Pos.WPAG_RATA_SOPR_SPORT, Len.Int.WPAG_RATA_SOPR_SPORT, Len.Fract.WPAG_RATA_SOPR_SPORT);
    }

    public byte[] getWpagRataSoprSportAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_SOPR_SPORT, Pos.WPAG_RATA_SOPR_SPORT);
        return buffer;
    }

    public void initWpagRataSoprSportSpaces() {
        fill(Pos.WPAG_RATA_SOPR_SPORT, Len.WPAG_RATA_SOPR_SPORT, Types.SPACE_CHAR);
    }

    public void setWpagRataSoprSportNull(String wpagRataSoprSportNull) {
        writeString(Pos.WPAG_RATA_SOPR_SPORT_NULL, wpagRataSoprSportNull, Len.WPAG_RATA_SOPR_SPORT_NULL);
    }

    /**Original name: WPAG-RATA-SOPR-SPORT-NULL<br>*/
    public String getWpagRataSoprSportNull() {
        return readString(Pos.WPAG_RATA_SOPR_SPORT_NULL, Len.WPAG_RATA_SOPR_SPORT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_SOPR_SPORT = 1;
        public static final int WPAG_RATA_SOPR_SPORT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_SOPR_SPORT = 8;
        public static final int WPAG_RATA_SOPR_SPORT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_SOPR_SPORT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_SOPR_SPORT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
