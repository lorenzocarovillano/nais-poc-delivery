package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-INVRIO-INI<br>
 * Variable: WTGA-PRE-INVRIO-INI from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPreInvrioIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPreInvrioIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_INVRIO_INI;
    }

    public void setWtgaPreInvrioIni(AfDecimal wtgaPreInvrioIni) {
        writeDecimalAsPacked(Pos.WTGA_PRE_INVRIO_INI, wtgaPreInvrioIni.copy());
    }

    public void setWtgaPreInvrioIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_INVRIO_INI, Pos.WTGA_PRE_INVRIO_INI);
    }

    /**Original name: WTGA-PRE-INVRIO-INI<br>*/
    public AfDecimal getWtgaPreInvrioIni() {
        return readPackedAsDecimal(Pos.WTGA_PRE_INVRIO_INI, Len.Int.WTGA_PRE_INVRIO_INI, Len.Fract.WTGA_PRE_INVRIO_INI);
    }

    public byte[] getWtgaPreInvrioIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_INVRIO_INI, Pos.WTGA_PRE_INVRIO_INI);
        return buffer;
    }

    public void initWtgaPreInvrioIniSpaces() {
        fill(Pos.WTGA_PRE_INVRIO_INI, Len.WTGA_PRE_INVRIO_INI, Types.SPACE_CHAR);
    }

    public void setWtgaPreInvrioIniNull(String wtgaPreInvrioIniNull) {
        writeString(Pos.WTGA_PRE_INVRIO_INI_NULL, wtgaPreInvrioIniNull, Len.WTGA_PRE_INVRIO_INI_NULL);
    }

    /**Original name: WTGA-PRE-INVRIO-INI-NULL<br>*/
    public String getWtgaPreInvrioIniNull() {
        return readString(Pos.WTGA_PRE_INVRIO_INI_NULL, Len.WTGA_PRE_INVRIO_INI_NULL);
    }

    public String getWtgaPreInvrioIniNullFormatted() {
        return Functions.padBlanks(getWtgaPreInvrioIniNull(), Len.WTGA_PRE_INVRIO_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_INVRIO_INI = 1;
        public static final int WTGA_PRE_INVRIO_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_INVRIO_INI = 8;
        public static final int WTGA_PRE_INVRIO_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_INVRIO_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_INVRIO_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
