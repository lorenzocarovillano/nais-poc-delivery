package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-ID-RAPP-ANA<br>
 * Variable: WTDR-ID-RAPP-ANA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrIdRappAna extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrIdRappAna() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_ID_RAPP_ANA;
    }

    public void setWtdrIdRappAna(int wtdrIdRappAna) {
        writeIntAsPacked(Pos.WTDR_ID_RAPP_ANA, wtdrIdRappAna, Len.Int.WTDR_ID_RAPP_ANA);
    }

    /**Original name: WTDR-ID-RAPP-ANA<br>*/
    public int getWtdrIdRappAna() {
        return readPackedAsInt(Pos.WTDR_ID_RAPP_ANA, Len.Int.WTDR_ID_RAPP_ANA);
    }

    public void setWtdrIdRappAnaNull(String wtdrIdRappAnaNull) {
        writeString(Pos.WTDR_ID_RAPP_ANA_NULL, wtdrIdRappAnaNull, Len.WTDR_ID_RAPP_ANA_NULL);
    }

    /**Original name: WTDR-ID-RAPP-ANA-NULL<br>*/
    public String getWtdrIdRappAnaNull() {
        return readString(Pos.WTDR_ID_RAPP_ANA_NULL, Len.WTDR_ID_RAPP_ANA_NULL);
    }

    public String getWtdrIdRappAnaNullFormatted() {
        return Functions.padBlanks(getWtdrIdRappAnaNull(), Len.WTDR_ID_RAPP_ANA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_ID_RAPP_ANA = 1;
        public static final int WTDR_ID_RAPP_ANA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_ID_RAPP_ANA = 5;
        public static final int WTDR_ID_RAPP_ANA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_ID_RAPP_ANA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
