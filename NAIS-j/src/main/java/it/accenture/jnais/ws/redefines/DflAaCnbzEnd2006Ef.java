package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-AA-CNBZ-END2006-EF<br>
 * Variable: DFL-AA-CNBZ-END2006-EF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAaCnbzEnd2006Ef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAaCnbzEnd2006Ef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_AA_CNBZ_END2006_EF;
    }

    public void setDflAaCnbzEnd2006Ef(int dflAaCnbzEnd2006Ef) {
        writeIntAsPacked(Pos.DFL_AA_CNBZ_END2006_EF, dflAaCnbzEnd2006Ef, Len.Int.DFL_AA_CNBZ_END2006_EF);
    }

    public void setDflAaCnbzEnd2006EfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_AA_CNBZ_END2006_EF, Pos.DFL_AA_CNBZ_END2006_EF);
    }

    /**Original name: DFL-AA-CNBZ-END2006-EF<br>*/
    public int getDflAaCnbzEnd2006Ef() {
        return readPackedAsInt(Pos.DFL_AA_CNBZ_END2006_EF, Len.Int.DFL_AA_CNBZ_END2006_EF);
    }

    public byte[] getDflAaCnbzEnd2006EfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_AA_CNBZ_END2006_EF, Pos.DFL_AA_CNBZ_END2006_EF);
        return buffer;
    }

    public void setDflAaCnbzEnd2006EfNull(String dflAaCnbzEnd2006EfNull) {
        writeString(Pos.DFL_AA_CNBZ_END2006_EF_NULL, dflAaCnbzEnd2006EfNull, Len.DFL_AA_CNBZ_END2006_EF_NULL);
    }

    /**Original name: DFL-AA-CNBZ-END2006-EF-NULL<br>*/
    public String getDflAaCnbzEnd2006EfNull() {
        return readString(Pos.DFL_AA_CNBZ_END2006_EF_NULL, Len.DFL_AA_CNBZ_END2006_EF_NULL);
    }

    public String getDflAaCnbzEnd2006EfNullFormatted() {
        return Functions.padBlanks(getDflAaCnbzEnd2006EfNull(), Len.DFL_AA_CNBZ_END2006_EF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_AA_CNBZ_END2006_EF = 1;
        public static final int DFL_AA_CNBZ_END2006_EF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_AA_CNBZ_END2006_EF = 3;
        public static final int DFL_AA_CNBZ_END2006_EF_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_AA_CNBZ_END2006_EF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
