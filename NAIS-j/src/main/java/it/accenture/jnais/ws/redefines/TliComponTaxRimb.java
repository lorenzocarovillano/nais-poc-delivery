package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-COMPON-TAX-RIMB<br>
 * Variable: TLI-COMPON-TAX-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliComponTaxRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliComponTaxRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_COMPON_TAX_RIMB;
    }

    public void setTliComponTaxRimb(AfDecimal tliComponTaxRimb) {
        writeDecimalAsPacked(Pos.TLI_COMPON_TAX_RIMB, tliComponTaxRimb.copy());
    }

    public void setTliComponTaxRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_COMPON_TAX_RIMB, Pos.TLI_COMPON_TAX_RIMB);
    }

    /**Original name: TLI-COMPON-TAX-RIMB<br>*/
    public AfDecimal getTliComponTaxRimb() {
        return readPackedAsDecimal(Pos.TLI_COMPON_TAX_RIMB, Len.Int.TLI_COMPON_TAX_RIMB, Len.Fract.TLI_COMPON_TAX_RIMB);
    }

    public byte[] getTliComponTaxRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_COMPON_TAX_RIMB, Pos.TLI_COMPON_TAX_RIMB);
        return buffer;
    }

    public void setTliComponTaxRimbNull(String tliComponTaxRimbNull) {
        writeString(Pos.TLI_COMPON_TAX_RIMB_NULL, tliComponTaxRimbNull, Len.TLI_COMPON_TAX_RIMB_NULL);
    }

    /**Original name: TLI-COMPON-TAX-RIMB-NULL<br>*/
    public String getTliComponTaxRimbNull() {
        return readString(Pos.TLI_COMPON_TAX_RIMB_NULL, Len.TLI_COMPON_TAX_RIMB_NULL);
    }

    public String getTliComponTaxRimbNullFormatted() {
        return Functions.padBlanks(getTliComponTaxRimbNull(), Len.TLI_COMPON_TAX_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_COMPON_TAX_RIMB = 1;
        public static final int TLI_COMPON_TAX_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_COMPON_TAX_RIMB = 8;
        public static final int TLI_COMPON_TAX_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_COMPON_TAX_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_COMPON_TAX_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
