package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ALQ-TAX-SEP-EFFLQ<br>
 * Variable: DFL-ALQ-TAX-SEP-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAlqTaxSepEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAlqTaxSepEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ALQ_TAX_SEP_EFFLQ;
    }

    public void setDflAlqTaxSepEfflq(AfDecimal dflAlqTaxSepEfflq) {
        writeDecimalAsPacked(Pos.DFL_ALQ_TAX_SEP_EFFLQ, dflAlqTaxSepEfflq.copy());
    }

    public void setDflAlqTaxSepEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ALQ_TAX_SEP_EFFLQ, Pos.DFL_ALQ_TAX_SEP_EFFLQ);
    }

    /**Original name: DFL-ALQ-TAX-SEP-EFFLQ<br>*/
    public AfDecimal getDflAlqTaxSepEfflq() {
        return readPackedAsDecimal(Pos.DFL_ALQ_TAX_SEP_EFFLQ, Len.Int.DFL_ALQ_TAX_SEP_EFFLQ, Len.Fract.DFL_ALQ_TAX_SEP_EFFLQ);
    }

    public byte[] getDflAlqTaxSepEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ALQ_TAX_SEP_EFFLQ, Pos.DFL_ALQ_TAX_SEP_EFFLQ);
        return buffer;
    }

    public void setDflAlqTaxSepEfflqNull(String dflAlqTaxSepEfflqNull) {
        writeString(Pos.DFL_ALQ_TAX_SEP_EFFLQ_NULL, dflAlqTaxSepEfflqNull, Len.DFL_ALQ_TAX_SEP_EFFLQ_NULL);
    }

    /**Original name: DFL-ALQ-TAX-SEP-EFFLQ-NULL<br>*/
    public String getDflAlqTaxSepEfflqNull() {
        return readString(Pos.DFL_ALQ_TAX_SEP_EFFLQ_NULL, Len.DFL_ALQ_TAX_SEP_EFFLQ_NULL);
    }

    public String getDflAlqTaxSepEfflqNullFormatted() {
        return Functions.padBlanks(getDflAlqTaxSepEfflqNull(), Len.DFL_ALQ_TAX_SEP_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_TAX_SEP_EFFLQ = 1;
        public static final int DFL_ALQ_TAX_SEP_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_TAX_SEP_EFFLQ = 4;
        public static final int DFL_ALQ_TAX_SEP_EFFLQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_TAX_SEP_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_TAX_SEP_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
