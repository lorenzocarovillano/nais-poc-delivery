package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0301-ESITO<br>
 * Variable: IDSV0301-ESITO from copybook IDSV0301<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0301Esito {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.ESITO);
    public static final String OK = "OK";
    public static final String KO = "KO";

    //==== METHODS ====
    public void setEsito(String esito) {
        this.value = Functions.subString(esito, Len.ESITO);
    }

    public String getEsito() {
        return this.value;
    }

    public boolean isOk() {
        return value.equals(OK);
    }

    public void setOk() {
        value = OK;
    }

    public boolean isKo() {
        return value.equals(KO);
    }

    public void setKo() {
        value = KO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ESITO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
