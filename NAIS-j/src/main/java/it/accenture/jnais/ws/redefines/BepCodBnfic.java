package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEP-COD-BNFIC<br>
 * Variable: BEP-COD-BNFIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BepCodBnfic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BepCodBnfic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEP_COD_BNFIC;
    }

    public void setBepCodBnfic(short bepCodBnfic) {
        writeShortAsPacked(Pos.BEP_COD_BNFIC, bepCodBnfic, Len.Int.BEP_COD_BNFIC);
    }

    public void setBepCodBnficFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEP_COD_BNFIC, Pos.BEP_COD_BNFIC);
    }

    /**Original name: BEP-COD-BNFIC<br>*/
    public short getBepCodBnfic() {
        return readPackedAsShort(Pos.BEP_COD_BNFIC, Len.Int.BEP_COD_BNFIC);
    }

    public byte[] getBepCodBnficAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEP_COD_BNFIC, Pos.BEP_COD_BNFIC);
        return buffer;
    }

    public void setBepCodBnficNull(String bepCodBnficNull) {
        writeString(Pos.BEP_COD_BNFIC_NULL, bepCodBnficNull, Len.BEP_COD_BNFIC_NULL);
    }

    /**Original name: BEP-COD-BNFIC-NULL<br>*/
    public String getBepCodBnficNull() {
        return readString(Pos.BEP_COD_BNFIC_NULL, Len.BEP_COD_BNFIC_NULL);
    }

    public String getBepCodBnficNullFormatted() {
        return Functions.padBlanks(getBepCodBnficNull(), Len.BEP_COD_BNFIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEP_COD_BNFIC = 1;
        public static final int BEP_COD_BNFIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEP_COD_BNFIC = 2;
        public static final int BEP_COD_BNFIC_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEP_COD_BNFIC = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
