package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-TOT-INTR-PREST<br>
 * Variable: DTR-TOT-INTR-PREST from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrTotIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrTotIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_TOT_INTR_PREST;
    }

    public void setDtrTotIntrPrest(AfDecimal dtrTotIntrPrest) {
        writeDecimalAsPacked(Pos.DTR_TOT_INTR_PREST, dtrTotIntrPrest.copy());
    }

    public void setDtrTotIntrPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_TOT_INTR_PREST, Pos.DTR_TOT_INTR_PREST);
    }

    /**Original name: DTR-TOT-INTR-PREST<br>*/
    public AfDecimal getDtrTotIntrPrest() {
        return readPackedAsDecimal(Pos.DTR_TOT_INTR_PREST, Len.Int.DTR_TOT_INTR_PREST, Len.Fract.DTR_TOT_INTR_PREST);
    }

    public byte[] getDtrTotIntrPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_TOT_INTR_PREST, Pos.DTR_TOT_INTR_PREST);
        return buffer;
    }

    public void setDtrTotIntrPrestNull(String dtrTotIntrPrestNull) {
        writeString(Pos.DTR_TOT_INTR_PREST_NULL, dtrTotIntrPrestNull, Len.DTR_TOT_INTR_PREST_NULL);
    }

    /**Original name: DTR-TOT-INTR-PREST-NULL<br>*/
    public String getDtrTotIntrPrestNull() {
        return readString(Pos.DTR_TOT_INTR_PREST_NULL, Len.DTR_TOT_INTR_PREST_NULL);
    }

    public String getDtrTotIntrPrestNullFormatted() {
        return Functions.padBlanks(getDtrTotIntrPrestNull(), Len.DTR_TOT_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_TOT_INTR_PREST = 1;
        public static final int DTR_TOT_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_TOT_INTR_PREST = 8;
        public static final int DTR_TOT_INTR_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_TOT_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_TOT_INTR_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
