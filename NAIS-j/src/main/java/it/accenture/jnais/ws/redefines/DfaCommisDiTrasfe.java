package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-COMMIS-DI-TRASFE<br>
 * Variable: DFA-COMMIS-DI-TRASFE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaCommisDiTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaCommisDiTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_COMMIS_DI_TRASFE;
    }

    public void setDfaCommisDiTrasfe(AfDecimal dfaCommisDiTrasfe) {
        writeDecimalAsPacked(Pos.DFA_COMMIS_DI_TRASFE, dfaCommisDiTrasfe.copy());
    }

    public void setDfaCommisDiTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_COMMIS_DI_TRASFE, Pos.DFA_COMMIS_DI_TRASFE);
    }

    /**Original name: DFA-COMMIS-DI-TRASFE<br>*/
    public AfDecimal getDfaCommisDiTrasfe() {
        return readPackedAsDecimal(Pos.DFA_COMMIS_DI_TRASFE, Len.Int.DFA_COMMIS_DI_TRASFE, Len.Fract.DFA_COMMIS_DI_TRASFE);
    }

    public byte[] getDfaCommisDiTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_COMMIS_DI_TRASFE, Pos.DFA_COMMIS_DI_TRASFE);
        return buffer;
    }

    public void setDfaCommisDiTrasfeNull(String dfaCommisDiTrasfeNull) {
        writeString(Pos.DFA_COMMIS_DI_TRASFE_NULL, dfaCommisDiTrasfeNull, Len.DFA_COMMIS_DI_TRASFE_NULL);
    }

    /**Original name: DFA-COMMIS-DI-TRASFE-NULL<br>*/
    public String getDfaCommisDiTrasfeNull() {
        return readString(Pos.DFA_COMMIS_DI_TRASFE_NULL, Len.DFA_COMMIS_DI_TRASFE_NULL);
    }

    public String getDfaCommisDiTrasfeNullFormatted() {
        return Functions.padBlanks(getDfaCommisDiTrasfeNull(), Len.DFA_COMMIS_DI_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_COMMIS_DI_TRASFE = 1;
        public static final int DFA_COMMIS_DI_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_COMMIS_DI_TRASFE = 8;
        public static final int DFA_COMMIS_DI_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_COMMIS_DI_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_COMMIS_DI_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
