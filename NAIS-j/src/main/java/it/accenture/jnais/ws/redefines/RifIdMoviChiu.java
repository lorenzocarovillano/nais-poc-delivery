package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIF-ID-MOVI-CHIU<br>
 * Variable: RIF-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RifIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RifIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIF_ID_MOVI_CHIU;
    }

    public void setRifIdMoviChiu(int rifIdMoviChiu) {
        writeIntAsPacked(Pos.RIF_ID_MOVI_CHIU, rifIdMoviChiu, Len.Int.RIF_ID_MOVI_CHIU);
    }

    public void setRifIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIF_ID_MOVI_CHIU, Pos.RIF_ID_MOVI_CHIU);
    }

    /**Original name: RIF-ID-MOVI-CHIU<br>*/
    public int getRifIdMoviChiu() {
        return readPackedAsInt(Pos.RIF_ID_MOVI_CHIU, Len.Int.RIF_ID_MOVI_CHIU);
    }

    public byte[] getRifIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIF_ID_MOVI_CHIU, Pos.RIF_ID_MOVI_CHIU);
        return buffer;
    }

    public void setRifIdMoviChiuNull(String rifIdMoviChiuNull) {
        writeString(Pos.RIF_ID_MOVI_CHIU_NULL, rifIdMoviChiuNull, Len.RIF_ID_MOVI_CHIU_NULL);
    }

    /**Original name: RIF-ID-MOVI-CHIU-NULL<br>*/
    public String getRifIdMoviChiuNull() {
        return readString(Pos.RIF_ID_MOVI_CHIU_NULL, Len.RIF_ID_MOVI_CHIU_NULL);
    }

    public String getRifIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getRifIdMoviChiuNull(), Len.RIF_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIF_ID_MOVI_CHIU = 1;
        public static final int RIF_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIF_ID_MOVI_CHIU = 5;
        public static final int RIF_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIF_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
