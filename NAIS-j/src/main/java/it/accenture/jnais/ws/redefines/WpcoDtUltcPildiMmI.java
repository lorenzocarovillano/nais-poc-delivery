package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-PILDI-MM-I<br>
 * Variable: WPCO-DT-ULTC-PILDI-MM-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcPildiMmI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcPildiMmI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_PILDI_MM_I;
    }

    public void setWpcoDtUltcPildiMmI(int wpcoDtUltcPildiMmI) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_PILDI_MM_I, wpcoDtUltcPildiMmI, Len.Int.WPCO_DT_ULTC_PILDI_MM_I);
    }

    public void setDpcoDtUltcPildiMmIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_PILDI_MM_I, Pos.WPCO_DT_ULTC_PILDI_MM_I);
    }

    /**Original name: WPCO-DT-ULTC-PILDI-MM-I<br>*/
    public int getWpcoDtUltcPildiMmI() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_PILDI_MM_I, Len.Int.WPCO_DT_ULTC_PILDI_MM_I);
    }

    public byte[] getWpcoDtUltcPildiMmIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_PILDI_MM_I, Pos.WPCO_DT_ULTC_PILDI_MM_I);
        return buffer;
    }

    public void setWpcoDtUltcPildiMmINull(String wpcoDtUltcPildiMmINull) {
        writeString(Pos.WPCO_DT_ULTC_PILDI_MM_I_NULL, wpcoDtUltcPildiMmINull, Len.WPCO_DT_ULTC_PILDI_MM_I_NULL);
    }

    /**Original name: WPCO-DT-ULTC-PILDI-MM-I-NULL<br>*/
    public String getWpcoDtUltcPildiMmINull() {
        return readString(Pos.WPCO_DT_ULTC_PILDI_MM_I_NULL, Len.WPCO_DT_ULTC_PILDI_MM_I_NULL);
    }

    public String getWpcoDtUltcPildiMmINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcPildiMmINull(), Len.WPCO_DT_ULTC_PILDI_MM_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_PILDI_MM_I = 1;
        public static final int WPCO_DT_ULTC_PILDI_MM_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_PILDI_MM_I = 5;
        public static final int WPCO_DT_ULTC_PILDI_MM_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_PILDI_MM_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
