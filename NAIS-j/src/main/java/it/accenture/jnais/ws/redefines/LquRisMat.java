package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-RIS-MAT<br>
 * Variable: LQU-RIS-MAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquRisMat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquRisMat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_RIS_MAT;
    }

    public void setLquRisMat(AfDecimal lquRisMat) {
        writeDecimalAsPacked(Pos.LQU_RIS_MAT, lquRisMat.copy());
    }

    public void setLquRisMatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_RIS_MAT, Pos.LQU_RIS_MAT);
    }

    /**Original name: LQU-RIS-MAT<br>*/
    public AfDecimal getLquRisMat() {
        return readPackedAsDecimal(Pos.LQU_RIS_MAT, Len.Int.LQU_RIS_MAT, Len.Fract.LQU_RIS_MAT);
    }

    public byte[] getLquRisMatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_RIS_MAT, Pos.LQU_RIS_MAT);
        return buffer;
    }

    public void setLquRisMatNull(String lquRisMatNull) {
        writeString(Pos.LQU_RIS_MAT_NULL, lquRisMatNull, Len.LQU_RIS_MAT_NULL);
    }

    /**Original name: LQU-RIS-MAT-NULL<br>*/
    public String getLquRisMatNull() {
        return readString(Pos.LQU_RIS_MAT_NULL, Len.LQU_RIS_MAT_NULL);
    }

    public String getLquRisMatNullFormatted() {
        return Functions.padBlanks(getLquRisMatNull(), Len.LQU_RIS_MAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_RIS_MAT = 1;
        public static final int LQU_RIS_MAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_RIS_MAT = 8;
        public static final int LQU_RIS_MAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_RIS_MAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_RIS_MAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
