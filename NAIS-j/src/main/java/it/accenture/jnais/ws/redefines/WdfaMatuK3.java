package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-MATU-K3<br>
 * Variable: WDFA-MATU-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaMatuK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaMatuK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_MATU_K3;
    }

    public void setWdfaMatuK3(AfDecimal wdfaMatuK3) {
        writeDecimalAsPacked(Pos.WDFA_MATU_K3, wdfaMatuK3.copy());
    }

    public void setWdfaMatuK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_MATU_K3, Pos.WDFA_MATU_K3);
    }

    /**Original name: WDFA-MATU-K3<br>*/
    public AfDecimal getWdfaMatuK3() {
        return readPackedAsDecimal(Pos.WDFA_MATU_K3, Len.Int.WDFA_MATU_K3, Len.Fract.WDFA_MATU_K3);
    }

    public byte[] getWdfaMatuK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_MATU_K3, Pos.WDFA_MATU_K3);
        return buffer;
    }

    public void setWdfaMatuK3Null(String wdfaMatuK3Null) {
        writeString(Pos.WDFA_MATU_K3_NULL, wdfaMatuK3Null, Len.WDFA_MATU_K3_NULL);
    }

    /**Original name: WDFA-MATU-K3-NULL<br>*/
    public String getWdfaMatuK3Null() {
        return readString(Pos.WDFA_MATU_K3_NULL, Len.WDFA_MATU_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_MATU_K3 = 1;
        public static final int WDFA_MATU_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_MATU_K3 = 8;
        public static final int WDFA_MATU_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_MATU_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_MATU_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
