package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-INDICI<br>
 * Variable: WS-INDICI from program LCCS0033<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsIndiciLccs0033 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-PVT
    private short tabPvt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-PAG
    private short tabPag = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-GAR-TARI
    private short garTari = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-RIC-RAN
    private short ricRan = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setTabPvt(short tabPvt) {
        this.tabPvt = tabPvt;
    }

    public short getTabPvt() {
        return this.tabPvt;
    }

    public void setTabPag(short tabPag) {
        this.tabPag = tabPag;
    }

    public short getTabPag() {
        return this.tabPag;
    }

    public void setGarTari(short garTari) {
        this.garTari = garTari;
    }

    public short getGarTari() {
        return this.garTari;
    }

    public void setRicRan(short ricRan) {
        this.ricRan = ricRan;
    }

    public short getRicRan() {
        return this.ricRan;
    }
}
