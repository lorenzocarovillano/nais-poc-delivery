package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: APPO-DATE2<br>
 * Variable: APPO-DATE2 from program LVVS1280<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AppoDate2 {

    //==== PROPERTIES ====
    //Original name: AA-APPO2
    private String aaAppo2 = DefaultValues.stringVal(Len.AA_APPO2);
    //Original name: MM-APPO2
    private String mmAppo2 = DefaultValues.stringVal(Len.MM_APPO2);
    //Original name: GG-APPO2
    private String ggAppo2 = DefaultValues.stringVal(Len.GG_APPO2);

    //==== METHODS ====
    public void setAppoDate2Formatted(String data) {
        byte[] buffer = new byte[Len.APPO_DATE2];
        MarshalByte.writeString(buffer, 1, data, Len.APPO_DATE2);
        setAppoDate2Bytes(buffer, 1);
    }

    public void setAppoDate2Bytes(byte[] buffer, int offset) {
        int position = offset;
        aaAppo2 = MarshalByte.readFixedString(buffer, position, Len.AA_APPO2);
        position += Len.AA_APPO2;
        mmAppo2 = MarshalByte.readFixedString(buffer, position, Len.MM_APPO2);
        position += Len.MM_APPO2;
        ggAppo2 = MarshalByte.readFixedString(buffer, position, Len.GG_APPO2);
    }

    public short getAaAppo2() {
        return NumericDisplay.asShort(this.aaAppo2);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA_APPO2 = 4;
        public static final int MM_APPO2 = 2;
        public static final int GG_APPO2 = 2;
        public static final int APPO_DATE2 = AA_APPO2 + MM_APPO2 + GG_APPO2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
