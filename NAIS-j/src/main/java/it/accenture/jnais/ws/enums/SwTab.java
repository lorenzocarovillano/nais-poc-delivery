package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-TAB<br>
 * Variable: SW-TAB from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwTab {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char OK = 'S';
    public static final char KO = 'N';

    //==== METHODS ====
    public void setSwTab(char swTab) {
        this.value = swTab;
    }

    public char getSwTab() {
        return this.value;
    }

    public boolean isOk() {
        return value == OK;
    }

    public void setOk() {
        value = OK;
    }

    public void setKo() {
        value = KO;
    }
}
