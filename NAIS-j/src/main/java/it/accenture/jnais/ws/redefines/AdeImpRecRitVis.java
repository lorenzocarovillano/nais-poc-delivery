package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-IMP-REC-RIT-VIS<br>
 * Variable: ADE-IMP-REC-RIT-VIS from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeImpRecRitVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeImpRecRitVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_IMP_REC_RIT_VIS;
    }

    public void setAdeImpRecRitVis(AfDecimal adeImpRecRitVis) {
        writeDecimalAsPacked(Pos.ADE_IMP_REC_RIT_VIS, adeImpRecRitVis.copy());
    }

    public void setAdeImpRecRitVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_IMP_REC_RIT_VIS, Pos.ADE_IMP_REC_RIT_VIS);
    }

    /**Original name: ADE-IMP-REC-RIT-VIS<br>*/
    public AfDecimal getAdeImpRecRitVis() {
        return readPackedAsDecimal(Pos.ADE_IMP_REC_RIT_VIS, Len.Int.ADE_IMP_REC_RIT_VIS, Len.Fract.ADE_IMP_REC_RIT_VIS);
    }

    public byte[] getAdeImpRecRitVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_IMP_REC_RIT_VIS, Pos.ADE_IMP_REC_RIT_VIS);
        return buffer;
    }

    public void setAdeImpRecRitVisNull(String adeImpRecRitVisNull) {
        writeString(Pos.ADE_IMP_REC_RIT_VIS_NULL, adeImpRecRitVisNull, Len.ADE_IMP_REC_RIT_VIS_NULL);
    }

    /**Original name: ADE-IMP-REC-RIT-VIS-NULL<br>*/
    public String getAdeImpRecRitVisNull() {
        return readString(Pos.ADE_IMP_REC_RIT_VIS_NULL, Len.ADE_IMP_REC_RIT_VIS_NULL);
    }

    public String getAdeImpRecRitVisNullFormatted() {
        return Functions.padBlanks(getAdeImpRecRitVisNull(), Len.ADE_IMP_REC_RIT_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_IMP_REC_RIT_VIS = 1;
        public static final int ADE_IMP_REC_RIT_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_IMP_REC_RIT_VIS = 8;
        public static final int ADE_IMP_REC_RIT_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_IMP_REC_RIT_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_IMP_REC_RIT_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
