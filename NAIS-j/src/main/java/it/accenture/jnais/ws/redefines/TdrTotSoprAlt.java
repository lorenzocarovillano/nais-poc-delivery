package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-SOPR-ALT<br>
 * Variable: TDR-TOT-SOPR-ALT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotSoprAlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotSoprAlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_SOPR_ALT;
    }

    public void setTdrTotSoprAlt(AfDecimal tdrTotSoprAlt) {
        writeDecimalAsPacked(Pos.TDR_TOT_SOPR_ALT, tdrTotSoprAlt.copy());
    }

    public void setTdrTotSoprAltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_SOPR_ALT, Pos.TDR_TOT_SOPR_ALT);
    }

    /**Original name: TDR-TOT-SOPR-ALT<br>*/
    public AfDecimal getTdrTotSoprAlt() {
        return readPackedAsDecimal(Pos.TDR_TOT_SOPR_ALT, Len.Int.TDR_TOT_SOPR_ALT, Len.Fract.TDR_TOT_SOPR_ALT);
    }

    public byte[] getTdrTotSoprAltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_SOPR_ALT, Pos.TDR_TOT_SOPR_ALT);
        return buffer;
    }

    public void setTdrTotSoprAltNull(String tdrTotSoprAltNull) {
        writeString(Pos.TDR_TOT_SOPR_ALT_NULL, tdrTotSoprAltNull, Len.TDR_TOT_SOPR_ALT_NULL);
    }

    /**Original name: TDR-TOT-SOPR-ALT-NULL<br>*/
    public String getTdrTotSoprAltNull() {
        return readString(Pos.TDR_TOT_SOPR_ALT_NULL, Len.TDR_TOT_SOPR_ALT_NULL);
    }

    public String getTdrTotSoprAltNullFormatted() {
        return Functions.padBlanks(getTdrTotSoprAltNull(), Len.TDR_TOT_SOPR_ALT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SOPR_ALT = 1;
        public static final int TDR_TOT_SOPR_ALT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SOPR_ALT = 8;
        public static final int TDR_TOT_SOPR_ALT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SOPR_ALT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SOPR_ALT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
