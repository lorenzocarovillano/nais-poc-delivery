package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-PRE-VERS<br>
 * Variable: WP67-PRE-VERS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67PreVers extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67PreVers() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_PRE_VERS;
    }

    public void setWp67PreVers(AfDecimal wp67PreVers) {
        writeDecimalAsPacked(Pos.WP67_PRE_VERS, wp67PreVers.copy());
    }

    public void setWp67PreVersFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_PRE_VERS, Pos.WP67_PRE_VERS);
    }

    /**Original name: WP67-PRE-VERS<br>*/
    public AfDecimal getWp67PreVers() {
        return readPackedAsDecimal(Pos.WP67_PRE_VERS, Len.Int.WP67_PRE_VERS, Len.Fract.WP67_PRE_VERS);
    }

    public byte[] getWp67PreVersAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_PRE_VERS, Pos.WP67_PRE_VERS);
        return buffer;
    }

    public void setWp67PreVersNull(String wp67PreVersNull) {
        writeString(Pos.WP67_PRE_VERS_NULL, wp67PreVersNull, Len.WP67_PRE_VERS_NULL);
    }

    /**Original name: WP67-PRE-VERS-NULL<br>*/
    public String getWp67PreVersNull() {
        return readString(Pos.WP67_PRE_VERS_NULL, Len.WP67_PRE_VERS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_PRE_VERS = 1;
        public static final int WP67_PRE_VERS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_PRE_VERS = 8;
        public static final int WP67_PRE_VERS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_PRE_VERS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_PRE_VERS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
