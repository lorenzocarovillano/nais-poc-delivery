package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPB-CNBT-INPSTFM<br>
 * Variable: S089-IMPB-CNBT-INPSTFM from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpbCnbtInpstfm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpbCnbtInpstfm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPB_CNBT_INPSTFM;
    }

    public void setWlquImpbCnbtInpstfm(AfDecimal wlquImpbCnbtInpstfm) {
        writeDecimalAsPacked(Pos.S089_IMPB_CNBT_INPSTFM, wlquImpbCnbtInpstfm.copy());
    }

    public void setWlquImpbCnbtInpstfmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPB_CNBT_INPSTFM, Pos.S089_IMPB_CNBT_INPSTFM);
    }

    /**Original name: WLQU-IMPB-CNBT-INPSTFM<br>*/
    public AfDecimal getWlquImpbCnbtInpstfm() {
        return readPackedAsDecimal(Pos.S089_IMPB_CNBT_INPSTFM, Len.Int.WLQU_IMPB_CNBT_INPSTFM, Len.Fract.WLQU_IMPB_CNBT_INPSTFM);
    }

    public byte[] getWlquImpbCnbtInpstfmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPB_CNBT_INPSTFM, Pos.S089_IMPB_CNBT_INPSTFM);
        return buffer;
    }

    public void initWlquImpbCnbtInpstfmSpaces() {
        fill(Pos.S089_IMPB_CNBT_INPSTFM, Len.S089_IMPB_CNBT_INPSTFM, Types.SPACE_CHAR);
    }

    public void setWlquImpbCnbtInpstfmNull(String wlquImpbCnbtInpstfmNull) {
        writeString(Pos.S089_IMPB_CNBT_INPSTFM_NULL, wlquImpbCnbtInpstfmNull, Len.WLQU_IMPB_CNBT_INPSTFM_NULL);
    }

    /**Original name: WLQU-IMPB-CNBT-INPSTFM-NULL<br>*/
    public String getWlquImpbCnbtInpstfmNull() {
        return readString(Pos.S089_IMPB_CNBT_INPSTFM_NULL, Len.WLQU_IMPB_CNBT_INPSTFM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPB_CNBT_INPSTFM = 1;
        public static final int S089_IMPB_CNBT_INPSTFM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPB_CNBT_INPSTFM = 8;
        public static final int WLQU_IMPB_CNBT_INPSTFM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_CNBT_INPSTFM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_CNBT_INPSTFM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
