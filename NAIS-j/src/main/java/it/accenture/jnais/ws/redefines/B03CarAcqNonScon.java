package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CAR-ACQ-NON-SCON<br>
 * Variable: B03-CAR-ACQ-NON-SCON from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CarAcqNonScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CarAcqNonScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CAR_ACQ_NON_SCON;
    }

    public void setB03CarAcqNonScon(AfDecimal b03CarAcqNonScon) {
        writeDecimalAsPacked(Pos.B03_CAR_ACQ_NON_SCON, b03CarAcqNonScon.copy());
    }

    public void setB03CarAcqNonSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CAR_ACQ_NON_SCON, Pos.B03_CAR_ACQ_NON_SCON);
    }

    /**Original name: B03-CAR-ACQ-NON-SCON<br>*/
    public AfDecimal getB03CarAcqNonScon() {
        return readPackedAsDecimal(Pos.B03_CAR_ACQ_NON_SCON, Len.Int.B03_CAR_ACQ_NON_SCON, Len.Fract.B03_CAR_ACQ_NON_SCON);
    }

    public byte[] getB03CarAcqNonSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CAR_ACQ_NON_SCON, Pos.B03_CAR_ACQ_NON_SCON);
        return buffer;
    }

    public void setB03CarAcqNonSconNull(String b03CarAcqNonSconNull) {
        writeString(Pos.B03_CAR_ACQ_NON_SCON_NULL, b03CarAcqNonSconNull, Len.B03_CAR_ACQ_NON_SCON_NULL);
    }

    /**Original name: B03-CAR-ACQ-NON-SCON-NULL<br>*/
    public String getB03CarAcqNonSconNull() {
        return readString(Pos.B03_CAR_ACQ_NON_SCON_NULL, Len.B03_CAR_ACQ_NON_SCON_NULL);
    }

    public String getB03CarAcqNonSconNullFormatted() {
        return Functions.padBlanks(getB03CarAcqNonSconNull(), Len.B03_CAR_ACQ_NON_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CAR_ACQ_NON_SCON = 1;
        public static final int B03_CAR_ACQ_NON_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CAR_ACQ_NON_SCON = 8;
        public static final int B03_CAR_ACQ_NON_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CAR_ACQ_NON_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CAR_ACQ_NON_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
