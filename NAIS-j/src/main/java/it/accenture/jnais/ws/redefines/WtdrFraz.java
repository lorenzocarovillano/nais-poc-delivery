package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-FRAZ<br>
 * Variable: WTDR-FRAZ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_FRAZ;
    }

    public void setWtdrFraz(int wtdrFraz) {
        writeIntAsPacked(Pos.WTDR_FRAZ, wtdrFraz, Len.Int.WTDR_FRAZ);
    }

    /**Original name: WTDR-FRAZ<br>*/
    public int getWtdrFraz() {
        return readPackedAsInt(Pos.WTDR_FRAZ, Len.Int.WTDR_FRAZ);
    }

    public void setWtdrFrazNull(String wtdrFrazNull) {
        writeString(Pos.WTDR_FRAZ_NULL, wtdrFrazNull, Len.WTDR_FRAZ_NULL);
    }

    /**Original name: WTDR-FRAZ-NULL<br>*/
    public String getWtdrFrazNull() {
        return readString(Pos.WTDR_FRAZ_NULL, Len.WTDR_FRAZ_NULL);
    }

    public String getWtdrFrazNullFormatted() {
        return Functions.padBlanks(getWtdrFrazNull(), Len.WTDR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_FRAZ = 1;
        public static final int WTDR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_FRAZ = 3;
        public static final int WTDR_FRAZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_FRAZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
