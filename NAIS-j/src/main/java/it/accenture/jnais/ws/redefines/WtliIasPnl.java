package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-IAS-PNL<br>
 * Variable: WTLI-IAS-PNL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliIasPnl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliIasPnl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_IAS_PNL;
    }

    public void setWtliIasPnl(AfDecimal wtliIasPnl) {
        writeDecimalAsPacked(Pos.WTLI_IAS_PNL, wtliIasPnl.copy());
    }

    public void setWtliIasPnlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_IAS_PNL, Pos.WTLI_IAS_PNL);
    }

    /**Original name: WTLI-IAS-PNL<br>*/
    public AfDecimal getWtliIasPnl() {
        return readPackedAsDecimal(Pos.WTLI_IAS_PNL, Len.Int.WTLI_IAS_PNL, Len.Fract.WTLI_IAS_PNL);
    }

    public byte[] getWtliIasPnlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_IAS_PNL, Pos.WTLI_IAS_PNL);
        return buffer;
    }

    public void initWtliIasPnlSpaces() {
        fill(Pos.WTLI_IAS_PNL, Len.WTLI_IAS_PNL, Types.SPACE_CHAR);
    }

    public void setWtliIasPnlNull(String wtliIasPnlNull) {
        writeString(Pos.WTLI_IAS_PNL_NULL, wtliIasPnlNull, Len.WTLI_IAS_PNL_NULL);
    }

    /**Original name: WTLI-IAS-PNL-NULL<br>*/
    public String getWtliIasPnlNull() {
        return readString(Pos.WTLI_IAS_PNL_NULL, Len.WTLI_IAS_PNL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_IAS_PNL = 1;
        public static final int WTLI_IAS_PNL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_IAS_PNL = 8;
        public static final int WTLI_IAS_PNL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_IAS_PNL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_IAS_PNL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
