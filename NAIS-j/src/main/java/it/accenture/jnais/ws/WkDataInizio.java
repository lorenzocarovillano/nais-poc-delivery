package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-DATA-INIZIO<br>
 * Variable: WK-DATA-INIZIO from program LVVS2720<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkDataInizio {

    //==== PROPERTIES ====
    //Original name: WK-ANNO-INI
    private String annoIni = DefaultValues.stringVal(Len.ANNO_INI);
    //Original name: WK-MESE-INI
    private String meseIni = DefaultValues.stringVal(Len.MESE_INI);
    //Original name: WK-GG-INI
    private String ggIni = DefaultValues.stringVal(Len.GG_INI);

    //==== METHODS ====
    public byte[] getWkDataInizioBytes() {
        byte[] buffer = new byte[Len.WK_DATA_INIZIO];
        return getWkDataInizioBytes(buffer, 1);
    }

    public byte[] getWkDataInizioBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, annoIni, Len.ANNO_INI);
        position += Len.ANNO_INI;
        MarshalByte.writeString(buffer, position, meseIni, Len.MESE_INI);
        position += Len.MESE_INI;
        MarshalByte.writeString(buffer, position, ggIni, Len.GG_INI);
        return buffer;
    }

    public void setAnnoIniFormatted(String annoIni) {
        this.annoIni = Trunc.toUnsignedNumeric(annoIni, Len.ANNO_INI);
    }

    public short getAnnoIni() {
        return NumericDisplay.asShort(this.annoIni);
    }

    public void setMeseIni(short meseIni) {
        this.meseIni = NumericDisplay.asString(meseIni, Len.MESE_INI);
    }

    public short getMeseIni() {
        return NumericDisplay.asShort(this.meseIni);
    }

    public void setGgIni(short ggIni) {
        this.ggIni = NumericDisplay.asString(ggIni, Len.GG_INI);
    }

    public short getGgIni() {
        return NumericDisplay.asShort(this.ggIni);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ANNO_INI = 4;
        public static final int MESE_INI = 2;
        public static final int GG_INI = 2;
        public static final int WK_DATA_INIZIO = ANNO_INI + MESE_INI + GG_INI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
