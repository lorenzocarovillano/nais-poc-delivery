package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVSXXXX<br>
 * Generated as a class for rule WS.<br>*/
public class LvvsxxxxData {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVSXXXX";
    //Original name: IDBSPRE0
    private String idbspre0 = "IDBSPRE0";
    /**Original name: WS-SOM-IMP-PRE<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wsSomImpPre = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: PREST
    private PrestIdbspre0 prest = new PrestIdbspre0();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getIdbspre0() {
        return this.idbspre0;
    }

    public void setWsSomImpPre(AfDecimal wsSomImpPre) {
        this.wsSomImpPre.assign(wsSomImpPre);
    }

    public AfDecimal getWsSomImpPre() {
        return this.wsSomImpPre.copy();
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public PrestIdbspre0 getPrest() {
        return prest;
    }
}
