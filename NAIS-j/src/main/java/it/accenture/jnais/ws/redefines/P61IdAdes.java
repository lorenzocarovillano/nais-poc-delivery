package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P61-ID-ADES<br>
 * Variable: P61-ID-ADES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P61IdAdes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P61IdAdes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P61_ID_ADES;
    }

    public void setP61IdAdes(int p61IdAdes) {
        writeIntAsPacked(Pos.P61_ID_ADES, p61IdAdes, Len.Int.P61_ID_ADES);
    }

    public void setP61IdAdesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P61_ID_ADES, Pos.P61_ID_ADES);
    }

    /**Original name: P61-ID-ADES<br>*/
    public int getP61IdAdes() {
        return readPackedAsInt(Pos.P61_ID_ADES, Len.Int.P61_ID_ADES);
    }

    public byte[] getP61IdAdesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P61_ID_ADES, Pos.P61_ID_ADES);
        return buffer;
    }

    public void setP61IdAdesNull(String p61IdAdesNull) {
        writeString(Pos.P61_ID_ADES_NULL, p61IdAdesNull, Len.P61_ID_ADES_NULL);
    }

    /**Original name: P61-ID-ADES-NULL<br>*/
    public String getP61IdAdesNull() {
        return readString(Pos.P61_ID_ADES_NULL, Len.P61_ID_ADES_NULL);
    }

    public String getP61IdAdesNullFormatted() {
        return Functions.padBlanks(getP61IdAdesNull(), Len.P61_ID_ADES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P61_ID_ADES = 1;
        public static final int P61_ID_ADES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_ID_ADES = 5;
        public static final int P61_ID_ADES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
