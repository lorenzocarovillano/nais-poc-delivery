package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-1O-PER-MM<br>
 * Variable: WB03-DUR-1O-PER-MM from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03Dur1oPerMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03Dur1oPerMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR1O_PER_MM;
    }

    public void setWb03Dur1oPerMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR1O_PER_MM, Pos.WB03_DUR1O_PER_MM);
    }

    /**Original name: WB03-DUR-1O-PER-MM<br>*/
    public int getWb03Dur1oPerMm() {
        return readPackedAsInt(Pos.WB03_DUR1O_PER_MM, Len.Int.WB03_DUR1O_PER_MM);
    }

    public byte[] getWb03Dur1oPerMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR1O_PER_MM, Pos.WB03_DUR1O_PER_MM);
        return buffer;
    }

    public void setWb03Dur1oPerMmNull(String wb03Dur1oPerMmNull) {
        writeString(Pos.WB03_DUR1O_PER_MM_NULL, wb03Dur1oPerMmNull, Len.WB03_DUR1O_PER_MM_NULL);
    }

    /**Original name: WB03-DUR-1O-PER-MM-NULL<br>*/
    public String getWb03Dur1oPerMmNull() {
        return readString(Pos.WB03_DUR1O_PER_MM_NULL, Len.WB03_DUR1O_PER_MM_NULL);
    }

    public String getWb03Dur1oPerMmNullFormatted() {
        return Functions.padBlanks(getWb03Dur1oPerMmNull(), Len.WB03_DUR1O_PER_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR1O_PER_MM = 1;
        public static final int WB03_DUR1O_PER_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR1O_PER_MM = 3;
        public static final int WB03_DUR1O_PER_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR1O_PER_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
