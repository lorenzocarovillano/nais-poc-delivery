package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.AnaBlocco;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.MoviBatchSosp;
import it.accenture.jnais.copy.OggBlocco;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.enums.WsTpStatBlocco;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0024<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0024Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LCCS0024";
    //Original name: WK-TABELLA
    private String wkTabella = "";
    /**Original name: WS-TP-STAT-BLOCCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*
	 * --  TIPO STATO BLOCCO</pre>*/
    private WsTpStatBlocco wsTpStatBlocco = new WsTpStatBlocco();
    //Original name: IX-MOV-SOSP
    private short ixMovSosp = ((short)0);
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    /**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    //Original name: AREA-IO-LCCS0090
    private LinkArea areaIoLccs0090 = new LinkArea();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: ANA-BLOCCO
    private AnaBlocco anaBlocco = new AnaBlocco();
    //Original name: OGG-BLOCCO
    private OggBlocco oggBlocco = new OggBlocco();
    //Original name: MOVI-BATCH-SOSP
    private MoviBatchSosp moviBatchSosp = new MoviBatchSosp();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public void setIxMovSosp(short ixMovSosp) {
        this.ixMovSosp = ixMovSosp;
    }

    public short getIxMovSosp() {
        return this.ixMovSosp;
    }

    public AnaBlocco getAnaBlocco() {
        return anaBlocco;
    }

    public LinkArea getAreaIoLccs0090() {
        return areaIoLccs0090;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public MoviBatchSosp getMoviBatchSosp() {
        return moviBatchSosp;
    }

    public OggBlocco getOggBlocco() {
        return oggBlocco;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    public WsTpStatBlocco getWsTpStatBlocco() {
        return wsTpStatBlocco;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TABELLA = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
