package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMP-LRD-LIQTO<br>
 * Variable: LQU-TOT-IMP-LRD-LIQTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpLrdLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpLrdLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMP_LRD_LIQTO;
    }

    public void setLquTotImpLrdLiqto(AfDecimal lquTotImpLrdLiqto) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMP_LRD_LIQTO, lquTotImpLrdLiqto.copy());
    }

    public void setLquTotImpLrdLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMP_LRD_LIQTO, Pos.LQU_TOT_IMP_LRD_LIQTO);
    }

    /**Original name: LQU-TOT-IMP-LRD-LIQTO<br>*/
    public AfDecimal getLquTotImpLrdLiqto() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMP_LRD_LIQTO, Len.Int.LQU_TOT_IMP_LRD_LIQTO, Len.Fract.LQU_TOT_IMP_LRD_LIQTO);
    }

    public byte[] getLquTotImpLrdLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMP_LRD_LIQTO, Pos.LQU_TOT_IMP_LRD_LIQTO);
        return buffer;
    }

    public void setLquTotImpLrdLiqtoNull(String lquTotImpLrdLiqtoNull) {
        writeString(Pos.LQU_TOT_IMP_LRD_LIQTO_NULL, lquTotImpLrdLiqtoNull, Len.LQU_TOT_IMP_LRD_LIQTO_NULL);
    }

    /**Original name: LQU-TOT-IMP-LRD-LIQTO-NULL<br>*/
    public String getLquTotImpLrdLiqtoNull() {
        return readString(Pos.LQU_TOT_IMP_LRD_LIQTO_NULL, Len.LQU_TOT_IMP_LRD_LIQTO_NULL);
    }

    public String getLquTotImpLrdLiqtoNullFormatted() {
        return Functions.padBlanks(getLquTotImpLrdLiqtoNull(), Len.LQU_TOT_IMP_LRD_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_LRD_LIQTO = 1;
        public static final int LQU_TOT_IMP_LRD_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_LRD_LIQTO = 8;
        public static final int LQU_TOT_IMP_LRD_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_LRD_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_LRD_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
