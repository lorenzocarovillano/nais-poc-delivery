package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-TS-TARI-DOV<br>
 * Variable: B03-TS-TARI-DOV from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03TsTariDov extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03TsTariDov() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_TS_TARI_DOV;
    }

    public void setB03TsTariDov(AfDecimal b03TsTariDov) {
        writeDecimalAsPacked(Pos.B03_TS_TARI_DOV, b03TsTariDov.copy());
    }

    public void setB03TsTariDovFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_TS_TARI_DOV, Pos.B03_TS_TARI_DOV);
    }

    /**Original name: B03-TS-TARI-DOV<br>*/
    public AfDecimal getB03TsTariDov() {
        return readPackedAsDecimal(Pos.B03_TS_TARI_DOV, Len.Int.B03_TS_TARI_DOV, Len.Fract.B03_TS_TARI_DOV);
    }

    public byte[] getB03TsTariDovAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_TS_TARI_DOV, Pos.B03_TS_TARI_DOV);
        return buffer;
    }

    public void setB03TsTariDovNull(String b03TsTariDovNull) {
        writeString(Pos.B03_TS_TARI_DOV_NULL, b03TsTariDovNull, Len.B03_TS_TARI_DOV_NULL);
    }

    /**Original name: B03-TS-TARI-DOV-NULL<br>*/
    public String getB03TsTariDovNull() {
        return readString(Pos.B03_TS_TARI_DOV_NULL, Len.B03_TS_TARI_DOV_NULL);
    }

    public String getB03TsTariDovNullFormatted() {
        return Functions.padBlanks(getB03TsTariDovNull(), Len.B03_TS_TARI_DOV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_TS_TARI_DOV = 1;
        public static final int B03_TS_TARI_DOV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_TS_TARI_DOV = 8;
        public static final int B03_TS_TARI_DOV_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_TS_TARI_DOV = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_TS_TARI_DOV = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
