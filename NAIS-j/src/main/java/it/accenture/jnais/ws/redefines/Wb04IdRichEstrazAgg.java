package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB04-ID-RICH-ESTRAZ-AGG<br>
 * Variable: WB04-ID-RICH-ESTRAZ-AGG from program LLBS0266<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb04IdRichEstrazAgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb04IdRichEstrazAgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB04_ID_RICH_ESTRAZ_AGG;
    }

    public void setWb04IdRichEstrazAgg(int wb04IdRichEstrazAgg) {
        writeIntAsPacked(Pos.WB04_ID_RICH_ESTRAZ_AGG, wb04IdRichEstrazAgg, Len.Int.WB04_ID_RICH_ESTRAZ_AGG);
    }

    public void setWb04IdRichEstrazAggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB04_ID_RICH_ESTRAZ_AGG, Pos.WB04_ID_RICH_ESTRAZ_AGG);
    }

    /**Original name: WB04-ID-RICH-ESTRAZ-AGG<br>*/
    public int getWb04IdRichEstrazAgg() {
        return readPackedAsInt(Pos.WB04_ID_RICH_ESTRAZ_AGG, Len.Int.WB04_ID_RICH_ESTRAZ_AGG);
    }

    public byte[] getWb04IdRichEstrazAggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB04_ID_RICH_ESTRAZ_AGG, Pos.WB04_ID_RICH_ESTRAZ_AGG);
        return buffer;
    }

    public void setWb04IdRichEstrazAggNull(String wb04IdRichEstrazAggNull) {
        writeString(Pos.WB04_ID_RICH_ESTRAZ_AGG_NULL, wb04IdRichEstrazAggNull, Len.WB04_ID_RICH_ESTRAZ_AGG_NULL);
    }

    /**Original name: WB04-ID-RICH-ESTRAZ-AGG-NULL<br>*/
    public String getWb04IdRichEstrazAggNull() {
        return readString(Pos.WB04_ID_RICH_ESTRAZ_AGG_NULL, Len.WB04_ID_RICH_ESTRAZ_AGG_NULL);
    }

    public String getWb04IdRichEstrazAggNullFormatted() {
        return Functions.padBlanks(getWb04IdRichEstrazAggNull(), Len.WB04_ID_RICH_ESTRAZ_AGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB04_ID_RICH_ESTRAZ_AGG = 1;
        public static final int WB04_ID_RICH_ESTRAZ_AGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB04_ID_RICH_ESTRAZ_AGG = 5;
        public static final int WB04_ID_RICH_ESTRAZ_AGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB04_ID_RICH_ESTRAZ_AGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
