package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-NUM-PRE-PATT<br>
 * Variable: WB03-NUM-PRE-PATT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03NumPrePatt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03NumPrePatt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_NUM_PRE_PATT;
    }

    public void setWb03NumPrePattFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_NUM_PRE_PATT, Pos.WB03_NUM_PRE_PATT);
    }

    /**Original name: WB03-NUM-PRE-PATT<br>*/
    public int getWb03NumPrePatt() {
        return readPackedAsInt(Pos.WB03_NUM_PRE_PATT, Len.Int.WB03_NUM_PRE_PATT);
    }

    public byte[] getWb03NumPrePattAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_NUM_PRE_PATT, Pos.WB03_NUM_PRE_PATT);
        return buffer;
    }

    public void setWb03NumPrePattNull(String wb03NumPrePattNull) {
        writeString(Pos.WB03_NUM_PRE_PATT_NULL, wb03NumPrePattNull, Len.WB03_NUM_PRE_PATT_NULL);
    }

    /**Original name: WB03-NUM-PRE-PATT-NULL<br>*/
    public String getWb03NumPrePattNull() {
        return readString(Pos.WB03_NUM_PRE_PATT_NULL, Len.WB03_NUM_PRE_PATT_NULL);
    }

    public String getWb03NumPrePattNullFormatted() {
        return Functions.padBlanks(getWb03NumPrePattNull(), Len.WB03_NUM_PRE_PATT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_NUM_PRE_PATT = 1;
        public static final int WB03_NUM_PRE_PATT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_NUM_PRE_PATT = 3;
        public static final int WB03_NUM_PRE_PATT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_NUM_PRE_PATT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
