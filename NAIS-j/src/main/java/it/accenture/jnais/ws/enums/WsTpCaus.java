package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-CAUS<br>
 * Variable: WS-TP-CAUS from copybook LCCVXCA0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpCaus {

    //==== PROPERTIES ====
    private String value = "";
    public static final String RIDOTTA = "RI";
    public static final String ATTESA_COMUNIC_PAG_REN = "AE";
    public static final String RIATTIVATA = "RA";
    public static final String RISCATTO_TOTALE_LIMITATO = "RL";
    public static final String RISCATTO_TOTALE_LIMITATO_RIII = "LM";
    public static final String ALTRE_CAUSE = "AC";
    public static final String SINISTRO_PRIMO_ASSICURATO = "SP";
    public static final String SINISTRO_TERMINE_FISSO = "ST";
    public static final String PAG_TRASF_POSIZ_PREV = "PV";
    public static final String ATTESA_LIQ_RENDITA = "AR";
    public static final String DIFFERITA = "DF";
    public static final String PROROGATA = "PR";
    public static final String CAUS_ATTESA_ATTIVAZIONE = "AA";
    public static final String SENZA_SEGUITO = "SS";
    public static final String ATTESA_PERFEZIONAMENTO = "AP";
    public static final String REVOCATO = "RE";
    public static final String REVOCATO_NOPERF = "RV";
    public static final String INSOLVENZA = "IN";
    public static final String MANCATO_PERFEZIONAMENTO = "MP";
    public static final String MANCATO_RINNOVO = "MR";
    public static final String STORNATO_TRASFORMAZIONE = "TR";
    public static final String RECESSO = "RO";
    public static final String RECESSO_NOPERF = "RC";
    public static final String SINISTRO = "SI";
    public static final String LIQUID_RECESSO = "LR";
    public static final String LIQUID_RIMBORSO_PREMI = "LP";
    public static final String LIQUID_RISCATTO_TOTALE = "LT";
    public static final String LIQUID_RISCATTO_TOTALE_ORD = "LD";
    public static final String LIQUID_RISCATTO_TOTALE_SPE = "LC";
    public static final String LIQUID_RISCATTO_TOTALE_VOL = "LN";
    public static final String LIQUID_RISCATTO_TOTALE_DIF = "LF";
    public static final String LIQUID_RISCATTO_TOTALE_ANT = "LU";
    public static final String LIQUID_ANTICIPAZIONE = "LG";
    public static final String LIQUID_RISCATTO_PARZIALE = "LL";
    public static final String LIQUID_RISCATTO_PARZIALE_DIF = "LK";
    public static final String LIQUID_RISCATTO_UFFICIO = "RU";
    public static final String LIQUID_SCADENZA_ANTICIPATA = "LA";
    public static final String LIQUID_SCADENZA = "LZ";
    public static final String LIQUID_SCADENZA_ECC_CONTR = "LH";
    public static final String LIQUID_SCADENZA_ECC_RIII = "LE";
    public static final String LIQUID_SINISTRO = "LS";
    public static final String LIQUID_SINISTRO_MORTE = "LX";
    public static final String LIQUID_REVOCA = "LV";
    public static final String LIQUID_TRASF_POSIZ_PREV = "LO";
    public static final String LIQUID_TRASF_POSIZ_PREV_RIS = "LI";
    public static final String RENDITA_EROGAZIONE = "RZ";
    public static final String RIMBORSO_PREMI = "RP";
    public static final String RISCATTO_PARZIALE = "RT";
    public static final String RISCATTO_TOTALE = "TT";
    public static final String RISCATTO_UFFICIO = "RU";
    public static final String SCADENZA = "SC";
    public static final String SCADENZA_ANTICIPATA = "SA";
    public static final String STABILIZZATA = "SZ";
    public static final String TRASF_POSIZ_PREV = "TP";
    public static final String PAGAMENTO_RECESSO = "PO";
    public static final String PAGAMENTO_REVOCA = "PH";
    public static final String PAGAMENT_RISCATTO_PARZIALE = "PX";
    public static final String PAGAMENT_RISCATTO_TOTALE = "PT";
    public static final String PAGAMENT_RISCATTO_UFFICIO = "LY";
    public static final String PAGAMENTO_SINISTRO = "PS";
    public static final String PAGAMENTO_SCAD_ANTICIPATA = "PA";
    public static final String PAGAMENTO_SCADENZA = "PM";
    public static final String PAGAMENTO_RIMBORSO_TRANCHE = "PP";
    public static final String PERFEZIONATA = "PZ";
    public static final String COMUNICAZIONE_ISTRUTTORIA = "CI";
    public static final String COMUNICAZIONE_SENZA_ISTRUT = "SO";
    public static final String ISTRUTTORIA_COMPETATA = "IT";
    public static final String LIQUIDAZIONE_COMPLETATA = "LQ";
    public static final String PAGAMENTO_EFFETTUATO = "PE";
    public static final String SINISTRO_RIFIUTATO = "SR";
    public static final String PRENOTATO_SCADENZA = "PN";
    public static final String IN_DEROGA = "DE";
    public static final String PAGAMENTO_IN_ATTESA_CONFERMA = "PI";
    public static final String PAGAMENTO_PARZIALE = "P1";
    public static final String STORNO_CEDOLA = "CE";
    public static final String STORNO_SWITCH_FND = "SW";
    public static final String CONCLUSIONE_EROG_RENDITA = "CR";
    public static final String RENDITA_EROG_REVISIONARIO = "RR";
    public static final String RENDITA_SOSP_ACCERTAMENTI = "RS";
    public static final String DORMIENTE_NON_LIQUIDABILE = "DL";
    public static final String ATTESA_PAG_MEF = "AM";
    public static final String CORRETTA_IN_ATTESA_DI_PREMIO = "CP";
    public static final String CORRETTA_EMETTIBILE = "EM";
    public static final String ATTESA_LIQ_REN_PRIMO_ASSIC = "AS";
    public static final String ATTESA_COM_PAG_REN_PRIMO_ASS = "AT";
    public static final String SINISTRO_TERM_FISS_PRIMO_ASS = "SF";
    public static final String ATTESA_LIQ_POL_COL = "AL";
    public static final String SINISTRO_SU_RENDITA_IN_EROGAZ = "SE";
    public static final String SCAD_RISCATTO_TOT_LIMITATO = "SL";
    public static final String LIQ_SCAD_RISC_TOT_LIMITATO = "LB";
    public static final String SINISTRO_RISC_TOT_LIMITATO = "SG";
    public static final String SINISTRO_SU_REVERSIONARIO = "SY";
    public static final String PRESCRITTO_COMPAGNIA = "PC";
    public static final String DA_PAGARE = "DP";
    public static final String STORNO_PER_REVISIONE = "SV";
    public static final String PAGAMENTO_DISPOSTO = "PD";
    public static final String STORNO_PER_DISDETTA = "DS";
    public static final String STORNO_PER_EST_ANTICIPATA = "EA";
    public static final String LIQUID_PER_DISDETTA = "L1";
    public static final String LIQUID_PER_EST_ANTICIPATA = "L2";
    public static final String PAGAMENTO_PER_DISDETTA = "PK";
    public static final String PAGAMENTO_PER_EST_ANT = "PL";
    public static final String PAGAMENTO_PER_RIMBORSO = "PJ";
    public static final String STORNO_PER_DISDETTA_COMM = "DC";
    public static final String STORNO_MANCATO_RINNOVO_TACITO = "SM";
    public static final String SINISTRO_RENDITA_DA_EROGARE = "ER";

    //==== METHODS ====
    public void setWsTpCaus(String wsTpCaus) {
        this.value = Functions.subString(wsTpCaus, Len.WS_TP_CAUS);
    }

    public String getWsTpCaus() {
        return this.value;
    }

    public boolean isRidotta() {
        return value.equals(RIDOTTA);
    }

    public boolean isAttesaComunicPagRen() {
        return value.equals(ATTESA_COMUNIC_PAG_REN);
    }

    public boolean isAttesaLiqRendita() {
        return value.equals(ATTESA_LIQ_RENDITA);
    }

    public boolean isProrogata() {
        return value.equals(PROROGATA);
    }

    public boolean isAttesaPerfezionamento() {
        return value.equals(ATTESA_PERFEZIONAMENTO);
    }

    public void setAttesaPerfezionamento() {
        value = ATTESA_PERFEZIONAMENTO;
    }

    public boolean isInsolvenza() {
        return value.equals(INSOLVENZA);
    }

    public boolean isRenditaErogazione() {
        return value.equals(RENDITA_EROGAZIONE);
    }

    public void setRenditaErogazione() {
        value = RENDITA_EROGAZIONE;
    }

    public boolean isRiscattoTotale() {
        return value.equals(RISCATTO_TOTALE);
    }

    public boolean isPagamentRiscattoTotale() {
        return value.equals(PAGAMENT_RISCATTO_TOTALE);
    }

    public boolean isPerfezionata() {
        return value.equals(PERFEZIONATA);
    }

    public boolean isRenditaErogRevisionario() {
        return value.equals(RENDITA_EROG_REVISIONARIO);
    }

    public boolean isAttesaLiqRenPrimoAssic() {
        return value.equals(ATTESA_LIQ_REN_PRIMO_ASSIC);
    }

    public boolean isAttesaComPagRenPrimoAss() {
        return value.equals(ATTESA_COM_PAG_REN_PRIMO_ASS);
    }

    public boolean isSinistroRenditaDaErogare() {
        return value.equals(SINISTRO_RENDITA_DA_EROGARE);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_CAUS = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
