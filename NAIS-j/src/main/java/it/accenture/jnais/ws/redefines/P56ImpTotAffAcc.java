package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-IMP-TOT-AFF-ACC<br>
 * Variable: P56-IMP-TOT-AFF-ACC from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56ImpTotAffAcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56ImpTotAffAcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_IMP_TOT_AFF_ACC;
    }

    public void setP56ImpTotAffAcc(AfDecimal p56ImpTotAffAcc) {
        writeDecimalAsPacked(Pos.P56_IMP_TOT_AFF_ACC, p56ImpTotAffAcc.copy());
    }

    public void setP56ImpTotAffAccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_IMP_TOT_AFF_ACC, Pos.P56_IMP_TOT_AFF_ACC);
    }

    /**Original name: P56-IMP-TOT-AFF-ACC<br>*/
    public AfDecimal getP56ImpTotAffAcc() {
        return readPackedAsDecimal(Pos.P56_IMP_TOT_AFF_ACC, Len.Int.P56_IMP_TOT_AFF_ACC, Len.Fract.P56_IMP_TOT_AFF_ACC);
    }

    public byte[] getP56ImpTotAffAccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_IMP_TOT_AFF_ACC, Pos.P56_IMP_TOT_AFF_ACC);
        return buffer;
    }

    public void setP56ImpTotAffAccNull(String p56ImpTotAffAccNull) {
        writeString(Pos.P56_IMP_TOT_AFF_ACC_NULL, p56ImpTotAffAccNull, Len.P56_IMP_TOT_AFF_ACC_NULL);
    }

    /**Original name: P56-IMP-TOT-AFF-ACC-NULL<br>*/
    public String getP56ImpTotAffAccNull() {
        return readString(Pos.P56_IMP_TOT_AFF_ACC_NULL, Len.P56_IMP_TOT_AFF_ACC_NULL);
    }

    public String getP56ImpTotAffAccNullFormatted() {
        return Functions.padBlanks(getP56ImpTotAffAccNull(), Len.P56_IMP_TOT_AFF_ACC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_IMP_TOT_AFF_ACC = 1;
        public static final int P56_IMP_TOT_AFF_ACC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_IMP_TOT_AFF_ACC = 8;
        public static final int P56_IMP_TOT_AFF_ACC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_IMP_TOT_AFF_ACC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_IMP_TOT_AFF_ACC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
