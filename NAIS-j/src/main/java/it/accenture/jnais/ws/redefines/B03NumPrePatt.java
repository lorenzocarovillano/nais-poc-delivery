package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-NUM-PRE-PATT<br>
 * Variable: B03-NUM-PRE-PATT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03NumPrePatt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03NumPrePatt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_NUM_PRE_PATT;
    }

    public void setB03NumPrePatt(int b03NumPrePatt) {
        writeIntAsPacked(Pos.B03_NUM_PRE_PATT, b03NumPrePatt, Len.Int.B03_NUM_PRE_PATT);
    }

    public void setB03NumPrePattFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_NUM_PRE_PATT, Pos.B03_NUM_PRE_PATT);
    }

    /**Original name: B03-NUM-PRE-PATT<br>*/
    public int getB03NumPrePatt() {
        return readPackedAsInt(Pos.B03_NUM_PRE_PATT, Len.Int.B03_NUM_PRE_PATT);
    }

    public byte[] getB03NumPrePattAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_NUM_PRE_PATT, Pos.B03_NUM_PRE_PATT);
        return buffer;
    }

    public void setB03NumPrePattNull(String b03NumPrePattNull) {
        writeString(Pos.B03_NUM_PRE_PATT_NULL, b03NumPrePattNull, Len.B03_NUM_PRE_PATT_NULL);
    }

    /**Original name: B03-NUM-PRE-PATT-NULL<br>*/
    public String getB03NumPrePattNull() {
        return readString(Pos.B03_NUM_PRE_PATT_NULL, Len.B03_NUM_PRE_PATT_NULL);
    }

    public String getB03NumPrePattNullFormatted() {
        return Functions.padBlanks(getB03NumPrePattNull(), Len.B03_NUM_PRE_PATT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_NUM_PRE_PATT = 1;
        public static final int B03_NUM_PRE_PATT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_NUM_PRE_PATT = 3;
        public static final int B03_NUM_PRE_PATT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_NUM_PRE_PATT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
