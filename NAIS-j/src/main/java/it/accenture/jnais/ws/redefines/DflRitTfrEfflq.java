package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RIT-TFR-EFFLQ<br>
 * Variable: DFL-RIT-TFR-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflRitTfrEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflRitTfrEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RIT_TFR_EFFLQ;
    }

    public void setDflRitTfrEfflq(AfDecimal dflRitTfrEfflq) {
        writeDecimalAsPacked(Pos.DFL_RIT_TFR_EFFLQ, dflRitTfrEfflq.copy());
    }

    public void setDflRitTfrEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RIT_TFR_EFFLQ, Pos.DFL_RIT_TFR_EFFLQ);
    }

    /**Original name: DFL-RIT-TFR-EFFLQ<br>*/
    public AfDecimal getDflRitTfrEfflq() {
        return readPackedAsDecimal(Pos.DFL_RIT_TFR_EFFLQ, Len.Int.DFL_RIT_TFR_EFFLQ, Len.Fract.DFL_RIT_TFR_EFFLQ);
    }

    public byte[] getDflRitTfrEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RIT_TFR_EFFLQ, Pos.DFL_RIT_TFR_EFFLQ);
        return buffer;
    }

    public void setDflRitTfrEfflqNull(String dflRitTfrEfflqNull) {
        writeString(Pos.DFL_RIT_TFR_EFFLQ_NULL, dflRitTfrEfflqNull, Len.DFL_RIT_TFR_EFFLQ_NULL);
    }

    /**Original name: DFL-RIT-TFR-EFFLQ-NULL<br>*/
    public String getDflRitTfrEfflqNull() {
        return readString(Pos.DFL_RIT_TFR_EFFLQ_NULL, Len.DFL_RIT_TFR_EFFLQ_NULL);
    }

    public String getDflRitTfrEfflqNullFormatted() {
        return Functions.padBlanks(getDflRitTfrEfflqNull(), Len.DFL_RIT_TFR_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RIT_TFR_EFFLQ = 1;
        public static final int DFL_RIT_TFR_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RIT_TFR_EFFLQ = 8;
        public static final int DFL_RIT_TFR_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RIT_TFR_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RIT_TFR_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
