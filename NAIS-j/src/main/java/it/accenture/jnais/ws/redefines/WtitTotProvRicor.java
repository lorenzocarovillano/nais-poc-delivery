package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-PROV-RICOR<br>
 * Variable: WTIT-TOT-PROV-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_PROV_RICOR;
    }

    public void setWtitTotProvRicor(AfDecimal wtitTotProvRicor) {
        writeDecimalAsPacked(Pos.WTIT_TOT_PROV_RICOR, wtitTotProvRicor.copy());
    }

    public void setWtitTotProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_PROV_RICOR, Pos.WTIT_TOT_PROV_RICOR);
    }

    /**Original name: WTIT-TOT-PROV-RICOR<br>*/
    public AfDecimal getWtitTotProvRicor() {
        return readPackedAsDecimal(Pos.WTIT_TOT_PROV_RICOR, Len.Int.WTIT_TOT_PROV_RICOR, Len.Fract.WTIT_TOT_PROV_RICOR);
    }

    public byte[] getWtitTotProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_PROV_RICOR, Pos.WTIT_TOT_PROV_RICOR);
        return buffer;
    }

    public void initWtitTotProvRicorSpaces() {
        fill(Pos.WTIT_TOT_PROV_RICOR, Len.WTIT_TOT_PROV_RICOR, Types.SPACE_CHAR);
    }

    public void setWtitTotProvRicorNull(String wtitTotProvRicorNull) {
        writeString(Pos.WTIT_TOT_PROV_RICOR_NULL, wtitTotProvRicorNull, Len.WTIT_TOT_PROV_RICOR_NULL);
    }

    /**Original name: WTIT-TOT-PROV-RICOR-NULL<br>*/
    public String getWtitTotProvRicorNull() {
        return readString(Pos.WTIT_TOT_PROV_RICOR_NULL, Len.WTIT_TOT_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PROV_RICOR = 1;
        public static final int WTIT_TOT_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PROV_RICOR = 8;
        public static final int WTIT_TOT_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
