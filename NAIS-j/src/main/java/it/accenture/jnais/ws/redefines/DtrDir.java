package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-DIR<br>
 * Variable: DTR-DIR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrDir extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrDir() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_DIR;
    }

    public void setDtrDir(AfDecimal dtrDir) {
        writeDecimalAsPacked(Pos.DTR_DIR, dtrDir.copy());
    }

    public void setDtrDirFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_DIR, Pos.DTR_DIR);
    }

    /**Original name: DTR-DIR<br>*/
    public AfDecimal getDtrDir() {
        return readPackedAsDecimal(Pos.DTR_DIR, Len.Int.DTR_DIR, Len.Fract.DTR_DIR);
    }

    public byte[] getDtrDirAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_DIR, Pos.DTR_DIR);
        return buffer;
    }

    public void setDtrDirNull(String dtrDirNull) {
        writeString(Pos.DTR_DIR_NULL, dtrDirNull, Len.DTR_DIR_NULL);
    }

    /**Original name: DTR-DIR-NULL<br>*/
    public String getDtrDirNull() {
        return readString(Pos.DTR_DIR_NULL, Len.DTR_DIR_NULL);
    }

    public String getDtrDirNullFormatted() {
        return Functions.padBlanks(getDtrDirNull(), Len.DTR_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_DIR = 1;
        public static final int DTR_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_DIR = 8;
        public static final int DTR_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
