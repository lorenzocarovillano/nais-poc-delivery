package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-ADER<br>
 * Variable: TGA-IMP-ADER from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_ADER;
    }

    public void setTgaImpAder(AfDecimal tgaImpAder) {
        writeDecimalAsPacked(Pos.TGA_IMP_ADER, tgaImpAder.copy());
    }

    public void setTgaImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_ADER, Pos.TGA_IMP_ADER);
    }

    /**Original name: TGA-IMP-ADER<br>*/
    public AfDecimal getTgaImpAder() {
        return readPackedAsDecimal(Pos.TGA_IMP_ADER, Len.Int.TGA_IMP_ADER, Len.Fract.TGA_IMP_ADER);
    }

    public byte[] getTgaImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_ADER, Pos.TGA_IMP_ADER);
        return buffer;
    }

    public void setTgaImpAderNull(String tgaImpAderNull) {
        writeString(Pos.TGA_IMP_ADER_NULL, tgaImpAderNull, Len.TGA_IMP_ADER_NULL);
    }

    /**Original name: TGA-IMP-ADER-NULL<br>*/
    public String getTgaImpAderNull() {
        return readString(Pos.TGA_IMP_ADER_NULL, Len.TGA_IMP_ADER_NULL);
    }

    public String getTgaImpAderNullFormatted() {
        return Functions.padBlanks(getTgaImpAderNull(), Len.TGA_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_ADER = 1;
        public static final int TGA_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_ADER = 8;
        public static final int TGA_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
