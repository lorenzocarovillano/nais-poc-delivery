package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-MANFEE-ANTIC<br>
 * Variable: WPAG-MANFEE-ANTIC from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_MANFEE_ANTIC;
    }

    public void setWpagManfeeAntic(AfDecimal wpagManfeeAntic) {
        writeDecimalAsPacked(Pos.WPAG_MANFEE_ANTIC, wpagManfeeAntic.copy());
    }

    public void setWpagManfeeAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_MANFEE_ANTIC, Pos.WPAG_MANFEE_ANTIC);
    }

    /**Original name: WPAG-MANFEE-ANTIC<br>*/
    public AfDecimal getWpagManfeeAntic() {
        return readPackedAsDecimal(Pos.WPAG_MANFEE_ANTIC, Len.Int.WPAG_MANFEE_ANTIC, Len.Fract.WPAG_MANFEE_ANTIC);
    }

    public byte[] getWpagManfeeAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_MANFEE_ANTIC, Pos.WPAG_MANFEE_ANTIC);
        return buffer;
    }

    public void initWpagManfeeAnticSpaces() {
        fill(Pos.WPAG_MANFEE_ANTIC, Len.WPAG_MANFEE_ANTIC, Types.SPACE_CHAR);
    }

    public void setWpagManfeeAnticNull(String wpagManfeeAnticNull) {
        writeString(Pos.WPAG_MANFEE_ANTIC_NULL, wpagManfeeAnticNull, Len.WPAG_MANFEE_ANTIC_NULL);
    }

    /**Original name: WPAG-MANFEE-ANTIC-NULL<br>*/
    public String getWpagManfeeAnticNull() {
        return readString(Pos.WPAG_MANFEE_ANTIC_NULL, Len.WPAG_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_MANFEE_ANTIC = 1;
        public static final int WPAG_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_MANFEE_ANTIC = 8;
        public static final int WPAG_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
