package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPRE-SDO-INTR<br>
 * Variable: WPRE-SDO-INTR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreSdoIntr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpreSdoIntr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_SDO_INTR;
    }

    public void setWpreSdoIntr(AfDecimal wpreSdoIntr) {
        writeDecimalAsPacked(Pos.WPRE_SDO_INTR, wpreSdoIntr.copy());
    }

    public void setWpreSdoIntrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_SDO_INTR, Pos.WPRE_SDO_INTR);
    }

    /**Original name: WPRE-SDO-INTR<br>*/
    public AfDecimal getWpreSdoIntr() {
        return readPackedAsDecimal(Pos.WPRE_SDO_INTR, Len.Int.WPRE_SDO_INTR, Len.Fract.WPRE_SDO_INTR);
    }

    public byte[] getWpreSdoIntrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_SDO_INTR, Pos.WPRE_SDO_INTR);
        return buffer;
    }

    public void initWpreSdoIntrSpaces() {
        fill(Pos.WPRE_SDO_INTR, Len.WPRE_SDO_INTR, Types.SPACE_CHAR);
    }

    public void setWpreSdoIntrNull(String wpreSdoIntrNull) {
        writeString(Pos.WPRE_SDO_INTR_NULL, wpreSdoIntrNull, Len.WPRE_SDO_INTR_NULL);
    }

    /**Original name: WPRE-SDO-INTR-NULL<br>*/
    public String getWpreSdoIntrNull() {
        return readString(Pos.WPRE_SDO_INTR_NULL, Len.WPRE_SDO_INTR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_SDO_INTR = 1;
        public static final int WPRE_SDO_INTR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_SDO_INTR = 8;
        public static final int WPRE_SDO_INTR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPRE_SDO_INTR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_SDO_INTR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
