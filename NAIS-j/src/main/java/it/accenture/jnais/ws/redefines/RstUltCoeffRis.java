package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-ULT-COEFF-RIS<br>
 * Variable: RST-ULT-COEFF-RIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstUltCoeffRis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstUltCoeffRis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_ULT_COEFF_RIS;
    }

    public void setRstUltCoeffRis(AfDecimal rstUltCoeffRis) {
        writeDecimalAsPacked(Pos.RST_ULT_COEFF_RIS, rstUltCoeffRis.copy());
    }

    public void setRstUltCoeffRisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_ULT_COEFF_RIS, Pos.RST_ULT_COEFF_RIS);
    }

    /**Original name: RST-ULT-COEFF-RIS<br>*/
    public AfDecimal getRstUltCoeffRis() {
        return readPackedAsDecimal(Pos.RST_ULT_COEFF_RIS, Len.Int.RST_ULT_COEFF_RIS, Len.Fract.RST_ULT_COEFF_RIS);
    }

    public byte[] getRstUltCoeffRisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_ULT_COEFF_RIS, Pos.RST_ULT_COEFF_RIS);
        return buffer;
    }

    public void setRstUltCoeffRisNull(String rstUltCoeffRisNull) {
        writeString(Pos.RST_ULT_COEFF_RIS_NULL, rstUltCoeffRisNull, Len.RST_ULT_COEFF_RIS_NULL);
    }

    /**Original name: RST-ULT-COEFF-RIS-NULL<br>*/
    public String getRstUltCoeffRisNull() {
        return readString(Pos.RST_ULT_COEFF_RIS_NULL, Len.RST_ULT_COEFF_RIS_NULL);
    }

    public String getRstUltCoeffRisNullFormatted() {
        return Functions.padBlanks(getRstUltCoeffRisNull(), Len.RST_ULT_COEFF_RIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_ULT_COEFF_RIS = 1;
        public static final int RST_ULT_COEFF_RIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_ULT_COEFF_RIS = 8;
        public static final int RST_ULT_COEFF_RIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_ULT_COEFF_RIS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_ULT_COEFF_RIS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
