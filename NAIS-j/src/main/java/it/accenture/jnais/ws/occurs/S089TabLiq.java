package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvlqu1;
import it.accenture.jnais.copy.S089Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: S089-TAB-LIQ<br>
 * Variables: S089-TAB-LIQ from copybook LCCVLQUA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class S089TabLiq {

    //==== PROPERTIES ====
    //Original name: LCCVLQU1
    private Lccvlqu1 lccvlqu1 = new Lccvlqu1();

    //==== METHODS ====
    public void setWlquTabLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvlqu1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvlqu1.setWlquIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvlqu1.Len.Int.WLQU_ID_PTF, 0));
        position += Lccvlqu1.Len.WLQU_ID_PTF;
        lccvlqu1.getDati().setWlquDatiBytes(buffer, position);
    }

    public byte[] getWlquTabLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvlqu1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvlqu1.getWlquIdPtf(), Lccvlqu1.Len.Int.WLQU_ID_PTF, 0);
        position += Lccvlqu1.Len.WLQU_ID_PTF;
        lccvlqu1.getDati().getWlquDatiBytes(buffer, position);
        return buffer;
    }

    public void initWlquTabLiqSpaces() {
        lccvlqu1.initLccvlqu1Spaces();
    }

    public Lccvlqu1 getLccvlqu1() {
        return lccvlqu1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WLQU_TAB_LIQ = WpolStatus.Len.STATUS + Lccvlqu1.Len.WLQU_ID_PTF + S089Dati.Len.WLQU_DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
