package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WBEL-DT-DORMIENZA<br>
 * Variable: WBEL-DT-DORMIENZA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelDtDormienza extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelDtDormienza() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_DT_DORMIENZA;
    }

    public void setWbelDtDormienza(int wbelDtDormienza) {
        writeIntAsPacked(Pos.WBEL_DT_DORMIENZA, wbelDtDormienza, Len.Int.WBEL_DT_DORMIENZA);
    }

    public void setWbelDtDormienzaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_DT_DORMIENZA, Pos.WBEL_DT_DORMIENZA);
    }

    /**Original name: WBEL-DT-DORMIENZA<br>*/
    public int getWbelDtDormienza() {
        return readPackedAsInt(Pos.WBEL_DT_DORMIENZA, Len.Int.WBEL_DT_DORMIENZA);
    }

    public byte[] getWbelDtDormienzaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_DT_DORMIENZA, Pos.WBEL_DT_DORMIENZA);
        return buffer;
    }

    public void initWbelDtDormienzaSpaces() {
        fill(Pos.WBEL_DT_DORMIENZA, Len.WBEL_DT_DORMIENZA, Types.SPACE_CHAR);
    }

    public void setWbelDtDormienzaNull(String wbelDtDormienzaNull) {
        writeString(Pos.WBEL_DT_DORMIENZA_NULL, wbelDtDormienzaNull, Len.WBEL_DT_DORMIENZA_NULL);
    }

    /**Original name: WBEL-DT-DORMIENZA-NULL<br>*/
    public String getWbelDtDormienzaNull() {
        return readString(Pos.WBEL_DT_DORMIENZA_NULL, Len.WBEL_DT_DORMIENZA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_DT_DORMIENZA = 1;
        public static final int WBEL_DT_DORMIENZA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_DT_DORMIENZA = 5;
        public static final int WBEL_DT_DORMIENZA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_DT_DORMIENZA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
