package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-IMP-VOLO<br>
 * Variable: DTR-IMP-VOLO from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_IMP_VOLO;
    }

    public void setDtrImpVolo(AfDecimal dtrImpVolo) {
        writeDecimalAsPacked(Pos.DTR_IMP_VOLO, dtrImpVolo.copy());
    }

    public void setDtrImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_IMP_VOLO, Pos.DTR_IMP_VOLO);
    }

    /**Original name: DTR-IMP-VOLO<br>*/
    public AfDecimal getDtrImpVolo() {
        return readPackedAsDecimal(Pos.DTR_IMP_VOLO, Len.Int.DTR_IMP_VOLO, Len.Fract.DTR_IMP_VOLO);
    }

    public byte[] getDtrImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_IMP_VOLO, Pos.DTR_IMP_VOLO);
        return buffer;
    }

    public void setDtrImpVoloNull(String dtrImpVoloNull) {
        writeString(Pos.DTR_IMP_VOLO_NULL, dtrImpVoloNull, Len.DTR_IMP_VOLO_NULL);
    }

    /**Original name: DTR-IMP-VOLO-NULL<br>*/
    public String getDtrImpVoloNull() {
        return readString(Pos.DTR_IMP_VOLO_NULL, Len.DTR_IMP_VOLO_NULL);
    }

    public String getDtrImpVoloNullFormatted() {
        return Functions.padBlanks(getDtrImpVoloNull(), Len.DTR_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_IMP_VOLO = 1;
        public static final int DTR_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_IMP_VOLO = 8;
        public static final int DTR_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
