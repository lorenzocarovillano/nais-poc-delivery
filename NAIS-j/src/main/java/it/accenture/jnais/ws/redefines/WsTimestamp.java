package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WS-TIMESTAMP<br>
 * Variable: WS-TIMESTAMP from program LDBS1130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsTimestamp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsTimestamp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_TIMESTAMP;
    }

    public void setWsTimestamp(String wsTimestamp) {
        writeString(Pos.WS_TIMESTAMP, wsTimestamp, Len.WS_TIMESTAMP);
    }

    /**Original name: WS-TIMESTAMP<br>*/
    public String getWsTimestamp() {
        return readString(Pos.WS_TIMESTAMP, Len.WS_TIMESTAMP);
    }

    public void setWsTimestampNum(long wsTimestampNum) {
        writeLong(Pos.WS_TIMESTAMP_NUM, wsTimestampNum, Len.Int.WS_TIMESTAMP_NUM, SignType.NO_SIGN);
    }

    /**Original name: WS-TIMESTAMP-NUM<br>*/
    public long getWsTimestampNum() {
        return readNumDispUnsignedLong(Pos.WS_TIMESTAMP_NUM, Len.WS_TIMESTAMP_NUM);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_TIMESTAMP = 1;
        public static final int WS_TIMESTAMP_NUM = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TIMESTAMP = 18;
        public static final int WS_TIMESTAMP_NUM = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WS_TIMESTAMP_NUM = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
