package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-CORI-I<br>
 * Variable: WPCO-DT-ULT-BOLL-CORI-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollCoriI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollCoriI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_CORI_I;
    }

    public void setWpcoDtUltBollCoriI(int wpcoDtUltBollCoriI) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_CORI_I, wpcoDtUltBollCoriI, Len.Int.WPCO_DT_ULT_BOLL_CORI_I);
    }

    public void setDpcoDtUltBollCoriIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_CORI_I, Pos.WPCO_DT_ULT_BOLL_CORI_I);
    }

    /**Original name: WPCO-DT-ULT-BOLL-CORI-I<br>*/
    public int getWpcoDtUltBollCoriI() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_CORI_I, Len.Int.WPCO_DT_ULT_BOLL_CORI_I);
    }

    public byte[] getWpcoDtUltBollCoriIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_CORI_I, Pos.WPCO_DT_ULT_BOLL_CORI_I);
        return buffer;
    }

    public void setWpcoDtUltBollCoriINull(String wpcoDtUltBollCoriINull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_CORI_I_NULL, wpcoDtUltBollCoriINull, Len.WPCO_DT_ULT_BOLL_CORI_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-CORI-I-NULL<br>*/
    public String getWpcoDtUltBollCoriINull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_CORI_I_NULL, Len.WPCO_DT_ULT_BOLL_CORI_I_NULL);
    }

    public String getWpcoDtUltBollCoriINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollCoriINull(), Len.WPCO_DT_ULT_BOLL_CORI_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_CORI_I = 1;
        public static final int WPCO_DT_ULT_BOLL_CORI_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_CORI_I = 5;
        public static final int WPCO_DT_ULT_BOLL_CORI_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_CORI_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
