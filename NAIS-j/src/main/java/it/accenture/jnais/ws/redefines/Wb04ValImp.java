package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB04-VAL-IMP<br>
 * Variable: WB04-VAL-IMP from program LLBS0266<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb04ValImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb04ValImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB04_VAL_IMP;
    }

    public void setWb04ValImp(AfDecimal wb04ValImp) {
        writeDecimalAsPacked(Pos.WB04_VAL_IMP, wb04ValImp.copy());
    }

    public void setWb04ValImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB04_VAL_IMP, Pos.WB04_VAL_IMP);
    }

    /**Original name: WB04-VAL-IMP<br>*/
    public AfDecimal getWb04ValImp() {
        return readPackedAsDecimal(Pos.WB04_VAL_IMP, Len.Int.WB04_VAL_IMP, Len.Fract.WB04_VAL_IMP);
    }

    public byte[] getWb04ValImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB04_VAL_IMP, Pos.WB04_VAL_IMP);
        return buffer;
    }

    public void setWb04ValImpNull(String wb04ValImpNull) {
        writeString(Pos.WB04_VAL_IMP_NULL, wb04ValImpNull, Len.WB04_VAL_IMP_NULL);
    }

    /**Original name: WB04-VAL-IMP-NULL<br>*/
    public String getWb04ValImpNull() {
        return readString(Pos.WB04_VAL_IMP_NULL, Len.WB04_VAL_IMP_NULL);
    }

    public String getWb04ValImpNullFormatted() {
        return Functions.padBlanks(getWb04ValImpNull(), Len.WB04_VAL_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB04_VAL_IMP = 1;
        public static final int WB04_VAL_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB04_VAL_IMP = 10;
        public static final int WB04_VAL_IMP_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB04_VAL_IMP = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB04_VAL_IMP = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
