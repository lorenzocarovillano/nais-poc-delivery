package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: S089-ID-MOVI-CHIU<br>
 * Variable: S089-ID-MOVI-CHIU from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_ID_MOVI_CHIU;
    }

    public void setWlquIdMoviChiu(int wlquIdMoviChiu) {
        writeIntAsPacked(Pos.S089_ID_MOVI_CHIU, wlquIdMoviChiu, Len.Int.WLQU_ID_MOVI_CHIU);
    }

    public void setWlquIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_ID_MOVI_CHIU, Pos.S089_ID_MOVI_CHIU);
    }

    /**Original name: WLQU-ID-MOVI-CHIU<br>*/
    public int getWlquIdMoviChiu() {
        return readPackedAsInt(Pos.S089_ID_MOVI_CHIU, Len.Int.WLQU_ID_MOVI_CHIU);
    }

    public byte[] getWlquIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_ID_MOVI_CHIU, Pos.S089_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWlquIdMoviChiuSpaces() {
        fill(Pos.S089_ID_MOVI_CHIU, Len.S089_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWlquIdMoviChiuNull(String wlquIdMoviChiuNull) {
        writeString(Pos.S089_ID_MOVI_CHIU_NULL, wlquIdMoviChiuNull, Len.WLQU_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WLQU-ID-MOVI-CHIU-NULL<br>*/
    public String getWlquIdMoviChiuNull() {
        return readString(Pos.S089_ID_MOVI_CHIU_NULL, Len.WLQU_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_ID_MOVI_CHIU = 1;
        public static final int S089_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_ID_MOVI_CHIU = 5;
        public static final int WLQU_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
