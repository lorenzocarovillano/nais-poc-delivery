package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-CORI-C<br>
 * Variable: WPCO-DT-ULT-BOLL-CORI-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollCoriC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollCoriC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_CORI_C;
    }

    public void setWpcoDtUltBollCoriC(int wpcoDtUltBollCoriC) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_CORI_C, wpcoDtUltBollCoriC, Len.Int.WPCO_DT_ULT_BOLL_CORI_C);
    }

    public void setDpcoDtUltBollCoriCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_CORI_C, Pos.WPCO_DT_ULT_BOLL_CORI_C);
    }

    /**Original name: WPCO-DT-ULT-BOLL-CORI-C<br>*/
    public int getWpcoDtUltBollCoriC() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_CORI_C, Len.Int.WPCO_DT_ULT_BOLL_CORI_C);
    }

    public byte[] getWpcoDtUltBollCoriCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_CORI_C, Pos.WPCO_DT_ULT_BOLL_CORI_C);
        return buffer;
    }

    public void setWpcoDtUltBollCoriCNull(String wpcoDtUltBollCoriCNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_CORI_C_NULL, wpcoDtUltBollCoriCNull, Len.WPCO_DT_ULT_BOLL_CORI_C_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-CORI-C-NULL<br>*/
    public String getWpcoDtUltBollCoriCNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_CORI_C_NULL, Len.WPCO_DT_ULT_BOLL_CORI_C_NULL);
    }

    public String getWpcoDtUltBollCoriCNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollCoriCNull(), Len.WPCO_DT_ULT_BOLL_CORI_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_CORI_C = 1;
        public static final int WPCO_DT_ULT_BOLL_CORI_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_CORI_C = 5;
        public static final int WPCO_DT_ULT_BOLL_CORI_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_CORI_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
