package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ALQ-CNBT-INPSTFM-E<br>
 * Variable: WDFL-ALQ-CNBT-INPSTFM-E from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAlqCnbtInpstfmE extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAlqCnbtInpstfmE() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ALQ_CNBT_INPSTFM_E;
    }

    public void setWdflAlqCnbtInpstfmE(AfDecimal wdflAlqCnbtInpstfmE) {
        writeDecimalAsPacked(Pos.WDFL_ALQ_CNBT_INPSTFM_E, wdflAlqCnbtInpstfmE.copy());
    }

    public void setWdflAlqCnbtInpstfmEFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ALQ_CNBT_INPSTFM_E, Pos.WDFL_ALQ_CNBT_INPSTFM_E);
    }

    /**Original name: WDFL-ALQ-CNBT-INPSTFM-E<br>*/
    public AfDecimal getWdflAlqCnbtInpstfmE() {
        return readPackedAsDecimal(Pos.WDFL_ALQ_CNBT_INPSTFM_E, Len.Int.WDFL_ALQ_CNBT_INPSTFM_E, Len.Fract.WDFL_ALQ_CNBT_INPSTFM_E);
    }

    public byte[] getWdflAlqCnbtInpstfmEAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ALQ_CNBT_INPSTFM_E, Pos.WDFL_ALQ_CNBT_INPSTFM_E);
        return buffer;
    }

    public void setWdflAlqCnbtInpstfmENull(String wdflAlqCnbtInpstfmENull) {
        writeString(Pos.WDFL_ALQ_CNBT_INPSTFM_E_NULL, wdflAlqCnbtInpstfmENull, Len.WDFL_ALQ_CNBT_INPSTFM_E_NULL);
    }

    /**Original name: WDFL-ALQ-CNBT-INPSTFM-E-NULL<br>*/
    public String getWdflAlqCnbtInpstfmENull() {
        return readString(Pos.WDFL_ALQ_CNBT_INPSTFM_E_NULL, Len.WDFL_ALQ_CNBT_INPSTFM_E_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_CNBT_INPSTFM_E = 1;
        public static final int WDFL_ALQ_CNBT_INPSTFM_E_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_CNBT_INPSTFM_E = 4;
        public static final int WDFL_ALQ_CNBT_INPSTFM_E_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_CNBT_INPSTFM_E = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_CNBT_INPSTFM_E = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
