package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvp881;
import it.accenture.jnais.copy.Wp88Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WP88-TAB-SERV-VAL<br>
 * Variables: WP88-TAB-SERV-VAL from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wp88TabServVal {

    //==== PROPERTIES ====
    //Original name: LCCVP881
    private Lccvp881 lccvp881 = new Lccvp881();

    //==== METHODS ====
    public void setWp88TabServValBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp881.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp881.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp881.Len.Int.ID_PTF, 0));
        position += Lccvp881.Len.ID_PTF;
        lccvp881.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp88TabServValBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp881.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp881.getIdPtf(), Lccvp881.Len.Int.ID_PTF, 0);
        position += Lccvp881.Len.ID_PTF;
        lccvp881.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWp88TabServValSpaces() {
        lccvp881.initLccvp881Spaces();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WP88_TAB_SERV_VAL = WpolStatus.Len.STATUS + Lccvp881.Len.ID_PTF + Wp88Dati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
