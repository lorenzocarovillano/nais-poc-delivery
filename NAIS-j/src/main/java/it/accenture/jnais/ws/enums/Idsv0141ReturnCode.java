package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0141-RETURN-CODE<br>
 * Variable: IDSV0141-RETURN-CODE from copybook IDSV0141<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0141ReturnCode {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.RETURN_CODE);
    public static final String SUCCESSFUL_RC = "OK";
    public static final String UNSUCCESSFUL_RC = "KO";

    //==== METHODS ====
    public void setReturnCode(String returnCode) {
        this.value = Functions.subString(returnCode, Len.RETURN_CODE);
    }

    public String getReturnCode() {
        return this.value;
    }

    public boolean isSuccessfulRc() {
        return value.equals(SUCCESSFUL_RC);
    }

    public void setSuccessfulRc() {
        value = SUCCESSFUL_RC;
    }

    public boolean isIdsv0141UnsuccessfulRc() {
        return value.equals(UNSUCCESSFUL_RC);
    }

    public void setUnsuccessfulRc() {
        value = UNSUCCESSFUL_RC;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RETURN_CODE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
