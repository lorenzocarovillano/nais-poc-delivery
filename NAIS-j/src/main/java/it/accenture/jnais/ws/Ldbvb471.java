package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.Ldbvb471TotIntPre;

/**Original name: LDBVB471<br>
 * Variable: LDBVB471 from copybook LDBVB471<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbvb471 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBVB471-TP-TIT-01
    private String ldbvb471TpTit01 = DefaultValues.stringVal(Len.LDBVB471_TP_TIT01);
    //Original name: LDBVB471-TP-TIT-02
    private String ldbvb471TpTit02 = DefaultValues.stringVal(Len.LDBVB471_TP_TIT02);
    //Original name: LDBVB471-TP-STAT-TIT-1
    private String ldbvb471TpStatTit1 = DefaultValues.stringVal(Len.LDBVB471_TP_STAT_TIT1);
    //Original name: LDBVB471-TP-STAT-TIT-2
    private String ldbvb471TpStatTit2 = DefaultValues.stringVal(Len.LDBVB471_TP_STAT_TIT2);
    //Original name: LDBVB471-TP-STAT-TIT-3
    private String ldbvb471TpStatTit3 = DefaultValues.stringVal(Len.LDBVB471_TP_STAT_TIT3);
    //Original name: LDBVB471-DT-MAX-DB
    private String ldbvb471DtMaxDb = DefaultValues.stringVal(Len.LDBVB471_DT_MAX_DB);
    //Original name: LDBVB471-DT-MAX
    private int ldbvb471DtMax = DefaultValues.INT_VAL;
    //Original name: LDBVB471-TOT-INT-PRE
    private Ldbvb471TotIntPre ldbvb471TotIntPre = new Ldbvb471TotIntPre();
    //Original name: LDBVB471-ID-OGG
    private int ldbvb471IdOgg = DefaultValues.INT_VAL;
    //Original name: LDBVB471-TP-OGG
    private String ldbvb471TpOgg = DefaultValues.stringVal(Len.LDBVB471_TP_OGG);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBVB471;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbvb471Bytes(buf);
    }

    public String getLdbvb471Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvb471Bytes());
    }

    public void setLdbvb471Bytes(byte[] buffer) {
        setLdbvb471Bytes(buffer, 1);
    }

    public byte[] getLdbvb471Bytes() {
        byte[] buffer = new byte[Len.LDBVB471];
        return getLdbvb471Bytes(buffer, 1);
    }

    public void setLdbvb471Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbvb471TpTit01 = MarshalByte.readString(buffer, position, Len.LDBVB471_TP_TIT01);
        position += Len.LDBVB471_TP_TIT01;
        ldbvb471TpTit02 = MarshalByte.readString(buffer, position, Len.LDBVB471_TP_TIT02);
        position += Len.LDBVB471_TP_TIT02;
        ldbvb471TpStatTit1 = MarshalByte.readString(buffer, position, Len.LDBVB471_TP_STAT_TIT1);
        position += Len.LDBVB471_TP_STAT_TIT1;
        ldbvb471TpStatTit2 = MarshalByte.readString(buffer, position, Len.LDBVB471_TP_STAT_TIT2);
        position += Len.LDBVB471_TP_STAT_TIT2;
        ldbvb471TpStatTit3 = MarshalByte.readString(buffer, position, Len.LDBVB471_TP_STAT_TIT3);
        position += Len.LDBVB471_TP_STAT_TIT3;
        ldbvb471DtMaxDb = MarshalByte.readString(buffer, position, Len.LDBVB471_DT_MAX_DB);
        position += Len.LDBVB471_DT_MAX_DB;
        ldbvb471DtMax = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBVB471_DT_MAX, 0);
        position += Len.LDBVB471_DT_MAX;
        ldbvb471TotIntPre.setLdbvb471TotIntPreFromBuffer(buffer, position);
        position += Ldbvb471TotIntPre.Len.LDBVB471_TOT_INT_PRE;
        ldbvb471IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBVB471_ID_OGG, 0);
        position += Len.LDBVB471_ID_OGG;
        ldbvb471TpOgg = MarshalByte.readString(buffer, position, Len.LDBVB471_TP_OGG);
    }

    public byte[] getLdbvb471Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ldbvb471TpTit01, Len.LDBVB471_TP_TIT01);
        position += Len.LDBVB471_TP_TIT01;
        MarshalByte.writeString(buffer, position, ldbvb471TpTit02, Len.LDBVB471_TP_TIT02);
        position += Len.LDBVB471_TP_TIT02;
        MarshalByte.writeString(buffer, position, ldbvb471TpStatTit1, Len.LDBVB471_TP_STAT_TIT1);
        position += Len.LDBVB471_TP_STAT_TIT1;
        MarshalByte.writeString(buffer, position, ldbvb471TpStatTit2, Len.LDBVB471_TP_STAT_TIT2);
        position += Len.LDBVB471_TP_STAT_TIT2;
        MarshalByte.writeString(buffer, position, ldbvb471TpStatTit3, Len.LDBVB471_TP_STAT_TIT3);
        position += Len.LDBVB471_TP_STAT_TIT3;
        MarshalByte.writeString(buffer, position, ldbvb471DtMaxDb, Len.LDBVB471_DT_MAX_DB);
        position += Len.LDBVB471_DT_MAX_DB;
        MarshalByte.writeIntAsPacked(buffer, position, ldbvb471DtMax, Len.Int.LDBVB471_DT_MAX, 0);
        position += Len.LDBVB471_DT_MAX;
        ldbvb471TotIntPre.getLdbvb471TotIntPreAsBuffer(buffer, position);
        position += Ldbvb471TotIntPre.Len.LDBVB471_TOT_INT_PRE;
        MarshalByte.writeIntAsPacked(buffer, position, ldbvb471IdOgg, Len.Int.LDBVB471_ID_OGG, 0);
        position += Len.LDBVB471_ID_OGG;
        MarshalByte.writeString(buffer, position, ldbvb471TpOgg, Len.LDBVB471_TP_OGG);
        return buffer;
    }

    public void setLdbvb471TpTit01(String ldbvb471TpTit01) {
        this.ldbvb471TpTit01 = Functions.subString(ldbvb471TpTit01, Len.LDBVB471_TP_TIT01);
    }

    public String getLdbvb471TpTit01() {
        return this.ldbvb471TpTit01;
    }

    public void setLdbvb471TpTit02(String ldbvb471TpTit02) {
        this.ldbvb471TpTit02 = Functions.subString(ldbvb471TpTit02, Len.LDBVB471_TP_TIT02);
    }

    public String getLdbvb471TpTit02() {
        return this.ldbvb471TpTit02;
    }

    public void setLdbvb471TpStatTit1(String ldbvb471TpStatTit1) {
        this.ldbvb471TpStatTit1 = Functions.subString(ldbvb471TpStatTit1, Len.LDBVB471_TP_STAT_TIT1);
    }

    public String getLdbvb471TpStatTit1() {
        return this.ldbvb471TpStatTit1;
    }

    public void setLdbvb471TpStatTit2(String ldbvb471TpStatTit2) {
        this.ldbvb471TpStatTit2 = Functions.subString(ldbvb471TpStatTit2, Len.LDBVB471_TP_STAT_TIT2);
    }

    public String getLdbvb471TpStatTit2() {
        return this.ldbvb471TpStatTit2;
    }

    public void setLdbvb471TpStatTit3(String ldbvb471TpStatTit3) {
        this.ldbvb471TpStatTit3 = Functions.subString(ldbvb471TpStatTit3, Len.LDBVB471_TP_STAT_TIT3);
    }

    public String getLdbvb471TpStatTit3() {
        return this.ldbvb471TpStatTit3;
    }

    public void setLdbvb471DtMaxDb(String ldbvb471DtMaxDb) {
        this.ldbvb471DtMaxDb = Functions.subString(ldbvb471DtMaxDb, Len.LDBVB471_DT_MAX_DB);
    }

    public String getLdbvb471DtMaxDb() {
        return this.ldbvb471DtMaxDb;
    }

    public void setLdbvb471DtMax(int ldbvb471DtMax) {
        this.ldbvb471DtMax = ldbvb471DtMax;
    }

    public int getLdbvb471DtMax() {
        return this.ldbvb471DtMax;
    }

    public void setLdbvb471IdOgg(int ldbvb471IdOgg) {
        this.ldbvb471IdOgg = ldbvb471IdOgg;
    }

    public int getLdbvb471IdOgg() {
        return this.ldbvb471IdOgg;
    }

    public void setLdbvb471TpOgg(String ldbvb471TpOgg) {
        this.ldbvb471TpOgg = Functions.subString(ldbvb471TpOgg, Len.LDBVB471_TP_OGG);
    }

    public String getLdbvb471TpOgg() {
        return this.ldbvb471TpOgg;
    }

    public Ldbvb471TotIntPre getLdbvb471TotIntPre() {
        return ldbvb471TotIntPre;
    }

    @Override
    public byte[] serialize() {
        return getLdbvb471Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBVB471_TP_TIT01 = 2;
        public static final int LDBVB471_TP_TIT02 = 2;
        public static final int LDBVB471_TP_STAT_TIT1 = 2;
        public static final int LDBVB471_TP_STAT_TIT2 = 2;
        public static final int LDBVB471_TP_STAT_TIT3 = 2;
        public static final int LDBVB471_DT_MAX_DB = 10;
        public static final int LDBVB471_DT_MAX = 5;
        public static final int LDBVB471_ID_OGG = 5;
        public static final int LDBVB471_TP_OGG = 2;
        public static final int LDBVB471 = LDBVB471_TP_TIT01 + LDBVB471_TP_TIT02 + LDBVB471_TP_STAT_TIT1 + LDBVB471_TP_STAT_TIT2 + LDBVB471_TP_STAT_TIT3 + LDBVB471_DT_MAX_DB + LDBVB471_DT_MAX + Ldbvb471TotIntPre.Len.LDBVB471_TOT_INT_PRE + LDBVB471_ID_OGG + LDBVB471_TP_OGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBVB471_DT_MAX = 8;
            public static final int LDBVB471_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
