package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-MANFEE-RICOR<br>
 * Variable: WTDR-TOT-MANFEE-RICOR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotManfeeRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotManfeeRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_MANFEE_RICOR;
    }

    public void setWtdrTotManfeeRicor(AfDecimal wtdrTotManfeeRicor) {
        writeDecimalAsPacked(Pos.WTDR_TOT_MANFEE_RICOR, wtdrTotManfeeRicor.copy());
    }

    /**Original name: WTDR-TOT-MANFEE-RICOR<br>*/
    public AfDecimal getWtdrTotManfeeRicor() {
        return readPackedAsDecimal(Pos.WTDR_TOT_MANFEE_RICOR, Len.Int.WTDR_TOT_MANFEE_RICOR, Len.Fract.WTDR_TOT_MANFEE_RICOR);
    }

    public void setWtdrTotManfeeRicorNull(String wtdrTotManfeeRicorNull) {
        writeString(Pos.WTDR_TOT_MANFEE_RICOR_NULL, wtdrTotManfeeRicorNull, Len.WTDR_TOT_MANFEE_RICOR_NULL);
    }

    /**Original name: WTDR-TOT-MANFEE-RICOR-NULL<br>*/
    public String getWtdrTotManfeeRicorNull() {
        return readString(Pos.WTDR_TOT_MANFEE_RICOR_NULL, Len.WTDR_TOT_MANFEE_RICOR_NULL);
    }

    public String getWtdrTotManfeeRicorNullFormatted() {
        return Functions.padBlanks(getWtdrTotManfeeRicorNull(), Len.WTDR_TOT_MANFEE_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_MANFEE_RICOR = 1;
        public static final int WTDR_TOT_MANFEE_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_MANFEE_RICOR = 8;
        public static final int WTDR_TOT_MANFEE_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_MANFEE_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_MANFEE_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
