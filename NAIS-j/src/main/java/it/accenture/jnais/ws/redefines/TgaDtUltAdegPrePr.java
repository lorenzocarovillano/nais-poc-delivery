package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-DT-ULT-ADEG-PRE-PR<br>
 * Variable: TGA-DT-ULT-ADEG-PRE-PR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaDtUltAdegPrePr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaDtUltAdegPrePr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_DT_ULT_ADEG_PRE_PR;
    }

    public void setTgaDtUltAdegPrePr(int tgaDtUltAdegPrePr) {
        writeIntAsPacked(Pos.TGA_DT_ULT_ADEG_PRE_PR, tgaDtUltAdegPrePr, Len.Int.TGA_DT_ULT_ADEG_PRE_PR);
    }

    public void setTgaDtUltAdegPrePrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_DT_ULT_ADEG_PRE_PR, Pos.TGA_DT_ULT_ADEG_PRE_PR);
    }

    /**Original name: TGA-DT-ULT-ADEG-PRE-PR<br>*/
    public int getTgaDtUltAdegPrePr() {
        return readPackedAsInt(Pos.TGA_DT_ULT_ADEG_PRE_PR, Len.Int.TGA_DT_ULT_ADEG_PRE_PR);
    }

    public byte[] getTgaDtUltAdegPrePrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_DT_ULT_ADEG_PRE_PR, Pos.TGA_DT_ULT_ADEG_PRE_PR);
        return buffer;
    }

    public void setTgaDtUltAdegPrePrNull(String tgaDtUltAdegPrePrNull) {
        writeString(Pos.TGA_DT_ULT_ADEG_PRE_PR_NULL, tgaDtUltAdegPrePrNull, Len.TGA_DT_ULT_ADEG_PRE_PR_NULL);
    }

    /**Original name: TGA-DT-ULT-ADEG-PRE-PR-NULL<br>*/
    public String getTgaDtUltAdegPrePrNull() {
        return readString(Pos.TGA_DT_ULT_ADEG_PRE_PR_NULL, Len.TGA_DT_ULT_ADEG_PRE_PR_NULL);
    }

    public String getTgaDtUltAdegPrePrNullFormatted() {
        return Functions.padBlanks(getTgaDtUltAdegPrePrNull(), Len.TGA_DT_ULT_ADEG_PRE_PR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_DT_ULT_ADEG_PRE_PR = 1;
        public static final int TGA_DT_ULT_ADEG_PRE_PR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_DT_ULT_ADEG_PRE_PR = 5;
        public static final int TGA_DT_ULT_ADEG_PRE_PR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_DT_ULT_ADEG_PRE_PR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
