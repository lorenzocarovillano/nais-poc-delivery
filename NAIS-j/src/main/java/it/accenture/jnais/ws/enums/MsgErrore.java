package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: MSG-ERRORE<br>
 * Variable: MSG-ERRORE from copybook LRGC0661<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MsgErrore {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.MSG_ERRORE);
    public static final String CNTRL1 = "1; Il numero delle polizze non concide con il numero delle polizze del tipo Record 3 - Garanzie";
    public static final String CNTRL2 = "2; DT-RIF-DA E DT-RIF-A non sono coerenti con il periodo di riferimento richiesto e con la DT-DECOR-POLI";
    public static final String CNTRL3 = "3; CAP-LIQ-DT-RIF-EST-CC - PRESTAZIONE-AC-POL < -0,01 euro";
    public static final String CNTRL4 = "4; IMP-LOR-CED-LIQU - Flusso generazione cedole batch  <> 0";
    public static final String CNTRL5 = "5; IMP-LOR-CED-LIQU > - Campi CC-PAESE - CC-CHECK-DIGIT - CC-CIN - CC-ABI - CC-CAB - CC-NUM-CONTO non valorizzati";
    public static final String CNTRL6 = "6; TOT-CUM-PREMI-VERS-TOT-CUM-PRE-INVST-AA-RIF)/TOT-CUM-PREMI-VERS  > 6,00% (MR-UL) - > 25% (RV)";
    public static final String CNTRL7 = "7; TOT-CUM-PREMI-VERS = 0,  TOT-CUM-PRE-INVST-AA-RIF diverso da 0";
    public static final String CNTRL8 = "8; DT-DECOR-POLI < DT-RIF-DA, allora TOT-CUM-PREMI-VERS-AP non > 0";
    public static final String CNTRL9 = "9; DT-RIF-DA=DT-RIF-A-valorizzati TOT-CUM-PREMI-VERS-AP- PRESTAZIONE-AP-POL - IMP-LRD-RISC-PARZ-AP-POL -IMP-LRD-RISC-PARZ-AC-POL";
    public static final String CNTRL10 = "10; DT-DECOR-POLI < DT-RIF-DA,PRESTAZIONE-AP-POL non > 0";
    public static final String CNTRL11 = "11; PRESTAZIONE-AC-POL <=0";
    public static final String CNTRL12 = "12; PRESTAZIONE-AC-POL - RISC-TOT-DT-RIF-EST-CC < -0,01";
    public static final String CNTRL13 = "13; La somma del campo TOT-CUM-PRE-INVST-AA-RIF non corrisponde alla somma del campo CUM-PREM-INVST-AA-RIF(Rec. 3)";
    public static final String CNTRL14 = "14; La somma del campo TOT-CUM-PREMI-VERS non corrisponde alla somma del campo CUM-PREM-VERS-AA-RIF (Rec. 3)";
    public static final String CNTRL15 = "15; La somma del campo PRESTAZIONE-AC-POL non corrisponde alla somma del campo PREST-MATUR-AA-RIF  (Rec. 3)";
    public static final String CNTRL16 = "16; La somma del campo CAP-LIQ-DT-RIF-EST-CC noncorrisponde alla somma del campo CAP-LIQ-GA (Rec. 3), filtrato per TP-GAR = 1";
    public static final String CNTRL17 = "17; La somma del campo RISC-TOT-DT-RIF-EST-CC noncorrisponde alla somma del campo RISC-TOT-GA (Rec. 3)";
    public static final String CNTRL18 = "18; La somma del campo TOT-CUM-PREMI-VERS-AP non corrisponde alla somma del campo CUM-PREM-VERS-EC-PREC  (Rec. 3)";
    public static final String CNTRL19 = "19; La somma del campo PRESTAZIONE-AP-POL  noncorrisponde alla somma del campo  PREST-MATUR-EC-PREC (Rec. 3), filtrato per TP-GAR = 1";
    public static final String CNTRL20 = "20; La somma del campo IMP-LRD-RISC-PARZ-AP-POL  noncorrisponde alla somma del campo IMP-LRD-RISC-PARZ-AP-POL (Rec. 3)";
    public static final String CNTRL21 = "21; La somma del campo IMP-LRD-RISC-PARZ-AC-POL  noncorrisponde alla somma del campo IMP-LRD-RISC-PARZ-AC-POL (Rec. 3)";
    public static final String CNTRL22 = "22; BOLLO - Output flusso calcolo bollo 31/12Esclusione casi <> 0";
    public static final String CNTRL23 = "23; COGNOME-ASSICU/NOME-ASSICU non valorizzato";
    public static final String CNTRL24 = "24; CAP-LIQ-GA - PREST-MATUR-AA-RIF filtrato per TP-GAR =1 < -0,01 euro";
    public static final String CNTRL25 = "25; TASSO-TECNICO - Flusso rivalutazione batch filtrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0";
    public static final String CNTRL26 = "26; REND-LOR-GEST-SEP-GAR - Flusso rivalutazione batchfiltrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0";
    public static final String CNTRL27 = "27; ALIQ-RETROC-RICON-GAR - Flusso rivalutazione batchfiltrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0";
    public static final String CNTRL28 = "28; TASSO-ANNO-REND-RETROC-GAR - Flusso rivalutazione batch filtrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0";
    public static final String CNTRL29 = "29; PC-COMM-GEST-GAR - Flusso rivalutazione batchfiltrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0";
    public static final String CNTRL30 = "30; TASSO-ANN-RIVAL-PREST-GAR - Flusso rivalutazione batchfiltrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0";
    public static final String CNTRL31 = "31; REN-MIN-TRNUT - Flusso rivalutazione batch filtrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0";
    public static final String CNTRL32 = "32; REN-MIN-GARTO - Flusso rivalutazione batch filtrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0";
    public static final String CNTRL33 = "33; (PREST-MATUR-AA-RIF - g. base - CAP-LIQ-GA- g. acc. -)- CAP-LIQ-GA - g. base.  < - 1,00 o  > 1,00";
    public static final String CNTRL34 = "34; NON coerenza campi DT-INI-PER-INV - DT-FIN-PER-INV con periodo di riferimento elaborato";
    public static final String CNTRL35 = "35; Incoerenza campi PREST-MATUR-EC-PREC, per TP-INVST-GAR=7 con campo CNTRVAL-PREC   (Rec.8). Esclusione casi in cui la differenza  < - 0,01 o > 0,01 euro";
    public static final String CNTRL36 = "36; Incoerenza campi PREST-MATUR-AA-RIF filtrato per TP-INVST-GAR=7 con campo CNTRVAL-ATTUALE (Rec 8) , esclusione casi  < - 0,01 > 0,01 euro";
    public static final String CNTRL37 = "37;CONTROLLO SUL RECORD 8";
    public static final String CNTRL38 = "38;Incoerenza campo IMP-LORDO-RISC-PARZ (Rec. 9) con i corrispondenti campi: IMP-LRD-RISC-PARZ-AC-POL (Rec. 1) IMP-LRD-RISC-PARZ-AC-GAR (Rec. 3)";
    public static final String CNTRL39 = "39; DESC-TP-MOVI=PAGAMENTO CEDOLA AND IMP-OPER con il corrispondente campo IMP-LOR-CED-LIQU (Rec. 1)";
    public static final String CNTRL40 = "40; DESC-TP-MOVI=RISCATTO PARZIALE AND IMP-OPER con il corrispondente campo IMP-LORDO-RISC-PARZ (Rec.9)";
    public static final String CNTRL41 = "41; DESC-TP-MOVI=VERSAMENTO PREMI AND IMP-IMP-VERS con ilcorrispondente campo TOT-CUM-PREMI-VERS (Rec.1)";
    public static final String CNTRL42 = "42; DESC-TP-MOVI=VERSAMENTO PREMI AND IMP-IMP-OPER con ilcorrispondente campo TOT-CUM-PRE-INVST-AA-RIF (Rec. 1)";
    public static final String CNTRL43 = "43; DESC-TP-MOVI=CALCOLO IMPOSTA SOSTITUTIVA AND IMP-OPER) Flusso imposta";
    public static final String CNTRL44 = "44; DESC-TP-MOVI=COMMISSIONI DI GESTIONE AND NUMERO QUO con il corrispondente campo NUM-QUO-DISINV-COMMGES (Rec. 8)";
    public static final String CNTRL45 = "45; DESC-TP-MOVI=COSTO CASO MORTE AND NUMERO QUO con il corrispondente campo NUM-QUO-DISINV-PREL-COSTI (Rec. 8)";
    public static final String CNTRL46 = "46; DESC-TP-MOVI=REBATE AND NUMERO QUO con il corrispondente campo NUM-QUO-INVST-REBATE  (Rec. 8)";
    public static final String CNTRL47 = "47; DESC-TP-MOVI=VERSAMENTO PREMI AND NUMERO QUO con il corrispondente campo NUM-QUO-INVST-VERS  (Rec. 8)";
    public static final String CNTRL48 = "48; DESC-TP-MOVI=sum SWITCH INGRESSO (stand alone/massivo/quick) AND NUMERO QUO con il corrispondente campo NUM-QUO-DISINV-SWITCH (Rec. 8)";
    public static final String CNTRL49 = "49; DESC-TP-MOVI= SWITCH USCITA AND NUMERO QUO con il corrispondente campo  NUM-QUO-INV-SWITCH (Rec. 8)";
    public static final String CNTRL50 = "50; DESC-TP-MOVI=COMPENSAZIONE NAV NEGATIVA AND NUMERO QUO con il corrispondente campo NUM-QUO-DISINV-COMP-NAV (Rec. 8)";
    public static final String CNTRL51 = "51; DESC-TP-MOVI=COMPENSAZIONE NAV POSITIVA AND NUMERO QUO con il corrispondente campo NUM-QUO-INVST-COMP-NAV (Rec. 8)";

    //==== METHODS ====
    public void setMsgErrore(String msgErrore) {
        this.value = Functions.subString(msgErrore, Len.MSG_ERRORE);
    }

    public String getMsgErrore() {
        return this.value;
    }

    public void setCntrl1() {
        value = CNTRL1;
    }

    public void setCntrl2() {
        value = CNTRL2;
    }

    public void setCntrl3() {
        value = CNTRL3;
    }

    public void setCntrl4() {
        value = CNTRL4;
    }

    public void setCntrl5() {
        value = CNTRL5;
    }

    public void setCntrl6() {
        value = CNTRL6;
    }

    public void setCntrl7() {
        value = CNTRL7;
    }

    public void setCntrl8() {
        value = CNTRL8;
    }

    public void setCntrl9() {
        value = CNTRL9;
    }

    public void setCntrl10() {
        value = CNTRL10;
    }

    public void setCntrl11() {
        value = CNTRL11;
    }

    public void setCntrl12() {
        value = CNTRL12;
    }

    public void setCntrl13() {
        value = CNTRL13;
    }

    public void setCntrl14() {
        value = CNTRL14;
    }

    public void setCntrl15() {
        value = CNTRL15;
    }

    public void setCntrl16() {
        value = CNTRL16;
    }

    public void setCntrl17() {
        value = CNTRL17;
    }

    public void setCntrl18() {
        value = CNTRL18;
    }

    public void setCntrl19() {
        value = CNTRL19;
    }

    public void setCntrl20() {
        value = CNTRL20;
    }

    public void setCntrl21() {
        value = CNTRL21;
    }

    public void setCntrl23() {
        value = CNTRL23;
    }

    public void setCntrl24() {
        value = CNTRL24;
    }

    public void setCntrl25() {
        value = CNTRL25;
    }

    public void setCntrl26() {
        value = CNTRL26;
    }

    public void setCntrl27() {
        value = CNTRL27;
    }

    public void setCntrl28() {
        value = CNTRL28;
    }

    public void setCntrl29() {
        value = CNTRL29;
    }

    public void setCntrl30() {
        value = CNTRL30;
    }

    public void setCntrl31() {
        value = CNTRL31;
    }

    public void setCntrl32() {
        value = CNTRL32;
    }

    public void setCntrl33() {
        value = CNTRL33;
    }

    public void setCntrl34() {
        value = CNTRL34;
    }

    public void setCntrl35() {
        value = CNTRL35;
    }

    public void setCntrl36() {
        value = CNTRL36;
    }

    public void setCntrl37() {
        value = CNTRL37;
    }

    public void setCntrl38() {
        value = CNTRL38;
    }

    public void setCntrl39() {
        value = CNTRL39;
    }

    public void setCntrl40() {
        value = CNTRL40;
    }

    public void setCntrl42() {
        value = CNTRL42;
    }

    public void setCntrl43() {
        value = CNTRL43;
    }

    public void setCntrl44() {
        value = CNTRL44;
    }

    public void setCntrl45() {
        value = CNTRL45;
    }

    public void setCntrl46() {
        value = CNTRL46;
    }

    public void setCntrl47() {
        value = CNTRL47;
    }

    public void setCntrl48() {
        value = CNTRL48;
    }

    public void setCntrl49() {
        value = CNTRL49;
    }

    public void setCntrl50() {
        value = CNTRL50;
    }

    public void setCntrl51() {
        value = CNTRL51;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MSG_ERRORE = 185;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
