package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RRE-COD-PNT-RETE-END<br>
 * Variable: RRE-COD-PNT-RETE-END from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RreCodPntReteEnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RreCodPntReteEnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RRE_COD_PNT_RETE_END;
    }

    public void setRreCodPntReteEnd(long rreCodPntReteEnd) {
        writeLongAsPacked(Pos.RRE_COD_PNT_RETE_END, rreCodPntReteEnd, Len.Int.RRE_COD_PNT_RETE_END);
    }

    public void setRreCodPntReteEndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RRE_COD_PNT_RETE_END, Pos.RRE_COD_PNT_RETE_END);
    }

    /**Original name: RRE-COD-PNT-RETE-END<br>*/
    public long getRreCodPntReteEnd() {
        return readPackedAsLong(Pos.RRE_COD_PNT_RETE_END, Len.Int.RRE_COD_PNT_RETE_END);
    }

    public byte[] getRreCodPntReteEndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RRE_COD_PNT_RETE_END, Pos.RRE_COD_PNT_RETE_END);
        return buffer;
    }

    public void setRreCodPntReteEndNull(String rreCodPntReteEndNull) {
        writeString(Pos.RRE_COD_PNT_RETE_END_NULL, rreCodPntReteEndNull, Len.RRE_COD_PNT_RETE_END_NULL);
    }

    /**Original name: RRE-COD-PNT-RETE-END-NULL<br>*/
    public String getRreCodPntReteEndNull() {
        return readString(Pos.RRE_COD_PNT_RETE_END_NULL, Len.RRE_COD_PNT_RETE_END_NULL);
    }

    public String getRreCodPntReteEndNullFormatted() {
        return Functions.padBlanks(getRreCodPntReteEndNull(), Len.RRE_COD_PNT_RETE_END_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RRE_COD_PNT_RETE_END = 1;
        public static final int RRE_COD_PNT_RETE_END_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RRE_COD_PNT_RETE_END = 6;
        public static final int RRE_COD_PNT_RETE_END_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RRE_COD_PNT_RETE_END = 10;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
