package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-TS-STAB-PRE<br>
 * Variable: W-B03-TS-STAB-PRE from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03TsStabPreLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03TsStabPreLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_TS_STAB_PRE;
    }

    public void setwB03TsStabPre(AfDecimal wB03TsStabPre) {
        writeDecimalAsPacked(Pos.W_B03_TS_STAB_PRE, wB03TsStabPre.copy());
    }

    /**Original name: W-B03-TS-STAB-PRE<br>*/
    public AfDecimal getwB03TsStabPre() {
        return readPackedAsDecimal(Pos.W_B03_TS_STAB_PRE, Len.Int.W_B03_TS_STAB_PRE, Len.Fract.W_B03_TS_STAB_PRE);
    }

    public byte[] getwB03TsStabPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_TS_STAB_PRE, Pos.W_B03_TS_STAB_PRE);
        return buffer;
    }

    public void setwB03TsStabPreNull(String wB03TsStabPreNull) {
        writeString(Pos.W_B03_TS_STAB_PRE_NULL, wB03TsStabPreNull, Len.W_B03_TS_STAB_PRE_NULL);
    }

    /**Original name: W-B03-TS-STAB-PRE-NULL<br>*/
    public String getwB03TsStabPreNull() {
        return readString(Pos.W_B03_TS_STAB_PRE_NULL, Len.W_B03_TS_STAB_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_TS_STAB_PRE = 1;
        public static final int W_B03_TS_STAB_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_TS_STAB_PRE = 8;
        public static final int W_B03_TS_STAB_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_TS_STAB_PRE = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_TS_STAB_PRE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
