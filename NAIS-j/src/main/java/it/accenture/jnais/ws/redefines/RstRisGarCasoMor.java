package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-GAR-CASO-MOR<br>
 * Variable: RST-RIS-GAR-CASO-MOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisGarCasoMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisGarCasoMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_GAR_CASO_MOR;
    }

    public void setRstRisGarCasoMor(AfDecimal rstRisGarCasoMor) {
        writeDecimalAsPacked(Pos.RST_RIS_GAR_CASO_MOR, rstRisGarCasoMor.copy());
    }

    public void setRstRisGarCasoMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_GAR_CASO_MOR, Pos.RST_RIS_GAR_CASO_MOR);
    }

    /**Original name: RST-RIS-GAR-CASO-MOR<br>*/
    public AfDecimal getRstRisGarCasoMor() {
        return readPackedAsDecimal(Pos.RST_RIS_GAR_CASO_MOR, Len.Int.RST_RIS_GAR_CASO_MOR, Len.Fract.RST_RIS_GAR_CASO_MOR);
    }

    public byte[] getRstRisGarCasoMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_GAR_CASO_MOR, Pos.RST_RIS_GAR_CASO_MOR);
        return buffer;
    }

    public void setRstRisGarCasoMorNull(String rstRisGarCasoMorNull) {
        writeString(Pos.RST_RIS_GAR_CASO_MOR_NULL, rstRisGarCasoMorNull, Len.RST_RIS_GAR_CASO_MOR_NULL);
    }

    /**Original name: RST-RIS-GAR-CASO-MOR-NULL<br>*/
    public String getRstRisGarCasoMorNull() {
        return readString(Pos.RST_RIS_GAR_CASO_MOR_NULL, Len.RST_RIS_GAR_CASO_MOR_NULL);
    }

    public String getRstRisGarCasoMorNullFormatted() {
        return Functions.padBlanks(getRstRisGarCasoMorNull(), Len.RST_RIS_GAR_CASO_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_GAR_CASO_MOR = 1;
        public static final int RST_RIS_GAR_CASO_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_GAR_CASO_MOR = 8;
        public static final int RST_RIS_GAR_CASO_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_GAR_CASO_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_GAR_CASO_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
