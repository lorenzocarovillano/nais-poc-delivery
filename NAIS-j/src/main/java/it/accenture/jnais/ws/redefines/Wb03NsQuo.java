package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-NS-QUO<br>
 * Variable: WB03-NS-QUO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03NsQuo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03NsQuo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_NS_QUO;
    }

    public void setWb03NsQuoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_NS_QUO, Pos.WB03_NS_QUO);
    }

    /**Original name: WB03-NS-QUO<br>*/
    public AfDecimal getWb03NsQuo() {
        return readPackedAsDecimal(Pos.WB03_NS_QUO, Len.Int.WB03_NS_QUO, Len.Fract.WB03_NS_QUO);
    }

    public byte[] getWb03NsQuoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_NS_QUO, Pos.WB03_NS_QUO);
        return buffer;
    }

    public void setWb03NsQuoNull(String wb03NsQuoNull) {
        writeString(Pos.WB03_NS_QUO_NULL, wb03NsQuoNull, Len.WB03_NS_QUO_NULL);
    }

    /**Original name: WB03-NS-QUO-NULL<br>*/
    public String getWb03NsQuoNull() {
        return readString(Pos.WB03_NS_QUO_NULL, Len.WB03_NS_QUO_NULL);
    }

    public String getWb03NsQuoNullFormatted() {
        return Functions.padBlanks(getWb03NsQuoNull(), Len.WB03_NS_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_NS_QUO = 1;
        public static final int WB03_NS_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_NS_QUO = 4;
        public static final int WB03_NS_QUO_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_NS_QUO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_NS_QUO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
