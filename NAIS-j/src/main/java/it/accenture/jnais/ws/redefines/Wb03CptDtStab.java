package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CPT-DT-STAB<br>
 * Variable: WB03-CPT-DT-STAB from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CptDtStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CptDtStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CPT_DT_STAB;
    }

    public void setWb03CptDtStab(AfDecimal wb03CptDtStab) {
        writeDecimalAsPacked(Pos.WB03_CPT_DT_STAB, wb03CptDtStab.copy());
    }

    public void setWb03CptDtStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CPT_DT_STAB, Pos.WB03_CPT_DT_STAB);
    }

    /**Original name: WB03-CPT-DT-STAB<br>*/
    public AfDecimal getWb03CptDtStab() {
        return readPackedAsDecimal(Pos.WB03_CPT_DT_STAB, Len.Int.WB03_CPT_DT_STAB, Len.Fract.WB03_CPT_DT_STAB);
    }

    public byte[] getWb03CptDtStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CPT_DT_STAB, Pos.WB03_CPT_DT_STAB);
        return buffer;
    }

    public void setWb03CptDtStabNull(String wb03CptDtStabNull) {
        writeString(Pos.WB03_CPT_DT_STAB_NULL, wb03CptDtStabNull, Len.WB03_CPT_DT_STAB_NULL);
    }

    /**Original name: WB03-CPT-DT-STAB-NULL<br>*/
    public String getWb03CptDtStabNull() {
        return readString(Pos.WB03_CPT_DT_STAB_NULL, Len.WB03_CPT_DT_STAB_NULL);
    }

    public String getWb03CptDtStabNullFormatted() {
        return Functions.padBlanks(getWb03CptDtStabNull(), Len.WB03_CPT_DT_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CPT_DT_STAB = 1;
        public static final int WB03_CPT_DT_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CPT_DT_STAB = 8;
        public static final int WB03_CPT_DT_STAB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CPT_DT_STAB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CPT_DT_STAB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
