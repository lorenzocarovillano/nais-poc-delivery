package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B01-NUM-QUO-DT-CALC<br>
 * Variable: W-B01-NUM-QUO-DT-CALC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB01NumQuoDtCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB01NumQuoDtCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B01_NUM_QUO_DT_CALC;
    }

    public void setwB01NumQuoDtCalc(AfDecimal wB01NumQuoDtCalc) {
        writeDecimalAsPacked(Pos.W_B01_NUM_QUO_DT_CALC, wB01NumQuoDtCalc.copy());
    }

    /**Original name: W-B01-NUM-QUO-DT-CALC<br>*/
    public AfDecimal getwB01NumQuoDtCalc() {
        return readPackedAsDecimal(Pos.W_B01_NUM_QUO_DT_CALC, Len.Int.W_B01_NUM_QUO_DT_CALC, Len.Fract.W_B01_NUM_QUO_DT_CALC);
    }

    public byte[] getwB01NumQuoDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B01_NUM_QUO_DT_CALC, Pos.W_B01_NUM_QUO_DT_CALC);
        return buffer;
    }

    public void setwB01NumQuoDtCalcNull(String wB01NumQuoDtCalcNull) {
        writeString(Pos.W_B01_NUM_QUO_DT_CALC_NULL, wB01NumQuoDtCalcNull, Len.W_B01_NUM_QUO_DT_CALC_NULL);
    }

    /**Original name: W-B01-NUM-QUO-DT-CALC-NULL<br>*/
    public String getwB01NumQuoDtCalcNull() {
        return readString(Pos.W_B01_NUM_QUO_DT_CALC_NULL, Len.W_B01_NUM_QUO_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B01_NUM_QUO_DT_CALC = 1;
        public static final int W_B01_NUM_QUO_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B01_NUM_QUO_DT_CALC = 7;
        public static final int W_B01_NUM_QUO_DT_CALC_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B01_NUM_QUO_DT_CALC = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B01_NUM_QUO_DT_CALC = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
