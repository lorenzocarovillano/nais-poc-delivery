package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-RENDTO-RETR<br>
 * Variable: TGA-RENDTO-RETR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaRendtoRetr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaRendtoRetr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_RENDTO_RETR;
    }

    public void setTgaRendtoRetr(AfDecimal tgaRendtoRetr) {
        writeDecimalAsPacked(Pos.TGA_RENDTO_RETR, tgaRendtoRetr.copy());
    }

    public void setTgaRendtoRetrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_RENDTO_RETR, Pos.TGA_RENDTO_RETR);
    }

    /**Original name: TGA-RENDTO-RETR<br>*/
    public AfDecimal getTgaRendtoRetr() {
        return readPackedAsDecimal(Pos.TGA_RENDTO_RETR, Len.Int.TGA_RENDTO_RETR, Len.Fract.TGA_RENDTO_RETR);
    }

    public byte[] getTgaRendtoRetrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_RENDTO_RETR, Pos.TGA_RENDTO_RETR);
        return buffer;
    }

    public void setTgaRendtoRetrNull(String tgaRendtoRetrNull) {
        writeString(Pos.TGA_RENDTO_RETR_NULL, tgaRendtoRetrNull, Len.TGA_RENDTO_RETR_NULL);
    }

    /**Original name: TGA-RENDTO-RETR-NULL<br>*/
    public String getTgaRendtoRetrNull() {
        return readString(Pos.TGA_RENDTO_RETR_NULL, Len.TGA_RENDTO_RETR_NULL);
    }

    public String getTgaRendtoRetrNullFormatted() {
        return Functions.padBlanks(getTgaRendtoRetrNull(), Len.TGA_RENDTO_RETR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_RENDTO_RETR = 1;
        public static final int TGA_RENDTO_RETR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_RENDTO_RETR = 8;
        public static final int TGA_RENDTO_RETR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_RENDTO_RETR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_RENDTO_RETR = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
