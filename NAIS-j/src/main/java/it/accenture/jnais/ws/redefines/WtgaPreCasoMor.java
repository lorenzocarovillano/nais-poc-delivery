package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-CASO-MOR<br>
 * Variable: WTGA-PRE-CASO-MOR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPreCasoMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPreCasoMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_CASO_MOR;
    }

    public void setWtgaPreCasoMor(AfDecimal wtgaPreCasoMor) {
        writeDecimalAsPacked(Pos.WTGA_PRE_CASO_MOR, wtgaPreCasoMor.copy());
    }

    public void setWtgaPreCasoMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_CASO_MOR, Pos.WTGA_PRE_CASO_MOR);
    }

    /**Original name: WTGA-PRE-CASO-MOR<br>*/
    public AfDecimal getWtgaPreCasoMor() {
        return readPackedAsDecimal(Pos.WTGA_PRE_CASO_MOR, Len.Int.WTGA_PRE_CASO_MOR, Len.Fract.WTGA_PRE_CASO_MOR);
    }

    public byte[] getWtgaPreCasoMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_CASO_MOR, Pos.WTGA_PRE_CASO_MOR);
        return buffer;
    }

    public void initWtgaPreCasoMorSpaces() {
        fill(Pos.WTGA_PRE_CASO_MOR, Len.WTGA_PRE_CASO_MOR, Types.SPACE_CHAR);
    }

    public void setWtgaPreCasoMorNull(String wtgaPreCasoMorNull) {
        writeString(Pos.WTGA_PRE_CASO_MOR_NULL, wtgaPreCasoMorNull, Len.WTGA_PRE_CASO_MOR_NULL);
    }

    /**Original name: WTGA-PRE-CASO-MOR-NULL<br>*/
    public String getWtgaPreCasoMorNull() {
        return readString(Pos.WTGA_PRE_CASO_MOR_NULL, Len.WTGA_PRE_CASO_MOR_NULL);
    }

    public String getWtgaPreCasoMorNullFormatted() {
        return Functions.padBlanks(getWtgaPreCasoMorNull(), Len.WTGA_PRE_CASO_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_CASO_MOR = 1;
        public static final int WTGA_PRE_CASO_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_CASO_MOR = 8;
        public static final int WTGA_PRE_CASO_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_CASO_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_CASO_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
