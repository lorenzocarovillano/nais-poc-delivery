package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP61-ID-TRCH-DI-GAR<br>
 * Variable: WP61-ID-TRCH-DI-GAR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp61IdTrchDiGar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp61IdTrchDiGar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP61_ID_TRCH_DI_GAR;
    }

    public void setWp61IdTrchDiGar(int wp61IdTrchDiGar) {
        writeIntAsPacked(Pos.WP61_ID_TRCH_DI_GAR, wp61IdTrchDiGar, Len.Int.WP61_ID_TRCH_DI_GAR);
    }

    public void setWp61IdTrchDiGarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP61_ID_TRCH_DI_GAR, Pos.WP61_ID_TRCH_DI_GAR);
    }

    /**Original name: WP61-ID-TRCH-DI-GAR<br>*/
    public int getWp61IdTrchDiGar() {
        return readPackedAsInt(Pos.WP61_ID_TRCH_DI_GAR, Len.Int.WP61_ID_TRCH_DI_GAR);
    }

    public byte[] getWp61IdTrchDiGarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP61_ID_TRCH_DI_GAR, Pos.WP61_ID_TRCH_DI_GAR);
        return buffer;
    }

    public void setWp61IdTrchDiGarNull(String wp61IdTrchDiGarNull) {
        writeString(Pos.WP61_ID_TRCH_DI_GAR_NULL, wp61IdTrchDiGarNull, Len.WP61_ID_TRCH_DI_GAR_NULL);
    }

    /**Original name: WP61-ID-TRCH-DI-GAR-NULL<br>*/
    public String getWp61IdTrchDiGarNull() {
        return readString(Pos.WP61_ID_TRCH_DI_GAR_NULL, Len.WP61_ID_TRCH_DI_GAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP61_ID_TRCH_DI_GAR = 1;
        public static final int WP61_ID_TRCH_DI_GAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_ID_TRCH_DI_GAR = 5;
        public static final int WP61_ID_TRCH_DI_GAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_ID_TRCH_DI_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
