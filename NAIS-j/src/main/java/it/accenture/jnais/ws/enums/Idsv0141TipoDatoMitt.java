package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0141-TIPO-DATO-MITT<br>
 * Variable: IDSV0141-TIPO-DATO-MITT from copybook IDSV0141<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0141TipoDatoMitt {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TIPO_DATO_MITT);
    public static final String ALFANUM = "AL";
    public static final String COMP = "C0";
    public static final String COMP1 = "C1";
    public static final String COMP3 = "C3";
    public static final String COMP4 = "C4";
    public static final String NUM_SEGN = "NS";
    public static final String NUM_NON_SEGN = "NN";

    //==== METHODS ====
    public void setTipoDatoMitt(String tipoDatoMitt) {
        this.value = Functions.subString(tipoDatoMitt, Len.TIPO_DATO_MITT);
    }

    public String getTipoDatoMitt() {
        return this.value;
    }

    public boolean isAlfanum() {
        return value.equals(ALFANUM);
    }

    public boolean isComp3() {
        return value.equals(COMP3);
    }

    public void setIdsv0141MittComp3() {
        value = COMP3;
    }

    public void setIdsv0141MittNumSegn() {
        value = NUM_SEGN;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_DATO_MITT = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
