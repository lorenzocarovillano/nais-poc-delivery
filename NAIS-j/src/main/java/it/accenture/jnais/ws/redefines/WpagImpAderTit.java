package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-ADER-TIT<br>
 * Variable: WPAG-IMP-ADER-TIT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpAderTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpAderTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_ADER_TIT;
    }

    public void setWpagImpAderTit(AfDecimal wpagImpAderTit) {
        writeDecimalAsPacked(Pos.WPAG_IMP_ADER_TIT, wpagImpAderTit.copy());
    }

    public void setWpagImpAderTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_ADER_TIT, Pos.WPAG_IMP_ADER_TIT);
    }

    /**Original name: WPAG-IMP-ADER-TIT<br>*/
    public AfDecimal getWpagImpAderTit() {
        return readPackedAsDecimal(Pos.WPAG_IMP_ADER_TIT, Len.Int.WPAG_IMP_ADER_TIT, Len.Fract.WPAG_IMP_ADER_TIT);
    }

    public byte[] getWpagImpAderTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_ADER_TIT, Pos.WPAG_IMP_ADER_TIT);
        return buffer;
    }

    public void initWpagImpAderTitSpaces() {
        fill(Pos.WPAG_IMP_ADER_TIT, Len.WPAG_IMP_ADER_TIT, Types.SPACE_CHAR);
    }

    public void setWpagImpAderTitNull(String wpagImpAderTitNull) {
        writeString(Pos.WPAG_IMP_ADER_TIT_NULL, wpagImpAderTitNull, Len.WPAG_IMP_ADER_TIT_NULL);
    }

    /**Original name: WPAG-IMP-ADER-TIT-NULL<br>*/
    public String getWpagImpAderTitNull() {
        return readString(Pos.WPAG_IMP_ADER_TIT_NULL, Len.WPAG_IMP_ADER_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ADER_TIT = 1;
        public static final int WPAG_IMP_ADER_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ADER_TIT = 8;
        public static final int WPAG_IMP_ADER_TIT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ADER_TIT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ADER_TIT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
