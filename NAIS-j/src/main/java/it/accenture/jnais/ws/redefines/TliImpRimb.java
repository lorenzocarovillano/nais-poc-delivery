package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-IMP-RIMB<br>
 * Variable: TLI-IMP-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliImpRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliImpRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_IMP_RIMB;
    }

    public void setTliImpRimb(AfDecimal tliImpRimb) {
        writeDecimalAsPacked(Pos.TLI_IMP_RIMB, tliImpRimb.copy());
    }

    public void setTliImpRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_IMP_RIMB, Pos.TLI_IMP_RIMB);
    }

    /**Original name: TLI-IMP-RIMB<br>*/
    public AfDecimal getTliImpRimb() {
        return readPackedAsDecimal(Pos.TLI_IMP_RIMB, Len.Int.TLI_IMP_RIMB, Len.Fract.TLI_IMP_RIMB);
    }

    public byte[] getTliImpRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_IMP_RIMB, Pos.TLI_IMP_RIMB);
        return buffer;
    }

    public void setTliImpRimbNull(String tliImpRimbNull) {
        writeString(Pos.TLI_IMP_RIMB_NULL, tliImpRimbNull, Len.TLI_IMP_RIMB_NULL);
    }

    /**Original name: TLI-IMP-RIMB-NULL<br>*/
    public String getTliImpRimbNull() {
        return readString(Pos.TLI_IMP_RIMB_NULL, Len.TLI_IMP_RIMB_NULL);
    }

    public String getTliImpRimbNullFormatted() {
        return Functions.padBlanks(getTliImpRimbNull(), Len.TLI_IMP_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_IMP_RIMB = 1;
        public static final int TLI_IMP_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_IMP_RIMB = 8;
        public static final int TLI_IMP_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_IMP_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_IMP_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
