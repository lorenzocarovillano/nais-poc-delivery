package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-PRE-PP-IAS<br>
 * Variable: WDTR-PRE-PP-IAS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrPrePpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrPrePpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_PRE_PP_IAS;
    }

    public void setWdtrPrePpIas(AfDecimal wdtrPrePpIas) {
        writeDecimalAsPacked(Pos.WDTR_PRE_PP_IAS, wdtrPrePpIas.copy());
    }

    /**Original name: WDTR-PRE-PP-IAS<br>*/
    public AfDecimal getWdtrPrePpIas() {
        return readPackedAsDecimal(Pos.WDTR_PRE_PP_IAS, Len.Int.WDTR_PRE_PP_IAS, Len.Fract.WDTR_PRE_PP_IAS);
    }

    public void setWdtrPrePpIasNull(String wdtrPrePpIasNull) {
        writeString(Pos.WDTR_PRE_PP_IAS_NULL, wdtrPrePpIasNull, Len.WDTR_PRE_PP_IAS_NULL);
    }

    /**Original name: WDTR-PRE-PP-IAS-NULL<br>*/
    public String getWdtrPrePpIasNull() {
        return readString(Pos.WDTR_PRE_PP_IAS_NULL, Len.WDTR_PRE_PP_IAS_NULL);
    }

    public String getWdtrPrePpIasNullFormatted() {
        return Functions.padBlanks(getWdtrPrePpIasNull(), Len.WDTR_PRE_PP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_PRE_PP_IAS = 1;
        public static final int WDTR_PRE_PP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_PRE_PP_IAS = 8;
        public static final int WDTR_PRE_PP_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_PRE_PP_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_PRE_PP_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
