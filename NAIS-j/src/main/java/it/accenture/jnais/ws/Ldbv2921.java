package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LDBV2921<br>
 * Variable: LDBV2921 from copybook LDBV2921<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv2921 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV2921-ID-POL
    private int idPol = DefaultValues.INT_VAL;
    //Original name: LDBV2921-DT-EFF
    private String dtEff = DefaultValues.stringVal(Len.DT_EFF);
    //Original name: LDBV2921-DT-EMIS
    private String dtEmis = DefaultValues.stringVal(Len.DT_EMIS);
    //Original name: LDBV2921-DT-DECOR-MAX
    private String dtDecorMax = DefaultValues.stringVal(Len.DT_DECOR_MAX);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV2921;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv2921Bytes(buf);
    }

    public String getLdbv2921Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv2921Bytes());
    }

    public void setLdbv2921Bytes(byte[] buffer) {
        setLdbv2921Bytes(buffer, 1);
    }

    public byte[] getLdbv2921Bytes() {
        byte[] buffer = new byte[Len.LDBV2921];
        return getLdbv2921Bytes(buffer, 1);
    }

    public void setLdbv2921Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPol = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POL, 0);
        position += Len.ID_POL;
        dtEff = MarshalByte.readFixedString(buffer, position, Len.DT_EFF);
        position += Len.DT_EFF;
        dtEmis = MarshalByte.readFixedString(buffer, position, Len.DT_EMIS);
        position += Len.DT_EMIS;
        dtDecorMax = MarshalByte.readFixedString(buffer, position, Len.DT_DECOR_MAX);
    }

    public byte[] getLdbv2921Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPol, Len.Int.ID_POL, 0);
        position += Len.ID_POL;
        MarshalByte.writeString(buffer, position, dtEff, Len.DT_EFF);
        position += Len.DT_EFF;
        MarshalByte.writeString(buffer, position, dtEmis, Len.DT_EMIS);
        position += Len.DT_EMIS;
        MarshalByte.writeString(buffer, position, dtDecorMax, Len.DT_DECOR_MAX);
        return buffer;
    }

    public void setIdPol(int idPol) {
        this.idPol = idPol;
    }

    public int getIdPol() {
        return this.idPol;
    }

    public void setDtEffFormatted(String dtEff) {
        this.dtEff = Trunc.toUnsignedNumeric(dtEff, Len.DT_EFF);
    }

    public int getDtEff() {
        return NumericDisplay.asInt(this.dtEff);
    }

    public void setDtEmis(int dtEmis) {
        this.dtEmis = NumericDisplay.asString(dtEmis, Len.DT_EMIS);
    }

    public void setDtEmisFormatted(String dtEmis) {
        this.dtEmis = Trunc.toUnsignedNumeric(dtEmis, Len.DT_EMIS);
    }

    public int getDtEmis() {
        return NumericDisplay.asInt(this.dtEmis);
    }

    public String getDtEmisFormatted() {
        return this.dtEmis;
    }

    public void setDtDecorMaxFormatted(String dtDecorMax) {
        this.dtDecorMax = Trunc.toUnsignedNumeric(dtDecorMax, Len.DT_DECOR_MAX);
    }

    public int getDtDecorMax() {
        return NumericDisplay.asInt(this.dtDecorMax);
    }

    public String getDtDecorMaxFormatted() {
        return this.dtDecorMax;
    }

    @Override
    public byte[] serialize() {
        return getLdbv2921Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POL = 5;
        public static final int DT_EFF = 8;
        public static final int DT_EMIS = 8;
        public static final int DT_DECOR_MAX = 8;
        public static final int LDBV2921 = ID_POL + DT_EFF + DT_EMIS + DT_DECOR_MAX;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POL = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
