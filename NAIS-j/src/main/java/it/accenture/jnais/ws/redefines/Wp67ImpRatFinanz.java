package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-IMP-RAT-FINANZ<br>
 * Variable: WP67-IMP-RAT-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67ImpRatFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67ImpRatFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_IMP_RAT_FINANZ;
    }

    public void setWp67ImpRatFinanz(AfDecimal wp67ImpRatFinanz) {
        writeDecimalAsPacked(Pos.WP67_IMP_RAT_FINANZ, wp67ImpRatFinanz.copy());
    }

    public void setWp67ImpRatFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_IMP_RAT_FINANZ, Pos.WP67_IMP_RAT_FINANZ);
    }

    /**Original name: WP67-IMP-RAT-FINANZ<br>*/
    public AfDecimal getWp67ImpRatFinanz() {
        return readPackedAsDecimal(Pos.WP67_IMP_RAT_FINANZ, Len.Int.WP67_IMP_RAT_FINANZ, Len.Fract.WP67_IMP_RAT_FINANZ);
    }

    public byte[] getWp67ImpRatFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_IMP_RAT_FINANZ, Pos.WP67_IMP_RAT_FINANZ);
        return buffer;
    }

    public void setWp67ImpRatFinanzNull(String wp67ImpRatFinanzNull) {
        writeString(Pos.WP67_IMP_RAT_FINANZ_NULL, wp67ImpRatFinanzNull, Len.WP67_IMP_RAT_FINANZ_NULL);
    }

    /**Original name: WP67-IMP-RAT-FINANZ-NULL<br>*/
    public String getWp67ImpRatFinanzNull() {
        return readString(Pos.WP67_IMP_RAT_FINANZ_NULL, Len.WP67_IMP_RAT_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_IMP_RAT_FINANZ = 1;
        public static final int WP67_IMP_RAT_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_IMP_RAT_FINANZ = 8;
        public static final int WP67_IMP_RAT_FINANZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_IMP_RAT_FINANZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_IMP_RAT_FINANZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
