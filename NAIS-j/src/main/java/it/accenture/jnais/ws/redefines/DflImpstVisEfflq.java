package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-VIS-EFFLQ<br>
 * Variable: DFL-IMPST-VIS-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstVisEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstVisEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_VIS_EFFLQ;
    }

    public void setDflImpstVisEfflq(AfDecimal dflImpstVisEfflq) {
        writeDecimalAsPacked(Pos.DFL_IMPST_VIS_EFFLQ, dflImpstVisEfflq.copy());
    }

    public void setDflImpstVisEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_VIS_EFFLQ, Pos.DFL_IMPST_VIS_EFFLQ);
    }

    /**Original name: DFL-IMPST-VIS-EFFLQ<br>*/
    public AfDecimal getDflImpstVisEfflq() {
        return readPackedAsDecimal(Pos.DFL_IMPST_VIS_EFFLQ, Len.Int.DFL_IMPST_VIS_EFFLQ, Len.Fract.DFL_IMPST_VIS_EFFLQ);
    }

    public byte[] getDflImpstVisEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_VIS_EFFLQ, Pos.DFL_IMPST_VIS_EFFLQ);
        return buffer;
    }

    public void setDflImpstVisEfflqNull(String dflImpstVisEfflqNull) {
        writeString(Pos.DFL_IMPST_VIS_EFFLQ_NULL, dflImpstVisEfflqNull, Len.DFL_IMPST_VIS_EFFLQ_NULL);
    }

    /**Original name: DFL-IMPST-VIS-EFFLQ-NULL<br>*/
    public String getDflImpstVisEfflqNull() {
        return readString(Pos.DFL_IMPST_VIS_EFFLQ_NULL, Len.DFL_IMPST_VIS_EFFLQ_NULL);
    }

    public String getDflImpstVisEfflqNullFormatted() {
        return Functions.padBlanks(getDflImpstVisEfflqNull(), Len.DFL_IMPST_VIS_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS_EFFLQ = 1;
        public static final int DFL_IMPST_VIS_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS_EFFLQ = 8;
        public static final int DFL_IMPST_VIS_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
