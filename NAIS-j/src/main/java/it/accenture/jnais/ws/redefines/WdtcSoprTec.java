package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-SOPR-TEC<br>
 * Variable: WDTC-SOPR-TEC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_SOPR_TEC;
    }

    public void setWdtcSoprTec(AfDecimal wdtcSoprTec) {
        writeDecimalAsPacked(Pos.WDTC_SOPR_TEC, wdtcSoprTec.copy());
    }

    public void setWdtcSoprTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_SOPR_TEC, Pos.WDTC_SOPR_TEC);
    }

    /**Original name: WDTC-SOPR-TEC<br>*/
    public AfDecimal getWdtcSoprTec() {
        return readPackedAsDecimal(Pos.WDTC_SOPR_TEC, Len.Int.WDTC_SOPR_TEC, Len.Fract.WDTC_SOPR_TEC);
    }

    public byte[] getWdtcSoprTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_SOPR_TEC, Pos.WDTC_SOPR_TEC);
        return buffer;
    }

    public void initWdtcSoprTecSpaces() {
        fill(Pos.WDTC_SOPR_TEC, Len.WDTC_SOPR_TEC, Types.SPACE_CHAR);
    }

    public void setWdtcSoprTecNull(String wdtcSoprTecNull) {
        writeString(Pos.WDTC_SOPR_TEC_NULL, wdtcSoprTecNull, Len.WDTC_SOPR_TEC_NULL);
    }

    /**Original name: WDTC-SOPR-TEC-NULL<br>*/
    public String getWdtcSoprTecNull() {
        return readString(Pos.WDTC_SOPR_TEC_NULL, Len.WDTC_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_SOPR_TEC = 1;
        public static final int WDTC_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_SOPR_TEC = 8;
        public static final int WDTC_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
