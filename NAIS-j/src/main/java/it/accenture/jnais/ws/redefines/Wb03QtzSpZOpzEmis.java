package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-QTZ-SP-Z-OPZ-EMIS<br>
 * Variable: WB03-QTZ-SP-Z-OPZ-EMIS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03QtzSpZOpzEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03QtzSpZOpzEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_QTZ_SP_Z_OPZ_EMIS;
    }

    public void setWb03QtzSpZOpzEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_QTZ_SP_Z_OPZ_EMIS, Pos.WB03_QTZ_SP_Z_OPZ_EMIS);
    }

    /**Original name: WB03-QTZ-SP-Z-OPZ-EMIS<br>*/
    public AfDecimal getWb03QtzSpZOpzEmis() {
        return readPackedAsDecimal(Pos.WB03_QTZ_SP_Z_OPZ_EMIS, Len.Int.WB03_QTZ_SP_Z_OPZ_EMIS, Len.Fract.WB03_QTZ_SP_Z_OPZ_EMIS);
    }

    public byte[] getWb03QtzSpZOpzEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_QTZ_SP_Z_OPZ_EMIS, Pos.WB03_QTZ_SP_Z_OPZ_EMIS);
        return buffer;
    }

    public void setWb03QtzSpZOpzEmisNull(String wb03QtzSpZOpzEmisNull) {
        writeString(Pos.WB03_QTZ_SP_Z_OPZ_EMIS_NULL, wb03QtzSpZOpzEmisNull, Len.WB03_QTZ_SP_Z_OPZ_EMIS_NULL);
    }

    /**Original name: WB03-QTZ-SP-Z-OPZ-EMIS-NULL<br>*/
    public String getWb03QtzSpZOpzEmisNull() {
        return readString(Pos.WB03_QTZ_SP_Z_OPZ_EMIS_NULL, Len.WB03_QTZ_SP_Z_OPZ_EMIS_NULL);
    }

    public String getWb03QtzSpZOpzEmisNullFormatted() {
        return Functions.padBlanks(getWb03QtzSpZOpzEmisNull(), Len.WB03_QTZ_SP_Z_OPZ_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_SP_Z_OPZ_EMIS = 1;
        public static final int WB03_QTZ_SP_Z_OPZ_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_SP_Z_OPZ_EMIS = 7;
        public static final int WB03_QTZ_SP_Z_OPZ_EMIS_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_SP_Z_OPZ_EMIS = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_SP_Z_OPZ_EMIS = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
