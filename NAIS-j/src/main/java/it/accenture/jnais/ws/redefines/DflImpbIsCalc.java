package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-IS-CALC<br>
 * Variable: DFL-IMPB-IS-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbIsCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbIsCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_IS_CALC;
    }

    public void setDflImpbIsCalc(AfDecimal dflImpbIsCalc) {
        writeDecimalAsPacked(Pos.DFL_IMPB_IS_CALC, dflImpbIsCalc.copy());
    }

    public void setDflImpbIsCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_IS_CALC, Pos.DFL_IMPB_IS_CALC);
    }

    /**Original name: DFL-IMPB-IS-CALC<br>*/
    public AfDecimal getDflImpbIsCalc() {
        return readPackedAsDecimal(Pos.DFL_IMPB_IS_CALC, Len.Int.DFL_IMPB_IS_CALC, Len.Fract.DFL_IMPB_IS_CALC);
    }

    public byte[] getDflImpbIsCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_IS_CALC, Pos.DFL_IMPB_IS_CALC);
        return buffer;
    }

    public void setDflImpbIsCalcNull(String dflImpbIsCalcNull) {
        writeString(Pos.DFL_IMPB_IS_CALC_NULL, dflImpbIsCalcNull, Len.DFL_IMPB_IS_CALC_NULL);
    }

    /**Original name: DFL-IMPB-IS-CALC-NULL<br>*/
    public String getDflImpbIsCalcNull() {
        return readString(Pos.DFL_IMPB_IS_CALC_NULL, Len.DFL_IMPB_IS_CALC_NULL);
    }

    public String getDflImpbIsCalcNullFormatted() {
        return Functions.padBlanks(getDflImpbIsCalcNull(), Len.DFL_IMPB_IS_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_IS_CALC = 1;
        public static final int DFL_IMPB_IS_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_IS_CALC = 8;
        public static final int DFL_IMPB_IS_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_IS_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_IS_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
