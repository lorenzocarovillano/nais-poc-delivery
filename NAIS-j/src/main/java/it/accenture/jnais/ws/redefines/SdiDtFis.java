package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: SDI-DT-FIS<br>
 * Variable: SDI-DT-FIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SdiDtFis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public SdiDtFis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SDI_DT_FIS;
    }

    public void setSdiDtFis(short sdiDtFis) {
        writeShortAsPacked(Pos.SDI_DT_FIS, sdiDtFis, Len.Int.SDI_DT_FIS);
    }

    public void setSdiDtFisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SDI_DT_FIS, Pos.SDI_DT_FIS);
    }

    /**Original name: SDI-DT-FIS<br>*/
    public short getSdiDtFis() {
        return readPackedAsShort(Pos.SDI_DT_FIS, Len.Int.SDI_DT_FIS);
    }

    public byte[] getSdiDtFisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.SDI_DT_FIS, Pos.SDI_DT_FIS);
        return buffer;
    }

    public void setSdiDtFisNull(String sdiDtFisNull) {
        writeString(Pos.SDI_DT_FIS_NULL, sdiDtFisNull, Len.SDI_DT_FIS_NULL);
    }

    /**Original name: SDI-DT-FIS-NULL<br>*/
    public String getSdiDtFisNull() {
        return readString(Pos.SDI_DT_FIS_NULL, Len.SDI_DT_FIS_NULL);
    }

    public String getSdiDtFisNullFormatted() {
        return Functions.padBlanks(getSdiDtFisNull(), Len.SDI_DT_FIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SDI_DT_FIS = 1;
        public static final int SDI_DT_FIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SDI_DT_FIS = 3;
        public static final int SDI_DT_FIS_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SDI_DT_FIS = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
