package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: SDI-MOD-GEST<br>
 * Variable: SDI-MOD-GEST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SdiModGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public SdiModGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SDI_MOD_GEST;
    }

    public void setSdiModGest(short sdiModGest) {
        writeShortAsPacked(Pos.SDI_MOD_GEST, sdiModGest, Len.Int.SDI_MOD_GEST);
    }

    public void setSdiModGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SDI_MOD_GEST, Pos.SDI_MOD_GEST);
    }

    /**Original name: SDI-MOD-GEST<br>*/
    public short getSdiModGest() {
        return readPackedAsShort(Pos.SDI_MOD_GEST, Len.Int.SDI_MOD_GEST);
    }

    public byte[] getSdiModGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.SDI_MOD_GEST, Pos.SDI_MOD_GEST);
        return buffer;
    }

    public void setSdiModGestNull(String sdiModGestNull) {
        writeString(Pos.SDI_MOD_GEST_NULL, sdiModGestNull, Len.SDI_MOD_GEST_NULL);
    }

    /**Original name: SDI-MOD-GEST-NULL<br>*/
    public String getSdiModGestNull() {
        return readString(Pos.SDI_MOD_GEST_NULL, Len.SDI_MOD_GEST_NULL);
    }

    public String getSdiModGestNullFormatted() {
        return Functions.padBlanks(getSdiModGestNull(), Len.SDI_MOD_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SDI_MOD_GEST = 1;
        public static final int SDI_MOD_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SDI_MOD_GEST = 2;
        public static final int SDI_MOD_GEST_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SDI_MOD_GEST = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
