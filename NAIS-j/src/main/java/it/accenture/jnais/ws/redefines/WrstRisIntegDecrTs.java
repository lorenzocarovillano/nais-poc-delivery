package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-INTEG-DECR-TS<br>
 * Variable: WRST-RIS-INTEG-DECR-TS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisIntegDecrTs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisIntegDecrTs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_INTEG_DECR_TS;
    }

    public void setWrstRisIntegDecrTs(AfDecimal wrstRisIntegDecrTs) {
        writeDecimalAsPacked(Pos.WRST_RIS_INTEG_DECR_TS, wrstRisIntegDecrTs.copy());
    }

    public void setWrstRisIntegDecrTsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_INTEG_DECR_TS, Pos.WRST_RIS_INTEG_DECR_TS);
    }

    /**Original name: WRST-RIS-INTEG-DECR-TS<br>*/
    public AfDecimal getWrstRisIntegDecrTs() {
        return readPackedAsDecimal(Pos.WRST_RIS_INTEG_DECR_TS, Len.Int.WRST_RIS_INTEG_DECR_TS, Len.Fract.WRST_RIS_INTEG_DECR_TS);
    }

    public byte[] getWrstRisIntegDecrTsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_INTEG_DECR_TS, Pos.WRST_RIS_INTEG_DECR_TS);
        return buffer;
    }

    public void initWrstRisIntegDecrTsSpaces() {
        fill(Pos.WRST_RIS_INTEG_DECR_TS, Len.WRST_RIS_INTEG_DECR_TS, Types.SPACE_CHAR);
    }

    public void setWrstRisIntegDecrTsNull(String wrstRisIntegDecrTsNull) {
        writeString(Pos.WRST_RIS_INTEG_DECR_TS_NULL, wrstRisIntegDecrTsNull, Len.WRST_RIS_INTEG_DECR_TS_NULL);
    }

    /**Original name: WRST-RIS-INTEG-DECR-TS-NULL<br>*/
    public String getWrstRisIntegDecrTsNull() {
        return readString(Pos.WRST_RIS_INTEG_DECR_TS_NULL, Len.WRST_RIS_INTEG_DECR_TS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_INTEG_DECR_TS = 1;
        public static final int WRST_RIS_INTEG_DECR_TS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_INTEG_DECR_TS = 8;
        public static final int WRST_RIS_INTEG_DECR_TS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_INTEG_DECR_TS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_INTEG_DECR_TS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
