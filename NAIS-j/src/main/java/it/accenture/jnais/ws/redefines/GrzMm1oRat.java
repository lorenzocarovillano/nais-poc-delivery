package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-MM-1O-RAT<br>
 * Variable: GRZ-MM-1O-RAT from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzMm1oRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzMm1oRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_MM1O_RAT;
    }

    public void setGrzMm1oRat(short grzMm1oRat) {
        writeShortAsPacked(Pos.GRZ_MM1O_RAT, grzMm1oRat, Len.Int.GRZ_MM1O_RAT);
    }

    public void setGrzMm1oRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_MM1O_RAT, Pos.GRZ_MM1O_RAT);
    }

    /**Original name: GRZ-MM-1O-RAT<br>*/
    public short getGrzMm1oRat() {
        return readPackedAsShort(Pos.GRZ_MM1O_RAT, Len.Int.GRZ_MM1O_RAT);
    }

    public byte[] getGrzMm1oRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_MM1O_RAT, Pos.GRZ_MM1O_RAT);
        return buffer;
    }

    public void setGrzMm1oRatNull(String grzMm1oRatNull) {
        writeString(Pos.GRZ_MM1O_RAT_NULL, grzMm1oRatNull, Len.GRZ_MM1O_RAT_NULL);
    }

    /**Original name: GRZ-MM-1O-RAT-NULL<br>*/
    public String getGrzMm1oRatNull() {
        return readString(Pos.GRZ_MM1O_RAT_NULL, Len.GRZ_MM1O_RAT_NULL);
    }

    public String getGrzMm1oRatNullFormatted() {
        return Functions.padBlanks(getGrzMm1oRatNull(), Len.GRZ_MM1O_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_MM1O_RAT = 1;
        public static final int GRZ_MM1O_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_MM1O_RAT = 2;
        public static final int GRZ_MM1O_RAT_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_MM1O_RAT = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
