package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPRE-IMP-PREST<br>
 * Variable: WPRE-IMP-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreImpPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpreImpPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_IMP_PREST;
    }

    public void setWpreImpPrest(AfDecimal wpreImpPrest) {
        writeDecimalAsPacked(Pos.WPRE_IMP_PREST, wpreImpPrest.copy());
    }

    public void setWpreImpPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_IMP_PREST, Pos.WPRE_IMP_PREST);
    }

    /**Original name: WPRE-IMP-PREST<br>*/
    public AfDecimal getWpreImpPrest() {
        return readPackedAsDecimal(Pos.WPRE_IMP_PREST, Len.Int.WPRE_IMP_PREST, Len.Fract.WPRE_IMP_PREST);
    }

    public byte[] getWpreImpPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_IMP_PREST, Pos.WPRE_IMP_PREST);
        return buffer;
    }

    public void initWpreImpPrestSpaces() {
        fill(Pos.WPRE_IMP_PREST, Len.WPRE_IMP_PREST, Types.SPACE_CHAR);
    }

    public void setWpreImpPrestNull(String wpreImpPrestNull) {
        writeString(Pos.WPRE_IMP_PREST_NULL, wpreImpPrestNull, Len.WPRE_IMP_PREST_NULL);
    }

    /**Original name: WPRE-IMP-PREST-NULL<br>*/
    public String getWpreImpPrestNull() {
        return readString(Pos.WPRE_IMP_PREST_NULL, Len.WPRE_IMP_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_IMP_PREST = 1;
        public static final int WPRE_IMP_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_IMP_PREST = 8;
        public static final int WPRE_IMP_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPRE_IMP_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_IMP_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
