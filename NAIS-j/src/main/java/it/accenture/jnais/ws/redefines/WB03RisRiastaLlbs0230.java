package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-RIS-RIASTA<br>
 * Variable: W-B03-RIS-RIASTA from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03RisRiastaLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03RisRiastaLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_RIS_RIASTA;
    }

    public void setwB03RisRiasta(AfDecimal wB03RisRiasta) {
        writeDecimalAsPacked(Pos.W_B03_RIS_RIASTA, wB03RisRiasta.copy());
    }

    /**Original name: W-B03-RIS-RIASTA<br>*/
    public AfDecimal getwB03RisRiasta() {
        return readPackedAsDecimal(Pos.W_B03_RIS_RIASTA, Len.Int.W_B03_RIS_RIASTA, Len.Fract.W_B03_RIS_RIASTA);
    }

    public byte[] getwB03RisRiastaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_RIS_RIASTA, Pos.W_B03_RIS_RIASTA);
        return buffer;
    }

    public void setwB03RisRiastaNull(String wB03RisRiastaNull) {
        writeString(Pos.W_B03_RIS_RIASTA_NULL, wB03RisRiastaNull, Len.W_B03_RIS_RIASTA_NULL);
    }

    /**Original name: W-B03-RIS-RIASTA-NULL<br>*/
    public String getwB03RisRiastaNull() {
        return readString(Pos.W_B03_RIS_RIASTA_NULL, Len.W_B03_RIS_RIASTA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_RIASTA = 1;
        public static final int W_B03_RIS_RIASTA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_RIASTA = 8;
        public static final int W_B03_RIS_RIASTA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_RIASTA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_RIASTA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
