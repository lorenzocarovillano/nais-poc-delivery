package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: V1391-IDP-MATR-VAL-VAR<br>
 * Variable: V1391-IDP-MATR-VAL-VAR from program LDBS1390<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class V1391IdpMatrValVar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public V1391IdpMatrValVar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.V1391_IDP_MATR_VAL_VAR;
    }

    public void setV1391IdpMatrValVar(int v1391IdpMatrValVar) {
        writeBinaryInt(Pos.V1391_IDP_MATR_VAL_VAR, v1391IdpMatrValVar);
    }

    public void setV1391IdpMatrValVarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Types.INT_SIZE, Pos.V1391_IDP_MATR_VAL_VAR);
    }

    /**Original name: V1391-IDP-MATR-VAL-VAR<br>*/
    public int getV1391IdpMatrValVar() {
        return readBinaryInt(Pos.V1391_IDP_MATR_VAL_VAR);
    }

    public byte[] getV1391IdpMatrValVarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Types.INT_SIZE, Pos.V1391_IDP_MATR_VAL_VAR);
        return buffer;
    }

    public void initV1391IdpMatrValVarSpaces() {
        fill(Pos.V1391_IDP_MATR_VAL_VAR, Len.V1391_IDP_MATR_VAL_VAR, Types.SPACE_CHAR);
    }

    public void setV1391IdpMatrValVarNull(String v1391IdpMatrValVarNull) {
        writeString(Pos.V1391_IDP_MATR_VAL_VAR_NULL, v1391IdpMatrValVarNull, Len.V1391_IDP_MATR_VAL_VAR_NULL);
    }

    /**Original name: V1391-IDP-MATR-VAL-VAR-NULL<br>*/
    public String getV1391IdpMatrValVarNull() {
        return readString(Pos.V1391_IDP_MATR_VAL_VAR_NULL, Len.V1391_IDP_MATR_VAL_VAR_NULL);
    }

    public String getV1391IdpMatrValVarNullFormatted() {
        return Functions.padBlanks(getV1391IdpMatrValVarNull(), Len.V1391_IDP_MATR_VAL_VAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int V1391_IDP_MATR_VAL_VAR = 1;
        public static final int V1391_IDP_MATR_VAL_VAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int V1391_IDP_MATR_VAL_VAR = 4;
        public static final int V1391_IDP_MATR_VAL_VAR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
