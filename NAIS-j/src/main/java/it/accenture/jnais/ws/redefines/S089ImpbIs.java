package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPB-IS<br>
 * Variable: S089-IMPB-IS from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpbIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpbIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPB_IS;
    }

    public void setWlquImpbIs(AfDecimal wlquImpbIs) {
        writeDecimalAsPacked(Pos.S089_IMPB_IS, wlquImpbIs.copy());
    }

    public void setWlquImpbIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPB_IS, Pos.S089_IMPB_IS);
    }

    /**Original name: WLQU-IMPB-IS<br>*/
    public AfDecimal getWlquImpbIs() {
        return readPackedAsDecimal(Pos.S089_IMPB_IS, Len.Int.WLQU_IMPB_IS, Len.Fract.WLQU_IMPB_IS);
    }

    public byte[] getWlquImpbIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPB_IS, Pos.S089_IMPB_IS);
        return buffer;
    }

    public void initWlquImpbIsSpaces() {
        fill(Pos.S089_IMPB_IS, Len.S089_IMPB_IS, Types.SPACE_CHAR);
    }

    public void setWlquImpbIsNull(String wlquImpbIsNull) {
        writeString(Pos.S089_IMPB_IS_NULL, wlquImpbIsNull, Len.WLQU_IMPB_IS_NULL);
    }

    /**Original name: WLQU-IMPB-IS-NULL<br>*/
    public String getWlquImpbIsNull() {
        return readString(Pos.S089_IMPB_IS_NULL, Len.WLQU_IMPB_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPB_IS = 1;
        public static final int S089_IMPB_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPB_IS = 8;
        public static final int WLQU_IMPB_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
