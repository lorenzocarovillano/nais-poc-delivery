package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.redefines.WsTimestamp;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IABS0090<br>
 * Generated as a class for rule WS.<br>*/
public class Iabs0090Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WS-TIMESTAMP
    private WsTimestamp wsTimestamp = new WsTimestamp();
    //Original name: LCCC0090
    private LinkArea lccc0090 = new LinkArea();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public LinkArea getLccc0090() {
        return lccc0090;
    }

    public WsTimestamp getWsTimestamp() {
        return wsTimestamp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
