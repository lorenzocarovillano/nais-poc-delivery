package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RDF-COMMIS-GEST<br>
 * Variable: RDF-COMMIS-GEST from program IDBSRDF0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RdfCommisGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RdfCommisGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RDF_COMMIS_GEST;
    }

    public void setRdfCommisGest(AfDecimal rdfCommisGest) {
        writeDecimalAsPacked(Pos.RDF_COMMIS_GEST, rdfCommisGest.copy());
    }

    public void setRdfCommisGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RDF_COMMIS_GEST, Pos.RDF_COMMIS_GEST);
    }

    /**Original name: RDF-COMMIS-GEST<br>*/
    public AfDecimal getRdfCommisGest() {
        return readPackedAsDecimal(Pos.RDF_COMMIS_GEST, Len.Int.RDF_COMMIS_GEST, Len.Fract.RDF_COMMIS_GEST);
    }

    public byte[] getRdfCommisGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RDF_COMMIS_GEST, Pos.RDF_COMMIS_GEST);
        return buffer;
    }

    public void setRdfCommisGestNull(String rdfCommisGestNull) {
        writeString(Pos.RDF_COMMIS_GEST_NULL, rdfCommisGestNull, Len.RDF_COMMIS_GEST_NULL);
    }

    /**Original name: RDF-COMMIS-GEST-NULL<br>*/
    public String getRdfCommisGestNull() {
        return readString(Pos.RDF_COMMIS_GEST_NULL, Len.RDF_COMMIS_GEST_NULL);
    }

    public String getRdfCommisGestNullFormatted() {
        return Functions.padBlanks(getRdfCommisGestNull(), Len.RDF_COMMIS_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RDF_COMMIS_GEST = 1;
        public static final int RDF_COMMIS_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_COMMIS_GEST = 10;
        public static final int RDF_COMMIS_GEST_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_COMMIS_GEST = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RDF_COMMIS_GEST = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
