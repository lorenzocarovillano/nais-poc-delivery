package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-CPT-RSH-MOR<br>
 * Variable: TGA-CPT-RSH-MOR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaCptRshMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaCptRshMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_CPT_RSH_MOR;
    }

    public void setTgaCptRshMor(AfDecimal tgaCptRshMor) {
        writeDecimalAsPacked(Pos.TGA_CPT_RSH_MOR, tgaCptRshMor.copy());
    }

    public void setTgaCptRshMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_CPT_RSH_MOR, Pos.TGA_CPT_RSH_MOR);
    }

    /**Original name: TGA-CPT-RSH-MOR<br>*/
    public AfDecimal getTgaCptRshMor() {
        return readPackedAsDecimal(Pos.TGA_CPT_RSH_MOR, Len.Int.TGA_CPT_RSH_MOR, Len.Fract.TGA_CPT_RSH_MOR);
    }

    public byte[] getTgaCptRshMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_CPT_RSH_MOR, Pos.TGA_CPT_RSH_MOR);
        return buffer;
    }

    public void setTgaCptRshMorNull(String tgaCptRshMorNull) {
        writeString(Pos.TGA_CPT_RSH_MOR_NULL, tgaCptRshMorNull, Len.TGA_CPT_RSH_MOR_NULL);
    }

    /**Original name: TGA-CPT-RSH-MOR-NULL<br>*/
    public String getTgaCptRshMorNull() {
        return readString(Pos.TGA_CPT_RSH_MOR_NULL, Len.TGA_CPT_RSH_MOR_NULL);
    }

    public String getTgaCptRshMorNullFormatted() {
        return Functions.padBlanks(getTgaCptRshMorNull(), Len.TGA_CPT_RSH_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_CPT_RSH_MOR = 1;
        public static final int TGA_CPT_RSH_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_CPT_RSH_MOR = 8;
        public static final int TGA_CPT_RSH_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_CPT_RSH_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_CPT_RSH_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
