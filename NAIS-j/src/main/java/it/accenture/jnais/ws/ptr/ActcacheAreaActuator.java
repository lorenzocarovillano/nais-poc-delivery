package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: ACTCACHE-AREA-ACTUATOR<br>
 * Variable: ACTCACHE-AREA-ACTUATOR from copybook IJCC0050<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class ActcacheAreaActuator extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ACTCACHE030_ELE_TAB_MAXOCCURS = 10;
    public static final int ACTCACHE040_ELE_TAB_MAXOCCURS = 40;
    public static final int ACTCACHE050_ELE_TAB_MAXOCCURS = 5;
    public static final int ACTCACHE060_ELE_TAB_MAXOCCURS = 10;
    public static final int ACTCACHE070_ELE_TAB_MAXOCCURS = 40;
    public static final int ACTCACHE100_ELE_TAB_MAXOCCURS = 10;
    public static final int ACTCACHE110_ELE_TAB_MAXOCCURS = 10;
    public static final int ACTCACHE130_ELE_TAB_MAXOCCURS = 10;
    public static final int ACTCACHE160_ELE_TAB_MAXOCCURS = 10;
    public static final int ACTCACHE290_ELE_TAB_MAXOCCURS = 50;
    public static final int ACTCACHE310_ELE_TAB_MAXOCCURS = 10;
    public static final int ACTCACHE320_ELE_TAB_MAXOCCURS = 10;
    public static final int ACTCACHE410_ELE_TAB_MAXOCCURS = 10;
    public static final int ACTCACHE450_ELE_TAB_MAXOCCURS = 10;
    public static final int ACTCACHE511_ELE_TAB_MAXOCCURS = 5;
    public static final int ACTCACHE640_ELE_TAB_MAXOCCURS = 20;
    public static final int ACTCACHE600_ELE_TAB_MAXOCCURS = 5;
    public static final int ACTCACHE650_ELE_TAB_MAXOCCURS = 20;
    public static final int ACTCACHE660_ELE_TAB_MAXOCCURS = 30;
    public static final int ACTCACHE680_ELE_TAB_MAXOCCURS = 15;
    public static final int ACTCACHE690_ELE_TAB_MAXOCCURS = 100;
    public static final int ACTCACHE700_ELE_TAB_MAXOCCURS = 10;
    public static final int ACTCACHE740_ELE_TAB_MAXOCCURS = 10;
    public static final char SERVIZIO_GESTITO_SI = 'S';
    public static final char SERVIZIO_GESTITO_NO = 'N';
    public static final char INPUT_INSERITO_NO = 'N';
    public static final char INPUT_INSERITO_SI = 'S';

    //==== CONSTRUCTORS ====
    public ActcacheAreaActuator() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ACTCACHE_AREA_ACTUATOR;
    }

    public void setServizioGestito(char servizioGestito) {
        writeChar(Pos.SERVIZIO_GESTITO, servizioGestito);
    }

    /**Original name: ACTCACHE-SERVIZIO-GESTITO<br>*/
    public char getServizioGestito() {
        return readChar(Pos.SERVIZIO_GESTITO);
    }

    public void setInputInserito(char inputInserito) {
        writeChar(Pos.INPUT_INSERITO, inputInserito);
    }

    /**Original name: ACTCACHE-INPUT-INSERITO<br>*/
    public char getInputInserito() {
        return readChar(Pos.INPUT_INSERITO);
    }

    public void setIndice(int indice) {
        writeIntAsPacked(Pos.INDICE, indice, Len.Int.INDICE);
    }

    /**Original name: ACTCACHE-INDICE<br>*/
    public int getIndice() {
        return readPackedAsInt(Pos.INDICE, Len.Int.INDICE);
    }

    public void setActcache030NumEleServizio(int actcache030NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE030_NUM_ELE_SERVIZIO, actcache030NumEleServizio, Len.Int.ACTCACHE030_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-030-NUM-ELE-SERVIZIO<br>*/
    public int getActcache030NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE030_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE030_NUM_ELE_SERVIZIO);
    }

    public void setActcache040NumEleServizio(int actcache040NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE040_NUM_ELE_SERVIZIO, actcache040NumEleServizio, Len.Int.ACTCACHE040_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-040-NUM-ELE-SERVIZIO<br>*/
    public int getActcache040NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE040_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE040_NUM_ELE_SERVIZIO);
    }

    public void setActcache050NumEleServizio(int actcache050NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE050_NUM_ELE_SERVIZIO, actcache050NumEleServizio, Len.Int.ACTCACHE050_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-050-NUM-ELE-SERVIZIO<br>*/
    public int getActcache050NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE050_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE050_NUM_ELE_SERVIZIO);
    }

    public void setActcache060NumEleServizio(int actcache060NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE060_NUM_ELE_SERVIZIO, actcache060NumEleServizio, Len.Int.ACTCACHE060_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-060-NUM-ELE-SERVIZIO<br>*/
    public int getActcache060NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE060_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE060_NUM_ELE_SERVIZIO);
    }

    public void setActcache070NumEleServizio(int actcache070NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE070_NUM_ELE_SERVIZIO, actcache070NumEleServizio, Len.Int.ACTCACHE070_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-070-NUM-ELE-SERVIZIO<br>*/
    public int getActcache070NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE070_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE070_NUM_ELE_SERVIZIO);
    }

    public void setActcache080NumEleServizio(int actcache080NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE080_NUM_ELE_SERVIZIO, actcache080NumEleServizio, Len.Int.ACTCACHE080_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-080-NUM-ELE-SERVIZIO<br>*/
    public int getActcache080NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE080_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE080_NUM_ELE_SERVIZIO);
    }

    public void setActcache090NumEleServizio(int actcache090NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE090_NUM_ELE_SERVIZIO, actcache090NumEleServizio, Len.Int.ACTCACHE090_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-090-NUM-ELE-SERVIZIO<br>*/
    public int getActcache090NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE090_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE090_NUM_ELE_SERVIZIO);
    }

    public void setActcache100NumEleServizio(int actcache100NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE100_NUM_ELE_SERVIZIO, actcache100NumEleServizio, Len.Int.ACTCACHE100_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-100-NUM-ELE-SERVIZIO<br>*/
    public int getActcache100NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE100_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE100_NUM_ELE_SERVIZIO);
    }

    public void setActcache110NumEleServizio(int actcache110NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE110_NUM_ELE_SERVIZIO, actcache110NumEleServizio, Len.Int.ACTCACHE110_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-110-NUM-ELE-SERVIZIO<br>*/
    public int getActcache110NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE110_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE110_NUM_ELE_SERVIZIO);
    }

    public void setActcache130NumEleServizio(int actcache130NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE130_NUM_ELE_SERVIZIO, actcache130NumEleServizio, Len.Int.ACTCACHE130_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-130-NUM-ELE-SERVIZIO<br>*/
    public int getActcache130NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE130_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE130_NUM_ELE_SERVIZIO);
    }

    public void setActcache160NumEleServizio(int actcache160NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE160_NUM_ELE_SERVIZIO, actcache160NumEleServizio, Len.Int.ACTCACHE160_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-160-NUM-ELE-SERVIZIO<br>*/
    public int getActcache160NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE160_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE160_NUM_ELE_SERVIZIO);
    }

    public void setActcache290NumEleServizio(int actcache290NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE290_NUM_ELE_SERVIZIO, actcache290NumEleServizio, Len.Int.ACTCACHE290_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-290-NUM-ELE-SERVIZIO<br>*/
    public int getActcache290NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE290_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE290_NUM_ELE_SERVIZIO);
    }

    public void setActcache310NumEleServizio(int actcache310NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE310_NUM_ELE_SERVIZIO, actcache310NumEleServizio, Len.Int.ACTCACHE310_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-310-NUM-ELE-SERVIZIO<br>*/
    public int getActcache310NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE310_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE310_NUM_ELE_SERVIZIO);
    }

    public void setActcache320NumEleServizio(int actcache320NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE320_NUM_ELE_SERVIZIO, actcache320NumEleServizio, Len.Int.ACTCACHE320_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-320-NUM-ELE-SERVIZIO<br>*/
    public int getActcache320NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE320_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE320_NUM_ELE_SERVIZIO);
    }

    public void setActcache410NumEleServizio(int actcache410NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE410_NUM_ELE_SERVIZIO, actcache410NumEleServizio, Len.Int.ACTCACHE410_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-410-NUM-ELE-SERVIZIO<br>*/
    public int getActcache410NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE410_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE410_NUM_ELE_SERVIZIO);
    }

    public void setActcache450NumEleServizio(int actcache450NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE450_NUM_ELE_SERVIZIO, actcache450NumEleServizio, Len.Int.ACTCACHE450_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-450-NUM-ELE-SERVIZIO<br>*/
    public int getActcache450NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE450_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE450_NUM_ELE_SERVIZIO);
    }

    public void setActcache511NumEleServizio(int actcache511NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE511_NUM_ELE_SERVIZIO, actcache511NumEleServizio, Len.Int.ACTCACHE511_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-511-NUM-ELE-SERVIZIO<br>*/
    public int getActcache511NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE511_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE511_NUM_ELE_SERVIZIO);
    }

    public void setActcache600NumEleServizio(int actcache600NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE600_NUM_ELE_SERVIZIO, actcache600NumEleServizio, Len.Int.ACTCACHE600_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-600-NUM-ELE-SERVIZIO<br>*/
    public int getActcache600NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE600_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE600_NUM_ELE_SERVIZIO);
    }

    public void setActcache640NumEleServizio(int actcache640NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE640_NUM_ELE_SERVIZIO, actcache640NumEleServizio, Len.Int.ACTCACHE640_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-640-NUM-ELE-SERVIZIO<br>*/
    public int getActcache640NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE640_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE640_NUM_ELE_SERVIZIO);
    }

    public void setActcache650NumEleServizio(int actcache650NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE650_NUM_ELE_SERVIZIO, actcache650NumEleServizio, Len.Int.ACTCACHE650_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-650-NUM-ELE-SERVIZIO<br>*/
    public int getActcache650NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE650_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE650_NUM_ELE_SERVIZIO);
    }

    public void setActcache660NumEleServizio(int actcache660NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE660_NUM_ELE_SERVIZIO, actcache660NumEleServizio, Len.Int.ACTCACHE660_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-660-NUM-ELE-SERVIZIO<br>*/
    public int getActcache660NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE660_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE660_NUM_ELE_SERVIZIO);
    }

    public void setActcache680NumEleServizio(int actcache680NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE680_NUM_ELE_SERVIZIO, actcache680NumEleServizio, Len.Int.ACTCACHE680_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-680-NUM-ELE-SERVIZIO<br>*/
    public int getActcache680NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE680_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE680_NUM_ELE_SERVIZIO);
    }

    public void setActcache690NumEleServizio(int actcache690NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE690_NUM_ELE_SERVIZIO, actcache690NumEleServizio, Len.Int.ACTCACHE690_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-690-NUM-ELE-SERVIZIO<br>*/
    public int getActcache690NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE690_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE690_NUM_ELE_SERVIZIO);
    }

    public void setActcache700NumEleServizio(int actcache700NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE700_NUM_ELE_SERVIZIO, actcache700NumEleServizio, Len.Int.ACTCACHE700_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-700-NUM-ELE-SERVIZIO<br>*/
    public int getActcache700NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE700_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE700_NUM_ELE_SERVIZIO);
    }

    public void setActcache740NumEleServizio(int actcache740NumEleServizio) {
        writeIntAsPacked(Pos.ACTCACHE740_NUM_ELE_SERVIZIO, actcache740NumEleServizio, Len.Int.ACTCACHE740_NUM_ELE_SERVIZIO);
    }

    /**Original name: ACTCACHE-740-NUM-ELE-SERVIZIO<br>*/
    public int getActcache740NumEleServizio() {
        return readPackedAsInt(Pos.ACTCACHE740_NUM_ELE_SERVIZIO, Len.Int.ACTCACHE740_NUM_ELE_SERVIZIO);
    }

    public void setTotEleCaricati(int totEleCaricati) {
        writeIntAsPacked(Pos.TOT_ELE_CARICATI, totEleCaricati, Len.Int.TOT_ELE_CARICATI);
    }

    /**Original name: ACTCACHE-TOT-ELE-CARICATI<br>*/
    public int getTotEleCaricati() {
        return readPackedAsInt(Pos.TOT_ELE_CARICATI, Len.Int.TOT_ELE_CARICATI);
    }

    public String getTotEleCaricatiFormatted() {
        return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.PACKED)).format(getTotEleCaricati()).toString();
    }

    public String getTotEleCaricatiAsString() {
        return getTotEleCaricatiFormatted();
    }

    public void setContaCache(int contaCache) {
        writeIntAsPacked(Pos.CONTA_CACHE, contaCache, Len.Int.CONTA_CACHE);
    }

    /**Original name: ACTCACHE-CONTA-CACHE<br>*/
    public int getContaCache() {
        return readPackedAsInt(Pos.CONTA_CACHE, Len.Int.CONTA_CACHE);
    }

    public String getContaCacheFormatted() {
        return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.PACKED)).format(getContaCache()).toString();
    }

    public String getContaCacheAsString() {
        return getContaCacheFormatted();
    }

    public void setActcache030DatiInput(int actcache030DatiInputIdx, String actcache030DatiInput) {
        int position = Pos.actcache030DatiInput(actcache030DatiInputIdx - 1);
        writeString(position, actcache030DatiInput, Len.ACTCACHE030_DATI_INPUT);
    }

    /**Original name: ACTCACHE-030-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-030-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-030-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache030DatiInput(int actcache030DatiInputIdx) {
        int position = Pos.actcache030DatiInput(actcache030DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE030_DATI_INPUT);
    }

    public void setActcache030DatiOutput(int actcache030DatiOutputIdx, String actcache030DatiOutput) {
        int position = Pos.actcache030DatiOutput(actcache030DatiOutputIdx - 1);
        writeString(position, actcache030DatiOutput, Len.ACTCACHE030_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-030-DATI-OUTPUT<br>
	 * <pre>            15 ACTCACHE-030-DATI-OUTPUT           PIC X(28003).</pre>*/
    public String getActcache030DatiOutput(int actcache030DatiOutputIdx) {
        int position = Pos.actcache030DatiOutput(actcache030DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE030_DATI_OUTPUT);
    }

    public void setActcache040DatiInput(int actcache040DatiInputIdx, String actcache040DatiInput) {
        int position = Pos.actcache040DatiInput(actcache040DatiInputIdx - 1);
        writeString(position, actcache040DatiInput, Len.ACTCACHE040_DATI_INPUT);
    }

    /**Original name: ACTCACHE-040-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-040-ELE-TAB OCCURS      10 TIMES.
	 *          10 ACTCACHE-040-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-040-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache040DatiInput(int actcache040DatiInputIdx) {
        int position = Pos.actcache040DatiInput(actcache040DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE040_DATI_INPUT);
    }

    public void setActcache040DatiOutput(int actcache040DatiOutputIdx, String actcache040DatiOutput) {
        int position = Pos.actcache040DatiOutput(actcache040DatiOutputIdx - 1);
        writeString(position, actcache040DatiOutput, Len.ACTCACHE040_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-040-DATI-OUTPUT<br>
	 * <pre>            15 ACTCACHE-040-DATI-OUTPUT           PIC X(20368).</pre>*/
    public String getActcache040DatiOutput(int actcache040DatiOutputIdx) {
        int position = Pos.actcache040DatiOutput(actcache040DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE040_DATI_OUTPUT);
    }

    public void setActcache050DatiInput(int actcache050DatiInputIdx, String actcache050DatiInput) {
        int position = Pos.actcache050DatiInput(actcache050DatiInputIdx - 1);
        writeString(position, actcache050DatiInput, Len.ACTCACHE050_DATI_INPUT);
    }

    /**Original name: ACTCACHE-050-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-050-ELE-TAB OCCURS 0 TO 05 TIMES
	 *             DEPENDING ON ACTCACHE-050-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache050DatiInput(int actcache050DatiInputIdx) {
        int position = Pos.actcache050DatiInput(actcache050DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE050_DATI_INPUT);
    }

    public void setActcache050DatiOutput(int actcache050DatiOutputIdx, String actcache050DatiOutput) {
        int position = Pos.actcache050DatiOutput(actcache050DatiOutputIdx - 1);
        writeString(position, actcache050DatiOutput, Len.ACTCACHE050_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-050-DATI-OUTPUT<br>*/
    public String getActcache050DatiOutput(int actcache050DatiOutputIdx) {
        int position = Pos.actcache050DatiOutput(actcache050DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE050_DATI_OUTPUT);
    }

    public void setActcache060DatiInput(int actcache060DatiInputIdx, String actcache060DatiInput) {
        int position = Pos.actcache060DatiInput(actcache060DatiInputIdx - 1);
        writeString(position, actcache060DatiInput, Len.ACTCACHE060_DATI_INPUT);
    }

    /**Original name: ACTCACHE-060-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-060-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-060-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache060DatiInput(int actcache060DatiInputIdx) {
        int position = Pos.actcache060DatiInput(actcache060DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE060_DATI_INPUT);
    }

    public void setActcache060DatiOutput(int actcache060DatiOutputIdx, String actcache060DatiOutput) {
        int position = Pos.actcache060DatiOutput(actcache060DatiOutputIdx - 1);
        writeString(position, actcache060DatiOutput, Len.ACTCACHE060_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-060-DATI-OUTPUT<br>*/
    public String getActcache060DatiOutput(int actcache060DatiOutputIdx) {
        int position = Pos.actcache060DatiOutput(actcache060DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE060_DATI_OUTPUT);
    }

    public void setActcache070DatiInput(int actcache070DatiInputIdx, String actcache070DatiInput) {
        int position = Pos.actcache070DatiInput(actcache070DatiInputIdx - 1);
        writeString(position, actcache070DatiInput, Len.ACTCACHE070_DATI_INPUT);
    }

    /**Original name: ACTCACHE-070-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-070-ELE-TAB OCCURS      20 TIMES.
	 *          10 ACTCACHE-070-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-070-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache070DatiInput(int actcache070DatiInputIdx) {
        int position = Pos.actcache070DatiInput(actcache070DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE070_DATI_INPUT);
    }

    public void setActcache070DatiOutput(int actcache070DatiOutputIdx, String actcache070DatiOutput) {
        int position = Pos.actcache070DatiOutput(actcache070DatiOutputIdx - 1);
        writeString(position, actcache070DatiOutput, Len.ACTCACHE070_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-070-DATI-OUTPUT<br>*/
    public String getActcache070DatiOutput(int actcache070DatiOutputIdx) {
        int position = Pos.actcache070DatiOutput(actcache070DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE070_DATI_OUTPUT);
    }

    public void setActcache100DatiInput(int actcache100DatiInputIdx, String actcache100DatiInput) {
        int position = Pos.actcache100DatiInput(actcache100DatiInputIdx - 1);
        writeString(position, actcache100DatiInput, Len.ACTCACHE100_DATI_INPUT);
    }

    /**Original name: ACTCACHE-100-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-100-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-100-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache100DatiInput(int actcache100DatiInputIdx) {
        int position = Pos.actcache100DatiInput(actcache100DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE100_DATI_INPUT);
    }

    public void setActcache100DatiOutput(int actcache100DatiOutputIdx, String actcache100DatiOutput) {
        int position = Pos.actcache100DatiOutput(actcache100DatiOutputIdx - 1);
        writeString(position, actcache100DatiOutput, Len.ACTCACHE100_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-100-DATI-OUTPUT<br>*/
    public String getActcache100DatiOutput(int actcache100DatiOutputIdx) {
        int position = Pos.actcache100DatiOutput(actcache100DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE100_DATI_OUTPUT);
    }

    public void setActcache110DatiInput(int actcache110DatiInputIdx, String actcache110DatiInput) {
        int position = Pos.actcache110DatiInput(actcache110DatiInputIdx - 1);
        writeString(position, actcache110DatiInput, Len.ACTCACHE110_DATI_INPUT);
    }

    /**Original name: ACTCACHE-110-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-110-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-110-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache110DatiInput(int actcache110DatiInputIdx) {
        int position = Pos.actcache110DatiInput(actcache110DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE110_DATI_INPUT);
    }

    public void setActcache110DatiOutput(int actcache110DatiOutputIdx, String actcache110DatiOutput) {
        int position = Pos.actcache110DatiOutput(actcache110DatiOutputIdx - 1);
        writeString(position, actcache110DatiOutput, Len.ACTCACHE110_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-110-DATI-OUTPUT<br>*/
    public String getActcache110DatiOutput(int actcache110DatiOutputIdx) {
        int position = Pos.actcache110DatiOutput(actcache110DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE110_DATI_OUTPUT);
    }

    public void setActcache130DatiInput(int actcache130DatiInputIdx, String actcache130DatiInput) {
        int position = Pos.actcache130DatiInput(actcache130DatiInputIdx - 1);
        writeString(position, actcache130DatiInput, Len.ACTCACHE130_DATI_INPUT);
    }

    /**Original name: ACTCACHE-130-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-130-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-130-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache130DatiInput(int actcache130DatiInputIdx) {
        int position = Pos.actcache130DatiInput(actcache130DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE130_DATI_INPUT);
    }

    public void setActcache130DatiOutput(int actcache130DatiOutputIdx, String actcache130DatiOutput) {
        int position = Pos.actcache130DatiOutput(actcache130DatiOutputIdx - 1);
        writeString(position, actcache130DatiOutput, Len.ACTCACHE130_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-130-DATI-OUTPUT<br>*/
    public String getActcache130DatiOutput(int actcache130DatiOutputIdx) {
        int position = Pos.actcache130DatiOutput(actcache130DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE130_DATI_OUTPUT);
    }

    public void setActcache160DatiInput(int actcache160DatiInputIdx, String actcache160DatiInput) {
        int position = Pos.actcache160DatiInput(actcache160DatiInputIdx - 1);
        writeString(position, actcache160DatiInput, Len.ACTCACHE160_DATI_INPUT);
    }

    /**Original name: ACTCACHE-160-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-160-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-160-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache160DatiInput(int actcache160DatiInputIdx) {
        int position = Pos.actcache160DatiInput(actcache160DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE160_DATI_INPUT);
    }

    public void setActcache160DatiOutput(int actcache160DatiOutputIdx, String actcache160DatiOutput) {
        int position = Pos.actcache160DatiOutput(actcache160DatiOutputIdx - 1);
        writeString(position, actcache160DatiOutput, Len.ACTCACHE160_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-160-DATI-OUTPUT<br>*/
    public String getActcache160DatiOutput(int actcache160DatiOutputIdx) {
        int position = Pos.actcache160DatiOutput(actcache160DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE160_DATI_OUTPUT);
    }

    public void setActcache290DatiInput(int actcache290DatiInputIdx, String actcache290DatiInput) {
        int position = Pos.actcache290DatiInput(actcache290DatiInputIdx - 1);
        writeString(position, actcache290DatiInput, Len.ACTCACHE290_DATI_INPUT);
    }

    /**Original name: ACTCACHE-290-DATI-INPUT<br>*/
    public String getActcache290DatiInput(int actcache290DatiInputIdx) {
        int position = Pos.actcache290DatiInput(actcache290DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE290_DATI_INPUT);
    }

    public void setActcache290DatiOutput(int actcache290DatiOutputIdx, String actcache290DatiOutput) {
        int position = Pos.actcache290DatiOutput(actcache290DatiOutputIdx - 1);
        writeString(position, actcache290DatiOutput, Len.ACTCACHE290_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-290-DATI-OUTPUT<br>*/
    public String getActcache290DatiOutput(int actcache290DatiOutputIdx) {
        int position = Pos.actcache290DatiOutput(actcache290DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE290_DATI_OUTPUT);
    }

    public void setActcache310DatiInput(int actcache310DatiInputIdx, String actcache310DatiInput) {
        int position = Pos.actcache310DatiInput(actcache310DatiInputIdx - 1);
        writeString(position, actcache310DatiInput, Len.ACTCACHE310_DATI_INPUT);
    }

    /**Original name: ACTCACHE-310-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-310-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-310-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache310DatiInput(int actcache310DatiInputIdx) {
        int position = Pos.actcache310DatiInput(actcache310DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE310_DATI_INPUT);
    }

    public void setActcache310DatiOutput(int actcache310DatiOutputIdx, String actcache310DatiOutput) {
        int position = Pos.actcache310DatiOutput(actcache310DatiOutputIdx - 1);
        writeString(position, actcache310DatiOutput, Len.ACTCACHE310_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-310-DATI-OUTPUT<br>
	 * <pre>            15 ACTCACHE-310-DATI-OUTPUT           PIC X(80004).</pre>*/
    public String getActcache310DatiOutput(int actcache310DatiOutputIdx) {
        int position = Pos.actcache310DatiOutput(actcache310DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE310_DATI_OUTPUT);
    }

    public void setActcache320DatiInput(int actcache320DatiInputIdx, String actcache320DatiInput) {
        int position = Pos.actcache320DatiInput(actcache320DatiInputIdx - 1);
        writeString(position, actcache320DatiInput, Len.ACTCACHE320_DATI_INPUT);
    }

    /**Original name: ACTCACHE-320-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-320-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-320-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache320DatiInput(int actcache320DatiInputIdx) {
        int position = Pos.actcache320DatiInput(actcache320DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE320_DATI_INPUT);
    }

    public void setActcache320DatiOutput(int actcache320DatiOutputIdx, String actcache320DatiOutput) {
        int position = Pos.actcache320DatiOutput(actcache320DatiOutputIdx - 1);
        writeString(position, actcache320DatiOutput, Len.ACTCACHE320_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-320-DATI-OUTPUT<br>
	 * <pre>            15 ACTCACHE-320-DATI-OUTPUT           PIC X(2050).</pre>*/
    public String getActcache320DatiOutput(int actcache320DatiOutputIdx) {
        int position = Pos.actcache320DatiOutput(actcache320DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE320_DATI_OUTPUT);
    }

    public void setActcache410DatiInput(int actcache410DatiInputIdx, String actcache410DatiInput) {
        int position = Pos.actcache410DatiInput(actcache410DatiInputIdx - 1);
        writeString(position, actcache410DatiInput, Len.ACTCACHE410_DATI_INPUT);
    }

    /**Original name: ACTCACHE-410-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-410-ELE-TAB OCCURS 0 TO 5 TIMES
	 *             DEPENDING ON ACTCACHE-410-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache410DatiInput(int actcache410DatiInputIdx) {
        int position = Pos.actcache410DatiInput(actcache410DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE410_DATI_INPUT);
    }

    public void setActcache410DatiOutput(int actcache410DatiOutputIdx, String actcache410DatiOutput) {
        int position = Pos.actcache410DatiOutput(actcache410DatiOutputIdx - 1);
        writeString(position, actcache410DatiOutput, Len.ACTCACHE410_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-410-DATI-OUTPUT<br>*/
    public String getActcache410DatiOutput(int actcache410DatiOutputIdx) {
        int position = Pos.actcache410DatiOutput(actcache410DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE410_DATI_OUTPUT);
    }

    public void setActcache450DatiInput(int actcache450DatiInputIdx, String actcache450DatiInput) {
        int position = Pos.actcache450DatiInput(actcache450DatiInputIdx - 1);
        writeString(position, actcache450DatiInput, Len.ACTCACHE450_DATI_INPUT);
    }

    /**Original name: ACTCACHE-450-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-450-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-450-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache450DatiInput(int actcache450DatiInputIdx) {
        int position = Pos.actcache450DatiInput(actcache450DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE450_DATI_INPUT);
    }

    public void setActcache450DatiOutput(int actcache450DatiOutputIdx, String actcache450DatiOutput) {
        int position = Pos.actcache450DatiOutput(actcache450DatiOutputIdx - 1);
        writeString(position, actcache450DatiOutput, Len.ACTCACHE450_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-450-DATI-OUTPUT<br>
	 * <pre>            15 ACTCACHE-450-DATI-OUTPUT           PIC X(47903).</pre>*/
    public String getActcache450DatiOutput(int actcache450DatiOutputIdx) {
        int position = Pos.actcache450DatiOutput(actcache450DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE450_DATI_OUTPUT);
    }

    public void setActcache511DatiInput(int actcache511DatiInputIdx, String actcache511DatiInput) {
        int position = Pos.actcache511DatiInput(actcache511DatiInputIdx - 1);
        writeString(position, actcache511DatiInput, Len.ACTCACHE511_DATI_INPUT);
    }

    /**Original name: ACTCACHE-511-DATI-INPUT<br>*/
    public String getActcache511DatiInput(int actcache511DatiInputIdx) {
        int position = Pos.actcache511DatiInput(actcache511DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE511_DATI_INPUT);
    }

    public void setActcache511DatiOutput(int actcache511DatiOutputIdx, String actcache511DatiOutput) {
        int position = Pos.actcache511DatiOutput(actcache511DatiOutputIdx - 1);
        writeString(position, actcache511DatiOutput, Len.ACTCACHE511_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-511-DATI-OUTPUT<br>*/
    public String getActcache511DatiOutput(int actcache511DatiOutputIdx) {
        int position = Pos.actcache511DatiOutput(actcache511DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE511_DATI_OUTPUT);
    }

    public void setActcache640DatiInput(int actcache640DatiInputIdx, String actcache640DatiInput) {
        int position = Pos.actcache640DatiInput(actcache640DatiInputIdx - 1);
        writeString(position, actcache640DatiInput, Len.ACTCACHE640_DATI_INPUT);
    }

    /**Original name: ACTCACHE-640-DATI-INPUT<br>*/
    public String getActcache640DatiInput(int actcache640DatiInputIdx) {
        int position = Pos.actcache640DatiInput(actcache640DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE640_DATI_INPUT);
    }

    public void setActcache640DatiOutput(int actcache640DatiOutputIdx, String actcache640DatiOutput) {
        int position = Pos.actcache640DatiOutput(actcache640DatiOutputIdx - 1);
        writeString(position, actcache640DatiOutput, Len.ACTCACHE640_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-640-DATI-OUTPUT<br>*/
    public String getActcache640DatiOutput(int actcache640DatiOutputIdx) {
        int position = Pos.actcache640DatiOutput(actcache640DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE640_DATI_OUTPUT);
    }

    public void setActcache600DatiInput(int actcache600DatiInputIdx, String actcache600DatiInput) {
        int position = Pos.actcache600DatiInput(actcache600DatiInputIdx - 1);
        writeString(position, actcache600DatiInput, Len.ACTCACHE600_DATI_INPUT);
    }

    /**Original name: ACTCACHE-600-DATI-INPUT<br>*/
    public String getActcache600DatiInput(int actcache600DatiInputIdx) {
        int position = Pos.actcache600DatiInput(actcache600DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE600_DATI_INPUT);
    }

    public void setActcache600DatiOutput(int actcache600DatiOutputIdx, String actcache600DatiOutput) {
        int position = Pos.actcache600DatiOutput(actcache600DatiOutputIdx - 1);
        writeString(position, actcache600DatiOutput, Len.ACTCACHE600_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-600-DATI-OUTPUT<br>*/
    public String getActcache600DatiOutput(int actcache600DatiOutputIdx) {
        int position = Pos.actcache600DatiOutput(actcache600DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE600_DATI_OUTPUT);
    }

    public void setActcache650DatiInput(int actcache650DatiInputIdx, String actcache650DatiInput) {
        int position = Pos.actcache650DatiInput(actcache650DatiInputIdx - 1);
        writeString(position, actcache650DatiInput, Len.ACTCACHE650_DATI_INPUT);
    }

    /**Original name: ACTCACHE-650-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-650-ELE-TAB OCCURS       6  TIMES.</pre>*/
    public String getActcache650DatiInput(int actcache650DatiInputIdx) {
        int position = Pos.actcache650DatiInput(actcache650DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE650_DATI_INPUT);
    }

    public void setActcache650DatiOutput(int actcache650DatiOutputIdx, String actcache650DatiOutput) {
        int position = Pos.actcache650DatiOutput(actcache650DatiOutputIdx - 1);
        writeString(position, actcache650DatiOutput, Len.ACTCACHE650_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-650-DATI-OUTPUT<br>*/
    public String getActcache650DatiOutput(int actcache650DatiOutputIdx) {
        int position = Pos.actcache650DatiOutput(actcache650DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE650_DATI_OUTPUT);
    }

    public void setActcache660DatiInput(int actcache660DatiInputIdx, String actcache660DatiInput) {
        int position = Pos.actcache660DatiInput(actcache660DatiInputIdx - 1);
        writeString(position, actcache660DatiInput, Len.ACTCACHE660_DATI_INPUT);
    }

    /**Original name: ACTCACHE-660-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-660-ELE-TAB OCCURS 0 TO 10  TIMES
	 *             DEPENDING ON ACTCACHE-660-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache660DatiInput(int actcache660DatiInputIdx) {
        int position = Pos.actcache660DatiInput(actcache660DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE660_DATI_INPUT);
    }

    public void setActcache660DatiOutput(int actcache660DatiOutputIdx, String actcache660DatiOutput) {
        int position = Pos.actcache660DatiOutput(actcache660DatiOutputIdx - 1);
        writeString(position, actcache660DatiOutput, Len.ACTCACHE660_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-660-DATI-OUTPUT<br>*/
    public String getActcache660DatiOutput(int actcache660DatiOutputIdx) {
        int position = Pos.actcache660DatiOutput(actcache660DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE660_DATI_OUTPUT);
    }

    public void setActcache680DatiInput(int actcache680DatiInputIdx, String actcache680DatiInput) {
        int position = Pos.actcache680DatiInput(actcache680DatiInputIdx - 1);
        writeString(position, actcache680DatiInput, Len.ACTCACHE680_DATI_INPUT);
    }

    /**Original name: ACTCACHE-680-DATI-INPUT<br>*/
    public String getActcache680DatiInput(int actcache680DatiInputIdx) {
        int position = Pos.actcache680DatiInput(actcache680DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE680_DATI_INPUT);
    }

    public void setActcache680DatiOutput(int actcache680DatiOutputIdx, String actcache680DatiOutput) {
        int position = Pos.actcache680DatiOutput(actcache680DatiOutputIdx - 1);
        writeString(position, actcache680DatiOutput, Len.ACTCACHE680_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-680-DATI-OUTPUT<br>*/
    public String getActcache680DatiOutput(int actcache680DatiOutputIdx) {
        int position = Pos.actcache680DatiOutput(actcache680DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE680_DATI_OUTPUT);
    }

    public void setActcache690DatiInput(int actcache690DatiInputIdx, String actcache690DatiInput) {
        int position = Pos.actcache690DatiInput(actcache690DatiInputIdx - 1);
        writeString(position, actcache690DatiInput, Len.ACTCACHE690_DATI_INPUT);
    }

    /**Original name: ACTCACHE-690-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-690-ELE-TAB OCCURS 0 TO 100 TIMES
	 *             DEPENDING ON ACTCACHE-690-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache690DatiInput(int actcache690DatiInputIdx) {
        int position = Pos.actcache690DatiInput(actcache690DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE690_DATI_INPUT);
    }

    public void setActcache690DatiOutput(int actcache690DatiOutputIdx, String actcache690DatiOutput) {
        int position = Pos.actcache690DatiOutput(actcache690DatiOutputIdx - 1);
        writeString(position, actcache690DatiOutput, Len.ACTCACHE690_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-690-DATI-OUTPUT<br>*/
    public String getActcache690DatiOutput(int actcache690DatiOutputIdx) {
        int position = Pos.actcache690DatiOutput(actcache690DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE690_DATI_OUTPUT);
    }

    public void setActcache700DatiInput(int actcache700DatiInputIdx, String actcache700DatiInput) {
        int position = Pos.actcache700DatiInput(actcache700DatiInputIdx - 1);
        writeString(position, actcache700DatiInput, Len.ACTCACHE700_DATI_INPUT);
    }

    /**Original name: ACTCACHE-700-DATI-INPUT<br>
	 * <pre>         10 ACTCACHE-700-ELE-TAB OCCURS 0 TO 10 TIMES
	 *             DEPENDING ON ACTCACHE-700-NUM-ELE-SERVIZIO.</pre>*/
    public String getActcache700DatiInput(int actcache700DatiInputIdx) {
        int position = Pos.actcache700DatiInput(actcache700DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE700_DATI_INPUT);
    }

    public void setActcache700DatiOutput(int actcache700DatiOutputIdx, String actcache700DatiOutput) {
        int position = Pos.actcache700DatiOutput(actcache700DatiOutputIdx - 1);
        writeString(position, actcache700DatiOutput, Len.ACTCACHE700_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-700-DATI-OUTPUT<br>*/
    public String getActcache700DatiOutput(int actcache700DatiOutputIdx) {
        int position = Pos.actcache700DatiOutput(actcache700DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE700_DATI_OUTPUT);
    }

    public void setActcache740DatiInput(int actcache740DatiInputIdx, String actcache740DatiInput) {
        int position = Pos.actcache740DatiInput(actcache740DatiInputIdx - 1);
        writeString(position, actcache740DatiInput, Len.ACTCACHE740_DATI_INPUT);
    }

    /**Original name: ACTCACHE-740-DATI-INPUT<br>*/
    public String getActcache740DatiInput(int actcache740DatiInputIdx) {
        int position = Pos.actcache740DatiInput(actcache740DatiInputIdx - 1);
        return readString(position, Len.ACTCACHE740_DATI_INPUT);
    }

    public void setActcache740DatiOutput(int actcache740DatiOutputIdx, String actcache740DatiOutput) {
        int position = Pos.actcache740DatiOutput(actcache740DatiOutputIdx - 1);
        writeString(position, actcache740DatiOutput, Len.ACTCACHE740_DATI_OUTPUT);
    }

    /**Original name: ACTCACHE-740-DATI-OUTPUT<br>*/
    public String getActcache740DatiOutput(int actcache740DatiOutputIdx) {
        int position = Pos.actcache740DatiOutput(actcache740DatiOutputIdx - 1);
        return readString(position, Len.ACTCACHE740_DATI_OUTPUT);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ACTCACHE_AREA_ACTUATOR = 1;
        public static final int CONTATORI_CACHE = ACTCACHE_AREA_ACTUATOR;
        public static final int SERVIZIO_GESTITO = CONTATORI_CACHE;
        public static final int INPUT_INSERITO = SERVIZIO_GESTITO + Len.SERVIZIO_GESTITO;
        public static final int INDICE = INPUT_INSERITO + Len.INPUT_INSERITO;
        public static final int ACTCACHE030_NUM_ELE_SERVIZIO = INDICE + Len.INDICE;
        public static final int ACTCACHE040_NUM_ELE_SERVIZIO = ACTCACHE030_NUM_ELE_SERVIZIO + Len.ACTCACHE030_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE050_NUM_ELE_SERVIZIO = ACTCACHE040_NUM_ELE_SERVIZIO + Len.ACTCACHE040_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE060_NUM_ELE_SERVIZIO = ACTCACHE050_NUM_ELE_SERVIZIO + Len.ACTCACHE050_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE070_NUM_ELE_SERVIZIO = ACTCACHE060_NUM_ELE_SERVIZIO + Len.ACTCACHE060_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE080_NUM_ELE_SERVIZIO = ACTCACHE070_NUM_ELE_SERVIZIO + Len.ACTCACHE070_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE090_NUM_ELE_SERVIZIO = ACTCACHE080_NUM_ELE_SERVIZIO + Len.ACTCACHE080_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE100_NUM_ELE_SERVIZIO = ACTCACHE090_NUM_ELE_SERVIZIO + Len.ACTCACHE090_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE110_NUM_ELE_SERVIZIO = ACTCACHE100_NUM_ELE_SERVIZIO + Len.ACTCACHE100_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE130_NUM_ELE_SERVIZIO = ACTCACHE110_NUM_ELE_SERVIZIO + Len.ACTCACHE110_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE160_NUM_ELE_SERVIZIO = ACTCACHE130_NUM_ELE_SERVIZIO + Len.ACTCACHE130_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE290_NUM_ELE_SERVIZIO = ACTCACHE160_NUM_ELE_SERVIZIO + Len.ACTCACHE160_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE310_NUM_ELE_SERVIZIO = ACTCACHE290_NUM_ELE_SERVIZIO + Len.ACTCACHE290_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE320_NUM_ELE_SERVIZIO = ACTCACHE310_NUM_ELE_SERVIZIO + Len.ACTCACHE310_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE410_NUM_ELE_SERVIZIO = ACTCACHE320_NUM_ELE_SERVIZIO + Len.ACTCACHE320_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE450_NUM_ELE_SERVIZIO = ACTCACHE410_NUM_ELE_SERVIZIO + Len.ACTCACHE410_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE511_NUM_ELE_SERVIZIO = ACTCACHE450_NUM_ELE_SERVIZIO + Len.ACTCACHE450_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE600_NUM_ELE_SERVIZIO = ACTCACHE511_NUM_ELE_SERVIZIO + Len.ACTCACHE511_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE640_NUM_ELE_SERVIZIO = ACTCACHE600_NUM_ELE_SERVIZIO + Len.ACTCACHE600_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE650_NUM_ELE_SERVIZIO = ACTCACHE640_NUM_ELE_SERVIZIO + Len.ACTCACHE640_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE660_NUM_ELE_SERVIZIO = ACTCACHE650_NUM_ELE_SERVIZIO + Len.ACTCACHE650_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE680_NUM_ELE_SERVIZIO = ACTCACHE660_NUM_ELE_SERVIZIO + Len.ACTCACHE660_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE690_NUM_ELE_SERVIZIO = ACTCACHE680_NUM_ELE_SERVIZIO + Len.ACTCACHE680_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE700_NUM_ELE_SERVIZIO = ACTCACHE690_NUM_ELE_SERVIZIO + Len.ACTCACHE690_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE740_NUM_ELE_SERVIZIO = ACTCACHE700_NUM_ELE_SERVIZIO + Len.ACTCACHE700_NUM_ELE_SERVIZIO;
        public static final int TOT_ELE_CARICATI = ACTCACHE740_NUM_ELE_SERVIZIO + Len.ACTCACHE740_NUM_ELE_SERVIZIO;
        public static final int CONTA_CACHE = TOT_ELE_CARICATI + Len.TOT_ELE_CARICATI;
        public static final int AREA_TABELLE = CONTA_CACHE + Len.CONTA_CACHE;
        public static final int ACTCACHE030_TAB = AREA_TABELLE;
        public static final int ACTCACHE040_TAB = actcache030DatiOutput(ACTCACHE030_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE030_DATI_OUTPUT;
        public static final int ACTCACHE050_TAB = actcache040DatiOutput(ACTCACHE040_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE040_DATI_OUTPUT;
        public static final int ACTCACHE060_TAB = actcache050DatiOutput(ACTCACHE050_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE050_DATI_OUTPUT;
        public static final int ACTCACHE070_TAB = actcache060DatiOutput(ACTCACHE060_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE060_DATI_OUTPUT;
        public static final int ACTCACHE100_TAB = actcache070DatiOutput(ACTCACHE070_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE070_DATI_OUTPUT;
        public static final int ACTCACHE110_TAB = actcache100DatiOutput(ACTCACHE100_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE100_DATI_OUTPUT;
        public static final int ACTCACHE130_TAB = actcache110DatiOutput(ACTCACHE110_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE110_DATI_OUTPUT;
        public static final int ACTCACHE160_TAB = actcache130DatiOutput(ACTCACHE130_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE130_DATI_OUTPUT;
        public static final int ACTCACHE290_TAB = actcache160DatiOutput(ACTCACHE160_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE160_DATI_OUTPUT;
        public static final int ACTCACHE310_TAB = actcache290DatiOutput(ACTCACHE290_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE290_DATI_OUTPUT;
        public static final int ACTCACHE320_TAB = actcache310DatiOutput(ACTCACHE310_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE310_DATI_OUTPUT;
        public static final int ACTCACHE410_TAB = actcache320DatiOutput(ACTCACHE320_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE320_DATI_OUTPUT;
        public static final int ACTCACHE450_TAB = actcache410DatiOutput(ACTCACHE410_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE410_DATI_OUTPUT;
        public static final int ACTCACHE511_TAB = actcache450DatiOutput(ACTCACHE450_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE450_DATI_OUTPUT;
        public static final int ACTCACHE640_TAB = actcache511DatiOutput(ACTCACHE511_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE511_DATI_OUTPUT;
        public static final int ACTCACHE600_TAB = actcache640DatiOutput(ACTCACHE640_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE640_DATI_OUTPUT;
        public static final int ACTCACHE650_TAB = actcache600DatiOutput(ACTCACHE600_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE600_DATI_OUTPUT;
        public static final int ACTCACHE660_TAB = actcache650DatiOutput(ACTCACHE650_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE650_DATI_OUTPUT;
        public static final int ACTCACHE680_TAB = actcache660DatiOutput(ACTCACHE660_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE660_DATI_OUTPUT;
        public static final int ACTCACHE690_TAB = actcache680DatiOutput(ACTCACHE680_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE680_DATI_OUTPUT;
        public static final int ACTCACHE700_TAB = actcache690DatiOutput(ACTCACHE690_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE690_DATI_OUTPUT;
        public static final int ACTCACHE740_TAB = actcache700DatiOutput(ACTCACHE700_ELE_TAB_MAXOCCURS - 1) + Len.ACTCACHE700_DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int actcache030EleTab(int idx) {
            return ACTCACHE030_TAB + idx * Len.ACTCACHE030_ELE_TAB;
        }

        public static int actcache030DatiInput(int idx) {
            return actcache030EleTab(idx);
        }

        public static int actcache030DatiOutput(int idx) {
            return actcache030DatiInput(idx) + Len.ACTCACHE030_DATI_INPUT;
        }

        public static int actcache040EleTab(int idx) {
            return ACTCACHE040_TAB + idx * Len.ACTCACHE040_ELE_TAB;
        }

        public static int actcache040DatiInput(int idx) {
            return actcache040EleTab(idx);
        }

        public static int actcache040DatiOutput(int idx) {
            return actcache040DatiInput(idx) + Len.ACTCACHE040_DATI_INPUT;
        }

        public static int actcache050EleTab(int idx) {
            return ACTCACHE050_TAB + idx * Len.ACTCACHE050_ELE_TAB;
        }

        public static int actcache050DatiInput(int idx) {
            return actcache050EleTab(idx);
        }

        public static int actcache050DatiOutput(int idx) {
            return actcache050DatiInput(idx) + Len.ACTCACHE050_DATI_INPUT;
        }

        public static int actcache060EleTab(int idx) {
            return ACTCACHE060_TAB + idx * Len.ACTCACHE060_ELE_TAB;
        }

        public static int actcache060DatiInput(int idx) {
            return actcache060EleTab(idx);
        }

        public static int actcache060DatiOutput(int idx) {
            return actcache060DatiInput(idx) + Len.ACTCACHE060_DATI_INPUT;
        }

        public static int actcache070EleTab(int idx) {
            return ACTCACHE070_TAB + idx * Len.ACTCACHE070_ELE_TAB;
        }

        public static int actcache070DatiInput(int idx) {
            return actcache070EleTab(idx);
        }

        public static int actcache070DatiOutput(int idx) {
            return actcache070DatiInput(idx) + Len.ACTCACHE070_DATI_INPUT;
        }

        public static int actcache100EleTab(int idx) {
            return ACTCACHE100_TAB + idx * Len.ACTCACHE100_ELE_TAB;
        }

        public static int actcache100DatiInput(int idx) {
            return actcache100EleTab(idx);
        }

        public static int actcache100DatiOutput(int idx) {
            return actcache100DatiInput(idx) + Len.ACTCACHE100_DATI_INPUT;
        }

        public static int actcache110EleTab(int idx) {
            return ACTCACHE110_TAB + idx * Len.ACTCACHE110_ELE_TAB;
        }

        public static int actcache110DatiInput(int idx) {
            return actcache110EleTab(idx);
        }

        public static int actcache110DatiOutput(int idx) {
            return actcache110DatiInput(idx) + Len.ACTCACHE110_DATI_INPUT;
        }

        public static int actcache130EleTab(int idx) {
            return ACTCACHE130_TAB + idx * Len.ACTCACHE130_ELE_TAB;
        }

        public static int actcache130DatiInput(int idx) {
            return actcache130EleTab(idx);
        }

        public static int actcache130DatiOutput(int idx) {
            return actcache130DatiInput(idx) + Len.ACTCACHE130_DATI_INPUT;
        }

        public static int actcache160EleTab(int idx) {
            return ACTCACHE160_TAB + idx * Len.ACTCACHE160_ELE_TAB;
        }

        public static int actcache160DatiInput(int idx) {
            return actcache160EleTab(idx);
        }

        public static int actcache160DatiOutput(int idx) {
            return actcache160DatiInput(idx) + Len.ACTCACHE160_DATI_INPUT;
        }

        public static int actcache290EleTab(int idx) {
            return ACTCACHE290_TAB + idx * Len.ACTCACHE290_ELE_TAB;
        }

        public static int actcache290DatiInput(int idx) {
            return actcache290EleTab(idx);
        }

        public static int actcache290DatiOutput(int idx) {
            return actcache290DatiInput(idx) + Len.ACTCACHE290_DATI_INPUT;
        }

        public static int actcache310EleTab(int idx) {
            return ACTCACHE310_TAB + idx * Len.ACTCACHE310_ELE_TAB;
        }

        public static int actcache310DatiInput(int idx) {
            return actcache310EleTab(idx);
        }

        public static int actcache310DatiOutput(int idx) {
            return actcache310DatiInput(idx) + Len.ACTCACHE310_DATI_INPUT;
        }

        public static int actcache320EleTab(int idx) {
            return ACTCACHE320_TAB + idx * Len.ACTCACHE320_ELE_TAB;
        }

        public static int actcache320DatiInput(int idx) {
            return actcache320EleTab(idx);
        }

        public static int actcache320DatiOutput(int idx) {
            return actcache320DatiInput(idx) + Len.ACTCACHE320_DATI_INPUT;
        }

        public static int actcache410EleTab(int idx) {
            return ACTCACHE410_TAB + idx * Len.ACTCACHE410_ELE_TAB;
        }

        public static int actcache410DatiInput(int idx) {
            return actcache410EleTab(idx);
        }

        public static int actcache410DatiOutput(int idx) {
            return actcache410DatiInput(idx) + Len.ACTCACHE410_DATI_INPUT;
        }

        public static int actcache450EleTab(int idx) {
            return ACTCACHE450_TAB + idx * Len.ACTCACHE450_ELE_TAB;
        }

        public static int actcache450DatiInput(int idx) {
            return actcache450EleTab(idx);
        }

        public static int actcache450DatiOutput(int idx) {
            return actcache450DatiInput(idx) + Len.ACTCACHE450_DATI_INPUT;
        }

        public static int actcache511EleTab(int idx) {
            return ACTCACHE511_TAB + idx * Len.ACTCACHE511_ELE_TAB;
        }

        public static int actcache511DatiInput(int idx) {
            return actcache511EleTab(idx);
        }

        public static int actcache511DatiOutput(int idx) {
            return actcache511DatiInput(idx) + Len.ACTCACHE511_DATI_INPUT;
        }

        public static int actcache640EleTab(int idx) {
            return ACTCACHE640_TAB + idx * Len.ACTCACHE640_ELE_TAB;
        }

        public static int actcache640DatiInput(int idx) {
            return actcache640EleTab(idx);
        }

        public static int actcache640DatiOutput(int idx) {
            return actcache640DatiInput(idx) + Len.ACTCACHE640_DATI_INPUT;
        }

        public static int actcache600EleTab(int idx) {
            return ACTCACHE600_TAB + idx * Len.ACTCACHE600_ELE_TAB;
        }

        public static int actcache600DatiInput(int idx) {
            return actcache600EleTab(idx);
        }

        public static int actcache600DatiOutput(int idx) {
            return actcache600DatiInput(idx) + Len.ACTCACHE600_DATI_INPUT;
        }

        public static int actcache650EleTab(int idx) {
            return ACTCACHE650_TAB + idx * Len.ACTCACHE650_ELE_TAB;
        }

        public static int actcache650DatiInput(int idx) {
            return actcache650EleTab(idx);
        }

        public static int actcache650DatiOutput(int idx) {
            return actcache650DatiInput(idx) + Len.ACTCACHE650_DATI_INPUT;
        }

        public static int actcache660EleTab(int idx) {
            return ACTCACHE660_TAB + idx * Len.ACTCACHE660_ELE_TAB;
        }

        public static int actcache660DatiInput(int idx) {
            return actcache660EleTab(idx);
        }

        public static int actcache660DatiOutput(int idx) {
            return actcache660DatiInput(idx) + Len.ACTCACHE660_DATI_INPUT;
        }

        public static int actcache680EleTab(int idx) {
            return ACTCACHE680_TAB + idx * Len.ACTCACHE680_ELE_TAB;
        }

        public static int actcache680DatiInput(int idx) {
            return actcache680EleTab(idx);
        }

        public static int actcache680DatiOutput(int idx) {
            return actcache680DatiInput(idx) + Len.ACTCACHE680_DATI_INPUT;
        }

        public static int actcache690EleTab(int idx) {
            return ACTCACHE690_TAB + idx * Len.ACTCACHE690_ELE_TAB;
        }

        public static int actcache690DatiInput(int idx) {
            return actcache690EleTab(idx);
        }

        public static int actcache690DatiOutput(int idx) {
            return actcache690DatiInput(idx) + Len.ACTCACHE690_DATI_INPUT;
        }

        public static int actcache700EleTab(int idx) {
            return ACTCACHE700_TAB + idx * Len.ACTCACHE700_ELE_TAB;
        }

        public static int actcache700DatiInput(int idx) {
            return actcache700EleTab(idx);
        }

        public static int actcache700DatiOutput(int idx) {
            return actcache700DatiInput(idx) + Len.ACTCACHE700_DATI_INPUT;
        }

        public static int actcache740EleTab(int idx) {
            return ACTCACHE740_TAB + idx * Len.ACTCACHE740_ELE_TAB;
        }

        public static int actcache740DatiInput(int idx) {
            return actcache740EleTab(idx);
        }

        public static int actcache740DatiOutput(int idx) {
            return actcache740DatiInput(idx) + Len.ACTCACHE740_DATI_INPUT;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SERVIZIO_GESTITO = 1;
        public static final int INPUT_INSERITO = 1;
        public static final int INDICE = 5;
        public static final int ACTCACHE030_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE040_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE050_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE060_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE070_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE080_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE090_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE100_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE110_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE130_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE160_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE290_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE310_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE320_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE410_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE450_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE511_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE600_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE640_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE650_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE660_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE680_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE690_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE700_NUM_ELE_SERVIZIO = 5;
        public static final int ACTCACHE740_NUM_ELE_SERVIZIO = 5;
        public static final int TOT_ELE_CARICATI = 5;
        public static final int CONTA_CACHE = 5;
        public static final int ACTCACHE030_DATI_INPUT = 74;
        public static final int ACTCACHE030_DATI_OUTPUT = 28742;
        public static final int ACTCACHE030_ELE_TAB = ACTCACHE030_DATI_INPUT + ACTCACHE030_DATI_OUTPUT;
        public static final int ACTCACHE040_DATI_INPUT = 88;
        public static final int ACTCACHE040_DATI_OUTPUT = 21931;
        public static final int ACTCACHE040_ELE_TAB = ACTCACHE040_DATI_INPUT + ACTCACHE040_DATI_OUTPUT;
        public static final int ACTCACHE050_DATI_INPUT = 312;
        public static final int ACTCACHE050_DATI_OUTPUT = 63212;
        public static final int ACTCACHE050_ELE_TAB = ACTCACHE050_DATI_INPUT + ACTCACHE050_DATI_OUTPUT;
        public static final int ACTCACHE060_DATI_INPUT = 396;
        public static final int ACTCACHE060_DATI_OUTPUT = 15575;
        public static final int ACTCACHE060_ELE_TAB = ACTCACHE060_DATI_INPUT + ACTCACHE060_DATI_OUTPUT;
        public static final int ACTCACHE070_DATI_INPUT = 367;
        public static final int ACTCACHE070_DATI_OUTPUT = 51895;
        public static final int ACTCACHE070_ELE_TAB = ACTCACHE070_DATI_INPUT + ACTCACHE070_DATI_OUTPUT;
        public static final int ACTCACHE100_DATI_INPUT = 9959;
        public static final int ACTCACHE100_DATI_OUTPUT = 10624;
        public static final int ACTCACHE100_ELE_TAB = ACTCACHE100_DATI_INPUT + ACTCACHE100_DATI_OUTPUT;
        public static final int ACTCACHE110_DATI_INPUT = 312;
        public static final int ACTCACHE110_DATI_OUTPUT = 1340;
        public static final int ACTCACHE110_ELE_TAB = ACTCACHE110_DATI_INPUT + ACTCACHE110_DATI_OUTPUT;
        public static final int ACTCACHE130_DATI_INPUT = 1018;
        public static final int ACTCACHE130_DATI_OUTPUT = 76026;
        public static final int ACTCACHE130_ELE_TAB = ACTCACHE130_DATI_INPUT + ACTCACHE130_DATI_OUTPUT;
        public static final int ACTCACHE160_DATI_INPUT = 312;
        public static final int ACTCACHE160_DATI_OUTPUT = 10738;
        public static final int ACTCACHE160_ELE_TAB = ACTCACHE160_DATI_INPUT + ACTCACHE160_DATI_OUTPUT;
        public static final int ACTCACHE290_DATI_INPUT = 17606;
        public static final int ACTCACHE290_DATI_OUTPUT = 60440;
        public static final int ACTCACHE290_ELE_TAB = ACTCACHE290_DATI_INPUT + ACTCACHE290_DATI_OUTPUT;
        public static final int ACTCACHE310_DATI_INPUT = 6;
        public static final int ACTCACHE310_DATI_OUTPUT = 96675;
        public static final int ACTCACHE310_ELE_TAB = ACTCACHE310_DATI_INPUT + ACTCACHE310_DATI_OUTPUT;
        public static final int ACTCACHE320_DATI_INPUT = 326;
        public static final int ACTCACHE320_DATI_OUTPUT = 20194;
        public static final int ACTCACHE320_ELE_TAB = ACTCACHE320_DATI_INPUT + ACTCACHE320_DATI_OUTPUT;
        public static final int ACTCACHE410_DATI_INPUT = 34875;
        public static final int ACTCACHE410_DATI_OUTPUT = 35556;
        public static final int ACTCACHE410_ELE_TAB = ACTCACHE410_DATI_INPUT + ACTCACHE410_DATI_OUTPUT;
        public static final int ACTCACHE450_DATI_INPUT = 56;
        public static final int ACTCACHE450_DATI_OUTPUT = 48624;
        public static final int ACTCACHE450_ELE_TAB = ACTCACHE450_DATI_INPUT + ACTCACHE450_DATI_OUTPUT;
        public static final int ACTCACHE511_DATI_INPUT = 40;
        public static final int ACTCACHE511_DATI_OUTPUT = 7408;
        public static final int ACTCACHE511_ELE_TAB = ACTCACHE511_DATI_INPUT + ACTCACHE511_DATI_OUTPUT;
        public static final int ACTCACHE640_DATI_INPUT = 117232;
        public static final int ACTCACHE640_DATI_OUTPUT = 117915;
        public static final int ACTCACHE640_ELE_TAB = ACTCACHE640_DATI_INPUT + ACTCACHE640_DATI_OUTPUT;
        public static final int ACTCACHE600_DATI_INPUT = 49;
        public static final int ACTCACHE600_DATI_OUTPUT = 723;
        public static final int ACTCACHE600_ELE_TAB = ACTCACHE600_DATI_INPUT + ACTCACHE600_DATI_OUTPUT;
        public static final int ACTCACHE650_DATI_INPUT = 72;
        public static final int ACTCACHE650_DATI_OUTPUT = 24341;
        public static final int ACTCACHE650_ELE_TAB = ACTCACHE650_DATI_INPUT + ACTCACHE650_DATI_OUTPUT;
        public static final int ACTCACHE660_DATI_INPUT = 279;
        public static final int ACTCACHE660_DATI_OUTPUT = 1066;
        public static final int ACTCACHE660_ELE_TAB = ACTCACHE660_DATI_INPUT + ACTCACHE660_DATI_OUTPUT;
        public static final int ACTCACHE680_DATI_INPUT = 3038;
        public static final int ACTCACHE680_DATI_OUTPUT = 30956;
        public static final int ACTCACHE680_ELE_TAB = ACTCACHE680_DATI_INPUT + ACTCACHE680_DATI_OUTPUT;
        public static final int ACTCACHE690_DATI_INPUT = 102;
        public static final int ACTCACHE690_DATI_OUTPUT = 775;
        public static final int ACTCACHE690_ELE_TAB = ACTCACHE690_DATI_INPUT + ACTCACHE690_DATI_OUTPUT;
        public static final int ACTCACHE700_DATI_INPUT = 72;
        public static final int ACTCACHE700_DATI_OUTPUT = 4853;
        public static final int ACTCACHE700_ELE_TAB = ACTCACHE700_DATI_INPUT + ACTCACHE700_DATI_OUTPUT;
        public static final int ACTCACHE740_DATI_INPUT = 338;
        public static final int ACTCACHE740_DATI_OUTPUT = 128798;
        public static final int ACTCACHE740_ELE_TAB = ACTCACHE740_DATI_INPUT + ACTCACHE740_DATI_OUTPUT;
        public static final int CONTATORI_CACHE = SERVIZIO_GESTITO + INPUT_INSERITO + INDICE + ACTCACHE030_NUM_ELE_SERVIZIO + ACTCACHE040_NUM_ELE_SERVIZIO + ACTCACHE050_NUM_ELE_SERVIZIO + ACTCACHE060_NUM_ELE_SERVIZIO + ACTCACHE070_NUM_ELE_SERVIZIO + ACTCACHE080_NUM_ELE_SERVIZIO + ACTCACHE090_NUM_ELE_SERVIZIO + ACTCACHE100_NUM_ELE_SERVIZIO + ACTCACHE110_NUM_ELE_SERVIZIO + ACTCACHE130_NUM_ELE_SERVIZIO + ACTCACHE160_NUM_ELE_SERVIZIO + ACTCACHE290_NUM_ELE_SERVIZIO + ACTCACHE310_NUM_ELE_SERVIZIO + ACTCACHE320_NUM_ELE_SERVIZIO + ACTCACHE410_NUM_ELE_SERVIZIO + ACTCACHE450_NUM_ELE_SERVIZIO + ACTCACHE511_NUM_ELE_SERVIZIO + ACTCACHE600_NUM_ELE_SERVIZIO + ACTCACHE640_NUM_ELE_SERVIZIO + ACTCACHE650_NUM_ELE_SERVIZIO + ACTCACHE660_NUM_ELE_SERVIZIO + ACTCACHE680_NUM_ELE_SERVIZIO + ACTCACHE690_NUM_ELE_SERVIZIO + ACTCACHE700_NUM_ELE_SERVIZIO + ACTCACHE740_NUM_ELE_SERVIZIO;
        public static final int ACTCACHE030_TAB = ActcacheAreaActuator.ACTCACHE030_ELE_TAB_MAXOCCURS * ACTCACHE030_ELE_TAB;
        public static final int ACTCACHE040_TAB = ActcacheAreaActuator.ACTCACHE040_ELE_TAB_MAXOCCURS * ACTCACHE040_ELE_TAB;
        public static final int ACTCACHE050_TAB = ActcacheAreaActuator.ACTCACHE050_ELE_TAB_MAXOCCURS * ACTCACHE050_ELE_TAB;
        public static final int ACTCACHE060_TAB = ActcacheAreaActuator.ACTCACHE060_ELE_TAB_MAXOCCURS * ACTCACHE060_ELE_TAB;
        public static final int ACTCACHE070_TAB = ActcacheAreaActuator.ACTCACHE070_ELE_TAB_MAXOCCURS * ACTCACHE070_ELE_TAB;
        public static final int ACTCACHE100_TAB = ActcacheAreaActuator.ACTCACHE100_ELE_TAB_MAXOCCURS * ACTCACHE100_ELE_TAB;
        public static final int ACTCACHE110_TAB = ActcacheAreaActuator.ACTCACHE110_ELE_TAB_MAXOCCURS * ACTCACHE110_ELE_TAB;
        public static final int ACTCACHE130_TAB = ActcacheAreaActuator.ACTCACHE130_ELE_TAB_MAXOCCURS * ACTCACHE130_ELE_TAB;
        public static final int ACTCACHE160_TAB = ActcacheAreaActuator.ACTCACHE160_ELE_TAB_MAXOCCURS * ACTCACHE160_ELE_TAB;
        public static final int ACTCACHE290_TAB = ActcacheAreaActuator.ACTCACHE290_ELE_TAB_MAXOCCURS * ACTCACHE290_ELE_TAB;
        public static final int ACTCACHE310_TAB = ActcacheAreaActuator.ACTCACHE310_ELE_TAB_MAXOCCURS * ACTCACHE310_ELE_TAB;
        public static final int ACTCACHE320_TAB = ActcacheAreaActuator.ACTCACHE320_ELE_TAB_MAXOCCURS * ACTCACHE320_ELE_TAB;
        public static final int ACTCACHE410_TAB = ActcacheAreaActuator.ACTCACHE410_ELE_TAB_MAXOCCURS * ACTCACHE410_ELE_TAB;
        public static final int ACTCACHE450_TAB = ActcacheAreaActuator.ACTCACHE450_ELE_TAB_MAXOCCURS * ACTCACHE450_ELE_TAB;
        public static final int ACTCACHE511_TAB = ActcacheAreaActuator.ACTCACHE511_ELE_TAB_MAXOCCURS * ACTCACHE511_ELE_TAB;
        public static final int ACTCACHE640_TAB = ActcacheAreaActuator.ACTCACHE640_ELE_TAB_MAXOCCURS * ACTCACHE640_ELE_TAB;
        public static final int ACTCACHE600_TAB = ActcacheAreaActuator.ACTCACHE600_ELE_TAB_MAXOCCURS * ACTCACHE600_ELE_TAB;
        public static final int ACTCACHE650_TAB = ActcacheAreaActuator.ACTCACHE650_ELE_TAB_MAXOCCURS * ACTCACHE650_ELE_TAB;
        public static final int ACTCACHE660_TAB = ActcacheAreaActuator.ACTCACHE660_ELE_TAB_MAXOCCURS * ACTCACHE660_ELE_TAB;
        public static final int ACTCACHE680_TAB = ActcacheAreaActuator.ACTCACHE680_ELE_TAB_MAXOCCURS * ACTCACHE680_ELE_TAB;
        public static final int ACTCACHE690_TAB = ActcacheAreaActuator.ACTCACHE690_ELE_TAB_MAXOCCURS * ACTCACHE690_ELE_TAB;
        public static final int ACTCACHE700_TAB = ActcacheAreaActuator.ACTCACHE700_ELE_TAB_MAXOCCURS * ACTCACHE700_ELE_TAB;
        public static final int ACTCACHE740_TAB = ActcacheAreaActuator.ACTCACHE740_ELE_TAB_MAXOCCURS * ACTCACHE740_ELE_TAB;
        public static final int AREA_TABELLE = ACTCACHE030_TAB + ACTCACHE040_TAB + ACTCACHE050_TAB + ACTCACHE060_TAB + ACTCACHE070_TAB + ACTCACHE100_TAB + ACTCACHE110_TAB + ACTCACHE130_TAB + ACTCACHE160_TAB + ACTCACHE290_TAB + ACTCACHE310_TAB + ACTCACHE320_TAB + ACTCACHE410_TAB + ACTCACHE450_TAB + ACTCACHE511_TAB + ACTCACHE640_TAB + ACTCACHE600_TAB + ACTCACHE650_TAB + ACTCACHE660_TAB + ACTCACHE680_TAB + ACTCACHE690_TAB + ACTCACHE700_TAB + ACTCACHE740_TAB;
        public static final int ACTCACHE_AREA_ACTUATOR = CONTATORI_CACHE + TOT_ELE_CARICATI + CONTA_CACHE + AREA_TABELLE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int INDICE = 9;
            public static final int ACTCACHE030_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE040_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE050_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE060_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE070_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE080_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE090_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE100_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE110_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE130_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE160_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE290_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE310_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE320_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE410_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE450_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE511_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE600_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE640_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE650_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE660_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE680_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE690_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE700_NUM_ELE_SERVIZIO = 9;
            public static final int ACTCACHE740_NUM_ELE_SERVIZIO = 9;
            public static final int TOT_ELE_CARICATI = 9;
            public static final int CONTA_CACHE = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
