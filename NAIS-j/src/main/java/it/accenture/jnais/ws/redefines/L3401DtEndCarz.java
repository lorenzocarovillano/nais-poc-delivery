package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-DT-END-CARZ<br>
 * Variable: L3401-DT-END-CARZ from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401DtEndCarz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401DtEndCarz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_DT_END_CARZ;
    }

    public void setL3401DtEndCarz(int l3401DtEndCarz) {
        writeIntAsPacked(Pos.L3401_DT_END_CARZ, l3401DtEndCarz, Len.Int.L3401_DT_END_CARZ);
    }

    public void setL3401DtEndCarzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_DT_END_CARZ, Pos.L3401_DT_END_CARZ);
    }

    /**Original name: L3401-DT-END-CARZ<br>*/
    public int getL3401DtEndCarz() {
        return readPackedAsInt(Pos.L3401_DT_END_CARZ, Len.Int.L3401_DT_END_CARZ);
    }

    public byte[] getL3401DtEndCarzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_DT_END_CARZ, Pos.L3401_DT_END_CARZ);
        return buffer;
    }

    /**Original name: L3401-DT-END-CARZ-NULL<br>*/
    public String getL3401DtEndCarzNull() {
        return readString(Pos.L3401_DT_END_CARZ_NULL, Len.L3401_DT_END_CARZ_NULL);
    }

    public String getL3401DtEndCarzNullFormatted() {
        return Functions.padBlanks(getL3401DtEndCarzNull(), Len.L3401_DT_END_CARZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_DT_END_CARZ = 1;
        public static final int L3401_DT_END_CARZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_DT_END_CARZ = 5;
        public static final int L3401_DT_END_CARZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_DT_END_CARZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
