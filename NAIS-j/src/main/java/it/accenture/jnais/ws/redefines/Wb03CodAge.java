package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-COD-AGE<br>
 * Variable: WB03-COD-AGE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CodAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CodAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_COD_AGE;
    }

    public void setWb03CodAge(int wb03CodAge) {
        writeIntAsPacked(Pos.WB03_COD_AGE, wb03CodAge, Len.Int.WB03_COD_AGE);
    }

    public void setWb03CodAgeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_COD_AGE, Pos.WB03_COD_AGE);
    }

    /**Original name: WB03-COD-AGE<br>*/
    public int getWb03CodAge() {
        return readPackedAsInt(Pos.WB03_COD_AGE, Len.Int.WB03_COD_AGE);
    }

    public byte[] getWb03CodAgeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_COD_AGE, Pos.WB03_COD_AGE);
        return buffer;
    }

    public void setWb03CodAgeNull(String wb03CodAgeNull) {
        writeString(Pos.WB03_COD_AGE_NULL, wb03CodAgeNull, Len.WB03_COD_AGE_NULL);
    }

    /**Original name: WB03-COD-AGE-NULL<br>*/
    public String getWb03CodAgeNull() {
        return readString(Pos.WB03_COD_AGE_NULL, Len.WB03_COD_AGE_NULL);
    }

    public String getWb03CodAgeNullFormatted() {
        return Functions.padBlanks(getWb03CodAgeNull(), Len.WB03_COD_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_COD_AGE = 1;
        public static final int WB03_COD_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_COD_AGE = 3;
        public static final int WB03_COD_AGE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_COD_AGE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
