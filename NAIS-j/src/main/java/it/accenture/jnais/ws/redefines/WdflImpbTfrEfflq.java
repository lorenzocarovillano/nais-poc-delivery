package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-TFR-EFFLQ<br>
 * Variable: WDFL-IMPB-TFR-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbTfrEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbTfrEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_TFR_EFFLQ;
    }

    public void setWdflImpbTfrEfflq(AfDecimal wdflImpbTfrEfflq) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_TFR_EFFLQ, wdflImpbTfrEfflq.copy());
    }

    public void setWdflImpbTfrEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_TFR_EFFLQ, Pos.WDFL_IMPB_TFR_EFFLQ);
    }

    /**Original name: WDFL-IMPB-TFR-EFFLQ<br>*/
    public AfDecimal getWdflImpbTfrEfflq() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_TFR_EFFLQ, Len.Int.WDFL_IMPB_TFR_EFFLQ, Len.Fract.WDFL_IMPB_TFR_EFFLQ);
    }

    public byte[] getWdflImpbTfrEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_TFR_EFFLQ, Pos.WDFL_IMPB_TFR_EFFLQ);
        return buffer;
    }

    public void setWdflImpbTfrEfflqNull(String wdflImpbTfrEfflqNull) {
        writeString(Pos.WDFL_IMPB_TFR_EFFLQ_NULL, wdflImpbTfrEfflqNull, Len.WDFL_IMPB_TFR_EFFLQ_NULL);
    }

    /**Original name: WDFL-IMPB-TFR-EFFLQ-NULL<br>*/
    public String getWdflImpbTfrEfflqNull() {
        return readString(Pos.WDFL_IMPB_TFR_EFFLQ_NULL, Len.WDFL_IMPB_TFR_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_TFR_EFFLQ = 1;
        public static final int WDFL_IMPB_TFR_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_TFR_EFFLQ = 8;
        public static final int WDFL_IMPB_TFR_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_TFR_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_TFR_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
