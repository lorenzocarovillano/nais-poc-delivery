package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-EC-TCM-IND<br>
 * Variable: WPCO-DT-ULT-EC-TCM-IND from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEcTcmInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEcTcmInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_EC_TCM_IND;
    }

    public void setWpcoDtUltEcTcmInd(int wpcoDtUltEcTcmInd) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_EC_TCM_IND, wpcoDtUltEcTcmInd, Len.Int.WPCO_DT_ULT_EC_TCM_IND);
    }

    public void setDpcoDtUltEcTcmIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_EC_TCM_IND, Pos.WPCO_DT_ULT_EC_TCM_IND);
    }

    /**Original name: WPCO-DT-ULT-EC-TCM-IND<br>*/
    public int getWpcoDtUltEcTcmInd() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_EC_TCM_IND, Len.Int.WPCO_DT_ULT_EC_TCM_IND);
    }

    public byte[] getWpcoDtUltEcTcmIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_EC_TCM_IND, Pos.WPCO_DT_ULT_EC_TCM_IND);
        return buffer;
    }

    public void setWpcoDtUltEcTcmIndNull(String wpcoDtUltEcTcmIndNull) {
        writeString(Pos.WPCO_DT_ULT_EC_TCM_IND_NULL, wpcoDtUltEcTcmIndNull, Len.WPCO_DT_ULT_EC_TCM_IND_NULL);
    }

    /**Original name: WPCO-DT-ULT-EC-TCM-IND-NULL<br>*/
    public String getWpcoDtUltEcTcmIndNull() {
        return readString(Pos.WPCO_DT_ULT_EC_TCM_IND_NULL, Len.WPCO_DT_ULT_EC_TCM_IND_NULL);
    }

    public String getWpcoDtUltEcTcmIndNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEcTcmIndNull(), Len.WPCO_DT_ULT_EC_TCM_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_TCM_IND = 1;
        public static final int WPCO_DT_ULT_EC_TCM_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_TCM_IND = 5;
        public static final int WPCO_DT_ULT_EC_TCM_IND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_EC_TCM_IND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
