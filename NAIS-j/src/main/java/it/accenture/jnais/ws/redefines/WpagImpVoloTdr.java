package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-VOLO-TDR<br>
 * Variable: WPAG-IMP-VOLO-TDR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpVoloTdr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpVoloTdr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_VOLO_TDR;
    }

    public void setWpagImpVoloTdr(AfDecimal wpagImpVoloTdr) {
        writeDecimalAsPacked(Pos.WPAG_IMP_VOLO_TDR, wpagImpVoloTdr.copy());
    }

    public void setWpagImpVoloTdrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_VOLO_TDR, Pos.WPAG_IMP_VOLO_TDR);
    }

    /**Original name: WPAG-IMP-VOLO-TDR<br>*/
    public AfDecimal getWpagImpVoloTdr() {
        return readPackedAsDecimal(Pos.WPAG_IMP_VOLO_TDR, Len.Int.WPAG_IMP_VOLO_TDR, Len.Fract.WPAG_IMP_VOLO_TDR);
    }

    public byte[] getWpagImpVoloTdrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_VOLO_TDR, Pos.WPAG_IMP_VOLO_TDR);
        return buffer;
    }

    public void initWpagImpVoloTdrSpaces() {
        fill(Pos.WPAG_IMP_VOLO_TDR, Len.WPAG_IMP_VOLO_TDR, Types.SPACE_CHAR);
    }

    public void setWpagImpVoloTdrNull(String wpagImpVoloTdrNull) {
        writeString(Pos.WPAG_IMP_VOLO_TDR_NULL, wpagImpVoloTdrNull, Len.WPAG_IMP_VOLO_TDR_NULL);
    }

    /**Original name: WPAG-IMP-VOLO-TDR-NULL<br>*/
    public String getWpagImpVoloTdrNull() {
        return readString(Pos.WPAG_IMP_VOLO_TDR_NULL, Len.WPAG_IMP_VOLO_TDR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_VOLO_TDR = 1;
        public static final int WPAG_IMP_VOLO_TDR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_VOLO_TDR = 8;
        public static final int WPAG_IMP_VOLO_TDR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_VOLO_TDR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_VOLO_TDR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
