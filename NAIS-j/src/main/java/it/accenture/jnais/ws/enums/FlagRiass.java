package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-RIASS<br>
 * Variable: FLAG-RIASS from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagRiass {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagRiass(char flagRiass) {
        this.value = flagRiass;
    }

    public char getFlagRiass() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
