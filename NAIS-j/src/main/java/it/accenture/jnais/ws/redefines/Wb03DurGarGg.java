package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-GAR-GG<br>
 * Variable: WB03-DUR-GAR-GG from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DurGarGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DurGarGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR_GAR_GG;
    }

    public void setWb03DurGarGg(int wb03DurGarGg) {
        writeIntAsPacked(Pos.WB03_DUR_GAR_GG, wb03DurGarGg, Len.Int.WB03_DUR_GAR_GG);
    }

    public void setWb03DurGarGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR_GAR_GG, Pos.WB03_DUR_GAR_GG);
    }

    /**Original name: WB03-DUR-GAR-GG<br>*/
    public int getWb03DurGarGg() {
        return readPackedAsInt(Pos.WB03_DUR_GAR_GG, Len.Int.WB03_DUR_GAR_GG);
    }

    public byte[] getWb03DurGarGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR_GAR_GG, Pos.WB03_DUR_GAR_GG);
        return buffer;
    }

    public void setWb03DurGarGgNull(String wb03DurGarGgNull) {
        writeString(Pos.WB03_DUR_GAR_GG_NULL, wb03DurGarGgNull, Len.WB03_DUR_GAR_GG_NULL);
    }

    /**Original name: WB03-DUR-GAR-GG-NULL<br>*/
    public String getWb03DurGarGgNull() {
        return readString(Pos.WB03_DUR_GAR_GG_NULL, Len.WB03_DUR_GAR_GG_NULL);
    }

    public String getWb03DurGarGgNullFormatted() {
        return Functions.padBlanks(getWb03DurGarGgNull(), Len.WB03_DUR_GAR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR_GAR_GG = 1;
        public static final int WB03_DUR_GAR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR_GAR_GG = 3;
        public static final int WB03_DUR_GAR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR_GAR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
