package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-IS-662014D<br>
 * Variable: DFL-IMPB-IS-662014D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbIs662014d extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbIs662014d() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_IS662014D;
    }

    public void setDflImpbIs662014d(AfDecimal dflImpbIs662014d) {
        writeDecimalAsPacked(Pos.DFL_IMPB_IS662014D, dflImpbIs662014d.copy());
    }

    public void setDflImpbIs662014dFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_IS662014D, Pos.DFL_IMPB_IS662014D);
    }

    /**Original name: DFL-IMPB-IS-662014D<br>*/
    public AfDecimal getDflImpbIs662014d() {
        return readPackedAsDecimal(Pos.DFL_IMPB_IS662014D, Len.Int.DFL_IMPB_IS662014D, Len.Fract.DFL_IMPB_IS662014D);
    }

    public byte[] getDflImpbIs662014dAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_IS662014D, Pos.DFL_IMPB_IS662014D);
        return buffer;
    }

    public void setDflImpbIs662014dNull(String dflImpbIs662014dNull) {
        writeString(Pos.DFL_IMPB_IS662014D_NULL, dflImpbIs662014dNull, Len.DFL_IMPB_IS662014D_NULL);
    }

    /**Original name: DFL-IMPB-IS-662014D-NULL<br>*/
    public String getDflImpbIs662014dNull() {
        return readString(Pos.DFL_IMPB_IS662014D_NULL, Len.DFL_IMPB_IS662014D_NULL);
    }

    public String getDflImpbIs662014dNullFormatted() {
        return Functions.padBlanks(getDflImpbIs662014dNull(), Len.DFL_IMPB_IS662014D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_IS662014D = 1;
        public static final int DFL_IMPB_IS662014D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_IS662014D = 8;
        public static final int DFL_IMPB_IS662014D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_IS662014D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_IS662014D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
