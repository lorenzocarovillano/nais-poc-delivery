package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ABB-TOT-ULT<br>
 * Variable: WTGA-ABB-TOT-ULT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaAbbTotUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaAbbTotUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ABB_TOT_ULT;
    }

    public void setWtgaAbbTotUlt(AfDecimal wtgaAbbTotUlt) {
        writeDecimalAsPacked(Pos.WTGA_ABB_TOT_ULT, wtgaAbbTotUlt.copy());
    }

    public void setWtgaAbbTotUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ABB_TOT_ULT, Pos.WTGA_ABB_TOT_ULT);
    }

    /**Original name: WTGA-ABB-TOT-ULT<br>*/
    public AfDecimal getWtgaAbbTotUlt() {
        return readPackedAsDecimal(Pos.WTGA_ABB_TOT_ULT, Len.Int.WTGA_ABB_TOT_ULT, Len.Fract.WTGA_ABB_TOT_ULT);
    }

    public byte[] getWtgaAbbTotUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ABB_TOT_ULT, Pos.WTGA_ABB_TOT_ULT);
        return buffer;
    }

    public void initWtgaAbbTotUltSpaces() {
        fill(Pos.WTGA_ABB_TOT_ULT, Len.WTGA_ABB_TOT_ULT, Types.SPACE_CHAR);
    }

    public void setWtgaAbbTotUltNull(String wtgaAbbTotUltNull) {
        writeString(Pos.WTGA_ABB_TOT_ULT_NULL, wtgaAbbTotUltNull, Len.WTGA_ABB_TOT_ULT_NULL);
    }

    /**Original name: WTGA-ABB-TOT-ULT-NULL<br>*/
    public String getWtgaAbbTotUltNull() {
        return readString(Pos.WTGA_ABB_TOT_ULT_NULL, Len.WTGA_ABB_TOT_ULT_NULL);
    }

    public String getWtgaAbbTotUltNullFormatted() {
        return Functions.padBlanks(getWtgaAbbTotUltNull(), Len.WTGA_ABB_TOT_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ABB_TOT_ULT = 1;
        public static final int WTGA_ABB_TOT_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ABB_TOT_ULT = 8;
        public static final int WTGA_ABB_TOT_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_ABB_TOT_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ABB_TOT_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
