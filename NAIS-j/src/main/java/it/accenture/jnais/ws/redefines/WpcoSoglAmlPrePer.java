package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-SOGL-AML-PRE-PER<br>
 * Variable: WPCO-SOGL-AML-PRE-PER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoSoglAmlPrePer extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoSoglAmlPrePer() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_SOGL_AML_PRE_PER;
    }

    public void setWpcoSoglAmlPrePer(AfDecimal wpcoSoglAmlPrePer) {
        writeDecimalAsPacked(Pos.WPCO_SOGL_AML_PRE_PER, wpcoSoglAmlPrePer.copy());
    }

    public void setDpcoSoglAmlPrePerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_SOGL_AML_PRE_PER, Pos.WPCO_SOGL_AML_PRE_PER);
    }

    /**Original name: WPCO-SOGL-AML-PRE-PER<br>*/
    public AfDecimal getWpcoSoglAmlPrePer() {
        return readPackedAsDecimal(Pos.WPCO_SOGL_AML_PRE_PER, Len.Int.WPCO_SOGL_AML_PRE_PER, Len.Fract.WPCO_SOGL_AML_PRE_PER);
    }

    public byte[] getWpcoSoglAmlPrePerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_SOGL_AML_PRE_PER, Pos.WPCO_SOGL_AML_PRE_PER);
        return buffer;
    }

    public void setWpcoSoglAmlPrePerNull(String wpcoSoglAmlPrePerNull) {
        writeString(Pos.WPCO_SOGL_AML_PRE_PER_NULL, wpcoSoglAmlPrePerNull, Len.WPCO_SOGL_AML_PRE_PER_NULL);
    }

    /**Original name: WPCO-SOGL-AML-PRE-PER-NULL<br>*/
    public String getWpcoSoglAmlPrePerNull() {
        return readString(Pos.WPCO_SOGL_AML_PRE_PER_NULL, Len.WPCO_SOGL_AML_PRE_PER_NULL);
    }

    public String getWpcoSoglAmlPrePerNullFormatted() {
        return Functions.padBlanks(getWpcoSoglAmlPrePerNull(), Len.WPCO_SOGL_AML_PRE_PER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_SOGL_AML_PRE_PER = 1;
        public static final int WPCO_SOGL_AML_PRE_PER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_SOGL_AML_PRE_PER = 8;
        public static final int WPCO_SOGL_AML_PRE_PER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_SOGL_AML_PRE_PER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_SOGL_AML_PRE_PER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
