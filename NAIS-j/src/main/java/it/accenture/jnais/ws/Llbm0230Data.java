package it.accenture.jnais.ws;

import java.util.Arrays;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.util.Functions;
import com.fasterxml.jackson.core.util.ByteArrayBuilder;
import com.sun.mail.iap.ByteArray;

import it.accenture.jnais.copy.Iabcsq99;
import it.accenture.jnais.copy.Iabv0007;
import it.accenture.jnais.copy.Iabv0008;
import it.accenture.jnais.copy.Iabvsqg1;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Ieav9904;
import it.accenture.jnais.copy.Ldbvc821Llbm0230;
import it.accenture.jnais.ws.ptr.AreaIdsv0102;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LLBM0230<br>
 * Generated as a class for rule WS.<br>*/
public class Llbm0230Data {

    //==== PROPERTIES ====
    //Original name: IABVSQG1
    private Iabvsqg1 iabvsqg1 = new Iabvsqg1();
    //Original name: WK-PGM
    private String wkPgm = "LLBM0230";
    //Original name: IABCSQ99
    private Iabcsq99 iabcsq99 = new Iabcsq99();
    /**Original name: WK-PGM-SERV-ROTTURA<br>
	 * <pre>--
	 * --    valorizzare i campi PGM
	 * --    per un 'eventuale servizio in caso di rottura chiave
	 * --</pre>*/
    private String wkPgmServRottura = "";
    //Original name: WK-PGM-SERV-SECON-BUS
    private String wkPgmServSeconBus = "";
    //Original name: WK-PGM-SERV-SECON-ROTT
    private String wkPgmServSeconRott = "";
    //Original name: WLB-REC-PREN
    private WlbRecPren wlbRecPren = new WlbRecPren();
    //Original name: LDBVC821
    private Ldbvc821Llbm0230 ldbvc821 = new Ldbvc821Llbm0230();
    //Original name: WS-FINE-PARAMETRI-ERR
    private String wsFineParametriErr = "";
    //Original name: REC-SCARTATI-1
    private short recScartati1 = ((short)1);
    /**Original name: AREA-IDSV0102<br>
	 * <pre>*****************************************************************
	 *  STRUTTURA PER GESTIONE CACHE COMP_STR_DATO / ANAG_DATO
	 * *****************************************************************</pre>*/
    private AreaIdsv0102 areaIdsv0102 = new AreaIdsv0102(new byte[400000]);
    //Original name: IABV0007
    private Iabv0007 iabv0007 = new Iabv0007();
    //Original name: IABV0008
    private Iabv0008 iabv0008 = new Iabv0008();
    //Original name: BTC-BATCH-STATE
    private BtcElabState btcBatchState = new BtcElabState();
    //Original name: BTC-BATCH-TYPE
    private BtcBatchType btcBatchType = new BtcBatchType();
    //Original name: BTC-EXE-MESSAGE
    private BtcExeMessage btcExeMessage = new BtcExeMessage();
    //Original name: BTC-ELAB-STATE
    private BtcElabState btcElabState = new BtcElabState();
    //Original name: BTC-JOB-EXECUTION
    private BtcJobExecution btcJobExecution = new BtcJobExecution();
    //Original name: BTC-JOB-SCHEDULE
    private BtcJobSchedule btcJobSchedule = new BtcJobSchedule();
    //Original name: BTC-REC-SCHEDULE
    private BtcRecScheduleLlbm0230 btcRecSchedule = new BtcRecScheduleLlbm0230();
    //Original name: BTC-BATCH
    private BtcBatch btcBatch = new BtcBatch();
    //Original name: LOG-ERRORE
    private LogErroreIabs0120 logErrore = new LogErroreIabs0120();
    //Original name: GRU-ARZ
    private GruArz gruArz = new GruArz();
    //Original name: BTC-PARALLELISM
    private BtcParallelism btcParallelism = new BtcParallelism();
    //Original name: IABV0006
    private Iabv0006 iabv0006 = new Iabv0006();
    //Original name: POLI
    private PoliIdbspol0 poli = new PoliIdbspol0();
    //Original name: ADES
    private Ades ades = new Ades();
    //Original name: STAT-OGG-BUS
    private StatOggBusIdbsstb0 statOggBus = new StatOggBusIdbsstb0();
    //Original name: RICH
    private RichIabs0900 rich = new RichIabs0900();
    //Original name: ITSO0041
    private Itso0041 itso0041 = new Itso0041();
    //Original name: LLBV0269
    private Llbv0269 llbv0269 = new Llbv0269();
    //Original name: IEAV9904
    private Ieav9904 ieav9904 = new Ieav9904();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: AREA-INP-SCARTI
    private AreaInpScarti areaInpScarti = new AreaInpScarti();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getWkPgmFormatted() {
        return Functions.padBlanks(getWkPgm(), Len.WK_PGM);
    }

    public void setWkPgmServRottura(String wkPgmServRottura) {
        this.wkPgmServRottura = Functions.subString(wkPgmServRottura, Len.WK_PGM_SERV_ROTTURA);
    }

    public String getWkPgmServRottura() {
        return this.wkPgmServRottura;
    }

    public String getWkPgmServRotturaFormatted() {
        return Functions.padBlanks(getWkPgmServRottura(), Len.WK_PGM_SERV_ROTTURA);
    }

    public void setWkPgmServSeconBus(String wkPgmServSeconBus) {
        this.wkPgmServSeconBus = Functions.subString(wkPgmServSeconBus, Len.WK_PGM_SERV_SECON_BUS);
    }

    public String getWkPgmServSeconBus() {
        return this.wkPgmServSeconBus;
    }

    public String getWkPgmServSeconBusFormatted() {
        return Functions.padBlanks(getWkPgmServSeconBus(), Len.WK_PGM_SERV_SECON_BUS);
    }

    public void setWkPgmServSeconRott(String wkPgmServSeconRott) {
        this.wkPgmServSeconRott = Functions.subString(wkPgmServSeconRott, Len.WK_PGM_SERV_SECON_ROTT);
    }

    public String getWkPgmServSeconRott() {
        return this.wkPgmServSeconRott;
    }

    public String getWkPgmServSeconRottFormatted() {
        return Functions.padBlanks(getWkPgmServSeconRott(), Len.WK_PGM_SERV_SECON_ROTT);
    }

    public void setWsFineParametriErr(String wsFineParametriErr) {
        this.wsFineParametriErr = Functions.subString(wsFineParametriErr, Len.WS_FINE_PARAMETRI_ERR);
    }

    public String getWsFineParametriErr() {
        return this.wsFineParametriErr;
    }

    public String getWsFineParametriErrFormatted() {
        return Functions.padBlanks(getWsFineParametriErr(), Len.WS_FINE_PARAMETRI_ERR);
    }

    public short getRecScartati1() {
        return this.recScartati1;
    }

    public Ades getAdes() {
        return ades;
    }

    public AreaIdsv0102 getAreaIdsv0102() {
        return areaIdsv0102;
    }

    public AreaInpScarti getAreaInpScarti() {
        return areaInpScarti;
    }

    public BtcBatch getBtcBatch() {
        return btcBatch;
    }

    public BtcElabState getBtcBatchState() {
        return btcBatchState;
    }

    public BtcBatchType getBtcBatchType() {
        return btcBatchType;
    }

    public BtcElabState getBtcElabState() {
        return btcElabState;
    }

    public BtcExeMessage getBtcExeMessage() {
        return btcExeMessage;
    }

    public BtcJobExecution getBtcJobExecution() {
        return btcJobExecution;
    }

    public BtcJobSchedule getBtcJobSchedule() {
        return btcJobSchedule;
    }

    public BtcParallelism getBtcParallelism() {
        return btcParallelism;
    }

    public BtcRecScheduleLlbm0230 getBtcRecSchedule() {
        return btcRecSchedule;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public GruArz getGruArz() {
        return gruArz;
    }

    public Iabcsq99 getIabcsq99() {
        return iabcsq99;
    }

    public Iabv0006 getIabv0006() {
        return iabv0006;
    }

    public Iabv0007 getIabv0007() {
        return iabv0007;
    }

    public Iabv0008 getIabv0008() {
        return iabv0008;
    }

    public Iabvsqg1 getIabvsqg1() {
        return iabvsqg1;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieav9904 getIeav9904() {
        return ieav9904;
    }

    public Itso0041 getItso0041() {
        return itso0041;
    }

    public Ldbvc821Llbm0230 getLdbvc821() {
        return ldbvc821;
    }

    public Llbv0269 getLlbv0269() {
        return llbv0269;
    }

    public LogErroreIabs0120 getLogErrore() {
        return logErrore;
    }

    public PoliIdbspol0 getPoli() {
        return poli;
    }

    public RichIabs0900 getRich() {
        return rich;
    }

    public StatOggBusIdbsstb0 getStatOggBus() {
        return statOggBus;
    }

    public WlbRecPren getWlbRecPren() {
        return wlbRecPren;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NUM_ID_POLI = 9;
        public static final int WK_PGM = 8;
        public static final int WS_FINE_PARAMETRI_ERR = 33;
        public static final int WK_PGM_SERV_ROTTURA = 8;
        public static final int WK_PGM_SERV_SECON_BUS = 8;
        public static final int WK_PGM_SERV_SECON_ROTT = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
