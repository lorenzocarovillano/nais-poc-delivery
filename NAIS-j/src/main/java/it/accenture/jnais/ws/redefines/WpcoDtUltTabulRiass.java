package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-TABUL-RIASS<br>
 * Variable: WPCO-DT-ULT-TABUL-RIASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltTabulRiass extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltTabulRiass() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_TABUL_RIASS;
    }

    public void setWpcoDtUltTabulRiass(int wpcoDtUltTabulRiass) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_TABUL_RIASS, wpcoDtUltTabulRiass, Len.Int.WPCO_DT_ULT_TABUL_RIASS);
    }

    public void setDpcoDtUltTabulRiassFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_TABUL_RIASS, Pos.WPCO_DT_ULT_TABUL_RIASS);
    }

    /**Original name: WPCO-DT-ULT-TABUL-RIASS<br>*/
    public int getWpcoDtUltTabulRiass() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_TABUL_RIASS, Len.Int.WPCO_DT_ULT_TABUL_RIASS);
    }

    public byte[] getWpcoDtUltTabulRiassAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_TABUL_RIASS, Pos.WPCO_DT_ULT_TABUL_RIASS);
        return buffer;
    }

    public void setWpcoDtUltTabulRiassNull(String wpcoDtUltTabulRiassNull) {
        writeString(Pos.WPCO_DT_ULT_TABUL_RIASS_NULL, wpcoDtUltTabulRiassNull, Len.WPCO_DT_ULT_TABUL_RIASS_NULL);
    }

    /**Original name: WPCO-DT-ULT-TABUL-RIASS-NULL<br>*/
    public String getWpcoDtUltTabulRiassNull() {
        return readString(Pos.WPCO_DT_ULT_TABUL_RIASS_NULL, Len.WPCO_DT_ULT_TABUL_RIASS_NULL);
    }

    public String getWpcoDtUltTabulRiassNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltTabulRiassNull(), Len.WPCO_DT_ULT_TABUL_RIASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_TABUL_RIASS = 1;
        public static final int WPCO_DT_ULT_TABUL_RIASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_TABUL_RIASS = 5;
        public static final int WPCO_DT_ULT_TABUL_RIASS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_TABUL_RIASS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
