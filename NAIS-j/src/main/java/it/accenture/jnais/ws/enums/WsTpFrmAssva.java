package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-FRM-ASSVA<br>
 * Variable: WS-TP-FRM-ASSVA from copybook LCCVXFA0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpFrmAssva {

    //==== PROPERTIES ====
    private String value = "";
    public static final String INDIVIDUALE = "IN";
    public static final String COLLETTIVA = "CO";
    public static final String ENTRAMBI = "EN";

    //==== METHODS ====
    public void setWsTpFrmAssva(String wsTpFrmAssva) {
        this.value = Functions.subString(wsTpFrmAssva, Len.WS_TP_FRM_ASSVA);
    }

    public String getWsTpFrmAssva() {
        return this.value;
    }

    public boolean isIndividuale() {
        return value.equals(INDIVIDUALE);
    }

    public boolean isCollettiva() {
        return value.equals(COLLETTIVA);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_FRM_ASSVA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
