package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-COLLGM<br>
 * Variable: WS-TP-COLLGM from copybook LCCVXCO0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpCollgm {

    //==== PROPERTIES ====
    private String value = "";
    public static final String TRASFORMAZIONE = "TR";
    public static final String ABBINAMENTO = "AB";
    public static final String CONTINUAZIONE = "CO";
    public static final String TRASFERIMENTO_TESTA_POSIZIONE = "TT";
    public static final String POLIZZA_DA_REINVESTIMENTO = "PR";
    public static final String VERSAMENTO_DA_REINVESTIMENTO = "VR";
    public static final String CONGUAGLIO_MANAGMENT_FEE = "CM";
    public static final String RECUP_PROVV_STORNI_ALRE_POL = "RS";
    public static final String RECUP_PROVV_EMISS_ALRE_POL = "RE";
    public static final String PIANO_REALTA_ABILITATO = "RA";
    public static final String PIANO_REALTA_DISABILITATO = "RD";

    //==== METHODS ====
    public void setWsTpCollgm(String wsTpCollgm) {
        this.value = Functions.subString(wsTpCollgm, Len.WS_TP_COLLGM);
    }

    public String getWsTpCollgm() {
        return this.value;
    }

    public void setRecupProvvStorniAlrePol() {
        value = RECUP_PROVV_STORNI_ALRE_POL;
    }

    public void setRecupProvvEmissAlrePol() {
        value = RECUP_PROVV_EMISS_ALRE_POL;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_COLLGM = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
