package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-TGA-ALIQ-INCAS<br>
 * Variable: WPAG-TGA-ALIQ-INCAS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaAliqIncas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTgaAliqIncas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TGA_ALIQ_INCAS;
    }

    public void setWpagTgaAliqIncas(AfDecimal wpagTgaAliqIncas) {
        writeDecimalAsPacked(Pos.WPAG_TGA_ALIQ_INCAS, wpagTgaAliqIncas.copy());
    }

    public void setWpagTgaAliqIncasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TGA_ALIQ_INCAS, Pos.WPAG_TGA_ALIQ_INCAS);
    }

    /**Original name: WPAG-TGA-ALIQ-INCAS<br>*/
    public AfDecimal getWpagTgaAliqIncas() {
        return readPackedAsDecimal(Pos.WPAG_TGA_ALIQ_INCAS, Len.Int.WPAG_TGA_ALIQ_INCAS, Len.Fract.WPAG_TGA_ALIQ_INCAS);
    }

    public byte[] getWpagTgaAliqIncasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TGA_ALIQ_INCAS, Pos.WPAG_TGA_ALIQ_INCAS);
        return buffer;
    }

    public void initWpagTgaAliqIncasSpaces() {
        fill(Pos.WPAG_TGA_ALIQ_INCAS, Len.WPAG_TGA_ALIQ_INCAS, Types.SPACE_CHAR);
    }

    public void setWpagTgaAliqIncasNull(String wpagTgaAliqIncasNull) {
        writeString(Pos.WPAG_TGA_ALIQ_INCAS_NULL, wpagTgaAliqIncasNull, Len.WPAG_TGA_ALIQ_INCAS_NULL);
    }

    /**Original name: WPAG-TGA-ALIQ-INCAS-NULL<br>*/
    public String getWpagTgaAliqIncasNull() {
        return readString(Pos.WPAG_TGA_ALIQ_INCAS_NULL, Len.WPAG_TGA_ALIQ_INCAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_ALIQ_INCAS = 1;
        public static final int WPAG_TGA_ALIQ_INCAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_ALIQ_INCAS = 8;
        public static final int WPAG_TGA_ALIQ_INCAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_ALIQ_INCAS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_ALIQ_INCAS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
