package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WADE-DT-ULT-CONS-CNBT<br>
 * Variable: WADE-DT-ULT-CONS-CNBT from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeDtUltConsCnbt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeDtUltConsCnbt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_DT_ULT_CONS_CNBT;
    }

    public void setWadeDtUltConsCnbt(int wadeDtUltConsCnbt) {
        writeIntAsPacked(Pos.WADE_DT_ULT_CONS_CNBT, wadeDtUltConsCnbt, Len.Int.WADE_DT_ULT_CONS_CNBT);
    }

    public void setWadeDtUltConsCnbtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_DT_ULT_CONS_CNBT, Pos.WADE_DT_ULT_CONS_CNBT);
    }

    /**Original name: WADE-DT-ULT-CONS-CNBT<br>*/
    public int getWadeDtUltConsCnbt() {
        return readPackedAsInt(Pos.WADE_DT_ULT_CONS_CNBT, Len.Int.WADE_DT_ULT_CONS_CNBT);
    }

    public byte[] getWadeDtUltConsCnbtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_DT_ULT_CONS_CNBT, Pos.WADE_DT_ULT_CONS_CNBT);
        return buffer;
    }

    public void initWadeDtUltConsCnbtSpaces() {
        fill(Pos.WADE_DT_ULT_CONS_CNBT, Len.WADE_DT_ULT_CONS_CNBT, Types.SPACE_CHAR);
    }

    public void setWadeDtUltConsCnbtNull(String wadeDtUltConsCnbtNull) {
        writeString(Pos.WADE_DT_ULT_CONS_CNBT_NULL, wadeDtUltConsCnbtNull, Len.WADE_DT_ULT_CONS_CNBT_NULL);
    }

    /**Original name: WADE-DT-ULT-CONS-CNBT-NULL<br>*/
    public String getWadeDtUltConsCnbtNull() {
        return readString(Pos.WADE_DT_ULT_CONS_CNBT_NULL, Len.WADE_DT_ULT_CONS_CNBT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_DT_ULT_CONS_CNBT = 1;
        public static final int WADE_DT_ULT_CONS_CNBT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_DT_ULT_CONS_CNBT = 5;
        public static final int WADE_DT_ULT_CONS_CNBT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_DT_ULT_CONS_CNBT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
