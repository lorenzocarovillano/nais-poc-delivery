package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0001-TIPO-ENTE-CHIAMANTE<br>
 * Variable: IDSV0001-TIPO-ENTE-CHIAMANTE from copybook IDSV0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0001TipoEnteChiamante {

    //==== PROPERTIES ====
    public String value = DefaultValues.stringVal(Len.TIPO_ENTE_CHIAMANTE);
    public static final String AGENZIA = "1";
    public static final String SUB_AGENZIA = "2";
    public static final String DIREZIONE = "3";
    public static final String COLLOCATORE = "4";

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_ENTE_CHIAMANTE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
