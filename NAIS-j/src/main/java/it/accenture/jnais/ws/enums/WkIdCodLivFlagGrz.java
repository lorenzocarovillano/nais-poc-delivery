package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-ID-COD-LIV-FLAG-GRZ<br>
 * Variable: WK-ID-COD-LIV-FLAG-GRZ from program LVVS1890<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkIdCodLivFlagGrz {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO = 'S';
    public static final char NON_TROVATO = 'N';

    //==== METHODS ====
    public void setWkIdCodLivFlagGrz(char wkIdCodLivFlagGrz) {
        this.value = wkIdCodLivFlagGrz;
    }

    public char getWkIdCodLivFlagGrz() {
        return this.value;
    }

    public boolean isTrovato() {
        return value == TROVATO;
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public void setNonTrovato() {
        value = NON_TROVATO;
    }
}
