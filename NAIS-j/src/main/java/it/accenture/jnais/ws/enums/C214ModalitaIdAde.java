package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: C214-MODALITA-ID-ADE<br>
 * Variable: C214-MODALITA-ID-ADE from copybook IVVC0213<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class C214ModalitaIdAde {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char FITTZIO = 'F';
    public static final char AUTENTICO = 'A';

    //==== METHODS ====
    public void setModalitaIdAde(char modalitaIdAde) {
        this.value = modalitaIdAde;
    }

    public char getModalitaIdAde() {
        return this.value;
    }

    public boolean isIvvc0213IdAdeFittzio() {
        return value == FITTZIO;
    }

    public void setFittzio() {
        value = FITTZIO;
    }

    public boolean isIvvc0213IdAdeAutentico() {
        return value == AUTENTICO;
    }

    public void setAutentico() {
        value = AUTENTICO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MODALITA_ID_ADE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
