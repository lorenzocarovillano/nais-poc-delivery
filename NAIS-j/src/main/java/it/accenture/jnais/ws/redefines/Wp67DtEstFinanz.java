package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-DT-EST-FINANZ<br>
 * Variable: WP67-DT-EST-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67DtEstFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67DtEstFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_DT_EST_FINANZ;
    }

    public void setWp67DtEstFinanz(int wp67DtEstFinanz) {
        writeIntAsPacked(Pos.WP67_DT_EST_FINANZ, wp67DtEstFinanz, Len.Int.WP67_DT_EST_FINANZ);
    }

    public void setWp67DtEstFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_DT_EST_FINANZ, Pos.WP67_DT_EST_FINANZ);
    }

    /**Original name: WP67-DT-EST-FINANZ<br>*/
    public int getWp67DtEstFinanz() {
        return readPackedAsInt(Pos.WP67_DT_EST_FINANZ, Len.Int.WP67_DT_EST_FINANZ);
    }

    public byte[] getWp67DtEstFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_DT_EST_FINANZ, Pos.WP67_DT_EST_FINANZ);
        return buffer;
    }

    public void setWp67DtEstFinanzNull(String wp67DtEstFinanzNull) {
        writeString(Pos.WP67_DT_EST_FINANZ_NULL, wp67DtEstFinanzNull, Len.WP67_DT_EST_FINANZ_NULL);
    }

    /**Original name: WP67-DT-EST-FINANZ-NULL<br>*/
    public String getWp67DtEstFinanzNull() {
        return readString(Pos.WP67_DT_EST_FINANZ_NULL, Len.WP67_DT_EST_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_DT_EST_FINANZ = 1;
        public static final int WP67_DT_EST_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_DT_EST_FINANZ = 5;
        public static final int WP67_DT_EST_FINANZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_DT_EST_FINANZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
