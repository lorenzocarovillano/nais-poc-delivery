package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.redefines.WtgaTab;

/**Original name: WTGA-AREA-TRANCHE<br>
 * Variable: WTGA-AREA-TRANCHE from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WtgaAreaTrancheIvvs0211 {

    //==== PROPERTIES ====
    //Original name: WTGA-ELE-TRAN-MAX
    private short wtgaEleTranMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTGA-TAB
    private WtgaTab wtgaTab = new WtgaTab();

    //==== METHODS ====
    public void setWtgaAreaTrancheFormatted(String data) {
        byte[] buffer = new byte[Len.WTGA_AREA_TRANCHE];
        MarshalByte.writeString(buffer, 1, data, Len.WTGA_AREA_TRANCHE);
        setWtgaAreaTrancheBytes(buffer, 1);
    }

    public void setWtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        wtgaEleTranMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wtgaTab.setWtgaTabBytes(buffer, position);
    }

    public byte[] getWtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtgaEleTranMax);
        position += Types.SHORT_SIZE;
        wtgaTab.getWtgaTabBytes(buffer, position);
        return buffer;
    }

    public void setWtgaEleTranMax(short wtgaEleTranMax) {
        this.wtgaEleTranMax = wtgaEleTranMax;
    }

    public short getWtgaEleTranMax() {
        return this.wtgaEleTranMax;
    }

    public WtgaTab getWtgaTab() {
        return wtgaTab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ELE_TRAN_MAX = 2;
        public static final int WTGA_AREA_TRANCHE = WTGA_ELE_TRAN_MAX + WtgaTab.Len.WTGA_TAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
