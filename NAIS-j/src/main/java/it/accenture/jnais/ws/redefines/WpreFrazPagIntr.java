package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPRE-FRAZ-PAG-INTR<br>
 * Variable: WPRE-FRAZ-PAG-INTR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreFrazPagIntr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpreFrazPagIntr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_FRAZ_PAG_INTR;
    }

    public void setWpreFrazPagIntr(int wpreFrazPagIntr) {
        writeIntAsPacked(Pos.WPRE_FRAZ_PAG_INTR, wpreFrazPagIntr, Len.Int.WPRE_FRAZ_PAG_INTR);
    }

    public void setWpreFrazPagIntrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_FRAZ_PAG_INTR, Pos.WPRE_FRAZ_PAG_INTR);
    }

    /**Original name: WPRE-FRAZ-PAG-INTR<br>*/
    public int getWpreFrazPagIntr() {
        return readPackedAsInt(Pos.WPRE_FRAZ_PAG_INTR, Len.Int.WPRE_FRAZ_PAG_INTR);
    }

    public byte[] getWpreFrazPagIntrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_FRAZ_PAG_INTR, Pos.WPRE_FRAZ_PAG_INTR);
        return buffer;
    }

    public void initWpreFrazPagIntrSpaces() {
        fill(Pos.WPRE_FRAZ_PAG_INTR, Len.WPRE_FRAZ_PAG_INTR, Types.SPACE_CHAR);
    }

    public void setWpreFrazPagIntrNull(String wpreFrazPagIntrNull) {
        writeString(Pos.WPRE_FRAZ_PAG_INTR_NULL, wpreFrazPagIntrNull, Len.WPRE_FRAZ_PAG_INTR_NULL);
    }

    /**Original name: WPRE-FRAZ-PAG-INTR-NULL<br>*/
    public String getWpreFrazPagIntrNull() {
        return readString(Pos.WPRE_FRAZ_PAG_INTR_NULL, Len.WPRE_FRAZ_PAG_INTR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_FRAZ_PAG_INTR = 1;
        public static final int WPRE_FRAZ_PAG_INTR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_FRAZ_PAG_INTR = 3;
        public static final int WPRE_FRAZ_PAG_INTR_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_FRAZ_PAG_INTR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
