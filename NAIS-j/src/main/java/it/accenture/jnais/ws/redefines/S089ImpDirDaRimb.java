package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMP-DIR-DA-RIMB<br>
 * Variable: S089-IMP-DIR-DA-RIMB from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpDirDaRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpDirDaRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMP_DIR_DA_RIMB;
    }

    public void setWlquImpDirDaRimb(AfDecimal wlquImpDirDaRimb) {
        writeDecimalAsPacked(Pos.S089_IMP_DIR_DA_RIMB, wlquImpDirDaRimb.copy());
    }

    public void setWlquImpDirDaRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMP_DIR_DA_RIMB, Pos.S089_IMP_DIR_DA_RIMB);
    }

    /**Original name: WLQU-IMP-DIR-DA-RIMB<br>*/
    public AfDecimal getWlquImpDirDaRimb() {
        return readPackedAsDecimal(Pos.S089_IMP_DIR_DA_RIMB, Len.Int.WLQU_IMP_DIR_DA_RIMB, Len.Fract.WLQU_IMP_DIR_DA_RIMB);
    }

    public byte[] getWlquImpDirDaRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMP_DIR_DA_RIMB, Pos.S089_IMP_DIR_DA_RIMB);
        return buffer;
    }

    public void initWlquImpDirDaRimbSpaces() {
        fill(Pos.S089_IMP_DIR_DA_RIMB, Len.S089_IMP_DIR_DA_RIMB, Types.SPACE_CHAR);
    }

    public void setWlquImpDirDaRimbNull(String wlquImpDirDaRimbNull) {
        writeString(Pos.S089_IMP_DIR_DA_RIMB_NULL, wlquImpDirDaRimbNull, Len.WLQU_IMP_DIR_DA_RIMB_NULL);
    }

    /**Original name: WLQU-IMP-DIR-DA-RIMB-NULL<br>*/
    public String getWlquImpDirDaRimbNull() {
        return readString(Pos.S089_IMP_DIR_DA_RIMB_NULL, Len.WLQU_IMP_DIR_DA_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMP_DIR_DA_RIMB = 1;
        public static final int S089_IMP_DIR_DA_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMP_DIR_DA_RIMB = 8;
        public static final int WLQU_IMP_DIR_DA_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_DIR_DA_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_DIR_DA_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
