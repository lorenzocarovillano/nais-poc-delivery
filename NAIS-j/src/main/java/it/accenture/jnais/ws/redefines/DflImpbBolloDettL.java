package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-BOLLO-DETT-L<br>
 * Variable: DFL-IMPB-BOLLO-DETT-L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbBolloDettL extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbBolloDettL() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_BOLLO_DETT_L;
    }

    public void setDflImpbBolloDettL(AfDecimal dflImpbBolloDettL) {
        writeDecimalAsPacked(Pos.DFL_IMPB_BOLLO_DETT_L, dflImpbBolloDettL.copy());
    }

    public void setDflImpbBolloDettLFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_BOLLO_DETT_L, Pos.DFL_IMPB_BOLLO_DETT_L);
    }

    /**Original name: DFL-IMPB-BOLLO-DETT-L<br>*/
    public AfDecimal getDflImpbBolloDettL() {
        return readPackedAsDecimal(Pos.DFL_IMPB_BOLLO_DETT_L, Len.Int.DFL_IMPB_BOLLO_DETT_L, Len.Fract.DFL_IMPB_BOLLO_DETT_L);
    }

    public byte[] getDflImpbBolloDettLAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_BOLLO_DETT_L, Pos.DFL_IMPB_BOLLO_DETT_L);
        return buffer;
    }

    public void setDflImpbBolloDettLNull(String dflImpbBolloDettLNull) {
        writeString(Pos.DFL_IMPB_BOLLO_DETT_L_NULL, dflImpbBolloDettLNull, Len.DFL_IMPB_BOLLO_DETT_L_NULL);
    }

    /**Original name: DFL-IMPB-BOLLO-DETT-L-NULL<br>*/
    public String getDflImpbBolloDettLNull() {
        return readString(Pos.DFL_IMPB_BOLLO_DETT_L_NULL, Len.DFL_IMPB_BOLLO_DETT_L_NULL);
    }

    public String getDflImpbBolloDettLNullFormatted() {
        return Functions.padBlanks(getDflImpbBolloDettLNull(), Len.DFL_IMPB_BOLLO_DETT_L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_BOLLO_DETT_L = 1;
        public static final int DFL_IMPB_BOLLO_DETT_L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_BOLLO_DETT_L = 8;
        public static final int DFL_IMPB_BOLLO_DETT_L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_BOLLO_DETT_L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_BOLLO_DETT_L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
