package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-IMP-PA-ESP-MSC-2<br>
 * Variable: P56-IMP-PA-ESP-MSC-2 from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56ImpPaEspMsc2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56ImpPaEspMsc2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_IMP_PA_ESP_MSC2;
    }

    public void setP56ImpPaEspMsc2(AfDecimal p56ImpPaEspMsc2) {
        writeDecimalAsPacked(Pos.P56_IMP_PA_ESP_MSC2, p56ImpPaEspMsc2.copy());
    }

    public void setP56ImpPaEspMsc2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_IMP_PA_ESP_MSC2, Pos.P56_IMP_PA_ESP_MSC2);
    }

    /**Original name: P56-IMP-PA-ESP-MSC-2<br>*/
    public AfDecimal getP56ImpPaEspMsc2() {
        return readPackedAsDecimal(Pos.P56_IMP_PA_ESP_MSC2, Len.Int.P56_IMP_PA_ESP_MSC2, Len.Fract.P56_IMP_PA_ESP_MSC2);
    }

    public byte[] getP56ImpPaEspMsc2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_IMP_PA_ESP_MSC2, Pos.P56_IMP_PA_ESP_MSC2);
        return buffer;
    }

    public void setP56ImpPaEspMsc2Null(String p56ImpPaEspMsc2Null) {
        writeString(Pos.P56_IMP_PA_ESP_MSC2_NULL, p56ImpPaEspMsc2Null, Len.P56_IMP_PA_ESP_MSC2_NULL);
    }

    /**Original name: P56-IMP-PA-ESP-MSC-2-NULL<br>*/
    public String getP56ImpPaEspMsc2Null() {
        return readString(Pos.P56_IMP_PA_ESP_MSC2_NULL, Len.P56_IMP_PA_ESP_MSC2_NULL);
    }

    public String getP56ImpPaEspMsc2NullFormatted() {
        return Functions.padBlanks(getP56ImpPaEspMsc2Null(), Len.P56_IMP_PA_ESP_MSC2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_IMP_PA_ESP_MSC2 = 1;
        public static final int P56_IMP_PA_ESP_MSC2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_IMP_PA_ESP_MSC2 = 8;
        public static final int P56_IMP_PA_ESP_MSC2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_IMP_PA_ESP_MSC2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_IMP_PA_ESP_MSC2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
