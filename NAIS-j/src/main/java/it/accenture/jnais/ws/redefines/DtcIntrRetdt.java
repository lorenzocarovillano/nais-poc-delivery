package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-INTR-RETDT<br>
 * Variable: DTC-INTR-RETDT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcIntrRetdt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcIntrRetdt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_INTR_RETDT;
    }

    public void setDtcIntrRetdt(AfDecimal dtcIntrRetdt) {
        writeDecimalAsPacked(Pos.DTC_INTR_RETDT, dtcIntrRetdt.copy());
    }

    public void setDtcIntrRetdtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_INTR_RETDT, Pos.DTC_INTR_RETDT);
    }

    /**Original name: DTC-INTR-RETDT<br>*/
    public AfDecimal getDtcIntrRetdt() {
        return readPackedAsDecimal(Pos.DTC_INTR_RETDT, Len.Int.DTC_INTR_RETDT, Len.Fract.DTC_INTR_RETDT);
    }

    public byte[] getDtcIntrRetdtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_INTR_RETDT, Pos.DTC_INTR_RETDT);
        return buffer;
    }

    public void setDtcIntrRetdtNull(String dtcIntrRetdtNull) {
        writeString(Pos.DTC_INTR_RETDT_NULL, dtcIntrRetdtNull, Len.DTC_INTR_RETDT_NULL);
    }

    /**Original name: DTC-INTR-RETDT-NULL<br>*/
    public String getDtcIntrRetdtNull() {
        return readString(Pos.DTC_INTR_RETDT_NULL, Len.DTC_INTR_RETDT_NULL);
    }

    public String getDtcIntrRetdtNullFormatted() {
        return Functions.padBlanks(getDtcIntrRetdtNull(), Len.DTC_INTR_RETDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_INTR_RETDT = 1;
        public static final int DTC_INTR_RETDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_INTR_RETDT = 8;
        public static final int DTC_INTR_RETDT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_INTR_RETDT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_INTR_RETDT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
