package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DEQ-RISP-NUM<br>
 * Variable: DEQ-RISP-NUM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DeqRispNum extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DeqRispNum() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DEQ_RISP_NUM;
    }

    public void setDeqRispNum(int deqRispNum) {
        writeIntAsPacked(Pos.DEQ_RISP_NUM, deqRispNum, Len.Int.DEQ_RISP_NUM);
    }

    public void setDeqRispNumFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DEQ_RISP_NUM, Pos.DEQ_RISP_NUM);
    }

    /**Original name: DEQ-RISP-NUM<br>*/
    public int getDeqRispNum() {
        return readPackedAsInt(Pos.DEQ_RISP_NUM, Len.Int.DEQ_RISP_NUM);
    }

    public byte[] getDeqRispNumAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DEQ_RISP_NUM, Pos.DEQ_RISP_NUM);
        return buffer;
    }

    public void setDeqRispNumNull(String deqRispNumNull) {
        writeString(Pos.DEQ_RISP_NUM_NULL, deqRispNumNull, Len.DEQ_RISP_NUM_NULL);
    }

    /**Original name: DEQ-RISP-NUM-NULL<br>*/
    public String getDeqRispNumNull() {
        return readString(Pos.DEQ_RISP_NUM_NULL, Len.DEQ_RISP_NUM_NULL);
    }

    public String getDeqRispNumNullFormatted() {
        return Functions.padBlanks(getDeqRispNumNull(), Len.DEQ_RISP_NUM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DEQ_RISP_NUM = 1;
        public static final int DEQ_RISP_NUM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DEQ_RISP_NUM = 3;
        public static final int DEQ_RISP_NUM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DEQ_RISP_NUM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
