package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-RIS-RISTORNI-CAP<br>
 * Variable: B03-RIS-RISTORNI-CAP from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03RisRistorniCap extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03RisRistorniCap() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_RIS_RISTORNI_CAP;
    }

    public void setB03RisRistorniCap(AfDecimal b03RisRistorniCap) {
        writeDecimalAsPacked(Pos.B03_RIS_RISTORNI_CAP, b03RisRistorniCap.copy());
    }

    public void setB03RisRistorniCapFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_RIS_RISTORNI_CAP, Pos.B03_RIS_RISTORNI_CAP);
    }

    /**Original name: B03-RIS-RISTORNI-CAP<br>*/
    public AfDecimal getB03RisRistorniCap() {
        return readPackedAsDecimal(Pos.B03_RIS_RISTORNI_CAP, Len.Int.B03_RIS_RISTORNI_CAP, Len.Fract.B03_RIS_RISTORNI_CAP);
    }

    public byte[] getB03RisRistorniCapAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_RIS_RISTORNI_CAP, Pos.B03_RIS_RISTORNI_CAP);
        return buffer;
    }

    public void setB03RisRistorniCapNull(String b03RisRistorniCapNull) {
        writeString(Pos.B03_RIS_RISTORNI_CAP_NULL, b03RisRistorniCapNull, Len.B03_RIS_RISTORNI_CAP_NULL);
    }

    /**Original name: B03-RIS-RISTORNI-CAP-NULL<br>*/
    public String getB03RisRistorniCapNull() {
        return readString(Pos.B03_RIS_RISTORNI_CAP_NULL, Len.B03_RIS_RISTORNI_CAP_NULL);
    }

    public String getB03RisRistorniCapNullFormatted() {
        return Functions.padBlanks(getB03RisRistorniCapNull(), Len.B03_RIS_RISTORNI_CAP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_RIS_RISTORNI_CAP = 1;
        public static final int B03_RIS_RISTORNI_CAP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_RIS_RISTORNI_CAP = 8;
        public static final int B03_RIS_RISTORNI_CAP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_RIS_RISTORNI_CAP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_RIS_RISTORNI_CAP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
