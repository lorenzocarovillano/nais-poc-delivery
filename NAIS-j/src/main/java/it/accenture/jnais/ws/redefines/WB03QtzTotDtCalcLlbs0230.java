package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-QTZ-TOT-DT-CALC<br>
 * Variable: W-B03-QTZ-TOT-DT-CALC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03QtzTotDtCalcLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03QtzTotDtCalcLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_QTZ_TOT_DT_CALC;
    }

    public void setwB03QtzTotDtCalc(AfDecimal wB03QtzTotDtCalc) {
        writeDecimalAsPacked(Pos.W_B03_QTZ_TOT_DT_CALC, wB03QtzTotDtCalc.copy());
    }

    /**Original name: W-B03-QTZ-TOT-DT-CALC<br>*/
    public AfDecimal getwB03QtzTotDtCalc() {
        return readPackedAsDecimal(Pos.W_B03_QTZ_TOT_DT_CALC, Len.Int.W_B03_QTZ_TOT_DT_CALC, Len.Fract.W_B03_QTZ_TOT_DT_CALC);
    }

    public byte[] getwB03QtzTotDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_QTZ_TOT_DT_CALC, Pos.W_B03_QTZ_TOT_DT_CALC);
        return buffer;
    }

    public void setwB03QtzTotDtCalcNull(String wB03QtzTotDtCalcNull) {
        writeString(Pos.W_B03_QTZ_TOT_DT_CALC_NULL, wB03QtzTotDtCalcNull, Len.W_B03_QTZ_TOT_DT_CALC_NULL);
    }

    /**Original name: W-B03-QTZ-TOT-DT-CALC-NULL<br>*/
    public String getwB03QtzTotDtCalcNull() {
        return readString(Pos.W_B03_QTZ_TOT_DT_CALC_NULL, Len.W_B03_QTZ_TOT_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_QTZ_TOT_DT_CALC = 1;
        public static final int W_B03_QTZ_TOT_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_QTZ_TOT_DT_CALC = 7;
        public static final int W_B03_QTZ_TOT_DT_CALC_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_QTZ_TOT_DT_CALC = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_QTZ_TOT_DT_CALC = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
