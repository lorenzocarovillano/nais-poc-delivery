package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-IMP-ADER<br>
 * Variable: TIT-IMP-ADER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_IMP_ADER;
    }

    public void setTitImpAder(AfDecimal titImpAder) {
        writeDecimalAsPacked(Pos.TIT_IMP_ADER, titImpAder.copy());
    }

    public void setTitImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_IMP_ADER, Pos.TIT_IMP_ADER);
    }

    /**Original name: TIT-IMP-ADER<br>*/
    public AfDecimal getTitImpAder() {
        return readPackedAsDecimal(Pos.TIT_IMP_ADER, Len.Int.TIT_IMP_ADER, Len.Fract.TIT_IMP_ADER);
    }

    public byte[] getTitImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_IMP_ADER, Pos.TIT_IMP_ADER);
        return buffer;
    }

    public void setTitImpAderNull(String titImpAderNull) {
        writeString(Pos.TIT_IMP_ADER_NULL, titImpAderNull, Len.TIT_IMP_ADER_NULL);
    }

    /**Original name: TIT-IMP-ADER-NULL<br>*/
    public String getTitImpAderNull() {
        return readString(Pos.TIT_IMP_ADER_NULL, Len.TIT_IMP_ADER_NULL);
    }

    public String getTitImpAderNullFormatted() {
        return Functions.padBlanks(getTitImpAderNull(), Len.TIT_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_IMP_ADER = 1;
        public static final int TIT_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_IMP_ADER = 8;
        public static final int TIT_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
