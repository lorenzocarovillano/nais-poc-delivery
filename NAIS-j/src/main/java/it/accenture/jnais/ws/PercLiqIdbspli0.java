package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.PliDtVlt;
import it.accenture.jnais.ws.redefines.PliIdMoviChiu;
import it.accenture.jnais.ws.redefines.PliIdRappAna;
import it.accenture.jnais.ws.redefines.PliImpLiq;
import it.accenture.jnais.ws.redefines.PliPcLiq;

/**Original name: PERC-LIQ<br>
 * Variable: PERC-LIQ from copybook IDBVPLI1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class PercLiqIdbspli0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: PLI-ID-PERC-LIQ
    private int pliIdPercLiq = DefaultValues.INT_VAL;
    //Original name: PLI-ID-BNFICR-LIQ
    private int pliIdBnficrLiq = DefaultValues.INT_VAL;
    //Original name: PLI-ID-MOVI-CRZ
    private int pliIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: PLI-ID-MOVI-CHIU
    private PliIdMoviChiu pliIdMoviChiu = new PliIdMoviChiu();
    //Original name: PLI-ID-RAPP-ANA
    private PliIdRappAna pliIdRappAna = new PliIdRappAna();
    //Original name: PLI-DT-INI-EFF
    private int pliDtIniEff = DefaultValues.INT_VAL;
    //Original name: PLI-DT-END-EFF
    private int pliDtEndEff = DefaultValues.INT_VAL;
    //Original name: PLI-COD-COMP-ANIA
    private int pliCodCompAnia = DefaultValues.INT_VAL;
    //Original name: PLI-PC-LIQ
    private PliPcLiq pliPcLiq = new PliPcLiq();
    //Original name: PLI-IMP-LIQ
    private PliImpLiq pliImpLiq = new PliImpLiq();
    //Original name: PLI-TP-MEZ-PAG
    private String pliTpMezPag = DefaultValues.stringVal(Len.PLI_TP_MEZ_PAG);
    //Original name: PLI-ITER-PAG-AVV
    private char pliIterPagAvv = DefaultValues.CHAR_VAL;
    //Original name: PLI-DS-RIGA
    private long pliDsRiga = DefaultValues.LONG_VAL;
    //Original name: PLI-DS-OPER-SQL
    private char pliDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: PLI-DS-VER
    private int pliDsVer = DefaultValues.INT_VAL;
    //Original name: PLI-DS-TS-INI-CPTZ
    private long pliDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: PLI-DS-TS-END-CPTZ
    private long pliDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: PLI-DS-UTENTE
    private String pliDsUtente = DefaultValues.stringVal(Len.PLI_DS_UTENTE);
    //Original name: PLI-DS-STATO-ELAB
    private char pliDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: PLI-DT-VLT
    private PliDtVlt pliDtVlt = new PliDtVlt();
    //Original name: PLI-INT-CNT-CORR-ACCR-LEN
    private short pliIntCntCorrAccrLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: PLI-INT-CNT-CORR-ACCR
    private String pliIntCntCorrAccr = DefaultValues.stringVal(Len.PLI_INT_CNT_CORR_ACCR);
    //Original name: PLI-COD-IBAN-RIT-CON
    private String pliCodIbanRitCon = DefaultValues.stringVal(Len.PLI_COD_IBAN_RIT_CON);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PERC_LIQ;
    }

    @Override
    public void deserialize(byte[] buf) {
        setPercLiqBytes(buf);
    }

    public void setPercLiqFormatted(String data) {
        byte[] buffer = new byte[Len.PERC_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.PERC_LIQ);
        setPercLiqBytes(buffer, 1);
    }

    public String getPercLiqFormatted() {
        return MarshalByteExt.bufferToStr(getPercLiqBytes());
    }

    public void setPercLiqBytes(byte[] buffer) {
        setPercLiqBytes(buffer, 1);
    }

    public byte[] getPercLiqBytes() {
        byte[] buffer = new byte[Len.PERC_LIQ];
        return getPercLiqBytes(buffer, 1);
    }

    public void setPercLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        pliIdPercLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PLI_ID_PERC_LIQ, 0);
        position += Len.PLI_ID_PERC_LIQ;
        pliIdBnficrLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PLI_ID_BNFICR_LIQ, 0);
        position += Len.PLI_ID_BNFICR_LIQ;
        pliIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PLI_ID_MOVI_CRZ, 0);
        position += Len.PLI_ID_MOVI_CRZ;
        pliIdMoviChiu.setPliIdMoviChiuFromBuffer(buffer, position);
        position += PliIdMoviChiu.Len.PLI_ID_MOVI_CHIU;
        pliIdRappAna.setPliIdRappAnaFromBuffer(buffer, position);
        position += PliIdRappAna.Len.PLI_ID_RAPP_ANA;
        pliDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PLI_DT_INI_EFF, 0);
        position += Len.PLI_DT_INI_EFF;
        pliDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PLI_DT_END_EFF, 0);
        position += Len.PLI_DT_END_EFF;
        pliCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PLI_COD_COMP_ANIA, 0);
        position += Len.PLI_COD_COMP_ANIA;
        pliPcLiq.setPliPcLiqFromBuffer(buffer, position);
        position += PliPcLiq.Len.PLI_PC_LIQ;
        pliImpLiq.setPliImpLiqFromBuffer(buffer, position);
        position += PliImpLiq.Len.PLI_IMP_LIQ;
        pliTpMezPag = MarshalByte.readString(buffer, position, Len.PLI_TP_MEZ_PAG);
        position += Len.PLI_TP_MEZ_PAG;
        pliIterPagAvv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pliDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PLI_DS_RIGA, 0);
        position += Len.PLI_DS_RIGA;
        pliDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pliDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PLI_DS_VER, 0);
        position += Len.PLI_DS_VER;
        pliDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PLI_DS_TS_INI_CPTZ, 0);
        position += Len.PLI_DS_TS_INI_CPTZ;
        pliDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PLI_DS_TS_END_CPTZ, 0);
        position += Len.PLI_DS_TS_END_CPTZ;
        pliDsUtente = MarshalByte.readString(buffer, position, Len.PLI_DS_UTENTE);
        position += Len.PLI_DS_UTENTE;
        pliDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pliDtVlt.setPliDtVltFromBuffer(buffer, position);
        position += PliDtVlt.Len.PLI_DT_VLT;
        setPliIntCntCorrAccrVcharBytes(buffer, position);
        position += Len.PLI_INT_CNT_CORR_ACCR_VCHAR;
        pliCodIbanRitCon = MarshalByte.readString(buffer, position, Len.PLI_COD_IBAN_RIT_CON);
    }

    public byte[] getPercLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, pliIdPercLiq, Len.Int.PLI_ID_PERC_LIQ, 0);
        position += Len.PLI_ID_PERC_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, pliIdBnficrLiq, Len.Int.PLI_ID_BNFICR_LIQ, 0);
        position += Len.PLI_ID_BNFICR_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, pliIdMoviCrz, Len.Int.PLI_ID_MOVI_CRZ, 0);
        position += Len.PLI_ID_MOVI_CRZ;
        pliIdMoviChiu.getPliIdMoviChiuAsBuffer(buffer, position);
        position += PliIdMoviChiu.Len.PLI_ID_MOVI_CHIU;
        pliIdRappAna.getPliIdRappAnaAsBuffer(buffer, position);
        position += PliIdRappAna.Len.PLI_ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, pliDtIniEff, Len.Int.PLI_DT_INI_EFF, 0);
        position += Len.PLI_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, pliDtEndEff, Len.Int.PLI_DT_END_EFF, 0);
        position += Len.PLI_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, pliCodCompAnia, Len.Int.PLI_COD_COMP_ANIA, 0);
        position += Len.PLI_COD_COMP_ANIA;
        pliPcLiq.getPliPcLiqAsBuffer(buffer, position);
        position += PliPcLiq.Len.PLI_PC_LIQ;
        pliImpLiq.getPliImpLiqAsBuffer(buffer, position);
        position += PliImpLiq.Len.PLI_IMP_LIQ;
        MarshalByte.writeString(buffer, position, pliTpMezPag, Len.PLI_TP_MEZ_PAG);
        position += Len.PLI_TP_MEZ_PAG;
        MarshalByte.writeChar(buffer, position, pliIterPagAvv);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, pliDsRiga, Len.Int.PLI_DS_RIGA, 0);
        position += Len.PLI_DS_RIGA;
        MarshalByte.writeChar(buffer, position, pliDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, pliDsVer, Len.Int.PLI_DS_VER, 0);
        position += Len.PLI_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, pliDsTsIniCptz, Len.Int.PLI_DS_TS_INI_CPTZ, 0);
        position += Len.PLI_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, pliDsTsEndCptz, Len.Int.PLI_DS_TS_END_CPTZ, 0);
        position += Len.PLI_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, pliDsUtente, Len.PLI_DS_UTENTE);
        position += Len.PLI_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, pliDsStatoElab);
        position += Types.CHAR_SIZE;
        pliDtVlt.getPliDtVltAsBuffer(buffer, position);
        position += PliDtVlt.Len.PLI_DT_VLT;
        getPliIntCntCorrAccrVcharBytes(buffer, position);
        position += Len.PLI_INT_CNT_CORR_ACCR_VCHAR;
        MarshalByte.writeString(buffer, position, pliCodIbanRitCon, Len.PLI_COD_IBAN_RIT_CON);
        return buffer;
    }

    public void setPliIdPercLiq(int pliIdPercLiq) {
        this.pliIdPercLiq = pliIdPercLiq;
    }

    public int getPliIdPercLiq() {
        return this.pliIdPercLiq;
    }

    public void setPliIdBnficrLiq(int pliIdBnficrLiq) {
        this.pliIdBnficrLiq = pliIdBnficrLiq;
    }

    public int getPliIdBnficrLiq() {
        return this.pliIdBnficrLiq;
    }

    public void setPliIdMoviCrz(int pliIdMoviCrz) {
        this.pliIdMoviCrz = pliIdMoviCrz;
    }

    public int getPliIdMoviCrz() {
        return this.pliIdMoviCrz;
    }

    public void setPliDtIniEff(int pliDtIniEff) {
        this.pliDtIniEff = pliDtIniEff;
    }

    public int getPliDtIniEff() {
        return this.pliDtIniEff;
    }

    public void setPliDtEndEff(int pliDtEndEff) {
        this.pliDtEndEff = pliDtEndEff;
    }

    public int getPliDtEndEff() {
        return this.pliDtEndEff;
    }

    public void setPliCodCompAnia(int pliCodCompAnia) {
        this.pliCodCompAnia = pliCodCompAnia;
    }

    public int getPliCodCompAnia() {
        return this.pliCodCompAnia;
    }

    public void setPliTpMezPag(String pliTpMezPag) {
        this.pliTpMezPag = Functions.subString(pliTpMezPag, Len.PLI_TP_MEZ_PAG);
    }

    public String getPliTpMezPag() {
        return this.pliTpMezPag;
    }

    public String getPliTpMezPagFormatted() {
        return Functions.padBlanks(getPliTpMezPag(), Len.PLI_TP_MEZ_PAG);
    }

    public void setPliIterPagAvv(char pliIterPagAvv) {
        this.pliIterPagAvv = pliIterPagAvv;
    }

    public char getPliIterPagAvv() {
        return this.pliIterPagAvv;
    }

    public void setPliDsRiga(long pliDsRiga) {
        this.pliDsRiga = pliDsRiga;
    }

    public long getPliDsRiga() {
        return this.pliDsRiga;
    }

    public void setPliDsOperSql(char pliDsOperSql) {
        this.pliDsOperSql = pliDsOperSql;
    }

    public void setPliDsOperSqlFormatted(String pliDsOperSql) {
        setPliDsOperSql(Functions.charAt(pliDsOperSql, Types.CHAR_SIZE));
    }

    public char getPliDsOperSql() {
        return this.pliDsOperSql;
    }

    public void setPliDsVer(int pliDsVer) {
        this.pliDsVer = pliDsVer;
    }

    public int getPliDsVer() {
        return this.pliDsVer;
    }

    public void setPliDsTsIniCptz(long pliDsTsIniCptz) {
        this.pliDsTsIniCptz = pliDsTsIniCptz;
    }

    public long getPliDsTsIniCptz() {
        return this.pliDsTsIniCptz;
    }

    public void setPliDsTsEndCptz(long pliDsTsEndCptz) {
        this.pliDsTsEndCptz = pliDsTsEndCptz;
    }

    public long getPliDsTsEndCptz() {
        return this.pliDsTsEndCptz;
    }

    public void setPliDsUtente(String pliDsUtente) {
        this.pliDsUtente = Functions.subString(pliDsUtente, Len.PLI_DS_UTENTE);
    }

    public String getPliDsUtente() {
        return this.pliDsUtente;
    }

    public void setPliDsStatoElab(char pliDsStatoElab) {
        this.pliDsStatoElab = pliDsStatoElab;
    }

    public void setPliDsStatoElabFormatted(String pliDsStatoElab) {
        setPliDsStatoElab(Functions.charAt(pliDsStatoElab, Types.CHAR_SIZE));
    }

    public char getPliDsStatoElab() {
        return this.pliDsStatoElab;
    }

    public void setPliIntCntCorrAccrVcharFormatted(String data) {
        byte[] buffer = new byte[Len.PLI_INT_CNT_CORR_ACCR_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.PLI_INT_CNT_CORR_ACCR_VCHAR);
        setPliIntCntCorrAccrVcharBytes(buffer, 1);
    }

    public String getPliIntCntCorrAccrVcharFormatted() {
        return MarshalByteExt.bufferToStr(getPliIntCntCorrAccrVcharBytes());
    }

    /**Original name: PLI-INT-CNT-CORR-ACCR-VCHAR<br>*/
    public byte[] getPliIntCntCorrAccrVcharBytes() {
        byte[] buffer = new byte[Len.PLI_INT_CNT_CORR_ACCR_VCHAR];
        return getPliIntCntCorrAccrVcharBytes(buffer, 1);
    }

    public void setPliIntCntCorrAccrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        pliIntCntCorrAccrLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        pliIntCntCorrAccr = MarshalByte.readString(buffer, position, Len.PLI_INT_CNT_CORR_ACCR);
    }

    public byte[] getPliIntCntCorrAccrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, pliIntCntCorrAccrLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, pliIntCntCorrAccr, Len.PLI_INT_CNT_CORR_ACCR);
        return buffer;
    }

    public void setPliIntCntCorrAccrLen(short pliIntCntCorrAccrLen) {
        this.pliIntCntCorrAccrLen = pliIntCntCorrAccrLen;
    }

    public short getPliIntCntCorrAccrLen() {
        return this.pliIntCntCorrAccrLen;
    }

    public void setPliIntCntCorrAccr(String pliIntCntCorrAccr) {
        this.pliIntCntCorrAccr = Functions.subString(pliIntCntCorrAccr, Len.PLI_INT_CNT_CORR_ACCR);
    }

    public String getPliIntCntCorrAccr() {
        return this.pliIntCntCorrAccr;
    }

    public void setPliCodIbanRitCon(String pliCodIbanRitCon) {
        this.pliCodIbanRitCon = Functions.subString(pliCodIbanRitCon, Len.PLI_COD_IBAN_RIT_CON);
    }

    public String getPliCodIbanRitCon() {
        return this.pliCodIbanRitCon;
    }

    public PliDtVlt getPliDtVlt() {
        return pliDtVlt;
    }

    public PliIdMoviChiu getPliIdMoviChiu() {
        return pliIdMoviChiu;
    }

    public PliIdRappAna getPliIdRappAna() {
        return pliIdRappAna;
    }

    public PliImpLiq getPliImpLiq() {
        return pliImpLiq;
    }

    public PliPcLiq getPliPcLiq() {
        return pliPcLiq;
    }

    @Override
    public byte[] serialize() {
        return getPercLiqBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PLI_ID_PERC_LIQ = 5;
        public static final int PLI_ID_BNFICR_LIQ = 5;
        public static final int PLI_ID_MOVI_CRZ = 5;
        public static final int PLI_DT_INI_EFF = 5;
        public static final int PLI_DT_END_EFF = 5;
        public static final int PLI_COD_COMP_ANIA = 3;
        public static final int PLI_TP_MEZ_PAG = 2;
        public static final int PLI_ITER_PAG_AVV = 1;
        public static final int PLI_DS_RIGA = 6;
        public static final int PLI_DS_OPER_SQL = 1;
        public static final int PLI_DS_VER = 5;
        public static final int PLI_DS_TS_INI_CPTZ = 10;
        public static final int PLI_DS_TS_END_CPTZ = 10;
        public static final int PLI_DS_UTENTE = 20;
        public static final int PLI_DS_STATO_ELAB = 1;
        public static final int PLI_INT_CNT_CORR_ACCR_LEN = 2;
        public static final int PLI_INT_CNT_CORR_ACCR = 100;
        public static final int PLI_INT_CNT_CORR_ACCR_VCHAR = PLI_INT_CNT_CORR_ACCR_LEN + PLI_INT_CNT_CORR_ACCR;
        public static final int PLI_COD_IBAN_RIT_CON = 34;
        public static final int PERC_LIQ = PLI_ID_PERC_LIQ + PLI_ID_BNFICR_LIQ + PLI_ID_MOVI_CRZ + PliIdMoviChiu.Len.PLI_ID_MOVI_CHIU + PliIdRappAna.Len.PLI_ID_RAPP_ANA + PLI_DT_INI_EFF + PLI_DT_END_EFF + PLI_COD_COMP_ANIA + PliPcLiq.Len.PLI_PC_LIQ + PliImpLiq.Len.PLI_IMP_LIQ + PLI_TP_MEZ_PAG + PLI_ITER_PAG_AVV + PLI_DS_RIGA + PLI_DS_OPER_SQL + PLI_DS_VER + PLI_DS_TS_INI_CPTZ + PLI_DS_TS_END_CPTZ + PLI_DS_UTENTE + PLI_DS_STATO_ELAB + PliDtVlt.Len.PLI_DT_VLT + PLI_INT_CNT_CORR_ACCR_VCHAR + PLI_COD_IBAN_RIT_CON;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PLI_ID_PERC_LIQ = 9;
            public static final int PLI_ID_BNFICR_LIQ = 9;
            public static final int PLI_ID_MOVI_CRZ = 9;
            public static final int PLI_DT_INI_EFF = 8;
            public static final int PLI_DT_END_EFF = 8;
            public static final int PLI_COD_COMP_ANIA = 5;
            public static final int PLI_DS_RIGA = 10;
            public static final int PLI_DS_VER = 9;
            public static final int PLI_DS_TS_INI_CPTZ = 18;
            public static final int PLI_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
