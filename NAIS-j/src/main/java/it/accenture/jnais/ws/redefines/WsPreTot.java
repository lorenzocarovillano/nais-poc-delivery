package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-PRE-TOT<br>
 * Variable: WS-PRE-TOT from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsPreTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsPreTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_PRE_TOT;
    }

    public void setWsPreTot(AfDecimal wsPreTot) {
        writeDecimalAsPacked(Pos.WS_PRE_TOT, wsPreTot.copy());
    }

    /**Original name: WS-PRE-TOT<br>
	 * <pre>-- PREMIO TOTALE (DETTAGLIO TITOLO CONTABILE)</pre>*/
    public AfDecimal getWsPreTot() {
        return readPackedAsDecimal(Pos.WS_PRE_TOT, Len.Int.WS_PRE_TOT, Len.Fract.WS_PRE_TOT);
    }

    public void setWsPreTotNull(String wsPreTotNull) {
        writeString(Pos.WS_PRE_TOT_NULL, wsPreTotNull, Len.WS_PRE_TOT_NULL);
    }

    /**Original name: WS-PRE-TOT-NULL<br>*/
    public String getWsPreTotNull() {
        return readString(Pos.WS_PRE_TOT_NULL, Len.WS_PRE_TOT_NULL);
    }

    public String getWsPreTotNullFormatted() {
        return Functions.padBlanks(getWsPreTotNull(), Len.WS_PRE_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_PRE_TOT = 1;
        public static final int WS_PRE_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_PRE_TOT = 8;
        public static final int WS_PRE_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WS_PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WS_PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
