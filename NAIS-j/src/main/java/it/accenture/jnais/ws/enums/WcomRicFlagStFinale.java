package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WCOM-RIC-FLAG-ST-FINALE<br>
 * Variable: WCOM-RIC-FLAG-ST-FINALE from copybook LCCC0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomRicFlagStFinale {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char FINALE_CON_EFF = '1';
    public static final char FINALE_SENZA_EFF = '2';
    public static final char NON_FINALE = '3';

    //==== METHODS ====
    public void setRicFlagStFinale(char ricFlagStFinale) {
        this.value = ricFlagStFinale;
    }

    public char getRicFlagStFinale() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RIC_FLAG_ST_FINALE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
