package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ESTR-ASS-MIN70A<br>
 * Variable: WPCO-DT-ESTR-ASS-MIN70A from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtEstrAssMin70a extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtEstrAssMin70a() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ESTR_ASS_MIN70A;
    }

    public void setWpcoDtEstrAssMin70a(int wpcoDtEstrAssMin70a) {
        writeIntAsPacked(Pos.WPCO_DT_ESTR_ASS_MIN70A, wpcoDtEstrAssMin70a, Len.Int.WPCO_DT_ESTR_ASS_MIN70A);
    }

    public void setDpcoDtEstrAssMin70aFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ESTR_ASS_MIN70A, Pos.WPCO_DT_ESTR_ASS_MIN70A);
    }

    /**Original name: WPCO-DT-ESTR-ASS-MIN70A<br>*/
    public int getWpcoDtEstrAssMin70a() {
        return readPackedAsInt(Pos.WPCO_DT_ESTR_ASS_MIN70A, Len.Int.WPCO_DT_ESTR_ASS_MIN70A);
    }

    public byte[] getWpcoDtEstrAssMin70aAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ESTR_ASS_MIN70A, Pos.WPCO_DT_ESTR_ASS_MIN70A);
        return buffer;
    }

    public void setWpcoDtEstrAssMin70aNull(String wpcoDtEstrAssMin70aNull) {
        writeString(Pos.WPCO_DT_ESTR_ASS_MIN70A_NULL, wpcoDtEstrAssMin70aNull, Len.WPCO_DT_ESTR_ASS_MIN70A_NULL);
    }

    /**Original name: WPCO-DT-ESTR-ASS-MIN70A-NULL<br>*/
    public String getWpcoDtEstrAssMin70aNull() {
        return readString(Pos.WPCO_DT_ESTR_ASS_MIN70A_NULL, Len.WPCO_DT_ESTR_ASS_MIN70A_NULL);
    }

    public String getWpcoDtEstrAssMin70aNullFormatted() {
        return Functions.padBlanks(getWpcoDtEstrAssMin70aNull(), Len.WPCO_DT_ESTR_ASS_MIN70A_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ESTR_ASS_MIN70A = 1;
        public static final int WPCO_DT_ESTR_ASS_MIN70A_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ESTR_ASS_MIN70A = 5;
        public static final int WPCO_DT_ESTR_ASS_MIN70A_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ESTR_ASS_MIN70A = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
