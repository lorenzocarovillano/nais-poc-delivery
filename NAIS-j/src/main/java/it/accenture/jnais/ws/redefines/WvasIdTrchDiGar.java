package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WVAS-ID-TRCH-DI-GAR<br>
 * Variable: WVAS-ID-TRCH-DI-GAR from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasIdTrchDiGar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasIdTrchDiGar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_ID_TRCH_DI_GAR;
    }

    public void setWvasIdTrchDiGar(int wvasIdTrchDiGar) {
        writeIntAsPacked(Pos.WVAS_ID_TRCH_DI_GAR, wvasIdTrchDiGar, Len.Int.WVAS_ID_TRCH_DI_GAR);
    }

    /**Original name: WVAS-ID-TRCH-DI-GAR<br>*/
    public int getWvasIdTrchDiGar() {
        return readPackedAsInt(Pos.WVAS_ID_TRCH_DI_GAR, Len.Int.WVAS_ID_TRCH_DI_GAR);
    }

    public void setWvasIdTrchDiGarNull(String wvasIdTrchDiGarNull) {
        writeString(Pos.WVAS_ID_TRCH_DI_GAR_NULL, wvasIdTrchDiGarNull, Len.WVAS_ID_TRCH_DI_GAR_NULL);
    }

    /**Original name: WVAS-ID-TRCH-DI-GAR-NULL<br>*/
    public String getWvasIdTrchDiGarNull() {
        return readString(Pos.WVAS_ID_TRCH_DI_GAR_NULL, Len.WVAS_ID_TRCH_DI_GAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_ID_TRCH_DI_GAR = 1;
        public static final int WVAS_ID_TRCH_DI_GAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_ID_TRCH_DI_GAR = 5;
        public static final int WVAS_ID_TRCH_DI_GAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_ID_TRCH_DI_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
