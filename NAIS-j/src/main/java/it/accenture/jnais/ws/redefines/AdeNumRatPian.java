package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-NUM-RAT-PIAN<br>
 * Variable: ADE-NUM-RAT-PIAN from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeNumRatPian extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeNumRatPian() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_NUM_RAT_PIAN;
    }

    public void setAdeNumRatPian(AfDecimal adeNumRatPian) {
        writeDecimalAsPacked(Pos.ADE_NUM_RAT_PIAN, adeNumRatPian.copy());
    }

    public void setAdeNumRatPianFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_NUM_RAT_PIAN, Pos.ADE_NUM_RAT_PIAN);
    }

    /**Original name: ADE-NUM-RAT-PIAN<br>*/
    public AfDecimal getAdeNumRatPian() {
        return readPackedAsDecimal(Pos.ADE_NUM_RAT_PIAN, Len.Int.ADE_NUM_RAT_PIAN, Len.Fract.ADE_NUM_RAT_PIAN);
    }

    public byte[] getAdeNumRatPianAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_NUM_RAT_PIAN, Pos.ADE_NUM_RAT_PIAN);
        return buffer;
    }

    public void setAdeNumRatPianNull(String adeNumRatPianNull) {
        writeString(Pos.ADE_NUM_RAT_PIAN_NULL, adeNumRatPianNull, Len.ADE_NUM_RAT_PIAN_NULL);
    }

    /**Original name: ADE-NUM-RAT-PIAN-NULL<br>*/
    public String getAdeNumRatPianNull() {
        return readString(Pos.ADE_NUM_RAT_PIAN_NULL, Len.ADE_NUM_RAT_PIAN_NULL);
    }

    public String getAdeNumRatPianNullFormatted() {
        return Functions.padBlanks(getAdeNumRatPianNull(), Len.ADE_NUM_RAT_PIAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_NUM_RAT_PIAN = 1;
        public static final int ADE_NUM_RAT_PIAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_NUM_RAT_PIAN = 7;
        public static final int ADE_NUM_RAT_PIAN_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_NUM_RAT_PIAN = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_NUM_RAT_PIAN = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
