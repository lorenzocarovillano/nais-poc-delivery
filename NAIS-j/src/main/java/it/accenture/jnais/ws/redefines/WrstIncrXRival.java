package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-INCR-X-RIVAL<br>
 * Variable: WRST-INCR-X-RIVAL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstIncrXRival extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstIncrXRival() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_INCR_X_RIVAL;
    }

    public void setWrstIncrXRival(AfDecimal wrstIncrXRival) {
        writeDecimalAsPacked(Pos.WRST_INCR_X_RIVAL, wrstIncrXRival.copy());
    }

    public void setWrstIncrXRivalFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_INCR_X_RIVAL, Pos.WRST_INCR_X_RIVAL);
    }

    /**Original name: WRST-INCR-X-RIVAL<br>*/
    public AfDecimal getWrstIncrXRival() {
        return readPackedAsDecimal(Pos.WRST_INCR_X_RIVAL, Len.Int.WRST_INCR_X_RIVAL, Len.Fract.WRST_INCR_X_RIVAL);
    }

    public byte[] getWrstIncrXRivalAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_INCR_X_RIVAL, Pos.WRST_INCR_X_RIVAL);
        return buffer;
    }

    public void initWrstIncrXRivalSpaces() {
        fill(Pos.WRST_INCR_X_RIVAL, Len.WRST_INCR_X_RIVAL, Types.SPACE_CHAR);
    }

    public void setWrstIncrXRivalNull(String wrstIncrXRivalNull) {
        writeString(Pos.WRST_INCR_X_RIVAL_NULL, wrstIncrXRivalNull, Len.WRST_INCR_X_RIVAL_NULL);
    }

    /**Original name: WRST-INCR-X-RIVAL-NULL<br>*/
    public String getWrstIncrXRivalNull() {
        return readString(Pos.WRST_INCR_X_RIVAL_NULL, Len.WRST_INCR_X_RIVAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_INCR_X_RIVAL = 1;
        public static final int WRST_INCR_X_RIVAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_INCR_X_RIVAL = 8;
        public static final int WRST_INCR_X_RIVAL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_INCR_X_RIVAL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_INCR_X_RIVAL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
