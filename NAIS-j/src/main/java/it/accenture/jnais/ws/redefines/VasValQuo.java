package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-VAL-QUO<br>
 * Variable: VAS-VAL-QUO from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasValQuo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasValQuo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_VAL_QUO;
    }

    public void setVasValQuo(AfDecimal vasValQuo) {
        writeDecimalAsPacked(Pos.VAS_VAL_QUO, vasValQuo.copy());
    }

    public void setVasValQuoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_VAL_QUO, Pos.VAS_VAL_QUO);
    }

    /**Original name: VAS-VAL-QUO<br>*/
    public AfDecimal getVasValQuo() {
        return readPackedAsDecimal(Pos.VAS_VAL_QUO, Len.Int.VAS_VAL_QUO, Len.Fract.VAS_VAL_QUO);
    }

    public byte[] getVasValQuoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_VAL_QUO, Pos.VAS_VAL_QUO);
        return buffer;
    }

    public void setVasValQuoNull(String vasValQuoNull) {
        writeString(Pos.VAS_VAL_QUO_NULL, vasValQuoNull, Len.VAS_VAL_QUO_NULL);
    }

    /**Original name: VAS-VAL-QUO-NULL<br>*/
    public String getVasValQuoNull() {
        return readString(Pos.VAS_VAL_QUO_NULL, Len.VAS_VAL_QUO_NULL);
    }

    public String getVasValQuoNullFormatted() {
        return Functions.padBlanks(getVasValQuoNull(), Len.VAS_VAL_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_VAL_QUO = 1;
        public static final int VAS_VAL_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_VAL_QUO = 7;
        public static final int VAS_VAL_QUO_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAS_VAL_QUO = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_VAL_QUO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
