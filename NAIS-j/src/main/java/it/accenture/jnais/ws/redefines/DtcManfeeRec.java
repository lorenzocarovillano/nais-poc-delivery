package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-MANFEE-REC<br>
 * Variable: DTC-MANFEE-REC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcManfeeRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcManfeeRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_MANFEE_REC;
    }

    public void setDtcManfeeRec(AfDecimal dtcManfeeRec) {
        writeDecimalAsPacked(Pos.DTC_MANFEE_REC, dtcManfeeRec.copy());
    }

    public void setDtcManfeeRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_MANFEE_REC, Pos.DTC_MANFEE_REC);
    }

    /**Original name: DTC-MANFEE-REC<br>*/
    public AfDecimal getDtcManfeeRec() {
        return readPackedAsDecimal(Pos.DTC_MANFEE_REC, Len.Int.DTC_MANFEE_REC, Len.Fract.DTC_MANFEE_REC);
    }

    public byte[] getDtcManfeeRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_MANFEE_REC, Pos.DTC_MANFEE_REC);
        return buffer;
    }

    public void setDtcManfeeRecNull(String dtcManfeeRecNull) {
        writeString(Pos.DTC_MANFEE_REC_NULL, dtcManfeeRecNull, Len.DTC_MANFEE_REC_NULL);
    }

    /**Original name: DTC-MANFEE-REC-NULL<br>*/
    public String getDtcManfeeRecNull() {
        return readString(Pos.DTC_MANFEE_REC_NULL, Len.DTC_MANFEE_REC_NULL);
    }

    public String getDtcManfeeRecNullFormatted() {
        return Functions.padBlanks(getDtcManfeeRecNull(), Len.DTC_MANFEE_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_MANFEE_REC = 1;
        public static final int DTC_MANFEE_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_MANFEE_REC = 8;
        public static final int DTC_MANFEE_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_MANFEE_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_MANFEE_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
