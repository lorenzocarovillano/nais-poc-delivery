package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCO-PC-SCON<br>
 * Variable: DCO-PC-SCON from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DcoPcScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DcoPcScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DCO_PC_SCON;
    }

    public void setDcoPcScon(AfDecimal dcoPcScon) {
        writeDecimalAsPacked(Pos.DCO_PC_SCON, dcoPcScon.copy());
    }

    public void setDcoPcSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DCO_PC_SCON, Pos.DCO_PC_SCON);
    }

    /**Original name: DCO-PC-SCON<br>*/
    public AfDecimal getDcoPcScon() {
        return readPackedAsDecimal(Pos.DCO_PC_SCON, Len.Int.DCO_PC_SCON, Len.Fract.DCO_PC_SCON);
    }

    public byte[] getDcoPcSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DCO_PC_SCON, Pos.DCO_PC_SCON);
        return buffer;
    }

    public void setDcoPcSconNull(String dcoPcSconNull) {
        writeString(Pos.DCO_PC_SCON_NULL, dcoPcSconNull, Len.DCO_PC_SCON_NULL);
    }

    /**Original name: DCO-PC-SCON-NULL<br>*/
    public String getDcoPcSconNull() {
        return readString(Pos.DCO_PC_SCON_NULL, Len.DCO_PC_SCON_NULL);
    }

    public String getDcoPcSconNullFormatted() {
        return Functions.padBlanks(getDcoPcSconNull(), Len.DCO_PC_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DCO_PC_SCON = 1;
        public static final int DCO_PC_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_PC_SCON = 4;
        public static final int DCO_PC_SCON_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_PC_SCON = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DCO_PC_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
