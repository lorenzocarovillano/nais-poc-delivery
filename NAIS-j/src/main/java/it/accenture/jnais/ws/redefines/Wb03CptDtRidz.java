package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CPT-DT-RIDZ<br>
 * Variable: WB03-CPT-DT-RIDZ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CptDtRidz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CptDtRidz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CPT_DT_RIDZ;
    }

    public void setWb03CptDtRidzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CPT_DT_RIDZ, Pos.WB03_CPT_DT_RIDZ);
    }

    /**Original name: WB03-CPT-DT-RIDZ<br>*/
    public AfDecimal getWb03CptDtRidz() {
        return readPackedAsDecimal(Pos.WB03_CPT_DT_RIDZ, Len.Int.WB03_CPT_DT_RIDZ, Len.Fract.WB03_CPT_DT_RIDZ);
    }

    public byte[] getWb03CptDtRidzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CPT_DT_RIDZ, Pos.WB03_CPT_DT_RIDZ);
        return buffer;
    }

    public void setWb03CptDtRidzNull(String wb03CptDtRidzNull) {
        writeString(Pos.WB03_CPT_DT_RIDZ_NULL, wb03CptDtRidzNull, Len.WB03_CPT_DT_RIDZ_NULL);
    }

    /**Original name: WB03-CPT-DT-RIDZ-NULL<br>*/
    public String getWb03CptDtRidzNull() {
        return readString(Pos.WB03_CPT_DT_RIDZ_NULL, Len.WB03_CPT_DT_RIDZ_NULL);
    }

    public String getWb03CptDtRidzNullFormatted() {
        return Functions.padBlanks(getWb03CptDtRidzNull(), Len.WB03_CPT_DT_RIDZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CPT_DT_RIDZ = 1;
        public static final int WB03_CPT_DT_RIDZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CPT_DT_RIDZ = 8;
        public static final int WB03_CPT_DT_RIDZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CPT_DT_RIDZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CPT_DT_RIDZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
