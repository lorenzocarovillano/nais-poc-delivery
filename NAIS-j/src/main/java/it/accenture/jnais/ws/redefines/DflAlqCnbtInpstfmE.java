package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ALQ-CNBT-INPSTFM-E<br>
 * Variable: DFL-ALQ-CNBT-INPSTFM-E from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAlqCnbtInpstfmE extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAlqCnbtInpstfmE() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ALQ_CNBT_INPSTFM_E;
    }

    public void setDflAlqCnbtInpstfmE(AfDecimal dflAlqCnbtInpstfmE) {
        writeDecimalAsPacked(Pos.DFL_ALQ_CNBT_INPSTFM_E, dflAlqCnbtInpstfmE.copy());
    }

    public void setDflAlqCnbtInpstfmEFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ALQ_CNBT_INPSTFM_E, Pos.DFL_ALQ_CNBT_INPSTFM_E);
    }

    /**Original name: DFL-ALQ-CNBT-INPSTFM-E<br>*/
    public AfDecimal getDflAlqCnbtInpstfmE() {
        return readPackedAsDecimal(Pos.DFL_ALQ_CNBT_INPSTFM_E, Len.Int.DFL_ALQ_CNBT_INPSTFM_E, Len.Fract.DFL_ALQ_CNBT_INPSTFM_E);
    }

    public byte[] getDflAlqCnbtInpstfmEAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ALQ_CNBT_INPSTFM_E, Pos.DFL_ALQ_CNBT_INPSTFM_E);
        return buffer;
    }

    public void setDflAlqCnbtInpstfmENull(String dflAlqCnbtInpstfmENull) {
        writeString(Pos.DFL_ALQ_CNBT_INPSTFM_E_NULL, dflAlqCnbtInpstfmENull, Len.DFL_ALQ_CNBT_INPSTFM_E_NULL);
    }

    /**Original name: DFL-ALQ-CNBT-INPSTFM-E-NULL<br>*/
    public String getDflAlqCnbtInpstfmENull() {
        return readString(Pos.DFL_ALQ_CNBT_INPSTFM_E_NULL, Len.DFL_ALQ_CNBT_INPSTFM_E_NULL);
    }

    public String getDflAlqCnbtInpstfmENullFormatted() {
        return Functions.padBlanks(getDflAlqCnbtInpstfmENull(), Len.DFL_ALQ_CNBT_INPSTFM_E_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_CNBT_INPSTFM_E = 1;
        public static final int DFL_ALQ_CNBT_INPSTFM_E_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_CNBT_INPSTFM_E = 4;
        public static final int DFL_ALQ_CNBT_INPSTFM_E_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_CNBT_INPSTFM_E = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_CNBT_INPSTFM_E = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
