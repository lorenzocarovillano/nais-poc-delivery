package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-LM-RIS-CON-INT<br>
 * Variable: PCO-LM-RIS-CON-INT from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoLmRisConInt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoLmRisConInt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_LM_RIS_CON_INT;
    }

    public void setPcoLmRisConInt(AfDecimal pcoLmRisConInt) {
        writeDecimalAsPacked(Pos.PCO_LM_RIS_CON_INT, pcoLmRisConInt.copy());
    }

    public void setPcoLmRisConIntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_LM_RIS_CON_INT, Pos.PCO_LM_RIS_CON_INT);
    }

    /**Original name: PCO-LM-RIS-CON-INT<br>*/
    public AfDecimal getPcoLmRisConInt() {
        return readPackedAsDecimal(Pos.PCO_LM_RIS_CON_INT, Len.Int.PCO_LM_RIS_CON_INT, Len.Fract.PCO_LM_RIS_CON_INT);
    }

    public byte[] getPcoLmRisConIntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_LM_RIS_CON_INT, Pos.PCO_LM_RIS_CON_INT);
        return buffer;
    }

    public void initPcoLmRisConIntHighValues() {
        fill(Pos.PCO_LM_RIS_CON_INT, Len.PCO_LM_RIS_CON_INT, Types.HIGH_CHAR_VAL);
    }

    public void setPcoLmRisConIntNull(String pcoLmRisConIntNull) {
        writeString(Pos.PCO_LM_RIS_CON_INT_NULL, pcoLmRisConIntNull, Len.PCO_LM_RIS_CON_INT_NULL);
    }

    /**Original name: PCO-LM-RIS-CON-INT-NULL<br>*/
    public String getPcoLmRisConIntNull() {
        return readString(Pos.PCO_LM_RIS_CON_INT_NULL, Len.PCO_LM_RIS_CON_INT_NULL);
    }

    public String getPcoLmRisConIntNullFormatted() {
        return Functions.padBlanks(getPcoLmRisConIntNull(), Len.PCO_LM_RIS_CON_INT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_LM_RIS_CON_INT = 1;
        public static final int PCO_LM_RIS_CON_INT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_LM_RIS_CON_INT = 4;
        public static final int PCO_LM_RIS_CON_INT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_LM_RIS_CON_INT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_LM_RIS_CON_INT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
