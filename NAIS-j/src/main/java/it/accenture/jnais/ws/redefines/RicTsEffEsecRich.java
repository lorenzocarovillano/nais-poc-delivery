package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIC-TS-EFF-ESEC-RICH<br>
 * Variable: RIC-TS-EFF-ESEC-RICH from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RicTsEffEsecRich extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RicTsEffEsecRich() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIC_TS_EFF_ESEC_RICH;
    }

    public void setRicTsEffEsecRich(long ricTsEffEsecRich) {
        writeLongAsPacked(Pos.RIC_TS_EFF_ESEC_RICH, ricTsEffEsecRich, Len.Int.RIC_TS_EFF_ESEC_RICH);
    }

    public void setRicTsEffEsecRichFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIC_TS_EFF_ESEC_RICH, Pos.RIC_TS_EFF_ESEC_RICH);
    }

    /**Original name: RIC-TS-EFF-ESEC-RICH<br>*/
    public long getRicTsEffEsecRich() {
        return readPackedAsLong(Pos.RIC_TS_EFF_ESEC_RICH, Len.Int.RIC_TS_EFF_ESEC_RICH);
    }

    public byte[] getRicTsEffEsecRichAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIC_TS_EFF_ESEC_RICH, Pos.RIC_TS_EFF_ESEC_RICH);
        return buffer;
    }

    public void setRicTsEffEsecRichNull(String ricTsEffEsecRichNull) {
        writeString(Pos.RIC_TS_EFF_ESEC_RICH_NULL, ricTsEffEsecRichNull, Len.RIC_TS_EFF_ESEC_RICH_NULL);
    }

    /**Original name: RIC-TS-EFF-ESEC-RICH-NULL<br>*/
    public String getRicTsEffEsecRichNull() {
        return readString(Pos.RIC_TS_EFF_ESEC_RICH_NULL, Len.RIC_TS_EFF_ESEC_RICH_NULL);
    }

    public String getRicTsEffEsecRichNullFormatted() {
        return Functions.padBlanks(getRicTsEffEsecRichNull(), Len.RIC_TS_EFF_ESEC_RICH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIC_TS_EFF_ESEC_RICH = 1;
        public static final int RIC_TS_EFF_ESEC_RICH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIC_TS_EFF_ESEC_RICH = 10;
        public static final int RIC_TS_EFF_ESEC_RICH_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIC_TS_EFF_ESEC_RICH = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
