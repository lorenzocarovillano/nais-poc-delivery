package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-DUR-GG<br>
 * Variable: ADE-DUR-GG from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeDurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeDurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_DUR_GG;
    }

    public void setAdeDurGg(int adeDurGg) {
        writeIntAsPacked(Pos.ADE_DUR_GG, adeDurGg, Len.Int.ADE_DUR_GG);
    }

    public void setAdeDurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_DUR_GG, Pos.ADE_DUR_GG);
    }

    /**Original name: ADE-DUR-GG<br>*/
    public int getAdeDurGg() {
        return readPackedAsInt(Pos.ADE_DUR_GG, Len.Int.ADE_DUR_GG);
    }

    public byte[] getAdeDurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_DUR_GG, Pos.ADE_DUR_GG);
        return buffer;
    }

    public void setAdeDurGgNull(String adeDurGgNull) {
        writeString(Pos.ADE_DUR_GG_NULL, adeDurGgNull, Len.ADE_DUR_GG_NULL);
    }

    /**Original name: ADE-DUR-GG-NULL<br>*/
    public String getAdeDurGgNull() {
        return readString(Pos.ADE_DUR_GG_NULL, Len.ADE_DUR_GG_NULL);
    }

    public String getAdeDurGgNullFormatted() {
        return Functions.padBlanks(getAdeDurGgNull(), Len.ADE_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_DUR_GG = 1;
        public static final int ADE_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_DUR_GG = 3;
        public static final int ADE_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
