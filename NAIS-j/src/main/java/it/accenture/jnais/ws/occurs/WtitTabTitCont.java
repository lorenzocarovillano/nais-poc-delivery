package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvtit1;
import it.accenture.jnais.copy.WtitDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WTIT-TAB-TIT-CONT<br>
 * Variables: WTIT-TAB-TIT-CONT from copybook LCCVTITA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WtitTabTitCont {

    //==== PROPERTIES ====
    //Original name: LCCVTIT1
    private Lccvtit1 lccvtit1 = new Lccvtit1();

    //==== METHODS ====
    public void setWtitTabTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvtit1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvtit1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvtit1.Len.Int.ID_PTF, 0));
        position += Lccvtit1.Len.ID_PTF;
        lccvtit1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWtitTabTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvtit1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvtit1.getIdPtf(), Lccvtit1.Len.Int.ID_PTF, 0);
        position += Lccvtit1.Len.ID_PTF;
        lccvtit1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWtitTabTitContSpaces() {
        lccvtit1.initLccvtit1Spaces();
    }

    public Lccvtit1 getLccvtit1() {
        return lccvtit1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TAB_TIT_CONT = WpolStatus.Len.STATUS + Lccvtit1.Len.ID_PTF + WtitDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
