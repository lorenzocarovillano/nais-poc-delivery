package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRE-DOV-INI<br>
 * Variable: WB03-PRE-DOV-INI from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PreDovIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PreDovIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRE_DOV_INI;
    }

    public void setWb03PreDovIni(AfDecimal wb03PreDovIni) {
        writeDecimalAsPacked(Pos.WB03_PRE_DOV_INI, wb03PreDovIni.copy());
    }

    public void setWb03PreDovIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRE_DOV_INI, Pos.WB03_PRE_DOV_INI);
    }

    /**Original name: WB03-PRE-DOV-INI<br>*/
    public AfDecimal getWb03PreDovIni() {
        return readPackedAsDecimal(Pos.WB03_PRE_DOV_INI, Len.Int.WB03_PRE_DOV_INI, Len.Fract.WB03_PRE_DOV_INI);
    }

    public byte[] getWb03PreDovIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRE_DOV_INI, Pos.WB03_PRE_DOV_INI);
        return buffer;
    }

    public void setWb03PreDovIniNull(String wb03PreDovIniNull) {
        writeString(Pos.WB03_PRE_DOV_INI_NULL, wb03PreDovIniNull, Len.WB03_PRE_DOV_INI_NULL);
    }

    /**Original name: WB03-PRE-DOV-INI-NULL<br>*/
    public String getWb03PreDovIniNull() {
        return readString(Pos.WB03_PRE_DOV_INI_NULL, Len.WB03_PRE_DOV_INI_NULL);
    }

    public String getWb03PreDovIniNullFormatted() {
        return Functions.padBlanks(getWb03PreDovIniNull(), Len.WB03_PRE_DOV_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRE_DOV_INI = 1;
        public static final int WB03_PRE_DOV_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRE_DOV_INI = 8;
        public static final int WB03_PRE_DOV_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRE_DOV_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRE_DOV_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
