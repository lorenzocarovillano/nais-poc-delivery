package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-DT-ESI-TIT<br>
 * Variable: TDR-DT-ESI-TIT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrDtEsiTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrDtEsiTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_DT_ESI_TIT;
    }

    public void setTdrDtEsiTit(int tdrDtEsiTit) {
        writeIntAsPacked(Pos.TDR_DT_ESI_TIT, tdrDtEsiTit, Len.Int.TDR_DT_ESI_TIT);
    }

    public void setTdrDtEsiTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_DT_ESI_TIT, Pos.TDR_DT_ESI_TIT);
    }

    /**Original name: TDR-DT-ESI-TIT<br>*/
    public int getTdrDtEsiTit() {
        return readPackedAsInt(Pos.TDR_DT_ESI_TIT, Len.Int.TDR_DT_ESI_TIT);
    }

    public byte[] getTdrDtEsiTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_DT_ESI_TIT, Pos.TDR_DT_ESI_TIT);
        return buffer;
    }

    public void setTdrDtEsiTitNull(String tdrDtEsiTitNull) {
        writeString(Pos.TDR_DT_ESI_TIT_NULL, tdrDtEsiTitNull, Len.TDR_DT_ESI_TIT_NULL);
    }

    /**Original name: TDR-DT-ESI-TIT-NULL<br>*/
    public String getTdrDtEsiTitNull() {
        return readString(Pos.TDR_DT_ESI_TIT_NULL, Len.TDR_DT_ESI_TIT_NULL);
    }

    public String getTdrDtEsiTitNullFormatted() {
        return Functions.padBlanks(getTdrDtEsiTitNull(), Len.TDR_DT_ESI_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_DT_ESI_TIT = 1;
        public static final int TDR_DT_ESI_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_DT_ESI_TIT = 5;
        public static final int TDR_DT_ESI_TIT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_DT_ESI_TIT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
