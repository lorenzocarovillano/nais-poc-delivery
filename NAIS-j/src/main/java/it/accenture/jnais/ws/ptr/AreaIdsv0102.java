package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: AREA-IDSV0102<br>
 * Variable: AREA-IDSV0102 from program IVVS0216<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class AreaIdsv0102 extends BytesClass {

    //==== PROPERTIES ====
    public static final int AREA_COD_STR_DATO_MAXOCCURS = 4000;
    public static final int A_TAB_PARAM_MAXOCCURS = 300;

    //==== CONSTRUCTORS ====
    public AreaIdsv0102() {
        super();
    }

    public AreaIdsv0102(byte[] data) {
        super(data);
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IDSV0102;
    }

    @Override
    public void init() {
        setLimiteMax(4000);
        setaLimiteMax(300);
    }

    public void setLimiteMax(int limiteMax) {
        writeInt(Pos.LIMITE_MAX, limiteMax, Len.Int.LIMITE_MAX, SignType.NO_SIGN);
    }

    /**Original name: IDSV0102-LIMITE-MAX<br>
	 * <pre>*****************************************************************
	 *    area cache x lettura CSV
	 * *****************************************************************</pre>*/
    public int getLimiteMax() {
        return readNumDispUnsignedInt(Pos.LIMITE_MAX, Len.LIMITE_MAX);
    }

    public void setEleMaxCodStrDato(int eleMaxCodStrDato) {
        writeBinaryInt(Pos.ELE_MAX_COD_STR_DATO, eleMaxCodStrDato);
    }

    /**Original name: IDSV0102-ELE-MAX-COD-STR-DATO<br>*/
    public int getEleMaxCodStrDato() {
        return readBinaryInt(Pos.ELE_MAX_COD_STR_DATO);
    }

    public String getEleMaxCodStrDatoFormatted() {
        return PicFormatter.display(new PicParams("S9(5)").setUsage(PicUsage.BINARY)).format(getEleMaxCodStrDato()).toString();
    }

    public String getEleMaxCodStrDatoAsString() {
        return getEleMaxCodStrDatoFormatted();
    }

    public void setContaCacheCsv(int contaCacheCsv) {
        writeBinaryInt(Pos.CONTA_CACHE_CSV, contaCacheCsv);
    }

    /**Original name: IDSV0102-CONTA-CACHE-CSV<br>
	 * <pre> CONTA-CACHE-CSV : numero di entità CSV trovate nell'area
	 *                    di cache</pre>*/
    public int getContaCacheCsv() {
        return readBinaryInt(Pos.CONTA_CACHE_CSV);
    }

    public String getContaCacheCsvFormatted() {
        return PicFormatter.display(new PicParams("S9(5)").setUsage(PicUsage.BINARY)).format(getContaCacheCsv()).toString();
    }

    public String getContaCacheCsvAsString() {
        return getContaCacheCsvFormatted();
    }

    public String getStrCodStrDatoFormatted() {
        return readFixedString(Pos.STR_COD_STR_DATO, Len.STR_COD_STR_DATO);
    }

    public void setCodStrDato(int codStrDatoIdx, String codStrDato) {
        int position = Pos.idsv0102CodStrDato(codStrDatoIdx - 1);
        writeString(position, codStrDato, Len.COD_STR_DATO);
    }

    /**Original name: IDSV0102-COD-STR-DATO<br>*/
    public String getCodStrDato(int codStrDatoIdx) {
        int position = Pos.idsv0102CodStrDato(codStrDatoIdx - 1);
        return readString(position, Len.COD_STR_DATO);
    }

    public void setCodiceDato(int codiceDatoIdx, String codiceDato) {
        int position = Pos.idsv0102CodiceDato(codiceDatoIdx - 1);
        writeString(position, codiceDato, Len.CODICE_DATO);
    }

    /**Original name: IDSV0102-CODICE-DATO<br>*/
    public String getCodiceDato(int codiceDatoIdx) {
        int position = Pos.idsv0102CodiceDato(codiceDatoIdx - 1);
        return readString(position, Len.CODICE_DATO);
    }

    public void setTipoDato(int tipoDatoIdx, String tipoDato) {
        int position = Pos.idsv0102TipoDato(tipoDatoIdx - 1);
        writeString(position, tipoDato, Len.TIPO_DATO);
    }

    /**Original name: IDSV0102-TIPO-DATO<br>*/
    public String getTipoDato(int tipoDatoIdx) {
        int position = Pos.idsv0102TipoDato(tipoDatoIdx - 1);
        return readString(position, Len.TIPO_DATO);
    }

    public void setIniPosi(int iniPosiIdx, long iniPosi) {
        int position = Pos.idsv0102IniPosi(iniPosiIdx - 1);
        writeBinaryUnsignedInt(position, iniPosi);
    }

    /**Original name: IDSV0102-INI-POSI<br>*/
    public long getIniPosi(int iniPosiIdx) {
        int position = Pos.idsv0102IniPosi(iniPosiIdx - 1);
        return readBinaryUnsignedInt(position);
    }

    public void setLunDato(int lunDatoIdx, long lunDato) {
        int position = Pos.idsv0102LunDato(lunDatoIdx - 1);
        writeBinaryUnsignedInt(position, lunDato);
    }

    /**Original name: IDSV0102-LUN-DATO<br>*/
    public long getLunDato(int lunDatoIdx) {
        int position = Pos.idsv0102LunDato(lunDatoIdx - 1);
        return readBinaryUnsignedInt(position);
    }

    public void setLunghezzaDatoNom(int lunghezzaDatoNomIdx, long lunghezzaDatoNom) {
        int position = Pos.idsv0102LunghezzaDatoNom(lunghezzaDatoNomIdx - 1);
        writeBinaryUnsignedInt(position, lunghezzaDatoNom);
    }

    /**Original name: IDSV0102-LUNGHEZZA-DATO-NOM<br>*/
    public long getLunghezzaDatoNom(int lunghezzaDatoNomIdx) {
        int position = Pos.idsv0102LunghezzaDatoNom(lunghezzaDatoNomIdx - 1);
        return readBinaryUnsignedInt(position);
    }

    public void setPrecisioneDato(int precisioneDatoIdx, int precisioneDato) {
        int position = Pos.idsv0102PrecisioneDato(precisioneDatoIdx - 1);
        writeBinaryUnsignedShort(position, precisioneDato);
    }

    /**Original name: IDSV0102-PRECISIONE-DATO<br>*/
    public int getPrecisioneDato(int precisioneDatoIdx) {
        int position = Pos.idsv0102PrecisioneDato(precisioneDatoIdx - 1);
        return readBinaryUnsignedShort(position);
    }

    public void setRestoTabella(String restoTabella) {
        writeString(Pos.RESTO_TABELLA, restoTabella, Len.RESTO_TABELLA);
    }

    /**Original name: IDSV0102-RESTO-TABELLA<br>*/
    public String getRestoTabella() {
        return readString(Pos.RESTO_TABELLA, Len.RESTO_TABELLA);
    }

    public void setaLimiteMax(int aLimiteMax) {
        writeInt(Pos.A_LIMITE_MAX, aLimiteMax, Len.Int.A_LIMITE_MAX, SignType.NO_SIGN);
    }

    /**Original name: IDSV0102A-LIMITE-MAX<br>
	 * <pre>*****************************************************************
	 *    area cache x lettura ADA - MVV
	 * *****************************************************************</pre>*/
    public int getaLimiteMax() {
        return readNumDispUnsignedInt(Pos.A_LIMITE_MAX, Len.A_LIMITE_MAX);
    }

    public void setaEleMaxActu(int aEleMaxActu) {
        writeBinaryInt(Pos.A_ELE_MAX_ACTU, aEleMaxActu);
    }

    /**Original name: IDSV0102A-ELE-MAX-ACTU<br>*/
    public int getaEleMaxActu() {
        return readBinaryInt(Pos.A_ELE_MAX_ACTU);
    }

    public String getaEleMaxActuFormatted() {
        return PicFormatter.display(new PicParams("S9(5)").setUsage(PicUsage.BINARY)).format(getaEleMaxActu()).toString();
    }

    public String getaEleMaxActuAsString() {
        return getaEleMaxActuFormatted();
    }

    public void setaContaCacheAdaMvv(int aContaCacheAdaMvv) {
        writeBinaryInt(Pos.A_CONTA_CACHE_ADA_MVV, aContaCacheAdaMvv);
    }

    /**Original name: IDSV0102A-CONTA-CACHE-ADA-MVV<br>
	 * <pre> CONTA-CACHE-ADA-MVV : numero di entità ADA-MVV trovate nell'area
	 *                        di cache</pre>*/
    public int getaContaCacheAdaMvv() {
        return readBinaryInt(Pos.A_CONTA_CACHE_ADA_MVV);
    }

    public String getaContaCacheAdaMvvFormatted() {
        return PicFormatter.display(new PicParams("S9(5)").setUsage(PicUsage.BINARY)).format(getaContaCacheAdaMvv()).toString();
    }

    public String getaContaCacheAdaMvvAsString() {
        return getaContaCacheAdaMvvFormatted();
    }

    public String getaAdaMvvFormatted() {
        return readFixedString(Pos.A_ADA_MVV, Len.A_ADA_MVV);
    }

    public void setaTipoDato(int aTipoDatoIdx, String aTipoDato) {
        int position = Pos.idsv0102aTipoDato(aTipoDatoIdx - 1);
        writeString(position, aTipoDato, Len.A_TIPO_DATO);
    }

    /**Original name: IDSV0102A-TIPO-DATO<br>
	 * <pre>- area ADA</pre>*/
    public String getaTipoDato(int aTipoDatoIdx) {
        int position = Pos.idsv0102aTipoDato(aTipoDatoIdx - 1);
        return readString(position, Len.A_TIPO_DATO);
    }

    public void setaTipoDatoNull(int aTipoDatoNullIdx, String aTipoDatoNull) {
        int position = Pos.idsv0102aTipoDatoNull(aTipoDatoNullIdx - 1);
        writeString(position, aTipoDatoNull, Len.A_TIPO_DATO_NULL);
    }

    /**Original name: IDSV0102A-TIPO-DATO-NULL<br>*/
    public String getaTipoDatoNull(int aTipoDatoNullIdx) {
        int position = Pos.idsv0102aTipoDatoNull(aTipoDatoNullIdx - 1);
        return readString(position, Len.A_TIPO_DATO_NULL);
    }

    public String getaTipoDatoNullFormatted(int aTipoDatoNullIdx) {
        return Functions.padBlanks(getaTipoDatoNull(aTipoDatoNullIdx), Len.A_TIPO_DATO_NULL);
    }

    public void setaLunghezzaDato(int aLunghezzaDatoIdx, int aLunghezzaDato) {
        int position = Pos.idsv0102aLunghezzaDato(aLunghezzaDatoIdx - 1);
        writeIntAsPacked(position, aLunghezzaDato, Len.Int.A_LUNGHEZZA_DATO);
    }

    /**Original name: IDSV0102A-LUNGHEZZA-DATO<br>*/
    public int getaLunghezzaDato(int aLunghezzaDatoIdx) {
        int position = Pos.idsv0102aLunghezzaDato(aLunghezzaDatoIdx - 1);
        return readPackedAsInt(position, Len.Int.A_LUNGHEZZA_DATO);
    }

    public void setaLunghezzaDatoNull(int aLunghezzaDatoNullIdx, String aLunghezzaDatoNull) {
        int position = Pos.idsv0102aLunghezzaDatoNull(aLunghezzaDatoNullIdx - 1);
        writeString(position, aLunghezzaDatoNull, Len.A_LUNGHEZZA_DATO_NULL);
    }

    /**Original name: IDSV0102A-LUNGHEZZA-DATO-NULL<br>*/
    public String getaLunghezzaDatoNull(int aLunghezzaDatoNullIdx) {
        int position = Pos.idsv0102aLunghezzaDatoNull(aLunghezzaDatoNullIdx - 1);
        return readString(position, Len.A_LUNGHEZZA_DATO_NULL);
    }

    public String getaLunghezzaDatoNullFormatted(int aLunghezzaDatoNullIdx) {
        return Functions.padBlanks(getaLunghezzaDatoNull(aLunghezzaDatoNullIdx), Len.A_LUNGHEZZA_DATO_NULL);
    }

    public void setaPrecisioneDato(int aPrecisioneDatoIdx, short aPrecisioneDato) {
        int position = Pos.idsv0102aPrecisioneDato(aPrecisioneDatoIdx - 1);
        writeShortAsPacked(position, aPrecisioneDato, Len.Int.A_PRECISIONE_DATO);
    }

    /**Original name: IDSV0102A-PRECISIONE-DATO<br>*/
    public short getaPrecisioneDato(int aPrecisioneDatoIdx) {
        int position = Pos.idsv0102aPrecisioneDato(aPrecisioneDatoIdx - 1);
        return readPackedAsShort(position, Len.Int.A_PRECISIONE_DATO);
    }

    public void setaPrecisioneDatoNull(int aPrecisioneDatoNullIdx, String aPrecisioneDatoNull) {
        int position = Pos.idsv0102aPrecisioneDatoNull(aPrecisioneDatoNullIdx - 1);
        writeString(position, aPrecisioneDatoNull, Len.A_PRECISIONE_DATO_NULL);
    }

    /**Original name: IDSV0102A-PRECISIONE-DATO-NULL<br>*/
    public String getaPrecisioneDatoNull(int aPrecisioneDatoNullIdx) {
        int position = Pos.idsv0102aPrecisioneDatoNull(aPrecisioneDatoNullIdx - 1);
        return readString(position, Len.A_PRECISIONE_DATO_NULL);
    }

    public String getaPrecisioneDatoNullFormatted(int aPrecisioneDatoNullIdx) {
        return Functions.padBlanks(getaPrecisioneDatoNull(aPrecisioneDatoNullIdx), Len.A_PRECISIONE_DATO_NULL);
    }

    public void setaFormattazioneDato(int aFormattazioneDatoIdx, String aFormattazioneDato) {
        int position = Pos.idsv0102aFormattazioneDato(aFormattazioneDatoIdx - 1);
        writeString(position, aFormattazioneDato, Len.A_FORMATTAZIONE_DATO);
    }

    /**Original name: IDSV0102A-FORMATTAZIONE-DATO<br>*/
    public String getaFormattazioneDato(int aFormattazioneDatoIdx) {
        int position = Pos.idsv0102aFormattazioneDato(aFormattazioneDatoIdx - 1);
        return readString(position, Len.A_FORMATTAZIONE_DATO);
    }

    public void setaIdMatrValVar(int aIdMatrValVarIdx, int aIdMatrValVar) {
        int position = Pos.idsv0102aIdMatrValVar(aIdMatrValVarIdx - 1);
        writeBinaryInt(position, aIdMatrValVar);
    }

    /**Original name: IDSV0102A-ID-MATR-VAL-VAR<br>
	 * <pre>-> area MVV</pre>*/
    public int getaIdMatrValVar(int aIdMatrValVarIdx) {
        int position = Pos.idsv0102aIdMatrValVar(aIdMatrValVarIdx - 1);
        return readBinaryInt(position);
    }

    public void setaIdpMatrValVar(int aIdpMatrValVarIdx, int aIdpMatrValVar) {
        int position = Pos.idsv0102aIdpMatrValVar(aIdpMatrValVarIdx - 1);
        writeBinaryInt(position, aIdpMatrValVar);
    }

    /**Original name: IDSV0102A-IDP-MATR-VAL-VAR<br>*/
    public int getaIdpMatrValVar(int aIdpMatrValVarIdx) {
        int position = Pos.idsv0102aIdpMatrValVar(aIdpMatrValVarIdx - 1);
        return readBinaryInt(position);
    }

    public void setaIdpMatValVarNull(int aIdpMatValVarNullIdx, String aIdpMatValVarNull) {
        int position = Pos.idsv0102aIdpMatValVarNull(aIdpMatValVarNullIdx - 1);
        writeString(position, aIdpMatValVarNull, Len.A_IDP_MAT_VAL_VAR_NULL);
    }

    /**Original name: IDSV0102A-IDP-MAT-VAL-VAR-NULL<br>*/
    public String getaIdpMatValVarNull(int aIdpMatValVarNullIdx) {
        int position = Pos.idsv0102aIdpMatValVarNull(aIdpMatValVarNullIdx - 1);
        return readString(position, Len.A_IDP_MAT_VAL_VAR_NULL);
    }

    public String getaIdpMatValVarNullFormatted(int aIdpMatValVarNullIdx) {
        return Functions.padBlanks(getaIdpMatValVarNull(aIdpMatValVarNullIdx), Len.A_IDP_MAT_VAL_VAR_NULL);
    }

    public void setaTipoMovimento(int aTipoMovimentoIdx, int aTipoMovimento) {
        int position = Pos.idsv0102aTipoMovimento(aTipoMovimentoIdx - 1);
        writeIntAsPacked(position, aTipoMovimento, Len.Int.A_TIPO_MOVIMENTO);
    }

    /**Original name: IDSV0102A-TIPO-MOVIMENTO<br>*/
    public int getaTipoMovimento(int aTipoMovimentoIdx) {
        int position = Pos.idsv0102aTipoMovimento(aTipoMovimentoIdx - 1);
        return readPackedAsInt(position, Len.Int.A_TIPO_MOVIMENTO);
    }

    public void setaTipoMovimentoNull(int aTipoMovimentoNullIdx, String aTipoMovimentoNull) {
        int position = Pos.idsv0102aTipoMovimentoNull(aTipoMovimentoNullIdx - 1);
        writeString(position, aTipoMovimentoNull, Len.A_TIPO_MOVIMENTO_NULL);
    }

    /**Original name: IDSV0102A-TIPO-MOVIMENTO-NULL<br>*/
    public String getaTipoMovimentoNull(int aTipoMovimentoNullIdx) {
        int position = Pos.idsv0102aTipoMovimentoNull(aTipoMovimentoNullIdx - 1);
        return readString(position, Len.A_TIPO_MOVIMENTO_NULL);
    }

    public String getaTipoMovimentoNullFormatted(int aTipoMovimentoNullIdx) {
        return Functions.padBlanks(getaTipoMovimentoNull(aTipoMovimentoNullIdx), Len.A_TIPO_MOVIMENTO_NULL);
    }

    public void setaCodDatoExt(int aCodDatoExtIdx, String aCodDatoExt) {
        int position = Pos.idsv0102aCodDatoExt(aCodDatoExtIdx - 1);
        writeString(position, aCodDatoExt, Len.A_COD_DATO_EXT);
    }

    /**Original name: IDSV0102A-COD-DATO-EXT<br>*/
    public String getaCodDatoExt(int aCodDatoExtIdx) {
        int position = Pos.idsv0102aCodDatoExt(aCodDatoExtIdx - 1);
        return readString(position, Len.A_COD_DATO_EXT);
    }

    public void setaObbligatorieta(int aObbligatorietaIdx, char aObbligatorieta) {
        int position = Pos.idsv0102aObbligatorieta(aObbligatorietaIdx - 1);
        writeChar(position, aObbligatorieta);
    }

    /**Original name: IDSV0102A-OBBLIGATORIETA<br>*/
    public char getaObbligatorieta(int aObbligatorietaIdx) {
        int position = Pos.idsv0102aObbligatorieta(aObbligatorietaIdx - 1);
        return readChar(position);
    }

    public void setaObbligatorietaNull(int aObbligatorietaNullIdx, char aObbligatorietaNull) {
        int position = Pos.idsv0102aObbligatorietaNull(aObbligatorietaNullIdx - 1);
        writeChar(position, aObbligatorietaNull);
    }

    /**Original name: IDSV0102A-OBBLIGATORIETA-NULL<br>*/
    public char getaObbligatorietaNull(int aObbligatorietaNullIdx) {
        int position = Pos.idsv0102aObbligatorietaNull(aObbligatorietaNullIdx - 1);
        return readChar(position);
    }

    public void setaValoreDefault(int aValoreDefaultIdx, String aValoreDefault) {
        int position = Pos.idsv0102aValoreDefault(aValoreDefaultIdx - 1);
        writeString(position, aValoreDefault, Len.A_VALORE_DEFAULT);
    }

    /**Original name: IDSV0102A-VALORE-DEFAULT<br>*/
    public String getaValoreDefault(int aValoreDefaultIdx) {
        int position = Pos.idsv0102aValoreDefault(aValoreDefaultIdx - 1);
        return readString(position, Len.A_VALORE_DEFAULT);
    }

    public void setaCodStrDatoPtf(int aCodStrDatoPtfIdx, String aCodStrDatoPtf) {
        int position = Pos.idsv0102aCodStrDatoPtf(aCodStrDatoPtfIdx - 1);
        writeString(position, aCodStrDatoPtf, Len.A_COD_STR_DATO_PTF);
    }

    /**Original name: IDSV0102A-COD-STR-DATO-PTF<br>*/
    public String getaCodStrDatoPtf(int aCodStrDatoPtfIdx) {
        int position = Pos.idsv0102aCodStrDatoPtf(aCodStrDatoPtfIdx - 1);
        return readString(position, Len.A_COD_STR_DATO_PTF);
    }

    public void setaCodDatoPtf(int aCodDatoPtfIdx, String aCodDatoPtf) {
        int position = Pos.idsv0102aCodDatoPtf(aCodDatoPtfIdx - 1);
        writeString(position, aCodDatoPtf, Len.A_COD_DATO_PTF);
    }

    /**Original name: IDSV0102A-COD-DATO-PTF<br>*/
    public String getaCodDatoPtf(int aCodDatoPtfIdx) {
        int position = Pos.idsv0102aCodDatoPtf(aCodDatoPtfIdx - 1);
        return readString(position, Len.A_COD_DATO_PTF);
    }

    public void setaCodParametro(int aCodParametroIdx, String aCodParametro) {
        int position = Pos.idsv0102aCodParametro(aCodParametroIdx - 1);
        writeString(position, aCodParametro, Len.A_COD_PARAMETRO);
    }

    /**Original name: IDSV0102A-COD-PARAMETRO<br>*/
    public String getaCodParametro(int aCodParametroIdx) {
        int position = Pos.idsv0102aCodParametro(aCodParametroIdx - 1);
        return readString(position, Len.A_COD_PARAMETRO);
    }

    public void setaOperazione(int aOperazioneIdx, String aOperazione) {
        int position = Pos.idsv0102aOperazione(aOperazioneIdx - 1);
        writeString(position, aOperazione, Len.A_OPERAZIONE);
    }

    /**Original name: IDSV0102A-OPERAZIONE<br>*/
    public String getaOperazione(int aOperazioneIdx) {
        int position = Pos.idsv0102aOperazione(aOperazioneIdx - 1);
        return readString(position, Len.A_OPERAZIONE);
    }

    public void setaLivOperazione(int aLivOperazioneIdx, String aLivOperazione) {
        int position = Pos.idsv0102aLivOperazione(aLivOperazioneIdx - 1);
        writeString(position, aLivOperazione, Len.A_LIV_OPERAZIONE);
    }

    /**Original name: IDSV0102A-LIV-OPERAZIONE<br>*/
    public String getaLivOperazione(int aLivOperazioneIdx) {
        int position = Pos.idsv0102aLivOperazione(aLivOperazioneIdx - 1);
        return readString(position, Len.A_LIV_OPERAZIONE);
    }

    public void setaLivOperazioneNull(int aLivOperazioneNullIdx, String aLivOperazioneNull) {
        int position = Pos.idsv0102aLivOperazioneNull(aLivOperazioneNullIdx - 1);
        writeString(position, aLivOperazioneNull, Len.A_LIV_OPERAZIONE_NULL);
    }

    /**Original name: IDSV0102A-LIV-OPERAZIONE-NULL<br>*/
    public String getaLivOperazioneNull(int aLivOperazioneNullIdx) {
        int position = Pos.idsv0102aLivOperazioneNull(aLivOperazioneNullIdx - 1);
        return readString(position, Len.A_LIV_OPERAZIONE_NULL);
    }

    public String getaLivOperazioneNullFormatted(int aLivOperazioneNullIdx) {
        return Functions.padBlanks(getaLivOperazioneNull(aLivOperazioneNullIdx), Len.A_LIV_OPERAZIONE_NULL);
    }

    public void setaTipoOggetto(int aTipoOggettoIdx, String aTipoOggetto) {
        int position = Pos.idsv0102aTipoOggetto(aTipoOggettoIdx - 1);
        writeString(position, aTipoOggetto, Len.A_TIPO_OGGETTO);
    }

    /**Original name: IDSV0102A-TIPO-OGGETTO<br>*/
    public String getaTipoOggetto(int aTipoOggettoIdx) {
        int position = Pos.idsv0102aTipoOggetto(aTipoOggettoIdx - 1);
        return readString(position, Len.A_TIPO_OGGETTO);
    }

    public void setaTipoOggettoNull(int aTipoOggettoNullIdx, String aTipoOggettoNull) {
        int position = Pos.idsv0102aTipoOggettoNull(aTipoOggettoNullIdx - 1);
        writeString(position, aTipoOggettoNull, Len.A_TIPO_OGGETTO_NULL);
    }

    /**Original name: IDSV0102A-TIPO-OGGETTO-NULL<br>*/
    public String getaTipoOggettoNull(int aTipoOggettoNullIdx) {
        int position = Pos.idsv0102aTipoOggettoNull(aTipoOggettoNullIdx - 1);
        return readString(position, Len.A_TIPO_OGGETTO_NULL);
    }

    public String getaTipoOggettoNullFormatted(int aTipoOggettoNullIdx) {
        return Functions.padBlanks(getaTipoOggettoNull(aTipoOggettoNullIdx), Len.A_TIPO_OGGETTO_NULL);
    }

    public void setaWhereCondition(int aWhereConditionIdx, String aWhereCondition) {
        int position = Pos.idsv0102aWhereCondition(aWhereConditionIdx - 1);
        writeString(position, aWhereCondition, Len.A_WHERE_CONDITION);
    }

    /**Original name: IDSV0102A-WHERE-CONDITION<br>*/
    public String getaWhereCondition(int aWhereConditionIdx) {
        int position = Pos.idsv0102aWhereCondition(aWhereConditionIdx - 1);
        return readString(position, Len.A_WHERE_CONDITION);
    }

    public void setaServizioLettura(int aServizioLetturaIdx, String aServizioLettura) {
        int position = Pos.idsv0102aServizioLettura(aServizioLetturaIdx - 1);
        writeString(position, aServizioLettura, Len.A_SERVIZIO_LETTURA);
    }

    /**Original name: IDSV0102A-SERVIZIO-LETTURA<br>*/
    public String getaServizioLettura(int aServizioLetturaIdx) {
        int position = Pos.idsv0102aServizioLettura(aServizioLetturaIdx - 1);
        return readString(position, Len.A_SERVIZIO_LETTURA);
    }

    public void setaServLetturaNull(int aServLetturaNullIdx, String aServLetturaNull) {
        int position = Pos.idsv0102aServLetturaNull(aServLetturaNullIdx - 1);
        writeString(position, aServLetturaNull, Len.A_SERV_LETTURA_NULL);
    }

    /**Original name: IDSV0102A-SERV-LETTURA-NULL<br>*/
    public String getaServLetturaNull(int aServLetturaNullIdx) {
        int position = Pos.idsv0102aServLetturaNull(aServLetturaNullIdx - 1);
        return readString(position, Len.A_SERV_LETTURA_NULL);
    }

    public String getaServLetturaNullFormatted(int aServLetturaNullIdx) {
        return Functions.padBlanks(getaServLetturaNull(aServLetturaNullIdx), Len.A_SERV_LETTURA_NULL);
    }

    public void setaModuloCalcolo(int aModuloCalcoloIdx, String aModuloCalcolo) {
        int position = Pos.idsv0102aModuloCalcolo(aModuloCalcoloIdx - 1);
        writeString(position, aModuloCalcolo, Len.A_MODULO_CALCOLO);
    }

    /**Original name: IDSV0102A-MODULO-CALCOLO<br>*/
    public String getaModuloCalcolo(int aModuloCalcoloIdx) {
        int position = Pos.idsv0102aModuloCalcolo(aModuloCalcoloIdx - 1);
        return readString(position, Len.A_MODULO_CALCOLO);
    }

    public void setaModuloCalcoloNull(int aModuloCalcoloNullIdx, String aModuloCalcoloNull) {
        int position = Pos.idsv0102aModuloCalcoloNull(aModuloCalcoloNullIdx - 1);
        writeString(position, aModuloCalcoloNull, Len.A_MODULO_CALCOLO_NULL);
    }

    /**Original name: IDSV0102A-MODULO-CALCOLO-NULL<br>*/
    public String getaModuloCalcoloNull(int aModuloCalcoloNullIdx) {
        int position = Pos.idsv0102aModuloCalcoloNull(aModuloCalcoloNullIdx - 1);
        return readString(position, Len.A_MODULO_CALCOLO_NULL);
    }

    public String getaModuloCalcoloNullFormatted(int aModuloCalcoloNullIdx) {
        return Functions.padBlanks(getaModuloCalcoloNull(aModuloCalcoloNullIdx), Len.A_MODULO_CALCOLO_NULL);
    }

    public void setaCodDatoInterno(int aCodDatoInternoIdx, String aCodDatoInterno) {
        int position = Pos.idsv0102aCodDatoInterno(aCodDatoInternoIdx - 1);
        writeString(position, aCodDatoInterno, Len.A_COD_DATO_INTERNO);
    }

    /**Original name: IDSV0102A-COD-DATO-INTERNO<br>*/
    public String getaCodDatoInterno(int aCodDatoInternoIdx) {
        int position = Pos.idsv0102aCodDatoInterno(aCodDatoInternoIdx - 1);
        return readString(position, Len.A_COD_DATO_INTERNO);
    }

    public void setaRestoTabella(String aRestoTabella) {
        writeString(Pos.A_RESTO_TABELLA, aRestoTabella, Len.A_RESTO_TABELLA);
    }

    /**Original name: IDSV0102A-RESTO-TABELLA<br>*/
    public String getaRestoTabella() {
        return readString(Pos.A_RESTO_TABELLA, Len.A_RESTO_TABELLA);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int AREA_IDSV0102 = 1;
        public static final int LIMITE_MAX = AREA_IDSV0102;
        public static final int ELE_MAX_COD_STR_DATO = LIMITE_MAX + Len.LIMITE_MAX;
        public static final int CONTA_CACHE_CSV = ELE_MAX_COD_STR_DATO + Len.ELE_MAX_COD_STR_DATO;
        public static final int STR_COD_STR_DATO = CONTA_CACHE_CSV + Len.CONTA_CACHE_CSV;
        public static final int STR_COD_STR_DATO_R = STR_COD_STR_DATO;
        public static final int FLR1 = STR_COD_STR_DATO_R;
        public static final int RESTO_TABELLA = FLR1 + Len.FLR1;
        public static final int A_LIMITE_MAX = idsv0102PrecisioneDato(AREA_COD_STR_DATO_MAXOCCURS - 1) + Len.PRECISIONE_DATO;
        public static final int A_ELE_MAX_ACTU = A_LIMITE_MAX + Len.A_LIMITE_MAX;
        public static final int A_CONTA_CACHE_ADA_MVV = A_ELE_MAX_ACTU + Len.A_ELE_MAX_ACTU;
        public static final int A_ADA_MVV = A_CONTA_CACHE_ADA_MVV + Len.A_CONTA_CACHE_ADA_MVV;
        public static final int A_ADA_MVV_R = A_ADA_MVV;
        public static final int FLR2 = A_ADA_MVV_R;
        public static final int A_RESTO_TABELLA = FLR2 + Len.FLR2;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int idsv0102AreaCodStrDato(int idx) {
            return STR_COD_STR_DATO + idx * Len.AREA_COD_STR_DATO;
        }

        public static int idsv0102CodStrDato(int idx) {
            return idsv0102AreaCodStrDato(idx);
        }

        public static int idsv0102CodiceDato(int idx) {
            return idsv0102CodStrDato(idx) + Len.COD_STR_DATO;
        }

        public static int idsv0102TipoDato(int idx) {
            return idsv0102CodiceDato(idx) + Len.CODICE_DATO;
        }

        public static int idsv0102IniPosi(int idx) {
            return idsv0102TipoDato(idx) + Len.TIPO_DATO;
        }

        public static int idsv0102LunDato(int idx) {
            return idsv0102IniPosi(idx) + Len.INI_POSI;
        }

        public static int idsv0102LunghezzaDatoNom(int idx) {
            return idsv0102LunDato(idx) + Len.LUN_DATO;
        }

        public static int idsv0102PrecisioneDato(int idx) {
            return idsv0102LunghezzaDatoNom(idx) + Len.LUNGHEZZA_DATO_NOM;
        }

        public static int idsv0102aTabParam(int idx) {
            return A_ADA_MVV + idx * Len.A_TAB_PARAM;
        }

        public static int idsv0102aTipoDato(int idx) {
            return idsv0102aTabParam(idx);
        }

        public static int idsv0102aTipoDatoNull(int idx) {
            return idsv0102aTipoDato(idx);
        }

        public static int idsv0102aLunghezzaDato(int idx) {
            return idsv0102aTipoDato(idx) + Len.A_TIPO_DATO;
        }

        public static int idsv0102aLunghezzaDatoNull(int idx) {
            return idsv0102aLunghezzaDato(idx);
        }

        public static int idsv0102aPrecisioneDato(int idx) {
            return idsv0102aLunghezzaDato(idx) + Len.A_LUNGHEZZA_DATO;
        }

        public static int idsv0102aPrecisioneDatoNull(int idx) {
            return idsv0102aPrecisioneDato(idx);
        }

        public static int idsv0102aFormattazioneDato(int idx) {
            return idsv0102aPrecisioneDato(idx) + Len.A_PRECISIONE_DATO;
        }

        public static int idsv0102aIdMatrValVar(int idx) {
            return idsv0102aFormattazioneDato(idx) + Len.A_FORMATTAZIONE_DATO;
        }

        public static int idsv0102aIdpMatrValVar(int idx) {
            return idsv0102aIdMatrValVar(idx) + Len.A_ID_MATR_VAL_VAR;
        }

        public static int idsv0102aIdpMatValVarNull(int idx) {
            return idsv0102aIdpMatrValVar(idx);
        }

        public static int idsv0102aTipoMovimento(int idx) {
            return idsv0102aIdpMatrValVar(idx) + Len.A_IDP_MATR_VAL_VAR;
        }

        public static int idsv0102aTipoMovimentoNull(int idx) {
            return idsv0102aTipoMovimento(idx);
        }

        public static int idsv0102aCodDatoExt(int idx) {
            return idsv0102aTipoMovimento(idx) + Len.A_TIPO_MOVIMENTO;
        }

        public static int idsv0102aObbligatorieta(int idx) {
            return idsv0102aCodDatoExt(idx) + Len.A_COD_DATO_EXT;
        }

        public static int idsv0102aObbligatorietaNull(int idx) {
            return idsv0102aObbligatorieta(idx);
        }

        public static int idsv0102aValoreDefault(int idx) {
            return idsv0102aObbligatorieta(idx) + Len.A_OBBLIGATORIETA;
        }

        public static int idsv0102aCodStrDatoPtf(int idx) {
            return idsv0102aValoreDefault(idx) + Len.A_VALORE_DEFAULT;
        }

        public static int idsv0102aCodDatoPtf(int idx) {
            return idsv0102aCodStrDatoPtf(idx) + Len.A_COD_STR_DATO_PTF;
        }

        public static int idsv0102aCodParametro(int idx) {
            return idsv0102aCodDatoPtf(idx) + Len.A_COD_DATO_PTF;
        }

        public static int idsv0102aOperazione(int idx) {
            return idsv0102aCodParametro(idx) + Len.A_COD_PARAMETRO;
        }

        public static int idsv0102aLivOperazione(int idx) {
            return idsv0102aOperazione(idx) + Len.A_OPERAZIONE;
        }

        public static int idsv0102aLivOperazioneNull(int idx) {
            return idsv0102aLivOperazione(idx);
        }

        public static int idsv0102aTipoOggetto(int idx) {
            return idsv0102aLivOperazione(idx) + Len.A_LIV_OPERAZIONE;
        }

        public static int idsv0102aTipoOggettoNull(int idx) {
            return idsv0102aTipoOggetto(idx);
        }

        public static int idsv0102aWhereCondition(int idx) {
            return idsv0102aTipoOggetto(idx) + Len.A_TIPO_OGGETTO;
        }

        public static int idsv0102aServizioLettura(int idx) {
            return idsv0102aWhereCondition(idx) + Len.A_WHERE_CONDITION;
        }

        public static int idsv0102aServLetturaNull(int idx) {
            return idsv0102aServizioLettura(idx);
        }

        public static int idsv0102aModuloCalcolo(int idx) {
            return idsv0102aServizioLettura(idx) + Len.A_SERVIZIO_LETTURA;
        }

        public static int idsv0102aModuloCalcoloNull(int idx) {
            return idsv0102aModuloCalcolo(idx);
        }

        public static int idsv0102aCodDatoInterno(int idx) {
            return idsv0102aModuloCalcolo(idx) + Len.A_MODULO_CALCOLO;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LIMITE_MAX = 5;
        public static final int ELE_MAX_COD_STR_DATO = 4;
        public static final int CONTA_CACHE_CSV = 4;
        public static final int COD_STR_DATO = 30;
        public static final int CODICE_DATO = 30;
        public static final int TIPO_DATO = 2;
        public static final int INI_POSI = 4;
        public static final int LUN_DATO = 4;
        public static final int LUNGHEZZA_DATO_NOM = 4;
        public static final int PRECISIONE_DATO = 2;
        public static final int AREA_COD_STR_DATO = COD_STR_DATO + CODICE_DATO + TIPO_DATO + INI_POSI + LUN_DATO + LUNGHEZZA_DATO_NOM + PRECISIONE_DATO;
        public static final int FLR1 = 76;
        public static final int A_LIMITE_MAX = 5;
        public static final int A_ELE_MAX_ACTU = 4;
        public static final int A_CONTA_CACHE_ADA_MVV = 4;
        public static final int A_TIPO_DATO = 2;
        public static final int A_LUNGHEZZA_DATO = 3;
        public static final int A_PRECISIONE_DATO = 2;
        public static final int A_FORMATTAZIONE_DATO = 20;
        public static final int A_ID_MATR_VAL_VAR = 4;
        public static final int A_IDP_MATR_VAL_VAR = 4;
        public static final int A_TIPO_MOVIMENTO = 3;
        public static final int A_COD_DATO_EXT = 30;
        public static final int A_OBBLIGATORIETA = 1;
        public static final int A_VALORE_DEFAULT = 50;
        public static final int A_COD_STR_DATO_PTF = 30;
        public static final int A_COD_DATO_PTF = 30;
        public static final int A_COD_PARAMETRO = 20;
        public static final int A_OPERAZIONE = 15;
        public static final int A_LIV_OPERAZIONE = 3;
        public static final int A_TIPO_OGGETTO = 2;
        public static final int A_WHERE_CONDITION = 300;
        public static final int A_SERVIZIO_LETTURA = 8;
        public static final int A_MODULO_CALCOLO = 8;
        public static final int A_COD_DATO_INTERNO = 30;
        public static final int A_TAB_PARAM = A_TIPO_DATO + A_LUNGHEZZA_DATO + A_PRECISIONE_DATO + A_FORMATTAZIONE_DATO + A_ID_MATR_VAL_VAR + A_IDP_MATR_VAL_VAR + A_TIPO_MOVIMENTO + A_COD_DATO_EXT + A_OBBLIGATORIETA + A_VALORE_DEFAULT + A_COD_STR_DATO_PTF + A_COD_DATO_PTF + A_COD_PARAMETRO + A_OPERAZIONE + A_LIV_OPERAZIONE + A_TIPO_OGGETTO + A_WHERE_CONDITION + A_SERVIZIO_LETTURA + A_MODULO_CALCOLO + A_COD_DATO_INTERNO;
        public static final int FLR2 = 565;
        public static final int STR_COD_STR_DATO = AreaIdsv0102.AREA_COD_STR_DATO_MAXOCCURS * AREA_COD_STR_DATO;
        public static final int A_ADA_MVV = AreaIdsv0102.A_TAB_PARAM_MAXOCCURS * A_TAB_PARAM;
        public static final int AREA_IDSV0102 = LIMITE_MAX + ELE_MAX_COD_STR_DATO + CONTA_CACHE_CSV + STR_COD_STR_DATO + A_LIMITE_MAX + A_ELE_MAX_ACTU + A_CONTA_CACHE_ADA_MVV + A_ADA_MVV;
        public static final int A_TIPO_DATO_NULL = 2;
        public static final int A_LUNGHEZZA_DATO_NULL = 3;
        public static final int A_PRECISIONE_DATO_NULL = 2;
        public static final int A_IDP_MAT_VAL_VAR_NULL = 4;
        public static final int A_TIPO_MOVIMENTO_NULL = 3;
        public static final int A_LIV_OPERAZIONE_NULL = 3;
        public static final int A_TIPO_OGGETTO_NULL = 2;
        public static final int A_SERV_LETTURA_NULL = 8;
        public static final int A_MODULO_CALCOLO_NULL = 8;
        public static final int RESTO_TABELLA = 303924;
        public static final int A_RESTO_TABELLA = 168935;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LIMITE_MAX = 5;
            public static final int A_LIMITE_MAX = 5;
            public static final int A_LUNGHEZZA_DATO = 5;
            public static final int A_PRECISIONE_DATO = 2;
            public static final int A_TIPO_MOVIMENTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
