package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPVT-PROV-INC<br>
 * Variable: WPVT-PROV-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_PROV_INC;
    }

    public void setWpvtProvInc(AfDecimal wpvtProvInc) {
        writeDecimalAsPacked(Pos.WPVT_PROV_INC, wpvtProvInc.copy());
    }

    public void setWpvtProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_PROV_INC, Pos.WPVT_PROV_INC);
    }

    /**Original name: WPVT-PROV-INC<br>*/
    public AfDecimal getWpvtProvInc() {
        return readPackedAsDecimal(Pos.WPVT_PROV_INC, Len.Int.WPVT_PROV_INC, Len.Fract.WPVT_PROV_INC);
    }

    public byte[] getWpvtProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_PROV_INC, Pos.WPVT_PROV_INC);
        return buffer;
    }

    public void initWpvtProvIncSpaces() {
        fill(Pos.WPVT_PROV_INC, Len.WPVT_PROV_INC, Types.SPACE_CHAR);
    }

    public void setWpvtProvIncNull(String wpvtProvIncNull) {
        writeString(Pos.WPVT_PROV_INC_NULL, wpvtProvIncNull, Len.WPVT_PROV_INC_NULL);
    }

    /**Original name: WPVT-PROV-INC-NULL<br>*/
    public String getWpvtProvIncNull() {
        return readString(Pos.WPVT_PROV_INC_NULL, Len.WPVT_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_PROV_INC = 1;
        public static final int WPVT_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_PROV_INC = 8;
        public static final int WPVT_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPVT_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
