package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: AC5-ISPRECALC<br>
 * Variable: AC5-ISPRECALC from program LDBS8800<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ac5Isprecalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Ac5Isprecalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AC5_ISPRECALC;
    }

    public void setAc5Isprecalc(short ac5Isprecalc) {
        writeBinaryShort(Pos.AC5_ISPRECALC, ac5Isprecalc);
    }

    public void setAc5IsprecalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Types.SHORT_SIZE, Pos.AC5_ISPRECALC);
    }

    /**Original name: AC5-ISPRECALC<br>*/
    public short getAc5Isprecalc() {
        return readBinaryShort(Pos.AC5_ISPRECALC);
    }

    public byte[] getAc5IsprecalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Types.SHORT_SIZE, Pos.AC5_ISPRECALC);
        return buffer;
    }

    public void setAc5IsprecalcNull(String ac5IsprecalcNull) {
        writeString(Pos.AC5_ISPRECALC_NULL, ac5IsprecalcNull, Len.AC5_ISPRECALC_NULL);
    }

    /**Original name: AC5-ISPRECALC-NULL<br>*/
    public String getAc5IsprecalcNull() {
        return readString(Pos.AC5_ISPRECALC_NULL, Len.AC5_ISPRECALC_NULL);
    }

    public String getAc5IsprecalcNullFormatted() {
        return Functions.padBlanks(getAc5IsprecalcNull(), Len.AC5_ISPRECALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int AC5_ISPRECALC = 1;
        public static final int AC5_ISPRECALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int AC5_ISPRECALC = 2;
        public static final int AC5_ISPRECALC_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
