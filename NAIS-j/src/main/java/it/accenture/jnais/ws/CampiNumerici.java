package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: CAMPI-NUMERICI<br>
 * Variable: CAMPI-NUMERICI from program IDSS0140<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class CampiNumerici {

    //==== PROPERTIES ====
    //Original name: CHR-X-DI-1
    private char chrXDi1 = DefaultValues.CHAR_VAL;
    //Original name: CHR-X-DI-3
    private String chrXDi3 = DefaultValues.stringVal(Len.CHR_X_DI3);
    //Original name: CHR-X-DI-20
    private String chrXDi20 = DefaultValues.stringVal(Len.CHR_X_DI20);
    //Original name: CHR-X-DI-40
    private String chrXDi40 = DefaultValues.stringVal(Len.CHR_X_DI40);
    //Original name: NUM-NOVE-DI-2
    private String numNoveDi2 = DefaultValues.stringVal(Len.NUM_NOVE_DI2);
    //Original name: NUM-S-NOVE-DI-1-V-COMP
    private short numSNoveDi1VComp = DefaultValues.SHORT_VAL;
    //Original name: FILLER-S-NOVE-DI-1-V-COMP
    private String flr5 = DefaultValues.stringVal(Len.FLR1);
    //Original name: NUM-NOVE-DI-2-V2-COMP
    private AfDecimal numNoveDi2V2Comp = new AfDecimal(DefaultValues.DEC_VAL, 4, 2);
    //Original name: NUM-NOVE-DI-2-V3-COMP
    private AfDecimal numNoveDi2V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 5, 3);
    //Original name: NUM-NOVE-DI-3-COMP
    private short numNoveDi3Comp = DefaultValues.SHORT_VAL;
    //Original name: NUM-NOVE-DI-3-V2-COMP
    private AfDecimal numNoveDi3V2Comp = new AfDecimal(DefaultValues.DEC_VAL, 5, 2);
    //Original name: NUM-NOVE-DI-3-V3-COMP
    private AfDecimal numNoveDi3V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: NUM-NOVE-DI-5-COMP
    private int numNoveDi5Comp = DefaultValues.INT_VAL;
    //Original name: NUM-NOVE-DI-5-V3-COMP
    private AfDecimal numNoveDi5V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 8, 3);
    //Original name: NUM-NOVE-DI-7-V3-COMP
    private AfDecimal numNoveDi7V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 10, 3);
    //Original name: NUM-NOVE-DI-7-COMP
    private int numNoveDi7Comp = DefaultValues.INT_VAL;
    //Original name: NUM-NOVE-DI-8-V3-COMP
    private AfDecimal numNoveDi8V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 11, 3);
    //Original name: NUM-NOVE-DI-9-COMP
    private int numNoveDi9Comp = DefaultValues.INT_VAL;
    //Original name: NUM-NOVE-DI-9-V3-COMP
    private AfDecimal numNoveDi9V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 12, 3);
    //Original name: NUM-NOVE-DI-11-V3-COMP
    private AfDecimal numNoveDi11V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 14, 3);
    //Original name: NUM-S-NOVE-DI-2-V-COMP
    private short numSNoveDi2VComp = DefaultValues.SHORT_VAL;
    //Original name: FILLER-S-NOVE-DI-2-V-COMP
    private String flr19 = DefaultValues.stringVal(Len.FLR4);
    //Original name: NUM-S-NOVE-DI-2-V2-COMP
    private AfDecimal numSNoveDi2V2Comp = new AfDecimal(DefaultValues.DEC_VAL, 4, 2);
    //Original name: NUM-S-DI-2-V3-COMP
    private AfDecimal numSDi2V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 5, 3);
    //Original name: NUM-S-NOVE-DI-3-V-COMP
    private short numSNoveDi3VComp = DefaultValues.SHORT_VAL;
    //Original name: FILLER-S-NOVE-DI-3-V-COMP
    private String flr22 = DefaultValues.stringVal(Len.FLR4);
    //Original name: NUM-S-NOVE-DI-3-V2-COMP
    private AfDecimal numSNoveDi3V2Comp = new AfDecimal(DefaultValues.DEC_VAL, 5, 2);
    //Original name: NUM-S-NOVE-DI-3-V3-COMP
    private AfDecimal numSNoveDi3V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: FILLER-S-NOVE-DI-3-V3-COMP
    private String flr24 = DefaultValues.stringVal(Len.FLR2);
    //Original name: NUM-S-NOVE-DI-4-V-COMP
    private short numSNoveDi4VComp = DefaultValues.SHORT_VAL;
    //Original name: NUM-S-NOVE-DI-5-V-COMP
    private int numSNoveDi5VComp = DefaultValues.INT_VAL;
    //Original name: FILLER-S-NOVE-DI-5-V-COMP
    private String flr26 = DefaultValues.stringVal(Len.FLR2);
    //Original name: NUM-S-NOVE-DI-5-V9-COMP
    private AfDecimal numSNoveDi5V9Comp = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: FILLER-S-NOVE-DI-5-V9-COMP
    private String flr27 = DefaultValues.stringVal(Len.FLR27);
    //Original name: NUM-S-NOVE-DI-6-V-COMP
    private int numSNoveDi6VComp = DefaultValues.INT_VAL;
    //Original name: FILLER-S-NOVE-DI-6-V-COMP
    private String flr28 = DefaultValues.stringVal(Len.FLR12);
    //Original name: NUM-S-NOVE-DI-5-V3-COMP
    private AfDecimal numSNoveDi5V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 8, 3);
    //Original name: NUM-S-NOVE-DI-7-V3-COMP
    private AfDecimal numSNoveDi7V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 10, 3);
    //Original name: NUM-S-NOVE-DI-7-V5-COMP
    private AfDecimal numSNoveDi7V5Comp = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: FILLER-S-NOVE-DI-7-V5-COMP
    private String flr31 = DefaultValues.stringVal(Len.FLR18);
    //Original name: NUM-S-NOVE-DI-7-COMP
    private int numSNoveDi7Comp = DefaultValues.INT_VAL;
    //Original name: NUM-S-NOVE-DI-8-V3-COMP
    private AfDecimal numSNoveDi8V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 11, 3);
    //Original name: NUM-S-NOVE-DI-8-V-COMP
    private int numSNoveDi8VComp = DefaultValues.INT_VAL;
    //Original name: FILLER-S-NOVE-DI-8-V-COMP
    private String flr34 = DefaultValues.stringVal(Len.FLR13);
    //Original name: NUM-S-NOVE-DI-10-V-COMP
    private long numSNoveDi10VComp = DefaultValues.LONG_VAL;
    //Original name: FILLER-S-NOVE-DI-10-V-COMP
    private String flr35 = DefaultValues.stringVal(Len.FLR15);
    //Original name: NUM-S-NOVE-DI-9-V-COMP
    private int numSNoveDi9VComp = DefaultValues.INT_VAL;
    //Original name: FILLER-S-NOVE-DI-9-V-COMP
    private String flr36 = DefaultValues.stringVal(Len.FLR13);
    //Original name: NUM-S-NOVE-DI-11-V3-COMP
    private AfDecimal numSNoveDi11V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 14, 3);
    //Original name: NUM-S-NOVE-DI-12-V3-COMP
    private AfDecimal numSNoveDi12V3Comp = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: FILLER-S-NOVE-DI-12-V3-COMP
    private String flr38 = DefaultValues.stringVal(Len.FLR27);
    //Original name: NUM-S-NOVE-DI-1-V
    private short numSNoveDi1V = DefaultValues.SHORT_VAL;
    //Original name: FILLER-S-NOVE-DI-1-V
    private String flr39 = DefaultValues.stringVal(Len.FLR1);
    //Original name: NUM-S-NOVE-DI-2-V
    private short numSNoveDi2V = DefaultValues.SHORT_VAL;
    //Original name: FILLER-S-NOVE-DI-2-V
    private String flr40 = DefaultValues.stringVal(Len.FLR4);
    //Original name: NUM-S-NOVE-DI-2-V2
    private AfDecimal numSNoveDi2V2 = new AfDecimal(DefaultValues.DEC_VAL, 4, 2);
    //Original name: NUM-S-NOVE-DI-3-V
    private short numSNoveDi3V = DefaultValues.SHORT_VAL;
    //Original name: FILLER-S-NOVE-DI-3-V
    private String flr42 = DefaultValues.stringVal(Len.FLR4);
    //Original name: NUM-S-NOVE-DI-3-V2
    private AfDecimal numSNoveDi3V21 = new AfDecimal(DefaultValues.DEC_VAL, 5, 2);
    //Original name: NUM-S-NOVE-DI-5-V
    private int numSNoveDi5V = DefaultValues.INT_VAL;
    //Original name: FILLER-S-NOVE-DI-5-V
    private String flr44 = DefaultValues.stringVal(Len.FLR2);
    //Original name: NUM-S-NOVE-DI-5-V9
    private AfDecimal numSNoveDi5V9 = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: FILLER-S-NOVE-DI-5-V9
    private String flr45 = DefaultValues.stringVal(Len.FLR27);
    //Original name: NUM-S-NOVE-DI-3-V2
    private AfDecimal numSNoveDi3V22 = new AfDecimal(DefaultValues.DEC_VAL, 5, 2);
    //Original name: NUM-S-NOVE-DI-3-V3
    private AfDecimal numSNoveDi3V3 = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: FILLER-S-NOVE-DI-3-V3
    private String flr47 = DefaultValues.stringVal(Len.FLR12);
    //Original name: NUM-S-NOVE-DI-6-V
    private int numSNoveDi6V = DefaultValues.INT_VAL;
    //Original name: FILLER-S-NOVE-DI-6-V
    private String flr48 = DefaultValues.stringVal(Len.FLR12);
    //Original name: NUM-S-NOVE-DI-8-V
    private int numSNoveDi8V = DefaultValues.INT_VAL;
    //Original name: FILLER-S-NOVE-DI-8-V
    private String flr49 = DefaultValues.stringVal(Len.FLR13);
    //Original name: NUM-S-NOVE-DI-9-V
    private int numSNoveDi9V = DefaultValues.INT_VAL;
    //Original name: FILLER-S-NOVE-DI-9-V
    private String flr50 = DefaultValues.stringVal(Len.FLR13);
    //Original name: NUM-S-NOVE-DI-8-V3
    private AfDecimal numSNoveDi8V3 = new AfDecimal(DefaultValues.DEC_VAL, 11, 3);
    //Original name: NUM-S-NOVE-DI-11-COMP
    private long numSNoveDi11Comp = DefaultValues.LONG_VAL;
    //Original name: NUM-S-NOVE-DI-12-V3
    private AfDecimal numSNoveDi12V3 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: FILLER-S-NOVE-DI-12-V3
    private String flr53 = DefaultValues.stringVal(Len.FLR53);
    //Original name: NUM-S-NOVE-DI-3-V7
    private AfDecimal numSNoveDi3V7 = new AfDecimal(DefaultValues.DEC_VAL, 10, 7);
    //Original name: FILLER-S-NOVE-DI-3-V7
    private String flr54 = DefaultValues.stringVal(Len.FLR54);

    //==== METHODS ====
    public void setChrXDi1(char chrXDi1) {
        this.chrXDi1 = chrXDi1;
    }

    public char getChrXDi1() {
        return this.chrXDi1;
    }

    public void setChrXDi3(String chrXDi3) {
        this.chrXDi3 = Functions.subString(chrXDi3, Len.CHR_X_DI3);
    }

    public String getChrXDi3() {
        return this.chrXDi3;
    }

    public void setChrXDi20(String chrXDi20) {
        this.chrXDi20 = Functions.subString(chrXDi20, Len.CHR_X_DI20);
    }

    public String getChrXDi20() {
        return this.chrXDi20;
    }

    public void setChrXDi40(String chrXDi40) {
        this.chrXDi40 = Functions.subString(chrXDi40, Len.CHR_X_DI40);
    }

    public String getChrXDi40() {
        return this.chrXDi40;
    }

    public void setNumNoveDi2Formatted(String numNoveDi2) {
        this.numNoveDi2 = Trunc.toUnsignedNumeric(numNoveDi2, Len.NUM_NOVE_DI2);
    }

    public short getNumNoveDi2() {
        return NumericDisplay.asShort(this.numNoveDi2);
    }

    public void setsNoveDi1VCompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI1_V_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI1_V_COMP);
        setsNoveDi1VCompBytes(buffer, 1);
    }

    public void setsNoveDi1VCompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi1VComp = MarshalByte.readShort(buffer, position, Len.NUM_S_NOVE_DI1_V_COMP);
        position += Len.NUM_S_NOVE_DI1_V_COMP;
        flr5 = MarshalByte.readString(buffer, position, Len.FLR1);
    }

    public void setNumSNoveDi1VComp(short numSNoveDi1VComp) {
        this.numSNoveDi1VComp = numSNoveDi1VComp;
    }

    public short getNumSNoveDi1VComp() {
        return this.numSNoveDi1VComp;
    }

    public void setFlr5(String flr5) {
        this.flr5 = Functions.subString(flr5, Len.FLR1);
    }

    public String getFlr5() {
        return this.flr5;
    }

    public void setNumNoveDi2V2Comp(AfDecimal numNoveDi2V2Comp) {
        this.numNoveDi2V2Comp.assign(numNoveDi2V2Comp);
    }

    public AfDecimal getNumNoveDi2V2Comp() {
        return this.numNoveDi2V2Comp.copy();
    }

    public void setNumNoveDi2V3Comp(AfDecimal numNoveDi2V3Comp) {
        this.numNoveDi2V3Comp.assign(numNoveDi2V3Comp);
    }

    public AfDecimal getNumNoveDi2V3Comp() {
        return this.numNoveDi2V3Comp.copy();
    }

    public void setNumNoveDi3Comp(short numNoveDi3Comp) {
        this.numNoveDi3Comp = numNoveDi3Comp;
    }

    public short getNumNoveDi3Comp() {
        return this.numNoveDi3Comp;
    }

    public void setNumNoveDi3V2Comp(AfDecimal numNoveDi3V2Comp) {
        this.numNoveDi3V2Comp.assign(numNoveDi3V2Comp);
    }

    public AfDecimal getNumNoveDi3V2Comp() {
        return this.numNoveDi3V2Comp.copy();
    }

    public void setNumNoveDi3V3Comp(AfDecimal numNoveDi3V3Comp) {
        this.numNoveDi3V3Comp.assign(numNoveDi3V3Comp);
    }

    public AfDecimal getNumNoveDi3V3Comp() {
        return this.numNoveDi3V3Comp.copy();
    }

    public void setNumNoveDi5Comp(int numNoveDi5Comp) {
        this.numNoveDi5Comp = numNoveDi5Comp;
    }

    public int getNumNoveDi5Comp() {
        return this.numNoveDi5Comp;
    }

    public void setNumNoveDi5V3Comp(AfDecimal numNoveDi5V3Comp) {
        this.numNoveDi5V3Comp.assign(numNoveDi5V3Comp);
    }

    public AfDecimal getNumNoveDi5V3Comp() {
        return this.numNoveDi5V3Comp.copy();
    }

    public void setNumNoveDi7V3Comp(AfDecimal numNoveDi7V3Comp) {
        this.numNoveDi7V3Comp.assign(numNoveDi7V3Comp);
    }

    public AfDecimal getNumNoveDi7V3Comp() {
        return this.numNoveDi7V3Comp.copy();
    }

    public void setNumNoveDi7Comp(int numNoveDi7Comp) {
        this.numNoveDi7Comp = numNoveDi7Comp;
    }

    public int getNumNoveDi7Comp() {
        return this.numNoveDi7Comp;
    }

    public void setNumNoveDi8V3Comp(AfDecimal numNoveDi8V3Comp) {
        this.numNoveDi8V3Comp.assign(numNoveDi8V3Comp);
    }

    public AfDecimal getNumNoveDi8V3Comp() {
        return this.numNoveDi8V3Comp.copy();
    }

    public void setNumNoveDi9Comp(int numNoveDi9Comp) {
        this.numNoveDi9Comp = numNoveDi9Comp;
    }

    public int getNumNoveDi9Comp() {
        return this.numNoveDi9Comp;
    }

    public void setNumNoveDi9V3Comp(AfDecimal numNoveDi9V3Comp) {
        this.numNoveDi9V3Comp.assign(numNoveDi9V3Comp);
    }

    public AfDecimal getNumNoveDi9V3Comp() {
        return this.numNoveDi9V3Comp.copy();
    }

    public void setNumNoveDi11V3Comp(AfDecimal numNoveDi11V3Comp) {
        this.numNoveDi11V3Comp.assign(numNoveDi11V3Comp);
    }

    public AfDecimal getNumNoveDi11V3Comp() {
        return this.numNoveDi11V3Comp.copy();
    }

    public void setsNoveDi2VCompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI2_V_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI2_V_COMP);
        setsNoveDi2VCompBytes(buffer, 1);
    }

    public void setsNoveDi2VCompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi2VComp = MarshalByte.readPackedAsShort(buffer, position, Len.Int.NUM_S_NOVE_DI2_V_COMP, 0);
        position += Len.NUM_S_NOVE_DI2_V_COMP;
        flr19 = MarshalByte.readString(buffer, position, Len.FLR4);
    }

    public void setNumSNoveDi2VComp(short numSNoveDi2VComp) {
        this.numSNoveDi2VComp = numSNoveDi2VComp;
    }

    public short getNumSNoveDi2VComp() {
        return this.numSNoveDi2VComp;
    }

    public void setFlr19(String flr19) {
        this.flr19 = Functions.subString(flr19, Len.FLR4);
    }

    public String getFlr19() {
        return this.flr19;
    }

    public void setNumSNoveDi2V2Comp(AfDecimal numSNoveDi2V2Comp) {
        this.numSNoveDi2V2Comp.assign(numSNoveDi2V2Comp);
    }

    public AfDecimal getNumSNoveDi2V2Comp() {
        return this.numSNoveDi2V2Comp.copy();
    }

    public void setNumSDi2V3Comp(AfDecimal numSDi2V3Comp) {
        this.numSDi2V3Comp.assign(numSDi2V3Comp);
    }

    public AfDecimal getNumSDi2V3Comp() {
        return this.numSDi2V3Comp.copy();
    }

    public void setsNoveDi3VCompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI3_V_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI3_V_COMP);
        setsNoveDi3VCompBytes(buffer, 1);
    }

    public void setsNoveDi3VCompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi3VComp = MarshalByte.readPackedAsShort(buffer, position, Len.Int.NUM_S_NOVE_DI3_V_COMP, 0);
        position += Len.NUM_S_NOVE_DI3_V_COMP;
        flr22 = MarshalByte.readString(buffer, position, Len.FLR4);
    }

    public void setNumSNoveDi3VComp(short numSNoveDi3VComp) {
        this.numSNoveDi3VComp = numSNoveDi3VComp;
    }

    public short getNumSNoveDi3VComp() {
        return this.numSNoveDi3VComp;
    }

    public void setFlr22(String flr22) {
        this.flr22 = Functions.subString(flr22, Len.FLR4);
    }

    public String getFlr22() {
        return this.flr22;
    }

    public void setNumSNoveDi3V2Comp(AfDecimal numSNoveDi3V2Comp) {
        this.numSNoveDi3V2Comp.assign(numSNoveDi3V2Comp);
    }

    public AfDecimal getNumSNoveDi3V2Comp() {
        return this.numSNoveDi3V2Comp.copy();
    }

    public void setsNoveDi3V3CompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI3_V3_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI3_V3_COMP);
        setsNoveDi3V3CompBytes(buffer, 1);
    }

    public void setsNoveDi3V3CompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi3V3Comp.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.NUM_S_NOVE_DI3_V3_COMP, Len.Fract.NUM_S_NOVE_DI3_V3_COMP));
        position += Len.NUM_S_NOVE_DI3_V3_COMP;
        flr24 = MarshalByte.readString(buffer, position, Len.FLR2);
    }

    public void setNumSNoveDi3V3Comp(AfDecimal numSNoveDi3V3Comp) {
        this.numSNoveDi3V3Comp.assign(numSNoveDi3V3Comp);
    }

    public AfDecimal getNumSNoveDi3V3Comp() {
        return this.numSNoveDi3V3Comp.copy();
    }

    public void setFlr24(String flr24) {
        this.flr24 = Functions.subString(flr24, Len.FLR2);
    }

    public String getFlr24() {
        return this.flr24;
    }

    public void setNumSNoveDi4VComp(short numSNoveDi4VComp) {
        this.numSNoveDi4VComp = numSNoveDi4VComp;
    }

    public short getNumSNoveDi4VComp() {
        return this.numSNoveDi4VComp;
    }

    public void setsNoveDi5VCompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI5_V_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI5_V_COMP);
        setsNoveDi5VCompBytes(buffer, 1);
    }

    public void setsNoveDi5VCompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi5VComp = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_S_NOVE_DI5_V_COMP, 0);
        position += Len.NUM_S_NOVE_DI5_V_COMP;
        flr26 = MarshalByte.readString(buffer, position, Len.FLR2);
    }

    public void setNumSNoveDi5VComp(int numSNoveDi5VComp) {
        this.numSNoveDi5VComp = numSNoveDi5VComp;
    }

    public int getNumSNoveDi5VComp() {
        return this.numSNoveDi5VComp;
    }

    public void setFlr26(String flr26) {
        this.flr26 = Functions.subString(flr26, Len.FLR2);
    }

    public String getFlr26() {
        return this.flr26;
    }

    public void setsNoveDi5V9CompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI5_V9_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI5_V9_COMP);
        setsNoveDi5V9CompBytes(buffer, 1);
    }

    public void setsNoveDi5V9CompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi5V9Comp.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.NUM_S_NOVE_DI5_V9_COMP, Len.Fract.NUM_S_NOVE_DI5_V9_COMP));
        position += Len.NUM_S_NOVE_DI5_V9_COMP;
        flr27 = MarshalByte.readString(buffer, position, Len.FLR27);
    }

    public void setNumSNoveDi5V9Comp(AfDecimal numSNoveDi5V9Comp) {
        this.numSNoveDi5V9Comp.assign(numSNoveDi5V9Comp);
    }

    public AfDecimal getNumSNoveDi5V9Comp() {
        return this.numSNoveDi5V9Comp.copy();
    }

    public void setFlr27(String flr27) {
        this.flr27 = Functions.subString(flr27, Len.FLR27);
    }

    public String getFlr27() {
        return this.flr27;
    }

    public void setsNoveDi6VCompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI6_V_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI6_V_COMP);
        setsNoveDi6VCompBytes(buffer, 1);
    }

    public void setsNoveDi6VCompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi6VComp = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_S_NOVE_DI6_V_COMP, 0);
        position += Len.NUM_S_NOVE_DI6_V_COMP;
        flr28 = MarshalByte.readString(buffer, position, Len.FLR12);
    }

    public void setNumSNoveDi6VComp(int numSNoveDi6VComp) {
        this.numSNoveDi6VComp = numSNoveDi6VComp;
    }

    public int getNumSNoveDi6VComp() {
        return this.numSNoveDi6VComp;
    }

    public void setFlr28(String flr28) {
        this.flr28 = Functions.subString(flr28, Len.FLR12);
    }

    public String getFlr28() {
        return this.flr28;
    }

    public void setNumSNoveDi5V3Comp(AfDecimal numSNoveDi5V3Comp) {
        this.numSNoveDi5V3Comp.assign(numSNoveDi5V3Comp);
    }

    public AfDecimal getNumSNoveDi5V3Comp() {
        return this.numSNoveDi5V3Comp.copy();
    }

    public void setNumSNoveDi7V3Comp(AfDecimal numSNoveDi7V3Comp) {
        this.numSNoveDi7V3Comp.assign(numSNoveDi7V3Comp);
    }

    public AfDecimal getNumSNoveDi7V3Comp() {
        return this.numSNoveDi7V3Comp.copy();
    }

    public void setsNoveDi7V5CompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI7_V5_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI7_V5_COMP);
        setsNoveDi7V5CompBytes(buffer, 1);
    }

    public void setsNoveDi7V5CompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi7V5Comp.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.NUM_S_NOVE_DI7_V5_COMP, Len.Fract.NUM_S_NOVE_DI7_V5_COMP));
        position += Len.NUM_S_NOVE_DI7_V5_COMP;
        flr31 = MarshalByte.readString(buffer, position, Len.FLR18);
    }

    public void setNumSNoveDi7V5Comp(AfDecimal numSNoveDi7V5Comp) {
        this.numSNoveDi7V5Comp.assign(numSNoveDi7V5Comp);
    }

    public AfDecimal getNumSNoveDi7V5Comp() {
        return this.numSNoveDi7V5Comp.copy();
    }

    public void setFlr31(String flr31) {
        this.flr31 = Functions.subString(flr31, Len.FLR18);
    }

    public String getFlr31() {
        return this.flr31;
    }

    public void setNumSNoveDi7Comp(int numSNoveDi7Comp) {
        this.numSNoveDi7Comp = numSNoveDi7Comp;
    }

    public int getNumSNoveDi7Comp() {
        return this.numSNoveDi7Comp;
    }

    public void setNumSNoveDi8V3Comp(AfDecimal numSNoveDi8V3Comp) {
        this.numSNoveDi8V3Comp.assign(numSNoveDi8V3Comp);
    }

    public AfDecimal getNumSNoveDi8V3Comp() {
        return this.numSNoveDi8V3Comp.copy();
    }

    public void setsNoveDi8VCompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI8_V_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI8_V_COMP);
        setsNoveDi8VCompBytes(buffer, 1);
    }

    public void setsNoveDi8VCompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi8VComp = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_S_NOVE_DI8_V_COMP, 0);
        position += Len.NUM_S_NOVE_DI8_V_COMP;
        flr34 = MarshalByte.readString(buffer, position, Len.FLR13);
    }

    public void setNumSNoveDi8VComp(int numSNoveDi8VComp) {
        this.numSNoveDi8VComp = numSNoveDi8VComp;
    }

    public int getNumSNoveDi8VComp() {
        return this.numSNoveDi8VComp;
    }

    public void setFlr34(String flr34) {
        this.flr34 = Functions.subString(flr34, Len.FLR13);
    }

    public String getFlr34() {
        return this.flr34;
    }

    public void setsNoveDi10VCompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI10_V_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI10_V_COMP);
        setsNoveDi10VCompBytes(buffer, 1);
    }

    public void setsNoveDi10VCompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi10VComp = MarshalByte.readPackedAsLong(buffer, position, Len.Int.NUM_S_NOVE_DI10_V_COMP, 0);
        position += Len.NUM_S_NOVE_DI10_V_COMP;
        flr35 = MarshalByte.readString(buffer, position, Len.FLR15);
    }

    public void setNumSNoveDi10VComp(long numSNoveDi10VComp) {
        this.numSNoveDi10VComp = numSNoveDi10VComp;
    }

    public long getNumSNoveDi10VComp() {
        return this.numSNoveDi10VComp;
    }

    public void setFlr35(String flr35) {
        this.flr35 = Functions.subString(flr35, Len.FLR15);
    }

    public String getFlr35() {
        return this.flr35;
    }

    public void setsNoveDi9VCompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI9_V_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI9_V_COMP);
        setsNoveDi9VCompBytes(buffer, 1);
    }

    public void setsNoveDi9VCompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi9VComp = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_S_NOVE_DI9_V_COMP, 0);
        position += Len.NUM_S_NOVE_DI9_V_COMP;
        flr36 = MarshalByte.readString(buffer, position, Len.FLR13);
    }

    public void setNumSNoveDi9VComp(int numSNoveDi9VComp) {
        this.numSNoveDi9VComp = numSNoveDi9VComp;
    }

    public int getNumSNoveDi9VComp() {
        return this.numSNoveDi9VComp;
    }

    public void setFlr36(String flr36) {
        this.flr36 = Functions.subString(flr36, Len.FLR13);
    }

    public String getFlr36() {
        return this.flr36;
    }

    public void setNumSNoveDi11V3Comp(AfDecimal numSNoveDi11V3Comp) {
        this.numSNoveDi11V3Comp.assign(numSNoveDi11V3Comp);
    }

    public AfDecimal getNumSNoveDi11V3Comp() {
        return this.numSNoveDi11V3Comp.copy();
    }

    public void setsNoveDi12V3CompFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI12_V3_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI12_V3_COMP);
        setsNoveDi12V3CompBytes(buffer, 1);
    }

    public void setsNoveDi12V3CompBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi12V3Comp.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.NUM_S_NOVE_DI12_V3_COMP, Len.Fract.NUM_S_NOVE_DI12_V3_COMP));
        position += Len.NUM_S_NOVE_DI12_V3_COMP;
        flr38 = MarshalByte.readString(buffer, position, Len.FLR27);
    }

    public void setNumSNoveDi12V3Comp(AfDecimal numSNoveDi12V3Comp) {
        this.numSNoveDi12V3Comp.assign(numSNoveDi12V3Comp);
    }

    public AfDecimal getNumSNoveDi12V3Comp() {
        return this.numSNoveDi12V3Comp.copy();
    }

    public void setFlr38(String flr38) {
        this.flr38 = Functions.subString(flr38, Len.FLR27);
    }

    public String getFlr38() {
        return this.flr38;
    }

    public void setsNoveDi1VFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI1_V];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI1_V);
        setsNoveDi1VBytes(buffer, 1);
    }

    public void setsNoveDi1VBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi1V = MarshalByte.readShort(buffer, position, Len.NUM_S_NOVE_DI1_V);
        position += Len.NUM_S_NOVE_DI1_V;
        flr39 = MarshalByte.readString(buffer, position, Len.FLR1);
    }

    public void setNumSNoveDi1V(short numSNoveDi1V) {
        this.numSNoveDi1V = numSNoveDi1V;
    }

    public short getNumSNoveDi1V() {
        return this.numSNoveDi1V;
    }

    public void setFlr39(String flr39) {
        this.flr39 = Functions.subString(flr39, Len.FLR1);
    }

    public String getFlr39() {
        return this.flr39;
    }

    public void setsNoveDi2VFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI2_V];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI2_V);
        setsNoveDi2VBytes(buffer, 1);
    }

    public void setsNoveDi2VBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi2V = MarshalByte.readShort(buffer, position, Len.NUM_S_NOVE_DI2_V);
        position += Len.NUM_S_NOVE_DI2_V;
        flr40 = MarshalByte.readString(buffer, position, Len.FLR4);
    }

    public void setNumSNoveDi2V(short numSNoveDi2V) {
        this.numSNoveDi2V = numSNoveDi2V;
    }

    public short getNumSNoveDi2V() {
        return this.numSNoveDi2V;
    }

    public void setFlr40(String flr40) {
        this.flr40 = Functions.subString(flr40, Len.FLR4);
    }

    public String getFlr40() {
        return this.flr40;
    }

    public void setNumSNoveDi2V2(AfDecimal numSNoveDi2V2) {
        this.numSNoveDi2V2.assign(numSNoveDi2V2);
    }

    public AfDecimal getNumSNoveDi2V2() {
        return this.numSNoveDi2V2.copy();
    }

    public void setsNoveDi3VFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI3_V];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI3_V);
        setsNoveDi3VBytes(buffer, 1);
    }

    public void setsNoveDi3VBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi3V = MarshalByte.readShort(buffer, position, Len.NUM_S_NOVE_DI3_V);
        position += Len.NUM_S_NOVE_DI3_V;
        flr42 = MarshalByte.readString(buffer, position, Len.FLR4);
    }

    public void setNumSNoveDi3V(short numSNoveDi3V) {
        this.numSNoveDi3V = numSNoveDi3V;
    }

    public short getNumSNoveDi3V() {
        return this.numSNoveDi3V;
    }

    public void setFlr42(String flr42) {
        this.flr42 = Functions.subString(flr42, Len.FLR4);
    }

    public String getFlr42() {
        return this.flr42;
    }

    public void setNumSNoveDi3V21(AfDecimal numSNoveDi3V21) {
        this.numSNoveDi3V21.assign(numSNoveDi3V21);
    }

    public AfDecimal getNumSNoveDi3V21() {
        return this.numSNoveDi3V21.copy();
    }

    public void setsNoveDi5VFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI5_V];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI5_V);
        setsNoveDi5VBytes(buffer, 1);
    }

    public void setsNoveDi5VBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi5V = MarshalByte.readInt(buffer, position, Len.NUM_S_NOVE_DI5_V);
        position += Len.NUM_S_NOVE_DI5_V;
        flr44 = MarshalByte.readString(buffer, position, Len.FLR2);
    }

    public void setNumSNoveDi5V(int numSNoveDi5V) {
        this.numSNoveDi5V = numSNoveDi5V;
    }

    public int getNumSNoveDi5V() {
        return this.numSNoveDi5V;
    }

    public void setFlr44(String flr44) {
        this.flr44 = Functions.subString(flr44, Len.FLR2);
    }

    public String getFlr44() {
        return this.flr44;
    }

    public void setsNoveDi5V9Formatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI5_V9];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI5_V9);
        setsNoveDi5V9Bytes(buffer, 1);
    }

    public void setsNoveDi5V9Bytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi5V9.assign(MarshalByte.readDecimal(buffer, position, Len.Int.NUM_S_NOVE_DI5_V9, Len.Fract.NUM_S_NOVE_DI5_V9));
        position += Len.NUM_S_NOVE_DI5_V9;
        flr45 = MarshalByte.readString(buffer, position, Len.FLR27);
    }

    public void setNumSNoveDi5V9(AfDecimal numSNoveDi5V9) {
        this.numSNoveDi5V9.assign(numSNoveDi5V9);
    }

    public AfDecimal getNumSNoveDi5V9() {
        return this.numSNoveDi5V9.copy();
    }

    public void setFlr45(String flr45) {
        this.flr45 = Functions.subString(flr45, Len.FLR27);
    }

    public String getFlr45() {
        return this.flr45;
    }

    public void setNumSNoveDi3V22(AfDecimal numSNoveDi3V22) {
        this.numSNoveDi3V22.assign(numSNoveDi3V22);
    }

    public AfDecimal getNumSNoveDi3V22() {
        return this.numSNoveDi3V22.copy();
    }

    public void setsNoveDi3V3Formatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI3_V3];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI3_V3);
        setsNoveDi3V3Bytes(buffer, 1);
    }

    public void setsNoveDi3V3Bytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi3V3.assign(MarshalByte.readDecimal(buffer, position, Len.Int.NUM_S_NOVE_DI3_V3, Len.Fract.NUM_S_NOVE_DI3_V3));
        position += Len.NUM_S_NOVE_DI3_V3;
        flr47 = MarshalByte.readString(buffer, position, Len.FLR12);
    }

    public void setNumSNoveDi3V3(AfDecimal numSNoveDi3V3) {
        this.numSNoveDi3V3.assign(numSNoveDi3V3);
    }

    public AfDecimal getNumSNoveDi3V3() {
        return this.numSNoveDi3V3.copy();
    }

    public void setFlr47(String flr47) {
        this.flr47 = Functions.subString(flr47, Len.FLR12);
    }

    public String getFlr47() {
        return this.flr47;
    }

    public void setsNoveDi6VFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI6_V];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI6_V);
        setsNoveDi6VBytes(buffer, 1);
    }

    public void setsNoveDi6VBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi6V = MarshalByte.readInt(buffer, position, Len.NUM_S_NOVE_DI6_V);
        position += Len.NUM_S_NOVE_DI6_V;
        flr48 = MarshalByte.readString(buffer, position, Len.FLR12);
    }

    public void setNumSNoveDi6V(int numSNoveDi6V) {
        this.numSNoveDi6V = numSNoveDi6V;
    }

    public int getNumSNoveDi6V() {
        return this.numSNoveDi6V;
    }

    public void setFlr48(String flr48) {
        this.flr48 = Functions.subString(flr48, Len.FLR12);
    }

    public String getFlr48() {
        return this.flr48;
    }

    public void setsNoveDi8VFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI8_V];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI8_V);
        setsNoveDi8VBytes(buffer, 1);
    }

    public void setsNoveDi8VBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi8V = MarshalByte.readInt(buffer, position, Len.NUM_S_NOVE_DI8_V);
        position += Len.NUM_S_NOVE_DI8_V;
        flr49 = MarshalByte.readString(buffer, position, Len.FLR13);
    }

    public void setNumSNoveDi8V(int numSNoveDi8V) {
        this.numSNoveDi8V = numSNoveDi8V;
    }

    public int getNumSNoveDi8V() {
        return this.numSNoveDi8V;
    }

    public void setFlr49(String flr49) {
        this.flr49 = Functions.subString(flr49, Len.FLR13);
    }

    public String getFlr49() {
        return this.flr49;
    }

    public void setsNoveDi9VFormatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI9_V];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI9_V);
        setsNoveDi9VBytes(buffer, 1);
    }

    public void setsNoveDi9VBytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi9V = MarshalByte.readInt(buffer, position, Len.NUM_S_NOVE_DI9_V);
        position += Len.NUM_S_NOVE_DI9_V;
        flr50 = MarshalByte.readString(buffer, position, Len.FLR13);
    }

    public void setNumSNoveDi9V(int numSNoveDi9V) {
        this.numSNoveDi9V = numSNoveDi9V;
    }

    public int getNumSNoveDi9V() {
        return this.numSNoveDi9V;
    }

    public void setFlr50(String flr50) {
        this.flr50 = Functions.subString(flr50, Len.FLR13);
    }

    public String getFlr50() {
        return this.flr50;
    }

    public void setNumSNoveDi8V3(AfDecimal numSNoveDi8V3) {
        this.numSNoveDi8V3.assign(numSNoveDi8V3);
    }

    public AfDecimal getNumSNoveDi8V3() {
        return this.numSNoveDi8V3.copy();
    }

    public void setNumSNoveDi11Comp(long numSNoveDi11Comp) {
        this.numSNoveDi11Comp = numSNoveDi11Comp;
    }

    public long getNumSNoveDi11Comp() {
        return this.numSNoveDi11Comp;
    }

    public void setsNoveDi12V3Formatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI12_V3];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI12_V3);
        setsNoveDi12V3Bytes(buffer, 1);
    }

    public void setsNoveDi12V3Bytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi12V3.assign(MarshalByte.readDecimal(buffer, position, Len.Int.NUM_S_NOVE_DI12_V3, Len.Fract.NUM_S_NOVE_DI12_V3));
        position += Len.NUM_S_NOVE_DI12_V3;
        flr53 = MarshalByte.readString(buffer, position, Len.FLR53);
    }

    public void setNumSNoveDi12V3(AfDecimal numSNoveDi12V3) {
        this.numSNoveDi12V3.assign(numSNoveDi12V3);
    }

    public AfDecimal getNumSNoveDi12V3() {
        return this.numSNoveDi12V3.copy();
    }

    public void setFlr53(String flr53) {
        this.flr53 = Functions.subString(flr53, Len.FLR53);
    }

    public String getFlr53() {
        return this.flr53;
    }

    public void setsNoveDi3V7Formatted(String data) {
        byte[] buffer = new byte[Len.S_NOVE_DI3_V7];
        MarshalByte.writeString(buffer, 1, data, Len.S_NOVE_DI3_V7);
        setsNoveDi3V7Bytes(buffer, 1);
    }

    public void setsNoveDi3V7Bytes(byte[] buffer, int offset) {
        int position = offset;
        numSNoveDi3V7.assign(MarshalByte.readDecimal(buffer, position, Len.Int.NUM_S_NOVE_DI3_V7, Len.Fract.NUM_S_NOVE_DI3_V7));
        position += Len.NUM_S_NOVE_DI3_V7;
        flr54 = MarshalByte.readString(buffer, position, Len.FLR54);
    }

    public void setNumSNoveDi3V7(AfDecimal numSNoveDi3V7) {
        this.numSNoveDi3V7.assign(numSNoveDi3V7);
    }

    public AfDecimal getNumSNoveDi3V7() {
        return this.numSNoveDi3V7.copy();
    }

    public void setFlr54(String flr54) {
        this.flr54 = Functions.subString(flr54, Len.FLR54);
    }

    public String getFlr54() {
        return this.flr54;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 39;
        public static final int CHR_X_DI3 = 3;
        public static final int FLR2 = 37;
        public static final int CHR_X_DI20 = 20;
        public static final int FLR3 = 20;
        public static final int CHR_X_DI40 = 40;
        public static final int NUM_NOVE_DI2 = 2;
        public static final int FLR4 = 38;
        public static final int FLR12 = 36;
        public static final int FLR13 = 35;
        public static final int FLR15 = 34;
        public static final int FLR18 = 33;
        public static final int FLR27 = 32;
        public static final int FLR51 = 28;
        public static final int FLR53 = 24;
        public static final int FLR54 = 29;
        public static final int NUM_S_NOVE_DI1_V_COMP = 1;
        public static final int S_NOVE_DI1_V_COMP = NUM_S_NOVE_DI1_V_COMP + FLR1;
        public static final int NUM_S_NOVE_DI2_V_COMP = 2;
        public static final int S_NOVE_DI2_V_COMP = NUM_S_NOVE_DI2_V_COMP + FLR4;
        public static final int NUM_S_NOVE_DI3_V3_COMP = 4;
        public static final int S_NOVE_DI3_V3_COMP = NUM_S_NOVE_DI3_V3_COMP + FLR2;
        public static final int NUM_S_NOVE_DI3_V_COMP = 2;
        public static final int S_NOVE_DI3_V_COMP = NUM_S_NOVE_DI3_V_COMP + FLR4;
        public static final int NUM_S_NOVE_DI5_V_COMP = 3;
        public static final int S_NOVE_DI5_V_COMP = NUM_S_NOVE_DI5_V_COMP + FLR2;
        public static final int NUM_S_NOVE_DI5_V9_COMP = 8;
        public static final int S_NOVE_DI5_V9_COMP = NUM_S_NOVE_DI5_V9_COMP + FLR27;
        public static final int NUM_S_NOVE_DI6_V_COMP = 4;
        public static final int S_NOVE_DI6_V_COMP = NUM_S_NOVE_DI6_V_COMP + FLR12;
        public static final int NUM_S_NOVE_DI7_V5_COMP = 7;
        public static final int S_NOVE_DI7_V5_COMP = NUM_S_NOVE_DI7_V5_COMP + FLR18;
        public static final int NUM_S_NOVE_DI8_V_COMP = 5;
        public static final int S_NOVE_DI8_V_COMP = NUM_S_NOVE_DI8_V_COMP + FLR13;
        public static final int NUM_S_NOVE_DI9_V_COMP = 5;
        public static final int S_NOVE_DI9_V_COMP = NUM_S_NOVE_DI9_V_COMP + FLR13;
        public static final int NUM_S_NOVE_DI10_V_COMP = 6;
        public static final int S_NOVE_DI10_V_COMP = NUM_S_NOVE_DI10_V_COMP + FLR15;
        public static final int NUM_S_NOVE_DI12_V3_COMP = 8;
        public static final int S_NOVE_DI12_V3_COMP = NUM_S_NOVE_DI12_V3_COMP + FLR27;
        public static final int NUM_S_NOVE_DI1_V = 1;
        public static final int S_NOVE_DI1_V = NUM_S_NOVE_DI1_V + FLR1;
        public static final int NUM_S_NOVE_DI2_V = 2;
        public static final int S_NOVE_DI2_V = NUM_S_NOVE_DI2_V + FLR4;
        public static final int NUM_S_NOVE_DI3_V3 = 6;
        public static final int S_NOVE_DI3_V3 = NUM_S_NOVE_DI3_V3 + FLR12;
        public static final int NUM_S_NOVE_DI3_V = 3;
        public static final int S_NOVE_DI3_V = NUM_S_NOVE_DI3_V + FLR4;
        public static final int NUM_S_NOVE_DI3_V7 = 10;
        public static final int S_NOVE_DI3_V7 = NUM_S_NOVE_DI3_V7 + FLR54;
        public static final int NUM_S_NOVE_DI5_V = 5;
        public static final int S_NOVE_DI5_V = NUM_S_NOVE_DI5_V + FLR2;
        public static final int NUM_S_NOVE_DI5_V9 = 14;
        public static final int S_NOVE_DI5_V9 = NUM_S_NOVE_DI5_V9 + FLR27;
        public static final int NUM_S_NOVE_DI6_V = 6;
        public static final int S_NOVE_DI6_V = NUM_S_NOVE_DI6_V + FLR12;
        public static final int NUM_S_NOVE_DI8_V = 8;
        public static final int S_NOVE_DI8_V = NUM_S_NOVE_DI8_V + FLR13;
        public static final int NUM_S_NOVE_DI9_V = 9;
        public static final int S_NOVE_DI9_V = NUM_S_NOVE_DI9_V + FLR13;
        public static final int NUM_S_NOVE_DI12_V3 = 15;
        public static final int S_NOVE_DI12_V3 = NUM_S_NOVE_DI12_V3 + FLR53;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int NUM_S_NOVE_DI2_V_COMP = 2;
            public static final int NUM_S_NOVE_DI3_V_COMP = 3;
            public static final int NUM_S_NOVE_DI3_V3_COMP = 3;
            public static final int NUM_S_NOVE_DI5_V_COMP = 5;
            public static final int NUM_S_NOVE_DI5_V9_COMP = 5;
            public static final int NUM_S_NOVE_DI6_V_COMP = 6;
            public static final int NUM_S_NOVE_DI7_V5_COMP = 7;
            public static final int NUM_S_NOVE_DI8_V_COMP = 8;
            public static final int NUM_S_NOVE_DI10_V_COMP = 10;
            public static final int NUM_S_NOVE_DI9_V_COMP = 9;
            public static final int NUM_S_NOVE_DI12_V3_COMP = 12;
            public static final int NUM_S_NOVE_DI5_V9 = 5;
            public static final int NUM_S_NOVE_DI3_V3 = 3;
            public static final int NUM_S_NOVE_DI12_V3 = 12;
            public static final int NUM_S_NOVE_DI3_V7 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int NUM_S_NOVE_DI3_V3_COMP = 3;
            public static final int NUM_S_NOVE_DI5_V9_COMP = 9;
            public static final int NUM_S_NOVE_DI7_V5_COMP = 5;
            public static final int NUM_S_NOVE_DI12_V3_COMP = 3;
            public static final int NUM_S_NOVE_DI5_V9 = 9;
            public static final int NUM_S_NOVE_DI3_V3 = 3;
            public static final int NUM_S_NOVE_DI12_V3 = 3;
            public static final int NUM_S_NOVE_DI3_V7 = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
