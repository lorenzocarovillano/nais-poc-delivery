package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvnot1;
import it.accenture.jnais.copy.WnotDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WNOT-AREA-NOTE-OGG<br>
 * Variable: WNOT-AREA-NOTE-OGG from program LCCS0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WnotAreaNoteOgg extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WNOT-ELE-NOTE-OGG-MAX
    private short wnotEleNoteOggMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVNOT1
    private Lccvnot1 lccvnot1 = new Lccvnot1();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WNOT_AREA_NOTE_OGG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWnotAreaNoteOggBytes(buf);
    }

    public String getWnotAreaNoteOggFormatted() {
        return MarshalByteExt.bufferToStr(getWnotAreaNoteOggBytes());
    }

    public void setWnotAreaNoteOggBytes(byte[] buffer) {
        setWnotAreaNoteOggBytes(buffer, 1);
    }

    public byte[] getWnotAreaNoteOggBytes() {
        byte[] buffer = new byte[Len.WNOT_AREA_NOTE_OGG];
        return getWnotAreaNoteOggBytes(buffer, 1);
    }

    public void setWnotAreaNoteOggBytes(byte[] buffer, int offset) {
        int position = offset;
        wnotEleNoteOggMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWnotTabNoteOggBytes(buffer, position);
    }

    public byte[] getWnotAreaNoteOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wnotEleNoteOggMax);
        position += Types.SHORT_SIZE;
        getWnotTabNoteOggBytes(buffer, position);
        return buffer;
    }

    public void setWnotEleNoteOggMax(short wnotEleNoteOggMax) {
        this.wnotEleNoteOggMax = wnotEleNoteOggMax;
    }

    public short getWnotEleNoteOggMax() {
        return this.wnotEleNoteOggMax;
    }

    public void setWnotTabNoteOggBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvnot1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvnot1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvnot1.Len.Int.ID_PTF, 0));
        position += Lccvnot1.Len.ID_PTF;
        lccvnot1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWnotTabNoteOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvnot1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvnot1.getIdPtf(), Lccvnot1.Len.Int.ID_PTF, 0);
        position += Lccvnot1.Len.ID_PTF;
        lccvnot1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public Lccvnot1 getLccvnot1() {
        return lccvnot1;
    }

    @Override
    public byte[] serialize() {
        return getWnotAreaNoteOggBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WNOT_ELE_NOTE_OGG_MAX = 2;
        public static final int WNOT_TAB_NOTE_OGG = WpolStatus.Len.STATUS + Lccvnot1.Len.ID_PTF + WnotDati.Len.DATI;
        public static final int WNOT_AREA_NOTE_OGG = WNOT_ELE_NOTE_OGG_MAX + WNOT_TAB_NOTE_OGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
