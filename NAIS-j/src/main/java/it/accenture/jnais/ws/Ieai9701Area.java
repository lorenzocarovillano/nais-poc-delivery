package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IEAI9701-AREA<br>
 * Variable: IEAI9701-AREA from copybook IEAI9701<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ieai9701Area extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IEAI9701-ID-GRAVITA-ERRORE
    private String ieai9701IdGravitaErrore = DefaultValues.stringVal(Len.IEAI9701_ID_GRAVITA_ERRORE);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IEAI9701_AREA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIeai9701AreaBytes(buf);
    }

    public String getIeai9701AreaFormatted() {
        return getIeai9701IdGravitaErroreFormatted();
    }

    public void setIeai9701AreaBytes(byte[] buffer) {
        setIeai9701AreaBytes(buffer, 1);
    }

    public byte[] getIeai9701AreaBytes() {
        byte[] buffer = new byte[Len.IEAI9701_AREA];
        return getIeai9701AreaBytes(buffer, 1);
    }

    public void setIeai9701AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        ieai9701IdGravitaErrore = MarshalByte.readFixedString(buffer, position, Len.IEAI9701_ID_GRAVITA_ERRORE);
    }

    public byte[] getIeai9701AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ieai9701IdGravitaErrore, Len.IEAI9701_ID_GRAVITA_ERRORE);
        return buffer;
    }

    public void initIeai9701AreaSpaces() {
        ieai9701IdGravitaErrore = "";
    }

    public void setIeai9701IdGravitaErrore(int ieai9701IdGravitaErrore) {
        this.ieai9701IdGravitaErrore = NumericDisplay.asString(ieai9701IdGravitaErrore, Len.IEAI9701_ID_GRAVITA_ERRORE);
    }

    public int getIeai9701IdGravitaErrore() {
        return NumericDisplay.asInt(this.ieai9701IdGravitaErrore);
    }

    public String getIeai9701IdGravitaErroreFormatted() {
        return this.ieai9701IdGravitaErrore;
    }

    @Override
    public byte[] serialize() {
        return getIeai9701AreaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IEAI9701_ID_GRAVITA_ERRORE = 9;
        public static final int IEAI9701_AREA = IEAI9701_ID_GRAVITA_ERRORE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
