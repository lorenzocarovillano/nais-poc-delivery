package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP85-COMMIS-GEST<br>
 * Variable: WP85-COMMIS-GEST from program LRGS0660<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp85CommisGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp85CommisGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP85_COMMIS_GEST;
    }

    public void setWp85CommisGest(AfDecimal wp85CommisGest) {
        writeDecimalAsPacked(Pos.WP85_COMMIS_GEST, wp85CommisGest.copy());
    }

    /**Original name: WP85-COMMIS-GEST<br>*/
    public AfDecimal getWp85CommisGest() {
        return readPackedAsDecimal(Pos.WP85_COMMIS_GEST, Len.Int.WP85_COMMIS_GEST, Len.Fract.WP85_COMMIS_GEST);
    }

    public void setWp85CommisGestNull(String wp85CommisGestNull) {
        writeString(Pos.WP85_COMMIS_GEST_NULL, wp85CommisGestNull, Len.WP85_COMMIS_GEST_NULL);
    }

    /**Original name: WP85-COMMIS-GEST-NULL<br>*/
    public String getWp85CommisGestNull() {
        return readString(Pos.WP85_COMMIS_GEST_NULL, Len.WP85_COMMIS_GEST_NULL);
    }

    public String getWp85CommisGestNullFormatted() {
        return Functions.padBlanks(getWp85CommisGestNull(), Len.WP85_COMMIS_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP85_COMMIS_GEST = 1;
        public static final int WP85_COMMIS_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP85_COMMIS_GEST = 8;
        public static final int WP85_COMMIS_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP85_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP85_COMMIS_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
