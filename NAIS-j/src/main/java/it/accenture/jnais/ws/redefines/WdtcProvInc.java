package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-PROV-INC<br>
 * Variable: WDTC-PROV-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_PROV_INC;
    }

    public void setWdtcProvInc(AfDecimal wdtcProvInc) {
        writeDecimalAsPacked(Pos.WDTC_PROV_INC, wdtcProvInc.copy());
    }

    public void setWdtcProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_PROV_INC, Pos.WDTC_PROV_INC);
    }

    /**Original name: WDTC-PROV-INC<br>*/
    public AfDecimal getWdtcProvInc() {
        return readPackedAsDecimal(Pos.WDTC_PROV_INC, Len.Int.WDTC_PROV_INC, Len.Fract.WDTC_PROV_INC);
    }

    public byte[] getWdtcProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_PROV_INC, Pos.WDTC_PROV_INC);
        return buffer;
    }

    public void initWdtcProvIncSpaces() {
        fill(Pos.WDTC_PROV_INC, Len.WDTC_PROV_INC, Types.SPACE_CHAR);
    }

    public void setWdtcProvIncNull(String wdtcProvIncNull) {
        writeString(Pos.WDTC_PROV_INC_NULL, wdtcProvIncNull, Len.WDTC_PROV_INC_NULL);
    }

    /**Original name: WDTC-PROV-INC-NULL<br>*/
    public String getWdtcProvIncNull() {
        return readString(Pos.WDTC_PROV_INC_NULL, Len.WDTC_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_PROV_INC = 1;
        public static final int WDTC_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_PROV_INC = 8;
        public static final int WDTC_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
