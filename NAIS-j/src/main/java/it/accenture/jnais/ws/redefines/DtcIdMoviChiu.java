package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-ID-MOVI-CHIU<br>
 * Variable: DTC-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_ID_MOVI_CHIU;
    }

    public void setDtcIdMoviChiu(int dtcIdMoviChiu) {
        writeIntAsPacked(Pos.DTC_ID_MOVI_CHIU, dtcIdMoviChiu, Len.Int.DTC_ID_MOVI_CHIU);
    }

    public void setDtcIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_ID_MOVI_CHIU, Pos.DTC_ID_MOVI_CHIU);
    }

    /**Original name: DTC-ID-MOVI-CHIU<br>*/
    public int getDtcIdMoviChiu() {
        return readPackedAsInt(Pos.DTC_ID_MOVI_CHIU, Len.Int.DTC_ID_MOVI_CHIU);
    }

    public byte[] getDtcIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_ID_MOVI_CHIU, Pos.DTC_ID_MOVI_CHIU);
        return buffer;
    }

    public void setDtcIdMoviChiuNull(String dtcIdMoviChiuNull) {
        writeString(Pos.DTC_ID_MOVI_CHIU_NULL, dtcIdMoviChiuNull, Len.DTC_ID_MOVI_CHIU_NULL);
    }

    /**Original name: DTC-ID-MOVI-CHIU-NULL<br>*/
    public String getDtcIdMoviChiuNull() {
        return readString(Pos.DTC_ID_MOVI_CHIU_NULL, Len.DTC_ID_MOVI_CHIU_NULL);
    }

    public String getDtcIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getDtcIdMoviChiuNull(), Len.DTC_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_ID_MOVI_CHIU = 1;
        public static final int DTC_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_ID_MOVI_CHIU = 5;
        public static final int DTC_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
