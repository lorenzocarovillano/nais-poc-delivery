package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.WkAppoBlobData;

/**Original name: WK-APPO-DATA-JOB<br>
 * Variable: WK-APPO-DATA-JOB from program LRGM0380<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WkAppoDataJob extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int BLOB_DATA_MAXOCCURS = 1000;
    //Original name: WK-APPO-ELE-MAX
    private String eleMax = DefaultValues.stringVal(Len.ELE_MAX);
    //Original name: WK-APPO-BLOB-DATA
    private LazyArrayCopy<WkAppoBlobData> blobData = new LazyArrayCopy<WkAppoBlobData>(new WkAppoBlobData(), 1, BLOB_DATA_MAXOCCURS);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_APPO_DATA_JOB;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWkAppoDataJobBytes(buf);
    }

    public String getWkAppoDataJobFormatted() {
        return MarshalByteExt.bufferToStr(getWkAppoDataJobBytes());
    }

    public void setWkAppoDataJobBytes(byte[] buffer) {
        setWkAppoDataJobBytes(buffer, 1);
    }

    public byte[] getWkAppoDataJobBytes() {
        byte[] buffer = new byte[Len.WK_APPO_DATA_JOB];
        return getWkAppoDataJobBytes(buffer, 1);
    }

    public void setWkAppoDataJobBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMax = MarshalByte.readFixedString(buffer, position, Len.ELE_MAX);
        position += Len.ELE_MAX;
        for (int idx = 1; idx <= BLOB_DATA_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                blobData.get(idx - 1).setBlobDataBytes(buffer, position);
                position += WkAppoBlobData.Len.BLOB_DATA;
            }
            else {
                WkAppoBlobData temp_blobData = new WkAppoBlobData();
                temp_blobData.initBlobDataSpaces();
                getBlobDataObj().fill(temp_blobData);
                position += WkAppoBlobData.Len.BLOB_DATA * (BLOB_DATA_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public byte[] getWkAppoDataJobBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, eleMax, Len.ELE_MAX);
        position += Len.ELE_MAX;
        for (int idx = 1; idx <= BLOB_DATA_MAXOCCURS; idx++) {
            blobData.get(idx - 1).getBlobDataBytes(buffer, position);
            position += WkAppoBlobData.Len.BLOB_DATA;
        }
        return buffer;
    }

    public void setEleMax(short eleMax) {
        this.eleMax = NumericDisplay.asString(eleMax, Len.ELE_MAX);
    }

    public void setEleMaxFormatted(String eleMax) {
        this.eleMax = Trunc.toUnsignedNumeric(eleMax, Len.ELE_MAX);
    }

    public short getEleMax() {
        return NumericDisplay.asShort(this.eleMax);
    }

    public WkAppoBlobData getBlobData(int idx) {
        return blobData.get(idx - 1);
    }

    public LazyArrayCopy<WkAppoBlobData> getBlobDataObj() {
        return blobData;
    }

    @Override
    public byte[] serialize() {
        return getWkAppoDataJobBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_MAX = 3;
        public static final int WK_APPO_DATA_JOB = ELE_MAX + WkAppoDataJob.BLOB_DATA_MAXOCCURS * WkAppoBlobData.Len.BLOB_DATA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
