package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-PROV-RICOR<br>
 * Variable: DTC-PROV-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_PROV_RICOR;
    }

    public void setDtcProvRicor(AfDecimal dtcProvRicor) {
        writeDecimalAsPacked(Pos.DTC_PROV_RICOR, dtcProvRicor.copy());
    }

    public void setDtcProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_PROV_RICOR, Pos.DTC_PROV_RICOR);
    }

    /**Original name: DTC-PROV-RICOR<br>*/
    public AfDecimal getDtcProvRicor() {
        return readPackedAsDecimal(Pos.DTC_PROV_RICOR, Len.Int.DTC_PROV_RICOR, Len.Fract.DTC_PROV_RICOR);
    }

    public byte[] getDtcProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_PROV_RICOR, Pos.DTC_PROV_RICOR);
        return buffer;
    }

    public void setDtcProvRicorNull(String dtcProvRicorNull) {
        writeString(Pos.DTC_PROV_RICOR_NULL, dtcProvRicorNull, Len.DTC_PROV_RICOR_NULL);
    }

    /**Original name: DTC-PROV-RICOR-NULL<br>*/
    public String getDtcProvRicorNull() {
        return readString(Pos.DTC_PROV_RICOR_NULL, Len.DTC_PROV_RICOR_NULL);
    }

    public String getDtcProvRicorNullFormatted() {
        return Functions.padBlanks(getDtcProvRicorNull(), Len.DTC_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_PROV_RICOR = 1;
        public static final int DTC_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_PROV_RICOR = 8;
        public static final int DTC_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
