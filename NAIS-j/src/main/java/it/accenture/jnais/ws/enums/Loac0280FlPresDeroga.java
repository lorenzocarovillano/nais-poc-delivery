package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LOAC0280-FL-PRES-DEROGA<br>
 * Variable: LOAC0280-FL-PRES-DEROGA from copybook LOAC0280<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Loac0280FlPresDeroga {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_DEROGA = 'S';
    public static final char NO_DEROGA = 'N';

    //==== METHODS ====
    public void setFlPresDeroga(char flPresDeroga) {
        this.value = flPresDeroga;
    }

    public char getFlPresDeroga() {
        return this.value;
    }

    public void setSiDeroga() {
        value = SI_DEROGA;
    }

    public void setNoDeroga() {
        value = NO_DEROGA;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_PRES_DEROGA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
