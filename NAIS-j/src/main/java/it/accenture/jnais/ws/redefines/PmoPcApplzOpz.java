package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-PC-APPLZ-OPZ<br>
 * Variable: PMO-PC-APPLZ-OPZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoPcApplzOpz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoPcApplzOpz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_PC_APPLZ_OPZ;
    }

    public void setPmoPcApplzOpz(AfDecimal pmoPcApplzOpz) {
        writeDecimalAsPacked(Pos.PMO_PC_APPLZ_OPZ, pmoPcApplzOpz.copy());
    }

    public void setPmoPcApplzOpzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_PC_APPLZ_OPZ, Pos.PMO_PC_APPLZ_OPZ);
    }

    /**Original name: PMO-PC-APPLZ-OPZ<br>*/
    public AfDecimal getPmoPcApplzOpz() {
        return readPackedAsDecimal(Pos.PMO_PC_APPLZ_OPZ, Len.Int.PMO_PC_APPLZ_OPZ, Len.Fract.PMO_PC_APPLZ_OPZ);
    }

    public byte[] getPmoPcApplzOpzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_PC_APPLZ_OPZ, Pos.PMO_PC_APPLZ_OPZ);
        return buffer;
    }

    public void initPmoPcApplzOpzHighValues() {
        fill(Pos.PMO_PC_APPLZ_OPZ, Len.PMO_PC_APPLZ_OPZ, Types.HIGH_CHAR_VAL);
    }

    public void setPmoPcApplzOpzNull(String pmoPcApplzOpzNull) {
        writeString(Pos.PMO_PC_APPLZ_OPZ_NULL, pmoPcApplzOpzNull, Len.PMO_PC_APPLZ_OPZ_NULL);
    }

    /**Original name: PMO-PC-APPLZ-OPZ-NULL<br>*/
    public String getPmoPcApplzOpzNull() {
        return readString(Pos.PMO_PC_APPLZ_OPZ_NULL, Len.PMO_PC_APPLZ_OPZ_NULL);
    }

    public String getPmoPcApplzOpzNullFormatted() {
        return Functions.padBlanks(getPmoPcApplzOpzNull(), Len.PMO_PC_APPLZ_OPZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_PC_APPLZ_OPZ = 1;
        public static final int PMO_PC_APPLZ_OPZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_PC_APPLZ_OPZ = 4;
        public static final int PMO_PC_APPLZ_OPZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_PC_APPLZ_OPZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_PC_APPLZ_OPZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
