package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-MOVI-ORI-COLL<br>
 * Variable: WS-MOVI-ORI-COLL from program LVVS2800<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsMoviOriColl {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WS_MOVI_ORI_COLL);
    public static final String LIQUI_RISPAR_ADE = "05008";
    public static final String LIQUI_RISTOT_ADE = "05009";
    public static final String LIQUI_SINADE = "05017";
    public static final String SCANT_ADESIO = "05014";
    public static final String LIQUI_CEDOLE = "06018";

    //==== METHODS ====
    public void setWsMoviOriCollFormatted(String wsMoviOriColl) {
        this.value = Trunc.toUnsignedNumeric(wsMoviOriColl, Len.WS_MOVI_ORI_COLL);
    }

    public int getWsMoviOriColl() {
        return NumericDisplay.asInt(this.value);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_MOVI_ORI_COLL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
