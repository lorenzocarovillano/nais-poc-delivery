package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMP-RIT-TFR<br>
 * Variable: LQU-TOT-IMP-RIT-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpRitTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpRitTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMP_RIT_TFR;
    }

    public void setLquTotImpRitTfr(AfDecimal lquTotImpRitTfr) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMP_RIT_TFR, lquTotImpRitTfr.copy());
    }

    public void setLquTotImpRitTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMP_RIT_TFR, Pos.LQU_TOT_IMP_RIT_TFR);
    }

    /**Original name: LQU-TOT-IMP-RIT-TFR<br>*/
    public AfDecimal getLquTotImpRitTfr() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMP_RIT_TFR, Len.Int.LQU_TOT_IMP_RIT_TFR, Len.Fract.LQU_TOT_IMP_RIT_TFR);
    }

    public byte[] getLquTotImpRitTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMP_RIT_TFR, Pos.LQU_TOT_IMP_RIT_TFR);
        return buffer;
    }

    public void setLquTotImpRitTfrNull(String lquTotImpRitTfrNull) {
        writeString(Pos.LQU_TOT_IMP_RIT_TFR_NULL, lquTotImpRitTfrNull, Len.LQU_TOT_IMP_RIT_TFR_NULL);
    }

    /**Original name: LQU-TOT-IMP-RIT-TFR-NULL<br>*/
    public String getLquTotImpRitTfrNull() {
        return readString(Pos.LQU_TOT_IMP_RIT_TFR_NULL, Len.LQU_TOT_IMP_RIT_TFR_NULL);
    }

    public String getLquTotImpRitTfrNullFormatted() {
        return Functions.padBlanks(getLquTotImpRitTfrNull(), Len.LQU_TOT_IMP_RIT_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_RIT_TFR = 1;
        public static final int LQU_TOT_IMP_RIT_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_RIT_TFR = 8;
        public static final int LQU_TOT_IMP_RIT_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_RIT_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_RIT_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
