package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ALQ-MARG-RIS<br>
 * Variable: B03-ALQ-MARG-RIS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03AlqMargRis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03AlqMargRis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ALQ_MARG_RIS;
    }

    public void setB03AlqMargRis(AfDecimal b03AlqMargRis) {
        writeDecimalAsPacked(Pos.B03_ALQ_MARG_RIS, b03AlqMargRis.copy());
    }

    public void setB03AlqMargRisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ALQ_MARG_RIS, Pos.B03_ALQ_MARG_RIS);
    }

    /**Original name: B03-ALQ-MARG-RIS<br>*/
    public AfDecimal getB03AlqMargRis() {
        return readPackedAsDecimal(Pos.B03_ALQ_MARG_RIS, Len.Int.B03_ALQ_MARG_RIS, Len.Fract.B03_ALQ_MARG_RIS);
    }

    public byte[] getB03AlqMargRisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ALQ_MARG_RIS, Pos.B03_ALQ_MARG_RIS);
        return buffer;
    }

    public void setB03AlqMargRisNull(String b03AlqMargRisNull) {
        writeString(Pos.B03_ALQ_MARG_RIS_NULL, b03AlqMargRisNull, Len.B03_ALQ_MARG_RIS_NULL);
    }

    /**Original name: B03-ALQ-MARG-RIS-NULL<br>*/
    public String getB03AlqMargRisNull() {
        return readString(Pos.B03_ALQ_MARG_RIS_NULL, Len.B03_ALQ_MARG_RIS_NULL);
    }

    public String getB03AlqMargRisNullFormatted() {
        return Functions.padBlanks(getB03AlqMargRisNull(), Len.B03_ALQ_MARG_RIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ALQ_MARG_RIS = 1;
        public static final int B03_ALQ_MARG_RIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ALQ_MARG_RIS = 4;
        public static final int B03_ALQ_MARG_RIS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_ALQ_MARG_RIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ALQ_MARG_RIS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
