package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRSTZ-AGG-INI<br>
 * Variable: TGA-PRSTZ-AGG-INI from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPrstzAggIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPrstzAggIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRSTZ_AGG_INI;
    }

    public void setTgaPrstzAggIni(AfDecimal tgaPrstzAggIni) {
        writeDecimalAsPacked(Pos.TGA_PRSTZ_AGG_INI, tgaPrstzAggIni.copy());
    }

    public void setTgaPrstzAggIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRSTZ_AGG_INI, Pos.TGA_PRSTZ_AGG_INI);
    }

    /**Original name: TGA-PRSTZ-AGG-INI<br>*/
    public AfDecimal getTgaPrstzAggIni() {
        return readPackedAsDecimal(Pos.TGA_PRSTZ_AGG_INI, Len.Int.TGA_PRSTZ_AGG_INI, Len.Fract.TGA_PRSTZ_AGG_INI);
    }

    public byte[] getTgaPrstzAggIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRSTZ_AGG_INI, Pos.TGA_PRSTZ_AGG_INI);
        return buffer;
    }

    public void setTgaPrstzAggIniNull(String tgaPrstzAggIniNull) {
        writeString(Pos.TGA_PRSTZ_AGG_INI_NULL, tgaPrstzAggIniNull, Len.TGA_PRSTZ_AGG_INI_NULL);
    }

    /**Original name: TGA-PRSTZ-AGG-INI-NULL<br>*/
    public String getTgaPrstzAggIniNull() {
        return readString(Pos.TGA_PRSTZ_AGG_INI_NULL, Len.TGA_PRSTZ_AGG_INI_NULL);
    }

    public String getTgaPrstzAggIniNullFormatted() {
        return Functions.padBlanks(getTgaPrstzAggIniNull(), Len.TGA_PRSTZ_AGG_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_AGG_INI = 1;
        public static final int TGA_PRSTZ_AGG_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_AGG_INI = 8;
        public static final int TGA_PRSTZ_AGG_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_AGG_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_AGG_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
