package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-PASPAS<br>
 * Variable: WPCO-DT-ULT-ELAB-PASPAS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabPaspas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabPaspas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_PASPAS;
    }

    public void setWpcoDtUltElabPaspas(int wpcoDtUltElabPaspas) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_PASPAS, wpcoDtUltElabPaspas, Len.Int.WPCO_DT_ULT_ELAB_PASPAS);
    }

    public void setDpcoDtUltElabPaspasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_PASPAS, Pos.WPCO_DT_ULT_ELAB_PASPAS);
    }

    /**Original name: WPCO-DT-ULT-ELAB-PASPAS<br>*/
    public int getWpcoDtUltElabPaspas() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_PASPAS, Len.Int.WPCO_DT_ULT_ELAB_PASPAS);
    }

    public byte[] getWpcoDtUltElabPaspasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_PASPAS, Pos.WPCO_DT_ULT_ELAB_PASPAS);
        return buffer;
    }

    public void setWpcoDtUltElabPaspasNull(String wpcoDtUltElabPaspasNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_PASPAS_NULL, wpcoDtUltElabPaspasNull, Len.WPCO_DT_ULT_ELAB_PASPAS_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-PASPAS-NULL<br>*/
    public String getWpcoDtUltElabPaspasNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_PASPAS_NULL, Len.WPCO_DT_ULT_ELAB_PASPAS_NULL);
    }

    public String getWpcoDtUltElabPaspasNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabPaspasNull(), Len.WPCO_DT_ULT_ELAB_PASPAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_PASPAS = 1;
        public static final int WPCO_DT_ULT_ELAB_PASPAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_PASPAS = 5;
        public static final int WPCO_DT_ULT_ELAB_PASPAS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_PASPAS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
