package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0503-POS-SEGNO<br>
 * Variable: IDSV0503-POS-SEGNO from copybook IDSV0503<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0503PosSegno {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char ANTERIORE = 'A';
    public static final char POSTERIORE = 'P';

    //==== METHODS ====
    public void setIdsv0503PosSegno(char idsv0503PosSegno) {
        this.value = idsv0503PosSegno;
    }

    public char getIdsv0503PosSegno() {
        return this.value;
    }

    public boolean isAnteriore() {
        return value == ANTERIORE;
    }

    public void setAnteriore() {
        value = ANTERIORE;
    }

    public boolean isPosteriore() {
        return value == POSTERIORE;
    }
}
