package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WSDI-PC-PROTEZIONE<br>
 * Variable: WSDI-PC-PROTEZIONE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsdiPcProtezione extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsdiPcProtezione() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSDI_PC_PROTEZIONE;
    }

    public void setWsdiPcProtezione(AfDecimal wsdiPcProtezione) {
        writeDecimalAsPacked(Pos.WSDI_PC_PROTEZIONE, wsdiPcProtezione.copy());
    }

    public void setWsdiPcProtezioneFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSDI_PC_PROTEZIONE, Pos.WSDI_PC_PROTEZIONE);
    }

    /**Original name: WSDI-PC-PROTEZIONE<br>*/
    public AfDecimal getWsdiPcProtezione() {
        return readPackedAsDecimal(Pos.WSDI_PC_PROTEZIONE, Len.Int.WSDI_PC_PROTEZIONE, Len.Fract.WSDI_PC_PROTEZIONE);
    }

    public byte[] getWsdiPcProtezioneAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSDI_PC_PROTEZIONE, Pos.WSDI_PC_PROTEZIONE);
        return buffer;
    }

    public void initWsdiPcProtezioneSpaces() {
        fill(Pos.WSDI_PC_PROTEZIONE, Len.WSDI_PC_PROTEZIONE, Types.SPACE_CHAR);
    }

    public void setWsdiPcProtezioneNull(String wsdiPcProtezioneNull) {
        writeString(Pos.WSDI_PC_PROTEZIONE_NULL, wsdiPcProtezioneNull, Len.WSDI_PC_PROTEZIONE_NULL);
    }

    /**Original name: WSDI-PC-PROTEZIONE-NULL<br>*/
    public String getWsdiPcProtezioneNull() {
        return readString(Pos.WSDI_PC_PROTEZIONE_NULL, Len.WSDI_PC_PROTEZIONE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSDI_PC_PROTEZIONE = 1;
        public static final int WSDI_PC_PROTEZIONE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WSDI_PC_PROTEZIONE = 4;
        public static final int WSDI_PC_PROTEZIONE_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WSDI_PC_PROTEZIONE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WSDI_PC_PROTEZIONE = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
