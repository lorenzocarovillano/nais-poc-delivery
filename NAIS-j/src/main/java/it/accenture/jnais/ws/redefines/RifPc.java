package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIF-PC<br>
 * Variable: RIF-PC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RifPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RifPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIF_PC;
    }

    public void setRifPc(AfDecimal rifPc) {
        writeDecimalAsPacked(Pos.RIF_PC, rifPc.copy());
    }

    public void setRifPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIF_PC, Pos.RIF_PC);
    }

    /**Original name: RIF-PC<br>*/
    public AfDecimal getRifPc() {
        return readPackedAsDecimal(Pos.RIF_PC, Len.Int.RIF_PC, Len.Fract.RIF_PC);
    }

    public byte[] getRifPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIF_PC, Pos.RIF_PC);
        return buffer;
    }

    public void setRifPcNull(String rifPcNull) {
        writeString(Pos.RIF_PC_NULL, rifPcNull, Len.RIF_PC_NULL);
    }

    /**Original name: RIF-PC-NULL<br>*/
    public String getRifPcNull() {
        return readString(Pos.RIF_PC_NULL, Len.RIF_PC_NULL);
    }

    public String getRifPcNullFormatted() {
        return Functions.padBlanks(getRifPcNull(), Len.RIF_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIF_PC = 1;
        public static final int RIF_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIF_PC = 4;
        public static final int RIF_PC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIF_PC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RIF_PC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
