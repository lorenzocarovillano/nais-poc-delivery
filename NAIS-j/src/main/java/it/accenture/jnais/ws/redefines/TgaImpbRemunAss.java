package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMPB-REMUN-ASS<br>
 * Variable: TGA-IMPB-REMUN-ASS from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpbRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpbRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMPB_REMUN_ASS;
    }

    public void setTgaImpbRemunAss(AfDecimal tgaImpbRemunAss) {
        writeDecimalAsPacked(Pos.TGA_IMPB_REMUN_ASS, tgaImpbRemunAss.copy());
    }

    public void setTgaImpbRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMPB_REMUN_ASS, Pos.TGA_IMPB_REMUN_ASS);
    }

    /**Original name: TGA-IMPB-REMUN-ASS<br>*/
    public AfDecimal getTgaImpbRemunAss() {
        return readPackedAsDecimal(Pos.TGA_IMPB_REMUN_ASS, Len.Int.TGA_IMPB_REMUN_ASS, Len.Fract.TGA_IMPB_REMUN_ASS);
    }

    public byte[] getTgaImpbRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMPB_REMUN_ASS, Pos.TGA_IMPB_REMUN_ASS);
        return buffer;
    }

    public void setTgaImpbRemunAssNull(String tgaImpbRemunAssNull) {
        writeString(Pos.TGA_IMPB_REMUN_ASS_NULL, tgaImpbRemunAssNull, Len.TGA_IMPB_REMUN_ASS_NULL);
    }

    /**Original name: TGA-IMPB-REMUN-ASS-NULL<br>*/
    public String getTgaImpbRemunAssNull() {
        return readString(Pos.TGA_IMPB_REMUN_ASS_NULL, Len.TGA_IMPB_REMUN_ASS_NULL);
    }

    public String getTgaImpbRemunAssNullFormatted() {
        return Functions.padBlanks(getTgaImpbRemunAssNull(), Len.TGA_IMPB_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_REMUN_ASS = 1;
        public static final int TGA_IMPB_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_REMUN_ASS = 8;
        public static final int TGA_IMPB_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
