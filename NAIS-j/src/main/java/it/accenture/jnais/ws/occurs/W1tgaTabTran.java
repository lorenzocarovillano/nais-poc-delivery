package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.copy.Lccvtga1Ivvs0216;
import it.accenture.jnais.copy.WtgaDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: W1TGA-TAB-TRAN<br>
 * Variables: W1TGA-TAB-TRAN from copybook LCCVTGAC<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class W1tgaTabTran implements ICopyable<W1tgaTabTran> {

    //==== PROPERTIES ====
    //Original name: LCCVTGA1
    private Lccvtga1Ivvs0216 lccvtga1 = new Lccvtga1Ivvs0216();

    //==== CONSTRUCTORS ====
    public W1tgaTabTran() {
    }

    public W1tgaTabTran(W1tgaTabTran w1tgaTabTran) {
        this();
        this.lccvtga1 = ((Lccvtga1Ivvs0216)w1tgaTabTran.lccvtga1.copy());
    }

    //==== METHODS ====
    public byte[] getW1tgaTabTranBytes() {
        byte[] buffer = new byte[Len.W1TGA_TAB_TRAN];
        return getW1tgaTabTranBytes(buffer, 1);
    }

    public void setW1tgaTabTranBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvtga1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvtga1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvtga1Ivvs0216.Len.Int.ID_PTF, 0));
        position += Lccvtga1Ivvs0216.Len.ID_PTF;
        lccvtga1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getW1tgaTabTranBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvtga1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvtga1.getIdPtf(), Lccvtga1Ivvs0216.Len.Int.ID_PTF, 0);
        position += Lccvtga1Ivvs0216.Len.ID_PTF;
        lccvtga1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public W1tgaTabTran initW1tgaTabTranSpaces() {
        lccvtga1.initLccvtga1Spaces();
        return this;
    }

    public Lccvtga1Ivvs0216 getLccvtga1() {
        return lccvtga1;
    }

    public W1tgaTabTran copy() {
        return new W1tgaTabTran(this);
    }

    public W1tgaTabTran initW1tgaTabTran() {
        lccvtga1.getStatus().setStatus(Types.SPACE_CHAR);
        lccvtga1.setIdPtf(0);
        lccvtga1.getDati().setWtgaIdTrchDiGar(0);
        lccvtga1.getDati().setWtgaIdGar(0);
        lccvtga1.getDati().setWtgaIdAdes(0);
        lccvtga1.getDati().setWtgaIdPoli(0);
        lccvtga1.getDati().setWtgaIdMoviCrz(0);
        lccvtga1.getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(0);
        lccvtga1.getDati().setWtgaDtIniEff(0);
        lccvtga1.getDati().setWtgaDtEndEff(0);
        lccvtga1.getDati().setWtgaCodCompAnia(0);
        lccvtga1.getDati().setWtgaDtDecor(0);
        lccvtga1.getDati().getWtgaDtScad().setWtgaDtScad(0);
        lccvtga1.getDati().setWtgaIbOgg("");
        lccvtga1.getDati().setWtgaTpRgmFisc("");
        lccvtga1.getDati().getWtgaDtEmis().setWtgaDtEmis(0);
        lccvtga1.getDati().setWtgaTpTrch("");
        lccvtga1.getDati().getWtgaDurAa().setWtgaDurAa(0);
        lccvtga1.getDati().getWtgaDurMm().setWtgaDurMm(0);
        lccvtga1.getDati().getWtgaDurGg().setWtgaDurGg(0);
        lccvtga1.getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(new AfDecimal(0, 6, 3));
        lccvtga1.getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPreIniNet().setWtgaPreIniNet(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPrePpIni().setWtgaPrePpIni(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPreTariIni().setWtgaPreTariIni(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPreRivto().setWtgaPreRivto(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPreStab().setWtgaPreStab(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaDtEffStab().setWtgaDtEffStab(0);
        lccvtga1.getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(new AfDecimal(0, 14, 9));
        lccvtga1.getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
        lccvtga1.getDati().getWtgaOldTsTec().setWtgaOldTsTec(new AfDecimal(0, 14, 9));
        lccvtga1.getDati().getWtgaRatLrd().setWtgaRatLrd(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPreLrd().setWtgaPreLrd(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPrstzIni().setWtgaPrstzIni(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaCptRshMor().setWtgaCptRshMor(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().setWtgaFlCarCont(Types.SPACE_CHAR);
        lccvtga1.getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpBns().setWtgaImpBns(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().setWtgaCodDvs("");
        lccvtga1.getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpScon().setWtgaImpScon(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaAlqScon().setWtgaAlqScon(new AfDecimal(0, 6, 3));
        lccvtga1.getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpCarInc().setWtgaImpCarInc(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpCarGest().setWtgaImpCarGest(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(((short)0));
        lccvtga1.getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(((short)0));
        lccvtga1.getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(((short)0));
        lccvtga1.getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(((short)0));
        lccvtga1.getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(((short)0));
        lccvtga1.getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(((short)0));
        lccvtga1.getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(new AfDecimal(0, 14, 9));
        lccvtga1.getDati().getWtgaPcRetr().setWtgaPcRetr(new AfDecimal(0, 6, 3));
        lccvtga1.getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(new AfDecimal(0, 14, 9));
        lccvtga1.getDati().getWtgaMinGarto().setWtgaMinGarto(new AfDecimal(0, 14, 9));
        lccvtga1.getDati().getWtgaMinTrnut().setWtgaMinTrnut(new AfDecimal(0, 14, 9));
        lccvtga1.getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaDurAbb().setWtgaDurAbb(0);
        lccvtga1.getDati().setWtgaTpAdegAbb(Types.SPACE_CHAR);
        lccvtga1.getDati().setWtgaModCalc("");
        lccvtga1.getDati().getWtgaImpAz().setWtgaImpAz(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpAder().setWtgaImpAder(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpTfr().setWtgaImpTfr(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpVolo().setWtgaImpVolo(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(0);
        lccvtga1.getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(0);
        lccvtga1.getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPcRipPre().setWtgaPcRipPre(new AfDecimal(0, 6, 3));
        lccvtga1.getDati().setWtgaFlImportiForz(Types.SPACE_CHAR);
        lccvtga1.getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaIntrMora().setWtgaIntrMora(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaProvRicor().setWtgaProvRicor(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaProvInc().setWtgaProvInc(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(new AfDecimal(0, 6, 3));
        lccvtga1.getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(new AfDecimal(0, 6, 3));
        lccvtga1.getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(new AfDecimal(0, 6, 3));
        lccvtga1.getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().setWtgaFlProvForz(Types.SPACE_CHAR);
        lccvtga1.getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaIncrPre().setWtgaIncrPre(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(0);
        lccvtga1.getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(new AfDecimal(0, 14, 9));
        lccvtga1.getDati().getWtgaPrePattuito().setWtgaPrePattuito(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().setWtgaTpRival("");
        lccvtga1.getDati().getWtgaRisMat().setWtgaRisMat(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaCptMinScad().setWtgaCptMinScad(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaCommisGest().setWtgaCommisGest(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().setWtgaTpManfeeAppl("");
        lccvtga1.getDati().setWtgaDsRiga(0);
        lccvtga1.getDati().setWtgaDsOperSql(Types.SPACE_CHAR);
        lccvtga1.getDati().setWtgaDsVer(0);
        lccvtga1.getDati().setWtgaDsTsIniCptz(0);
        lccvtga1.getDati().setWtgaDsTsEndCptz(0);
        lccvtga1.getDati().setWtgaDsUtente("");
        lccvtga1.getDati().setWtgaDsStatoElab(Types.SPACE_CHAR);
        lccvtga1.getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(new AfDecimal(0, 6, 3));
        lccvtga1.getDati().getWtgaNumGgRival().setWtgaNumGgRival(0);
        lccvtga1.getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaAcqExp().setWtgaAcqExp(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaRemunAss().setWtgaRemunAss(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaCommisInter().setWtgaCommisInter(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(new AfDecimal(0, 6, 3));
        lccvtga1.getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(new AfDecimal(0, 6, 3));
        lccvtga1.getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(new AfDecimal(0, 15, 3));
        lccvtga1.getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
        return this;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int W1TGA_TAB_TRAN = WpolStatus.Len.STATUS + Lccvtga1Ivvs0216.Len.ID_PTF + WtgaDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
