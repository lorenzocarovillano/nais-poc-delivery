package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WPRE-IND-MOD-REC<br>
 * Variable: WPRE-IND-MOD-REC from copybook LOAC0560<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpreIndModRec {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char C = 'C';
    public static final char P = 'P';

    //==== METHODS ====
    public void setIndModRec(char indModRec) {
        this.value = indModRec;
    }

    public char getIndModRec() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IND_MOD_REC = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
