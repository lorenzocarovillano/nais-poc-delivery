package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-ACCESSO-X-RANGE<br>
 * Variable: FLAG-ACCESSO-X-RANGE from program IABS0060<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagAccessoXRange {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagAccessoXRange(char flagAccessoXRange) {
        this.value = flagAccessoXRange;
    }

    public char getFlagAccessoXRange() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
