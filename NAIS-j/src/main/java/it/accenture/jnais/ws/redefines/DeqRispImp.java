package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DEQ-RISP-IMP<br>
 * Variable: DEQ-RISP-IMP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DeqRispImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DeqRispImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DEQ_RISP_IMP;
    }

    public void setDeqRispImp(AfDecimal deqRispImp) {
        writeDecimalAsPacked(Pos.DEQ_RISP_IMP, deqRispImp.copy());
    }

    public void setDeqRispImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DEQ_RISP_IMP, Pos.DEQ_RISP_IMP);
    }

    /**Original name: DEQ-RISP-IMP<br>*/
    public AfDecimal getDeqRispImp() {
        return readPackedAsDecimal(Pos.DEQ_RISP_IMP, Len.Int.DEQ_RISP_IMP, Len.Fract.DEQ_RISP_IMP);
    }

    public byte[] getDeqRispImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DEQ_RISP_IMP, Pos.DEQ_RISP_IMP);
        return buffer;
    }

    public void setDeqRispImpNull(String deqRispImpNull) {
        writeString(Pos.DEQ_RISP_IMP_NULL, deqRispImpNull, Len.DEQ_RISP_IMP_NULL);
    }

    /**Original name: DEQ-RISP-IMP-NULL<br>*/
    public String getDeqRispImpNull() {
        return readString(Pos.DEQ_RISP_IMP_NULL, Len.DEQ_RISP_IMP_NULL);
    }

    public String getDeqRispImpNullFormatted() {
        return Functions.padBlanks(getDeqRispImpNull(), Len.DEQ_RISP_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DEQ_RISP_IMP = 1;
        public static final int DEQ_RISP_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DEQ_RISP_IMP = 8;
        public static final int DEQ_RISP_IMP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DEQ_RISP_IMP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DEQ_RISP_IMP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
