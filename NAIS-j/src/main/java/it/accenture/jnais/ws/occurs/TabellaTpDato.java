package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: TABELLA-TP-DATO<br>
 * Variables: TABELLA-TP-DATO from copybook IDSV0501<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TabellaTpDato {

    //==== PROPERTIES ====
    //Original name: TP-DATO-PARALLELO
    private char tpDatoParallelo = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setTpDatoParallelo(char tpDatoParallelo) {
        this.tpDatoParallelo = tpDatoParallelo;
    }

    public char getTpDatoParallelo() {
        return this.tpDatoParallelo;
    }
}
