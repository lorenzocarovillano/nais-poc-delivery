package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-TGA-ALIQ-ACQ<br>
 * Variable: WPAG-TGA-ALIQ-ACQ from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaAliqAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTgaAliqAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TGA_ALIQ_ACQ;
    }

    public void setWpagTgaAliqAcq(AfDecimal wpagTgaAliqAcq) {
        writeDecimalAsPacked(Pos.WPAG_TGA_ALIQ_ACQ, wpagTgaAliqAcq.copy());
    }

    public void setWpagTgaAliqAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TGA_ALIQ_ACQ, Pos.WPAG_TGA_ALIQ_ACQ);
    }

    /**Original name: WPAG-TGA-ALIQ-ACQ<br>*/
    public AfDecimal getWpagTgaAliqAcq() {
        return readPackedAsDecimal(Pos.WPAG_TGA_ALIQ_ACQ, Len.Int.WPAG_TGA_ALIQ_ACQ, Len.Fract.WPAG_TGA_ALIQ_ACQ);
    }

    public byte[] getWpagTgaAliqAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TGA_ALIQ_ACQ, Pos.WPAG_TGA_ALIQ_ACQ);
        return buffer;
    }

    public void initWpagTgaAliqAcqSpaces() {
        fill(Pos.WPAG_TGA_ALIQ_ACQ, Len.WPAG_TGA_ALIQ_ACQ, Types.SPACE_CHAR);
    }

    public void setWpagTgaAliqAcqNull(String wpagTgaAliqAcqNull) {
        writeString(Pos.WPAG_TGA_ALIQ_ACQ_NULL, wpagTgaAliqAcqNull, Len.WPAG_TGA_ALIQ_ACQ_NULL);
    }

    /**Original name: WPAG-TGA-ALIQ-ACQ-NULL<br>*/
    public String getWpagTgaAliqAcqNull() {
        return readString(Pos.WPAG_TGA_ALIQ_ACQ_NULL, Len.WPAG_TGA_ALIQ_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_ALIQ_ACQ = 1;
        public static final int WPAG_TGA_ALIQ_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_ALIQ_ACQ = 8;
        public static final int WPAG_TGA_ALIQ_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_ALIQ_ACQ = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_ALIQ_ACQ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
