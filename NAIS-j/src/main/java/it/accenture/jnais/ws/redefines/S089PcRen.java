package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-PC-REN<br>
 * Variable: S089-PC-REN from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089PcRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089PcRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_PC_REN;
    }

    public void setWlquPcRen(AfDecimal wlquPcRen) {
        writeDecimalAsPacked(Pos.S089_PC_REN, wlquPcRen.copy());
    }

    public void setWlquPcRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_PC_REN, Pos.S089_PC_REN);
    }

    /**Original name: WLQU-PC-REN<br>*/
    public AfDecimal getWlquPcRen() {
        return readPackedAsDecimal(Pos.S089_PC_REN, Len.Int.WLQU_PC_REN, Len.Fract.WLQU_PC_REN);
    }

    public byte[] getWlquPcRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_PC_REN, Pos.S089_PC_REN);
        return buffer;
    }

    public void initWlquPcRenSpaces() {
        fill(Pos.S089_PC_REN, Len.S089_PC_REN, Types.SPACE_CHAR);
    }

    public void setWlquPcRenNull(String wlquPcRenNull) {
        writeString(Pos.S089_PC_REN_NULL, wlquPcRenNull, Len.WLQU_PC_REN_NULL);
    }

    /**Original name: WLQU-PC-REN-NULL<br>*/
    public String getWlquPcRenNull() {
        return readString(Pos.S089_PC_REN_NULL, Len.WLQU_PC_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_PC_REN = 1;
        public static final int S089_PC_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_PC_REN = 4;
        public static final int WLQU_PC_REN_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_PC_REN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_PC_REN = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
