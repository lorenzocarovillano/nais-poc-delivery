package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-ID-JOB<br>
 * Variable: WCOM-ID-JOB from program LOAS0110<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WcomIdJob extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WcomIdJob() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WCOM_ID_JOB;
    }

    public void setWcomIdJob(int wcomIdJob) {
        writeIntAsPacked(Pos.WCOM_ID_JOB, wcomIdJob, Len.Int.WCOM_ID_JOB);
    }

    public void setWcomIdJobFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WCOM_ID_JOB, Pos.WCOM_ID_JOB);
    }

    /**Original name: WCOM-ID-JOB<br>*/
    public int getWcomIdJob() {
        return readPackedAsInt(Pos.WCOM_ID_JOB, Len.Int.WCOM_ID_JOB);
    }

    public byte[] getWcomIdJobAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WCOM_ID_JOB, Pos.WCOM_ID_JOB);
        return buffer;
    }

    public void setWcomIdJobNull(String wcomIdJobNull) {
        writeString(Pos.WCOM_ID_JOB_NULL, wcomIdJobNull, Len.WCOM_ID_JOB_NULL);
    }

    /**Original name: WCOM-ID-JOB-NULL<br>*/
    public String getWcomIdJobNull() {
        return readString(Pos.WCOM_ID_JOB_NULL, Len.WCOM_ID_JOB_NULL);
    }

    public String getWcomIdJobNullFormatted() {
        return Functions.padBlanks(getWcomIdJobNull(), Len.WCOM_ID_JOB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WCOM_ID_JOB = 1;
        public static final int WCOM_ID_JOB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WCOM_ID_JOB = 5;
        public static final int WCOM_ID_JOB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WCOM_ID_JOB = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
