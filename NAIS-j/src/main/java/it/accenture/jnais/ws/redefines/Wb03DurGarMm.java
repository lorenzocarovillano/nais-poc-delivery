package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-GAR-MM<br>
 * Variable: WB03-DUR-GAR-MM from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DurGarMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DurGarMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR_GAR_MM;
    }

    public void setWb03DurGarMm(int wb03DurGarMm) {
        writeIntAsPacked(Pos.WB03_DUR_GAR_MM, wb03DurGarMm, Len.Int.WB03_DUR_GAR_MM);
    }

    public void setWb03DurGarMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR_GAR_MM, Pos.WB03_DUR_GAR_MM);
    }

    /**Original name: WB03-DUR-GAR-MM<br>*/
    public int getWb03DurGarMm() {
        return readPackedAsInt(Pos.WB03_DUR_GAR_MM, Len.Int.WB03_DUR_GAR_MM);
    }

    public byte[] getWb03DurGarMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR_GAR_MM, Pos.WB03_DUR_GAR_MM);
        return buffer;
    }

    public void setWb03DurGarMmNull(String wb03DurGarMmNull) {
        writeString(Pos.WB03_DUR_GAR_MM_NULL, wb03DurGarMmNull, Len.WB03_DUR_GAR_MM_NULL);
    }

    /**Original name: WB03-DUR-GAR-MM-NULL<br>*/
    public String getWb03DurGarMmNull() {
        return readString(Pos.WB03_DUR_GAR_MM_NULL, Len.WB03_DUR_GAR_MM_NULL);
    }

    public String getWb03DurGarMmNullFormatted() {
        return Functions.padBlanks(getWb03DurGarMmNull(), Len.WB03_DUR_GAR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR_GAR_MM = 1;
        public static final int WB03_DUR_GAR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR_GAR_MM = 3;
        public static final int WB03_DUR_GAR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR_GAR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
