package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WE15-ID-MOVI-CHIU<br>
 * Variable: WE15-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class We15IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public We15IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WE15_ID_MOVI_CHIU;
    }

    public void setWe15IdMoviChiu(int we15IdMoviChiu) {
        writeIntAsPacked(Pos.WE15_ID_MOVI_CHIU, we15IdMoviChiu, Len.Int.WE15_ID_MOVI_CHIU);
    }

    public void setWe15IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WE15_ID_MOVI_CHIU, Pos.WE15_ID_MOVI_CHIU);
    }

    /**Original name: WE15-ID-MOVI-CHIU<br>*/
    public int getWe15IdMoviChiu() {
        return readPackedAsInt(Pos.WE15_ID_MOVI_CHIU, Len.Int.WE15_ID_MOVI_CHIU);
    }

    public byte[] getWe15IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WE15_ID_MOVI_CHIU, Pos.WE15_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWe15IdMoviChiuSpaces() {
        fill(Pos.WE15_ID_MOVI_CHIU, Len.WE15_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWe15IdMoviChiuNull(String we15IdMoviChiuNull) {
        writeString(Pos.WE15_ID_MOVI_CHIU_NULL, we15IdMoviChiuNull, Len.WE15_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WE15-ID-MOVI-CHIU-NULL<br>*/
    public String getWe15IdMoviChiuNull() {
        return readString(Pos.WE15_ID_MOVI_CHIU_NULL, Len.WE15_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WE15_ID_MOVI_CHIU = 1;
        public static final int WE15_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WE15_ID_MOVI_CHIU = 5;
        public static final int WE15_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WE15_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
