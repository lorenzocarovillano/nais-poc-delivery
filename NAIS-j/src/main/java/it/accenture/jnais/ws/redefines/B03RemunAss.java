package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-REMUN-ASS<br>
 * Variable: B03-REMUN-ASS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03RemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03RemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_REMUN_ASS;
    }

    public void setB03RemunAss(AfDecimal b03RemunAss) {
        writeDecimalAsPacked(Pos.B03_REMUN_ASS, b03RemunAss.copy());
    }

    public void setB03RemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_REMUN_ASS, Pos.B03_REMUN_ASS);
    }

    /**Original name: B03-REMUN-ASS<br>*/
    public AfDecimal getB03RemunAss() {
        return readPackedAsDecimal(Pos.B03_REMUN_ASS, Len.Int.B03_REMUN_ASS, Len.Fract.B03_REMUN_ASS);
    }

    public byte[] getB03RemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_REMUN_ASS, Pos.B03_REMUN_ASS);
        return buffer;
    }

    public void setB03RemunAssNull(String b03RemunAssNull) {
        writeString(Pos.B03_REMUN_ASS_NULL, b03RemunAssNull, Len.B03_REMUN_ASS_NULL);
    }

    /**Original name: B03-REMUN-ASS-NULL<br>*/
    public String getB03RemunAssNull() {
        return readString(Pos.B03_REMUN_ASS_NULL, Len.B03_REMUN_ASS_NULL);
    }

    public String getB03RemunAssNullFormatted() {
        return Functions.padBlanks(getB03RemunAssNull(), Len.B03_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_REMUN_ASS = 1;
        public static final int B03_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_REMUN_ASS = 8;
        public static final int B03_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
