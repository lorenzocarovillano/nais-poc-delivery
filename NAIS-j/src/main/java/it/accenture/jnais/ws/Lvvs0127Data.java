package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.AreaLdbv1131;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.occurs.WtgaTabTran;
import it.accenture.jnais.ws.redefines.WkDataAmgPol;
import it.accenture.jnais.ws.redefines.WkDataAmgTga;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0127<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0127Data {

    //==== PROPERTIES ====
    public static final int WTGA_TAB_TRAN_MAXOCCURS = 20;
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: LCCS0004<br>
	 * <pre>  --> MODULO PER CALCOLO DATA FINE MESE</pre>*/
    private String lccs0004 = "LCCS0004";
    //Original name: WK-APPO-DT
    private WkAppoDt wkAppoDt = new WkAppoDt();
    //Original name: IX-CICLO
    private short ixCiclo = ((short)0);
    /**Original name: PARAM<br>
	 * <pre>----------------------------------------------------------------*
	 *  AREA LCCS0004
	 * ----------------------------------------------------------------*</pre>*/
    private String param2 = DefaultValues.stringVal(Len.PARAM2);
    //Original name: X-DATA
    private XDataLccs0029 xData = new XDataLccs0029();
    //Original name: IN-RCODE
    private String inRcode = DefaultValues.stringVal(Len.IN_RCODE);
    //Original name: WK-DATA-9
    private String wkData9 = DefaultValues.stringVal(Len.WK_DATA9);
    //Original name: WK-DATA-X
    private String wkDataX = DefaultValues.stringVal(Len.WK_DATA_X);
    //Original name: WK-DT-CALC
    private String wkDtCalc = DefaultValues.stringVal(Len.WK_DT_CALC);
    /**Original name: WK-DATA-AMG-POL<br>
	 * <pre>----------------------------------------------------------------*
	 *  DEFINIZIONE AREA DI APPOGGIO PER FORMATTAZIONE DATE
	 * ----------------------------------------------------------------*
	 *  PER DATA DECORRENZA POLIZZA E ADESIONE</pre>*/
    private WkDataAmgPol wkDataAmgPol = new WkDataAmgPol();
    /**Original name: WK-DATA-AMG-TGA<br>
	 * <pre> PER DATA DECORRENZA TRANCHE</pre>*/
    private WkDataAmgTga wkDataAmgTga = new WkDataAmgTga();
    //Original name: POLI
    private PoliIdbspol0 poli = new PoliIdbspol0();
    //Original name: PARAM-OGG
    private ParamOgg paramOgg = new ParamOgg();
    //Original name: ADES
    private Ades ades = new Ades();
    //Original name: WTGA-ELE-TRAN-MAX
    private short wtgaEleTranMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTGA-TAB-TRAN
    private WtgaTabTran[] wtgaTabTran = new WtgaTabTran[WTGA_TAB_TRAN_MAXOCCURS];
    //Original name: AREA-LDBV1131
    private AreaLdbv1131 areaLdbv1131 = new AreaLdbv1131();
    //Original name: IO-A2K-LCCC0003
    private IoA2kLccc0003 ioA2kLccc0003 = new IoA2kLccc0003();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0127Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wtgaTabTranIdx = 1; wtgaTabTranIdx <= WTGA_TAB_TRAN_MAXOCCURS; wtgaTabTranIdx++) {
            wtgaTabTran[wtgaTabTranIdx - 1] = new WtgaTabTran();
        }
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public String getLccs0004() {
        return this.lccs0004;
    }

    public void setIxCiclo(short ixCiclo) {
        this.ixCiclo = ixCiclo;
    }

    public short getIxCiclo() {
        return this.ixCiclo;
    }

    public void setParam2(short param2) {
        this.param2 = NumericDisplay.asString(param2, Len.PARAM2);
    }

    public void setParam2FromBuffer(byte[] buffer) {
        param2 = MarshalByte.readFixedString(buffer, 1, Len.PARAM2);
    }

    public short getParam2() {
        return NumericDisplay.asShort(this.param2);
    }

    public String getParam2Formatted() {
        return this.param2;
    }

    public String getParam2AsString() {
        return getParam2Formatted();
    }

    public void setInRcodeFromBuffer(byte[] buffer) {
        inRcode = MarshalByte.readFixedString(buffer, 1, Len.IN_RCODE);
    }

    public String getInRcodeFormatted() {
        return this.inRcode;
    }

    public String getInRcodeAsString() {
        return getInRcodeFormatted();
    }

    public void setWkData9(int wkData9) {
        this.wkData9 = NumericDisplay.asString(wkData9, Len.WK_DATA9);
    }

    public void setWkData9Formatted(String wkData9) {
        this.wkData9 = Trunc.toUnsignedNumeric(wkData9, Len.WK_DATA9);
    }

    public int getWkData9() {
        return NumericDisplay.asInt(this.wkData9);
    }

    public String getWkData9Formatted() {
        return this.wkData9;
    }

    public void setWkDataX(String wkDataX) {
        this.wkDataX = Functions.subString(wkDataX, Len.WK_DATA_X);
    }

    public String getWkDataX() {
        return this.wkDataX;
    }

    public String getWkDataXFormatted() {
        return Functions.padBlanks(getWkDataX(), Len.WK_DATA_X);
    }

    public void setWkDtCalcFormatted(String wkDtCalc) {
        this.wkDtCalc = Trunc.toUnsignedNumeric(wkDtCalc, Len.WK_DT_CALC);
    }

    public void setWkDtCalcFromBuffer(byte[] buffer) {
        wkDtCalc = MarshalByte.readFixedString(buffer, 1, Len.WK_DT_CALC);
    }

    public int getWkDtCalc() {
        return NumericDisplay.asInt(this.wkDtCalc);
    }

    public String getWkDtCalcFormatted() {
        return this.wkDtCalc;
    }

    public void setWtgaAreaTrancheFormatted(String data) {
        byte[] buffer = new byte[Len.WTGA_AREA_TRANCHE];
        MarshalByte.writeString(buffer, 1, data, Len.WTGA_AREA_TRANCHE);
        setWtgaAreaTrancheBytes(buffer, 1);
    }

    public void setWtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        wtgaEleTranMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WTGA_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wtgaTabTran[idx - 1].setTabTranBytes(buffer, position);
                position += WtgaTabTran.Len.TAB_TRAN;
            }
            else {
                wtgaTabTran[idx - 1].initTabTranSpaces();
                position += WtgaTabTran.Len.TAB_TRAN;
            }
        }
    }

    public void setWtgaEleTranMax(short wtgaEleTranMax) {
        this.wtgaEleTranMax = wtgaEleTranMax;
    }

    public short getWtgaEleTranMax() {
        return this.wtgaEleTranMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public Ades getAdes() {
        return ades;
    }

    public AreaLdbv1131 getAreaLdbv1131() {
        return areaLdbv1131;
    }

    public IoA2kLccc0003 getIoA2kLccc0003() {
        return ioA2kLccc0003;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public ParamOgg getParamOgg() {
        return paramOgg;
    }

    public PoliIdbspol0 getPoli() {
        return poli;
    }

    public WkAppoDt getWkAppoDt() {
        return wkAppoDt;
    }

    public WkDataAmgPol getWkDataAmgPol() {
        return wkDataAmgPol;
    }

    public WkDataAmgTga getWkDataAmgTga() {
        return wkDataAmgTga;
    }

    public WtgaTabTran getWtgaTabTran(int idx) {
        return wtgaTabTran[idx - 1];
    }

    public XDataLccs0029 getxData() {
        return xData;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IX_CICLO = 1;
        public static final int PARAM2 = 1;
        public static final int IN_RCODE = 2;
        public static final int WK_DATA9 = 8;
        public static final int WK_DATA_X = 8;
        public static final int WK_DT_CALC = 8;
        public static final int WTGA_ELE_TRAN_MAX = 2;
        public static final int WTGA_AREA_TRANCHE = WTGA_ELE_TRAN_MAX + Lvvs0127Data.WTGA_TAB_TRAN_MAXOCCURS * WtgaTabTran.Len.TAB_TRAN;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
