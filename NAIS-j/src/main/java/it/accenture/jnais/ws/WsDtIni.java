package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-DT-INI<br>
 * Variable: WS-DT-INI from program LLBS0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDtIni {

    //==== PROPERTIES ====
    //Original name: WS-DT-INI-AA
    private String aa = DefaultValues.stringVal(Len.AA);
    //Original name: WS-DT-INI-MM
    private String mm = "01";
    //Original name: WS-DT-INI-GG
    private String gg = "01";

    //==== METHODS ====
    public byte[] getWsDtIniBytes() {
        byte[] buffer = new byte[Len.WS_DT_INI];
        return getWsDtIniBytes(buffer, 1);
    }

    public byte[] getWsDtIniBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aa, Len.AA);
        position += Len.AA;
        MarshalByte.writeString(buffer, position, mm, Len.MM);
        position += Len.MM;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        return buffer;
    }

    public void setAaFormatted(String aa) {
        this.aa = Trunc.toUnsignedNumeric(aa, Len.AA);
    }

    public short getAa() {
        return NumericDisplay.asShort(this.aa);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WS_DT_INI = AA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
