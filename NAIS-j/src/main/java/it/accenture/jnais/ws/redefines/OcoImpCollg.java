package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-IMP-COLLG<br>
 * Variable: OCO-IMP-COLLG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoImpCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoImpCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_IMP_COLLG;
    }

    public void setOcoImpCollg(AfDecimal ocoImpCollg) {
        writeDecimalAsPacked(Pos.OCO_IMP_COLLG, ocoImpCollg.copy());
    }

    public void setOcoImpCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_IMP_COLLG, Pos.OCO_IMP_COLLG);
    }

    /**Original name: OCO-IMP-COLLG<br>*/
    public AfDecimal getOcoImpCollg() {
        return readPackedAsDecimal(Pos.OCO_IMP_COLLG, Len.Int.OCO_IMP_COLLG, Len.Fract.OCO_IMP_COLLG);
    }

    public byte[] getOcoImpCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_IMP_COLLG, Pos.OCO_IMP_COLLG);
        return buffer;
    }

    public void setOcoImpCollgNull(String ocoImpCollgNull) {
        writeString(Pos.OCO_IMP_COLLG_NULL, ocoImpCollgNull, Len.OCO_IMP_COLLG_NULL);
    }

    /**Original name: OCO-IMP-COLLG-NULL<br>*/
    public String getOcoImpCollgNull() {
        return readString(Pos.OCO_IMP_COLLG_NULL, Len.OCO_IMP_COLLG_NULL);
    }

    public String getOcoImpCollgNullFormatted() {
        return Functions.padBlanks(getOcoImpCollgNull(), Len.OCO_IMP_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_IMP_COLLG = 1;
        public static final int OCO_IMP_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_IMP_COLLG = 8;
        public static final int OCO_IMP_COLLG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_IMP_COLLG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_IMP_COLLG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
