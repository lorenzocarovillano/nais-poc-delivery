package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPAG-IMP-CAR-ACQ-TDR<br>
 * Variable: WPAG-IMP-CAR-ACQ-TDR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpCarAcqTdr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpCarAcqTdr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_CAR_ACQ_TDR;
    }

    public void setWpagImpCarAcqTdr(AfDecimal wpagImpCarAcqTdr) {
        writeDecimalAsPacked(Pos.WPAG_IMP_CAR_ACQ_TDR, wpagImpCarAcqTdr.copy());
    }

    public void setWpagImpCarAcqTdrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_CAR_ACQ_TDR, Pos.WPAG_IMP_CAR_ACQ_TDR);
    }

    /**Original name: WPAG-IMP-CAR-ACQ-TDR<br>*/
    public AfDecimal getWpagImpCarAcqTdr() {
        return readPackedAsDecimal(Pos.WPAG_IMP_CAR_ACQ_TDR, Len.Int.WPAG_IMP_CAR_ACQ_TDR, Len.Fract.WPAG_IMP_CAR_ACQ_TDR);
    }

    public byte[] getWpagImpCarAcqTdrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_CAR_ACQ_TDR, Pos.WPAG_IMP_CAR_ACQ_TDR);
        return buffer;
    }

    public void initWpagImpCarAcqTdrSpaces() {
        fill(Pos.WPAG_IMP_CAR_ACQ_TDR, Len.WPAG_IMP_CAR_ACQ_TDR, Types.SPACE_CHAR);
    }

    public void setWpagImpCarAcqTdrNull(String wpagImpCarAcqTdrNull) {
        writeString(Pos.WPAG_IMP_CAR_ACQ_TDR_NULL, wpagImpCarAcqTdrNull, Len.WPAG_IMP_CAR_ACQ_TDR_NULL);
    }

    /**Original name: WPAG-IMP-CAR-ACQ-TDR-NULL<br>*/
    public String getWpagImpCarAcqTdrNull() {
        return readString(Pos.WPAG_IMP_CAR_ACQ_TDR_NULL, Len.WPAG_IMP_CAR_ACQ_TDR_NULL);
    }

    public String getWpagImpCarAcqTdrNullFormatted() {
        return Functions.padBlanks(getWpagImpCarAcqTdrNull(), Len.WPAG_IMP_CAR_ACQ_TDR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_ACQ_TDR = 1;
        public static final int WPAG_IMP_CAR_ACQ_TDR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_ACQ_TDR = 8;
        public static final int WPAG_IMP_CAR_ACQ_TDR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_ACQ_TDR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_ACQ_TDR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
