package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-PREMIO-TOT<br>
 * Variable: WPAG-RATA-PREMIO-TOT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataPremioTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataPremioTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_PREMIO_TOT;
    }

    public void setWpagRataPremioTot(AfDecimal wpagRataPremioTot) {
        writeDecimalAsPacked(Pos.WPAG_RATA_PREMIO_TOT, wpagRataPremioTot.copy());
    }

    public void setWpagRataPremioTotFormatted(String wpagRataPremioTot) {
        setWpagRataPremioTot(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_PREMIO_TOT + Len.Fract.WPAG_RATA_PREMIO_TOT, Len.Fract.WPAG_RATA_PREMIO_TOT, wpagRataPremioTot));
    }

    public void setWpagRataPremioTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_PREMIO_TOT, Pos.WPAG_RATA_PREMIO_TOT);
    }

    /**Original name: WPAG-RATA-PREMIO-TOT<br>*/
    public AfDecimal getWpagRataPremioTot() {
        return readPackedAsDecimal(Pos.WPAG_RATA_PREMIO_TOT, Len.Int.WPAG_RATA_PREMIO_TOT, Len.Fract.WPAG_RATA_PREMIO_TOT);
    }

    public byte[] getWpagRataPremioTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_PREMIO_TOT, Pos.WPAG_RATA_PREMIO_TOT);
        return buffer;
    }

    public void initWpagRataPremioTotSpaces() {
        fill(Pos.WPAG_RATA_PREMIO_TOT, Len.WPAG_RATA_PREMIO_TOT, Types.SPACE_CHAR);
    }

    public void setWpagRataPremioTotNull(String wpagRataPremioTotNull) {
        writeString(Pos.WPAG_RATA_PREMIO_TOT_NULL, wpagRataPremioTotNull, Len.WPAG_RATA_PREMIO_TOT_NULL);
    }

    /**Original name: WPAG-RATA-PREMIO-TOT-NULL<br>*/
    public String getWpagRataPremioTotNull() {
        return readString(Pos.WPAG_RATA_PREMIO_TOT_NULL, Len.WPAG_RATA_PREMIO_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_PREMIO_TOT = 1;
        public static final int WPAG_RATA_PREMIO_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_PREMIO_TOT = 8;
        public static final int WPAG_RATA_PREMIO_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_PREMIO_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_PREMIO_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
