package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRE-PP-ULT<br>
 * Variable: WPAG-PRE-PP-ULT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPrePpUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPrePpUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRE_PP_ULT;
    }

    public void setWpagPrePpUlt(AfDecimal wpagPrePpUlt) {
        writeDecimalAsPacked(Pos.WPAG_PRE_PP_ULT, wpagPrePpUlt.copy());
    }

    public void setWpagPrePpUltFormatted(String wpagPrePpUlt) {
        setWpagPrePpUlt(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRE_PP_ULT + Len.Fract.WPAG_PRE_PP_ULT, Len.Fract.WPAG_PRE_PP_ULT, wpagPrePpUlt));
    }

    public void setWpagPrePpUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRE_PP_ULT, Pos.WPAG_PRE_PP_ULT);
    }

    /**Original name: WPAG-PRE-PP-ULT<br>*/
    public AfDecimal getWpagPrePpUlt() {
        return readPackedAsDecimal(Pos.WPAG_PRE_PP_ULT, Len.Int.WPAG_PRE_PP_ULT, Len.Fract.WPAG_PRE_PP_ULT);
    }

    public byte[] getWpagPrePpUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRE_PP_ULT, Pos.WPAG_PRE_PP_ULT);
        return buffer;
    }

    public void initWpagPrePpUltSpaces() {
        fill(Pos.WPAG_PRE_PP_ULT, Len.WPAG_PRE_PP_ULT, Types.SPACE_CHAR);
    }

    public void setWpagPrePpUltNull(String wpagPrePpUltNull) {
        writeString(Pos.WPAG_PRE_PP_ULT_NULL, wpagPrePpUltNull, Len.WPAG_PRE_PP_ULT_NULL);
    }

    /**Original name: WPAG-PRE-PP-ULT-NULL<br>*/
    public String getWpagPrePpUltNull() {
        return readString(Pos.WPAG_PRE_PP_ULT_NULL, Len.WPAG_PRE_PP_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_PP_ULT = 1;
        public static final int WPAG_PRE_PP_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_PP_ULT = 8;
        public static final int WPAG_PRE_PP_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_PP_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_PP_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
