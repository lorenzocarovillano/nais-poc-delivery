package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMP-CNBT-ISC-K2<br>
 * Variable: WDFA-IMP-CNBT-ISC-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpCnbtIscK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpCnbtIscK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMP_CNBT_ISC_K2;
    }

    public void setWdfaImpCnbtIscK2(AfDecimal wdfaImpCnbtIscK2) {
        writeDecimalAsPacked(Pos.WDFA_IMP_CNBT_ISC_K2, wdfaImpCnbtIscK2.copy());
    }

    public void setWdfaImpCnbtIscK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMP_CNBT_ISC_K2, Pos.WDFA_IMP_CNBT_ISC_K2);
    }

    /**Original name: WDFA-IMP-CNBT-ISC-K2<br>*/
    public AfDecimal getWdfaImpCnbtIscK2() {
        return readPackedAsDecimal(Pos.WDFA_IMP_CNBT_ISC_K2, Len.Int.WDFA_IMP_CNBT_ISC_K2, Len.Fract.WDFA_IMP_CNBT_ISC_K2);
    }

    public byte[] getWdfaImpCnbtIscK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMP_CNBT_ISC_K2, Pos.WDFA_IMP_CNBT_ISC_K2);
        return buffer;
    }

    public void setWdfaImpCnbtIscK2Null(String wdfaImpCnbtIscK2Null) {
        writeString(Pos.WDFA_IMP_CNBT_ISC_K2_NULL, wdfaImpCnbtIscK2Null, Len.WDFA_IMP_CNBT_ISC_K2_NULL);
    }

    /**Original name: WDFA-IMP-CNBT-ISC-K2-NULL<br>*/
    public String getWdfaImpCnbtIscK2Null() {
        return readString(Pos.WDFA_IMP_CNBT_ISC_K2_NULL, Len.WDFA_IMP_CNBT_ISC_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_ISC_K2 = 1;
        public static final int WDFA_IMP_CNBT_ISC_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_ISC_K2 = 8;
        public static final int WDFA_IMP_CNBT_ISC_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_ISC_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_ISC_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
