package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RIT-ACC-EFFLQ<br>
 * Variable: DFL-RIT-ACC-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflRitAccEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflRitAccEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RIT_ACC_EFFLQ;
    }

    public void setDflRitAccEfflq(AfDecimal dflRitAccEfflq) {
        writeDecimalAsPacked(Pos.DFL_RIT_ACC_EFFLQ, dflRitAccEfflq.copy());
    }

    public void setDflRitAccEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RIT_ACC_EFFLQ, Pos.DFL_RIT_ACC_EFFLQ);
    }

    /**Original name: DFL-RIT-ACC-EFFLQ<br>*/
    public AfDecimal getDflRitAccEfflq() {
        return readPackedAsDecimal(Pos.DFL_RIT_ACC_EFFLQ, Len.Int.DFL_RIT_ACC_EFFLQ, Len.Fract.DFL_RIT_ACC_EFFLQ);
    }

    public byte[] getDflRitAccEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RIT_ACC_EFFLQ, Pos.DFL_RIT_ACC_EFFLQ);
        return buffer;
    }

    public void setDflRitAccEfflqNull(String dflRitAccEfflqNull) {
        writeString(Pos.DFL_RIT_ACC_EFFLQ_NULL, dflRitAccEfflqNull, Len.DFL_RIT_ACC_EFFLQ_NULL);
    }

    /**Original name: DFL-RIT-ACC-EFFLQ-NULL<br>*/
    public String getDflRitAccEfflqNull() {
        return readString(Pos.DFL_RIT_ACC_EFFLQ_NULL, Len.DFL_RIT_ACC_EFFLQ_NULL);
    }

    public String getDflRitAccEfflqNullFormatted() {
        return Functions.padBlanks(getDflRitAccEfflqNull(), Len.DFL_RIT_ACC_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RIT_ACC_EFFLQ = 1;
        public static final int DFL_RIT_ACC_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RIT_ACC_EFFLQ = 8;
        public static final int DFL_RIT_ACC_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RIT_ACC_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RIT_ACC_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
