package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WADE-DT-NOVA-RGM-FISC<br>
 * Variable: WADE-DT-NOVA-RGM-FISC from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeDtNovaRgmFisc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeDtNovaRgmFisc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_DT_NOVA_RGM_FISC;
    }

    public void setWadeDtNovaRgmFisc(int wadeDtNovaRgmFisc) {
        writeIntAsPacked(Pos.WADE_DT_NOVA_RGM_FISC, wadeDtNovaRgmFisc, Len.Int.WADE_DT_NOVA_RGM_FISC);
    }

    public void setWadeDtNovaRgmFiscFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_DT_NOVA_RGM_FISC, Pos.WADE_DT_NOVA_RGM_FISC);
    }

    /**Original name: WADE-DT-NOVA-RGM-FISC<br>*/
    public int getWadeDtNovaRgmFisc() {
        return readPackedAsInt(Pos.WADE_DT_NOVA_RGM_FISC, Len.Int.WADE_DT_NOVA_RGM_FISC);
    }

    public byte[] getWadeDtNovaRgmFiscAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_DT_NOVA_RGM_FISC, Pos.WADE_DT_NOVA_RGM_FISC);
        return buffer;
    }

    public void initWadeDtNovaRgmFiscSpaces() {
        fill(Pos.WADE_DT_NOVA_RGM_FISC, Len.WADE_DT_NOVA_RGM_FISC, Types.SPACE_CHAR);
    }

    public void setWadeDtNovaRgmFiscNull(String wadeDtNovaRgmFiscNull) {
        writeString(Pos.WADE_DT_NOVA_RGM_FISC_NULL, wadeDtNovaRgmFiscNull, Len.WADE_DT_NOVA_RGM_FISC_NULL);
    }

    /**Original name: WADE-DT-NOVA-RGM-FISC-NULL<br>*/
    public String getWadeDtNovaRgmFiscNull() {
        return readString(Pos.WADE_DT_NOVA_RGM_FISC_NULL, Len.WADE_DT_NOVA_RGM_FISC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_DT_NOVA_RGM_FISC = 1;
        public static final int WADE_DT_NOVA_RGM_FISC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_DT_NOVA_RGM_FISC = 5;
        public static final int WADE_DT_NOVA_RGM_FISC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_DT_NOVA_RGM_FISC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
