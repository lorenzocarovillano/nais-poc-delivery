package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: IABV0003-BLOB-DATA-ARRAY<br>
 * Variable: IABV0003-BLOB-DATA-ARRAY from program IABS0900<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Iabv0003BlobDataArray extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int DATA_ELE_MAXOCCURS = 100;
    public static final int DATA_ELE2K_MAXOCCURS = 500;
    public static final int DATA_ELE1K_MAXOCCURS = 650;
    public static final int DATA_ELE3K_MAXOCCURS = 350;

    //==== CONSTRUCTORS ====
    public Iabv0003BlobDataArray() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IABV0003_BLOB_DATA_ARRAY;
    }

    public void setIabv0003BlobDataArray(String iabv0003BlobDataArray) {
        writeString(Pos.IABV0003_BLOB_DATA_ARRAY, iabv0003BlobDataArray, Len.IABV0003_BLOB_DATA_ARRAY);
    }

    public void setIabv0003BlobDataArrayFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.IABV0003_BLOB_DATA_ARRAY, Pos.IABV0003_BLOB_DATA_ARRAY);
    }

    /**Original name: IABV0003-BLOB-DATA-ARRAY<br>*/
    public String getIabv0003BlobDataArray() {
        return readString(Pos.IABV0003_BLOB_DATA_ARRAY, Len.IABV0003_BLOB_DATA_ARRAY);
    }

    public byte[] getIabv0003BlobDataArrayAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.IABV0003_BLOB_DATA_ARRAY, Pos.IABV0003_BLOB_DATA_ARRAY);
        return buffer;
    }

    public void setDataTypeRec(int dataTypeRecIdx, String dataTypeRec) {
        int position = Pos.iabv0003BlobDataTypeRec(dataTypeRecIdx - 1);
        writeString(position, dataTypeRec, Len.DATA_TYPE_REC);
    }

    /**Original name: IABV0003-BLOB-DATA-TYPE-REC<br>*/
    public String getDataTypeRec(int dataTypeRecIdx) {
        int position = Pos.iabv0003BlobDataTypeRec(dataTypeRecIdx - 1);
        return readString(position, Len.DATA_TYPE_REC);
    }

    public void setDataRec(int dataRecIdx, String dataRec) {
        int position = Pos.iabv0003BlobDataRec(dataRecIdx - 1);
        writeString(position, dataRec, Len.DATA_REC);
    }

    /**Original name: IABV0003-BLOB-DATA-REC<br>*/
    public String getDataRec(int dataRecIdx) {
        int position = Pos.iabv0003BlobDataRec(dataRecIdx - 1);
        return readString(position, Len.DATA_REC);
    }

    public void setIabv0003BlobDataTypeRec3k(int iabv0003BlobDataTypeRec3kIdx, String iabv0003BlobDataTypeRec3k) {
        int position = Pos.iabv0003BlobDataTypeRec3k(iabv0003BlobDataTypeRec3kIdx - 1);
        writeString(position, iabv0003BlobDataTypeRec3k, Len.DATA_TYPE_REC3K);
    }

    /**Original name: IABV0003-BLOB-DATA-TYPE-REC-3K<br>*/
    public String getIabv0003BlobDataTypeRec3k(int iabv0003BlobDataTypeRec3kIdx) {
        int position = Pos.iabv0003BlobDataTypeRec3k(iabv0003BlobDataTypeRec3kIdx - 1);
        return readString(position, Len.DATA_TYPE_REC3K);
    }

    public void setIabv0003BlobDataRec3k(int iabv0003BlobDataRec3kIdx, String iabv0003BlobDataRec3k) {
        int position = Pos.iabv0003BlobDataRec3k(iabv0003BlobDataRec3kIdx - 1);
        writeString(position, iabv0003BlobDataRec3k, Len.DATA_REC3K);
    }

    /**Original name: IABV0003-BLOB-DATA-REC-3K<br>*/
    public String getIabv0003BlobDataRec3k(int iabv0003BlobDataRec3kIdx) {
        int position = Pos.iabv0003BlobDataRec3k(iabv0003BlobDataRec3kIdx - 1);
        return readString(position, Len.DATA_REC3K);
    }

    public void setIabv0003BlobDataTypeRec2k(int iabv0003BlobDataTypeRec2kIdx, String iabv0003BlobDataTypeRec2k) {
        int position = Pos.iabv0003BlobDataTypeRec2k(iabv0003BlobDataTypeRec2kIdx - 1);
        writeString(position, iabv0003BlobDataTypeRec2k, Len.DATA_TYPE_REC2K);
    }

    /**Original name: IABV0003-BLOB-DATA-TYPE-REC-2K<br>*/
    public String getIabv0003BlobDataTypeRec2k(int iabv0003BlobDataTypeRec2kIdx) {
        int position = Pos.iabv0003BlobDataTypeRec2k(iabv0003BlobDataTypeRec2kIdx - 1);
        return readString(position, Len.DATA_TYPE_REC2K);
    }

    public void setIabv0003BlobDataRec2k(int iabv0003BlobDataRec2kIdx, String iabv0003BlobDataRec2k) {
        int position = Pos.iabv0003BlobDataRec2k(iabv0003BlobDataRec2kIdx - 1);
        writeString(position, iabv0003BlobDataRec2k, Len.DATA_REC2K);
    }

    /**Original name: IABV0003-BLOB-DATA-REC-2K<br>*/
    public String getIabv0003BlobDataRec2k(int iabv0003BlobDataRec2kIdx) {
        int position = Pos.iabv0003BlobDataRec2k(iabv0003BlobDataRec2kIdx - 1);
        return readString(position, Len.DATA_REC2K);
    }

    public void setIabv0003BlobDataTypeRec1k(int iabv0003BlobDataTypeRec1kIdx, String iabv0003BlobDataTypeRec1k) {
        int position = Pos.iabv0003BlobDataTypeRec1k(iabv0003BlobDataTypeRec1kIdx - 1);
        writeString(position, iabv0003BlobDataTypeRec1k, Len.DATA_TYPE_REC1K);
    }

    /**Original name: IABV0003-BLOB-DATA-TYPE-REC-1K<br>*/
    public String getIabv0003BlobDataTypeRec1k(int iabv0003BlobDataTypeRec1kIdx) {
        int position = Pos.iabv0003BlobDataTypeRec1k(iabv0003BlobDataTypeRec1kIdx - 1);
        return readString(position, Len.DATA_TYPE_REC1K);
    }

    public void setIabv0003BlobDataRec1k(int iabv0003BlobDataRec1kIdx, String iabv0003BlobDataRec1k) {
        int position = Pos.iabv0003BlobDataRec1k(iabv0003BlobDataRec1kIdx - 1);
        writeString(position, iabv0003BlobDataRec1k, Len.DATA_REC1K);
    }

    /**Original name: IABV0003-BLOB-DATA-REC-1K<br>*/
    public String getIabv0003BlobDataRec1k(int iabv0003BlobDataRec1kIdx) {
        int position = Pos.iabv0003BlobDataRec1k(iabv0003BlobDataRec1kIdx - 1);
        return readString(position, Len.DATA_REC1K);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int IABV0003_BLOB_DATA_ARRAY = 1;
        public static final int IABV0003_BLOB_TABLE = 1;
        public static final int FLR1 = iabv0003BlobDataRec(DATA_ELE_MAXOCCURS - 1) + Len.DATA_REC;
        public static final int IABV0003_BLOB_TABLE3K = 1;
        public static final int IABV0003_BLOB_TABLE2K = 1;
        public static final int FLR2 = iabv0003BlobDataRec2k(DATA_ELE2K_MAXOCCURS - 1) + Len.DATA_REC2K;
        public static final int IABV0003_BLOB_TABLE1K = 1;
        public static final int FLR3 = iabv0003BlobDataRec1k(DATA_ELE1K_MAXOCCURS - 1) + Len.DATA_REC1K;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int iabv0003BlobDataEle(int idx) {
            return IABV0003_BLOB_TABLE + idx * Len.DATA_ELE;
        }

        public static int iabv0003BlobDataTypeRec(int idx) {
            return iabv0003BlobDataEle(idx);
        }

        public static int iabv0003BlobDataRec(int idx) {
            return iabv0003BlobDataTypeRec(idx) + Len.DATA_TYPE_REC;
        }

        public static int iabv0003BlobDataEle3k(int idx) {
            return IABV0003_BLOB_TABLE3K + idx * Len.DATA_ELE3K;
        }

        public static int iabv0003BlobDataTypeRec3k(int idx) {
            return iabv0003BlobDataEle3k(idx);
        }

        public static int iabv0003BlobDataRec3k(int idx) {
            return iabv0003BlobDataTypeRec3k(idx) + Len.DATA_TYPE_REC3K;
        }

        public static int iabv0003BlobDataEle2k(int idx) {
            return IABV0003_BLOB_TABLE2K + idx * Len.DATA_ELE2K;
        }

        public static int iabv0003BlobDataTypeRec2k(int idx) {
            return iabv0003BlobDataEle2k(idx);
        }

        public static int iabv0003BlobDataRec2k(int idx) {
            return iabv0003BlobDataTypeRec2k(idx) + Len.DATA_TYPE_REC2K;
        }

        public static int iabv0003BlobDataEle1k(int idx) {
            return IABV0003_BLOB_TABLE1K + idx * Len.DATA_ELE1K;
        }

        public static int iabv0003BlobDataTypeRec1k(int idx) {
            return iabv0003BlobDataEle1k(idx);
        }

        public static int iabv0003BlobDataRec1k(int idx) {
            return iabv0003BlobDataTypeRec1k(idx) + Len.DATA_TYPE_REC1K;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DATA_TYPE_REC = 3;
        public static final int DATA_REC = 10000;
        public static final int DATA_ELE = DATA_TYPE_REC + DATA_REC;
        public static final int DATA_TYPE_REC3K = 3;
        public static final int DATA_REC3K = 3000;
        public static final int DATA_ELE3K = DATA_TYPE_REC3K + DATA_REC3K;
        public static final int DATA_TYPE_REC2K = 3;
        public static final int DATA_REC2K = 2000;
        public static final int DATA_ELE2K = DATA_TYPE_REC2K + DATA_REC2K;
        public static final int DATA_TYPE_REC1K = 3;
        public static final int DATA_REC1K = 1503;
        public static final int DATA_ELE1K = DATA_TYPE_REC1K + DATA_REC1K;
        public static final int IABV0003_BLOB_DATA_ARRAY = 1051050;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
