package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-DT-DECOR<br>
 * Variable: L3401-DT-DECOR from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401DtDecor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401DtDecor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_DT_DECOR;
    }

    public void setL3401DtDecor(int l3401DtDecor) {
        writeIntAsPacked(Pos.L3401_DT_DECOR, l3401DtDecor, Len.Int.L3401_DT_DECOR);
    }

    public void setL3401DtDecorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_DT_DECOR, Pos.L3401_DT_DECOR);
    }

    /**Original name: L3401-DT-DECOR<br>*/
    public int getL3401DtDecor() {
        return readPackedAsInt(Pos.L3401_DT_DECOR, Len.Int.L3401_DT_DECOR);
    }

    public byte[] getL3401DtDecorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_DT_DECOR, Pos.L3401_DT_DECOR);
        return buffer;
    }

    /**Original name: L3401-DT-DECOR-NULL<br>*/
    public String getL3401DtDecorNull() {
        return readString(Pos.L3401_DT_DECOR_NULL, Len.L3401_DT_DECOR_NULL);
    }

    public String getL3401DtDecorNullFormatted() {
        return Functions.padBlanks(getL3401DtDecorNull(), Len.L3401_DT_DECOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_DT_DECOR = 1;
        public static final int L3401_DT_DECOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_DT_DECOR = 5;
        public static final int L3401_DT_DECOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_DT_DECOR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
