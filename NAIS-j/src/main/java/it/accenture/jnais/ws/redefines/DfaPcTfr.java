package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-PC-TFR<br>
 * Variable: DFA-PC-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaPcTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaPcTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_PC_TFR;
    }

    public void setDfaPcTfr(AfDecimal dfaPcTfr) {
        writeDecimalAsPacked(Pos.DFA_PC_TFR, dfaPcTfr.copy());
    }

    public void setDfaPcTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_PC_TFR, Pos.DFA_PC_TFR);
    }

    /**Original name: DFA-PC-TFR<br>*/
    public AfDecimal getDfaPcTfr() {
        return readPackedAsDecimal(Pos.DFA_PC_TFR, Len.Int.DFA_PC_TFR, Len.Fract.DFA_PC_TFR);
    }

    public byte[] getDfaPcTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_PC_TFR, Pos.DFA_PC_TFR);
        return buffer;
    }

    public void setDfaPcTfrNull(String dfaPcTfrNull) {
        writeString(Pos.DFA_PC_TFR_NULL, dfaPcTfrNull, Len.DFA_PC_TFR_NULL);
    }

    /**Original name: DFA-PC-TFR-NULL<br>*/
    public String getDfaPcTfrNull() {
        return readString(Pos.DFA_PC_TFR_NULL, Len.DFA_PC_TFR_NULL);
    }

    public String getDfaPcTfrNullFormatted() {
        return Functions.padBlanks(getDfaPcTfrNull(), Len.DFA_PC_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_PC_TFR = 1;
        public static final int DFA_PC_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_PC_TFR = 4;
        public static final int DFA_PC_TFR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
