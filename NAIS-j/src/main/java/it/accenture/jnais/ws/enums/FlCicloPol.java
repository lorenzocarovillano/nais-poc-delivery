package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FL-CICLO-POL<br>
 * Variable: FL-CICLO-POL from program LCCS0033<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlCicloPol {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlCicloPol(char flCicloPol) {
        this.value = flCicloPol;
    }

    public char getFlCicloPol() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
