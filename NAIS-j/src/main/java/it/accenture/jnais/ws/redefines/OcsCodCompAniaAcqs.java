package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCS-COD-COMP-ANIA-ACQS<br>
 * Variable: OCS-COD-COMP-ANIA-ACQS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcsCodCompAniaAcqs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcsCodCompAniaAcqs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCS_COD_COMP_ANIA_ACQS;
    }

    public void setOcsCodCompAniaAcqs(int ocsCodCompAniaAcqs) {
        writeIntAsPacked(Pos.OCS_COD_COMP_ANIA_ACQS, ocsCodCompAniaAcqs, Len.Int.OCS_COD_COMP_ANIA_ACQS);
    }

    public void setOcsCodCompAniaAcqsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCS_COD_COMP_ANIA_ACQS, Pos.OCS_COD_COMP_ANIA_ACQS);
    }

    /**Original name: OCS-COD-COMP-ANIA-ACQS<br>*/
    public int getOcsCodCompAniaAcqs() {
        return readPackedAsInt(Pos.OCS_COD_COMP_ANIA_ACQS, Len.Int.OCS_COD_COMP_ANIA_ACQS);
    }

    public byte[] getOcsCodCompAniaAcqsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCS_COD_COMP_ANIA_ACQS, Pos.OCS_COD_COMP_ANIA_ACQS);
        return buffer;
    }

    public void setOcsCodCompAniaAcqsNull(String ocsCodCompAniaAcqsNull) {
        writeString(Pos.OCS_COD_COMP_ANIA_ACQS_NULL, ocsCodCompAniaAcqsNull, Len.OCS_COD_COMP_ANIA_ACQS_NULL);
    }

    /**Original name: OCS-COD-COMP-ANIA-ACQS-NULL<br>*/
    public String getOcsCodCompAniaAcqsNull() {
        return readString(Pos.OCS_COD_COMP_ANIA_ACQS_NULL, Len.OCS_COD_COMP_ANIA_ACQS_NULL);
    }

    public String getOcsCodCompAniaAcqsNullFormatted() {
        return Functions.padBlanks(getOcsCodCompAniaAcqsNull(), Len.OCS_COD_COMP_ANIA_ACQS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCS_COD_COMP_ANIA_ACQS = 1;
        public static final int OCS_COD_COMP_ANIA_ACQS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCS_COD_COMP_ANIA_ACQS = 3;
        public static final int OCS_COD_COMP_ANIA_ACQS_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCS_COD_COMP_ANIA_ACQS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
