package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CUM-RISCPAR<br>
 * Variable: WB03-CUM-RISCPAR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CumRiscpar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CumRiscpar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CUM_RISCPAR;
    }

    public void setWb03CumRiscpar(AfDecimal wb03CumRiscpar) {
        writeDecimalAsPacked(Pos.WB03_CUM_RISCPAR, wb03CumRiscpar.copy());
    }

    public void setWb03CumRiscparFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CUM_RISCPAR, Pos.WB03_CUM_RISCPAR);
    }

    /**Original name: WB03-CUM-RISCPAR<br>*/
    public AfDecimal getWb03CumRiscpar() {
        return readPackedAsDecimal(Pos.WB03_CUM_RISCPAR, Len.Int.WB03_CUM_RISCPAR, Len.Fract.WB03_CUM_RISCPAR);
    }

    public byte[] getWb03CumRiscparAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CUM_RISCPAR, Pos.WB03_CUM_RISCPAR);
        return buffer;
    }

    public void setWb03CumRiscparNull(String wb03CumRiscparNull) {
        writeString(Pos.WB03_CUM_RISCPAR_NULL, wb03CumRiscparNull, Len.WB03_CUM_RISCPAR_NULL);
    }

    /**Original name: WB03-CUM-RISCPAR-NULL<br>*/
    public String getWb03CumRiscparNull() {
        return readString(Pos.WB03_CUM_RISCPAR_NULL, Len.WB03_CUM_RISCPAR_NULL);
    }

    public String getWb03CumRiscparNullFormatted() {
        return Functions.padBlanks(getWb03CumRiscparNull(), Len.WB03_CUM_RISCPAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CUM_RISCPAR = 1;
        public static final int WB03_CUM_RISCPAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CUM_RISCPAR = 8;
        public static final int WB03_CUM_RISCPAR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CUM_RISCPAR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CUM_RISCPAR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
