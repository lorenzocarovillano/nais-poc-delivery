package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.AdesIvvs0216;
import it.accenture.jnais.copy.DettTitCont;
import it.accenture.jnais.copy.GarIvvs0216;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Idsv0015;
import it.accenture.jnais.copy.Idsv8888;
import it.accenture.jnais.copy.Ispc0040DatiInput;
import it.accenture.jnais.copy.Ispv0000;
import it.accenture.jnais.copy.Ivvc0212;
import it.accenture.jnais.copy.Ivvc0218Loas0310;
import it.accenture.jnais.copy.Lccc0211;
import it.accenture.jnais.copy.Lccvgrzz;
import it.accenture.jnais.copy.Lccvmov1Loas0310;
import it.accenture.jnais.copy.Ldbv0011;
import it.accenture.jnais.copy.Ldbv3361;
import it.accenture.jnais.copy.Ldbvd601;
import it.accenture.jnais.copy.Loac0002AreaGestDeroghe;
import it.accenture.jnais.copy.Loar0171;
import it.accenture.jnais.copy.Movi;
import it.accenture.jnais.copy.OggDeroga;
import it.accenture.jnais.copy.ParamComp;
import it.accenture.jnais.copy.Poli;
import it.accenture.jnais.copy.StatOggBus;
import it.accenture.jnais.copy.TitCont;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.copy.WkTgaMax;
import it.accenture.jnais.ws.enums.SwCurDtc;
import it.accenture.jnais.ws.enums.SwTrovatoDtc;
import it.accenture.jnais.ws.enums.WkBlcTrovato;
import it.accenture.jnais.ws.enums.WkFineFetch;
import it.accenture.jnais.ws.enums.WkFlagProd;
import it.accenture.jnais.ws.enums.WkGrzTrovata;
import it.accenture.jnais.ws.enums.WkOrigRiv;
import it.accenture.jnais.ws.enums.WkPrecTrovato;
import it.accenture.jnais.ws.enums.WkScartaGar;
import it.accenture.jnais.ws.enums.WkScartaTga;
import it.accenture.jnais.ws.enums.WkStbTrovato;
import it.accenture.jnais.ws.enums.WkTitTrovato;
import it.accenture.jnais.ws.enums.WkTrancheDaCaricare;
import it.accenture.jnais.ws.enums.WkTrovato;
import it.accenture.jnais.ws.enums.WsTpCaus;
import it.accenture.jnais.ws.enums.WsTpFrmAssva;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.enums.WsTpStatBus;
import it.accenture.jnais.ws.occurs.WgrzTabGar;
import it.accenture.jnais.ws.occurs.Wk0040Tabella;
import it.accenture.jnais.ws.occurs.WprecTabTran;
import it.accenture.jnais.ws.redefines.WrecOut;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS0310<br>
 * Generated as a class for rule WS.<br>*/
public class Loas0310Data {

    //==== PROPERTIES ====
    public static final int WK0040_TABELLA_MAXOCCURS = 500;
    public static final int WPREC_TAB_TRAN_MAXOCCURS = 300;
    public static final int IWFI0051_CAMPO_INPUT_MAXOCCURS = 20;
    public static final int VGRZ_TAB_GAR_MAXOCCURS = 20;
    //Original name: FS-OUT
    private String fsOut = "00";
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: IDSV8888
    private Idsv8888 idsv8888 = new Idsv8888();
    //Original name: MOVI
    private Movi movi = new Movi();
    //Original name: POLI
    private Poli poli = new Poli();
    //Original name: ADES
    private AdesIvvs0216 ades = new AdesIvvs0216();
    //Original name: GAR
    private GarIvvs0216 gar = new GarIvvs0216();
    //Original name: TRCH-DI-GAR
    private TrchDiGarIvvs0216 trchDiGar = new TrchDiGarIvvs0216();
    //Original name: PARAM-COMP
    private ParamComp paramComp = new ParamComp();
    //Original name: STAT-OGG-BUS
    private StatOggBus statOggBus = new StatOggBus();
    //Original name: TIT-CONT
    private TitCont titCont = new TitCont();
    //Original name: OGG-DEROGA
    private OggDeroga oggDeroga = new OggDeroga();
    //Original name: LDBV0011
    private Ldbv0011 ldbv0011 = new Ldbv0011();
    //Original name: LDBVD601
    private Ldbvd601 ldbvd601 = new Ldbvd601();
    //Original name: LDBV3361
    private Ldbv3361 ldbv3361 = new Ldbv3361();
    //Original name: DETT-TIT-CONT
    private DettTitCont dettTitCont = new DettTitCont();
    //Original name: IX-INDICI
    private IxIndiciLoas0310 ixIndici = new IxIndiciLoas0310();
    //Original name: WK-COSTANTI-TABELLE
    private WkCostantiTabelle wkCostantiTabelle = new WkCostantiTabelle();
    /**Original name: WK-OGGETTO-9<br>
	 * <pre> --> Comodi per string errore</pre>*/
    private String wkOggetto9 = DefaultValues.stringVal(Len.WK_OGGETTO9);
    //Original name: WK-OGGETTO-X
    private String wkOggettoX = DefaultValues.stringVal(Len.WK_OGGETTO_X);
    //Original name: WK-LIV-DEROGA
    private String wkLivDeroga = DefaultValues.stringVal(Len.WK_LIV_DEROGA);
    /**Original name: WK-SCARTA-GAR<br>
	 * <pre> --> Flag per scarto Garanzia x esito ko stato o causale</pre>*/
    private WkScartaGar wkScartaGar = new WkScartaGar();
    /**Original name: WK-TRANCHE-DA-CARICARE<br>
	 * <pre> --> FLAG PER CARICARE AREA DEL VALORIZZATORE PER
	 *  --> RIVALUTAZIONE DI INCASSO</pre>*/
    private WkTrancheDaCaricare wkTrancheDaCaricare = new WkTrancheDaCaricare();
    /**Original name: WK-SCARTA-TGA<br>
	 * <pre> --> Flag per scarto Tranche x esito ko stato o causale</pre>*/
    private WkScartaTga wkScartaTga = new WkScartaTga();
    /**Original name: WK-BLC-TROVATO<br>
	 * <pre> --> Ricerca occorrenza Blocco attiva</pre>*/
    private WkBlcTrovato wkBlcTrovato = new WkBlcTrovato();
    /**Original name: WK-STB-TROVATO<br>
	 * <pre> --> Ricerca occorrenza Stato Oggetto Business</pre>*/
    private WkStbTrovato wkStbTrovato = new WkStbTrovato();
    /**Original name: WK-TROVATO<br>
	 * <pre> --> Ricerca occorrenza</pre>*/
    private WkTrovato wkTrovato = new WkTrovato();
    /**Original name: WK-ORIG-RIV<br>
	 * <pre>   Rivalutazione per Incasso o per Emesso</pre>*/
    private WkOrigRiv wkOrigRiv = new WkOrigRiv();
    /**Original name: WK-FINE-FETCH<br>
	 * <pre> -->  Flag fine fetch</pre>*/
    private WkFineFetch wkFineFetch = new WkFineFetch();
    /**Original name: WK-TIT-TROVATO<br>
	 * <pre> --> Ricerca occorrenza Titolo Contabile</pre>*/
    private WkTitTrovato wkTitTrovato = new WkTitTrovato();
    /**Original name: FL-MOV-FOUND<br>
	 * <pre> --> Flag garanzie di rendita
	 * 01 FL-GAR-REND                   PIC X(001) VALUE 'N'.
	 *    88 SI-GAR-REND                  VALUE 'S'.
	 *    88 NO-GAR-REND                  VALUE 'N'.
	 *  --> Flag per movimento gi&#x17; trovato</pre>*/
    private boolean flMovFound = false;
    /**Original name: WK-GRZ-TROVATA<br>
	 * <pre> --> Ricerca occorrenza Garanzia</pre>*/
    private WkGrzTrovata wkGrzTrovata = new WkGrzTrovata();
    //Original name: WK-PREC-TROVATO
    private WkPrecTrovato wkPrecTrovato = new WkPrecTrovato();
    /**Original name: SW-CUR-DTC<br>
	 * <pre> --> Flag fine cursore dettaglio titolo contabile</pre>*/
    private SwCurDtc swCurDtc = new SwCurDtc();
    /**Original name: SW-TROVATO-DTC<br>
	 * <pre> --> Flag fine cursore dettaglio titolo contabile</pre>*/
    private SwTrovatoDtc swTrovatoDtc = new SwTrovatoDtc();
    /**Original name: WK-FLAG-PROD<br>
	 * <pre>--> VERIFICA PRESENZA AREA CACHE PRODOTTO</pre>*/
    private WkFlagProd wkFlagProd = new WkFlagProd();
    //Original name: WK-0040-ELE-MAX
    private short wk0040EleMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WK-0040-TABELLA
    private LazyArrayCopy<Wk0040Tabella> wk0040Tabella = new LazyArrayCopy<Wk0040Tabella>(new Wk0040Tabella(), 1, WK0040_TABELLA_MAXOCCURS);
    //Original name: WS-0040-DATI-INPUT
    private Ispc0040DatiInput ws0040DatiInput = new Ispc0040DatiInput();
    //Original name: WK-GESTIONE-MSG-ERR
    private WkGestioneMsgErr wkGestioneMsgErr = new WkGestioneMsgErr();
    //Original name: WK-APPO-OGB-ID-OGG-BLOCCO
    private int wkAppoOgbIdOggBlocco = 0;
    //Original name: WK-DT-EFF-BLC
    private int wkDtEffBlc = 0;
    //Original name: WK-APPO-DT-NUM
    private String wkAppoDtNum = "00000000";
    //Original name: WK-DT-RICOR-PREC
    private int wkDtRicorPrec = 0;
    //Original name: WK-DT-RICOR-SUCC
    private int wkDtRicorSucc = 0;
    //Original name: WK-STB-ID-OGG
    private int wkStbIdOgg = DefaultValues.INT_VAL;
    //Original name: WK-STB-TP-OGG
    private String wkStbTpOgg = DefaultValues.stringVal(Len.WK_STB_TP_OGG);
    //Original name: WK-APPO-DT
    private WkAppoDtLoas0670 wkAppoDt = new WkAppoDtLoas0670();
    //Original name: WK-TABELLA
    private String wkTabella = "";
    /**Original name: WK-0040-ELEMENTI-MAX<br>
	 * <pre> --> File Status file di output</pre>*/
    private short wk0040ElementiMax = ((short)500);
    /**Original name: WS-FS-OUT<br>
	 * <pre> changes SIR FCTVI00011410 starts here</pre>*/
    private String wsFsOut = "00";
    /**Original name: WK-PGM<br>
	 * <pre> changes SIR FCTVI00011410 ends here
	 *  --> Nome del programma Businesss Service</pre>*/
    private String wkPgm = "LOAS0310";
    /**Original name: WK-APPO-ID-GAR<br>
	 * <pre> --> Area di appoggio per gli identificativi
	 *  --> degli oggetti lavorati</pre>*/
    private int wkAppoIdGar = 0;
    //Original name: WK-ID-POLI-DISPLAY
    private String wkIdPoliDisplay = DefaultValues.stringVal(Len.WK_ID_POLI_DISPLAY);
    //Original name: WK-IB-POLI-DISPLAY
    private String wkIbPoliDisplay = DefaultValues.stringVal(Len.WK_IB_POLI_DISPLAY);
    //Original name: WK-DATA-AMG
    private WkDataAmg wkDataAmg = new WkDataAmg();
    //Original name: WSKD-AREA-SCHEDA
    private WskdAreaScheda wskdAreaScheda = new WskdAreaScheda();
    //Original name: IVVC0212
    private Ivvc0212 ivvc0212 = new Ivvc0212();
    //Original name: AREA-IO-IVVS0211
    private InputIvvs0211 areaIoIvvs0211 = new InputIvvs0211();
    //Original name: IVVC0218
    private Ivvc0218Loas0310 ivvc0218 = new Ivvc0218Loas0310();
    //Original name: WK-APPO-LUNGHEZZA
    private int wkAppoLunghezza = DefaultValues.INT_VAL;
    //Original name: LCCC0211
    private Lccc0211 lccc0211 = new Lccc0211();
    //Original name: LOAR0171
    private Loar0171 loar0171 = new Loar0171();
    //Original name: LCCVGRZZ
    private Lccvgrzz lccvgrzz = new Lccvgrzz();
    //Original name: WK-TGA-MAX
    private WkTgaMax wkTgaMax = new WkTgaMax();
    /**Original name: LDBS3360<br>
	 * <pre>  --> MODULO PER LETTURA TABELLA OGGETTO DEROGA</pre>*/
    private String ldbs3360 = "LDBS3360";
    //Original name: W660-AREA-LOAS0660
    private W660AreaPag w660AreaLoas0660 = new W660AreaPag();
    //Original name: W670-AREA-LOAS0670
    private W670AreaLoas0670 w670AreaLoas0670 = new W670AreaLoas0670();
    //Original name: W870-AREA-LOAS0870
    private W870AreaLoas0870 w870AreaLoas0870 = new W870AreaLoas0870();
    //Original name: LOAC0002-AREA-GEST-DEROGHE
    private Loac0002AreaGestDeroghe loac0002AreaGestDeroghe = new Loac0002AreaGestDeroghe();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: AREA-IO-ISPS0040
    private AreaIoIsps0040 areaIoIsps0040 = new AreaIoIsps0040();
    /**Original name: LOAP1-ID-POLI<br>
	 * <pre>-----------------------------------------------------------------
	 *  AREA COPY PROC. LOAP0001
	 * -----------------------------------------------------------------
	 * -->  Identificativo Polizza</pre>*/
    private int loap1IdPoli = DefaultValues.INT_VAL;
    /**Original name: LOAP1-ID-ADES<br>
	 * <pre>-->  Identificativo Adesione</pre>*/
    private int loap1IdAdes = DefaultValues.INT_VAL;
    //Original name: IWFI0051-CAMPO-INPUT
    private char[] iwfi0051CampoInput = new char[IWFI0051_CAMPO_INPUT_MAXOCCURS];
    //Original name: WAPPL-NUM-ELAB
    private WapplNumElab wapplNumElab = new WapplNumElab();
    //Original name: AREA-IO-LCCS0022
    private Lccc0022AreaComunicaz areaIoLccs0022 = new Lccc0022AreaComunicaz();
    //Original name: AREA-IO-LCCS0023
    private AreaIoLccs0023 areaIoLccs0023 = new AreaIoLccs0023();
    //Original name: AREA-IO-LOAS0280
    private Workarea areaIoLoas0280 = new Workarea();
    /**Original name: WS-TP-FRM-ASSVA<br>
	 * <pre>----------------------------------------------------------------*
	 *   AREE TIPOLOGICHE DI PORTAFOGLIO
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_FRM_ASSVA (Forma Assicurativa)
	 * *****************************************************************</pre>*/
    private WsTpFrmAssva wsTpFrmAssva = new WsTpFrmAssva();
    /**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    /**Original name: WS-TP-CAUS<br>
	 * <pre>*****************************************************************
	 *     TP_CAUS (Tipo Causale)
	 * *****************************************************************</pre>*/
    private WsTpCaus wsTpCaus = new WsTpCaus();
    /**Original name: WS-TP-STAT-BUS<br>
	 * <pre>*****************************************************************
	 *     TP_STAT_BUS (Stato Oggetto di Business)
	 * *****************************************************************</pre>*/
    private WsTpStatBus wsTpStatBus = new WsTpStatBus();
    //Original name: ISPV0000
    private Ispv0000 ispv0000 = new Ispv0000();
    //Original name: WORK-COMMAREA
    private WorkCommarea workCommarea = new WorkCommarea();
    //Original name: VGRZ-ELE-GAR-MAX
    private short vgrzEleGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: VGRZ-TAB-GAR
    private WgrzTabGar[] vgrzTabGar = new WgrzTabGar[VGRZ_TAB_GAR_MAXOCCURS];
    //Original name: VTGA-AREA-TRANCHE
    private VtgaAreaTranche vtgaAreaTranche = new VtgaAreaTranche();
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia = new WgrzAreaGaranziaLccs0005();
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTrancheLoas0310 wtgaAreaTranche = new WtgaAreaTrancheLoas0310();
    //Original name: WPREC-TAB-TRAN
    private WprecTabTran[] wprecTabTran = new WprecTabTran[WPREC_TAB_TRAN_MAXOCCURS];
    //Original name: LCCVMOV1
    private Lccvmov1Loas0310 lccvmov1 = new Lccvmov1Loas0310();
    //Original name: IDSV0015
    private Idsv0015 idsv0015 = new Idsv0015();
    //Original name: INT-REGISTER1
    private short intRegister1 = DefaultValues.SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Loas0310Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int iwfi0051CampoInputIdx = 1; iwfi0051CampoInputIdx <= IWFI0051_CAMPO_INPUT_MAXOCCURS; iwfi0051CampoInputIdx++) {
            setIwfi0051CampoInput(iwfi0051CampoInputIdx, DefaultValues.CHAR_VAL);
        }
        for (int vgrzTabGarIdx = 1; vgrzTabGarIdx <= VGRZ_TAB_GAR_MAXOCCURS; vgrzTabGarIdx++) {
            vgrzTabGar[vgrzTabGarIdx - 1] = new WgrzTabGar();
        }
        for (int wprecTabTranIdx = 1; wprecTabTranIdx <= WPREC_TAB_TRAN_MAXOCCURS; wprecTabTranIdx++) {
            wprecTabTran[wprecTabTranIdx - 1] = new WprecTabTran();
        }
    }

    public void setFsOut(String fsOut) {
        this.fsOut = Functions.subString(fsOut, Len.FS_OUT);
    }

    public String getFsOut() {
        return this.fsOut;
    }

    public void setWkOggetto9(int wkOggetto9) {
        this.wkOggetto9 = NumericDisplay.asString(wkOggetto9, Len.WK_OGGETTO9);
    }

    public int getWkOggetto9() {
        return NumericDisplay.asInt(this.wkOggetto9);
    }

    public String getWkOggetto9Formatted() {
        return this.wkOggetto9;
    }

    public void setWkOggettoX(String wkOggettoX) {
        this.wkOggettoX = Functions.subString(wkOggettoX, Len.WK_OGGETTO_X);
    }

    public String getWkOggettoX() {
        return this.wkOggettoX;
    }

    public String getWkOggettoXFormatted() {
        return Functions.padBlanks(getWkOggettoX(), Len.WK_OGGETTO_X);
    }

    public void setWkLivDeroga(String wkLivDeroga) {
        this.wkLivDeroga = Functions.subString(wkLivDeroga, Len.WK_LIV_DEROGA);
    }

    public String getWkLivDeroga() {
        return this.wkLivDeroga;
    }

    public String getWkLivDerogaFormatted() {
        return Functions.padBlanks(getWkLivDeroga(), Len.WK_LIV_DEROGA);
    }

    public void setFlMovFound(boolean flMovFound) {
        this.flMovFound = flMovFound;
    }

    public boolean isFlMovFound() {
        return this.flMovFound;
    }

    public void setWk0040EleMax(short wk0040EleMax) {
        this.wk0040EleMax = wk0040EleMax;
    }

    public short getWk0040EleMax() {
        return this.wk0040EleMax;
    }

    public void setWkAppoOgbIdOggBlocco(int wkAppoOgbIdOggBlocco) {
        this.wkAppoOgbIdOggBlocco = wkAppoOgbIdOggBlocco;
    }

    public int getWkAppoOgbIdOggBlocco() {
        return this.wkAppoOgbIdOggBlocco;
    }

    public void setWkDtEffBlc(int wkDtEffBlc) {
        this.wkDtEffBlc = wkDtEffBlc;
    }

    public int getWkDtEffBlc() {
        return this.wkDtEffBlc;
    }

    public void setWkAppoDtNum(int wkAppoDtNum) {
        this.wkAppoDtNum = NumericDisplay.asString(wkAppoDtNum, Len.WK_APPO_DT_NUM);
    }

    public int getWkAppoDtNum() {
        return NumericDisplay.asInt(this.wkAppoDtNum);
    }

    public String getWkAppoDtNumFormatted() {
        return this.wkAppoDtNum;
    }

    public void setWkDtRicorPrec(int wkDtRicorPrec) {
        this.wkDtRicorPrec = wkDtRicorPrec;
    }

    public int getWkDtRicorPrec() {
        return this.wkDtRicorPrec;
    }

    public void setWkDtRicorSucc(int wkDtRicorSucc) {
        this.wkDtRicorSucc = wkDtRicorSucc;
    }

    public int getWkDtRicorSucc() {
        return this.wkDtRicorSucc;
    }

    public void setWkStbIdOgg(int wkStbIdOgg) {
        this.wkStbIdOgg = wkStbIdOgg;
    }

    public int getWkStbIdOgg() {
        return this.wkStbIdOgg;
    }

    public void setWkStbTpOgg(String wkStbTpOgg) {
        this.wkStbTpOgg = Functions.subString(wkStbTpOgg, Len.WK_STB_TP_OGG);
    }

    public String getWkStbTpOgg() {
        return this.wkStbTpOgg;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public short getWk0040ElementiMax() {
        return this.wk0040ElementiMax;
    }

    public void setWsFsOut(String wsFsOut) {
        this.wsFsOut = Functions.subString(wsFsOut, Len.WS_FS_OUT);
    }

    public String getWsFsOut() {
        return this.wsFsOut;
    }

    public String getWsFsOutFormatted() {
        return Functions.padBlanks(getWsFsOut(), Len.WS_FS_OUT);
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkAppoIdGar(int wkAppoIdGar) {
        this.wkAppoIdGar = wkAppoIdGar;
    }

    public int getWkAppoIdGar() {
        return this.wkAppoIdGar;
    }

    public void setWkIdPoliDisplay(int wkIdPoliDisplay) {
        this.wkIdPoliDisplay = NumericDisplay.asString(wkIdPoliDisplay, Len.WK_ID_POLI_DISPLAY);
    }

    public int getWkIdPoliDisplay() {
        return NumericDisplay.asInt(this.wkIdPoliDisplay);
    }

    public String getWkIdPoliDisplayFormatted() {
        return this.wkIdPoliDisplay;
    }

    public String getWkIdPoliDisplayAsString() {
        return getWkIdPoliDisplayFormatted();
    }

    public void setWkIbPoliDisplay(String wkIbPoliDisplay) {
        this.wkIbPoliDisplay = Functions.subString(wkIbPoliDisplay, Len.WK_IB_POLI_DISPLAY);
    }

    public String getWkIbPoliDisplay() {
        return this.wkIbPoliDisplay;
    }

    public String getWkIbPoliDisplayFormatted() {
        return Functions.padBlanks(getWkIbPoliDisplay(), Len.WK_IB_POLI_DISPLAY);
    }

    public void setWkAppoLunghezza(int wkAppoLunghezza) {
        this.wkAppoLunghezza = wkAppoLunghezza;
    }

    public int getWkAppoLunghezza() {
        return this.wkAppoLunghezza;
    }

    public void setwRecOutrivaFormatted(String data) {
        byte[] buffer = new byte[Len.W_REC_OUTRIVA];
        MarshalByte.writeString(buffer, 1, data, Len.W_REC_OUTRIVA);
        setwRecOutrivaBytes(buffer, 1);
    }

    public String getwRecOutrivaFormatted() {
        return MarshalByteExt.bufferToStr(getwRecOutrivaBytes());
    }

    /**Original name: W-REC-OUTRIVA<br>
	 * <pre>----------------------------------------------------------------*
	 *   AREE PER LA GESTIONE DEL FILE DI OUTPUT
	 * ----------------------------------------------------------------*
	 *  --> Copy del tracciato record del file di output</pre>*/
    public byte[] getwRecOutrivaBytes() {
        byte[] buffer = new byte[Len.W_REC_OUTRIVA];
        return getwRecOutrivaBytes(buffer, 1);
    }

    public void setwRecOutrivaBytes(byte[] buffer, int offset) {
        int position = offset;
        loar0171.setWrecTipoRec(MarshalByte.readString(buffer, position, Loar0171.Len.WREC_TIPO_REC));
        position += Loar0171.Len.WREC_TIPO_REC;
        loar0171.getWrecOut().setWrecOutFromBuffer(buffer, position);
    }

    public byte[] getwRecOutrivaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, loar0171.getWrecTipoRec(), Loar0171.Len.WREC_TIPO_REC);
        position += Loar0171.Len.WREC_TIPO_REC;
        loar0171.getWrecOut().getWrecOutAsBuffer(buffer, position);
        return buffer;
    }

    public String getLdbs3360() {
        return this.ldbs3360;
    }

    public String getLdbs3360Formatted() {
        return Functions.padBlanks(getLdbs3360(), Len.LDBS3360);
    }

    public void setLoap1IdPoli(int loap1IdPoli) {
        this.loap1IdPoli = loap1IdPoli;
    }

    public int getLoap1IdPoli() {
        return this.loap1IdPoli;
    }

    public void setLoap1IdAdes(int loap1IdAdes) {
        this.loap1IdAdes = loap1IdAdes;
    }

    public int getLoap1IdAdes() {
        return this.loap1IdAdes;
    }

    public void setIwfi0051CampoInput(int iwfi0051CampoInputIdx, char iwfi0051CampoInput) {
        this.iwfi0051CampoInput[iwfi0051CampoInputIdx - 1] = iwfi0051CampoInput;
    }

    public char getIwfi0051CampoInput(int iwfi0051CampoInputIdx) {
        return this.iwfi0051CampoInput[iwfi0051CampoInputIdx - 1];
    }

    public String getVgrzAreaGaranziaFormatted() {
        return MarshalByteExt.bufferToStr(getVgrzAreaGaranziaBytes());
    }

    public void setVgrzAreaGaranziaBytes(byte[] buffer) {
        setVgrzAreaGaranziaBytes(buffer, 1);
    }

    /**Original name: VGRZ-AREA-GARANZIA<br>
	 * <pre>----------------------------------------------------------------*
	 *     AREA TABELLE RIVALUTAZIONE DA PASSARE AL VALORIZZATORE
	 * ----------------------------------------------------------------*
	 *  --  AREA GARANZIA</pre>*/
    public byte[] getVgrzAreaGaranziaBytes() {
        byte[] buffer = new byte[Len.VGRZ_AREA_GARANZIA];
        return getVgrzAreaGaranziaBytes(buffer, 1);
    }

    public void setVgrzAreaGaranziaBytes(byte[] buffer, int offset) {
        int position = offset;
        vgrzEleGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VGRZ_TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                vgrzTabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                vgrzTabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public byte[] getVgrzAreaGaranziaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, vgrzEleGarMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= VGRZ_TAB_GAR_MAXOCCURS; idx++) {
            vgrzTabGar[idx - 1].getTabGarBytes(buffer, position);
            position += WgrzTabGar.Len.TAB_GAR;
        }
        return buffer;
    }

    public void setVgrzEleGarMax(short vgrzEleGarMax) {
        this.vgrzEleGarMax = vgrzEleGarMax;
    }

    public short getVgrzEleGarMax() {
        return this.vgrzEleGarMax;
    }

    public void setIntRegister1(short intRegister1) {
        this.intRegister1 = intRegister1;
    }

    public short getIntRegister1() {
        return this.intRegister1;
    }

    public AdesIvvs0216 getAdes() {
        return ades;
    }

    public AreaIoIsps0040 getAreaIoIsps0040() {
        return areaIoIsps0040;
    }

    public InputIvvs0211 getAreaIoIvvs0211() {
        return areaIoIvvs0211;
    }

    public Lccc0022AreaComunicaz getAreaIoLccs0022() {
        return areaIoLccs0022;
    }

    public AreaIoLccs0023 getAreaIoLccs0023() {
        return areaIoLccs0023;
    }

    public Workarea getAreaIoLoas0280() {
        return areaIoLoas0280;
    }

    public DettTitCont getDettTitCont() {
        return dettTitCont;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public GarIvvs0216 getGar() {
        return gar;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Idsv0015 getIdsv0015() {
        return idsv0015;
    }

    public Idsv8888 getIdsv8888() {
        return idsv8888;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Ispv0000 getIspv0000() {
        return ispv0000;
    }

    public Ivvc0212 getIvvc0212() {
        return ivvc0212;
    }

    public Ivvc0218Loas0310 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndiciLoas0310 getIxIndici() {
        return ixIndici;
    }

    public Lccc0211 getLccc0211() {
        return lccc0211;
    }

    public Lccvgrzz getLccvgrzz() {
        return lccvgrzz;
    }

    public Lccvmov1Loas0310 getLccvmov1() {
        return lccvmov1;
    }

    public Ldbv0011 getLdbv0011() {
        return ldbv0011;
    }

    public Ldbv3361 getLdbv3361() {
        return ldbv3361;
    }

    public Ldbvd601 getLdbvd601() {
        return ldbvd601;
    }

    public Loac0002AreaGestDeroghe getLoac0002AreaGestDeroghe() {
        return loac0002AreaGestDeroghe;
    }

    public Loar0171 getLoar0171() {
        return loar0171;
    }

    public Movi getMovi() {
        return movi;
    }

    public OggDeroga getOggDeroga() {
        return oggDeroga;
    }

    public ParamComp getParamComp() {
        return paramComp;
    }

    public Poli getPoli() {
        return poli;
    }

    public StatOggBus getStatOggBus() {
        return statOggBus;
    }

    public SwCurDtc getSwCurDtc() {
        return swCurDtc;
    }

    public SwTrovatoDtc getSwTrovatoDtc() {
        return swTrovatoDtc;
    }

    public TitCont getTitCont() {
        return titCont;
    }

    public TrchDiGarIvvs0216 getTrchDiGar() {
        return trchDiGar;
    }

    public WgrzTabGar getVgrzTabGar(int idx) {
        return vgrzTabGar[idx - 1];
    }

    public VtgaAreaTranche getVtgaAreaTranche() {
        return vtgaAreaTranche;
    }

    public W660AreaPag getW660AreaLoas0660() {
        return w660AreaLoas0660;
    }

    public W670AreaLoas0670 getW670AreaLoas0670() {
        return w670AreaLoas0670;
    }

    public W870AreaLoas0870 getW870AreaLoas0870() {
        return w870AreaLoas0870;
    }

    public WapplNumElab getWapplNumElab() {
        return wapplNumElab;
    }

    public WgrzAreaGaranziaLccs0005 getWgrzAreaGaranzia() {
        return wgrzAreaGaranzia;
    }

    public Wk0040Tabella getWk0040Tabella(int idx) {
        return wk0040Tabella.get(idx - 1);
    }

    public LazyArrayCopy<Wk0040Tabella> getWk0040TabellaObj() {
        return wk0040Tabella;
    }

    public WkAppoDtLoas0670 getWkAppoDt() {
        return wkAppoDt;
    }

    public WkBlcTrovato getWkBlcTrovato() {
        return wkBlcTrovato;
    }

    public WkCostantiTabelle getWkCostantiTabelle() {
        return wkCostantiTabelle;
    }

    public WkDataAmg getWkDataAmg() {
        return wkDataAmg;
    }

    public WkFineFetch getWkFineFetch() {
        return wkFineFetch;
    }

    public WkFlagProd getWkFlagProd() {
        return wkFlagProd;
    }

    public WkGestioneMsgErr getWkGestioneMsgErr() {
        return wkGestioneMsgErr;
    }

    public WkGrzTrovata getWkGrzTrovata() {
        return wkGrzTrovata;
    }

    public WkOrigRiv getWkOrigRiv() {
        return wkOrigRiv;
    }

    public WkPrecTrovato getWkPrecTrovato() {
        return wkPrecTrovato;
    }

    public WkScartaGar getWkScartaGar() {
        return wkScartaGar;
    }

    public WkScartaTga getWkScartaTga() {
        return wkScartaTga;
    }

    public WkStbTrovato getWkStbTrovato() {
        return wkStbTrovato;
    }

    public WkTgaMax getWkTgaMax() {
        return wkTgaMax;
    }

    public WkTitTrovato getWkTitTrovato() {
        return wkTitTrovato;
    }

    public WkTrancheDaCaricare getWkTrancheDaCaricare() {
        return wkTrancheDaCaricare;
    }

    public WkTrovato getWkTrovato() {
        return wkTrovato;
    }

    public WorkCommarea getWorkCommarea() {
        return workCommarea;
    }

    public WprecTabTran getWprecTabTran(int idx) {
        return wprecTabTran[idx - 1];
    }

    public Ispc0040DatiInput getWs0040DatiInput() {
        return ws0040DatiInput;
    }

    public WsTpCaus getWsTpCaus() {
        return wsTpCaus;
    }

    public WsTpFrmAssva getWsTpFrmAssva() {
        return wsTpFrmAssva;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    public WsTpStatBus getWsTpStatBus() {
        return wsTpStatBus;
    }

    public WskdAreaScheda getWskdAreaScheda() {
        return wskdAreaScheda;
    }

    public WtgaAreaTrancheLoas0310 getWtgaAreaTranche() {
        return wtgaAreaTranche;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_OGGETTO9 = 9;
        public static final int WK_OGGETTO_X = 9;
        public static final int WK_LIV_DEROGA = 10;
        public static final int WK_APPO_DT_RICOR_SUCC = 8;
        public static final int WK_APPO_DT_NUM = 8;
        public static final int WK_DT_RICOR_PREC = 8;
        public static final int WK_DT_RICOR_SUCC = 8;
        public static final int WK_STB_TP_OGG = 2;
        public static final int WK_PGM_CALL = 8;
        public static final int WK_CURRENT_DATE = 8;
        public static final int WK_GG_INC = 5;
        public static final int WK_PERMANFEE = 5;
        public static final int WK_MESIDIFFMFEE = 5;
        public static final int WK_ID_POLI_DISPLAY = 9;
        public static final int WK_IB_POLI_DISPLAY = 11;
        public static final int GG_DIFF = 5;
        public static final int WK_COM_AA = 4;
        public static final int WK_COM_MM = 2;
        public static final int WK_APPO_LUNGHEZZA = 8;
        public static final int IN_RCODE = 2;
        public static final int WK_DATA_CALCOLATA = 8;
        public static final int WK_TABELLA = 8;
        public static final int FS_OUT = 2;
        public static final int VGRZ_ELE_GAR_MAX = 2;
        public static final int VGRZ_AREA_GARANZIA = VGRZ_ELE_GAR_MAX + Loas0310Data.VGRZ_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int W_REC_OUTRIVA = Loar0171.Len.WREC_TIPO_REC + WrecOut.Len.WREC_OUT;
        public static final int WS_FS_OUT = 2;
        public static final int LDBS3360 = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
