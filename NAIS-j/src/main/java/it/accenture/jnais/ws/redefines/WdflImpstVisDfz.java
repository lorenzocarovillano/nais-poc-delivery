package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-VIS-DFZ<br>
 * Variable: WDFL-IMPST-VIS-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstVisDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstVisDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_VIS_DFZ;
    }

    public void setWdflImpstVisDfz(AfDecimal wdflImpstVisDfz) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_VIS_DFZ, wdflImpstVisDfz.copy());
    }

    public void setWdflImpstVisDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_VIS_DFZ, Pos.WDFL_IMPST_VIS_DFZ);
    }

    /**Original name: WDFL-IMPST-VIS-DFZ<br>*/
    public AfDecimal getWdflImpstVisDfz() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_VIS_DFZ, Len.Int.WDFL_IMPST_VIS_DFZ, Len.Fract.WDFL_IMPST_VIS_DFZ);
    }

    public byte[] getWdflImpstVisDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_VIS_DFZ, Pos.WDFL_IMPST_VIS_DFZ);
        return buffer;
    }

    public void setWdflImpstVisDfzNull(String wdflImpstVisDfzNull) {
        writeString(Pos.WDFL_IMPST_VIS_DFZ_NULL, wdflImpstVisDfzNull, Len.WDFL_IMPST_VIS_DFZ_NULL);
    }

    /**Original name: WDFL-IMPST-VIS-DFZ-NULL<br>*/
    public String getWdflImpstVisDfzNull() {
        return readString(Pos.WDFL_IMPST_VIS_DFZ_NULL, Len.WDFL_IMPST_VIS_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS_DFZ = 1;
        public static final int WDFL_IMPST_VIS_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS_DFZ = 8;
        public static final int WDFL_IMPST_VIS_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
