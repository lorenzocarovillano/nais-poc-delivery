package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-FG-MOVI-ANN<br>
 * Variable: WS-FG-MOVI-ANN from program LCCS0023<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFgMoviAnn {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_MOVI_ANN = 'S';
    public static final char NO_MOVI_ANN = 'N';

    //==== METHODS ====
    public void setWsFgMoviAnn(char wsFgMoviAnn) {
        this.value = wsFgMoviAnn;
    }

    public char getWsFgMoviAnn() {
        return this.value;
    }

    public void setSiMoviAnn() {
        value = SI_MOVI_ANN;
    }

    public boolean isNoMoviAnn() {
        return value == NO_MOVI_ANN;
    }

    public void setNoMoviAnn() {
        value = NO_MOVI_ANN;
    }
}
