package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-DUR-GG-ADES-DFLT<br>
 * Variable: DAD-DUR-GG-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadDurGgAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadDurGgAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_DUR_GG_ADES_DFLT;
    }

    public void setDadDurGgAdesDflt(int dadDurGgAdesDflt) {
        writeIntAsPacked(Pos.DAD_DUR_GG_ADES_DFLT, dadDurGgAdesDflt, Len.Int.DAD_DUR_GG_ADES_DFLT);
    }

    public void setDadDurGgAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_DUR_GG_ADES_DFLT, Pos.DAD_DUR_GG_ADES_DFLT);
    }

    /**Original name: DAD-DUR-GG-ADES-DFLT<br>*/
    public int getDadDurGgAdesDflt() {
        return readPackedAsInt(Pos.DAD_DUR_GG_ADES_DFLT, Len.Int.DAD_DUR_GG_ADES_DFLT);
    }

    public byte[] getDadDurGgAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_DUR_GG_ADES_DFLT, Pos.DAD_DUR_GG_ADES_DFLT);
        return buffer;
    }

    public void setDadDurGgAdesDfltNull(String dadDurGgAdesDfltNull) {
        writeString(Pos.DAD_DUR_GG_ADES_DFLT_NULL, dadDurGgAdesDfltNull, Len.DAD_DUR_GG_ADES_DFLT_NULL);
    }

    /**Original name: DAD-DUR-GG-ADES-DFLT-NULL<br>*/
    public String getDadDurGgAdesDfltNull() {
        return readString(Pos.DAD_DUR_GG_ADES_DFLT_NULL, Len.DAD_DUR_GG_ADES_DFLT_NULL);
    }

    public String getDadDurGgAdesDfltNullFormatted() {
        return Functions.padBlanks(getDadDurGgAdesDfltNull(), Len.DAD_DUR_GG_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_DUR_GG_ADES_DFLT = 1;
        public static final int DAD_DUR_GG_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_DUR_GG_ADES_DFLT = 3;
        public static final int DAD_DUR_GG_ADES_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_DUR_GG_ADES_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
