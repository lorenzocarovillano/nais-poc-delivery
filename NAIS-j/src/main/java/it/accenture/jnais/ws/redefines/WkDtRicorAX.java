package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WK-DT-RICOR-A-X<br>
 * Variable: WK-DT-RICOR-A-X from program LRGS0660<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkDtRicorAX extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkDtRicorAX() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_DT_RICOR_A_X;
    }

    public void setWkDtRicorAX(String wkDtRicorAX) {
        writeString(Pos.WK_DT_RICOR_A_X, wkDtRicorAX, Len.WK_DT_RICOR_A_X);
    }

    /**Original name: WK-DT-RICOR-A-X<br>*/
    public String getWkDtRicorAX() {
        return readString(Pos.WK_DT_RICOR_A_X, Len.WK_DT_RICOR_A_X);
    }

    public String getWkDtRicorAFormatted() {
        return readFixedString(Pos.WK_DT_RICOR_A, Len.WK_DT_RICOR_A);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_DT_RICOR_A_X = 1;
        public static final int WK_DT_RICOR_A = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_DT_RICOR_A_X = 8;
        public static final int WK_DT_RICOR_A = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
