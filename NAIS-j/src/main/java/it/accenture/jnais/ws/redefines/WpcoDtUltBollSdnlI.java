package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-SDNL-I<br>
 * Variable: WPCO-DT-ULT-BOLL-SDNL-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollSdnlI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollSdnlI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_SDNL_I;
    }

    public void setWpcoDtUltBollSdnlI(int wpcoDtUltBollSdnlI) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_SDNL_I, wpcoDtUltBollSdnlI, Len.Int.WPCO_DT_ULT_BOLL_SDNL_I);
    }

    public void setDpcoDtUltBollSdnlIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_SDNL_I, Pos.WPCO_DT_ULT_BOLL_SDNL_I);
    }

    /**Original name: WPCO-DT-ULT-BOLL-SDNL-I<br>*/
    public int getWpcoDtUltBollSdnlI() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_SDNL_I, Len.Int.WPCO_DT_ULT_BOLL_SDNL_I);
    }

    public byte[] getWpcoDtUltBollSdnlIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_SDNL_I, Pos.WPCO_DT_ULT_BOLL_SDNL_I);
        return buffer;
    }

    public void setWpcoDtUltBollSdnlINull(String wpcoDtUltBollSdnlINull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_SDNL_I_NULL, wpcoDtUltBollSdnlINull, Len.WPCO_DT_ULT_BOLL_SDNL_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-SDNL-I-NULL<br>*/
    public String getWpcoDtUltBollSdnlINull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_SDNL_I_NULL, Len.WPCO_DT_ULT_BOLL_SDNL_I_NULL);
    }

    public String getWpcoDtUltBollSdnlINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollSdnlINull(), Len.WPCO_DT_ULT_BOLL_SDNL_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_SDNL_I = 1;
        public static final int WPCO_DT_ULT_BOLL_SDNL_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_SDNL_I = 5;
        public static final int WPCO_DT_ULT_BOLL_SDNL_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_SDNL_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
