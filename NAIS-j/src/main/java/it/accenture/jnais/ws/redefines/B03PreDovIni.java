package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRE-DOV-INI<br>
 * Variable: B03-PRE-DOV-INI from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PreDovIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PreDovIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRE_DOV_INI;
    }

    public void setB03PreDovIni(AfDecimal b03PreDovIni) {
        writeDecimalAsPacked(Pos.B03_PRE_DOV_INI, b03PreDovIni.copy());
    }

    public void setB03PreDovIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRE_DOV_INI, Pos.B03_PRE_DOV_INI);
    }

    /**Original name: B03-PRE-DOV-INI<br>*/
    public AfDecimal getB03PreDovIni() {
        return readPackedAsDecimal(Pos.B03_PRE_DOV_INI, Len.Int.B03_PRE_DOV_INI, Len.Fract.B03_PRE_DOV_INI);
    }

    public byte[] getB03PreDovIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRE_DOV_INI, Pos.B03_PRE_DOV_INI);
        return buffer;
    }

    public void setB03PreDovIniNull(String b03PreDovIniNull) {
        writeString(Pos.B03_PRE_DOV_INI_NULL, b03PreDovIniNull, Len.B03_PRE_DOV_INI_NULL);
    }

    /**Original name: B03-PRE-DOV-INI-NULL<br>*/
    public String getB03PreDovIniNull() {
        return readString(Pos.B03_PRE_DOV_INI_NULL, Len.B03_PRE_DOV_INI_NULL);
    }

    public String getB03PreDovIniNullFormatted() {
        return Functions.padBlanks(getB03PreDovIniNull(), Len.B03_PRE_DOV_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRE_DOV_INI = 1;
        public static final int B03_PRE_DOV_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRE_DOV_INI = 8;
        public static final int B03_PRE_DOV_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRE_DOV_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRE_DOV_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
