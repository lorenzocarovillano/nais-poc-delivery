package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-NET-TRASFERITO<br>
 * Variable: DFA-IMP-NET-TRASFERITO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpNetTrasferito extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpNetTrasferito() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_NET_TRASFERITO;
    }

    public void setDfaImpNetTrasferito(AfDecimal dfaImpNetTrasferito) {
        writeDecimalAsPacked(Pos.DFA_IMP_NET_TRASFERITO, dfaImpNetTrasferito.copy());
    }

    public void setDfaImpNetTrasferitoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_NET_TRASFERITO, Pos.DFA_IMP_NET_TRASFERITO);
    }

    /**Original name: DFA-IMP-NET-TRASFERITO<br>*/
    public AfDecimal getDfaImpNetTrasferito() {
        return readPackedAsDecimal(Pos.DFA_IMP_NET_TRASFERITO, Len.Int.DFA_IMP_NET_TRASFERITO, Len.Fract.DFA_IMP_NET_TRASFERITO);
    }

    public byte[] getDfaImpNetTrasferitoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_NET_TRASFERITO, Pos.DFA_IMP_NET_TRASFERITO);
        return buffer;
    }

    public void setDfaImpNetTrasferitoNull(String dfaImpNetTrasferitoNull) {
        writeString(Pos.DFA_IMP_NET_TRASFERITO_NULL, dfaImpNetTrasferitoNull, Len.DFA_IMP_NET_TRASFERITO_NULL);
    }

    /**Original name: DFA-IMP-NET-TRASFERITO-NULL<br>*/
    public String getDfaImpNetTrasferitoNull() {
        return readString(Pos.DFA_IMP_NET_TRASFERITO_NULL, Len.DFA_IMP_NET_TRASFERITO_NULL);
    }

    public String getDfaImpNetTrasferitoNullFormatted() {
        return Functions.padBlanks(getDfaImpNetTrasferitoNull(), Len.DFA_IMP_NET_TRASFERITO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_NET_TRASFERITO = 1;
        public static final int DFA_IMP_NET_TRASFERITO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_NET_TRASFERITO = 8;
        public static final int DFA_IMP_NET_TRASFERITO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_NET_TRASFERITO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_NET_TRASFERITO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
