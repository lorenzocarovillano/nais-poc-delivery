package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-AT92-I<br>
 * Variable: PCO-DT-ULT-ELAB-AT92-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabAt92I extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabAt92I() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_AT92_I;
    }

    public void setPcoDtUltElabAt92I(int pcoDtUltElabAt92I) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_AT92_I, pcoDtUltElabAt92I, Len.Int.PCO_DT_ULT_ELAB_AT92_I);
    }

    public void setPcoDtUltElabAt92IFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_AT92_I, Pos.PCO_DT_ULT_ELAB_AT92_I);
    }

    /**Original name: PCO-DT-ULT-ELAB-AT92-I<br>*/
    public int getPcoDtUltElabAt92I() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_AT92_I, Len.Int.PCO_DT_ULT_ELAB_AT92_I);
    }

    public byte[] getPcoDtUltElabAt92IAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_AT92_I, Pos.PCO_DT_ULT_ELAB_AT92_I);
        return buffer;
    }

    public void initPcoDtUltElabAt92IHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_AT92_I, Len.PCO_DT_ULT_ELAB_AT92_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabAt92INull(String pcoDtUltElabAt92INull) {
        writeString(Pos.PCO_DT_ULT_ELAB_AT92_I_NULL, pcoDtUltElabAt92INull, Len.PCO_DT_ULT_ELAB_AT92_I_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-AT92-I-NULL<br>*/
    public String getPcoDtUltElabAt92INull() {
        return readString(Pos.PCO_DT_ULT_ELAB_AT92_I_NULL, Len.PCO_DT_ULT_ELAB_AT92_I_NULL);
    }

    public String getPcoDtUltElabAt92INullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabAt92INull(), Len.PCO_DT_ULT_ELAB_AT92_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_AT92_I = 1;
        public static final int PCO_DT_ULT_ELAB_AT92_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_AT92_I = 5;
        public static final int PCO_DT_ULT_ELAB_AT92_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_AT92_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
