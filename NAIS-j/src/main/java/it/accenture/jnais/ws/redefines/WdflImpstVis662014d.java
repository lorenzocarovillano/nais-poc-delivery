package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-VIS-662014D<br>
 * Variable: WDFL-IMPST-VIS-662014D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstVis662014d extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstVis662014d() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_VIS662014D;
    }

    public void setWdflImpstVis662014d(AfDecimal wdflImpstVis662014d) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_VIS662014D, wdflImpstVis662014d.copy());
    }

    public void setWdflImpstVis662014dFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_VIS662014D, Pos.WDFL_IMPST_VIS662014D);
    }

    /**Original name: WDFL-IMPST-VIS-662014D<br>*/
    public AfDecimal getWdflImpstVis662014d() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_VIS662014D, Len.Int.WDFL_IMPST_VIS662014D, Len.Fract.WDFL_IMPST_VIS662014D);
    }

    public byte[] getWdflImpstVis662014dAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_VIS662014D, Pos.WDFL_IMPST_VIS662014D);
        return buffer;
    }

    public void setWdflImpstVis662014dNull(String wdflImpstVis662014dNull) {
        writeString(Pos.WDFL_IMPST_VIS662014D_NULL, wdflImpstVis662014dNull, Len.WDFL_IMPST_VIS662014D_NULL);
    }

    /**Original name: WDFL-IMPST-VIS-662014D-NULL<br>*/
    public String getWdflImpstVis662014dNull() {
        return readString(Pos.WDFL_IMPST_VIS662014D_NULL, Len.WDFL_IMPST_VIS662014D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS662014D = 1;
        public static final int WDFL_IMPST_VIS662014D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS662014D = 8;
        public static final int WDFL_IMPST_VIS662014D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS662014D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS662014D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
