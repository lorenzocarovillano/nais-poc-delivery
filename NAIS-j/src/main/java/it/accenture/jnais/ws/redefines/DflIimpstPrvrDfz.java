package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IIMPST-PRVR-DFZ<br>
 * Variable: DFL-IIMPST-PRVR-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIimpstPrvrDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIimpstPrvrDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IIMPST_PRVR_DFZ;
    }

    public void setDflIimpstPrvrDfz(AfDecimal dflIimpstPrvrDfz) {
        writeDecimalAsPacked(Pos.DFL_IIMPST_PRVR_DFZ, dflIimpstPrvrDfz.copy());
    }

    public void setDflIimpstPrvrDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IIMPST_PRVR_DFZ, Pos.DFL_IIMPST_PRVR_DFZ);
    }

    /**Original name: DFL-IIMPST-PRVR-DFZ<br>*/
    public AfDecimal getDflIimpstPrvrDfz() {
        return readPackedAsDecimal(Pos.DFL_IIMPST_PRVR_DFZ, Len.Int.DFL_IIMPST_PRVR_DFZ, Len.Fract.DFL_IIMPST_PRVR_DFZ);
    }

    public byte[] getDflIimpstPrvrDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IIMPST_PRVR_DFZ, Pos.DFL_IIMPST_PRVR_DFZ);
        return buffer;
    }

    public void setDflIimpstPrvrDfzNull(String dflIimpstPrvrDfzNull) {
        writeString(Pos.DFL_IIMPST_PRVR_DFZ_NULL, dflIimpstPrvrDfzNull, Len.DFL_IIMPST_PRVR_DFZ_NULL);
    }

    /**Original name: DFL-IIMPST-PRVR-DFZ-NULL<br>*/
    public String getDflIimpstPrvrDfzNull() {
        return readString(Pos.DFL_IIMPST_PRVR_DFZ_NULL, Len.DFL_IIMPST_PRVR_DFZ_NULL);
    }

    public String getDflIimpstPrvrDfzNullFormatted() {
        return Functions.padBlanks(getDflIimpstPrvrDfzNull(), Len.DFL_IIMPST_PRVR_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST_PRVR_DFZ = 1;
        public static final int DFL_IIMPST_PRVR_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST_PRVR_DFZ = 8;
        public static final int DFL_IIMPST_PRVR_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST_PRVR_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST_PRVR_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
