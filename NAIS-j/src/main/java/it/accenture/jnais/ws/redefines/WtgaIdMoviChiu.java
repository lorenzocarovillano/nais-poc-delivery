package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ID-MOVI-CHIU<br>
 * Variable: WTGA-ID-MOVI-CHIU from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ID_MOVI_CHIU;
    }

    public void setWtgaIdMoviChiu(int wtgaIdMoviChiu) {
        writeIntAsPacked(Pos.WTGA_ID_MOVI_CHIU, wtgaIdMoviChiu, Len.Int.WTGA_ID_MOVI_CHIU);
    }

    public void setWtgaIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ID_MOVI_CHIU, Pos.WTGA_ID_MOVI_CHIU);
    }

    /**Original name: WTGA-ID-MOVI-CHIU<br>*/
    public int getWtgaIdMoviChiu() {
        return readPackedAsInt(Pos.WTGA_ID_MOVI_CHIU, Len.Int.WTGA_ID_MOVI_CHIU);
    }

    public byte[] getWtgaIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ID_MOVI_CHIU, Pos.WTGA_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWtgaIdMoviChiuSpaces() {
        fill(Pos.WTGA_ID_MOVI_CHIU, Len.WTGA_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWtgaIdMoviChiuNull(String wtgaIdMoviChiuNull) {
        writeString(Pos.WTGA_ID_MOVI_CHIU_NULL, wtgaIdMoviChiuNull, Len.WTGA_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WTGA-ID-MOVI-CHIU-NULL<br>*/
    public String getWtgaIdMoviChiuNull() {
        return readString(Pos.WTGA_ID_MOVI_CHIU_NULL, Len.WTGA_ID_MOVI_CHIU_NULL);
    }

    public String getWtgaIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getWtgaIdMoviChiuNull(), Len.WTGA_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ID_MOVI_CHIU = 1;
        public static final int WTGA_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ID_MOVI_CHIU = 5;
        public static final int WTGA_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
