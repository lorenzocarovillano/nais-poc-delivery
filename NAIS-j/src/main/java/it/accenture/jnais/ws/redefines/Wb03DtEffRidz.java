package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-EFF-RIDZ<br>
 * Variable: WB03-DT-EFF-RIDZ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtEffRidz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtEffRidz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_EFF_RIDZ;
    }

    public void setWb03DtEffRidz(int wb03DtEffRidz) {
        writeIntAsPacked(Pos.WB03_DT_EFF_RIDZ, wb03DtEffRidz, Len.Int.WB03_DT_EFF_RIDZ);
    }

    public void setWb03DtEffRidzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_EFF_RIDZ, Pos.WB03_DT_EFF_RIDZ);
    }

    /**Original name: WB03-DT-EFF-RIDZ<br>*/
    public int getWb03DtEffRidz() {
        return readPackedAsInt(Pos.WB03_DT_EFF_RIDZ, Len.Int.WB03_DT_EFF_RIDZ);
    }

    public byte[] getWb03DtEffRidzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_EFF_RIDZ, Pos.WB03_DT_EFF_RIDZ);
        return buffer;
    }

    public void setWb03DtEffRidzNull(String wb03DtEffRidzNull) {
        writeString(Pos.WB03_DT_EFF_RIDZ_NULL, wb03DtEffRidzNull, Len.WB03_DT_EFF_RIDZ_NULL);
    }

    /**Original name: WB03-DT-EFF-RIDZ-NULL<br>*/
    public String getWb03DtEffRidzNull() {
        return readString(Pos.WB03_DT_EFF_RIDZ_NULL, Len.WB03_DT_EFF_RIDZ_NULL);
    }

    public String getWb03DtEffRidzNullFormatted() {
        return Functions.padBlanks(getWb03DtEffRidzNull(), Len.WB03_DT_EFF_RIDZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_EFF_RIDZ = 1;
        public static final int WB03_DT_EFF_RIDZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_EFF_RIDZ = 5;
        public static final int WB03_DT_EFF_RIDZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_EFF_RIDZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
