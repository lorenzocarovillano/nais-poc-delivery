package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0503-TIPO-SEGNO<br>
 * Variable: IDSV0503-TIPO-SEGNO from copybook IDSV0503<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0503TipoSegno {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SENZA_SEGNO = ' ';
    public static final char SEGNO_POSITIVO = '+';
    public static final char SEGNO_NEGATIVO = '-';

    //==== METHODS ====
    public void setIdsv0503TipoSegno(char idsv0503TipoSegno) {
        this.value = idsv0503TipoSegno;
    }

    public char getIdsv0503TipoSegno() {
        return this.value;
    }

    public void setSenzaSegno() {
        value = SENZA_SEGNO;
    }

    public void setSegnoPositivo() {
        value = SEGNO_POSITIVO;
    }

    public boolean isSegnoNegativo() {
        return value == SEGNO_NEGATIVO;
    }

    public void setSegnoNegativo() {
        value = SEGNO_NEGATIVO;
    }
}
