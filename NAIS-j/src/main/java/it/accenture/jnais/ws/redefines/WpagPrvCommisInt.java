package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRV-COMMIS-INT<br>
 * Variable: WPAG-PRV-COMMIS-INT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPrvCommisInt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPrvCommisInt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRV_COMMIS_INT;
    }

    public void setWpagPrvCommisInt(AfDecimal wpagPrvCommisInt) {
        writeDecimalAsPacked(Pos.WPAG_PRV_COMMIS_INT, wpagPrvCommisInt.copy());
    }

    public void setWpagPrvCommisIntFormatted(String wpagPrvCommisInt) {
        setWpagPrvCommisInt(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRV_COMMIS_INT + Len.Fract.WPAG_PRV_COMMIS_INT, Len.Fract.WPAG_PRV_COMMIS_INT, wpagPrvCommisInt));
    }

    public void setWpagPrvCommisIntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRV_COMMIS_INT, Pos.WPAG_PRV_COMMIS_INT);
    }

    /**Original name: WPAG-PRV-COMMIS-INT<br>*/
    public AfDecimal getWpagPrvCommisInt() {
        return readPackedAsDecimal(Pos.WPAG_PRV_COMMIS_INT, Len.Int.WPAG_PRV_COMMIS_INT, Len.Fract.WPAG_PRV_COMMIS_INT);
    }

    public byte[] getWpagPrvCommisIntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRV_COMMIS_INT, Pos.WPAG_PRV_COMMIS_INT);
        return buffer;
    }

    public void initWpagPrvCommisIntSpaces() {
        fill(Pos.WPAG_PRV_COMMIS_INT, Len.WPAG_PRV_COMMIS_INT, Types.SPACE_CHAR);
    }

    public void setWpagPrvCommisIntNull(String wpagPrvCommisIntNull) {
        writeString(Pos.WPAG_PRV_COMMIS_INT_NULL, wpagPrvCommisIntNull, Len.WPAG_PRV_COMMIS_INT_NULL);
    }

    /**Original name: WPAG-PRV-COMMIS-INT-NULL<br>*/
    public String getWpagPrvCommisIntNull() {
        return readString(Pos.WPAG_PRV_COMMIS_INT_NULL, Len.WPAG_PRV_COMMIS_INT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRV_COMMIS_INT = 1;
        public static final int WPAG_PRV_COMMIS_INT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRV_COMMIS_INT = 8;
        public static final int WPAG_PRV_COMMIS_INT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRV_COMMIS_INT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRV_COMMIS_INT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
