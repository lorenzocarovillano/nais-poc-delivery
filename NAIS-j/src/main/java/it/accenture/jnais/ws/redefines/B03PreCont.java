package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRE-CONT<br>
 * Variable: B03-PRE-CONT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PreCont extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PreCont() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRE_CONT;
    }

    public void setB03PreCont(AfDecimal b03PreCont) {
        writeDecimalAsPacked(Pos.B03_PRE_CONT, b03PreCont.copy());
    }

    public void setB03PreContFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRE_CONT, Pos.B03_PRE_CONT);
    }

    /**Original name: B03-PRE-CONT<br>*/
    public AfDecimal getB03PreCont() {
        return readPackedAsDecimal(Pos.B03_PRE_CONT, Len.Int.B03_PRE_CONT, Len.Fract.B03_PRE_CONT);
    }

    public byte[] getB03PreContAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRE_CONT, Pos.B03_PRE_CONT);
        return buffer;
    }

    public void setB03PreContNull(String b03PreContNull) {
        writeString(Pos.B03_PRE_CONT_NULL, b03PreContNull, Len.B03_PRE_CONT_NULL);
    }

    /**Original name: B03-PRE-CONT-NULL<br>*/
    public String getB03PreContNull() {
        return readString(Pos.B03_PRE_CONT_NULL, Len.B03_PRE_CONT_NULL);
    }

    public String getB03PreContNullFormatted() {
        return Functions.padBlanks(getB03PreContNull(), Len.B03_PRE_CONT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRE_CONT = 1;
        public static final int B03_PRE_CONT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRE_CONT = 8;
        public static final int B03_PRE_CONT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRE_CONT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRE_CONT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
