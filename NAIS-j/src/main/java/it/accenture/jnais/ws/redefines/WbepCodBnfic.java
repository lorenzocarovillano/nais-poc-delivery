package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WBEP-COD-BNFIC<br>
 * Variable: WBEP-COD-BNFIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbepCodBnfic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbepCodBnfic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEP_COD_BNFIC;
    }

    public void setWbepCodBnfic(short wbepCodBnfic) {
        writeShortAsPacked(Pos.WBEP_COD_BNFIC, wbepCodBnfic, Len.Int.WBEP_COD_BNFIC);
    }

    public void setWbepCodBnficFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEP_COD_BNFIC, Pos.WBEP_COD_BNFIC);
    }

    /**Original name: WBEP-COD-BNFIC<br>*/
    public short getWbepCodBnfic() {
        return readPackedAsShort(Pos.WBEP_COD_BNFIC, Len.Int.WBEP_COD_BNFIC);
    }

    public byte[] getWbepCodBnficAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEP_COD_BNFIC, Pos.WBEP_COD_BNFIC);
        return buffer;
    }

    public void initWbepCodBnficSpaces() {
        fill(Pos.WBEP_COD_BNFIC, Len.WBEP_COD_BNFIC, Types.SPACE_CHAR);
    }

    public void setWbepCodBnficNull(String wbepCodBnficNull) {
        writeString(Pos.WBEP_COD_BNFIC_NULL, wbepCodBnficNull, Len.WBEP_COD_BNFIC_NULL);
    }

    /**Original name: WBEP-COD-BNFIC-NULL<br>*/
    public String getWbepCodBnficNull() {
        return readString(Pos.WBEP_COD_BNFIC_NULL, Len.WBEP_COD_BNFIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEP_COD_BNFIC = 1;
        public static final int WBEP_COD_BNFIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEP_COD_BNFIC = 2;
        public static final int WBEP_COD_BNFIC_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEP_COD_BNFIC = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
