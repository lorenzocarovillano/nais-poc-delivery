package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-INTR-PREST-DFZ<br>
 * Variable: WDFL-INTR-PREST-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIntrPrestDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIntrPrestDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_INTR_PREST_DFZ;
    }

    public void setWdflIntrPrestDfz(AfDecimal wdflIntrPrestDfz) {
        writeDecimalAsPacked(Pos.WDFL_INTR_PREST_DFZ, wdflIntrPrestDfz.copy());
    }

    public void setWdflIntrPrestDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_INTR_PREST_DFZ, Pos.WDFL_INTR_PREST_DFZ);
    }

    /**Original name: WDFL-INTR-PREST-DFZ<br>*/
    public AfDecimal getWdflIntrPrestDfz() {
        return readPackedAsDecimal(Pos.WDFL_INTR_PREST_DFZ, Len.Int.WDFL_INTR_PREST_DFZ, Len.Fract.WDFL_INTR_PREST_DFZ);
    }

    public byte[] getWdflIntrPrestDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_INTR_PREST_DFZ, Pos.WDFL_INTR_PREST_DFZ);
        return buffer;
    }

    public void setWdflIntrPrestDfzNull(String wdflIntrPrestDfzNull) {
        writeString(Pos.WDFL_INTR_PREST_DFZ_NULL, wdflIntrPrestDfzNull, Len.WDFL_INTR_PREST_DFZ_NULL);
    }

    /**Original name: WDFL-INTR-PREST-DFZ-NULL<br>*/
    public String getWdflIntrPrestDfzNull() {
        return readString(Pos.WDFL_INTR_PREST_DFZ_NULL, Len.WDFL_INTR_PREST_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_INTR_PREST_DFZ = 1;
        public static final int WDFL_INTR_PREST_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_INTR_PREST_DFZ = 8;
        public static final int WDFL_INTR_PREST_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_INTR_PREST_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_INTR_PREST_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
