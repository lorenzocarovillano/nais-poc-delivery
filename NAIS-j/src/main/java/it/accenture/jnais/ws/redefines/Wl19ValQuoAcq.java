package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WL19-VAL-QUO-ACQ<br>
 * Variable: WL19-VAL-QUO-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl19ValQuoAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl19ValQuoAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL19_VAL_QUO_ACQ;
    }

    public void setDl19ValQuoAcq(AfDecimal dl19ValQuoAcq) {
        writeDecimalAsPacked(Pos.WL19_VAL_QUO_ACQ, dl19ValQuoAcq.copy());
    }

    public void setWl19ValQuoAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL19_VAL_QUO_ACQ, Pos.WL19_VAL_QUO_ACQ);
    }

    /**Original name: DL19-VAL-QUO-ACQ<br>*/
    public AfDecimal getDl19ValQuoAcq() {
        return readPackedAsDecimal(Pos.WL19_VAL_QUO_ACQ, Len.Int.DL19_VAL_QUO_ACQ, Len.Fract.DL19_VAL_QUO_ACQ);
    }

    public byte[] getWl19ValQuoAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL19_VAL_QUO_ACQ, Pos.WL19_VAL_QUO_ACQ);
        return buffer;
    }

    public void initWl19ValQuoAcqSpaces() {
        fill(Pos.WL19_VAL_QUO_ACQ, Len.WL19_VAL_QUO_ACQ, Types.SPACE_CHAR);
    }

    public void setWl19ValQuoAcqNull(String wl19ValQuoAcqNull) {
        writeString(Pos.WL19_VAL_QUO_ACQ_NULL, wl19ValQuoAcqNull, Len.WL19_VAL_QUO_ACQ_NULL);
    }

    /**Original name: WL19-VAL-QUO-ACQ-NULL<br>*/
    public String getWl19ValQuoAcqNull() {
        return readString(Pos.WL19_VAL_QUO_ACQ_NULL, Len.WL19_VAL_QUO_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL19_VAL_QUO_ACQ = 1;
        public static final int WL19_VAL_QUO_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL19_VAL_QUO_ACQ = 7;
        public static final int WL19_VAL_QUO_ACQ_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DL19_VAL_QUO_ACQ = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DL19_VAL_QUO_ACQ = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
