package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-DT-APPLZ-MORA<br>
 * Variable: TIT-DT-APPLZ-MORA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitDtApplzMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitDtApplzMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_DT_APPLZ_MORA;
    }

    public void setTitDtApplzMora(int titDtApplzMora) {
        writeIntAsPacked(Pos.TIT_DT_APPLZ_MORA, titDtApplzMora, Len.Int.TIT_DT_APPLZ_MORA);
    }

    public void setTitDtApplzMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_DT_APPLZ_MORA, Pos.TIT_DT_APPLZ_MORA);
    }

    /**Original name: TIT-DT-APPLZ-MORA<br>*/
    public int getTitDtApplzMora() {
        return readPackedAsInt(Pos.TIT_DT_APPLZ_MORA, Len.Int.TIT_DT_APPLZ_MORA);
    }

    public byte[] getTitDtApplzMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_DT_APPLZ_MORA, Pos.TIT_DT_APPLZ_MORA);
        return buffer;
    }

    public void setTitDtApplzMoraNull(String titDtApplzMoraNull) {
        writeString(Pos.TIT_DT_APPLZ_MORA_NULL, titDtApplzMoraNull, Len.TIT_DT_APPLZ_MORA_NULL);
    }

    /**Original name: TIT-DT-APPLZ-MORA-NULL<br>*/
    public String getTitDtApplzMoraNull() {
        return readString(Pos.TIT_DT_APPLZ_MORA_NULL, Len.TIT_DT_APPLZ_MORA_NULL);
    }

    public String getTitDtApplzMoraNullFormatted() {
        return Functions.padBlanks(getTitDtApplzMoraNull(), Len.TIT_DT_APPLZ_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_DT_APPLZ_MORA = 1;
        public static final int TIT_DT_APPLZ_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_DT_APPLZ_MORA = 5;
        public static final int TIT_DT_APPLZ_MORA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_DT_APPLZ_MORA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
