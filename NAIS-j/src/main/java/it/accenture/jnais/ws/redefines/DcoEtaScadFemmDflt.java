package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCO-ETA-SCAD-FEMM-DFLT<br>
 * Variable: DCO-ETA-SCAD-FEMM-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DcoEtaScadFemmDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DcoEtaScadFemmDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DCO_ETA_SCAD_FEMM_DFLT;
    }

    public void setDcoEtaScadFemmDflt(int dcoEtaScadFemmDflt) {
        writeIntAsPacked(Pos.DCO_ETA_SCAD_FEMM_DFLT, dcoEtaScadFemmDflt, Len.Int.DCO_ETA_SCAD_FEMM_DFLT);
    }

    public void setDcoEtaScadFemmDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DCO_ETA_SCAD_FEMM_DFLT, Pos.DCO_ETA_SCAD_FEMM_DFLT);
    }

    /**Original name: DCO-ETA-SCAD-FEMM-DFLT<br>*/
    public int getDcoEtaScadFemmDflt() {
        return readPackedAsInt(Pos.DCO_ETA_SCAD_FEMM_DFLT, Len.Int.DCO_ETA_SCAD_FEMM_DFLT);
    }

    public byte[] getDcoEtaScadFemmDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DCO_ETA_SCAD_FEMM_DFLT, Pos.DCO_ETA_SCAD_FEMM_DFLT);
        return buffer;
    }

    public void setDcoEtaScadFemmDfltNull(String dcoEtaScadFemmDfltNull) {
        writeString(Pos.DCO_ETA_SCAD_FEMM_DFLT_NULL, dcoEtaScadFemmDfltNull, Len.DCO_ETA_SCAD_FEMM_DFLT_NULL);
    }

    /**Original name: DCO-ETA-SCAD-FEMM-DFLT-NULL<br>*/
    public String getDcoEtaScadFemmDfltNull() {
        return readString(Pos.DCO_ETA_SCAD_FEMM_DFLT_NULL, Len.DCO_ETA_SCAD_FEMM_DFLT_NULL);
    }

    public String getDcoEtaScadFemmDfltNullFormatted() {
        return Functions.padBlanks(getDcoEtaScadFemmDfltNull(), Len.DCO_ETA_SCAD_FEMM_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DCO_ETA_SCAD_FEMM_DFLT = 1;
        public static final int DCO_ETA_SCAD_FEMM_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_ETA_SCAD_FEMM_DFLT = 3;
        public static final int DCO_ETA_SCAD_FEMM_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_ETA_SCAD_FEMM_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
