package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CAR-ACQ-PRECONTATO<br>
 * Variable: B03-CAR-ACQ-PRECONTATO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CarAcqPrecontato extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CarAcqPrecontato() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CAR_ACQ_PRECONTATO;
    }

    public void setB03CarAcqPrecontato(AfDecimal b03CarAcqPrecontato) {
        writeDecimalAsPacked(Pos.B03_CAR_ACQ_PRECONTATO, b03CarAcqPrecontato.copy());
    }

    public void setB03CarAcqPrecontatoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CAR_ACQ_PRECONTATO, Pos.B03_CAR_ACQ_PRECONTATO);
    }

    /**Original name: B03-CAR-ACQ-PRECONTATO<br>*/
    public AfDecimal getB03CarAcqPrecontato() {
        return readPackedAsDecimal(Pos.B03_CAR_ACQ_PRECONTATO, Len.Int.B03_CAR_ACQ_PRECONTATO, Len.Fract.B03_CAR_ACQ_PRECONTATO);
    }

    public byte[] getB03CarAcqPrecontatoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CAR_ACQ_PRECONTATO, Pos.B03_CAR_ACQ_PRECONTATO);
        return buffer;
    }

    public void setB03CarAcqPrecontatoNull(String b03CarAcqPrecontatoNull) {
        writeString(Pos.B03_CAR_ACQ_PRECONTATO_NULL, b03CarAcqPrecontatoNull, Len.B03_CAR_ACQ_PRECONTATO_NULL);
    }

    /**Original name: B03-CAR-ACQ-PRECONTATO-NULL<br>*/
    public String getB03CarAcqPrecontatoNull() {
        return readString(Pos.B03_CAR_ACQ_PRECONTATO_NULL, Len.B03_CAR_ACQ_PRECONTATO_NULL);
    }

    public String getB03CarAcqPrecontatoNullFormatted() {
        return Functions.padBlanks(getB03CarAcqPrecontatoNull(), Len.B03_CAR_ACQ_PRECONTATO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CAR_ACQ_PRECONTATO = 1;
        public static final int B03_CAR_ACQ_PRECONTATO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CAR_ACQ_PRECONTATO = 8;
        public static final int B03_CAR_ACQ_PRECONTATO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CAR_ACQ_PRECONTATO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CAR_ACQ_PRECONTATO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
