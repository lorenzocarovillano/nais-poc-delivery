package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-SOPR-PROF<br>
 * Variable: WTIT-TOT-SOPR-PROF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_SOPR_PROF;
    }

    public void setWtitTotSoprProf(AfDecimal wtitTotSoprProf) {
        writeDecimalAsPacked(Pos.WTIT_TOT_SOPR_PROF, wtitTotSoprProf.copy());
    }

    public void setWtitTotSoprProfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_SOPR_PROF, Pos.WTIT_TOT_SOPR_PROF);
    }

    /**Original name: WTIT-TOT-SOPR-PROF<br>*/
    public AfDecimal getWtitTotSoprProf() {
        return readPackedAsDecimal(Pos.WTIT_TOT_SOPR_PROF, Len.Int.WTIT_TOT_SOPR_PROF, Len.Fract.WTIT_TOT_SOPR_PROF);
    }

    public byte[] getWtitTotSoprProfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_SOPR_PROF, Pos.WTIT_TOT_SOPR_PROF);
        return buffer;
    }

    public void initWtitTotSoprProfSpaces() {
        fill(Pos.WTIT_TOT_SOPR_PROF, Len.WTIT_TOT_SOPR_PROF, Types.SPACE_CHAR);
    }

    public void setWtitTotSoprProfNull(String wtitTotSoprProfNull) {
        writeString(Pos.WTIT_TOT_SOPR_PROF_NULL, wtitTotSoprProfNull, Len.WTIT_TOT_SOPR_PROF_NULL);
    }

    /**Original name: WTIT-TOT-SOPR-PROF-NULL<br>*/
    public String getWtitTotSoprProfNull() {
        return readString(Pos.WTIT_TOT_SOPR_PROF_NULL, Len.WTIT_TOT_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SOPR_PROF = 1;
        public static final int WTIT_TOT_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SOPR_PROF = 8;
        public static final int WTIT_TOT_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
