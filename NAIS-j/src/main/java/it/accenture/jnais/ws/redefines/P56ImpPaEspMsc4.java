package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-IMP-PA-ESP-MSC-4<br>
 * Variable: P56-IMP-PA-ESP-MSC-4 from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56ImpPaEspMsc4 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56ImpPaEspMsc4() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_IMP_PA_ESP_MSC4;
    }

    public void setP56ImpPaEspMsc4(AfDecimal p56ImpPaEspMsc4) {
        writeDecimalAsPacked(Pos.P56_IMP_PA_ESP_MSC4, p56ImpPaEspMsc4.copy());
    }

    public void setP56ImpPaEspMsc4FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_IMP_PA_ESP_MSC4, Pos.P56_IMP_PA_ESP_MSC4);
    }

    /**Original name: P56-IMP-PA-ESP-MSC-4<br>*/
    public AfDecimal getP56ImpPaEspMsc4() {
        return readPackedAsDecimal(Pos.P56_IMP_PA_ESP_MSC4, Len.Int.P56_IMP_PA_ESP_MSC4, Len.Fract.P56_IMP_PA_ESP_MSC4);
    }

    public byte[] getP56ImpPaEspMsc4AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_IMP_PA_ESP_MSC4, Pos.P56_IMP_PA_ESP_MSC4);
        return buffer;
    }

    public void setP56ImpPaEspMsc4Null(String p56ImpPaEspMsc4Null) {
        writeString(Pos.P56_IMP_PA_ESP_MSC4_NULL, p56ImpPaEspMsc4Null, Len.P56_IMP_PA_ESP_MSC4_NULL);
    }

    /**Original name: P56-IMP-PA-ESP-MSC-4-NULL<br>*/
    public String getP56ImpPaEspMsc4Null() {
        return readString(Pos.P56_IMP_PA_ESP_MSC4_NULL, Len.P56_IMP_PA_ESP_MSC4_NULL);
    }

    public String getP56ImpPaEspMsc4NullFormatted() {
        return Functions.padBlanks(getP56ImpPaEspMsc4Null(), Len.P56_IMP_PA_ESP_MSC4_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_IMP_PA_ESP_MSC4 = 1;
        public static final int P56_IMP_PA_ESP_MSC4_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_IMP_PA_ESP_MSC4 = 8;
        public static final int P56_IMP_PA_ESP_MSC4_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_IMP_PA_ESP_MSC4 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_IMP_PA_ESP_MSC4 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
