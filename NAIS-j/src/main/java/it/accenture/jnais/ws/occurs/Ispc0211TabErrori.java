package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ISPC0211-TAB-ERRORI<br>
 * Variables: ISPC0211-TAB-ERRORI from copybook ISPC0211<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0211TabErrori {

    //==== PROPERTIES ====
    //Original name: ISPC0211-COD-ERR
    private String codErr = DefaultValues.stringVal(Len.COD_ERR);
    //Original name: ISPC0211-DESCRIZIONE-ERR
    private String descrizioneErr = DefaultValues.stringVal(Len.DESCRIZIONE_ERR);
    //Original name: ISPC0211-GRAVITA-ERR
    private String gravitaErr = DefaultValues.stringVal(Len.GRAVITA_ERR);
    //Original name: ISPC0211-LIVELLO-DEROGA
    private String livelloDeroga = DefaultValues.stringVal(Len.LIVELLO_DEROGA);

    //==== METHODS ====
    public void setIspc0211TabErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        codErr = MarshalByte.readString(buffer, position, Len.COD_ERR);
        position += Len.COD_ERR;
        descrizioneErr = MarshalByte.readString(buffer, position, Len.DESCRIZIONE_ERR);
        position += Len.DESCRIZIONE_ERR;
        gravitaErr = MarshalByte.readFixedString(buffer, position, Len.GRAVITA_ERR);
        position += Len.GRAVITA_ERR;
        livelloDeroga = MarshalByte.readFixedString(buffer, position, Len.LIVELLO_DEROGA);
    }

    public byte[] getIspc0211TabErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codErr, Len.COD_ERR);
        position += Len.COD_ERR;
        MarshalByte.writeString(buffer, position, descrizioneErr, Len.DESCRIZIONE_ERR);
        position += Len.DESCRIZIONE_ERR;
        MarshalByte.writeString(buffer, position, gravitaErr, Len.GRAVITA_ERR);
        position += Len.GRAVITA_ERR;
        MarshalByte.writeString(buffer, position, livelloDeroga, Len.LIVELLO_DEROGA);
        return buffer;
    }

    public void initIspc0211TabErroriSpaces() {
        codErr = "";
        descrizioneErr = "";
        gravitaErr = "";
        livelloDeroga = "";
    }

    public void setCodErr(String codErr) {
        this.codErr = Functions.subString(codErr, Len.COD_ERR);
    }

    public String getCodErr() {
        return this.codErr;
    }

    public void setDescrizioneErr(String descrizioneErr) {
        this.descrizioneErr = Functions.subString(descrizioneErr, Len.DESCRIZIONE_ERR);
    }

    public String getDescrizioneErr() {
        return this.descrizioneErr;
    }

    public void setIspc0211GravitaErrFormatted(String ispc0211GravitaErr) {
        this.gravitaErr = Trunc.toUnsignedNumeric(ispc0211GravitaErr, Len.GRAVITA_ERR);
    }

    public short getIspc0211GravitaErr() {
        return NumericDisplay.asShort(this.gravitaErr);
    }

    public void setIspc0211LivelloDerogaFormatted(String ispc0211LivelloDeroga) {
        this.livelloDeroga = Trunc.toUnsignedNumeric(ispc0211LivelloDeroga, Len.LIVELLO_DEROGA);
    }

    public short getIspc0211LivelloDeroga() {
        return NumericDisplay.asShort(this.livelloDeroga);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_ERR = 12;
        public static final int DESCRIZIONE_ERR = 50;
        public static final int GRAVITA_ERR = 2;
        public static final int LIVELLO_DEROGA = 2;
        public static final int ISPC0211_TAB_ERRORI = COD_ERR + DESCRIZIONE_ERR + GRAVITA_ERR + LIVELLO_DEROGA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
