package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-INTR-PREST<br>
 * Variable: TIT-TOT-INTR-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_INTR_PREST;
    }

    public void setTitTotIntrPrest(AfDecimal titTotIntrPrest) {
        writeDecimalAsPacked(Pos.TIT_TOT_INTR_PREST, titTotIntrPrest.copy());
    }

    public void setTitTotIntrPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_INTR_PREST, Pos.TIT_TOT_INTR_PREST);
    }

    /**Original name: TIT-TOT-INTR-PREST<br>*/
    public AfDecimal getTitTotIntrPrest() {
        return readPackedAsDecimal(Pos.TIT_TOT_INTR_PREST, Len.Int.TIT_TOT_INTR_PREST, Len.Fract.TIT_TOT_INTR_PREST);
    }

    public byte[] getTitTotIntrPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_INTR_PREST, Pos.TIT_TOT_INTR_PREST);
        return buffer;
    }

    public void setTitTotIntrPrestNull(String titTotIntrPrestNull) {
        writeString(Pos.TIT_TOT_INTR_PREST_NULL, titTotIntrPrestNull, Len.TIT_TOT_INTR_PREST_NULL);
    }

    /**Original name: TIT-TOT-INTR-PREST-NULL<br>*/
    public String getTitTotIntrPrestNull() {
        return readString(Pos.TIT_TOT_INTR_PREST_NULL, Len.TIT_TOT_INTR_PREST_NULL);
    }

    public String getTitTotIntrPrestNullFormatted() {
        return Functions.padBlanks(getTitTotIntrPrestNull(), Len.TIT_TOT_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_INTR_PREST = 1;
        public static final int TIT_TOT_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_INTR_PREST = 8;
        public static final int TIT_TOT_INTR_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_INTR_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
