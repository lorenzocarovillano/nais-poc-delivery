package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-MINUS-VALENZA<br>
 * Variable: VAS-MINUS-VALENZA from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasMinusValenza extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasMinusValenza() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_MINUS_VALENZA;
    }

    public void setVasMinusValenza(AfDecimal vasMinusValenza) {
        writeDecimalAsPacked(Pos.VAS_MINUS_VALENZA, vasMinusValenza.copy());
    }

    public void setVasMinusValenzaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_MINUS_VALENZA, Pos.VAS_MINUS_VALENZA);
    }

    /**Original name: VAS-MINUS-VALENZA<br>*/
    public AfDecimal getVasMinusValenza() {
        return readPackedAsDecimal(Pos.VAS_MINUS_VALENZA, Len.Int.VAS_MINUS_VALENZA, Len.Fract.VAS_MINUS_VALENZA);
    }

    public byte[] getVasMinusValenzaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_MINUS_VALENZA, Pos.VAS_MINUS_VALENZA);
        return buffer;
    }

    public void setVasMinusValenzaNull(String vasMinusValenzaNull) {
        writeString(Pos.VAS_MINUS_VALENZA_NULL, vasMinusValenzaNull, Len.VAS_MINUS_VALENZA_NULL);
    }

    /**Original name: VAS-MINUS-VALENZA-NULL<br>*/
    public String getVasMinusValenzaNull() {
        return readString(Pos.VAS_MINUS_VALENZA_NULL, Len.VAS_MINUS_VALENZA_NULL);
    }

    public String getVasMinusValenzaNullFormatted() {
        return Functions.padBlanks(getVasMinusValenzaNull(), Len.VAS_MINUS_VALENZA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_MINUS_VALENZA = 1;
        public static final int VAS_MINUS_VALENZA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_MINUS_VALENZA = 8;
        public static final int VAS_MINUS_VALENZA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAS_MINUS_VALENZA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_MINUS_VALENZA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
