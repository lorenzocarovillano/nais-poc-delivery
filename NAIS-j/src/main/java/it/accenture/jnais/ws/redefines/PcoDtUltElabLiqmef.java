package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-LIQMEF<br>
 * Variable: PCO-DT-ULT-ELAB-LIQMEF from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabLiqmef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabLiqmef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_LIQMEF;
    }

    public void setPcoDtUltElabLiqmef(int pcoDtUltElabLiqmef) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_LIQMEF, pcoDtUltElabLiqmef, Len.Int.PCO_DT_ULT_ELAB_LIQMEF);
    }

    public void setPcoDtUltElabLiqmefFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_LIQMEF, Pos.PCO_DT_ULT_ELAB_LIQMEF);
    }

    /**Original name: PCO-DT-ULT-ELAB-LIQMEF<br>*/
    public int getPcoDtUltElabLiqmef() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_LIQMEF, Len.Int.PCO_DT_ULT_ELAB_LIQMEF);
    }

    public byte[] getPcoDtUltElabLiqmefAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_LIQMEF, Pos.PCO_DT_ULT_ELAB_LIQMEF);
        return buffer;
    }

    public void initPcoDtUltElabLiqmefHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_LIQMEF, Len.PCO_DT_ULT_ELAB_LIQMEF, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabLiqmefNull(String pcoDtUltElabLiqmefNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_LIQMEF_NULL, pcoDtUltElabLiqmefNull, Len.PCO_DT_ULT_ELAB_LIQMEF_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-LIQMEF-NULL<br>*/
    public String getPcoDtUltElabLiqmefNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_LIQMEF_NULL, Len.PCO_DT_ULT_ELAB_LIQMEF_NULL);
    }

    public String getPcoDtUltElabLiqmefNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabLiqmefNull(), Len.PCO_DT_ULT_ELAB_LIQMEF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_LIQMEF = 1;
        public static final int PCO_DT_ULT_ELAB_LIQMEF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_LIQMEF = 5;
        public static final int PCO_DT_ULT_ELAB_LIQMEF_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_LIQMEF = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
