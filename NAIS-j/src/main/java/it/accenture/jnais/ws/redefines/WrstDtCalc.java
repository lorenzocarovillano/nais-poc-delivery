package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRST-DT-CALC<br>
 * Variable: WRST-DT-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstDtCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstDtCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_DT_CALC;
    }

    public void setWrstDtCalc(int wrstDtCalc) {
        writeIntAsPacked(Pos.WRST_DT_CALC, wrstDtCalc, Len.Int.WRST_DT_CALC);
    }

    public void setWrstDtCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_DT_CALC, Pos.WRST_DT_CALC);
    }

    /**Original name: WRST-DT-CALC<br>*/
    public int getWrstDtCalc() {
        return readPackedAsInt(Pos.WRST_DT_CALC, Len.Int.WRST_DT_CALC);
    }

    public byte[] getWrstDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_DT_CALC, Pos.WRST_DT_CALC);
        return buffer;
    }

    public void initWrstDtCalcSpaces() {
        fill(Pos.WRST_DT_CALC, Len.WRST_DT_CALC, Types.SPACE_CHAR);
    }

    public void setWrstDtCalcNull(String wrstDtCalcNull) {
        writeString(Pos.WRST_DT_CALC_NULL, wrstDtCalcNull, Len.WRST_DT_CALC_NULL);
    }

    /**Original name: WRST-DT-CALC-NULL<br>*/
    public String getWrstDtCalcNull() {
        return readString(Pos.WRST_DT_CALC_NULL, Len.WRST_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_DT_CALC = 1;
        public static final int WRST_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_DT_CALC = 5;
        public static final int WRST_DT_CALC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_DT_CALC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
