package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-ULT-COEFF-AGG-RIS<br>
 * Variable: WRST-ULT-COEFF-AGG-RIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstUltCoeffAggRis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstUltCoeffAggRis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_ULT_COEFF_AGG_RIS;
    }

    public void setWrstUltCoeffAggRis(AfDecimal wrstUltCoeffAggRis) {
        writeDecimalAsPacked(Pos.WRST_ULT_COEFF_AGG_RIS, wrstUltCoeffAggRis.copy());
    }

    public void setWrstUltCoeffAggRisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_ULT_COEFF_AGG_RIS, Pos.WRST_ULT_COEFF_AGG_RIS);
    }

    /**Original name: WRST-ULT-COEFF-AGG-RIS<br>*/
    public AfDecimal getWrstUltCoeffAggRis() {
        return readPackedAsDecimal(Pos.WRST_ULT_COEFF_AGG_RIS, Len.Int.WRST_ULT_COEFF_AGG_RIS, Len.Fract.WRST_ULT_COEFF_AGG_RIS);
    }

    public byte[] getWrstUltCoeffAggRisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_ULT_COEFF_AGG_RIS, Pos.WRST_ULT_COEFF_AGG_RIS);
        return buffer;
    }

    public void initWrstUltCoeffAggRisSpaces() {
        fill(Pos.WRST_ULT_COEFF_AGG_RIS, Len.WRST_ULT_COEFF_AGG_RIS, Types.SPACE_CHAR);
    }

    public void setWrstUltCoeffAggRisNull(String wrstUltCoeffAggRisNull) {
        writeString(Pos.WRST_ULT_COEFF_AGG_RIS_NULL, wrstUltCoeffAggRisNull, Len.WRST_ULT_COEFF_AGG_RIS_NULL);
    }

    /**Original name: WRST-ULT-COEFF-AGG-RIS-NULL<br>*/
    public String getWrstUltCoeffAggRisNull() {
        return readString(Pos.WRST_ULT_COEFF_AGG_RIS_NULL, Len.WRST_ULT_COEFF_AGG_RIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_ULT_COEFF_AGG_RIS = 1;
        public static final int WRST_ULT_COEFF_AGG_RIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_ULT_COEFF_AGG_RIS = 8;
        public static final int WRST_ULT_COEFF_AGG_RIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_ULT_COEFF_AGG_RIS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_ULT_COEFF_AGG_RIS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
