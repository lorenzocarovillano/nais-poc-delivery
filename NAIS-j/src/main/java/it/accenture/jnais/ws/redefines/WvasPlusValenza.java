package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WVAS-PLUS-VALENZA<br>
 * Variable: WVAS-PLUS-VALENZA from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasPlusValenza extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasPlusValenza() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_PLUS_VALENZA;
    }

    public void setWvasPlusValenza(AfDecimal wvasPlusValenza) {
        writeDecimalAsPacked(Pos.WVAS_PLUS_VALENZA, wvasPlusValenza.copy());
    }

    /**Original name: WVAS-PLUS-VALENZA<br>*/
    public AfDecimal getWvasPlusValenza() {
        return readPackedAsDecimal(Pos.WVAS_PLUS_VALENZA, Len.Int.WVAS_PLUS_VALENZA, Len.Fract.WVAS_PLUS_VALENZA);
    }

    public void setWvasPlusValenzaNull(String wvasPlusValenzaNull) {
        writeString(Pos.WVAS_PLUS_VALENZA_NULL, wvasPlusValenzaNull, Len.WVAS_PLUS_VALENZA_NULL);
    }

    /**Original name: WVAS-PLUS-VALENZA-NULL<br>*/
    public String getWvasPlusValenzaNull() {
        return readString(Pos.WVAS_PLUS_VALENZA_NULL, Len.WVAS_PLUS_VALENZA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_PLUS_VALENZA = 1;
        public static final int WVAS_PLUS_VALENZA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_PLUS_VALENZA = 8;
        public static final int WVAS_PLUS_VALENZA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WVAS_PLUS_VALENZA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_PLUS_VALENZA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
