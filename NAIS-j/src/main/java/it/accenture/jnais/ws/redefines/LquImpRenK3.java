package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-REN-K3<br>
 * Variable: LQU-IMP-REN-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpRenK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpRenK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_REN_K3;
    }

    public void setLquImpRenK3(AfDecimal lquImpRenK3) {
        writeDecimalAsPacked(Pos.LQU_IMP_REN_K3, lquImpRenK3.copy());
    }

    public void setLquImpRenK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_REN_K3, Pos.LQU_IMP_REN_K3);
    }

    /**Original name: LQU-IMP-REN-K3<br>*/
    public AfDecimal getLquImpRenK3() {
        return readPackedAsDecimal(Pos.LQU_IMP_REN_K3, Len.Int.LQU_IMP_REN_K3, Len.Fract.LQU_IMP_REN_K3);
    }

    public byte[] getLquImpRenK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_REN_K3, Pos.LQU_IMP_REN_K3);
        return buffer;
    }

    public void setLquImpRenK3Null(String lquImpRenK3Null) {
        writeString(Pos.LQU_IMP_REN_K3_NULL, lquImpRenK3Null, Len.LQU_IMP_REN_K3_NULL);
    }

    /**Original name: LQU-IMP-REN-K3-NULL<br>*/
    public String getLquImpRenK3Null() {
        return readString(Pos.LQU_IMP_REN_K3_NULL, Len.LQU_IMP_REN_K3_NULL);
    }

    public String getLquImpRenK3NullFormatted() {
        return Functions.padBlanks(getLquImpRenK3Null(), Len.LQU_IMP_REN_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_REN_K3 = 1;
        public static final int LQU_IMP_REN_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_REN_K3 = 8;
        public static final int LQU_IMP_REN_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_REN_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_REN_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
