package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-FG-ANN-VAG<br>
 * Variable: WS-FG-ANN-VAG from program LCCS0023<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFgAnnVag {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWsFgAnnVag(char wsFgAnnVag) {
        this.value = wsFgAnnVag;
    }

    public char getWsFgAnnVag() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
