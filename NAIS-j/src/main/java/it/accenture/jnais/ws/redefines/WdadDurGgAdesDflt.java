package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDAD-DUR-GG-ADES-DFLT<br>
 * Variable: WDAD-DUR-GG-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadDurGgAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadDurGgAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_DUR_GG_ADES_DFLT;
    }

    public void setWdadDurGgAdesDflt(int wdadDurGgAdesDflt) {
        writeIntAsPacked(Pos.WDAD_DUR_GG_ADES_DFLT, wdadDurGgAdesDflt, Len.Int.WDAD_DUR_GG_ADES_DFLT);
    }

    public void setWdadDurGgAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_DUR_GG_ADES_DFLT, Pos.WDAD_DUR_GG_ADES_DFLT);
    }

    /**Original name: WDAD-DUR-GG-ADES-DFLT<br>*/
    public int getWdadDurGgAdesDflt() {
        return readPackedAsInt(Pos.WDAD_DUR_GG_ADES_DFLT, Len.Int.WDAD_DUR_GG_ADES_DFLT);
    }

    public byte[] getWdadDurGgAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_DUR_GG_ADES_DFLT, Pos.WDAD_DUR_GG_ADES_DFLT);
        return buffer;
    }

    public void setWdadDurGgAdesDfltNull(String wdadDurGgAdesDfltNull) {
        writeString(Pos.WDAD_DUR_GG_ADES_DFLT_NULL, wdadDurGgAdesDfltNull, Len.WDAD_DUR_GG_ADES_DFLT_NULL);
    }

    /**Original name: WDAD-DUR-GG-ADES-DFLT-NULL<br>*/
    public String getWdadDurGgAdesDfltNull() {
        return readString(Pos.WDAD_DUR_GG_ADES_DFLT_NULL, Len.WDAD_DUR_GG_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_DUR_GG_ADES_DFLT = 1;
        public static final int WDAD_DUR_GG_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_DUR_GG_ADES_DFLT = 3;
        public static final int WDAD_DUR_GG_ADES_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_DUR_GG_ADES_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
