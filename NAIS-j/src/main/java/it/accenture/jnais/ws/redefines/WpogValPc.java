package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPOG-VAL-PC<br>
 * Variable: WPOG-VAL-PC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpogValPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpogValPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOG_VAL_PC;
    }

    public void setWpogValPc(AfDecimal wpogValPc) {
        writeDecimalAsPacked(Pos.WPOG_VAL_PC, wpogValPc.copy());
    }

    public void setWpogValPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOG_VAL_PC, Pos.WPOG_VAL_PC);
    }

    /**Original name: WPOG-VAL-PC<br>*/
    public AfDecimal getWpogValPc() {
        return readPackedAsDecimal(Pos.WPOG_VAL_PC, Len.Int.WPOG_VAL_PC, Len.Fract.WPOG_VAL_PC);
    }

    public byte[] getWpogValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOG_VAL_PC, Pos.WPOG_VAL_PC);
        return buffer;
    }

    public void initWpogValPcSpaces() {
        fill(Pos.WPOG_VAL_PC, Len.WPOG_VAL_PC, Types.SPACE_CHAR);
    }

    public void setWpogValPcNull(String wpogValPcNull) {
        writeString(Pos.WPOG_VAL_PC_NULL, wpogValPcNull, Len.WPOG_VAL_PC_NULL);
    }

    /**Original name: WPOG-VAL-PC-NULL<br>*/
    public String getWpogValPcNull() {
        return readString(Pos.WPOG_VAL_PC_NULL, Len.WPOG_VAL_PC_NULL);
    }

    public String getDpogValPcNullFormatted() {
        return Functions.padBlanks(getWpogValPcNull(), Len.WPOG_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOG_VAL_PC = 1;
        public static final int WPOG_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOG_VAL_PC = 8;
        public static final int WPOG_VAL_PC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPOG_VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOG_VAL_PC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
