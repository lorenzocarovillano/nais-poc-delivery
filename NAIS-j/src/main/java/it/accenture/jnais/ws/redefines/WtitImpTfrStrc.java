package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-IMP-TFR-STRC<br>
 * Variable: WTIT-IMP-TFR-STRC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitImpTfrStrc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitImpTfrStrc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_IMP_TFR_STRC;
    }

    public void setWtitImpTfrStrc(AfDecimal wtitImpTfrStrc) {
        writeDecimalAsPacked(Pos.WTIT_IMP_TFR_STRC, wtitImpTfrStrc.copy());
    }

    public void setWtitImpTfrStrcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_IMP_TFR_STRC, Pos.WTIT_IMP_TFR_STRC);
    }

    /**Original name: WTIT-IMP-TFR-STRC<br>*/
    public AfDecimal getWtitImpTfrStrc() {
        return readPackedAsDecimal(Pos.WTIT_IMP_TFR_STRC, Len.Int.WTIT_IMP_TFR_STRC, Len.Fract.WTIT_IMP_TFR_STRC);
    }

    public byte[] getWtitImpTfrStrcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_IMP_TFR_STRC, Pos.WTIT_IMP_TFR_STRC);
        return buffer;
    }

    public void initWtitImpTfrStrcSpaces() {
        fill(Pos.WTIT_IMP_TFR_STRC, Len.WTIT_IMP_TFR_STRC, Types.SPACE_CHAR);
    }

    public void setWtitImpTfrStrcNull(String wtitImpTfrStrcNull) {
        writeString(Pos.WTIT_IMP_TFR_STRC_NULL, wtitImpTfrStrcNull, Len.WTIT_IMP_TFR_STRC_NULL);
    }

    /**Original name: WTIT-IMP-TFR-STRC-NULL<br>*/
    public String getWtitImpTfrStrcNull() {
        return readString(Pos.WTIT_IMP_TFR_STRC_NULL, Len.WTIT_IMP_TFR_STRC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_TFR_STRC = 1;
        public static final int WTIT_IMP_TFR_STRC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_TFR_STRC = 8;
        public static final int WTIT_IMP_TFR_STRC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_TFR_STRC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_TFR_STRC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
