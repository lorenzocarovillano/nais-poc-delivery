package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-PROV-ACQ-2AA<br>
 * Variable: TIT-TOT-PROV-ACQ-2AA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotProvAcq2aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotProvAcq2aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_PROV_ACQ2AA;
    }

    public void setTitTotProvAcq2aa(AfDecimal titTotProvAcq2aa) {
        writeDecimalAsPacked(Pos.TIT_TOT_PROV_ACQ2AA, titTotProvAcq2aa.copy());
    }

    public void setTitTotProvAcq2aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_PROV_ACQ2AA, Pos.TIT_TOT_PROV_ACQ2AA);
    }

    /**Original name: TIT-TOT-PROV-ACQ-2AA<br>*/
    public AfDecimal getTitTotProvAcq2aa() {
        return readPackedAsDecimal(Pos.TIT_TOT_PROV_ACQ2AA, Len.Int.TIT_TOT_PROV_ACQ2AA, Len.Fract.TIT_TOT_PROV_ACQ2AA);
    }

    public byte[] getTitTotProvAcq2aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_PROV_ACQ2AA, Pos.TIT_TOT_PROV_ACQ2AA);
        return buffer;
    }

    public void setTitTotProvAcq2aaNull(String titTotProvAcq2aaNull) {
        writeString(Pos.TIT_TOT_PROV_ACQ2AA_NULL, titTotProvAcq2aaNull, Len.TIT_TOT_PROV_ACQ2AA_NULL);
    }

    /**Original name: TIT-TOT-PROV-ACQ-2AA-NULL<br>*/
    public String getTitTotProvAcq2aaNull() {
        return readString(Pos.TIT_TOT_PROV_ACQ2AA_NULL, Len.TIT_TOT_PROV_ACQ2AA_NULL);
    }

    public String getTitTotProvAcq2aaNullFormatted() {
        return Functions.padBlanks(getTitTotProvAcq2aaNull(), Len.TIT_TOT_PROV_ACQ2AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PROV_ACQ2AA = 1;
        public static final int TIT_TOT_PROV_ACQ2AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PROV_ACQ2AA = 8;
        public static final int TIT_TOT_PROV_ACQ2AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PROV_ACQ2AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PROV_ACQ2AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
