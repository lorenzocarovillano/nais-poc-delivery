package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-DT-VLT<br>
 * Variable: TCL-DT-VLT from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclDtVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclDtVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_DT_VLT;
    }

    public void setTclDtVlt(int tclDtVlt) {
        writeIntAsPacked(Pos.TCL_DT_VLT, tclDtVlt, Len.Int.TCL_DT_VLT);
    }

    public void setTclDtVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_DT_VLT, Pos.TCL_DT_VLT);
    }

    /**Original name: TCL-DT-VLT<br>*/
    public int getTclDtVlt() {
        return readPackedAsInt(Pos.TCL_DT_VLT, Len.Int.TCL_DT_VLT);
    }

    public byte[] getTclDtVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_DT_VLT, Pos.TCL_DT_VLT);
        return buffer;
    }

    public void setTclDtVltNull(String tclDtVltNull) {
        writeString(Pos.TCL_DT_VLT_NULL, tclDtVltNull, Len.TCL_DT_VLT_NULL);
    }

    /**Original name: TCL-DT-VLT-NULL<br>*/
    public String getTclDtVltNull() {
        return readString(Pos.TCL_DT_VLT_NULL, Len.TCL_DT_VLT_NULL);
    }

    public String getTclDtVltNullFormatted() {
        return Functions.padBlanks(getTclDtVltNull(), Len.TCL_DT_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_DT_VLT = 1;
        public static final int TCL_DT_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_DT_VLT = 5;
        public static final int TCL_DT_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_DT_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
