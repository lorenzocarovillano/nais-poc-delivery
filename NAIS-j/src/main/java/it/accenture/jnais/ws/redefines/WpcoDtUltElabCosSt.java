package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-COS-ST<br>
 * Variable: WPCO-DT-ULT-ELAB-COS-ST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabCosSt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabCosSt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_COS_ST;
    }

    public void setWpcoDtUltElabCosSt(int wpcoDtUltElabCosSt) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_COS_ST, wpcoDtUltElabCosSt, Len.Int.WPCO_DT_ULT_ELAB_COS_ST);
    }

    public void setDpcoDtUltElabCosStFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_COS_ST, Pos.WPCO_DT_ULT_ELAB_COS_ST);
    }

    /**Original name: WPCO-DT-ULT-ELAB-COS-ST<br>*/
    public int getWpcoDtUltElabCosSt() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_COS_ST, Len.Int.WPCO_DT_ULT_ELAB_COS_ST);
    }

    public byte[] getWpcoDtUltElabCosStAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_COS_ST, Pos.WPCO_DT_ULT_ELAB_COS_ST);
        return buffer;
    }

    public void setWpcoDtUltElabCosStNull(String wpcoDtUltElabCosStNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_COS_ST_NULL, wpcoDtUltElabCosStNull, Len.WPCO_DT_ULT_ELAB_COS_ST_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-COS-ST-NULL<br>*/
    public String getWpcoDtUltElabCosStNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_COS_ST_NULL, Len.WPCO_DT_ULT_ELAB_COS_ST_NULL);
    }

    public String getWpcoDtUltElabCosStNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabCosStNull(), Len.WPCO_DT_ULT_ELAB_COS_ST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_COS_ST = 1;
        public static final int WPCO_DT_ULT_ELAB_COS_ST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_COS_ST = 5;
        public static final int WPCO_DT_ULT_ELAB_COS_ST_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_COS_ST = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
