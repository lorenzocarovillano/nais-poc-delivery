package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-TASSE<br>
 * Variable: WPAG-RATA-TASSE from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataTasse extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataTasse() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_TASSE;
    }

    public void setWpagRataTasse(AfDecimal wpagRataTasse) {
        writeDecimalAsPacked(Pos.WPAG_RATA_TASSE, wpagRataTasse.copy());
    }

    public void setWpagRataTasseFormatted(String wpagRataTasse) {
        setWpagRataTasse(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_TASSE + Len.Fract.WPAG_RATA_TASSE, Len.Fract.WPAG_RATA_TASSE, wpagRataTasse));
    }

    public void setWpagRataTasseFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_TASSE, Pos.WPAG_RATA_TASSE);
    }

    /**Original name: WPAG-RATA-TASSE<br>*/
    public AfDecimal getWpagRataTasse() {
        return readPackedAsDecimal(Pos.WPAG_RATA_TASSE, Len.Int.WPAG_RATA_TASSE, Len.Fract.WPAG_RATA_TASSE);
    }

    public byte[] getWpagRataTasseAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_TASSE, Pos.WPAG_RATA_TASSE);
        return buffer;
    }

    public void initWpagRataTasseSpaces() {
        fill(Pos.WPAG_RATA_TASSE, Len.WPAG_RATA_TASSE, Types.SPACE_CHAR);
    }

    public void setWpagRataTasseNull(String wpagRataTasseNull) {
        writeString(Pos.WPAG_RATA_TASSE_NULL, wpagRataTasseNull, Len.WPAG_RATA_TASSE_NULL);
    }

    /**Original name: WPAG-RATA-TASSE-NULL<br>*/
    public String getWpagRataTasseNull() {
        return readString(Pos.WPAG_RATA_TASSE_NULL, Len.WPAG_RATA_TASSE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_TASSE = 1;
        public static final int WPAG_RATA_TASSE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_TASSE = 8;
        public static final int WPAG_RATA_TASSE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_TASSE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_TASSE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
