package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-BOLLO-TOT-VD<br>
 * Variable: WDFL-IMPST-BOLLO-TOT-VD from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstBolloTotVd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstBolloTotVd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_BOLLO_TOT_VD;
    }

    public void setWdflImpstBolloTotVd(AfDecimal wdflImpstBolloTotVd) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_BOLLO_TOT_VD, wdflImpstBolloTotVd.copy());
    }

    public void setWdflImpstBolloTotVdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_BOLLO_TOT_VD, Pos.WDFL_IMPST_BOLLO_TOT_VD);
    }

    /**Original name: WDFL-IMPST-BOLLO-TOT-VD<br>*/
    public AfDecimal getWdflImpstBolloTotVd() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_BOLLO_TOT_VD, Len.Int.WDFL_IMPST_BOLLO_TOT_VD, Len.Fract.WDFL_IMPST_BOLLO_TOT_VD);
    }

    public byte[] getWdflImpstBolloTotVdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_BOLLO_TOT_VD, Pos.WDFL_IMPST_BOLLO_TOT_VD);
        return buffer;
    }

    public void setWdflImpstBolloTotVdNull(String wdflImpstBolloTotVdNull) {
        writeString(Pos.WDFL_IMPST_BOLLO_TOT_VD_NULL, wdflImpstBolloTotVdNull, Len.WDFL_IMPST_BOLLO_TOT_VD_NULL);
    }

    /**Original name: WDFL-IMPST-BOLLO-TOT-VD-NULL<br>*/
    public String getWdflImpstBolloTotVdNull() {
        return readString(Pos.WDFL_IMPST_BOLLO_TOT_VD_NULL, Len.WDFL_IMPST_BOLLO_TOT_VD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_BOLLO_TOT_VD = 1;
        public static final int WDFL_IMPST_BOLLO_TOT_VD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_BOLLO_TOT_VD = 8;
        public static final int WDFL_IMPST_BOLLO_TOT_VD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_BOLLO_TOT_VD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_BOLLO_TOT_VD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
