package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndImpstSost;
import it.accenture.jnais.copy.Ldbv6001;
import it.accenture.jnais.copy.RichDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS6000<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs6000Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-VAL-AST
    private IndImpstSost indValAst = new IndImpstSost();
    //Original name: VAL-AST-DB
    private RichDb valAstDb = new RichDb();
    //Original name: LDBV6001
    private Ldbv6001 ldbv6001 = new Ldbv6001();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndImpstSost getIndValAst() {
        return indValAst;
    }

    public Ldbv6001 getLdbv6001() {
        return ldbv6001;
    }

    public RichDb getValAstDb() {
        return valAstDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
