package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-IMP-ADER<br>
 * Variable: WTIT-IMP-ADER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_IMP_ADER;
    }

    public void setWtitImpAder(AfDecimal wtitImpAder) {
        writeDecimalAsPacked(Pos.WTIT_IMP_ADER, wtitImpAder.copy());
    }

    public void setWtitImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_IMP_ADER, Pos.WTIT_IMP_ADER);
    }

    /**Original name: WTIT-IMP-ADER<br>*/
    public AfDecimal getWtitImpAder() {
        return readPackedAsDecimal(Pos.WTIT_IMP_ADER, Len.Int.WTIT_IMP_ADER, Len.Fract.WTIT_IMP_ADER);
    }

    public byte[] getWtitImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_IMP_ADER, Pos.WTIT_IMP_ADER);
        return buffer;
    }

    public void initWtitImpAderSpaces() {
        fill(Pos.WTIT_IMP_ADER, Len.WTIT_IMP_ADER, Types.SPACE_CHAR);
    }

    public void setWtitImpAderNull(String wtitImpAderNull) {
        writeString(Pos.WTIT_IMP_ADER_NULL, wtitImpAderNull, Len.WTIT_IMP_ADER_NULL);
    }

    /**Original name: WTIT-IMP-ADER-NULL<br>*/
    public String getWtitImpAderNull() {
        return readString(Pos.WTIT_IMP_ADER_NULL, Len.WTIT_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_ADER = 1;
        public static final int WTIT_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_ADER = 8;
        public static final int WTIT_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
