package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-TROVATO<br>
 * Variable: FLAG-TROVATO from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagTrovato(char flagTrovato) {
        this.value = flagTrovato;
    }

    public char getFlagTrovato() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
