package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0503-CAMPO-INTERI<br>
 * Variable: IDSV0503-CAMPO-INTERI from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Idsv0503CampoInteri extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELE_STRINGA_INTERI_MAXOCCURS = 18;

    //==== CONSTRUCTORS ====
    public Idsv0503CampoInteri() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IDSV0503_CAMPO_INTERI;
    }

    public void setIdsv0503CampoInteri(String idsv0503CampoInteri) {
        writeString(Pos.IDSV0503_CAMPO_INTERI, idsv0503CampoInteri, Len.IDSV0503_CAMPO_INTERI);
    }

    /**Original name: IDSV0503-CAMPO-INTERI<br>*/
    public String getIdsv0503CampoInteri() {
        return readString(Pos.IDSV0503_CAMPO_INTERI, Len.IDSV0503_CAMPO_INTERI);
    }

    public String getIdsv0503CampoInteriFormatted() {
        return Functions.padBlanks(getIdsv0503CampoInteri(), Len.IDSV0503_CAMPO_INTERI);
    }

    public void setEleStringaInteri(int eleStringaInteriIdx, char eleStringaInteri) {
        int position = Pos.idsv0503EleStringaInteri(eleStringaInteriIdx - 1);
        writeChar(position, eleStringaInteri);
    }

    /**Original name: IDSV0503-ELE-STRINGA-INTERI<br>*/
    public char getEleStringaInteri(int eleStringaInteriIdx) {
        int position = Pos.idsv0503EleStringaInteri(eleStringaInteriIdx - 1);
        return readChar(position);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int IDSV0503_CAMPO_INTERI = 1;
        public static final int IDSV0503_ARRAY_STRINGA_INTERI = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int idsv0503EleStringaInteri(int idx) {
            return IDSV0503_ARRAY_STRINGA_INTERI + idx * Len.ELE_STRINGA_INTERI;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_STRINGA_INTERI = 1;
        public static final int IDSV0503_CAMPO_INTERI = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
