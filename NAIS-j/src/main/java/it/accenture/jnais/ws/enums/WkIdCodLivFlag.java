package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-ID-COD-LIV-FLAG<br>
 * Variable: WK-ID-COD-LIV-FLAG from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkIdCodLivFlag {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO = 'S';
    public static final char NON_TROVATO = 'N';

    //==== METHODS ====
    public void setWkIdCodLivFlag(char wkIdCodLivFlag) {
        this.value = wkIdCodLivFlag;
    }

    public char getWkIdCodLivFlag() {
        return this.value;
    }

    public boolean isTrovato() {
        return value == TROVATO;
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public boolean isNonTrovato() {
        return value == NON_TROVATO;
    }

    public void setNonTrovato() {
        value = NON_TROVATO;
    }
}
