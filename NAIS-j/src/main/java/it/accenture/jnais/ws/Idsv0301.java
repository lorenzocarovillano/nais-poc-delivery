package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.notifier.StringChangeNotifier;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.Idsv0301ActionType;
import it.accenture.jnais.ws.enums.Idsv0301Esito;
import it.accenture.jnais.ws.enums.Idsv0301FlContiguous;
import it.accenture.jnais.ws.enums.Idsv0301Operazione;
import it.accenture.jnais.ws.enums.Idsv0301TemporaryTable;

/**Original name: IDSV0301<br>
 * Variable: IDSV0301 from copybook IDSV0301<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Idsv0301 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IDSV0301-ADDRESS
    private int address = DefaultValues.BIN_INT_VAL;
    //Original name: IDSV0301-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: IDSV0301-ID-TEMPORARY-DATA
    private long idTemporaryData = DefaultValues.LONG_VAL;
    //Original name: IDSV0301-ALIAS-STR-DATO
    private String aliasStrDato = DefaultValues.stringVal(Len.ALIAS_STR_DATO);
    //Original name: IDSV0301-NUM-FRAME
    private int numFrame = DefaultValues.INT_VAL;
    //Original name: IDSV0301-ID-SESSION
    private String idSession = DefaultValues.stringVal(Len.ID_SESSION);
    //Original name: IDSV0301-FL-CONTIGUOUS
    private Idsv0301FlContiguous flContiguous = new Idsv0301FlContiguous();
    //Original name: IDSV0301-TOTAL-RECURRENCE
    private int totalRecurrence = DefaultValues.INT_VAL;
    //Original name: IDSV0301-PARTIAL-RECURRENCE
    private int partialRecurrence = DefaultValues.INT_VAL;
    //Original name: IDSV0301-ACTUAL-RECURRENCE
    private int actualRecurrence = DefaultValues.INT_VAL;
    //Original name: IDSV0301-TYPE-RECORD
    private String typeRecord = DefaultValues.stringVal(Len.TYPE_RECORD);
    //Original name: IDSV0301-OPERAZIONE
    private Idsv0301Operazione operazione = new Idsv0301Operazione();
    //Original name: IDSV0301-ACTION-TYPE
    private Idsv0301ActionType actionType = new Idsv0301ActionType();
    //Original name: IDSV0301-TEMPORARY-TABLE
    private Idsv0301TemporaryTable temporaryTable = new Idsv0301TemporaryTable();
    //Original name: IDSV0301-BUFFER-DATA-LEN
    private StringChangeNotifier bufferDataLen = new StringChangeNotifier(Len.BUFFER_DATA_LEN);
    //Original name: IDSV0301-ESITO
    private Idsv0301Esito esito = new Idsv0301Esito();
    //Original name: IDSV0301-COD-SERVIZIO-BE
    private String codServizioBe = DefaultValues.stringVal(Len.COD_SERVIZIO_BE);
    //Original name: IDSV0301-DESC-ERRORE-ESTESA
    private String descErroreEstesa = DefaultValues.stringVal(Len.DESC_ERRORE_ESTESA);
    //Original name: IDSV0301-SQLCODE-SIGNED
    private Idso0011SqlcodeSigned sqlcodeSigned = new Idso0011SqlcodeSigned();
    //Original name: IDSV0301-SQLCODE
    private String sqlcode = DefaultValues.stringVal(Len.SQLCODE);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IDSV0301;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIdsv0301Bytes(buf);
    }

    public void setIdsv0301Bytes(byte[] buffer) {
        setIdsv0301Bytes(buffer, 1);
    }

    public byte[] getIdsv0301Bytes() {
        byte[] buffer = new byte[Len.IDSV0301];
        return getIdsv0301Bytes(buffer, 1);
    }

    public void setIdsv0301Bytes(byte[] buffer, int offset) {
        int position = offset;
        address = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        idTemporaryData = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ID_TEMPORARY_DATA, 0);
        position += Len.ID_TEMPORARY_DATA;
        aliasStrDato = MarshalByte.readString(buffer, position, Len.ALIAS_STR_DATO);
        position += Len.ALIAS_STR_DATO;
        numFrame = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_FRAME, 0);
        position += Len.NUM_FRAME;
        idSession = MarshalByte.readString(buffer, position, Len.ID_SESSION);
        position += Len.ID_SESSION;
        flContiguous.setFlContiguous(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        totalRecurrence = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TOTAL_RECURRENCE, 0);
        position += Len.TOTAL_RECURRENCE;
        partialRecurrence = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PARTIAL_RECURRENCE, 0);
        position += Len.PARTIAL_RECURRENCE;
        actualRecurrence = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ACTUAL_RECURRENCE, 0);
        position += Len.ACTUAL_RECURRENCE;
        typeRecord = MarshalByte.readString(buffer, position, Len.TYPE_RECORD);
        position += Len.TYPE_RECORD;
        operazione.setOperazione(MarshalByte.readString(buffer, position, Idsv0301Operazione.Len.OPERAZIONE));
        position += Idsv0301Operazione.Len.OPERAZIONE;
        actionType.setActionType(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        temporaryTable.setTemporaryTable(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        setBufferDataLen(MarshalByte.readInt(buffer, position, Len.BUFFER_DATA_LEN, SignType.NO_SIGN));
        position += Len.BUFFER_DATA_LEN;
        setAreaErroriBytes(buffer, position);
        position += Len.AREA_ERRORI;
        sqlcodeSigned.setSqlcodeSigned(MarshalByte.readInt(buffer, position, Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED));
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        sqlcode = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.SQLCODE), Len.SQLCODE);
    }

    public byte[] getIdsv0301Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, address);
        position += Types.INT_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeLongAsPacked(buffer, position, idTemporaryData, Len.Int.ID_TEMPORARY_DATA, 0);
        position += Len.ID_TEMPORARY_DATA;
        MarshalByte.writeString(buffer, position, aliasStrDato, Len.ALIAS_STR_DATO);
        position += Len.ALIAS_STR_DATO;
        MarshalByte.writeIntAsPacked(buffer, position, numFrame, Len.Int.NUM_FRAME, 0);
        position += Len.NUM_FRAME;
        MarshalByte.writeString(buffer, position, idSession, Len.ID_SESSION);
        position += Len.ID_SESSION;
        MarshalByte.writeChar(buffer, position, flContiguous.getFlContiguous());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, totalRecurrence, Len.Int.TOTAL_RECURRENCE, 0);
        position += Len.TOTAL_RECURRENCE;
        MarshalByte.writeIntAsPacked(buffer, position, partialRecurrence, Len.Int.PARTIAL_RECURRENCE, 0);
        position += Len.PARTIAL_RECURRENCE;
        MarshalByte.writeIntAsPacked(buffer, position, actualRecurrence, Len.Int.ACTUAL_RECURRENCE, 0);
        position += Len.ACTUAL_RECURRENCE;
        MarshalByte.writeString(buffer, position, typeRecord, Len.TYPE_RECORD);
        position += Len.TYPE_RECORD;
        MarshalByte.writeString(buffer, position, operazione.getOperazione(), Idsv0301Operazione.Len.OPERAZIONE);
        position += Idsv0301Operazione.Len.OPERAZIONE;
        MarshalByte.writeChar(buffer, position, actionType.getActionType());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, temporaryTable.getTemporaryTable());
        position += Types.CHAR_SIZE;
        MarshalByte.writeInt(buffer, position, getBufferDataLen0(), Len.BUFFER_DATA_LEN, SignType.NO_SIGN);
        position += Len.BUFFER_DATA_LEN;
        getAreaErroriBytes(buffer, position);
        position += Len.AREA_ERRORI;
        MarshalByte.writeInt(buffer, position, sqlcodeSigned.getSqlcodeSigned(), Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED);
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
        return buffer;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public int getAddress() {
        return this.address;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setIdTemporaryData(long idTemporaryData) {
        this.idTemporaryData = idTemporaryData;
    }

    public long getIdTemporaryData() {
        return this.idTemporaryData;
    }

    public void setAliasStrDato(String aliasStrDato) {
        this.aliasStrDato = Functions.subString(aliasStrDato, Len.ALIAS_STR_DATO);
    }

    public String getAliasStrDato() {
        return this.aliasStrDato;
    }

    public void setNumFrame(int numFrame) {
        this.numFrame = numFrame;
    }

    public int getNumFrame() {
        return this.numFrame;
    }

    public void setIdSession(String idSession) {
        this.idSession = Functions.subString(idSession, Len.ID_SESSION);
    }

    public String getIdSession() {
        return this.idSession;
    }

    public String getIdSessionFormatted() {
        return Functions.padBlanks(getIdSession(), Len.ID_SESSION);
    }

    public void setTotalRecurrence(int totalRecurrence) {
        this.totalRecurrence = totalRecurrence;
    }

    public int getTotalRecurrence() {
        return this.totalRecurrence;
    }

    public void setPartialRecurrence(int partialRecurrence) {
        this.partialRecurrence = partialRecurrence;
    }

    public int getPartialRecurrence() {
        return this.partialRecurrence;
    }

    public void setActualRecurrence(int actualRecurrence) {
        this.actualRecurrence = actualRecurrence;
    }

    public int getActualRecurrence() {
        return this.actualRecurrence;
    }

    public void setTypeRecord(String typeRecord) {
        this.typeRecord = Functions.subString(typeRecord, Len.TYPE_RECORD);
    }

    public String getTypeRecord() {
        return this.typeRecord;
    }

    public void setBufferDataLen(int bufferDataLen) {
        this.bufferDataLen.setValue(bufferDataLen);
    }

    public int getBufferDataLen0() {
        return bufferDataLen.getValue();
    }

    public void setAreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        esito.setEsito(MarshalByte.readString(buffer, position, Idsv0301Esito.Len.ESITO));
        position += Idsv0301Esito.Len.ESITO;
        setLogErroreBytes(buffer, position);
    }

    public byte[] getAreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, esito.getEsito(), Idsv0301Esito.Len.ESITO);
        position += Idsv0301Esito.Len.ESITO;
        getLogErroreBytes(buffer, position);
        return buffer;
    }

    public void setLogErroreBytes(byte[] buffer, int offset) {
        int position = offset;
        codServizioBe = MarshalByte.readString(buffer, position, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        descErroreEstesa = MarshalByte.readString(buffer, position, Len.DESC_ERRORE_ESTESA);
    }

    public byte[] getLogErroreBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codServizioBe, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        MarshalByte.writeString(buffer, position, descErroreEstesa, Len.DESC_ERRORE_ESTESA);
        return buffer;
    }

    public void setCodServizioBe(String codServizioBe) {
        this.codServizioBe = Functions.subString(codServizioBe, Len.COD_SERVIZIO_BE);
    }

    public String getCodServizioBe() {
        return this.codServizioBe;
    }

    public void setDescErroreEstesa(String descErroreEstesa) {
        this.descErroreEstesa = Functions.subString(descErroreEstesa, Len.DESC_ERRORE_ESTESA);
    }

    public String getDescErroreEstesa() {
        return this.descErroreEstesa;
    }

    public void setSqlcode(long sqlcode) {
        this.sqlcode = PicFormatter.display("--(7)9").format(sqlcode).toString();
    }

    public long getSqlcode() {
        return PicParser.display("--(7)9").parseLong(this.sqlcode);
    }

    public Idsv0301ActionType getActionType() {
        return actionType;
    }

    public StringChangeNotifier getBufferDataLen() {
        return bufferDataLen;
    }

    public Idsv0301Esito getEsito() {
        return esito;
    }

    public Idsv0301FlContiguous getFlContiguous() {
        return flContiguous;
    }

    public Idsv0301Operazione getOperazione() {
        return operazione;
    }

    public Idso0011SqlcodeSigned getSqlcodeSigned() {
        return sqlcodeSigned;
    }

    public Idsv0301TemporaryTable getTemporaryTable() {
        return temporaryTable;
    }

    @Override
    public byte[] serialize() {
        return getIdsv0301Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ADDRESS = 4;
        public static final int COD_COMP_ANIA = 3;
        public static final int ID_TEMPORARY_DATA = 6;
        public static final int ALIAS_STR_DATO = 30;
        public static final int NUM_FRAME = 3;
        public static final int ID_SESSION = 20;
        public static final int TOTAL_RECURRENCE = 3;
        public static final int PARTIAL_RECURRENCE = 3;
        public static final int ACTUAL_RECURRENCE = 3;
        public static final int TYPE_RECORD = 3;
        public static final int BUFFER_DATA_LEN = 7;
        public static final int COD_SERVIZIO_BE = 8;
        public static final int DESC_ERRORE_ESTESA = 200;
        public static final int LOG_ERRORE = COD_SERVIZIO_BE + DESC_ERRORE_ESTESA;
        public static final int AREA_ERRORI = Idsv0301Esito.Len.ESITO + LOG_ERRORE;
        public static final int SQLCODE = 9;
        public static final int IDSV0301 = ADDRESS + COD_COMP_ANIA + ID_TEMPORARY_DATA + ALIAS_STR_DATO + NUM_FRAME + ID_SESSION + Idsv0301FlContiguous.Len.FL_CONTIGUOUS + TOTAL_RECURRENCE + PARTIAL_RECURRENCE + ACTUAL_RECURRENCE + TYPE_RECORD + Idsv0301Operazione.Len.OPERAZIONE + Idsv0301ActionType.Len.ACTION_TYPE + Idsv0301TemporaryTable.Len.TEMPORARY_TABLE + BUFFER_DATA_LEN + AREA_ERRORI + Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED + SQLCODE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_COMP_ANIA = 5;
            public static final int ID_TEMPORARY_DATA = 10;
            public static final int NUM_FRAME = 5;
            public static final int TOTAL_RECURRENCE = 5;
            public static final int PARTIAL_RECURRENCE = 5;
            public static final int ACTUAL_RECURRENCE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
