package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-NS-QUO<br>
 * Variable: B03-NS-QUO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03NsQuo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03NsQuo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_NS_QUO;
    }

    public void setB03NsQuo(AfDecimal b03NsQuo) {
        writeDecimalAsPacked(Pos.B03_NS_QUO, b03NsQuo.copy());
    }

    public void setB03NsQuoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_NS_QUO, Pos.B03_NS_QUO);
    }

    /**Original name: B03-NS-QUO<br>*/
    public AfDecimal getB03NsQuo() {
        return readPackedAsDecimal(Pos.B03_NS_QUO, Len.Int.B03_NS_QUO, Len.Fract.B03_NS_QUO);
    }

    public byte[] getB03NsQuoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_NS_QUO, Pos.B03_NS_QUO);
        return buffer;
    }

    public void setB03NsQuoNull(String b03NsQuoNull) {
        writeString(Pos.B03_NS_QUO_NULL, b03NsQuoNull, Len.B03_NS_QUO_NULL);
    }

    /**Original name: B03-NS-QUO-NULL<br>*/
    public String getB03NsQuoNull() {
        return readString(Pos.B03_NS_QUO_NULL, Len.B03_NS_QUO_NULL);
    }

    public String getB03NsQuoNullFormatted() {
        return Functions.padBlanks(getB03NsQuoNull(), Len.B03_NS_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_NS_QUO = 1;
        public static final int B03_NS_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_NS_QUO = 4;
        public static final int B03_NS_QUO_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_NS_QUO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_NS_QUO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
