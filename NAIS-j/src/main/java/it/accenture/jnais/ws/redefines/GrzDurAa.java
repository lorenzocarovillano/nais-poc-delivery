package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-DUR-AA<br>
 * Variable: GRZ-DUR-AA from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzDurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzDurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_DUR_AA;
    }

    public void setGrzDurAa(int grzDurAa) {
        writeIntAsPacked(Pos.GRZ_DUR_AA, grzDurAa, Len.Int.GRZ_DUR_AA);
    }

    public void setGrzDurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_DUR_AA, Pos.GRZ_DUR_AA);
    }

    /**Original name: GRZ-DUR-AA<br>*/
    public int getGrzDurAa() {
        return readPackedAsInt(Pos.GRZ_DUR_AA, Len.Int.GRZ_DUR_AA);
    }

    public byte[] getGrzDurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_DUR_AA, Pos.GRZ_DUR_AA);
        return buffer;
    }

    public void setGrzDurAaNull(String grzDurAaNull) {
        writeString(Pos.GRZ_DUR_AA_NULL, grzDurAaNull, Len.GRZ_DUR_AA_NULL);
    }

    /**Original name: GRZ-DUR-AA-NULL<br>*/
    public String getGrzDurAaNull() {
        return readString(Pos.GRZ_DUR_AA_NULL, Len.GRZ_DUR_AA_NULL);
    }

    public String getGrzDurAaNullFormatted() {
        return Functions.padBlanks(getGrzDurAaNull(), Len.GRZ_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_DUR_AA = 1;
        public static final int GRZ_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_DUR_AA = 3;
        public static final int GRZ_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
