package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-RIC-GRZ<br>
 * Variable: WK-RIC-GRZ from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkRicGrz {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATA = 'S';
    public static final char NON_TROVATA = 'N';

    //==== METHODS ====
    public void setWkRicGrz(char wkRicGrz) {
        this.value = wkRicGrz;
    }

    public char getWkRicGrz() {
        return this.value;
    }

    public boolean isTrovata() {
        return value == TROVATA;
    }

    public void setTrovata() {
        value = TROVATA;
    }

    public boolean isNonTrovata() {
        return value == NON_TROVATA;
    }

    public void setNonTrovata() {
        value = NON_TROVATA;
    }
}
