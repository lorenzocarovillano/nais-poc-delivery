package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-VARIABILI<br>
 * Variable: WS-VARIABILI from program IEAS9900<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsVariabiliIeas9900 {

    //==== PROPERTIES ====
    //Original name: WS-COMPAGNIA
    private String compagnia = DefaultValues.stringVal(Len.COMPAGNIA);
    //Original name: WS-DESC-ESTESA
    private String descEstesa = DefaultValues.stringVal(Len.DESC_ESTESA);

    //==== METHODS ====
    public void setCompagniaFormatted(String compagnia) {
        this.compagnia = Trunc.toUnsignedNumeric(compagnia, Len.COMPAGNIA);
    }

    public int getCompagnia() {
        return NumericDisplay.asInt(this.compagnia);
    }

    public String getCompagniaFormatted() {
        return this.compagnia;
    }

    public String getCompagniaAsString() {
        return getCompagniaFormatted();
    }

    public void setDescEstesa(String descEstesa) {
        this.descEstesa = Functions.subString(descEstesa, Len.DESC_ESTESA);
    }

    public String getDescEstesa() {
        return this.descEstesa;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COMPAGNIA = 5;
        public static final int DESC_ESTESA = 200;
        public static final int NUM_ACCESSO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
