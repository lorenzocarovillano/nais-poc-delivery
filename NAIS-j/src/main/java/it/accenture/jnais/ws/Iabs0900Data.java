package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.commons.data.to.IDettRich;
import it.accenture.jnais.copy.DettRich;
import it.accenture.jnais.copy.Idbvder2;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndCompStrDato;
import it.accenture.jnais.copy.RichDb;
import it.accenture.jnais.ws.enums.Lccc006TrattDati;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IABS0900<br>
 * Generated as a class for rule WS.<br>*/
public class Iabs0900Data implements IDettRich {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: PRENOTAZIONE
    private String prenotazione = "PR";
    //Original name: FLAG-ACCESSO
    private Lccc006TrattDati flagAccesso = new Lccc006TrattDati();
    //Original name: WK-ID-RICH
    private String wkIdRich = DefaultValues.stringVal(Len.WK_ID_RICH);
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-RICH
    private IndCompStrDato indRich = new IndCompStrDato();
    //Original name: RICH-DB
    private RichDb richDb = new RichDb();
    //Original name: DETT-RICH
    private DettRich dettRich = new DettRich();
    //Original name: IDBVDER2
    private Idbvder2 idbvder2 = new Idbvder2();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setPrenotazione(String prenotazione) {
        this.prenotazione = Functions.subString(prenotazione, Len.PRENOTAZIONE);
    }

    public String getPrenotazione() {
        return this.prenotazione;
    }

    public void setWkIdRich(int wkIdRich) {
        this.wkIdRich = NumericDisplay.asString(wkIdRich, Len.WK_ID_RICH);
    }

    public int getWkIdRich() {
        return NumericDisplay.asInt(this.wkIdRich);
    }

    public String getWkIdRichFormatted() {
        return this.wkIdRich;
    }

    @Override
    public String getAreaDRichVchar() {
        return dettRich.getAreaDRichVcharFormatted();
    }

    @Override
    public void setAreaDRichVchar(String areaDRichVchar) {
        this.dettRich.setAreaDRichVcharFormatted(areaDRichVchar);
    }

    @Override
    public String getAreaDRichVcharObj() {
        if (idbvder2.getDerAreaDRich() >= 0) {
            return getAreaDRichVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAreaDRichVcharObj(String areaDRichVcharObj) {
        if (areaDRichVcharObj != null) {
            setAreaDRichVchar(areaDRichVcharObj);
            idbvder2.setDerAreaDRich(((short)0));
        }
        else {
            idbvder2.setDerAreaDRich(((short)-1));
        }
    }

    public DettRich getDettRich() {
        return dettRich;
    }

    public Lccc006TrattDati getFlagAccesso() {
        return flagAccesso;
    }

    public Idbvder2 getIdbvder2() {
        return idbvder2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndCompStrDato getIndRich() {
        return indRich;
    }

    public RichDb getRichDb() {
        return richDb;
    }

    @Override
    public String getTpAreaDRich() {
        return dettRich.getTpAreaDRich();
    }

    @Override
    public void setTpAreaDRich(String tpAreaDRich) {
        this.dettRich.setTpAreaDRich(tpAreaDRich);
    }

    @Override
    public String getTpAreaDRichObj() {
        if (idbvder2.getDerTpAreaDRich() >= 0) {
            return getTpAreaDRich();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAreaDRichObj(String tpAreaDRichObj) {
        if (tpAreaDRichObj != null) {
            setTpAreaDRich(tpAreaDRichObj);
            idbvder2.setDerTpAreaDRich(((short)0));
        }
        else {
            idbvder2.setDerTpAreaDRich(((short)-1));
        }
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PRENOTAZIONE = 2;
        public static final int WK_ID_RICH = 9;
        public static final int FLR1 = 300;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
