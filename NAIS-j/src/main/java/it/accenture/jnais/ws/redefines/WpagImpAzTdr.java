package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-AZ-TDR<br>
 * Variable: WPAG-IMP-AZ-TDR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpAzTdr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpAzTdr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_AZ_TDR;
    }

    public void setWpagImpAzTdr(AfDecimal wpagImpAzTdr) {
        writeDecimalAsPacked(Pos.WPAG_IMP_AZ_TDR, wpagImpAzTdr.copy());
    }

    public void setWpagImpAzTdrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_AZ_TDR, Pos.WPAG_IMP_AZ_TDR);
    }

    /**Original name: WPAG-IMP-AZ-TDR<br>*/
    public AfDecimal getWpagImpAzTdr() {
        return readPackedAsDecimal(Pos.WPAG_IMP_AZ_TDR, Len.Int.WPAG_IMP_AZ_TDR, Len.Fract.WPAG_IMP_AZ_TDR);
    }

    public byte[] getWpagImpAzTdrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_AZ_TDR, Pos.WPAG_IMP_AZ_TDR);
        return buffer;
    }

    public void initWpagImpAzTdrSpaces() {
        fill(Pos.WPAG_IMP_AZ_TDR, Len.WPAG_IMP_AZ_TDR, Types.SPACE_CHAR);
    }

    public void setWpagImpAzTdrNull(String wpagImpAzTdrNull) {
        writeString(Pos.WPAG_IMP_AZ_TDR_NULL, wpagImpAzTdrNull, Len.WPAG_IMP_AZ_TDR_NULL);
    }

    /**Original name: WPAG-IMP-AZ-TDR-NULL<br>*/
    public String getWpagImpAzTdrNull() {
        return readString(Pos.WPAG_IMP_AZ_TDR_NULL, Len.WPAG_IMP_AZ_TDR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_AZ_TDR = 1;
        public static final int WPAG_IMP_AZ_TDR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_AZ_TDR = 8;
        public static final int WPAG_IMP_AZ_TDR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_AZ_TDR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_AZ_TDR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
