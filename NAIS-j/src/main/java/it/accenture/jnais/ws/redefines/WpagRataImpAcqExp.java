package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPAG-RATA-IMP-ACQ-EXP<br>
 * Variable: WPAG-RATA-IMP-ACQ-EXP from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataImpAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataImpAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_IMP_ACQ_EXP;
    }

    public void setWpagRataImpAcqExp(AfDecimal wpagRataImpAcqExp) {
        writeDecimalAsPacked(Pos.WPAG_RATA_IMP_ACQ_EXP, wpagRataImpAcqExp.copy());
    }

    public void setWpagRataImpAcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_IMP_ACQ_EXP, Pos.WPAG_RATA_IMP_ACQ_EXP);
    }

    /**Original name: WPAG-RATA-IMP-ACQ-EXP<br>*/
    public AfDecimal getWpagRataImpAcqExp() {
        return readPackedAsDecimal(Pos.WPAG_RATA_IMP_ACQ_EXP, Len.Int.WPAG_RATA_IMP_ACQ_EXP, Len.Fract.WPAG_RATA_IMP_ACQ_EXP);
    }

    public byte[] getWpagRataImpAcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_IMP_ACQ_EXP, Pos.WPAG_RATA_IMP_ACQ_EXP);
        return buffer;
    }

    public void initWpagRataImpAcqExpSpaces() {
        fill(Pos.WPAG_RATA_IMP_ACQ_EXP, Len.WPAG_RATA_IMP_ACQ_EXP, Types.SPACE_CHAR);
    }

    public void setWpagRataImpAcqExpNull(String wpagRataImpAcqExpNull) {
        writeString(Pos.WPAG_RATA_IMP_ACQ_EXP_NULL, wpagRataImpAcqExpNull, Len.WPAG_RATA_IMP_ACQ_EXP_NULL);
    }

    /**Original name: WPAG-RATA-IMP-ACQ-EXP-NULL<br>*/
    public String getWpagRataImpAcqExpNull() {
        return readString(Pos.WPAG_RATA_IMP_ACQ_EXP_NULL, Len.WPAG_RATA_IMP_ACQ_EXP_NULL);
    }

    public String getWpagRataImpAcqExpNullFormatted() {
        return Functions.padBlanks(getWpagRataImpAcqExpNull(), Len.WPAG_RATA_IMP_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_IMP_ACQ_EXP = 1;
        public static final int WPAG_RATA_IMP_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_IMP_ACQ_EXP = 8;
        public static final int WPAG_RATA_IMP_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_IMP_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_IMP_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
