package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-ALLINEA-STATI<br>
 * Variable: FLAG-ALLINEA-STATI from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagAllineaStati {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_ALLINEA_STATI);
    public static final String OK = "OK";
    public static final String KO = "KO";

    //==== METHODS ====
    public void setFlagAllineaStati(String flagAllineaStati) {
        this.value = Functions.subString(flagAllineaStati, Len.FLAG_ALLINEA_STATI);
    }

    public String getFlagAllineaStati() {
        return this.value;
    }

    public boolean isOk() {
        return value.equals(OK);
    }

    public void setOk() {
        value = OK;
    }

    public void setKo() {
        value = KO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_ALLINEA_STATI = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
