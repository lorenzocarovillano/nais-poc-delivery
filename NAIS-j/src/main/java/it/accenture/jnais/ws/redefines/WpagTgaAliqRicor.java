package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-TGA-ALIQ-RICOR<br>
 * Variable: WPAG-TGA-ALIQ-RICOR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaAliqRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTgaAliqRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TGA_ALIQ_RICOR;
    }

    public void setWpagTgaAliqRicor(AfDecimal wpagTgaAliqRicor) {
        writeDecimalAsPacked(Pos.WPAG_TGA_ALIQ_RICOR, wpagTgaAliqRicor.copy());
    }

    public void setWpagTgaAliqRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TGA_ALIQ_RICOR, Pos.WPAG_TGA_ALIQ_RICOR);
    }

    /**Original name: WPAG-TGA-ALIQ-RICOR<br>*/
    public AfDecimal getWpagTgaAliqRicor() {
        return readPackedAsDecimal(Pos.WPAG_TGA_ALIQ_RICOR, Len.Int.WPAG_TGA_ALIQ_RICOR, Len.Fract.WPAG_TGA_ALIQ_RICOR);
    }

    public byte[] getWpagTgaAliqRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TGA_ALIQ_RICOR, Pos.WPAG_TGA_ALIQ_RICOR);
        return buffer;
    }

    public void initWpagTgaAliqRicorSpaces() {
        fill(Pos.WPAG_TGA_ALIQ_RICOR, Len.WPAG_TGA_ALIQ_RICOR, Types.SPACE_CHAR);
    }

    public void setWpagTgaAliqRicorNull(String wpagTgaAliqRicorNull) {
        writeString(Pos.WPAG_TGA_ALIQ_RICOR_NULL, wpagTgaAliqRicorNull, Len.WPAG_TGA_ALIQ_RICOR_NULL);
    }

    /**Original name: WPAG-TGA-ALIQ-RICOR-NULL<br>*/
    public String getWpagTgaAliqRicorNull() {
        return readString(Pos.WPAG_TGA_ALIQ_RICOR_NULL, Len.WPAG_TGA_ALIQ_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_ALIQ_RICOR = 1;
        public static final int WPAG_TGA_ALIQ_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_ALIQ_RICOR = 8;
        public static final int WPAG_TGA_ALIQ_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_ALIQ_RICOR = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_ALIQ_RICOR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
