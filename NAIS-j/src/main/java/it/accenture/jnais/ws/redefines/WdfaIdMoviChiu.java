package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-ID-MOVI-CHIU<br>
 * Variable: WDFA-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_ID_MOVI_CHIU;
    }

    public void setWdfaIdMoviChiu(int wdfaIdMoviChiu) {
        writeIntAsPacked(Pos.WDFA_ID_MOVI_CHIU, wdfaIdMoviChiu, Len.Int.WDFA_ID_MOVI_CHIU);
    }

    public void setWdfaIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_ID_MOVI_CHIU, Pos.WDFA_ID_MOVI_CHIU);
    }

    /**Original name: WDFA-ID-MOVI-CHIU<br>*/
    public int getWdfaIdMoviChiu() {
        return readPackedAsInt(Pos.WDFA_ID_MOVI_CHIU, Len.Int.WDFA_ID_MOVI_CHIU);
    }

    public byte[] getWdfaIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_ID_MOVI_CHIU, Pos.WDFA_ID_MOVI_CHIU);
        return buffer;
    }

    public void setWdfaIdMoviChiuNull(String wdfaIdMoviChiuNull) {
        writeString(Pos.WDFA_ID_MOVI_CHIU_NULL, wdfaIdMoviChiuNull, Len.WDFA_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WDFA-ID-MOVI-CHIU-NULL<br>*/
    public String getWdfaIdMoviChiuNull() {
        return readString(Pos.WDFA_ID_MOVI_CHIU_NULL, Len.WDFA_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_ID_MOVI_CHIU = 1;
        public static final int WDFA_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_ID_MOVI_CHIU = 5;
        public static final int WDFA_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
