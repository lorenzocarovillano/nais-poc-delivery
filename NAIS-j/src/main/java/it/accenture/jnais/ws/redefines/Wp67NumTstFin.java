package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-NUM-TST-FIN<br>
 * Variable: WP67-NUM-TST-FIN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67NumTstFin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67NumTstFin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_NUM_TST_FIN;
    }

    public void setWp67NumTstFin(int wp67NumTstFin) {
        writeIntAsPacked(Pos.WP67_NUM_TST_FIN, wp67NumTstFin, Len.Int.WP67_NUM_TST_FIN);
    }

    public void setWp67NumTstFinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_NUM_TST_FIN, Pos.WP67_NUM_TST_FIN);
    }

    /**Original name: WP67-NUM-TST-FIN<br>*/
    public int getWp67NumTstFin() {
        return readPackedAsInt(Pos.WP67_NUM_TST_FIN, Len.Int.WP67_NUM_TST_FIN);
    }

    public byte[] getWp67NumTstFinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_NUM_TST_FIN, Pos.WP67_NUM_TST_FIN);
        return buffer;
    }

    public void setWp67NumTstFinNull(String wp67NumTstFinNull) {
        writeString(Pos.WP67_NUM_TST_FIN_NULL, wp67NumTstFinNull, Len.WP67_NUM_TST_FIN_NULL);
    }

    /**Original name: WP67-NUM-TST-FIN-NULL<br>*/
    public String getWp67NumTstFinNull() {
        return readString(Pos.WP67_NUM_TST_FIN_NULL, Len.WP67_NUM_TST_FIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_NUM_TST_FIN = 1;
        public static final int WP67_NUM_TST_FIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_NUM_TST_FIN = 3;
        public static final int WP67_NUM_TST_FIN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_NUM_TST_FIN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
