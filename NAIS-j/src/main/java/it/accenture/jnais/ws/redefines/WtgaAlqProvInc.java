package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ALQ-PROV-INC<br>
 * Variable: WTGA-ALQ-PROV-INC from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaAlqProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaAlqProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ALQ_PROV_INC;
    }

    public void setWtgaAlqProvInc(AfDecimal wtgaAlqProvInc) {
        writeDecimalAsPacked(Pos.WTGA_ALQ_PROV_INC, wtgaAlqProvInc.copy());
    }

    public void setWtgaAlqProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ALQ_PROV_INC, Pos.WTGA_ALQ_PROV_INC);
    }

    /**Original name: WTGA-ALQ-PROV-INC<br>*/
    public AfDecimal getWtgaAlqProvInc() {
        return readPackedAsDecimal(Pos.WTGA_ALQ_PROV_INC, Len.Int.WTGA_ALQ_PROV_INC, Len.Fract.WTGA_ALQ_PROV_INC);
    }

    public byte[] getWtgaAlqProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ALQ_PROV_INC, Pos.WTGA_ALQ_PROV_INC);
        return buffer;
    }

    public void initWtgaAlqProvIncSpaces() {
        fill(Pos.WTGA_ALQ_PROV_INC, Len.WTGA_ALQ_PROV_INC, Types.SPACE_CHAR);
    }

    public void setWtgaAlqProvIncNull(String wtgaAlqProvIncNull) {
        writeString(Pos.WTGA_ALQ_PROV_INC_NULL, wtgaAlqProvIncNull, Len.WTGA_ALQ_PROV_INC_NULL);
    }

    /**Original name: WTGA-ALQ-PROV-INC-NULL<br>*/
    public String getWtgaAlqProvIncNull() {
        return readString(Pos.WTGA_ALQ_PROV_INC_NULL, Len.WTGA_ALQ_PROV_INC_NULL);
    }

    public String getWtgaAlqProvIncNullFormatted() {
        return Functions.padBlanks(getWtgaAlqProvIncNull(), Len.WTGA_ALQ_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_PROV_INC = 1;
        public static final int WTGA_ALQ_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_PROV_INC = 4;
        public static final int WTGA_ALQ_PROV_INC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
