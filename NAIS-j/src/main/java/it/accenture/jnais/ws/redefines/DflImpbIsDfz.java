package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-IS-DFZ<br>
 * Variable: DFL-IMPB-IS-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbIsDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbIsDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_IS_DFZ;
    }

    public void setDflImpbIsDfz(AfDecimal dflImpbIsDfz) {
        writeDecimalAsPacked(Pos.DFL_IMPB_IS_DFZ, dflImpbIsDfz.copy());
    }

    public void setDflImpbIsDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_IS_DFZ, Pos.DFL_IMPB_IS_DFZ);
    }

    /**Original name: DFL-IMPB-IS-DFZ<br>*/
    public AfDecimal getDflImpbIsDfz() {
        return readPackedAsDecimal(Pos.DFL_IMPB_IS_DFZ, Len.Int.DFL_IMPB_IS_DFZ, Len.Fract.DFL_IMPB_IS_DFZ);
    }

    public byte[] getDflImpbIsDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_IS_DFZ, Pos.DFL_IMPB_IS_DFZ);
        return buffer;
    }

    public void setDflImpbIsDfzNull(String dflImpbIsDfzNull) {
        writeString(Pos.DFL_IMPB_IS_DFZ_NULL, dflImpbIsDfzNull, Len.DFL_IMPB_IS_DFZ_NULL);
    }

    /**Original name: DFL-IMPB-IS-DFZ-NULL<br>*/
    public String getDflImpbIsDfzNull() {
        return readString(Pos.DFL_IMPB_IS_DFZ_NULL, Len.DFL_IMPB_IS_DFZ_NULL);
    }

    public String getDflImpbIsDfzNullFormatted() {
        return Functions.padBlanks(getDflImpbIsDfzNull(), Len.DFL_IMPB_IS_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_IS_DFZ = 1;
        public static final int DFL_IMPB_IS_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_IS_DFZ = 8;
        public static final int DFL_IMPB_IS_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_IS_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_IS_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
