package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: WS-OGGETTO-IN-PTF<br>
 * Variable: WS-OGGETTO-IN-PTF from program LCCS0070<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsOggettoInPtf {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWsOggettoInPtf(char wsOggettoInPtf) {
        this.value = wsOggettoInPtf;
    }

    public char getWsOggettoInPtf() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
