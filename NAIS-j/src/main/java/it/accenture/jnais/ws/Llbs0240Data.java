package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.BilaTrchEstr;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LLBS0240<br>
 * Generated as a class for rule WS.<br>*/
public class Llbs0240Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LLBS0240";
    //Original name: WK-TABELLA
    private String wkTabella = "";
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: AREA-IO-LCCS0090
    private LinkArea areaIoLccs0090 = new LinkArea();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: BILA-TRCH-ESTR
    private BilaTrchEstr bilaTrchEstr = new BilaTrchEstr();
    //Original name: IX-TAB-B03
    private short ixTabB03 = ((short)0);
    //Original name: IX-WCOM
    private short ixWcom = ((short)0);

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public void setIxTabB03(short ixTabB03) {
        this.ixTabB03 = ixTabB03;
    }

    public short getIxTabB03() {
        return this.ixTabB03;
    }

    public void setIxWcom(short ixWcom) {
        this.ixWcom = ixWcom;
    }

    public short getIxWcom() {
        return this.ixWcom;
    }

    public LinkArea getAreaIoLccs0090() {
        return areaIoLccs0090;
    }

    public BilaTrchEstr getBilaTrchEstr() {
        return bilaTrchEstr;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TABELLA = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
