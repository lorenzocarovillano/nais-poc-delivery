package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: MSG-OPERAZIONI<br>
 * Variable: MSG-OPERAZIONI from copybook IABCSQ99<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MsgOperazioni {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.OPERAZIONI);
    public static final String OPEN = "OPEN";
    public static final String READ = "READ";
    public static final String WRITE = "WRITE";
    public static final String REWRITE = "REWRITE";
    public static final String CLOSE = "CLOSE";

    //==== METHODS ====
    public void setOperazioni(String operazioni) {
        this.value = Functions.subString(operazioni, Len.OPERAZIONI);
    }

    public String getOperazioni() {
        return this.value;
    }

    public void setOpen() {
        value = OPEN;
    }

    public void setRead() {
        value = READ;
    }

    public void setMsgWrite() {
        value = WRITE;
    }

    public void setMsgRewrite() {
        value = REWRITE;
    }

    public void setClose() {
        value = CLOSE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OPERAZIONI = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
