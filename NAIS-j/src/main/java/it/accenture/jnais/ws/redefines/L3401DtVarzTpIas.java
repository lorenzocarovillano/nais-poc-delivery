package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-DT-VARZ-TP-IAS<br>
 * Variable: L3401-DT-VARZ-TP-IAS from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401DtVarzTpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401DtVarzTpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_DT_VARZ_TP_IAS;
    }

    public void setL3401DtVarzTpIas(int l3401DtVarzTpIas) {
        writeIntAsPacked(Pos.L3401_DT_VARZ_TP_IAS, l3401DtVarzTpIas, Len.Int.L3401_DT_VARZ_TP_IAS);
    }

    public void setL3401DtVarzTpIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_DT_VARZ_TP_IAS, Pos.L3401_DT_VARZ_TP_IAS);
    }

    /**Original name: L3401-DT-VARZ-TP-IAS<br>*/
    public int getL3401DtVarzTpIas() {
        return readPackedAsInt(Pos.L3401_DT_VARZ_TP_IAS, Len.Int.L3401_DT_VARZ_TP_IAS);
    }

    public byte[] getL3401DtVarzTpIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_DT_VARZ_TP_IAS, Pos.L3401_DT_VARZ_TP_IAS);
        return buffer;
    }

    /**Original name: L3401-DT-VARZ-TP-IAS-NULL<br>*/
    public String getL3401DtVarzTpIasNull() {
        return readString(Pos.L3401_DT_VARZ_TP_IAS_NULL, Len.L3401_DT_VARZ_TP_IAS_NULL);
    }

    public String getL3401DtVarzTpIasNullFormatted() {
        return Functions.padBlanks(getL3401DtVarzTpIasNull(), Len.L3401_DT_VARZ_TP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_DT_VARZ_TP_IAS = 1;
        public static final int L3401_DT_VARZ_TP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_DT_VARZ_TP_IAS = 5;
        public static final int L3401_DT_VARZ_TP_IAS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_DT_VARZ_TP_IAS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
