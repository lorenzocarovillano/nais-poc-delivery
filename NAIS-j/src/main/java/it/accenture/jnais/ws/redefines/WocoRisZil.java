package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-RIS-ZIL<br>
 * Variable: WOCO-RIS-ZIL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoRisZil extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoRisZil() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_RIS_ZIL;
    }

    public void setWocoRisZil(AfDecimal wocoRisZil) {
        writeDecimalAsPacked(Pos.WOCO_RIS_ZIL, wocoRisZil.copy());
    }

    public void setWocoRisZilFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_RIS_ZIL, Pos.WOCO_RIS_ZIL);
    }

    /**Original name: WOCO-RIS-ZIL<br>*/
    public AfDecimal getWocoRisZil() {
        return readPackedAsDecimal(Pos.WOCO_RIS_ZIL, Len.Int.WOCO_RIS_ZIL, Len.Fract.WOCO_RIS_ZIL);
    }

    public byte[] getWocoRisZilAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_RIS_ZIL, Pos.WOCO_RIS_ZIL);
        return buffer;
    }

    public void initWocoRisZilSpaces() {
        fill(Pos.WOCO_RIS_ZIL, Len.WOCO_RIS_ZIL, Types.SPACE_CHAR);
    }

    public void setWocoRisZilNull(String wocoRisZilNull) {
        writeString(Pos.WOCO_RIS_ZIL_NULL, wocoRisZilNull, Len.WOCO_RIS_ZIL_NULL);
    }

    /**Original name: WOCO-RIS-ZIL-NULL<br>*/
    public String getWocoRisZilNull() {
        return readString(Pos.WOCO_RIS_ZIL_NULL, Len.WOCO_RIS_ZIL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_RIS_ZIL = 1;
        public static final int WOCO_RIS_ZIL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_RIS_ZIL = 8;
        public static final int WOCO_RIS_ZIL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_RIS_ZIL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_RIS_ZIL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
