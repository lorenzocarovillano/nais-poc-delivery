package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-OUTRIVA<br>
 * Variable: WK-OUTRIVA from program LOAS0820<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkOutriva {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char OPEN = 'S';
    public static final char CLOSE = 'N';

    //==== METHODS ====
    public void setWkOutriva(char wkOutriva) {
        this.value = wkOutriva;
    }

    public char getWkOutriva() {
        return this.value;
    }

    public boolean isOpen() {
        return value == OPEN;
    }

    public void setOpen() {
        value = OPEN;
    }
}
