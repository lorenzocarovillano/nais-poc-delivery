package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-SOPR-PROF<br>
 * Variable: TGA-IMP-SOPR-PROF from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_SOPR_PROF;
    }

    public void setTgaImpSoprProf(AfDecimal tgaImpSoprProf) {
        writeDecimalAsPacked(Pos.TGA_IMP_SOPR_PROF, tgaImpSoprProf.copy());
    }

    public void setTgaImpSoprProfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_SOPR_PROF, Pos.TGA_IMP_SOPR_PROF);
    }

    /**Original name: TGA-IMP-SOPR-PROF<br>*/
    public AfDecimal getTgaImpSoprProf() {
        return readPackedAsDecimal(Pos.TGA_IMP_SOPR_PROF, Len.Int.TGA_IMP_SOPR_PROF, Len.Fract.TGA_IMP_SOPR_PROF);
    }

    public byte[] getTgaImpSoprProfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_SOPR_PROF, Pos.TGA_IMP_SOPR_PROF);
        return buffer;
    }

    public void setTgaImpSoprProfNull(String tgaImpSoprProfNull) {
        writeString(Pos.TGA_IMP_SOPR_PROF_NULL, tgaImpSoprProfNull, Len.TGA_IMP_SOPR_PROF_NULL);
    }

    /**Original name: TGA-IMP-SOPR-PROF-NULL<br>*/
    public String getTgaImpSoprProfNull() {
        return readString(Pos.TGA_IMP_SOPR_PROF_NULL, Len.TGA_IMP_SOPR_PROF_NULL);
    }

    public String getTgaImpSoprProfNullFormatted() {
        return Functions.padBlanks(getTgaImpSoprProfNull(), Len.TGA_IMP_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_SOPR_PROF = 1;
        public static final int TGA_IMP_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_SOPR_PROF = 8;
        public static final int TGA_IMP_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
