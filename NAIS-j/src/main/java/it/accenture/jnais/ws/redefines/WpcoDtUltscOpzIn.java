package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTSC-OPZ-IN<br>
 * Variable: WPCO-DT-ULTSC-OPZ-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltscOpzIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltscOpzIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTSC_OPZ_IN;
    }

    public void setWpcoDtUltscOpzIn(int wpcoDtUltscOpzIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULTSC_OPZ_IN, wpcoDtUltscOpzIn, Len.Int.WPCO_DT_ULTSC_OPZ_IN);
    }

    public void setDpcoDtUltscOpzInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTSC_OPZ_IN, Pos.WPCO_DT_ULTSC_OPZ_IN);
    }

    /**Original name: WPCO-DT-ULTSC-OPZ-IN<br>*/
    public int getWpcoDtUltscOpzIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULTSC_OPZ_IN, Len.Int.WPCO_DT_ULTSC_OPZ_IN);
    }

    public byte[] getWpcoDtUltscOpzInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTSC_OPZ_IN, Pos.WPCO_DT_ULTSC_OPZ_IN);
        return buffer;
    }

    public void setWpcoDtUltscOpzInNull(String wpcoDtUltscOpzInNull) {
        writeString(Pos.WPCO_DT_ULTSC_OPZ_IN_NULL, wpcoDtUltscOpzInNull, Len.WPCO_DT_ULTSC_OPZ_IN_NULL);
    }

    /**Original name: WPCO-DT-ULTSC-OPZ-IN-NULL<br>*/
    public String getWpcoDtUltscOpzInNull() {
        return readString(Pos.WPCO_DT_ULTSC_OPZ_IN_NULL, Len.WPCO_DT_ULTSC_OPZ_IN_NULL);
    }

    public String getWpcoDtUltscOpzInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltscOpzInNull(), Len.WPCO_DT_ULTSC_OPZ_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTSC_OPZ_IN = 1;
        public static final int WPCO_DT_ULTSC_OPZ_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTSC_OPZ_IN = 5;
        public static final int WPCO_DT_ULTSC_OPZ_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTSC_OPZ_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
