package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndTrchDiGar;
import it.accenture.jnais.copy.Ldbv3021;
import it.accenture.jnais.copy.TrchDiGarDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS3020<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs3020Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-TRCH-DI-GAR
    private IndTrchDiGar indTrchDiGar = new IndTrchDiGar();
    //Original name: TRCH-DI-GAR-DB
    private TrchDiGarDb trchDiGarDb = new TrchDiGarDb();
    //Original name: LDBV3021
    private Ldbv3021 ldbv3021 = new Ldbv3021();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndTrchDiGar getIndTrchDiGar() {
        return indTrchDiGar;
    }

    public Ldbv3021 getLdbv3021() {
        return ldbv3021;
    }

    public TrchDiGarDb getTrchDiGarDb() {
        return trchDiGarDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
