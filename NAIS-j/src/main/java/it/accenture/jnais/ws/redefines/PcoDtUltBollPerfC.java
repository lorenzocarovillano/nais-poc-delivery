package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-PERF-C<br>
 * Variable: PCO-DT-ULT-BOLL-PERF-C from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollPerfC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollPerfC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_PERF_C;
    }

    public void setPcoDtUltBollPerfC(int pcoDtUltBollPerfC) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_PERF_C, pcoDtUltBollPerfC, Len.Int.PCO_DT_ULT_BOLL_PERF_C);
    }

    public void setPcoDtUltBollPerfCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_PERF_C, Pos.PCO_DT_ULT_BOLL_PERF_C);
    }

    /**Original name: PCO-DT-ULT-BOLL-PERF-C<br>*/
    public int getPcoDtUltBollPerfC() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_PERF_C, Len.Int.PCO_DT_ULT_BOLL_PERF_C);
    }

    public byte[] getPcoDtUltBollPerfCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_PERF_C, Pos.PCO_DT_ULT_BOLL_PERF_C);
        return buffer;
    }

    public void initPcoDtUltBollPerfCHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_PERF_C, Len.PCO_DT_ULT_BOLL_PERF_C, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollPerfCNull(String pcoDtUltBollPerfCNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_PERF_C_NULL, pcoDtUltBollPerfCNull, Len.PCO_DT_ULT_BOLL_PERF_C_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-PERF-C-NULL<br>*/
    public String getPcoDtUltBollPerfCNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_PERF_C_NULL, Len.PCO_DT_ULT_BOLL_PERF_C_NULL);
    }

    public String getPcoDtUltBollPerfCNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollPerfCNull(), Len.PCO_DT_ULT_BOLL_PERF_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_PERF_C = 1;
        public static final int PCO_DT_ULT_BOLL_PERF_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_PERF_C = 5;
        public static final int PCO_DT_ULT_BOLL_PERF_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_PERF_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
