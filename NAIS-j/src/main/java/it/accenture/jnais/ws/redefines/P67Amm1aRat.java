package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-AMM-1A-RAT<br>
 * Variable: P67-AMM-1A-RAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67Amm1aRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67Amm1aRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_AMM1A_RAT;
    }

    public void setP67Amm1aRat(AfDecimal p67Amm1aRat) {
        writeDecimalAsPacked(Pos.P67_AMM1A_RAT, p67Amm1aRat.copy());
    }

    public void setP67Amm1aRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_AMM1A_RAT, Pos.P67_AMM1A_RAT);
    }

    /**Original name: P67-AMM-1A-RAT<br>*/
    public AfDecimal getP67Amm1aRat() {
        return readPackedAsDecimal(Pos.P67_AMM1A_RAT, Len.Int.P67_AMM1A_RAT, Len.Fract.P67_AMM1A_RAT);
    }

    public byte[] getP67Amm1aRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_AMM1A_RAT, Pos.P67_AMM1A_RAT);
        return buffer;
    }

    public void setP67Amm1aRatNull(String p67Amm1aRatNull) {
        writeString(Pos.P67_AMM1A_RAT_NULL, p67Amm1aRatNull, Len.P67_AMM1A_RAT_NULL);
    }

    /**Original name: P67-AMM-1A-RAT-NULL<br>*/
    public String getP67Amm1aRatNull() {
        return readString(Pos.P67_AMM1A_RAT_NULL, Len.P67_AMM1A_RAT_NULL);
    }

    public String getP67Amm1aRatNullFormatted() {
        return Functions.padBlanks(getP67Amm1aRatNull(), Len.P67_AMM1A_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_AMM1A_RAT = 1;
        public static final int P67_AMM1A_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_AMM1A_RAT = 8;
        public static final int P67_AMM1A_RAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_AMM1A_RAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_AMM1A_RAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
