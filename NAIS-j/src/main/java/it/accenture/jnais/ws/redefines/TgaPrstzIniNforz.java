package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRSTZ-INI-NFORZ<br>
 * Variable: TGA-PRSTZ-INI-NFORZ from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPrstzIniNforz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPrstzIniNforz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRSTZ_INI_NFORZ;
    }

    public void setTgaPrstzIniNforz(AfDecimal tgaPrstzIniNforz) {
        writeDecimalAsPacked(Pos.TGA_PRSTZ_INI_NFORZ, tgaPrstzIniNforz.copy());
    }

    public void setTgaPrstzIniNforzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRSTZ_INI_NFORZ, Pos.TGA_PRSTZ_INI_NFORZ);
    }

    /**Original name: TGA-PRSTZ-INI-NFORZ<br>*/
    public AfDecimal getTgaPrstzIniNforz() {
        return readPackedAsDecimal(Pos.TGA_PRSTZ_INI_NFORZ, Len.Int.TGA_PRSTZ_INI_NFORZ, Len.Fract.TGA_PRSTZ_INI_NFORZ);
    }

    public byte[] getTgaPrstzIniNforzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRSTZ_INI_NFORZ, Pos.TGA_PRSTZ_INI_NFORZ);
        return buffer;
    }

    public void setTgaPrstzIniNforzNull(String tgaPrstzIniNforzNull) {
        writeString(Pos.TGA_PRSTZ_INI_NFORZ_NULL, tgaPrstzIniNforzNull, Len.TGA_PRSTZ_INI_NFORZ_NULL);
    }

    /**Original name: TGA-PRSTZ-INI-NFORZ-NULL<br>*/
    public String getTgaPrstzIniNforzNull() {
        return readString(Pos.TGA_PRSTZ_INI_NFORZ_NULL, Len.TGA_PRSTZ_INI_NFORZ_NULL);
    }

    public String getTgaPrstzIniNforzNullFormatted() {
        return Functions.padBlanks(getTgaPrstzIniNforzNull(), Len.TGA_PRSTZ_INI_NFORZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_INI_NFORZ = 1;
        public static final int TGA_PRSTZ_INI_NFORZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_INI_NFORZ = 8;
        public static final int TGA_PRSTZ_INI_NFORZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_INI_NFORZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_INI_NFORZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
