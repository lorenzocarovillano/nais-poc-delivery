package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRSTZ-ULT<br>
 * Variable: WTGA-PRSTZ-ULT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPrstzUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPrstzUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRSTZ_ULT;
    }

    public void setWtgaPrstzUlt(AfDecimal wtgaPrstzUlt) {
        writeDecimalAsPacked(Pos.WTGA_PRSTZ_ULT, wtgaPrstzUlt.copy());
    }

    public void setWtgaPrstzUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRSTZ_ULT, Pos.WTGA_PRSTZ_ULT);
    }

    /**Original name: WTGA-PRSTZ-ULT<br>*/
    public AfDecimal getWtgaPrstzUlt() {
        return readPackedAsDecimal(Pos.WTGA_PRSTZ_ULT, Len.Int.WTGA_PRSTZ_ULT, Len.Fract.WTGA_PRSTZ_ULT);
    }

    public byte[] getWtgaPrstzUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRSTZ_ULT, Pos.WTGA_PRSTZ_ULT);
        return buffer;
    }

    public void initWtgaPrstzUltSpaces() {
        fill(Pos.WTGA_PRSTZ_ULT, Len.WTGA_PRSTZ_ULT, Types.SPACE_CHAR);
    }

    public void setWtgaPrstzUltNull(String wtgaPrstzUltNull) {
        writeString(Pos.WTGA_PRSTZ_ULT_NULL, wtgaPrstzUltNull, Len.WTGA_PRSTZ_ULT_NULL);
    }

    /**Original name: WTGA-PRSTZ-ULT-NULL<br>*/
    public String getWtgaPrstzUltNull() {
        return readString(Pos.WTGA_PRSTZ_ULT_NULL, Len.WTGA_PRSTZ_ULT_NULL);
    }

    public String getWtgaPrstzUltNullFormatted() {
        return Functions.padBlanks(getWtgaPrstzUltNull(), Len.WTGA_PRSTZ_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_ULT = 1;
        public static final int WTGA_PRSTZ_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_ULT = 8;
        public static final int WTGA_PRSTZ_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
