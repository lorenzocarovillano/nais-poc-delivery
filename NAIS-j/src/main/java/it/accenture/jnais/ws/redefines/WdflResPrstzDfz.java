package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RES-PRSTZ-DFZ<br>
 * Variable: WDFL-RES-PRSTZ-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflResPrstzDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflResPrstzDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RES_PRSTZ_DFZ;
    }

    public void setWdflResPrstzDfz(AfDecimal wdflResPrstzDfz) {
        writeDecimalAsPacked(Pos.WDFL_RES_PRSTZ_DFZ, wdflResPrstzDfz.copy());
    }

    public void setWdflResPrstzDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RES_PRSTZ_DFZ, Pos.WDFL_RES_PRSTZ_DFZ);
    }

    /**Original name: WDFL-RES-PRSTZ-DFZ<br>*/
    public AfDecimal getWdflResPrstzDfz() {
        return readPackedAsDecimal(Pos.WDFL_RES_PRSTZ_DFZ, Len.Int.WDFL_RES_PRSTZ_DFZ, Len.Fract.WDFL_RES_PRSTZ_DFZ);
    }

    public byte[] getWdflResPrstzDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RES_PRSTZ_DFZ, Pos.WDFL_RES_PRSTZ_DFZ);
        return buffer;
    }

    public void setWdflResPrstzDfzNull(String wdflResPrstzDfzNull) {
        writeString(Pos.WDFL_RES_PRSTZ_DFZ_NULL, wdflResPrstzDfzNull, Len.WDFL_RES_PRSTZ_DFZ_NULL);
    }

    /**Original name: WDFL-RES-PRSTZ-DFZ-NULL<br>*/
    public String getWdflResPrstzDfzNull() {
        return readString(Pos.WDFL_RES_PRSTZ_DFZ_NULL, Len.WDFL_RES_PRSTZ_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRSTZ_DFZ = 1;
        public static final int WDFL_RES_PRSTZ_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRSTZ_DFZ = 8;
        public static final int WDFL_RES_PRSTZ_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRSTZ_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRSTZ_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
