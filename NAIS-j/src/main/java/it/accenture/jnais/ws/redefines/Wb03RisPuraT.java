package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-RIS-PURA-T<br>
 * Variable: WB03-RIS-PURA-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03RisPuraT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03RisPuraT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_RIS_PURA_T;
    }

    public void setWb03RisPuraT(AfDecimal wb03RisPuraT) {
        writeDecimalAsPacked(Pos.WB03_RIS_PURA_T, wb03RisPuraT.copy());
    }

    public void setWb03RisPuraTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_RIS_PURA_T, Pos.WB03_RIS_PURA_T);
    }

    /**Original name: WB03-RIS-PURA-T<br>*/
    public AfDecimal getWb03RisPuraT() {
        return readPackedAsDecimal(Pos.WB03_RIS_PURA_T, Len.Int.WB03_RIS_PURA_T, Len.Fract.WB03_RIS_PURA_T);
    }

    public byte[] getWb03RisPuraTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_RIS_PURA_T, Pos.WB03_RIS_PURA_T);
        return buffer;
    }

    public void setWb03RisPuraTNull(String wb03RisPuraTNull) {
        writeString(Pos.WB03_RIS_PURA_T_NULL, wb03RisPuraTNull, Len.WB03_RIS_PURA_T_NULL);
    }

    /**Original name: WB03-RIS-PURA-T-NULL<br>*/
    public String getWb03RisPuraTNull() {
        return readString(Pos.WB03_RIS_PURA_T_NULL, Len.WB03_RIS_PURA_T_NULL);
    }

    public String getWb03RisPuraTNullFormatted() {
        return Functions.padBlanks(getWb03RisPuraTNull(), Len.WB03_RIS_PURA_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_RIS_PURA_T = 1;
        public static final int WB03_RIS_PURA_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_RIS_PURA_T = 8;
        public static final int WB03_RIS_PURA_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_RIS_PURA_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_RIS_PURA_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
