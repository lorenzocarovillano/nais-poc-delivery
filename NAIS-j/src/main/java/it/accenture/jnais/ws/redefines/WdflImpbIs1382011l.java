package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-IS-1382011L<br>
 * Variable: WDFL-IMPB-IS-1382011L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbIs1382011l extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbIs1382011l() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_IS1382011L;
    }

    public void setWdflImpbIs1382011l(AfDecimal wdflImpbIs1382011l) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_IS1382011L, wdflImpbIs1382011l.copy());
    }

    public void setWdflImpbIs1382011lFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_IS1382011L, Pos.WDFL_IMPB_IS1382011L);
    }

    /**Original name: WDFL-IMPB-IS-1382011L<br>*/
    public AfDecimal getWdflImpbIs1382011l() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_IS1382011L, Len.Int.WDFL_IMPB_IS1382011L, Len.Fract.WDFL_IMPB_IS1382011L);
    }

    public byte[] getWdflImpbIs1382011lAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_IS1382011L, Pos.WDFL_IMPB_IS1382011L);
        return buffer;
    }

    public void setWdflImpbIs1382011lNull(String wdflImpbIs1382011lNull) {
        writeString(Pos.WDFL_IMPB_IS1382011L_NULL, wdflImpbIs1382011lNull, Len.WDFL_IMPB_IS1382011L_NULL);
    }

    /**Original name: WDFL-IMPB-IS-1382011L-NULL<br>*/
    public String getWdflImpbIs1382011lNull() {
        return readString(Pos.WDFL_IMPB_IS1382011L_NULL, Len.WDFL_IMPB_IS1382011L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS1382011L = 1;
        public static final int WDFL_IMPB_IS1382011L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS1382011L = 8;
        public static final int WDFL_IMPB_IS1382011L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS1382011L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS1382011L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
