package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0033-MODALITA-SERVIZIO<br>
 * Variable: LCCC0033-MODALITA-SERVIZIO from copybook LCCC0033<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0033ModalitaServizio {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.MODALITA_SERVIZIO);
    public static final String VERIFICA_REC_PROVV = "01";
    public static final String ESTR_POL_REC_PROVV = "02";

    //==== METHODS ====
    public void setModalitaServizio(String modalitaServizio) {
        this.value = Functions.subString(modalitaServizio, Len.MODALITA_SERVIZIO);
    }

    public String getModalitaServizio() {
        return this.value;
    }

    public boolean isVerificaRecProvv() {
        return value.equals(VERIFICA_REC_PROVV);
    }

    public boolean isEstrPolRecProvv() {
        return value.equals(ESTR_POL_REC_PROVV);
    }

    public void setLccc0033EstrPolRecProvv() {
        value = ESTR_POL_REC_PROVV;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MODALITA_SERVIZIO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
