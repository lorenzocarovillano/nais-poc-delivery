package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-DT-END-COP<br>
 * Variable: WDTR-DT-END-COP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrDtEndCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrDtEndCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_DT_END_COP;
    }

    public void setWdtrDtEndCop(int wdtrDtEndCop) {
        writeIntAsPacked(Pos.WDTR_DT_END_COP, wdtrDtEndCop, Len.Int.WDTR_DT_END_COP);
    }

    /**Original name: WDTR-DT-END-COP<br>*/
    public int getWdtrDtEndCop() {
        return readPackedAsInt(Pos.WDTR_DT_END_COP, Len.Int.WDTR_DT_END_COP);
    }

    public void setWdtrDtEndCopNull(String wdtrDtEndCopNull) {
        writeString(Pos.WDTR_DT_END_COP_NULL, wdtrDtEndCopNull, Len.WDTR_DT_END_COP_NULL);
    }

    /**Original name: WDTR-DT-END-COP-NULL<br>*/
    public String getWdtrDtEndCopNull() {
        return readString(Pos.WDTR_DT_END_COP_NULL, Len.WDTR_DT_END_COP_NULL);
    }

    public String getWdtrDtEndCopNullFormatted() {
        return Functions.padBlanks(getWdtrDtEndCopNull(), Len.WDTR_DT_END_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_DT_END_COP = 1;
        public static final int WDTR_DT_END_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_DT_END_COP = 5;
        public static final int WDTR_DT_END_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_DT_END_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
