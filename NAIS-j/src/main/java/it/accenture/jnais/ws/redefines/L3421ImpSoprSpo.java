package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-SOPR-SPO<br>
 * Variable: L3421-IMP-SOPR-SPO from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_SOPR_SPO;
    }

    public void setL3421ImpSoprSpoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_SOPR_SPO, Pos.L3421_IMP_SOPR_SPO);
    }

    /**Original name: L3421-IMP-SOPR-SPO<br>*/
    public AfDecimal getL3421ImpSoprSpo() {
        return readPackedAsDecimal(Pos.L3421_IMP_SOPR_SPO, Len.Int.L3421_IMP_SOPR_SPO, Len.Fract.L3421_IMP_SOPR_SPO);
    }

    public byte[] getL3421ImpSoprSpoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_SOPR_SPO, Pos.L3421_IMP_SOPR_SPO);
        return buffer;
    }

    /**Original name: L3421-IMP-SOPR-SPO-NULL<br>*/
    public String getL3421ImpSoprSpoNull() {
        return readString(Pos.L3421_IMP_SOPR_SPO_NULL, Len.L3421_IMP_SOPR_SPO_NULL);
    }

    public String getL3421ImpSoprSpoNullFormatted() {
        return Functions.padBlanks(getL3421ImpSoprSpoNull(), Len.L3421_IMP_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_SOPR_SPO = 1;
        public static final int L3421_IMP_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_SOPR_SPO = 8;
        public static final int L3421_IMP_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
