package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WK-APPO-DT<br>
 * Variable: WK-APPO-DT from program LOAS0670<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkAppoDtLoas0670 {

    //==== PROPERTIES ====
    //Original name: WK-APPO-DT-AA
    private String aa = "0000";
    //Original name: WK-APPO-DT-MM
    private String mm = "00";
    //Original name: WK-APPO-DT-GG
    private String gg = "00";

    //==== METHODS ====
    public void setWkAppoDtFormatted(String data) {
        byte[] buffer = new byte[Len.WK_APPO_DT];
        MarshalByte.writeString(buffer, 1, data, Len.WK_APPO_DT);
        setWkAppoDtBytes(buffer, 1);
    }

    public void setWkAppoDtBytes(byte[] buffer, int offset) {
        int position = offset;
        aa = MarshalByte.readFixedString(buffer, position, Len.AA);
        position += Len.AA;
        mm = MarshalByte.readFixedString(buffer, position, Len.MM);
        position += Len.MM;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
    }

    public String getMmFormatted() {
        return this.mm;
    }

    public String getGgFormatted() {
        return this.gg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WK_APPO_DT = AA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
