package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccc0024DatiInput;
import it.accenture.jnais.ws.enums.Lccc0024VerificaBlocco;

/**Original name: AREA-IO-LCCS0024<br>
 * Variable: AREA-IO-LCCS0024 from program LCCS0024<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoLccs0024 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LCCC0024-DATI-INPUT
    private Lccc0024DatiInput datiInput = new Lccc0024DatiInput();
    //Original name: LCCC0024-ID-OGG-BLOCCO
    private int idOggBlocco = DefaultValues.INT_VAL;
    //Original name: LCCC0024-VERIFICA-BLOCCO
    private Lccc0024VerificaBlocco verificaBlocco = new Lccc0024VerificaBlocco();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IO_LCCS0024;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIoLccs0024Bytes(buf);
    }

    public String getAreaIoLccs0024Formatted() {
        return MarshalByteExt.bufferToStr(getAreaIoLccs0024Bytes());
    }

    public void setAreaIoLccs0024Bytes(byte[] buffer) {
        setAreaIoLccs0024Bytes(buffer, 1);
    }

    public byte[] getAreaIoLccs0024Bytes() {
        byte[] buffer = new byte[Len.AREA_IO_LCCS0024];
        return getAreaIoLccs0024Bytes(buffer, 1);
    }

    public void setAreaIoLccs0024Bytes(byte[] buffer, int offset) {
        int position = offset;
        datiInput.setDatiInputBytes(buffer, position);
        position += Lccc0024DatiInput.Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
    }

    public byte[] getAreaIoLccs0024Bytes(byte[] buffer, int offset) {
        int position = offset;
        datiInput.getDatiInputBytes(buffer, position);
        position += Lccc0024DatiInput.Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        idOggBlocco = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG_BLOCCO, 0);
        position += Len.ID_OGG_BLOCCO;
        verificaBlocco.setVerificaBlocco(MarshalByte.readChar(buffer, position));
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOggBlocco, Len.Int.ID_OGG_BLOCCO, 0);
        position += Len.ID_OGG_BLOCCO;
        MarshalByte.writeChar(buffer, position, verificaBlocco.getVerificaBlocco());
        return buffer;
    }

    public void setIdOggBlocco(int idOggBlocco) {
        this.idOggBlocco = idOggBlocco;
    }

    public int getIdOggBlocco() {
        return this.idOggBlocco;
    }

    public Lccc0024DatiInput getDatiInput() {
        return datiInput;
    }

    public Lccc0024VerificaBlocco getVerificaBlocco() {
        return verificaBlocco;
    }

    @Override
    public byte[] serialize() {
        return getAreaIoLccs0024Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG_BLOCCO = 5;
        public static final int DATI_OUTPUT = ID_OGG_BLOCCO + Lccc0024VerificaBlocco.Len.VERIFICA_BLOCCO;
        public static final int AREA_IO_LCCS0024 = Lccc0024DatiInput.Len.DATI_INPUT + DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG_BLOCCO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
