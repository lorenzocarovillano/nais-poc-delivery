package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-DT-EMIS<br>
 * Variable: TGA-DT-EMIS from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaDtEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaDtEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_DT_EMIS;
    }

    public void setTgaDtEmis(int tgaDtEmis) {
        writeIntAsPacked(Pos.TGA_DT_EMIS, tgaDtEmis, Len.Int.TGA_DT_EMIS);
    }

    public void setTgaDtEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_DT_EMIS, Pos.TGA_DT_EMIS);
    }

    /**Original name: TGA-DT-EMIS<br>*/
    public int getTgaDtEmis() {
        return readPackedAsInt(Pos.TGA_DT_EMIS, Len.Int.TGA_DT_EMIS);
    }

    public byte[] getTgaDtEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_DT_EMIS, Pos.TGA_DT_EMIS);
        return buffer;
    }

    public void setTgaDtEmisNull(String tgaDtEmisNull) {
        writeString(Pos.TGA_DT_EMIS_NULL, tgaDtEmisNull, Len.TGA_DT_EMIS_NULL);
    }

    /**Original name: TGA-DT-EMIS-NULL<br>*/
    public String getTgaDtEmisNull() {
        return readString(Pos.TGA_DT_EMIS_NULL, Len.TGA_DT_EMIS_NULL);
    }

    public String getTgaDtEmisNullFormatted() {
        return Functions.padBlanks(getTgaDtEmisNull(), Len.TGA_DT_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_DT_EMIS = 1;
        public static final int TGA_DT_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_DT_EMIS = 5;
        public static final int TGA_DT_EMIS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_DT_EMIS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
