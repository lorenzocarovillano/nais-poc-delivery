package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.EstTrchDiGar;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndAnagDato;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS7120<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs7120Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-EST-TRCH-DI-GAR
    private IndAnagDato indEstTrchDiGar = new IndAnagDato();
    //Original name: EST-TRCH-DI-GAR
    private EstTrchDiGar estTrchDiGar = new EstTrchDiGar();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public EstTrchDiGar getEstTrchDiGar() {
        return estTrchDiGar;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndAnagDato getIndEstTrchDiGar() {
        return indEstTrchDiGar;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
