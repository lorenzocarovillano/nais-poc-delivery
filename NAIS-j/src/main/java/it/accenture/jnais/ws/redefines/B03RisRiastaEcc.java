package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-RIS-RIASTA-ECC<br>
 * Variable: B03-RIS-RIASTA-ECC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03RisRiastaEcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03RisRiastaEcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_RIS_RIASTA_ECC;
    }

    public void setB03RisRiastaEcc(AfDecimal b03RisRiastaEcc) {
        writeDecimalAsPacked(Pos.B03_RIS_RIASTA_ECC, b03RisRiastaEcc.copy());
    }

    public void setB03RisRiastaEccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_RIS_RIASTA_ECC, Pos.B03_RIS_RIASTA_ECC);
    }

    /**Original name: B03-RIS-RIASTA-ECC<br>*/
    public AfDecimal getB03RisRiastaEcc() {
        return readPackedAsDecimal(Pos.B03_RIS_RIASTA_ECC, Len.Int.B03_RIS_RIASTA_ECC, Len.Fract.B03_RIS_RIASTA_ECC);
    }

    public byte[] getB03RisRiastaEccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_RIS_RIASTA_ECC, Pos.B03_RIS_RIASTA_ECC);
        return buffer;
    }

    public void setB03RisRiastaEccNull(String b03RisRiastaEccNull) {
        writeString(Pos.B03_RIS_RIASTA_ECC_NULL, b03RisRiastaEccNull, Len.B03_RIS_RIASTA_ECC_NULL);
    }

    /**Original name: B03-RIS-RIASTA-ECC-NULL<br>*/
    public String getB03RisRiastaEccNull() {
        return readString(Pos.B03_RIS_RIASTA_ECC_NULL, Len.B03_RIS_RIASTA_ECC_NULL);
    }

    public String getB03RisRiastaEccNullFormatted() {
        return Functions.padBlanks(getB03RisRiastaEccNull(), Len.B03_RIS_RIASTA_ECC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_RIS_RIASTA_ECC = 1;
        public static final int B03_RIS_RIASTA_ECC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_RIS_RIASTA_ECC = 8;
        public static final int B03_RIS_RIASTA_ECC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_RIS_RIASTA_ECC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_RIS_RIASTA_ECC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
