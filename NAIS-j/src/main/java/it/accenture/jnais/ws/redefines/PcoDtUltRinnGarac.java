package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-RINN-GARAC<br>
 * Variable: PCO-DT-ULT-RINN-GARAC from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltRinnGarac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltRinnGarac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_RINN_GARAC;
    }

    public void setPcoDtUltRinnGarac(int pcoDtUltRinnGarac) {
        writeIntAsPacked(Pos.PCO_DT_ULT_RINN_GARAC, pcoDtUltRinnGarac, Len.Int.PCO_DT_ULT_RINN_GARAC);
    }

    public void setPcoDtUltRinnGaracFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_RINN_GARAC, Pos.PCO_DT_ULT_RINN_GARAC);
    }

    /**Original name: PCO-DT-ULT-RINN-GARAC<br>*/
    public int getPcoDtUltRinnGarac() {
        return readPackedAsInt(Pos.PCO_DT_ULT_RINN_GARAC, Len.Int.PCO_DT_ULT_RINN_GARAC);
    }

    public byte[] getPcoDtUltRinnGaracAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_RINN_GARAC, Pos.PCO_DT_ULT_RINN_GARAC);
        return buffer;
    }

    public void initPcoDtUltRinnGaracHighValues() {
        fill(Pos.PCO_DT_ULT_RINN_GARAC, Len.PCO_DT_ULT_RINN_GARAC, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltRinnGaracNull(String pcoDtUltRinnGaracNull) {
        writeString(Pos.PCO_DT_ULT_RINN_GARAC_NULL, pcoDtUltRinnGaracNull, Len.PCO_DT_ULT_RINN_GARAC_NULL);
    }

    /**Original name: PCO-DT-ULT-RINN-GARAC-NULL<br>*/
    public String getPcoDtUltRinnGaracNull() {
        return readString(Pos.PCO_DT_ULT_RINN_GARAC_NULL, Len.PCO_DT_ULT_RINN_GARAC_NULL);
    }

    public String getPcoDtUltRinnGaracNullFormatted() {
        return Functions.padBlanks(getPcoDtUltRinnGaracNull(), Len.PCO_DT_ULT_RINN_GARAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RINN_GARAC = 1;
        public static final int PCO_DT_ULT_RINN_GARAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RINN_GARAC = 5;
        public static final int PCO_DT_ULT_RINN_GARAC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_RINN_GARAC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
