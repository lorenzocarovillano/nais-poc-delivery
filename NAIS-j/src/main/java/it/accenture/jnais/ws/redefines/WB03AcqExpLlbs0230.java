package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-ACQ-EXP<br>
 * Variable: W-B03-ACQ-EXP from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03AcqExpLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03AcqExpLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_ACQ_EXP;
    }

    public void setwB03AcqExp(AfDecimal wB03AcqExp) {
        writeDecimalAsPacked(Pos.W_B03_ACQ_EXP, wB03AcqExp.copy());
    }

    /**Original name: W-B03-ACQ-EXP<br>*/
    public AfDecimal getwB03AcqExp() {
        return readPackedAsDecimal(Pos.W_B03_ACQ_EXP, Len.Int.W_B03_ACQ_EXP, Len.Fract.W_B03_ACQ_EXP);
    }

    public byte[] getwB03AcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_ACQ_EXP, Pos.W_B03_ACQ_EXP);
        return buffer;
    }

    public void setwB03AcqExpNull(String wB03AcqExpNull) {
        writeString(Pos.W_B03_ACQ_EXP_NULL, wB03AcqExpNull, Len.W_B03_ACQ_EXP_NULL);
    }

    /**Original name: W-B03-ACQ-EXP-NULL<br>*/
    public String getwB03AcqExpNull() {
        return readString(Pos.W_B03_ACQ_EXP_NULL, Len.W_B03_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_ACQ_EXP = 1;
        public static final int W_B03_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_ACQ_EXP = 8;
        public static final int W_B03_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
