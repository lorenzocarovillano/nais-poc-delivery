package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-ID-MOVI-CHIU<br>
 * Variable: TIT-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_ID_MOVI_CHIU;
    }

    public void setTitIdMoviChiu(int titIdMoviChiu) {
        writeIntAsPacked(Pos.TIT_ID_MOVI_CHIU, titIdMoviChiu, Len.Int.TIT_ID_MOVI_CHIU);
    }

    public void setTitIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_ID_MOVI_CHIU, Pos.TIT_ID_MOVI_CHIU);
    }

    /**Original name: TIT-ID-MOVI-CHIU<br>*/
    public int getTitIdMoviChiu() {
        return readPackedAsInt(Pos.TIT_ID_MOVI_CHIU, Len.Int.TIT_ID_MOVI_CHIU);
    }

    public byte[] getTitIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_ID_MOVI_CHIU, Pos.TIT_ID_MOVI_CHIU);
        return buffer;
    }

    public void setTitIdMoviChiuNull(String titIdMoviChiuNull) {
        writeString(Pos.TIT_ID_MOVI_CHIU_NULL, titIdMoviChiuNull, Len.TIT_ID_MOVI_CHIU_NULL);
    }

    /**Original name: TIT-ID-MOVI-CHIU-NULL<br>*/
    public String getTitIdMoviChiuNull() {
        return readString(Pos.TIT_ID_MOVI_CHIU_NULL, Len.TIT_ID_MOVI_CHIU_NULL);
    }

    public String getTitIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getTitIdMoviChiuNull(), Len.TIT_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_ID_MOVI_CHIU = 1;
        public static final int TIT_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_ID_MOVI_CHIU = 5;
        public static final int TIT_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
