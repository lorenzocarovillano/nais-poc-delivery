package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTGZ-CED<br>
 * Variable: PCO-DT-ULTGZ-CED from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltgzCed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltgzCed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTGZ_CED;
    }

    public void setPcoDtUltgzCed(int pcoDtUltgzCed) {
        writeIntAsPacked(Pos.PCO_DT_ULTGZ_CED, pcoDtUltgzCed, Len.Int.PCO_DT_ULTGZ_CED);
    }

    public void setPcoDtUltgzCedFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTGZ_CED, Pos.PCO_DT_ULTGZ_CED);
    }

    /**Original name: PCO-DT-ULTGZ-CED<br>*/
    public int getPcoDtUltgzCed() {
        return readPackedAsInt(Pos.PCO_DT_ULTGZ_CED, Len.Int.PCO_DT_ULTGZ_CED);
    }

    public byte[] getPcoDtUltgzCedAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTGZ_CED, Pos.PCO_DT_ULTGZ_CED);
        return buffer;
    }

    public void initPcoDtUltgzCedHighValues() {
        fill(Pos.PCO_DT_ULTGZ_CED, Len.PCO_DT_ULTGZ_CED, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltgzCedNull(String pcoDtUltgzCedNull) {
        writeString(Pos.PCO_DT_ULTGZ_CED_NULL, pcoDtUltgzCedNull, Len.PCO_DT_ULTGZ_CED_NULL);
    }

    /**Original name: PCO-DT-ULTGZ-CED-NULL<br>*/
    public String getPcoDtUltgzCedNull() {
        return readString(Pos.PCO_DT_ULTGZ_CED_NULL, Len.PCO_DT_ULTGZ_CED_NULL);
    }

    public String getPcoDtUltgzCedNullFormatted() {
        return Functions.padBlanks(getPcoDtUltgzCedNull(), Len.PCO_DT_ULTGZ_CED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTGZ_CED = 1;
        public static final int PCO_DT_ULTGZ_CED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTGZ_CED = 5;
        public static final int PCO_DT_ULTGZ_CED_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTGZ_CED = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
