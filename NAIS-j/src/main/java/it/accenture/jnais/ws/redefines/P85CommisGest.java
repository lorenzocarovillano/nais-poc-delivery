package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P85-COMMIS-GEST<br>
 * Variable: P85-COMMIS-GEST from program IDBSP850<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P85CommisGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P85CommisGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P85_COMMIS_GEST;
    }

    public void setP85CommisGest(AfDecimal p85CommisGest) {
        writeDecimalAsPacked(Pos.P85_COMMIS_GEST, p85CommisGest.copy());
    }

    public void setP85CommisGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P85_COMMIS_GEST, Pos.P85_COMMIS_GEST);
    }

    /**Original name: P85-COMMIS-GEST<br>*/
    public AfDecimal getP85CommisGest() {
        return readPackedAsDecimal(Pos.P85_COMMIS_GEST, Len.Int.P85_COMMIS_GEST, Len.Fract.P85_COMMIS_GEST);
    }

    public byte[] getP85CommisGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P85_COMMIS_GEST, Pos.P85_COMMIS_GEST);
        return buffer;
    }

    public void setP85CommisGestNull(String p85CommisGestNull) {
        writeString(Pos.P85_COMMIS_GEST_NULL, p85CommisGestNull, Len.P85_COMMIS_GEST_NULL);
    }

    /**Original name: P85-COMMIS-GEST-NULL<br>*/
    public String getP85CommisGestNull() {
        return readString(Pos.P85_COMMIS_GEST_NULL, Len.P85_COMMIS_GEST_NULL);
    }

    public String getP85CommisGestNullFormatted() {
        return Functions.padBlanks(getP85CommisGestNull(), Len.P85_COMMIS_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P85_COMMIS_GEST = 1;
        public static final int P85_COMMIS_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P85_COMMIS_GEST = 8;
        public static final int P85_COMMIS_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P85_COMMIS_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P85_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
