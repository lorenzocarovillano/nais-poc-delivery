package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-SOST-DFZ<br>
 * Variable: DFL-IMPST-SOST-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstSostDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstSostDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_SOST_DFZ;
    }

    public void setDflImpstSostDfz(AfDecimal dflImpstSostDfz) {
        writeDecimalAsPacked(Pos.DFL_IMPST_SOST_DFZ, dflImpstSostDfz.copy());
    }

    public void setDflImpstSostDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_SOST_DFZ, Pos.DFL_IMPST_SOST_DFZ);
    }

    /**Original name: DFL-IMPST-SOST-DFZ<br>*/
    public AfDecimal getDflImpstSostDfz() {
        return readPackedAsDecimal(Pos.DFL_IMPST_SOST_DFZ, Len.Int.DFL_IMPST_SOST_DFZ, Len.Fract.DFL_IMPST_SOST_DFZ);
    }

    public byte[] getDflImpstSostDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_SOST_DFZ, Pos.DFL_IMPST_SOST_DFZ);
        return buffer;
    }

    public void setDflImpstSostDfzNull(String dflImpstSostDfzNull) {
        writeString(Pos.DFL_IMPST_SOST_DFZ_NULL, dflImpstSostDfzNull, Len.DFL_IMPST_SOST_DFZ_NULL);
    }

    /**Original name: DFL-IMPST-SOST-DFZ-NULL<br>*/
    public String getDflImpstSostDfzNull() {
        return readString(Pos.DFL_IMPST_SOST_DFZ_NULL, Len.DFL_IMPST_SOST_DFZ_NULL);
    }

    public String getDflImpstSostDfzNullFormatted() {
        return Functions.padBlanks(getDflImpstSostDfzNull(), Len.DFL_IMPST_SOST_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_SOST_DFZ = 1;
        public static final int DFL_IMPST_SOST_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_SOST_DFZ = 8;
        public static final int DFL_IMPST_SOST_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_SOST_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_SOST_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
