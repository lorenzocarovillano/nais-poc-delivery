package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-DIR<br>
 * Variable: W-B03-DIR from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03DirLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03DirLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DIR;
    }

    public void setwB03Dir(AfDecimal wB03Dir) {
        writeDecimalAsPacked(Pos.W_B03_DIR, wB03Dir.copy());
    }

    /**Original name: W-B03-DIR<br>*/
    public AfDecimal getwB03Dir() {
        return readPackedAsDecimal(Pos.W_B03_DIR, Len.Int.W_B03_DIR, Len.Fract.W_B03_DIR);
    }

    public byte[] getwB03DirAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DIR, Pos.W_B03_DIR);
        return buffer;
    }

    public void setwB03DirNull(String wB03DirNull) {
        writeString(Pos.W_B03_DIR_NULL, wB03DirNull, Len.W_B03_DIR_NULL);
    }

    /**Original name: W-B03-DIR-NULL<br>*/
    public String getwB03DirNull() {
        return readString(Pos.W_B03_DIR_NULL, Len.W_B03_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DIR = 1;
        public static final int W_B03_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DIR = 8;
        public static final int W_B03_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
