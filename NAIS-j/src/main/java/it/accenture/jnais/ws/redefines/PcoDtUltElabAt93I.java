package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-AT93-I<br>
 * Variable: PCO-DT-ULT-ELAB-AT93-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabAt93I extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabAt93I() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_AT93_I;
    }

    public void setPcoDtUltElabAt93I(int pcoDtUltElabAt93I) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_AT93_I, pcoDtUltElabAt93I, Len.Int.PCO_DT_ULT_ELAB_AT93_I);
    }

    public void setPcoDtUltElabAt93IFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_AT93_I, Pos.PCO_DT_ULT_ELAB_AT93_I);
    }

    /**Original name: PCO-DT-ULT-ELAB-AT93-I<br>*/
    public int getPcoDtUltElabAt93I() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_AT93_I, Len.Int.PCO_DT_ULT_ELAB_AT93_I);
    }

    public byte[] getPcoDtUltElabAt93IAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_AT93_I, Pos.PCO_DT_ULT_ELAB_AT93_I);
        return buffer;
    }

    public void initPcoDtUltElabAt93IHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_AT93_I, Len.PCO_DT_ULT_ELAB_AT93_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabAt93INull(String pcoDtUltElabAt93INull) {
        writeString(Pos.PCO_DT_ULT_ELAB_AT93_I_NULL, pcoDtUltElabAt93INull, Len.PCO_DT_ULT_ELAB_AT93_I_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-AT93-I-NULL<br>*/
    public String getPcoDtUltElabAt93INull() {
        return readString(Pos.PCO_DT_ULT_ELAB_AT93_I_NULL, Len.PCO_DT_ULT_ELAB_AT93_I_NULL);
    }

    public String getPcoDtUltElabAt93INullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabAt93INull(), Len.PCO_DT_ULT_ELAB_AT93_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_AT93_I = 1;
        public static final int PCO_DT_ULT_ELAB_AT93_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_AT93_I = 5;
        public static final int PCO_DT_ULT_ELAB_AT93_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_AT93_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
