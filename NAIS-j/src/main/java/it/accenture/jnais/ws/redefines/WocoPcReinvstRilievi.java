package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-PC-REINVST-RILIEVI<br>
 * Variable: WOCO-PC-REINVST-RILIEVI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoPcReinvstRilievi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoPcReinvstRilievi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_PC_REINVST_RILIEVI;
    }

    public void setWocoPcReinvstRilievi(AfDecimal wocoPcReinvstRilievi) {
        writeDecimalAsPacked(Pos.WOCO_PC_REINVST_RILIEVI, wocoPcReinvstRilievi.copy());
    }

    public void setWocoPcReinvstRilieviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_PC_REINVST_RILIEVI, Pos.WOCO_PC_REINVST_RILIEVI);
    }

    /**Original name: WOCO-PC-REINVST-RILIEVI<br>*/
    public AfDecimal getWocoPcReinvstRilievi() {
        return readPackedAsDecimal(Pos.WOCO_PC_REINVST_RILIEVI, Len.Int.WOCO_PC_REINVST_RILIEVI, Len.Fract.WOCO_PC_REINVST_RILIEVI);
    }

    public byte[] getWocoPcReinvstRilieviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_PC_REINVST_RILIEVI, Pos.WOCO_PC_REINVST_RILIEVI);
        return buffer;
    }

    public void initWocoPcReinvstRilieviSpaces() {
        fill(Pos.WOCO_PC_REINVST_RILIEVI, Len.WOCO_PC_REINVST_RILIEVI, Types.SPACE_CHAR);
    }

    public void setWocoPcReinvstRilieviNull(String wocoPcReinvstRilieviNull) {
        writeString(Pos.WOCO_PC_REINVST_RILIEVI_NULL, wocoPcReinvstRilieviNull, Len.WOCO_PC_REINVST_RILIEVI_NULL);
    }

    /**Original name: WOCO-PC-REINVST-RILIEVI-NULL<br>*/
    public String getWocoPcReinvstRilieviNull() {
        return readString(Pos.WOCO_PC_REINVST_RILIEVI_NULL, Len.WOCO_PC_REINVST_RILIEVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_PC_REINVST_RILIEVI = 1;
        public static final int WOCO_PC_REINVST_RILIEVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_PC_REINVST_RILIEVI = 4;
        public static final int WOCO_PC_REINVST_RILIEVI_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_PC_REINVST_RILIEVI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_PC_REINVST_RILIEVI = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
