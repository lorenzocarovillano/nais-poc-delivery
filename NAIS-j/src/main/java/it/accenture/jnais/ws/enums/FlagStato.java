package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-STATO<br>
 * Variable: FLAG-STATO from program LDBS1350<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagStato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char NOT_STATO = 'N';
    public static final char STATO = 'S';

    //==== METHODS ====
    public void setFlagStato(char flagStato) {
        this.value = flagStato;
    }

    public char getFlagStato() {
        return this.value;
    }

    public boolean isNotStato() {
        return value == NOT_STATO;
    }

    public void setNotStato() {
        value = NOT_STATO;
    }

    public boolean isStato() {
        return value == STATO;
    }

    public void setStato() {
        value = STATO;
    }
}
