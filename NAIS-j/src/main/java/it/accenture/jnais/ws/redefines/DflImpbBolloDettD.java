package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-BOLLO-DETT-D<br>
 * Variable: DFL-IMPB-BOLLO-DETT-D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbBolloDettD extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbBolloDettD() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_BOLLO_DETT_D;
    }

    public void setDflImpbBolloDettD(AfDecimal dflImpbBolloDettD) {
        writeDecimalAsPacked(Pos.DFL_IMPB_BOLLO_DETT_D, dflImpbBolloDettD.copy());
    }

    public void setDflImpbBolloDettDFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_BOLLO_DETT_D, Pos.DFL_IMPB_BOLLO_DETT_D);
    }

    /**Original name: DFL-IMPB-BOLLO-DETT-D<br>*/
    public AfDecimal getDflImpbBolloDettD() {
        return readPackedAsDecimal(Pos.DFL_IMPB_BOLLO_DETT_D, Len.Int.DFL_IMPB_BOLLO_DETT_D, Len.Fract.DFL_IMPB_BOLLO_DETT_D);
    }

    public byte[] getDflImpbBolloDettDAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_BOLLO_DETT_D, Pos.DFL_IMPB_BOLLO_DETT_D);
        return buffer;
    }

    public void setDflImpbBolloDettDNull(String dflImpbBolloDettDNull) {
        writeString(Pos.DFL_IMPB_BOLLO_DETT_D_NULL, dflImpbBolloDettDNull, Len.DFL_IMPB_BOLLO_DETT_D_NULL);
    }

    /**Original name: DFL-IMPB-BOLLO-DETT-D-NULL<br>*/
    public String getDflImpbBolloDettDNull() {
        return readString(Pos.DFL_IMPB_BOLLO_DETT_D_NULL, Len.DFL_IMPB_BOLLO_DETT_D_NULL);
    }

    public String getDflImpbBolloDettDNullFormatted() {
        return Functions.padBlanks(getDflImpbBolloDettDNull(), Len.DFL_IMPB_BOLLO_DETT_D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_BOLLO_DETT_D = 1;
        public static final int DFL_IMPB_BOLLO_DETT_D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_BOLLO_DETT_D = 8;
        public static final int DFL_IMPB_BOLLO_DETT_D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_BOLLO_DETT_D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_BOLLO_DETT_D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
