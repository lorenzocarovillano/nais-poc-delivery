package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-VIS-1382011D<br>
 * Variable: WDFL-IMPST-VIS-1382011D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstVis1382011d extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstVis1382011d() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_VIS1382011D;
    }

    public void setWdflImpstVis1382011d(AfDecimal wdflImpstVis1382011d) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_VIS1382011D, wdflImpstVis1382011d.copy());
    }

    public void setWdflImpstVis1382011dFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_VIS1382011D, Pos.WDFL_IMPST_VIS1382011D);
    }

    /**Original name: WDFL-IMPST-VIS-1382011D<br>*/
    public AfDecimal getWdflImpstVis1382011d() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_VIS1382011D, Len.Int.WDFL_IMPST_VIS1382011D, Len.Fract.WDFL_IMPST_VIS1382011D);
    }

    public byte[] getWdflImpstVis1382011dAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_VIS1382011D, Pos.WDFL_IMPST_VIS1382011D);
        return buffer;
    }

    public void setWdflImpstVis1382011dNull(String wdflImpstVis1382011dNull) {
        writeString(Pos.WDFL_IMPST_VIS1382011D_NULL, wdflImpstVis1382011dNull, Len.WDFL_IMPST_VIS1382011D_NULL);
    }

    /**Original name: WDFL-IMPST-VIS-1382011D-NULL<br>*/
    public String getWdflImpstVis1382011dNull() {
        return readString(Pos.WDFL_IMPST_VIS1382011D_NULL, Len.WDFL_IMPST_VIS1382011D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS1382011D = 1;
        public static final int WDFL_IMPST_VIS1382011D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS1382011D = 8;
        public static final int WDFL_IMPST_VIS1382011D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS1382011D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS1382011D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
