package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IProgrNumOgg;
import it.accenture.jnais.ws.redefines.D03ProgrFinale;
import it.accenture.jnais.ws.redefines.D03ProgrIniziale;

/**Original name: PROGR-NUM-OGG<br>
 * Variable: PROGR-NUM-OGG from copybook IDBVD031<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ProgrNumOggIdbsd030 extends SerializableParameter implements IProgrNumOgg {

    //==== PROPERTIES ====
    //Original name: D03-COD-COMPAGNIA-ANIA
    private int d03CodCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: D03-FORMA-ASSICURATIVA
    private String d03FormaAssicurativa = DefaultValues.stringVal(Len.D03_FORMA_ASSICURATIVA);
    //Original name: D03-COD-OGGETTO
    private String d03CodOggetto = DefaultValues.stringVal(Len.D03_COD_OGGETTO);
    //Original name: D03-KEY-BUSINESS
    private String d03KeyBusiness = DefaultValues.stringVal(Len.D03_KEY_BUSINESS);
    //Original name: D03-ULT-PROGR
    private long d03UltProgr = DefaultValues.LONG_VAL;
    //Original name: D03-PROGR-INIZIALE
    private D03ProgrIniziale d03ProgrIniziale = new D03ProgrIniziale();
    //Original name: D03-PROGR-FINALE
    private D03ProgrFinale d03ProgrFinale = new D03ProgrFinale();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PROGR_NUM_OGG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setProgrNumOggBytes(buf);
    }

    public void setProgrNumOggBytes(byte[] buffer) {
        setProgrNumOggBytes(buffer, 1);
    }

    public byte[] getProgrNumOggBytes() {
        byte[] buffer = new byte[Len.PROGR_NUM_OGG];
        return getProgrNumOggBytes(buffer, 1);
    }

    public void setProgrNumOggBytes(byte[] buffer, int offset) {
        int position = offset;
        d03CodCompagniaAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.D03_COD_COMPAGNIA_ANIA, 0);
        position += Len.D03_COD_COMPAGNIA_ANIA;
        d03FormaAssicurativa = MarshalByte.readString(buffer, position, Len.D03_FORMA_ASSICURATIVA);
        position += Len.D03_FORMA_ASSICURATIVA;
        d03CodOggetto = MarshalByte.readString(buffer, position, Len.D03_COD_OGGETTO);
        position += Len.D03_COD_OGGETTO;
        d03KeyBusiness = MarshalByte.readString(buffer, position, Len.D03_KEY_BUSINESS);
        position += Len.D03_KEY_BUSINESS;
        d03UltProgr = MarshalByte.readPackedAsLong(buffer, position, Len.Int.D03_ULT_PROGR, 0);
        position += Len.D03_ULT_PROGR;
        d03ProgrIniziale.setD03ProgrInizialeFromBuffer(buffer, position);
        position += D03ProgrIniziale.Len.D03_PROGR_INIZIALE;
        d03ProgrFinale.setD03ProgrFinaleFromBuffer(buffer, position);
    }

    public byte[] getProgrNumOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, d03CodCompagniaAnia, Len.Int.D03_COD_COMPAGNIA_ANIA, 0);
        position += Len.D03_COD_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, d03FormaAssicurativa, Len.D03_FORMA_ASSICURATIVA);
        position += Len.D03_FORMA_ASSICURATIVA;
        MarshalByte.writeString(buffer, position, d03CodOggetto, Len.D03_COD_OGGETTO);
        position += Len.D03_COD_OGGETTO;
        MarshalByte.writeString(buffer, position, d03KeyBusiness, Len.D03_KEY_BUSINESS);
        position += Len.D03_KEY_BUSINESS;
        MarshalByte.writeLongAsPacked(buffer, position, d03UltProgr, Len.Int.D03_ULT_PROGR, 0);
        position += Len.D03_ULT_PROGR;
        d03ProgrIniziale.getD03ProgrInizialeAsBuffer(buffer, position);
        position += D03ProgrIniziale.Len.D03_PROGR_INIZIALE;
        d03ProgrFinale.getD03ProgrFinaleAsBuffer(buffer, position);
        return buffer;
    }

    public void setD03CodCompagniaAnia(int d03CodCompagniaAnia) {
        this.d03CodCompagniaAnia = d03CodCompagniaAnia;
    }

    public int getD03CodCompagniaAnia() {
        return this.d03CodCompagniaAnia;
    }

    public void setD03FormaAssicurativa(String d03FormaAssicurativa) {
        this.d03FormaAssicurativa = Functions.subString(d03FormaAssicurativa, Len.D03_FORMA_ASSICURATIVA);
    }

    public String getD03FormaAssicurativa() {
        return this.d03FormaAssicurativa;
    }

    public void setD03CodOggetto(String d03CodOggetto) {
        this.d03CodOggetto = Functions.subString(d03CodOggetto, Len.D03_COD_OGGETTO);
    }

    public String getD03CodOggetto() {
        return this.d03CodOggetto;
    }

    public void setD03KeyBusiness(String d03KeyBusiness) {
        this.d03KeyBusiness = Functions.subString(d03KeyBusiness, Len.D03_KEY_BUSINESS);
    }

    public String getD03KeyBusiness() {
        return this.d03KeyBusiness;
    }

    public void setD03UltProgr(long d03UltProgr) {
        this.d03UltProgr = d03UltProgr;
    }

    public long getD03UltProgr() {
        return this.d03UltProgr;
    }

    @Override
    public int getCodCompagniaAnia() {
        return getD03CodCompagniaAnia();
    }

    @Override
    public void setCodCompagniaAnia(int codCompagniaAnia) {
        this.setD03CodCompagniaAnia(codCompagniaAnia);
    }

    @Override
    public String getCodOggetto() {
        return getD03CodOggetto();
    }

    @Override
    public void setCodOggetto(String codOggetto) {
        this.setD03CodOggetto(codOggetto);
    }

    public D03ProgrFinale getD03ProgrFinale() {
        return d03ProgrFinale;
    }

    public D03ProgrIniziale getD03ProgrIniziale() {
        return d03ProgrIniziale;
    }

    @Override
    public String getFormaAssicurativa() {
        return getD03FormaAssicurativa();
    }

    @Override
    public void setFormaAssicurativa(String formaAssicurativa) {
        this.setD03FormaAssicurativa(formaAssicurativa);
    }

    @Override
    public String getKeyBusiness() {
        return getD03KeyBusiness();
    }

    @Override
    public void setKeyBusiness(String keyBusiness) {
        this.setD03KeyBusiness(keyBusiness);
    }

    @Override
    public long getProgrFinale() {
        throw new FieldNotMappedException("progrFinale");
    }

    @Override
    public void setProgrFinale(long progrFinale) {
        throw new FieldNotMappedException("progrFinale");
    }

    @Override
    public Long getProgrFinaleObj() {
        return ((Long)getProgrFinale());
    }

    @Override
    public void setProgrFinaleObj(Long progrFinaleObj) {
        setProgrFinale(((long)progrFinaleObj));
    }

    @Override
    public long getProgrIniziale() {
        throw new FieldNotMappedException("progrIniziale");
    }

    @Override
    public void setProgrIniziale(long progrIniziale) {
        throw new FieldNotMappedException("progrIniziale");
    }

    @Override
    public Long getProgrInizialeObj() {
        return ((Long)getProgrIniziale());
    }

    @Override
    public void setProgrInizialeObj(Long progrInizialeObj) {
        setProgrIniziale(((long)progrInizialeObj));
    }

    @Override
    public long getUltProgr() {
        return getD03UltProgr();
    }

    @Override
    public void setUltProgr(long ultProgr) {
        this.setD03UltProgr(ultProgr);
    }

    @Override
    public byte[] serialize() {
        return getProgrNumOggBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int D03_COD_COMPAGNIA_ANIA = 3;
        public static final int D03_FORMA_ASSICURATIVA = 2;
        public static final int D03_COD_OGGETTO = 30;
        public static final int D03_KEY_BUSINESS = 100;
        public static final int D03_ULT_PROGR = 10;
        public static final int PROGR_NUM_OGG = D03_COD_COMPAGNIA_ANIA + D03_FORMA_ASSICURATIVA + D03_COD_OGGETTO + D03_KEY_BUSINESS + D03_ULT_PROGR + D03ProgrIniziale.Len.D03_PROGR_INIZIALE + D03ProgrFinale.Len.D03_PROGR_FINALE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int D03_COD_COMPAGNIA_ANIA = 5;
            public static final int D03_ULT_PROGR = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
