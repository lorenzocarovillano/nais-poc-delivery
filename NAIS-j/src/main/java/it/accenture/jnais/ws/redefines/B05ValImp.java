package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B05-VAL-IMP<br>
 * Variable: B05-VAL-IMP from program LLBS0250<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B05ValImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B05ValImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B05_VAL_IMP;
    }

    public void setB05ValImp(AfDecimal b05ValImp) {
        writeDecimalAsPacked(Pos.B05_VAL_IMP, b05ValImp.copy());
    }

    public void setB05ValImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B05_VAL_IMP, Pos.B05_VAL_IMP);
    }

    /**Original name: B05-VAL-IMP<br>*/
    public AfDecimal getB05ValImp() {
        return readPackedAsDecimal(Pos.B05_VAL_IMP, Len.Int.B05_VAL_IMP, Len.Fract.B05_VAL_IMP);
    }

    public byte[] getB05ValImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B05_VAL_IMP, Pos.B05_VAL_IMP);
        return buffer;
    }

    public void setB05ValImpNull(String b05ValImpNull) {
        writeString(Pos.B05_VAL_IMP_NULL, b05ValImpNull, Len.B05_VAL_IMP_NULL);
    }

    /**Original name: B05-VAL-IMP-NULL<br>*/
    public String getB05ValImpNull() {
        return readString(Pos.B05_VAL_IMP_NULL, Len.B05_VAL_IMP_NULL);
    }

    public String getB05ValImpNullFormatted() {
        return Functions.padBlanks(getB05ValImpNull(), Len.B05_VAL_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B05_VAL_IMP = 1;
        public static final int B05_VAL_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B05_VAL_IMP = 10;
        public static final int B05_VAL_IMP_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B05_VAL_IMP = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B05_VAL_IMP = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
