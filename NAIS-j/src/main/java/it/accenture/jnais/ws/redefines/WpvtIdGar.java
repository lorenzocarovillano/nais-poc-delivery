package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPVT-ID-GAR<br>
 * Variable: WPVT-ID-GAR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtIdGar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtIdGar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_ID_GAR;
    }

    public void setWpvtIdGar(int wpvtIdGar) {
        writeIntAsPacked(Pos.WPVT_ID_GAR, wpvtIdGar, Len.Int.WPVT_ID_GAR);
    }

    public void setWpvtIdGarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_ID_GAR, Pos.WPVT_ID_GAR);
    }

    /**Original name: WPVT-ID-GAR<br>*/
    public int getWpvtIdGar() {
        return readPackedAsInt(Pos.WPVT_ID_GAR, Len.Int.WPVT_ID_GAR);
    }

    public byte[] getWpvtIdGarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_ID_GAR, Pos.WPVT_ID_GAR);
        return buffer;
    }

    public void initWpvtIdGarSpaces() {
        fill(Pos.WPVT_ID_GAR, Len.WPVT_ID_GAR, Types.SPACE_CHAR);
    }

    public void setWpvtIdGarNull(String wpvtIdGarNull) {
        writeString(Pos.WPVT_ID_GAR_NULL, wpvtIdGarNull, Len.WPVT_ID_GAR_NULL);
    }

    /**Original name: WPVT-ID-GAR-NULL<br>*/
    public String getWpvtIdGarNull() {
        return readString(Pos.WPVT_ID_GAR_NULL, Len.WPVT_ID_GAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_ID_GAR = 1;
        public static final int WPVT_ID_GAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_ID_GAR = 5;
        public static final int WPVT_ID_GAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_ID_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
