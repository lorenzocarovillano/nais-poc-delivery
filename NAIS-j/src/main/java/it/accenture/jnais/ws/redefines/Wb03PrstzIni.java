package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRSTZ-INI<br>
 * Variable: WB03-PRSTZ-INI from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PrstzIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PrstzIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRSTZ_INI;
    }

    public void setWb03PrstzIni(AfDecimal wb03PrstzIni) {
        writeDecimalAsPacked(Pos.WB03_PRSTZ_INI, wb03PrstzIni.copy());
    }

    public void setWb03PrstzIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRSTZ_INI, Pos.WB03_PRSTZ_INI);
    }

    /**Original name: WB03-PRSTZ-INI<br>*/
    public AfDecimal getWb03PrstzIni() {
        return readPackedAsDecimal(Pos.WB03_PRSTZ_INI, Len.Int.WB03_PRSTZ_INI, Len.Fract.WB03_PRSTZ_INI);
    }

    public byte[] getWb03PrstzIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRSTZ_INI, Pos.WB03_PRSTZ_INI);
        return buffer;
    }

    public void setWb03PrstzIniNull(String wb03PrstzIniNull) {
        writeString(Pos.WB03_PRSTZ_INI_NULL, wb03PrstzIniNull, Len.WB03_PRSTZ_INI_NULL);
    }

    /**Original name: WB03-PRSTZ-INI-NULL<br>*/
    public String getWb03PrstzIniNull() {
        return readString(Pos.WB03_PRSTZ_INI_NULL, Len.WB03_PRSTZ_INI_NULL);
    }

    public String getWb03PrstzIniNullFormatted() {
        return Functions.padBlanks(getWb03PrstzIniNull(), Len.WB03_PRSTZ_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRSTZ_INI = 1;
        public static final int WB03_PRSTZ_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRSTZ_INI = 8;
        public static final int WB03_PRSTZ_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRSTZ_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRSTZ_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
