package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-SOPR-ALT<br>
 * Variable: WDTR-SOPR-ALT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrSoprAlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrSoprAlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_SOPR_ALT;
    }

    public void setWdtrSoprAlt(AfDecimal wdtrSoprAlt) {
        writeDecimalAsPacked(Pos.WDTR_SOPR_ALT, wdtrSoprAlt.copy());
    }

    /**Original name: WDTR-SOPR-ALT<br>*/
    public AfDecimal getWdtrSoprAlt() {
        return readPackedAsDecimal(Pos.WDTR_SOPR_ALT, Len.Int.WDTR_SOPR_ALT, Len.Fract.WDTR_SOPR_ALT);
    }

    public void setWdtrSoprAltNull(String wdtrSoprAltNull) {
        writeString(Pos.WDTR_SOPR_ALT_NULL, wdtrSoprAltNull, Len.WDTR_SOPR_ALT_NULL);
    }

    /**Original name: WDTR-SOPR-ALT-NULL<br>*/
    public String getWdtrSoprAltNull() {
        return readString(Pos.WDTR_SOPR_ALT_NULL, Len.WDTR_SOPR_ALT_NULL);
    }

    public String getWdtrSoprAltNullFormatted() {
        return Functions.padBlanks(getWdtrSoprAltNull(), Len.WDTR_SOPR_ALT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_SOPR_ALT = 1;
        public static final int WDTR_SOPR_ALT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_SOPR_ALT = 8;
        public static final int WDTR_SOPR_ALT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_SOPR_ALT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_SOPR_ALT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
