package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-TS-PP<br>
 * Variable: W-B03-TS-PP from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03TsPpLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03TsPpLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_TS_PP;
    }

    public void setwB03TsPp(AfDecimal wB03TsPp) {
        writeDecimalAsPacked(Pos.W_B03_TS_PP, wB03TsPp.copy());
    }

    /**Original name: W-B03-TS-PP<br>*/
    public AfDecimal getwB03TsPp() {
        return readPackedAsDecimal(Pos.W_B03_TS_PP, Len.Int.W_B03_TS_PP, Len.Fract.W_B03_TS_PP);
    }

    public byte[] getwB03TsPpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_TS_PP, Pos.W_B03_TS_PP);
        return buffer;
    }

    public void setwB03TsPpNull(String wB03TsPpNull) {
        writeString(Pos.W_B03_TS_PP_NULL, wB03TsPpNull, Len.W_B03_TS_PP_NULL);
    }

    /**Original name: W-B03-TS-PP-NULL<br>*/
    public String getwB03TsPpNull() {
        return readString(Pos.W_B03_TS_PP_NULL, Len.W_B03_TS_PP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_TS_PP = 1;
        public static final int W_B03_TS_PP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_TS_PP = 8;
        public static final int W_B03_TS_PP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_TS_PP = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_TS_PP = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
