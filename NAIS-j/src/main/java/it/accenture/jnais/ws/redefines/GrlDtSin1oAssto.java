package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRL-DT-SIN-1O-ASSTO<br>
 * Variable: GRL-DT-SIN-1O-ASSTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrlDtSin1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrlDtSin1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRL_DT_SIN1O_ASSTO;
    }

    public void setGrlDtSin1oAssto(int grlDtSin1oAssto) {
        writeIntAsPacked(Pos.GRL_DT_SIN1O_ASSTO, grlDtSin1oAssto, Len.Int.GRL_DT_SIN1O_ASSTO);
    }

    public void setGrlDtSin1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRL_DT_SIN1O_ASSTO, Pos.GRL_DT_SIN1O_ASSTO);
    }

    /**Original name: GRL-DT-SIN-1O-ASSTO<br>*/
    public int getGrlDtSin1oAssto() {
        return readPackedAsInt(Pos.GRL_DT_SIN1O_ASSTO, Len.Int.GRL_DT_SIN1O_ASSTO);
    }

    public byte[] getGrlDtSin1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRL_DT_SIN1O_ASSTO, Pos.GRL_DT_SIN1O_ASSTO);
        return buffer;
    }

    public void setGrlDtSin1oAsstoNull(String grlDtSin1oAsstoNull) {
        writeString(Pos.GRL_DT_SIN1O_ASSTO_NULL, grlDtSin1oAsstoNull, Len.GRL_DT_SIN1O_ASSTO_NULL);
    }

    /**Original name: GRL-DT-SIN-1O-ASSTO-NULL<br>*/
    public String getGrlDtSin1oAsstoNull() {
        return readString(Pos.GRL_DT_SIN1O_ASSTO_NULL, Len.GRL_DT_SIN1O_ASSTO_NULL);
    }

    public String getGrlDtSin1oAsstoNullFormatted() {
        return Functions.padBlanks(getGrlDtSin1oAsstoNull(), Len.GRL_DT_SIN1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRL_DT_SIN1O_ASSTO = 1;
        public static final int GRL_DT_SIN1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRL_DT_SIN1O_ASSTO = 5;
        public static final int GRL_DT_SIN1O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRL_DT_SIN1O_ASSTO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
