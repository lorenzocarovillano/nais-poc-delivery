package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFL-AA-CNBZ-DAL2007-EF<br>
 * Variable: WDFL-AA-CNBZ-DAL2007-EF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAaCnbzDal2007Ef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAaCnbzDal2007Ef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_AA_CNBZ_DAL2007_EF;
    }

    public void setWdflAaCnbzDal2007Ef(int wdflAaCnbzDal2007Ef) {
        writeIntAsPacked(Pos.WDFL_AA_CNBZ_DAL2007_EF, wdflAaCnbzDal2007Ef, Len.Int.WDFL_AA_CNBZ_DAL2007_EF);
    }

    public void setWdflAaCnbzDal2007EfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_AA_CNBZ_DAL2007_EF, Pos.WDFL_AA_CNBZ_DAL2007_EF);
    }

    /**Original name: WDFL-AA-CNBZ-DAL2007-EF<br>*/
    public int getWdflAaCnbzDal2007Ef() {
        return readPackedAsInt(Pos.WDFL_AA_CNBZ_DAL2007_EF, Len.Int.WDFL_AA_CNBZ_DAL2007_EF);
    }

    public byte[] getWdflAaCnbzDal2007EfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_AA_CNBZ_DAL2007_EF, Pos.WDFL_AA_CNBZ_DAL2007_EF);
        return buffer;
    }

    public void setWdflAaCnbzDal2007EfNull(String wdflAaCnbzDal2007EfNull) {
        writeString(Pos.WDFL_AA_CNBZ_DAL2007_EF_NULL, wdflAaCnbzDal2007EfNull, Len.WDFL_AA_CNBZ_DAL2007_EF_NULL);
    }

    /**Original name: WDFL-AA-CNBZ-DAL2007-EF-NULL<br>*/
    public String getWdflAaCnbzDal2007EfNull() {
        return readString(Pos.WDFL_AA_CNBZ_DAL2007_EF_NULL, Len.WDFL_AA_CNBZ_DAL2007_EF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_AA_CNBZ_DAL2007_EF = 1;
        public static final int WDFL_AA_CNBZ_DAL2007_EF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_AA_CNBZ_DAL2007_EF = 3;
        public static final int WDFL_AA_CNBZ_DAL2007_EF_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_AA_CNBZ_DAL2007_EF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
