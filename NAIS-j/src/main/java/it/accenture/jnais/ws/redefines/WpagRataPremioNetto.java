package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-PREMIO-NETTO<br>
 * Variable: WPAG-RATA-PREMIO-NETTO from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataPremioNetto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataPremioNetto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_PREMIO_NETTO;
    }

    public void setWpagRataPremioNetto(AfDecimal wpagRataPremioNetto) {
        writeDecimalAsPacked(Pos.WPAG_RATA_PREMIO_NETTO, wpagRataPremioNetto.copy());
    }

    public void setWpagRataPremioNettoFormatted(String wpagRataPremioNetto) {
        setWpagRataPremioNetto(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_PREMIO_NETTO + Len.Fract.WPAG_RATA_PREMIO_NETTO, Len.Fract.WPAG_RATA_PREMIO_NETTO, wpagRataPremioNetto));
    }

    public void setWpagRataPremioNettoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_PREMIO_NETTO, Pos.WPAG_RATA_PREMIO_NETTO);
    }

    /**Original name: WPAG-RATA-PREMIO-NETTO<br>*/
    public AfDecimal getWpagRataPremioNetto() {
        return readPackedAsDecimal(Pos.WPAG_RATA_PREMIO_NETTO, Len.Int.WPAG_RATA_PREMIO_NETTO, Len.Fract.WPAG_RATA_PREMIO_NETTO);
    }

    public byte[] getWpagRataPremioNettoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_PREMIO_NETTO, Pos.WPAG_RATA_PREMIO_NETTO);
        return buffer;
    }

    public void initWpagRataPremioNettoSpaces() {
        fill(Pos.WPAG_RATA_PREMIO_NETTO, Len.WPAG_RATA_PREMIO_NETTO, Types.SPACE_CHAR);
    }

    public void setWpagRataPremioNettoNull(String wpagRataPremioNettoNull) {
        writeString(Pos.WPAG_RATA_PREMIO_NETTO_NULL, wpagRataPremioNettoNull, Len.WPAG_RATA_PREMIO_NETTO_NULL);
    }

    /**Original name: WPAG-RATA-PREMIO-NETTO-NULL<br>*/
    public String getWpagRataPremioNettoNull() {
        return readString(Pos.WPAG_RATA_PREMIO_NETTO_NULL, Len.WPAG_RATA_PREMIO_NETTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_PREMIO_NETTO = 1;
        public static final int WPAG_RATA_PREMIO_NETTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_PREMIO_NETTO = 8;
        public static final int WPAG_RATA_PREMIO_NETTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_PREMIO_NETTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_PREMIO_NETTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
