package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIC-ID-OGG<br>
 * Variable: RIC-ID-OGG from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RicIdOgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RicIdOgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIC_ID_OGG;
    }

    public void setRicIdOgg(int ricIdOgg) {
        writeIntAsPacked(Pos.RIC_ID_OGG, ricIdOgg, Len.Int.RIC_ID_OGG);
    }

    public void setRicIdOggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIC_ID_OGG, Pos.RIC_ID_OGG);
    }

    /**Original name: RIC-ID-OGG<br>*/
    public int getRicIdOgg() {
        return readPackedAsInt(Pos.RIC_ID_OGG, Len.Int.RIC_ID_OGG);
    }

    public byte[] getRicIdOggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIC_ID_OGG, Pos.RIC_ID_OGG);
        return buffer;
    }

    public void setRicIdOggNull(String ricIdOggNull) {
        writeString(Pos.RIC_ID_OGG_NULL, ricIdOggNull, Len.RIC_ID_OGG_NULL);
    }

    /**Original name: RIC-ID-OGG-NULL<br>*/
    public String getRicIdOggNull() {
        return readString(Pos.RIC_ID_OGG_NULL, Len.RIC_ID_OGG_NULL);
    }

    public String getRicIdOggNullFormatted() {
        return Functions.padBlanks(getRicIdOggNull(), Len.RIC_ID_OGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIC_ID_OGG = 1;
        public static final int RIC_ID_OGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIC_ID_OGG = 5;
        public static final int RIC_ID_OGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIC_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
