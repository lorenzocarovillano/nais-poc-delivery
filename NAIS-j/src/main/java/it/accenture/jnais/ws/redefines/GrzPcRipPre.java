package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-PC-RIP-PRE<br>
 * Variable: GRZ-PC-RIP-PRE from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzPcRipPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzPcRipPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_PC_RIP_PRE;
    }

    public void setGrzPcRipPre(AfDecimal grzPcRipPre) {
        writeDecimalAsPacked(Pos.GRZ_PC_RIP_PRE, grzPcRipPre.copy());
    }

    public void setGrzPcRipPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_PC_RIP_PRE, Pos.GRZ_PC_RIP_PRE);
    }

    /**Original name: GRZ-PC-RIP-PRE<br>*/
    public AfDecimal getGrzPcRipPre() {
        return readPackedAsDecimal(Pos.GRZ_PC_RIP_PRE, Len.Int.GRZ_PC_RIP_PRE, Len.Fract.GRZ_PC_RIP_PRE);
    }

    public byte[] getGrzPcRipPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_PC_RIP_PRE, Pos.GRZ_PC_RIP_PRE);
        return buffer;
    }

    public void setGrzPcRipPreNull(String grzPcRipPreNull) {
        writeString(Pos.GRZ_PC_RIP_PRE_NULL, grzPcRipPreNull, Len.GRZ_PC_RIP_PRE_NULL);
    }

    /**Original name: GRZ-PC-RIP-PRE-NULL<br>*/
    public String getGrzPcRipPreNull() {
        return readString(Pos.GRZ_PC_RIP_PRE_NULL, Len.GRZ_PC_RIP_PRE_NULL);
    }

    public String getGrzPcRipPreNullFormatted() {
        return Functions.padBlanks(getGrzPcRipPreNull(), Len.GRZ_PC_RIP_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_PC_RIP_PRE = 1;
        public static final int GRZ_PC_RIP_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_PC_RIP_PRE = 4;
        public static final int GRZ_PC_RIP_PRE_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_PC_RIP_PRE = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int GRZ_PC_RIP_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
