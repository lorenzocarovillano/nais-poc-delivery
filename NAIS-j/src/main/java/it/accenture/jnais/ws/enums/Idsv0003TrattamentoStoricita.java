package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0003-TRATTAMENTO-STORICITA<br>
 * Variable: IDSV0003-TRATTAMENTO-STORICITA from copybook IDSV0003<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0003TrattamentoStoricita {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TRATTAMENTO_STORICITA);
    public static final String TRATT_DEFAULT = "DEF";
    public static final String TRATT_SENZA_STOR = "NST";
    public static final String TRATT_X_EFFETTO = "EFF";
    public static final String TRATT_X_COMPETENZA = "CPZ";
    public static final String ANNULLO_MOVIMENTO = "ANN";

    //==== METHODS ====
    public void setTrattamentoStoricita(String trattamentoStoricita) {
        this.value = Functions.subString(trattamentoStoricita, Len.TRATTAMENTO_STORICITA);
    }

    public String getTrattamentoStoricita() {
        return this.value;
    }

    public boolean isTrattDefault() {
        return value.equals(TRATT_DEFAULT);
    }

    public void setIdsi0011TrattDefault() {
        value = TRATT_DEFAULT;
    }

    public boolean isTrattSenzaStor() {
        return value.equals(TRATT_SENZA_STOR);
    }

    public void setTrattSenzaStor() {
        value = TRATT_SENZA_STOR;
    }

    public boolean isTrattXEffetto() {
        return value.equals(TRATT_X_EFFETTO);
    }

    public void setIdsi0011TrattXEffetto() {
        value = TRATT_X_EFFETTO;
    }

    public boolean isTrattXCompetenza() {
        return value.equals(TRATT_X_COMPETENZA);
    }

    public void setTrattXCompetenza() {
        value = TRATT_X_COMPETENZA;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TRATTAMENTO_STORICITA = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
