package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP01-ID-RICHIEDENTE<br>
 * Variable: WP01-ID-RICHIEDENTE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp01IdRichiedente extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp01IdRichiedente() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP01_ID_RICHIEDENTE;
    }

    public void setWp01IdRichiedente(int wp01IdRichiedente) {
        writeIntAsPacked(Pos.WP01_ID_RICHIEDENTE, wp01IdRichiedente, Len.Int.WP01_ID_RICHIEDENTE);
    }

    public void setWp01IdRichiedenteFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP01_ID_RICHIEDENTE, Pos.WP01_ID_RICHIEDENTE);
    }

    /**Original name: WP01-ID-RICHIEDENTE<br>*/
    public int getWp01IdRichiedente() {
        return readPackedAsInt(Pos.WP01_ID_RICHIEDENTE, Len.Int.WP01_ID_RICHIEDENTE);
    }

    public byte[] getWp01IdRichiedenteAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP01_ID_RICHIEDENTE, Pos.WP01_ID_RICHIEDENTE);
        return buffer;
    }

    public void setWp01IdRichiedenteNull(String wp01IdRichiedenteNull) {
        writeString(Pos.WP01_ID_RICHIEDENTE_NULL, wp01IdRichiedenteNull, Len.WP01_ID_RICHIEDENTE_NULL);
    }

    /**Original name: WP01-ID-RICHIEDENTE-NULL<br>*/
    public String getWp01IdRichiedenteNull() {
        return readString(Pos.WP01_ID_RICHIEDENTE_NULL, Len.WP01_ID_RICHIEDENTE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP01_ID_RICHIEDENTE = 1;
        public static final int WP01_ID_RICHIEDENTE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP01_ID_RICHIEDENTE = 5;
        public static final int WP01_ID_RICHIEDENTE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP01_ID_RICHIEDENTE = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
