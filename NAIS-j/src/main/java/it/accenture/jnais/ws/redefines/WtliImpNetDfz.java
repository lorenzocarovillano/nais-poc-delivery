package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-IMP-NET-DFZ<br>
 * Variable: WTLI-IMP-NET-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliImpNetDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliImpNetDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_IMP_NET_DFZ;
    }

    public void setWtliImpNetDfz(AfDecimal wtliImpNetDfz) {
        writeDecimalAsPacked(Pos.WTLI_IMP_NET_DFZ, wtliImpNetDfz.copy());
    }

    public void setWtliImpNetDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_IMP_NET_DFZ, Pos.WTLI_IMP_NET_DFZ);
    }

    /**Original name: WTLI-IMP-NET-DFZ<br>*/
    public AfDecimal getWtliImpNetDfz() {
        return readPackedAsDecimal(Pos.WTLI_IMP_NET_DFZ, Len.Int.WTLI_IMP_NET_DFZ, Len.Fract.WTLI_IMP_NET_DFZ);
    }

    public byte[] getWtliImpNetDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_IMP_NET_DFZ, Pos.WTLI_IMP_NET_DFZ);
        return buffer;
    }

    public void initWtliImpNetDfzSpaces() {
        fill(Pos.WTLI_IMP_NET_DFZ, Len.WTLI_IMP_NET_DFZ, Types.SPACE_CHAR);
    }

    public void setWtliImpNetDfzNull(String wtliImpNetDfzNull) {
        writeString(Pos.WTLI_IMP_NET_DFZ_NULL, wtliImpNetDfzNull, Len.WTLI_IMP_NET_DFZ_NULL);
    }

    /**Original name: WTLI-IMP-NET-DFZ-NULL<br>*/
    public String getWtliImpNetDfzNull() {
        return readString(Pos.WTLI_IMP_NET_DFZ_NULL, Len.WTLI_IMP_NET_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_IMP_NET_DFZ = 1;
        public static final int WTLI_IMP_NET_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_IMP_NET_DFZ = 8;
        public static final int WTLI_IMP_NET_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_IMP_NET_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_IMP_NET_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
