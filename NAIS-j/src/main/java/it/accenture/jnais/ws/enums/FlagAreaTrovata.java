package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-AREA-TROVATA<br>
 * Variable: FLAG-AREA-TROVATA from program LVVS0029<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagAreaTrovata {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagAreaTrovata(char flagAreaTrovata) {
        this.value = flagAreaTrovata;
    }

    public char getFlagAreaTrovata() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
