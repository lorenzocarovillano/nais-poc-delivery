package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.redefines.WtliTab;

/**Original name: WTLI-AREA-TRCH-LIQ<br>
 * Variable: WTLI-AREA-TRCH-LIQ from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WtliAreaTrchLiq {

    //==== PROPERTIES ====
    //Original name: WTLI-ELE-TRCH-LIQ-MAX
    private short wtliEleTrchLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTLI-TAB
    private WtliTab wtliTab = new WtliTab();

    //==== METHODS ====
    public void setWtliAreaTrchLiqFormatted(String data) {
        byte[] buffer = new byte[Len.WTLI_AREA_TRCH_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.WTLI_AREA_TRCH_LIQ);
        setWtliAreaTrchLiqBytes(buffer, 1);
    }

    public void setWtliAreaTrchLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        wtliEleTrchLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wtliTab.setWtliTabBytes(buffer, position);
    }

    public byte[] getWtliAreaTrchLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtliEleTrchLiqMax);
        position += Types.SHORT_SIZE;
        wtliTab.getWtliTabBytes(buffer, position);
        return buffer;
    }

    public void setWtliEleTrchLiqMax(short wtliEleTrchLiqMax) {
        this.wtliEleTrchLiqMax = wtliEleTrchLiqMax;
    }

    public short getWtliEleTrchLiqMax() {
        return this.wtliEleTrchLiqMax;
    }

    public WtliTab getWtliTab() {
        return wtliTab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_ELE_TRCH_LIQ_MAX = 2;
        public static final int WTLI_AREA_TRCH_LIQ = WTLI_ELE_TRCH_LIQ_MAX + WtliTab.Len.WTLI_TAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
