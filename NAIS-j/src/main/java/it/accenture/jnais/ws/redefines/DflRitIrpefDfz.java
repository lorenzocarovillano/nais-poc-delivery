package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RIT-IRPEF-DFZ<br>
 * Variable: DFL-RIT-IRPEF-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflRitIrpefDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflRitIrpefDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RIT_IRPEF_DFZ;
    }

    public void setDflRitIrpefDfz(AfDecimal dflRitIrpefDfz) {
        writeDecimalAsPacked(Pos.DFL_RIT_IRPEF_DFZ, dflRitIrpefDfz.copy());
    }

    public void setDflRitIrpefDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RIT_IRPEF_DFZ, Pos.DFL_RIT_IRPEF_DFZ);
    }

    /**Original name: DFL-RIT-IRPEF-DFZ<br>*/
    public AfDecimal getDflRitIrpefDfz() {
        return readPackedAsDecimal(Pos.DFL_RIT_IRPEF_DFZ, Len.Int.DFL_RIT_IRPEF_DFZ, Len.Fract.DFL_RIT_IRPEF_DFZ);
    }

    public byte[] getDflRitIrpefDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RIT_IRPEF_DFZ, Pos.DFL_RIT_IRPEF_DFZ);
        return buffer;
    }

    public void setDflRitIrpefDfzNull(String dflRitIrpefDfzNull) {
        writeString(Pos.DFL_RIT_IRPEF_DFZ_NULL, dflRitIrpefDfzNull, Len.DFL_RIT_IRPEF_DFZ_NULL);
    }

    /**Original name: DFL-RIT-IRPEF-DFZ-NULL<br>*/
    public String getDflRitIrpefDfzNull() {
        return readString(Pos.DFL_RIT_IRPEF_DFZ_NULL, Len.DFL_RIT_IRPEF_DFZ_NULL);
    }

    public String getDflRitIrpefDfzNullFormatted() {
        return Functions.padBlanks(getDflRitIrpefDfzNull(), Len.DFL_RIT_IRPEF_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RIT_IRPEF_DFZ = 1;
        public static final int DFL_RIT_IRPEF_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RIT_IRPEF_DFZ = 8;
        public static final int DFL_RIT_IRPEF_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RIT_IRPEF_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RIT_IRPEF_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
