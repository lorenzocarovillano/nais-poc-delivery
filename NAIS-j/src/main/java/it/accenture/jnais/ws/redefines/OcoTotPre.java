package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-TOT-PRE<br>
 * Variable: OCO-TOT-PRE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoTotPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoTotPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_TOT_PRE;
    }

    public void setOcoTotPre(AfDecimal ocoTotPre) {
        writeDecimalAsPacked(Pos.OCO_TOT_PRE, ocoTotPre.copy());
    }

    public void setOcoTotPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_TOT_PRE, Pos.OCO_TOT_PRE);
    }

    /**Original name: OCO-TOT-PRE<br>*/
    public AfDecimal getOcoTotPre() {
        return readPackedAsDecimal(Pos.OCO_TOT_PRE, Len.Int.OCO_TOT_PRE, Len.Fract.OCO_TOT_PRE);
    }

    public byte[] getOcoTotPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_TOT_PRE, Pos.OCO_TOT_PRE);
        return buffer;
    }

    public void setOcoTotPreNull(String ocoTotPreNull) {
        writeString(Pos.OCO_TOT_PRE_NULL, ocoTotPreNull, Len.OCO_TOT_PRE_NULL);
    }

    /**Original name: OCO-TOT-PRE-NULL<br>*/
    public String getOcoTotPreNull() {
        return readString(Pos.OCO_TOT_PRE_NULL, Len.OCO_TOT_PRE_NULL);
    }

    public String getOcoTotPreNullFormatted() {
        return Functions.padBlanks(getOcoTotPreNull(), Len.OCO_TOT_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_TOT_PRE = 1;
        public static final int OCO_TOT_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_TOT_PRE = 8;
        public static final int OCO_TOT_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_TOT_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_TOT_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
