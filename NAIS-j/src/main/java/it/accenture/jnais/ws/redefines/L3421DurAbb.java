package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-DUR-ABB<br>
 * Variable: L3421-DUR-ABB from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421DurAbb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421DurAbb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_DUR_ABB;
    }

    public void setL3421DurAbbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_DUR_ABB, Pos.L3421_DUR_ABB);
    }

    /**Original name: L3421-DUR-ABB<br>*/
    public int getL3421DurAbb() {
        return readPackedAsInt(Pos.L3421_DUR_ABB, Len.Int.L3421_DUR_ABB);
    }

    public byte[] getL3421DurAbbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_DUR_ABB, Pos.L3421_DUR_ABB);
        return buffer;
    }

    /**Original name: L3421-DUR-ABB-NULL<br>*/
    public String getL3421DurAbbNull() {
        return readString(Pos.L3421_DUR_ABB_NULL, Len.L3421_DUR_ABB_NULL);
    }

    public String getL3421DurAbbNullFormatted() {
        return Functions.padBlanks(getL3421DurAbbNull(), Len.L3421_DUR_ABB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_DUR_ABB = 1;
        public static final int L3421_DUR_ABB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_DUR_ABB = 4;
        public static final int L3421_DUR_ABB_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_DUR_ABB = 6;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
