package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDAD-IMP-VOLO<br>
 * Variable: WDAD-IMP-VOLO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_IMP_VOLO;
    }

    public void setWdadImpVolo(AfDecimal wdadImpVolo) {
        writeDecimalAsPacked(Pos.WDAD_IMP_VOLO, wdadImpVolo.copy());
    }

    public void setWdadImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_IMP_VOLO, Pos.WDAD_IMP_VOLO);
    }

    /**Original name: WDAD-IMP-VOLO<br>*/
    public AfDecimal getWdadImpVolo() {
        return readPackedAsDecimal(Pos.WDAD_IMP_VOLO, Len.Int.WDAD_IMP_VOLO, Len.Fract.WDAD_IMP_VOLO);
    }

    public byte[] getWdadImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_IMP_VOLO, Pos.WDAD_IMP_VOLO);
        return buffer;
    }

    public void setWdadImpVoloNull(String wdadImpVoloNull) {
        writeString(Pos.WDAD_IMP_VOLO_NULL, wdadImpVoloNull, Len.WDAD_IMP_VOLO_NULL);
    }

    /**Original name: WDAD-IMP-VOLO-NULL<br>*/
    public String getWdadImpVoloNull() {
        return readString(Pos.WDAD_IMP_VOLO_NULL, Len.WDAD_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_VOLO = 1;
        public static final int WDAD_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_VOLO = 8;
        public static final int WDAD_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
