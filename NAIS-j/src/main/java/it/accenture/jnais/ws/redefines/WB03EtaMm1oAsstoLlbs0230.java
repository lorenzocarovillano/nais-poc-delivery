package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-ETA-MM-1O-ASSTO<br>
 * Variable: W-B03-ETA-MM-1O-ASSTO from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03EtaMm1oAsstoLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03EtaMm1oAsstoLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_ETA_MM1O_ASSTO;
    }

    public void setwB03EtaMm1oAssto(int wB03EtaMm1oAssto) {
        writeIntAsPacked(Pos.W_B03_ETA_MM1O_ASSTO, wB03EtaMm1oAssto, Len.Int.W_B03_ETA_MM1O_ASSTO);
    }

    /**Original name: W-B03-ETA-MM-1O-ASSTO<br>*/
    public int getwB03EtaMm1oAssto() {
        return readPackedAsInt(Pos.W_B03_ETA_MM1O_ASSTO, Len.Int.W_B03_ETA_MM1O_ASSTO);
    }

    public byte[] getwB03EtaMm1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_ETA_MM1O_ASSTO, Pos.W_B03_ETA_MM1O_ASSTO);
        return buffer;
    }

    public void setwB03EtaMm1oAsstoNull(String wB03EtaMm1oAsstoNull) {
        writeString(Pos.W_B03_ETA_MM1O_ASSTO_NULL, wB03EtaMm1oAsstoNull, Len.W_B03_ETA_MM1O_ASSTO_NULL);
    }

    /**Original name: W-B03-ETA-MM-1O-ASSTO-NULL<br>*/
    public String getwB03EtaMm1oAsstoNull() {
        return readString(Pos.W_B03_ETA_MM1O_ASSTO_NULL, Len.W_B03_ETA_MM1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_ETA_MM1O_ASSTO = 1;
        public static final int W_B03_ETA_MM1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_ETA_MM1O_ASSTO = 3;
        public static final int W_B03_ETA_MM1O_ASSTO_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_ETA_MM1O_ASSTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
