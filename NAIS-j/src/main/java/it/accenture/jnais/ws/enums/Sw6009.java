package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-6009<br>
 * Variable: SW-6009 from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Sw6009 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO6009 = 'S';
    public static final char NO_TROVATO6009 = 'N';

    //==== METHODS ====
    public void setSw6009(char sw6009) {
        this.value = sw6009;
    }

    public char getSw6009() {
        return this.value;
    }

    public void setTrovato6009() {
        value = TROVATO6009;
    }

    public boolean isNoTrovato6009() {
        return value == NO_TROVATO6009;
    }

    public void setNoTrovato6009() {
        value = NO_TROVATO6009;
    }
}
