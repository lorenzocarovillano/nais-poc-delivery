package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-IMPST-252-IPT<br>
 * Variable: BEL-IMPST-252-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelImpst252Ipt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelImpst252Ipt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_IMPST252_IPT;
    }

    public void setBelImpst252Ipt(AfDecimal belImpst252Ipt) {
        writeDecimalAsPacked(Pos.BEL_IMPST252_IPT, belImpst252Ipt.copy());
    }

    public void setBelImpst252IptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_IMPST252_IPT, Pos.BEL_IMPST252_IPT);
    }

    /**Original name: BEL-IMPST-252-IPT<br>*/
    public AfDecimal getBelImpst252Ipt() {
        return readPackedAsDecimal(Pos.BEL_IMPST252_IPT, Len.Int.BEL_IMPST252_IPT, Len.Fract.BEL_IMPST252_IPT);
    }

    public byte[] getBelImpst252IptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_IMPST252_IPT, Pos.BEL_IMPST252_IPT);
        return buffer;
    }

    public void setBelImpst252IptNull(String belImpst252IptNull) {
        writeString(Pos.BEL_IMPST252_IPT_NULL, belImpst252IptNull, Len.BEL_IMPST252_IPT_NULL);
    }

    /**Original name: BEL-IMPST-252-IPT-NULL<br>*/
    public String getBelImpst252IptNull() {
        return readString(Pos.BEL_IMPST252_IPT_NULL, Len.BEL_IMPST252_IPT_NULL);
    }

    public String getBelImpst252IptNullFormatted() {
        return Functions.padBlanks(getBelImpst252IptNull(), Len.BEL_IMPST252_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_IMPST252_IPT = 1;
        public static final int BEL_IMPST252_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_IMPST252_IPT = 8;
        public static final int BEL_IMPST252_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_IMPST252_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_IMPST252_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
