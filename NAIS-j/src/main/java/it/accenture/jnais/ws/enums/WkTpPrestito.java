package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WK-TP-PRESTITO<br>
 * Variable: WK-TP-PRESTITO from program LVVS0740<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkTpPrestito {

    //==== PROPERTIES ====
    private String value = "";
    public static final String SEPARATO = "SE";
    public static final String UNICO = "UN";

    //==== METHODS ====
    public void setWkTpPrestito(String wkTpPrestito) {
        this.value = Functions.subString(wkTpPrestito, Len.WK_TP_PRESTITO);
    }

    public String getWkTpPrestito() {
        return this.value;
    }

    public boolean isSeparato() {
        return value.equals(SEPARATO);
    }

    public boolean isUnico() {
        return value.equals(UNICO);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TP_PRESTITO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
