package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABV0006-FLAG-PROCESS-TYPE<br>
 * Variable: IABV0006-FLAG-PROCESS-TYPE from copybook IABV0006<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0006FlagProcessType {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_PROCESS_TYPE);
    public static final String STD_TYPE_REC_NO = "TRN";
    public static final String STD_TYPE_REC_YES = "TRY";
    public static final String GUIDE_AD_HOC = "HOC";
    public static final String BY_BOOKING = "BKG";

    //==== METHODS ====
    public void setFlagProcessType(String flagProcessType) {
        this.value = Functions.subString(flagProcessType, Len.FLAG_PROCESS_TYPE);
    }

    public String getFlagProcessType() {
        return this.value;
    }

    public boolean isIabv0006StdTypeRecNo() {
        return value.equals(STD_TYPE_REC_NO);
    }

    public void setIabv0006StdTypeRecNo() {
        value = STD_TYPE_REC_NO;
    }

    public boolean isIabv0006StdTypeRecYes() {
        return value.equals(STD_TYPE_REC_YES);
    }

    public void setIabv0006StdTypeRecYes() {
        value = STD_TYPE_REC_YES;
    }

    public boolean isIabv0006GuideAdHoc() {
        return value.equals(GUIDE_AD_HOC);
    }

    public void setIabv0006GuideAdHoc() {
        value = GUIDE_AD_HOC;
    }

    public boolean isIabv0006ByBooking() {
        return value.equals(BY_BOOKING);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_PROCESS_TYPE = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
