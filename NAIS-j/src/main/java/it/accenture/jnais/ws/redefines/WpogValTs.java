package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPOG-VAL-TS<br>
 * Variable: WPOG-VAL-TS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpogValTs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpogValTs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOG_VAL_TS;
    }

    public void setWpogValTs(AfDecimal wpogValTs) {
        writeDecimalAsPacked(Pos.WPOG_VAL_TS, wpogValTs.copy());
    }

    public void setWpogValTsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOG_VAL_TS, Pos.WPOG_VAL_TS);
    }

    /**Original name: WPOG-VAL-TS<br>*/
    public AfDecimal getWpogValTs() {
        return readPackedAsDecimal(Pos.WPOG_VAL_TS, Len.Int.WPOG_VAL_TS, Len.Fract.WPOG_VAL_TS);
    }

    public byte[] getWpogValTsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOG_VAL_TS, Pos.WPOG_VAL_TS);
        return buffer;
    }

    public void initWpogValTsSpaces() {
        fill(Pos.WPOG_VAL_TS, Len.WPOG_VAL_TS, Types.SPACE_CHAR);
    }

    public void setWpogValTsNull(String wpogValTsNull) {
        writeString(Pos.WPOG_VAL_TS_NULL, wpogValTsNull, Len.WPOG_VAL_TS_NULL);
    }

    /**Original name: WPOG-VAL-TS-NULL<br>*/
    public String getWpogValTsNull() {
        return readString(Pos.WPOG_VAL_TS_NULL, Len.WPOG_VAL_TS_NULL);
    }

    public String getDpogValTsNullFormatted() {
        return Functions.padBlanks(getWpogValTsNull(), Len.WPOG_VAL_TS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOG_VAL_TS = 1;
        public static final int WPOG_VAL_TS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOG_VAL_TS = 8;
        public static final int WPOG_VAL_TS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPOG_VAL_TS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOG_VAL_TS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
