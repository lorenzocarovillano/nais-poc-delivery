package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P85-RENDTO-RETR<br>
 * Variable: P85-RENDTO-RETR from program IDBSP850<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P85RendtoRetr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P85RendtoRetr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P85_RENDTO_RETR;
    }

    public void setP85RendtoRetr(AfDecimal p85RendtoRetr) {
        writeDecimalAsPacked(Pos.P85_RENDTO_RETR, p85RendtoRetr.copy());
    }

    public void setP85RendtoRetrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P85_RENDTO_RETR, Pos.P85_RENDTO_RETR);
    }

    /**Original name: P85-RENDTO-RETR<br>*/
    public AfDecimal getP85RendtoRetr() {
        return readPackedAsDecimal(Pos.P85_RENDTO_RETR, Len.Int.P85_RENDTO_RETR, Len.Fract.P85_RENDTO_RETR);
    }

    public byte[] getP85RendtoRetrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P85_RENDTO_RETR, Pos.P85_RENDTO_RETR);
        return buffer;
    }

    public void setP85RendtoRetrNull(String p85RendtoRetrNull) {
        writeString(Pos.P85_RENDTO_RETR_NULL, p85RendtoRetrNull, Len.P85_RENDTO_RETR_NULL);
    }

    /**Original name: P85-RENDTO-RETR-NULL<br>*/
    public String getP85RendtoRetrNull() {
        return readString(Pos.P85_RENDTO_RETR_NULL, Len.P85_RENDTO_RETR_NULL);
    }

    public String getP85RendtoRetrNullFormatted() {
        return Functions.padBlanks(getP85RendtoRetrNull(), Len.P85_RENDTO_RETR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P85_RENDTO_RETR = 1;
        public static final int P85_RENDTO_RETR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P85_RENDTO_RETR = 8;
        public static final int P85_RENDTO_RETR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P85_RENDTO_RETR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P85_RENDTO_RETR = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
