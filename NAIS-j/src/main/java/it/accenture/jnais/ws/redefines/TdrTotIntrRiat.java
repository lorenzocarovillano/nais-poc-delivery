package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-INTR-RIAT<br>
 * Variable: TDR-TOT-INTR-RIAT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotIntrRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotIntrRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_INTR_RIAT;
    }

    public void setTdrTotIntrRiat(AfDecimal tdrTotIntrRiat) {
        writeDecimalAsPacked(Pos.TDR_TOT_INTR_RIAT, tdrTotIntrRiat.copy());
    }

    public void setTdrTotIntrRiatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_INTR_RIAT, Pos.TDR_TOT_INTR_RIAT);
    }

    /**Original name: TDR-TOT-INTR-RIAT<br>*/
    public AfDecimal getTdrTotIntrRiat() {
        return readPackedAsDecimal(Pos.TDR_TOT_INTR_RIAT, Len.Int.TDR_TOT_INTR_RIAT, Len.Fract.TDR_TOT_INTR_RIAT);
    }

    public byte[] getTdrTotIntrRiatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_INTR_RIAT, Pos.TDR_TOT_INTR_RIAT);
        return buffer;
    }

    public void setTdrTotIntrRiatNull(String tdrTotIntrRiatNull) {
        writeString(Pos.TDR_TOT_INTR_RIAT_NULL, tdrTotIntrRiatNull, Len.TDR_TOT_INTR_RIAT_NULL);
    }

    /**Original name: TDR-TOT-INTR-RIAT-NULL<br>*/
    public String getTdrTotIntrRiatNull() {
        return readString(Pos.TDR_TOT_INTR_RIAT_NULL, Len.TDR_TOT_INTR_RIAT_NULL);
    }

    public String getTdrTotIntrRiatNullFormatted() {
        return Functions.padBlanks(getTdrTotIntrRiatNull(), Len.TDR_TOT_INTR_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_INTR_RIAT = 1;
        public static final int TDR_TOT_INTR_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_INTR_RIAT = 8;
        public static final int TDR_TOT_INTR_RIAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_INTR_RIAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
