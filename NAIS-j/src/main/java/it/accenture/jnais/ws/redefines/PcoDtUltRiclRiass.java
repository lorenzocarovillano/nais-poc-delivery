package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-RICL-RIASS<br>
 * Variable: PCO-DT-ULT-RICL-RIASS from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltRiclRiass extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltRiclRiass() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_RICL_RIASS;
    }

    public void setPcoDtUltRiclRiass(int pcoDtUltRiclRiass) {
        writeIntAsPacked(Pos.PCO_DT_ULT_RICL_RIASS, pcoDtUltRiclRiass, Len.Int.PCO_DT_ULT_RICL_RIASS);
    }

    public void setPcoDtUltRiclRiassFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_RICL_RIASS, Pos.PCO_DT_ULT_RICL_RIASS);
    }

    /**Original name: PCO-DT-ULT-RICL-RIASS<br>*/
    public int getPcoDtUltRiclRiass() {
        return readPackedAsInt(Pos.PCO_DT_ULT_RICL_RIASS, Len.Int.PCO_DT_ULT_RICL_RIASS);
    }

    public byte[] getPcoDtUltRiclRiassAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_RICL_RIASS, Pos.PCO_DT_ULT_RICL_RIASS);
        return buffer;
    }

    public void initPcoDtUltRiclRiassHighValues() {
        fill(Pos.PCO_DT_ULT_RICL_RIASS, Len.PCO_DT_ULT_RICL_RIASS, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltRiclRiassNull(String pcoDtUltRiclRiassNull) {
        writeString(Pos.PCO_DT_ULT_RICL_RIASS_NULL, pcoDtUltRiclRiassNull, Len.PCO_DT_ULT_RICL_RIASS_NULL);
    }

    /**Original name: PCO-DT-ULT-RICL-RIASS-NULL<br>*/
    public String getPcoDtUltRiclRiassNull() {
        return readString(Pos.PCO_DT_ULT_RICL_RIASS_NULL, Len.PCO_DT_ULT_RICL_RIASS_NULL);
    }

    public String getPcoDtUltRiclRiassNullFormatted() {
        return Functions.padBlanks(getPcoDtUltRiclRiassNull(), Len.PCO_DT_ULT_RICL_RIASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RICL_RIASS = 1;
        public static final int PCO_DT_ULT_RICL_RIASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RICL_RIASS = 5;
        public static final int PCO_DT_ULT_RICL_RIASS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_RICL_RIASS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
