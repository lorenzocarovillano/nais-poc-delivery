package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: WK-APPO-BLOB-DATA<br>
 * Variables: WK-APPO-BLOB-DATA from program LRGM0380<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WkAppoBlobData implements ICopyable<WkAppoBlobData> {

    //==== PROPERTIES ====
    //Original name: WK-APPO-BLOB-DATA-REC
    private String wkAppoBlobDataRec = DefaultValues.stringVal(Len.WK_APPO_BLOB_DATA_REC);

    //==== CONSTRUCTORS ====
    public WkAppoBlobData() {
    }

    public WkAppoBlobData(WkAppoBlobData blobData) {
        this();
        this.wkAppoBlobDataRec = blobData.wkAppoBlobDataRec;
    }

    //==== METHODS ====
    public void setBlobDataBytes(byte[] buffer, int offset) {
        int position = offset;
        wkAppoBlobDataRec = MarshalByte.readString(buffer, position, Len.WK_APPO_BLOB_DATA_REC);
    }

    public byte[] getBlobDataBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wkAppoBlobDataRec, Len.WK_APPO_BLOB_DATA_REC);
        return buffer;
    }

    public WkAppoBlobData initBlobDataSpaces() {
        wkAppoBlobDataRec = "";
        return this;
    }

    public void setWkAppoBlobDataRec(String wkAppoBlobDataRec) {
        this.wkAppoBlobDataRec = Functions.subString(wkAppoBlobDataRec, Len.WK_APPO_BLOB_DATA_REC);
    }

    public String getWkAppoBlobDataRec() {
        return this.wkAppoBlobDataRec;
    }

    public String getWkAppoBlobDataRecFormatted() {
        return Functions.padBlanks(getWkAppoBlobDataRec(), Len.WK_APPO_BLOB_DATA_REC);
    }

    public WkAppoBlobData copy() {
        return new WkAppoBlobData(this);
    }

    public WkAppoBlobData initWkAppoBlobData() {
        wkAppoBlobDataRec = "";
        return this;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_APPO_BLOB_DATA_REC = 2500;
        public static final int BLOB_DATA = WK_APPO_BLOB_DATA_REC;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
