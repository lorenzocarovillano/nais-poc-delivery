package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;

/**Original name: ISPC0211-TAB-VAL-T<br>
 * Variable: ISPC0211-TAB-VAL-T from program ISPS0211<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ispc0211TabValT extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int AREA_VARIABILI_T_MAXOCCURS = 75;
    public static final int TAB_SCHEDA_T_MAXOCCURS = 750;
    public static final String ISPC0211_FLG_ITN_POS = "0";
    public static final String ISPC0211_FLG_ITN_NEG = "1";

    //==== CONSTRUCTORS ====
    public Ispc0211TabValT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISPC0211_TAB_VAL_T;
    }

    public String getIspc0211TabValTFormatted() {
        return readFixedString(Pos.ISPC0211_TAB_VAL_T, Len.ISPC0211_TAB_VAL_T);
    }

    public void setIspc0211TabValTBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISPC0211_TAB_VAL_T, Pos.ISPC0211_TAB_VAL_T);
    }

    public byte[] getIspc0211TabValTBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISPC0211_TAB_VAL_T, Pos.ISPC0211_TAB_VAL_T);
        return buffer;
    }

    public void setIspc0211TipoLivelloT(int ispc0211TipoLivelloTIdx, char ispc0211TipoLivelloT) {
        int position = Pos.ispc0211TipoLivelloT(ispc0211TipoLivelloTIdx - 1);
        writeChar(position, ispc0211TipoLivelloT);
    }

    /**Original name: ISPC0211-TIPO-LIVELLO-T<br>*/
    public char getIspc0211TipoLivelloT(int ispc0211TipoLivelloTIdx) {
        int position = Pos.ispc0211TipoLivelloT(ispc0211TipoLivelloTIdx - 1);
        return readChar(position);
    }

    public void setIspc0211CodiceLivelloT(int ispc0211CodiceLivelloTIdx, String ispc0211CodiceLivelloT) {
        int position = Pos.ispc0211CodiceLivelloT(ispc0211CodiceLivelloTIdx - 1);
        writeString(position, ispc0211CodiceLivelloT, Len.CODICE_LIVELLO_T);
    }

    /**Original name: ISPC0211-CODICE-LIVELLO-T<br>*/
    public String getIspc0211CodiceLivelloT(int ispc0211CodiceLivelloTIdx) {
        int position = Pos.ispc0211CodiceLivelloT(ispc0211CodiceLivelloTIdx - 1);
        return readString(position, Len.CODICE_LIVELLO_T);
    }

    public void setIspc0211IdentLivelloTFormatted(int ispc0211IdentLivelloTIdx, String ispc0211IdentLivelloT) {
        int position = Pos.ispc0211IdentLivelloT(ispc0211IdentLivelloTIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ispc0211IdentLivelloT, Len.IDENT_LIVELLO_T), Len.IDENT_LIVELLO_T);
    }

    /**Original name: ISPC0211-IDENT-LIVELLO-T<br>*/
    public int getIspc0211IdentLivelloT(int ispc0211IdentLivelloTIdx) {
        int position = Pos.ispc0211IdentLivelloT(ispc0211IdentLivelloTIdx - 1);
        return readNumDispUnsignedInt(position, Len.IDENT_LIVELLO_T);
    }

    public void setIspc0211DtInizTari(int ispc0211DtInizTariIdx, String ispc0211DtInizTari) {
        int position = Pos.ispc0211DtInizTari(ispc0211DtInizTariIdx - 1);
        writeString(position, ispc0211DtInizTari, Len.DT_INIZ_TARI);
    }

    /**Original name: ISPC0211-DT-INIZ-TARI<br>*/
    public String getIspc0211DtInizTari(int ispc0211DtInizTariIdx) {
        int position = Pos.ispc0211DtInizTari(ispc0211DtInizTariIdx - 1);
        return readString(position, Len.DT_INIZ_TARI);
    }

    public void setIspc0211CodRgmFiscTFormatted(int ispc0211CodRgmFiscTIdx, String ispc0211CodRgmFiscT) {
        int position = Pos.ispc0211CodRgmFiscT(ispc0211CodRgmFiscTIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ispc0211CodRgmFiscT, Len.COD_RGM_FISC_T), Len.COD_RGM_FISC_T);
    }

    /**Original name: ISPC0211-COD-RGM-FISC-T<br>*/
    public short getIspc0211CodRgmFiscT(int ispc0211CodRgmFiscTIdx) {
        int position = Pos.ispc0211CodRgmFiscT(ispc0211CodRgmFiscTIdx - 1);
        return readNumDispUnsignedShort(position, Len.COD_RGM_FISC_T);
    }

    public void setIspc0211DtDecorTrch(int ispc0211DtDecorTrchIdx, String ispc0211DtDecorTrch) {
        int position = Pos.ispc0211DtDecorTrch(ispc0211DtDecorTrchIdx - 1);
        writeString(position, ispc0211DtDecorTrch, Len.DT_DECOR_TRCH);
    }

    /**Original name: ISPC0211-DT-DECOR-TRCH<br>*/
    public String getIspc0211DtDecorTrch(int ispc0211DtDecorTrchIdx) {
        int position = Pos.ispc0211DtDecorTrch(ispc0211DtDecorTrchIdx - 1);
        return readString(position, Len.DT_DECOR_TRCH);
    }

    public void setIspc0211FlgLiqT(int ispc0211FlgLiqTIdx, String ispc0211FlgLiqT) {
        int position = Pos.ispc0211FlgLiqT(ispc0211FlgLiqTIdx - 1);
        writeString(position, ispc0211FlgLiqT, Len.FLG_LIQ_T);
    }

    /**Original name: ISPC0211-FLG-LIQ-T<br>*/
    public String getIspc0211FlgLiqT(int ispc0211FlgLiqTIdx) {
        int position = Pos.ispc0211FlgLiqT(ispc0211FlgLiqTIdx - 1);
        return readString(position, Len.FLG_LIQ_T);
    }

    public void setIspc0211CodiceOpzioneT(int ispc0211CodiceOpzioneTIdx, String ispc0211CodiceOpzioneT) {
        int position = Pos.ispc0211CodiceOpzioneT(ispc0211CodiceOpzioneTIdx - 1);
        writeString(position, ispc0211CodiceOpzioneT, Len.CODICE_OPZIONE_T);
    }

    /**Original name: ISPC0211-CODICE-OPZIONE-T<br>*/
    public String getIspc0211CodiceOpzioneT(int ispc0211CodiceOpzioneTIdx) {
        int position = Pos.ispc0211CodiceOpzioneT(ispc0211CodiceOpzioneTIdx - 1);
        return readString(position, Len.CODICE_OPZIONE_T);
    }

    public void setIspc0211TipoTrchFormatted(int ispc0211TipoTrchIdx, String ispc0211TipoTrch) {
        int position = Pos.ispc0211TipoTrch(ispc0211TipoTrchIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ispc0211TipoTrch, Len.TIPO_TRCH), Len.TIPO_TRCH);
    }

    /**Original name: ISPC0211-TIPO-TRCH<br>*/
    public short getIspc0211TipoTrch(int ispc0211TipoTrchIdx) {
        int position = Pos.ispc0211TipoTrch(ispc0211TipoTrchIdx - 1);
        return readNumDispUnsignedShort(position, Len.TIPO_TRCH);
    }

    public void setIspc0211FlgItnFormatted(int ispc0211FlgItnIdx, String ispc0211FlgItn) {
        int position = Pos.ispc0211FlgItn(ispc0211FlgItnIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ispc0211FlgItn, Len.FLG_ITN), Len.FLG_ITN);
    }

    /**Original name: ISPC0211-FLG-ITN<br>*/
    public short getIspc0211FlgItn(int ispc0211FlgItnIdx) {
        int position = Pos.ispc0211FlgItn(ispc0211FlgItnIdx - 1);
        return readNumDispUnsignedShort(position, Len.FLG_ITN);
    }

    public void setIspc0211NumComponMaxEleT(int ispc0211NumComponMaxEleTIdx, short ispc0211NumComponMaxEleT) {
        int position = Pos.ispc0211NumComponMaxEleT(ispc0211NumComponMaxEleTIdx - 1);
        writeShort(position, ispc0211NumComponMaxEleT, Len.Int.ISPC0211_NUM_COMPON_MAX_ELE_T, SignType.NO_SIGN);
    }

    public void setIspc0211NumComponMaxEleTFormatted(int ispc0211NumComponMaxEleTIdx, String ispc0211NumComponMaxEleT) {
        int position = Pos.ispc0211NumComponMaxEleT(ispc0211NumComponMaxEleTIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ispc0211NumComponMaxEleT, Len.NUM_COMPON_MAX_ELE_T), Len.NUM_COMPON_MAX_ELE_T);
    }

    /**Original name: ISPC0211-NUM-COMPON-MAX-ELE-T<br>*/
    public short getIspc0211NumComponMaxEleT(int ispc0211NumComponMaxEleTIdx) {
        int position = Pos.ispc0211NumComponMaxEleT(ispc0211NumComponMaxEleTIdx - 1);
        return readNumDispUnsignedShort(position, Len.NUM_COMPON_MAX_ELE_T);
    }

    public void setIspc0211CodiceVariabileT(int ispc0211CodiceVariabileTIdx1, int ispc0211CodiceVariabileTIdx2, String ispc0211CodiceVariabileT) {
        int position = Pos.ispc0211CodiceVariabileT(ispc0211CodiceVariabileTIdx1 - 1, ispc0211CodiceVariabileTIdx2 - 1);
        writeString(position, ispc0211CodiceVariabileT, Len.CODICE_VARIABILE_T);
    }

    /**Original name: ISPC0211-CODICE-VARIABILE-T<br>*/
    public String getIspc0211CodiceVariabileT(int ispc0211CodiceVariabileTIdx1, int ispc0211CodiceVariabileTIdx2) {
        int position = Pos.ispc0211CodiceVariabileT(ispc0211CodiceVariabileTIdx1 - 1, ispc0211CodiceVariabileTIdx2 - 1);
        return readString(position, Len.CODICE_VARIABILE_T);
    }

    public void setIspc0211TipoDatoT(int ispc0211TipoDatoTIdx1, int ispc0211TipoDatoTIdx2, char ispc0211TipoDatoT) {
        int position = Pos.ispc0211TipoDatoT(ispc0211TipoDatoTIdx1 - 1, ispc0211TipoDatoTIdx2 - 1);
        writeChar(position, ispc0211TipoDatoT);
    }

    /**Original name: ISPC0211-TIPO-DATO-T<br>*/
    public char getIspc0211TipoDatoT(int ispc0211TipoDatoTIdx1, int ispc0211TipoDatoTIdx2) {
        int position = Pos.ispc0211TipoDatoT(ispc0211TipoDatoTIdx1 - 1, ispc0211TipoDatoTIdx2 - 1);
        return readChar(position);
    }

    public void setIspc0211ValGenericoT(int ispc0211ValGenericoTIdx1, int ispc0211ValGenericoTIdx2, String ispc0211ValGenericoT) {
        int position = Pos.ispc0211ValGenericoT(ispc0211ValGenericoTIdx1 - 1, ispc0211ValGenericoTIdx2 - 1);
        writeString(position, ispc0211ValGenericoT, Len.VAL_GENERICO_T);
    }

    /**Original name: ISPC0211-VAL-GENERICO-T<br>*/
    public String getIspc0211ValGenericoT(int ispc0211ValGenericoTIdx1, int ispc0211ValGenericoTIdx2) {
        int position = Pos.ispc0211ValGenericoT(ispc0211ValGenericoTIdx1 - 1, ispc0211ValGenericoTIdx2 - 1);
        return readString(position, Len.VAL_GENERICO_T);
    }

    /**Original name: ISPC0211-VALORE-IMP-T<br>*/
    public AfDecimal getIspc0211ValoreImpT(int ispc0211ValoreImpTIdx1, int ispc0211ValoreImpTIdx2) {
        int position = Pos.ispc0211ValoreImpT(ispc0211ValoreImpTIdx1 - 1, ispc0211ValoreImpTIdx2 - 1);
        return readDecimal(position, Len.Int.ISPC0211_VALORE_IMP_T, Len.Fract.ISPC0211_VALORE_IMP_T);
    }

    /**Original name: ISPC0211-VALORE-PERC-T<br>*/
    public AfDecimal getIspc0211ValorePercT(int ispc0211ValorePercTIdx1, int ispc0211ValorePercTIdx2) {
        int position = Pos.ispc0211ValorePercT(ispc0211ValorePercTIdx1 - 1, ispc0211ValorePercTIdx2 - 1);
        return readDecimal(position, Len.Int.ISPC0211_VALORE_PERC_T, Len.Fract.ISPC0211_VALORE_PERC_T, SignType.NO_SIGN);
    }

    public String getIspc0211TabSchedaRTFormatted() {
        return readFixedString(Pos.ISPC0211_TAB_SCHEDA_R_T, Len.ISPC0211_TAB_SCHEDA_R_T);
    }

    public void setIspc0211RestoTabSchedaT(String ispc0211RestoTabSchedaT) {
        writeString(Pos.RESTO_TAB_SCHEDA_T, ispc0211RestoTabSchedaT, Len.ISPC0211_RESTO_TAB_SCHEDA_T);
    }

    /**Original name: ISPC0211-RESTO-TAB-SCHEDA-T<br>*/
    public String getIspc0211RestoTabSchedaT() {
        return readString(Pos.RESTO_TAB_SCHEDA_T, Len.ISPC0211_RESTO_TAB_SCHEDA_T);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISPC0211_TAB_VAL_T = 1;
        public static final int ISPC0211_TAB_SCHEDA_R_T = 1;
        public static final int FLR1 = ISPC0211_TAB_SCHEDA_R_T;
        public static final int RESTO_TAB_SCHEDA_T = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int ispc0211TabSchedaT(int idx) {
            return ISPC0211_TAB_VAL_T + idx * Len.TAB_SCHEDA_T;
        }

        public static int ispc0211TipoLivelloT(int idx) {
            return ispc0211TabSchedaT(idx);
        }

        public static int ispc0211CodiceLivelloT(int idx) {
            return ispc0211TipoLivelloT(idx) + Len.TIPO_LIVELLO_T;
        }

        public static int ispc0211IdentLivelloT(int idx) {
            return ispc0211CodiceLivelloT(idx) + Len.CODICE_LIVELLO_T;
        }

        public static int ispc0211DtInizTari(int idx) {
            return ispc0211IdentLivelloT(idx) + Len.IDENT_LIVELLO_T;
        }

        public static int ispc0211CodRgmFiscT(int idx) {
            return ispc0211DtInizTari(idx) + Len.DT_INIZ_TARI;
        }

        public static int ispc0211DtDecorTrch(int idx) {
            return ispc0211CodRgmFiscT(idx) + Len.COD_RGM_FISC_T;
        }

        public static int ispc0211FlgLiqT(int idx) {
            return ispc0211DtDecorTrch(idx) + Len.DT_DECOR_TRCH;
        }

        public static int ispc0211CodiceOpzioneT(int idx) {
            return ispc0211FlgLiqT(idx) + Len.FLG_LIQ_T;
        }

        public static int ispc0211TipoTrch(int idx) {
            return ispc0211CodiceOpzioneT(idx) + Len.CODICE_OPZIONE_T;
        }

        public static int ispc0211FlgItn(int idx) {
            return ispc0211TipoTrch(idx) + Len.TIPO_TRCH;
        }

        public static int ispc0211NumComponMaxEleT(int idx) {
            return ispc0211FlgItn(idx) + Len.FLG_ITN;
        }

        public static int ispc0211AreaVariabiliT(int idx1, int idx2) {
            return ispc0211NumComponMaxEleT(idx1) + Len.NUM_COMPON_MAX_ELE_T + idx2 * Len.AREA_VARIABILI_T;
        }

        public static int ispc0211CodiceVariabileT(int idx1, int idx2) {
            return ispc0211AreaVariabiliT(idx1, idx2);
        }

        public static int ispc0211TipoDatoT(int idx1, int idx2) {
            return ispc0211CodiceVariabileT(idx1, idx2) + Len.CODICE_VARIABILE_T;
        }

        public static int ispc0211ValGenericoT(int idx1, int idx2) {
            return ispc0211TipoDatoT(idx1, idx2) + Len.TIPO_DATO_T;
        }

        public static int ispc0211ValImpGenT(int idx1, int idx2) {
            return ispc0211ValGenericoT(idx1, idx2);
        }

        public static int ispc0211ValoreImpT(int idx1, int idx2) {
            return ispc0211ValImpGenT(idx1, idx2);
        }

        public static int ispc0211ValImpFillerT(int idx1, int idx2) {
            return ispc0211ValoreImpT(idx1, idx2) + Len.VALORE_IMP_T;
        }

        public static int ispc0211ValPercGenT(int idx1, int idx2) {
            return ispc0211ValGenericoT(idx1, idx2);
        }

        public static int ispc0211ValorePercT(int idx1, int idx2) {
            return ispc0211ValPercGenT(idx1, idx2);
        }

        public static int ispc0211ValPercFillerT(int idx1, int idx2) {
            return ispc0211ValorePercT(idx1, idx2) + Len.VALORE_PERC_T;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_LIVELLO_T = 1;
        public static final int CODICE_LIVELLO_T = 12;
        public static final int IDENT_LIVELLO_T = 9;
        public static final int DT_INIZ_TARI = 8;
        public static final int COD_RGM_FISC_T = 2;
        public static final int DT_DECOR_TRCH = 8;
        public static final int FLG_LIQ_T = 2;
        public static final int CODICE_OPZIONE_T = 2;
        public static final int TIPO_TRCH = 2;
        public static final int FLG_ITN = 1;
        public static final int NUM_COMPON_MAX_ELE_T = 4;
        public static final int CODICE_VARIABILE_T = 12;
        public static final int TIPO_DATO_T = 1;
        public static final int VAL_GENERICO_T = 60;
        public static final int AREA_VARIABILI_T = CODICE_VARIABILE_T + TIPO_DATO_T + VAL_GENERICO_T;
        public static final int TAB_SCHEDA_T = TIPO_LIVELLO_T + CODICE_LIVELLO_T + IDENT_LIVELLO_T + DT_INIZ_TARI + COD_RGM_FISC_T + DT_DECOR_TRCH + FLG_LIQ_T + CODICE_OPZIONE_T + TIPO_TRCH + FLG_ITN + NUM_COMPON_MAX_ELE_T + Ispc0211TabValT.AREA_VARIABILI_T_MAXOCCURS * AREA_VARIABILI_T;
        public static final int VALORE_IMP_T = 18;
        public static final int VALORE_PERC_T = 14;
        public static final int FLR1 = 5526;
        public static final int ISPC0211_TAB_VAL_T = Ispc0211TabValT.TAB_SCHEDA_T_MAXOCCURS * TAB_SCHEDA_T;
        public static final int ISPC0211_RESTO_TAB_SCHEDA_T = 4138974;
        public static final int ISPC0211_TAB_SCHEDA_R_T = ISPC0211_RESTO_TAB_SCHEDA_T + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISPC0211_NUM_COMPON_MAX_ELE_T = 4;
            public static final int ISPC0211_VALORE_IMP_T = 11;
            public static final int ISPC0211_VALORE_PERC_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISPC0211_VALORE_IMP_T = 7;
            public static final int ISPC0211_VALORE_PERC_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
