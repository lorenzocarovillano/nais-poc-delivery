package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-PRE-C<br>
 * Variable: PCO-DT-ULT-BOLL-PRE-C from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollPreC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollPreC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_PRE_C;
    }

    public void setPcoDtUltBollPreC(int pcoDtUltBollPreC) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_PRE_C, pcoDtUltBollPreC, Len.Int.PCO_DT_ULT_BOLL_PRE_C);
    }

    public void setPcoDtUltBollPreCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_PRE_C, Pos.PCO_DT_ULT_BOLL_PRE_C);
    }

    /**Original name: PCO-DT-ULT-BOLL-PRE-C<br>*/
    public int getPcoDtUltBollPreC() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_PRE_C, Len.Int.PCO_DT_ULT_BOLL_PRE_C);
    }

    public byte[] getPcoDtUltBollPreCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_PRE_C, Pos.PCO_DT_ULT_BOLL_PRE_C);
        return buffer;
    }

    public void initPcoDtUltBollPreCHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_PRE_C, Len.PCO_DT_ULT_BOLL_PRE_C, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollPreCNull(String pcoDtUltBollPreCNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_PRE_C_NULL, pcoDtUltBollPreCNull, Len.PCO_DT_ULT_BOLL_PRE_C_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-PRE-C-NULL<br>*/
    public String getPcoDtUltBollPreCNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_PRE_C_NULL, Len.PCO_DT_ULT_BOLL_PRE_C_NULL);
    }

    public String getPcoDtUltBollPreCNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollPreCNull(), Len.PCO_DT_ULT_BOLL_PRE_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_PRE_C = 1;
        public static final int PCO_DT_ULT_BOLL_PRE_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_PRE_C = 5;
        public static final int PCO_DT_ULT_BOLL_PRE_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_PRE_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
