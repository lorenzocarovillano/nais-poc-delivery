package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPOG-VAL-DT<br>
 * Variable: WPOG-VAL-DT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpogValDt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpogValDt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOG_VAL_DT;
    }

    public void setWpogValDt(int wpogValDt) {
        writeIntAsPacked(Pos.WPOG_VAL_DT, wpogValDt, Len.Int.WPOG_VAL_DT);
    }

    public void setWpogValDtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOG_VAL_DT, Pos.WPOG_VAL_DT);
    }

    /**Original name: WPOG-VAL-DT<br>*/
    public int getWpogValDt() {
        return readPackedAsInt(Pos.WPOG_VAL_DT, Len.Int.WPOG_VAL_DT);
    }

    public String getDpogValDtFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getWpogValDt()).toString();
    }

    public byte[] getWpogValDtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOG_VAL_DT, Pos.WPOG_VAL_DT);
        return buffer;
    }

    public void initWpogValDtSpaces() {
        fill(Pos.WPOG_VAL_DT, Len.WPOG_VAL_DT, Types.SPACE_CHAR);
    }

    public void setWpogValDtNull(String wpogValDtNull) {
        writeString(Pos.WPOG_VAL_DT_NULL, wpogValDtNull, Len.WPOG_VAL_DT_NULL);
    }

    /**Original name: WPOG-VAL-DT-NULL<br>*/
    public String getWpogValDtNull() {
        return readString(Pos.WPOG_VAL_DT_NULL, Len.WPOG_VAL_DT_NULL);
    }

    public String getDpogValDtNullFormatted() {
        return Functions.padBlanks(getWpogValDtNull(), Len.WPOG_VAL_DT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOG_VAL_DT = 1;
        public static final int WPOG_VAL_DT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOG_VAL_DT = 5;
        public static final int WPOG_VAL_DT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOG_VAL_DT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
