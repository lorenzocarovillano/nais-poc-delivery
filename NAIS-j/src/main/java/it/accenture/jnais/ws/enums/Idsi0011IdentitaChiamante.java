package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSI0011-IDENTITA-CHIAMANTE<br>
 * Variable: IDSI0011-IDENTITA-CHIAMANTE from copybook IDSI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsi0011IdentitaChiamante {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.IDSI0011_IDENTITA_CHIAMANTE);
    public static final String PTF_NEWLIFE = "LPF";
    public static final String REFACTORING = "REF";
    public static final String CONVERSIONE = "CON";

    //==== METHODS ====
    public void setIdsi0011IdentitaChiamante(String idsi0011IdentitaChiamante) {
        this.value = Functions.subString(idsi0011IdentitaChiamante, Len.IDSI0011_IDENTITA_CHIAMANTE);
    }

    public String getIdsi0011IdentitaChiamante() {
        return this.value;
    }

    public void setPtfNewlife() {
        value = PTF_NEWLIFE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSI0011_IDENTITA_CHIAMANTE = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
