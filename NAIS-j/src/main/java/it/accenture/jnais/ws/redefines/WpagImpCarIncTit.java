package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPAG-IMP-CAR-INC-TIT<br>
 * Variable: WPAG-IMP-CAR-INC-TIT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpCarIncTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpCarIncTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_CAR_INC_TIT;
    }

    public void setWpagImpCarIncTit(AfDecimal wpagImpCarIncTit) {
        writeDecimalAsPacked(Pos.WPAG_IMP_CAR_INC_TIT, wpagImpCarIncTit.copy());
    }

    public void setWpagImpCarIncTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_CAR_INC_TIT, Pos.WPAG_IMP_CAR_INC_TIT);
    }

    /**Original name: WPAG-IMP-CAR-INC-TIT<br>*/
    public AfDecimal getWpagImpCarIncTit() {
        return readPackedAsDecimal(Pos.WPAG_IMP_CAR_INC_TIT, Len.Int.WPAG_IMP_CAR_INC_TIT, Len.Fract.WPAG_IMP_CAR_INC_TIT);
    }

    public byte[] getWpagImpCarIncTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_CAR_INC_TIT, Pos.WPAG_IMP_CAR_INC_TIT);
        return buffer;
    }

    public void initWpagImpCarIncTitSpaces() {
        fill(Pos.WPAG_IMP_CAR_INC_TIT, Len.WPAG_IMP_CAR_INC_TIT, Types.SPACE_CHAR);
    }

    public void setWpagImpCarIncTitNull(String wpagImpCarIncTitNull) {
        writeString(Pos.WPAG_IMP_CAR_INC_TIT_NULL, wpagImpCarIncTitNull, Len.WPAG_IMP_CAR_INC_TIT_NULL);
    }

    /**Original name: WPAG-IMP-CAR-INC-TIT-NULL<br>*/
    public String getWpagImpCarIncTitNull() {
        return readString(Pos.WPAG_IMP_CAR_INC_TIT_NULL, Len.WPAG_IMP_CAR_INC_TIT_NULL);
    }

    public String getWpagImpCarIncTitNullFormatted() {
        return Functions.padBlanks(getWpagImpCarIncTitNull(), Len.WPAG_IMP_CAR_INC_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_INC_TIT = 1;
        public static final int WPAG_IMP_CAR_INC_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_INC_TIT = 8;
        public static final int WPAG_IMP_CAR_INC_TIT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_INC_TIT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_INC_TIT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
