package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import it.accenture.jnais.ws.redefines.WpagContAcq1oAnno;
import it.accenture.jnais.ws.redefines.WpagContAcq2oAnno;
import it.accenture.jnais.ws.redefines.WpagContAltriSopr;
import it.accenture.jnais.ws.redefines.WpagContDiritti;
import it.accenture.jnais.ws.redefines.WpagContImpAcqExp;
import it.accenture.jnais.ws.redefines.WpagContImpCommisInt;
import it.accenture.jnais.ws.redefines.WpagContImpRenAss;
import it.accenture.jnais.ws.redefines.WpagContIncas;
import it.accenture.jnais.ws.redefines.WpagContIntFraz;
import it.accenture.jnais.ws.redefines.WpagContIntRetrodt;
import it.accenture.jnais.ws.redefines.WpagContPremioNetto;
import it.accenture.jnais.ws.redefines.WpagContPremioPuroIas;
import it.accenture.jnais.ws.redefines.WpagContPremioRisc;
import it.accenture.jnais.ws.redefines.WpagContPremioTot;
import it.accenture.jnais.ws.redefines.WpagContRicor;
import it.accenture.jnais.ws.redefines.WpagContSoprProfes;
import it.accenture.jnais.ws.redefines.WpagContSoprSanit;
import it.accenture.jnais.ws.redefines.WpagContSoprSport;
import it.accenture.jnais.ws.redefines.WpagContSoprTecn;
import it.accenture.jnais.ws.redefines.WpagContSpeseMediche;
import it.accenture.jnais.ws.redefines.WpagContTasse;
import it.accenture.jnais.ws.redefines.WpagImpAderTit;
import it.accenture.jnais.ws.redefines.WpagImpAzTit;
import it.accenture.jnais.ws.redefines.WpagImpCarAcqTit;
import it.accenture.jnais.ws.redefines.WpagImpCarGestTit;
import it.accenture.jnais.ws.redefines.WpagImpCarIncTit;
import it.accenture.jnais.ws.redefines.WpagImpTfrTit;
import it.accenture.jnais.ws.redefines.WpagImpVoloTit;

/**Original name: WPAG-DATI-TIT-CONT<br>
 * Variables: WPAG-DATI-TIT-CONT from copybook LVEC0268<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpagDatiTitCont {

    //==== PROPERTIES ====
    //Original name: WPAG-CONT-PREMIO-NETTO
    private WpagContPremioNetto wpagContPremioNetto = new WpagContPremioNetto();
    //Original name: WPAG-CONT-INT-FRAZ
    private WpagContIntFraz wpagContIntFraz = new WpagContIntFraz();
    //Original name: WPAG-CONT-INT-RETRODT
    private WpagContIntRetrodt wpagContIntRetrodt = new WpagContIntRetrodt();
    //Original name: WPAG-CONT-DIRITTI
    private WpagContDiritti wpagContDiritti = new WpagContDiritti();
    //Original name: WPAG-CONT-SPESE-MEDICHE
    private WpagContSpeseMediche wpagContSpeseMediche = new WpagContSpeseMediche();
    //Original name: WPAG-CONT-TASSE
    private WpagContTasse wpagContTasse = new WpagContTasse();
    //Original name: WPAG-CONT-SOPR-SANIT
    private WpagContSoprSanit wpagContSoprSanit = new WpagContSoprSanit();
    //Original name: WPAG-CONT-SOPR-PROFES
    private WpagContSoprProfes wpagContSoprProfes = new WpagContSoprProfes();
    //Original name: WPAG-CONT-SOPR-SPORT
    private WpagContSoprSport wpagContSoprSport = new WpagContSoprSport();
    //Original name: WPAG-CONT-SOPR-TECN
    private WpagContSoprTecn wpagContSoprTecn = new WpagContSoprTecn();
    //Original name: WPAG-CONT-ALTRI-SOPR
    private WpagContAltriSopr wpagContAltriSopr = new WpagContAltriSopr();
    //Original name: WPAG-CONT-PREMIO-TOT
    private WpagContPremioTot wpagContPremioTot = new WpagContPremioTot();
    //Original name: WPAG-CONT-PREMIO-PURO-IAS
    private WpagContPremioPuroIas wpagContPremioPuroIas = new WpagContPremioPuroIas();
    //Original name: WPAG-CONT-PREMIO-RISC
    private WpagContPremioRisc wpagContPremioRisc = new WpagContPremioRisc();
    //Original name: WPAG-IMP-CAR-ACQ-TIT
    private WpagImpCarAcqTit wpagImpCarAcqTit = new WpagImpCarAcqTit();
    //Original name: WPAG-IMP-CAR-INC-TIT
    private WpagImpCarIncTit wpagImpCarIncTit = new WpagImpCarIncTit();
    //Original name: WPAG-IMP-CAR-GEST-TIT
    private WpagImpCarGestTit wpagImpCarGestTit = new WpagImpCarGestTit();
    //Original name: WPAG-CONT-ACQ-1O-ANNO
    private WpagContAcq1oAnno wpagContAcq1oAnno = new WpagContAcq1oAnno();
    //Original name: WPAG-CONT-ACQ-2O-ANNO
    private WpagContAcq2oAnno wpagContAcq2oAnno = new WpagContAcq2oAnno();
    //Original name: WPAG-CONT-RICOR
    private WpagContRicor wpagContRicor = new WpagContRicor();
    //Original name: WPAG-CONT-INCAS
    private WpagContIncas wpagContIncas = new WpagContIncas();
    //Original name: WPAG-IMP-AZ-TIT
    private WpagImpAzTit wpagImpAzTit = new WpagImpAzTit();
    //Original name: WPAG-IMP-ADER-TIT
    private WpagImpAderTit wpagImpAderTit = new WpagImpAderTit();
    //Original name: WPAG-IMP-TFR-TIT
    private WpagImpTfrTit wpagImpTfrTit = new WpagImpTfrTit();
    //Original name: WPAG-IMP-VOLO-TIT
    private WpagImpVoloTit wpagImpVoloTit = new WpagImpVoloTit();
    //Original name: WPAG-CONT-IMP-ACQ-EXP
    private WpagContImpAcqExp wpagContImpAcqExp = new WpagContImpAcqExp();
    //Original name: WPAG-CONT-IMP-REN-ASS
    private WpagContImpRenAss wpagContImpRenAss = new WpagContImpRenAss();
    //Original name: WPAG-CONT-IMP-COMMIS-INT
    private WpagContImpCommisInt wpagContImpCommisInt = new WpagContImpCommisInt();
    //Original name: WPAG-CONT-ANTIRACKET
    private AfDecimal wpagContAntiracket = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WPAG-CONT-AUTOGEN-INC
    private char wpagContAutogenInc = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setWpagDatiTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagContPremioNetto.setWpagContPremioNettoFromBuffer(buffer, position);
        position += WpagContPremioNetto.Len.WPAG_CONT_PREMIO_NETTO;
        wpagContIntFraz.setWpagContIntFrazFromBuffer(buffer, position);
        position += WpagContIntFraz.Len.WPAG_CONT_INT_FRAZ;
        wpagContIntRetrodt.setWpagContIntRetrodtFromBuffer(buffer, position);
        position += WpagContIntRetrodt.Len.WPAG_CONT_INT_RETRODT;
        wpagContDiritti.setWpagContDirittiFromBuffer(buffer, position);
        position += WpagContDiritti.Len.WPAG_CONT_DIRITTI;
        wpagContSpeseMediche.setWpagContSpeseMedicheFromBuffer(buffer, position);
        position += WpagContSpeseMediche.Len.WPAG_CONT_SPESE_MEDICHE;
        wpagContTasse.setWpagContTasseFromBuffer(buffer, position);
        position += WpagContTasse.Len.WPAG_CONT_TASSE;
        wpagContSoprSanit.setWpagContSoprSanitFromBuffer(buffer, position);
        position += WpagContSoprSanit.Len.WPAG_CONT_SOPR_SANIT;
        wpagContSoprProfes.setWpagContSoprProfesFromBuffer(buffer, position);
        position += WpagContSoprProfes.Len.WPAG_CONT_SOPR_PROFES;
        wpagContSoprSport.setWpagContSoprSportFromBuffer(buffer, position);
        position += WpagContSoprSport.Len.WPAG_CONT_SOPR_SPORT;
        wpagContSoprTecn.setWpagContSoprTecnFromBuffer(buffer, position);
        position += WpagContSoprTecn.Len.WPAG_CONT_SOPR_TECN;
        wpagContAltriSopr.setWpagContAltriSoprFromBuffer(buffer, position);
        position += WpagContAltriSopr.Len.WPAG_CONT_ALTRI_SOPR;
        wpagContPremioTot.setWpagContPremioTotFromBuffer(buffer, position);
        position += WpagContPremioTot.Len.WPAG_CONT_PREMIO_TOT;
        wpagContPremioPuroIas.setWpagContPremioPuroIasFromBuffer(buffer, position);
        position += WpagContPremioPuroIas.Len.WPAG_CONT_PREMIO_PURO_IAS;
        wpagContPremioRisc.setWpagContPremioRiscFromBuffer(buffer, position);
        position += WpagContPremioRisc.Len.WPAG_CONT_PREMIO_RISC;
        wpagImpCarAcqTit.setWpagImpCarAcqTitFromBuffer(buffer, position);
        position += WpagImpCarAcqTit.Len.WPAG_IMP_CAR_ACQ_TIT;
        wpagImpCarIncTit.setWpagImpCarIncTitFromBuffer(buffer, position);
        position += WpagImpCarIncTit.Len.WPAG_IMP_CAR_INC_TIT;
        wpagImpCarGestTit.setWpagImpCarGestTitFromBuffer(buffer, position);
        position += WpagImpCarGestTit.Len.WPAG_IMP_CAR_GEST_TIT;
        wpagContAcq1oAnno.setWpagContAcq1oAnnoFromBuffer(buffer, position);
        position += WpagContAcq1oAnno.Len.WPAG_CONT_ACQ1O_ANNO;
        wpagContAcq2oAnno.setWpagContAcq2oAnnoFromBuffer(buffer, position);
        position += WpagContAcq2oAnno.Len.WPAG_CONT_ACQ2O_ANNO;
        wpagContRicor.setWpagContRicorFromBuffer(buffer, position);
        position += WpagContRicor.Len.WPAG_CONT_RICOR;
        wpagContIncas.setWpagContIncasFromBuffer(buffer, position);
        position += WpagContIncas.Len.WPAG_CONT_INCAS;
        wpagImpAzTit.setWpagImpAzTitFromBuffer(buffer, position);
        position += WpagImpAzTit.Len.WPAG_IMP_AZ_TIT;
        wpagImpAderTit.setWpagImpAderTitFromBuffer(buffer, position);
        position += WpagImpAderTit.Len.WPAG_IMP_ADER_TIT;
        wpagImpTfrTit.setWpagImpTfrTitFromBuffer(buffer, position);
        position += WpagImpTfrTit.Len.WPAG_IMP_TFR_TIT;
        wpagImpVoloTit.setWpagImpVoloTitFromBuffer(buffer, position);
        position += WpagImpVoloTit.Len.WPAG_IMP_VOLO_TIT;
        wpagContImpAcqExp.setWpagContImpAcqExpFromBuffer(buffer, position);
        position += WpagContImpAcqExp.Len.WPAG_CONT_IMP_ACQ_EXP;
        wpagContImpRenAss.setWpagContImpRenAssFromBuffer(buffer, position);
        position += WpagContImpRenAss.Len.WPAG_CONT_IMP_REN_ASS;
        wpagContImpCommisInt.setWpagContImpCommisIntFromBuffer(buffer, position);
        position += WpagContImpCommisInt.Len.WPAG_CONT_IMP_COMMIS_INT;
        wpagContAntiracket.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WPAG_CONT_ANTIRACKET, Len.Fract.WPAG_CONT_ANTIRACKET));
        position += Len.WPAG_CONT_ANTIRACKET;
        wpagContAutogenInc = MarshalByte.readChar(buffer, position);
    }

    public byte[] getWpagDatiTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagContPremioNetto.getWpagContPremioNettoAsBuffer(buffer, position);
        position += WpagContPremioNetto.Len.WPAG_CONT_PREMIO_NETTO;
        wpagContIntFraz.getWpagContIntFrazAsBuffer(buffer, position);
        position += WpagContIntFraz.Len.WPAG_CONT_INT_FRAZ;
        wpagContIntRetrodt.getWpagContIntRetrodtAsBuffer(buffer, position);
        position += WpagContIntRetrodt.Len.WPAG_CONT_INT_RETRODT;
        wpagContDiritti.getWpagContDirittiAsBuffer(buffer, position);
        position += WpagContDiritti.Len.WPAG_CONT_DIRITTI;
        wpagContSpeseMediche.getWpagContSpeseMedicheAsBuffer(buffer, position);
        position += WpagContSpeseMediche.Len.WPAG_CONT_SPESE_MEDICHE;
        wpagContTasse.getWpagContTasseAsBuffer(buffer, position);
        position += WpagContTasse.Len.WPAG_CONT_TASSE;
        wpagContSoprSanit.getWpagContSoprSanitAsBuffer(buffer, position);
        position += WpagContSoprSanit.Len.WPAG_CONT_SOPR_SANIT;
        wpagContSoprProfes.getWpagContSoprProfesAsBuffer(buffer, position);
        position += WpagContSoprProfes.Len.WPAG_CONT_SOPR_PROFES;
        wpagContSoprSport.getWpagContSoprSportAsBuffer(buffer, position);
        position += WpagContSoprSport.Len.WPAG_CONT_SOPR_SPORT;
        wpagContSoprTecn.getWpagContSoprTecnAsBuffer(buffer, position);
        position += WpagContSoprTecn.Len.WPAG_CONT_SOPR_TECN;
        wpagContAltriSopr.getWpagContAltriSoprAsBuffer(buffer, position);
        position += WpagContAltriSopr.Len.WPAG_CONT_ALTRI_SOPR;
        wpagContPremioTot.getWpagContPremioTotAsBuffer(buffer, position);
        position += WpagContPremioTot.Len.WPAG_CONT_PREMIO_TOT;
        wpagContPremioPuroIas.getWpagContPremioPuroIasAsBuffer(buffer, position);
        position += WpagContPremioPuroIas.Len.WPAG_CONT_PREMIO_PURO_IAS;
        wpagContPremioRisc.getWpagContPremioRiscAsBuffer(buffer, position);
        position += WpagContPremioRisc.Len.WPAG_CONT_PREMIO_RISC;
        wpagImpCarAcqTit.getWpagImpCarAcqTitAsBuffer(buffer, position);
        position += WpagImpCarAcqTit.Len.WPAG_IMP_CAR_ACQ_TIT;
        wpagImpCarIncTit.getWpagImpCarIncTitAsBuffer(buffer, position);
        position += WpagImpCarIncTit.Len.WPAG_IMP_CAR_INC_TIT;
        wpagImpCarGestTit.getWpagImpCarGestTitAsBuffer(buffer, position);
        position += WpagImpCarGestTit.Len.WPAG_IMP_CAR_GEST_TIT;
        wpagContAcq1oAnno.getWpagContAcq1oAnnoAsBuffer(buffer, position);
        position += WpagContAcq1oAnno.Len.WPAG_CONT_ACQ1O_ANNO;
        wpagContAcq2oAnno.getWpagContAcq2oAnnoAsBuffer(buffer, position);
        position += WpagContAcq2oAnno.Len.WPAG_CONT_ACQ2O_ANNO;
        wpagContRicor.getWpagContRicorAsBuffer(buffer, position);
        position += WpagContRicor.Len.WPAG_CONT_RICOR;
        wpagContIncas.getWpagContIncasAsBuffer(buffer, position);
        position += WpagContIncas.Len.WPAG_CONT_INCAS;
        wpagImpAzTit.getWpagImpAzTitAsBuffer(buffer, position);
        position += WpagImpAzTit.Len.WPAG_IMP_AZ_TIT;
        wpagImpAderTit.getWpagImpAderTitAsBuffer(buffer, position);
        position += WpagImpAderTit.Len.WPAG_IMP_ADER_TIT;
        wpagImpTfrTit.getWpagImpTfrTitAsBuffer(buffer, position);
        position += WpagImpTfrTit.Len.WPAG_IMP_TFR_TIT;
        wpagImpVoloTit.getWpagImpVoloTitAsBuffer(buffer, position);
        position += WpagImpVoloTit.Len.WPAG_IMP_VOLO_TIT;
        wpagContImpAcqExp.getWpagContImpAcqExpAsBuffer(buffer, position);
        position += WpagContImpAcqExp.Len.WPAG_CONT_IMP_ACQ_EXP;
        wpagContImpRenAss.getWpagContImpRenAssAsBuffer(buffer, position);
        position += WpagContImpRenAss.Len.WPAG_CONT_IMP_REN_ASS;
        wpagContImpCommisInt.getWpagContImpCommisIntAsBuffer(buffer, position);
        position += WpagContImpCommisInt.Len.WPAG_CONT_IMP_COMMIS_INT;
        MarshalByte.writeDecimalAsPacked(buffer, position, wpagContAntiracket.copy());
        position += Len.WPAG_CONT_ANTIRACKET;
        MarshalByte.writeChar(buffer, position, wpagContAutogenInc);
        return buffer;
    }

    public void initWpagDatiTitContSpaces() {
        wpagContPremioNetto.initWpagContPremioNettoSpaces();
        wpagContIntFraz.initWpagContIntFrazSpaces();
        wpagContIntRetrodt.initWpagContIntRetrodtSpaces();
        wpagContDiritti.initWpagContDirittiSpaces();
        wpagContSpeseMediche.initWpagContSpeseMedicheSpaces();
        wpagContTasse.initWpagContTasseSpaces();
        wpagContSoprSanit.initWpagContSoprSanitSpaces();
        wpagContSoprProfes.initWpagContSoprProfesSpaces();
        wpagContSoprSport.initWpagContSoprSportSpaces();
        wpagContSoprTecn.initWpagContSoprTecnSpaces();
        wpagContAltriSopr.initWpagContAltriSoprSpaces();
        wpagContPremioTot.initWpagContPremioTotSpaces();
        wpagContPremioPuroIas.initWpagContPremioPuroIasSpaces();
        wpagContPremioRisc.initWpagContPremioRiscSpaces();
        wpagImpCarAcqTit.initWpagImpCarAcqTitSpaces();
        wpagImpCarIncTit.initWpagImpCarIncTitSpaces();
        wpagImpCarGestTit.initWpagImpCarGestTitSpaces();
        wpagContAcq1oAnno.initWpagContAcq1oAnnoSpaces();
        wpagContAcq2oAnno.initWpagContAcq2oAnnoSpaces();
        wpagContRicor.initWpagContRicorSpaces();
        wpagContIncas.initWpagContIncasSpaces();
        wpagImpAzTit.initWpagImpAzTitSpaces();
        wpagImpAderTit.initWpagImpAderTitSpaces();
        wpagImpTfrTit.initWpagImpTfrTitSpaces();
        wpagImpVoloTit.initWpagImpVoloTitSpaces();
        wpagContImpAcqExp.initWpagContImpAcqExpSpaces();
        wpagContImpRenAss.initWpagContImpRenAssSpaces();
        wpagContImpCommisInt.initWpagContImpCommisIntSpaces();
        wpagContAntiracket.setNaN();
        wpagContAutogenInc = Types.SPACE_CHAR;
    }

    public void setWpagContAntiracket(AfDecimal wpagContAntiracket) {
        this.wpagContAntiracket.assign(wpagContAntiracket);
    }

    public void setWpagContAntiracketFormatted(String wpagContAntiracket) {
        setWpagContAntiracket(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_ANTIRACKET + Len.Fract.WPAG_CONT_ANTIRACKET, Len.Fract.WPAG_CONT_ANTIRACKET, wpagContAntiracket));
    }

    public AfDecimal getWpagContAntiracket() {
        return this.wpagContAntiracket.copy();
    }

    public void setWpagContAutogenInc(char wpagContAutogenInc) {
        this.wpagContAutogenInc = wpagContAutogenInc;
    }

    public char getWpagContAutogenInc() {
        return this.wpagContAutogenInc;
    }

    public WpagContAcq1oAnno getWpagContAcq1oAnno() {
        return wpagContAcq1oAnno;
    }

    public WpagContAcq2oAnno getWpagContAcq2oAnno() {
        return wpagContAcq2oAnno;
    }

    public WpagContAltriSopr getWpagContAltriSopr() {
        return wpagContAltriSopr;
    }

    public WpagContDiritti getWpagContDiritti() {
        return wpagContDiritti;
    }

    public WpagContImpAcqExp getWpagContImpAcqExp() {
        return wpagContImpAcqExp;
    }

    public WpagContImpCommisInt getWpagContImpCommisInt() {
        return wpagContImpCommisInt;
    }

    public WpagContImpRenAss getWpagContImpRenAss() {
        return wpagContImpRenAss;
    }

    public WpagContIncas getWpagContIncas() {
        return wpagContIncas;
    }

    public WpagContIntFraz getWpagContIntFraz() {
        return wpagContIntFraz;
    }

    public WpagContIntRetrodt getWpagContIntRetrodt() {
        return wpagContIntRetrodt;
    }

    public WpagContPremioNetto getWpagContPremioNetto() {
        return wpagContPremioNetto;
    }

    public WpagContPremioPuroIas getWpagContPremioPuroIas() {
        return wpagContPremioPuroIas;
    }

    public WpagContPremioRisc getWpagContPremioRisc() {
        return wpagContPremioRisc;
    }

    public WpagContPremioTot getWpagContPremioTot() {
        return wpagContPremioTot;
    }

    public WpagContRicor getWpagContRicor() {
        return wpagContRicor;
    }

    public WpagContSoprProfes getWpagContSoprProfes() {
        return wpagContSoprProfes;
    }

    public WpagContSoprSanit getWpagContSoprSanit() {
        return wpagContSoprSanit;
    }

    public WpagContSoprSport getWpagContSoprSport() {
        return wpagContSoprSport;
    }

    public WpagContSoprTecn getWpagContSoprTecn() {
        return wpagContSoprTecn;
    }

    public WpagContSpeseMediche getWpagContSpeseMediche() {
        return wpagContSpeseMediche;
    }

    public WpagContTasse getWpagContTasse() {
        return wpagContTasse;
    }

    public WpagImpAderTit getWpagImpAderTit() {
        return wpagImpAderTit;
    }

    public WpagImpAzTit getWpagImpAzTit() {
        return wpagImpAzTit;
    }

    public WpagImpCarAcqTit getWpagImpCarAcqTit() {
        return wpagImpCarAcqTit;
    }

    public WpagImpCarGestTit getWpagImpCarGestTit() {
        return wpagImpCarGestTit;
    }

    public WpagImpCarIncTit getWpagImpCarIncTit() {
        return wpagImpCarIncTit;
    }

    public WpagImpTfrTit getWpagImpTfrTit() {
        return wpagImpTfrTit;
    }

    public WpagImpVoloTit getWpagImpVoloTit() {
        return wpagImpVoloTit;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_ANTIRACKET = 8;
        public static final int WPAG_CONT_AUTOGEN_INC = 1;
        public static final int WPAG_DATI_TIT_CONT = WpagContPremioNetto.Len.WPAG_CONT_PREMIO_NETTO + WpagContIntFraz.Len.WPAG_CONT_INT_FRAZ + WpagContIntRetrodt.Len.WPAG_CONT_INT_RETRODT + WpagContDiritti.Len.WPAG_CONT_DIRITTI + WpagContSpeseMediche.Len.WPAG_CONT_SPESE_MEDICHE + WpagContTasse.Len.WPAG_CONT_TASSE + WpagContSoprSanit.Len.WPAG_CONT_SOPR_SANIT + WpagContSoprProfes.Len.WPAG_CONT_SOPR_PROFES + WpagContSoprSport.Len.WPAG_CONT_SOPR_SPORT + WpagContSoprTecn.Len.WPAG_CONT_SOPR_TECN + WpagContAltriSopr.Len.WPAG_CONT_ALTRI_SOPR + WpagContPremioTot.Len.WPAG_CONT_PREMIO_TOT + WpagContPremioPuroIas.Len.WPAG_CONT_PREMIO_PURO_IAS + WpagContPremioRisc.Len.WPAG_CONT_PREMIO_RISC + WpagImpCarAcqTit.Len.WPAG_IMP_CAR_ACQ_TIT + WpagImpCarIncTit.Len.WPAG_IMP_CAR_INC_TIT + WpagImpCarGestTit.Len.WPAG_IMP_CAR_GEST_TIT + WpagContAcq1oAnno.Len.WPAG_CONT_ACQ1O_ANNO + WpagContAcq2oAnno.Len.WPAG_CONT_ACQ2O_ANNO + WpagContRicor.Len.WPAG_CONT_RICOR + WpagContIncas.Len.WPAG_CONT_INCAS + WpagImpAzTit.Len.WPAG_IMP_AZ_TIT + WpagImpAderTit.Len.WPAG_IMP_ADER_TIT + WpagImpTfrTit.Len.WPAG_IMP_TFR_TIT + WpagImpVoloTit.Len.WPAG_IMP_VOLO_TIT + WpagContImpAcqExp.Len.WPAG_CONT_IMP_ACQ_EXP + WpagContImpRenAss.Len.WPAG_CONT_IMP_REN_ASS + WpagContImpCommisInt.Len.WPAG_CONT_IMP_COMMIS_INT + WPAG_CONT_ANTIRACKET + WPAG_CONT_AUTOGEN_INC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_ANTIRACKET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_ANTIRACKET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
