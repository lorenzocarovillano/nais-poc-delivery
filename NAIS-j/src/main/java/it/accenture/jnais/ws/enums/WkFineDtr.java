package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-FINE-DTR<br>
 * Variable: WK-FINE-DTR from program LOAS0320<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFineDtr {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_FINE_DTR);
    public static final String SI = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setWkFineDtr(String wkFineDtr) {
        this.value = Functions.subString(wkFineDtr, Len.WK_FINE_DTR);
    }

    public String getWkFineDtr() {
        return this.value;
    }

    public boolean isSi() {
        return value.equals(SI);
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_FINE_DTR = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
