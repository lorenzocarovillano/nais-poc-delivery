package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndTitCont;
import it.accenture.jnais.copy.Ldbvd601;
import it.accenture.jnais.copy.TitContDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBSD600<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbsd600Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-TIT-CONT
    private IndTitCont indTitCont = new IndTitCont();
    //Original name: TIT-CONT-DB
    private TitContDb titContDb = new TitContDb();
    //Original name: LDBVD601
    private Ldbvd601 ldbvd601 = new Ldbvd601();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndTitCont getIndTitCont() {
        return indTitCont;
    }

    public Ldbvd601 getLdbvd601() {
        return ldbvd601;
    }

    public TitContDb getTitContDb() {
        return titContDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
