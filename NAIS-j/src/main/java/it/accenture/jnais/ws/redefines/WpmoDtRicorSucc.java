package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-DT-RICOR-SUCC<br>
 * Variable: WPMO-DT-RICOR-SUCC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoDtRicorSucc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoDtRicorSucc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_DT_RICOR_SUCC;
    }

    public void setWpmoDtRicorSucc(int wpmoDtRicorSucc) {
        writeIntAsPacked(Pos.WPMO_DT_RICOR_SUCC, wpmoDtRicorSucc, Len.Int.WPMO_DT_RICOR_SUCC);
    }

    public void setWpmoDtRicorSuccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_DT_RICOR_SUCC, Pos.WPMO_DT_RICOR_SUCC);
    }

    /**Original name: WPMO-DT-RICOR-SUCC<br>*/
    public int getWpmoDtRicorSucc() {
        return readPackedAsInt(Pos.WPMO_DT_RICOR_SUCC, Len.Int.WPMO_DT_RICOR_SUCC);
    }

    public byte[] getWpmoDtRicorSuccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_DT_RICOR_SUCC, Pos.WPMO_DT_RICOR_SUCC);
        return buffer;
    }

    public void initWpmoDtRicorSuccSpaces() {
        fill(Pos.WPMO_DT_RICOR_SUCC, Len.WPMO_DT_RICOR_SUCC, Types.SPACE_CHAR);
    }

    public void setWpmoDtRicorSuccNull(String wpmoDtRicorSuccNull) {
        writeString(Pos.WPMO_DT_RICOR_SUCC_NULL, wpmoDtRicorSuccNull, Len.WPMO_DT_RICOR_SUCC_NULL);
    }

    /**Original name: WPMO-DT-RICOR-SUCC-NULL<br>*/
    public String getWpmoDtRicorSuccNull() {
        return readString(Pos.WPMO_DT_RICOR_SUCC_NULL, Len.WPMO_DT_RICOR_SUCC_NULL);
    }

    public String getWpmoDtRicorSuccNullFormatted() {
        return Functions.padBlanks(getWpmoDtRicorSuccNull(), Len.WPMO_DT_RICOR_SUCC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_DT_RICOR_SUCC = 1;
        public static final int WPMO_DT_RICOR_SUCC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_DT_RICOR_SUCC = 5;
        public static final int WPMO_DT_RICOR_SUCC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_DT_RICOR_SUCC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
