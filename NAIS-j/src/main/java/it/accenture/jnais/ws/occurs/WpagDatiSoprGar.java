package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpagImpSovrap;
import it.accenture.jnais.ws.redefines.WpagPercSovrap;
import it.accenture.jnais.ws.redefines.WpagSovram;

/**Original name: WPAG-DATI-SOPR-GAR<br>
 * Variables: WPAG-DATI-SOPR-GAR from copybook LVEC0268<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpagDatiSoprGar {

    //==== PROPERTIES ====
    //Original name: WPAG-COD-SOVRAP
    private String wpagCodSovrap = DefaultValues.stringVal(Len.WPAG_COD_SOVRAP);
    //Original name: WPAG-PERC-SOVRAP
    private WpagPercSovrap wpagPercSovrap = new WpagPercSovrap();
    //Original name: WPAG-IMP-SOVRAP
    private WpagImpSovrap wpagImpSovrap = new WpagImpSovrap();
    //Original name: WPAG-SOVRAM
    private WpagSovram wpagSovram = new WpagSovram();

    //==== METHODS ====
    public void setWpagDatiSoprGarBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagCodSovrap = MarshalByte.readString(buffer, position, Len.WPAG_COD_SOVRAP);
        position += Len.WPAG_COD_SOVRAP;
        wpagPercSovrap.setWpagPercSovrapFromBuffer(buffer, position);
        position += WpagPercSovrap.Len.WPAG_PERC_SOVRAP;
        wpagImpSovrap.setWpagImpSovrapFromBuffer(buffer, position);
        position += WpagImpSovrap.Len.WPAG_IMP_SOVRAP;
        wpagSovram.setWpagSovramFromBuffer(buffer, position);
    }

    public byte[] getWpagDatiSoprGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wpagCodSovrap, Len.WPAG_COD_SOVRAP);
        position += Len.WPAG_COD_SOVRAP;
        wpagPercSovrap.getWpagPercSovrapAsBuffer(buffer, position);
        position += WpagPercSovrap.Len.WPAG_PERC_SOVRAP;
        wpagImpSovrap.getWpagImpSovrapAsBuffer(buffer, position);
        position += WpagImpSovrap.Len.WPAG_IMP_SOVRAP;
        wpagSovram.getWpagSovramAsBuffer(buffer, position);
        return buffer;
    }

    public void initWpagDatiSoprGarSpaces() {
        wpagCodSovrap = "";
        wpagPercSovrap.initWpagPercSovrapSpaces();
        wpagImpSovrap.initWpagImpSovrapSpaces();
        wpagSovram.initWpagSovramSpaces();
    }

    public void setWpagCodSovrap(String wpagCodSovrap) {
        this.wpagCodSovrap = Functions.subString(wpagCodSovrap, Len.WPAG_COD_SOVRAP);
    }

    public String getWpagCodSovrap() {
        return this.wpagCodSovrap;
    }

    public WpagImpSovrap getWpagImpSovrap() {
        return wpagImpSovrap;
    }

    public WpagPercSovrap getWpagPercSovrap() {
        return wpagPercSovrap;
    }

    public WpagSovram getWpagSovram() {
        return wpagSovram;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_COD_SOVRAP = 2;
        public static final int WPAG_DATI_SOPR_GAR = WPAG_COD_SOVRAP + WpagPercSovrap.Len.WPAG_PERC_SOVRAP + WpagImpSovrap.Len.WPAG_IMP_SOVRAP + WpagSovram.Len.WPAG_SOVRAM;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
