package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0012-RECYCLE-TOT<br>
 * Variables: IDSV0012-RECYCLE-TOT from copybook IDSV0012<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idsv0012RecycleTot {

    //==== CONSTRUCTORS ====
    public Idsv0012RecycleTot() {
        init();
    }

    //==== METHODS ====
    public void init() {
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RECYCLE_COD_STR_DATO = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
