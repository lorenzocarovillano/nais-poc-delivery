package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPB-VIS<br>
 * Variable: DFA-IMPB-VIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpbVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpbVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPB_VIS;
    }

    public void setDfaImpbVis(AfDecimal dfaImpbVis) {
        writeDecimalAsPacked(Pos.DFA_IMPB_VIS, dfaImpbVis.copy());
    }

    public void setDfaImpbVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPB_VIS, Pos.DFA_IMPB_VIS);
    }

    /**Original name: DFA-IMPB-VIS<br>*/
    public AfDecimal getDfaImpbVis() {
        return readPackedAsDecimal(Pos.DFA_IMPB_VIS, Len.Int.DFA_IMPB_VIS, Len.Fract.DFA_IMPB_VIS);
    }

    public byte[] getDfaImpbVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPB_VIS, Pos.DFA_IMPB_VIS);
        return buffer;
    }

    public void setDfaImpbVisNull(String dfaImpbVisNull) {
        writeString(Pos.DFA_IMPB_VIS_NULL, dfaImpbVisNull, Len.DFA_IMPB_VIS_NULL);
    }

    /**Original name: DFA-IMPB-VIS-NULL<br>*/
    public String getDfaImpbVisNull() {
        return readString(Pos.DFA_IMPB_VIS_NULL, Len.DFA_IMPB_VIS_NULL);
    }

    public String getDfaImpbVisNullFormatted() {
        return Functions.padBlanks(getDfaImpbVisNull(), Len.DFA_IMPB_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_VIS = 1;
        public static final int DFA_IMPB_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_VIS = 8;
        public static final int DFA_IMPB_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
