package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-MM-PAG-PRE-UNI<br>
 * Variable: GRZ-MM-PAG-PRE-UNI from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzMmPagPreUni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzMmPagPreUni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_MM_PAG_PRE_UNI;
    }

    public void setGrzMmPagPreUni(int grzMmPagPreUni) {
        writeIntAsPacked(Pos.GRZ_MM_PAG_PRE_UNI, grzMmPagPreUni, Len.Int.GRZ_MM_PAG_PRE_UNI);
    }

    public void setGrzMmPagPreUniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_MM_PAG_PRE_UNI, Pos.GRZ_MM_PAG_PRE_UNI);
    }

    /**Original name: GRZ-MM-PAG-PRE-UNI<br>*/
    public int getGrzMmPagPreUni() {
        return readPackedAsInt(Pos.GRZ_MM_PAG_PRE_UNI, Len.Int.GRZ_MM_PAG_PRE_UNI);
    }

    public byte[] getGrzMmPagPreUniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_MM_PAG_PRE_UNI, Pos.GRZ_MM_PAG_PRE_UNI);
        return buffer;
    }

    public void setGrzMmPagPreUniNull(String grzMmPagPreUniNull) {
        writeString(Pos.GRZ_MM_PAG_PRE_UNI_NULL, grzMmPagPreUniNull, Len.GRZ_MM_PAG_PRE_UNI_NULL);
    }

    /**Original name: GRZ-MM-PAG-PRE-UNI-NULL<br>*/
    public String getGrzMmPagPreUniNull() {
        return readString(Pos.GRZ_MM_PAG_PRE_UNI_NULL, Len.GRZ_MM_PAG_PRE_UNI_NULL);
    }

    public String getGrzMmPagPreUniNullFormatted() {
        return Functions.padBlanks(getGrzMmPagPreUniNull(), Len.GRZ_MM_PAG_PRE_UNI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_MM_PAG_PRE_UNI = 1;
        public static final int GRZ_MM_PAG_PRE_UNI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_MM_PAG_PRE_UNI = 3;
        public static final int GRZ_MM_PAG_PRE_UNI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_MM_PAG_PRE_UNI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
