package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-SPE-MED<br>
 * Variable: WDTC-SPE-MED from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcSpeMed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcSpeMed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_SPE_MED;
    }

    public void setWdtcSpeMed(AfDecimal wdtcSpeMed) {
        writeDecimalAsPacked(Pos.WDTC_SPE_MED, wdtcSpeMed.copy());
    }

    public void setWdtcSpeMedFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_SPE_MED, Pos.WDTC_SPE_MED);
    }

    /**Original name: WDTC-SPE-MED<br>*/
    public AfDecimal getWdtcSpeMed() {
        return readPackedAsDecimal(Pos.WDTC_SPE_MED, Len.Int.WDTC_SPE_MED, Len.Fract.WDTC_SPE_MED);
    }

    public byte[] getWdtcSpeMedAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_SPE_MED, Pos.WDTC_SPE_MED);
        return buffer;
    }

    public void initWdtcSpeMedSpaces() {
        fill(Pos.WDTC_SPE_MED, Len.WDTC_SPE_MED, Types.SPACE_CHAR);
    }

    public void setWdtcSpeMedNull(String wdtcSpeMedNull) {
        writeString(Pos.WDTC_SPE_MED_NULL, wdtcSpeMedNull, Len.WDTC_SPE_MED_NULL);
    }

    /**Original name: WDTC-SPE-MED-NULL<br>*/
    public String getWdtcSpeMedNull() {
        return readString(Pos.WDTC_SPE_MED_NULL, Len.WDTC_SPE_MED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_SPE_MED = 1;
        public static final int WDTC_SPE_MED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_SPE_MED = 8;
        public static final int WDTC_SPE_MED_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_SPE_MED = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_SPE_MED = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
