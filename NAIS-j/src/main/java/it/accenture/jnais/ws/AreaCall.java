package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: AREA-CALL<br>
 * Variable: AREA-CALL from program IDSS0020<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaCall extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IDSV0044-AREA-SERVIZIO
    private String idsv0044AreaServizio = DefaultValues.stringVal(Len.IDSV0044_AREA_SERVIZIO);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_CALL;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaCallBytes(buf);
    }

    public String getAreaCallFormatted() {
        return getIdsv0044AreaServizioFormatted();
    }

    public void setAreaCallBytes(byte[] buffer) {
        setAreaCallBytes(buffer, 1);
    }

    public byte[] getAreaCallBytes() {
        byte[] buffer = new byte[Len.AREA_CALL];
        return getAreaCallBytes(buffer, 1);
    }

    public void setAreaCallBytes(byte[] buffer, int offset) {
        int position = offset;
        idsv0044AreaServizio = MarshalByte.readString(buffer, position, Len.IDSV0044_AREA_SERVIZIO);
    }

    public byte[] getAreaCallBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, idsv0044AreaServizio, Len.IDSV0044_AREA_SERVIZIO);
        return buffer;
    }

    public void initAreaCallSpaces() {
        idsv0044AreaServizio = "";
    }

    public void setIdsv0044AreaServizio(String idsv0044AreaServizio) {
        this.idsv0044AreaServizio = Functions.subString(idsv0044AreaServizio, Len.IDSV0044_AREA_SERVIZIO);
    }

    public String getIdsv0044AreaServizio() {
        return this.idsv0044AreaServizio;
    }

    public String getIdsv0044AreaServizioFormatted() {
        return Functions.padBlanks(getIdsv0044AreaServizio(), Len.IDSV0044_AREA_SERVIZIO);
    }

    @Override
    public byte[] serialize() {
        return getAreaCallBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSV0044_AREA_SERVIZIO = 300000;
        public static final int AREA_CALL = IDSV0044_AREA_SERVIZIO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
