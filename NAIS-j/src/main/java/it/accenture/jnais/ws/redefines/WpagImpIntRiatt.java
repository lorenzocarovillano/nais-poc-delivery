package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-INT-RIATT<br>
 * Variable: WPAG-IMP-INT-RIATT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpIntRiatt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpIntRiatt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_INT_RIATT;
    }

    public void setWpagImpIntRiatt(AfDecimal wpagImpIntRiatt) {
        writeDecimalAsPacked(Pos.WPAG_IMP_INT_RIATT, wpagImpIntRiatt.copy());
    }

    public void setWpagImpIntRiattFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_INT_RIATT, Pos.WPAG_IMP_INT_RIATT);
    }

    /**Original name: WPAG-IMP-INT-RIATT<br>*/
    public AfDecimal getWpagImpIntRiatt() {
        return readPackedAsDecimal(Pos.WPAG_IMP_INT_RIATT, Len.Int.WPAG_IMP_INT_RIATT, Len.Fract.WPAG_IMP_INT_RIATT);
    }

    public byte[] getWpagImpIntRiattAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_INT_RIATT, Pos.WPAG_IMP_INT_RIATT);
        return buffer;
    }

    public void initWpagImpIntRiattSpaces() {
        fill(Pos.WPAG_IMP_INT_RIATT, Len.WPAG_IMP_INT_RIATT, Types.SPACE_CHAR);
    }

    public void setWpagImpIntRiattNull(String wpagImpIntRiattNull) {
        writeString(Pos.WPAG_IMP_INT_RIATT_NULL, wpagImpIntRiattNull, Len.WPAG_IMP_INT_RIATT_NULL);
    }

    /**Original name: WPAG-IMP-INT-RIATT-NULL<br>*/
    public String getWpagImpIntRiattNull() {
        return readString(Pos.WPAG_IMP_INT_RIATT_NULL, Len.WPAG_IMP_INT_RIATT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_INT_RIATT = 1;
        public static final int WPAG_IMP_INT_RIATT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_INT_RIATT = 8;
        public static final int WPAG_IMP_INT_RIATT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_INT_RIATT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_INT_RIATT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
