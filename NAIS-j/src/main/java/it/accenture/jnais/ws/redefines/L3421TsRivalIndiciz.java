package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-TS-RIVAL-INDICIZ<br>
 * Variable: L3421-TS-RIVAL-INDICIZ from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421TsRivalIndiciz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421TsRivalIndiciz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_TS_RIVAL_INDICIZ;
    }

    public void setL3421TsRivalIndicizFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_TS_RIVAL_INDICIZ, Pos.L3421_TS_RIVAL_INDICIZ);
    }

    /**Original name: L3421-TS-RIVAL-INDICIZ<br>*/
    public AfDecimal getL3421TsRivalIndiciz() {
        return readPackedAsDecimal(Pos.L3421_TS_RIVAL_INDICIZ, Len.Int.L3421_TS_RIVAL_INDICIZ, Len.Fract.L3421_TS_RIVAL_INDICIZ);
    }

    public byte[] getL3421TsRivalIndicizAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_TS_RIVAL_INDICIZ, Pos.L3421_TS_RIVAL_INDICIZ);
        return buffer;
    }

    /**Original name: L3421-TS-RIVAL-INDICIZ-NULL<br>*/
    public String getL3421TsRivalIndicizNull() {
        return readString(Pos.L3421_TS_RIVAL_INDICIZ_NULL, Len.L3421_TS_RIVAL_INDICIZ_NULL);
    }

    public String getL3421TsRivalIndicizNullFormatted() {
        return Functions.padBlanks(getL3421TsRivalIndicizNull(), Len.L3421_TS_RIVAL_INDICIZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_TS_RIVAL_INDICIZ = 1;
        public static final int L3421_TS_RIVAL_INDICIZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_TS_RIVAL_INDICIZ = 8;
        public static final int L3421_TS_RIVAL_INDICIZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_TS_RIVAL_INDICIZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_TS_RIVAL_INDICIZ = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
