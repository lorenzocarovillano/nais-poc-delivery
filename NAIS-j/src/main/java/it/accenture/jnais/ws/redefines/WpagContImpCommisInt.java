package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-IMP-COMMIS-INT<br>
 * Variable: WPAG-CONT-IMP-COMMIS-INT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContImpCommisInt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContImpCommisInt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_IMP_COMMIS_INT;
    }

    public void setWpagContImpCommisInt(AfDecimal wpagContImpCommisInt) {
        writeDecimalAsPacked(Pos.WPAG_CONT_IMP_COMMIS_INT, wpagContImpCommisInt.copy());
    }

    public void setWpagContImpCommisIntFormatted(String wpagContImpCommisInt) {
        setWpagContImpCommisInt(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_IMP_COMMIS_INT + Len.Fract.WPAG_CONT_IMP_COMMIS_INT, Len.Fract.WPAG_CONT_IMP_COMMIS_INT, wpagContImpCommisInt));
    }

    public void setWpagContImpCommisIntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_IMP_COMMIS_INT, Pos.WPAG_CONT_IMP_COMMIS_INT);
    }

    /**Original name: WPAG-CONT-IMP-COMMIS-INT<br>*/
    public AfDecimal getWpagContImpCommisInt() {
        return readPackedAsDecimal(Pos.WPAG_CONT_IMP_COMMIS_INT, Len.Int.WPAG_CONT_IMP_COMMIS_INT, Len.Fract.WPAG_CONT_IMP_COMMIS_INT);
    }

    public byte[] getWpagContImpCommisIntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_IMP_COMMIS_INT, Pos.WPAG_CONT_IMP_COMMIS_INT);
        return buffer;
    }

    public void initWpagContImpCommisIntSpaces() {
        fill(Pos.WPAG_CONT_IMP_COMMIS_INT, Len.WPAG_CONT_IMP_COMMIS_INT, Types.SPACE_CHAR);
    }

    public void setWpagContImpCommisIntNull(String wpagContImpCommisIntNull) {
        writeString(Pos.WPAG_CONT_IMP_COMMIS_INT_NULL, wpagContImpCommisIntNull, Len.WPAG_CONT_IMP_COMMIS_INT_NULL);
    }

    /**Original name: WPAG-CONT-IMP-COMMIS-INT-NULL<br>*/
    public String getWpagContImpCommisIntNull() {
        return readString(Pos.WPAG_CONT_IMP_COMMIS_INT_NULL, Len.WPAG_CONT_IMP_COMMIS_INT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_IMP_COMMIS_INT = 1;
        public static final int WPAG_CONT_IMP_COMMIS_INT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_IMP_COMMIS_INT = 8;
        public static final int WPAG_CONT_IMP_COMMIS_INT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_IMP_COMMIS_INT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_IMP_COMMIS_INT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
