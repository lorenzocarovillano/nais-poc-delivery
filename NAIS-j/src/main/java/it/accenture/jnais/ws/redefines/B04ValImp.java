package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B04-VAL-IMP<br>
 * Variable: B04-VAL-IMP from program LLBS0266<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B04ValImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B04ValImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B04_VAL_IMP;
    }

    public void setB04ValImp(AfDecimal b04ValImp) {
        writeDecimalAsPacked(Pos.B04_VAL_IMP, b04ValImp.copy());
    }

    public void setB04ValImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B04_VAL_IMP, Pos.B04_VAL_IMP);
    }

    /**Original name: B04-VAL-IMP<br>*/
    public AfDecimal getB04ValImp() {
        return readPackedAsDecimal(Pos.B04_VAL_IMP, Len.Int.B04_VAL_IMP, Len.Fract.B04_VAL_IMP);
    }

    public byte[] getB04ValImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B04_VAL_IMP, Pos.B04_VAL_IMP);
        return buffer;
    }

    public void setB04ValImpNull(String b04ValImpNull) {
        writeString(Pos.B04_VAL_IMP_NULL, b04ValImpNull, Len.B04_VAL_IMP_NULL);
    }

    /**Original name: B04-VAL-IMP-NULL<br>*/
    public String getB04ValImpNull() {
        return readString(Pos.B04_VAL_IMP_NULL, Len.B04_VAL_IMP_NULL);
    }

    public String getB04ValImpNullFormatted() {
        return Functions.padBlanks(getB04ValImpNull(), Len.B04_VAL_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B04_VAL_IMP = 1;
        public static final int B04_VAL_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B04_VAL_IMP = 10;
        public static final int B04_VAL_IMP_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B04_VAL_IMP = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B04_VAL_IMP = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
