package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-IMP-TRASFERITO<br>
 * Variable: OCO-IMP-TRASFERITO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoImpTrasferito extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoImpTrasferito() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_IMP_TRASFERITO;
    }

    public void setOcoImpTrasferito(AfDecimal ocoImpTrasferito) {
        writeDecimalAsPacked(Pos.OCO_IMP_TRASFERITO, ocoImpTrasferito.copy());
    }

    public void setOcoImpTrasferitoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_IMP_TRASFERITO, Pos.OCO_IMP_TRASFERITO);
    }

    /**Original name: OCO-IMP-TRASFERITO<br>*/
    public AfDecimal getOcoImpTrasferito() {
        return readPackedAsDecimal(Pos.OCO_IMP_TRASFERITO, Len.Int.OCO_IMP_TRASFERITO, Len.Fract.OCO_IMP_TRASFERITO);
    }

    public byte[] getOcoImpTrasferitoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_IMP_TRASFERITO, Pos.OCO_IMP_TRASFERITO);
        return buffer;
    }

    public void setOcoImpTrasferitoNull(String ocoImpTrasferitoNull) {
        writeString(Pos.OCO_IMP_TRASFERITO_NULL, ocoImpTrasferitoNull, Len.OCO_IMP_TRASFERITO_NULL);
    }

    /**Original name: OCO-IMP-TRASFERITO-NULL<br>*/
    public String getOcoImpTrasferitoNull() {
        return readString(Pos.OCO_IMP_TRASFERITO_NULL, Len.OCO_IMP_TRASFERITO_NULL);
    }

    public String getOcoImpTrasferitoNullFormatted() {
        return Functions.padBlanks(getOcoImpTrasferitoNull(), Len.OCO_IMP_TRASFERITO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_IMP_TRASFERITO = 1;
        public static final int OCO_IMP_TRASFERITO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_IMP_TRASFERITO = 8;
        public static final int OCO_IMP_TRASFERITO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_IMP_TRASFERITO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_IMP_TRASFERITO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
