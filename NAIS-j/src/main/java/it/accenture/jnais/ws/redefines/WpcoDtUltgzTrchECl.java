package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTGZ-TRCH-E-CL<br>
 * Variable: WPCO-DT-ULTGZ-TRCH-E-CL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltgzTrchECl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltgzTrchECl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTGZ_TRCH_E_CL;
    }

    public void setWpcoDtUltgzTrchECl(int wpcoDtUltgzTrchECl) {
        writeIntAsPacked(Pos.WPCO_DT_ULTGZ_TRCH_E_CL, wpcoDtUltgzTrchECl, Len.Int.WPCO_DT_ULTGZ_TRCH_E_CL);
    }

    public void setDpcoDtUltgzTrchEClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTGZ_TRCH_E_CL, Pos.WPCO_DT_ULTGZ_TRCH_E_CL);
    }

    /**Original name: WPCO-DT-ULTGZ-TRCH-E-CL<br>*/
    public int getWpcoDtUltgzTrchECl() {
        return readPackedAsInt(Pos.WPCO_DT_ULTGZ_TRCH_E_CL, Len.Int.WPCO_DT_ULTGZ_TRCH_E_CL);
    }

    public byte[] getWpcoDtUltgzTrchEClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTGZ_TRCH_E_CL, Pos.WPCO_DT_ULTGZ_TRCH_E_CL);
        return buffer;
    }

    public void setWpcoDtUltgzTrchEClNull(String wpcoDtUltgzTrchEClNull) {
        writeString(Pos.WPCO_DT_ULTGZ_TRCH_E_CL_NULL, wpcoDtUltgzTrchEClNull, Len.WPCO_DT_ULTGZ_TRCH_E_CL_NULL);
    }

    /**Original name: WPCO-DT-ULTGZ-TRCH-E-CL-NULL<br>*/
    public String getWpcoDtUltgzTrchEClNull() {
        return readString(Pos.WPCO_DT_ULTGZ_TRCH_E_CL_NULL, Len.WPCO_DT_ULTGZ_TRCH_E_CL_NULL);
    }

    public String getWpcoDtUltgzTrchEClNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltgzTrchEClNull(), Len.WPCO_DT_ULTGZ_TRCH_E_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTGZ_TRCH_E_CL = 1;
        public static final int WPCO_DT_ULTGZ_TRCH_E_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTGZ_TRCH_E_CL = 5;
        public static final int WPCO_DT_ULTGZ_TRCH_E_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTGZ_TRCH_E_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
