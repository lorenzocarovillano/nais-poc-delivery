package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-IMP-ASSTO<br>
 * Variable: WP67-IMP-ASSTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67ImpAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67ImpAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_IMP_ASSTO;
    }

    public void setWp67ImpAssto(AfDecimal wp67ImpAssto) {
        writeDecimalAsPacked(Pos.WP67_IMP_ASSTO, wp67ImpAssto.copy());
    }

    public void setWp67ImpAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_IMP_ASSTO, Pos.WP67_IMP_ASSTO);
    }

    /**Original name: WP67-IMP-ASSTO<br>*/
    public AfDecimal getWp67ImpAssto() {
        return readPackedAsDecimal(Pos.WP67_IMP_ASSTO, Len.Int.WP67_IMP_ASSTO, Len.Fract.WP67_IMP_ASSTO);
    }

    public byte[] getWp67ImpAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_IMP_ASSTO, Pos.WP67_IMP_ASSTO);
        return buffer;
    }

    public void setWp67ImpAsstoNull(String wp67ImpAsstoNull) {
        writeString(Pos.WP67_IMP_ASSTO_NULL, wp67ImpAsstoNull, Len.WP67_IMP_ASSTO_NULL);
    }

    /**Original name: WP67-IMP-ASSTO-NULL<br>*/
    public String getWp67ImpAsstoNull() {
        return readString(Pos.WP67_IMP_ASSTO_NULL, Len.WP67_IMP_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_IMP_ASSTO = 1;
        public static final int WP67_IMP_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_IMP_ASSTO = 8;
        public static final int WP67_IMP_ASSTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_IMP_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_IMP_ASSTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
