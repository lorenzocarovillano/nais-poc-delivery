package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: GRU-ARZ<br>
 * Variable: GRU-ARZ from copybook IDBVGRU1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class GruArz extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: GRU-COD-GRU-ARZ
    private long codGruArz = DefaultValues.LONG_VAL;
    //Original name: GRU-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: GRU-COD-LIV-OGZ
    private int codLivOgz = DefaultValues.INT_VAL;
    //Original name: GRU-DSC-GRU-ARZ-LEN
    private short dscGruArzLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: GRU-DSC-GRU-ARZ
    private String dscGruArz = DefaultValues.stringVal(Len.DSC_GRU_ARZ);
    //Original name: GRU-DAT-INI-GRU-ARZ
    private int datIniGruArz = DefaultValues.INT_VAL;
    //Original name: GRU-DAT-FINE-GRU-ARZ
    private int datFineGruArz = DefaultValues.INT_VAL;
    //Original name: GRU-DEN-RESP-GRU-ARZ-LEN
    private short denRespGruArzLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: GRU-DEN-RESP-GRU-ARZ
    private String denRespGruArz = DefaultValues.stringVal(Len.DEN_RESP_GRU_ARZ);
    //Original name: GRU-IND-TLM-RESP-LEN
    private short indTlmRespLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: GRU-IND-TLM-RESP
    private String indTlmResp = DefaultValues.stringVal(Len.IND_TLM_RESP);
    //Original name: GRU-COD-UTE-INS
    private String codUteIns = DefaultValues.stringVal(Len.COD_UTE_INS);
    //Original name: GRU-TMST-INS-RIG
    private long tmstInsRig = DefaultValues.LONG_VAL;
    //Original name: GRU-COD-UTE-AGR
    private String codUteAgr = DefaultValues.stringVal(Len.COD_UTE_AGR);
    //Original name: GRU-TMST-AGR-RIG
    private long tmstAgrRig = DefaultValues.LONG_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRU_ARZ;
    }

    @Override
    public void deserialize(byte[] buf) {
        setGruArzBytes(buf);
    }

    public String getGruArzFormatted() {
        return MarshalByteExt.bufferToStr(getGruArzBytes());
    }

    public void setGruArzBytes(byte[] buffer) {
        setGruArzBytes(buffer, 1);
    }

    public byte[] getGruArzBytes() {
        byte[] buffer = new byte[Len.GRU_ARZ];
        return getGruArzBytes(buffer, 1);
    }

    public void setGruArzBytes(byte[] buffer, int offset) {
        int position = offset;
        codGruArz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.COD_GRU_ARZ, 0);
        position += Len.COD_GRU_ARZ;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        codLivOgz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_LIV_OGZ, 0);
        position += Len.COD_LIV_OGZ;
        setDscGruArzVcharBytes(buffer, position);
        position += Len.DSC_GRU_ARZ_VCHAR;
        datIniGruArz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DAT_INI_GRU_ARZ, 0);
        position += Len.DAT_INI_GRU_ARZ;
        datFineGruArz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DAT_FINE_GRU_ARZ, 0);
        position += Len.DAT_FINE_GRU_ARZ;
        setDenRespGruArzVcharBytes(buffer, position);
        position += Len.DEN_RESP_GRU_ARZ_VCHAR;
        setIndTlmRespVcharBytes(buffer, position);
        position += Len.IND_TLM_RESP_VCHAR;
        codUteIns = MarshalByte.readString(buffer, position, Len.COD_UTE_INS);
        position += Len.COD_UTE_INS;
        tmstInsRig = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TMST_INS_RIG, 0);
        position += Len.TMST_INS_RIG;
        codUteAgr = MarshalByte.readString(buffer, position, Len.COD_UTE_AGR);
        position += Len.COD_UTE_AGR;
        tmstAgrRig = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TMST_AGR_RIG, 0);
    }

    public byte[] getGruArzBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeLongAsPacked(buffer, position, codGruArz, Len.Int.COD_GRU_ARZ, 0);
        position += Len.COD_GRU_ARZ;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, codLivOgz, Len.Int.COD_LIV_OGZ, 0);
        position += Len.COD_LIV_OGZ;
        getDscGruArzVcharBytes(buffer, position);
        position += Len.DSC_GRU_ARZ_VCHAR;
        MarshalByte.writeIntAsPacked(buffer, position, datIniGruArz, Len.Int.DAT_INI_GRU_ARZ, 0);
        position += Len.DAT_INI_GRU_ARZ;
        MarshalByte.writeIntAsPacked(buffer, position, datFineGruArz, Len.Int.DAT_FINE_GRU_ARZ, 0);
        position += Len.DAT_FINE_GRU_ARZ;
        getDenRespGruArzVcharBytes(buffer, position);
        position += Len.DEN_RESP_GRU_ARZ_VCHAR;
        getIndTlmRespVcharBytes(buffer, position);
        position += Len.IND_TLM_RESP_VCHAR;
        MarshalByte.writeString(buffer, position, codUteIns, Len.COD_UTE_INS);
        position += Len.COD_UTE_INS;
        MarshalByte.writeLongAsPacked(buffer, position, tmstInsRig, Len.Int.TMST_INS_RIG, 0);
        position += Len.TMST_INS_RIG;
        MarshalByte.writeString(buffer, position, codUteAgr, Len.COD_UTE_AGR);
        position += Len.COD_UTE_AGR;
        MarshalByte.writeLongAsPacked(buffer, position, tmstAgrRig, Len.Int.TMST_AGR_RIG, 0);
        return buffer;
    }

    public void setCodGruArz(long codGruArz) {
        this.codGruArz = codGruArz;
    }

    public long getCodGruArz() {
        return this.codGruArz;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setCodLivOgz(int codLivOgz) {
        this.codLivOgz = codLivOgz;
    }

    public int getCodLivOgz() {
        return this.codLivOgz;
    }

    public void setDscGruArzVcharFormatted(String data) {
        byte[] buffer = new byte[Len.DSC_GRU_ARZ_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.DSC_GRU_ARZ_VCHAR);
        setDscGruArzVcharBytes(buffer, 1);
    }

    public String getDscGruArzVcharFormatted() {
        return MarshalByteExt.bufferToStr(getDscGruArzVcharBytes());
    }

    /**Original name: GRU-DSC-GRU-ARZ-VCHAR<br>*/
    public byte[] getDscGruArzVcharBytes() {
        byte[] buffer = new byte[Len.DSC_GRU_ARZ_VCHAR];
        return getDscGruArzVcharBytes(buffer, 1);
    }

    public void setDscGruArzVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        dscGruArzLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        dscGruArz = MarshalByte.readString(buffer, position, Len.DSC_GRU_ARZ);
    }

    public byte[] getDscGruArzVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, dscGruArzLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, dscGruArz, Len.DSC_GRU_ARZ);
        return buffer;
    }

    public void setDscGruArzLen(short dscGruArzLen) {
        this.dscGruArzLen = dscGruArzLen;
    }

    public short getDscGruArzLen() {
        return this.dscGruArzLen;
    }

    public void setDscGruArz(String dscGruArz) {
        this.dscGruArz = Functions.subString(dscGruArz, Len.DSC_GRU_ARZ);
    }

    public String getDscGruArz() {
        return this.dscGruArz;
    }

    public void setDatIniGruArz(int datIniGruArz) {
        this.datIniGruArz = datIniGruArz;
    }

    public int getDatIniGruArz() {
        return this.datIniGruArz;
    }

    public void setDatFineGruArz(int datFineGruArz) {
        this.datFineGruArz = datFineGruArz;
    }

    public int getDatFineGruArz() {
        return this.datFineGruArz;
    }

    public void setDenRespGruArzVcharFormatted(String data) {
        byte[] buffer = new byte[Len.DEN_RESP_GRU_ARZ_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.DEN_RESP_GRU_ARZ_VCHAR);
        setDenRespGruArzVcharBytes(buffer, 1);
    }

    public String getDenRespGruArzVcharFormatted() {
        return MarshalByteExt.bufferToStr(getDenRespGruArzVcharBytes());
    }

    /**Original name: GRU-DEN-RESP-GRU-ARZ-VCHAR<br>*/
    public byte[] getDenRespGruArzVcharBytes() {
        byte[] buffer = new byte[Len.DEN_RESP_GRU_ARZ_VCHAR];
        return getDenRespGruArzVcharBytes(buffer, 1);
    }

    public void setDenRespGruArzVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        denRespGruArzLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        denRespGruArz = MarshalByte.readString(buffer, position, Len.DEN_RESP_GRU_ARZ);
    }

    public byte[] getDenRespGruArzVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, denRespGruArzLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, denRespGruArz, Len.DEN_RESP_GRU_ARZ);
        return buffer;
    }

    public void setDenRespGruArzLen(short denRespGruArzLen) {
        this.denRespGruArzLen = denRespGruArzLen;
    }

    public short getDenRespGruArzLen() {
        return this.denRespGruArzLen;
    }

    public void setDenRespGruArz(String denRespGruArz) {
        this.denRespGruArz = Functions.subString(denRespGruArz, Len.DEN_RESP_GRU_ARZ);
    }

    public String getDenRespGruArz() {
        return this.denRespGruArz;
    }

    public void setIndTlmRespVcharFormatted(String data) {
        byte[] buffer = new byte[Len.IND_TLM_RESP_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.IND_TLM_RESP_VCHAR);
        setIndTlmRespVcharBytes(buffer, 1);
    }

    public String getIndTlmRespVcharFormatted() {
        return MarshalByteExt.bufferToStr(getIndTlmRespVcharBytes());
    }

    /**Original name: GRU-IND-TLM-RESP-VCHAR<br>*/
    public byte[] getIndTlmRespVcharBytes() {
        byte[] buffer = new byte[Len.IND_TLM_RESP_VCHAR];
        return getIndTlmRespVcharBytes(buffer, 1);
    }

    public void setIndTlmRespVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        indTlmRespLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        indTlmResp = MarshalByte.readString(buffer, position, Len.IND_TLM_RESP);
    }

    public byte[] getIndTlmRespVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, indTlmRespLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, indTlmResp, Len.IND_TLM_RESP);
        return buffer;
    }

    public void setIndTlmRespLen(short indTlmRespLen) {
        this.indTlmRespLen = indTlmRespLen;
    }

    public short getIndTlmRespLen() {
        return this.indTlmRespLen;
    }

    public void setIndTlmResp(String indTlmResp) {
        this.indTlmResp = Functions.subString(indTlmResp, Len.IND_TLM_RESP);
    }

    public String getIndTlmResp() {
        return this.indTlmResp;
    }

    public void setCodUteIns(String codUteIns) {
        this.codUteIns = Functions.subString(codUteIns, Len.COD_UTE_INS);
    }

    public String getCodUteIns() {
        return this.codUteIns;
    }

    public void setTmstInsRig(long tmstInsRig) {
        this.tmstInsRig = tmstInsRig;
    }

    public long getTmstInsRig() {
        return this.tmstInsRig;
    }

    public void setCodUteAgr(String codUteAgr) {
        this.codUteAgr = Functions.subString(codUteAgr, Len.COD_UTE_AGR);
    }

    public String getCodUteAgr() {
        return this.codUteAgr;
    }

    public void setTmstAgrRig(long tmstAgrRig) {
        this.tmstAgrRig = tmstAgrRig;
    }

    public long getTmstAgrRig() {
        return this.tmstAgrRig;
    }

    @Override
    public byte[] serialize() {
        return getGruArzBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_GRU_ARZ = 6;
        public static final int COD_COMP_ANIA = 3;
        public static final int COD_LIV_OGZ = 3;
        public static final int DSC_GRU_ARZ_LEN = 2;
        public static final int DSC_GRU_ARZ = 255;
        public static final int DSC_GRU_ARZ_VCHAR = DSC_GRU_ARZ_LEN + DSC_GRU_ARZ;
        public static final int DAT_INI_GRU_ARZ = 5;
        public static final int DAT_FINE_GRU_ARZ = 5;
        public static final int DEN_RESP_GRU_ARZ_LEN = 2;
        public static final int DEN_RESP_GRU_ARZ = 255;
        public static final int DEN_RESP_GRU_ARZ_VCHAR = DEN_RESP_GRU_ARZ_LEN + DEN_RESP_GRU_ARZ;
        public static final int IND_TLM_RESP_LEN = 2;
        public static final int IND_TLM_RESP = 255;
        public static final int IND_TLM_RESP_VCHAR = IND_TLM_RESP_LEN + IND_TLM_RESP;
        public static final int COD_UTE_INS = 8;
        public static final int TMST_INS_RIG = 10;
        public static final int COD_UTE_AGR = 8;
        public static final int TMST_AGR_RIG = 10;
        public static final int GRU_ARZ = COD_GRU_ARZ + COD_COMP_ANIA + COD_LIV_OGZ + DSC_GRU_ARZ_VCHAR + DAT_INI_GRU_ARZ + DAT_FINE_GRU_ARZ + DEN_RESP_GRU_ARZ_VCHAR + IND_TLM_RESP_VCHAR + COD_UTE_INS + TMST_INS_RIG + COD_UTE_AGR + TMST_AGR_RIG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_GRU_ARZ = 10;
            public static final int COD_COMP_ANIA = 5;
            public static final int COD_LIV_OGZ = 5;
            public static final int DAT_INI_GRU_ARZ = 8;
            public static final int DAT_FINE_GRU_ARZ = 8;
            public static final int TMST_INS_RIG = 18;
            public static final int TMST_AGR_RIG = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
