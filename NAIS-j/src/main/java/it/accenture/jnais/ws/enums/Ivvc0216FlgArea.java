package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: IVVC0216-FLG-AREA<br>
 * Variable: IVVC0216-FLG-AREA from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ivvc0216FlgArea extends SerializableParameter {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.IVVC0216_FLG_AREA);
    public static final String VE = "VE";
    public static final String PV = "PV";

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IVVC0216_FLG_AREA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIvvc0216FlgAreaFromBuffer(buf);
    }

    public void setIvvc0216FlgArea(String ivvc0216FlgArea) {
        this.value = Functions.subString(ivvc0216FlgArea, Len.IVVC0216_FLG_AREA);
    }

    public void setIvvc0216FlgAreaFromBuffer(byte[] buffer) {
        value = MarshalByte.readString(buffer, 1, Len.IVVC0216_FLG_AREA);
    }

    public String getIvvc0216FlgArea() {
        return this.value;
    }

    public String getIvvc0216FlgAreaFormatted() {
        return Functions.padBlanks(getIvvc0216FlgArea(), Len.IVVC0216_FLG_AREA);
    }

    public boolean isVe() {
        return value.equals(VE);
    }

    @Override
    public byte[] serialize() {
        return MarshalByteExt.strToBuffer(getIvvc0216FlgArea(), Len.IVVC0216_FLG_AREA);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IVVC0216_FLG_AREA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IVVC0216_FLG_AREA = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IVVC0216_FLG_AREA = 0;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
