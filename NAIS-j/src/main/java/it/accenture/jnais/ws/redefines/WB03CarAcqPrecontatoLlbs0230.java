package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-CAR-ACQ-PRECONTATO<br>
 * Variable: W-B03-CAR-ACQ-PRECONTATO from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CarAcqPrecontatoLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CarAcqPrecontatoLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_CAR_ACQ_PRECONTATO;
    }

    public void setwB03CarAcqPrecontato(AfDecimal wB03CarAcqPrecontato) {
        writeDecimalAsPacked(Pos.W_B03_CAR_ACQ_PRECONTATO, wB03CarAcqPrecontato.copy());
    }

    /**Original name: W-B03-CAR-ACQ-PRECONTATO<br>*/
    public AfDecimal getwB03CarAcqPrecontato() {
        return readPackedAsDecimal(Pos.W_B03_CAR_ACQ_PRECONTATO, Len.Int.W_B03_CAR_ACQ_PRECONTATO, Len.Fract.W_B03_CAR_ACQ_PRECONTATO);
    }

    public byte[] getwB03CarAcqPrecontatoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_CAR_ACQ_PRECONTATO, Pos.W_B03_CAR_ACQ_PRECONTATO);
        return buffer;
    }

    public void setwB03CarAcqPrecontatoNull(String wB03CarAcqPrecontatoNull) {
        writeString(Pos.W_B03_CAR_ACQ_PRECONTATO_NULL, wB03CarAcqPrecontatoNull, Len.W_B03_CAR_ACQ_PRECONTATO_NULL);
    }

    /**Original name: W-B03-CAR-ACQ-PRECONTATO-NULL<br>*/
    public String getwB03CarAcqPrecontatoNull() {
        return readString(Pos.W_B03_CAR_ACQ_PRECONTATO_NULL, Len.W_B03_CAR_ACQ_PRECONTATO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_CAR_ACQ_PRECONTATO = 1;
        public static final int W_B03_CAR_ACQ_PRECONTATO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_CAR_ACQ_PRECONTATO = 8;
        public static final int W_B03_CAR_ACQ_PRECONTATO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_CAR_ACQ_PRECONTATO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_CAR_ACQ_PRECONTATO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
