package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-ACQ-EXP<br>
 * Variable: WB03-ACQ-EXP from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03AcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03AcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_ACQ_EXP;
    }

    public void setWb03AcqExp(AfDecimal wb03AcqExp) {
        writeDecimalAsPacked(Pos.WB03_ACQ_EXP, wb03AcqExp.copy());
    }

    public void setWb03AcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_ACQ_EXP, Pos.WB03_ACQ_EXP);
    }

    /**Original name: WB03-ACQ-EXP<br>*/
    public AfDecimal getWb03AcqExp() {
        return readPackedAsDecimal(Pos.WB03_ACQ_EXP, Len.Int.WB03_ACQ_EXP, Len.Fract.WB03_ACQ_EXP);
    }

    public byte[] getWb03AcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_ACQ_EXP, Pos.WB03_ACQ_EXP);
        return buffer;
    }

    public void setWb03AcqExpNull(String wb03AcqExpNull) {
        writeString(Pos.WB03_ACQ_EXP_NULL, wb03AcqExpNull, Len.WB03_ACQ_EXP_NULL);
    }

    /**Original name: WB03-ACQ-EXP-NULL<br>*/
    public String getWb03AcqExpNull() {
        return readString(Pos.WB03_ACQ_EXP_NULL, Len.WB03_ACQ_EXP_NULL);
    }

    public String getWb03AcqExpNullFormatted() {
        return Functions.padBlanks(getWb03AcqExpNull(), Len.WB03_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_ACQ_EXP = 1;
        public static final int WB03_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ACQ_EXP = 8;
        public static final int WB03_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
