package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-CAR-INC<br>
 * Variable: WDTR-CAR-INC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrCarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrCarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_CAR_INC;
    }

    public void setWdtrCarInc(AfDecimal wdtrCarInc) {
        writeDecimalAsPacked(Pos.WDTR_CAR_INC, wdtrCarInc.copy());
    }

    /**Original name: WDTR-CAR-INC<br>*/
    public AfDecimal getWdtrCarInc() {
        return readPackedAsDecimal(Pos.WDTR_CAR_INC, Len.Int.WDTR_CAR_INC, Len.Fract.WDTR_CAR_INC);
    }

    public void setWdtrCarIncNull(String wdtrCarIncNull) {
        writeString(Pos.WDTR_CAR_INC_NULL, wdtrCarIncNull, Len.WDTR_CAR_INC_NULL);
    }

    /**Original name: WDTR-CAR-INC-NULL<br>*/
    public String getWdtrCarIncNull() {
        return readString(Pos.WDTR_CAR_INC_NULL, Len.WDTR_CAR_INC_NULL);
    }

    public String getWdtrCarIncNullFormatted() {
        return Functions.padBlanks(getWdtrCarIncNull(), Len.WDTR_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_CAR_INC = 1;
        public static final int WDTR_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_CAR_INC = 8;
        public static final int WDTR_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
