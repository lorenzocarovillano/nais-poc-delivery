package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-VAL-DT<br>
 * Variable: WPAG-VAL-DT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagValDt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagValDt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_VAL_DT;
    }

    public void setWpagValDtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_VAL_DT, Pos.WPAG_VAL_DT);
    }

    public byte[] getWpagValDtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_VAL_DT, Pos.WPAG_VAL_DT);
        return buffer;
    }

    public void initWpagValDtSpaces() {
        fill(Pos.WPAG_VAL_DT, Len.WPAG_VAL_DT, Types.SPACE_CHAR);
    }

    public void setWpagValDtNull(String wpagValDtNull) {
        writeString(Pos.WPAG_VAL_DT_NULL, wpagValDtNull, Len.WPAG_VAL_DT_NULL);
    }

    /**Original name: WPAG-VAL-DT-NULL<br>*/
    public String getWpagValDtNull() {
        return readString(Pos.WPAG_VAL_DT_NULL, Len.WPAG_VAL_DT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_VAL_DT = 1;
        public static final int WPAG_VAL_DT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_VAL_DT = 5;
        public static final int WPAG_VAL_DT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
