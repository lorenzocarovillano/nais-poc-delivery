package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P01-ID-RICH-EST-COLLG<br>
 * Variable: P01-ID-RICH-EST-COLLG from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P01IdRichEstCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P01IdRichEstCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P01_ID_RICH_EST_COLLG;
    }

    public void setP01IdRichEstCollg(int p01IdRichEstCollg) {
        writeIntAsPacked(Pos.P01_ID_RICH_EST_COLLG, p01IdRichEstCollg, Len.Int.P01_ID_RICH_EST_COLLG);
    }

    public void setP01IdRichEstCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P01_ID_RICH_EST_COLLG, Pos.P01_ID_RICH_EST_COLLG);
    }

    /**Original name: P01-ID-RICH-EST-COLLG<br>*/
    public int getP01IdRichEstCollg() {
        return readPackedAsInt(Pos.P01_ID_RICH_EST_COLLG, Len.Int.P01_ID_RICH_EST_COLLG);
    }

    public byte[] getP01IdRichEstCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P01_ID_RICH_EST_COLLG, Pos.P01_ID_RICH_EST_COLLG);
        return buffer;
    }

    public void setP01IdRichEstCollgNull(String p01IdRichEstCollgNull) {
        writeString(Pos.P01_ID_RICH_EST_COLLG_NULL, p01IdRichEstCollgNull, Len.P01_ID_RICH_EST_COLLG_NULL);
    }

    /**Original name: P01-ID-RICH-EST-COLLG-NULL<br>*/
    public String getP01IdRichEstCollgNull() {
        return readString(Pos.P01_ID_RICH_EST_COLLG_NULL, Len.P01_ID_RICH_EST_COLLG_NULL);
    }

    public String getP01IdRichEstCollgNullFormatted() {
        return Functions.padBlanks(getP01IdRichEstCollgNull(), Len.P01_ID_RICH_EST_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P01_ID_RICH_EST_COLLG = 1;
        public static final int P01_ID_RICH_EST_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P01_ID_RICH_EST_COLLG = 5;
        public static final int P01_ID_RICH_EST_COLLG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P01_ID_RICH_EST_COLLG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
