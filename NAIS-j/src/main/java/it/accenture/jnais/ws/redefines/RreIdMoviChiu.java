package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RRE-ID-MOVI-CHIU<br>
 * Variable: RRE-ID-MOVI-CHIU from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RreIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RreIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RRE_ID_MOVI_CHIU;
    }

    public void setRreIdMoviChiu(int rreIdMoviChiu) {
        writeIntAsPacked(Pos.RRE_ID_MOVI_CHIU, rreIdMoviChiu, Len.Int.RRE_ID_MOVI_CHIU);
    }

    public void setRreIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RRE_ID_MOVI_CHIU, Pos.RRE_ID_MOVI_CHIU);
    }

    /**Original name: RRE-ID-MOVI-CHIU<br>*/
    public int getRreIdMoviChiu() {
        return readPackedAsInt(Pos.RRE_ID_MOVI_CHIU, Len.Int.RRE_ID_MOVI_CHIU);
    }

    public byte[] getRreIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RRE_ID_MOVI_CHIU, Pos.RRE_ID_MOVI_CHIU);
        return buffer;
    }

    public void setRreIdMoviChiuNull(String rreIdMoviChiuNull) {
        writeString(Pos.RRE_ID_MOVI_CHIU_NULL, rreIdMoviChiuNull, Len.RRE_ID_MOVI_CHIU_NULL);
    }

    /**Original name: RRE-ID-MOVI-CHIU-NULL<br>*/
    public String getRreIdMoviChiuNull() {
        return readString(Pos.RRE_ID_MOVI_CHIU_NULL, Len.RRE_ID_MOVI_CHIU_NULL);
    }

    public String getRreIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getRreIdMoviChiuNull(), Len.RRE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RRE_ID_MOVI_CHIU = 1;
        public static final int RRE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RRE_ID_MOVI_CHIU = 5;
        public static final int RRE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RRE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
