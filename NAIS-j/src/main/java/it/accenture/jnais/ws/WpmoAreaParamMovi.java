package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WpmoTabParamMov;

/**Original name: WPMO-AREA-PARAM-MOVI<br>
 * Variable: WPMO-AREA-PARAM-MOVI from program LOAS0110<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WpmoAreaParamMovi extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_PARAM_MOV_MAXOCCURS = 100;
    //Original name: WPMO-ELE-PARAM-MOV-MAX
    private short eleParamMovMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPMO-TAB-PARAM-MOV
    private WpmoTabParamMov[] tabParamMov = new WpmoTabParamMov[TAB_PARAM_MOV_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WpmoAreaParamMovi() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_AREA_PARAM_MOVI;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWpmoAreaParamMoviBytes(buf);
    }

    public void init() {
        for (int tabParamMovIdx = 1; tabParamMovIdx <= TAB_PARAM_MOV_MAXOCCURS; tabParamMovIdx++) {
            tabParamMov[tabParamMovIdx - 1] = new WpmoTabParamMov();
        }
    }

    public String getWpmoAreaParamMovFormatted() {
        return MarshalByteExt.bufferToStr(getWpmoAreaParamMoviBytes());
    }

    public void setWpmoAreaParamMoviBytes(byte[] buffer) {
        setWpmoAreaParamMoviBytes(buffer, 1);
    }

    public byte[] getWpmoAreaParamMoviBytes() {
        byte[] buffer = new byte[Len.WPMO_AREA_PARAM_MOVI];
        return getWpmoAreaParamMoviBytes(buffer, 1);
    }

    public void setWpmoAreaParamMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        eleParamMovMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_PARAM_MOV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabParamMov[idx - 1].setWpmoTabParamMovBytes(buffer, position);
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
            else {
                tabParamMov[idx - 1].initWpmoTabParamMovSpaces();
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
        }
    }

    public byte[] getWpmoAreaParamMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleParamMovMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_PARAM_MOV_MAXOCCURS; idx++) {
            tabParamMov[idx - 1].getWpmoTabParamMovBytes(buffer, position);
            position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
        }
        return buffer;
    }

    public void setEleParamMovMax(short eleParamMovMax) {
        this.eleParamMovMax = eleParamMovMax;
    }

    public short getEleParamMovMax() {
        return this.eleParamMovMax;
    }

    public WpmoTabParamMov getTabParamMov(int idx) {
        return tabParamMov[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWpmoAreaParamMoviBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_PARAM_MOV_MAX = 2;
        public static final int WPMO_AREA_PARAM_MOVI = ELE_PARAM_MOV_MAX + WpmoAreaParamMovi.TAB_PARAM_MOV_MAXOCCURS * WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
