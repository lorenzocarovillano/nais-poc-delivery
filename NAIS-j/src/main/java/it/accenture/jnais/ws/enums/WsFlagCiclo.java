package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: WS-FLAG-CICLO<br>
 * Variable: WS-FLAG-CICLO from program LLBS0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFlagCiclo {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWsFlagCiclo(char wsFlagCiclo) {
        this.value = wsFlagCiclo;
    }

    public char getWsFlagCiclo() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
