package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WK-DATA-SEPARATA<br>
 * Variable: WK-DATA-SEPARATA from program LVVS2720<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WkDataSeparata {

    //==== PROPERTIES ====
    //Original name: WK-ANNO
    private String anno = DefaultValues.stringVal(Len.ANNO);
    //Original name: WK-MESE
    private String mese = DefaultValues.stringVal(Len.MESE);
    //Original name: WK-GG
    private String gg = DefaultValues.stringVal(Len.GG);

    //==== METHODS ====
    public void setWkData(int wkData) {
        int position = 1;
        byte[] buffer = getWkDataSeparataBytes();
        MarshalByte.writeInt(buffer, position, wkData, Len.Int.WK_DATA, SignType.NO_SIGN);
        setWkDataSeparataBytes(buffer);
    }

    public void setWkDataFromBuffer(byte[] buffer) {
        setWkDataSeparataBytes(buffer, 1);
    }

    /**Original name: WK-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    public int getWkData() {
        int position = 1;
        return MarshalByte.readInt(getWkDataSeparataBytes(), position, Len.Int.WK_DATA, SignType.NO_SIGN);
    }

    public void setWkDataSeparataBytes(byte[] buffer) {
        setWkDataSeparataBytes(buffer, 1);
    }

    /**Original name: WK-DATA-SEPARATA<br>*/
    public byte[] getWkDataSeparataBytes() {
        byte[] buffer = new byte[Len.WK_DATA_SEPARATA];
        return getWkDataSeparataBytes(buffer, 1);
    }

    public void setWkDataSeparataBytes(byte[] buffer, int offset) {
        int position = offset;
        anno = MarshalByte.readFixedString(buffer, position, Len.ANNO);
        position += Len.ANNO;
        mese = MarshalByte.readFixedString(buffer, position, Len.MESE);
        position += Len.MESE;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
    }

    public byte[] getWkDataSeparataBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, anno, Len.ANNO);
        position += Len.ANNO;
        MarshalByte.writeString(buffer, position, mese, Len.MESE);
        position += Len.MESE;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        return buffer;
    }

    public String getAnnoFormatted() {
        return this.anno;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ANNO = 4;
        public static final int MESE = 2;
        public static final int GG = 2;
        public static final int WK_DATA_SEPARATA = ANNO + MESE + GG;
        public static final int WK_DATA = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_DATA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
