package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-CAR-GEST-TGA<br>
 * Variable: WPAG-IMP-CAR-GEST-TGA from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpCarGestTga extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpCarGestTga() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_CAR_GEST_TGA;
    }

    public void setWpagImpCarGestTga(AfDecimal wpagImpCarGestTga) {
        writeDecimalAsPacked(Pos.WPAG_IMP_CAR_GEST_TGA, wpagImpCarGestTga.copy());
    }

    public void setWpagImpCarGestTgaFormatted(String wpagImpCarGestTga) {
        setWpagImpCarGestTga(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_CAR_GEST_TGA + Len.Fract.WPAG_IMP_CAR_GEST_TGA, Len.Fract.WPAG_IMP_CAR_GEST_TGA, wpagImpCarGestTga));
    }

    public void setWpagImpCarGestTgaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_CAR_GEST_TGA, Pos.WPAG_IMP_CAR_GEST_TGA);
    }

    /**Original name: WPAG-IMP-CAR-GEST-TGA<br>*/
    public AfDecimal getWpagImpCarGestTga() {
        return readPackedAsDecimal(Pos.WPAG_IMP_CAR_GEST_TGA, Len.Int.WPAG_IMP_CAR_GEST_TGA, Len.Fract.WPAG_IMP_CAR_GEST_TGA);
    }

    public byte[] getWpagImpCarGestTgaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_CAR_GEST_TGA, Pos.WPAG_IMP_CAR_GEST_TGA);
        return buffer;
    }

    public void initWpagImpCarGestTgaSpaces() {
        fill(Pos.WPAG_IMP_CAR_GEST_TGA, Len.WPAG_IMP_CAR_GEST_TGA, Types.SPACE_CHAR);
    }

    public void setWpagImpCarGestTgaNull(String wpagImpCarGestTgaNull) {
        writeString(Pos.WPAG_IMP_CAR_GEST_TGA_NULL, wpagImpCarGestTgaNull, Len.WPAG_IMP_CAR_GEST_TGA_NULL);
    }

    /**Original name: WPAG-IMP-CAR-GEST-TGA-NULL<br>*/
    public String getWpagImpCarGestTgaNull() {
        return readString(Pos.WPAG_IMP_CAR_GEST_TGA_NULL, Len.WPAG_IMP_CAR_GEST_TGA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_GEST_TGA = 1;
        public static final int WPAG_IMP_CAR_GEST_TGA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_GEST_TGA = 8;
        public static final int WPAG_IMP_CAR_GEST_TGA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_GEST_TGA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_GEST_TGA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
