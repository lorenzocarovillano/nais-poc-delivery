package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-NUM-RAT-ACCORPATE<br>
 * Variable: WTIT-NUM-RAT-ACCORPATE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitNumRatAccorpate extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitNumRatAccorpate() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_NUM_RAT_ACCORPATE;
    }

    public void setWtitNumRatAccorpate(int wtitNumRatAccorpate) {
        writeIntAsPacked(Pos.WTIT_NUM_RAT_ACCORPATE, wtitNumRatAccorpate, Len.Int.WTIT_NUM_RAT_ACCORPATE);
    }

    public void setWtitNumRatAccorpateFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_NUM_RAT_ACCORPATE, Pos.WTIT_NUM_RAT_ACCORPATE);
    }

    /**Original name: WTIT-NUM-RAT-ACCORPATE<br>*/
    public int getWtitNumRatAccorpate() {
        return readPackedAsInt(Pos.WTIT_NUM_RAT_ACCORPATE, Len.Int.WTIT_NUM_RAT_ACCORPATE);
    }

    public byte[] getWtitNumRatAccorpateAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_NUM_RAT_ACCORPATE, Pos.WTIT_NUM_RAT_ACCORPATE);
        return buffer;
    }

    public void initWtitNumRatAccorpateSpaces() {
        fill(Pos.WTIT_NUM_RAT_ACCORPATE, Len.WTIT_NUM_RAT_ACCORPATE, Types.SPACE_CHAR);
    }

    public void setWtitNumRatAccorpateNull(String wtitNumRatAccorpateNull) {
        writeString(Pos.WTIT_NUM_RAT_ACCORPATE_NULL, wtitNumRatAccorpateNull, Len.WTIT_NUM_RAT_ACCORPATE_NULL);
    }

    /**Original name: WTIT-NUM-RAT-ACCORPATE-NULL<br>*/
    public String getWtitNumRatAccorpateNull() {
        return readString(Pos.WTIT_NUM_RAT_ACCORPATE_NULL, Len.WTIT_NUM_RAT_ACCORPATE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_NUM_RAT_ACCORPATE = 1;
        public static final int WTIT_NUM_RAT_ACCORPATE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_NUM_RAT_ACCORPATE = 3;
        public static final int WTIT_NUM_RAT_ACCORPATE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_NUM_RAT_ACCORPATE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
