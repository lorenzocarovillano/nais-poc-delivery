package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.TabGiorni;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0004<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0004Data {

    //==== PROPERTIES ====
    //Original name: RESTO
    private short resto = DefaultValues.SHORT_VAL;
    //Original name: RISULT
    private short risult = DefaultValues.SHORT_VAL;
    //Original name: TAB-GIORNI
    private TabGiorni tabGiorni = new TabGiorni();
    //Original name: COM-DATA
    private ComData comData = new ComData();

    //==== METHODS ====
    public void setResto(short resto) {
        this.resto = resto;
    }

    public short getResto() {
        return this.resto;
    }

    public void setRisult(short risult) {
        this.risult = risult;
    }

    public short getRisult() {
        return this.risult;
    }

    public ComData getComData() {
        return comData;
    }

    public TabGiorni getTabGiorni() {
        return tabGiorni;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RESTO = 1;
        public static final int RISULT = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
