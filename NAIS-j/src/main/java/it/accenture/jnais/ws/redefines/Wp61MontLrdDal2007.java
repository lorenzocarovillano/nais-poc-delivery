package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP61-MONT-LRD-DAL2007<br>
 * Variable: WP61-MONT-LRD-DAL2007 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp61MontLrdDal2007 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp61MontLrdDal2007() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP61_MONT_LRD_DAL2007;
    }

    public void setWp61MontLrdDal2007(AfDecimal wp61MontLrdDal2007) {
        writeDecimalAsPacked(Pos.WP61_MONT_LRD_DAL2007, wp61MontLrdDal2007.copy());
    }

    public void setWp61MontLrdDal2007FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP61_MONT_LRD_DAL2007, Pos.WP61_MONT_LRD_DAL2007);
    }

    /**Original name: WP61-MONT-LRD-DAL2007<br>*/
    public AfDecimal getWp61MontLrdDal2007() {
        return readPackedAsDecimal(Pos.WP61_MONT_LRD_DAL2007, Len.Int.WP61_MONT_LRD_DAL2007, Len.Fract.WP61_MONT_LRD_DAL2007);
    }

    public byte[] getWp61MontLrdDal2007AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP61_MONT_LRD_DAL2007, Pos.WP61_MONT_LRD_DAL2007);
        return buffer;
    }

    public void setWp61MontLrdDal2007Null(String wp61MontLrdDal2007Null) {
        writeString(Pos.WP61_MONT_LRD_DAL2007_NULL, wp61MontLrdDal2007Null, Len.WP61_MONT_LRD_DAL2007_NULL);
    }

    /**Original name: WP61-MONT-LRD-DAL2007-NULL<br>*/
    public String getWp61MontLrdDal2007Null() {
        return readString(Pos.WP61_MONT_LRD_DAL2007_NULL, Len.WP61_MONT_LRD_DAL2007_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP61_MONT_LRD_DAL2007 = 1;
        public static final int WP61_MONT_LRD_DAL2007_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_MONT_LRD_DAL2007 = 8;
        public static final int WP61_MONT_LRD_DAL2007_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP61_MONT_LRD_DAL2007 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_MONT_LRD_DAL2007 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
