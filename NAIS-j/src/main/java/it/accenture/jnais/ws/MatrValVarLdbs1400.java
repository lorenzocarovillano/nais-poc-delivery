package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.MvvIdpMatrValVar;
import it.accenture.jnais.ws.redefines.MvvTipoMovimento;

/**Original name: MATR-VAL-VAR<br>
 * Variable: MATR-VAL-VAR from copybook IDBVMVV1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class MatrValVarLdbs1400 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: MVV-ID-MATR-VAL-VAR
    private int mvvIdMatrValVar = DefaultValues.BIN_INT_VAL;
    //Original name: MVV-IDP-MATR-VAL-VAR
    private MvvIdpMatrValVar mvvIdpMatrValVar = new MvvIdpMatrValVar();
    //Original name: MVV-COD-COMPAGNIA-ANIA
    private int mvvCodCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: MVV-TIPO-MOVIMENTO
    private MvvTipoMovimento mvvTipoMovimento = new MvvTipoMovimento();
    //Original name: MVV-COD-DATO-EXT
    private String mvvCodDatoExt = DefaultValues.stringVal(Len.MVV_COD_DATO_EXT);
    //Original name: MVV-OBBLIGATORIETA
    private char mvvObbligatorieta = DefaultValues.CHAR_VAL;
    //Original name: MVV-VALORE-DEFAULT
    private String mvvValoreDefault = DefaultValues.stringVal(Len.MVV_VALORE_DEFAULT);
    //Original name: MVV-COD-STR-DATO-PTF
    private String mvvCodStrDatoPtf = DefaultValues.stringVal(Len.MVV_COD_STR_DATO_PTF);
    //Original name: MVV-COD-DATO-PTF
    private String mvvCodDatoPtf = DefaultValues.stringVal(Len.MVV_COD_DATO_PTF);
    //Original name: MVV-COD-PARAMETRO
    private String mvvCodParametro = DefaultValues.stringVal(Len.MVV_COD_PARAMETRO);
    //Original name: MVV-OPERAZIONE
    private String mvvOperazione = DefaultValues.stringVal(Len.MVV_OPERAZIONE);
    //Original name: MVV-LIVELLO-OPERAZIONE
    private String mvvLivelloOperazione = DefaultValues.stringVal(Len.MVV_LIVELLO_OPERAZIONE);
    //Original name: MVV-TIPO-OGGETTO
    private String mvvTipoOggetto = DefaultValues.stringVal(Len.MVV_TIPO_OGGETTO);
    //Original name: MVV-WHERE-CONDITION-LEN
    private short mvvWhereConditionLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: MVV-WHERE-CONDITION
    private String mvvWhereCondition = DefaultValues.stringVal(Len.MVV_WHERE_CONDITION);
    //Original name: MVV-SERVIZIO-LETTURA
    private String mvvServizioLettura = DefaultValues.stringVal(Len.MVV_SERVIZIO_LETTURA);
    //Original name: MVV-MODULO-CALCOLO
    private String mvvModuloCalcolo = DefaultValues.stringVal(Len.MVV_MODULO_CALCOLO);
    //Original name: MVV-STEP-VALORIZZATORE
    private char mvvStepValorizzatore = DefaultValues.CHAR_VAL;
    //Original name: MVV-STEP-CONVERSAZIONE
    private char mvvStepConversazione = DefaultValues.CHAR_VAL;
    //Original name: MVV-OPER-LOG-STATO-BUS
    private String mvvOperLogStatoBus = DefaultValues.stringVal(Len.MVV_OPER_LOG_STATO_BUS);
    //Original name: MVV-ARRAY-STATO-BUS
    private String mvvArrayStatoBus = DefaultValues.stringVal(Len.MVV_ARRAY_STATO_BUS);
    //Original name: MVV-OPER-LOG-CAUSALE
    private String mvvOperLogCausale = DefaultValues.stringVal(Len.MVV_OPER_LOG_CAUSALE);
    //Original name: MVV-ARRAY-CAUSALE
    private String mvvArrayCausale = DefaultValues.stringVal(Len.MVV_ARRAY_CAUSALE);
    //Original name: MVV-COD-DATO-INTERNO
    private String mvvCodDatoInterno = DefaultValues.stringVal(Len.MVV_COD_DATO_INTERNO);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MATR_VAL_VAR;
    }

    @Override
    public void deserialize(byte[] buf) {
        setMatrValVarBytes(buf);
    }

    public String getMatrValVarFormatted() {
        return MarshalByteExt.bufferToStr(getMatrValVarBytes());
    }

    public void setMatrValVarBytes(byte[] buffer) {
        setMatrValVarBytes(buffer, 1);
    }

    public byte[] getMatrValVarBytes() {
        byte[] buffer = new byte[Len.MATR_VAL_VAR];
        return getMatrValVarBytes(buffer, 1);
    }

    public void setMatrValVarBytes(byte[] buffer, int offset) {
        int position = offset;
        mvvIdMatrValVar = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        mvvIdpMatrValVar.setMvvIdpMatrValVarFromBuffer(buffer, position);
        position += Types.INT_SIZE;
        mvvCodCompagniaAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MVV_COD_COMPAGNIA_ANIA, 0);
        position += Len.MVV_COD_COMPAGNIA_ANIA;
        mvvTipoMovimento.setMvvTipoMovimentoFromBuffer(buffer, position);
        position += MvvTipoMovimento.Len.MVV_TIPO_MOVIMENTO;
        mvvCodDatoExt = MarshalByte.readString(buffer, position, Len.MVV_COD_DATO_EXT);
        position += Len.MVV_COD_DATO_EXT;
        mvvObbligatorieta = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        mvvValoreDefault = MarshalByte.readString(buffer, position, Len.MVV_VALORE_DEFAULT);
        position += Len.MVV_VALORE_DEFAULT;
        mvvCodStrDatoPtf = MarshalByte.readString(buffer, position, Len.MVV_COD_STR_DATO_PTF);
        position += Len.MVV_COD_STR_DATO_PTF;
        mvvCodDatoPtf = MarshalByte.readString(buffer, position, Len.MVV_COD_DATO_PTF);
        position += Len.MVV_COD_DATO_PTF;
        mvvCodParametro = MarshalByte.readString(buffer, position, Len.MVV_COD_PARAMETRO);
        position += Len.MVV_COD_PARAMETRO;
        mvvOperazione = MarshalByte.readString(buffer, position, Len.MVV_OPERAZIONE);
        position += Len.MVV_OPERAZIONE;
        mvvLivelloOperazione = MarshalByte.readString(buffer, position, Len.MVV_LIVELLO_OPERAZIONE);
        position += Len.MVV_LIVELLO_OPERAZIONE;
        mvvTipoOggetto = MarshalByte.readString(buffer, position, Len.MVV_TIPO_OGGETTO);
        position += Len.MVV_TIPO_OGGETTO;
        setMvvWhereConditionVcharBytes(buffer, position);
        position += Len.MVV_WHERE_CONDITION_VCHAR;
        mvvServizioLettura = MarshalByte.readString(buffer, position, Len.MVV_SERVIZIO_LETTURA);
        position += Len.MVV_SERVIZIO_LETTURA;
        mvvModuloCalcolo = MarshalByte.readString(buffer, position, Len.MVV_MODULO_CALCOLO);
        position += Len.MVV_MODULO_CALCOLO;
        mvvStepValorizzatore = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        mvvStepConversazione = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        mvvOperLogStatoBus = MarshalByte.readString(buffer, position, Len.MVV_OPER_LOG_STATO_BUS);
        position += Len.MVV_OPER_LOG_STATO_BUS;
        mvvArrayStatoBus = MarshalByte.readString(buffer, position, Len.MVV_ARRAY_STATO_BUS);
        position += Len.MVV_ARRAY_STATO_BUS;
        mvvOperLogCausale = MarshalByte.readString(buffer, position, Len.MVV_OPER_LOG_CAUSALE);
        position += Len.MVV_OPER_LOG_CAUSALE;
        mvvArrayCausale = MarshalByte.readString(buffer, position, Len.MVV_ARRAY_CAUSALE);
        position += Len.MVV_ARRAY_CAUSALE;
        mvvCodDatoInterno = MarshalByte.readString(buffer, position, Len.MVV_COD_DATO_INTERNO);
    }

    public byte[] getMatrValVarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, mvvIdMatrValVar);
        position += Types.INT_SIZE;
        mvvIdpMatrValVar.getMvvIdpMatrValVarAsBuffer(buffer, position);
        position += Types.INT_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, mvvCodCompagniaAnia, Len.Int.MVV_COD_COMPAGNIA_ANIA, 0);
        position += Len.MVV_COD_COMPAGNIA_ANIA;
        mvvTipoMovimento.getMvvTipoMovimentoAsBuffer(buffer, position);
        position += MvvTipoMovimento.Len.MVV_TIPO_MOVIMENTO;
        MarshalByte.writeString(buffer, position, mvvCodDatoExt, Len.MVV_COD_DATO_EXT);
        position += Len.MVV_COD_DATO_EXT;
        MarshalByte.writeChar(buffer, position, mvvObbligatorieta);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, mvvValoreDefault, Len.MVV_VALORE_DEFAULT);
        position += Len.MVV_VALORE_DEFAULT;
        MarshalByte.writeString(buffer, position, mvvCodStrDatoPtf, Len.MVV_COD_STR_DATO_PTF);
        position += Len.MVV_COD_STR_DATO_PTF;
        MarshalByte.writeString(buffer, position, mvvCodDatoPtf, Len.MVV_COD_DATO_PTF);
        position += Len.MVV_COD_DATO_PTF;
        MarshalByte.writeString(buffer, position, mvvCodParametro, Len.MVV_COD_PARAMETRO);
        position += Len.MVV_COD_PARAMETRO;
        MarshalByte.writeString(buffer, position, mvvOperazione, Len.MVV_OPERAZIONE);
        position += Len.MVV_OPERAZIONE;
        MarshalByte.writeString(buffer, position, mvvLivelloOperazione, Len.MVV_LIVELLO_OPERAZIONE);
        position += Len.MVV_LIVELLO_OPERAZIONE;
        MarshalByte.writeString(buffer, position, mvvTipoOggetto, Len.MVV_TIPO_OGGETTO);
        position += Len.MVV_TIPO_OGGETTO;
        getMvvWhereConditionVcharBytes(buffer, position);
        position += Len.MVV_WHERE_CONDITION_VCHAR;
        MarshalByte.writeString(buffer, position, mvvServizioLettura, Len.MVV_SERVIZIO_LETTURA);
        position += Len.MVV_SERVIZIO_LETTURA;
        MarshalByte.writeString(buffer, position, mvvModuloCalcolo, Len.MVV_MODULO_CALCOLO);
        position += Len.MVV_MODULO_CALCOLO;
        MarshalByte.writeChar(buffer, position, mvvStepValorizzatore);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, mvvStepConversazione);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, mvvOperLogStatoBus, Len.MVV_OPER_LOG_STATO_BUS);
        position += Len.MVV_OPER_LOG_STATO_BUS;
        MarshalByte.writeString(buffer, position, mvvArrayStatoBus, Len.MVV_ARRAY_STATO_BUS);
        position += Len.MVV_ARRAY_STATO_BUS;
        MarshalByte.writeString(buffer, position, mvvOperLogCausale, Len.MVV_OPER_LOG_CAUSALE);
        position += Len.MVV_OPER_LOG_CAUSALE;
        MarshalByte.writeString(buffer, position, mvvArrayCausale, Len.MVV_ARRAY_CAUSALE);
        position += Len.MVV_ARRAY_CAUSALE;
        MarshalByte.writeString(buffer, position, mvvCodDatoInterno, Len.MVV_COD_DATO_INTERNO);
        return buffer;
    }

    public void setMvvIdMatrValVar(int mvvIdMatrValVar) {
        this.mvvIdMatrValVar = mvvIdMatrValVar;
    }

    public int getMvvIdMatrValVar() {
        return this.mvvIdMatrValVar;
    }

    public void setMvvCodCompagniaAnia(int mvvCodCompagniaAnia) {
        this.mvvCodCompagniaAnia = mvvCodCompagniaAnia;
    }

    public int getMvvCodCompagniaAnia() {
        return this.mvvCodCompagniaAnia;
    }

    public void setMvvCodDatoExt(String mvvCodDatoExt) {
        this.mvvCodDatoExt = Functions.subString(mvvCodDatoExt, Len.MVV_COD_DATO_EXT);
    }

    public String getMvvCodDatoExt() {
        return this.mvvCodDatoExt;
    }

    public void setMvvObbligatorieta(char mvvObbligatorieta) {
        this.mvvObbligatorieta = mvvObbligatorieta;
    }

    public char getMvvObbligatorieta() {
        return this.mvvObbligatorieta;
    }

    public void setMvvValoreDefault(String mvvValoreDefault) {
        this.mvvValoreDefault = Functions.subString(mvvValoreDefault, Len.MVV_VALORE_DEFAULT);
    }

    public String getMvvValoreDefault() {
        return this.mvvValoreDefault;
    }

    public void setMvvCodStrDatoPtf(String mvvCodStrDatoPtf) {
        this.mvvCodStrDatoPtf = Functions.subString(mvvCodStrDatoPtf, Len.MVV_COD_STR_DATO_PTF);
    }

    public String getMvvCodStrDatoPtf() {
        return this.mvvCodStrDatoPtf;
    }

    public void setMvvCodDatoPtf(String mvvCodDatoPtf) {
        this.mvvCodDatoPtf = Functions.subString(mvvCodDatoPtf, Len.MVV_COD_DATO_PTF);
    }

    public String getMvvCodDatoPtf() {
        return this.mvvCodDatoPtf;
    }

    public void setMvvCodParametro(String mvvCodParametro) {
        this.mvvCodParametro = Functions.subString(mvvCodParametro, Len.MVV_COD_PARAMETRO);
    }

    public String getMvvCodParametro() {
        return this.mvvCodParametro;
    }

    public void setMvvOperazione(String mvvOperazione) {
        this.mvvOperazione = Functions.subString(mvvOperazione, Len.MVV_OPERAZIONE);
    }

    public String getMvvOperazione() {
        return this.mvvOperazione;
    }

    public void setMvvLivelloOperazione(String mvvLivelloOperazione) {
        this.mvvLivelloOperazione = Functions.subString(mvvLivelloOperazione, Len.MVV_LIVELLO_OPERAZIONE);
    }

    public String getMvvLivelloOperazione() {
        return this.mvvLivelloOperazione;
    }

    public void setMvvTipoOggetto(String mvvTipoOggetto) {
        this.mvvTipoOggetto = Functions.subString(mvvTipoOggetto, Len.MVV_TIPO_OGGETTO);
    }

    public String getMvvTipoOggetto() {
        return this.mvvTipoOggetto;
    }

    public void setMvvWhereConditionVcharFormatted(String data) {
        byte[] buffer = new byte[Len.MVV_WHERE_CONDITION_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.MVV_WHERE_CONDITION_VCHAR);
        setMvvWhereConditionVcharBytes(buffer, 1);
    }

    public String getMvvWhereConditionVcharFormatted() {
        return MarshalByteExt.bufferToStr(getMvvWhereConditionVcharBytes());
    }

    /**Original name: MVV-WHERE-CONDITION-VCHAR<br>*/
    public byte[] getMvvWhereConditionVcharBytes() {
        byte[] buffer = new byte[Len.MVV_WHERE_CONDITION_VCHAR];
        return getMvvWhereConditionVcharBytes(buffer, 1);
    }

    public void setMvvWhereConditionVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        mvvWhereConditionLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        mvvWhereCondition = MarshalByte.readString(buffer, position, Len.MVV_WHERE_CONDITION);
    }

    public byte[] getMvvWhereConditionVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, mvvWhereConditionLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, mvvWhereCondition, Len.MVV_WHERE_CONDITION);
        return buffer;
    }

    public void setMvvWhereConditionLen(short mvvWhereConditionLen) {
        this.mvvWhereConditionLen = mvvWhereConditionLen;
    }

    public short getMvvWhereConditionLen() {
        return this.mvvWhereConditionLen;
    }

    public void setMvvWhereCondition(String mvvWhereCondition) {
        this.mvvWhereCondition = Functions.subString(mvvWhereCondition, Len.MVV_WHERE_CONDITION);
    }

    public String getMvvWhereCondition() {
        return this.mvvWhereCondition;
    }

    public void setMvvServizioLettura(String mvvServizioLettura) {
        this.mvvServizioLettura = Functions.subString(mvvServizioLettura, Len.MVV_SERVIZIO_LETTURA);
    }

    public String getMvvServizioLettura() {
        return this.mvvServizioLettura;
    }

    public void setMvvModuloCalcolo(String mvvModuloCalcolo) {
        this.mvvModuloCalcolo = Functions.subString(mvvModuloCalcolo, Len.MVV_MODULO_CALCOLO);
    }

    public String getMvvModuloCalcolo() {
        return this.mvvModuloCalcolo;
    }

    public void setMvvStepValorizzatore(char mvvStepValorizzatore) {
        this.mvvStepValorizzatore = mvvStepValorizzatore;
    }

    public void setMvvStepValorizzatoreFormatted(String mvvStepValorizzatore) {
        setMvvStepValorizzatore(Functions.charAt(mvvStepValorizzatore, Types.CHAR_SIZE));
    }

    public char getMvvStepValorizzatore() {
        return this.mvvStepValorizzatore;
    }

    public void setMvvStepConversazione(char mvvStepConversazione) {
        this.mvvStepConversazione = mvvStepConversazione;
    }

    public char getMvvStepConversazione() {
        return this.mvvStepConversazione;
    }

    public void setMvvOperLogStatoBus(String mvvOperLogStatoBus) {
        this.mvvOperLogStatoBus = Functions.subString(mvvOperLogStatoBus, Len.MVV_OPER_LOG_STATO_BUS);
    }

    public String getMvvOperLogStatoBus() {
        return this.mvvOperLogStatoBus;
    }

    public void setMvvArrayStatoBus(String mvvArrayStatoBus) {
        this.mvvArrayStatoBus = Functions.subString(mvvArrayStatoBus, Len.MVV_ARRAY_STATO_BUS);
    }

    public String getMvvArrayStatoBus() {
        return this.mvvArrayStatoBus;
    }

    public void setMvvOperLogCausale(String mvvOperLogCausale) {
        this.mvvOperLogCausale = Functions.subString(mvvOperLogCausale, Len.MVV_OPER_LOG_CAUSALE);
    }

    public String getMvvOperLogCausale() {
        return this.mvvOperLogCausale;
    }

    public void setMvvArrayCausale(String mvvArrayCausale) {
        this.mvvArrayCausale = Functions.subString(mvvArrayCausale, Len.MVV_ARRAY_CAUSALE);
    }

    public String getMvvArrayCausale() {
        return this.mvvArrayCausale;
    }

    public void setMvvCodDatoInterno(String mvvCodDatoInterno) {
        this.mvvCodDatoInterno = Functions.subString(mvvCodDatoInterno, Len.MVV_COD_DATO_INTERNO);
    }

    public String getMvvCodDatoInterno() {
        return this.mvvCodDatoInterno;
    }

    public MvvIdpMatrValVar getMvvIdpMatrValVar() {
        return mvvIdpMatrValVar;
    }

    public MvvTipoMovimento getMvvTipoMovimento() {
        return mvvTipoMovimento;
    }

    @Override
    public byte[] serialize() {
        return getMatrValVarBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MVV_ID_MATR_VAL_VAR = 4;
        public static final int MVV_COD_COMPAGNIA_ANIA = 3;
        public static final int MVV_COD_DATO_EXT = 30;
        public static final int MVV_OBBLIGATORIETA = 1;
        public static final int MVV_VALORE_DEFAULT = 50;
        public static final int MVV_COD_STR_DATO_PTF = 30;
        public static final int MVV_COD_DATO_PTF = 30;
        public static final int MVV_COD_PARAMETRO = 20;
        public static final int MVV_OPERAZIONE = 15;
        public static final int MVV_LIVELLO_OPERAZIONE = 3;
        public static final int MVV_TIPO_OGGETTO = 2;
        public static final int MVV_WHERE_CONDITION_LEN = 2;
        public static final int MVV_WHERE_CONDITION = 300;
        public static final int MVV_WHERE_CONDITION_VCHAR = MVV_WHERE_CONDITION_LEN + MVV_WHERE_CONDITION;
        public static final int MVV_SERVIZIO_LETTURA = 8;
        public static final int MVV_MODULO_CALCOLO = 8;
        public static final int MVV_STEP_VALORIZZATORE = 1;
        public static final int MVV_STEP_CONVERSAZIONE = 1;
        public static final int MVV_OPER_LOG_STATO_BUS = 3;
        public static final int MVV_ARRAY_STATO_BUS = 20;
        public static final int MVV_OPER_LOG_CAUSALE = 3;
        public static final int MVV_ARRAY_CAUSALE = 20;
        public static final int MVV_COD_DATO_INTERNO = 30;
        public static final int MATR_VAL_VAR = MVV_ID_MATR_VAL_VAR + MvvIdpMatrValVar.Len.MVV_IDP_MATR_VAL_VAR + MVV_COD_COMPAGNIA_ANIA + MvvTipoMovimento.Len.MVV_TIPO_MOVIMENTO + MVV_COD_DATO_EXT + MVV_OBBLIGATORIETA + MVV_VALORE_DEFAULT + MVV_COD_STR_DATO_PTF + MVV_COD_DATO_PTF + MVV_COD_PARAMETRO + MVV_OPERAZIONE + MVV_LIVELLO_OPERAZIONE + MVV_TIPO_OGGETTO + MVV_WHERE_CONDITION_VCHAR + MVV_SERVIZIO_LETTURA + MVV_MODULO_CALCOLO + MVV_STEP_VALORIZZATORE + MVV_STEP_CONVERSAZIONE + MVV_OPER_LOG_STATO_BUS + MVV_ARRAY_STATO_BUS + MVV_OPER_LOG_CAUSALE + MVV_ARRAY_CAUSALE + MVV_COD_DATO_INTERNO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MVV_COD_COMPAGNIA_ANIA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
