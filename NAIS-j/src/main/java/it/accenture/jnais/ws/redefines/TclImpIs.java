package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-IS<br>
 * Variable: TCL-IMP-IS from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_IS;
    }

    public void setTclImpIs(AfDecimal tclImpIs) {
        writeDecimalAsPacked(Pos.TCL_IMP_IS, tclImpIs.copy());
    }

    public void setTclImpIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_IS, Pos.TCL_IMP_IS);
    }

    /**Original name: TCL-IMP-IS<br>*/
    public AfDecimal getTclImpIs() {
        return readPackedAsDecimal(Pos.TCL_IMP_IS, Len.Int.TCL_IMP_IS, Len.Fract.TCL_IMP_IS);
    }

    public byte[] getTclImpIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_IS, Pos.TCL_IMP_IS);
        return buffer;
    }

    public void setTclImpIsNull(String tclImpIsNull) {
        writeString(Pos.TCL_IMP_IS_NULL, tclImpIsNull, Len.TCL_IMP_IS_NULL);
    }

    /**Original name: TCL-IMP-IS-NULL<br>*/
    public String getTclImpIsNull() {
        return readString(Pos.TCL_IMP_IS_NULL, Len.TCL_IMP_IS_NULL);
    }

    public String getTclImpIsNullFormatted() {
        return Functions.padBlanks(getTclImpIsNull(), Len.TCL_IMP_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_IS = 1;
        public static final int TCL_IMP_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_IS = 8;
        public static final int TCL_IMP_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
