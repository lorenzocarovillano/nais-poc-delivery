package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-IMP-VOLO<br>
 * Variable: WTDR-IMP-VOLO from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_IMP_VOLO;
    }

    public void setWtdrImpVolo(AfDecimal wtdrImpVolo) {
        writeDecimalAsPacked(Pos.WTDR_IMP_VOLO, wtdrImpVolo.copy());
    }

    /**Original name: WTDR-IMP-VOLO<br>*/
    public AfDecimal getWtdrImpVolo() {
        return readPackedAsDecimal(Pos.WTDR_IMP_VOLO, Len.Int.WTDR_IMP_VOLO, Len.Fract.WTDR_IMP_VOLO);
    }

    public void setWtdrImpVoloNull(String wtdrImpVoloNull) {
        writeString(Pos.WTDR_IMP_VOLO_NULL, wtdrImpVoloNull, Len.WTDR_IMP_VOLO_NULL);
    }

    /**Original name: WTDR-IMP-VOLO-NULL<br>*/
    public String getWtdrImpVoloNull() {
        return readString(Pos.WTDR_IMP_VOLO_NULL, Len.WTDR_IMP_VOLO_NULL);
    }

    public String getWtdrImpVoloNullFormatted() {
        return Functions.padBlanks(getWtdrImpVoloNull(), Len.WTDR_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_VOLO = 1;
        public static final int WTDR_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_VOLO = 8;
        public static final int WTDR_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
