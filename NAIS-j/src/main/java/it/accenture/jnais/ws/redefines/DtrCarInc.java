package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-CAR-INC<br>
 * Variable: DTR-CAR-INC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrCarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrCarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_CAR_INC;
    }

    public void setDtrCarInc(AfDecimal dtrCarInc) {
        writeDecimalAsPacked(Pos.DTR_CAR_INC, dtrCarInc.copy());
    }

    public void setDtrCarIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_CAR_INC, Pos.DTR_CAR_INC);
    }

    /**Original name: DTR-CAR-INC<br>*/
    public AfDecimal getDtrCarInc() {
        return readPackedAsDecimal(Pos.DTR_CAR_INC, Len.Int.DTR_CAR_INC, Len.Fract.DTR_CAR_INC);
    }

    public byte[] getDtrCarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_CAR_INC, Pos.DTR_CAR_INC);
        return buffer;
    }

    public void setDtrCarIncNull(String dtrCarIncNull) {
        writeString(Pos.DTR_CAR_INC_NULL, dtrCarIncNull, Len.DTR_CAR_INC_NULL);
    }

    /**Original name: DTR-CAR-INC-NULL<br>*/
    public String getDtrCarIncNull() {
        return readString(Pos.DTR_CAR_INC_NULL, Len.DTR_CAR_INC_NULL);
    }

    public String getDtrCarIncNullFormatted() {
        return Functions.padBlanks(getDtrCarIncNull(), Len.DTR_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_CAR_INC = 1;
        public static final int DTR_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_CAR_INC = 8;
        public static final int DTR_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
