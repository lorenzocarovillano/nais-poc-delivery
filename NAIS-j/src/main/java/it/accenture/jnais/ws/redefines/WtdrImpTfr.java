package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-IMP-TFR<br>
 * Variable: WTDR-IMP-TFR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_IMP_TFR;
    }

    public void setWtdrImpTfr(AfDecimal wtdrImpTfr) {
        writeDecimalAsPacked(Pos.WTDR_IMP_TFR, wtdrImpTfr.copy());
    }

    /**Original name: WTDR-IMP-TFR<br>*/
    public AfDecimal getWtdrImpTfr() {
        return readPackedAsDecimal(Pos.WTDR_IMP_TFR, Len.Int.WTDR_IMP_TFR, Len.Fract.WTDR_IMP_TFR);
    }

    public void setWtdrImpTfrNull(String wtdrImpTfrNull) {
        writeString(Pos.WTDR_IMP_TFR_NULL, wtdrImpTfrNull, Len.WTDR_IMP_TFR_NULL);
    }

    /**Original name: WTDR-IMP-TFR-NULL<br>*/
    public String getWtdrImpTfrNull() {
        return readString(Pos.WTDR_IMP_TFR_NULL, Len.WTDR_IMP_TFR_NULL);
    }

    public String getWtdrImpTfrNullFormatted() {
        return Functions.padBlanks(getWtdrImpTfrNull(), Len.WTDR_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_TFR = 1;
        public static final int WTDR_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_TFR = 8;
        public static final int WTDR_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
