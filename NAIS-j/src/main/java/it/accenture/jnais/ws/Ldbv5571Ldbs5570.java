package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LDBV5571<br>
 * Variable: LDBV5571 from copybook LDBV5571<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv5571Ldbs5570 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV5571-ID-OGG-DER
    private int idOggDer = DefaultValues.INT_VAL;
    //Original name: LDBV5571-TP-OGG-DER
    private String tpOggDer = DefaultValues.stringVal(Len.TP_OGG_DER);
    //Original name: LDBV5571-TP-COLL-1
    private String tpColl1 = DefaultValues.stringVal(Len.TP_COLL1);
    //Original name: LDBV5571-TP-COLL-2
    private String tpColl2 = DefaultValues.stringVal(Len.TP_COLL2);
    //Original name: LDBV5571-TP-COLL-3
    private String tpColl3 = DefaultValues.stringVal(Len.TP_COLL3);
    //Original name: LDBV5571-TOT-IMP-COLL
    private AfDecimal totImpColl = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV5571;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv5571Bytes(buf);
    }

    public void setLdbv5571Bytes(byte[] buffer) {
        setLdbv5571Bytes(buffer, 1);
    }

    public byte[] getLdbv5571Bytes() {
        byte[] buffer = new byte[Len.LDBV5571];
        return getLdbv5571Bytes(buffer, 1);
    }

    public void setLdbv5571Bytes(byte[] buffer, int offset) {
        int position = offset;
        setInputBytes(buffer, position);
        position += Len.INPUT;
        setOutputBytes(buffer, position);
    }

    public byte[] getLdbv5571Bytes(byte[] buffer, int offset) {
        int position = offset;
        getInputBytes(buffer, position);
        position += Len.INPUT;
        getOutputBytes(buffer, position);
        return buffer;
    }

    public void setInputBytes(byte[] buffer, int offset) {
        int position = offset;
        idOggDer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG_DER, 0);
        position += Len.ID_OGG_DER;
        tpOggDer = MarshalByte.readString(buffer, position, Len.TP_OGG_DER);
        position += Len.TP_OGG_DER;
        tpColl1 = MarshalByte.readString(buffer, position, Len.TP_COLL1);
        position += Len.TP_COLL1;
        tpColl2 = MarshalByte.readString(buffer, position, Len.TP_COLL2);
        position += Len.TP_COLL2;
        tpColl3 = MarshalByte.readString(buffer, position, Len.TP_COLL3);
    }

    public byte[] getInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOggDer, Len.Int.ID_OGG_DER, 0);
        position += Len.ID_OGG_DER;
        MarshalByte.writeString(buffer, position, tpOggDer, Len.TP_OGG_DER);
        position += Len.TP_OGG_DER;
        MarshalByte.writeString(buffer, position, tpColl1, Len.TP_COLL1);
        position += Len.TP_COLL1;
        MarshalByte.writeString(buffer, position, tpColl2, Len.TP_COLL2);
        position += Len.TP_COLL2;
        MarshalByte.writeString(buffer, position, tpColl3, Len.TP_COLL3);
        return buffer;
    }

    public void setIdOggDer(int idOggDer) {
        this.idOggDer = idOggDer;
    }

    public int getIdOggDer() {
        return this.idOggDer;
    }

    public void setTpOggDer(String tpOggDer) {
        this.tpOggDer = Functions.subString(tpOggDer, Len.TP_OGG_DER);
    }

    public String getTpOggDer() {
        return this.tpOggDer;
    }

    public void setTpColl1(String tpColl1) {
        this.tpColl1 = Functions.subString(tpColl1, Len.TP_COLL1);
    }

    public String getTpColl1() {
        return this.tpColl1;
    }

    public void setTpColl2(String tpColl2) {
        this.tpColl2 = Functions.subString(tpColl2, Len.TP_COLL2);
    }

    public String getTpColl2() {
        return this.tpColl2;
    }

    public void setTpColl3(String tpColl3) {
        this.tpColl3 = Functions.subString(tpColl3, Len.TP_COLL3);
    }

    public String getTpColl3() {
        return this.tpColl3;
    }

    public void setOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        totImpColl.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.TOT_IMP_COLL, Len.Fract.TOT_IMP_COLL));
    }

    public byte[] getOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimalAsPacked(buffer, position, totImpColl.copy());
        return buffer;
    }

    public void setTotImpColl(AfDecimal totImpColl) {
        this.totImpColl.assign(totImpColl);
    }

    public AfDecimal getTotImpColl() {
        return this.totImpColl.copy();
    }

    @Override
    public byte[] serialize() {
        return getLdbv5571Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG_DER = 5;
        public static final int TP_OGG_DER = 2;
        public static final int TP_COLL1 = 2;
        public static final int TP_COLL2 = 2;
        public static final int TP_COLL3 = 2;
        public static final int INPUT = ID_OGG_DER + TP_OGG_DER + TP_COLL1 + TP_COLL2 + TP_COLL3;
        public static final int TOT_IMP_COLL = 8;
        public static final int OUTPUT = TOT_IMP_COLL;
        public static final int LDBV5571 = INPUT + OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG_DER = 9;
            public static final int TOT_IMP_COLL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TOT_IMP_COLL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
