package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: STB-ID-MOVI-CHIU<br>
 * Variable: STB-ID-MOVI-CHIU from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class StbIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public StbIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.STB_ID_MOVI_CHIU;
    }

    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        writeIntAsPacked(Pos.STB_ID_MOVI_CHIU, stbIdMoviChiu, Len.Int.STB_ID_MOVI_CHIU);
    }

    public void setStbIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.STB_ID_MOVI_CHIU, Pos.STB_ID_MOVI_CHIU);
    }

    /**Original name: STB-ID-MOVI-CHIU<br>*/
    public int getStbIdMoviChiu() {
        return readPackedAsInt(Pos.STB_ID_MOVI_CHIU, Len.Int.STB_ID_MOVI_CHIU);
    }

    public byte[] getStbIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.STB_ID_MOVI_CHIU, Pos.STB_ID_MOVI_CHIU);
        return buffer;
    }

    public void setStbIdMoviChiuNull(String stbIdMoviChiuNull) {
        writeString(Pos.STB_ID_MOVI_CHIU_NULL, stbIdMoviChiuNull, Len.STB_ID_MOVI_CHIU_NULL);
    }

    /**Original name: STB-ID-MOVI-CHIU-NULL<br>*/
    public String getStbIdMoviChiuNull() {
        return readString(Pos.STB_ID_MOVI_CHIU_NULL, Len.STB_ID_MOVI_CHIU_NULL);
    }

    public String getStbIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getStbIdMoviChiuNull(), Len.STB_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int STB_ID_MOVI_CHIU = 1;
        public static final int STB_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STB_ID_MOVI_CHIU = 5;
        public static final int STB_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int STB_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
