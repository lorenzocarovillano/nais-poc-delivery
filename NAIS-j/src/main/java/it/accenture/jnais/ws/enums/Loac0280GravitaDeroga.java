package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LOAC0280-GRAVITA-DEROGA<br>
 * Variable: LOAC0280-GRAVITA-DEROGA from copybook LOAC0280<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Loac0280GravitaDeroga {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setGravitaDeroga(char gravitaDeroga) {
        this.value = gravitaDeroga;
    }

    public char getGravitaDeroga() {
        return this.value;
    }

    public boolean isLoac0280DerogaBloccanteSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GRAVITA_DEROGA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
