package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.AdesIvvs0216;
import it.accenture.jnais.copy.GarIvvs0216;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Ldbv5571;
import it.accenture.jnais.copy.Ldbvd511;
import it.accenture.jnais.copy.ParamComp;
import it.accenture.jnais.copy.Poli;
import it.accenture.jnais.copy.StatOggBus;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.ws.enums.ActTpGaranzia;
import it.accenture.jnais.ws.enums.FlCicloGar;
import it.accenture.jnais.ws.enums.FlCicloPol;
import it.accenture.jnais.ws.enums.FlCicloTga;
import it.accenture.jnais.ws.enums.FlScartoPol;
import it.accenture.jnais.ws.enums.WsContraente;
import it.accenture.jnais.ws.enums.WsGaranziaBase;
import it.accenture.jnais.ws.enums.WsTpCaus;
import it.accenture.jnais.ws.enums.WsTpCollgm;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.enums.WsTpRappAna;
import it.accenture.jnais.ws.enums.WsTpStatBus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0033<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0033Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LCCS0033";
    //Original name: WK-TABELLA
    private String wkTabella = "";
    //Original name: WS-DT-INFINITO-1
    private int wsDtInfinito1 = 99991230;
    //Original name: WK-MAX-POL-RECUP-PROVV
    private short wkMaxPolRecupProvv = ((short)15);
    //Original name: IO-A2K-LCCC0003
    private IoA2kLccc0003 ioA2kLccc0003 = new IoA2kLccc0003();
    //Original name: IN-RCODE
    private String inRcode = "00";
    /**Original name: DATA-INF-AMG<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private String dataInfAmg = DefaultValues.stringVal(Len.DATA_INF_AMG);
    //Original name: DATA-SUP-AMG
    private int dataSupAmg = DefaultValues.INT_VAL;
    //Original name: WS-VARIABILI
    private WsVariabiliLccs0033 wsVariabili = new WsVariabiliLccs0033();
    /**Original name: FL-CICLO-POL<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG/SWITCH
	 * ----------------------------------------------------------------*</pre>*/
    private FlCicloPol flCicloPol = new FlCicloPol();
    //Original name: FL-CICLO-GAR
    private FlCicloGar flCicloGar = new FlCicloGar();
    //Original name: FL-CICLO-TGA
    private FlCicloTga flCicloTga = new FlCicloTga();
    //Original name: WS-CONTRAENTE
    private WsContraente wsContraente = new WsContraente();
    //Original name: WS-GARANZIA-BASE
    private WsGaranziaBase wsGaranziaBase = new WsGaranziaBase();
    //Original name: FL-SCARTO-POL
    private FlScartoPol flScartoPol = new FlScartoPol();
    /**Original name: WS-TP-RAPP-ANA<br>
	 * <pre>----------------------------------------------------------------*
	 * --  Tipologiche di PTF
	 * ----------------------------------------------------------------*
	 * --  TP_RAPP_ANA
	 * *****************************************************************
	 *     TP_RAPP_ANA (Tipo Rapporto Anagrafico)
	 * *****************************************************************</pre>*/
    private WsTpRappAna wsTpRappAna = new WsTpRappAna();
    /**Original name: WS-TP-STAT-BUS<br>
	 * <pre>--  TP_STAT_BUS
	 * *****************************************************************
	 *     TP_STAT_BUS (Stato Oggetto di Business)
	 * *****************************************************************</pre>*/
    private WsTpStatBus wsTpStatBus = new WsTpStatBus();
    /**Original name: WS-TP-CAUS<br>
	 * <pre>--  TP_CAUS
	 * *****************************************************************
	 *     TP_CAUS (Tipo Causale)
	 * *****************************************************************</pre>*/
    private WsTpCaus wsTpCaus = new WsTpCaus();
    /**Original name: WS-TP-OGG<br>
	 * <pre>--  TP_OGG
	 * *****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    /**Original name: WS-TP-COLLGM<br>
	 * <pre>--  TP_COLLGM
	 * *****************************************************************
	 *     TP_COLLGM
	 * *****************************************************************</pre>*/
    private WsTpCollgm wsTpCollgm = new WsTpCollgm();
    /**Original name: ACT-TP-GARANZIA<br>
	 * <pre>----------------------------------------------------------------*
	 * --  Tipologiche di ACT
	 * ----------------------------------------------------------------*</pre>*/
    private ActTpGaranzia actTpGaranzia = new ActTpGaranzia();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: PARAM-COMP
    private ParamComp paramComp = new ParamComp();
    //Original name: POLI
    private Poli poli = new Poli();
    //Original name: ADES
    private AdesIvvs0216 ades = new AdesIvvs0216();
    //Original name: GAR
    private GarIvvs0216 gar = new GarIvvs0216();
    //Original name: TRCH-DI-GAR
    private TrchDiGarIvvs0216 trchDiGar = new TrchDiGarIvvs0216();
    //Original name: STAT-OGG-BUS
    private StatOggBus statOggBus = new StatOggBus();
    //Original name: LDBV5561-COD-SOGG
    private String ldbv5561CodSogg = DefaultValues.stringVal(Len.LDBV5561_COD_SOGG);
    //Original name: LDBV5571
    private Ldbv5571 ldbv5571 = new Ldbv5571();
    //Original name: LDBVD511
    private Ldbvd511 ldbvd511 = new Ldbvd511();
    //Original name: WS-INDICI
    private WsIndiciLccs0033 wsIndici = new WsIndiciLccs0033();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public int getWsDtInfinito1() {
        return this.wsDtInfinito1;
    }

    public short getWkMaxPolRecupProvv() {
        return this.wkMaxPolRecupProvv;
    }

    public void setInRcodeFromBuffer(byte[] buffer) {
        inRcode = MarshalByte.readFixedString(buffer, 1, Len.IN_RCODE);
    }

    public String getInRcodeFormatted() {
        return this.inRcode;
    }

    public void setDataInfAmgFormatted(String dataInfAmg) {
        this.dataInfAmg = Trunc.toUnsignedNumeric(dataInfAmg, Len.DATA_INF_AMG);
    }

    public int getDataInfAmg() {
        return NumericDisplay.asInt(this.dataInfAmg);
    }

    public void setDataSupAmg(int dataSupAmg) {
        this.dataSupAmg = dataSupAmg;
    }

    public int getDataSupAmg() {
        return this.dataSupAmg;
    }

    public String getLdbv5561Formatted() {
        return getLdbv5561CodSoggFormatted();
    }

    public void setLdbv5561CodSogg(String ldbv5561CodSogg) {
        this.ldbv5561CodSogg = Functions.subString(ldbv5561CodSogg, Len.LDBV5561_COD_SOGG);
    }

    public String getLdbv5561CodSogg() {
        return this.ldbv5561CodSogg;
    }

    public String getLdbv5561CodSoggFormatted() {
        return Functions.padBlanks(getLdbv5561CodSogg(), Len.LDBV5561_COD_SOGG);
    }

    public ActTpGaranzia getActTpGaranzia() {
        return actTpGaranzia;
    }

    public AdesIvvs0216 getAdes() {
        return ades;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public FlCicloGar getFlCicloGar() {
        return flCicloGar;
    }

    public FlCicloPol getFlCicloPol() {
        return flCicloPol;
    }

    public FlCicloTga getFlCicloTga() {
        return flCicloTga;
    }

    public FlScartoPol getFlScartoPol() {
        return flScartoPol;
    }

    public GarIvvs0216 getGar() {
        return gar;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public IoA2kLccc0003 getIoA2kLccc0003() {
        return ioA2kLccc0003;
    }

    public Ldbv5571 getLdbv5571() {
        return ldbv5571;
    }

    public Ldbvd511 getLdbvd511() {
        return ldbvd511;
    }

    public ParamComp getParamComp() {
        return paramComp;
    }

    public Poli getPoli() {
        return poli;
    }

    public StatOggBus getStatOggBus() {
        return statOggBus;
    }

    public TrchDiGarIvvs0216 getTrchDiGar() {
        return trchDiGar;
    }

    public WsContraente getWsContraente() {
        return wsContraente;
    }

    public WsGaranziaBase getWsGaranziaBase() {
        return wsGaranziaBase;
    }

    public WsIndiciLccs0033 getWsIndici() {
        return wsIndici;
    }

    public WsTpCaus getWsTpCaus() {
        return wsTpCaus;
    }

    public WsTpCollgm getWsTpCollgm() {
        return wsTpCollgm;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    public WsTpRappAna getWsTpRappAna() {
        return wsTpRappAna;
    }

    public WsTpStatBus getWsTpStatBus() {
        return wsTpStatBus;
    }

    public WsVariabiliLccs0033 getWsVariabili() {
        return wsVariabili;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IN_RCODE = 2;
        public static final int DATA_INF_AMG = 8;
        public static final int DATA_SUP_AMG = 8;
        public static final int LDBV5561_COD_SOGG = 20;
        public static final int WK_TABELLA = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
