package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-SCAD-PAG-PRE<br>
 * Variable: WB03-DT-SCAD-PAG-PRE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtScadPagPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtScadPagPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_SCAD_PAG_PRE;
    }

    public void setWb03DtScadPagPre(int wb03DtScadPagPre) {
        writeIntAsPacked(Pos.WB03_DT_SCAD_PAG_PRE, wb03DtScadPagPre, Len.Int.WB03_DT_SCAD_PAG_PRE);
    }

    public void setWb03DtScadPagPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_SCAD_PAG_PRE, Pos.WB03_DT_SCAD_PAG_PRE);
    }

    /**Original name: WB03-DT-SCAD-PAG-PRE<br>*/
    public int getWb03DtScadPagPre() {
        return readPackedAsInt(Pos.WB03_DT_SCAD_PAG_PRE, Len.Int.WB03_DT_SCAD_PAG_PRE);
    }

    public byte[] getWb03DtScadPagPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_SCAD_PAG_PRE, Pos.WB03_DT_SCAD_PAG_PRE);
        return buffer;
    }

    public void setWb03DtScadPagPreNull(String wb03DtScadPagPreNull) {
        writeString(Pos.WB03_DT_SCAD_PAG_PRE_NULL, wb03DtScadPagPreNull, Len.WB03_DT_SCAD_PAG_PRE_NULL);
    }

    /**Original name: WB03-DT-SCAD-PAG-PRE-NULL<br>*/
    public String getWb03DtScadPagPreNull() {
        return readString(Pos.WB03_DT_SCAD_PAG_PRE_NULL, Len.WB03_DT_SCAD_PAG_PRE_NULL);
    }

    public String getWb03DtScadPagPreNullFormatted() {
        return Functions.padBlanks(getWb03DtScadPagPreNull(), Len.WB03_DT_SCAD_PAG_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_SCAD_PAG_PRE = 1;
        public static final int WB03_DT_SCAD_PAG_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_SCAD_PAG_PRE = 5;
        public static final int WB03_DT_SCAD_PAG_PRE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_SCAD_PAG_PRE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
