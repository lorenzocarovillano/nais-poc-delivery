package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDCO-PC-SCON<br>
 * Variable: WDCO-PC-SCON from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdcoPcScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdcoPcScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_PC_SCON;
    }

    public void setWdcoPcScon(AfDecimal wdcoPcScon) {
        writeDecimalAsPacked(Pos.WDCO_PC_SCON, wdcoPcScon.copy());
    }

    public void setWdcoPcSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDCO_PC_SCON, Pos.WDCO_PC_SCON);
    }

    /**Original name: WDCO-PC-SCON<br>*/
    public AfDecimal getWdcoPcScon() {
        return readPackedAsDecimal(Pos.WDCO_PC_SCON, Len.Int.WDCO_PC_SCON, Len.Fract.WDCO_PC_SCON);
    }

    public byte[] getWdcoPcSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDCO_PC_SCON, Pos.WDCO_PC_SCON);
        return buffer;
    }

    public void setWdcoPcSconNull(String wdcoPcSconNull) {
        writeString(Pos.WDCO_PC_SCON_NULL, wdcoPcSconNull, Len.WDCO_PC_SCON_NULL);
    }

    /**Original name: WDCO-PC-SCON-NULL<br>*/
    public String getWdcoPcSconNull() {
        return readString(Pos.WDCO_PC_SCON_NULL, Len.WDCO_PC_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDCO_PC_SCON = 1;
        public static final int WDCO_PC_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_PC_SCON = 4;
        public static final int WDCO_PC_SCON_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDCO_PC_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_PC_SCON = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
