package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0501;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Ldbv4911;
import it.accenture.jnais.ws.enums.FlagFondoTrovato;
import it.accenture.jnais.ws.enums.WkIdCodLivFlag;
import it.accenture.jnais.ws.enums.WkTpValAst;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0101;
import it.accenture.jnais.ws.occurs.WgrzTabGar;
import it.accenture.jnais.ws.occurs.Wl19TabFnd;
import it.accenture.jnais.ws.occurs.WtgaTabTran;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0560<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0560Data {

    //==== PROPERTIES ====
    public static final int DTGA_TAB_TRAN_MAXOCCURS = 20;
    public static final int DGRZ_TAB_GAR_MAXOCCURS = 20;
    public static final int DL19_TAB_FND_MAXOCCURS = 250;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0560";
    //Original name: LDBS4910
    private String ldbs4910 = "LDBS4910";
    //Original name: LDBS1530
    private String ldbs1530 = "LDBS1530";
    //Original name: LDBS5950
    private String ldbs5950 = "LDBS5950";
    /**Original name: WK-VAL-FONDO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkValFondo = new AfDecimal("0", 15, 3);
    //Original name: WK-VAL-FONDO-2DEC
    private AfDecimal wkValFondo2dec = new AfDecimal("0", 14, 2);
    //Original name: WK-ID-COD-LIV-FLAG
    private WkIdCodLivFlag wkIdCodLivFlag = new WkIdCodLivFlag();
    //Original name: FLAG-FONDO-TROVATO
    private FlagFondoTrovato flagFondoTrovato = new FlagFondoTrovato();
    //Original name: WK-ID-MOVI-FINRIO
    private int wkIdMoviFinrio = DefaultValues.INT_VAL;
    //Original name: WK-TP-VAL-AST
    private WkTpValAst wkTpValAst = new WkTpValAst();
    //Original name: WK-APPO-DATE
    private WkAppoDate wkAppoDate = new WkAppoDate();
    //Original name: WK-TEMP-COD-FND
    private String wkTempCodFnd = DefaultValues.stringVal(Len.WK_TEMP_COD_FND);
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>---------------------------------------------------------------*
	 *   COPY TIPOLOGICHE
	 * ---------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0101 wsMovimento = new WsMovimentoLvvs0101();
    //Original name: VAL-AST
    private ValAstLdbs4910 valAst = new ValAstLdbs4910();
    //Original name: MOVI
    private MoviLdbs1530 movi = new MoviLdbs1530();
    //Original name: MOVI-FINRIO
    private MoviFinrioLdbs5950 moviFinrio = new MoviFinrioLdbs5950();
    //Original name: RICH-INVST-FND
    private RichInvstFndIdbsrif0 richInvstFnd = new RichInvstFndIdbsrif0();
    //Original name: RICH-DIS-FND
    private RichDisFnd richDisFnd = new RichDisFnd();
    //Original name: LDBV4911
    private Ldbv4911 ldbv4911 = new Ldbv4911();
    //Original name: DTGA-ELE-TGA-MAX
    private short dtgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTGA-TAB-TRAN
    private WtgaTabTran[] dtgaTabTran = new WtgaTabTran[DTGA_TAB_TRAN_MAXOCCURS];
    //Original name: DGRZ-ELE-GAR-MAX
    private short dgrzEleGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DGRZ-TAB-GAR
    private WgrzTabGar[] dgrzTabGar = new WgrzTabGar[DGRZ_TAB_GAR_MAXOCCURS];
    /**Original name: DL19-ELE-FND-MAX<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY 7 PER LA GESTIONE DELLE OCCURS
	 * ----------------------------------------------------------------*</pre>*/
    private short dl19EleFndMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DL19-TAB-FND
    private Wl19TabFnd[] dl19TabFnd = new Wl19TabFnd[DL19_TAB_FND_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-INDICI
    private IxIndiciLvvs0560 ixIndici = new IxIndiciLvvs0560();
    //Original name: IDSV0501
    private Idsv0501 idsv0501 = new Idsv0501();
    //Original name: INT-REGISTER1
    private short intRegister1 = DefaultValues.SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0560Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dtgaTabTranIdx = 1; dtgaTabTranIdx <= DTGA_TAB_TRAN_MAXOCCURS; dtgaTabTranIdx++) {
            dtgaTabTran[dtgaTabTranIdx - 1] = new WtgaTabTran();
        }
        for (int dgrzTabGarIdx = 1; dgrzTabGarIdx <= DGRZ_TAB_GAR_MAXOCCURS; dgrzTabGarIdx++) {
            dgrzTabGar[dgrzTabGarIdx - 1] = new WgrzTabGar();
        }
        for (int dl19TabFndIdx = 1; dl19TabFndIdx <= DL19_TAB_FND_MAXOCCURS; dl19TabFndIdx++) {
            dl19TabFnd[dl19TabFndIdx - 1] = new Wl19TabFnd();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getLdbs4910() {
        return this.ldbs4910;
    }

    public String getLdbs1530() {
        return this.ldbs1530;
    }

    public String getLdbs5950() {
        return this.ldbs5950;
    }

    public void setWkValFondo(AfDecimal wkValFondo) {
        this.wkValFondo.assign(wkValFondo);
    }

    public AfDecimal getWkValFondo() {
        return this.wkValFondo.copy();
    }

    public void setWkValFondo2dec(AfDecimal wkValFondo2dec) {
        this.wkValFondo2dec.assign(wkValFondo2dec);
    }

    public AfDecimal getWkValFondo2dec() {
        return this.wkValFondo2dec.copy();
    }

    public void setWkIdMoviFinrio(int wkIdMoviFinrio) {
        this.wkIdMoviFinrio = wkIdMoviFinrio;
    }

    public int getWkIdMoviFinrio() {
        return this.wkIdMoviFinrio;
    }

    public void setWkTempCodFnd(String wkTempCodFnd) {
        this.wkTempCodFnd = Functions.subString(wkTempCodFnd, Len.WK_TEMP_COD_FND);
    }

    public String getWkTempCodFnd() {
        return this.wkTempCodFnd;
    }

    public void setDtgaAreaTgaFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TGA];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TGA);
        setDtgaAreaTgaBytes(buffer, 1);
    }

    public void setDtgaAreaTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleTgaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DTGA_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dtgaTabTran[idx - 1].setTabTranBytes(buffer, position);
                position += WtgaTabTran.Len.TAB_TRAN;
            }
            else {
                dtgaTabTran[idx - 1].initTabTranSpaces();
                position += WtgaTabTran.Len.TAB_TRAN;
            }
        }
    }

    public void setDtgaEleTgaMax(short dtgaEleTgaMax) {
        this.dtgaEleTgaMax = dtgaEleTgaMax;
    }

    public short getDtgaEleTgaMax() {
        return this.dtgaEleTgaMax;
    }

    public void setDgrzAreaGraFormatted(String data) {
        byte[] buffer = new byte[Len.DGRZ_AREA_GRA];
        MarshalByte.writeString(buffer, 1, data, Len.DGRZ_AREA_GRA);
        setDgrzAreaGraBytes(buffer, 1);
    }

    public void setDgrzAreaGraBytes(byte[] buffer, int offset) {
        int position = offset;
        dgrzEleGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DGRZ_TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dgrzTabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                dgrzTabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public void setDgrzEleGarMax(short dgrzEleGarMax) {
        this.dgrzEleGarMax = dgrzEleGarMax;
    }

    public short getDgrzEleGarMax() {
        return this.dgrzEleGarMax;
    }

    public void setDl19AreaQuotaFormatted(String data) {
        byte[] buffer = new byte[Len.DL19_AREA_QUOTA];
        MarshalByte.writeString(buffer, 1, data, Len.DL19_AREA_QUOTA);
        setDl19AreaQuotaBytes(buffer, 1);
    }

    public void setDl19AreaQuotaBytes(byte[] buffer, int offset) {
        int position = offset;
        dl19EleFndMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDl19TabellaBytes(buffer, position);
    }

    public void setDl19EleFndMax(short dl19EleFndMax) {
        this.dl19EleFndMax = dl19EleFndMax;
    }

    public short getDl19EleFndMax() {
        return this.dl19EleFndMax;
    }

    public void setDl19TabellaBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= DL19_TAB_FND_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dl19TabFnd[idx - 1].setWl19TabFndBytes(buffer, position);
                position += Wl19TabFnd.Len.WL19_TAB_FND;
            }
            else {
                dl19TabFnd[idx - 1].initWl19TabFndSpaces();
                position += Wl19TabFnd.Len.WL19_TAB_FND;
            }
        }
    }

    public void setIntRegister1(short intRegister1) {
        this.intRegister1 = intRegister1;
    }

    public short getIntRegister1() {
        return this.intRegister1;
    }

    public WgrzTabGar getDgrzTabGar(int idx) {
        return dgrzTabGar[idx - 1];
    }

    public Wl19TabFnd getDl19TabFnd(int idx) {
        return dl19TabFnd[idx - 1];
    }

    public WtgaTabTran getDtgaTabTran(int idx) {
        return dtgaTabTran[idx - 1];
    }

    public FlagFondoTrovato getFlagFondoTrovato() {
        return flagFondoTrovato;
    }

    public Idsv0501 getIdsv0501() {
        return idsv0501;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndiciLvvs0560 getIxIndici() {
        return ixIndici;
    }

    public Ldbv4911 getLdbv4911() {
        return ldbv4911;
    }

    public MoviLdbs1530 getMovi() {
        return movi;
    }

    public MoviFinrioLdbs5950 getMoviFinrio() {
        return moviFinrio;
    }

    public RichDisFnd getRichDisFnd() {
        return richDisFnd;
    }

    public RichInvstFndIdbsrif0 getRichInvstFnd() {
        return richInvstFnd;
    }

    public ValAstLdbs4910 getValAst() {
        return valAst;
    }

    public WkAppoDate getWkAppoDate() {
        return wkAppoDate;
    }

    public WkIdCodLivFlag getWkIdCodLivFlag() {
        return wkIdCodLivFlag;
    }

    public WkTpValAst getWkTpValAst() {
        return wkTpValAst;
    }

    public WsMovimentoLvvs0101 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TEMP_COD_FND = 20;
        public static final int DTGA_ELE_TGA_MAX = 2;
        public static final int DTGA_AREA_TGA = DTGA_ELE_TGA_MAX + Lvvs0560Data.DTGA_TAB_TRAN_MAXOCCURS * WtgaTabTran.Len.TAB_TRAN;
        public static final int DGRZ_ELE_GAR_MAX = 2;
        public static final int DGRZ_AREA_GRA = DGRZ_ELE_GAR_MAX + Lvvs0560Data.DGRZ_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int DL19_ELE_FND_MAX = 2;
        public static final int DL19_TABELLA = Lvvs0560Data.DL19_TAB_FND_MAXOCCURS * Wl19TabFnd.Len.WL19_TAB_FND;
        public static final int DL19_AREA_QUOTA = DL19_ELE_FND_MAX + DL19_TABELLA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
