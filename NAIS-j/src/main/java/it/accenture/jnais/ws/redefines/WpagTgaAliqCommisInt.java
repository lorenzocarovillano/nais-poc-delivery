package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-TGA-ALIQ-COMMIS-INT<br>
 * Variable: WPAG-TGA-ALIQ-COMMIS-INT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaAliqCommisInt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTgaAliqCommisInt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TGA_ALIQ_COMMIS_INT;
    }

    public void setWpagTgaAliqCommisIntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TGA_ALIQ_COMMIS_INT, Pos.WPAG_TGA_ALIQ_COMMIS_INT);
    }

    public byte[] getWpagTgaAliqCommisIntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TGA_ALIQ_COMMIS_INT, Pos.WPAG_TGA_ALIQ_COMMIS_INT);
        return buffer;
    }

    public void initWpagTgaAliqCommisIntSpaces() {
        fill(Pos.WPAG_TGA_ALIQ_COMMIS_INT, Len.WPAG_TGA_ALIQ_COMMIS_INT, Types.SPACE_CHAR);
    }

    public void setWpagTgaAliqCommisIntNull(String wpagTgaAliqCommisIntNull) {
        writeString(Pos.WPAG_TGA_ALIQ_COMMIS_INT_NULL, wpagTgaAliqCommisIntNull, Len.WPAG_TGA_ALIQ_COMMIS_INT_NULL);
    }

    /**Original name: WPAG-TGA-ALIQ-COMMIS-INT-NULL<br>*/
    public String getWpagTgaAliqCommisIntNull() {
        return readString(Pos.WPAG_TGA_ALIQ_COMMIS_INT_NULL, Len.WPAG_TGA_ALIQ_COMMIS_INT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_ALIQ_COMMIS_INT = 1;
        public static final int WPAG_TGA_ALIQ_COMMIS_INT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_ALIQ_COMMIS_INT = 8;
        public static final int WPAG_TGA_ALIQ_COMMIS_INT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
