package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.TabGiorni;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0017<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0017Data {

    //==== PROPERTIES ====
    /**Original name: TAB-GIORNI<br>
	 * <pre>¹   MODIFICA 09/02/89</pre>*/
    private TabGiorni tabGiorni = new TabGiorni();
    //Original name: AMGINF
    private Amginf amginf = new Amginf();
    //Original name: AMGSUP
    private Amgsup amgsup = new Amgsup();
    //Original name: AAAAMMGG
    private Aaaammgg aaaammgg = new Aaaammgg();
    //Original name: DREST
    private String drest = DefaultValues.stringVal(Len.DREST);
    //Original name: DQUOZ
    private short dquoz = DefaultValues.SHORT_VAL;

    //==== METHODS ====
    public void setDrest(short drest) {
        this.drest = NumericDisplay.asString(drest, Len.DREST);
    }

    public short getDrest() {
        return NumericDisplay.asShort(this.drest);
    }

    public String getDrestFormatted() {
        return this.drest;
    }

    public void setDquoz(short dquoz) {
        this.dquoz = dquoz;
    }

    public short getDquoz() {
        return this.dquoz;
    }

    public Aaaammgg getAaaammgg() {
        return aaaammgg;
    }

    public Amginf getAmginf() {
        return amginf;
    }

    public Amgsup getAmgsup() {
        return amgsup;
    }

    public TabGiorni getTabGiorni() {
        return tabGiorni;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RISUL = 3;
        public static final int RESTO = 1;
        public static final int DREST = 1;
        public static final int DQUOZ = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
