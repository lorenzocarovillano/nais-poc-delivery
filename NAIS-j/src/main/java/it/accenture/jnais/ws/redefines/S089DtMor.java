package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: S089-DT-MOR<br>
 * Variable: S089-DT-MOR from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089DtMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089DtMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_DT_MOR;
    }

    public void setWlquDtMor(int wlquDtMor) {
        writeIntAsPacked(Pos.S089_DT_MOR, wlquDtMor, Len.Int.WLQU_DT_MOR);
    }

    public void setWlquDtMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_DT_MOR, Pos.S089_DT_MOR);
    }

    /**Original name: WLQU-DT-MOR<br>*/
    public int getWlquDtMor() {
        return readPackedAsInt(Pos.S089_DT_MOR, Len.Int.WLQU_DT_MOR);
    }

    public byte[] getWlquDtMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_DT_MOR, Pos.S089_DT_MOR);
        return buffer;
    }

    public void initWlquDtMorSpaces() {
        fill(Pos.S089_DT_MOR, Len.S089_DT_MOR, Types.SPACE_CHAR);
    }

    public void setWlquDtMorNull(String wlquDtMorNull) {
        writeString(Pos.S089_DT_MOR_NULL, wlquDtMorNull, Len.WLQU_DT_MOR_NULL);
    }

    /**Original name: WLQU-DT-MOR-NULL<br>*/
    public String getWlquDtMorNull() {
        return readString(Pos.S089_DT_MOR_NULL, Len.WLQU_DT_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_DT_MOR = 1;
        public static final int S089_DT_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_DT_MOR = 5;
        public static final int WLQU_DT_MOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_DT_MOR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
