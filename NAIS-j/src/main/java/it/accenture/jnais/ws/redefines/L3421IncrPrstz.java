package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-INCR-PRSTZ<br>
 * Variable: L3421-INCR-PRSTZ from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421IncrPrstz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421IncrPrstz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_INCR_PRSTZ;
    }

    public void setL3421IncrPrstzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_INCR_PRSTZ, Pos.L3421_INCR_PRSTZ);
    }

    /**Original name: L3421-INCR-PRSTZ<br>*/
    public AfDecimal getL3421IncrPrstz() {
        return readPackedAsDecimal(Pos.L3421_INCR_PRSTZ, Len.Int.L3421_INCR_PRSTZ, Len.Fract.L3421_INCR_PRSTZ);
    }

    public byte[] getL3421IncrPrstzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_INCR_PRSTZ, Pos.L3421_INCR_PRSTZ);
        return buffer;
    }

    /**Original name: L3421-INCR-PRSTZ-NULL<br>*/
    public String getL3421IncrPrstzNull() {
        return readString(Pos.L3421_INCR_PRSTZ_NULL, Len.L3421_INCR_PRSTZ_NULL);
    }

    public String getL3421IncrPrstzNullFormatted() {
        return Functions.padBlanks(getL3421IncrPrstzNull(), Len.L3421_INCR_PRSTZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_INCR_PRSTZ = 1;
        public static final int L3421_INCR_PRSTZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_INCR_PRSTZ = 8;
        public static final int L3421_INCR_PRSTZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_INCR_PRSTZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_INCR_PRSTZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
