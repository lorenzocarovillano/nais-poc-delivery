package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-PC-PROV-INC-DFLT<br>
 * Variable: DAD-PC-PROV-INC-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadPcProvIncDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadPcProvIncDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_PC_PROV_INC_DFLT;
    }

    public void setDadPcProvIncDflt(AfDecimal dadPcProvIncDflt) {
        writeDecimalAsPacked(Pos.DAD_PC_PROV_INC_DFLT, dadPcProvIncDflt.copy());
    }

    public void setDadPcProvIncDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_PC_PROV_INC_DFLT, Pos.DAD_PC_PROV_INC_DFLT);
    }

    /**Original name: DAD-PC-PROV-INC-DFLT<br>*/
    public AfDecimal getDadPcProvIncDflt() {
        return readPackedAsDecimal(Pos.DAD_PC_PROV_INC_DFLT, Len.Int.DAD_PC_PROV_INC_DFLT, Len.Fract.DAD_PC_PROV_INC_DFLT);
    }

    public byte[] getDadPcProvIncDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_PC_PROV_INC_DFLT, Pos.DAD_PC_PROV_INC_DFLT);
        return buffer;
    }

    public void setDadPcProvIncDfltNull(String dadPcProvIncDfltNull) {
        writeString(Pos.DAD_PC_PROV_INC_DFLT_NULL, dadPcProvIncDfltNull, Len.DAD_PC_PROV_INC_DFLT_NULL);
    }

    /**Original name: DAD-PC-PROV-INC-DFLT-NULL<br>*/
    public String getDadPcProvIncDfltNull() {
        return readString(Pos.DAD_PC_PROV_INC_DFLT_NULL, Len.DAD_PC_PROV_INC_DFLT_NULL);
    }

    public String getDadPcProvIncDfltNullFormatted() {
        return Functions.padBlanks(getDadPcProvIncDfltNull(), Len.DAD_PC_PROV_INC_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_PC_PROV_INC_DFLT = 1;
        public static final int DAD_PC_PROV_INC_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_PC_PROV_INC_DFLT = 4;
        public static final int DAD_PC_PROV_INC_DFLT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_PC_PROV_INC_DFLT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DAD_PC_PROV_INC_DFLT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
