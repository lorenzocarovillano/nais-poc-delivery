package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-OPEN-FILESQS2<br>
 * Variable: FLAG-OPEN-FILESQS2 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagOpenFilesqs2 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagOpenFilesqs2(char flagOpenFilesqs2) {
        this.value = flagOpenFilesqs2;
    }

    public char getFlagOpenFilesqs2() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setOpenFilesqs2Si() {
        value = SI;
    }

    public void setOpenFilesqs2No() {
        value = NO;
    }
}
