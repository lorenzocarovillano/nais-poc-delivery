package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-IMP-ASS-SOCIALE<br>
 * Variable: WPCO-IMP-ASS-SOCIALE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoImpAssSociale extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoImpAssSociale() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_IMP_ASS_SOCIALE;
    }

    public void setWpcoImpAssSociale(AfDecimal wpcoImpAssSociale) {
        writeDecimalAsPacked(Pos.WPCO_IMP_ASS_SOCIALE, wpcoImpAssSociale.copy());
    }

    public void setDpcoImpAssSocialeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_IMP_ASS_SOCIALE, Pos.WPCO_IMP_ASS_SOCIALE);
    }

    /**Original name: WPCO-IMP-ASS-SOCIALE<br>*/
    public AfDecimal getWpcoImpAssSociale() {
        return readPackedAsDecimal(Pos.WPCO_IMP_ASS_SOCIALE, Len.Int.WPCO_IMP_ASS_SOCIALE, Len.Fract.WPCO_IMP_ASS_SOCIALE);
    }

    public byte[] getWpcoImpAssSocialeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_IMP_ASS_SOCIALE, Pos.WPCO_IMP_ASS_SOCIALE);
        return buffer;
    }

    public void setWpcoImpAssSocialeNull(String wpcoImpAssSocialeNull) {
        writeString(Pos.WPCO_IMP_ASS_SOCIALE_NULL, wpcoImpAssSocialeNull, Len.WPCO_IMP_ASS_SOCIALE_NULL);
    }

    /**Original name: WPCO-IMP-ASS-SOCIALE-NULL<br>*/
    public String getWpcoImpAssSocialeNull() {
        return readString(Pos.WPCO_IMP_ASS_SOCIALE_NULL, Len.WPCO_IMP_ASS_SOCIALE_NULL);
    }

    public String getWpcoImpAssSocialeNullFormatted() {
        return Functions.padBlanks(getWpcoImpAssSocialeNull(), Len.WPCO_IMP_ASS_SOCIALE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_IMP_ASS_SOCIALE = 1;
        public static final int WPCO_IMP_ASS_SOCIALE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_IMP_ASS_SOCIALE = 8;
        public static final int WPCO_IMP_ASS_SOCIALE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_IMP_ASS_SOCIALE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_IMP_ASS_SOCIALE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
