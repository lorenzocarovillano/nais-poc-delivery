package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMP-NET-LIQTO<br>
 * Variable: WBEL-IMP-NET-LIQTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpNetLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpNetLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMP_NET_LIQTO;
    }

    public void setWbelImpNetLiqto(AfDecimal wbelImpNetLiqto) {
        writeDecimalAsPacked(Pos.WBEL_IMP_NET_LIQTO, wbelImpNetLiqto.copy());
    }

    public void setWbelImpNetLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMP_NET_LIQTO, Pos.WBEL_IMP_NET_LIQTO);
    }

    /**Original name: WBEL-IMP-NET-LIQTO<br>*/
    public AfDecimal getWbelImpNetLiqto() {
        return readPackedAsDecimal(Pos.WBEL_IMP_NET_LIQTO, Len.Int.WBEL_IMP_NET_LIQTO, Len.Fract.WBEL_IMP_NET_LIQTO);
    }

    public byte[] getWbelImpNetLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMP_NET_LIQTO, Pos.WBEL_IMP_NET_LIQTO);
        return buffer;
    }

    public void initWbelImpNetLiqtoSpaces() {
        fill(Pos.WBEL_IMP_NET_LIQTO, Len.WBEL_IMP_NET_LIQTO, Types.SPACE_CHAR);
    }

    public void setWbelImpNetLiqtoNull(String wbelImpNetLiqtoNull) {
        writeString(Pos.WBEL_IMP_NET_LIQTO_NULL, wbelImpNetLiqtoNull, Len.WBEL_IMP_NET_LIQTO_NULL);
    }

    /**Original name: WBEL-IMP-NET-LIQTO-NULL<br>*/
    public String getWbelImpNetLiqtoNull() {
        return readString(Pos.WBEL_IMP_NET_LIQTO_NULL, Len.WBEL_IMP_NET_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMP_NET_LIQTO = 1;
        public static final int WBEL_IMP_NET_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMP_NET_LIQTO = 8;
        public static final int WBEL_IMP_NET_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMP_NET_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMP_NET_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
