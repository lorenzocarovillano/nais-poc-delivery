package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-PRE-1A-ANNUALITA<br>
 * Variable: OCO-PRE-1A-ANNUALITA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoPre1aAnnualita extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoPre1aAnnualita() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_PRE1A_ANNUALITA;
    }

    public void setOcoPre1aAnnualita(AfDecimal ocoPre1aAnnualita) {
        writeDecimalAsPacked(Pos.OCO_PRE1A_ANNUALITA, ocoPre1aAnnualita.copy());
    }

    public void setOcoPre1aAnnualitaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_PRE1A_ANNUALITA, Pos.OCO_PRE1A_ANNUALITA);
    }

    /**Original name: OCO-PRE-1A-ANNUALITA<br>*/
    public AfDecimal getOcoPre1aAnnualita() {
        return readPackedAsDecimal(Pos.OCO_PRE1A_ANNUALITA, Len.Int.OCO_PRE1A_ANNUALITA, Len.Fract.OCO_PRE1A_ANNUALITA);
    }

    public byte[] getOcoPre1aAnnualitaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_PRE1A_ANNUALITA, Pos.OCO_PRE1A_ANNUALITA);
        return buffer;
    }

    public void setOcoPre1aAnnualitaNull(String ocoPre1aAnnualitaNull) {
        writeString(Pos.OCO_PRE1A_ANNUALITA_NULL, ocoPre1aAnnualitaNull, Len.OCO_PRE1A_ANNUALITA_NULL);
    }

    /**Original name: OCO-PRE-1A-ANNUALITA-NULL<br>*/
    public String getOcoPre1aAnnualitaNull() {
        return readString(Pos.OCO_PRE1A_ANNUALITA_NULL, Len.OCO_PRE1A_ANNUALITA_NULL);
    }

    public String getOcoPre1aAnnualitaNullFormatted() {
        return Functions.padBlanks(getOcoPre1aAnnualitaNull(), Len.OCO_PRE1A_ANNUALITA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_PRE1A_ANNUALITA = 1;
        public static final int OCO_PRE1A_ANNUALITA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_PRE1A_ANNUALITA = 8;
        public static final int OCO_PRE1A_ANNUALITA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_PRE1A_ANNUALITA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_PRE1A_ANNUALITA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
