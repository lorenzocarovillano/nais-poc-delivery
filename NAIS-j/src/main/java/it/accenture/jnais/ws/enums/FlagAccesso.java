package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-ACCESSO<br>
 * Variable: FLAG-ACCESSO from program IABS0060<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagAccesso {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char STD = '1';
    public static final char MCRFNCT = '2';
    public static final char TPMV = '3';
    public static final char MCRFNCT_TPMV = '4';
    public static final char IB_OGG = '5';

    //==== METHODS ====
    public void setFlagAccesso(char flagAccesso) {
        this.value = flagAccesso;
    }

    public void setFlagAccessoFormatted(String flagAccesso) {
        setFlagAccesso(Functions.charAt(flagAccesso, Types.CHAR_SIZE));
    }

    public char getFlagAccesso() {
        return this.value;
    }

    public void setStd() {
        value = STD;
    }

    public boolean isMcrfnct() {
        return value == MCRFNCT;
    }

    public void setMcrfnct() {
        value = MCRFNCT;
    }

    public void setTpmv() {
        value = TPMV;
    }

    public void setMcrfnctTpmv() {
        value = MCRFNCT_TPMV;
    }

    public boolean isIbOgg() {
        return value == IB_OGG;
    }
}
