package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-MOV-TROV<br>
 * Variable: FLAG-MOV-TROV from program LVVS0089<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagMovTrov {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagMovTrov(char flagMovTrov) {
        this.value = flagMovTrov;
    }

    public char getFlagMovTrov() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
