package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-VIS-662014<br>
 * Variable: LQU-IMPST-VIS-662014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpstVis662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpstVis662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST_VIS662014;
    }

    public void setLquImpstVis662014(AfDecimal lquImpstVis662014) {
        writeDecimalAsPacked(Pos.LQU_IMPST_VIS662014, lquImpstVis662014.copy());
    }

    public void setLquImpstVis662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST_VIS662014, Pos.LQU_IMPST_VIS662014);
    }

    /**Original name: LQU-IMPST-VIS-662014<br>*/
    public AfDecimal getLquImpstVis662014() {
        return readPackedAsDecimal(Pos.LQU_IMPST_VIS662014, Len.Int.LQU_IMPST_VIS662014, Len.Fract.LQU_IMPST_VIS662014);
    }

    public byte[] getLquImpstVis662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST_VIS662014, Pos.LQU_IMPST_VIS662014);
        return buffer;
    }

    public void setLquImpstVis662014Null(String lquImpstVis662014Null) {
        writeString(Pos.LQU_IMPST_VIS662014_NULL, lquImpstVis662014Null, Len.LQU_IMPST_VIS662014_NULL);
    }

    /**Original name: LQU-IMPST-VIS-662014-NULL<br>*/
    public String getLquImpstVis662014Null() {
        return readString(Pos.LQU_IMPST_VIS662014_NULL, Len.LQU_IMPST_VIS662014_NULL);
    }

    public String getLquImpstVis662014NullFormatted() {
        return Functions.padBlanks(getLquImpstVis662014Null(), Len.LQU_IMPST_VIS662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_VIS662014 = 1;
        public static final int LQU_IMPST_VIS662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_VIS662014 = 8;
        public static final int LQU_IMPST_VIS662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_VIS662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_VIS662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
