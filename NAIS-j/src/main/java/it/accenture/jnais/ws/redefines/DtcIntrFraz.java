package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-INTR-FRAZ<br>
 * Variable: DTC-INTR-FRAZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcIntrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcIntrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_INTR_FRAZ;
    }

    public void setDtcIntrFraz(AfDecimal dtcIntrFraz) {
        writeDecimalAsPacked(Pos.DTC_INTR_FRAZ, dtcIntrFraz.copy());
    }

    public void setDtcIntrFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_INTR_FRAZ, Pos.DTC_INTR_FRAZ);
    }

    /**Original name: DTC-INTR-FRAZ<br>*/
    public AfDecimal getDtcIntrFraz() {
        return readPackedAsDecimal(Pos.DTC_INTR_FRAZ, Len.Int.DTC_INTR_FRAZ, Len.Fract.DTC_INTR_FRAZ);
    }

    public byte[] getDtcIntrFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_INTR_FRAZ, Pos.DTC_INTR_FRAZ);
        return buffer;
    }

    public void setDtcIntrFrazNull(String dtcIntrFrazNull) {
        writeString(Pos.DTC_INTR_FRAZ_NULL, dtcIntrFrazNull, Len.DTC_INTR_FRAZ_NULL);
    }

    /**Original name: DTC-INTR-FRAZ-NULL<br>*/
    public String getDtcIntrFrazNull() {
        return readString(Pos.DTC_INTR_FRAZ_NULL, Len.DTC_INTR_FRAZ_NULL);
    }

    public String getDtcIntrFrazNullFormatted() {
        return Functions.padBlanks(getDtcIntrFrazNull(), Len.DTC_INTR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_INTR_FRAZ = 1;
        public static final int DTC_INTR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_INTR_FRAZ = 8;
        public static final int DTC_INTR_FRAZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_INTR_FRAZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
