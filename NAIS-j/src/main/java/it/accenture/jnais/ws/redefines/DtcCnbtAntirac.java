package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-CNBT-ANTIRAC<br>
 * Variable: DTC-CNBT-ANTIRAC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcCnbtAntirac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcCnbtAntirac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_CNBT_ANTIRAC;
    }

    public void setDtcCnbtAntirac(AfDecimal dtcCnbtAntirac) {
        writeDecimalAsPacked(Pos.DTC_CNBT_ANTIRAC, dtcCnbtAntirac.copy());
    }

    public void setDtcCnbtAntiracFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_CNBT_ANTIRAC, Pos.DTC_CNBT_ANTIRAC);
    }

    /**Original name: DTC-CNBT-ANTIRAC<br>*/
    public AfDecimal getDtcCnbtAntirac() {
        return readPackedAsDecimal(Pos.DTC_CNBT_ANTIRAC, Len.Int.DTC_CNBT_ANTIRAC, Len.Fract.DTC_CNBT_ANTIRAC);
    }

    public byte[] getDtcCnbtAntiracAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_CNBT_ANTIRAC, Pos.DTC_CNBT_ANTIRAC);
        return buffer;
    }

    public void setDtcCnbtAntiracNull(String dtcCnbtAntiracNull) {
        writeString(Pos.DTC_CNBT_ANTIRAC_NULL, dtcCnbtAntiracNull, Len.DTC_CNBT_ANTIRAC_NULL);
    }

    /**Original name: DTC-CNBT-ANTIRAC-NULL<br>*/
    public String getDtcCnbtAntiracNull() {
        return readString(Pos.DTC_CNBT_ANTIRAC_NULL, Len.DTC_CNBT_ANTIRAC_NULL);
    }

    public String getDtcCnbtAntiracNullFormatted() {
        return Functions.padBlanks(getDtcCnbtAntiracNull(), Len.DTC_CNBT_ANTIRAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_CNBT_ANTIRAC = 1;
        public static final int DTC_CNBT_ANTIRAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_CNBT_ANTIRAC = 8;
        public static final int DTC_CNBT_ANTIRAC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_CNBT_ANTIRAC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_CNBT_ANTIRAC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
