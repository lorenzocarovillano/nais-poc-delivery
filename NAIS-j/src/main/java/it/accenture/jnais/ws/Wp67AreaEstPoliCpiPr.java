package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvp671;
import it.accenture.jnais.copy.Wp67Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WP67-AREA-EST-POLI-CPI-PR<br>
 * Variable: WP67-AREA-EST-POLI-CPI-PR from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Wp67AreaEstPoliCpiPr extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WP67-EST-POLI-CPI-PR-MAX
    private short wp67EstPoliCpiPrMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVP671
    private Lccvp671 lccvp671 = new Lccvp671();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_AREA_EST_POLI_CPI_PR;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWp67AreaEstPoliCpiPrBytes(buf);
    }

    public String getWp67AreaEstPoliCpiPrFormatted() {
        return MarshalByteExt.bufferToStr(getWp67AreaEstPoliCpiPrBytes());
    }

    public void setWp67AreaEstPoliCpiPrBytes(byte[] buffer) {
        setWp67AreaEstPoliCpiPrBytes(buffer, 1);
    }

    public byte[] getWp67AreaEstPoliCpiPrBytes() {
        byte[] buffer = new byte[Len.WP67_AREA_EST_POLI_CPI_PR];
        return getWp67AreaEstPoliCpiPrBytes(buffer, 1);
    }

    public void setWp67AreaEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        wp67EstPoliCpiPrMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWp67TabEstPoliCpiPrBytes(buffer, position);
    }

    public byte[] getWp67AreaEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wp67EstPoliCpiPrMax);
        position += Types.SHORT_SIZE;
        getWp67TabEstPoliCpiPrBytes(buffer, position);
        return buffer;
    }

    public void setWp67EstPoliCpiPrMax(short wp67EstPoliCpiPrMax) {
        this.wp67EstPoliCpiPrMax = wp67EstPoliCpiPrMax;
    }

    public short getWp67EstPoliCpiPrMax() {
        return this.wp67EstPoliCpiPrMax;
    }

    public void setWp67TabEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp671.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp671.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp671.Len.Int.ID_PTF, 0));
        position += Lccvp671.Len.ID_PTF;
        lccvp671.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp67TabEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp671.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp671.getIdPtf(), Lccvp671.Len.Int.ID_PTF, 0);
        position += Lccvp671.Len.ID_PTF;
        lccvp671.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    @Override
    public byte[] serialize() {
        return getWp67AreaEstPoliCpiPrBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_EST_POLI_CPI_PR_MAX = 2;
        public static final int WP67_TAB_EST_POLI_CPI_PR = WpolStatus.Len.STATUS + Lccvp671.Len.ID_PTF + Wp67Dati.Len.DATI;
        public static final int WP67_AREA_EST_POLI_CPI_PR = WP67_EST_POLI_CPI_PR_MAX + WP67_TAB_EST_POLI_CPI_PR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
