package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-TGA-IMP-INCAS<br>
 * Variable: WPAG-TGA-IMP-INCAS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaImpIncas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTgaImpIncas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TGA_IMP_INCAS;
    }

    public void setWpagTgaImpIncas(AfDecimal wpagTgaImpIncas) {
        writeDecimalAsPacked(Pos.WPAG_TGA_IMP_INCAS, wpagTgaImpIncas.copy());
    }

    public void setWpagTgaImpIncasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TGA_IMP_INCAS, Pos.WPAG_TGA_IMP_INCAS);
    }

    /**Original name: WPAG-TGA-IMP-INCAS<br>*/
    public AfDecimal getWpagTgaImpIncas() {
        return readPackedAsDecimal(Pos.WPAG_TGA_IMP_INCAS, Len.Int.WPAG_TGA_IMP_INCAS, Len.Fract.WPAG_TGA_IMP_INCAS);
    }

    public byte[] getWpagTgaImpIncasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TGA_IMP_INCAS, Pos.WPAG_TGA_IMP_INCAS);
        return buffer;
    }

    public void initWpagTgaImpIncasSpaces() {
        fill(Pos.WPAG_TGA_IMP_INCAS, Len.WPAG_TGA_IMP_INCAS, Types.SPACE_CHAR);
    }

    public void setWpagTgaImpIncasNull(String wpagTgaImpIncasNull) {
        writeString(Pos.WPAG_TGA_IMP_INCAS_NULL, wpagTgaImpIncasNull, Len.WPAG_TGA_IMP_INCAS_NULL);
    }

    /**Original name: WPAG-TGA-IMP-INCAS-NULL<br>*/
    public String getWpagTgaImpIncasNull() {
        return readString(Pos.WPAG_TGA_IMP_INCAS_NULL, Len.WPAG_TGA_IMP_INCAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMP_INCAS = 1;
        public static final int WPAG_TGA_IMP_INCAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMP_INCAS = 8;
        public static final int WPAG_TGA_IMP_INCAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_IMP_INCAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_IMP_INCAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
