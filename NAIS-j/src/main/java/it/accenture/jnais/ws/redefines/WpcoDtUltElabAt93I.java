package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-AT93-I<br>
 * Variable: WPCO-DT-ULT-ELAB-AT93-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabAt93I extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabAt93I() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_AT93_I;
    }

    public void setWpcoDtUltElabAt93I(int wpcoDtUltElabAt93I) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_AT93_I, wpcoDtUltElabAt93I, Len.Int.WPCO_DT_ULT_ELAB_AT93_I);
    }

    public void setDpcoDtUltElabAt93IFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_AT93_I, Pos.WPCO_DT_ULT_ELAB_AT93_I);
    }

    /**Original name: WPCO-DT-ULT-ELAB-AT93-I<br>*/
    public int getWpcoDtUltElabAt93I() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_AT93_I, Len.Int.WPCO_DT_ULT_ELAB_AT93_I);
    }

    public byte[] getWpcoDtUltElabAt93IAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_AT93_I, Pos.WPCO_DT_ULT_ELAB_AT93_I);
        return buffer;
    }

    public void setWpcoDtUltElabAt93INull(String wpcoDtUltElabAt93INull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_AT93_I_NULL, wpcoDtUltElabAt93INull, Len.WPCO_DT_ULT_ELAB_AT93_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-AT93-I-NULL<br>*/
    public String getWpcoDtUltElabAt93INull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_AT93_I_NULL, Len.WPCO_DT_ULT_ELAB_AT93_I_NULL);
    }

    public String getWpcoDtUltElabAt93INullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabAt93INull(), Len.WPCO_DT_ULT_ELAB_AT93_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_AT93_I = 1;
        public static final int WPCO_DT_ULT_ELAB_AT93_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_AT93_I = 5;
        public static final int WPCO_DT_ULT_ELAB_AT93_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_AT93_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
