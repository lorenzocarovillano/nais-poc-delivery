package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: BPA-DT-END<br>
 * Variable: BPA-DT-END from program IABS0130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BpaDtEnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BpaDtEnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BPA_DT_END;
    }

    public void setBpaDtEnd(long bpaDtEnd) {
        writeLongAsPacked(Pos.BPA_DT_END, bpaDtEnd, Len.Int.BPA_DT_END);
    }

    public void setBpaDtEndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BPA_DT_END, Pos.BPA_DT_END);
    }

    /**Original name: BPA-DT-END<br>*/
    public long getBpaDtEnd() {
        return readPackedAsLong(Pos.BPA_DT_END, Len.Int.BPA_DT_END);
    }

    public byte[] getBpaDtEndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BPA_DT_END, Pos.BPA_DT_END);
        return buffer;
    }

    public void initBpaDtEndHighValues() {
        fill(Pos.BPA_DT_END, Len.BPA_DT_END, Types.HIGH_CHAR_VAL);
    }

    public void setBpaDtEndNull(String bpaDtEndNull) {
        writeString(Pos.BPA_DT_END_NULL, bpaDtEndNull, Len.BPA_DT_END_NULL);
    }

    /**Original name: BPA-DT-END-NULL<br>*/
    public String getBpaDtEndNull() {
        return readString(Pos.BPA_DT_END_NULL, Len.BPA_DT_END_NULL);
    }

    public String getBpaDtEndNullFormatted() {
        return Functions.padBlanks(getBpaDtEndNull(), Len.BPA_DT_END_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BPA_DT_END = 1;
        public static final int BPA_DT_END_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BPA_DT_END = 10;
        public static final int BPA_DT_END_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BPA_DT_END = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
