package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-PRE-NET<br>
 * Variable: WDTR-PRE-NET from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrPreNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrPreNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_PRE_NET;
    }

    public void setWdtrPreNet(AfDecimal wdtrPreNet) {
        writeDecimalAsPacked(Pos.WDTR_PRE_NET, wdtrPreNet.copy());
    }

    /**Original name: WDTR-PRE-NET<br>*/
    public AfDecimal getWdtrPreNet() {
        return readPackedAsDecimal(Pos.WDTR_PRE_NET, Len.Int.WDTR_PRE_NET, Len.Fract.WDTR_PRE_NET);
    }

    public void setWdtrPreNetNull(String wdtrPreNetNull) {
        writeString(Pos.WDTR_PRE_NET_NULL, wdtrPreNetNull, Len.WDTR_PRE_NET_NULL);
    }

    /**Original name: WDTR-PRE-NET-NULL<br>*/
    public String getWdtrPreNetNull() {
        return readString(Pos.WDTR_PRE_NET_NULL, Len.WDTR_PRE_NET_NULL);
    }

    public String getWdtrPreNetNullFormatted() {
        return Functions.padBlanks(getWdtrPreNetNull(), Len.WDTR_PRE_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_PRE_NET = 1;
        public static final int WDTR_PRE_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_PRE_NET = 8;
        public static final int WDTR_PRE_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_PRE_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_PRE_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
