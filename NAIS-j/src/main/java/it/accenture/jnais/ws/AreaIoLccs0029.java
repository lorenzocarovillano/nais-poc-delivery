package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: AREA-IO-LCCS0029<br>
 * Variable: AREA-IO-LCCS0029 from program LCCS0029<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoLccs0029 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: S029-DT-CALC
    private String dtCalc = DefaultValues.stringVal(Len.DT_CALC);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IO_LCCS0029;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIoLccs0029Bytes(buf);
    }

    public String getAreaIoLccs0029Formatted() {
        return getDatiInputFormatted();
    }

    public void setAreaIoLccs0029Bytes(byte[] buffer) {
        setAreaIoLccs0029Bytes(buffer, 1);
    }

    public byte[] getAreaIoLccs0029Bytes() {
        byte[] buffer = new byte[Len.AREA_IO_LCCS0029];
        return getAreaIoLccs0029Bytes(buffer, 1);
    }

    public void setAreaIoLccs0029Bytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiInputBytes(buffer, position);
    }

    public byte[] getAreaIoLccs0029Bytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiInputBytes(buffer, position);
        return buffer;
    }

    public String getDatiInputFormatted() {
        return getDtCalcFormatted();
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        dtCalc = MarshalByte.readFixedString(buffer, position, Len.DT_CALC);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, dtCalc, Len.DT_CALC);
        return buffer;
    }

    public void setDtCalcFromBuffer(byte[] buffer) {
        dtCalc = MarshalByte.readFixedString(buffer, 1, Len.DT_CALC);
    }

    public String getDtCalcFormatted() {
        return this.dtCalc;
    }

    @Override
    public byte[] serialize() {
        return getAreaIoLccs0029Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_CALC = 8;
        public static final int DATI_INPUT = DT_CALC;
        public static final int AREA_IO_LCCS0029 = DATI_INPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
