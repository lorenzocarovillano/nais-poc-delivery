package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-DT-END-CARZ<br>
 * Variable: GRZ-DT-END-CARZ from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzDtEndCarz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzDtEndCarz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_DT_END_CARZ;
    }

    public void setGrzDtEndCarz(int grzDtEndCarz) {
        writeIntAsPacked(Pos.GRZ_DT_END_CARZ, grzDtEndCarz, Len.Int.GRZ_DT_END_CARZ);
    }

    public void setGrzDtEndCarzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_DT_END_CARZ, Pos.GRZ_DT_END_CARZ);
    }

    /**Original name: GRZ-DT-END-CARZ<br>*/
    public int getGrzDtEndCarz() {
        return readPackedAsInt(Pos.GRZ_DT_END_CARZ, Len.Int.GRZ_DT_END_CARZ);
    }

    public byte[] getGrzDtEndCarzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_DT_END_CARZ, Pos.GRZ_DT_END_CARZ);
        return buffer;
    }

    public void setGrzDtEndCarzNull(String grzDtEndCarzNull) {
        writeString(Pos.GRZ_DT_END_CARZ_NULL, grzDtEndCarzNull, Len.GRZ_DT_END_CARZ_NULL);
    }

    /**Original name: GRZ-DT-END-CARZ-NULL<br>*/
    public String getGrzDtEndCarzNull() {
        return readString(Pos.GRZ_DT_END_CARZ_NULL, Len.GRZ_DT_END_CARZ_NULL);
    }

    public String getGrzDtEndCarzNullFormatted() {
        return Functions.padBlanks(getGrzDtEndCarzNull(), Len.GRZ_DT_END_CARZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_DT_END_CARZ = 1;
        public static final int GRZ_DT_END_CARZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_DT_END_CARZ = 5;
        public static final int GRZ_DT_END_CARZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_DT_END_CARZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
