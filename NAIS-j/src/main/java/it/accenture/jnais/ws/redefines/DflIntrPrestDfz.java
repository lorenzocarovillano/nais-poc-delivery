package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-INTR-PREST-DFZ<br>
 * Variable: DFL-INTR-PREST-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIntrPrestDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIntrPrestDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_INTR_PREST_DFZ;
    }

    public void setDflIntrPrestDfz(AfDecimal dflIntrPrestDfz) {
        writeDecimalAsPacked(Pos.DFL_INTR_PREST_DFZ, dflIntrPrestDfz.copy());
    }

    public void setDflIntrPrestDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_INTR_PREST_DFZ, Pos.DFL_INTR_PREST_DFZ);
    }

    /**Original name: DFL-INTR-PREST-DFZ<br>*/
    public AfDecimal getDflIntrPrestDfz() {
        return readPackedAsDecimal(Pos.DFL_INTR_PREST_DFZ, Len.Int.DFL_INTR_PREST_DFZ, Len.Fract.DFL_INTR_PREST_DFZ);
    }

    public byte[] getDflIntrPrestDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_INTR_PREST_DFZ, Pos.DFL_INTR_PREST_DFZ);
        return buffer;
    }

    public void setDflIntrPrestDfzNull(String dflIntrPrestDfzNull) {
        writeString(Pos.DFL_INTR_PREST_DFZ_NULL, dflIntrPrestDfzNull, Len.DFL_INTR_PREST_DFZ_NULL);
    }

    /**Original name: DFL-INTR-PREST-DFZ-NULL<br>*/
    public String getDflIntrPrestDfzNull() {
        return readString(Pos.DFL_INTR_PREST_DFZ_NULL, Len.DFL_INTR_PREST_DFZ_NULL);
    }

    public String getDflIntrPrestDfzNullFormatted() {
        return Functions.padBlanks(getDflIntrPrestDfzNull(), Len.DFL_INTR_PREST_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_INTR_PREST_DFZ = 1;
        public static final int DFL_INTR_PREST_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_INTR_PREST_DFZ = 8;
        public static final int DFL_INTR_PREST_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_INTR_PREST_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_INTR_PREST_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
