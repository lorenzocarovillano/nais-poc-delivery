package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: Q04-COD-CAN<br>
 * Variable: Q04-COD-CAN from program LDBS4990<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Q04CodCan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Q04CodCan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.Q04_COD_CAN;
    }

    public void setQ04CodCan(int q04CodCan) {
        writeIntAsPacked(Pos.Q04_COD_CAN, q04CodCan, Len.Int.Q04_COD_CAN);
    }

    public void setQ04CodCanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.Q04_COD_CAN, Pos.Q04_COD_CAN);
    }

    /**Original name: Q04-COD-CAN<br>*/
    public int getQ04CodCan() {
        return readPackedAsInt(Pos.Q04_COD_CAN, Len.Int.Q04_COD_CAN);
    }

    public byte[] getQ04CodCanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.Q04_COD_CAN, Pos.Q04_COD_CAN);
        return buffer;
    }

    public void setQ04CodCanNull(String q04CodCanNull) {
        writeString(Pos.Q04_COD_CAN_NULL, q04CodCanNull, Len.Q04_COD_CAN_NULL);
    }

    /**Original name: Q04-COD-CAN-NULL<br>*/
    public String getQ04CodCanNull() {
        return readString(Pos.Q04_COD_CAN_NULL, Len.Q04_COD_CAN_NULL);
    }

    public String getQ04CodCanNullFormatted() {
        return Functions.padBlanks(getQ04CodCanNull(), Len.Q04_COD_CAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int Q04_COD_CAN = 1;
        public static final int Q04_COD_CAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int Q04_COD_CAN = 3;
        public static final int Q04_COD_CAN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int Q04_COD_CAN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
