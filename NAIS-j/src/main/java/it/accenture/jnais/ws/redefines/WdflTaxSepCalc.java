package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-TAX-SEP-CALC<br>
 * Variable: WDFL-TAX-SEP-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflTaxSepCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflTaxSepCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_TAX_SEP_CALC;
    }

    public void setWdflTaxSepCalc(AfDecimal wdflTaxSepCalc) {
        writeDecimalAsPacked(Pos.WDFL_TAX_SEP_CALC, wdflTaxSepCalc.copy());
    }

    public void setWdflTaxSepCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_TAX_SEP_CALC, Pos.WDFL_TAX_SEP_CALC);
    }

    /**Original name: WDFL-TAX-SEP-CALC<br>*/
    public AfDecimal getWdflTaxSepCalc() {
        return readPackedAsDecimal(Pos.WDFL_TAX_SEP_CALC, Len.Int.WDFL_TAX_SEP_CALC, Len.Fract.WDFL_TAX_SEP_CALC);
    }

    public byte[] getWdflTaxSepCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_TAX_SEP_CALC, Pos.WDFL_TAX_SEP_CALC);
        return buffer;
    }

    public void setWdflTaxSepCalcNull(String wdflTaxSepCalcNull) {
        writeString(Pos.WDFL_TAX_SEP_CALC_NULL, wdflTaxSepCalcNull, Len.WDFL_TAX_SEP_CALC_NULL);
    }

    /**Original name: WDFL-TAX-SEP-CALC-NULL<br>*/
    public String getWdflTaxSepCalcNull() {
        return readString(Pos.WDFL_TAX_SEP_CALC_NULL, Len.WDFL_TAX_SEP_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_TAX_SEP_CALC = 1;
        public static final int WDFL_TAX_SEP_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_TAX_SEP_CALC = 8;
        public static final int WDFL_TAX_SEP_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_TAX_SEP_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_TAX_SEP_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
