package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPST-VIS<br>
 * Variable: WDFA-IMPST-VIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpstVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpstVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPST_VIS;
    }

    public void setWdfaImpstVis(AfDecimal wdfaImpstVis) {
        writeDecimalAsPacked(Pos.WDFA_IMPST_VIS, wdfaImpstVis.copy());
    }

    public void setWdfaImpstVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPST_VIS, Pos.WDFA_IMPST_VIS);
    }

    /**Original name: WDFA-IMPST-VIS<br>*/
    public AfDecimal getWdfaImpstVis() {
        return readPackedAsDecimal(Pos.WDFA_IMPST_VIS, Len.Int.WDFA_IMPST_VIS, Len.Fract.WDFA_IMPST_VIS);
    }

    public byte[] getWdfaImpstVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPST_VIS, Pos.WDFA_IMPST_VIS);
        return buffer;
    }

    public void setWdfaImpstVisNull(String wdfaImpstVisNull) {
        writeString(Pos.WDFA_IMPST_VIS_NULL, wdfaImpstVisNull, Len.WDFA_IMPST_VIS_NULL);
    }

    /**Original name: WDFA-IMPST-VIS-NULL<br>*/
    public String getWdfaImpstVisNull() {
        return readString(Pos.WDFA_IMPST_VIS_NULL, Len.WDFA_IMPST_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_VIS = 1;
        public static final int WDFA_IMPST_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_VIS = 8;
        public static final int WDFA_IMPST_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
