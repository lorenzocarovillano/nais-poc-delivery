package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.occurs.WisoTabImpSost;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS1870<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs1870Data {

    //==== PROPERTIES ====
    public static final int DISO_TAB_ISO_MAXOCCURS = 10;
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    //Original name: DISO-ELE-ISO-MAX
    private short disoEleIsoMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DISO-TAB-ISO
    private WisoTabImpSost[] disoTabIso = new WisoTabImpSost[DISO_TAB_ISO_MAXOCCURS];
    //Original name: IMPST-SOST
    private ImpstSostLdbs3990 impstSost = new ImpstSostLdbs3990();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ISO
    private short ixTabIso = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs1870Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int disoTabIsoIdx = 1; disoTabIsoIdx <= DISO_TAB_ISO_MAXOCCURS; disoTabIsoIdx++) {
            disoTabIso[disoTabIsoIdx - 1] = new WisoTabImpSost();
        }
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setDisoEleIsoMax(short disoEleIsoMax) {
        this.disoEleIsoMax = disoEleIsoMax;
    }

    public short getDisoEleIsoMax() {
        return this.disoEleIsoMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabIso(short ixTabIso) {
        this.ixTabIso = ixTabIso;
    }

    public short getIxTabIso() {
        return this.ixTabIso;
    }

    public WisoTabImpSost getDisoTabIso(int idx) {
        return disoTabIso[idx - 1];
    }

    public ImpstSostLdbs3990 getImpstSost() {
        return impstSost;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
