package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RIT-TFR-DFZ<br>
 * Variable: DFL-RIT-TFR-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflRitTfrDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflRitTfrDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RIT_TFR_DFZ;
    }

    public void setDflRitTfrDfz(AfDecimal dflRitTfrDfz) {
        writeDecimalAsPacked(Pos.DFL_RIT_TFR_DFZ, dflRitTfrDfz.copy());
    }

    public void setDflRitTfrDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RIT_TFR_DFZ, Pos.DFL_RIT_TFR_DFZ);
    }

    /**Original name: DFL-RIT-TFR-DFZ<br>*/
    public AfDecimal getDflRitTfrDfz() {
        return readPackedAsDecimal(Pos.DFL_RIT_TFR_DFZ, Len.Int.DFL_RIT_TFR_DFZ, Len.Fract.DFL_RIT_TFR_DFZ);
    }

    public byte[] getDflRitTfrDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RIT_TFR_DFZ, Pos.DFL_RIT_TFR_DFZ);
        return buffer;
    }

    public void setDflRitTfrDfzNull(String dflRitTfrDfzNull) {
        writeString(Pos.DFL_RIT_TFR_DFZ_NULL, dflRitTfrDfzNull, Len.DFL_RIT_TFR_DFZ_NULL);
    }

    /**Original name: DFL-RIT-TFR-DFZ-NULL<br>*/
    public String getDflRitTfrDfzNull() {
        return readString(Pos.DFL_RIT_TFR_DFZ_NULL, Len.DFL_RIT_TFR_DFZ_NULL);
    }

    public String getDflRitTfrDfzNullFormatted() {
        return Functions.padBlanks(getDflRitTfrDfzNull(), Len.DFL_RIT_TFR_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RIT_TFR_DFZ = 1;
        public static final int DFL_RIT_TFR_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RIT_TFR_DFZ = 8;
        public static final int DFL_RIT_TFR_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RIT_TFR_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RIT_TFR_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
