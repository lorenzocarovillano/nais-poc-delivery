package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-PP-ULT<br>
 * Variable: WTGA-PRE-PP-ULT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPrePpUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPrePpUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_PP_ULT;
    }

    public void setWtgaPrePpUlt(AfDecimal wtgaPrePpUlt) {
        writeDecimalAsPacked(Pos.WTGA_PRE_PP_ULT, wtgaPrePpUlt.copy());
    }

    public void setWtgaPrePpUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_PP_ULT, Pos.WTGA_PRE_PP_ULT);
    }

    /**Original name: WTGA-PRE-PP-ULT<br>*/
    public AfDecimal getWtgaPrePpUlt() {
        return readPackedAsDecimal(Pos.WTGA_PRE_PP_ULT, Len.Int.WTGA_PRE_PP_ULT, Len.Fract.WTGA_PRE_PP_ULT);
    }

    public byte[] getWtgaPrePpUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_PP_ULT, Pos.WTGA_PRE_PP_ULT);
        return buffer;
    }

    public void initWtgaPrePpUltSpaces() {
        fill(Pos.WTGA_PRE_PP_ULT, Len.WTGA_PRE_PP_ULT, Types.SPACE_CHAR);
    }

    public void setWtgaPrePpUltNull(String wtgaPrePpUltNull) {
        writeString(Pos.WTGA_PRE_PP_ULT_NULL, wtgaPrePpUltNull, Len.WTGA_PRE_PP_ULT_NULL);
    }

    /**Original name: WTGA-PRE-PP-ULT-NULL<br>*/
    public String getWtgaPrePpUltNull() {
        return readString(Pos.WTGA_PRE_PP_ULT_NULL, Len.WTGA_PRE_PP_ULT_NULL);
    }

    public String getWtgaPrePpUltNullFormatted() {
        return Functions.padBlanks(getWtgaPrePpUltNull(), Len.WTGA_PRE_PP_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_PP_ULT = 1;
        public static final int WTGA_PRE_PP_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_PP_ULT = 8;
        public static final int WTGA_PRE_PP_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_PP_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_PP_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
