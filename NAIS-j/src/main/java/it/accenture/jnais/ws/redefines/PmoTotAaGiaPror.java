package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-TOT-AA-GIA-PROR<br>
 * Variable: PMO-TOT-AA-GIA-PROR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoTotAaGiaPror extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoTotAaGiaPror() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_TOT_AA_GIA_PROR;
    }

    public void setPmoTotAaGiaPror(int pmoTotAaGiaPror) {
        writeIntAsPacked(Pos.PMO_TOT_AA_GIA_PROR, pmoTotAaGiaPror, Len.Int.PMO_TOT_AA_GIA_PROR);
    }

    public void setPmoTotAaGiaProrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_TOT_AA_GIA_PROR, Pos.PMO_TOT_AA_GIA_PROR);
    }

    /**Original name: PMO-TOT-AA-GIA-PROR<br>*/
    public int getPmoTotAaGiaPror() {
        return readPackedAsInt(Pos.PMO_TOT_AA_GIA_PROR, Len.Int.PMO_TOT_AA_GIA_PROR);
    }

    public byte[] getPmoTotAaGiaProrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_TOT_AA_GIA_PROR, Pos.PMO_TOT_AA_GIA_PROR);
        return buffer;
    }

    public void initPmoTotAaGiaProrHighValues() {
        fill(Pos.PMO_TOT_AA_GIA_PROR, Len.PMO_TOT_AA_GIA_PROR, Types.HIGH_CHAR_VAL);
    }

    public void setPmoTotAaGiaProrNull(String pmoTotAaGiaProrNull) {
        writeString(Pos.PMO_TOT_AA_GIA_PROR_NULL, pmoTotAaGiaProrNull, Len.PMO_TOT_AA_GIA_PROR_NULL);
    }

    /**Original name: PMO-TOT-AA-GIA-PROR-NULL<br>*/
    public String getPmoTotAaGiaProrNull() {
        return readString(Pos.PMO_TOT_AA_GIA_PROR_NULL, Len.PMO_TOT_AA_GIA_PROR_NULL);
    }

    public String getPmoTotAaGiaProrNullFormatted() {
        return Functions.padBlanks(getPmoTotAaGiaProrNull(), Len.PMO_TOT_AA_GIA_PROR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_TOT_AA_GIA_PROR = 1;
        public static final int PMO_TOT_AA_GIA_PROR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_TOT_AA_GIA_PROR = 3;
        public static final int PMO_TOT_AA_GIA_PROR_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_TOT_AA_GIA_PROR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
