package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: W670-DT-VLT<br>
 * Variable: W670-DT-VLT from program LOAS0670<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class W670DtVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public W670DtVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W670_DT_VLT;
    }

    public void setW670DtVlt(int w670DtVlt) {
        writeIntAsPacked(Pos.W670_DT_VLT, w670DtVlt, Len.Int.W670_DT_VLT);
    }

    public void setW670DtVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.W670_DT_VLT, Pos.W670_DT_VLT);
    }

    /**Original name: W670-DT-VLT<br>*/
    public int getW670DtVlt() {
        return readPackedAsInt(Pos.W670_DT_VLT, Len.Int.W670_DT_VLT);
    }

    public byte[] getW670DtVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W670_DT_VLT, Pos.W670_DT_VLT);
        return buffer;
    }

    public void initW670DtVltSpaces() {
        fill(Pos.W670_DT_VLT, Len.W670_DT_VLT, Types.SPACE_CHAR);
    }

    public void setW670DtVltNull(String w670DtVltNull) {
        writeString(Pos.W670_DT_VLT_NULL, w670DtVltNull, Len.W670_DT_VLT_NULL);
    }

    /**Original name: W670-DT-VLT-NULL<br>*/
    public String getW670DtVltNull() {
        return readString(Pos.W670_DT_VLT_NULL, Len.W670_DT_VLT_NULL);
    }

    public String getW670DtVltNullFormatted() {
        return Functions.padBlanks(getW670DtVltNull(), Len.W670_DT_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W670_DT_VLT = 1;
        public static final int W670_DT_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W670_DT_VLT = 5;
        public static final int W670_DT_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W670_DT_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
