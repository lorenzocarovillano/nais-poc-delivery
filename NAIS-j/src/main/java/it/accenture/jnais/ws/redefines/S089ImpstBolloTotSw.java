package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPST-BOLLO-TOT-SW<br>
 * Variable: S089-IMPST-BOLLO-TOT-SW from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpstBolloTotSw extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpstBolloTotSw() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPST_BOLLO_TOT_SW;
    }

    public void setWlquImpstBolloTotSw(AfDecimal wlquImpstBolloTotSw) {
        writeDecimalAsPacked(Pos.S089_IMPST_BOLLO_TOT_SW, wlquImpstBolloTotSw.copy());
    }

    public void setWlquImpstBolloTotSwFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPST_BOLLO_TOT_SW, Pos.S089_IMPST_BOLLO_TOT_SW);
    }

    /**Original name: WLQU-IMPST-BOLLO-TOT-SW<br>*/
    public AfDecimal getWlquImpstBolloTotSw() {
        return readPackedAsDecimal(Pos.S089_IMPST_BOLLO_TOT_SW, Len.Int.WLQU_IMPST_BOLLO_TOT_SW, Len.Fract.WLQU_IMPST_BOLLO_TOT_SW);
    }

    public byte[] getWlquImpstBolloTotSwAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPST_BOLLO_TOT_SW, Pos.S089_IMPST_BOLLO_TOT_SW);
        return buffer;
    }

    public void initWlquImpstBolloTotSwSpaces() {
        fill(Pos.S089_IMPST_BOLLO_TOT_SW, Len.S089_IMPST_BOLLO_TOT_SW, Types.SPACE_CHAR);
    }

    public void setWlquImpstBolloTotSwNull(String wlquImpstBolloTotSwNull) {
        writeString(Pos.S089_IMPST_BOLLO_TOT_SW_NULL, wlquImpstBolloTotSwNull, Len.WLQU_IMPST_BOLLO_TOT_SW_NULL);
    }

    /**Original name: WLQU-IMPST-BOLLO-TOT-SW-NULL<br>*/
    public String getWlquImpstBolloTotSwNull() {
        return readString(Pos.S089_IMPST_BOLLO_TOT_SW_NULL, Len.WLQU_IMPST_BOLLO_TOT_SW_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPST_BOLLO_TOT_SW = 1;
        public static final int S089_IMPST_BOLLO_TOT_SW_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPST_BOLLO_TOT_SW = 8;
        public static final int WLQU_IMPST_BOLLO_TOT_SW_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_BOLLO_TOT_SW = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_BOLLO_TOT_SW = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
