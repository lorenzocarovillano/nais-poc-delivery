package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-CNTRL-12-RV<br>
 * Variable: FLAG-CNTRL-12-RV from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCntrl12Rv {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCntrl12Rv(char flagCntrl12Rv) {
        this.value = flagCntrl12Rv;
    }

    public char getFlagCntrl12Rv() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
