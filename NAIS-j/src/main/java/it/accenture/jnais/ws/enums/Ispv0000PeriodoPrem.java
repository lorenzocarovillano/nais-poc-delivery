package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPV0000-PERIODO-PREM<br>
 * Variable: ISPV0000-PERIODO-PREM from copybook ISPV0000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispv0000PeriodoPrem {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char UNICO = 'U';
    public static final char UNICO_RICORRENTE = 'R';
    public static final char ANNUALE = 'A';

    //==== METHODS ====
    public void setPeriodoPrem(char periodoPrem) {
        this.value = periodoPrem;
    }

    public void setPeriodoPremFormatted(String periodoPrem) {
        setPeriodoPrem(Functions.charAt(periodoPrem, Types.CHAR_SIZE));
    }

    public char getPeriodoPrem() {
        return this.value;
    }

    public boolean isUnico() {
        return value == UNICO;
    }

    public boolean isUnicoRicorrente() {
        return value == UNICO_RICORRENTE;
    }
}
