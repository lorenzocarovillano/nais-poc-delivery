package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-CAR-GEST<br>
 * Variable: TGA-IMP-CAR-GEST from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_CAR_GEST;
    }

    public void setTgaImpCarGest(AfDecimal tgaImpCarGest) {
        writeDecimalAsPacked(Pos.TGA_IMP_CAR_GEST, tgaImpCarGest.copy());
    }

    public void setTgaImpCarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_CAR_GEST, Pos.TGA_IMP_CAR_GEST);
    }

    /**Original name: TGA-IMP-CAR-GEST<br>*/
    public AfDecimal getTgaImpCarGest() {
        return readPackedAsDecimal(Pos.TGA_IMP_CAR_GEST, Len.Int.TGA_IMP_CAR_GEST, Len.Fract.TGA_IMP_CAR_GEST);
    }

    public byte[] getTgaImpCarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_CAR_GEST, Pos.TGA_IMP_CAR_GEST);
        return buffer;
    }

    public void setTgaImpCarGestNull(String tgaImpCarGestNull) {
        writeString(Pos.TGA_IMP_CAR_GEST_NULL, tgaImpCarGestNull, Len.TGA_IMP_CAR_GEST_NULL);
    }

    /**Original name: TGA-IMP-CAR-GEST-NULL<br>*/
    public String getTgaImpCarGestNull() {
        return readString(Pos.TGA_IMP_CAR_GEST_NULL, Len.TGA_IMP_CAR_GEST_NULL);
    }

    public String getTgaImpCarGestNullFormatted() {
        return Functions.padBlanks(getTgaImpCarGestNull(), Len.TGA_IMP_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_CAR_GEST = 1;
        public static final int TGA_IMP_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_CAR_GEST = 8;
        public static final int TGA_IMP_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
