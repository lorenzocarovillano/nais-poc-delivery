package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.FlagCurMov;
import it.accenture.jnais.ws.enums.FlagMovTrov;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTipoMov;
import it.accenture.jnais.ws.redefines.WsTabLiquiComun;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0089<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0089Data {

    //==== PROPERTIES ====
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: WK-DS-TS-CPTZ
    private long wkDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: FLAG-CUR-MOV
    private FlagCurMov flagCurMov = new FlagCurMov();
    //Original name: FLAG-MOV-TROV
    private FlagMovTrov flagMovTrov = new FlagMovTrov();
    //Original name: WS-TIPO-MOV
    private WsTipoMov wsTipoMov = new WsTipoMov();
    //Original name: WS-TAB-MAX
    private short wsTabMax = ((short)50);
    //Original name: WS-TAB-LIQUI-COMUN
    private WsTabLiquiComun wsTabLiquiComun = new WsTabLiquiComun();
    //Original name: WS-TRCH-POS
    private WsTrchPosLvvs0089 wsTrchPos = new WsTrchPosLvvs0089();
    //Original name: WS-TRCH-NEG
    private WsTrchNegLvvs0089 wsTrchNeg = new WsTrchNegLvvs0089();
    //Original name: WS-IMPB-VIS-END2000
    private AfDecimal wsImpbVisEnd2000 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-DATA-EFF-RIP
    private int wkDataEffRip = DefaultValues.INT_VAL;
    //Original name: WK-DATA-CPTZ-RIP
    private long wkDataCptzRip = DefaultValues.LONG_VAL;
    //Original name: LDBVE261
    private Ldbve261 ldbve261 = new Ldbve261();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    //Original name: MOVI
    private MoviLdbs1530 movi = new MoviLdbs1530();
    //Original name: IX-INDICI
    private IxIndiciLvvs0089 ixIndici = new IxIndiciLvvs0089();

    //==== METHODS ====
    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setWkDsTsCptz(long wkDsTsCptz) {
        this.wkDsTsCptz = wkDsTsCptz;
    }

    public long getWkDsTsCptz() {
        return this.wkDsTsCptz;
    }

    public short getWsTabMax() {
        return this.wsTabMax;
    }

    public void setWsImpbVisEnd2000(AfDecimal wsImpbVisEnd2000) {
        this.wsImpbVisEnd2000.assign(wsImpbVisEnd2000);
    }

    public AfDecimal getWsImpbVisEnd2000() {
        return this.wsImpbVisEnd2000.copy();
    }

    public void setWkDataEffRip(int wkDataEffRip) {
        this.wkDataEffRip = wkDataEffRip;
    }

    public int getWkDataEffRip() {
        return this.wkDataEffRip;
    }

    public void setWkDataCptzRip(long wkDataCptzRip) {
        this.wkDataCptzRip = wkDataCptzRip;
    }

    public long getWkDataCptzRip() {
        return this.wkDataCptzRip;
    }

    public FlagCurMov getFlagCurMov() {
        return flagCurMov;
    }

    public FlagMovTrov getFlagMovTrov() {
        return flagMovTrov;
    }

    public IxIndiciLvvs0089 getIxIndici() {
        return ixIndici;
    }

    public Ldbve261 getLdbve261() {
        return ldbve261;
    }

    public MoviLdbs1530 getMovi() {
        return movi;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTabLiquiComun getWsTabLiquiComun() {
        return wsTabLiquiComun;
    }

    public WsTipoMov getWsTipoMov() {
        return wsTipoMov;
    }

    public WsTrchNegLvvs0089 getWsTrchNeg() {
        return wsTrchNeg;
    }

    public WsTrchPosLvvs0089 getWsTrchPos() {
        return wsTrchPos;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_DATA_INFERIORE = 8;
        public static final int GG_DIFF = 5;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
