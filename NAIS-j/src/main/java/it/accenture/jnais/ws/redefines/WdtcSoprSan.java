package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-SOPR-SAN<br>
 * Variable: WDTC-SOPR-SAN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_SOPR_SAN;
    }

    public void setWdtcSoprSan(AfDecimal wdtcSoprSan) {
        writeDecimalAsPacked(Pos.WDTC_SOPR_SAN, wdtcSoprSan.copy());
    }

    public void setWdtcSoprSanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_SOPR_SAN, Pos.WDTC_SOPR_SAN);
    }

    /**Original name: WDTC-SOPR-SAN<br>*/
    public AfDecimal getWdtcSoprSan() {
        return readPackedAsDecimal(Pos.WDTC_SOPR_SAN, Len.Int.WDTC_SOPR_SAN, Len.Fract.WDTC_SOPR_SAN);
    }

    public byte[] getWdtcSoprSanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_SOPR_SAN, Pos.WDTC_SOPR_SAN);
        return buffer;
    }

    public void initWdtcSoprSanSpaces() {
        fill(Pos.WDTC_SOPR_SAN, Len.WDTC_SOPR_SAN, Types.SPACE_CHAR);
    }

    public void setWdtcSoprSanNull(String wdtcSoprSanNull) {
        writeString(Pos.WDTC_SOPR_SAN_NULL, wdtcSoprSanNull, Len.WDTC_SOPR_SAN_NULL);
    }

    /**Original name: WDTC-SOPR-SAN-NULL<br>*/
    public String getWdtcSoprSanNull() {
        return readString(Pos.WDTC_SOPR_SAN_NULL, Len.WDTC_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_SOPR_SAN = 1;
        public static final int WDTC_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_SOPR_SAN = 8;
        public static final int WDTC_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
