package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: QUE-ID-MOVI-CHIU<br>
 * Variable: QUE-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class QueIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public QueIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.QUE_ID_MOVI_CHIU;
    }

    public void setQueIdMoviChiu(int queIdMoviChiu) {
        writeIntAsPacked(Pos.QUE_ID_MOVI_CHIU, queIdMoviChiu, Len.Int.QUE_ID_MOVI_CHIU);
    }

    public void setQueIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.QUE_ID_MOVI_CHIU, Pos.QUE_ID_MOVI_CHIU);
    }

    /**Original name: QUE-ID-MOVI-CHIU<br>*/
    public int getQueIdMoviChiu() {
        return readPackedAsInt(Pos.QUE_ID_MOVI_CHIU, Len.Int.QUE_ID_MOVI_CHIU);
    }

    public byte[] getQueIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.QUE_ID_MOVI_CHIU, Pos.QUE_ID_MOVI_CHIU);
        return buffer;
    }

    public void setQueIdMoviChiuNull(String queIdMoviChiuNull) {
        writeString(Pos.QUE_ID_MOVI_CHIU_NULL, queIdMoviChiuNull, Len.QUE_ID_MOVI_CHIU_NULL);
    }

    /**Original name: QUE-ID-MOVI-CHIU-NULL<br>*/
    public String getQueIdMoviChiuNull() {
        return readString(Pos.QUE_ID_MOVI_CHIU_NULL, Len.QUE_ID_MOVI_CHIU_NULL);
    }

    public String getQueIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getQueIdMoviChiuNull(), Len.QUE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int QUE_ID_MOVI_CHIU = 1;
        public static final int QUE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int QUE_ID_MOVI_CHIU = 5;
        public static final int QUE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int QUE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
