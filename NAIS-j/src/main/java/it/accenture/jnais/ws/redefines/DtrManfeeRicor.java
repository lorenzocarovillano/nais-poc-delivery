package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-MANFEE-RICOR<br>
 * Variable: DTR-MANFEE-RICOR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrManfeeRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrManfeeRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_MANFEE_RICOR;
    }

    public void setDtrManfeeRicor(AfDecimal dtrManfeeRicor) {
        writeDecimalAsPacked(Pos.DTR_MANFEE_RICOR, dtrManfeeRicor.copy());
    }

    public void setDtrManfeeRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_MANFEE_RICOR, Pos.DTR_MANFEE_RICOR);
    }

    /**Original name: DTR-MANFEE-RICOR<br>*/
    public AfDecimal getDtrManfeeRicor() {
        return readPackedAsDecimal(Pos.DTR_MANFEE_RICOR, Len.Int.DTR_MANFEE_RICOR, Len.Fract.DTR_MANFEE_RICOR);
    }

    public byte[] getDtrManfeeRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_MANFEE_RICOR, Pos.DTR_MANFEE_RICOR);
        return buffer;
    }

    public void setDtrManfeeRicorNull(String dtrManfeeRicorNull) {
        writeString(Pos.DTR_MANFEE_RICOR_NULL, dtrManfeeRicorNull, Len.DTR_MANFEE_RICOR_NULL);
    }

    /**Original name: DTR-MANFEE-RICOR-NULL<br>*/
    public String getDtrManfeeRicorNull() {
        return readString(Pos.DTR_MANFEE_RICOR_NULL, Len.DTR_MANFEE_RICOR_NULL);
    }

    public String getDtrManfeeRicorNullFormatted() {
        return Functions.padBlanks(getDtrManfeeRicorNull(), Len.DTR_MANFEE_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_MANFEE_RICOR = 1;
        public static final int DTR_MANFEE_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_MANFEE_RICOR = 8;
        public static final int DTR_MANFEE_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_MANFEE_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_MANFEE_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
