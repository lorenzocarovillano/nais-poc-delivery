package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-FIN-RIC<br>
 * Variable: WK-FIN-RIC from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkFinRic {

    //==== PROPERTIES ====
    //Original name: WK-DT-FIN-AA
    private String aa = DefaultValues.stringVal(Len.AA);
    //Original name: WK-DT-FIN-MM
    private String mm = "12";
    //Original name: WK-DT-FIN-GG
    private String gg = "31";

    //==== METHODS ====
    public byte[] getWkFinRicBytes() {
        byte[] buffer = new byte[Len.WK_FIN_RIC];
        return getWkFinRicBytes(buffer, 1);
    }

    public byte[] getWkFinRicBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aa, Len.AA);
        position += Len.AA;
        MarshalByte.writeString(buffer, position, mm, Len.MM);
        position += Len.MM;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        return buffer;
    }

    public void setAaFormatted(String aa) {
        this.aa = Trunc.toUnsignedNumeric(aa, Len.AA);
    }

    public short getAa() {
        return NumericDisplay.asShort(this.aa);
    }

    public void setMm(short mm) {
        this.mm = NumericDisplay.asString(mm, Len.MM);
    }

    public void setMmFormatted(String mm) {
        this.mm = Trunc.toUnsignedNumeric(mm, Len.MM);
    }

    public short getMm() {
        return NumericDisplay.asShort(this.mm);
    }

    public void setGg(short gg) {
        this.gg = NumericDisplay.asString(gg, Len.GG);
    }

    public void setGgFormatted(String gg) {
        this.gg = Trunc.toUnsignedNumeric(gg, Len.GG);
    }

    public short getGg() {
        return NumericDisplay.asShort(this.gg);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WK_FIN_RIC = AA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
