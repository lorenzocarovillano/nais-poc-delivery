package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-ARROT-PRE<br>
 * Variable: PCO-ARROT-PRE from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoArrotPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoArrotPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_ARROT_PRE;
    }

    public void setPcoArrotPre(AfDecimal pcoArrotPre) {
        writeDecimalAsPacked(Pos.PCO_ARROT_PRE, pcoArrotPre.copy());
    }

    public void setPcoArrotPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_ARROT_PRE, Pos.PCO_ARROT_PRE);
    }

    /**Original name: PCO-ARROT-PRE<br>*/
    public AfDecimal getPcoArrotPre() {
        return readPackedAsDecimal(Pos.PCO_ARROT_PRE, Len.Int.PCO_ARROT_PRE, Len.Fract.PCO_ARROT_PRE);
    }

    public byte[] getPcoArrotPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_ARROT_PRE, Pos.PCO_ARROT_PRE);
        return buffer;
    }

    public void initPcoArrotPreHighValues() {
        fill(Pos.PCO_ARROT_PRE, Len.PCO_ARROT_PRE, Types.HIGH_CHAR_VAL);
    }

    public void setPcoArrotPreNull(String pcoArrotPreNull) {
        writeString(Pos.PCO_ARROT_PRE_NULL, pcoArrotPreNull, Len.PCO_ARROT_PRE_NULL);
    }

    /**Original name: PCO-ARROT-PRE-NULL<br>*/
    public String getPcoArrotPreNull() {
        return readString(Pos.PCO_ARROT_PRE_NULL, Len.PCO_ARROT_PRE_NULL);
    }

    public String getPcoArrotPreNullFormatted() {
        return Functions.padBlanks(getPcoArrotPreNull(), Len.PCO_ARROT_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_ARROT_PRE = 1;
        public static final int PCO_ARROT_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_ARROT_PRE = 8;
        public static final int PCO_ARROT_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_ARROT_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_ARROT_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
