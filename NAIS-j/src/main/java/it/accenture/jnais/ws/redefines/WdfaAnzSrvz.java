package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-ANZ-SRVZ<br>
 * Variable: WDFA-ANZ-SRVZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaAnzSrvz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaAnzSrvz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_ANZ_SRVZ;
    }

    public void setWdfaAnzSrvz(short wdfaAnzSrvz) {
        writeShortAsPacked(Pos.WDFA_ANZ_SRVZ, wdfaAnzSrvz, Len.Int.WDFA_ANZ_SRVZ);
    }

    public void setWdfaAnzSrvzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_ANZ_SRVZ, Pos.WDFA_ANZ_SRVZ);
    }

    /**Original name: WDFA-ANZ-SRVZ<br>*/
    public short getWdfaAnzSrvz() {
        return readPackedAsShort(Pos.WDFA_ANZ_SRVZ, Len.Int.WDFA_ANZ_SRVZ);
    }

    public byte[] getWdfaAnzSrvzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_ANZ_SRVZ, Pos.WDFA_ANZ_SRVZ);
        return buffer;
    }

    public void setWdfaAnzSrvzNull(String wdfaAnzSrvzNull) {
        writeString(Pos.WDFA_ANZ_SRVZ_NULL, wdfaAnzSrvzNull, Len.WDFA_ANZ_SRVZ_NULL);
    }

    /**Original name: WDFA-ANZ-SRVZ-NULL<br>*/
    public String getWdfaAnzSrvzNull() {
        return readString(Pos.WDFA_ANZ_SRVZ_NULL, Len.WDFA_ANZ_SRVZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_ANZ_SRVZ = 1;
        public static final int WDFA_ANZ_SRVZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_ANZ_SRVZ = 3;
        public static final int WDFA_ANZ_SRVZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_ANZ_SRVZ = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
