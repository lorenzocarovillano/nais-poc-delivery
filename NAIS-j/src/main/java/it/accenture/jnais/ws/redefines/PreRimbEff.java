package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-RIMB-EFF<br>
 * Variable: PRE-RIMB-EFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreRimbEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreRimbEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_RIMB_EFF;
    }

    public void setPreRimbEff(AfDecimal preRimbEff) {
        writeDecimalAsPacked(Pos.PRE_RIMB_EFF, preRimbEff.copy());
    }

    public void setPreRimbEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_RIMB_EFF, Pos.PRE_RIMB_EFF);
    }

    /**Original name: PRE-RIMB-EFF<br>*/
    public AfDecimal getPreRimbEff() {
        return readPackedAsDecimal(Pos.PRE_RIMB_EFF, Len.Int.PRE_RIMB_EFF, Len.Fract.PRE_RIMB_EFF);
    }

    public byte[] getPreRimbEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_RIMB_EFF, Pos.PRE_RIMB_EFF);
        return buffer;
    }

    public void setPreRimbEffNull(String preRimbEffNull) {
        writeString(Pos.PRE_RIMB_EFF_NULL, preRimbEffNull, Len.PRE_RIMB_EFF_NULL);
    }

    /**Original name: PRE-RIMB-EFF-NULL<br>*/
    public String getPreRimbEffNull() {
        return readString(Pos.PRE_RIMB_EFF_NULL, Len.PRE_RIMB_EFF_NULL);
    }

    public String getPreRimbEffNullFormatted() {
        return Functions.padBlanks(getPreRimbEffNull(), Len.PRE_RIMB_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_RIMB_EFF = 1;
        public static final int PRE_RIMB_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_RIMB_EFF = 8;
        public static final int PRE_RIMB_EFF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_RIMB_EFF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_RIMB_EFF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
