package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-IMP-AFI<br>
 * Variable: P56-IMP-AFI from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56ImpAfi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56ImpAfi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_IMP_AFI;
    }

    public void setP56ImpAfi(AfDecimal p56ImpAfi) {
        writeDecimalAsPacked(Pos.P56_IMP_AFI, p56ImpAfi.copy());
    }

    public void setP56ImpAfiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_IMP_AFI, Pos.P56_IMP_AFI);
    }

    /**Original name: P56-IMP-AFI<br>*/
    public AfDecimal getP56ImpAfi() {
        return readPackedAsDecimal(Pos.P56_IMP_AFI, Len.Int.P56_IMP_AFI, Len.Fract.P56_IMP_AFI);
    }

    public byte[] getP56ImpAfiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_IMP_AFI, Pos.P56_IMP_AFI);
        return buffer;
    }

    public void setP56ImpAfiNull(String p56ImpAfiNull) {
        writeString(Pos.P56_IMP_AFI_NULL, p56ImpAfiNull, Len.P56_IMP_AFI_NULL);
    }

    /**Original name: P56-IMP-AFI-NULL<br>*/
    public String getP56ImpAfiNull() {
        return readString(Pos.P56_IMP_AFI_NULL, Len.P56_IMP_AFI_NULL);
    }

    public String getP56ImpAfiNullFormatted() {
        return Functions.padBlanks(getP56ImpAfiNull(), Len.P56_IMP_AFI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_IMP_AFI = 1;
        public static final int P56_IMP_AFI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_IMP_AFI = 8;
        public static final int P56_IMP_AFI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_IMP_AFI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_IMP_AFI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
