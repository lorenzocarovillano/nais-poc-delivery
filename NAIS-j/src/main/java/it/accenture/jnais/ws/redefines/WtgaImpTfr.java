package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-TFR<br>
 * Variable: WTGA-IMP-TFR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_TFR;
    }

    public void setWtgaImpTfr(AfDecimal wtgaImpTfr) {
        writeDecimalAsPacked(Pos.WTGA_IMP_TFR, wtgaImpTfr.copy());
    }

    public void setWtgaImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_TFR, Pos.WTGA_IMP_TFR);
    }

    /**Original name: WTGA-IMP-TFR<br>*/
    public AfDecimal getWtgaImpTfr() {
        return readPackedAsDecimal(Pos.WTGA_IMP_TFR, Len.Int.WTGA_IMP_TFR, Len.Fract.WTGA_IMP_TFR);
    }

    public byte[] getWtgaImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_TFR, Pos.WTGA_IMP_TFR);
        return buffer;
    }

    public void initWtgaImpTfrSpaces() {
        fill(Pos.WTGA_IMP_TFR, Len.WTGA_IMP_TFR, Types.SPACE_CHAR);
    }

    public void setWtgaImpTfrNull(String wtgaImpTfrNull) {
        writeString(Pos.WTGA_IMP_TFR_NULL, wtgaImpTfrNull, Len.WTGA_IMP_TFR_NULL);
    }

    /**Original name: WTGA-IMP-TFR-NULL<br>*/
    public String getWtgaImpTfrNull() {
        return readString(Pos.WTGA_IMP_TFR_NULL, Len.WTGA_IMP_TFR_NULL);
    }

    public String getWtgaImpTfrNullFormatted() {
        return Functions.padBlanks(getWtgaImpTfrNull(), Len.WTGA_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_TFR = 1;
        public static final int WTGA_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_TFR = 8;
        public static final int WTGA_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
