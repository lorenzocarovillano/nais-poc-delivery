package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WMFZ-ID-TIT-CONT<br>
 * Variable: WMFZ-ID-TIT-CONT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmfzIdTitCont extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmfzIdTitCont() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMFZ_ID_TIT_CONT;
    }

    public void setWmfzIdTitCont(int wmfzIdTitCont) {
        writeIntAsPacked(Pos.WMFZ_ID_TIT_CONT, wmfzIdTitCont, Len.Int.WMFZ_ID_TIT_CONT);
    }

    public void setWmfzIdTitContFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMFZ_ID_TIT_CONT, Pos.WMFZ_ID_TIT_CONT);
    }

    /**Original name: WMFZ-ID-TIT-CONT<br>*/
    public int getWmfzIdTitCont() {
        return readPackedAsInt(Pos.WMFZ_ID_TIT_CONT, Len.Int.WMFZ_ID_TIT_CONT);
    }

    public byte[] getWmfzIdTitContAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMFZ_ID_TIT_CONT, Pos.WMFZ_ID_TIT_CONT);
        return buffer;
    }

    public void initWmfzIdTitContSpaces() {
        fill(Pos.WMFZ_ID_TIT_CONT, Len.WMFZ_ID_TIT_CONT, Types.SPACE_CHAR);
    }

    public void setWmfzIdTitContNull(String wmfzIdTitContNull) {
        writeString(Pos.WMFZ_ID_TIT_CONT_NULL, wmfzIdTitContNull, Len.WMFZ_ID_TIT_CONT_NULL);
    }

    /**Original name: WMFZ-ID-TIT-CONT-NULL<br>*/
    public String getWmfzIdTitContNull() {
        return readString(Pos.WMFZ_ID_TIT_CONT_NULL, Len.WMFZ_ID_TIT_CONT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMFZ_ID_TIT_CONT = 1;
        public static final int WMFZ_ID_TIT_CONT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMFZ_ID_TIT_CONT = 5;
        public static final int WMFZ_ID_TIT_CONT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMFZ_ID_TIT_CONT = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
