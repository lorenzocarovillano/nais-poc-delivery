package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRSTZ-ULT<br>
 * Variable: WPAG-PRSTZ-ULT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPrstzUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPrstzUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRSTZ_ULT;
    }

    public void setWpagPrstzUlt(AfDecimal wpagPrstzUlt) {
        writeDecimalAsPacked(Pos.WPAG_PRSTZ_ULT, wpagPrstzUlt.copy());
    }

    public void setWpagPrstzUltFormatted(String wpagPrstzUlt) {
        setWpagPrstzUlt(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRSTZ_ULT + Len.Fract.WPAG_PRSTZ_ULT, Len.Fract.WPAG_PRSTZ_ULT, wpagPrstzUlt));
    }

    public void setWpagPrstzUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRSTZ_ULT, Pos.WPAG_PRSTZ_ULT);
    }

    /**Original name: WPAG-PRSTZ-ULT<br>*/
    public AfDecimal getWpagPrstzUlt() {
        return readPackedAsDecimal(Pos.WPAG_PRSTZ_ULT, Len.Int.WPAG_PRSTZ_ULT, Len.Fract.WPAG_PRSTZ_ULT);
    }

    public byte[] getWpagPrstzUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRSTZ_ULT, Pos.WPAG_PRSTZ_ULT);
        return buffer;
    }

    public void initWpagPrstzUltSpaces() {
        fill(Pos.WPAG_PRSTZ_ULT, Len.WPAG_PRSTZ_ULT, Types.SPACE_CHAR);
    }

    public void setWpagPrstzUltNull(String wpagPrstzUltNull) {
        writeString(Pos.WPAG_PRSTZ_ULT_NULL, wpagPrstzUltNull, Len.WPAG_PRSTZ_ULT_NULL);
    }

    /**Original name: WPAG-PRSTZ-ULT-NULL<br>*/
    public String getWpagPrstzUltNull() {
        return readString(Pos.WPAG_PRSTZ_ULT_NULL, Len.WPAG_PRSTZ_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRSTZ_ULT = 1;
        public static final int WPAG_PRSTZ_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRSTZ_ULT = 8;
        public static final int WPAG_PRSTZ_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRSTZ_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRSTZ_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
