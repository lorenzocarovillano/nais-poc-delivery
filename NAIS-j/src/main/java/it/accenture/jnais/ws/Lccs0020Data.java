package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.AreaLdbi0260;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0020<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0020Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LCCS0020";
    //Original name: WK-MOVI-INP
    private String wkMoviInp = DefaultValues.stringVal(Len.WK_MOVI_INP);
    //Original name: WK-MOVI-OUT
    private String wkMoviOut = DefaultValues.stringVal(Len.WK_MOVI_OUT);
    /**Original name: WS-ESITO<br>
	 * <pre>----------------------------------------------------------------*
	 *     DEFINIZIONE DI FLAG E SWITCH                                *
	 * ----------------------------------------------------------------*</pre>*/
    private boolean wsEsito = true;
    //Original name: MATR-MOVIMENTO
    private MatrMovimento matrMovimento = new MatrMovimento();
    //Original name: AREA-CALL-IWFS0050
    private AreaCallIwfs0050 areaCallIwfs0050 = new AreaCallIwfs0050();
    //Original name: AREA-LDBI0260
    private AreaLdbi0260 areaLdbi0260 = new AreaLdbi0260();
    //Original name: IDSV0003
    private Idsv0003 idsv0003 = new Idsv0003();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkMoviInp(String wkMoviInp) {
        this.wkMoviInp = Functions.subString(wkMoviInp, Len.WK_MOVI_INP);
    }

    public String getWkMoviInp() {
        return this.wkMoviInp;
    }

    public String getWkMoviInpFormatted() {
        return Functions.padBlanks(getWkMoviInp(), Len.WK_MOVI_INP);
    }

    public void setWkMoviOut(int wkMoviOut) {
        this.wkMoviOut = NumericDisplay.asString(wkMoviOut, Len.WK_MOVI_OUT);
    }

    public int getWkMoviOut() {
        return NumericDisplay.asInt(this.wkMoviOut);
    }

    public String getWkMoviOutFormatted() {
        return this.wkMoviOut;
    }

    public void setWsEsito(boolean wsEsito) {
        this.wsEsito = wsEsito;
    }

    public boolean isWsEsito() {
        return this.wsEsito;
    }

    public AreaCallIwfs0050 getAreaCallIwfs0050() {
        return areaCallIwfs0050;
    }

    public AreaLdbi0260 getAreaLdbi0260() {
        return areaLdbi0260;
    }

    public Idsv0003 getIdsv0003() {
        return idsv0003;
    }

    public MatrMovimento getMatrMovimento() {
        return matrMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_MOVI_INP = 5;
        public static final int WK_MOVI_OUT = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
