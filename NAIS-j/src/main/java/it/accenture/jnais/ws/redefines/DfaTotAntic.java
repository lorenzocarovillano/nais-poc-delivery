package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-TOT-ANTIC<br>
 * Variable: DFA-TOT-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaTotAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaTotAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_TOT_ANTIC;
    }

    public void setDfaTotAntic(AfDecimal dfaTotAntic) {
        writeDecimalAsPacked(Pos.DFA_TOT_ANTIC, dfaTotAntic.copy());
    }

    public void setDfaTotAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_TOT_ANTIC, Pos.DFA_TOT_ANTIC);
    }

    /**Original name: DFA-TOT-ANTIC<br>*/
    public AfDecimal getDfaTotAntic() {
        return readPackedAsDecimal(Pos.DFA_TOT_ANTIC, Len.Int.DFA_TOT_ANTIC, Len.Fract.DFA_TOT_ANTIC);
    }

    public byte[] getDfaTotAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_TOT_ANTIC, Pos.DFA_TOT_ANTIC);
        return buffer;
    }

    public void setDfaTotAnticNull(String dfaTotAnticNull) {
        writeString(Pos.DFA_TOT_ANTIC_NULL, dfaTotAnticNull, Len.DFA_TOT_ANTIC_NULL);
    }

    /**Original name: DFA-TOT-ANTIC-NULL<br>*/
    public String getDfaTotAnticNull() {
        return readString(Pos.DFA_TOT_ANTIC_NULL, Len.DFA_TOT_ANTIC_NULL);
    }

    public String getDfaTotAnticNullFormatted() {
        return Functions.padBlanks(getDfaTotAnticNull(), Len.DFA_TOT_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_TOT_ANTIC = 1;
        public static final int DFA_TOT_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_TOT_ANTIC = 8;
        public static final int DFA_TOT_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_TOT_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_TOT_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
