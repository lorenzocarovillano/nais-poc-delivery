package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.occurs.S089TabLiq;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0102<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0102Data {

    //==== PROPERTIES ====
    public static final int DLQU_TAB_LIQ_MAXOCCURS = 30;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0102";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: INPUT-LVVS0000
    private InputLvvs0000 inputLvvs0000 = new InputLvvs0000();
    //Original name: DLQU-ELE-LIQ-MAX
    private short dlquEleLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DLQU-TAB-LIQ
    private S089TabLiq[] dlquTabLiq = new S089TabLiq[DLQU_TAB_LIQ_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-LQU
    private short ixTabLqu = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0102Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dlquTabLiqIdx = 1; dlquTabLiqIdx <= DLQU_TAB_LIQ_MAXOCCURS; dlquTabLiqIdx++) {
            dlquTabLiq[dlquTabLiqIdx - 1] = new S089TabLiq();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setDlquAreaLiqFormatted(String data) {
        byte[] buffer = new byte[Len.DLQU_AREA_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.DLQU_AREA_LIQ);
        setDlquAreaLiqBytes(buffer, 1);
    }

    public void setDlquAreaLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        dlquEleLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DLQU_TAB_LIQ_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dlquTabLiq[idx - 1].setWlquTabLiqBytes(buffer, position);
                position += S089TabLiq.Len.WLQU_TAB_LIQ;
            }
            else {
                dlquTabLiq[idx - 1].initWlquTabLiqSpaces();
                position += S089TabLiq.Len.WLQU_TAB_LIQ;
            }
        }
    }

    public void setDlquEleLiqMax(short dlquEleLiqMax) {
        this.dlquEleLiqMax = dlquEleLiqMax;
    }

    public short getDlquEleLiqMax() {
        return this.dlquEleLiqMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabLqu(short ixTabLqu) {
        this.ixTabLqu = ixTabLqu;
    }

    public short getIxTabLqu() {
        return this.ixTabLqu;
    }

    public S089TabLiq getDlquTabLiq(int idx) {
        return dlquTabLiq[idx - 1];
    }

    public InputLvvs0000 getInputLvvs0000() {
        return inputLvvs0000;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DLQU_ELE_LIQ_MAX = 2;
        public static final int DLQU_AREA_LIQ = DLQU_ELE_LIQ_MAX + Lvvs0102Data.DLQU_TAB_LIQ_MAXOCCURS * S089TabLiq.Len.WLQU_TAB_LIQ;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
