package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-INTR-RETDT<br>
 * Variable: WDTC-INTR-RETDT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcIntrRetdt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcIntrRetdt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_INTR_RETDT;
    }

    public void setWdtcIntrRetdt(AfDecimal wdtcIntrRetdt) {
        writeDecimalAsPacked(Pos.WDTC_INTR_RETDT, wdtcIntrRetdt.copy());
    }

    public void setWdtcIntrRetdtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_INTR_RETDT, Pos.WDTC_INTR_RETDT);
    }

    /**Original name: WDTC-INTR-RETDT<br>*/
    public AfDecimal getWdtcIntrRetdt() {
        return readPackedAsDecimal(Pos.WDTC_INTR_RETDT, Len.Int.WDTC_INTR_RETDT, Len.Fract.WDTC_INTR_RETDT);
    }

    public byte[] getWdtcIntrRetdtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_INTR_RETDT, Pos.WDTC_INTR_RETDT);
        return buffer;
    }

    public void initWdtcIntrRetdtSpaces() {
        fill(Pos.WDTC_INTR_RETDT, Len.WDTC_INTR_RETDT, Types.SPACE_CHAR);
    }

    public void setWdtcIntrRetdtNull(String wdtcIntrRetdtNull) {
        writeString(Pos.WDTC_INTR_RETDT_NULL, wdtcIntrRetdtNull, Len.WDTC_INTR_RETDT_NULL);
    }

    /**Original name: WDTC-INTR-RETDT-NULL<br>*/
    public String getWdtcIntrRetdtNull() {
        return readString(Pos.WDTC_INTR_RETDT_NULL, Len.WDTC_INTR_RETDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_INTR_RETDT = 1;
        public static final int WDTC_INTR_RETDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_INTR_RETDT = 8;
        public static final int WDTC_INTR_RETDT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_INTR_RETDT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_INTR_RETDT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
