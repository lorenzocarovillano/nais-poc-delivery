package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-EMES<br>
 * Variable: PCO-DT-ULT-BOLL-EMES from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollEmes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollEmes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_EMES;
    }

    public void setPcoDtUltBollEmes(int pcoDtUltBollEmes) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_EMES, pcoDtUltBollEmes, Len.Int.PCO_DT_ULT_BOLL_EMES);
    }

    public void setPcoDtUltBollEmesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_EMES, Pos.PCO_DT_ULT_BOLL_EMES);
    }

    /**Original name: PCO-DT-ULT-BOLL-EMES<br>*/
    public int getPcoDtUltBollEmes() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_EMES, Len.Int.PCO_DT_ULT_BOLL_EMES);
    }

    public byte[] getPcoDtUltBollEmesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_EMES, Pos.PCO_DT_ULT_BOLL_EMES);
        return buffer;
    }

    public void initPcoDtUltBollEmesHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_EMES, Len.PCO_DT_ULT_BOLL_EMES, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollEmesNull(String pcoDtUltBollEmesNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_EMES_NULL, pcoDtUltBollEmesNull, Len.PCO_DT_ULT_BOLL_EMES_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-EMES-NULL<br>*/
    public String getPcoDtUltBollEmesNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_EMES_NULL, Len.PCO_DT_ULT_BOLL_EMES_NULL);
    }

    public String getPcoDtUltBollEmesNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollEmesNull(), Len.PCO_DT_ULT_BOLL_EMES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_EMES = 1;
        public static final int PCO_DT_ULT_BOLL_EMES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_EMES = 5;
        public static final int PCO_DT_ULT_BOLL_EMES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_EMES = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
