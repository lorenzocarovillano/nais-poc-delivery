package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ODE-ID-MOVI-CHIU<br>
 * Variable: ODE-ID-MOVI-CHIU from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OdeIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OdeIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ODE_ID_MOVI_CHIU;
    }

    public void setOdeIdMoviChiu(int odeIdMoviChiu) {
        writeIntAsPacked(Pos.ODE_ID_MOVI_CHIU, odeIdMoviChiu, Len.Int.ODE_ID_MOVI_CHIU);
    }

    public void setOdeIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ODE_ID_MOVI_CHIU, Pos.ODE_ID_MOVI_CHIU);
    }

    /**Original name: ODE-ID-MOVI-CHIU<br>*/
    public int getOdeIdMoviChiu() {
        return readPackedAsInt(Pos.ODE_ID_MOVI_CHIU, Len.Int.ODE_ID_MOVI_CHIU);
    }

    public byte[] getOdeIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ODE_ID_MOVI_CHIU, Pos.ODE_ID_MOVI_CHIU);
        return buffer;
    }

    public void setOdeIdMoviChiuNull(String odeIdMoviChiuNull) {
        writeString(Pos.ODE_ID_MOVI_CHIU_NULL, odeIdMoviChiuNull, Len.ODE_ID_MOVI_CHIU_NULL);
    }

    /**Original name: ODE-ID-MOVI-CHIU-NULL<br>*/
    public String getOdeIdMoviChiuNull() {
        return readString(Pos.ODE_ID_MOVI_CHIU_NULL, Len.ODE_ID_MOVI_CHIU_NULL);
    }

    public String getOdeIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getOdeIdMoviChiuNull(), Len.ODE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ODE_ID_MOVI_CHIU = 1;
        public static final int ODE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ODE_ID_MOVI_CHIU = 5;
        public static final int ODE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ODE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
