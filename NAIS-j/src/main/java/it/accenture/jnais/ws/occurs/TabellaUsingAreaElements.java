package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: TABELLA-USING-AREA-ELEMENTS<br>
 * Variables: TABELLA-USING-AREA-ELEMENTS from program IDSS0020<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TabellaUsingAreaElements {

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_STR_DATO_U_A = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
