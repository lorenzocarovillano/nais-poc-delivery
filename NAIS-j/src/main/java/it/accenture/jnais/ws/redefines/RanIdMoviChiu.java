package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RAN-ID-MOVI-CHIU<br>
 * Variable: RAN-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RanIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RanIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RAN_ID_MOVI_CHIU;
    }

    public void setRanIdMoviChiu(int ranIdMoviChiu) {
        writeIntAsPacked(Pos.RAN_ID_MOVI_CHIU, ranIdMoviChiu, Len.Int.RAN_ID_MOVI_CHIU);
    }

    public void setRanIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RAN_ID_MOVI_CHIU, Pos.RAN_ID_MOVI_CHIU);
    }

    /**Original name: RAN-ID-MOVI-CHIU<br>*/
    public int getRanIdMoviChiu() {
        return readPackedAsInt(Pos.RAN_ID_MOVI_CHIU, Len.Int.RAN_ID_MOVI_CHIU);
    }

    public byte[] getRanIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RAN_ID_MOVI_CHIU, Pos.RAN_ID_MOVI_CHIU);
        return buffer;
    }

    public void setRanIdMoviChiuNull(String ranIdMoviChiuNull) {
        writeString(Pos.RAN_ID_MOVI_CHIU_NULL, ranIdMoviChiuNull, Len.RAN_ID_MOVI_CHIU_NULL);
    }

    /**Original name: RAN-ID-MOVI-CHIU-NULL<br>*/
    public String getRanIdMoviChiuNull() {
        return readString(Pos.RAN_ID_MOVI_CHIU_NULL, Len.RAN_ID_MOVI_CHIU_NULL);
    }

    public String getRanIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getRanIdMoviChiuNull(), Len.RAN_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RAN_ID_MOVI_CHIU = 1;
        public static final int RAN_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RAN_ID_MOVI_CHIU = 5;
        public static final int RAN_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RAN_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
