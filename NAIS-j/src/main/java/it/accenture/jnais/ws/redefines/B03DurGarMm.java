package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DUR-GAR-MM<br>
 * Variable: B03-DUR-GAR-MM from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DurGarMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DurGarMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DUR_GAR_MM;
    }

    public void setB03DurGarMm(int b03DurGarMm) {
        writeIntAsPacked(Pos.B03_DUR_GAR_MM, b03DurGarMm, Len.Int.B03_DUR_GAR_MM);
    }

    public void setB03DurGarMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DUR_GAR_MM, Pos.B03_DUR_GAR_MM);
    }

    /**Original name: B03-DUR-GAR-MM<br>*/
    public int getB03DurGarMm() {
        return readPackedAsInt(Pos.B03_DUR_GAR_MM, Len.Int.B03_DUR_GAR_MM);
    }

    public byte[] getB03DurGarMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DUR_GAR_MM, Pos.B03_DUR_GAR_MM);
        return buffer;
    }

    public void setB03DurGarMmNull(String b03DurGarMmNull) {
        writeString(Pos.B03_DUR_GAR_MM_NULL, b03DurGarMmNull, Len.B03_DUR_GAR_MM_NULL);
    }

    /**Original name: B03-DUR-GAR-MM-NULL<br>*/
    public String getB03DurGarMmNull() {
        return readString(Pos.B03_DUR_GAR_MM_NULL, Len.B03_DUR_GAR_MM_NULL);
    }

    public String getB03DurGarMmNullFormatted() {
        return Functions.padBlanks(getB03DurGarMmNull(), Len.B03_DUR_GAR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DUR_GAR_MM = 1;
        public static final int B03_DUR_GAR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DUR_GAR_MM = 3;
        public static final int B03_DUR_GAR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DUR_GAR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
