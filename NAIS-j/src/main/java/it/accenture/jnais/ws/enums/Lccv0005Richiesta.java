package it.accenture.jnais.ws.enums;

/**Original name: LCCV0005-RICHIESTA<br>
 * Variable: LCCV0005-RICHIESTA from copybook LCCV0005<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccv0005Richiesta {

    //==== PROPERTIES ====
    private char value = 'N';
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setRichiesta(char richiesta) {
        this.value = richiesta;
    }

    public char getRichiesta() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setLccv0005RichiestaNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RICHIESTA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
