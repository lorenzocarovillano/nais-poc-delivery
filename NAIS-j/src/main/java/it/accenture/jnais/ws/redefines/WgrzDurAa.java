package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-DUR-AA<br>
 * Variable: WGRZ-DUR-AA from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzDurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzDurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_DUR_AA;
    }

    public void setWgrzDurAa(int wgrzDurAa) {
        writeIntAsPacked(Pos.WGRZ_DUR_AA, wgrzDurAa, Len.Int.WGRZ_DUR_AA);
    }

    public void setWgrzDurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_DUR_AA, Pos.WGRZ_DUR_AA);
    }

    /**Original name: WGRZ-DUR-AA<br>*/
    public int getWgrzDurAa() {
        return readPackedAsInt(Pos.WGRZ_DUR_AA, Len.Int.WGRZ_DUR_AA);
    }

    public byte[] getWgrzDurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_DUR_AA, Pos.WGRZ_DUR_AA);
        return buffer;
    }

    public void initWgrzDurAaSpaces() {
        fill(Pos.WGRZ_DUR_AA, Len.WGRZ_DUR_AA, Types.SPACE_CHAR);
    }

    public void setWgrzDurAaNull(String wgrzDurAaNull) {
        writeString(Pos.WGRZ_DUR_AA_NULL, wgrzDurAaNull, Len.WGRZ_DUR_AA_NULL);
    }

    /**Original name: WGRZ-DUR-AA-NULL<br>*/
    public String getWgrzDurAaNull() {
        return readString(Pos.WGRZ_DUR_AA_NULL, Len.WGRZ_DUR_AA_NULL);
    }

    public String getWgrzDurAaNullFormatted() {
        return Functions.padBlanks(getWgrzDurAaNull(), Len.WGRZ_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_DUR_AA = 1;
        public static final int WGRZ_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_DUR_AA = 3;
        public static final int WGRZ_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
