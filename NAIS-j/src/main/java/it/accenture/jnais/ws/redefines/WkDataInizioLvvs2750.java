package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WK-DATA-INIZIO<br>
 * Variable: WK-DATA-INIZIO from program LVVS2750<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkDataInizioLvvs2750 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkDataInizioLvvs2750() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_DATA_INIZIO;
    }

    public void setWkDataInizioFormatted(String data) {
        writeString(Pos.WK_DATA_INIZIO, data, Len.WK_DATA_INIZIO);
    }

    public void setWkDataInizioD(int wkDataInizioD) {
        writeIntAsPacked(Pos.WK_DATA_INIZIO_D, wkDataInizioD, Len.Int.WK_DATA_INIZIO_D);
    }

    public void setWkDataInizioDFormatted(String wkDataInizioD) {
        setWkDataInizioD(PicParser.display(new PicParams("S9(8)V").setUsage(PicUsage.PACKED)).parseInt(wkDataInizioD));
    }

    /**Original name: WK-DATA-INIZIO-D<br>*/
    public int getWkDataInizioD() {
        return readPackedAsInt(Pos.WK_DATA_INIZIO_D, Len.Int.WK_DATA_INIZIO_D);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_DATA_INIZIO = 1;
        public static final int ANNO_INI = WK_DATA_INIZIO;
        public static final int MESE_INI = ANNO_INI + Len.ANNO_INI;
        public static final int GG_INI = MESE_INI + Len.MESE_INI;
        public static final int WK_DATA_INIZIO_D = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ANNO_INI = 4;
        public static final int MESE_INI = 2;
        public static final int GG_INI = 2;
        public static final int WK_DATA_INIZIO = ANNO_INI + MESE_INI + GG_INI;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_DATA_INIZIO_D = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
