package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-ACQ<br>
 * Variable: RST-RIS-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_ACQ;
    }

    public void setRstRisAcq(AfDecimal rstRisAcq) {
        writeDecimalAsPacked(Pos.RST_RIS_ACQ, rstRisAcq.copy());
    }

    public void setRstRisAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_ACQ, Pos.RST_RIS_ACQ);
    }

    /**Original name: RST-RIS-ACQ<br>*/
    public AfDecimal getRstRisAcq() {
        return readPackedAsDecimal(Pos.RST_RIS_ACQ, Len.Int.RST_RIS_ACQ, Len.Fract.RST_RIS_ACQ);
    }

    public byte[] getRstRisAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_ACQ, Pos.RST_RIS_ACQ);
        return buffer;
    }

    public void setRstRisAcqNull(String rstRisAcqNull) {
        writeString(Pos.RST_RIS_ACQ_NULL, rstRisAcqNull, Len.RST_RIS_ACQ_NULL);
    }

    /**Original name: RST-RIS-ACQ-NULL<br>*/
    public String getRstRisAcqNull() {
        return readString(Pos.RST_RIS_ACQ_NULL, Len.RST_RIS_ACQ_NULL);
    }

    public String getRstRisAcqNullFormatted() {
        return Functions.padBlanks(getRstRisAcqNull(), Len.RST_RIS_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_ACQ = 1;
        public static final int RST_RIS_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_ACQ = 8;
        public static final int RST_RIS_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
