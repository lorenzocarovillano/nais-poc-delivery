package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idbvq042;
import it.accenture.jnais.copy.Idbvq043;
import it.accenture.jnais.copy.Idsv0010;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS4990<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs4990Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IDBVQ042
    private Idbvq042 idbvq042 = new Idbvq042();
    //Original name: IDBVQ043
    private Idbvq043 idbvq043 = new Idbvq043();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idbvq042 getIdbvq042() {
        return idbvq042;
    }

    public Idbvq043 getIdbvq043() {
        return idbvq043;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
