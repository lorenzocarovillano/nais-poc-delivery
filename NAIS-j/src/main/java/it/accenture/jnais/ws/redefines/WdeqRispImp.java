package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDEQ-RISP-IMP<br>
 * Variable: WDEQ-RISP-IMP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdeqRispImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdeqRispImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDEQ_RISP_IMP;
    }

    public void setWdeqRispImp(AfDecimal wdeqRispImp) {
        writeDecimalAsPacked(Pos.WDEQ_RISP_IMP, wdeqRispImp.copy());
    }

    public void setWdeqRispImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDEQ_RISP_IMP, Pos.WDEQ_RISP_IMP);
    }

    /**Original name: WDEQ-RISP-IMP<br>*/
    public AfDecimal getWdeqRispImp() {
        return readPackedAsDecimal(Pos.WDEQ_RISP_IMP, Len.Int.WDEQ_RISP_IMP, Len.Fract.WDEQ_RISP_IMP);
    }

    public byte[] getWdeqRispImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDEQ_RISP_IMP, Pos.WDEQ_RISP_IMP);
        return buffer;
    }

    public void initWdeqRispImpSpaces() {
        fill(Pos.WDEQ_RISP_IMP, Len.WDEQ_RISP_IMP, Types.SPACE_CHAR);
    }

    public void setWdeqRispImpNull(String wdeqRispImpNull) {
        writeString(Pos.WDEQ_RISP_IMP_NULL, wdeqRispImpNull, Len.WDEQ_RISP_IMP_NULL);
    }

    /**Original name: WDEQ-RISP-IMP-NULL<br>*/
    public String getWdeqRispImpNull() {
        return readString(Pos.WDEQ_RISP_IMP_NULL, Len.WDEQ_RISP_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDEQ_RISP_IMP = 1;
        public static final int WDEQ_RISP_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDEQ_RISP_IMP = 8;
        public static final int WDEQ_RISP_IMP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDEQ_RISP_IMP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDEQ_RISP_IMP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
