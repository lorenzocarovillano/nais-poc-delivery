package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-IMP-VOLO<br>
 * Variable: ADE-IMP-VOLO from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_IMP_VOLO;
    }

    public void setAdeImpVolo(AfDecimal adeImpVolo) {
        writeDecimalAsPacked(Pos.ADE_IMP_VOLO, adeImpVolo.copy());
    }

    public void setAdeImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_IMP_VOLO, Pos.ADE_IMP_VOLO);
    }

    /**Original name: ADE-IMP-VOLO<br>*/
    public AfDecimal getAdeImpVolo() {
        return readPackedAsDecimal(Pos.ADE_IMP_VOLO, Len.Int.ADE_IMP_VOLO, Len.Fract.ADE_IMP_VOLO);
    }

    public byte[] getAdeImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_IMP_VOLO, Pos.ADE_IMP_VOLO);
        return buffer;
    }

    public void setAdeImpVoloNull(String adeImpVoloNull) {
        writeString(Pos.ADE_IMP_VOLO_NULL, adeImpVoloNull, Len.ADE_IMP_VOLO_NULL);
    }

    /**Original name: ADE-IMP-VOLO-NULL<br>*/
    public String getAdeImpVoloNull() {
        return readString(Pos.ADE_IMP_VOLO_NULL, Len.ADE_IMP_VOLO_NULL);
    }

    public String getAdeImpVoloNullFormatted() {
        return Functions.padBlanks(getAdeImpVoloNull(), Len.ADE_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_IMP_VOLO = 1;
        public static final int ADE_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_IMP_VOLO = 8;
        public static final int ADE_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
