package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDCO-ETA-SCAD-MASC-DFLT<br>
 * Variable: WDCO-ETA-SCAD-MASC-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdcoEtaScadMascDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdcoEtaScadMascDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_ETA_SCAD_MASC_DFLT;
    }

    public void setWdcoEtaScadMascDflt(int wdcoEtaScadMascDflt) {
        writeIntAsPacked(Pos.WDCO_ETA_SCAD_MASC_DFLT, wdcoEtaScadMascDflt, Len.Int.WDCO_ETA_SCAD_MASC_DFLT);
    }

    public void setWdcoEtaScadMascDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDCO_ETA_SCAD_MASC_DFLT, Pos.WDCO_ETA_SCAD_MASC_DFLT);
    }

    /**Original name: WDCO-ETA-SCAD-MASC-DFLT<br>*/
    public int getWdcoEtaScadMascDflt() {
        return readPackedAsInt(Pos.WDCO_ETA_SCAD_MASC_DFLT, Len.Int.WDCO_ETA_SCAD_MASC_DFLT);
    }

    public byte[] getWdcoEtaScadMascDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDCO_ETA_SCAD_MASC_DFLT, Pos.WDCO_ETA_SCAD_MASC_DFLT);
        return buffer;
    }

    public void setWdcoEtaScadMascDfltNull(String wdcoEtaScadMascDfltNull) {
        writeString(Pos.WDCO_ETA_SCAD_MASC_DFLT_NULL, wdcoEtaScadMascDfltNull, Len.WDCO_ETA_SCAD_MASC_DFLT_NULL);
    }

    /**Original name: WDCO-ETA-SCAD-MASC-DFLT-NULL<br>*/
    public String getWdcoEtaScadMascDfltNull() {
        return readString(Pos.WDCO_ETA_SCAD_MASC_DFLT_NULL, Len.WDCO_ETA_SCAD_MASC_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDCO_ETA_SCAD_MASC_DFLT = 1;
        public static final int WDCO_ETA_SCAD_MASC_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_ETA_SCAD_MASC_DFLT = 3;
        public static final int WDCO_ETA_SCAD_MASC_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_ETA_SCAD_MASC_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
