package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RRE-COD-CAN<br>
 * Variable: RRE-COD-CAN from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RreCodCan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RreCodCan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RRE_COD_CAN;
    }

    public void setRreCodCan(int rreCodCan) {
        writeIntAsPacked(Pos.RRE_COD_CAN, rreCodCan, Len.Int.RRE_COD_CAN);
    }

    public void setRreCodCanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RRE_COD_CAN, Pos.RRE_COD_CAN);
    }

    /**Original name: RRE-COD-CAN<br>*/
    public int getRreCodCan() {
        return readPackedAsInt(Pos.RRE_COD_CAN, Len.Int.RRE_COD_CAN);
    }

    public byte[] getRreCodCanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RRE_COD_CAN, Pos.RRE_COD_CAN);
        return buffer;
    }

    public void setRreCodCanNull(String rreCodCanNull) {
        writeString(Pos.RRE_COD_CAN_NULL, rreCodCanNull, Len.RRE_COD_CAN_NULL);
    }

    /**Original name: RRE-COD-CAN-NULL<br>*/
    public String getRreCodCanNull() {
        return readString(Pos.RRE_COD_CAN_NULL, Len.RRE_COD_CAN_NULL);
    }

    public String getRreCodCanNullFormatted() {
        return Functions.padBlanks(getRreCodCanNull(), Len.RRE_COD_CAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RRE_COD_CAN = 1;
        public static final int RRE_COD_CAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RRE_COD_CAN = 3;
        public static final int RRE_COD_CAN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RRE_COD_CAN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
