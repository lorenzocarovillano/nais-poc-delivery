package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-IMPST-SOST-IPT<br>
 * Variable: BEL-IMPST-SOST-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelImpstSostIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelImpstSostIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_IMPST_SOST_IPT;
    }

    public void setBelImpstSostIpt(AfDecimal belImpstSostIpt) {
        writeDecimalAsPacked(Pos.BEL_IMPST_SOST_IPT, belImpstSostIpt.copy());
    }

    public void setBelImpstSostIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_IMPST_SOST_IPT, Pos.BEL_IMPST_SOST_IPT);
    }

    /**Original name: BEL-IMPST-SOST-IPT<br>*/
    public AfDecimal getBelImpstSostIpt() {
        return readPackedAsDecimal(Pos.BEL_IMPST_SOST_IPT, Len.Int.BEL_IMPST_SOST_IPT, Len.Fract.BEL_IMPST_SOST_IPT);
    }

    public byte[] getBelImpstSostIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_IMPST_SOST_IPT, Pos.BEL_IMPST_SOST_IPT);
        return buffer;
    }

    public void setBelImpstSostIptNull(String belImpstSostIptNull) {
        writeString(Pos.BEL_IMPST_SOST_IPT_NULL, belImpstSostIptNull, Len.BEL_IMPST_SOST_IPT_NULL);
    }

    /**Original name: BEL-IMPST-SOST-IPT-NULL<br>*/
    public String getBelImpstSostIptNull() {
        return readString(Pos.BEL_IMPST_SOST_IPT_NULL, Len.BEL_IMPST_SOST_IPT_NULL);
    }

    public String getBelImpstSostIptNullFormatted() {
        return Functions.padBlanks(getBelImpstSostIptNull(), Len.BEL_IMPST_SOST_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_SOST_IPT = 1;
        public static final int BEL_IMPST_SOST_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_SOST_IPT = 8;
        public static final int BEL_IMPST_SOST_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_SOST_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_SOST_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
