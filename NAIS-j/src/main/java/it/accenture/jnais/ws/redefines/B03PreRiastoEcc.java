package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRE-RIASTO-ECC<br>
 * Variable: B03-PRE-RIASTO-ECC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PreRiastoEcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PreRiastoEcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRE_RIASTO_ECC;
    }

    public void setB03PreRiastoEcc(AfDecimal b03PreRiastoEcc) {
        writeDecimalAsPacked(Pos.B03_PRE_RIASTO_ECC, b03PreRiastoEcc.copy());
    }

    public void setB03PreRiastoEccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRE_RIASTO_ECC, Pos.B03_PRE_RIASTO_ECC);
    }

    /**Original name: B03-PRE-RIASTO-ECC<br>*/
    public AfDecimal getB03PreRiastoEcc() {
        return readPackedAsDecimal(Pos.B03_PRE_RIASTO_ECC, Len.Int.B03_PRE_RIASTO_ECC, Len.Fract.B03_PRE_RIASTO_ECC);
    }

    public byte[] getB03PreRiastoEccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRE_RIASTO_ECC, Pos.B03_PRE_RIASTO_ECC);
        return buffer;
    }

    public void setB03PreRiastoEccNull(String b03PreRiastoEccNull) {
        writeString(Pos.B03_PRE_RIASTO_ECC_NULL, b03PreRiastoEccNull, Len.B03_PRE_RIASTO_ECC_NULL);
    }

    /**Original name: B03-PRE-RIASTO-ECC-NULL<br>*/
    public String getB03PreRiastoEccNull() {
        return readString(Pos.B03_PRE_RIASTO_ECC_NULL, Len.B03_PRE_RIASTO_ECC_NULL);
    }

    public String getB03PreRiastoEccNullFormatted() {
        return Functions.padBlanks(getB03PreRiastoEccNull(), Len.B03_PRE_RIASTO_ECC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRE_RIASTO_ECC = 1;
        public static final int B03_PRE_RIASTO_ECC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRE_RIASTO_ECC = 8;
        public static final int B03_PRE_RIASTO_ECC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRE_RIASTO_ECC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRE_RIASTO_ECC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
