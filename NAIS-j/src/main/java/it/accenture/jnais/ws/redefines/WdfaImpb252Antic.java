package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPB-252-ANTIC<br>
 * Variable: WDFA-IMPB-252-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpb252Antic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpb252Antic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPB252_ANTIC;
    }

    public void setWdfaImpb252Antic(AfDecimal wdfaImpb252Antic) {
        writeDecimalAsPacked(Pos.WDFA_IMPB252_ANTIC, wdfaImpb252Antic.copy());
    }

    public void setWdfaImpb252AnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPB252_ANTIC, Pos.WDFA_IMPB252_ANTIC);
    }

    /**Original name: WDFA-IMPB-252-ANTIC<br>*/
    public AfDecimal getWdfaImpb252Antic() {
        return readPackedAsDecimal(Pos.WDFA_IMPB252_ANTIC, Len.Int.WDFA_IMPB252_ANTIC, Len.Fract.WDFA_IMPB252_ANTIC);
    }

    public byte[] getWdfaImpb252AnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPB252_ANTIC, Pos.WDFA_IMPB252_ANTIC);
        return buffer;
    }

    public void setWdfaImpb252AnticNull(String wdfaImpb252AnticNull) {
        writeString(Pos.WDFA_IMPB252_ANTIC_NULL, wdfaImpb252AnticNull, Len.WDFA_IMPB252_ANTIC_NULL);
    }

    /**Original name: WDFA-IMPB-252-ANTIC-NULL<br>*/
    public String getWdfaImpb252AnticNull() {
        return readString(Pos.WDFA_IMPB252_ANTIC_NULL, Len.WDFA_IMPB252_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB252_ANTIC = 1;
        public static final int WDFA_IMPB252_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB252_ANTIC = 8;
        public static final int WDFA_IMPB252_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB252_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB252_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
