package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-PRE-PP-IAS<br>
 * Variable: WTIT-TOT-PRE-PP-IAS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotPrePpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotPrePpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_PRE_PP_IAS;
    }

    public void setWtitTotPrePpIas(AfDecimal wtitTotPrePpIas) {
        writeDecimalAsPacked(Pos.WTIT_TOT_PRE_PP_IAS, wtitTotPrePpIas.copy());
    }

    public void setWtitTotPrePpIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_PRE_PP_IAS, Pos.WTIT_TOT_PRE_PP_IAS);
    }

    /**Original name: WTIT-TOT-PRE-PP-IAS<br>*/
    public AfDecimal getWtitTotPrePpIas() {
        return readPackedAsDecimal(Pos.WTIT_TOT_PRE_PP_IAS, Len.Int.WTIT_TOT_PRE_PP_IAS, Len.Fract.WTIT_TOT_PRE_PP_IAS);
    }

    public byte[] getWtitTotPrePpIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_PRE_PP_IAS, Pos.WTIT_TOT_PRE_PP_IAS);
        return buffer;
    }

    public void initWtitTotPrePpIasSpaces() {
        fill(Pos.WTIT_TOT_PRE_PP_IAS, Len.WTIT_TOT_PRE_PP_IAS, Types.SPACE_CHAR);
    }

    public void setWtitTotPrePpIasNull(String wtitTotPrePpIasNull) {
        writeString(Pos.WTIT_TOT_PRE_PP_IAS_NULL, wtitTotPrePpIasNull, Len.WTIT_TOT_PRE_PP_IAS_NULL);
    }

    /**Original name: WTIT-TOT-PRE-PP-IAS-NULL<br>*/
    public String getWtitTotPrePpIasNull() {
        return readString(Pos.WTIT_TOT_PRE_PP_IAS_NULL, Len.WTIT_TOT_PRE_PP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PRE_PP_IAS = 1;
        public static final int WTIT_TOT_PRE_PP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PRE_PP_IAS = 8;
        public static final int WTIT_TOT_PRE_PP_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PRE_PP_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PRE_PP_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
