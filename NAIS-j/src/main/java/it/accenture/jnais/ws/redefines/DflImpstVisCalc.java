package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-VIS-CALC<br>
 * Variable: DFL-IMPST-VIS-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstVisCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstVisCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_VIS_CALC;
    }

    public void setDflImpstVisCalc(AfDecimal dflImpstVisCalc) {
        writeDecimalAsPacked(Pos.DFL_IMPST_VIS_CALC, dflImpstVisCalc.copy());
    }

    public void setDflImpstVisCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_VIS_CALC, Pos.DFL_IMPST_VIS_CALC);
    }

    /**Original name: DFL-IMPST-VIS-CALC<br>*/
    public AfDecimal getDflImpstVisCalc() {
        return readPackedAsDecimal(Pos.DFL_IMPST_VIS_CALC, Len.Int.DFL_IMPST_VIS_CALC, Len.Fract.DFL_IMPST_VIS_CALC);
    }

    public byte[] getDflImpstVisCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_VIS_CALC, Pos.DFL_IMPST_VIS_CALC);
        return buffer;
    }

    public void setDflImpstVisCalcNull(String dflImpstVisCalcNull) {
        writeString(Pos.DFL_IMPST_VIS_CALC_NULL, dflImpstVisCalcNull, Len.DFL_IMPST_VIS_CALC_NULL);
    }

    /**Original name: DFL-IMPST-VIS-CALC-NULL<br>*/
    public String getDflImpstVisCalcNull() {
        return readString(Pos.DFL_IMPST_VIS_CALC_NULL, Len.DFL_IMPST_VIS_CALC_NULL);
    }

    public String getDflImpstVisCalcNullFormatted() {
        return Functions.padBlanks(getDflImpstVisCalcNull(), Len.DFL_IMPST_VIS_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS_CALC = 1;
        public static final int DFL_IMPST_VIS_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS_CALC = 8;
        public static final int DFL_IMPST_VIS_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
