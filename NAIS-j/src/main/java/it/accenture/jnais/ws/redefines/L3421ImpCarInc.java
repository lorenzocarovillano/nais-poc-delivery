package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-CAR-INC<br>
 * Variable: L3421-IMP-CAR-INC from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpCarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpCarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_CAR_INC;
    }

    public void setL3421ImpCarIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_CAR_INC, Pos.L3421_IMP_CAR_INC);
    }

    /**Original name: L3421-IMP-CAR-INC<br>*/
    public AfDecimal getL3421ImpCarInc() {
        return readPackedAsDecimal(Pos.L3421_IMP_CAR_INC, Len.Int.L3421_IMP_CAR_INC, Len.Fract.L3421_IMP_CAR_INC);
    }

    public byte[] getL3421ImpCarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_CAR_INC, Pos.L3421_IMP_CAR_INC);
        return buffer;
    }

    /**Original name: L3421-IMP-CAR-INC-NULL<br>*/
    public String getL3421ImpCarIncNull() {
        return readString(Pos.L3421_IMP_CAR_INC_NULL, Len.L3421_IMP_CAR_INC_NULL);
    }

    public String getL3421ImpCarIncNullFormatted() {
        return Functions.padBlanks(getL3421ImpCarIncNull(), Len.L3421_IMP_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_CAR_INC = 1;
        public static final int L3421_IMP_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_CAR_INC = 8;
        public static final int L3421_IMP_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
