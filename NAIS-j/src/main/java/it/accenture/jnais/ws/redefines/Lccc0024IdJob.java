package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0024-ID-JOB<br>
 * Variable: LCCC0024-ID-JOB from program LCCS0024<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Lccc0024IdJob extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Lccc0024IdJob() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0024_ID_JOB;
    }

    public void setLccc0024IdJob(int lccc0024IdJob) {
        writeIntAsPacked(Pos.LCCC0024_ID_JOB, lccc0024IdJob, Len.Int.LCCC0024_ID_JOB);
    }

    public void setLccc0024IdJobFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LCCC0024_ID_JOB, Pos.LCCC0024_ID_JOB);
    }

    /**Original name: LCCC0024-ID-JOB<br>*/
    public int getLccc0024IdJob() {
        return readPackedAsInt(Pos.LCCC0024_ID_JOB, Len.Int.LCCC0024_ID_JOB);
    }

    public byte[] getLccc0024IdJobAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LCCC0024_ID_JOB, Pos.LCCC0024_ID_JOB);
        return buffer;
    }

    public void initLccc0024IdJobSpaces() {
        fill(Pos.LCCC0024_ID_JOB, Len.LCCC0024_ID_JOB, Types.SPACE_CHAR);
    }

    public void setLccc0024IdJobNull(String lccc0024IdJobNull) {
        writeString(Pos.LCCC0024_ID_JOB_NULL, lccc0024IdJobNull, Len.LCCC0024_ID_JOB_NULL);
    }

    /**Original name: LCCC0024-ID-JOB-NULL<br>*/
    public String getLccc0024IdJobNull() {
        return readString(Pos.LCCC0024_ID_JOB_NULL, Len.LCCC0024_ID_JOB_NULL);
    }

    public String getLccc0024IdJobNullFormatted() {
        return Functions.padBlanks(getLccc0024IdJobNull(), Len.LCCC0024_ID_JOB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LCCC0024_ID_JOB = 1;
        public static final int LCCC0024_ID_JOB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0024_ID_JOB = 5;
        public static final int LCCC0024_ID_JOB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LCCC0024_ID_JOB = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
