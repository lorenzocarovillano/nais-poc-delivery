package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WSKD-TAB-VAL-T<br>
 * Variable: WSKD-TAB-VAL-T from program LOAS0800<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WskdTabValT extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_VARIABILI_T_MAXOCCURS = 75;
    public static final int TAB_LIVELLO_T_MAXOCCURS = 750;
    public static final String WSKD_FLG_ITN_POS = "0";
    public static final String WSKD_FLG_ITN_NEG = "1";

    //==== CONSTRUCTORS ====
    public WskdTabValT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSKD_TAB_VAL_T;
    }

    public String getWskdTabValTFormatted() {
        return readFixedString(Pos.WSKD_TAB_VAL_T, Len.WSKD_TAB_VAL_T);
    }

    public void setWskdTabValTBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSKD_TAB_VAL_T, Pos.WSKD_TAB_VAL_T);
    }

    public byte[] getWskdTabValTBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSKD_TAB_VAL_T, Pos.WSKD_TAB_VAL_T);
        return buffer;
    }

    public void setWskdIdGarTFormatted(int wskdIdGarTIdx, String wskdIdGarT) {
        int position = Pos.wskdIdGarT(wskdIdGarTIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(wskdIdGarT, Len.ID_GAR_T), Len.ID_GAR_T);
    }

    /**Original name: WSKD-ID-GAR-T<br>*/
    public int getWskdIdGarT(int wskdIdGarTIdx) {
        int position = Pos.wskdIdGarT(wskdIdGarTIdx - 1);
        return readNumDispUnsignedInt(position, Len.ID_GAR_T);
    }

    public void setWskdCodTipoOpzioneT(int wskdCodTipoOpzioneTIdx, String wskdCodTipoOpzioneT) {
        int position = Pos.wskdCodTipoOpzioneT(wskdCodTipoOpzioneTIdx - 1);
        writeString(position, wskdCodTipoOpzioneT, Len.COD_TIPO_OPZIONE_T);
    }

    /**Original name: WSKD-COD-TIPO-OPZIONE-T<br>*/
    public String getCodTipoOpzioneT(int codTipoOpzioneTIdx) {
        int position = Pos.wskdCodTipoOpzioneT(codTipoOpzioneTIdx - 1);
        return readString(position, Len.COD_TIPO_OPZIONE_T);
    }

    public void setWskdTpLivelloT(int wskdTpLivelloTIdx, char wskdTpLivelloT) {
        int position = Pos.wskdTpLivelloT(wskdTpLivelloTIdx - 1);
        writeChar(position, wskdTpLivelloT);
    }

    /**Original name: WSKD-TP-LIVELLO-T<br>*/
    public char getTpLivelloT(int tpLivelloTIdx) {
        int position = Pos.wskdTpLivelloT(tpLivelloTIdx - 1);
        return readChar(position);
    }

    public void setWskdCodLivelloT(int wskdCodLivelloTIdx, String wskdCodLivelloT) {
        int position = Pos.wskdCodLivelloT(wskdCodLivelloTIdx - 1);
        writeString(position, wskdCodLivelloT, Len.COD_LIVELLO_T);
    }

    /**Original name: WSKD-COD-LIVELLO-T<br>*/
    public String getCodLivelloT(int codLivelloTIdx) {
        int position = Pos.wskdCodLivelloT(codLivelloTIdx - 1);
        return readString(position, Len.COD_LIVELLO_T);
    }

    public void setWskdIdLivelloTFormatted(int wskdIdLivelloTIdx, String wskdIdLivelloT) {
        int position = Pos.wskdIdLivelloT(wskdIdLivelloTIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(wskdIdLivelloT, Len.ID_LIVELLO_T), Len.ID_LIVELLO_T);
    }

    /**Original name: WSKD-ID-LIVELLO-T<br>*/
    public int getWskdIdLivelloT(int wskdIdLivelloTIdx) {
        int position = Pos.wskdIdLivelloT(wskdIdLivelloTIdx - 1);
        return readNumDispUnsignedInt(position, Len.ID_LIVELLO_T);
    }

    public String getIdLivelloTFormatted(int idLivelloTIdx) {
        int position = Pos.wskdIdLivelloT(idLivelloTIdx - 1);
        return readFixedString(position, Len.ID_LIVELLO_T);
    }

    public void setWskdDtDecorTrchT(int wskdDtDecorTrchTIdx, String wskdDtDecorTrchT) {
        int position = Pos.wskdDtDecorTrchT(wskdDtDecorTrchTIdx - 1);
        writeString(position, wskdDtDecorTrchT, Len.DT_DECOR_TRCH_T);
    }

    /**Original name: WSKD-DT-DECOR-TRCH-T<br>*/
    public String getDtDecorTrchT(int dtDecorTrchTIdx) {
        int position = Pos.wskdDtDecorTrchT(dtDecorTrchTIdx - 1);
        return readString(position, Len.DT_DECOR_TRCH_T);
    }

    public String getWskdDtDecorTrchTFormatted(int wskdDtDecorTrchTIdx) {
        return Functions.padBlanks(getDtDecorTrchT(wskdDtDecorTrchTIdx), Len.DT_DECOR_TRCH_T);
    }

    public void setWskdDtInizTariT(int wskdDtInizTariTIdx, String wskdDtInizTariT) {
        int position = Pos.wskdDtInizTariT(wskdDtInizTariTIdx - 1);
        writeString(position, wskdDtInizTariT, Len.DT_INIZ_TARI_T);
    }

    /**Original name: WSKD-DT-INIZ-TARI-T<br>*/
    public String getDtInizTariT(int dtInizTariTIdx) {
        int position = Pos.wskdDtInizTariT(dtInizTariTIdx - 1);
        return readString(position, Len.DT_INIZ_TARI_T);
    }

    public String getWskdDtInizTariTFormatted(int wskdDtInizTariTIdx) {
        return Functions.padBlanks(getDtInizTariT(wskdDtInizTariTIdx), Len.DT_INIZ_TARI_T);
    }

    public void setWskdCodRgmFiscT(int wskdCodRgmFiscTIdx, String wskdCodRgmFiscT) {
        int position = Pos.wskdCodRgmFiscT(wskdCodRgmFiscTIdx - 1);
        writeString(position, wskdCodRgmFiscT, Len.COD_RGM_FISC_T);
    }

    /**Original name: WSKD-COD-RGM-FISC-T<br>*/
    public String getCodRgmFiscT(int codRgmFiscTIdx) {
        int position = Pos.wskdCodRgmFiscT(codRgmFiscTIdx - 1);
        return readString(position, Len.COD_RGM_FISC_T);
    }

    public String getCodRgmFiscTFormatted(int codRgmFiscTIdx) {
        return Functions.padBlanks(getCodRgmFiscT(codRgmFiscTIdx), Len.COD_RGM_FISC_T);
    }

    public void setWskdNomeServizioT(int wskdNomeServizioTIdx, String wskdNomeServizioT) {
        int position = Pos.wskdNomeServizioT(wskdNomeServizioTIdx - 1);
        writeString(position, wskdNomeServizioT, Len.NOME_SERVIZIO_T);
    }

    /**Original name: WSKD-NOME-SERVIZIO-T<br>*/
    public String getWskdNomeServizioT(int wskdNomeServizioTIdx) {
        int position = Pos.wskdNomeServizioT(wskdNomeServizioTIdx - 1);
        return readString(position, Len.NOME_SERVIZIO_T);
    }

    /**Original name: WSKD-TIPO-TRCH<br>*/
    public short getWskdTipoTrch(int wskdTipoTrchIdx) {
        int position = Pos.wskdTipoTrch(wskdTipoTrchIdx - 1);
        return readNumDispUnsignedShort(position, Len.TIPO_TRCH);
    }

    public String getTipoTrchFormatted(int tipoTrchIdx) {
        int position = Pos.wskdTipoTrch(tipoTrchIdx - 1);
        return readFixedString(position, Len.TIPO_TRCH);
    }

    /**Original name: WSKD-FLG-ITN<br>*/
    public short getWskdFlgItn(int wskdFlgItnIdx) {
        int position = Pos.wskdFlgItn(wskdFlgItnIdx - 1);
        return readNumDispUnsignedShort(position, Len.FLG_ITN);
    }

    public String getFlgItnFormatted(int flgItnIdx) {
        int position = Pos.wskdFlgItn(flgItnIdx - 1);
        return readFixedString(position, Len.FLG_ITN);
    }

    /**Original name: WSKD-AREA-VARIABILI-T<br>*/
    public byte[] getWskdAreaVariabiliTBytes(int wskdAreaVariabiliTIdx) {
        byte[] buffer = new byte[Len.AREA_VARIABILI_T];
        return getWskdAreaVariabiliTBytes(wskdAreaVariabiliTIdx, buffer, 1);
    }

    public byte[] getWskdAreaVariabiliTBytes(int wskdAreaVariabiliTIdx, byte[] buffer, int offset) {
        int position = Pos.wskdAreaVariabiliT(wskdAreaVariabiliTIdx - 1);
        getBytes(buffer, offset, Len.AREA_VARIABILI_T, position);
        return buffer;
    }

    public void setWskdEleVariabiliMaxT(int wskdEleVariabiliMaxTIdx, short wskdEleVariabiliMaxT) {
        int position = Pos.wskdEleVariabiliMaxT(wskdEleVariabiliMaxTIdx - 1);
        writeShortAsPacked(position, wskdEleVariabiliMaxT, Len.Int.ELE_VARIABILI_MAX_T);
    }

    /**Original name: WSKD-ELE-VARIABILI-MAX-T<br>*/
    public short getEleVariabiliMaxT(int eleVariabiliMaxTIdx) {
        int position = Pos.wskdEleVariabiliMaxT(eleVariabiliMaxTIdx - 1);
        return readPackedAsShort(position, Len.Int.ELE_VARIABILI_MAX_T);
    }

    public void setWskdCodVariabileT(int wskdCodVariabileTIdx1, int wskdCodVariabileTIdx2, String wskdCodVariabileT) {
        int position = Pos.wskdCodVariabileT(wskdCodVariabileTIdx1 - 1, wskdCodVariabileTIdx2 - 1);
        writeString(position, wskdCodVariabileT, Len.COD_VARIABILE_T);
    }

    /**Original name: WSKD-COD-VARIABILE-T<br>*/
    public String getCodVariabileT(int codVariabileTIdx1, int codVariabileTIdx2) {
        int position = Pos.wskdCodVariabileT(codVariabileTIdx1 - 1, codVariabileTIdx2 - 1);
        return readString(position, Len.COD_VARIABILE_T);
    }

    public void setWskdTpDatoT(int wskdTpDatoTIdx1, int wskdTpDatoTIdx2, char wskdTpDatoT) {
        int position = Pos.wskdTpDatoT(wskdTpDatoTIdx1 - 1, wskdTpDatoTIdx2 - 1);
        writeChar(position, wskdTpDatoT);
    }

    public void setWskdTpDatoTFormatted(int wskdTpDatoTIdx1, int wskdTpDatoTIdx2, String wskdTpDatoT) {
        setWskdTpDatoT(wskdTpDatoTIdx1, wskdTpDatoTIdx2, Functions.charAt(wskdTpDatoT, Types.CHAR_SIZE));
    }

    /**Original name: WSKD-TP-DATO-T<br>*/
    public char getTpDatoT(int tpDatoTIdx1, int tpDatoTIdx2) {
        int position = Pos.wskdTpDatoT(tpDatoTIdx1 - 1, tpDatoTIdx2 - 1);
        return readChar(position);
    }

    public void setWskdValGenericoT(int wskdValGenericoTIdx1, int wskdValGenericoTIdx2, String wskdValGenericoT) {
        int position = Pos.wskdValGenericoT(wskdValGenericoTIdx1 - 1, wskdValGenericoTIdx2 - 1);
        writeString(position, wskdValGenericoT, Len.VAL_GENERICO_T);
    }

    /**Original name: WSKD-VAL-GENERICO-T<br>*/
    public String getValGenericoT(int valGenericoTIdx1, int valGenericoTIdx2) {
        int position = Pos.wskdValGenericoT(valGenericoTIdx1 - 1, valGenericoTIdx2 - 1);
        return readString(position, Len.VAL_GENERICO_T);
    }

    public void setWskdValImpT(int wskdValImpTIdx1, int wskdValImpTIdx2, AfDecimal wskdValImpT) {
        int position = Pos.wskdValImpT(wskdValImpTIdx1 - 1, wskdValImpTIdx2 - 1);
        writeDecimal(position, wskdValImpT.copy());
    }

    /**Original name: WSKD-VAL-IMP-T<br>*/
    public AfDecimal getWskdValImpT(int wskdValImpTIdx1, int wskdValImpTIdx2) {
        int position = Pos.wskdValImpT(wskdValImpTIdx1 - 1, wskdValImpTIdx2 - 1);
        return readDecimal(position, Len.Int.WSKD_VAL_IMP_T, Len.Fract.WSKD_VAL_IMP_T);
    }

    /**Original name: WSKD-VAL-PERC-T<br>*/
    public AfDecimal getWskdValPercT(int wskdValPercTIdx1, int wskdValPercTIdx2) {
        int position = Pos.wskdValPercT(wskdValPercTIdx1 - 1, wskdValPercTIdx2 - 1);
        return readDecimal(position, Len.Int.WSKD_VAL_PERC_T, Len.Fract.WSKD_VAL_PERC_T, SignType.NO_SIGN);
    }

    /**Original name: WSKD-VAL-STR-T<br>*/
    public String getWskdValStrT(int wskdValStrTIdx1, int wskdValStrTIdx2) {
        int position = Pos.wskdValStrT(wskdValStrTIdx1 - 1, wskdValStrTIdx2 - 1);
        return readString(position, Len.WSKD_VAL_STR_T);
    }

    public void setWskdRestoTabValT(String wskdRestoTabValT) {
        writeString(Pos.RESTO_TAB_VAL_T, wskdRestoTabValT, Len.WSKD_RESTO_TAB_VAL_T);
    }

    /**Original name: WSKD-RESTO-TAB-VAL-T<br>*/
    public String getWskdRestoTabValT() {
        return readString(Pos.RESTO_TAB_VAL_T, Len.WSKD_RESTO_TAB_VAL_T);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSKD_TAB_VAL_T = 1;
        public static final int WSKD_TAB_VAL_R_T = 1;
        public static final int FLR1 = WSKD_TAB_VAL_R_T;
        public static final int RESTO_TAB_VAL_T = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wskdTabLivelloT(int idx) {
            return WSKD_TAB_VAL_T + idx * Len.TAB_LIVELLO_T;
        }

        public static int wskdIdGarT(int idx) {
            return wskdTabLivelloT(idx);
        }

        public static int wskdDatiLivelloT(int idx) {
            return wskdIdGarT(idx) + Len.ID_GAR_T;
        }

        public static int wskdCodTipoOpzioneT(int idx) {
            return wskdDatiLivelloT(idx);
        }

        public static int wskdTpLivelloT(int idx) {
            return wskdCodTipoOpzioneT(idx) + Len.COD_TIPO_OPZIONE_T;
        }

        public static int wskdCodLivelloT(int idx) {
            return wskdTpLivelloT(idx) + Len.TP_LIVELLO_T;
        }

        public static int wskdIdLivelloT(int idx) {
            return wskdCodLivelloT(idx) + Len.COD_LIVELLO_T;
        }

        public static int wskdDtDecorTrchT(int idx) {
            return wskdIdLivelloT(idx) + Len.ID_LIVELLO_T;
        }

        public static int wskdDtInizTariT(int idx) {
            return wskdDtDecorTrchT(idx) + Len.DT_DECOR_TRCH_T;
        }

        public static int wskdCodRgmFiscT(int idx) {
            return wskdDtInizTariT(idx) + Len.DT_INIZ_TARI_T;
        }

        public static int wskdNomeServizioT(int idx) {
            return wskdCodRgmFiscT(idx) + Len.COD_RGM_FISC_T;
        }

        public static int wskdTipoTrch(int idx) {
            return wskdNomeServizioT(idx) + Len.NOME_SERVIZIO_T;
        }

        public static int wskdFlgItn(int idx) {
            return wskdTipoTrch(idx) + Len.TIPO_TRCH;
        }

        public static int wskdAreaVariabiliT(int idx) {
            return wskdFlgItn(idx) + Len.FLG_ITN;
        }

        public static int wskdEleVariabiliMaxT(int idx) {
            return wskdAreaVariabiliT(idx);
        }

        public static int wskdTabVariabiliT(int idx1, int idx2) {
            return wskdEleVariabiliMaxT(idx1) + Len.ELE_VARIABILI_MAX_T + idx2 * Len.TAB_VARIABILI_T;
        }

        public static int wskdAreaVariabileT(int idx1, int idx2) {
            return wskdTabVariabiliT(idx1, idx2);
        }

        public static int wskdCodVariabileT(int idx1, int idx2) {
            return wskdAreaVariabileT(idx1, idx2);
        }

        public static int wskdTpDatoT(int idx1, int idx2) {
            return wskdCodVariabileT(idx1, idx2) + Len.COD_VARIABILE_T;
        }

        public static int wskdValGenericoT(int idx1, int idx2) {
            return wskdTpDatoT(idx1, idx2) + Len.TP_DATO_T;
        }

        public static int wskdValImpGenT(int idx1, int idx2) {
            return wskdValGenericoT(idx1, idx2);
        }

        public static int wskdValImpT(int idx1, int idx2) {
            return wskdValImpGenT(idx1, idx2);
        }

        public static int wskdValImpFillerT(int idx1, int idx2) {
            return wskdValImpT(idx1, idx2) + Len.VAL_IMP_T;
        }

        public static int wskdValPercGenT(int idx1, int idx2) {
            return wskdValGenericoT(idx1, idx2);
        }

        public static int wskdValPercT(int idx1, int idx2) {
            return wskdValPercGenT(idx1, idx2);
        }

        public static int wskdValPercFillerT(int idx1, int idx2) {
            return wskdValPercT(idx1, idx2) + Len.VAL_PERC_T;
        }

        public static int wskdValStrGenT(int idx1, int idx2) {
            return wskdValGenericoT(idx1, idx2);
        }

        public static int wskdValStrT(int idx1, int idx2) {
            return wskdValStrGenT(idx1, idx2);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_GAR_T = 9;
        public static final int COD_TIPO_OPZIONE_T = 2;
        public static final int TP_LIVELLO_T = 1;
        public static final int COD_LIVELLO_T = 12;
        public static final int ID_LIVELLO_T = 9;
        public static final int DT_DECOR_TRCH_T = 8;
        public static final int DT_INIZ_TARI_T = 8;
        public static final int COD_RGM_FISC_T = 2;
        public static final int NOME_SERVIZIO_T = 8;
        public static final int TIPO_TRCH = 2;
        public static final int FLG_ITN = 1;
        public static final int ELE_VARIABILI_MAX_T = 3;
        public static final int COD_VARIABILE_T = 12;
        public static final int TP_DATO_T = 1;
        public static final int VAL_GENERICO_T = 60;
        public static final int AREA_VARIABILE_T = COD_VARIABILE_T + TP_DATO_T + VAL_GENERICO_T;
        public static final int TAB_VARIABILI_T = AREA_VARIABILE_T;
        public static final int AREA_VARIABILI_T = ELE_VARIABILI_MAX_T + WskdTabValT.TAB_VARIABILI_T_MAXOCCURS * TAB_VARIABILI_T;
        public static final int DATI_LIVELLO_T = COD_TIPO_OPZIONE_T + TP_LIVELLO_T + COD_LIVELLO_T + ID_LIVELLO_T + DT_DECOR_TRCH_T + DT_INIZ_TARI_T + COD_RGM_FISC_T + NOME_SERVIZIO_T + TIPO_TRCH + FLG_ITN + AREA_VARIABILI_T;
        public static final int TAB_LIVELLO_T = ID_GAR_T + DATI_LIVELLO_T;
        public static final int VAL_IMP_T = 18;
        public static final int VAL_PERC_T = 14;
        public static final int FLR1 = 5540;
        public static final int WSKD_TAB_VAL_T = WskdTabValT.TAB_LIVELLO_T_MAXOCCURS * TAB_LIVELLO_T;
        public static final int WSKD_RESTO_TAB_VAL_T = 4149460;
        public static final int WSKD_VAL_STR_T = 60;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ELE_VARIABILI_MAX_T = 4;
            public static final int WSKD_VAL_IMP_T = 11;
            public static final int WSKD_VAL_PERC_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WSKD_VAL_IMP_T = 7;
            public static final int WSKD_VAL_PERC_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
