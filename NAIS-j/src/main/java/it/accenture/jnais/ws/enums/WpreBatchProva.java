package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WPRE-BATCH-PROVA<br>
 * Variable: WPRE-BATCH-PROVA from copybook LOAC0560<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpreBatchProva {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setBatchProva(char batchProva) {
        this.value = batchProva;
    }

    public char getBatchProva() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BATCH_PROVA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
