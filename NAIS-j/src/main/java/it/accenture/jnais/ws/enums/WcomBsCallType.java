package it.accenture.jnais.ws.enums;

/**Original name: WCOM-BS-CALL-TYPE<br>
 * Variable: WCOM-BS-CALL-TYPE from copybook LCCC0261<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomBsCallType {

    //==== PROPERTIES ====
    private char value = 'S';
    public static final char STANDARD_CALL = 'S';
    public static final char ALPO_CALL = 'A';

    //==== METHODS ====
    public void setBsCallType(char bsCallType) {
        this.value = bsCallType;
    }

    public char getBsCallType() {
        return this.value;
    }

    public boolean isStandardCall() {
        return value == STANDARD_CALL;
    }

    public void setStandardCall() {
        value = STANDARD_CALL;
    }

    public boolean isAlpoCall() {
        return value == ALPO_CALL;
    }

    public void setAlpoCall() {
        value = ALPO_CALL;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BS_CALL_TYPE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
