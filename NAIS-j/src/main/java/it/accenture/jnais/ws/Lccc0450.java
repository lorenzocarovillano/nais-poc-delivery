package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.Lccc0450TabMatrice;

/**Original name: LCCC0450<br>
 * Variable: LCCC0450 from copybook LCCC0450<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Lccc0450 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LCCC0450-DATA-EFFETTO
    private String lccc0450DataEffetto = DefaultValues.stringVal(Len.LCCC0450_DATA_EFFETTO);
    //Original name: LCCC0450-DATA-COMPETENZA
    private String lccc0450DataCompetenza = DefaultValues.stringVal(Len.LCCC0450_DATA_COMPETENZA);
    //Original name: LCCC0450-DATA-RICORR
    private String lccc0450DataRicorr = DefaultValues.stringVal(Len.LCCC0450_DATA_RICORR);
    //Original name: LCCC0450-FLAG-OPER
    private String lccc0450FlagOper = DefaultValues.stringVal(Len.LCCC0450_FLAG_OPER);
    //Original name: LCCC0450-DATA-RISERVA
    private String lccc0450DataRiserva = DefaultValues.stringVal(Len.LCCC0450_DATA_RISERVA);
    //Original name: LCCC0450-ELE-TAB-MAX
    private short lccc0450EleTabMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCC0450-TAB-MATRICE
    private Lccc0450TabMatrice lccc0450TabMatrice = new Lccc0450TabMatrice();
    //Original name: LCCC0450-TOT-CONTRATTO
    private AfDecimal lccc0450TotContratto = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LCCC0450-TOT-IMP-INVES
    private AfDecimal lccc0450TotImpInves = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LCCC0450-TOT-IMP-DISINV
    private AfDecimal lccc0450TotImpDisinv = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0450;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLccc0450Bytes(buf);
    }

    public String getLccc0450Formatted() {
        return MarshalByteExt.bufferToStr(getLccc0450Bytes());
    }

    public void setLccc0450Bytes(byte[] buffer) {
        setLccc0450Bytes(buffer, 1);
    }

    public byte[] getLccc0450Bytes() {
        byte[] buffer = new byte[Len.LCCC0450];
        return getLccc0450Bytes(buffer, 1);
    }

    public void setLccc0450Bytes(byte[] buffer, int offset) {
        int position = offset;
        setLccc0450AreaInputBytes(buffer, position);
        position += Len.LCCC0450_AREA_INPUT;
        setLccc0450AreaOutputBytes(buffer, position);
    }

    public byte[] getLccc0450Bytes(byte[] buffer, int offset) {
        int position = offset;
        getLccc0450AreaInputBytes(buffer, position);
        position += Len.LCCC0450_AREA_INPUT;
        getLccc0450AreaOutputBytes(buffer, position);
        return buffer;
    }

    public void setLccc0450AreaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        lccc0450DataEffetto = MarshalByte.readFixedString(buffer, position, Len.LCCC0450_DATA_EFFETTO);
        position += Len.LCCC0450_DATA_EFFETTO;
        lccc0450DataCompetenza = MarshalByte.readFixedString(buffer, position, Len.LCCC0450_DATA_COMPETENZA);
        position += Len.LCCC0450_DATA_COMPETENZA;
        lccc0450DataRicorr = MarshalByte.readFixedString(buffer, position, Len.LCCC0450_DATA_RICORR);
        position += Len.LCCC0450_DATA_RICORR;
        lccc0450FlagOper = MarshalByte.readString(buffer, position, Len.LCCC0450_FLAG_OPER);
        position += Len.LCCC0450_FLAG_OPER;
        lccc0450DataRiserva = MarshalByte.readFixedString(buffer, position, Len.LCCC0450_DATA_RISERVA);
    }

    public byte[] getLccc0450AreaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, lccc0450DataEffetto, Len.LCCC0450_DATA_EFFETTO);
        position += Len.LCCC0450_DATA_EFFETTO;
        MarshalByte.writeString(buffer, position, lccc0450DataCompetenza, Len.LCCC0450_DATA_COMPETENZA);
        position += Len.LCCC0450_DATA_COMPETENZA;
        MarshalByte.writeString(buffer, position, lccc0450DataRicorr, Len.LCCC0450_DATA_RICORR);
        position += Len.LCCC0450_DATA_RICORR;
        MarshalByte.writeString(buffer, position, lccc0450FlagOper, Len.LCCC0450_FLAG_OPER);
        position += Len.LCCC0450_FLAG_OPER;
        MarshalByte.writeString(buffer, position, lccc0450DataRiserva, Len.LCCC0450_DATA_RISERVA);
        return buffer;
    }

    public void setLccc0450DataEffettoFormatted(String lccc0450DataEffetto) {
        this.lccc0450DataEffetto = Trunc.toUnsignedNumeric(lccc0450DataEffetto, Len.LCCC0450_DATA_EFFETTO);
    }

    public int getLccc0450DataEffetto() {
        return NumericDisplay.asInt(this.lccc0450DataEffetto);
    }

    public void setLccc0450DataCompetenza(long lccc0450DataCompetenza) {
        this.lccc0450DataCompetenza = NumericDisplay.asString(lccc0450DataCompetenza, Len.LCCC0450_DATA_COMPETENZA);
    }

    public long getLccc0450DataCompetenza() {
        return NumericDisplay.asLong(this.lccc0450DataCompetenza);
    }

    public void setLccc0450DataRicorr(int lccc0450DataRicorr) {
        this.lccc0450DataRicorr = NumericDisplay.asString(lccc0450DataRicorr, Len.LCCC0450_DATA_RICORR);
    }

    public int getLccc0450DataRicorr() {
        return NumericDisplay.asInt(this.lccc0450DataRicorr);
    }

    public void setLccc0450FlagOper(String lccc0450FlagOper) {
        this.lccc0450FlagOper = Functions.subString(lccc0450FlagOper, Len.LCCC0450_FLAG_OPER);
    }

    public String getLccc0450FlagOper() {
        return this.lccc0450FlagOper;
    }

    public String getLccc0450FlagOperFormatted() {
        return Functions.padBlanks(getLccc0450FlagOper(), Len.LCCC0450_FLAG_OPER);
    }

    public void setLccc0450DataRiservaFormatted(String lccc0450DataRiserva) {
        this.lccc0450DataRiserva = Trunc.toUnsignedNumeric(lccc0450DataRiserva, Len.LCCC0450_DATA_RISERVA);
    }

    public int getLccc0450DataRiserva() {
        return NumericDisplay.asInt(this.lccc0450DataRiserva);
    }

    public String getLccc0450DataRiservaFormatted() {
        return this.lccc0450DataRiserva;
    }

    public void setLccc0450AreaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        lccc0450EleTabMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        lccc0450TabMatrice.setLccc0450TabMatriceBytes(buffer, position);
        position += Lccc0450TabMatrice.Len.LCCC0450_TAB_MATRICE;
        lccc0450TotContratto.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LCCC0450_TOT_CONTRATTO, Len.Fract.LCCC0450_TOT_CONTRATTO));
        position += Len.LCCC0450_TOT_CONTRATTO;
        lccc0450TotImpInves.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LCCC0450_TOT_IMP_INVES, Len.Fract.LCCC0450_TOT_IMP_INVES));
        position += Len.LCCC0450_TOT_IMP_INVES;
        lccc0450TotImpDisinv.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LCCC0450_TOT_IMP_DISINV, Len.Fract.LCCC0450_TOT_IMP_DISINV));
    }

    public byte[] getLccc0450AreaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, lccc0450EleTabMax);
        position += Types.SHORT_SIZE;
        lccc0450TabMatrice.getLccc0450TabMatriceBytes(buffer, position);
        position += Lccc0450TabMatrice.Len.LCCC0450_TAB_MATRICE;
        MarshalByte.writeDecimalAsPacked(buffer, position, lccc0450TotContratto.copy());
        position += Len.LCCC0450_TOT_CONTRATTO;
        MarshalByte.writeDecimalAsPacked(buffer, position, lccc0450TotImpInves.copy());
        position += Len.LCCC0450_TOT_IMP_INVES;
        MarshalByte.writeDecimalAsPacked(buffer, position, lccc0450TotImpDisinv.copy());
        return buffer;
    }

    public void setLccc0450EleTabMax(short lccc0450EleTabMax) {
        this.lccc0450EleTabMax = lccc0450EleTabMax;
    }

    public short getLccc0450EleTabMax() {
        return this.lccc0450EleTabMax;
    }

    public void setLccc0450TotContratto(AfDecimal lccc0450TotContratto) {
        this.lccc0450TotContratto.assign(lccc0450TotContratto);
    }

    public AfDecimal getLccc0450TotContratto() {
        return this.lccc0450TotContratto.copy();
    }

    public void setLccc0450TotImpInves(AfDecimal lccc0450TotImpInves) {
        this.lccc0450TotImpInves.assign(lccc0450TotImpInves);
    }

    public AfDecimal getLccc0450TotImpInves() {
        return this.lccc0450TotImpInves.copy();
    }

    public void setLccc0450TotImpDisinv(AfDecimal lccc0450TotImpDisinv) {
        this.lccc0450TotImpDisinv.assign(lccc0450TotImpDisinv);
    }

    public AfDecimal getLccc0450TotImpDisinv() {
        return this.lccc0450TotImpDisinv.copy();
    }

    public Lccc0450TabMatrice getLccc0450TabMatrice() {
        return lccc0450TabMatrice;
    }

    @Override
    public byte[] serialize() {
        return getLccc0450Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0450_DATA_EFFETTO = 8;
        public static final int LCCC0450_DATA_COMPETENZA = 18;
        public static final int LCCC0450_DATA_RICORR = 8;
        public static final int LCCC0450_FLAG_OPER = 2;
        public static final int LCCC0450_DATA_RISERVA = 8;
        public static final int LCCC0450_AREA_INPUT = LCCC0450_DATA_EFFETTO + LCCC0450_DATA_COMPETENZA + LCCC0450_DATA_RICORR + LCCC0450_FLAG_OPER + LCCC0450_DATA_RISERVA;
        public static final int LCCC0450_ELE_TAB_MAX = 2;
        public static final int LCCC0450_TOT_CONTRATTO = 8;
        public static final int LCCC0450_TOT_IMP_INVES = 8;
        public static final int LCCC0450_TOT_IMP_DISINV = 8;
        public static final int LCCC0450_AREA_OUTPUT = LCCC0450_ELE_TAB_MAX + Lccc0450TabMatrice.Len.LCCC0450_TAB_MATRICE + LCCC0450_TOT_CONTRATTO + LCCC0450_TOT_IMP_INVES + LCCC0450_TOT_IMP_DISINV;
        public static final int LCCC0450 = LCCC0450_AREA_INPUT + LCCC0450_AREA_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LCCC0450_TOT_CONTRATTO = 12;
            public static final int LCCC0450_TOT_IMP_INVES = 12;
            public static final int LCCC0450_TOT_IMP_DISINV = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LCCC0450_TOT_CONTRATTO = 3;
            public static final int LCCC0450_TOT_IMP_INVES = 3;
            public static final int LCCC0450_TOT_IMP_DISINV = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
