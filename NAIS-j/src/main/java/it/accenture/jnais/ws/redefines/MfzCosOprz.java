package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: MFZ-COS-OPRZ<br>
 * Variable: MFZ-COS-OPRZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MfzCosOprz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MfzCosOprz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MFZ_COS_OPRZ;
    }

    public void setMfzCosOprz(AfDecimal mfzCosOprz) {
        writeDecimalAsPacked(Pos.MFZ_COS_OPRZ, mfzCosOprz.copy());
    }

    public void setMfzCosOprzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MFZ_COS_OPRZ, Pos.MFZ_COS_OPRZ);
    }

    /**Original name: MFZ-COS-OPRZ<br>*/
    public AfDecimal getMfzCosOprz() {
        return readPackedAsDecimal(Pos.MFZ_COS_OPRZ, Len.Int.MFZ_COS_OPRZ, Len.Fract.MFZ_COS_OPRZ);
    }

    public byte[] getMfzCosOprzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MFZ_COS_OPRZ, Pos.MFZ_COS_OPRZ);
        return buffer;
    }

    public void setMfzCosOprzNull(String mfzCosOprzNull) {
        writeString(Pos.MFZ_COS_OPRZ_NULL, mfzCosOprzNull, Len.MFZ_COS_OPRZ_NULL);
    }

    /**Original name: MFZ-COS-OPRZ-NULL<br>*/
    public String getMfzCosOprzNull() {
        return readString(Pos.MFZ_COS_OPRZ_NULL, Len.MFZ_COS_OPRZ_NULL);
    }

    public String getMfzCosOprzNullFormatted() {
        return Functions.padBlanks(getMfzCosOprzNull(), Len.MFZ_COS_OPRZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MFZ_COS_OPRZ = 1;
        public static final int MFZ_COS_OPRZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MFZ_COS_OPRZ = 8;
        public static final int MFZ_COS_OPRZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MFZ_COS_OPRZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int MFZ_COS_OPRZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
