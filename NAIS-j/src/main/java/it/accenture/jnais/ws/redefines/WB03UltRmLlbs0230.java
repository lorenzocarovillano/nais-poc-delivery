package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-ULT-RM<br>
 * Variable: W-B03-ULT-RM from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03UltRmLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03UltRmLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_ULT_RM;
    }

    public void setwB03UltRm(AfDecimal wB03UltRm) {
        writeDecimalAsPacked(Pos.W_B03_ULT_RM, wB03UltRm.copy());
    }

    /**Original name: W-B03-ULT-RM<br>*/
    public AfDecimal getwB03UltRm() {
        return readPackedAsDecimal(Pos.W_B03_ULT_RM, Len.Int.W_B03_ULT_RM, Len.Fract.W_B03_ULT_RM);
    }

    public byte[] getwB03UltRmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_ULT_RM, Pos.W_B03_ULT_RM);
        return buffer;
    }

    public void setwB03UltRmNull(String wB03UltRmNull) {
        writeString(Pos.W_B03_ULT_RM_NULL, wB03UltRmNull, Len.W_B03_ULT_RM_NULL);
    }

    /**Original name: W-B03-ULT-RM-NULL<br>*/
    public String getwB03UltRmNull() {
        return readString(Pos.W_B03_ULT_RM_NULL, Len.W_B03_ULT_RM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_ULT_RM = 1;
        public static final int W_B03_ULT_RM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_ULT_RM = 8;
        public static final int W_B03_ULT_RM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_ULT_RM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_ULT_RM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
