package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-VIS-END2000<br>
 * Variable: TGA-VIS-END2000 from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaVisEnd2000 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaVisEnd2000() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_VIS_END2000;
    }

    public void setTgaVisEnd2000(AfDecimal tgaVisEnd2000) {
        writeDecimalAsPacked(Pos.TGA_VIS_END2000, tgaVisEnd2000.copy());
    }

    public void setTgaVisEnd2000FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_VIS_END2000, Pos.TGA_VIS_END2000);
    }

    /**Original name: TGA-VIS-END2000<br>*/
    public AfDecimal getTgaVisEnd2000() {
        return readPackedAsDecimal(Pos.TGA_VIS_END2000, Len.Int.TGA_VIS_END2000, Len.Fract.TGA_VIS_END2000);
    }

    public byte[] getTgaVisEnd2000AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_VIS_END2000, Pos.TGA_VIS_END2000);
        return buffer;
    }

    public void setTgaVisEnd2000Null(String tgaVisEnd2000Null) {
        writeString(Pos.TGA_VIS_END2000_NULL, tgaVisEnd2000Null, Len.TGA_VIS_END2000_NULL);
    }

    /**Original name: TGA-VIS-END2000-NULL<br>*/
    public String getTgaVisEnd2000Null() {
        return readString(Pos.TGA_VIS_END2000_NULL, Len.TGA_VIS_END2000_NULL);
    }

    public String getTgaVisEnd2000NullFormatted() {
        return Functions.padBlanks(getTgaVisEnd2000Null(), Len.TGA_VIS_END2000_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_VIS_END2000 = 1;
        public static final int TGA_VIS_END2000_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_VIS_END2000 = 8;
        public static final int TGA_VIS_END2000_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_VIS_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_VIS_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
