package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-FIND-FIGLIA<br>
 * Variable: WK-FIND-FIGLIA from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFindFiglia {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkFindFiglia(char wkFindFiglia) {
        this.value = wkFindFiglia;
    }

    public char getWkFindFiglia() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
