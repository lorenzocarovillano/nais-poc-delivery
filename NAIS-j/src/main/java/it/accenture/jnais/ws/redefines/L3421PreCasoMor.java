package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PRE-CASO-MOR<br>
 * Variable: L3421-PRE-CASO-MOR from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PreCasoMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PreCasoMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PRE_CASO_MOR;
    }

    public void setL3421PreCasoMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PRE_CASO_MOR, Pos.L3421_PRE_CASO_MOR);
    }

    /**Original name: L3421-PRE-CASO-MOR<br>*/
    public AfDecimal getL3421PreCasoMor() {
        return readPackedAsDecimal(Pos.L3421_PRE_CASO_MOR, Len.Int.L3421_PRE_CASO_MOR, Len.Fract.L3421_PRE_CASO_MOR);
    }

    public byte[] getL3421PreCasoMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PRE_CASO_MOR, Pos.L3421_PRE_CASO_MOR);
        return buffer;
    }

    /**Original name: L3421-PRE-CASO-MOR-NULL<br>*/
    public String getL3421PreCasoMorNull() {
        return readString(Pos.L3421_PRE_CASO_MOR_NULL, Len.L3421_PRE_CASO_MOR_NULL);
    }

    public String getL3421PreCasoMorNullFormatted() {
        return Functions.padBlanks(getL3421PreCasoMorNull(), Len.L3421_PRE_CASO_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PRE_CASO_MOR = 1;
        public static final int L3421_PRE_CASO_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PRE_CASO_MOR = 8;
        public static final int L3421_PRE_CASO_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PRE_CASO_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PRE_CASO_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
