package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WVAS-PC-INV-DIS<br>
 * Variable: WVAS-PC-INV-DIS from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasPcInvDis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasPcInvDis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_PC_INV_DIS;
    }

    public void setWvasPcInvDis(AfDecimal wvasPcInvDis) {
        writeDecimalAsPacked(Pos.WVAS_PC_INV_DIS, wvasPcInvDis.copy());
    }

    /**Original name: WVAS-PC-INV-DIS<br>*/
    public AfDecimal getWvasPcInvDis() {
        return readPackedAsDecimal(Pos.WVAS_PC_INV_DIS, Len.Int.WVAS_PC_INV_DIS, Len.Fract.WVAS_PC_INV_DIS);
    }

    public void setWvasPcInvDisNull(String wvasPcInvDisNull) {
        writeString(Pos.WVAS_PC_INV_DIS_NULL, wvasPcInvDisNull, Len.WVAS_PC_INV_DIS_NULL);
    }

    /**Original name: WVAS-PC-INV-DIS-NULL<br>*/
    public String getWvasPcInvDisNull() {
        return readString(Pos.WVAS_PC_INV_DIS_NULL, Len.WVAS_PC_INV_DIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_PC_INV_DIS = 1;
        public static final int WVAS_PC_INV_DIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_PC_INV_DIS = 8;
        public static final int WVAS_PC_INV_DIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WVAS_PC_INV_DIS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_PC_INV_DIS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
