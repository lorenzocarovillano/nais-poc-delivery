package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTGZ-CED-COLL<br>
 * Variable: WPCO-DT-ULTGZ-CED-COLL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltgzCedColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltgzCedColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTGZ_CED_COLL;
    }

    public void setWpcoDtUltgzCedColl(int wpcoDtUltgzCedColl) {
        writeIntAsPacked(Pos.WPCO_DT_ULTGZ_CED_COLL, wpcoDtUltgzCedColl, Len.Int.WPCO_DT_ULTGZ_CED_COLL);
    }

    public void setDpcoDtUltgzCedCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTGZ_CED_COLL, Pos.WPCO_DT_ULTGZ_CED_COLL);
    }

    /**Original name: WPCO-DT-ULTGZ-CED-COLL<br>*/
    public int getWpcoDtUltgzCedColl() {
        return readPackedAsInt(Pos.WPCO_DT_ULTGZ_CED_COLL, Len.Int.WPCO_DT_ULTGZ_CED_COLL);
    }

    public byte[] getWpcoDtUltgzCedCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTGZ_CED_COLL, Pos.WPCO_DT_ULTGZ_CED_COLL);
        return buffer;
    }

    public void setWpcoDtUltgzCedCollNull(String wpcoDtUltgzCedCollNull) {
        writeString(Pos.WPCO_DT_ULTGZ_CED_COLL_NULL, wpcoDtUltgzCedCollNull, Len.WPCO_DT_ULTGZ_CED_COLL_NULL);
    }

    /**Original name: WPCO-DT-ULTGZ-CED-COLL-NULL<br>*/
    public String getWpcoDtUltgzCedCollNull() {
        return readString(Pos.WPCO_DT_ULTGZ_CED_COLL_NULL, Len.WPCO_DT_ULTGZ_CED_COLL_NULL);
    }

    public String getWpcoDtUltgzCedCollNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltgzCedCollNull(), Len.WPCO_DT_ULTGZ_CED_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTGZ_CED_COLL = 1;
        public static final int WPCO_DT_ULTGZ_CED_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTGZ_CED_COLL = 5;
        public static final int WPCO_DT_ULTGZ_CED_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTGZ_CED_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
