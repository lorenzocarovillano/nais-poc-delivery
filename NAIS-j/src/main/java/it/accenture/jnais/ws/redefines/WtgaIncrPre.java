package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-INCR-PRE<br>
 * Variable: WTGA-INCR-PRE from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaIncrPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaIncrPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_INCR_PRE;
    }

    public void setWtgaIncrPre(AfDecimal wtgaIncrPre) {
        writeDecimalAsPacked(Pos.WTGA_INCR_PRE, wtgaIncrPre.copy());
    }

    public void setWtgaIncrPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_INCR_PRE, Pos.WTGA_INCR_PRE);
    }

    /**Original name: WTGA-INCR-PRE<br>*/
    public AfDecimal getWtgaIncrPre() {
        return readPackedAsDecimal(Pos.WTGA_INCR_PRE, Len.Int.WTGA_INCR_PRE, Len.Fract.WTGA_INCR_PRE);
    }

    public byte[] getWtgaIncrPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_INCR_PRE, Pos.WTGA_INCR_PRE);
        return buffer;
    }

    public void initWtgaIncrPreSpaces() {
        fill(Pos.WTGA_INCR_PRE, Len.WTGA_INCR_PRE, Types.SPACE_CHAR);
    }

    public void setWtgaIncrPreNull(String wtgaIncrPreNull) {
        writeString(Pos.WTGA_INCR_PRE_NULL, wtgaIncrPreNull, Len.WTGA_INCR_PRE_NULL);
    }

    /**Original name: WTGA-INCR-PRE-NULL<br>*/
    public String getWtgaIncrPreNull() {
        return readString(Pos.WTGA_INCR_PRE_NULL, Len.WTGA_INCR_PRE_NULL);
    }

    public String getWtgaIncrPreNullFormatted() {
        return Functions.padBlanks(getWtgaIncrPreNull(), Len.WTGA_INCR_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_INCR_PRE = 1;
        public static final int WTGA_INCR_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_INCR_PRE = 8;
        public static final int WTGA_INCR_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_INCR_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_INCR_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
