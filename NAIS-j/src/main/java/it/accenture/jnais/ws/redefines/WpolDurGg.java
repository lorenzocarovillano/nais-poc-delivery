package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPOL-DUR-GG<br>
 * Variable: WPOL-DUR-GG from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DUR_GG;
    }

    public void setWpolDurGg(int wpolDurGg) {
        writeIntAsPacked(Pos.WPOL_DUR_GG, wpolDurGg, Len.Int.WPOL_DUR_GG);
    }

    public void setWpolDurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DUR_GG, Pos.WPOL_DUR_GG);
    }

    /**Original name: WPOL-DUR-GG<br>*/
    public int getWpolDurGg() {
        return readPackedAsInt(Pos.WPOL_DUR_GG, Len.Int.WPOL_DUR_GG);
    }

    public byte[] getWpolDurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DUR_GG, Pos.WPOL_DUR_GG);
        return buffer;
    }

    public void setWpolDurGgNull(String wpolDurGgNull) {
        writeString(Pos.WPOL_DUR_GG_NULL, wpolDurGgNull, Len.WPOL_DUR_GG_NULL);
    }

    /**Original name: WPOL-DUR-GG-NULL<br>*/
    public String getWpolDurGgNull() {
        return readString(Pos.WPOL_DUR_GG_NULL, Len.WPOL_DUR_GG_NULL);
    }

    public String getWpolDurGgNullFormatted() {
        return Functions.padBlanks(getWpolDurGgNull(), Len.WPOL_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DUR_GG = 1;
        public static final int WPOL_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DUR_GG = 3;
        public static final int WPOL_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
