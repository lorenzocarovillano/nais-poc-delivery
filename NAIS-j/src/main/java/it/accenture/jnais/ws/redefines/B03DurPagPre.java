package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DUR-PAG-PRE<br>
 * Variable: B03-DUR-PAG-PRE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DurPagPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DurPagPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DUR_PAG_PRE;
    }

    public void setB03DurPagPre(int b03DurPagPre) {
        writeIntAsPacked(Pos.B03_DUR_PAG_PRE, b03DurPagPre, Len.Int.B03_DUR_PAG_PRE);
    }

    public void setB03DurPagPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DUR_PAG_PRE, Pos.B03_DUR_PAG_PRE);
    }

    /**Original name: B03-DUR-PAG-PRE<br>*/
    public int getB03DurPagPre() {
        return readPackedAsInt(Pos.B03_DUR_PAG_PRE, Len.Int.B03_DUR_PAG_PRE);
    }

    public byte[] getB03DurPagPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DUR_PAG_PRE, Pos.B03_DUR_PAG_PRE);
        return buffer;
    }

    public void setB03DurPagPreNull(String b03DurPagPreNull) {
        writeString(Pos.B03_DUR_PAG_PRE_NULL, b03DurPagPreNull, Len.B03_DUR_PAG_PRE_NULL);
    }

    /**Original name: B03-DUR-PAG-PRE-NULL<br>*/
    public String getB03DurPagPreNull() {
        return readString(Pos.B03_DUR_PAG_PRE_NULL, Len.B03_DUR_PAG_PRE_NULL);
    }

    public String getB03DurPagPreNullFormatted() {
        return Functions.padBlanks(getB03DurPagPreNull(), Len.B03_DUR_PAG_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DUR_PAG_PRE = 1;
        public static final int B03_DUR_PAG_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DUR_PAG_PRE = 3;
        public static final int B03_DUR_PAG_PRE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DUR_PAG_PRE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
