package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvpol1Lvvs0002;
import it.accenture.jnais.copy.Ldbv5061;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.WkParamMovi;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs3150;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3150<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3150Data {

    //==== PROPERTIES ====
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-DT-RICOR-PREC<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkDtRicorPrec = DefaultValues.stringVal(Len.WK_DT_RICOR_PREC);
    //Original name: WK-DT-RICOR-SUCC
    private String wkDtRicorSucc = DefaultValues.stringVal(Len.WK_DT_RICOR_SUCC);
    //Original name: WK-APPO-DT
    private String wkAppoDt = "00000000";
    /**Original name: FORMATO<br>
	 * <pre>-- AREA ROUTINE PER IL CALCOLO DIFFERENZA TRA DATE</pre>*/
    private char formato = DefaultValues.CHAR_VAL;
    //Original name: DATA-INFERIORE
    private DataInferioreLccs0490 dataInferiore = new DataInferioreLccs0490();
    //Original name: DATA-SUPERIORE
    private DataSuperioreLccs0490 dataSuperiore = new DataSuperioreLccs0490();
    //Original name: GG-DIFF
    private String ggDiff = DefaultValues.stringVal(Len.GG_DIFF);
    //Original name: CODICE-RITORNO
    private char codiceRitorno = DefaultValues.CHAR_VAL;
    //Original name: DPOL-ELE-POLI-MAX
    private short dpolElePoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1Lvvs0002 lccvpol1 = new Lccvpol1Lvvs0002();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: PARAM-MOVI
    private ParamMoviLdbs1470 paramMovi = new ParamMoviLdbs1470();
    //Original name: LDBV5061
    private Ldbv5061 ldbv5061 = new Ldbv5061();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY TIPOLOGICHE
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs3150 wsMovimento = new WsMovimentoLvvs3150();
    /**Original name: WK-PARAM-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *       FLAG                                                      *
	 * ----------------------------------------------------------------*</pre>*/
    private WkParamMovi wkParamMovi = new WkParamMovi();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkDtRicorPrec(int wkDtRicorPrec) {
        this.wkDtRicorPrec = NumericDisplay.asString(wkDtRicorPrec, Len.WK_DT_RICOR_PREC);
    }

    public void setWkDtRicorPrecFormatted(String wkDtRicorPrec) {
        this.wkDtRicorPrec = Trunc.toUnsignedNumeric(wkDtRicorPrec, Len.WK_DT_RICOR_PREC);
    }

    public int getWkDtRicorPrec() {
        return NumericDisplay.asInt(this.wkDtRicorPrec);
    }

    public String getWkDtRicorPrecFormatted() {
        return this.wkDtRicorPrec;
    }

    public void setWkDtRicorSucc(int wkDtRicorSucc) {
        this.wkDtRicorSucc = NumericDisplay.asString(wkDtRicorSucc, Len.WK_DT_RICOR_SUCC);
    }

    public void setWkDtRicorSuccFormatted(String wkDtRicorSucc) {
        this.wkDtRicorSucc = Trunc.toUnsignedNumeric(wkDtRicorSucc, Len.WK_DT_RICOR_SUCC);
    }

    public int getWkDtRicorSucc() {
        return NumericDisplay.asInt(this.wkDtRicorSucc);
    }

    public void setWkAppoDtFormatted(String wkAppoDt) {
        this.wkAppoDt = Trunc.toUnsignedNumeric(wkAppoDt, Len.WK_APPO_DT);
    }

    public int getWkAppoDt() {
        return NumericDisplay.asInt(this.wkAppoDt);
    }

    public String getWkAppoDtFormatted() {
        return this.wkAppoDt;
    }

    public void setFormato(char formato) {
        this.formato = formato;
    }

    public void setFormatoFormatted(String formato) {
        setFormato(Functions.charAt(formato, Types.CHAR_SIZE));
    }

    public void setFormatoFromBuffer(byte[] buffer) {
        formato = MarshalByte.readChar(buffer, 1);
    }

    public char getFormato() {
        return this.formato;
    }

    public void setGgDiffFromBuffer(byte[] buffer) {
        ggDiff = MarshalByte.readFixedString(buffer, 1, Len.GG_DIFF);
    }

    public int getGgDiff() {
        return NumericDisplay.asInt(this.ggDiff);
    }

    public String getGgDiffFormatted() {
        return this.ggDiff;
    }

    public void setCodiceRitorno(char codiceRitorno) {
        this.codiceRitorno = codiceRitorno;
    }

    public void setCodiceRitornoFromBuffer(byte[] buffer) {
        codiceRitorno = MarshalByte.readChar(buffer, 1);
    }

    public char getCodiceRitorno() {
        return this.codiceRitorno;
    }

    public void setDpolAreaPolizzaFormatted(String data) {
        byte[] buffer = new byte[Len.DPOL_AREA_POLIZZA];
        MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POLIZZA);
        setDpolAreaPolizzaBytes(buffer, 1);
    }

    public void setDpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        dpolElePoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDpolTabPoliBytes(buffer, position);
    }

    public void setDpolElePoliMax(short dpolElePoliMax) {
        this.dpolElePoliMax = dpolElePoliMax;
    }

    public short getDpolElePoliMax() {
        return this.dpolElePoliMax;
    }

    public void setDpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
        position += Lccvpol1Lvvs0002.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public DataInferioreLccs0490 getDataInferiore() {
        return dataInferiore;
    }

    public DataSuperioreLccs0490 getDataSuperiore() {
        return dataSuperiore;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvpol1Lvvs0002 getLccvpol1() {
        return lccvpol1;
    }

    public Ldbv5061 getLdbv5061() {
        return ldbv5061;
    }

    public ParamMoviLdbs1470 getParamMovi() {
        return paramMovi;
    }

    public WkParamMovi getWkParamMovi() {
        return wkParamMovi;
    }

    public WsMovimentoLvvs3150 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_DT_RICOR_PREC = 8;
        public static final int WK_DT_RICOR_SUCC = 8;
        public static final int WK_APPO_DT = 8;
        public static final int GG_DIFF = 5;
        public static final int DPOL_ELE_POLI_MAX = 2;
        public static final int DPOL_TAB_POLI = WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int DPOL_AREA_POLIZZA = DPOL_ELE_POLI_MAX + DPOL_TAB_POLI;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
