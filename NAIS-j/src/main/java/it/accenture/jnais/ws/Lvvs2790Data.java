package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvpol1Lvvs0002;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.FlagComunTrov;
import it.accenture.jnais.ws.enums.FlagCurMov;
import it.accenture.jnais.ws.enums.FlagUltimaLettura;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0037;
import it.accenture.jnais.ws.redefines.WkDataFineLvvs2750;
import it.accenture.jnais.ws.redefines.WkDataInizioLvvs2750;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS2790<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs2790Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS2790";
    //Original name: IDBSLQU0
    private String idbslqu0 = "IDBSLQU0";
    //Original name: LDBS6040
    private String ldbs6040 = "LDBS6040";
    //Original name: WK-SOMMA
    private AfDecimal wkSomma = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-DATA-INIZIO
    private WkDataInizioLvvs2750 wkDataInizio = new WkDataInizioLvvs2750();
    //Original name: WK-DATA-FINE
    private WkDataFineLvvs2750 wkDataFine = new WkDataFineLvvs2750();
    //Original name: WK-VARIABILI
    private WkVariabiliLvvs2790 wkVariabili = new WkVariabiliLvvs2790();
    //Original name: LIQ
    private LiqIdbslqu0 liq = new LiqIdbslqu0();
    //Original name: MOVI
    private MoviLdbs1530 movi = new MoviLdbs1530();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>---------------------------------------------------------------*
	 *   LISTA TABELLE E TIPOLOGICHE
	 * ---------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0037 wsMovimento = new WsMovimentoLvvs0037();
    //Original name: DPOL-ELE-POLI-MAX
    private short dpolElePoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1Lvvs0002 lccvpol1 = new Lccvpol1Lvvs0002();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ISO
    private short ixTabIso = DefaultValues.BIN_SHORT_VAL;
    /**Original name: FLAG-CUR-MOV<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG
	 * ----------------------------------------------------------------*</pre>*/
    private FlagCurMov flagCurMov = new FlagCurMov();
    //Original name: FLAG-ULTIMA-LETTURA
    private FlagUltimaLettura flagUltimaLettura = new FlagUltimaLettura();
    //Original name: FLAG-COMUN-TROV
    private FlagComunTrov flagComunTrov = new FlagComunTrov();
    //Original name: WK-VAR-MOVI-COMUN
    private WkVarMoviComun wkVarMoviComun = new WkVarMoviComun();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getIdbslqu0() {
        return this.idbslqu0;
    }

    public String getLdbs6040() {
        return this.ldbs6040;
    }

    public void setWkSomma(AfDecimal wkSomma) {
        this.wkSomma.assign(wkSomma);
    }

    public AfDecimal getWkSomma() {
        return this.wkSomma.copy();
    }

    public void setDpolAreaPoliFormatted(String data) {
        byte[] buffer = new byte[Len.DPOL_AREA_POLI];
        MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POLI);
        setDpolAreaPoliBytes(buffer, 1);
    }

    public void setDpolAreaPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        dpolElePoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDpolTabPoliBytes(buffer, position);
    }

    public void setDpolElePoliMax(short dpolElePoliMax) {
        this.dpolElePoliMax = dpolElePoliMax;
    }

    public short getDpolElePoliMax() {
        return this.dpolElePoliMax;
    }

    public void setDpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
        position += Lccvpol1Lvvs0002.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabIso(short ixTabIso) {
        this.ixTabIso = ixTabIso;
    }

    public short getIxTabIso() {
        return this.ixTabIso;
    }

    public FlagComunTrov getFlagComunTrov() {
        return flagComunTrov;
    }

    public FlagCurMov getFlagCurMov() {
        return flagCurMov;
    }

    public FlagUltimaLettura getFlagUltimaLettura() {
        return flagUltimaLettura;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvpol1Lvvs0002 getLccvpol1() {
        return lccvpol1;
    }

    public LiqIdbslqu0 getLiq() {
        return liq;
    }

    public MoviLdbs1530 getMovi() {
        return movi;
    }

    public WkDataFineLvvs2750 getWkDataFine() {
        return wkDataFine;
    }

    public WkDataInizioLvvs2750 getWkDataInizio() {
        return wkDataInizio;
    }

    public WkVarMoviComun getWkVarMoviComun() {
        return wkVarMoviComun;
    }

    public WkVariabiliLvvs2790 getWkVariabili() {
        return wkVariabili;
    }

    public WsMovimentoLvvs0037 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_DATA = 8;
        public static final int DPOL_ELE_POLI_MAX = 2;
        public static final int DPOL_TAB_POLI = WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int DPOL_AREA_POLI = DPOL_ELE_POLI_MAX + DPOL_TAB_POLI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
