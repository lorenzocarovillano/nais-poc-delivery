package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0003-LIVELLO-OPERAZIONE<br>
 * Variable: IDSV0003-LIVELLO-OPERAZIONE from copybook IDSV0003<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0003LivelloOperazione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.LIVELLO_OPERAZIONE);
    public static final String FIRST_ACTION = "FAC";
    public static final String NONE_ACTION = "NAC";
    public static final String PRIMARY_KEY = "PK";
    public static final String ID = "ID";
    public static final String IB_OGGETTO = "IBO";
    public static final String ID_PADRE = "IDP";
    public static final String IB_SECONDARIO = "IBS";
    public static final String ID_OGGETTO = "IDO";
    public static final String WHERE_CONDITION = "WC";

    //==== METHODS ====
    public void setLivelloOperazione(String livelloOperazione) {
        this.value = Functions.subString(livelloOperazione, Len.LIVELLO_OPERAZIONE);
    }

    public String getLivelloOperazione() {
        return this.value;
    }

    public boolean isIdsv0003FirstAction() {
        return value.equals(FIRST_ACTION);
    }

    public void setIdsv0003FirstAction() {
        value = FIRST_ACTION;
    }

    public void setIdsv0003NoneAction() {
        value = NONE_ACTION;
    }

    public boolean isIdsv0003PrimaryKey() {
        return value.equals(PRIMARY_KEY);
    }

    public void setPrimaryKey() {
        value = PRIMARY_KEY;
    }

    public void setIdsi0011Id() {
        value = ID;
    }

    public void setIdsi0011IbOggetto() {
        value = IB_OGGETTO;
    }

    public void setIdsi0011IdPadre() {
        value = ID_PADRE;
    }

    public void setIdsi0011IbSecondario() {
        value = IB_SECONDARIO;
    }

    public void setIdsi0011IdOggetto() {
        value = ID_OGGETTO;
    }

    public boolean isIdsv0003WhereCondition() {
        return value.equals(WHERE_CONDITION);
    }

    public void setWhereCondition() {
        value = WHERE_CONDITION;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LIVELLO_OPERAZIONE = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
