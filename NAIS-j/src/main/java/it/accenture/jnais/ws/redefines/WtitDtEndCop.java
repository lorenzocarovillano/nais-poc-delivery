package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTIT-DT-END-COP<br>
 * Variable: WTIT-DT-END-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitDtEndCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitDtEndCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_DT_END_COP;
    }

    public void setWtitDtEndCop(int wtitDtEndCop) {
        writeIntAsPacked(Pos.WTIT_DT_END_COP, wtitDtEndCop, Len.Int.WTIT_DT_END_COP);
    }

    public void setWtitDtEndCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_DT_END_COP, Pos.WTIT_DT_END_COP);
    }

    /**Original name: WTIT-DT-END-COP<br>*/
    public int getWtitDtEndCop() {
        return readPackedAsInt(Pos.WTIT_DT_END_COP, Len.Int.WTIT_DT_END_COP);
    }

    public String getDtitDtEndCopFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getWtitDtEndCop()).toString();
    }

    public byte[] getWtitDtEndCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_DT_END_COP, Pos.WTIT_DT_END_COP);
        return buffer;
    }

    public void initWtitDtEndCopSpaces() {
        fill(Pos.WTIT_DT_END_COP, Len.WTIT_DT_END_COP, Types.SPACE_CHAR);
    }

    public void setWtitDtEndCopNull(String wtitDtEndCopNull) {
        writeString(Pos.WTIT_DT_END_COP_NULL, wtitDtEndCopNull, Len.WTIT_DT_END_COP_NULL);
    }

    /**Original name: WTIT-DT-END-COP-NULL<br>*/
    public String getWtitDtEndCopNull() {
        return readString(Pos.WTIT_DT_END_COP_NULL, Len.WTIT_DT_END_COP_NULL);
    }

    public String getDtitDtEndCopNullFormatted() {
        return Functions.padBlanks(getWtitDtEndCopNull(), Len.WTIT_DT_END_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_DT_END_COP = 1;
        public static final int WTIT_DT_END_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_DT_END_COP = 5;
        public static final int WTIT_DT_END_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_DT_END_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
