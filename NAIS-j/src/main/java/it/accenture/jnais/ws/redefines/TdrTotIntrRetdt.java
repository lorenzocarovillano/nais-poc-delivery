package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-INTR-RETDT<br>
 * Variable: TDR-TOT-INTR-RETDT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotIntrRetdt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotIntrRetdt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_INTR_RETDT;
    }

    public void setTdrTotIntrRetdt(AfDecimal tdrTotIntrRetdt) {
        writeDecimalAsPacked(Pos.TDR_TOT_INTR_RETDT, tdrTotIntrRetdt.copy());
    }

    public void setTdrTotIntrRetdtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_INTR_RETDT, Pos.TDR_TOT_INTR_RETDT);
    }

    /**Original name: TDR-TOT-INTR-RETDT<br>*/
    public AfDecimal getTdrTotIntrRetdt() {
        return readPackedAsDecimal(Pos.TDR_TOT_INTR_RETDT, Len.Int.TDR_TOT_INTR_RETDT, Len.Fract.TDR_TOT_INTR_RETDT);
    }

    public byte[] getTdrTotIntrRetdtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_INTR_RETDT, Pos.TDR_TOT_INTR_RETDT);
        return buffer;
    }

    public void setTdrTotIntrRetdtNull(String tdrTotIntrRetdtNull) {
        writeString(Pos.TDR_TOT_INTR_RETDT_NULL, tdrTotIntrRetdtNull, Len.TDR_TOT_INTR_RETDT_NULL);
    }

    /**Original name: TDR-TOT-INTR-RETDT-NULL<br>*/
    public String getTdrTotIntrRetdtNull() {
        return readString(Pos.TDR_TOT_INTR_RETDT_NULL, Len.TDR_TOT_INTR_RETDT_NULL);
    }

    public String getTdrTotIntrRetdtNullFormatted() {
        return Functions.padBlanks(getTdrTotIntrRetdtNull(), Len.TDR_TOT_INTR_RETDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_INTR_RETDT = 1;
        public static final int TDR_TOT_INTR_RETDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_INTR_RETDT = 8;
        public static final int TDR_TOT_INTR_RETDT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_INTR_RETDT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_INTR_RETDT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
