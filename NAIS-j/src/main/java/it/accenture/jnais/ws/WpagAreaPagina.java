package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.WpagDatiOuput;
import it.accenture.jnais.ws.occurs.WpagTabGaranzie;

/**Original name: WPAG-AREA-PAGINA<br>
 * Variable: WPAG-AREA-PAGINA from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WpagAreaPagina extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_GARANZIE_MAXOCCURS = 20;
    //Original name: WPAG-ELE-MAX-GARNZIE
    private String eleMaxGarnzie = DefaultValues.stringVal(Len.ELE_MAX_GARNZIE);
    //Original name: WPAG-TAB-GARANZIE
    private WpagTabGaranzie[] tabGaranzie = new WpagTabGaranzie[TAB_GARANZIE_MAXOCCURS];
    //Original name: WPAG-DATI-OUPUT
    private WpagDatiOuput datiOuput = new WpagDatiOuput();

    //==== CONSTRUCTORS ====
    public WpagAreaPagina() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_AREA_PAGINA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWpagAreaPaginaBytes(buf);
    }

    public void init() {
        for (int tabGaranzieIdx = 1; tabGaranzieIdx <= TAB_GARANZIE_MAXOCCURS; tabGaranzieIdx++) {
            tabGaranzie[tabGaranzieIdx - 1] = new WpagTabGaranzie();
        }
    }

    public String getWpagAreaPaginaFormatted() {
        return MarshalByteExt.bufferToStr(getWpagAreaPaginaBytes());
    }

    public void setWpagAreaPaginaBytes(byte[] buffer) {
        setWpagAreaPaginaBytes(buffer, 1);
    }

    public byte[] getWpagAreaPaginaBytes() {
        byte[] buffer = new byte[Len.WPAG_AREA_PAGINA];
        return getWpagAreaPaginaBytes(buffer, 1);
    }

    public void setWpagAreaPaginaBytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        datiOuput.setDatiOuputBytes(buffer, position);
    }

    public byte[] getWpagAreaPaginaBytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        datiOuput.getDatiOuputBytes(buffer, position);
        return buffer;
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxGarnzie = MarshalByte.readFixedString(buffer, position, Len.ELE_MAX_GARNZIE);
        position += Len.ELE_MAX_GARNZIE;
        for (int idx = 1; idx <= TAB_GARANZIE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabGaranzie[idx - 1].setTabGaranzieBytes(buffer, position);
                position += WpagTabGaranzie.Len.TAB_GARANZIE;
            }
            else {
                tabGaranzie[idx - 1].initTabGaranzieSpaces();
                position += WpagTabGaranzie.Len.TAB_GARANZIE;
            }
        }
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, eleMaxGarnzie, Len.ELE_MAX_GARNZIE);
        position += Len.ELE_MAX_GARNZIE;
        for (int idx = 1; idx <= TAB_GARANZIE_MAXOCCURS; idx++) {
            tabGaranzie[idx - 1].getTabGaranzieBytes(buffer, position);
            position += WpagTabGaranzie.Len.TAB_GARANZIE;
        }
        return buffer;
    }

    public short getEleMaxGarnzie() {
        return NumericDisplay.asShort(this.eleMaxGarnzie);
    }

    public WpagDatiOuput getDatiOuput() {
        return datiOuput;
    }

    public WpagTabGaranzie getTabGaranzie(int idx) {
        return tabGaranzie[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWpagAreaPaginaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_MAX_GARNZIE = 2;
        public static final int DATI_INPUT = ELE_MAX_GARNZIE + WpagAreaPagina.TAB_GARANZIE_MAXOCCURS * WpagTabGaranzie.Len.TAB_GARANZIE;
        public static final int WPAG_AREA_PAGINA = DATI_INPUT + WpagDatiOuput.Len.DATI_OUPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
