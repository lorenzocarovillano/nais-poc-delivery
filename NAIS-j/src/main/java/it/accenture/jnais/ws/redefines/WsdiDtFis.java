package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WSDI-DT-FIS<br>
 * Variable: WSDI-DT-FIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsdiDtFis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsdiDtFis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSDI_DT_FIS;
    }

    public void setWsdiDtFis(short wsdiDtFis) {
        writeShortAsPacked(Pos.WSDI_DT_FIS, wsdiDtFis, Len.Int.WSDI_DT_FIS);
    }

    public void setWsdiDtFisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSDI_DT_FIS, Pos.WSDI_DT_FIS);
    }

    /**Original name: WSDI-DT-FIS<br>*/
    public short getWsdiDtFis() {
        return readPackedAsShort(Pos.WSDI_DT_FIS, Len.Int.WSDI_DT_FIS);
    }

    public byte[] getWsdiDtFisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSDI_DT_FIS, Pos.WSDI_DT_FIS);
        return buffer;
    }

    public void initWsdiDtFisSpaces() {
        fill(Pos.WSDI_DT_FIS, Len.WSDI_DT_FIS, Types.SPACE_CHAR);
    }

    public void setWsdiDtFisNull(String wsdiDtFisNull) {
        writeString(Pos.WSDI_DT_FIS_NULL, wsdiDtFisNull, Len.WSDI_DT_FIS_NULL);
    }

    /**Original name: WSDI-DT-FIS-NULL<br>*/
    public String getWsdiDtFisNull() {
        return readString(Pos.WSDI_DT_FIS_NULL, Len.WSDI_DT_FIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSDI_DT_FIS = 1;
        public static final int WSDI_DT_FIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WSDI_DT_FIS = 3;
        public static final int WSDI_DT_FIS_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WSDI_DT_FIS = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
