package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-TP-GAR<br>
 * Variable: WGRZ-TP-GAR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzTpGar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzTpGar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_TP_GAR;
    }

    public void setWgrzTpGar(short wgrzTpGar) {
        writeShortAsPacked(Pos.WGRZ_TP_GAR, wgrzTpGar, Len.Int.WGRZ_TP_GAR);
    }

    public void setWgrzTpGarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_TP_GAR, Pos.WGRZ_TP_GAR);
    }

    /**Original name: WGRZ-TP-GAR<br>*/
    public short getWgrzTpGar() {
        return readPackedAsShort(Pos.WGRZ_TP_GAR, Len.Int.WGRZ_TP_GAR);
    }

    public byte[] getWgrzTpGarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_TP_GAR, Pos.WGRZ_TP_GAR);
        return buffer;
    }

    public void initWgrzTpGarSpaces() {
        fill(Pos.WGRZ_TP_GAR, Len.WGRZ_TP_GAR, Types.SPACE_CHAR);
    }

    public void setWgrzTpGarNull(String wgrzTpGarNull) {
        writeString(Pos.WGRZ_TP_GAR_NULL, wgrzTpGarNull, Len.WGRZ_TP_GAR_NULL);
    }

    /**Original name: WGRZ-TP-GAR-NULL<br>*/
    public String getWgrzTpGarNull() {
        return readString(Pos.WGRZ_TP_GAR_NULL, Len.WGRZ_TP_GAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_TP_GAR = 1;
        public static final int WGRZ_TP_GAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_TP_GAR = 2;
        public static final int WGRZ_TP_GAR_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_TP_GAR = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
