package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-INTR-MORA<br>
 * Variable: TIT-TOT-INTR-MORA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotIntrMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotIntrMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_INTR_MORA;
    }

    public void setTitTotIntrMora(AfDecimal titTotIntrMora) {
        writeDecimalAsPacked(Pos.TIT_TOT_INTR_MORA, titTotIntrMora.copy());
    }

    public void setTitTotIntrMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_INTR_MORA, Pos.TIT_TOT_INTR_MORA);
    }

    /**Original name: TIT-TOT-INTR-MORA<br>*/
    public AfDecimal getTitTotIntrMora() {
        return readPackedAsDecimal(Pos.TIT_TOT_INTR_MORA, Len.Int.TIT_TOT_INTR_MORA, Len.Fract.TIT_TOT_INTR_MORA);
    }

    public byte[] getTitTotIntrMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_INTR_MORA, Pos.TIT_TOT_INTR_MORA);
        return buffer;
    }

    public void setTitTotIntrMoraNull(String titTotIntrMoraNull) {
        writeString(Pos.TIT_TOT_INTR_MORA_NULL, titTotIntrMoraNull, Len.TIT_TOT_INTR_MORA_NULL);
    }

    /**Original name: TIT-TOT-INTR-MORA-NULL<br>*/
    public String getTitTotIntrMoraNull() {
        return readString(Pos.TIT_TOT_INTR_MORA_NULL, Len.TIT_TOT_INTR_MORA_NULL);
    }

    public String getTitTotIntrMoraNullFormatted() {
        return Functions.padBlanks(getTitTotIntrMoraNull(), Len.TIT_TOT_INTR_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_INTR_MORA = 1;
        public static final int TIT_TOT_INTR_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_INTR_MORA = 8;
        public static final int TIT_TOT_INTR_MORA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_INTR_MORA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_INTR_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
