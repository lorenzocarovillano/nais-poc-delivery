package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-MANFEE-ANTIC<br>
 * Variable: DTC-MANFEE-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_MANFEE_ANTIC;
    }

    public void setDtcManfeeAntic(AfDecimal dtcManfeeAntic) {
        writeDecimalAsPacked(Pos.DTC_MANFEE_ANTIC, dtcManfeeAntic.copy());
    }

    public void setDtcManfeeAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_MANFEE_ANTIC, Pos.DTC_MANFEE_ANTIC);
    }

    /**Original name: DTC-MANFEE-ANTIC<br>*/
    public AfDecimal getDtcManfeeAntic() {
        return readPackedAsDecimal(Pos.DTC_MANFEE_ANTIC, Len.Int.DTC_MANFEE_ANTIC, Len.Fract.DTC_MANFEE_ANTIC);
    }

    public byte[] getDtcManfeeAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_MANFEE_ANTIC, Pos.DTC_MANFEE_ANTIC);
        return buffer;
    }

    public void setDtcManfeeAnticNull(String dtcManfeeAnticNull) {
        writeString(Pos.DTC_MANFEE_ANTIC_NULL, dtcManfeeAnticNull, Len.DTC_MANFEE_ANTIC_NULL);
    }

    /**Original name: DTC-MANFEE-ANTIC-NULL<br>*/
    public String getDtcManfeeAnticNull() {
        return readString(Pos.DTC_MANFEE_ANTIC_NULL, Len.DTC_MANFEE_ANTIC_NULL);
    }

    public String getDtcManfeeAnticNullFormatted() {
        return Functions.padBlanks(getDtcManfeeAnticNull(), Len.DTC_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_MANFEE_ANTIC = 1;
        public static final int DTC_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_MANFEE_ANTIC = 8;
        public static final int DTC_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
