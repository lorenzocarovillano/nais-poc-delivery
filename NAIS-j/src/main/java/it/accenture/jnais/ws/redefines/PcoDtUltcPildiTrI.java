package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-PILDI-TR-I<br>
 * Variable: PCO-DT-ULTC-PILDI-TR-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcPildiTrI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcPildiTrI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_PILDI_TR_I;
    }

    public void setPcoDtUltcPildiTrI(int pcoDtUltcPildiTrI) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_PILDI_TR_I, pcoDtUltcPildiTrI, Len.Int.PCO_DT_ULTC_PILDI_TR_I);
    }

    public void setPcoDtUltcPildiTrIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_PILDI_TR_I, Pos.PCO_DT_ULTC_PILDI_TR_I);
    }

    /**Original name: PCO-DT-ULTC-PILDI-TR-I<br>*/
    public int getPcoDtUltcPildiTrI() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_PILDI_TR_I, Len.Int.PCO_DT_ULTC_PILDI_TR_I);
    }

    public byte[] getPcoDtUltcPildiTrIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_PILDI_TR_I, Pos.PCO_DT_ULTC_PILDI_TR_I);
        return buffer;
    }

    public void initPcoDtUltcPildiTrIHighValues() {
        fill(Pos.PCO_DT_ULTC_PILDI_TR_I, Len.PCO_DT_ULTC_PILDI_TR_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcPildiTrINull(String pcoDtUltcPildiTrINull) {
        writeString(Pos.PCO_DT_ULTC_PILDI_TR_I_NULL, pcoDtUltcPildiTrINull, Len.PCO_DT_ULTC_PILDI_TR_I_NULL);
    }

    /**Original name: PCO-DT-ULTC-PILDI-TR-I-NULL<br>*/
    public String getPcoDtUltcPildiTrINull() {
        return readString(Pos.PCO_DT_ULTC_PILDI_TR_I_NULL, Len.PCO_DT_ULTC_PILDI_TR_I_NULL);
    }

    public String getPcoDtUltcPildiTrINullFormatted() {
        return Functions.padBlanks(getPcoDtUltcPildiTrINull(), Len.PCO_DT_ULTC_PILDI_TR_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_PILDI_TR_I = 1;
        public static final int PCO_DT_ULTC_PILDI_TR_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_PILDI_TR_I = 5;
        public static final int PCO_DT_ULTC_PILDI_TR_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_PILDI_TR_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
