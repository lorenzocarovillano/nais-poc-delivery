package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-ACQ-EXP<br>
 * Variable: WDTR-ACQ-EXP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_ACQ_EXP;
    }

    public void setWdtrAcqExp(AfDecimal wdtrAcqExp) {
        writeDecimalAsPacked(Pos.WDTR_ACQ_EXP, wdtrAcqExp.copy());
    }

    /**Original name: WDTR-ACQ-EXP<br>*/
    public AfDecimal getWdtrAcqExp() {
        return readPackedAsDecimal(Pos.WDTR_ACQ_EXP, Len.Int.WDTR_ACQ_EXP, Len.Fract.WDTR_ACQ_EXP);
    }

    public void setWdtrAcqExpNull(String wdtrAcqExpNull) {
        writeString(Pos.WDTR_ACQ_EXP_NULL, wdtrAcqExpNull, Len.WDTR_ACQ_EXP_NULL);
    }

    /**Original name: WDTR-ACQ-EXP-NULL<br>*/
    public String getWdtrAcqExpNull() {
        return readString(Pos.WDTR_ACQ_EXP_NULL, Len.WDTR_ACQ_EXP_NULL);
    }

    public String getWdtrAcqExpNullFormatted() {
        return Functions.padBlanks(getWdtrAcqExpNull(), Len.WDTR_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_ACQ_EXP = 1;
        public static final int WDTR_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_ACQ_EXP = 8;
        public static final int WDTR_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
