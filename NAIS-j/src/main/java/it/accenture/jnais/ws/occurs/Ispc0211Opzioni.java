package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0211-OPZIONI<br>
 * Variables: ISPC0211-OPZIONI from copybook ISPC0211<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0211Opzioni {

    //==== PROPERTIES ====
    //Original name: ISPC0211-OPZ-ESERCITATA
    private String ispc0211OpzEsercitata = DefaultValues.stringVal(Len.ISPC0211_OPZ_ESERCITATA);

    //==== METHODS ====
    public void setOpzioniBytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0211OpzEsercitata = MarshalByte.readString(buffer, position, Len.ISPC0211_OPZ_ESERCITATA);
    }

    public byte[] getOpzioniBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ispc0211OpzEsercitata, Len.ISPC0211_OPZ_ESERCITATA);
        return buffer;
    }

    public void initOpzioniSpaces() {
        ispc0211OpzEsercitata = "";
    }

    public void setIspc0211OpzEsercitata(String ispc0211OpzEsercitata) {
        this.ispc0211OpzEsercitata = Functions.subString(ispc0211OpzEsercitata, Len.ISPC0211_OPZ_ESERCITATA);
    }

    public String getIspc0211OpzEsercitata() {
        return this.ispc0211OpzEsercitata;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ISPC0211_OPZ_ESERCITATA = 2;
        public static final int OPZIONI = ISPC0211_OPZ_ESERCITATA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
