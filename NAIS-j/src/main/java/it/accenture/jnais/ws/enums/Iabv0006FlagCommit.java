package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IABV0006-FLAG-COMMIT<br>
 * Variable: IABV0006-FLAG-COMMIT from copybook IABV0006<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0006FlagCommit {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char YES = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCommit(char flagCommit) {
        this.value = flagCommit;
    }

    public char getFlagCommit() {
        return this.value;
    }

    public boolean isIabv0006CommitYes() {
        return value == YES;
    }

    public void setIabv0006CommitNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_COMMIT = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
