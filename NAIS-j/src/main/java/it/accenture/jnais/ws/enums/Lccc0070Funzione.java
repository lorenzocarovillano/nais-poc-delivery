package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0070-FUNZIONE<br>
 * Variable: LCCC0070-FUNZIONE from copybook LCCC0070<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0070Funzione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FUNZIONE);
    public static final String ADE = "AD";
    public static final String GAR = "GA";
    public static final String TGA = "TG";

    //==== METHODS ====
    public void setFunzione(String funzione) {
        this.value = Functions.subString(funzione, Len.FUNZIONE);
    }

    public String getFunzione() {
        return this.value;
    }

    public void setLccc0070CalcIbAde() {
        value = ADE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FUNZIONE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
