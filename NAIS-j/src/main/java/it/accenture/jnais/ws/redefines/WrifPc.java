package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRIF-PC<br>
 * Variable: WRIF-PC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrifPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrifPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIF_PC;
    }

    public void setWrifPc(AfDecimal wrifPc) {
        writeDecimalAsPacked(Pos.WRIF_PC, wrifPc.copy());
    }

    public void setWrifPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIF_PC, Pos.WRIF_PC);
    }

    /**Original name: WRIF-PC<br>*/
    public AfDecimal getWrifPc() {
        return readPackedAsDecimal(Pos.WRIF_PC, Len.Int.WRIF_PC, Len.Fract.WRIF_PC);
    }

    public byte[] getWrifPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIF_PC, Pos.WRIF_PC);
        return buffer;
    }

    public void initWrifPcSpaces() {
        fill(Pos.WRIF_PC, Len.WRIF_PC, Types.SPACE_CHAR);
    }

    public void setWrifPcNull(String wrifPcNull) {
        writeString(Pos.WRIF_PC_NULL, wrifPcNull, Len.WRIF_PC_NULL);
    }

    /**Original name: WRIF-PC-NULL<br>*/
    public String getWrifPcNull() {
        return readString(Pos.WRIF_PC_NULL, Len.WRIF_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIF_PC = 1;
        public static final int WRIF_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIF_PC = 4;
        public static final int WRIF_PC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRIF_PC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIF_PC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
