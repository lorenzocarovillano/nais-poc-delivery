package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIF-DT-INVST-CALC<br>
 * Variable: RIF-DT-INVST-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RifDtInvstCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RifDtInvstCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIF_DT_INVST_CALC;
    }

    public void setRifDtInvstCalc(int rifDtInvstCalc) {
        writeIntAsPacked(Pos.RIF_DT_INVST_CALC, rifDtInvstCalc, Len.Int.RIF_DT_INVST_CALC);
    }

    public void setRifDtInvstCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIF_DT_INVST_CALC, Pos.RIF_DT_INVST_CALC);
    }

    /**Original name: RIF-DT-INVST-CALC<br>*/
    public int getRifDtInvstCalc() {
        return readPackedAsInt(Pos.RIF_DT_INVST_CALC, Len.Int.RIF_DT_INVST_CALC);
    }

    public byte[] getRifDtInvstCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIF_DT_INVST_CALC, Pos.RIF_DT_INVST_CALC);
        return buffer;
    }

    public void setRifDtInvstCalcNull(String rifDtInvstCalcNull) {
        writeString(Pos.RIF_DT_INVST_CALC_NULL, rifDtInvstCalcNull, Len.RIF_DT_INVST_CALC_NULL);
    }

    /**Original name: RIF-DT-INVST-CALC-NULL<br>*/
    public String getRifDtInvstCalcNull() {
        return readString(Pos.RIF_DT_INVST_CALC_NULL, Len.RIF_DT_INVST_CALC_NULL);
    }

    public String getRifDtInvstCalcNullFormatted() {
        return Functions.padBlanks(getRifDtInvstCalcNull(), Len.RIF_DT_INVST_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIF_DT_INVST_CALC = 1;
        public static final int RIF_DT_INVST_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIF_DT_INVST_CALC = 5;
        public static final int RIF_DT_INVST_CALC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIF_DT_INVST_CALC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
