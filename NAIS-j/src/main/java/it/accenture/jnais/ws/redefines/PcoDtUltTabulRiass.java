package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-TABUL-RIASS<br>
 * Variable: PCO-DT-ULT-TABUL-RIASS from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltTabulRiass extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltTabulRiass() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_TABUL_RIASS;
    }

    public void setPcoDtUltTabulRiass(int pcoDtUltTabulRiass) {
        writeIntAsPacked(Pos.PCO_DT_ULT_TABUL_RIASS, pcoDtUltTabulRiass, Len.Int.PCO_DT_ULT_TABUL_RIASS);
    }

    public void setPcoDtUltTabulRiassFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_TABUL_RIASS, Pos.PCO_DT_ULT_TABUL_RIASS);
    }

    /**Original name: PCO-DT-ULT-TABUL-RIASS<br>*/
    public int getPcoDtUltTabulRiass() {
        return readPackedAsInt(Pos.PCO_DT_ULT_TABUL_RIASS, Len.Int.PCO_DT_ULT_TABUL_RIASS);
    }

    public byte[] getPcoDtUltTabulRiassAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_TABUL_RIASS, Pos.PCO_DT_ULT_TABUL_RIASS);
        return buffer;
    }

    public void initPcoDtUltTabulRiassHighValues() {
        fill(Pos.PCO_DT_ULT_TABUL_RIASS, Len.PCO_DT_ULT_TABUL_RIASS, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltTabulRiassNull(String pcoDtUltTabulRiassNull) {
        writeString(Pos.PCO_DT_ULT_TABUL_RIASS_NULL, pcoDtUltTabulRiassNull, Len.PCO_DT_ULT_TABUL_RIASS_NULL);
    }

    /**Original name: PCO-DT-ULT-TABUL-RIASS-NULL<br>*/
    public String getPcoDtUltTabulRiassNull() {
        return readString(Pos.PCO_DT_ULT_TABUL_RIASS_NULL, Len.PCO_DT_ULT_TABUL_RIASS_NULL);
    }

    public String getPcoDtUltTabulRiassNullFormatted() {
        return Functions.padBlanks(getPcoDtUltTabulRiassNull(), Len.PCO_DT_ULT_TABUL_RIASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_TABUL_RIASS = 1;
        public static final int PCO_DT_ULT_TABUL_RIASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_TABUL_RIASS = 5;
        public static final int PCO_DT_ULT_TABUL_RIASS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_TABUL_RIASS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
