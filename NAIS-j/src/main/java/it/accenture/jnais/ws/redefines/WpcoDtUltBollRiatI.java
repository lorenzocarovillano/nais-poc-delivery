package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-RIAT-I<br>
 * Variable: WPCO-DT-ULT-BOLL-RIAT-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollRiatI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollRiatI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_RIAT_I;
    }

    public void setWpcoDtUltBollRiatI(int wpcoDtUltBollRiatI) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_RIAT_I, wpcoDtUltBollRiatI, Len.Int.WPCO_DT_ULT_BOLL_RIAT_I);
    }

    public void setDpcoDtUltBollRiatIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RIAT_I, Pos.WPCO_DT_ULT_BOLL_RIAT_I);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RIAT-I<br>*/
    public int getWpcoDtUltBollRiatI() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_RIAT_I, Len.Int.WPCO_DT_ULT_BOLL_RIAT_I);
    }

    public byte[] getWpcoDtUltBollRiatIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RIAT_I, Pos.WPCO_DT_ULT_BOLL_RIAT_I);
        return buffer;
    }

    public void setWpcoDtUltBollRiatINull(String wpcoDtUltBollRiatINull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_RIAT_I_NULL, wpcoDtUltBollRiatINull, Len.WPCO_DT_ULT_BOLL_RIAT_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RIAT-I-NULL<br>*/
    public String getWpcoDtUltBollRiatINull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_RIAT_I_NULL, Len.WPCO_DT_ULT_BOLL_RIAT_I_NULL);
    }

    public String getWpcoDtUltBollRiatINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollRiatINull(), Len.WPCO_DT_ULT_BOLL_RIAT_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RIAT_I = 1;
        public static final int WPCO_DT_ULT_BOLL_RIAT_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RIAT_I = 5;
        public static final int WPCO_DT_ULT_BOLL_RIAT_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_RIAT_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
