package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-VAR-MOVI-COMUN<br>
 * Variable: WK-VAR-MOVI-COMUN from program LVVS0037<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVarMoviComun {

    //==== PROPERTIES ====
    //Original name: WK-ID-MOVI-COMUN
    private int idMoviComun = DefaultValues.INT_VAL;
    //Original name: WK-DATA-EFF-PREC
    private int dataEffPrec = DefaultValues.INT_VAL;
    //Original name: WK-DATA-CPTZ-PREC
    private long dataCptzPrec = DefaultValues.LONG_VAL;
    //Original name: WK-DATA-EFF-RIP
    private int dataEffRip = DefaultValues.INT_VAL;
    //Original name: WK-DATA-CPTZ-RIP
    private long dataCptzRip = DefaultValues.LONG_VAL;

    //==== METHODS ====
    public void setIdMoviComun(int idMoviComun) {
        this.idMoviComun = idMoviComun;
    }

    public int getIdMoviComun() {
        return this.idMoviComun;
    }

    public void setDataEffPrec(int dataEffPrec) {
        this.dataEffPrec = dataEffPrec;
    }

    public int getDataEffPrec() {
        return this.dataEffPrec;
    }

    public void setDataCptzPrec(long dataCptzPrec) {
        this.dataCptzPrec = dataCptzPrec;
    }

    public long getDataCptzPrec() {
        return this.dataCptzPrec;
    }

    public void setDataEffRip(int dataEffRip) {
        this.dataEffRip = dataEffRip;
    }

    public int getDataEffRip() {
        return this.dataEffRip;
    }

    public void setDataCptzRip(long dataCptzRip) {
        this.dataCptzRip = dataCptzRip;
    }

    public long getDataCptzRip() {
        return this.dataCptzRip;
    }
}
