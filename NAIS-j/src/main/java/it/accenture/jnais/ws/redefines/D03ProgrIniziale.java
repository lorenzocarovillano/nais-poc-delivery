package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: D03-PROGR-INIZIALE<br>
 * Variable: D03-PROGR-INIZIALE from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class D03ProgrIniziale extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public D03ProgrIniziale() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.D03_PROGR_INIZIALE;
    }

    public void setD03ProgrIniziale(long d03ProgrIniziale) {
        writeLongAsPacked(Pos.D03_PROGR_INIZIALE, d03ProgrIniziale, Len.Int.D03_PROGR_INIZIALE);
    }

    public void setD03ProgrInizialeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.D03_PROGR_INIZIALE, Pos.D03_PROGR_INIZIALE);
    }

    /**Original name: D03-PROGR-INIZIALE<br>*/
    public long getD03ProgrIniziale() {
        return readPackedAsLong(Pos.D03_PROGR_INIZIALE, Len.Int.D03_PROGR_INIZIALE);
    }

    public byte[] getD03ProgrInizialeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.D03_PROGR_INIZIALE, Pos.D03_PROGR_INIZIALE);
        return buffer;
    }

    public void setD03ProgrInizialeNull(String d03ProgrInizialeNull) {
        writeString(Pos.D03_PROGR_INIZIALE_NULL, d03ProgrInizialeNull, Len.D03_PROGR_INIZIALE_NULL);
    }

    /**Original name: D03-PROGR-INIZIALE-NULL<br>*/
    public String getD03ProgrInizialeNull() {
        return readString(Pos.D03_PROGR_INIZIALE_NULL, Len.D03_PROGR_INIZIALE_NULL);
    }

    public String getD03ProgrInizialeNullFormatted() {
        return Functions.padBlanks(getD03ProgrInizialeNull(), Len.D03_PROGR_INIZIALE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int D03_PROGR_INIZIALE = 1;
        public static final int D03_PROGR_INIZIALE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int D03_PROGR_INIZIALE = 10;
        public static final int D03_PROGR_INIZIALE_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int D03_PROGR_INIZIALE = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
