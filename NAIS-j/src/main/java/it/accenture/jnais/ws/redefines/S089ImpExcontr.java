package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMP-EXCONTR<br>
 * Variable: S089-IMP-EXCONTR from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpExcontr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpExcontr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMP_EXCONTR;
    }

    public void setWlquImpExcontr(AfDecimal wlquImpExcontr) {
        writeDecimalAsPacked(Pos.S089_IMP_EXCONTR, wlquImpExcontr.copy());
    }

    public void setWlquImpExcontrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMP_EXCONTR, Pos.S089_IMP_EXCONTR);
    }

    /**Original name: WLQU-IMP-EXCONTR<br>*/
    public AfDecimal getWlquImpExcontr() {
        return readPackedAsDecimal(Pos.S089_IMP_EXCONTR, Len.Int.WLQU_IMP_EXCONTR, Len.Fract.WLQU_IMP_EXCONTR);
    }

    public byte[] getWlquImpExcontrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMP_EXCONTR, Pos.S089_IMP_EXCONTR);
        return buffer;
    }

    public void initWlquImpExcontrSpaces() {
        fill(Pos.S089_IMP_EXCONTR, Len.S089_IMP_EXCONTR, Types.SPACE_CHAR);
    }

    public void setWlquImpExcontrNull(String wlquImpExcontrNull) {
        writeString(Pos.S089_IMP_EXCONTR_NULL, wlquImpExcontrNull, Len.WLQU_IMP_EXCONTR_NULL);
    }

    /**Original name: WLQU-IMP-EXCONTR-NULL<br>*/
    public String getWlquImpExcontrNull() {
        return readString(Pos.S089_IMP_EXCONTR_NULL, Len.WLQU_IMP_EXCONTR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMP_EXCONTR = 1;
        public static final int S089_IMP_EXCONTR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMP_EXCONTR = 8;
        public static final int WLQU_IMP_EXCONTR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_EXCONTR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_EXCONTR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
