package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-TS-STAB-PRE<br>
 * Variable: B03-TS-STAB-PRE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03TsStabPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03TsStabPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_TS_STAB_PRE;
    }

    public void setB03TsStabPre(AfDecimal b03TsStabPre) {
        writeDecimalAsPacked(Pos.B03_TS_STAB_PRE, b03TsStabPre.copy());
    }

    public void setB03TsStabPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_TS_STAB_PRE, Pos.B03_TS_STAB_PRE);
    }

    /**Original name: B03-TS-STAB-PRE<br>*/
    public AfDecimal getB03TsStabPre() {
        return readPackedAsDecimal(Pos.B03_TS_STAB_PRE, Len.Int.B03_TS_STAB_PRE, Len.Fract.B03_TS_STAB_PRE);
    }

    public byte[] getB03TsStabPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_TS_STAB_PRE, Pos.B03_TS_STAB_PRE);
        return buffer;
    }

    public void setB03TsStabPreNull(String b03TsStabPreNull) {
        writeString(Pos.B03_TS_STAB_PRE_NULL, b03TsStabPreNull, Len.B03_TS_STAB_PRE_NULL);
    }

    /**Original name: B03-TS-STAB-PRE-NULL<br>*/
    public String getB03TsStabPreNull() {
        return readString(Pos.B03_TS_STAB_PRE_NULL, Len.B03_TS_STAB_PRE_NULL);
    }

    public String getB03TsStabPreNullFormatted() {
        return Functions.padBlanks(getB03TsStabPreNull(), Len.B03_TS_STAB_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_TS_STAB_PRE = 1;
        public static final int B03_TS_STAB_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_TS_STAB_PRE = 8;
        public static final int B03_TS_STAB_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_TS_STAB_PRE = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_TS_STAB_PRE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
