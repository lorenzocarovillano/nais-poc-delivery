package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;

/**Original name: DATA-INFERIORE<br>
 * Variable: DATA-INFERIORE from program LVVS0097<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DataInferioreLvvs0097 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DataInferioreLvvs0097() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DATA_INFERIORE;
    }

    @Override
    public void init() {
        int position = 1;
        writeShort(position, ((short)0), Len.Int.AAAA_INF, SignType.NO_SIGN);
        position += Len.AAAA_INF;
        writeShort(position, ((short)1), Len.Int.MM_INF, SignType.NO_SIGN);
        position += Len.MM_INF;
        writeShort(position, ((short)1), Len.Int.GG_INF, SignType.NO_SIGN);
    }

    public String getDataInferioreFormatted() {
        return readFixedString(Pos.DATA_INFERIORE, Len.DATA_INFERIORE);
    }

    public void setAaaaInfFormatted(String aaaaInf) {
        writeString(Pos.AAAA_INF, Trunc.toUnsignedNumeric(aaaaInf, Len.AAAA_INF), Len.AAAA_INF);
    }

    /**Original name: AAAA-INF<br>*/
    public short getAaaaInf() {
        return readNumDispUnsignedShort(Pos.AAAA_INF, Len.AAAA_INF);
    }

    public void setDataInferioreN(int dataInferioreN) {
        writeInt(Pos.DATA_INFERIORE_N, dataInferioreN, Len.Int.DATA_INFERIORE_N, SignType.NO_SIGN);
    }

    public void setDataInferioreNFormatted(String dataInferioreN) {
        writeString(Pos.DATA_INFERIORE_N, Trunc.toUnsignedNumeric(dataInferioreN, Len.DATA_INFERIORE_N), Len.DATA_INFERIORE_N);
    }

    /**Original name: DATA-INFERIORE-N<br>*/
    public int getDataInferioreN() {
        return readNumDispUnsignedInt(Pos.DATA_INFERIORE_N, Len.DATA_INFERIORE_N);
    }

    public String getDataInferioreNFormatted() {
        return readFixedString(Pos.DATA_INFERIORE_N, Len.DATA_INFERIORE_N);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DATA_INFERIORE = 1;
        public static final int AAAA_INF = DATA_INFERIORE;
        public static final int MM_INF = AAAA_INF + Len.AAAA_INF;
        public static final int GG_INF = MM_INF + Len.MM_INF;
        public static final int DATA_INFERIORE_N = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA_INF = 4;
        public static final int MM_INF = 2;
        public static final int GG_INF = 2;
        public static final int DATA_INFERIORE = AAAA_INF + MM_INF + GG_INF;
        public static final int DATA_INFERIORE_N = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int AAAA_INF = 4;
            public static final int MM_INF = 2;
            public static final int GG_INF = 2;
            public static final int DATA_INFERIORE_N = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
