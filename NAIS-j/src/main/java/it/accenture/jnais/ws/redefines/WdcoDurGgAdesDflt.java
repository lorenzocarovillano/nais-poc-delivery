package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDCO-DUR-GG-ADES-DFLT<br>
 * Variable: WDCO-DUR-GG-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdcoDurGgAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdcoDurGgAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_DUR_GG_ADES_DFLT;
    }

    public void setWdcoDurGgAdesDflt(int wdcoDurGgAdesDflt) {
        writeIntAsPacked(Pos.WDCO_DUR_GG_ADES_DFLT, wdcoDurGgAdesDflt, Len.Int.WDCO_DUR_GG_ADES_DFLT);
    }

    public void setWdcoDurGgAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDCO_DUR_GG_ADES_DFLT, Pos.WDCO_DUR_GG_ADES_DFLT);
    }

    /**Original name: WDCO-DUR-GG-ADES-DFLT<br>*/
    public int getWdcoDurGgAdesDflt() {
        return readPackedAsInt(Pos.WDCO_DUR_GG_ADES_DFLT, Len.Int.WDCO_DUR_GG_ADES_DFLT);
    }

    public byte[] getWdcoDurGgAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDCO_DUR_GG_ADES_DFLT, Pos.WDCO_DUR_GG_ADES_DFLT);
        return buffer;
    }

    public void setWdcoDurGgAdesDfltNull(String wdcoDurGgAdesDfltNull) {
        writeString(Pos.WDCO_DUR_GG_ADES_DFLT_NULL, wdcoDurGgAdesDfltNull, Len.WDCO_DUR_GG_ADES_DFLT_NULL);
    }

    /**Original name: WDCO-DUR-GG-ADES-DFLT-NULL<br>*/
    public String getWdcoDurGgAdesDfltNull() {
        return readString(Pos.WDCO_DUR_GG_ADES_DFLT_NULL, Len.WDCO_DUR_GG_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDCO_DUR_GG_ADES_DFLT = 1;
        public static final int WDCO_DUR_GG_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_DUR_GG_ADES_DFLT = 3;
        public static final int WDCO_DUR_GG_ADES_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_DUR_GG_ADES_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
