package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndAmmbFunzFunz;
import it.accenture.jnais.copy.Ldbvg781;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBSG780<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbsg780Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-AMMB-FUNZ-FUNZ
    private IndAmmbFunzFunz indAmmbFunzFunz = new IndAmmbFunzFunz();
    //Original name: LDBVG781
    private Ldbvg781 ldbvg781 = new Ldbvg781();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndAmmbFunzFunz getIndAmmbFunzFunz() {
        return indAmmbFunzFunz;
    }

    public Ldbvg781 getLdbvg781() {
        return ldbvg781;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
