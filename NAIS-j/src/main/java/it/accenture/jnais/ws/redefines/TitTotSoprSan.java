package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-SOPR-SAN<br>
 * Variable: TIT-TOT-SOPR-SAN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_SOPR_SAN;
    }

    public void setTitTotSoprSan(AfDecimal titTotSoprSan) {
        writeDecimalAsPacked(Pos.TIT_TOT_SOPR_SAN, titTotSoprSan.copy());
    }

    public void setTitTotSoprSanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_SOPR_SAN, Pos.TIT_TOT_SOPR_SAN);
    }

    /**Original name: TIT-TOT-SOPR-SAN<br>*/
    public AfDecimal getTitTotSoprSan() {
        return readPackedAsDecimal(Pos.TIT_TOT_SOPR_SAN, Len.Int.TIT_TOT_SOPR_SAN, Len.Fract.TIT_TOT_SOPR_SAN);
    }

    public byte[] getTitTotSoprSanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_SOPR_SAN, Pos.TIT_TOT_SOPR_SAN);
        return buffer;
    }

    public void setTitTotSoprSanNull(String titTotSoprSanNull) {
        writeString(Pos.TIT_TOT_SOPR_SAN_NULL, titTotSoprSanNull, Len.TIT_TOT_SOPR_SAN_NULL);
    }

    /**Original name: TIT-TOT-SOPR-SAN-NULL<br>*/
    public String getTitTotSoprSanNull() {
        return readString(Pos.TIT_TOT_SOPR_SAN_NULL, Len.TIT_TOT_SOPR_SAN_NULL);
    }

    public String getTitTotSoprSanNullFormatted() {
        return Functions.padBlanks(getTitTotSoprSanNull(), Len.TIT_TOT_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SOPR_SAN = 1;
        public static final int TIT_TOT_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SOPR_SAN = 8;
        public static final int TIT_TOT_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
