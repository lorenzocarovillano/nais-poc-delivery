package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-NUM-GG-RIVAL<br>
 * Variable: L3421-NUM-GG-RIVAL from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421NumGgRival extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421NumGgRival() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_NUM_GG_RIVAL;
    }

    public void setL3421NumGgRivalFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_NUM_GG_RIVAL, Pos.L3421_NUM_GG_RIVAL);
    }

    /**Original name: L3421-NUM-GG-RIVAL<br>*/
    public int getL3421NumGgRival() {
        return readPackedAsInt(Pos.L3421_NUM_GG_RIVAL, Len.Int.L3421_NUM_GG_RIVAL);
    }

    public byte[] getL3421NumGgRivalAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_NUM_GG_RIVAL, Pos.L3421_NUM_GG_RIVAL);
        return buffer;
    }

    /**Original name: L3421-NUM-GG-RIVAL-NULL<br>*/
    public String getL3421NumGgRivalNull() {
        return readString(Pos.L3421_NUM_GG_RIVAL_NULL, Len.L3421_NUM_GG_RIVAL_NULL);
    }

    public String getL3421NumGgRivalNullFormatted() {
        return Functions.padBlanks(getL3421NumGgRivalNull(), Len.L3421_NUM_GG_RIVAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_NUM_GG_RIVAL = 1;
        public static final int L3421_NUM_GG_RIVAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_NUM_GG_RIVAL = 3;
        public static final int L3421_NUM_GG_RIVAL_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_NUM_GG_RIVAL = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
