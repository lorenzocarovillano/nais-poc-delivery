package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvb051;
import it.accenture.jnais.copy.Wb05Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: AREA-OUT<br>
 * Variable: AREA-OUT from program LLBS0250<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaOutLlbs0250 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WB05-ELE-B05-MAX
    private short wb05EleB05Max = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVB051
    private Lccvb051 lccvb051 = new Lccvb051();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_OUT;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaOutBytes(buf);
    }

    public void setAreaOutBytes(byte[] buffer) {
        setAreaOutBytes(buffer, 1);
    }

    public byte[] getAreaOutBytes() {
        byte[] buffer = new byte[Len.AREA_OUT];
        return getAreaOutBytes(buffer, 1);
    }

    public void setAreaOutBytes(byte[] buffer, int offset) {
        int position = offset;
        setWb05AreaB05Bytes(buffer, position);
    }

    public byte[] getAreaOutBytes(byte[] buffer, int offset) {
        int position = offset;
        getWb05AreaB05Bytes(buffer, position);
        return buffer;
    }

    public void setWb05AreaB05Bytes(byte[] buffer, int offset) {
        int position = offset;
        wb05EleB05Max = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWb05TabB05Bytes(buffer, position);
    }

    public byte[] getWb05AreaB05Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wb05EleB05Max);
        position += Types.SHORT_SIZE;
        getWb05TabB05Bytes(buffer, position);
        return buffer;
    }

    public void setWb05EleB05Max(short wb05EleB05Max) {
        this.wb05EleB05Max = wb05EleB05Max;
    }

    public short getWb05EleB05Max() {
        return this.wb05EleB05Max;
    }

    public void setWb05TabB05Bytes(byte[] buffer, int offset) {
        int position = offset;
        lccvb051.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvb051.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvb051.Len.Int.ID_PTF, 0));
        position += Lccvb051.Len.ID_PTF;
        lccvb051.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWb05TabB05Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvb051.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvb051.getIdPtf(), Lccvb051.Len.Int.ID_PTF, 0);
        position += Lccvb051.Len.ID_PTF;
        lccvb051.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public Lccvb051 getLccvb051() {
        return lccvb051;
    }

    @Override
    public byte[] serialize() {
        return getAreaOutBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WB05_ELE_B05_MAX = 2;
        public static final int WB05_TAB_B05 = WpolStatus.Len.STATUS + Lccvb051.Len.ID_PTF + Wb05Dati.Len.DATI;
        public static final int WB05_AREA_B05 = WB05_ELE_B05_MAX + WB05_TAB_B05;
        public static final int AREA_OUT = WB05_AREA_B05;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
