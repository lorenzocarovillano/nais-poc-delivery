package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-SOPR-SAN<br>
 * Variable: WDTR-SOPR-SAN from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_SOPR_SAN;
    }

    public void setWdtrSoprSan(AfDecimal wdtrSoprSan) {
        writeDecimalAsPacked(Pos.WDTR_SOPR_SAN, wdtrSoprSan.copy());
    }

    /**Original name: WDTR-SOPR-SAN<br>*/
    public AfDecimal getWdtrSoprSan() {
        return readPackedAsDecimal(Pos.WDTR_SOPR_SAN, Len.Int.WDTR_SOPR_SAN, Len.Fract.WDTR_SOPR_SAN);
    }

    public void setWdtrSoprSanNull(String wdtrSoprSanNull) {
        writeString(Pos.WDTR_SOPR_SAN_NULL, wdtrSoprSanNull, Len.WDTR_SOPR_SAN_NULL);
    }

    /**Original name: WDTR-SOPR-SAN-NULL<br>*/
    public String getWdtrSoprSanNull() {
        return readString(Pos.WDTR_SOPR_SAN_NULL, Len.WDTR_SOPR_SAN_NULL);
    }

    public String getWdtrSoprSanNullFormatted() {
        return Functions.padBlanks(getWdtrSoprSanNull(), Len.WDTR_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_SOPR_SAN = 1;
        public static final int WDTR_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_SOPR_SAN = 8;
        public static final int WDTR_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
