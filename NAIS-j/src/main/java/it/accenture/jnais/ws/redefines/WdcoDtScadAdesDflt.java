package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDCO-DT-SCAD-ADES-DFLT<br>
 * Variable: WDCO-DT-SCAD-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdcoDtScadAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdcoDtScadAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_DT_SCAD_ADES_DFLT;
    }

    public void setWdcoDtScadAdesDflt(int wdcoDtScadAdesDflt) {
        writeIntAsPacked(Pos.WDCO_DT_SCAD_ADES_DFLT, wdcoDtScadAdesDflt, Len.Int.WDCO_DT_SCAD_ADES_DFLT);
    }

    public void setWdcoDtScadAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDCO_DT_SCAD_ADES_DFLT, Pos.WDCO_DT_SCAD_ADES_DFLT);
    }

    /**Original name: WDCO-DT-SCAD-ADES-DFLT<br>*/
    public int getWdcoDtScadAdesDflt() {
        return readPackedAsInt(Pos.WDCO_DT_SCAD_ADES_DFLT, Len.Int.WDCO_DT_SCAD_ADES_DFLT);
    }

    public byte[] getWdcoDtScadAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDCO_DT_SCAD_ADES_DFLT, Pos.WDCO_DT_SCAD_ADES_DFLT);
        return buffer;
    }

    public void setWdcoDtScadAdesDfltNull(String wdcoDtScadAdesDfltNull) {
        writeString(Pos.WDCO_DT_SCAD_ADES_DFLT_NULL, wdcoDtScadAdesDfltNull, Len.WDCO_DT_SCAD_ADES_DFLT_NULL);
    }

    /**Original name: WDCO-DT-SCAD-ADES-DFLT-NULL<br>*/
    public String getWdcoDtScadAdesDfltNull() {
        return readString(Pos.WDCO_DT_SCAD_ADES_DFLT_NULL, Len.WDCO_DT_SCAD_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDCO_DT_SCAD_ADES_DFLT = 1;
        public static final int WDCO_DT_SCAD_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_DT_SCAD_ADES_DFLT = 5;
        public static final int WDCO_DT_SCAD_ADES_DFLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_DT_SCAD_ADES_DFLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
