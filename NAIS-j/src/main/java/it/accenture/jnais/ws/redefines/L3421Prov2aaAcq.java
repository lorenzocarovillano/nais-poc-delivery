package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PROV-2AA-ACQ<br>
 * Variable: L3421-PROV-2AA-ACQ from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421Prov2aaAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421Prov2aaAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PROV2AA_ACQ;
    }

    public void setL3421Prov2aaAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PROV2AA_ACQ, Pos.L3421_PROV2AA_ACQ);
    }

    /**Original name: L3421-PROV-2AA-ACQ<br>*/
    public AfDecimal getL3421Prov2aaAcq() {
        return readPackedAsDecimal(Pos.L3421_PROV2AA_ACQ, Len.Int.L3421_PROV2AA_ACQ, Len.Fract.L3421_PROV2AA_ACQ);
    }

    public byte[] getL3421Prov2aaAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PROV2AA_ACQ, Pos.L3421_PROV2AA_ACQ);
        return buffer;
    }

    /**Original name: L3421-PROV-2AA-ACQ-NULL<br>*/
    public String getL3421Prov2aaAcqNull() {
        return readString(Pos.L3421_PROV2AA_ACQ_NULL, Len.L3421_PROV2AA_ACQ_NULL);
    }

    public String getL3421Prov2aaAcqNullFormatted() {
        return Functions.padBlanks(getL3421Prov2aaAcqNull(), Len.L3421_PROV2AA_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PROV2AA_ACQ = 1;
        public static final int L3421_PROV2AA_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PROV2AA_ACQ = 8;
        public static final int L3421_PROV2AA_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PROV2AA_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PROV2AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
