package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIC-ID-JOB<br>
 * Variable: RIC-ID-JOB from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RicIdJob extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RicIdJob() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIC_ID_JOB;
    }

    public void setRicIdJob(int ricIdJob) {
        writeIntAsPacked(Pos.RIC_ID_JOB, ricIdJob, Len.Int.RIC_ID_JOB);
    }

    public void setRicIdJobFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIC_ID_JOB, Pos.RIC_ID_JOB);
    }

    /**Original name: RIC-ID-JOB<br>*/
    public int getRicIdJob() {
        return readPackedAsInt(Pos.RIC_ID_JOB, Len.Int.RIC_ID_JOB);
    }

    public byte[] getRicIdJobAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIC_ID_JOB, Pos.RIC_ID_JOB);
        return buffer;
    }

    public void setRicIdJobNull(String ricIdJobNull) {
        writeString(Pos.RIC_ID_JOB_NULL, ricIdJobNull, Len.RIC_ID_JOB_NULL);
    }

    /**Original name: RIC-ID-JOB-NULL<br>*/
    public String getRicIdJobNull() {
        return readString(Pos.RIC_ID_JOB_NULL, Len.RIC_ID_JOB_NULL);
    }

    public String getRicIdJobNullFormatted() {
        return Functions.padBlanks(getRicIdJobNull(), Len.RIC_ID_JOB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIC_ID_JOB = 1;
        public static final int RIC_ID_JOB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIC_ID_JOB = 5;
        public static final int RIC_ID_JOB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIC_ID_JOB = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
