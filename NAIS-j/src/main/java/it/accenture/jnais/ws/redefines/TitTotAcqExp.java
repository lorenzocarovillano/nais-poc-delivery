package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-ACQ-EXP<br>
 * Variable: TIT-TOT-ACQ-EXP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_ACQ_EXP;
    }

    public void setTitTotAcqExp(AfDecimal titTotAcqExp) {
        writeDecimalAsPacked(Pos.TIT_TOT_ACQ_EXP, titTotAcqExp.copy());
    }

    public void setTitTotAcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_ACQ_EXP, Pos.TIT_TOT_ACQ_EXP);
    }

    /**Original name: TIT-TOT-ACQ-EXP<br>*/
    public AfDecimal getTitTotAcqExp() {
        return readPackedAsDecimal(Pos.TIT_TOT_ACQ_EXP, Len.Int.TIT_TOT_ACQ_EXP, Len.Fract.TIT_TOT_ACQ_EXP);
    }

    public byte[] getTitTotAcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_ACQ_EXP, Pos.TIT_TOT_ACQ_EXP);
        return buffer;
    }

    public void setTitTotAcqExpNull(String titTotAcqExpNull) {
        writeString(Pos.TIT_TOT_ACQ_EXP_NULL, titTotAcqExpNull, Len.TIT_TOT_ACQ_EXP_NULL);
    }

    /**Original name: TIT-TOT-ACQ-EXP-NULL<br>*/
    public String getTitTotAcqExpNull() {
        return readString(Pos.TIT_TOT_ACQ_EXP_NULL, Len.TIT_TOT_ACQ_EXP_NULL);
    }

    public String getTitTotAcqExpNullFormatted() {
        return Functions.padBlanks(getTitTotAcqExpNull(), Len.TIT_TOT_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_ACQ_EXP = 1;
        public static final int TIT_TOT_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_ACQ_EXP = 8;
        public static final int TIT_TOT_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
