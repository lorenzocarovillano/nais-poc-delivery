package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPB-IRPEF-K1-ANTI<br>
 * Variable: DFA-IMPB-IRPEF-K1-ANTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpbIrpefK1Anti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpbIrpefK1Anti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPB_IRPEF_K1_ANTI;
    }

    public void setDfaImpbIrpefK1Anti(AfDecimal dfaImpbIrpefK1Anti) {
        writeDecimalAsPacked(Pos.DFA_IMPB_IRPEF_K1_ANTI, dfaImpbIrpefK1Anti.copy());
    }

    public void setDfaImpbIrpefK1AntiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPB_IRPEF_K1_ANTI, Pos.DFA_IMPB_IRPEF_K1_ANTI);
    }

    /**Original name: DFA-IMPB-IRPEF-K1-ANTI<br>*/
    public AfDecimal getDfaImpbIrpefK1Anti() {
        return readPackedAsDecimal(Pos.DFA_IMPB_IRPEF_K1_ANTI, Len.Int.DFA_IMPB_IRPEF_K1_ANTI, Len.Fract.DFA_IMPB_IRPEF_K1_ANTI);
    }

    public byte[] getDfaImpbIrpefK1AntiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPB_IRPEF_K1_ANTI, Pos.DFA_IMPB_IRPEF_K1_ANTI);
        return buffer;
    }

    public void setDfaImpbIrpefK1AntiNull(String dfaImpbIrpefK1AntiNull) {
        writeString(Pos.DFA_IMPB_IRPEF_K1_ANTI_NULL, dfaImpbIrpefK1AntiNull, Len.DFA_IMPB_IRPEF_K1_ANTI_NULL);
    }

    /**Original name: DFA-IMPB-IRPEF-K1-ANTI-NULL<br>*/
    public String getDfaImpbIrpefK1AntiNull() {
        return readString(Pos.DFA_IMPB_IRPEF_K1_ANTI_NULL, Len.DFA_IMPB_IRPEF_K1_ANTI_NULL);
    }

    public String getDfaImpbIrpefK1AntiNullFormatted() {
        return Functions.padBlanks(getDfaImpbIrpefK1AntiNull(), Len.DFA_IMPB_IRPEF_K1_ANTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IRPEF_K1_ANTI = 1;
        public static final int DFA_IMPB_IRPEF_K1_ANTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IRPEF_K1_ANTI = 8;
        public static final int DFA_IMPB_IRPEF_K1_ANTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IRPEF_K1_ANTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IRPEF_K1_ANTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
