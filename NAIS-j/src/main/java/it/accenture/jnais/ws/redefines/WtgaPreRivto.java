package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-RIVTO<br>
 * Variable: WTGA-PRE-RIVTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPreRivto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPreRivto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_RIVTO;
    }

    public void setWtgaPreRivto(AfDecimal wtgaPreRivto) {
        writeDecimalAsPacked(Pos.WTGA_PRE_RIVTO, wtgaPreRivto.copy());
    }

    public void setWtgaPreRivtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_RIVTO, Pos.WTGA_PRE_RIVTO);
    }

    /**Original name: WTGA-PRE-RIVTO<br>*/
    public AfDecimal getWtgaPreRivto() {
        return readPackedAsDecimal(Pos.WTGA_PRE_RIVTO, Len.Int.WTGA_PRE_RIVTO, Len.Fract.WTGA_PRE_RIVTO);
    }

    public byte[] getWtgaPreRivtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_RIVTO, Pos.WTGA_PRE_RIVTO);
        return buffer;
    }

    public void initWtgaPreRivtoSpaces() {
        fill(Pos.WTGA_PRE_RIVTO, Len.WTGA_PRE_RIVTO, Types.SPACE_CHAR);
    }

    public void setWtgaPreRivtoNull(String wtgaPreRivtoNull) {
        writeString(Pos.WTGA_PRE_RIVTO_NULL, wtgaPreRivtoNull, Len.WTGA_PRE_RIVTO_NULL);
    }

    /**Original name: WTGA-PRE-RIVTO-NULL<br>*/
    public String getWtgaPreRivtoNull() {
        return readString(Pos.WTGA_PRE_RIVTO_NULL, Len.WTGA_PRE_RIVTO_NULL);
    }

    public String getWtgaPreRivtoNullFormatted() {
        return Functions.padBlanks(getWtgaPreRivtoNull(), Len.WTGA_PRE_RIVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_RIVTO = 1;
        public static final int WTGA_PRE_RIVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_RIVTO = 8;
        public static final int WTGA_PRE_RIVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_RIVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_RIVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
