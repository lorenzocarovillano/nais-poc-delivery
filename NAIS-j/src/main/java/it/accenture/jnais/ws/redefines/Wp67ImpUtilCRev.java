package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-IMP-UTIL-C-REV<br>
 * Variable: WP67-IMP-UTIL-C-REV from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67ImpUtilCRev extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67ImpUtilCRev() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_IMP_UTIL_C_REV;
    }

    public void setWp67ImpUtilCRev(AfDecimal wp67ImpUtilCRev) {
        writeDecimalAsPacked(Pos.WP67_IMP_UTIL_C_REV, wp67ImpUtilCRev.copy());
    }

    public void setWp67ImpUtilCRevFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_IMP_UTIL_C_REV, Pos.WP67_IMP_UTIL_C_REV);
    }

    /**Original name: WP67-IMP-UTIL-C-REV<br>*/
    public AfDecimal getWp67ImpUtilCRev() {
        return readPackedAsDecimal(Pos.WP67_IMP_UTIL_C_REV, Len.Int.WP67_IMP_UTIL_C_REV, Len.Fract.WP67_IMP_UTIL_C_REV);
    }

    public byte[] getWp67ImpUtilCRevAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_IMP_UTIL_C_REV, Pos.WP67_IMP_UTIL_C_REV);
        return buffer;
    }

    public void setWp67ImpUtilCRevNull(String wp67ImpUtilCRevNull) {
        writeString(Pos.WP67_IMP_UTIL_C_REV_NULL, wp67ImpUtilCRevNull, Len.WP67_IMP_UTIL_C_REV_NULL);
    }

    /**Original name: WP67-IMP-UTIL-C-REV-NULL<br>*/
    public String getWp67ImpUtilCRevNull() {
        return readString(Pos.WP67_IMP_UTIL_C_REV_NULL, Len.WP67_IMP_UTIL_C_REV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_IMP_UTIL_C_REV = 1;
        public static final int WP67_IMP_UTIL_C_REV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_IMP_UTIL_C_REV = 8;
        public static final int WP67_IMP_UTIL_C_REV_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_IMP_UTIL_C_REV = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_IMP_UTIL_C_REV = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
