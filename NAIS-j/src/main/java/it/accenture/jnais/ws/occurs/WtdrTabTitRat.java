package it.accenture.jnais.ws.occurs;

import it.accenture.jnais.copy.Lccvtdr1;

/**Original name: WTDR-TAB-TIT-RAT<br>
 * Variables: WTDR-TAB-TIT-RAT from copybook LCCVTDRA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WtdrTabTitRat {

    //==== PROPERTIES ====
    //Original name: LCCVTDR1
    private Lccvtdr1 lccvtdr1 = new Lccvtdr1();

    //==== METHODS ====
    public Lccvtdr1 getLccvtdr1() {
        return lccvtdr1;
    }
}
