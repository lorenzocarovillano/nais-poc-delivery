package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-PROV-INC<br>
 * Variable: DTC-PROV-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_PROV_INC;
    }

    public void setDtcProvInc(AfDecimal dtcProvInc) {
        writeDecimalAsPacked(Pos.DTC_PROV_INC, dtcProvInc.copy());
    }

    public void setDtcProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_PROV_INC, Pos.DTC_PROV_INC);
    }

    /**Original name: DTC-PROV-INC<br>*/
    public AfDecimal getDtcProvInc() {
        return readPackedAsDecimal(Pos.DTC_PROV_INC, Len.Int.DTC_PROV_INC, Len.Fract.DTC_PROV_INC);
    }

    public byte[] getDtcProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_PROV_INC, Pos.DTC_PROV_INC);
        return buffer;
    }

    public void setDtcProvIncNull(String dtcProvIncNull) {
        writeString(Pos.DTC_PROV_INC_NULL, dtcProvIncNull, Len.DTC_PROV_INC_NULL);
    }

    /**Original name: DTC-PROV-INC-NULL<br>*/
    public String getDtcProvIncNull() {
        return readString(Pos.DTC_PROV_INC_NULL, Len.DTC_PROV_INC_NULL);
    }

    public String getDtcProvIncNullFormatted() {
        return Functions.padBlanks(getDtcProvIncNull(), Len.DTC_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_PROV_INC = 1;
        public static final int DTC_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_PROV_INC = 8;
        public static final int DTC_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
