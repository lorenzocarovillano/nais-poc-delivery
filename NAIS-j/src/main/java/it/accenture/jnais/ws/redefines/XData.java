package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: X-DATA<br>
 * Variable: X-DATA from program LCCS0004<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class XData extends BytesClass {

    //==== CONSTRUCTORS ====
    public XData() {
    }

    public XData(byte[] data) {
        super(data);
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.X_DATA;
    }

    public void setxDataBytes(byte[] buffer) {
        setxDataBytes(buffer, 1);
    }

    /**Original name: X-DATA<br>*/
    public byte[] getxDataBytes() {
        byte[] buffer = new byte[Len.X_DATA];
        return getxDataBytes(buffer, 1);
    }

    public void setxDataBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.X_DATA, Pos.X_DATA);
    }

    public byte[] getxDataBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.X_DATA, Pos.X_DATA);
        return buffer;
    }

    /**Original name: X-GG<br>*/
    public short getGg() {
        return readNumDispUnsignedShort(Pos.GG, Len.GG);
    }

    public String getGgFormatted() {
        return readFixedString(Pos.GG, Len.GG);
    }

    /**Original name: X-MM<br>*/
    public short getMm() {
        return readShort(Pos.MM, Len.Int.MM, SignType.NO_SIGN);
    }

    /**Original name: X-AAAA<br>*/
    public short getAaaa() {
        return readNumDispUnsignedShort(Pos.AAAA, Len.AAAA);
    }

    public String getAaaaFormatted() {
        return readFixedString(Pos.AAAA, Len.AAAA);
    }

    public String getAa1Formatted() {
        return readFixedString(Pos.AA1, Len.AA1);
    }

    public String getAa2Formatted() {
        return readFixedString(Pos.AA2, Len.AA2);
    }

    public String getxDatanumFormatted() {
        return readFixedString(Pos.X_DATANUM, Len.X_DATANUM);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int X_DATA = 1;
        public static final int GG = X_DATA;
        public static final int MM = GG + Len.GG;
        public static final int AAAA = MM + Len.MM;
        public static final int FLR1 = AAAA;
        public static final int AA1 = FLR1;
        public static final int AA2 = AA1 + Len.AA1;
        public static final int X_DATANUM = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GG = 2;
        public static final int MM = 2;
        public static final int AA1 = 2;
        public static final int AAAA = 4;
        public static final int X_DATA = GG + MM + AAAA;
        public static final int X_DATANUM = 8;
        public static final int AA2 = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MM = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
