package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.Lccc0023TabMovFuturi;

/**Original name: AREA-IO-LCCS0023<br>
 * Variable: AREA-IO-LCCS0023 from program LCCS0023<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoLccs0023 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_MOV_FUTURI_MAXOCCURS = 30;
    //Original name: LCCC0023-ID-OGG-PTF
    private int idOggPtf = DefaultValues.INT_VAL;
    //Original name: LCCC0023-TP-OGG-PTF
    private String tpOggPtf = DefaultValues.stringVal(Len.TP_OGG_PTF);
    //Original name: LCCC0023-PRE-GRAV-BLOCCANTE
    private char preGravBloccante = DefaultValues.CHAR_VAL;
    //Original name: LCCC0023-PRE-GRAV-ANNULLABILE
    private char preGravAnnullabile = DefaultValues.CHAR_VAL;
    //Original name: LCCC0023-ELE-MOV-FUTURI-MAX
    private short eleMovFuturiMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCC0023-TAB-MOV-FUTURI
    private Lccc0023TabMovFuturi[] tabMovFuturi = new Lccc0023TabMovFuturi[TAB_MOV_FUTURI_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public AreaIoLccs0023() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IO_LCCS0023;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIoLccs0023Bytes(buf);
    }

    public void init() {
        for (int tabMovFuturiIdx = 1; tabMovFuturiIdx <= TAB_MOV_FUTURI_MAXOCCURS; tabMovFuturiIdx++) {
            tabMovFuturi[tabMovFuturiIdx - 1] = new Lccc0023TabMovFuturi();
        }
    }

    public String getAreaIoLccs0023Formatted() {
        return MarshalByteExt.bufferToStr(getAreaIoLccs0023Bytes());
    }

    public void setAreaIoLccs0023Bytes(byte[] buffer) {
        setAreaIoLccs0023Bytes(buffer, 1);
    }

    public byte[] getAreaIoLccs0023Bytes() {
        byte[] buffer = new byte[Len.AREA_IO_LCCS0023];
        return getAreaIoLccs0023Bytes(buffer, 1);
    }

    public void setAreaIoLccs0023Bytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
    }

    public byte[] getAreaIoLccs0023Bytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        idOggPtf = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG_PTF, 0);
        position += Len.ID_OGG_PTF;
        tpOggPtf = MarshalByte.readString(buffer, position, Len.TP_OGG_PTF);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOggPtf, Len.Int.ID_OGG_PTF, 0);
        position += Len.ID_OGG_PTF;
        MarshalByte.writeString(buffer, position, tpOggPtf, Len.TP_OGG_PTF);
        return buffer;
    }

    public void setIdOggPtf(int idOggPtf) {
        this.idOggPtf = idOggPtf;
    }

    public int getIdOggPtf() {
        return this.idOggPtf;
    }

    public void setTpOggPtf(String tpOggPtf) {
        this.tpOggPtf = Functions.subString(tpOggPtf, Len.TP_OGG_PTF);
    }

    public String getTpOggPtf() {
        return this.tpOggPtf;
    }

    public String getTpOggPtfFormatted() {
        return Functions.padBlanks(getTpOggPtf(), Len.TP_OGG_PTF);
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        preGravBloccante = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        preGravAnnullabile = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        eleMovFuturiMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_MOV_FUTURI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabMovFuturi[idx - 1].setTabMovFuturiBytes(buffer, position);
                position += Lccc0023TabMovFuturi.Len.TAB_MOV_FUTURI;
            }
            else {
                tabMovFuturi[idx - 1].initTabMovFuturiSpaces();
                position += Lccc0023TabMovFuturi.Len.TAB_MOV_FUTURI;
            }
        }
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, preGravBloccante);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, preGravAnnullabile);
        position += Types.CHAR_SIZE;
        MarshalByte.writeBinaryShort(buffer, position, eleMovFuturiMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_MOV_FUTURI_MAXOCCURS; idx++) {
            tabMovFuturi[idx - 1].getTabMovFuturiBytes(buffer, position);
            position += Lccc0023TabMovFuturi.Len.TAB_MOV_FUTURI;
        }
        return buffer;
    }

    public void setPreGravBloccante(char preGravBloccante) {
        this.preGravBloccante = preGravBloccante;
    }

    public void setPreGravBloccanteFormatted(String preGravBloccante) {
        setPreGravBloccante(Functions.charAt(preGravBloccante, Types.CHAR_SIZE));
    }

    public char getPreGravBloccante() {
        return this.preGravBloccante;
    }

    public void setPreGravAnnullabile(char preGravAnnullabile) {
        this.preGravAnnullabile = preGravAnnullabile;
    }

    public void setPreGravAnnullabileFormatted(String preGravAnnullabile) {
        setPreGravAnnullabile(Functions.charAt(preGravAnnullabile, Types.CHAR_SIZE));
    }

    public char getPreGravAnnullabile() {
        return this.preGravAnnullabile;
    }

    public void setEleMovFuturiMax(short eleMovFuturiMax) {
        this.eleMovFuturiMax = eleMovFuturiMax;
    }

    public short getEleMovFuturiMax() {
        return this.eleMovFuturiMax;
    }

    public Lccc0023TabMovFuturi getTabMovFuturi(int idx) {
        return tabMovFuturi[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getAreaIoLccs0023Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG_PTF = 5;
        public static final int TP_OGG_PTF = 2;
        public static final int DATI_INPUT = ID_OGG_PTF + TP_OGG_PTF;
        public static final int PRE_GRAV_BLOCCANTE = 1;
        public static final int PRE_GRAV_ANNULLABILE = 1;
        public static final int ELE_MOV_FUTURI_MAX = 2;
        public static final int DATI_OUTPUT = PRE_GRAV_BLOCCANTE + PRE_GRAV_ANNULLABILE + ELE_MOV_FUTURI_MAX + AreaIoLccs0023.TAB_MOV_FUTURI_MAXOCCURS * Lccc0023TabMovFuturi.Len.TAB_MOV_FUTURI;
        public static final int AREA_IO_LCCS0023 = DATI_INPUT + DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
