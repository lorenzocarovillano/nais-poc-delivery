package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-ALQ-RETR-T<br>
 * Variable: W-B03-ALQ-RETR-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03AlqRetrTLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03AlqRetrTLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_ALQ_RETR_T;
    }

    public void setwB03AlqRetrT(AfDecimal wB03AlqRetrT) {
        writeDecimalAsPacked(Pos.W_B03_ALQ_RETR_T, wB03AlqRetrT.copy());
    }

    /**Original name: W-B03-ALQ-RETR-T<br>*/
    public AfDecimal getwB03AlqRetrT() {
        return readPackedAsDecimal(Pos.W_B03_ALQ_RETR_T, Len.Int.W_B03_ALQ_RETR_T, Len.Fract.W_B03_ALQ_RETR_T);
    }

    public byte[] getwB03AlqRetrTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_ALQ_RETR_T, Pos.W_B03_ALQ_RETR_T);
        return buffer;
    }

    public void setwB03AlqRetrTNull(String wB03AlqRetrTNull) {
        writeString(Pos.W_B03_ALQ_RETR_T_NULL, wB03AlqRetrTNull, Len.W_B03_ALQ_RETR_T_NULL);
    }

    /**Original name: W-B03-ALQ-RETR-T-NULL<br>*/
    public String getwB03AlqRetrTNull() {
        return readString(Pos.W_B03_ALQ_RETR_T_NULL, Len.W_B03_ALQ_RETR_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_ALQ_RETR_T = 1;
        public static final int W_B03_ALQ_RETR_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_ALQ_RETR_T = 4;
        public static final int W_B03_ALQ_RETR_T_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_ALQ_RETR_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_ALQ_RETR_T = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
