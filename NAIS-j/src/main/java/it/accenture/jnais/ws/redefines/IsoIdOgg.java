package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-ID-OGG<br>
 * Variable: ISO-ID-OGG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoIdOgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoIdOgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_ID_OGG;
    }

    public void setIsoIdOgg(int isoIdOgg) {
        writeIntAsPacked(Pos.ISO_ID_OGG, isoIdOgg, Len.Int.ISO_ID_OGG);
    }

    public void setIsoIdOggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_ID_OGG, Pos.ISO_ID_OGG);
    }

    /**Original name: ISO-ID-OGG<br>*/
    public int getIsoIdOgg() {
        return readPackedAsInt(Pos.ISO_ID_OGG, Len.Int.ISO_ID_OGG);
    }

    public byte[] getIsoIdOggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_ID_OGG, Pos.ISO_ID_OGG);
        return buffer;
    }

    public void setIsoIdOggNull(String isoIdOggNull) {
        writeString(Pos.ISO_ID_OGG_NULL, isoIdOggNull, Len.ISO_ID_OGG_NULL);
    }

    /**Original name: ISO-ID-OGG-NULL<br>*/
    public String getIsoIdOggNull() {
        return readString(Pos.ISO_ID_OGG_NULL, Len.ISO_ID_OGG_NULL);
    }

    public String getIsoIdOggNullFormatted() {
        return Functions.padBlanks(getIsoIdOggNull(), Len.ISO_ID_OGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_ID_OGG = 1;
        public static final int ISO_ID_OGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_ID_OGG = 5;
        public static final int ISO_ID_OGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
