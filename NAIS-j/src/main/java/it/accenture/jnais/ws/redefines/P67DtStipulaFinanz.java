package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-DT-STIPULA-FINANZ<br>
 * Variable: P67-DT-STIPULA-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67DtStipulaFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67DtStipulaFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_DT_STIPULA_FINANZ;
    }

    public void setP67DtStipulaFinanz(int p67DtStipulaFinanz) {
        writeIntAsPacked(Pos.P67_DT_STIPULA_FINANZ, p67DtStipulaFinanz, Len.Int.P67_DT_STIPULA_FINANZ);
    }

    public void setP67DtStipulaFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_DT_STIPULA_FINANZ, Pos.P67_DT_STIPULA_FINANZ);
    }

    /**Original name: P67-DT-STIPULA-FINANZ<br>*/
    public int getP67DtStipulaFinanz() {
        return readPackedAsInt(Pos.P67_DT_STIPULA_FINANZ, Len.Int.P67_DT_STIPULA_FINANZ);
    }

    public byte[] getP67DtStipulaFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_DT_STIPULA_FINANZ, Pos.P67_DT_STIPULA_FINANZ);
        return buffer;
    }

    public void setP67DtStipulaFinanzNull(String p67DtStipulaFinanzNull) {
        writeString(Pos.P67_DT_STIPULA_FINANZ_NULL, p67DtStipulaFinanzNull, Len.P67_DT_STIPULA_FINANZ_NULL);
    }

    /**Original name: P67-DT-STIPULA-FINANZ-NULL<br>*/
    public String getP67DtStipulaFinanzNull() {
        return readString(Pos.P67_DT_STIPULA_FINANZ_NULL, Len.P67_DT_STIPULA_FINANZ_NULL);
    }

    public String getP67DtStipulaFinanzNullFormatted() {
        return Functions.padBlanks(getP67DtStipulaFinanzNull(), Len.P67_DT_STIPULA_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_DT_STIPULA_FINANZ = 1;
        public static final int P67_DT_STIPULA_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_DT_STIPULA_FINANZ = 5;
        public static final int P67_DT_STIPULA_FINANZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_DT_STIPULA_FINANZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
