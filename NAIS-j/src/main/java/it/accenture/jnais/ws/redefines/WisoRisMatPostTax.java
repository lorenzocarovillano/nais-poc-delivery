package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WISO-RIS-MAT-POST-TAX<br>
 * Variable: WISO-RIS-MAT-POST-TAX from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoRisMatPostTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoRisMatPostTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_RIS_MAT_POST_TAX;
    }

    public void setWisoRisMatPostTax(AfDecimal wisoRisMatPostTax) {
        writeDecimalAsPacked(Pos.WISO_RIS_MAT_POST_TAX, wisoRisMatPostTax.copy());
    }

    public void setWisoRisMatPostTaxFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_RIS_MAT_POST_TAX, Pos.WISO_RIS_MAT_POST_TAX);
    }

    /**Original name: WISO-RIS-MAT-POST-TAX<br>*/
    public AfDecimal getWisoRisMatPostTax() {
        return readPackedAsDecimal(Pos.WISO_RIS_MAT_POST_TAX, Len.Int.WISO_RIS_MAT_POST_TAX, Len.Fract.WISO_RIS_MAT_POST_TAX);
    }

    public byte[] getWisoRisMatPostTaxAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_RIS_MAT_POST_TAX, Pos.WISO_RIS_MAT_POST_TAX);
        return buffer;
    }

    public void initWisoRisMatPostTaxSpaces() {
        fill(Pos.WISO_RIS_MAT_POST_TAX, Len.WISO_RIS_MAT_POST_TAX, Types.SPACE_CHAR);
    }

    public void setWisoRisMatPostTaxNull(String wisoRisMatPostTaxNull) {
        writeString(Pos.WISO_RIS_MAT_POST_TAX_NULL, wisoRisMatPostTaxNull, Len.WISO_RIS_MAT_POST_TAX_NULL);
    }

    /**Original name: WISO-RIS-MAT-POST-TAX-NULL<br>*/
    public String getWisoRisMatPostTaxNull() {
        return readString(Pos.WISO_RIS_MAT_POST_TAX_NULL, Len.WISO_RIS_MAT_POST_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_RIS_MAT_POST_TAX = 1;
        public static final int WISO_RIS_MAT_POST_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_RIS_MAT_POST_TAX = 8;
        public static final int WISO_RIS_MAT_POST_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WISO_RIS_MAT_POST_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_RIS_MAT_POST_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
