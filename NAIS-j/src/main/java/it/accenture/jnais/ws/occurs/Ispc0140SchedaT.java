package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Ispc0140FlgItn;
import it.accenture.jnais.ws.enums.Lccc006TrattDati;

/**Original name: ISPC0140-SCHEDA-T<br>
 * Variables: ISPC0140-SCHEDA-T from copybook ISPC0140<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0140SchedaT {

    //==== PROPERTIES ====
    public static final int AREA_VARIABILI_T_MAXOCCURS = 75;
    //Original name: ISPC0140-TIPO-LIVELLO-T
    private char tipoLivelloT = DefaultValues.CHAR_VAL;
    //Original name: ISPC0140-CODICE-LIVELLO-T
    private String codiceLivelloT = DefaultValues.stringVal(Len.CODICE_LIVELLO_T);
    //Original name: ISPC0140-GARANZIA-INCLUSA-T
    private char garanziaInclusaT = DefaultValues.CHAR_VAL;
    //Original name: ISPC0140-TIPO-PREMIO-T
    private char tipoPremioT = DefaultValues.CHAR_VAL;
    //Original name: ISPC0140-TIPO-TRCH
    private String tipoTrch = DefaultValues.stringVal(Len.TIPO_TRCH);
    //Original name: ISPC0140-FLG-ITN
    private Ispc0140FlgItn flgItn = new Ispc0140FlgItn();
    //Original name: ISPC0140-FLAG-MOD-CALC-PRE-T
    private Lccc006TrattDati flagModCalcPreT = new Lccc006TrattDati();
    //Original name: ISPC0140-PERC-RIP-PREMIO-T
    private AfDecimal percRipPremioT = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: ISPC0140-NUM-COMPON-MAX-ELE-T
    private String numComponMaxEleT = DefaultValues.stringVal(Len.NUM_COMPON_MAX_ELE_T);
    //Original name: ISPC0140-AREA-VARIABILI-T
    private Ispc0140AreaVariabiliP[] areaVariabiliT = new Ispc0140AreaVariabiliP[AREA_VARIABILI_T_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ispc0140SchedaT() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int areaVariabiliTIdx = 1; areaVariabiliTIdx <= AREA_VARIABILI_T_MAXOCCURS; areaVariabiliTIdx++) {
            areaVariabiliT[areaVariabiliTIdx - 1] = new Ispc0140AreaVariabiliP();
        }
    }

    public void setSchedaTBytes(byte[] buffer, int offset) {
        int position = offset;
        tipoLivelloT = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        codiceLivelloT = MarshalByte.readString(buffer, position, Len.CODICE_LIVELLO_T);
        position += Len.CODICE_LIVELLO_T;
        garanziaInclusaT = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tipoPremioT = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tipoTrch = MarshalByte.readFixedString(buffer, position, Len.TIPO_TRCH);
        position += Len.TIPO_TRCH;
        flgItn.value = MarshalByte.readFixedString(buffer, position, Ispc0140FlgItn.Len.FLG_ITN);
        position += Ispc0140FlgItn.Len.FLG_ITN;
        flagModCalcPreT.setFlagModCalcPreP(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        percRipPremioT.assign(MarshalByte.readDecimal(buffer, position, Len.Int.PERC_RIP_PREMIO_T, Len.Fract.PERC_RIP_PREMIO_T));
        position += Len.PERC_RIP_PREMIO_T;
        numComponMaxEleT = MarshalByte.readFixedString(buffer, position, Len.NUM_COMPON_MAX_ELE_T);
        position += Len.NUM_COMPON_MAX_ELE_T;
        for (int idx = 1; idx <= AREA_VARIABILI_T_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                areaVariabiliT[idx - 1].setAreaVariabiliPBytes(buffer, position);
                position += Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;
            }
            else {
                areaVariabiliT[idx - 1].initAreaVariabiliPSpaces();
                position += Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;
            }
        }
    }

    public byte[] getSchedaTBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, tipoLivelloT);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codiceLivelloT, Len.CODICE_LIVELLO_T);
        position += Len.CODICE_LIVELLO_T;
        MarshalByte.writeChar(buffer, position, garanziaInclusaT);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, tipoPremioT);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tipoTrch, Len.TIPO_TRCH);
        position += Len.TIPO_TRCH;
        MarshalByte.writeString(buffer, position, flgItn.value, Ispc0140FlgItn.Len.FLG_ITN);
        position += Ispc0140FlgItn.Len.FLG_ITN;
        MarshalByte.writeChar(buffer, position, flagModCalcPreT.getFlagModCalcPreP());
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimal(buffer, position, percRipPremioT.copy());
        position += Len.PERC_RIP_PREMIO_T;
        MarshalByte.writeString(buffer, position, numComponMaxEleT, Len.NUM_COMPON_MAX_ELE_T);
        position += Len.NUM_COMPON_MAX_ELE_T;
        for (int idx = 1; idx <= AREA_VARIABILI_T_MAXOCCURS; idx++) {
            areaVariabiliT[idx - 1].getAreaVariabiliPBytes(buffer, position);
            position += Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;
        }
        return buffer;
    }

    public void initSchedaTSpaces() {
        tipoLivelloT = Types.SPACE_CHAR;
        codiceLivelloT = "";
        garanziaInclusaT = Types.SPACE_CHAR;
        tipoPremioT = Types.SPACE_CHAR;
        tipoTrch = "";
        flgItn.setFlgItn(Types.INVALID_SHORT_VAL);
        flagModCalcPreT.setFlagModCalcPreP(Types.SPACE_CHAR);
        percRipPremioT.setNaN();
        numComponMaxEleT = "";
        for (int idx = 1; idx <= AREA_VARIABILI_T_MAXOCCURS; idx++) {
            areaVariabiliT[idx - 1].initAreaVariabiliPSpaces();
        }
    }

    public void setTipoLivelloT(char tipoLivelloT) {
        this.tipoLivelloT = tipoLivelloT;
    }

    public char getTipoLivelloT() {
        return this.tipoLivelloT;
    }

    public void setCodiceLivelloT(String codiceLivelloT) {
        this.codiceLivelloT = Functions.subString(codiceLivelloT, Len.CODICE_LIVELLO_T);
    }

    public String getCodiceLivelloT() {
        return this.codiceLivelloT;
    }

    public void setGaranziaInclusaT(char garanziaInclusaT) {
        this.garanziaInclusaT = garanziaInclusaT;
    }

    public char getGaranziaInclusaT() {
        return this.garanziaInclusaT;
    }

    public void setTipoPremioT(char tipoPremioT) {
        this.tipoPremioT = tipoPremioT;
    }

    public char getTipoPremioT() {
        return this.tipoPremioT;
    }

    public void setIspc0140TipoTrchFormatted(String ispc0140TipoTrch) {
        this.tipoTrch = Trunc.toUnsignedNumeric(ispc0140TipoTrch, Len.TIPO_TRCH);
    }

    public short getIspc0140TipoTrch() {
        return NumericDisplay.asShort(this.tipoTrch);
    }

    public void setPercRipPremioT(AfDecimal percRipPremioT) {
        this.percRipPremioT.assign(percRipPremioT);
    }

    public AfDecimal getPercRipPremioT() {
        return this.percRipPremioT.copy();
    }

    public void setIspc0140NumComponMaxEleT(short ispc0140NumComponMaxEleT) {
        this.numComponMaxEleT = NumericDisplay.asString(ispc0140NumComponMaxEleT, Len.NUM_COMPON_MAX_ELE_T);
    }

    public short getIspc0140NumComponMaxEleT() {
        return NumericDisplay.asShort(this.numComponMaxEleT);
    }

    public Ispc0140AreaVariabiliP getAreaVariabiliT(int idx) {
        return areaVariabiliT[idx - 1];
    }

    public Lccc006TrattDati getFlagModCalcPreT() {
        return flagModCalcPreT;
    }

    public Ispc0140FlgItn getFlgItn() {
        return flgItn;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_LIVELLO_T = 1;
        public static final int CODICE_LIVELLO_T = 12;
        public static final int GARANZIA_INCLUSA_T = 1;
        public static final int TIPO_PREMIO_T = 1;
        public static final int TIPO_TRCH = 2;
        public static final int PERC_RIP_PREMIO_T = 6;
        public static final int NUM_COMPON_MAX_ELE_T = 3;
        public static final int SCHEDA_T = TIPO_LIVELLO_T + CODICE_LIVELLO_T + GARANZIA_INCLUSA_T + TIPO_PREMIO_T + TIPO_TRCH + Ispc0140FlgItn.Len.FLG_ITN + Lccc006TrattDati.Len.FLAG_MOD_CALC_PRE_P + PERC_RIP_PREMIO_T + NUM_COMPON_MAX_ELE_T + Ispc0140SchedaT.AREA_VARIABILI_T_MAXOCCURS * Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PERC_RIP_PREMIO_T = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PERC_RIP_PREMIO_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
